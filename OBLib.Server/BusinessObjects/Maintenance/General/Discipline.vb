﻿' Generated 05 Jul 2014 12:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class Discipline
    Inherits SingularBusinessBase(Of Discipline)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "DisciplineBO.DisciplineBOToString(self)")

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="Description for the Discipline"),
    Required(ErrorMessage:="Discipline required"),
    StringLength(50, ErrorMessage:="Discipline cannot be more than 50 characters")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared PositionRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PositionRequiredInd, "Position Required", False)
    ''' <summary>
    ''' Gets and sets the Position Required value
    ''' </summary>
    <Display(Name:="Position Required", Description:="Tick indicates that Human Resource Skill Position(s) are required when creating a Human Resource Skill for a person"),
    Required(ErrorMessage:="Position Required required")>
    Public Property PositionRequiredInd() As Boolean
      Get
        Return GetProperty(PositionRequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PositionRequiredIndProperty, Value)
      End Set
    End Property

    Public Shared SkillLevelRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SkillLevelRequiredInd, "Skill Level Required", False)
    ''' <summary>
    ''' Gets and sets the Skill Level Required value
    ''' </summary>
    <Display(Name:="Skill Level Required", Description:="Tick indicates that a Skill Level is required for Human Resource Skill(s) that are linked to this discipline"),
    Required(ErrorMessage:="Skill Level Required required")>
    Public Property SkillLevelRequiredInd() As Boolean
      Get
        Return GetProperty(SkillLevelRequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SkillLevelRequiredIndProperty, Value)
      End Set
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", False)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="Tick indicates the a discipline is a system discipline and may not be modified"),
    Required(ErrorMessage:="System required"),
    Browsable(False)>
    Public Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SystemIndProperty, Value)
      End Set
    End Property

    Public Shared OrderNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNo, "Order No", 0)
    ''' <summary>
    ''' Gets and sets the Order No value
    ''' </summary>
    <Display(Name:="Order No", Description:=""),
    Required(ErrorMessage:="Order No required")>
    Public Property OrderNo() As Integer
      Get
        Return GetProperty(OrderNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OrderNoProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSystemList)),
    Browsable(False)>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ShiftWorkerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShiftWorkerInd, "Shift Worker", False)
    ''' <summary>
    ''' Gets and sets the Shift Worker value
    ''' </summary>
    <Display(Name:="Shift Worker", Description:="")>
    Public Property ShiftWorkerInd() As Boolean
      Get
        Return GetProperty(ShiftWorkerIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ShiftWorkerIndProperty, Value)
      End Set
    End Property

    Public Shared NonShiftWorkerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.NonShiftWorkerInd, "Non-Shift Worker", False)
    ''' <summary>
    ''' Gets and sets the Non-Shift Worker value
    ''' </summary>
    <Display(Name:="Non-Shift Worker", Description:="")>
    Public Property NonShiftWorkerInd() As Boolean
      Get
        Return GetProperty(NonShiftWorkerIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(NonShiftWorkerIndProperty, Value)
      End Set
    End Property

    Public Shared OperationalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OperationalInd, "Operational", False)
    ''' <summary>
    ''' Gets and sets the Operational value
    ''' </summary>
    <Display(Name:="Operational", Description:="")>
    Public Property OperationalInd() As Boolean
      Get
        Return GetProperty(OperationalIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OperationalIndProperty, Value)
      End Set
    End Property

    Public Shared ManagerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ManagerInd, "Manager", False)
    ''' <summary>
    ''' Gets and sets the Manager value
    ''' </summary>
    <Display(Name:="Manager", Description:="")>
    Public Property ManagerInd() As Boolean
      Get
        Return GetProperty(ManagerIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ManagerIndProperty, Value)
      End Set
    End Property

    Public Shared MealReimbursementManagerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MealReimbursementManagerInd, "Meal Reimbursement", False)
    ''' <summary>
    ''' Gets and sets the Meal Reimbursement value
    ''' </summary>
    <Display(Name:="Meal Reimbursement", Description:="")>
    Public Property MealReimbursementManagerInd() As Boolean
      Get
        Return GetProperty(MealReimbursementManagerIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(MealReimbursementManagerIndProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared PositionListProperty As PropertyInfo(Of PositionList) = RegisterProperty(Of PositionList)(Function(c) c.PositionList, "Position List")

    Public ReadOnly Property PositionList() As PositionList
      Get
        If GetProperty(PositionListProperty) Is Nothing Then
          LoadProperty(PositionListProperty, Maintenance.General.PositionList.NewPositionList())
        End If
        Return GetProperty(PositionListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DisciplineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Discipline.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Discipline")
        Else
          Return String.Format("Blank {0}", "Discipline")
        End If
      Else
        Return Me.Discipline
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"Positions"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(OrderNoProperty)
        .JavascriptRuleFunctionName = "DisciplineBO.OrderNoValid"
        .ServerRuleFunction = AddressOf OrderNoValid
      End With

    End Sub

    Public Shared Function OrderNoValid(Discipline As Discipline) As String
      Dim ErrorString = ""
      If Discipline.OrderNo < 1 Then
        ErrorString = "Order Number cannot be less then 1"
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewDiscipline() method.

    End Sub

    Public Shared Function NewDiscipline() As Discipline

      Return DataPortal.CreateChild(Of Discipline)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetDiscipline(dr As SafeDataReader) As Discipline

      Dim d As New Discipline()
      d.Fetch(dr)
      Return d

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(DisciplineIDProperty, .GetInt32(0))
          LoadProperty(DisciplineProperty, .GetString(1))
          LoadProperty(PositionRequiredIndProperty, .GetBoolean(2))
          LoadProperty(SkillLevelRequiredIndProperty, .GetBoolean(3))
          LoadProperty(SystemIndProperty, .GetBoolean(4))
          LoadProperty(OrderNoProperty, .GetInt32(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(ShiftWorkerIndProperty, .GetBoolean(11))
          LoadProperty(NonShiftWorkerIndProperty, .GetBoolean(12))
          LoadProperty(OperationalIndProperty, .GetBoolean(13))
          LoadProperty(ManagerIndProperty, .GetBoolean(14))
          LoadProperty(MealReimbursementManagerIndProperty, .GetBoolean(15))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insDiscipline"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updDiscipline"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramDisciplineID As SqlParameter = .Parameters.Add("@DisciplineID", SqlDbType.Int)
          paramDisciplineID.Value = GetProperty(DisciplineIDProperty)
          If Me.IsNew Then
            paramDisciplineID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Discipline", GetProperty(DisciplineProperty))
          .Parameters.AddWithValue("@PositionRequiredInd", GetProperty(PositionRequiredIndProperty))
          .Parameters.AddWithValue("@SkillLevelRequiredInd", GetProperty(SkillLevelRequiredIndProperty))
          .Parameters.AddWithValue("@SystemInd", GetProperty(SystemIndProperty))
          .Parameters.AddWithValue("@OrderNo", GetProperty(OrderNoProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ShiftWorkerInd", Singular.Misc.NothingDBNull(GetProperty(ShiftWorkerIndProperty)))
          .Parameters.AddWithValue("@NonShiftWorkerInd", Singular.Misc.NothingDBNull(GetProperty(NonShiftWorkerIndProperty)))
          .Parameters.AddWithValue("@OperationalInd", Singular.Misc.NothingDBNull(GetProperty(OperationalIndProperty)))
          .Parameters.AddWithValue("@ManagerInd", Singular.Misc.NothingDBNull(GetProperty(ManagerIndProperty)))
          .Parameters.AddWithValue("@MealReimbursementManagerInd", Singular.Misc.NothingDBNull(GetProperty(MealReimbursementManagerIndProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(DisciplineIDProperty, paramDisciplineID.Value)
          End If
          ' update child objects
          If GetProperty(PositionListProperty) IsNot Nothing Then
            Me.PositionList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(PositionListProperty) IsNot Nothing Then
          Me.PositionList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delDiscipline"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace