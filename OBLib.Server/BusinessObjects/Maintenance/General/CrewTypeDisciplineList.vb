﻿' Generated 29 Jun 2014 16:58 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class CrewTypeDisciplineList
    Inherits SingularBusinessListBase(Of CrewTypeDisciplineList, CrewTypeDiscipline)

#Region " Business Methods "

    Public Function GetItem(CrewTypeDisciplineID As Integer) As CrewTypeDiscipline

      For Each child As CrewTypeDiscipline In Me
        If child.CrewTypeDisciplineID = CrewTypeDisciplineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Crew Type Disciplines"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewCrewTypeDisciplineList() As CrewTypeDisciplineList

      Return New CrewTypeDisciplineList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As CrewTypeDiscipline In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As CrewTypeDiscipline In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace