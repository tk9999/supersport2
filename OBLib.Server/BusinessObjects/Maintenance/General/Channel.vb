﻿' Generated 13 Feb 2014 08:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance

  <Serializable()> _
  Public Class Channel
    Inherits SingularBusinessBase(Of Channel)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ChannelID() As Integer
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
    End Property

    Public Shared ChannelNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelName, "Channel Name", "")
    ''' <summary>
    ''' Gets and sets the Channel Name value
    ''' </summary>
    <Display(Name:="Channel Name", Description:="The name of the channel"),
    Required(ErrorMessage:="Channel Name required"),
    StringLength(100, ErrorMessage:="Channel Name cannot be more than 100 characters")>
    Public Property ChannelName() As String
      Get
        Return GetProperty(ChannelNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelNameProperty, Value)
      End Set
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel Short Name", "")
    ''' <summary>
    ''' Gets and sets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel Short Name", Description:="abbreviated version of channel name, similar to a channel code"),
    Required(ErrorMessage:="Channel short name required"),
    StringLength(10, ErrorMessage:="Channel Short Name cannot be more than 10 characters")>
    Public Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelShortNameProperty, Value)
      End Set
    End Property

    Public Shared HDIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HDInd, "HD", False)
    ''' <summary>
    ''' Gets and sets the HD value
    ''' </summary>
    <Display(Name:="HD", Description:="Is this channel an HD channel?"),
    Required(ErrorMessage:="HD required")>
    Public Property HDInd() As Boolean
      Get
        Return GetProperty(HDIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HDIndProperty, Value)
      End Set
    End Property

    Public Shared ImportedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ImportedInd, "Imported", False)
    ''' <summary>
    ''' Gets and sets the Imported value
    ''' </summary>
    <Display(Name:="Imported", Description:=""),
    Required(ErrorMessage:="Imported required")>
    Public Property ImportedInd() As Boolean
      Get
        Return GetProperty(ImportedIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ImportedIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryInd, "Primary", False)
    ''' <summary>
    ''' Gets and sets the Imported value
    ''' </summary>
    <Display(Name:="Primary", Description:="")>
    Public Property PrimaryInd() As Boolean
      Get
        Return GetProperty(PrimaryIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PrimaryIndProperty, Value)
      End Set
    End Property

    Public Shared PriorityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Priority, "Priority", 1000)
    ''' <summary>
    ''' Gets and sets the Imported value
    ''' </summary>
    <Display(Name:="Priority", Description:="")>
    Public Property Priority() As Integer
      Get
        Return GetProperty(PriorityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PriorityProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ChannelIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ChannelName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Channel")
        Else
          Return String.Format("Blank {0}", "Channel")
        End If
      Else
        Return Me.ChannelName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewChannel() method.

    End Sub

    Public Shared Function NewChannel() As Channel

      Return DataPortal.CreateChild(Of Channel)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetChannel(dr As SafeDataReader) As Channel

      Dim c As New Channel()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ChannelIDProperty, .GetInt32(0))
          LoadProperty(ChannelNameProperty, .GetString(1))
          LoadProperty(ChannelShortNameProperty, .GetString(2))
          LoadProperty(HDIndProperty, .GetBoolean(3))
          LoadProperty(ImportedIndProperty, .GetBoolean(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(PrimaryIndProperty, .GetBoolean(9))
          LoadProperty(PriorityProperty, .GetInt32(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insChannel"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updChannel"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramChannelID As SqlParameter = .Parameters.Add("@ChannelID", SqlDbType.Int)
          paramChannelID.Value = GetProperty(ChannelIDProperty)
          If Me.IsNew Then
            paramChannelID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ChannelName", GetProperty(ChannelNameProperty))
          .Parameters.AddWithValue("@ChannelShortName", GetProperty(ChannelShortNameProperty))
          .Parameters.AddWithValue("@HDInd", GetProperty(HDIndProperty))
          .Parameters.AddWithValue("@ImportedInd", GetProperty(ImportedIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@PrimaryInd", GetProperty(PrimaryIndProperty))
          .Parameters.AddWithValue("@Priority", GetProperty(PriorityProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ChannelIDProperty, paramChannelID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delChannel"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ChannelID", GetProperty(ChannelIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace