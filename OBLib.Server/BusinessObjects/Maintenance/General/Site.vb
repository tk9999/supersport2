﻿' Generated 07 May 2016 20:22 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General

  <Serializable()> _
  Public Class Site
    Inherits OBBusinessBase(Of Site)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SiteBO.SiteBOToString(self)")

    Public Shared SiteIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SiteID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SiteID() As Integer
      Get
        Return GetProperty(SiteIDProperty)
      End Get
    End Property

    Public Shared SiteNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SiteName, "Site Name", "")
    ''' <summary>
    ''' Gets and sets the Site Name value
    ''' </summary>
    <Display(Name:="Site Name", Description:=""),
    StringLength(50, ErrorMessage:="Site Name cannot be more than 50 characters")>
  Public Property SiteName() As String
      Get
        Return GetProperty(SiteNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SiteNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SiteIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SiteName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Site")
        Else
          Return String.Format("Blank {0}", "Site")
        End If
      Else
        Return Me.SiteName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSite() method.

    End Sub

    Public Shared Function NewSite() As Site

      Return DataPortal.CreateChild(Of Site)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSite(dr As SafeDataReader) As Site

      Dim s As New Site()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SiteIDProperty, .GetInt32(0))
          LoadProperty(SiteNameProperty, .GetString(1))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SiteIDProperty)

      cm.Parameters.AddWithValue("@SiteName", GetProperty(SiteNameProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SiteIDProperty, cm.Parameters("@SiteID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SiteID", GetProperty(SiteIDProperty))
    End Sub

#End Region

  End Class

End Namespace