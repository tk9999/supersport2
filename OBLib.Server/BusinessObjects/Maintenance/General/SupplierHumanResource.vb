﻿' Generated 29 Dec 2014 11:55 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance

  <Serializable()> _
  Public Class SupplierHumanResource
    Inherits OBBusinessBase(Of SupplierHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SupplierHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SupplierHumanResourceID() As Integer
      Get
        Return GetProperty(SupplierHumanResourceIDProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierIDProperty, Value)
      End Set
    End Property

    Public Shared FirstnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Firstname, "Firstname", "")
    ''' <summary>
    ''' Gets and sets the Firstname value
    ''' </summary>
    <Display(Name:="Firstname", Description:="Firstname of the person"),
    Required(ErrorMessage:="Firstname required"),
    StringLength(50, ErrorMessage:="Firstname cannot be more than 50 characters")>
    Public Property Firstname() As String
      Get
        Return GetProperty(FirstnameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FirstnameProperty, Value)
      End Set
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
    ''' <summary>
    ''' Gets and sets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="Surname of the person"),
    Required(ErrorMessage:="Surname required"),
    StringLength(50, ErrorMessage:="Surname cannot be more than 50 characters")>
    Public Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SurnameProperty, Value)
      End Set
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No", "")
    ''' <summary>
    ''' Gets and sets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="ID No. of the person"),
    StringLength(13, ErrorMessage:="ID No cannot be more than 13 characters")>
    Public Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IDNoProperty, Value)
      End Set
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number", "")
    ''' <summary>
    ''' Gets and sets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Cell Phone Number", Description:="Cellphone No. of the person"),
    StringLength(20, ErrorMessage:="Cell Phone Number cannot be more than 20 characters")>
    Public Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CellPhoneNumberProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Supplier

      Return CType(CType(Me.Parent, SupplierHumanResourceList).Parent, Supplier)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SupplierHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Firstname.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Supplier Human Resource")
        Else
          Return String.Format("Blank {0}", "Supplier Human Resource")
        End If
      Else
        Return Me.Firstname
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSupplierHumanResource() method.

    End Sub

    Public Shared Function NewSupplierHumanResource() As SupplierHumanResource

      Return DataPortal.CreateChild(Of SupplierHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSupplierHumanResource(dr As SafeDataReader) As SupplierHumanResource

      Dim s As New SupplierHumanResource()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SupplierHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(FirstnameProperty, .GetString(2))
          LoadProperty(SurnameProperty, .GetString(3))
          LoadProperty(IDNoProperty, .GetString(4))
          LoadProperty(CellPhoneNumberProperty, .GetString(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSupplierHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSupplierHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSupplierHumanResourceID As SqlParameter = .Parameters.Add("@SupplierHumanResourceID", SqlDbType.Int)
          paramSupplierHumanResourceID.Value = GetProperty(SupplierHumanResourceIDProperty)
          If Me.IsNew Then
            paramSupplierHumanResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SupplierID", Me.GetParent.SupplierID)
          .Parameters.AddWithValue("@Firstname", GetProperty(FirstnameProperty))
          .Parameters.AddWithValue("@Surname", GetProperty(SurnameProperty))
          .Parameters.AddWithValue("@IDNo", GetProperty(IDNoProperty))
          .Parameters.AddWithValue("@CellPhoneNumber", GetProperty(CellPhoneNumberProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SupplierHumanResourceIDProperty, paramSupplierHumanResourceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSupplierHumanResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SupplierHumanResourceID", GetProperty(SupplierHumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace