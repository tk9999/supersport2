﻿' Generated 04 Jun 2014 08:47 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance

  <Serializable()> _
  Public Class AudioConfiguration
    Inherits OBBusinessBase(Of AudioConfiguration)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared AudioConfigurationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AudioConfigurationID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AudioConfigurationID() As Integer
      Get
        Return GetProperty(AudioConfigurationIDProperty)
      End Get
    End Property

    Public Shared AudioConfigurationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AudioConfiguration, "Audio Configuration", "")
    ''' <summary>
    ''' Gets and sets the Audio Configuration value
    ''' </summary>
    <Display(Name:="Audio Configuration", Description:="Description of the audio configuration"),
    Required(ErrorMessage:="Audio Configuration Required"),
    StringLength(50, ErrorMessage:="Audio Configuration cannot be more than 50 characters")>
    Public Property AudioConfiguration() As String
      Get
        Return GetProperty(AudioConfigurationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AudioConfigurationProperty, Value)
      End Set
    End Property

    Public Shared InIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InInd, "In", False)
    ''' <summary>
    ''' Gets and sets the In value
    ''' </summary>
    <Display(Name:="In", Description:="Tick indicates that audio configuration is for audio in"),
    Required(ErrorMessage:="In required")>
    Public Property InInd() As Boolean
      Get
        Return GetProperty(InIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(InIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared AudioConfigurationDetailListProperty As PropertyInfo(Of AudioConfigurationDetailList) = RegisterProperty(Of AudioConfigurationDetailList)(Function(c) c.AudioConfigurationDetailList, "Audio Configuration Detail List")

    Public ReadOnly Property AudioConfigurationDetailList() As AudioConfigurationDetailList
      Get
        If GetProperty(AudioConfigurationDetailListProperty) Is Nothing Then
          LoadProperty(AudioConfigurationDetailListProperty, Maintenance.AudioConfigurationDetailList.NewAudioConfigurationDetailList())
        End If
        Return GetProperty(AudioConfigurationDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AudioConfigurationIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AudioConfiguration.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Audio Configuration")
        Else
          Return String.Format("Blank {0}", "Audio Configuration")
        End If
      Else
        Return Me.AudioConfiguration
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"AudioConfigurationDetails"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAudioConfiguration() method.

    End Sub

    Public Shared Function NewAudioConfiguration() As AudioConfiguration

      Return DataPortal.CreateChild(Of AudioConfiguration)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAudioConfiguration(dr As SafeDataReader) As AudioConfiguration

      Dim a As New AudioConfiguration()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AudioConfigurationIDProperty, .GetInt32(0))
          LoadProperty(AudioConfigurationProperty, .GetString(1))
          LoadProperty(InIndProperty, .GetBoolean(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAudioConfiguration"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAudioConfiguration"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAudioConfigurationID As SqlParameter = .Parameters.Add("@AudioConfigurationID", SqlDbType.Int)
          paramAudioConfigurationID.Value = GetProperty(AudioConfigurationIDProperty)
          If Me.IsNew Then
            paramAudioConfigurationID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AudioConfiguration", GetProperty(AudioConfigurationProperty))
          .Parameters.AddWithValue("@InInd", GetProperty(InIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AudioConfigurationIDProperty, paramAudioConfigurationID.Value)
          End If
          ' update child objects
          If GetProperty(AudioConfigurationDetailListProperty) IsNot Nothing Then
            Me.AudioConfigurationDetailList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(AudioConfigurationDetailListProperty) IsNot Nothing Then
          Me.AudioConfigurationDetailList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAudioConfiguration"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AudioConfigurationID", GetProperty(AudioConfigurationIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace