﻿' Generated 27 Jan 2015 10:56 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class Facility
    Inherits OBBusinessBase(Of Facility)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FacilityIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FacilityID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FacilityID() As Integer
      Get
        Return GetProperty(FacilityIDProperty)
      End Get
    End Property

    Public Shared FacilityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Facility, "Facility", "")
    ''' <summary>
    ''' Gets the Facility value
    ''' </summary>
    <Display(Name:="Facility", Description:="facility name"),
    Required(ErrorMessage:="Facility Required"),
    StringLength(100, ErrorMessage:="Facility cannot be more than 100 characters")>
    Public Property Facility() As String
      Get
        Return GetProperty(FacilityProperty)
      End Get
      Set(value As String)
        SetProperty(FacilityProperty, value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="description of the facility"),
    Required(ErrorMessage:="Description Required"),
    StringLength(500, ErrorMessage:="Description cannot be more than 500 characters")>
    Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(value As String)
        SetProperty(DescriptionProperty, value)
      End Set
    End Property

    Public Shared FacilityTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FacilityTypeID, "Facility Type", Nothing)
    ''' <summary>
    ''' Gets the Facility Type value
    ''' </summary>
    <Display(Name:="Facility Type", Description:="the type of the facility (training room, meeting room etc)"),
    Required(ErrorMessage:="Facility Type Required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROFacilityTypeList))>
    Public Property FacilityTypeID() As Integer?
      Get
        Return GetProperty(FacilityTypeIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(FacilityTypeIDProperty, value)
      End Set
    End Property


    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="the system to which this facility belongs"),
    Required(ErrorMessage:="System Required"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList))>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(SystemIDProperty, value)
      End Set
    End Property
    

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FacilityIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Facility.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Facility")
        Else
          Return String.Format("Blank {0}", "Facility")
        End If
      Else
        Return Me.Facility
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFacility() method.

    End Sub

    Public Shared Function NewFacility() As Facility

      Return DataPortal.CreateChild(Of Facility)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFacility(dr As SafeDataReader) As Facility

      Dim f As New Facility()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FacilityIDProperty, .GetInt32(0))
          LoadProperty(FacilityProperty, .GetString(1))
          LoadProperty(DescriptionProperty, .GetString(2))
          LoadProperty(FacilityTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insFacility"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFacility"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramFacilityID As SqlParameter = .Parameters.Add("@FacilityID", SqlDbType.Int)
          paramFacilityID.Value = GetProperty(FacilityIDProperty)
          If Me.IsNew Then
            paramFacilityID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Facility", GetProperty(FacilityProperty))
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          .Parameters.AddWithValue("@FacilityTypeID", GetProperty(FacilityTypeIDProperty))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(FacilityIDProperty, paramFacilityID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delFacility"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@FacilityID", GetProperty(FacilityIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace