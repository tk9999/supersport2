﻿' Generated 21 May 2014 13:27 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance

  <Serializable()> _
  Public Class SupplierList
    Inherits SingularBusinessListBase(Of SupplierList, Supplier)

#Region " Business Methods "

    Public Function GetItem(SupplierID As Integer) As Supplier

      For Each child As Supplier In Me
        If child.SupplierID = SupplierID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Suppliers"

    End Function

    Public Function GetSupplierHumanResource(SupplierHumanResourceID As Integer) As SupplierHumanResource

      Dim obj As SupplierHumanResource = Nothing
      For Each parent As Supplier In Me
        obj = parent.SupplierHumanResourceList.GetItem(SupplierHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSupplierList() As SupplierList

      Return New SupplierList()

    End Function

    Public Shared Sub BeginGetSupplierList(CallBack As EventHandler(Of DataPortalResult(Of SupplierList)))

      Dim dp As New DataPortal(Of SupplierList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSupplierList() As SupplierList

      Return DataPortal.Fetch(Of SupplierList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(Supplier.GetSupplier(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As Supplier = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SupplierID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SupplierHumanResourceList.RaiseListChangedEvents = False
          parent.SupplierHumanResourceList.Add(SupplierHumanResource.GetSupplierHumanResource(sdr))
          parent.SupplierHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As Supplier In Me
        child.CheckRules()
        For Each SupplierHumanResource As SupplierHumanResource In child.SupplierHumanResourceList
          SupplierHumanResource.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSupplierList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As Supplier In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As Supplier In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace