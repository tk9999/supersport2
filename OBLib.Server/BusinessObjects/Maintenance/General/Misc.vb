﻿' Generated 15 Apr 2014 12:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance

  <Serializable()> _
  Public Class Misc
    Inherits SingularBusinessBase(Of Misc)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared MaxVehicleTravelHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxVehicleTravelHours, "Max Vehicle Travel Hours", 10)
    ''' <summary>
    ''' Gets and sets the Max Vehicle Travel Hours value
    ''' </summary>
    <Display(Name:="Max Vehicle Travel Hours", Description:="The maximum number of hours that continous travel is allowed. This can be overriden by the event planner"),
    Required(ErrorMessage:="Max Vehicle Travel Hours required")>
    Public Property MaxVehicleTravelHours() As Integer
      Get
        Return GetProperty(MaxVehicleTravelHoursProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MaxVehicleTravelHoursProperty, Value)
      End Set
    End Property

    Public Shared MinCrewTravelDayHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MinCrewTravelDayHours, "Min Crew Travel Day Hours", 5)
    ''' <summary>
    ''' Gets and sets the Min Crew Travel Day Hours value
    ''' </summary>
    <Display(Name:="Min Crew Travel Day Hours", Description:="The minimum number of hours that will be credited to a human resource should they only travel on a day"),
    Required(ErrorMessage:="Min Crew Travel Day Hours required")>
    Public Property MinCrewTravelDayHours() As Integer
      Get
        Return GetProperty(MinCrewTravelDayHoursProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MinCrewTravelDayHoursProperty, Value)
      End Set
    End Property

    Public Shared MinCrewProductionDayHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MinCrewProductionDayHours, "Min Crew Production Day Hours", 10)
    ''' <summary>
    ''' Gets and sets the Min Crew Production Day Hours value
    ''' </summary>
    <Display(Name:="Min Crew Production Day Hours", Description:="The minimum number of hours that will be credited to a human resource should they only work on a production"),
    Required(ErrorMessage:="Min Crew Production Day Hours required")>
    Public Property MinCrewProductionDayHours() As Integer
      Get
        Return GetProperty(MinCrewProductionDayHoursProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MinCrewProductionDayHoursProperty, Value)
      End Set
    End Property

    Public Shared VersionNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VersionNo, "ID", "")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key(),
    StringLength(50, ErrorMessage:="ID cannot be more than 50 characters")>
    Public Property VersionNo() As String
      Get
        Return GetProperty(VersionNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VersionNoProperty, Value)
      End Set
    End Property

    Public Shared UpdatePathProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.UpdatePath, "Update Path", "")
    ''' <summary>
    ''' Gets and sets the Update Path value
    ''' </summary>
    <Display(Name:="Update Path", Description:=""),
    StringLength(300, ErrorMessage:="Update Path cannot be more than 300 characters")>
    Public Property UpdatePath() As String
      Get
        Return GetProperty(UpdatePathProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(UpdatePathProperty, Value)
      End Set
    End Property

    Public Shared FTPIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FTPInd, "FTP", False)
    ''' <summary>
    ''' Gets and sets the FTP value
    ''' </summary>
    <Display(Name:="FTP", Description:=""),
    Required(ErrorMessage:="FTP required")>
    Public Property FTPInd() As Boolean
      Get
        Return GetProperty(FTPIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(FTPIndProperty, Value)
      End Set
    End Property

    Public Shared FTPUsernameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FTPUsername, "FTP Username", "")
    ''' <summary>
    ''' Gets and sets the FTP Username value
    ''' </summary>
    <Display(Name:="FTP Username", Description:=""),
    StringLength(100, ErrorMessage:="FTP Username cannot be more than 100 characters")>
    Public Property FTPUsername() As String
      Get
        Return GetProperty(FTPUsernameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FTPUsernameProperty, Value)
      End Set
    End Property

    Public Shared FTPPasswordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FTPPassword, "FTP Password", "")
    ''' <summary>
    ''' Gets and sets the FTP Password value
    ''' </summary>
    <Display(Name:="FTP Password", Description:=""),
    StringLength(100, ErrorMessage:="FTP Password cannot be more than 100 characters")>
    Public Property FTPPassword() As String
      Get
        Return GetProperty(FTPPasswordProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FTPPasswordProperty, Value)
      End Set
    End Property

    Public Shared HRImagesPathProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRImagesPath, "HR Images Path", "")
    ''' <summary>
    ''' Gets and sets the HR Images Path value
    ''' </summary>
    <Display(Name:="HR Images Path", Description:=""),
    StringLength(300, ErrorMessage:="HR Images Path cannot be more than 300 characters")>
    Public Property HRImagesPath() As String
      Get
        Return GetProperty(HRImagesPathProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HRImagesPathProperty, Value)
      End Set
    End Property

    Public Shared TimesheetMonthProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TimesheetMonth, "Timesheet Month")
    ''' <summary>
    ''' Gets and sets the Timesheet Month value
    ''' </summary>
    <Display(Name:="Timesheet Month", Description:=""),
    Required(ErrorMessage:="Timesheet Month required")>
    Public Property TimesheetMonth As DateTime?
      Get
        If Not FieldManager.FieldExists(TimesheetMonthProperty) Then
          LoadProperty(TimesheetMonthProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(TimesheetMonthProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TimesheetMonthProperty, Value)
      End Set
    End Property

    Public Shared TimesheetOpeningDayProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetOpeningDay, "Timesheet Opening Day", 1)
    ''' <summary>
    ''' Gets and sets the Timesheet Opening Day value
    ''' </summary>
    <Display(Name:="Timesheet Opening Day", Description:=""),
    Required(ErrorMessage:="Timesheet Opening Day required")>
    Public Property TimesheetOpeningDay() As Integer
      Get
        Return GetProperty(TimesheetOpeningDayProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TimesheetOpeningDayProperty, Value)
      End Set
    End Property

    Public Shared TimesheetClosingDayProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetClosingDay, "Timesheet Closing Day", 31)
    ''' <summary>
    ''' Gets and sets the Timesheet Closing Day value
    ''' </summary>
    <Display(Name:="Timesheet Closing Day", Description:=""),
    Required(ErrorMessage:="Timesheet Closing Day required")>
    Public Property TimesheetClosingDay() As Integer
      Get
        Return GetProperty(TimesheetClosingDayProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TimesheetClosingDayProperty, Value)
      End Set
    End Property

    Public Shared LastUsedSessionIDProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LastUsedSessionID, "Last Used Session", "")
    ''' <summary>
    ''' Gets and sets the Last Used Session value
    ''' </summary>
    <Display(Name:="Last Used Session", Description:=""),
    StringLength(100, ErrorMessage:="Last Used Session cannot be more than 100 characters")>
    Public Property LastUsedSessionID() As String
      Get
        Return GetProperty(LastUsedSessionIDProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LastUsedSessionIDProperty, Value)
      End Set
    End Property

    Public Shared SessionKeyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SessionKey, "Session Key", "")
    ''' <summary>
    ''' Gets and sets the Session Key value
    ''' </summary>
    <Display(Name:="Session Key", Description:=""),
    StringLength(50, ErrorMessage:="Session Key cannot be more than 50 characters")>
    Public Property SessionKey() As String
      Get
        Return GetProperty(SessionKeyProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SessionKeyProperty, Value)
      End Set
    End Property

    Public Shared DefaultLeaveEndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DefaultLeaveEndTime, "Default Leave End Time")
    ''' <summary>
    ''' Gets and sets the Default Leave End Time value
    ''' </summary>
    <Display(Name:="Default Leave End Time", Description:="")>
    Public Property DefaultLeaveEndTime() As Object
      Get
        Dim value = GetProperty(DefaultLeaveEndTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(DefaultLeaveEndTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(DefaultLeaveEndTimeProperty, DateTime.MinValue)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(DefaultLeaveEndTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared DefaultLeaveStartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DefaultLeaveStartTime, "Default Leave Start Time")
    ''' <summary>
    ''' Gets and sets the Default Leave Start Time value
    ''' </summary>
    <Display(Name:="Default Leave Start Time", Description:="")>
    Public Property DefaultLeaveStartTime() As Object
      Get
        Dim value = GetProperty(DefaultLeaveStartTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(DefaultLeaveStartTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(DefaultLeaveStartTimeProperty, DateTime.MinValue)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(DefaultLeaveStartTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared ProductionTimesheetMonthProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ProductionTimesheetMonth, "Production Timesheet Month")
    ''' <summary>
    ''' Gets and sets the Production Timesheet Month value
    ''' </summary>
    <Display(Name:="Production Timesheet Month", Description:=""),
    Required(ErrorMessage:="Production Timesheet Month required")>
    Public Property ProductionTimesheetMonth As DateTime?
      Get
        If Not FieldManager.FieldExists(ProductionTimesheetMonthProperty) Then
          LoadProperty(ProductionTimesheetMonthProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(ProductionTimesheetMonthProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ProductionTimesheetMonthProperty, Value)
      End Set
    End Property

    Public Shared AccessLogConnectionStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccessLogConnectionString)
    ''' <summary>
    ''' Gets and sets the Access Log Connection String value
    ''' </summary>
    <Display(Name:="Access Log Connection String", Description:="")>
    Public Property AccessLogConnectionString() As String
      Get
        Return GetProperty(AccessLogConnectionStringProperty)
      End Get
      Set(value As String)
        SetProperty(AccessLogConnectionStringProperty, value)
      End Set
    End Property

    Public Shared PhotoFilePathProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PhotoFilePath)
    Public Property PhotoFilePath() As String
      Get
        Return GetProperty(PhotoFilePathProperty)
      End Get
      Set(value As String)
        SetProperty(PhotoFilePathProperty, value)
      End Set
    End Property

    Public Shared OBCityTimelineReadyEnabledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBCityTimelineReadyEnabled, "OB City Timeline Ready Enabled", False)
    ''' <summary>
    ''' Gets and sets the Last Used Session value
    ''' </summary>
    <Display(Name:="OB City Timeline Ready Enabled", Description:="")>
    Public Property OBCityTimelineReadyEnabled() As Boolean
      Get
        Return GetProperty(OBCityTimelineReadyEnabledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OBCityTimelineReadyEnabledProperty, Value)
      End Set
    End Property


#Region " Synergy "

    Public Shared SynergyWebServiceAuthTokenProperty As PropertyInfo(Of Guid) = RegisterSProperty(Of Guid)(Function(c) c.SynergyWebServiceAuthToken, CType(Nothing, Guid))
    Public Property SynergyWebServiceAuthToken() As Guid
      Get
        Return GetProperty(SynergyWebServiceAuthTokenProperty)
      End Get
      Set(value As Guid)
        SetProperty(SynergyWebServiceAuthTokenProperty, value)
      End Set
    End Property

#End Region

#Region " SmS "

    Public Shared SmSApiIDProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SmSApiID, "FTP Username", "")
    ''' <summary>
    ''' Gets and sets the FTP Username value
    ''' </summary>
    <Display(Name:="FTP Username", Description:=""),
    StringLength(100, ErrorMessage:="FTP Username cannot be more than 100 characters")>
    Public Property SmSApiID() As String
      Get
        Return GetProperty(SmSApiIDProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SmSApiIDProperty, Value)
      End Set
    End Property

    Public Shared SMSUsernameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SMSUsername, "FTP Password", "")
    ''' <summary>
    ''' Gets and sets the FTP Password value
    ''' </summary>
    <Display(Name:="FTP Password", Description:=""),
    StringLength(100, ErrorMessage:="FTP Password cannot be more than 100 characters")>
    Public Property SMSUsername() As String
      Get
        Return GetProperty(SMSUsernameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SMSUsernameProperty, Value)
      End Set
    End Property

    Public Shared SMSPasswordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SMSPassword, "FTP Username", "")
    ''' <summary>
    ''' Gets and sets the FTP Username value
    ''' </summary>
    <Display(Name:="FTP Username", Description:=""),
    StringLength(100, ErrorMessage:="FTP Username cannot be more than 100 characters")>
    Public Property SMSPassword() As String
      Get
        Return GetProperty(SMSPasswordProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SMSPasswordProperty, Value)
      End Set
    End Property

#End Region

#Region " Mail "

    Public Shared MailServerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MailServer, "FTP Username", "")
    ''' <summary>
    ''' Gets and sets the FTP Username value
    ''' </summary>
    Public Property MailServer() As String
      Get
        Return GetProperty(MailServerProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MailServerProperty, Value)
      End Set
    End Property

    Public Shared MailFriendlyFromProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MailFriendlyFrom, "FTP Username", "")
    ''' <summary>
    ''' Gets and sets the FTP Username value
    ''' </summary>
    Public Property MailFriendlyFrom() As String
      Get
        Return GetProperty(MailFriendlyFromProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MailFriendlyFromProperty, Value)
      End Set
    End Property

    Public Shared MailFromAccountProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MailFromAccount, "FTP Username", "")
    ''' <summary>
    ''' Gets and sets the FTP Username value
    ''' </summary>
    Public Property MailFromAccount() As String
      Get
        Return GetProperty(MailFromAccountProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MailFromAccountProperty, Value)
      End Set
    End Property

    Public Shared MailFromAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MailFromAddress, "FTP Username", "")
    ''' <summary>
    ''' Gets and sets the FTP Username value
    ''' </summary>
    Public Property MailFromAddress() As String
      Get
        Return GetProperty(MailFromAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MailFromAddressProperty, Value)
      End Set
    End Property

    Public Shared MailFromPasswordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MailFromPassword, "FTP Username", "")
    ''' <summary>
    ''' Gets and sets the FTP Username value
    ''' </summary>
    Public Property MailFromPassword() As String
      Get
        Return GetProperty(MailFromPasswordProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MailFromPasswordProperty, Value)
      End Set
    End Property

#End Region

#Region " ICR Dashboard "

    Public Shared ICRDashboardIntegrationPushKeyProperty As PropertyInfo(Of Guid) = RegisterSProperty(Of Guid)(Function(c) c.ICRDashboardIntegrationPushKey, CType(Nothing, Guid))
    Public Property ICRDashboardIntegrationPushKey() As Guid
      Get
        Return GetProperty(ICRDashboardIntegrationPushKeyProperty)
      End Get
      Set(value As Guid)
        SetProperty(ICRDashboardIntegrationPushKeyProperty, value)
      End Set
    End Property

    Public Shared ICRDashboardIntegrationPullKeyProperty As PropertyInfo(Of Guid) = RegisterSProperty(Of Guid)(Function(c) c.ICRDashboardIntegrationPullKey, CType(Nothing, Guid))
    Public Property ICRDashboardIntegrationPullKey() As Guid
      Get
        Return GetProperty(ICRDashboardIntegrationPullKeyProperty)
      End Get
      Set(value As Guid)
        SetProperty(ICRDashboardIntegrationPullKeyProperty, value)
      End Set
    End Property

#End Region

#Region " Slugs "

    Public Shared SlugsPushKeyProperty As PropertyInfo(Of Guid) = RegisterSProperty(Of Guid)(Function(c) c.SlugsPushKey, CType(Nothing, Guid))
    Public Property SlugsPushKey() As Guid
      Get
        Return GetProperty(SlugsPushKeyProperty)
      End Get
      Set(value As Guid)
        SetProperty(SlugsPushKeyProperty, value)
      End Set
    End Property

    Public Shared SlugsPullKeyProperty As PropertyInfo(Of Guid) = RegisterSProperty(Of Guid)(Function(c) c.SlugsPullKey, CType(Nothing, Guid))
    Public Property SlugsPullKey() As Guid
      Get
        Return GetProperty(SlugsPullKeyProperty)
      End Get
      Set(value As Guid)
        SetProperty(SlugsPullKeyProperty, value)
      End Set
    End Property

    Public Shared SlugsCallerIDProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SlugsCallerID)
    ''' <summary>
    ''' Gets and sets the Access Log Connection String value
    ''' </summary>
    <Display(Name:="SlugsCallerID", Description:="")>
    Public Property SlugsCallerID() As String
      Get
        Return GetProperty(SlugsCallerIDProperty)
      End Get
      Set(value As String)
        SetProperty(SlugsCallerIDProperty, value)
      End Set
    End Property

#End Region

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VersionNoProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.VersionNo.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Misc")
        Else
          Return String.Format("Blank {0}", "Misc")
        End If
      Else
        Return Me.VersionNo
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewMisc() method.

    End Sub

    Public Shared Function NewMisc() As Misc

      Return DataPortal.CreateChild(Of Misc)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetMisc(dr As SafeDataReader) As Misc

      Dim m As New Misc()
      m.Fetch(dr)
      Return m

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(MaxVehicleTravelHoursProperty, .GetInt32(0))
          LoadProperty(MinCrewTravelDayHoursProperty, .GetInt32(1))
          LoadProperty(MinCrewProductionDayHoursProperty, .GetInt32(2))
          LoadProperty(VersionNoProperty, .GetString(3))
          LoadProperty(UpdatePathProperty, .GetString(4))
          LoadProperty(FTPIndProperty, .GetBoolean(5))
          LoadProperty(FTPUsernameProperty, .GetString(6))
          LoadProperty(FTPPasswordProperty, .GetString(7))
          LoadProperty(HRImagesPathProperty, .GetString(8))
          LoadProperty(TimesheetMonthProperty, .GetValue(9))
          LoadProperty(TimesheetOpeningDayProperty, .GetInt32(10))
          LoadProperty(TimesheetClosingDayProperty, .GetInt32(11))
          LoadProperty(LastUsedSessionIDProperty, .GetString(12))
          LoadProperty(SessionKeyProperty, .GetString(13))
          If .IsDBNull(15) Then
            LoadProperty(DefaultLeaveEndTimeProperty, DateTime.MinValue)
          Else
            LoadProperty(DefaultLeaveEndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(14))))
          End If
          If .IsDBNull(14) Then
            LoadProperty(DefaultLeaveStartTimeProperty, DateTime.MinValue)
          Else
            LoadProperty(DefaultLeaveStartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(15))))
          End If
          LoadProperty(ProductionTimesheetMonthProperty, .GetValue(16))
          '17 OldProductionScreenDisabledInd
          LoadProperty(AccessLogConnectionStringProperty, .GetString(18))
          LoadProperty(SmSApiIDProperty, .GetString(19))
          LoadProperty(SMSUsernameProperty, .GetString(20))
          LoadProperty(SMSPasswordProperty, .GetString(21))
          LoadProperty(MailServerProperty, .GetString(22))
          LoadProperty(MailFriendlyFromProperty, .GetString(23))
          LoadProperty(MailFromAccountProperty, .GetString(24))
          LoadProperty(MailFromPasswordProperty, .GetString(25))
          LoadProperty(PhotoFilePathProperty, .GetString(26))
          If .GetValue(27) Is Nothing Then
            LoadProperty(SynergyWebServiceAuthTokenProperty, Nothing)
          Else
            LoadProperty(SynergyWebServiceAuthTokenProperty, .GetGuid(27))
          End If
          LoadProperty(OBCityTimelineReadyEnabledProperty, .GetBoolean(28))
          LoadProperty(MailFromAddressProperty, .GetString(29))
          If .GetValue(30) Is Nothing Then
            LoadProperty(ICRDashboardIntegrationPushKeyProperty, Nothing)
          Else
            LoadProperty(ICRDashboardIntegrationPushKeyProperty, .GetGuid(30))
          End If
          If .GetValue(31) Is Nothing Then
            LoadProperty(ICRDashboardIntegrationPullKeyProperty, Nothing)
          Else
            LoadProperty(ICRDashboardIntegrationPullKeyProperty, .GetGuid(31))
          End If
          If .GetValue(32) Is Nothing Then
            LoadProperty(SlugsPushKeyProperty, Nothing)
          Else
            LoadProperty(SlugsPushKeyProperty, .GetGuid(32))
          End If
          If .GetValue(33) Is Nothing Then
            LoadProperty(SlugsPullKeyProperty, Nothing)
          Else
            LoadProperty(SlugsPullKeyProperty, .GetGuid(33))
          End If
          LoadProperty(SlugsCallerIDProperty, .GetString(34))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insMisc"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updMisc"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          .Parameters.AddWithValue("@MaxVehicleTravelHours", GetProperty(MaxVehicleTravelHoursProperty))
          .Parameters.AddWithValue("@MinCrewTravelDayHours", GetProperty(MinCrewTravelDayHoursProperty))
          .Parameters.AddWithValue("@MinCrewProductionDayHours", GetProperty(MinCrewProductionDayHoursProperty))
          Dim paramVersionNo As SqlParameter = .Parameters.Add("@VersionNo", SqlDbType.VarChar, 50)
          paramVersionNo.Value = GetProperty(VersionNoProperty)
          If Me.IsNew Then
            paramVersionNo.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@UpdatePath", GetProperty(UpdatePathProperty))
          .Parameters.AddWithValue("@FTPInd", GetProperty(FTPIndProperty))
          .Parameters.AddWithValue("@FTPUsername", GetProperty(FTPUsernameProperty))
          .Parameters.AddWithValue("@FTPPassword", GetProperty(FTPPasswordProperty))
          .Parameters.AddWithValue("@HRImagesPath", GetProperty(HRImagesPathProperty))
          cm.Parameters.AddWithValue("@TimesheetMonth", (New SmartDate(GetProperty(TimesheetMonthProperty))).DBValue)
          .Parameters.AddWithValue("@TimesheetOpeningDay", GetProperty(TimesheetOpeningDayProperty))
          .Parameters.AddWithValue("@TimesheetClosingDay", GetProperty(TimesheetClosingDayProperty))
          .Parameters.AddWithValue("@LastUsedSessionID", GetProperty(LastUsedSessionIDProperty))
          .Parameters.AddWithValue("@SessionKey", GetProperty(SessionKeyProperty))
          cm.Parameters.AddWithValue("@DefaultLeaveEndTime", (New SmartDate(GetProperty(DefaultLeaveEndTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@DefaultLeaveStartTime", (New SmartDate(GetProperty(DefaultLeaveStartTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@ProductionTimesheetMonth", (New SmartDate(GetProperty(ProductionTimesheetMonthProperty))).DBValue)
          cm.Parameters.AddWithValue("@AccessLogConnectionString", GetProperty(AccessLogConnectionStringProperty))
          cm.Parameters.AddWithValue("@SynergyWebServiceAuthToken", NothingDBNull(GetProperty(SynergyWebServiceAuthTokenProperty)))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(VersionNoProperty, paramVersionNo.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delMisc"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@VersionNo", GetProperty(VersionNoProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace