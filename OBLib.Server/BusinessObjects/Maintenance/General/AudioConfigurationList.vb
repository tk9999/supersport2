﻿' Generated 04 Jun 2014 08:47 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance

  <Serializable()> _
  Public Class AudioConfigurationList
    Inherits OBBusinessListBase(Of AudioConfigurationList, AudioConfiguration)

#Region " Business Methods "

    Public Function GetItem(AudioConfigurationID As Integer) As AudioConfiguration

      For Each child As AudioConfiguration In Me
        If child.AudioConfigurationID = AudioConfigurationID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Audio Configurations"

    End Function

    Public Function GetAudioConfigurationDetail(AudioConfigurationDetailID As Integer) As AudioConfigurationDetail

      Dim obj As AudioConfigurationDetail = Nothing
      For Each parent As AudioConfiguration In Me
        obj = parent.AudioConfigurationDetailList.GetItem(AudioConfigurationDetailID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property AudioConfigurationID As Integer?

      Public Sub New(AudioConfigurationID As Integer?)
        Me.AudioConfigurationID = AudioConfigurationID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewAudioConfigurationList() As AudioConfigurationList

      Return New AudioConfigurationList()

    End Function

    Public Shared Sub BeginGetAudioConfigurationList(CallBack As EventHandler(Of DataPortalResult(Of AudioConfigurationList)))

      Dim dp As New DataPortal(Of AudioConfigurationList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAudioConfigurationList() As AudioConfigurationList

      Return DataPortal.Fetch(Of AudioConfigurationList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AudioConfiguration.GetAudioConfiguration(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As AudioConfiguration = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AudioConfigurationID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.AudioConfigurationDetailList.RaiseListChangedEvents = False
          parent.AudioConfigurationDetailList.Add(AudioConfigurationDetail.GetAudioConfigurationDetail(sdr))
          parent.AudioConfigurationDetailList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As AudioConfiguration In Me
        child.CheckRules()
        For Each AudioConfigurationDetail As AudioConfigurationDetail In child.AudioConfigurationDetailList
          AudioConfigurationDetail.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAudioConfigurationList"
            cm.Parameters.AddWithValue("@AudioConfigurationID", NothingDBNull(crit.AudioConfigurationID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AudioConfiguration In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AudioConfiguration In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace