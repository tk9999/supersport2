﻿' Generated 04 Jun 2014 08:47 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance

  <Serializable()> _
  Public Class AudioConfigurationDetail
    Inherits OBBusinessBase(Of AudioConfigurationDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AudioConfigurationDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AudioConfigurationDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AudioConfigurationDetailID() As Integer
      Get
        Return GetProperty(AudioConfigurationDetailIDProperty)
      End Get
    End Property

    Public Shared AudioConfigurationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AudioConfigurationID, "Audio Configuration", Nothing)
    ''' <summary>
    ''' Gets the Audio Configuration value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property AudioConfigurationID() As Integer?
      Get
        Return GetProperty(AudioConfigurationIDProperty)
      End Get
    End Property

    Public Shared AudioConfigurationDetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AudioConfigurationDetail, "Audio Configuration Detail", "")
    ''' <summary>
    ''' Gets and sets the Audio Configuration Detail value
    ''' </summary>
    <Display(Name:="Audio Configuration Detail", Description:="The detail for the audio configuration"),
    Required(ErrorMessage:="Configuration Detail Required"),
    StringLength(100, ErrorMessage:="Audio Configuration Detail cannot be more than 100 characters")>
    Public Property AudioConfigurationDetail() As String
      Get
        Return GetProperty(AudioConfigurationDetailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AudioConfigurationDetailProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As AudioConfiguration

      Return CType(CType(Me.Parent, AudioConfigurationDetailList).Parent, AudioConfiguration)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AudioConfigurationDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AudioConfigurationDetail.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Audio Configuration Detail")
        Else
          Return String.Format("Blank {0}", "Audio Configuration Detail")
        End If
      Else
        Return Me.AudioConfigurationDetail
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAudioConfigurationDetail() method.

    End Sub

    Public Shared Function NewAudioConfigurationDetail() As AudioConfigurationDetail

      Return DataPortal.CreateChild(Of AudioConfigurationDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAudioConfigurationDetail(dr As SafeDataReader) As AudioConfigurationDetail

      Dim a As New AudioConfigurationDetail()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AudioConfigurationDetailIDProperty, .GetInt32(0))
          LoadProperty(AudioConfigurationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AudioConfigurationDetailProperty, .GetString(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAudioConfigurationDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAudioConfigurationDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAudioConfigurationDetailID As SqlParameter = .Parameters.Add("@AudioConfigurationDetailID", SqlDbType.Int)
          paramAudioConfigurationDetailID.Value = GetProperty(AudioConfigurationDetailIDProperty)
          If Me.IsNew Then
            paramAudioConfigurationDetailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AudioConfigurationID", Me.GetParent().AudioConfigurationID)
          .Parameters.AddWithValue("@AudioConfigurationDetail", GetProperty(AudioConfigurationDetailProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AudioConfigurationDetailIDProperty, paramAudioConfigurationDetailID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAudioConfigurationDetail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AudioConfigurationDetailID", GetProperty(AudioConfigurationDetailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace