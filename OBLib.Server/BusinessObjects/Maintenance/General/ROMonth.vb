﻿' Generated 08 Feb 2016 17:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ReadOnly

  <Serializable()> _
  Public Class ROMonth
    Inherits OBReadOnlyBase(Of ROMonth)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared MonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MonthID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public ReadOnly Property MonthID() As Integer
      Get
        Return GetProperty(MonthIDProperty)
      End Get
    End Property

    Public Shared MonthNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MonthName, "Month Name")
    ''' <summary>
    ''' Gets the Month Name value
    ''' </summary>
    <Display(Name:="Month Name", Description:="")>
  Public ReadOnly Property MonthName() As String
      Get
        Return GetProperty(MonthNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(MonthIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.MonthName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROMonth(dr As SafeDataReader) As ROMonth

      Dim r As New ROMonth()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(MonthIDProperty, .GetInt32(0))
        LoadProperty(MonthNameProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace