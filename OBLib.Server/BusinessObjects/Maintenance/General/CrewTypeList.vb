﻿' Generated 29 Jun 2014 16:58 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class CrewTypeList
    Inherits SingularBusinessListBase(Of CrewTypeList, CrewType)

#Region " Business Methods "

    Public Function GetItem(CrewTypeID As Integer) As CrewType

      For Each child As CrewType In Me
        If child.CrewTypeID = CrewTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Crew Types"

    End Function

    Public Function GetCrewTypeDiscipline(CrewTypeDisciplineID As Integer) As CrewTypeDiscipline

      Dim obj As CrewTypeDiscipline = Nothing
      For Each parent As CrewType In Me
        obj = parent.CrewTypeDisciplineList.GetItem(CrewTypeDisciplineID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewCrewTypeList() As CrewTypeList

      Return New CrewTypeList()

    End Function

    Public Shared Sub BeginGetCrewTypeList(CallBack As EventHandler(Of DataPortalResult(Of CrewTypeList)))

      Dim dp As New DataPortal(Of CrewTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetCrewTypeList() As CrewTypeList

      Return DataPortal.Fetch(Of CrewTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(CrewType.GetCrewType(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As CrewType = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.CrewTypeID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.CrewTypeDisciplineList.RaiseListChangedEvents = False
          parent.CrewTypeDisciplineList.Add(CrewTypeDiscipline.GetCrewTypeDiscipline(sdr))
          parent.CrewTypeDisciplineList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As CrewType In Me
        child.CheckRules()
        For Each CrewTypeDiscipline As CrewTypeDiscipline In child.CrewTypeDisciplineList
          CrewTypeDiscipline.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getCrewTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As CrewType In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As CrewType In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace