﻿' Generated 26 Jan 2015 06:32 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class ISPCancellationRule
    Inherits SingularBusinessBase(Of ISPCancellationRule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ISPCancellationRuleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ISPCancellationRuleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ISPCancellationRuleID() As Integer
      Get
        Return GetProperty(ISPCancellationRuleIDProperty)
      End Get
    End Property

    Public Shared DaysFromProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DaysFrom, "Days From", 0)
    ''' <summary>
    ''' Gets and sets the Days From value
    ''' </summary>
    <Display(Name:="Days From", Description:="Start number of days until 8am on transmission day (from this day the ISP will be paid a percentage of their rate)"),
    Required(ErrorMessage:="Days From required")>
  Public Property DaysFrom() As Integer
      Get
        Return GetProperty(DaysFromProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(DaysFromProperty, Value)
      End Set
    End Property

    Public Shared DaysToProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DaysTo, "Days To", 0)
    ''' <summary>
    ''' Gets and sets the Days To value
    ''' </summary>
    <Display(Name:="Days To", Description:="End number of days until 8am on transmission day (from this day the ISP will be paid a percentage of their rate)"),
    Required(ErrorMessage:="Days To required")>
  Public Property DaysTo() As Integer
      Get
        Return GetProperty(DaysToProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(DaysToProperty, Value)
      End Set
    End Property

    Public Shared PercentageOfRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.PercentageOfRate, "Percentage Of Rate", 0)
    ''' <summary>
    ''' Gets and sets the Percentage Of Rate value
    ''' </summary>
    <Display(Name:="Percentage Of Rate", Description:="The percentage of the rate the must be paid if the cancellation is between the days from and days to"),
    Required(ErrorMessage:="Percentage Of Rate required")>
  Public Property PercentageOfRate() As Decimal
      Get
        Return GetProperty(PercentageOfRateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(PercentageOfRateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ISPCancellationRuleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "ISP Cancellation Rule")
        Else
          Return String.Format("Blank {0}", "ISP Cancellation Rule")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewISPCancellationRule() method.

    End Sub

    Public Shared Function NewISPCancellationRule() As ISPCancellationRule

      Return DataPortal.CreateChild(Of ISPCancellationRule)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetISPCancellationRule(dr As SafeDataReader) As ISPCancellationRule

      Dim i As New ISPCancellationRule()
      i.Fetch(dr)
      Return i

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ISPCancellationRuleIDProperty, .GetInt32(0))
          LoadProperty(DaysFromProperty, .GetInt32(1))
          LoadProperty(DaysToProperty, .GetInt32(2))
          LoadProperty(PercentageOfRateProperty, .GetDecimal(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insISPCancellationRule"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updISPCancellationRule"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramISPCancellationRuleID As SqlParameter = .Parameters.Add("@ISPCancellationRuleID", SqlDbType.Int)
          paramISPCancellationRuleID.Value = GetProperty(ISPCancellationRuleIDProperty)
          If Me.IsNew Then
            paramISPCancellationRuleID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@DaysFrom", GetProperty(DaysFromProperty))
          .Parameters.AddWithValue("@DaysTo", GetProperty(DaysToProperty))
          .Parameters.AddWithValue("@PercentageOfRate", GetProperty(PercentageOfRateProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ISPCancellationRuleIDProperty, paramISPCancellationRuleID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delISPCancellationRule"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ISPCancellationRuleID", GetProperty(ISPCancellationRuleIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace