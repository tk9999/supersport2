﻿' Generated 29 Jun 2014 16:58 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class CrewType
    Inherits SingularBusinessBase(Of CrewType)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CrewTypeID() As Integer
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
    End Property

    Public Shared CrewTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CrewType, "Crew Type", "")
    ''' <summary>
    ''' Gets and sets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="Description for the Crew Type"),
    Required(ErrorMessage:="Crew Type required"),
    StringLength(50, ErrorMessage:="Crew Type cannot be more than 50 characters")>
    Public Property CrewType() As String
      Get
        Return GetProperty(CrewTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CrewTypeProperty, Value)
      End Set
    End Property

    Public Shared RequiresVehicleIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresVehicleInd, "Requires Vehicle", False)
    ''' <summary>
    ''' Gets and sets the Requires Vehicle value
    ''' </summary>
    <Display(Name:="Requires Vehicle", Description:="True indicates that this crew type required a vehicle"),
    Required(ErrorMessage:="Requires Vehicle required")>
    Public Property RequiresVehicleInd() As Boolean
      Get
        Return GetProperty(RequiresVehicleIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RequiresVehicleIndProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList))>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared TravelScreenDisplayNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TravelScreenDisplayName, "Travel Screen Display Name", "")
    ''' <summary>
    ''' Gets and sets the Travel Screen Display Name value
    ''' </summary>
    <Display(Name:="Travel Screen Display Name", Description:=""),
    StringLength(50, ErrorMessage:="Travel Screen Display Name cannot be more than 50 characters")>
    Public Property TravelScreenDisplayName() As String
      Get
        Return GetProperty(TravelScreenDisplayNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TravelScreenDisplayNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared CrewTypeDisciplineListProperty As PropertyInfo(Of CrewTypeDisciplineList) = RegisterProperty(Of CrewTypeDisciplineList)(Function(c) c.CrewTypeDisciplineList, "Crew Type Discipline List")

    Public ReadOnly Property CrewTypeDisciplineList() As CrewTypeDisciplineList
      Get
        If GetProperty(CrewTypeDisciplineListProperty) Is Nothing Then
          LoadProperty(CrewTypeDisciplineListProperty, Maintenance.General.CrewTypeDisciplineList.NewCrewTypeDisciplineList())
        End If
        Return GetProperty(CrewTypeDisciplineListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CrewType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Crew Type")
        Else
          Return String.Format("Blank {0}", "Crew Type")
        End If
      Else
        Return Me.CrewType
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"CrewTypeDisciplines"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCrewType() method.

    End Sub

    Public Shared Function NewCrewType() As CrewType

      Return DataPortal.CreateChild(Of CrewType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCrewType(dr As SafeDataReader) As CrewType

      Dim c As New CrewType()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CrewTypeIDProperty, .GetInt32(0))
          LoadProperty(CrewTypeProperty, .GetString(1))
          LoadProperty(RequiresVehicleIndProperty, .GetBoolean(2))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(TravelScreenDisplayNameProperty, .GetString(4))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCrewType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCrewType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCrewTypeID As SqlParameter = .Parameters.Add("@CrewTypeID", SqlDbType.Int)
          paramCrewTypeID.Value = GetProperty(CrewTypeIDProperty)
          If Me.IsNew Then
            paramCrewTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@CrewType", GetProperty(CrewTypeProperty))
          .Parameters.AddWithValue("@RequiresVehicleInd", GetProperty(RequiresVehicleIndProperty))
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@TravelScreenDisplayName", GetProperty(TravelScreenDisplayNameProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CrewTypeIDProperty, paramCrewTypeID.Value)
          End If
          ' update child objects
          If GetProperty(CrewTypeDisciplineListProperty) IsNot Nothing Then
            Me.CrewTypeDisciplineList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(CrewTypeDisciplineListProperty) IsNot Nothing Then
          Me.CrewTypeDisciplineList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCrewType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CrewTypeID", GetProperty(CrewTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace