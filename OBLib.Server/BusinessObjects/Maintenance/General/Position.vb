﻿' Generated 05 Jul 2014 12:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class Position
    Inherits SingularBusinessBase(Of Position)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "PositionBO.PositionBOToString(self)")

    Public Shared PositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property PositionID() As Integer
      Get
        Return GetProperty(PositionIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position", "")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="Description for the Position"),
    Required(ErrorMessage:="Position required"),
    StringLength(50, ErrorMessage:="Position cannot be more than 50 characters")>
    Public Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionProperty, Value)
      End Set
    End Property

    Public Shared PositionCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionCode, "Position Code", "")
    ''' <summary>
    ''' Gets and sets the Position Code value
    ''' </summary>
    <Display(Name:="Position Code", Description:="Two character code for this position"),
    StringLength(2, ErrorMessage:="Position Code cannot be more than 2 characters")>
    Public Property PositionCode() As String
      Get
        Return GetProperty(PositionCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionCodeProperty, Value)
      End Set
    End Property

    Public Shared UnMannedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UnMannedInd, "Unmanned", False)
    ''' <summary>
    ''' Gets and sets the Unmanned value
    ''' </summary>
    <Display(Name:="Unmanned", Description:="Tick indicates that this position is unmanned"),
    Required(ErrorMessage:="Unmanned required")>
    Public Property UnMannedInd() As Boolean
      Get
        Return GetProperty(UnMannedIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(UnMannedIndProperty, Value)
      End Set
    End Property

    Public Shared PriorityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Priority, "Priority", 0)
    ''' <summary>
    ''' Gets and sets the Priority value
    ''' </summary>
    <Display(Name:="Priority", Description:=""),
    Required(ErrorMessage:="Priority required")>
    Public Property Priority() As Integer
      Get
        Return GetProperty(PriorityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PriorityProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.ReadOnly.ROSystemList))>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Discipline

      Return CType(CType(Me.Parent, PositionList).Parent, Discipline)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PositionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Position.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Position")
        Else
          Return String.Format("Blank {0}", "Position")
        End If
      Else
        Return Me.Position
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(PositionCodeProperty)
        .JavascriptRuleFunctionName = "PositionBO.PositionCodeValid"
        .ServerRuleFunction = AddressOf PositionCodeValid
      End With

    End Sub

    Public Shared Function PositionCodeValid(pos As Position) As String
      Dim ErrorString = ""
      If pos.Parent IsNot Nothing Then
        If pos.GetParent().DisciplineID = 4 AndAlso pos.PositionCode.Trim = "" Then
          ErrorString = "Position Code is required for discipline Camera Operator"
        End If
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPosition() method.

    End Sub

    Public Shared Function NewPosition() As Position

      Return DataPortal.CreateChild(Of Position)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetPosition(dr As SafeDataReader) As Position

      Dim p As New Position()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(PositionIDProperty, .GetInt32(0))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(PositionProperty, .GetString(2))
          LoadProperty(PositionCodeProperty, .GetString(3))
          LoadProperty(UnmannedIndProperty, .GetBoolean(4))
          LoadProperty(PriorityProperty, .GetInt32(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insPosition"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updPosition"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramPositionID As SqlParameter = .Parameters.Add("@PositionID", SqlDbType.Int)
          paramPositionID.Value = GetProperty(PositionIDProperty)
          If Me.IsNew Then
            paramPositionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@DisciplineID", Me.GetParent().DisciplineID)
          .Parameters.AddWithValue("@Position", GetProperty(PositionProperty))
          .Parameters.AddWithValue("@PositionCode", GetProperty(PositionCodeProperty))
          .Parameters.AddWithValue("@UnmannedInd", GetProperty(UnmannedIndProperty))
          .Parameters.AddWithValue("@Priority", GetProperty(PriorityProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(PositionIDProperty, paramPositionID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delPosition"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@PositionID", GetProperty(PositionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace