﻿' Generated 21 May 2014 13:27 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance

  <Serializable()> _
  Public Class Supplier
    Inherits SingularBusinessBase(Of Supplier)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SupplierID() As Integer
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared SupplierProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Supplier, "Supplier", "")
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="Name of the Supplier"),
    StringLength(50, ErrorMessage:="Supplier cannot be more than 50 characters")>
    Public Property Supplier() As String
      Get
        Return GetProperty(SupplierProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SupplierProperty, Value)
      End Set
    End Property

    Public Shared TelNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TelNo, "Tel No", "")
    ''' <summary>
    ''' Gets and sets the Tel No value
    ''' </summary>
    <Display(Name:="Tel No", Description:="Telephone number for the Supplier"),
    StringLength(20, ErrorMessage:="Tel No cannot be more than 20 characters")>
    Public Property TelNo() As String
      Get
        Return GetProperty(TelNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TelNoProperty, Value)
      End Set
    End Property

    Public Shared FaxNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FaxNo, "Fax No", "")
    ''' <summary>
    ''' Gets and sets the Fax No value
    ''' </summary>
    <Display(Name:="Fax No", Description:="Fax number for the Supplier"),
    StringLength(20, ErrorMessage:="Fax No cannot be more than 20 characters")>
    Public Property FaxNo() As String
      Get
        Return GetProperty(FaxNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FaxNoProperty, Value)
      End Set
    End Property

    Public Shared ContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactName, "Contact Name", "")
    ''' <summary>
    ''' Gets and sets the Contact Name value
    ''' </summary>
    <Display(Name:="Contact Name", Description:="Name of the contact person"),
    StringLength(100, ErrorMessage:="Contact Name cannot be more than 100 characters")>
    Public Property ContactName() As String
      Get
        Return GetProperty(ContactNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContactNameProperty, Value)
      End Set
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets and sets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="Email address for the Supplier"),
    StringLength(100, ErrorMessage:="Email Address cannot be more than 100 characters")>
    Public Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmailAddressProperty, Value)
      End Set
    End Property

    Public Shared VatNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VatNo, "Vat No", "")
    ''' <summary>
    ''' Gets and sets the Vat No value
    ''' </summary>
    <Display(Name:="Vat No", Description:="VAT number for the Supplier"),
    StringLength(20, ErrorMessage:="Vat No cannot be more than 20 characters")>
    Public Property VatNo() As String
      Get
        Return GetProperty(VatNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VatNoProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared SupplierHumanResourceListProperty As PropertyInfo(Of SupplierHumanResourceList) = RegisterProperty(Of SupplierHumanResourceList)(Function(c) c.SupplierHumanResourceList, "Position List")

    Public ReadOnly Property SupplierHumanResourceList() As SupplierHumanResourceList
      Get
        If GetProperty(SupplierHumanResourceListProperty) Is Nothing Then
          LoadProperty(SupplierHumanResourceListProperty, Maintenance.SupplierHumanResourceList.NewSupplierHumanResourceList())
        End If
        Return GetProperty(SupplierHumanResourceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SupplierIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Supplier.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Supplier")
        Else
          Return String.Format("Blank {0}", "Supplier")
        End If
      Else
        Return Me.Supplier
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSupplier() method.

    End Sub

    Public Shared Function NewSupplier() As Supplier

      Return DataPortal.CreateChild(Of Supplier)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSupplier(dr As SafeDataReader) As Supplier

      Dim s As New Supplier()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SupplierIDProperty, .GetInt32(0))
          LoadProperty(SupplierProperty, .GetString(1))
          LoadProperty(TelNoProperty, .GetString(2))
          LoadProperty(FaxNoProperty, .GetString(3))
          LoadProperty(ContactNameProperty, .GetString(4))
          LoadProperty(EmailAddressProperty, .GetString(5))
          LoadProperty(VatNoProperty, .GetString(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSupplier"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSupplier"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSupplierID As SqlParameter = .Parameters.Add("@SupplierID", SqlDbType.Int)
          paramSupplierID.Value = GetProperty(SupplierIDProperty)
          If Me.IsNew Then
            paramSupplierID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Supplier", GetProperty(SupplierProperty))
          .Parameters.AddWithValue("@TelNo", GetProperty(TelNoProperty))
          .Parameters.AddWithValue("@FaxNo", GetProperty(FaxNoProperty))
          .Parameters.AddWithValue("@ContactName", GetProperty(ContactNameProperty))
          .Parameters.AddWithValue("@EmailAddress", GetProperty(EmailAddressProperty))
          .Parameters.AddWithValue("@VatNo", GetProperty(VatNoProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SupplierIDProperty, paramSupplierID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSupplier"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SupplierID", GetProperty(SupplierIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace