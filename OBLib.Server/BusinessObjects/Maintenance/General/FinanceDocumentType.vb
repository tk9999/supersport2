﻿' Generated 26 Jan 2015 06:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class FinanceDocumentType
    Inherits SingularBusinessBase(Of FinanceDocumentType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FinanceDocumentTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FinanceDocumentTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FinanceDocumentTypeID() As Integer
      Get
        Return GetProperty(FinanceDocumentTypeIDProperty)
      End Get
    End Property

    Public Shared FinanceDocumentTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FinanceDocumentType, "Finance Document Type", "")
    ''' <summary>
    ''' Gets and sets the Finance Document Type value
    ''' </summary>
    <Display(Name:="Finance Document Type", Description:=""),
    StringLength(50, ErrorMessage:="Finance Document Type cannot be more than 50 characters")>
  Public Property FinanceDocumentType() As String
      Get
        Return GetProperty(FinanceDocumentTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FinanceDocumentTypeProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FinanceDocumentTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.FinanceDocumentType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Finance Document Type")
        Else
          Return String.Format("Blank {0}", "Finance Document Type")
        End If
      Else
        Return Me.FinanceDocumentType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFinanceDocumentType() method.

    End Sub

    Public Shared Function NewFinanceDocumentType() As FinanceDocumentType

      Return DataPortal.CreateChild(Of FinanceDocumentType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFinanceDocumentType(dr As SafeDataReader) As FinanceDocumentType

      Dim f As New FinanceDocumentType()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FinanceDocumentTypeIDProperty, .GetInt32(0))
          LoadProperty(FinanceDocumentTypeProperty, .GetString(1))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insFinanceDocumentType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFinanceDocumentType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramFinanceDocumentTypeID As SqlParameter = .Parameters.Add("@FinanceDocumentTypeID", SqlDbType.Int)
          paramFinanceDocumentTypeID.Value = GetProperty(FinanceDocumentTypeIDProperty)
          If Me.IsNew Then
            paramFinanceDocumentTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@FinanceDocumentType", GetProperty(FinanceDocumentTypeProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(FinanceDocumentTypeIDProperty, paramFinanceDocumentTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delFinanceDocumentType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@FinanceDocumentTypeID", GetProperty(FinanceDocumentTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace