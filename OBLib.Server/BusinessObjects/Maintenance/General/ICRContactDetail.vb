﻿' Generated 27 Jan 2015 11:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class ICRContactDetail
    Inherits OBBusinessBase(Of ICRContactDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ICRContactDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ICRContactDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ICRContactDetailID() As Integer
      Get
        Return GetProperty(ICRContactDetailIDProperty)
      End Get
    End Property

    Public Shared ICRContactDetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ICRContactDetail, "ICR Contact Detail", "")
    ''' <summary>
    ''' Gets and sets the ICR Contact Detail value
    ''' </summary>
    <Display(Name:="ICR Contact Detail", Description:="Contact detail information"),
    Required(ErrorMessage:="Contact Detail Required"),
    StringLength(500, ErrorMessage:="ICR Contact Detail cannot be more than 500 characters")>
    Public Property ICRContactDetail() As String
      Get
        Return GetProperty(ICRContactDetailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ICRContactDetailProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ICRContactDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ICRContactDetail.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "ICR Contact Detail")
        Else
          Return String.Format("Blank {0}", "ICR Contact Detail")
        End If
      Else
        Return Me.ICRContactDetail
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewICRContactDetail() method.

    End Sub

    Public Shared Function NewICRContactDetail() As ICRContactDetail

      Return DataPortal.CreateChild(Of ICRContactDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetICRContactDetail(dr As SafeDataReader) As ICRContactDetail

      Dim i As New ICRContactDetail()
      i.Fetch(dr)
      Return i

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ICRContactDetailIDProperty, .GetInt32(0))
          LoadProperty(ICRContactDetailProperty, .GetString(1))
          LoadProperty(CreatedByProperty, .GetInt32(2))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
          LoadProperty(ModifiedByProperty, .GetInt32(4))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insICRContactDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updICRContactDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramICRContactDetailID As SqlParameter = .Parameters.Add("@ICRContactDetailID", SqlDbType.Int)
          paramICRContactDetailID.Value = GetProperty(ICRContactDetailIDProperty)
          If Me.IsNew Then
            paramICRContactDetailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ICRContactDetail", GetProperty(ICRContactDetailProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ICRContactDetailIDProperty, paramICRContactDetailID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delICRContactDetail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ICRContactDetailID", GetProperty(ICRContactDetailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace