﻿' Generated 28 Apr 2015 09:43 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General

  <Serializable()> _
  Public Class AccessFlag
    Inherits OBBusinessBase(Of AccessFlag)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessFlagIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessFlagID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AccessFlagID() As Integer
      Get
        Return GetProperty(AccessFlagIDProperty)
      End Get
    End Property

    Public Shared FlagReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlagReason, "Flag Reason", "")
    ''' <summary>
    ''' Gets and sets the Flag Reason value
    ''' </summary>
    <Display(Name:="Flag Reason", Description:=""),
    StringLength(512, ErrorMessage:="Flag Reason cannot be more than 512 characters")>
  Public Property FlagReason() As String
      Get
        Return GetProperty(FlagReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FlagReasonProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessFlagIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.FlagReason.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Flag")
        Else
          Return String.Format("Blank {0}", "Access Flag")
        End If
      Else
        Return Me.FlagReason
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessFlag() method.

    End Sub

    Public Shared Function NewAccessFlag() As AccessFlag

      Return DataPortal.CreateChild(Of AccessFlag)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessFlag(dr As SafeDataReader) As AccessFlag

      Dim a As New AccessFlag()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessFlagIDProperty, .GetInt32(0))
          LoadProperty(FlagReasonProperty, .GetString(1))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessFlag"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessFlag"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessFlagID As SqlParameter = .Parameters.Add("@AccessFlagID", SqlDbType.Int)
          paramAccessFlagID.Value = GetProperty(AccessFlagIDProperty)
          If Me.IsNew Then
            paramAccessFlagID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@FlagReason", GetProperty(FlagReasonProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessFlagIDProperty, paramAccessFlagID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessFlag"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccessFlagID", GetProperty(AccessFlagIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace