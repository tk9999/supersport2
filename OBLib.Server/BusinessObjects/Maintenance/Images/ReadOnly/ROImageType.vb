﻿' Generated 08 Oct 2015 15:11 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.Images.ReadOnly

  <Serializable()> _
  Public Class ROImageType
    Inherits OBReadOnlyBase(Of ROImageType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ImageTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImageTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ImageTypeID() As Integer
      Get
        Return GetProperty(ImageTypeIDProperty)
      End Get
    End Property

    Public Shared ImageTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImageType, "Image Type", "")
    ''' <summary>
    ''' Gets the Image Type value
    ''' </summary>
    <Display(Name:="Image Type", Description:="")>
  Public ReadOnly Property ImageType() As String
      Get
        Return GetProperty(ImageTypeProperty)
      End Get
    End Property

    Public Shared WidthProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.Width, "Width", Nothing)
    ''' <summary>
    ''' Gets the Width value
    ''' </summary>
    <Display(Name:="Width")>
    Public ReadOnly Property Width() As Integer?
      Get
        Return GetProperty(WidthProperty)
      End Get
    End Property

    Public Shared HeightProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.Height, "Height", Nothing)
    ''' <summary>
    ''' Gets the Height value
    ''' </summary>
    <Display(Name:="Height")>
    Public ReadOnly Property Height() As Integer?
      Get
        Return GetProperty(HeightProperty)
      End Get
    End Property

    Public Shared FileSizeLimitProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FileSizeLimit, "FileSizeLimit", Nothing)
    ''' <summary>
    ''' Gets and sets the Height value
    ''' </summary>
    <Display(Name:="FileSizeLimit")>
    Public ReadOnly Property FileSizeLimit() As Integer?
      Get
        Return GetProperty(FileSizeLimitProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ImageTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ImageType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROImageType(dr As SafeDataReader) As ROImageType

      Dim r As New ROImageType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ImageTypeIDProperty, .GetInt32(0))
        LoadProperty(ImageTypeProperty, .GetString(1))
        LoadProperty(WidthProperty, ZeroNothing(.GetInt32(2)))
        LoadProperty(HeightProperty, ZeroNothing(.GetInt32(3)))
        LoadProperty(FileSizeLimitProperty, ZeroNothing(.GetInt32(4)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace