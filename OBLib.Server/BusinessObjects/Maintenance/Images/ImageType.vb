﻿' Generated 08 Oct 2015 15:10 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.Images

  <Serializable()> _
  Public Class ImageType
    Inherits OBBusinessBase(Of ImageType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ImageTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImageTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ImageTypeID() As Integer
      Get
        Return GetProperty(ImageTypeIDProperty)
      End Get
    End Property

    Public Shared ImageTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImageType, "Image Type", "")
    ''' <summary>
    ''' Gets and sets the Image Type value
    ''' </summary>
    <Display(Name:="Image Type", Description:=""),
    StringLength(50, ErrorMessage:="Image Type cannot be more than 50 characters")>
  Public Property ImageType() As String
      Get
        Return GetProperty(ImageTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ImageTypeProperty, Value)
      End Set
    End Property

    Public Shared WidthProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Width, "Width", 0)
    ''' <summary>
    ''' Gets and sets the Width value
    ''' </summary>
    <Display(Name:="Width", Description:="")>
  Public Property Width() As Integer
      Get
        Return GetProperty(WidthProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WidthProperty, Value)
      End Set
    End Property

    Public Shared HeightProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Height, "Height", 0)
    ''' <summary>
    ''' Gets and sets the Height value
    ''' </summary>
    <Display(Name:="Height", Description:="")>
  Public Property Height() As Integer
      Get
        Return GetProperty(HeightProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HeightProperty, Value)
      End Set
    End Property

    Public Shared FileSizeLimitProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FileSizeLimit, "FileSizeLimit", 0)
    ''' <summary>
    ''' Gets and sets the Height value
    ''' </summary>
    <Display(Name:="FileSizeLimit")>
    Public Property FileSizeLimit() As Integer
      Get
        Return GetProperty(FileSizeLimitProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(FileSizeLimitProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ImageTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ImageType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Image Type")
        Else
          Return String.Format("Blank {0}", "Image Type")
        End If
      Else
        Return Me.ImageType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewImageType() method.

    End Sub

    Public Shared Function NewImageType() As ImageType

      Return DataPortal.CreateChild(Of ImageType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetImageType(dr As SafeDataReader) As ImageType

      Dim i As New ImageType()
      i.Fetch(dr)
      Return i

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ImageTypeIDProperty, .GetInt32(0))
          LoadProperty(ImageTypeProperty, .GetString(1))
          LoadProperty(WidthProperty, .GetInt32(2))
          LoadProperty(HeightProperty, .GetInt32(3))
          LoadProperty(FileSizeLimitProperty, .GetInt32(4))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insImageType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updImageType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramImageTypeID As SqlParameter = .Parameters.Add("@ImageTypeID", SqlDbType.Int)
          paramImageTypeID.Value = GetProperty(ImageTypeIDProperty)
          If Me.IsNew Then
            paramImageTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ImageType", GetProperty(ImageTypeProperty))
          .Parameters.AddWithValue("@Width", GetProperty(WidthProperty))
          .Parameters.AddWithValue("@Height", GetProperty(HeightProperty))
          .Parameters.AddWithValue("@FileSizeLimit", GetProperty(FileSizeLimitProperty))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ImageTypeIDProperty, paramImageTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delImageType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ImageTypeID", GetProperty(ImageTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace