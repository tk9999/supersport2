﻿' Generated 19 Feb 2015 10:51 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Equipment

  <Serializable()> _
  Public Class EquipmentSubType
    Inherits OBBusinessBase(Of EquipmentSubType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EquipmentSubTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentSubTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property EquipmentSubTypeID() As Integer
      Get
        Return GetProperty(EquipmentSubTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentTypeID, "Equipment Type", Nothing)
    ''' <summary>
    ''' Gets the Equipment Type value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property EquipmentTypeID() As Integer?
      Get
        Return GetProperty(EquipmentTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentSubTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentSubType, "Equipment Sub Type", "")
    ''' <summary>
    ''' Gets and sets the Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Equipment Sub Type", Description:="Description for the Equipment Sub Type"),
    Required(ErrorMessage:="Equipment Sub Type required"),
    StringLength(50, ErrorMessage:="Equipment Sub Type cannot be more than 50 characters")>
    Public Property EquipmentSubType() As String
      Get
        Return GetProperty(EquipmentSubTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EquipmentSubTypeProperty, Value)
      End Set
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared UnMannedIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.UnMannedInd, "Un Manned", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Un Manned value
    ''' </summary>
    <Display(Name:="Un Manned", Description:="Does this equipment sub type need to be manned?")>
    Public Property UnMannedInd() As Boolean?
      Get
        Return GetProperty(UnMannedIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(UnMannedIndProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As EquipmentType

      Return CType(CType(Me.Parent, EquipmentSubTypeList).Parent, EquipmentType)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentSubTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EquipmentSubType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Equipment Sub Type")
        Else
          Return String.Format("Blank {0}", "Equipment Sub Type")
        End If
      Else
        Return Me.EquipmentSubType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipmentSubType() method.

    End Sub

    Public Shared Function NewEquipmentSubType() As EquipmentSubType

      Return DataPortal.CreateChild(Of EquipmentSubType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEquipmentSubType(dr As SafeDataReader) As EquipmentSubType

      Dim e As New EquipmentSubType()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EquipmentSubTypeIDProperty, .GetInt32(0))
          LoadProperty(EquipmentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EquipmentSubTypeProperty, .GetString(2))
          Dim tmpUnMannedInd = .GetValue(3)
          If IsDBNull(tmpUnMannedInd) Then
            LoadProperty(UnMannedIndProperty, Nothing)
          Else
            LoadProperty(UnMannedIndProperty, tmpUnMannedInd)
          End If
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))

        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEquipmentSubType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEquipmentSubType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramEquipmentSubTypeID As SqlParameter = .Parameters.Add("@EquipmentSubTypeID", SqlDbType.Int)
          paramEquipmentSubTypeID.Value = GetProperty(EquipmentSubTypeIDProperty)
          If Me.IsNew Then
            paramEquipmentSubTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EquipmentTypeID", Me.GetParent().EquipmentTypeID)
          .Parameters.AddWithValue("@EquipmentSubType", GetProperty(EquipmentSubTypeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@UnMannedInd", Singular.Misc.NothingDBNull(GetProperty(UnMannedIndProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EquipmentSubTypeIDProperty, paramEquipmentSubTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEquipmentSubType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EquipmentSubTypeID", GetProperty(EquipmentSubTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace