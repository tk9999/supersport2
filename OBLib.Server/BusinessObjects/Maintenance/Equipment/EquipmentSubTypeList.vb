﻿' Generated 19 Feb 2015 10:51 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Equipment

  <Serializable()> _
  Public Class EquipmentSubTypeList
    Inherits OBBusinessListBase(Of EquipmentSubTypeList, EquipmentSubType)

#Region " Business Methods "

    Public Function GetItem(EquipmentSubTypeID As Integer) As EquipmentSubType

      For Each child As EquipmentSubType In Me
        If child.EquipmentSubTypeID = EquipmentSubTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Equipment Sub Types"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewEquipmentSubTypeList() As EquipmentSubTypeList

      Return New EquipmentSubTypeList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As EquipmentSubType In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As EquipmentSubType In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace