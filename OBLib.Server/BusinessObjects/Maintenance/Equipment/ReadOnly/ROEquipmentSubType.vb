﻿' Generated 02 Mar 2015 15:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Equipment.ReadOnly

  <Serializable()> _
  Public Class ROEquipmentSubType
    Inherits OBReadOnlyBase(Of ROEquipmentSubType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EquipmentSubTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentSubTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property EquipmentSubTypeID() As Integer
      Get
        Return GetProperty(EquipmentSubTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentTypeID, "Equipment Type", Nothing)
    ''' <summary>
    ''' Gets the Equipment Type value
    ''' </summary>
    <Display(Name:="Equipment Type", Description:="The equipment type this sup type is for")>
  Public ReadOnly Property EquipmentTypeID() As Integer?
      Get
        Return GetProperty(EquipmentTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentSubTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentSubType, "Equipment Sub Type", "")
    ''' <summary>
    ''' Gets the Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Equipment Sub Type", Description:="Description for the Equipment Sub Type")>
  Public ReadOnly Property EquipmentSubType() As String
      Get
        Return GetProperty(EquipmentSubTypeProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared UnMannedIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.UnMannedInd, "Un Manned", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets the Un Manned value
    ''' </summary>
    <Display(Name:="Un Manned", Description:="Does this equipment sub type need to be manned?")>
  Public ReadOnly Property UnMannedInd() As Boolean?
      Get
        Return GetProperty(UnMannedIndProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentSubTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EquipmentSubType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEquipmentSubType(dr As SafeDataReader) As ROEquipmentSubType

      Dim r As New ROEquipmentSubType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EquipmentSubTypeIDProperty, .GetInt32(0))
        LoadProperty(EquipmentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(EquipmentSubTypeProperty, .GetString(2))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        Dim tmpUnMannedInd = .GetValue(7)
        If IsDBNull(tmpUnMannedInd) Then
          LoadProperty(UnMannedIndProperty, Nothing)
        Else
          LoadProperty(UnMannedIndProperty, tmpUnMannedInd)
        End If
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace