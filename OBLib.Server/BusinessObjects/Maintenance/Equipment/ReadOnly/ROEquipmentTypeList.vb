﻿' Generated 02 Mar 2015 15:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Equipment.ReadOnly

  <Serializable()> _
  Public Class ROEquipmentTypeList
    Inherits OBReadOnlyListBase(Of ROEquipmentTypeList, ROEquipmentType)

#Region " Business Methods "

    Public Function GetItem(EquipmentTypeID As Integer) As ROEquipmentType

      For Each child As ROEquipmentType In Me
        If child.EquipmentTypeID = EquipmentTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Equipment Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROEquipmentTypeList() As ROEquipmentTypeList

      Return New ROEquipmentTypeList()

    End Function

    Public Shared Sub BeginGetROEquipmentTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentTypeList)))

      Dim dp As New DataPortal(Of ROEquipmentTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROEquipmentTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentTypeList)))

      Dim dp As New DataPortal(Of ROEquipmentTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROEquipmentTypeList() As ROEquipmentTypeList

      Return DataPortal.Fetch(Of ROEquipmentTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEquipmentType.GetROEquipmentType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEquipmentTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace