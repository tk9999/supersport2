﻿' Generated 19 Feb 2015 10:51 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Equipment

  <Serializable()> _
  Public Class EquipmentType
    Inherits OBBusinessBase(Of EquipmentType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EquipmentTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property EquipmentTypeID() As Integer
      Get
        Return GetProperty(EquipmentTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentType, "Equipment Type", "")
    ''' <summary>
    ''' Gets and sets the Equipment Type value
    ''' </summary>
    <Display(Name:="Equipment Type", Description:="Description for the Equipment Type"),
    Required(ErrorMessage:="Equipment Type required"),
    StringLength(50, ErrorMessage:="Equipment Type cannot be more than 50 characters")>
    Public Property EquipmentType() As String
      Get
        Return GetProperty(EquipmentTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EquipmentTypeProperty, Value)
      End Set
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", False)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="Tick indicates that this OutsourceServiceType is core to the system"),
    Required(ErrorMessage:="System required")>
    Public Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SystemIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False


#End Region

#Region " Child Lists "

    Public Shared EquipmentSubTypeListProperty As PropertyInfo(Of EquipmentSubTypeList) = RegisterProperty(Of EquipmentSubTypeList)(Function(c) c.EquipmentSubTypeList, "Equipment Sub Type List")

    Public ReadOnly Property EquipmentSubTypeList() As EquipmentSubTypeList
      Get
        If GetProperty(EquipmentSubTypeListProperty) Is Nothing Then
          LoadProperty(EquipmentSubTypeListProperty, Maintenance.Equipment.EquipmentSubTypeList.NewEquipmentSubTypeList())
        End If
        Return GetProperty(EquipmentSubTypeListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EquipmentType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Equipment Type")
        Else
          Return String.Format("Blank {0}", "Equipment Type")
        End If
      Else
        Return Me.EquipmentType
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"EquipmentSubTypes"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipmentType() method.

    End Sub

    Public Shared Function NewEquipmentType() As EquipmentType

      Return DataPortal.CreateChild(Of EquipmentType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEquipmentType(dr As SafeDataReader) As EquipmentType

      Dim e As New EquipmentType()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EquipmentTypeIDProperty, .GetInt32(0))
          LoadProperty(EquipmentTypeProperty, .GetString(1))
          LoadProperty(SystemIndProperty, .GetBoolean(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEquipmentType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEquipmentType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramEquipmentTypeID As SqlParameter = .Parameters.Add("@EquipmentTypeID", SqlDbType.Int)
          paramEquipmentTypeID.Value = GetProperty(EquipmentTypeIDProperty)
          If Me.IsNew Then
            paramEquipmentTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EquipmentType", GetProperty(EquipmentTypeProperty))
          .Parameters.AddWithValue("@SystemInd", GetProperty(SystemIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EquipmentTypeIDProperty, paramEquipmentTypeID.Value)
          End If
          ' update child objects
          If GetProperty(EquipmentSubTypeListProperty) IsNot Nothing Then
            Me.EquipmentSubTypeList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(EquipmentSubTypeListProperty) IsNot Nothing Then
          Me.EquipmentSubTypeList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEquipmentType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EquipmentTypeID", GetProperty(EquipmentTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace