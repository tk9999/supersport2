﻿' Generated 19 Feb 2015 10:51 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Equipment

  <Serializable()> _
  Public Class EquipmentTypeList
    Inherits OBBusinessListBase(Of EquipmentTypeList, EquipmentType)

#Region " Business Methods "

    Public Function GetItem(EquipmentTypeID As Integer) As EquipmentType

      For Each child As EquipmentType In Me
        If child.EquipmentTypeID = EquipmentTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Equipment Types"

    End Function

    Public Function GetEquipmentSubType(EquipmentSubTypeID As Integer) As EquipmentSubType

      Dim obj As EquipmentSubType = Nothing
      For Each parent As EquipmentType In Me
        obj = parent.EquipmentSubTypeList.GetItem(EquipmentSubTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewEquipmentTypeList() As EquipmentTypeList

      Return New EquipmentTypeList()

    End Function

    Public Shared Sub BeginGetEquipmentTypeList(CallBack As EventHandler(Of DataPortalResult(Of EquipmentTypeList)))

      Dim dp As New DataPortal(Of EquipmentTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetEquipmentTypeList() As EquipmentTypeList

      Return DataPortal.Fetch(Of EquipmentTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(EquipmentType.GetEquipmentType(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As EquipmentType = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.EquipmentTypeID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.EquipmentSubTypeList.RaiseListChangedEvents = False
          parent.EquipmentSubTypeList.Add(EquipmentSubType.GetEquipmentSubType(sdr))
          parent.EquipmentSubTypeList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As EquipmentType In Me
        child.CheckRules()
        For Each EquipmentSubType As EquipmentSubType In child.EquipmentSubTypeList
          EquipmentSubType.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getEquipmentTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As EquipmentType In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As EquipmentType In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace