﻿' Generated 13 Jun 2015 16:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.Resources.ReadOnly

  <Serializable()> _
  Public Class ROResourceBookingType
    Inherits SingularReadOnlyBase(Of ROResourceBookingType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ResourceBookingTypeID() As Integer
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
    End Property

    Public Shared ResourceBookingTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingType, "Resource Booking Type", "")
    ''' <summary>
    ''' Gets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type", Description:="")>
  Public ReadOnly Property ResourceBookingType() As String
      Get
        Return GetProperty(ResourceBookingTypeProperty)
      End Get
    End Property

    Public Shared IsSharedTypeProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsSharedType, "Is Shared Type", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Shared Type")>
    Public ReadOnly Property IsSharedType() As Boolean
      Get
        Return GetProperty(IsSharedTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceBookingType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROResourceBookingType(dr As SafeDataReader) As ROResourceBookingType

      Dim r As New ROResourceBookingType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ResourceBookingTypeIDProperty, .GetInt32(0))
        LoadProperty(ResourceBookingTypeProperty, .GetString(1))
        LoadProperty(IsSharedTypeProperty, .GetBoolean(2))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace