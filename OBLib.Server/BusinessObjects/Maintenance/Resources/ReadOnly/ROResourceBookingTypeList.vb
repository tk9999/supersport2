﻿' Generated 13 Jun 2015 16:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.Resources.ReadOnly

  <Serializable()> _
  Public Class ROResourceBookingTypeList
    Inherits SingularReadOnlyListBase(Of ROResourceBookingTypeList, ROResourceBookingType)

#Region " Business Methods "

    Public Function GetItem(ResourceBookingTypeID As Integer) As ROResourceBookingType

      For Each child As ROResourceBookingType In Me
        If child.ResourceBookingTypeID = ResourceBookingTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Booking Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROResourceBookingTypeList() As ROResourceBookingTypeList

      Return New ROResourceBookingTypeList()

    End Function

    Public Shared Sub BeginGetROResourceBookingTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROResourceBookingTypeList)))

      Dim dp As New DataPortal(Of ROResourceBookingTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROResourceBookingTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROResourceBookingTypeList)))

      Dim dp As New DataPortal(Of ROResourceBookingTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROResourceBookingTypeList() As ROResourceBookingTypeList

      Return DataPortal.Fetch(Of ROResourceBookingTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROResourceBookingType.GetROResourceBookingType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROResourceBookingTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace