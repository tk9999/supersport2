﻿' Generated 30 Apr 2015 10:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Resources.ReadOnly

  <Serializable()> _
  Public Class ROResourceTypeList
    Inherits SingularReadOnlyListBase(Of ROResourceTypeList, ROResourceType)

#Region " Business Methods "

    Public Function GetItem(ResourceTypeID As Integer) As ROResourceType

      For Each child As ROResourceType In Me
        If child.ResourceTypeID = ResourceTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROResourceTypeList() As ROResourceTypeList

      Return New ROResourceTypeList()

    End Function

    Public Shared Sub BeginGetROResourceTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROResourceTypeList)))

      Dim dp As New DataPortal(Of ROResourceTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROResourceTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROResourceTypeList)))

      Dim dp As New DataPortal(Of ROResourceTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROResourceTypeList() As ROResourceTypeList

      Return DataPortal.Fetch(Of ROResourceTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROResourceType.GetROResourceType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROResourceTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace