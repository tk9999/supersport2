﻿' Generated 14 Apr 2014 08:58 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Invoicing

  <Serializable()> _
  Public Class TransactionType
    Inherits SingularBusinessBase(Of TransactionType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TransactionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TransactionTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property TransactionTypeID() As Integer
      Get
        Return GetProperty(TransactionTypeIDProperty)
      End Get
    End Property

    Public Shared TransactionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TransactionType, "Transaction Type", "")
    ''' <summary>
    ''' Gets and sets the Transaction Type value
    ''' </summary>
    <Display(Name:="Transaction Type", Description:="Name of the Transaction Type"),
    StringLength(50, ErrorMessage:="Transaction Type cannot be more than 50 characters")>
    Public Property TransactionType() As String
      Get
        Return GetProperty(TransactionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TransactionTypeProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TransactionTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.TransactionType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Transaction Type")
        Else
          Return String.Format("Blank {0}", "Transaction Type")
        End If
      Else
        Return Me.TransactionType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTransactionType() method.

    End Sub

    Public Shared Function NewTransactionType() As TransactionType

      Return DataPortal.CreateChild(Of TransactionType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTransactionType(dr As SafeDataReader) As TransactionType

      Dim t As New TransactionType()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TransactionTypeIDProperty, .GetInt32(0))
          LoadProperty(TransactionTypeProperty, .GetString(1))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insTransactionType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updTransactionType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTransactionTypeID As SqlParameter = .Parameters.Add("@TransactionTypeID", SqlDbType.Int)
          paramTransactionTypeID.Value = GetProperty(TransactionTypeIDProperty)
          If Me.IsNew Then
            paramTransactionTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@TransactionType", GetProperty(TransactionTypeProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(TransactionTypeIDProperty, paramTransactionTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delTransactionType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TransactionTypeID", GetProperty(TransactionTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace