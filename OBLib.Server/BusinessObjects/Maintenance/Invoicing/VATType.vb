﻿' Generated 14 Apr 2014 08:58 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance

  <Serializable()> _
  Public Class VATType
    Inherits SingularBusinessBase(Of VATType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VatTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VatTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property VatTypeID() As Integer
      Get
        Return GetProperty(VatTypeIDProperty)
      End Get
    End Property

    Public Shared VatTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VatType, "Vat Type", "")
    ''' <summary>
    ''' Gets and sets the Vat Type value
    ''' </summary>
    <Display(Name:="Vat Type", Description:="The name of the VAT Type"),
    StringLength(50, ErrorMessage:="Vat Type cannot be more than 50 characters")>
    Public Property VatType() As String
      Get
        Return GetProperty(VatTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VatTypeProperty, Value)
      End Set
    End Property

    Public Shared VatPercentageProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.VatPercentage, "Vat Percentage", 0)
    ''' <summary>
    ''' Gets and sets the Vat Percentage value
    ''' </summary>
    <Display(Name:="Vat Percentage", Description:="Percentage of VAT applicable for the VAT Type"),
    Required(ErrorMessage:="Vat Percentage required")>
    Public Property VatPercentage() As Decimal
      Get
        Return GetProperty(VatPercentageProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(VatPercentageProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VatTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.VatType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "VAT Type")
        Else
          Return String.Format("Blank {0}", "VAT Type")
        End If
      Else
        Return Me.VatType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewVATType() method.

    End Sub

    Public Shared Function NewVATType() As VATType

      Return DataPortal.CreateChild(Of VATType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetVATType(dr As SafeDataReader) As VATType

      Dim v As New VATType()
      v.Fetch(dr)
      Return v

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(VatTypeIDProperty, .GetInt32(0))
          LoadProperty(VatTypeProperty, .GetString(1))
          LoadProperty(VatPercentageProperty, .GetDecimal(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insVATType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updVATType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramVatTypeID As SqlParameter = .Parameters.Add("@VatTypeID", SqlDbType.Int)
          paramVatTypeID.Value = GetProperty(VatTypeIDProperty)
          If Me.IsNew Then
            paramVatTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@VatType", GetProperty(VatTypeProperty))
          .Parameters.AddWithValue("@VatPercentage", GetProperty(VatPercentageProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(VatTypeIDProperty, paramVatTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delVATType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@VatTypeID", GetProperty(VatTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace