﻿' Generated 14 Apr 2014 08:58 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Invoicing

  <Serializable()> _
  Public Class CreditorInvoiceDetailType
    Inherits SingularBusinessBase(Of CreditorInvoiceDetailType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CreditorInvoiceDetailTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreditorInvoiceDetailTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CreditorInvoiceDetailTypeID() As Integer
      Get
        Return GetProperty(CreditorInvoiceDetailTypeIDProperty)
      End Get
    End Property

    Public Shared CreditorInvoiceDetailTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreditorInvoiceDetailType, "Creditor Invoice Detail Type", "")
    ''' <summary>
    ''' Gets and sets the Creditor Invoice Detail Type value
    ''' </summary>
    <Display(Name:="Creditor Invoice Detail Type", Description:="Invoice detail type name"),
    StringLength(50, ErrorMessage:="Creditor Invoice Detail Type cannot be more than 50 characters")>
    Public Property CreditorInvoiceDetailType() As String
      Get
        Return GetProperty(CreditorInvoiceDetailTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CreditorInvoiceDetailTypeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CreditorInvoiceDetailTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreditorInvoiceDetailType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Creditor Invoice Detail Type")
        Else
          Return String.Format("Blank {0}", "Creditor Invoice Detail Type")
        End If
      Else
        Return Me.CreditorInvoiceDetailType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCreditorInvoiceDetailType() method.

    End Sub

    Public Shared Function NewCreditorInvoiceDetailType() As CreditorInvoiceDetailType

      Return DataPortal.CreateChild(Of CreditorInvoiceDetailType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCreditorInvoiceDetailType(dr As SafeDataReader) As CreditorInvoiceDetailType

      Dim c As New CreditorInvoiceDetailType()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CreditorInvoiceDetailTypeIDProperty, .GetInt32(0))
          LoadProperty(CreditorInvoiceDetailTypeProperty, .GetString(1))
          LoadProperty(CreatedByProperty, .GetInt32(2))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
          LoadProperty(ModifiedByProperty, .GetInt32(4))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insCreditorInvoiceDetailType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updCreditorInvoiceDetailType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCreditorInvoiceDetailTypeID As SqlParameter = .Parameters.Add("@CreditorInvoiceDetailTypeID", SqlDbType.Int)
          paramCreditorInvoiceDetailTypeID.Value = GetProperty(CreditorInvoiceDetailTypeIDProperty)
          If Me.IsNew Then
            paramCreditorInvoiceDetailTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@CreditorInvoiceDetailType", GetProperty(CreditorInvoiceDetailTypeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CreditorInvoiceDetailTypeIDProperty, paramCreditorInvoiceDetailTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delCreditorInvoiceDetailType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CreditorInvoiceDetailTypeID", GetProperty(CreditorInvoiceDetailTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace