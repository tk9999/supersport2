﻿' Generated 14 Apr 2014 09:35 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROTransactionType
    Inherits SingularReadOnlyBase(Of ROTransactionType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TransactionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TransactionTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property TransactionTypeID() As Integer
      Get
        Return GetProperty(TransactionTypeIDProperty)
      End Get
    End Property

    Public Shared TransactionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TransactionType, "Transaction Type", "")
    ''' <summary>
    ''' Gets the Transaction Type value
    ''' </summary>
    <Display(Name:="Transaction Type", Description:="Name of the Transaction Type")>
    Public ReadOnly Property TransactionType() As String
      Get
        Return GetProperty(TransactionTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TransactionTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.TransactionType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROTransactionType(dr As SafeDataReader) As ROTransactionType

      Dim r As New ROTransactionType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TransactionTypeIDProperty, .GetInt32(0))
        LoadProperty(TransactionTypeProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace