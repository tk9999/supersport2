﻿' Generated 14 Apr 2014 09:41 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROAccount
    Inherits SingularReadOnlyBase(Of ROAccount)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccountIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccountID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccountID() As Integer
      Get
        Return GetProperty(AccountIDProperty)
      End Get
    End Property

    Public Shared AccountCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccountCode, "Account Code", "")
    ''' <summary>
    ''' Gets the Account Code value
    ''' </summary>
    <Display(Name:="Account Code", Description:="")>
    Public ReadOnly Property AccountCode() As String
      Get
        Return GetProperty(AccountCodeProperty)
      End Get
    End Property

    Public Shared AccountProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Account, "Account", "")
    ''' <summary>
    ''' Gets the Account value
    ''' </summary>
    <Display(Name:="Account", Description:="")>
    Public ReadOnly Property Account() As String
      Get
        Return GetProperty(AccountProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccountIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AccountCode

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAccount(dr As SafeDataReader) As ROAccount

      Dim r As New ROAccount()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AccountIDProperty, .GetInt32(0))
        LoadProperty(AccountCodeProperty, .GetString(1))
        LoadProperty(AccountProperty, .GetString(2))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace