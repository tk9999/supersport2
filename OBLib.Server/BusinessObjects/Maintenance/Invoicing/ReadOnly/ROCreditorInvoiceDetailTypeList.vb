﻿' Generated 14 Apr 2014 09:35 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROCreditorInvoiceDetailTypeList
    Inherits SingularReadOnlyListBase(Of ROCreditorInvoiceDetailTypeList, ROCreditorInvoiceDetailType)

#Region " Business Methods "

    Public Function GetItem(CreditorInvoiceDetailTypeID As Integer) As ROCreditorInvoiceDetailType

      For Each child As ROCreditorInvoiceDetailType In Me
        If child.CreditorInvoiceDetailTypeID = CreditorInvoiceDetailTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Creditor Invoice Detail Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROCreditorInvoiceDetailTypeList() As ROCreditorInvoiceDetailTypeList

      Return New ROCreditorInvoiceDetailTypeList()

    End Function

    Public Shared Sub BeginGetROCreditorInvoiceDetailTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROCreditorInvoiceDetailTypeList)))

      Dim dp As New DataPortal(Of ROCreditorInvoiceDetailTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROCreditorInvoiceDetailTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROCreditorInvoiceDetailTypeList)))

      Dim dp As New DataPortal(Of ROCreditorInvoiceDetailTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROCreditorInvoiceDetailTypeList() As ROCreditorInvoiceDetailTypeList

      Return DataPortal.Fetch(Of ROCreditorInvoiceDetailTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROCreditorInvoiceDetailType.GetROCreditorInvoiceDetailType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcs.getROCreditorInvoiceDetailTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace