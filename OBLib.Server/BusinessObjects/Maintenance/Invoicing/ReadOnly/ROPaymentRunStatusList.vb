﻿' Generated 18 Jun 2014 17:41 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROPaymentRunStatusList
    Inherits SingularReadOnlyListBase(Of ROPaymentRunStatusList, ROPaymentRunStatus)

#Region " Business Methods "

    Public Function GetItem(PaymentRunStatusID As Integer) As ROPaymentRunStatus

      For Each child As ROPaymentRunStatus In Me
        If child.PaymentRunStatusID = PaymentRunStatusID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Payment Run Statuses"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROPaymentRunStatusList() As ROPaymentRunStatusList

      Return New ROPaymentRunStatusList()

    End Function

    Public Shared Sub BeginGetROPaymentRunStatusList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROPaymentRunStatusList)))

      Dim dp As New DataPortal(Of ROPaymentRunStatusList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROPaymentRunStatusList(CallBack As EventHandler(Of DataPortalResult(Of ROPaymentRunStatusList)))

      Dim dp As New DataPortal(Of ROPaymentRunStatusList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROPaymentRunStatusList() As ROPaymentRunStatusList

      Return DataPortal.Fetch(Of ROPaymentRunStatusList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPaymentRunStatus.GetROPaymentRunStatus(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROPaymentRunStatusList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace