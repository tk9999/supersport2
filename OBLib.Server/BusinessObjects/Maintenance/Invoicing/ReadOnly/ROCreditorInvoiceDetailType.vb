﻿' Generated 14 Apr 2014 09:35 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Invoicing.ReadOnly

  <Serializable()> _
  Public Class ROCreditorInvoiceDetailType
    Inherits SingularReadOnlyBase(Of ROCreditorInvoiceDetailType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CreditorInvoiceDetailTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreditorInvoiceDetailTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CreditorInvoiceDetailTypeID() As Integer
      Get
        Return GetProperty(CreditorInvoiceDetailTypeIDProperty)
      End Get
    End Property

    Public Shared CreditorInvoiceDetailTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreditorInvoiceDetailType, "Creditor Invoice Detail Type", "")
    ''' <summary>
    ''' Gets the Creditor Invoice Detail Type value
    ''' </summary>
    <Display(Name:="Creditor Invoice Detail Type", Description:="Invoice detail type name")>
    Public ReadOnly Property CreditorInvoiceDetailType() As String
      Get
        Return GetProperty(CreditorInvoiceDetailTypeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CreditorInvoiceDetailTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreditorInvoiceDetailType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCreditorInvoiceDetailType(dr As SafeDataReader) As ROCreditorInvoiceDetailType

      Dim r As New ROCreditorInvoiceDetailType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CreditorInvoiceDetailTypeIDProperty, .GetInt32(0))
        LoadProperty(CreditorInvoiceDetailTypeProperty, .GetString(1))
        LoadProperty(CreatedByProperty, .GetInt32(2))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
        LoadProperty(ModifiedByProperty, .GetInt32(4))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace