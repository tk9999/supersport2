﻿' Generated 18 Jun 2014 17:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Invoicing

  <Serializable()> _
  Public Class PaymentRunStatus
    Inherits SingularBusinessBase(Of PaymentRunStatus)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PaymentRunStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PaymentRunStatusID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property PaymentRunStatusID() As Integer
      Get
        Return GetProperty(PaymentRunStatusIDProperty)
      End Get
    End Property

    Public Shared PaymentRunStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PaymentRunStatus, "Payment Run Status", "")
    ''' <summary>
    ''' Gets and sets the Payment Run Status value
    ''' </summary>
    <Display(Name:="Payment Run Status", Description:="Status name"),
    Required(ErrorMessage:="Status Name Required"),
    StringLength(50, ErrorMessage:="Payment Run Status cannot be more than 50 characters")>
    Public Property PaymentRunStatus() As String
      Get
        Return GetProperty(PaymentRunStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PaymentRunStatusProperty, Value)
      End Set
    End Property

    Public Shared OrderNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNo, "Order No", 1)
    ''' <summary>
    ''' Gets and sets the Order No value
    ''' </summary>
    <Display(Name:="Order No", Description:="sequence no."),
    Required(ErrorMessage:="Order No required")>
    Public Property OrderNo() As Integer
      Get
        Return GetProperty(OrderNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OrderNoProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PaymentRunStatusIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.PaymentRunStatus.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Payment Run Status")
        Else
          Return String.Format("Blank {0}", "Payment Run Status")
        End If
      Else
        Return Me.PaymentRunStatus
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPaymentRunStatus() method.

    End Sub

    Public Shared Function NewPaymentRunStatus() As PaymentRunStatus

      Return DataPortal.CreateChild(Of PaymentRunStatus)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetPaymentRunStatus(dr As SafeDataReader) As PaymentRunStatus

      Dim p As New PaymentRunStatus()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(PaymentRunStatusIDProperty, .GetInt32(0))
          LoadProperty(PaymentRunStatusProperty, .GetString(1))
          LoadProperty(OrderNoProperty, .GetInt32(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insPaymentRunStatus"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updPaymentRunStatus"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramPaymentRunStatusID As SqlParameter = .Parameters.Add("@PaymentRunStatusID", SqlDbType.Int)
          paramPaymentRunStatusID.Value = GetProperty(PaymentRunStatusIDProperty)
          If Me.IsNew Then
            paramPaymentRunStatusID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@PaymentRunStatus", GetProperty(PaymentRunStatusProperty))
          .Parameters.AddWithValue("@OrderNo", GetProperty(OrderNoProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(PaymentRunStatusIDProperty, paramPaymentRunStatusID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delPaymentRunStatus"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@PaymentRunStatusID", GetProperty(PaymentRunStatusIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace