﻿' Generated 28 Mar 2015 12:29 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.SatOps.ReadOnly

Namespace Maintenance.SatOps

  <Serializable()> _
  Public Class TurnAroundPointList
    Inherits OBBusinessListBase(Of TurnAroundPointList, TurnAroundPoint)

#Region " Business Methods "

    Public Function GetItem(TurnAroundPointID As Integer) As TurnAroundPoint

      For Each child As TurnAroundPoint In Me
        If child.TurnAroundPointID = TurnAroundPointID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Turn Around Points"

        End Function

        Public Function GetTurnAroundPointContact(TurnAroundPointContactID As Integer) As TurnAroundPointContact

            Dim obj As TurnAroundPointContact = Nothing
            For Each parent As TurnAroundPoint In Me
                obj = parent.TurnAroundPointContactList.GetItem(TurnAroundPointContactID)
                If obj IsNot Nothing Then
                    Return obj
                End If
            Next
            Return Nothing

        End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewTurnAroundPointList() As TurnAroundPointList

      Return New TurnAroundPointList()

    End Function

    Public Shared Sub BeginGetTurnAroundPointList(CallBack As EventHandler(Of DataPortalResult(Of TurnAroundPointList)))

      Dim dp As New DataPortal(Of TurnAroundPointList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetTurnAroundPointList() As TurnAroundPointList

      Return DataPortal.Fetch(Of TurnAroundPointList)(New Criteria())

    End Function


        Private Sub Fetch(sdr As SafeDataReader)

            Me.RaiseListChangedEvents = False
            While sdr.Read
                Me.Add(TurnAroundPoint.GetTurnAroundPoint(sdr))
            End While
            Me.RaiseListChangedEvents = True

            Dim parent As TurnAroundPoint = Nothing
            If sdr.NextResult() Then
                While sdr.Read
                    If parent Is Nothing OrElse parent.TurnAroundPointID <> sdr.GetInt32(0) Then
                        parent = Me.GetItem(sdr.GetInt32(1))
                    End If
                    parent.TurnAroundPointContactList.RaiseListChangedEvents = False
                    parent.TurnAroundPointContactList.Add(TurnAroundPointContact.GetTurnAroundPointContact(sdr))
                    parent.TurnAroundPointContactList.RaiseListChangedEvents = True
                End While
            End If

            For Each child As TurnAroundPoint In Me
                child.CheckAllRules()
                For Each Contact In child.TurnAroundPointContactList
                    Contact.CheckAllRules()
                Next
            Next


        End Sub



        Protected Overrides Sub DataPortal_Fetch(criteria As Object)

            Dim crit As Criteria = criteria
            Using cn As New SqlConnection(Singular.Settings.ConnectionString)
                cn.Open()
                Try
                    Using cm As SqlCommand = cn.CreateCommand
                        cm.CommandType = CommandType.StoredProcedure
                        cm.CommandText = "GetProcsWeb.getTurnAroundPointList"
                        Using sdr As New SafeDataReader(cm.ExecuteReader)
                            Fetch(sdr)
                        End Using
                    End Using
                Finally
                    cn.Close()
                End Try
            End Using

        End Sub

        Friend Sub Update()

            Me.RaiseListChangedEvents = False
            Try
                ' Loop through each deleted child object and call its Update() method
                For Each Child As TurnAroundPoint In DeletedList
                    Child.DeleteSelf()
                Next

                ' Then clear the list of deleted objects because they are truly gone now.
                DeletedList.Clear()

                ' Loop through each non-deleted child object and call its Update() method
                For Each Child As TurnAroundPoint In Me
                    If Child.IsNew Then
                        Child.Insert()
                    Else
                        Child.Update()
                    End If
                Next
            Finally
                Me.RaiseListChangedEvents = True
            End Try

        End Sub

        Protected Overrides Sub DataPortal_Update()

            UpdateTransactional(AddressOf Update)

        End Sub

#End If

#End Region

#End Region

    End Class

End Namespace