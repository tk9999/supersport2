﻿' Generated 28 Mar 2015 12:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.SatOps

  <Serializable()> _
  Public Class EquipmentTypeAllowedScheduleType
    Inherits SingularBusinessBase(Of EquipmentTypeAllowedScheduleType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EquipmentTypeAllowedScheduleTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentTypeAllowedScheduleTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property EquipmentTypeAllowedScheduleTypeID() As Integer
      Get
        Return GetProperty(EquipmentTypeAllowedScheduleTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentTypeID, "Equipment Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment Type value
    ''' </summary>
    <Display(Name:="Equipment Type", Description:=""),
    Required(ErrorMessage:="Equipment Type required"),
    DropDownWeb(GetType(ROEquipmentTypeList))>
    Public Property EquipmentTypeID() As Integer?
      Get
        Return GetProperty(EquipmentTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentScheduleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentScheduleTypeID, "Booking Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment Schedule Type value
    ''' </summary>
    <Display(Name:="Booking Type", Description:=""),
    Required(ErrorMessage:="Booking Type required"),
    DropDownWeb(GetType(ROEquipmentScheduleTypeList))>
    Public Property EquipmentScheduleTypeID() As Integer?
      Get
        Return GetProperty(EquipmentScheduleTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentScheduleTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentTypeAllowedScheduleTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Equipment Type Allowed Schedule Type")
        Else
          Return String.Format("Blank {0}", "Equipment Type Allowed Schedule Type")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipmentTypeAllowedScheduleType() method.

    End Sub

    Public Shared Function NewEquipmentTypeAllowedScheduleType() As EquipmentTypeAllowedScheduleType

      Return DataPortal.CreateChild(Of EquipmentTypeAllowedScheduleType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEquipmentTypeAllowedScheduleType(dr As SafeDataReader) As EquipmentTypeAllowedScheduleType

      Dim e As New EquipmentTypeAllowedScheduleType()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EquipmentTypeAllowedScheduleTypeIDProperty, .GetInt32(0))
          LoadProperty(EquipmentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EquipmentScheduleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEquipmentTypeAllowedScheduleType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEquipmentTypeAllowedScheduleType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramEquipmentTypeAllowedScheduleTypeID As SqlParameter = .Parameters.Add("@EquipmentTypeAllowedScheduleTypeID", SqlDbType.Int)
          paramEquipmentTypeAllowedScheduleTypeID.Value = GetProperty(EquipmentTypeAllowedScheduleTypeIDProperty)
          If Me.IsNew Then
            paramEquipmentTypeAllowedScheduleTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EquipmentTypeID", GetProperty(EquipmentTypeIDProperty))
          .Parameters.AddWithValue("@EquipmentScheduleTypeID", GetProperty(EquipmentScheduleTypeIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EquipmentTypeAllowedScheduleTypeIDProperty, paramEquipmentTypeAllowedScheduleTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEquipmentTypeAllowedScheduleType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EquipmentTypeAllowedScheduleTypeID", GetProperty(EquipmentTypeAllowedScheduleTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace