﻿' Generated 15 Mar 2016 10:47 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.SatOps
Imports OBLib.Security
Imports OBLib.Maintenance.MiscList

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Csla.Core

Namespace Maintenance.SatOps


    <Serializable()> _
    Public Class TurnAroundPointContactList
        Inherits OBBusinessListBase(Of TurnAroundPointContactList, TurnAroundPointContact)



#Region " Business Methods "

        Public Function GetItem(TurnAroundPointContactID As Integer) As TurnAroundPointContact

            For Each child As TurnAroundPointContact In Me
                If child.TurnAroundPointContactID = TurnAroundPointContactID Then
                    Return child
                End If
            Next
            Return Nothing

        End Function

        Public Overrides Function ToString() As String

            Return "Turn Around Point Contacts"

        End Function

#End Region

#Region " Data Access "

        <Serializable()> _
        Public Class Criteria
            Inherits CriteriaBase(Of Criteria)

            Public Property ParentID As Integer? = Nothing
            Public Property ParentTable As String = ""
            Public Property TurnAroundPointContactID As Integer? = Nothing
            Public Property TurnAroundPointID As Integer? = Nothing

            Public Sub New(ParentID As Integer?, ParentTable As String,
                           SystemID As Integer?, TurnAroundPointID As Integer?)
                Me.ParentID = ParentID
                Me.ParentTable = ParentTable
                Me.TurnAroundPointContactID = TurnAroundPointContactID
                Me.TurnAroundPointID = TurnAroundPointID
            End Sub
        End Class


#Region "Common"


        Public Shared Function NewTurnAroundPointContactList() As TurnAroundPointContactList

            Return New TurnAroundPointContactList()

        End Function

        Public Sub New()


        End Sub

#End Region


#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else
#End If
#End Region

#Region " .Net Data Access "


        Friend Sub Update()

            Me.RaiseListChangedEvents = False
            Try
                ' Loop through each deleted child object and call its Update() method
                For Each Child As TurnAroundPointContact In DeletedList
                    Child.DeleteSelf()
                Next

                ' Then clear the list of deleted objects because they are truly gone now.
                DeletedList.Clear()

                ' Loop through each non-deleted child object and call its Update() method
                For Each Child As TurnAroundPointContact In Me
                    If Child.IsNew Then
                        Child.Insert()
                    Else
                        Child.Update()
                    End If
                Next
            Finally
                Me.RaiseListChangedEvents = True
            End Try

        End Sub



#End Region
#End Region

    End Class
End Namespace
