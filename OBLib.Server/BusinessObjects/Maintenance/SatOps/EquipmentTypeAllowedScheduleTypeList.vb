﻿' Generated 28 Mar 2015 12:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.SatOps

  <Serializable()> _
  Public Class EquipmentTypeAllowedScheduleTypeList
    Inherits SingularBusinessListBase(Of EquipmentTypeAllowedScheduleTypeList, EquipmentTypeAllowedScheduleType)

#Region " Business Methods "

    Public Function GetItem(EquipmentTypeAllowedScheduleTypeID As Integer) As EquipmentTypeAllowedScheduleType

      For Each child As EquipmentTypeAllowedScheduleType In Me
        If child.EquipmentTypeAllowedScheduleTypeID = EquipmentTypeAllowedScheduleTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Equipment Type Allowed Schedule Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public UseSearch As Boolean = False
      Public EquipmentTypeID As Object = Nothing
      Public EquipmentScheduleTypeID As Object = Nothing

      Public Sub New(EquipmentTypeID As Object, EquipmentScheduleTypeID As Object)

        UseSearch = True
        Me.EquipmentTypeID = EquipmentTypeID
        Me.EquipmentScheduleTypeID = EquipmentScheduleTypeID

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewEquipmentTypeAllowedScheduleTypeList() As EquipmentTypeAllowedScheduleTypeList

      Return New EquipmentTypeAllowedScheduleTypeList()

    End Function

    Public Shared Sub BeginGetEquipmentTypeAllowedScheduleTypeList(CallBack As EventHandler(Of DataPortalResult(Of EquipmentTypeAllowedScheduleTypeList)))

      Dim dp As New DataPortal(Of EquipmentTypeAllowedScheduleTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetEquipmentTypeAllowedScheduleTypeList() As EquipmentTypeAllowedScheduleTypeList

      Return DataPortal.Fetch(Of EquipmentTypeAllowedScheduleTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(EquipmentTypeAllowedScheduleType.GetEquipmentTypeAllowedScheduleType(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getEquipmentTypeAllowedScheduleTypeList"

            If crit.UseSearch Then
              cm.Parameters.AddWithValue("@EquipmentTypeID", Singular.Misc.NothingDBNull(crit.EquipmentTypeID))
              cm.Parameters.AddWithValue("@EquipmentScheduleTypeID", Singular.Misc.NothingDBNull(crit.EquipmentScheduleTypeID))
            End If

            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As EquipmentTypeAllowedScheduleType In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As EquipmentTypeAllowedScheduleType In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace