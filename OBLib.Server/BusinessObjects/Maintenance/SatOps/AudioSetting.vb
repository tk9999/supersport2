﻿' Generated 28 Mar 2015 12:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.SatOps

  <Serializable()> _
  Public Class AudioSetting
    Inherits SingularBusinessBase(Of AudioSetting)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AudioSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AudioSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AudioSettingID() As Integer
      Get
        Return GetProperty(AudioSettingIDProperty)
      End Get
    End Property

    Public Shared AudioSettingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AudioSetting, "Setting Name", "")
    ''' <summary>
    ''' Gets and sets the Audio Setting value
    ''' </summary>
    <Display(Name:="Setting Name", Description:=""),
    StringLength(50, ErrorMessage:="Audio Setting cannot be more than 50 characters"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Setting Name is required")>
    Public Property AudioSetting() As String
      Get
        Return GetProperty(AudioSettingProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AudioSettingProperty, Value)
      End Set
    End Property

    Public Shared InIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InInd, "In", False)
    ''' <summary>
    ''' Gets and sets the In value
    ''' </summary>
    <Display(Name:="In", Description:=""),
    Required(ErrorMessage:="In required")>
    Public Property InInd() As Boolean
      Get
        Return GetProperty(InIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(InIndProperty, Value)
      End Set
    End Property

    Public Shared OldIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.OldInd, "Old", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Old value
    ''' </summary>
    <Display(Name:="Old", Description:="")>
    Public Property OldInd() As Boolean?
      Get
        Return GetProperty(OldIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(OldIndProperty, Value)
      End Set
    End Property

    Public Shared OldDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OldDate, "Old Date")
    ''' <summary>
    ''' Gets and sets the Old Date value
    ''' </summary>
    <Display(Name:="Old Date", Description:="")>
    Public Property OldDate As DateTime?
      Get
        Return GetProperty(OldDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OldDateProperty, Value)
      End Set
    End Property

    Public Shared OldReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OldReason, "Old Reason", "")
    ''' <summary>
    ''' Gets and sets the Old Reason value
    ''' </summary>
    <Display(Name:="Old Reason", Description:=""),
    StringLength(100, ErrorMessage:="Old Reason cannot be more than 100 characters")>
    Public Property OldReason() As String
      Get
        Return GetProperty(OldReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OldReasonProperty, Value)
      End Set
    End Property

    Public Shared OldByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OldByUserID, "Old By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Old By User value
    ''' </summary>
    <Display(Name:="Old By User", Description:="")>
    Public Property OldByUserID() As Integer?
      Get
        Return GetProperty(OldByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OldByUserIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AudioSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AudioSetting.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Audio Setting")
        Else
          Return String.Format("Blank {0}", "Audio Setting")
        End If
      Else
        Return Me.AudioSetting
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAudioSetting() method.

    End Sub

    Public Shared Function NewAudioSetting() As AudioSetting

      Return DataPortal.CreateChild(Of AudioSetting)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAudioSetting(dr As SafeDataReader) As AudioSetting

      Dim a As New AudioSetting()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AudioSettingIDProperty, .GetInt32(0))
          LoadProperty(AudioSettingProperty, .GetString(1))
          LoadProperty(InIndProperty, .GetBoolean(2))
          Dim tmpOldInd = .GetValue(3)
          If IsDBNull(tmpOldInd) Then
            LoadProperty(OldIndProperty, Nothing)
          Else
            LoadProperty(OldIndProperty, tmpOldInd)
          End If
          LoadProperty(OldDateProperty, .GetValue(4))
          LoadProperty(OldReasonProperty, .GetString(5))
          LoadProperty(OldByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAudioSetting"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAudioSetting"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAudioSettingID As SqlParameter = .Parameters.Add("@AudioSettingID", SqlDbType.Int)
          paramAudioSettingID.Value = GetProperty(AudioSettingIDProperty)
          If Me.IsNew Then
            paramAudioSettingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AudioSetting", GetProperty(AudioSettingProperty))
          .Parameters.AddWithValue("@InInd", GetProperty(InIndProperty))
          .Parameters.AddWithValue("@OldInd", Singular.Misc.NothingDBNull(GetProperty(OldIndProperty)))
          .Parameters.AddWithValue("@OldDate", (New SmartDate(GetProperty(OldDateProperty))).DBValue)
          .Parameters.AddWithValue("@OldReason", GetProperty(OldReasonProperty))
          .Parameters.AddWithValue("@OldByUserID", Singular.Misc.NothingDBNull(GetProperty(OldByUserIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AudioSettingIDProperty, paramAudioSettingID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAudioSetting"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AudioSettingID", GetProperty(AudioSettingIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace