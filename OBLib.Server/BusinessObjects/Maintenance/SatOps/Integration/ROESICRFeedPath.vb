﻿' Generated 22 Feb 2016 14:45 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeedPath
    Inherits OBReadOnlyBase(Of ROESICRFeedPath)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedPathIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedPathID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeedPathID() As Integer
      Get
        Return GetProperty(FeedPathIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedID, "Feed")
    ''' <summary>
    ''' Gets the Feed value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property FeedID() As Integer
      Get
        Return GetProperty(FeedIDProperty)
      End Get
    End Property

    Public Shared PathCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PathComments, "Path Comments")
    ''' <summary>
    ''' Gets the Path Comments value
    ''' </summary>
    <Display(Name:="Path Comments", Description:="")>
  Public ReadOnly Property PathComments() As String
      Get
        Return GetProperty(PathCommentsProperty)
      End Get
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "Room Schedule")
    ''' <summary>
    ''' Gets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
  Public ReadOnly Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared RoomScheduleTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomScheduleTitle, "Room Schedule Title")
    ''' <summary>
    ''' Gets the Room Schedule Title value
    ''' </summary>
    <Display(Name:="Room Schedule Title", Description:="")>
  Public ReadOnly Property RoomScheduleTitle() As String
      Get
        Return GetProperty(RoomScheduleTitleProperty)
      End Get
    End Property

    Public Shared SynergyGenRefNoProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.SynergyGenRefNo, "Synergy Gen Ref No")
    ''' <summary>
    ''' Gets the Synergy Gen Ref No value
    ''' </summary>
    <Display(Name:="Synergy Gen Ref No", Description:="")>
  Public ReadOnly Property SynergyGenRefNo() As Int64
      Get
        Return GetProperty(SynergyGenRefNoProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedPathIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.PathComments

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROESICRFeedPath(dr As SafeDataReader) As ROESICRFeedPath

      Dim r As New ROESICRFeedPath()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedPathIDProperty, .GetInt32(0))
        LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(PathCommentsProperty, .GetString(2))
        LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(RoomProperty, .GetString(4))
        LoadProperty(RoomScheduleTitleProperty, .GetString(5))
        LoadProperty(SynergyGenRefNoProperty, .GetInt64(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace