﻿' Generated 22 Feb 2016 14:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeedProduction
    Inherits OBReadOnlyBase(Of ROESICRFeedProduction)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedProductionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeedProductionID() As Integer
      Get
        Return GetProperty(FeedProductionIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedID, "Feed")
    ''' <summary>
    ''' Gets the Feed value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property FeedID() As Integer
      Get
        Return GetProperty(FeedIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
  Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared SynergyGenRefNoProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.SynergyGenRefNo, "Synergy Gen Ref No")
    ''' <summary>
    ''' Gets the Synergy Gen Ref No value
    ''' </summary>
    <Display(Name:="Synergy Gen Ref No", Description:="")>
  Public ReadOnly Property SynergyGenRefNo() As Int64
      Get
        Return GetProperty(SynergyGenRefNoProperty)
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
  Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Production Venue")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="")>
  Public ReadOnly Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
    End Property

    Public Shared KickOffTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.KickOffTime, "Kick Off Time")
    ''' <summary>
    ''' Gets the Kick Off Time value
    ''' </summary>
    <Display(Name:="Kick Off Time", Description:="")>
    Public ReadOnly Property KickOffTime As DateTime?
      Get
        Return GetProperty(KickOffTimeProperty)
      End Get
    End Property

    Public Shared IsSamrandFeedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsSamrandFeed, "Samrand Feed?", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Samrand Feed?")>
    Public ReadOnly Property IsSamrandFeed() As Boolean
      Get
        Return GetProperty(IsSamrandFeedProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROESICRFeedProductionAudioListProperty As PropertyInfo(Of ROESICRFeedProductionAudioList) = RegisterProperty(Of ROESICRFeedProductionAudioList)(Function(c) c.ROESICRFeedProductionAudioList, "ROESICR Feed Production Audio List")

    Public ReadOnly Property ROESICRFeedProductionAudioList() As ROESICRFeedProductionAudioList
      Get
        If GetProperty(ROESICRFeedProductionAudioListProperty) Is Nothing Then
          LoadProperty(ROESICRFeedProductionAudioListProperty, SatOps.Integration.ROESICRFeedProductionAudioList.NewROESICRFeedProductionAudioList())
        End If
        Return GetProperty(ROESICRFeedProductionAudioListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Title

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROESICRFeedProduction(dr As SafeDataReader) As ROESICRFeedProduction

      Dim r As New ROESICRFeedProduction()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedProductionIDProperty, .GetInt32(0))
        LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SynergyGenRefNoProperty, .GetInt64(3))
        LoadProperty(TitleProperty, .GetString(4))
        LoadProperty(ProductionVenueProperty, .GetString(5))
        LoadProperty(KickOffTimeProperty, .GetValue(6))
        LoadProperty(IsSamrandFeedProperty, .GetBoolean(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace