﻿' Generated 22 Feb 2016 14:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeedProductionList
    Inherits OBReadOnlyListBase(Of ROESICRFeedProductionList, ROESICRFeedProduction)

#Region " Parent "

    <NotUndoable()> Private mParent As ROESICRFeed
#End Region

#Region " Business Methods "

    Public Function GetItem(FeedProductionID As Integer) As ROESICRFeedProduction

      For Each child As ROESICRFeedProduction In Me
        If child.FeedProductionID = FeedProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROESICRFeedProductionAudio(FeedProductionAudioSettingID As Integer) As ROESICRFeedProductionAudio

      Dim obj As ROESICRFeedProductionAudio = Nothing
      For Each parent As ROESICRFeedProduction In Me
        obj = parent.ROESICRFeedProductionAudioList.GetItem(FeedProductionAudioSettingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROESICRFeedProductionList() As ROESICRFeedProductionList

      Return New ROESICRFeedProductionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace