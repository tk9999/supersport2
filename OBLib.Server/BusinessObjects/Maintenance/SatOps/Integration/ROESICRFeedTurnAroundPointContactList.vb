﻿' Generated 08 Jun 2016 22:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeedTurnAroundPointContactList
    Inherits OBReadOnlyListBase(Of ROESICRFeedTurnAroundPointContactList, ROESICRFeedTurnAroundPointContact)

#Region " Business Methods "

    Public Function GetItem(ROESICRFeedTurnAroundPointContactID As Integer) As ROESICRFeedTurnAroundPointContact

      For Each child As ROESICRFeedTurnAroundPointContact In Me
        If child.ROESICRFeedTurnAroundPointContactID = ROESICRFeedTurnAroundPointContactID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Feed Turn Around Point Contacts"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROESICRFeedTurnAroundPointContactList() As ROESICRFeedTurnAroundPointContactList

      Return New ROESICRFeedTurnAroundPointContactList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROESICRFeedTurnAroundPointContactList() As ROESICRFeedTurnAroundPointContactList

      Return DataPortal.Fetch(Of ROESICRFeedTurnAroundPointContactList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ROESICRFeedTurnAroundPointContact.GetROESICRFeedTurnAroundPointContact(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROESICRFeedTurnAroundPointContactList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace