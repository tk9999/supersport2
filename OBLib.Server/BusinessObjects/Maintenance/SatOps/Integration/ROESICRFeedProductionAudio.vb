﻿' Generated 22 Feb 2016 14:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeedProductionAudio
    Inherits OBReadOnlyBase(Of ROESICRFeedProductionAudio)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedProductionAudioSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedProductionAudioSettingID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeedProductionAudioSettingID() As Integer
      Get
        Return GetProperty(FeedProductionAudioSettingIDProperty)
      End Get
    End Property

    Public Shared FeedProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedProductionID, "Feed Production")
    ''' <summary>
    ''' Gets the Feed Production value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property FeedProductionID() As Integer
      Get
        Return GetProperty(FeedProductionIDProperty)
      End Get
    End Property

    Public Shared AudioSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AudioSettingID, "Audio Setting")
    ''' <summary>
    ''' Gets the Audio Setting value
    ''' </summary>
    <Display(Name:="Audio Setting", Description:="")>
  Public ReadOnly Property AudioSettingID() As Integer
      Get
        Return GetProperty(AudioSettingIDProperty)
      End Get
    End Property

    Public Shared AudioSettingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AudioSetting, "Audio Setting")
    ''' <summary>
    ''' Gets the Audio Setting value
    ''' </summary>
    <Display(Name:="Audio Setting", Description:="")>
  Public ReadOnly Property AudioSetting() As String
      Get
        Return GetProperty(AudioSettingProperty)
      End Get
    End Property

    Public Shared ChannelNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelNumber, "Audio Setting")
    ''' <summary>
    ''' Gets the Audio Setting value
    ''' </summary>
    <Display(Name:="Audio Setting", Description:="")>
    Public ReadOnly Property ChannelNumber() As Integer
      Get
        Return GetProperty(ChannelNumberProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedProductionAudioSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AudioSetting

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROESICRFeedProductionAudio(dr As SafeDataReader) As ROESICRFeedProductionAudio

      Dim r As New ROESICRFeedProductionAudio()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedProductionAudioSettingIDProperty, .GetInt32(0))
        LoadProperty(FeedProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(AudioSettingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(AudioSettingProperty, .GetString(3))
        LoadProperty(ChannelNumberProperty, .GetInt32(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace