﻿' Generated 22 Feb 2016 14:45 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeedIngestInstructionList
    Inherits OBReadOnlyListBase(Of ROESICRFeedIngestInstructionList, ROESICRFeedIngestInstruction)

#Region " Parent "

    <NotUndoable()> Private mParent As ROESICRFeed
#End Region

#Region " Business Methods "

    Public Function GetItem(FeedIngestInstructionID As Integer) As ROESICRFeedIngestInstruction

      For Each child As ROESICRFeedIngestInstruction In Me
        If child.FeedIngestInstructionID = FeedIngestInstructionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROESICRFeedIngestInstructionList() As ROESICRFeedIngestInstructionList

      Return New ROESICRFeedIngestInstructionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace