﻿' Generated 22 Feb 2016 14:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeed
    Inherits OBReadOnlyBase(Of ROESICRFeed)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeedID() As Integer
      Get
        Return GetProperty(FeedIDProperty)
      End Get
    End Property

    Public Shared FeedDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FeedDescription, "Feed Description")
    ''' <summary>
    ''' Gets the Feed Description value
    ''' </summary>
    <Display(Name:="Feed Description", Description:="")>
  Public ReadOnly Property FeedDescription() As String
      Get
        Return GetProperty(FeedDescriptionProperty)
      End Get
    End Property

    Public Shared FeedTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedTypeID, "Feed Type")
    ''' <summary>
    ''' Gets the Feed Type value
    ''' </summary>
    <Display(Name:="Feed Type", Description:="")>
  Public ReadOnly Property FeedTypeID() As Integer
      Get
        Return GetProperty(FeedTypeIDProperty)
      End Get
    End Property

    Public Shared FeedTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FeedType, "Feed Type")
    ''' <summary>
    ''' Gets the Feed Type value
    ''' </summary>
    <Display(Name:="Feed Type", Description:="")>
  Public ReadOnly Property FeedType() As String
      Get
        Return GetProperty(FeedTypeProperty)
      End Get
    End Property

    Public Shared EquipmentScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentScheduleID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property EquipmentScheduleID() As Integer
      Get
        Return GetProperty(EquipmentScheduleIDProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROESICRFeedDefaultAudioListProperty As PropertyInfo(Of ROESICRFeedDefaultAudioList) = RegisterProperty(Of ROESICRFeedDefaultAudioList)(Function(c) c.ROESICRFeedDefaultAudioList, "ROESICR Feed Default Audio List")

    Public ReadOnly Property ROESICRFeedDefaultAudioList() As ROESICRFeedDefaultAudioList
      Get
        If GetProperty(ROESICRFeedDefaultAudioListProperty) Is Nothing Then
          LoadProperty(ROESICRFeedDefaultAudioListProperty, SatOps.Integration.ROESICRFeedDefaultAudioList.NewROESICRFeedDefaultAudioList())
        End If
        Return GetProperty(ROESICRFeedDefaultAudioListProperty)
      End Get
    End Property

    Public Shared ROESICRFeedProductionListProperty As PropertyInfo(Of ROESICRFeedProductionList) = RegisterProperty(Of ROESICRFeedProductionList)(Function(c) c.ROESICRFeedProductionList, "ROESICR Feed Production List")

    Public ReadOnly Property ROESICRFeedProductionList() As ROESICRFeedProductionList
      Get
        If GetProperty(ROESICRFeedProductionListProperty) Is Nothing Then
          LoadProperty(ROESICRFeedProductionListProperty, SatOps.Integration.ROESICRFeedProductionList.NewROESICRFeedProductionList())
        End If
        Return GetProperty(ROESICRFeedProductionListProperty)
      End Get
    End Property

    Public Shared ROESICRFeedTurnAroundPointListProperty As PropertyInfo(Of ROESICRFeedTurnAroundPointList) = RegisterProperty(Of ROESICRFeedTurnAroundPointList)(Function(c) c.ROESICRFeedTurnAroundPointList, "ROESICR Feed Turn Around Point List")

    Public ReadOnly Property ROESICRFeedTurnAroundPointList() As ROESICRFeedTurnAroundPointList
      Get
        If GetProperty(ROESICRFeedTurnAroundPointListProperty) Is Nothing Then
          LoadProperty(ROESICRFeedTurnAroundPointListProperty, SatOps.Integration.ROESICRFeedTurnAroundPointList.NewROESICRFeedTurnAroundPointList())
        End If
        Return GetProperty(ROESICRFeedTurnAroundPointListProperty)
      End Get
    End Property

    Public Shared ROESICRFeedPathListProperty As PropertyInfo(Of ROESICRFeedPathList) = RegisterProperty(Of ROESICRFeedPathList)(Function(c) c.ROESICRFeedPathList, "ROESICR Feed Path List")

    Public ReadOnly Property ROESICRFeedPathList() As ROESICRFeedPathList
      Get
        If GetProperty(ROESICRFeedPathListProperty) Is Nothing Then
          LoadProperty(ROESICRFeedPathListProperty, SatOps.Integration.ROESICRFeedPathList.NewROESICRFeedPathList())
        End If
        Return GetProperty(ROESICRFeedPathListProperty)
      End Get
    End Property

    Public Shared ROESICRFeedIngestInstructionListProperty As PropertyInfo(Of ROESICRFeedIngestInstructionList) = RegisterProperty(Of ROESICRFeedIngestInstructionList)(Function(c) c.ROESICRFeedIngestInstructionList, "ROESICR Feed Ingest Instruction List")

    Public ReadOnly Property ROESICRFeedIngestInstructionList() As ROESICRFeedIngestInstructionList
      Get
        If GetProperty(ROESICRFeedIngestInstructionListProperty) Is Nothing Then
          LoadProperty(ROESICRFeedIngestInstructionListProperty, SatOps.Integration.ROESICRFeedIngestInstructionList.NewROESICRFeedIngestInstructionList())
        End If
        Return GetProperty(ROESICRFeedIngestInstructionListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FeedDescription

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROESICRFeed(dr As SafeDataReader) As ROESICRFeed

      Dim r As New ROESICRFeed()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedIDProperty, .GetInt32(0))
        LoadProperty(FeedDescriptionProperty, .GetString(1))
        LoadProperty(FeedTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(FeedTypeProperty, .GetString(3))
        LoadProperty(EquipmentScheduleIDProperty, .GetInt32(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace