﻿' Generated 08 Jun 2016 22:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.SatOps.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeedTurnAroundPointContact
    Inherits OBReadOnlyBase(Of ROESICRFeedTurnAroundPointContact)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ROESICRFeedTurnAroundPointContactIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ROESICRFeedTurnAroundPointContactID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ROESICRFeedTurnAroundPointContactID() As Integer
      Get
        Return GetProperty(ROESICRFeedTurnAroundPointContactIDProperty)
      End Get
    End Property

    Public Shared FeedTurnAroundPointIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedTurnAroundPointID, "Feed Turn Around Point", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed Turn Around Point value
    ''' </summary>
    <Display(Name:="Feed Turn Around Point", Description:="")>
    Public ReadOnly Property FeedTurnAroundPointID() As Integer?
      Get
        Return GetProperty(FeedTurnAroundPointIDProperty)
      End Get
    End Property

    Public Shared TurnAroundPointContactIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TurnAroundPointContactID, "TurnAround Point Contact", Nothing)
    ''' <summary>
    ''' Gets and sets the Turn Around Point Contact value
    ''' </summary>
    <Display(Name:="TurnAround Point Contact", Description:="")>
    Public ReadOnly Property TurnAroundPointContactID() As Integer?
      Get
        Return GetProperty(TurnAroundPointContactIDProperty)
      End Get
    End Property

    Public Shared ContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactName, "Contact Name", "")
    ''' <summary>
    ''' Gets and sets the Contact Name value
    ''' </summary>
    <Display(Name:="Contact Name", Description:="")>
    Public ReadOnly Property ContactName() As String
      Get
        Return GetProperty(ContactNameProperty)
      End Get
    End Property

    Public Shared ContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactNumber, "Contact Number", "")
    ''' <summary>
    ''' Gets and sets the Contact Number value
    ''' </summary>
    <Display(Name:="Contact Number", Description:="")>
    Public ReadOnly Property ContactNumber() As String
      Get
        Return GetProperty(ContactNumberProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ROESICRFeedTurnAroundPointContactIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return ""

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Public Shared Function NewROESICRFeedTurnAroundPointContact() As ROESICRFeedTurnAroundPointContact

      Return DataPortal.CreateChild(Of ROESICRFeedTurnAroundPointContact)()

    End Function

    Friend Shared Function GetROESICRFeedTurnAroundPointContact(dr As SafeDataReader) As ROESICRFeedTurnAroundPointContact

      Dim f As New ROESICRFeedTurnAroundPointContact()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

        With sdr
          LoadProperty(ROESICRFeedTurnAroundPointContactIDProperty, .GetInt32(0))
          LoadProperty(FeedTurnAroundPointIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(TurnAroundPointContactIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ContactNameProperty, .GetString(3))
          LoadProperty(ContactNumberProperty, .GetString(4))
        End With

    End Sub

#End Region

  End Class

End Namespace