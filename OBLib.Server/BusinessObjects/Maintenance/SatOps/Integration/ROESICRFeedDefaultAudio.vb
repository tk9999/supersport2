﻿' Generated 22 Feb 2016 14:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeedDefaultAudio
    Inherits OBReadOnlyBase(Of ROESICRFeedDefaultAudio)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedDefaultAudioSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedDefaultAudioSettingID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeedDefaultAudioSettingID() As Integer
      Get
        Return GetProperty(FeedDefaultAudioSettingIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedID, "Feed")
    ''' <summary>
    ''' Gets the Feed value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property FeedID() As Integer
      Get
        Return GetProperty(FeedIDProperty)
      End Get
    End Property

    Public Shared AudioSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AudioSettingID, "Audio Setting")
    ''' <summary>
    ''' Gets the Audio Setting value
    ''' </summary>
    <Display(Name:="Audio Setting", Description:="")>
  Public ReadOnly Property AudioSettingID() As Integer
      Get
        Return GetProperty(AudioSettingIDProperty)
      End Get
    End Property

    Public Shared AudioSettingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AudioSetting, "Audio Setting")
    ''' <summary>
    ''' Gets the Audio Setting value
    ''' </summary>
    <Display(Name:="Audio Setting", Description:="")>
  Public ReadOnly Property AudioSetting() As String
      Get
        Return GetProperty(AudioSettingProperty)
      End Get
    End Property

    Public Shared ChannelNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelNumber, "Channel Number")
    ''' <summary>
    ''' Gets the Audio Setting value
    ''' </summary>
    <Display(Name:="Channel Number", Description:="")>
    Public ReadOnly Property ChannelNumber() As Integer
      Get
        Return GetProperty(ChannelNumberProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedDefaultAudioSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AudioSetting

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROESICRFeedDefaultAudio(dr As SafeDataReader) As ROESICRFeedDefaultAudio

      Dim r As New ROESICRFeedDefaultAudio()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedDefaultAudioSettingIDProperty, .GetInt32(0))
        LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(AudioSettingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(AudioSettingProperty, .GetString(3))
        LoadProperty(ChannelNumberProperty, .GetInt32(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace