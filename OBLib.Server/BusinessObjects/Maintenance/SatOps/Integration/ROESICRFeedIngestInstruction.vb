﻿' Generated 22 Feb 2016 14:45 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeedIngestInstruction
    Inherits OBReadOnlyBase(Of ROESICRFeedIngestInstruction)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedIngestInstructionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedIngestInstructionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeedIngestInstructionID() As Integer
      Get
        Return GetProperty(FeedIngestInstructionIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedID, "Feed")
    ''' <summary>
    ''' Gets the Feed value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property FeedID() As Integer
      Get
        Return GetProperty(FeedIDProperty)
      End Get
    End Property

    Public Shared IngestInstructionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IngestInstruction, "Ingest Instruction")
    ''' <summary>
    ''' Gets the Ingest Instruction value
    ''' </summary>
    <Display(Name:="Ingest Instruction", Description:="")>
  Public ReadOnly Property IngestInstruction() As String
      Get
        Return GetProperty(IngestInstructionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedIngestInstructionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.IngestInstruction

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROESICRFeedIngestInstruction(dr As SafeDataReader) As ROESICRFeedIngestInstruction

      Dim r As New ROESICRFeedIngestInstruction()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedIngestInstructionIDProperty, .GetInt32(0))
        LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(IngestInstructionProperty, .GetString(2))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace