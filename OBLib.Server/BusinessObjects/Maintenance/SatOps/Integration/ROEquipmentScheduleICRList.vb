﻿' Generated 22 Feb 2016 14:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROEquipmentScheduleICRList
    Inherits OBReadOnlyListBase(Of ROEquipmentScheduleICRList, ROEquipmentScheduleICR)

#Region " Business Methods "

    Public Function GetItem(EquipmentScheduleID As Integer) As ROEquipmentScheduleICR

      For Each child As ROEquipmentScheduleICR In Me
        If child.EquipmentScheduleID = EquipmentScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROESICRFeed(FeedID As Integer) As ROESICRFeed

      Dim obj As ROESICRFeed = Nothing
      For Each parent As ROEquipmentScheduleICR In Me
        obj = parent.ROESICRFeedList.GetItem(FeedID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROESICRFeedProduction(FeedProductionID As Integer) As ROESICRFeedProduction

      Dim obj As ROESICRFeedProduction = Nothing
      For Each parent As ROEquipmentScheduleICR In Me
        For Each feed As ROESICRFeed In parent.ROESICRFeedList
          obj = feed.ROESICRFeedProductionList.GetItem(FeedProductionID)
          If obj IsNot Nothing Then
            Return obj
          End If
        Next
      Next
      Return Nothing

    End Function

    Public Function GetROESICRFeedTAP(FeedTurnAroundPointID As Integer) As ROESICRFeedTurnAroundPoint

      Dim obj As ROESICRFeedTurnAroundPoint = Nothing
      For Each parent As ROEquipmentScheduleICR In Me
        For Each feed As ROESICRFeed In parent.ROESICRFeedList
          obj = feed.ROESICRFeedTurnAroundPointList.GetItem(FeedTurnAroundPointID)
          If obj IsNot Nothing Then
            Return obj
          End If
        Next
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property StartDate As Date?
      Public Property EndDate As Date?
      Public Property GenRef As Long?
      Public Property FeedID As Integer?
      Public Property FeedIDs As List(Of Integer)

      Public Sub New(StartDate As Date?, EndDate As Date?, GenRef As Long?, FeedID As Integer?)
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.GenRef = GenRef
        Me.FeedID = FeedID
      End Sub

      Public Sub New(FeedIDs As List(Of Integer))
        Me.FeedIDs = FeedIDs
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROEquipmentScheduleICRList() As ROEquipmentScheduleICRList

      Return New ROEquipmentScheduleICRList()

    End Function

    Public Shared Sub BeginGetROEquipmentScheduleICRList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentScheduleICRList)))

      Dim dp As New DataPortal(Of ROEquipmentScheduleICRList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROEquipmentScheduleICRList(CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentScheduleICRList)))

      Dim dp As New DataPortal(Of ROEquipmentScheduleICRList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROEquipmentScheduleICRList(StartDate As Date?, EndDate As Date?, GenRef As Long?, FeedID As Integer?) As ROEquipmentScheduleICRList

      Return DataPortal.Fetch(Of ROEquipmentScheduleICRList)(New Criteria(StartDate, EndDate, GenRef, FeedID))

    End Function

    Public Shared Function GetROEquipmentScheduleICRList(FeedIDs As List(Of Integer)) As ROEquipmentScheduleICRList

      Return DataPortal.Fetch(Of ROEquipmentScheduleICRList)(New Criteria(FeedIDs))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEquipmentScheduleICR.GetROEquipmentScheduleICR(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROEquipmentScheduleICR = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.EquipmentScheduleID <> sdr.GetInt32(4) Then
            parent = Me.GetItem(sdr.GetInt32(4))
          End If
          parent.ROESICRFeedList.RaiseListChangedEvents = False
          parent.ROESICRFeedList.Add(ROESICRFeed.GetROESICRFeed(sdr))
          parent.ROESICRFeedList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As ROESICRFeed = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.FeedID <> sdr.GetInt32(1) Then
            parentChild = Me.GetROESICRFeed(sdr.GetInt32(1))
          End If
          parentChild.ROESICRFeedDefaultAudioList.RaiseListChangedEvents = False
          parentChild.ROESICRFeedDefaultAudioList.Add(ROESICRFeedDefaultAudio.GetROESICRFeedDefaultAudio(sdr))
          parentChild.ROESICRFeedDefaultAudioList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.FeedID <> sdr.GetInt32(1) Then
            parentChild = Me.GetROESICRFeed(sdr.GetInt32(1))
          End If
          parentChild.ROESICRFeedProductionList.RaiseListChangedEvents = False
          parentChild.ROESICRFeedProductionList.Add(ROESICRFeedProduction.GetROESICRFeedProduction(sdr))
          parentChild.ROESICRFeedProductionList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentProduction As ROESICRFeedProduction = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentProduction Is Nothing OrElse parentProduction.FeedProductionID <> sdr.GetInt32(1) Then
            parentProduction = Me.GetROESICRFeedProduction(sdr.GetInt32(1))
          End If
          parentProduction.ROESICRFeedProductionAudioList.RaiseListChangedEvents = False
          parentProduction.ROESICRFeedProductionAudioList.Add(ROESICRFeedProductionAudio.GetROESICRFeedProductionAudio(sdr))
          parentProduction.ROESICRFeedProductionAudioList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.FeedID <> sdr.GetInt32(1) Then
            parentChild = Me.GetROESICRFeed(sdr.GetInt32(1))
          End If
          parentChild.ROESICRFeedTurnAroundPointList.RaiseListChangedEvents = False
          parentChild.ROESICRFeedTurnAroundPointList.Add(ROESICRFeedTurnAroundPoint.GetROESICRFeedTurnAroundPoint(sdr))
          parentChild.ROESICRFeedTurnAroundPointList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentTAP As ROESICRFeedTurnAroundPoint = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentTAP Is Nothing OrElse parentTAP.FeedTurnAroundPointID <> sdr.GetInt32(1) Then
            parentTAP = Me.GetROESICRFeedTAP(sdr.GetInt32(1))
          End If
          parentTAP.ROESICRFeedTurnAroundPointContactList.RaiseListChangedEvents = False
          parentTAP.ROESICRFeedTurnAroundPointContactList.Add(ROESICRFeedTurnAroundPointContact.GetROESICRFeedTurnAroundPointContact(sdr))
          parentTAP.ROESICRFeedTurnAroundPointContactList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.FeedID <> sdr.GetInt32(1) Then
            parentChild = Me.GetROESICRFeed(sdr.GetInt32(1))
          End If
          parentChild.ROESICRFeedPathList.RaiseListChangedEvents = False
          parentChild.ROESICRFeedPathList.Add(ROESICRFeedPath.GetROESICRFeedPath(sdr))
          parentChild.ROESICRFeedPathList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.FeedID <> sdr.GetInt32(1) Then
            parentChild = Me.GetROESICRFeed(sdr.GetInt32(1))
          End If
          parentChild.ROESICRFeedIngestInstructionList.RaiseListChangedEvents = False
          parentChild.ROESICRFeedIngestInstructionList.Add(ROESICRFeedIngestInstruction.GetROESICRFeedIngestInstruction(sdr))
          parentChild.ROESICRFeedIngestInstructionList.RaiseListChangedEvents = True
        End While
      End If


    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getROEquipmentScheduleListICRDashboardIntegration]"
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@GenRef", Singular.Misc.NothingDBNull(crit.GenRef))
            cm.Parameters.AddWithValue("@FeedID", Singular.Misc.NothingDBNull(crit.FeedID))
            cm.Parameters.AddWithValue("@FeedIDs", Singular.Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.FeedIDs)))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace