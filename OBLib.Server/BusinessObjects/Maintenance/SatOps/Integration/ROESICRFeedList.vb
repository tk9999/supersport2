﻿' Generated 22 Feb 2016 14:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeedList
    Inherits OBReadOnlyListBase(Of ROESICRFeedList, ROESICRFeed)

#Region " Parent "

    <NotUndoable()> Private mParent As ROEquipmentScheduleICR
#End Region

#Region " Business Methods "

    Public Function GetItem(FeedID As Integer) As ROESICRFeed

      For Each child As ROESICRFeed In Me
        If child.FeedID = FeedID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROESICRFeedDefaultAudio(FeedDefaultAudioSettingID As Integer) As ROESICRFeedDefaultAudio

      Dim obj As ROESICRFeedDefaultAudio = Nothing
      For Each parent As ROESICRFeed In Me
        obj = parent.ROESICRFeedDefaultAudioList.GetItem(FeedDefaultAudioSettingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROESICRFeedProduction(FeedProductionID As Integer) As ROESICRFeedProduction

      Dim obj As ROESICRFeedProduction = Nothing
      For Each parent As ROESICRFeed In Me
        obj = parent.ROESICRFeedProductionList.GetItem(FeedProductionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROESICRFeedTurnAroundPoint(FeedTurnAroundPointID As Integer) As ROESICRFeedTurnAroundPoint

      Dim obj As ROESICRFeedTurnAroundPoint = Nothing
      For Each parent As ROESICRFeed In Me
        obj = parent.ROESICRFeedTurnAroundPointList.GetItem(FeedTurnAroundPointID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROESICRFeedPath(FeedPathID As Integer) As ROESICRFeedPath

      Dim obj As ROESICRFeedPath = Nothing
      For Each parent As ROESICRFeed In Me
        obj = parent.ROESICRFeedPathList.GetItem(FeedPathID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROESICRFeedIngestInstruction(FeedIngestInstructionID As Integer) As ROESICRFeedIngestInstruction

      Dim obj As ROESICRFeedIngestInstruction = Nothing
      For Each parent As ROESICRFeed In Me
        obj = parent.ROESICRFeedIngestInstructionList.GetItem(FeedIngestInstructionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROESICRFeedList() As ROESICRFeedList

      Return New ROESICRFeedList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace