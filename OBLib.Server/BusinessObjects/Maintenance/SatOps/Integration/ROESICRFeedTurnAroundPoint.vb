﻿' Generated 22 Feb 2016 14:45 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeedTurnAroundPoint
    Inherits OBReadOnlyBase(Of ROESICRFeedTurnAroundPoint)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedTurnAroundPointIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedTurnAroundPointID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeedTurnAroundPointID() As Integer
      Get
        Return GetProperty(FeedTurnAroundPointIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedID, "Feed")
    ''' <summary>
    ''' Gets the Feed value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property FeedID() As Integer
      Get
        Return GetProperty(FeedIDProperty)
      End Get
    End Property

    Public Shared TurnAroundPointIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TurnAroundPointID, "Turn Around Point")
    ''' <summary>
    ''' Gets the Turn Around Point value
    ''' </summary>
    <Display(Name:="Turn Around Point", Description:="")>
  Public ReadOnly Property TurnAroundPointID() As Integer
      Get
        Return GetProperty(TurnAroundPointIDProperty)
      End Get
    End Property

    Public Shared TurnAroundPointProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TurnAroundPoint, "Turn Around Point")
    ''' <summary>
    ''' Gets the Turn Around Point value
    ''' </summary>
    <Display(Name:="Turn Around Point", Description:="")>
  Public ReadOnly Property TurnAroundPoint() As String
      Get
        Return GetProperty(TurnAroundPointProperty)
      End Get
    End Property

    Public Shared TurnAroundPointOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TurnAroundPointOrder, "Turn Around Point Order")
    ''' <summary>
    ''' Gets the Turn Around Point Order value
    ''' </summary>
    <Display(Name:="Turn Around Point Order", Description:="")>
  Public ReadOnly Property TurnAroundPointOrder() As Integer
      Get
        Return GetProperty(TurnAroundPointOrderProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedTurnAroundPointIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.TurnAroundPoint

    End Function

#End Region

#Region " Child Lists "

    Public Shared ROESICRFeedTurnAroundPointContactListProperty As PropertyInfo(Of ROESICRFeedTurnAroundPointContactList) = RegisterProperty(Of ROESICRFeedTurnAroundPointContactList)(Function(c) c.ROESICRFeedTurnAroundPointContactList, "ROESICR Feed Turn Around Point List")
    Public ReadOnly Property ROESICRFeedTurnAroundPointContactList() As ROESICRFeedTurnAroundPointContactList
      Get
        If GetProperty(ROESICRFeedTurnAroundPointContactListProperty) Is Nothing Then
          LoadProperty(ROESICRFeedTurnAroundPointContactListProperty, SatOps.Integration.ROESICRFeedTurnAroundPointContactList.NewROESICRFeedTurnAroundPointContactList())
        End If
        Return GetProperty(ROESICRFeedTurnAroundPointContactListProperty)
      End Get
    End Property

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROESICRFeedTurnAroundPoint(dr As SafeDataReader) As ROESICRFeedTurnAroundPoint

      Dim r As New ROESICRFeedTurnAroundPoint()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedTurnAroundPointIDProperty, .GetInt32(0))
        LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(TurnAroundPointIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(TurnAroundPointProperty, .GetString(3))
        LoadProperty(TurnAroundPointOrderProperty, .GetInt32(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace