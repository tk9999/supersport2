﻿' Generated 22 Feb 2016 14:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROEquipmentScheduleICR
    Inherits OBReadOnlyBase(Of ROEquipmentScheduleICR)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EquipmentScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentScheduleID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property EquipmentScheduleID() As Integer
      Get
        Return GetProperty(EquipmentScheduleIDProperty)
      End Get
    End Property

    Public Shared EquipmentScheduleTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentScheduleTitle, "Equipment Schedule Title")
    ''' <summary>
    ''' Gets the Equipment Schedule Title value
    ''' </summary>
    <Display(Name:="Equipment Schedule Title", Description:="")>
  Public ReadOnly Property EquipmentScheduleTitle() As String
      Get
        Return GetProperty(EquipmentScheduleTitleProperty)
      End Get
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentID, "Equipment")
    ''' <summary>
    ''' Gets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:="")>
  Public ReadOnly Property EquipmentID() As Integer
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
    End Property

    Public Shared EquipmentNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentName, "Equipment Name")
    ''' <summary>
    ''' Gets the Equipment Name value
    ''' </summary>
    <Display(Name:="Equipment Name", Description:="")>
  Public ReadOnly Property EquipmentName() As String
      Get
        Return GetProperty(EquipmentNameProperty)
      End Get
    End Property

    Public Shared EquipmentScheduleTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentScheduleTypeID, "Equipment Schedule Type")
    ''' <summary>
    ''' Gets the Equipment Schedule Type value
    ''' </summary>
    <Display(Name:="Equipment Schedule Type", Description:="")>
  Public ReadOnly Property EquipmentScheduleTypeID() As Integer
      Get
        Return GetProperty(EquipmentScheduleTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentScheduleTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentScheduleType, "Equipment Schedule Type")
    ''' <summary>
    ''' Gets the Equipment Schedule Type value
    ''' </summary>
    <Display(Name:="Equipment Schedule Type", Description:="")>
  Public ReadOnly Property EquipmentScheduleType() As String
      Get
        Return GetProperty(EquipmentScheduleTypeProperty)
      End Get
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Start Date Time Buffer")
    ''' <summary>
    ''' Gets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Date Time Buffer", Description:="")>
  Public ReadOnly Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
  Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
  Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "End Date Time Buffer")
    ''' <summary>
    ''' Gets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Date Time Buffer", Description:="")>
  Public ReadOnly Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
    End Property

    Public Shared EquipmentServiceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentServiceID, "Equipment Service")
    ''' <summary>
    ''' Gets the Equipment Service value
    ''' </summary>
    <Display(Name:="Equipment Service", Description:="")>
    Public ReadOnly Property EquipmentServiceID() As Integer?
      Get
        Return GetProperty(EquipmentServiceIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedID, "Feed")
    ''' <summary>
    ''' Gets the Feed value
    ''' </summary>
    <Display(Name:="Feed", Description:="")>
    Public ReadOnly Property FeedID() As Integer?
      Get
        Return GetProperty(FeedIDProperty)
      End Get
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Debtor")
    ''' <summary>
    ''' Gets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="")>
    Public ReadOnly Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
    End Property

    Public Shared DebtorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Debtor, "Debtor")
    ''' <summary>
    ''' Gets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="")>
  Public ReadOnly Property Debtor() As String
      Get
        Return GetProperty(DebtorProperty)
      End Get
    End Property

    Public Shared StatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StatusID, "Status")
    ''' <summary>
    ''' Gets the Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
  Public ReadOnly Property StatusID() As Integer
      Get
        Return GetProperty(StatusIDProperty)
      End Get
    End Property

    Public Shared StatusNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusName, "Status Name")
    ''' <summary>
    ''' Gets the Status Name value
    ''' </summary>
    <Display(Name:="Status Name", Description:="")>
  Public ReadOnly Property StatusName() As String
      Get
        Return GetProperty(StatusNameProperty)
      End Get
    End Property

    Public Shared EquipmentScheduleCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentScheduleComments, "Equipment Schedule Comments")
    ''' <summary>
    ''' Gets the Equipment Schedule Comments value
    ''' </summary>
    <Display(Name:="Equipment Schedule Comments", Description:="")>
    Public ReadOnly Property EquipmentScheduleComments() As String
      Get
        Return GetProperty(EquipmentScheduleCommentsProperty)
      End Get
    End Property

    Public Shared PriorityFeedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PriorityFeed, "Priority Feed?", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Priority Feed?")>
    Public ReadOnly Property PriorityFeed() As Boolean
      Get
        Return GetProperty(PriorityFeedProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROESICRFeedListProperty As PropertyInfo(Of ROESICRFeedList) = RegisterProperty(Of ROESICRFeedList)(Function(c) c.ROESICRFeedList, "ROESICR Feed List")

    Public ReadOnly Property ROESICRFeedList() As ROESICRFeedList
      Get
        If GetProperty(ROESICRFeedListProperty) Is Nothing Then
          LoadProperty(ROESICRFeedListProperty, SatOps.Integration.ROESICRFeedList.NewROESICRFeedList())
        End If
        Return GetProperty(ROESICRFeedListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EquipmentScheduleTitle

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEquipmentScheduleICR(dr As SafeDataReader) As ROEquipmentScheduleICR

      Dim r As New ROEquipmentScheduleICR()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EquipmentScheduleIDProperty, .GetInt32(0))
        LoadProperty(EquipmentScheduleTitleProperty, .GetString(1))
        LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(EquipmentNameProperty, .GetString(3))
        LoadProperty(EquipmentScheduleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(EquipmentScheduleTypeProperty, .GetString(5))
        LoadProperty(StartDateTimeBufferProperty, .GetValue(6))
        LoadProperty(StartDateTimeProperty, .GetValue(7))
        LoadProperty(EndDateTimeProperty, .GetValue(8))
        LoadProperty(EndDateTimeBufferProperty, .GetValue(9))
        LoadProperty(EquipmentServiceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(DebtorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(DebtorProperty, .GetString(13))
        LoadProperty(StatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(StatusNameProperty, .GetString(15))
        LoadProperty(EquipmentScheduleCommentsProperty, .GetString(16))
        LoadProperty(PriorityFeedProperty, .GetBoolean(17))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace