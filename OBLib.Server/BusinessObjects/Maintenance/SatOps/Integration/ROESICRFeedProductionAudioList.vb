﻿' Generated 22 Feb 2016 14:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.Integration

  <Serializable()> _
  Public Class ROESICRFeedProductionAudioList
    Inherits OBReadOnlyListBase(Of ROESICRFeedProductionAudioList, ROESICRFeedProductionAudio)

#Region " Parent "

    <NotUndoable()> Private mParent As ROESICRFeedProduction
#End Region

#Region " Business Methods "

    Public Function GetItem(FeedProductionAudioSettingID As Integer) As ROESICRFeedProductionAudio

      For Each child As ROESICRFeedProductionAudio In Me
        If child.FeedProductionAudioSettingID = FeedProductionAudioSettingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROESICRFeedProductionAudioList() As ROESICRFeedProductionAudioList

      Return New ROESICRFeedProductionAudioList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace