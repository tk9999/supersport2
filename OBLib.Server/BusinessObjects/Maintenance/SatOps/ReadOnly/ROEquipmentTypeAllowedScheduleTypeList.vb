﻿' Generated 28 Mar 2015 12:23 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.SatOps.ReadOnly

  <Serializable()> _
  Public Class ROEquipmentTypeAllowedScheduleTypeList
    Inherits SingularReadOnlyListBase(Of ROEquipmentTypeAllowedScheduleTypeList, ROEquipmentTypeAllowedScheduleType)

#Region " Business Methods "

    Public Function GetItem(EquipmentTypeAllowedScheduleTypeID As Integer) As ROEquipmentTypeAllowedScheduleType

      For Each child As ROEquipmentTypeAllowedScheduleType In Me
        If child.EquipmentTypeAllowedScheduleTypeID = EquipmentTypeAllowedScheduleTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Equipment Type Allowed Schedule Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public UseSearch As Boolean = False
      Public EquipmentTypeID As Object = Nothing
      Public EquipmentScheduleTypeID As Object = Nothing

      Public Sub New(EquipmentTypeID As Object, EquipmentScheduleTypeID As Object)

        UseSearch = True
        Me.EquipmentTypeID = EquipmentTypeID
        Me.EquipmentScheduleTypeID = EquipmentScheduleTypeID

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROEquipmentTypeAllowedScheduleTypeList() As ROEquipmentTypeAllowedScheduleTypeList

      Return New ROEquipmentTypeAllowedScheduleTypeList()

    End Function

    Public Shared Sub BeginGetROEquipmentTypeAllowedScheduleTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentTypeAllowedScheduleTypeList)))

      Dim dp As New DataPortal(Of ROEquipmentTypeAllowedScheduleTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROEquipmentTypeAllowedScheduleTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentTypeAllowedScheduleTypeList)))

      Dim dp As New DataPortal(Of ROEquipmentTypeAllowedScheduleTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROEquipmentTypeAllowedScheduleTypeList() As ROEquipmentTypeAllowedScheduleTypeList

      Return DataPortal.Fetch(Of ROEquipmentTypeAllowedScheduleTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEquipmentTypeAllowedScheduleType.GetROEquipmentTypeAllowedScheduleType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEquipmentTypeAllowedScheduleTypeList"

            If crit.UseSearch Then
              cm.Parameters.AddWithValue("@EquipmentTypeID", Singular.Misc.NothingDBNull(crit.EquipmentTypeID))
              cm.Parameters.AddWithValue("@EquipmentScheduleTypeID", Singular.Misc.NothingDBNull(crit.EquipmentScheduleTypeID))
            End If

            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace