﻿' Generated 28 Mar 2015 12:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.SatOps.ReadOnly

  <Serializable()> _
  Public Class ROAudioSettingList
    Inherits SingularReadOnlyListBase(Of ROAudioSettingList, ROAudioSetting)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(AudioSettingID As Integer) As ROAudioSetting

      For Each child As ROAudioSetting In Me
        If child.AudioSettingID = AudioSettingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Audio Settings"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared AudioSettingProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.AudioSetting, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Audio Setting", Description:="")>
      Public Property AudioSetting() As String
        Get
          Return ReadProperty(AudioSettingProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(AudioSettingProperty, Value)
        End Set
      End Property

      Public Sub New(AudioSetting As String)
        Me.AudioSetting = AudioSetting
      End Sub

      Public Sub New(All As Boolean)
        Me.PageNo = 1
        Me.PageSize = 1000
        Me.SortAsc = True
        Me.SortColumn = "AudioSetting"
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROAudioSettingList() As ROAudioSettingList

      Return New ROAudioSettingList()

    End Function

    Public Shared Sub BeginGetROAudioSettingList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROAudioSettingList)))

      Dim dp As New DataPortal(Of ROAudioSettingList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROAudioSettingList(CallBack As EventHandler(Of DataPortalResult(Of ROAudioSettingList)))

      Dim dp As New DataPortal(Of ROAudioSettingList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROAudioSettingList() As ROAudioSettingList

      Return DataPortal.Fetch(Of ROAudioSettingList)(New Criteria())

    End Function

    Public Shared Function GetROAudioSettingList(All As Boolean) As ROAudioSettingList

      Return DataPortal.Fetch(Of ROAudioSettingList)(New Criteria(All))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROAudioSetting.GetROAudioSetting(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROAudioSettingList"
            cm.Parameters.AddWithValue("@AudioSetting", Singular.Strings.MakeEmptyDBNull(crit.AudioSetting))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace