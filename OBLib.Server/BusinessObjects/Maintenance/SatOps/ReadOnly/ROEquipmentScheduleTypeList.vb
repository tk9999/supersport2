﻿' Generated 28 Mar 2015 13:15 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.SatOps.ReadOnly

  <Serializable()> _
  Public Class ROEquipmentScheduleTypeList
    Inherits SingularReadOnlyListBase(Of ROEquipmentScheduleTypeList, ROEquipmentScheduleType)

#Region " Business Methods "

    Public Function GetItem(EquipmentScheduleTypeID As Integer) As ROEquipmentScheduleType

      For Each child As ROEquipmentScheduleType In Me
        If child.EquipmentScheduleTypeID = EquipmentScheduleTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Equipment Schedule Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROEquipmentScheduleTypeList() As ROEquipmentScheduleTypeList

      Return New ROEquipmentScheduleTypeList()

    End Function

    Public Shared Sub BeginGetROEquipmentScheduleTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentScheduleTypeList)))

      Dim dp As New DataPortal(Of ROEquipmentScheduleTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROEquipmentScheduleTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentScheduleTypeList)))

      Dim dp As New DataPortal(Of ROEquipmentScheduleTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROEquipmentScheduleTypeList() As ROEquipmentScheduleTypeList

      Return DataPortal.Fetch(Of ROEquipmentScheduleTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEquipmentScheduleType.GetROEquipmentScheduleType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEquipmentScheduleTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace