﻿' Generated 08 Jun 2016 22:06 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SatOps.ReadOnly

  <Serializable()> _
  Public Class ROTurnAroundPointContact
    Inherits OBReadOnlyBase(Of ROTurnAroundPointContact)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROTurnAroundPointContactBO.ROTurnAroundPointContactBOToString(self)")

    Public Shared TurnAroundPointContactIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TurnAroundPointContactID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property TurnAroundPointContactID() As Integer
      Get
        Return GetProperty(TurnAroundPointContactIDProperty)
      End Get
    End Property

    Public Shared TurnAroundPointIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TurnAroundPointID, "Turn Around Point", Nothing)
    ''' <summary>
    ''' Gets the Turn Around Point value
    ''' </summary>
    <Display(Name:="Turn Around Point", Description:="")>
  Public ReadOnly Property TurnAroundPointID() As Integer?
      Get
        Return GetProperty(TurnAroundPointIDProperty)
      End Get
    End Property

    Public Shared ContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactName, "Contact Name", "")
    ''' <summary>
    ''' Gets the Contact Name value
    ''' </summary>
    <Display(Name:="Contact Name", Description:="")>
  Public ReadOnly Property ContactName() As String
      Get
        Return GetProperty(ContactNameProperty)
      End Get
    End Property

    Public Shared ContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactNumber, "Contact Number", "")
    ''' <summary>
    ''' Gets the Contact Number value
    ''' </summary>
    <Display(Name:="Contact Number", Description:="")>
  Public ReadOnly Property ContactNumber() As String
      Get
        Return GetProperty(ContactNumberProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TurnAroundPointContactIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ContactName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTurnAroundPointContact(dr As SafeDataReader) As ROTurnAroundPointContact

      Dim r As New ROTurnAroundPointContact()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TurnAroundPointContactIDProperty, .GetInt32(0))
        LoadProperty(TurnAroundPointIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ContactNameProperty, .GetString(2))
        LoadProperty(ContactNumberProperty, .GetString(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End Region

  End Class

End Namespace