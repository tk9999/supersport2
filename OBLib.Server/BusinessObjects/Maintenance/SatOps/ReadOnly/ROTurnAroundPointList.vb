﻿' Generated 28 Mar 2015 12:30 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.SatOps.ReadOnly

  <Serializable()> _
  Public Class ROTurnAroundPointList
    Inherits OBReadOnlyListBase(Of ROTurnAroundPointList, ROTurnAroundPoint)
    'Implements Singular.Paging.IPagedList

    'Private mTotalRecords As Integer = 0
    'Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
    '  Get
    '    Return mTotalRecords
    '  End Get
    'End Property

#Region " Business Methods "

    Public Function GetItem(TurnAroundPointID As Integer) As ROTurnAroundPoint

      For Each child As ROTurnAroundPoint In Me
        If child.TurnAroundPointID = TurnAroundPointID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Turn Around Points"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)
      ' Inherits Paging.PageCriteria(Of Criteria)

       <Singular.DataAnnotations.PrimarySearchField>
      Public Property TurnAroundPoint As String

      'Public Shared SelectedIDsProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.SelectedIDs, "")
      ' ''' <summary>
      ' ''' Gets and sets the Keyword value
      ' ''' </summary>
      '<Display(Name:="SelectedIDs", Description:="")>
      'Public Overridable Property SelectedIDs() As String
      '  Get
      '    Return ReadProperty(SelectedIDsProperty)
      '  End Get
      '  Set(ByVal Value As String)
      '    LoadProperty(SelectedIDsProperty, Value)
      '  End Set
      'End Property

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROTurnAroundPointList() As ROTurnAroundPointList

      Return New ROTurnAroundPointList()

    End Function

    Public Shared Sub BeginGetROTurnAroundPointList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROTurnAroundPointList)))

      Dim dp As New DataPortal(Of ROTurnAroundPointList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROTurnAroundPointList(CallBack As EventHandler(Of DataPortalResult(Of ROTurnAroundPointList)))

      Dim dp As New DataPortal(Of ROTurnAroundPointList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROTurnAroundPointList() As ROTurnAroundPointList

      Return DataPortal.Fetch(Of ROTurnAroundPointList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      'sdr.Read()
      'mTotalRecords = sdr.GetInt32(0)
      ''mTotalPages = sdr.GetInt32(1)
      'sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTurnAroundPoint.GetROTurnAroundPoint(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTurnAroundPointList"
            cm.Parameters.AddWithValue("@TurnAroundPoint", Singular.Strings.MakeEmptyDBNull(crit.TurnAroundPoint))
            'cm.Parameters.AddWithValue("@SelectedIDs", Singular.Strings.MakeEmptyDBNull(crit.SelectedIDs))
            'crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace