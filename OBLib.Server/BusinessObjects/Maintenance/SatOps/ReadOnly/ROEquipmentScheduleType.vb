﻿' Generated 28 Mar 2015 12:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.SatOps.ReadOnly

  <Serializable()> _
  Public Class ROEquipmentScheduleType
    Inherits SingularReadOnlyBase(Of ROEquipmentScheduleType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EquipmentScheduleTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentScheduleTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property EquipmentScheduleTypeID() As Integer
      Get
        Return GetProperty(EquipmentScheduleTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentScheduleTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentScheduleType, "Equipment Schedule Type", "")
    ''' <summary>
    ''' Gets the Equipment Schedule Type value
    ''' </summary>
    <Display(Name:="Equipment Schedule Type", Description:="")>
    Public ReadOnly Property EquipmentScheduleType() As String
      Get
        Return GetProperty(EquipmentScheduleTypeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentScheduleTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EquipmentScheduleType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEquipmentScheduleType(dr As SafeDataReader) As ROEquipmentScheduleType

      Dim r As New ROEquipmentScheduleType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EquipmentScheduleTypeIDProperty, .GetInt32(0))
        LoadProperty(EquipmentScheduleTypeProperty, .GetString(1))
        LoadProperty(CreatedByProperty, .GetInt32(2))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
        LoadProperty(ModifiedByProperty, .GetInt32(4))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace