﻿' Generated 28 Mar 2015 12:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.SatOps.ReadOnly

  <Serializable()> _
  Public Class ROVideoSettingList
    Inherits SingularReadOnlyListBase(Of ROVideoSettingList, ROVideoSetting)

#Region " Business Methods "

    Public Function GetItem(VideoSettingID As Integer) As ROVideoSetting

      For Each child As ROVideoSetting In Me
        If child.VideoSettingID = VideoSettingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Video Settings"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public UseSearch As Boolean = False
      Public VideoSetting As Object = Nothing

      Public Sub New(VideoSetting As Object)

        UseSearch = True
        Me.VideoSetting = VideoSetting

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROVideoSettingList() As ROVideoSettingList

      Return New ROVideoSettingList()

    End Function

    Public Shared Sub BeginGetROVideoSettingList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROVideoSettingList)))

      Dim dp As New DataPortal(Of ROVideoSettingList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROVideoSettingList(CallBack As EventHandler(Of DataPortalResult(Of ROVideoSettingList)))

      Dim dp As New DataPortal(Of ROVideoSettingList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROVideoSettingList() As ROVideoSettingList

      Return DataPortal.Fetch(Of ROVideoSettingList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROVideoSetting.GetROVideoSetting(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROVideoSettingList"

            If crit.UseSearch Then
              cm.Parameters.AddWithValue("@VideoSetting", Singular.Misc.NothingDBNull(crit.VideoSetting))
            End If

            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace