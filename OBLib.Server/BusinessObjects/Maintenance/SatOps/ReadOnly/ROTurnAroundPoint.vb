﻿' Generated 28 Mar 2015 12:30 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.SatOps.ReadOnly

  <Serializable()> _
  Public Class ROTurnAroundPoint
    Inherits OBReadOnlyBase(Of ROTurnAroundPoint)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TurnAroundPointIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TurnAroundPointID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property TurnAroundPointID() As Integer
      Get
        Return GetProperty(TurnAroundPointIDProperty)
      End Get
    End Property

    Public Shared TurnAroundPointProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TurnAroundPoint, "Turn Around Point", "")
    ''' <summary>
    ''' Gets the Turn Around Point value
    ''' </summary>
    <Display(Name:="Turn Around Point", Description:="Name of the turnaround point")>
    Public ReadOnly Property TurnAroundPoint() As String
      Get
        Return GetProperty(TurnAroundPointProperty)
      End Get
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CountryID, "Country", 0)
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="The country in which the turnaround point resides")>
    Public ReadOnly Property CountryID() As Integer
      Get
        Return GetProperty(CountryIDProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CityID, "City", 0)
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property CityID() As Integer
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared LocationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.LocationID, "Location", 0)
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public ReadOnly Property LocationID() As Integer
      Get
        Return GetProperty(LocationIDProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
    Public ReadOnly Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country", "")
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="The country in which the turnaround point resides")>
    Public ReadOnly Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City", "")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location", "")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public ReadOnly Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
    End Property

    Public Shared VehicleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Vehicle, "Vehicle", "")
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
    Public ReadOnly Property Vehicle() As String
      Get
        Return GetProperty(VehicleProperty)
      End Get
    End Property

    Public Shared DisplayProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TurnAroundPointDisplay, "Turn Around Point", "")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Turn Around Point", Description:="")>
    Public ReadOnly Property TurnAroundPointDisplay() As String
      Get
        Return TurnAroundPoint & " (" & IIf(City.Trim.Length > 0, City, "") & IIf(City.Trim.Length > 0, " - ", "") & Country & ")"
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TurnAroundPointIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.TurnAroundPoint

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROTurnAroundPoint(dr As SafeDataReader) As ROTurnAroundPoint

      Dim r As New ROTurnAroundPoint()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TurnAroundPointIDProperty, .GetInt32(0))
        LoadProperty(TurnAroundPointProperty, .GetString(1))
        LoadProperty(CountryIDProperty, .GetInt32(2))
        LoadProperty(CityIDProperty, .GetInt32(3))
        LoadProperty(LocationIDProperty, .GetInt32(4))
        LoadProperty(VehicleIDProperty, .GetInt32(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(CountryProperty, .GetString(10))
        LoadProperty(CityProperty, .GetString(11))
        LoadProperty(LocationProperty, .GetString(12))
        LoadProperty(VehicleProperty, .GetString(13))
        '14 = RowNum
        'LoadProperty(IsSelectedProperty, .GetBoolean(15))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace