﻿' Generated 28 Mar 2015 12:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.SatOps.ReadOnly

  <Serializable()> _
  Public Class ROVideoSetting
    Inherits SingularReadOnlyBase(Of ROVideoSetting)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VideoSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VideoSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property VideoSettingID() As Integer
      Get
        Return GetProperty(VideoSettingIDProperty)
      End Get
    End Property

    Public Shared VideoSettingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VideoSetting, "Video Setting", "")
    ''' <summary>
    ''' Gets the Video Setting value
    ''' </summary>
    <Display(Name:="Video Setting", Description:="")>
    Public ReadOnly Property VideoSetting() As String
      Get
        Return GetProperty(VideoSettingProperty)
      End Get
    End Property

    Public Shared OldIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.OldInd, "Old", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets the Old value
    ''' </summary>
    <Display(Name:="Old", Description:="")>
    Public ReadOnly Property OldInd() As Boolean?
      Get
        Return GetProperty(OldIndProperty)
      End Get
    End Property

    Public Shared OldDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OldDate, "Old Date")
    ''' <summary>
    ''' Gets the Old Date value
    ''' </summary>
    <Display(Name:="Old Date", Description:="")>
    Public ReadOnly Property OldDate As DateTime?
      Get
        Return GetProperty(OldDateProperty)
      End Get
    End Property

    Public Shared OldReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OldReason, "Old Reason", "")
    ''' <summary>
    ''' Gets the Old Reason value
    ''' </summary>
    <Display(Name:="Old Reason", Description:="")>
    Public ReadOnly Property OldReason() As String
      Get
        Return GetProperty(OldReasonProperty)
      End Get
    End Property

    Public Shared OldByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OldByUserID, "Old By User", Nothing)
    ''' <summary>
    ''' Gets the Old By User value
    ''' </summary>
    <Display(Name:="Old By User", Description:="")>
    Public ReadOnly Property OldByUserID() As Integer?
      Get
        Return GetProperty(OldByUserIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VideoSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.VideoSetting

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROVideoSetting(dr As SafeDataReader) As ROVideoSetting

      Dim r As New ROVideoSetting()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(VideoSettingIDProperty, .GetInt32(0))
        LoadProperty(VideoSettingProperty, .GetString(1))
        Dim tmpOldInd = .GetValue(2)
        If IsDBNull(tmpOldInd) Then
          LoadProperty(OldIndProperty, Nothing)
        Else
          LoadProperty(OldIndProperty, tmpOldInd)
        End If
        LoadProperty(OldDateProperty, .GetValue(3))
        LoadProperty(OldReasonProperty, .GetString(4))
        LoadProperty(OldByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace