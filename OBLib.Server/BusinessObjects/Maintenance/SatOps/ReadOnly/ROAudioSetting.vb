﻿' Generated 28 Mar 2015 12:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.SatOps.ReadOnly

  <Serializable()> _
  Public Class ROAudioSetting
    Inherits SingularReadOnlyBase(Of ROAudioSetting)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AudioSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AudioSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AudioSettingID() As Integer
      Get
        Return GetProperty(AudioSettingIDProperty)
      End Get
    End Property

    Public Shared AudioSettingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AudioSetting, "Audio Setting", "")
    ''' <summary>
    ''' Gets the Audio Setting value
    ''' </summary>
    <Display(Name:="Audio Setting", Description:="")>
    Public ReadOnly Property AudioSetting() As String
      Get
        Return GetProperty(AudioSettingProperty)
      End Get
    End Property

    Public Shared InIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InInd, "In", False)
    ''' <summary>
    ''' Gets the In value
    ''' </summary>
    <Display(Name:="In", Description:="")>
    Public ReadOnly Property InInd() As Boolean
      Get
        Return GetProperty(InIndProperty)
      End Get
    End Property

    Public Shared OldIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.OldInd, "Old", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets the Old value
    ''' </summary>
    <Display(Name:="Old", Description:="")>
    Public ReadOnly Property OldInd() As Boolean?
      Get
        Return GetProperty(OldIndProperty)
      End Get
    End Property

    Public Shared OldDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OldDate, "Old Date")
    ''' <summary>
    ''' Gets the Old Date value
    ''' </summary>
    <Display(Name:="Old Date", Description:="")>
    Public ReadOnly Property OldDate As DateTime?
      Get
        Return GetProperty(OldDateProperty)
      End Get
    End Property

    Public Shared OldReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OldReason, "Old Reason", "")
    ''' <summary>
    ''' Gets the Old Reason value
    ''' </summary>
    <Display(Name:="Old Reason", Description:="")>
    Public ReadOnly Property OldReason() As String
      Get
        Return GetProperty(OldReasonProperty)
      End Get
    End Property

    Public Shared OldByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OldByUserID, "Old By User", Nothing)
    ''' <summary>
    ''' Gets the Old By User value
    ''' </summary>
    <Display(Name:="Old By User", Description:="")>
    Public ReadOnly Property OldByUserID() As Integer?
      Get
        Return GetProperty(OldByUserIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AudioSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AudioSetting

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAudioSetting(dr As SafeDataReader) As ROAudioSetting

      Dim r As New ROAudioSetting()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AudioSettingIDProperty, .GetInt32(0))
        LoadProperty(AudioSettingProperty, .GetString(1))
        LoadProperty(InIndProperty, .GetBoolean(2))
        Dim tmpOldInd = .GetValue(3)
        If IsDBNull(tmpOldInd) Then
          LoadProperty(OldIndProperty, Nothing)
        Else
          LoadProperty(OldIndProperty, tmpOldInd)
        End If
        LoadProperty(OldDateProperty, .GetValue(4))
        LoadProperty(OldReasonProperty, .GetString(5))
        LoadProperty(OldByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(CreatedByProperty, .GetInt32(7))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(ModifiedByProperty, .GetInt32(9))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace