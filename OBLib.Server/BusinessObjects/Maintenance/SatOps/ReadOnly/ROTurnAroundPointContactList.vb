﻿' Generated 08 Jun 2016 22:06 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SatOps.ReadOnly

  <Serializable()> _
  Public Class ROTurnAroundPointContactList
    Inherits OBReadOnlyListBase(Of ROTurnAroundPointContactList, ROTurnAroundPointContact)

#Region " Business Methods "

    Public Function GetItem(TurnAroundPointContactID As Integer) As ROTurnAroundPointContact

      For Each child As ROTurnAroundPointContact In Me
        If child.TurnAroundPointContactID = TurnAroundPointContactID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Turn Around Point Contacts"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TurnAroundPointID As Integer? = Nothing

      <PrimarySearchField>
      Public Property ContactName As String = ""

      Public Sub New(TurnAroundPointID As Integer?)
        Me.TurnAroundPointID = TurnAroundPointID
      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROTurnAroundPointContactList() As ROTurnAroundPointContactList

      Return New ROTurnAroundPointContactList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTurnAroundPointContactList() As ROTurnAroundPointContactList

      Return DataPortal.Fetch(Of ROTurnAroundPointContactList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTurnAroundPointContact.GetROTurnAroundPointContact(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTurnAroundPointContactList"
            cm.Parameters.AddWithValue("@TurnAroundPointID", NothingDBNull(crit.TurnAroundPointID))
            cm.Parameters.AddWithValue("@ContactName", Strings.MakeEmptyDBNull(crit.ContactName))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace