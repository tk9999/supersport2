﻿' Generated 15 Mar 2016 10:47 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If


Namespace Maintenance.SatOps

  <Serializable()> _
  Public Class TurnAroundPointContact
    Inherits OBBusinessBase(Of TurnAroundPointContact)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TurnAroundPointContactIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TurnAroundPointContactID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property TurnAroundPointContactID() As Integer
      Get
        Return GetProperty(TurnAroundPointContactIDProperty)
      End Get
    End Property

    Public Shared TurnAroundPointIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TurnAroundPointID, "Turn Around Point", Nothing)
    ''' <summary>
    ''' Gets and sets the Turn Around Point value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property TurnAroundPointID() As Integer
      Get
        Return GetProperty(TurnAroundPointIDProperty)
      End Get
    End Property

    Public Shared ContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactName, "Contact Name", "")
    ''' <summary>
    ''' Gets and sets the Contact Name value
    ''' </summary>
    <Display(Name:="Contact Name", Description:=""),
    StringLength(50, ErrorMessage:="Contact Name cannot be more than 50 characters"),
    Required(ErrorMessage:="A contact name is required")>
    Public Property ContactName() As String
      Get
        Return GetProperty(ContactNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContactNameProperty, Value)
      End Set
    End Property

    Public Shared ContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactNumber, "Contact Number", "")
    ''' <summary>
    ''' Gets and sets the Contact Number value
    ''' </summary>
    <Display(Name:="Contact Number", Description:=""),
    StringLength(20, ErrorMessage:="Contact Number cannot be more than 20 characters"),
    Required(ErrorMessage:="A contact number is required")>
    Public Property ContactNumber() As String
      Get
        Return GetProperty(ContactNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContactNumberProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "




    Public Function GetParent() As TurnAroundPoint

      Return CType(CType(Me.Parent, TurnAroundPointContactList).Parent, TurnAroundPoint)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TurnAroundPointIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ContactName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Turn Around Point Contact")
        Else
          Return String.Format("Blank {0}", "Turn Around Point Contact")
        End If
      Else
        Return Me.ContactName
      End If

    End Function

#End Region

    '#Region " Child Lists "

    '        Public Shared TurnAroundPointContactProperty As PropertyInfo(Of TurnAroundPointContactList) = RegisterProperty(Of TurnAroundPointContactList)(Function(c) c.TurnAroundPointContactList, "Turn Around Point Contact List")

    '        Public ReadOnly Property TurnAroundPointContactList() As TurnAroundPointContactList
    '            Get
    '                If GetProperty(TurnAroundPointContactProperty) Is Nothing Then
    '                    LoadProperty(TurnAroundPointContactProperty, Maintenance.SatOps.TurnAroundPointContactList.NewTurnAroundPointContactList())
    '                End If
    '                Return GetProperty(TurnAroundPointContactProperty)
    '            End Get
    '        End Property

    '#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTurnAroundPointContact() method.

    End Sub

    Public Shared Function NewTurnAroundPointContact() As TurnAroundPointContact

      Return DataPortal.CreateChild(Of TurnAroundPointContact)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#Region ".Net Data Access"

    Friend Shared Function GetTurnAroundPointContact(dr As SafeDataReader) As TurnAroundPointContact

      Dim t As New TurnAroundPointContact()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TurnAroundPointContactIDProperty, .GetInt32(0))
          LoadProperty(TurnAroundPointIDProperty, .GetInt32(1))
          LoadProperty(ContactNameProperty, .GetString(2))
          LoadProperty(ContactNumberProperty, .GetString(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetValue(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetValue(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

#End Region

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTurnAroundPointContact"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTurnAroundPointContact"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTurnAroundPointContactID As SqlParameter = .Parameters.Add("@TurnAroundPointContactID", SqlDbType.Int)
          paramTurnAroundPointContactID.Value = GetProperty(TurnAroundPointContactIDProperty)
          If Me.IsNew Then
            paramTurnAroundPointContactID.Direction = ParameterDirection.Output
          End If
          cm.Parameters.AddWithValue("@TurnAroundPointID", Me.GetParent().TurnAroundPointID)
          cm.Parameters.AddWithValue("@ContactName", GetProperty(ContactNameProperty))
          cm.Parameters.AddWithValue("@ContactNumber", GetProperty(ContactNumberProperty))
          cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(TurnAroundPointContactIDProperty, paramTurnAroundPointContactID.Value)
          End If
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTurnAroundPointContact"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TurnAroundPointContactID", GetProperty(TurnAroundPointContactIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End Region

  End Class
End Namespace
