﻿' Generated 28 Mar 2015 12:21 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.SatOps

  <Serializable()> _
  Public Class EquipmentScheduleType
    Inherits SingularBusinessBase(Of EquipmentScheduleType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EquipmentScheduleTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentScheduleTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property EquipmentScheduleTypeID() As Integer
      Get
        Return GetProperty(EquipmentScheduleTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentScheduleTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentScheduleType, "Booking Type", "")
    ''' <summary>
    ''' Gets and sets the Equipment Schedule Type value
    ''' </summary>
    <Display(Name:="Booking Type", Description:=""),
    StringLength(100, ErrorMessage:="Booking Type cannot be more than 100 characters"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Booking Type is required")>
    Public Property EquipmentScheduleType() As String
      Get
        Return GetProperty(EquipmentScheduleTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EquipmentScheduleTypeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentScheduleTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EquipmentScheduleType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Equipment Schedule Type")
        Else
          Return String.Format("Blank {0}", "Equipment Schedule Type")
        End If
      Else
        Return Me.EquipmentScheduleType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipmentScheduleType() method.

    End Sub

    Public Shared Function NewEquipmentScheduleType() As EquipmentScheduleType

      Return DataPortal.CreateChild(Of EquipmentScheduleType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEquipmentScheduleType(dr As SafeDataReader) As EquipmentScheduleType

      Dim e As New EquipmentScheduleType()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EquipmentScheduleTypeIDProperty, .GetInt32(0))
          LoadProperty(EquipmentScheduleTypeProperty, .GetString(1))
          LoadProperty(CreatedByProperty, .GetInt32(2))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
          LoadProperty(ModifiedByProperty, .GetInt32(4))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEquipmentScheduleType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEquipmentScheduleType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramEquipmentScheduleTypeID As SqlParameter = .Parameters.Add("@EquipmentScheduleTypeID", SqlDbType.Int)
          paramEquipmentScheduleTypeID.Value = GetProperty(EquipmentScheduleTypeIDProperty)
          If Me.IsNew Then
            paramEquipmentScheduleTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EquipmentScheduleType", GetProperty(EquipmentScheduleTypeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EquipmentScheduleTypeIDProperty, paramEquipmentScheduleTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEquipmentScheduleType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EquipmentScheduleTypeID", GetProperty(EquipmentScheduleTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace