﻿' Generated 28 Mar 2015 12:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.SatOps

  <Serializable()> _
  Public Class VideoSetting
    Inherits SingularBusinessBase(Of VideoSetting)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VideoSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VideoSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property VideoSettingID() As Integer
      Get
        Return GetProperty(VideoSettingIDProperty)
      End Get
    End Property

    Public Shared VideoSettingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VideoSetting, "Video Setting", "")
    ''' <summary>
    ''' Gets and sets the Video Setting value
    ''' </summary>
    <Display(Name:="Video Setting", Description:=""),
    StringLength(50, ErrorMessage:="Video Setting cannot be more than 50 characters"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Setting Name is required")>
    Public Property VideoSetting() As String
      Get
        Return GetProperty(VideoSettingProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VideoSettingProperty, Value)
      End Set
    End Property

    Public Shared OldIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.OldInd, "Old", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Old value
    ''' </summary>
    <Display(Name:="Old", Description:="")>
    Public Property OldInd() As Boolean?
      Get
        Return GetProperty(OldIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(OldIndProperty, Value)
      End Set
    End Property

    Public Shared OldDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OldDate, "Old Date")
    ''' <summary>
    ''' Gets and sets the Old Date value
    ''' </summary>
    <Display(Name:="Old Date", Description:="")>
    Public Property OldDate As DateTime?
      Get
        Return GetProperty(OldDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OldDateProperty, Value)
      End Set
    End Property

    Public Shared OldReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OldReason, "Old Reason", "")
    ''' <summary>
    ''' Gets and sets the Old Reason value
    ''' </summary>
    <Display(Name:="Old Reason", Description:=""),
    StringLength(100, ErrorMessage:="Old Reason cannot be more than 100 characters")>
    Public Property OldReason() As String
      Get
        Return GetProperty(OldReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OldReasonProperty, Value)
      End Set
    End Property

    Public Shared OldByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OldByUserID, "Old By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Old By User value
    ''' </summary>
    <Display(Name:="Old By User", Description:="")>
    Public Property OldByUserID() As Integer?
      Get
        Return GetProperty(OldByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OldByUserIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VideoSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.VideoSetting.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Video Setting")
        Else
          Return String.Format("Blank {0}", "Video Setting")
        End If
      Else
        Return Me.VideoSetting
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewVideoSetting() method.

    End Sub

    Public Shared Function NewVideoSetting() As VideoSetting

      Return DataPortal.CreateChild(Of VideoSetting)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetVideoSetting(dr As SafeDataReader) As VideoSetting

      Dim v As New VideoSetting()
      v.Fetch(dr)
      Return v

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(VideoSettingIDProperty, .GetInt32(0))
          LoadProperty(VideoSettingProperty, .GetString(1))
          Dim tmpOldInd = .GetValue(2)
          If IsDBNull(tmpOldInd) Then
            LoadProperty(OldIndProperty, Nothing)
          Else
            LoadProperty(OldIndProperty, tmpOldInd)
          End If
          LoadProperty(OldDateProperty, .GetValue(3))
          LoadProperty(OldReasonProperty, .GetString(4))
          LoadProperty(OldByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insVideoSetting"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updVideoSetting"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramVideoSettingID As SqlParameter = .Parameters.Add("@VideoSettingID", SqlDbType.Int)
          paramVideoSettingID.Value = GetProperty(VideoSettingIDProperty)
          If Me.IsNew Then
            paramVideoSettingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@VideoSetting", GetProperty(VideoSettingProperty))
          .Parameters.AddWithValue("@OldInd", Singular.Misc.NothingDBNull(GetProperty(OldIndProperty)))
          .Parameters.AddWithValue("@OldDate", (New SmartDate(GetProperty(OldDateProperty))).DBValue)
          .Parameters.AddWithValue("@OldReason", GetProperty(OldReasonProperty))
          .Parameters.AddWithValue("@OldByUserID", Singular.Misc.NothingDBNull(GetProperty(OldByUserIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(VideoSettingIDProperty, paramVideoSettingID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delVideoSetting"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@VideoSettingID", GetProperty(VideoSettingIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace