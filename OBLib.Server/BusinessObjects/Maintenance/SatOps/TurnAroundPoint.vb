﻿' Generated 28 Mar 2015 12:29 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Vehicles.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.Maintenance.SatOps
Imports OBLib.SatOps.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Locations.ReadOnly

Namespace Maintenance.SatOps

  <Serializable()> _
  Public Class TurnAroundPoint
    Inherits OBBusinessBase(Of TurnAroundPoint)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ExpandedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Expanded, "Expanded", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Expanded")>
    Public Property Expanded() As Boolean
      Get
        Return GetProperty(ExpandedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(ExpandedProperty, value)
      End Set
    End Property

    Public Shared TurnAroundPointIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TurnAroundPointID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property TurnAroundPointID() As Integer
      Get
        Return GetProperty(TurnAroundPointIDProperty)
      End Get
    End Property

    Public Shared TurnAroundPointProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TurnAroundPoint, "Turn Around Point", "")
    ''' <summary>
    ''' Gets and sets the Turn Around Point value
    ''' </summary>
        <Display(Name:="Turn Around Point", Description:="Name of the turnaround point"),
        StringLength(100, ErrorMessage:="Turn Around Point cannot be more than 100 characters"),
        Required(ErrorMessage:="The turn around point name is required")>
        Public Property TurnAroundPoint() As String
            Get
                Return GetProperty(TurnAroundPointProperty)
            End Get
            Set(ByVal Value As String)
                SetProperty(TurnAroundPointProperty, Value)
            End Set
        End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryID, "Country", Nothing)
    ''' <summary>
    ''' Gets and sets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="The country in which the turnaround point resides"),
    Required(ErrorMessage:="Country required"),
    DropDownWeb(GetType(ROCountryList))>
    Public Property CountryID() As Integer?
      Get
        Return GetProperty(CountryIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CountryIDProperty, Value)
      End Set
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets and sets the City value
    ''' </summary>
    <Display(Name:="City", Description:=""),
    DropDownWeb(GetType(ROCityList))>
    Public Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CityIDProperty, Value)
      End Set
    End Property

    Public Shared LocationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LocationID, "Location", Nothing)
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:=""),
    DropDownWeb(GetType(ROLocationList))>
    Public Property LocationID() As Integer?
      Get
        Return GetProperty(LocationIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LocationIDProperty, Value)
      End Set
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:=""),
    DropDownWeb(GetType(ROVehicleOldList))>
    Public Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared TurnAroundPointContactListProperty As PropertyInfo(Of TurnAroundPointContactList) = RegisterProperty(Of TurnAroundPointContactList)(Function(c) c.TurnAroundPointContactList, "Turn Around Point Contact List")

    Public ReadOnly Property TurnAroundPointContactList() As TurnAroundPointContactList
      Get
        If GetProperty(TurnAroundPointContactListProperty) Is Nothing Then
          LoadProperty(TurnAroundPointContactListProperty, Maintenance.SatOps.TurnAroundPointContactList.NewTurnAroundPointContactList())
        End If
        Return GetProperty(TurnAroundPointContactListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TurnAroundPointIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.TurnAroundPoint.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Turn Around Point")
        Else
          Return String.Format("Blank {0}", "Turn Around Point")
        End If
      Else
        Return Me.TurnAroundPoint
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"TurnAroundPointContacts"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "




    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTurnAroundPoint() method.


    End Sub

    Public Shared Function NewTurnAroundPoint() As TurnAroundPoint

      Return DataPortal.CreateChild(Of TurnAroundPoint)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTurnAroundPoint(dr As SafeDataReader) As TurnAroundPoint

      Dim t As New TurnAroundPoint()
      t.Fetch(dr)
      Return t

    End Function



    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TurnAroundPointIDProperty, .GetInt32(0))
          LoadProperty(TurnAroundPointProperty, .GetString(1))
          LoadProperty(CountryIDProperty, ZeroNothing(.GetInt32(2)))
          LoadProperty(CityIDProperty, ZeroNothing(.GetInt32(3)))
          LoadProperty(LocationIDProperty, ZeroNothing(.GetInt32(4)))
          LoadProperty(VehicleIDProperty, ZeroNothing(.GetInt32(5)))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(9))

        End With
      End Using


      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTurnAroundPoint"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTurnAroundPoint"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTurnAroundPointID As SqlParameter = .Parameters.Add("@TurnAroundPointID", SqlDbType.Int)
          paramTurnAroundPointID.Value = GetProperty(TurnAroundPointIDProperty)
          If Me.IsNew Then
            paramTurnAroundPointID.Direction = ParameterDirection.Output
          End If

          .Parameters.AddWithValue("@TurnAroundPoint", NothingDBNull(GetProperty(TurnAroundPointProperty)))
          .Parameters.AddWithValue("@CountryID", NothingDBNull(GetProperty(CountryIDProperty)))
          .Parameters.AddWithValue("@CityID", NothingDBNull(GetProperty(CityIDProperty)))
          .Parameters.AddWithValue("@LocationID", NothingDBNull(GetProperty(LocationIDProperty)))
          .Parameters.AddWithValue("@VehicleID", NothingDBNull(GetProperty(VehicleIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .ExecuteNonQuery()


          If Me.IsNew Then
            LoadProperty(TurnAroundPointIDProperty, paramTurnAroundPointID.Value)
          End If
          ' update child objects
          If GetProperty(TurnAroundPointContactListProperty) IsNot Nothing Then
            Me.TurnAroundPointContactList.Update()
          End If

          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(TurnAroundPointContactListProperty) IsNot Nothing Then
          Me.TurnAroundPointContactList.Update()
        End If

      End If
    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTurnAroundPoint"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TurnAroundPointID", GetProperty(TurnAroundPointIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace