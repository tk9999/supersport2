﻿' Generated 13 Sep 2014 17:07 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ICR

  <Serializable()> _
  Public Class ICRContractTypeMonth
    Inherits OBBusinessBase(Of ICRContractTypeMonth)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IcrContractTypeMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.IcrContractTypeMonthID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property IcrContractTypeMonthID() As Integer
      Get
        Return GetProperty(IcrContractTypeMonthIDProperty)
      End Get
    End Property

    Public Shared IcrMonthIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IcrMonthID, "Icr Month", Nothing)
    ''' <summary>
    ''' Gets the Icr Month value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property IcrMonthID() As Integer?
      Get
        Return GetProperty(IcrMonthIDProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "Contract Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:=""),
    Required(ErrorMessage:="Contract Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROContractTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ContractTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ICRMonth

      Return CType(CType(Me.Parent, ICRContractTypeMonthList).Parent, ICRMonth)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(IcrContractTypeMonthIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ContractTypeID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "ICR Contract Type Month")
        Else
          Return String.Format("Blank {0}", "ICR Contract Type Month")
        End If
      Else
        Return Me.ContractTypeID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewICRContractTypeMonth() method.

    End Sub

    Public Shared Function NewICRContractTypeMonth() As ICRContractTypeMonth

      Return DataPortal.CreateChild(Of ICRContractTypeMonth)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetICRContractTypeMonth(dr As SafeDataReader) As ICRContractTypeMonth

      Dim i As New ICRContractTypeMonth()
      i.Fetch(dr)
      Return i

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(IcrContractTypeMonthIDProperty, .GetInt32(0))
          LoadProperty(IcrMonthIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insICRContractTypeMonth"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updICRContractTypeMonth"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramIcrContractTypeMonthID As SqlParameter = .Parameters.Add("@IcrContractTypeMonthID", SqlDbType.Int)
          paramIcrContractTypeMonthID.Value = GetProperty(IcrContractTypeMonthIDProperty)
          If Me.IsNew Then
            paramIcrContractTypeMonthID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@IcrMonthID", Me.GetParent().IcrMonthID)
          .Parameters.AddWithValue("@ContractTypeID", GetProperty(ContractTypeIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(IcrContractTypeMonthIDProperty, paramIcrContractTypeMonthID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delICRContractTypeMonth"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@IcrContractTypeMonthID", GetProperty(IcrContractTypeMonthIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace