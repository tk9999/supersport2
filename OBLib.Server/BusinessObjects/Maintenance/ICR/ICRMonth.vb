﻿' Generated 13 Sep 2014 17:07 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ICR

  <Serializable()> _
  Public Class ICRMonth
    Inherits OBBusinessBase(Of ICRMonth)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "ICRMonthBO.ICRMonthBOToString(self)")

    Public Shared IcrMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.IcrMonthID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property IcrMonthID() As Integer
      Get
        Return GetProperty(IcrMonthIDProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="Starting Date of the month"),
    Required(ErrorMessage:="Start Date required"),
    Singular.DataAnnotations.DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate) _
                                                                  .AddSetExpression("ICRMonthBO.SetMonthYear(self)")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="End Date of the month"),
    Required(ErrorMessage:="End Date required"),
    Singular.DataAnnotations.DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared MonthProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.Month, "Month", Nothing)
    ''' <summary>
    ''' Gets and sets the Month value
    ''' </summary>
    <Display(Name:="Month", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.CommonData.Enums.Month), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property Month() As Integer?
      Get
        Return GetProperty(MonthProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(MonthProperty, Value)
      End Set
    End Property

    Public Shared YearProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.Year, "Month", Nothing)
    ''' <summary>
    ''' Gets and sets the Year value
    ''' </summary>
    <Display(Name:="Year", Description:="")>
    Public Property Year() As Integer?
      Get
        Return GetProperty(YearProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(YearProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared WorkHoursRequiredProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.WorkHoursRequired, "Work Hours Required", 0)
    ''' <summary>
    ''' Gets and sets the Work Hours Required value
    ''' </summary>
    <Display(Name:="Work Hours Required", Description:=""),
    Required(ErrorMessage:="Work Hours for period required")>
    Public Property WorkHoursRequired() As Integer?
      Get
        Return GetProperty(WorkHoursRequiredProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(WorkHoursRequiredProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ICRContractTypeMonthListProperty As PropertyInfo(Of ICRContractTypeMonthList) = RegisterProperty(Of ICRContractTypeMonthList)(Function(c) c.ICRContractTypeMonthList, "ICR Contract Type Month List")

    Public ReadOnly Property ICRContractTypeMonthList() As ICRContractTypeMonthList
      Get
        If GetProperty(ICRContractTypeMonthListProperty) Is Nothing Then
          LoadProperty(ICRContractTypeMonthListProperty, Maintenance.ICR.ICRContractTypeMonthList.NewICRContractTypeMonthList())
        End If
        Return GetProperty(ICRContractTypeMonthListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(IcrMonthIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.StartDate.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "ICR Month")
        Else
          Return String.Format("Blank {0}", "ICR Month")
        End If
      Else
        Return Me.StartDate.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ICRContractTypeMonths"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(EndDateProperty)
        .JavascriptRuleFunctionName = "ICRMonthBO.StartEndDateDifference"
        .ServerRuleFunction = AddressOf StartEndDateDifference
      End With

    End Sub

    Public Shared Function StartEndDateDifference(ICRM As ICRMonth) As String
      Dim ErrorString = ""

      Dim startDate = ICRM.StartDate
      Dim endDate = ICRM.EndDate
      Dim difference As TimeSpan = endDate - startDate
      If difference.Days > 31 Then
        ErrorString = "Start and End Date Greater than 1 month"
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewICRMonth() method.

    End Sub

    Public Shared Function NewICRMonth() As ICRMonth

      Return DataPortal.CreateChild(Of ICRMonth)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetICRMonth(dr As SafeDataReader) As ICRMonth

      Dim i As New ICRMonth()
      i.Fetch(dr)
      Return i

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(IcrMonthIDProperty, .GetInt32(0))
          LoadProperty(MonthProperty, .GetInt32(1))
          LoadProperty(YearProperty, .GetInt32(2))
          LoadProperty(StartDateProperty, .GetValue(3))
          LoadProperty(EndDateProperty, .GetValue(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(WorkHoursRequiredProperty, .GetInt32(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insICRMonth"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updICRMonth"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramIcrMonthID As SqlParameter = .Parameters.Add("@IcrMonthID", SqlDbType.Int)
          paramIcrMonthID.Value = GetProperty(IcrMonthIDProperty)
          If Me.IsNew Then
            paramIcrMonthID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Month", NothingDBNull(GetProperty(MonthProperty)))
          .Parameters.AddWithValue("@Year", NothingDBNull(GetProperty(YearProperty)))
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@WorkHoursRequired", GetProperty(WorkHoursRequiredProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(IcrMonthIDProperty, paramIcrMonthID.Value)
          End If
          ' update child objects
          If GetProperty(ICRContractTypeMonthListProperty) IsNot Nothing Then
            Me.ICRContractTypeMonthList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ICRContractTypeMonthListProperty) IsNot Nothing Then
          Me.ICRContractTypeMonthList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delICRMonth"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@IcrMonthID", GetProperty(IcrMonthIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace