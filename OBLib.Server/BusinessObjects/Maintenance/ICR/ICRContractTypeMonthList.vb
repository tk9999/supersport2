﻿' Generated 13 Sep 2014 17:07 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ICR

  <Serializable()> _
  Public Class ICRContractTypeMonthList
    Inherits OBBusinessListBase(Of ICRContractTypeMonthList, ICRContractTypeMonth)

#Region " Business Methods "

    Public Function GetItem(IcrContractTypeMonthID As Integer) As ICRContractTypeMonth

      For Each child As ICRContractTypeMonth In Me
        If child.IcrContractTypeMonthID = IcrContractTypeMonthID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "ICR Contract Type Months"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewICRContractTypeMonthList() As ICRContractTypeMonthList

      Return New ICRContractTypeMonthList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ICRContractTypeMonth In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ICRContractTypeMonth In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace