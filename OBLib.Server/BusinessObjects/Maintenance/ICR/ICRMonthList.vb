﻿' Generated 13 Sep 2014 17:07 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ICR

  <Serializable()> _
  Public Class ICRMonthList
    Inherits OBBusinessListBase(Of ICRMonthList, ICRMonth)

#Region " Business Methods "

    Public Function GetItem(IcrMonthID As Integer) As ICRMonth

      For Each child As ICRMonth In Me
        If child.IcrMonthID = IcrMonthID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "ICR Months"

    End Function

    Public Function GetICRContractTypeMonth(IcrContractTypeMonthID As Integer) As ICRContractTypeMonth

      Dim obj As ICRContractTypeMonth = Nothing
      For Each parent As ICRMonth In Me
        obj = parent.ICRContractTypeMonthList.GetItem(IcrContractTypeMonthID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewICRMonthList() As ICRMonthList

      Return New ICRMonthList()

    End Function

    Public Shared Sub BeginGetICRMonthList(CallBack As EventHandler(Of DataPortalResult(Of ICRMonthList)))

      Dim dp As New DataPortal(Of ICRMonthList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetICRMonthList() As ICRMonthList

      Return DataPortal.Fetch(Of ICRMonthList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ICRMonth.GetICRMonth(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ICRMonth = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.IcrMonthID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ICRContractTypeMonthList.RaiseListChangedEvents = False
          parent.ICRContractTypeMonthList.Add(ICRContractTypeMonth.GetICRContractTypeMonth(sdr))
          parent.ICRContractTypeMonthList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As ICRMonth In Me
        child.CheckRules()
        For Each ICRContractTypeMonth As ICRContractTypeMonth In child.ICRContractTypeMonthList
          ICRContractTypeMonth.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getICRMonthList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ICRMonth In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ICRMonth In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace