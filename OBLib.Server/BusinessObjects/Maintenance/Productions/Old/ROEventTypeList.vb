﻿' Generated 19 Feb 2014 17:18 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.ReadOnly.Old

  <Serializable()> _
  Public Class ROEventTypeList
    Inherits OBReadOnlyListBase(Of ROEventTypeList, ROEventType)

#Region " Parent "

    <NotUndoable()> Private mParent As ROProductionType
#End Region

#Region " Business Methods "

    Public Function GetItem(EventTypeID As Integer) As ROEventType

      For Each child As ROEventType In Me
        If child.EventTypeID = EventTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Event Types"

    End Function

    'Public Function GetROEventTypeSystem(EventTypeSystemID As Integer) As ROEventTypeSystem

    '  Dim obj As ROEventTypeSystem = Nothing
    '  For Each parent As ROEventType In Me
    '    obj = parent.ROEventTypeSystemList.GetItem(EventTypeSystemID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionTypeID As Integer? = Nothing

      <PrimarySearchField>
      Public Property EventType As String = ""

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROEventTypeList() As ROEventTypeList

      Return New ROEventTypeList()

    End Function

    Public Shared Function GetROEventTypeList() As ROEventTypeList

      Return DataPortal.Fetch(Of ROEventTypeList)(New Criteria())

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionTypeList() As ROProductionTypeList

      Return DataPortal.Fetch(Of ROProductionTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEventType.GetROEventType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      'Dim parent As ROProductionType = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.ProductionTypeID <> sdr.GetInt32(2) Then
      '      parent = Me.GetItem(sdr.GetInt32(2))
      '    End If
      '    parent.ROEventTypeList.RaiseListChangedEvents = False
      '    parent.ROEventTypeList.Add(ROEventType.GetROEventType(sdr))
      '    parent.ROEventTypeList.RaiseListChangedEvents = True
      '  End While
      'End If

      'Dim parentChild As ROEventType = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentChild Is Nothing OrElse parentChild.EventTypeID <> sdr.GetInt32(1) Then
      '      parentChild = Me.GetROEventType(sdr.GetInt32(1))
      '    End If
      '    parentChild.ROEventTypeSystemList.RaiseListChangedEvents = False
      '    parentChild.ROEventTypeSystemList.Add(ROEventTypeSystem.GetROEventTypeSystem(sdr))
      '    parentChild.ROEventTypeSystemList.RaiseListChangedEvents = True
      '  End While
      'End If


    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcs.getROEventTypeListOld"
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@EventType", Singular.Strings.MakeEmptyDBNull(crit.EventType))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace