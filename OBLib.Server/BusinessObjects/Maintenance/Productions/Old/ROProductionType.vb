﻿' Generated 19 Feb 2014 17:18 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.ReadOnly.Old

  <Serializable()> _
  Public Class ROProductionType
    Inherits OBReadOnlyBase(Of ROProductionType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionTypeID() As Integer
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Production Type", "")
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="Description for the Production Type")>
    Public ReadOnly Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
    End Property

    Public Shared ReportingColourProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportingColour, "Reporting Colour", "")
    ''' <summary>
    ''' Gets the Reporting Colour value
    ''' </summary>
    <Display(Name:="Reporting Colour", Description:="The colour to use on the Board Report")>
    Public ReadOnly Property ReportingColour() As String
      Get
        Return GetProperty(ReportingColourProperty)
      End Get
    End Property

    Public Shared TextColourProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TextColour, "Text Colour", "Brown")
    ''' <summary>
    ''' Gets the Text Colour value
    ''' </summary>
    <Display(Name:="Text Colour", Description:="")>
    Public ReadOnly Property TextColour() As String
      Get
        Return GetProperty(TextColourProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared OldIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OldInd, "Old", False)
    ''' <summary>
    ''' Gets the Old value
    ''' </summary>
    <Display(Name:="Old", Description:="")>
    Public ReadOnly Property OldInd() As Boolean
      Get
        Return GetProperty(OldIndProperty)
      End Get
    End Property

    Public Shared ImportedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ImportedInd, "Imported", False)
    ''' <summary>
    ''' Gets the Imported value
    ''' </summary>
    <Display(Name:="Imported", Description:="")>
    Public ReadOnly Property ImportedInd() As Boolean
      Get
        Return GetProperty(ImportedIndProperty)
      End Get
    End Property

    <ClientOnly()>
    Public Property VisibleClient As Boolean = True

#End Region

#Region " Child Lists "

    Public Shared ROEventTypeListProperty As PropertyInfo(Of ROEventTypeList) = RegisterProperty(Of ROEventTypeList)(Function(c) c.ROEventTypeList, "RO Event Type List")

    Public ReadOnly Property ROEventTypeList() As ROEventTypeList
      Get
        If GetProperty(ROEventTypeListProperty) Is Nothing Then
          LoadProperty(ROEventTypeListProperty, Maintenance.Productions.ReadOnly.Old.ROEventTypeList.NewROEventTypeList())
        End If
        Return GetProperty(ROEventTypeListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionType

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionType(dr As SafeDataReader) As ROProductionType

      Dim r As New ROProductionType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionTypeIDProperty, .GetInt32(0))
        LoadProperty(ProductionTypeProperty, .GetString(1))
        LoadProperty(ReportingColourProperty, .GetString(2))
        LoadProperty(TextColourProperty, .GetString(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(OldIndProperty, .GetBoolean(8))
        LoadProperty(ImportedIndProperty, .GetBoolean(9))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace