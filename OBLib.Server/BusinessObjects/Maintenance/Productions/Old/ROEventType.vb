﻿' Generated 19 Feb 2014 17:18 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.ReadOnly.Old

  <Serializable()> _
  Public Class ROEventType
    Inherits OBReadOnlyBase(Of ROEventType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property EventTypeID() As Integer
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "Event Type", "")
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="Description for the Event Type")>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    ''' 
    Public ReadOnly Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 0)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="Department", Description:="", AutoGenerateField:=False)>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    <ClientOnly()>
    Public Property VisibleClient As Boolean = True

    Public Shared ShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortName, "Short Name", "")
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Short Name", Description:="Brief Description for the Event Type, to be used in Smses")>
    Public ReadOnly Property ShortName() As String
      Get
        Return GetProperty(ShortNameProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    'Public Shared ROEventTypeSystemListProperty As PropertyInfo(Of ROEventTypeSystemList) = RegisterProperty(Of ROEventTypeSystemList)(Function(c) c.ROEventTypeSystemList, "RO Event Type System List")

    '<Display(AutoGenerateField:=False)>
    'Public ReadOnly Property ROEventTypeSystemList() As ROEventTypeSystemList
    '  Get
    '    If GetProperty(ROEventTypeSystemListProperty) Is Nothing Then
    '      LoadProperty(ROEventTypeSystemListProperty, Maintenance.ReadOnly.ROEventTypeSystemList.NewROEventTypeSystemList())
    '    End If
    '    Return GetProperty(ROEventTypeSystemListProperty)
    '  End Get
    'End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EventTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EventType

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEventType(dr As SafeDataReader) As ROEventType

      Dim r As New ROEventType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EventTypeIDProperty, .GetInt32(0))
        LoadProperty(EventTypeProperty, .GetString(1))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(SystemIDProperty, .GetInt32(7))
        LoadProperty(ShortNameProperty, .GetString(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace