﻿' Generated 12 Feb 2014 18:16 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.Productions.ReadOnly

  <Serializable()> _
  Public Class ROProductionVenueList
    Inherits OBReadOnlyListBase(Of ROProductionVenueList, ROProductionVenue)

#Region " Business Methods "

    Public Function GetItem(ProductionVenueID As Integer) As ROProductionVenue

      For Each child As ROProductionVenue In Me
        If child.ProductionVenueID = ProductionVenueID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property ProductionVenue As String

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionVenueList() As ROProductionVenueList

      Return New ROProductionVenueList()

    End Function

    Public Shared Sub BeginGetROProductionVenueList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionVenueList)))

      Dim dp As New DataPortal(Of ROProductionVenueList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionVenueList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionVenueList)))

      Dim dp As New DataPortal(Of ROProductionVenueList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionVenueList() As ROProductionVenueList

      Return DataPortal.Fetch(Of ROProductionVenueList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionVenue.GetROProductionVenue(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionVenueList"
            cm.Parameters.AddWithValue("@ProductionVenue", Strings.MakeEmptyDBNull(crit.ProductionVenue))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace