﻿' Generated 10 Aug 2014 14:30 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Areas.ReadOnly

  <Serializable()> _
  Public Class ROProductionAreaAllowedDisciplineList
    Inherits OBReadOnlyListBase(Of ROProductionAreaAllowedDisciplineList, ROProductionAreaAllowedDiscipline)

#Region " Business Methods "

    Public Function GetItem(AllowedDisciplineID As Integer) As ROProductionAreaAllowedDiscipline

      For Each child As ROProductionAreaAllowedDiscipline In Me
        If child.AllowedDisciplineID = AllowedDisciplineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Area Allowed Disciplines"

    End Function

    Public Function GetDisciplinesForSystemArea(SystemID As Integer?, ProductionAreaID As Integer?) As ROProductionAreaAllowedDisciplineList

      Dim Results As New ROProductionAreaAllowedDisciplineList
      For Each d As ROProductionAreaAllowedDiscipline In Me
        If d.SystemID = SystemID AndAlso d.ProductionAreaID = ProductionAreaID Then
          Results.Add(d)
        End If
      Next
      Return Results

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property SystemIDs As String = ""
      Public Property ProductionAreaIDs As String = ""

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionAreaAllowedDisciplineList() As ROProductionAreaAllowedDisciplineList

      Return New ROProductionAreaAllowedDisciplineList()

    End Function

    Public Shared Sub BeginGetROProductionAreaAllowedDisciplineList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionAreaAllowedDisciplineList)))

      Dim dp As New DataPortal(Of ROProductionAreaAllowedDisciplineList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionAreaAllowedDisciplineList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionAreaAllowedDisciplineList)))

      Dim dp As New DataPortal(Of ROProductionAreaAllowedDisciplineList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionAreaAllowedDisciplineList() As ROProductionAreaAllowedDisciplineList

      Return DataPortal.Fetch(Of ROProductionAreaAllowedDisciplineList)(New Criteria())

    End Function

    Public Shared Function GetROProductionAreaAllowedDisciplineList(SystemID As Integer?, ProductionAreaID As Integer?) As ROProductionAreaAllowedDisciplineList

      Return DataPortal.Fetch(Of ROProductionAreaAllowedDisciplineList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionAreaAllowedDiscipline.GetROProductionAreaAllowedDiscipline(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionAreaAllowedDisciplineList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@SystemIDs", Strings.MakeEmptyDBNull(crit.SystemIDs))
            cm.Parameters.AddWithValue("@ProductionAreaIDs", Strings.MakeEmptyDBNull(crit.ProductionAreaIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace