﻿' Generated 20 Apr 2016 12:08 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.ReadOnly

  <Serializable()> _
  Public Class ROEventTypeReport
    Inherits OBReadOnlyBase(Of ROEventTypeReport)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventTypeID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property EventTypeID() As Integer
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "Event Type")
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:=""), SetExpression("ROEventTypesCriteriaBO.SetEventTypes", , 250), TextField()>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeID, "Production Type")
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionTypeID() As Integer
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ImportedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ImportedInd, "Imported", False)
    ''' <summary>
    ''' Gets the Imported value
    ''' </summary>
    <Display(Name:="Imported", Description:="")>
    Public ReadOnly Property ImportedInd() As Boolean
      Get
        Return GetProperty(ImportedIndProperty)
      End Get
    End Property

    Public Shared OBIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBInd, "OB", False)
    ''' <summary>
    ''' Gets the OB value
    ''' </summary>
    <Display(Name:="OB", Description:="")>
    Public ReadOnly Property OBInd() As Boolean
      Get
        Return GetProperty(OBIndProperty)
      End Get
    End Property

    Public Shared StudioIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.StudioInd, "Studio", False)
    ''' <summary>
    ''' Gets the Studio value
    ''' </summary>
    <Display(Name:="Studio", Description:="")>
    Public ReadOnly Property StudioInd() As Boolean
      Get
        Return GetProperty(StudioIndProperty)
      End Get
    End Property

    Public Shared PlayoutIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PlayoutInd, "Playout", False)
    ''' <summary>
    ''' Gets the Playout value
    ''' </summary>
    <Display(Name:="Playout", Description:="")>
    Public ReadOnly Property PlayoutInd() As Boolean
      Get
        Return GetProperty(PlayoutIndProperty)
      End Get
    End Property

    Public Shared HighlightsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HighlightsInd, "Highlights", False)
    ''' <summary>
    ''' Gets the Highlights value
    ''' </summary>
    <Display(Name:="Highlights", Description:="")>
    Public ReadOnly Property HighlightsInd() As Boolean
      Get
        Return GetProperty(HighlightsIndProperty)
      End Get
    End Property

    Public Shared MagazineShowIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MagazineShowInd, "Magazine Show", False)
    ''' <summary>
    ''' Gets the Magazine Show value
    ''' </summary>
    <Display(Name:="Magazine Show", Description:="")>
    Public ReadOnly Property MagazineShowInd() As Boolean
      Get
        Return GetProperty(MagazineShowIndProperty)
      End Get
    End Property

    Public Shared ShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortName, "Short Name")
    ''' <summary>
    ''' Gets the Short Name value
    ''' </summary>
    <Display(Name:="Short Name", Description:="")>
    Public ReadOnly Property ShortName() As String
      Get
        Return GetProperty(ShortNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EventTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EventType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROEventTypeReport(dr As SafeDataReader) As ROEventTypeReport

      Dim r As New ROEventTypeReport()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EventTypeIDProperty, .GetInt32(0))
        LoadProperty(EventTypeProperty, .GetString(1))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(6))
        LoadProperty(ImportedIndProperty, .GetBoolean(7))
        LoadProperty(OBIndProperty, .GetBoolean(8))
        LoadProperty(StudioIndProperty, .GetBoolean(9))
        LoadProperty(PlayoutIndProperty, .GetBoolean(10))
        LoadProperty(HighlightsIndProperty, .GetBoolean(11))
        LoadProperty(MagazineShowIndProperty, .GetBoolean(12))
        LoadProperty(ShortNameProperty, .GetString(13))
      End With

    End Sub

#End Region

  End Class
End Namespace
