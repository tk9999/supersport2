﻿' Generated 26 Jun 2014 17:55 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.Productions.Areas.ReadOnly

  <Serializable()> _
  Public Class ROProductionAreaStatusList
    Inherits SingularReadOnlyListBase(Of ROProductionAreaStatusList, ROProductionAreaStatus)

#Region " Business Methods "

    Public Function GetItem(ProductionAreaStatusID As Integer) As ROProductionAreaStatus

      For Each child As ROProductionAreaStatus In Me
        If child.ProductionAreaStatusID = ProductionAreaStatusID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Area Statuses"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionAreaStatusID As Integer? = Nothing

      <Display(Name:="ProductionAreaStatus", Description:=""), PrimarySearchField>
      Public Property ProductionAreaStatus() As String

      Public Sub New(ProductionAreaStatusID As Integer?)
        Me.ProductionAreaStatusID = ProductionAreaStatusID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionAreaStatusList() As ROProductionAreaStatusList

      Return New ROProductionAreaStatusList()

    End Function

    Public Shared Sub BeginGetROProductionAreaStatusList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionAreaStatusList)))

      Dim dp As New DataPortal(Of ROProductionAreaStatusList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionAreaStatusList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionAreaStatusList)))

      Dim dp As New DataPortal(Of ROProductionAreaStatusList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionAreaStatusList() As ROProductionAreaStatusList

      Return DataPortal.Fetch(Of ROProductionAreaStatusList)(New Criteria())

    End Function

    Public Shared Function GetROPositionList(ProductionAreaStatusID As Integer?) As ROProductionAreaStatusList

      Return DataPortal.Fetch(Of ROProductionAreaStatusList)(New Criteria(ProductionAreaStatusID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionAreaStatus.GetROProductionAreaStatus(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionAreaStatusList"
            cm.Parameters.AddWithValue("@ProductionAreaStatus", NothingDBNull(crit.ProductionAreaStatus))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace