﻿' Generated 25 Jul 2014 19:18 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROProductionVenueFull
    Inherits SingularReadOnlyBase(Of ROProductionVenueFull)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionVenueID() As Integer
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Production Venue", "")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="The name of the production venue")>
    Public ReadOnly Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="The city that this venue is in")>
    Public ReadOnly Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared DirectionsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Directions, "Directions", "")
    ''' <summary>
    ''' Gets the Directions value
    ''' </summary>
    <Display(Name:="Directions", Description:="")>
    Public ReadOnly Property Directions() As String
      Get
        Return GetProperty(DirectionsProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    <ClientOnly()>
    Public Property VisibleClient As Boolean = True

#End Region

#Region " Child Lists "

    Public Shared ROProductionVenueProductionTypeListProperty As PropertyInfo(Of ROProductionVenueProductionTypeList) = RegisterProperty(Of ROProductionVenueProductionTypeList)(Function(c) c.ROProductionVenueProductionTypeList, "RO Production Venue Production Type List")

    Public ReadOnly Property ROProductionVenueProductionTypeList() As ROProductionVenueProductionTypeList
      Get
        If GetProperty(ROProductionVenueProductionTypeListProperty) Is Nothing Then
          LoadProperty(ROProductionVenueProductionTypeListProperty, Maintenance.General.ReadOnly.ROProductionVenueProductionTypeList.NewROProductionVenueProductionTypeList())
        End If
        Return GetProperty(ROProductionVenueProductionTypeListProperty)
      End Get
    End Property

    Public Shared ROProductionVenueOutsourceServiceTypeListProperty As PropertyInfo(Of ROProductionVenueOutsourceServiceTypeList) = RegisterProperty(Of ROProductionVenueOutsourceServiceTypeList)(Function(c) c.ROProductionVenueOutsourceServiceTypeList, "RO Production Venue Outsource Service Type List")

    Public ReadOnly Property ROProductionVenueOutsourceServiceTypeList() As ROProductionVenueOutsourceServiceTypeList
      Get
        If GetProperty(ROProductionVenueOutsourceServiceTypeListProperty) Is Nothing Then
          LoadProperty(ROProductionVenueOutsourceServiceTypeListProperty, Maintenance.General.ReadOnly.ROProductionVenueOutsourceServiceTypeList.NewROProductionVenueOutsourceServiceTypeList())
        End If
        Return GetProperty(ROProductionVenueOutsourceServiceTypeListProperty)
      End Get
    End Property

    Public Shared ROProductionVenueContactListProperty As PropertyInfo(Of ROProductionVenueContactList) = RegisterProperty(Of ROProductionVenueContactList)(Function(c) c.ROProductionVenueContactList, "RO Production Venue Contact List")

    Public ReadOnly Property ROProductionVenueContactList() As ROProductionVenueContactList
      Get
        If GetProperty(ROProductionVenueContactListProperty) Is Nothing Then
          LoadProperty(ROProductionVenueContactListProperty, Maintenance.General.ReadOnly.ROProductionVenueContactList.NewROProductionVenueContactList())
        End If
        Return GetProperty(ROProductionVenueContactListProperty)
      End Get
    End Property

    Public Shared ROProductionVenueAreaListProperty As PropertyInfo(Of ROProductionVenueAreaList) = RegisterProperty(Of ROProductionVenueAreaList)(Function(c) c.ROProductionVenueAreaList, "RO Production Venue Area List")

    Public ReadOnly Property ROProductionVenueAreaList() As ROProductionVenueAreaList
      Get
        If GetProperty(ROProductionVenueAreaListProperty) Is Nothing Then
          LoadProperty(ROProductionVenueAreaListProperty, Maintenance.General.ReadOnly.ROProductionVenueAreaList.NewROProductionVenueAreaList())
        End If
        Return GetProperty(ROProductionVenueAreaListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionVenue

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionVenueFull(dr As SafeDataReader) As ROProductionVenueFull

      Dim r As New ROProductionVenueFull()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionVenueIDProperty, .GetInt32(0))
        LoadProperty(ProductionVenueProperty, .GetString(1))
        LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(DirectionsProperty, .GetString(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(CityProperty, .GetString(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace