﻿' Generated 24 Jul 2014 16:52 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Correspondence.ReadOnly

  <Serializable()> _
  Public Class ROProductionPettyCashType
    Inherits SingularReadOnlyBase(Of ROProductionPettyCashType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionPettyCashTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionPettyCashTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionPettyCashTypeID() As Integer
      Get
        Return GetProperty(ProductionPettyCashTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionPettyCashTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionPettyCashType, "Production Petty Cash Type", "")
    ''' <summary>
    ''' Gets the Production Petty Cash Type value
    ''' </summary>
    <Display(Name:="Production Petty Cash Type", Description:="")>
    Public ReadOnly Property ProductionPettyCashType() As String
      Get
        Return GetProperty(ProductionPettyCashTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionPettyCashTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionPettyCashType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionPettyCashType(dr As SafeDataReader) As ROProductionPettyCashType

      Dim r As New ROProductionPettyCashType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionPettyCashTypeIDProperty, .GetInt32(0))
        LoadProperty(ProductionPettyCashTypeProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace