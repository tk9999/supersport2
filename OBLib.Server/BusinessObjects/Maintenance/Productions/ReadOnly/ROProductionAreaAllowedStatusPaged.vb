﻿' Generated 26 Jun 2014 17:55 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Areas.ReadOnly

  <Serializable()> _
  Public Class ROProductionAreaAllowedStatusPaged
    Inherits SingularReadOnlyBase(Of ROProductionAreaAllowedStatusPaged)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AllowedStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AllowedStatusID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AllowedStatusID() As Integer
      Get
        Return GetProperty(AllowedStatusIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "Production Area Status", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared OrderNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNo, "Order No", 1)
    ''' <summary>
    ''' Gets the Order No value
    ''' </summary>
    <Display(Name:="Order No", Description:="")>
    Public ReadOnly Property OrderNo() As Integer
      Get
        Return GetProperty(OrderNoProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Status", "")
    ''' <summary>
    ''' Gets and sets the Order No value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
    End Property

    Public Shared BackColorStyleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BackColorStyle, "Back Color Style", "")
    ''' <summary>
    ''' Gets the Back Color Style value
    ''' </summary>
    <Display(Name:="Back Color Style", Description:="")>
    Public ReadOnly Property BackColorStyle() As String
      Get
        Return GetProperty(BackColorStyleProperty)
      End Get
    End Property

    Public Shared ForeColorStyleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ForeColorStyle, "Fore Color Style", "")
    ''' <summary>
    ''' Gets the Fore Color Style value
    ''' </summary>
    <Display(Name:="Fore Color Style", Description:="")>
    Public ReadOnly Property ForeColorStyle() As String
      Get
        Return GetProperty(ForeColorStyleProperty)
      End Get
    End Property

    Public Shared CssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CssClass, "Css Class", "")
    ''' <summary>
    ''' Gets the Css Class value
    ''' </summary>
    <Display(Name:="Css Class", Description:="")>
    Public ReadOnly Property CssClass() As String
      Get
        Return GetProperty(CssClassProperty)
      End Get
    End Property


#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AllowedStatusIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionAreaAllowedStatusPaged(dr As SafeDataReader) As ROProductionAreaAllowedStatusPaged

      Dim r As New ROProductionAreaAllowedStatusPaged()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AllowedStatusIDProperty, .GetInt32(0))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(OrderNoProperty, .GetInt32(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(ProductionAreaStatusProperty, .GetString(9))
        LoadProperty(BackColorStyleProperty, .GetString(10))
        LoadProperty(ForeColorStyleProperty, .GetString(11))
        LoadProperty(CssClassProperty, .GetString(12))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace