﻿' Generated 14 Apr 2015 20:19 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.ReadOnly

  <Serializable()> _
  Public Class ROEventTypePaged
    Inherits OBReadOnlyBase(Of ROEventTypePaged)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property EventTypeID() As Integer
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "Event Type", "")
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Series", Description:="Description for the Event Type")>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Genre", Description:="The parent production type for this event type")>
    Public ReadOnly Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 0)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ImportedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ImportedInd, "Imported", False)
    ''' <summary>
    ''' Gets the Imported value
    ''' </summary>
    <Display(Name:="Imported", Description:="")>
    Public ReadOnly Property ImportedInd() As Boolean
      Get
        Return GetProperty(ImportedIndProperty)
      End Get
    End Property

    Public Shared OBIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBInd, "OB", False)
    ''' <summary>
    ''' Gets the OB value
    ''' </summary>
    <Display(Name:="OB", Description:="")>
    Public ReadOnly Property OBInd() As Boolean
      Get
        Return GetProperty(OBIndProperty)
      End Get
    End Property

    Public Shared StudioIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.StudioInd, "Studio", True)
    ''' <summary>
    ''' Gets the Studio value
    ''' </summary>
    <Display(Name:="Studio", Description:="")>
    Public ReadOnly Property StudioInd() As Boolean
      Get
        Return GetProperty(StudioIndProperty)
      End Get
    End Property

    Public Shared PlayoutIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PlayoutInd, "Playout", False)
    ''' <summary>
    ''' Gets the Playout value
    ''' </summary>
    <Display(Name:="Playout", Description:="")>
    Public ReadOnly Property PlayoutInd() As Boolean
      Get
        Return GetProperty(PlayoutIndProperty)
      End Get
    End Property

    Public Shared HighlightsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HighlightsInd, "Highlights", False)
    ''' <summary>
    ''' Gets the Highlights value
    ''' </summary>
    <Display(Name:="Highlights", Description:="")>
    Public ReadOnly Property HighlightsInd() As Boolean
      Get
        Return GetProperty(HighlightsIndProperty)
      End Get
    End Property

    Public Shared MagazineShowIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MagazineShowInd, "Magazine Show", False)
    ''' <summary>
    ''' Gets the Magazine Show value
    ''' </summary>
    <Display(Name:="Magazine Show", Description:="")>
    Public ReadOnly Property MagazineShowInd() As Boolean
      Get
        Return GetProperty(MagazineShowIndProperty)
      End Get
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Production Type", "")
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="Description for the Production Type")>
    Public ReadOnly Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
    End Property

    Public Shared ShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortName, "Short Name", "")
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Short Name", Description:="Brief Description for the Event Type, to be used in Smses")>
    Public ReadOnly Property ShortName() As String
      Get
        Return GetProperty(ShortNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EventTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EventType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEventTypePaged(dr As SafeDataReader) As ROEventTypePaged

      Dim r As New ROEventTypePaged()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EventTypeIDProperty, .GetInt32(0))
        LoadProperty(EventTypeProperty, .GetString(1))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(SystemIDProperty, .GetInt32(7))
        LoadProperty(ImportedIndProperty, .GetBoolean(8))
        LoadProperty(OBIndProperty, .GetBoolean(9))
        LoadProperty(StudioIndProperty, .GetBoolean(10))
        LoadProperty(PlayoutIndProperty, .GetBoolean(11))
        LoadProperty(HighlightsIndProperty, .GetBoolean(12))
        LoadProperty(MagazineShowIndProperty, .GetBoolean(13))
        LoadProperty(ProductionTypeProperty, .GetString(14))
        'RowNo
        LoadProperty(ShortNameProperty, .GetString(16))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace