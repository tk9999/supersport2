﻿' Generated 25 Jul 2014 19:18 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROProductionVenueOutsourceServiceTypeDetailList
    Inherits SingularReadOnlyListBase(Of ROProductionVenueOutsourceServiceTypeDetailList, ROProductionVenueOutsourceServiceTypeDetail)

#Region " Parent "

    <NotUndoable()> Private mParent As ROProductionVenueOutsourceServiceType
#End Region

#Region " Business Methods "

    Public Function GetItem(ProductionVenueOutsourceServiceTypeDetailID As Integer) As ROProductionVenueOutsourceServiceTypeDetail

      For Each child As ROProductionVenueOutsourceServiceTypeDetail In Me
        If child.ProductionVenueOutsourceServiceTypeDetailID = ProductionVenueOutsourceServiceTypeDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Venue Outsource Service Type Details"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROProductionVenueOutsourceServiceTypeDetailList() As ROProductionVenueOutsourceServiceTypeDetailList

      Return New ROProductionVenueOutsourceServiceTypeDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace