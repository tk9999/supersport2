﻿' Generated 05 Jul 2014 09:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROPositionType
    Inherits SingularReadOnlyBase(Of ROPositionType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property PositionTypeID() As Integer
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
    End Property

    Public Shared PositionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionType, "Position Type", "")
    ''' <summary>
    ''' Gets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
    Public ReadOnly Property PositionType() As String
      Get
        Return GetProperty(PositionTypeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    'Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ' ''' <summary>
    ' ''' Gets the Discipline value
    ' ''' </summary>
    '<Display(Name:="Discipline", Description:="")>
    'Public ReadOnly Property DisciplineID() As Integer?
    '  Get
    '    Return GetProperty(DisciplineIDProperty)
    '  End Get
    'End Property

    'Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ' ''' <summary>
    ' ''' Gets the Production Type value
    ' ''' </summary>
    '<Display(Name:="Production Type", Description:="")>
    'Public ReadOnly Property ProductionTypeID() As Integer?
    '  Get
    '    Return GetProperty(ProductionTypeIDProperty)
    '  End Get
    'End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PositionTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.PositionType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROPositionType(dr As SafeDataReader) As ROPositionType

      Dim r As New ROPositionType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(PositionTypeIDProperty, .GetInt32(0))
        LoadProperty(PositionTypeProperty, .GetString(1))
        LoadProperty(CreatedByProperty, .GetInt32(2))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
        LoadProperty(ModifiedByProperty, .GetInt32(4))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        'LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        'LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace