﻿' Generated 20 Apr 2016 12:08 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.ReadOnly

  <Serializable()> _
  Public Class ROEventTypeListReport
    Inherits OBReadOnlyListBase(Of ROEventTypeListReport, ROEventTypeReport)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(EventTypeID As Integer) As ROEventTypeReport

      For Each child As ROEventTypeReport In Me
        If child.EventTypeID = EventTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      <Display(Name:="Keyword"), SetExpression("ROEventTypesCriteriaBO.SetEventTypes(self)", , 250), TextField()>
      Public Property KeyWord As String = ""

      Public Property ProductionTypeID As Integer?

      Public Sub New()


      End Sub

      Public Sub New(KeyWord As String)
        Me.KeyWord = KeyWord
      End Sub

    End Class

    Public Shared Function NewROEventTypeListReport() As ROEventTypeListReport

      Return New ROEventTypeListReport()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROEventTypeListReport() As ROEventTypeListReport

      Return DataPortal.Fetch(Of ROEventTypeListReport)(New Criteria())

    End Function

    Public Shared Function GetROEventTypeListReport(KeyWord As String) As ROEventTypeListReport

      Return DataPortal.Fetch(Of ROEventTypeListReport)(New Criteria(KeyWord))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEventTypeReport.GetROEventTypeReport(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEventTypeListReport"
            cm.Parameters.AddWithValue("@KeyWord", Singular.Strings.MakeEmptyDBNull(crit.KeyWord))
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region


  End Class
End Namespace