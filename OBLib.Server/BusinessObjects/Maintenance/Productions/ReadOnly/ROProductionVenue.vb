﻿' Generated 12 Feb 2014 18:16 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.ReadOnly

  <Serializable()> _
  Public Class ROProductionVenue
    Inherits OBReadOnlyBase(Of ROProductionVenue)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Production Venue")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="")>
    Public ReadOnly Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CityID, "CityID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="CityID")>
    Public ReadOnly Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CountryID, "CountryID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="CountryID")>
    Public ReadOnly Property CountryID() As Integer?
      Get
        Return GetProperty(CountryIDProperty)
      End Get
    End Property

    <ClientOnly()>
    Public Property VisibleClient As Boolean = True

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionVenue

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionVenue(dr As SafeDataReader) As ROProductionVenue

      Dim r As New ROProductionVenue()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionVenueIDProperty, .GetInt32(0))
        LoadProperty(ProductionVenueProperty, .GetString(1))
        LoadProperty(CityProperty, .GetString(2))
        LoadProperty(CityIDProperty, .GetInt32(3))
        LoadProperty(CountryIDProperty, .GetInt32(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace