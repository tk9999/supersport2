﻿' Generated 26 Jun 2014 17:55 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Areas.ReadOnly

  <Serializable()> _
  Public Class ROProductionAreaAllowedStatusListPaged
    Inherits SingularReadOnlyListBase(Of ROProductionAreaAllowedStatusListPaged, ROProductionAreaAllowedStatusPaged)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(AllowedStatusID As Integer) As ROProductionAreaAllowedStatusPaged

      For Each child As ROProductionAreaAllowedStatusPaged In Me
        If child.AllowedStatusID = AllowedStatusID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItem(StatusID As Integer, SystemID As Integer, ProductionAreaID As Integer) As ROProductionAreaAllowedStatusPaged

      Return Me.Where(Function(d) d.ProductionAreaStatusID = StatusID And d.SystemID = SystemID And d.ProductionAreaID = ProductionAreaID).FirstOrDefault

    End Function

    Public Overrides Function ToString() As String

      Return "Production Area Allowed Statuses"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property ProductionAreaID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing

      Public Sub New(ProductionAreaID As Integer?, SystemID As Integer?)
        Me.ProductionAreaID = ProductionAreaID
        Me.SystemID = SystemID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionAreaAllowedStatusListPaged() As ROProductionAreaAllowedStatusListPaged

      Return New ROProductionAreaAllowedStatusListPaged()

    End Function

    Public Shared Sub BeginGetROProductionAreaAllowedStatusListPaged(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionAreaAllowedStatusListPaged)))

      Dim dp As New DataPortal(Of ROProductionAreaAllowedStatusListPaged)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionAreaAllowedStatusListPaged(CallBack As EventHandler(Of DataPortalResult(Of ROProductionAreaAllowedStatusListPaged)))

      Dim dp As New DataPortal(Of ROProductionAreaAllowedStatusListPaged)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionAreaAllowedStatusListPaged() As ROProductionAreaAllowedStatusListPaged

      Return DataPortal.Fetch(Of ROProductionAreaAllowedStatusListPaged)(New Criteria())

    End Function

    Public Shared Function GetROProductionAreaAllowedStatusListPaged(ProductionAreaID As Integer?, SystemID As Integer?) As ROProductionAreaAllowedStatusListPaged

      Return DataPortal.Fetch(Of ROProductionAreaAllowedStatusListPaged)(New Criteria(ProductionAreaID, SystemID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionAreaAllowedStatusPaged.GetROProductionAreaAllowedStatusPaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionAreaAllowedStatusListPaged"
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace