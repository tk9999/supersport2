﻿' Generated 26 Jan 2015 06:16 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.ReadOnly

  <Serializable()> _
  Public Class ROPlanningLevelType
    Inherits SingularReadOnlyBase(Of ROPlanningLevelType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PlanningLevelTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PlanningLevelTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property PlanningLevelTypeID() As Integer
      Get
        Return GetProperty(PlanningLevelTypeIDProperty)
      End Get
    End Property

    Public Shared PlanningLevelTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PlanningLevelType, "Planning Level Type", "")
    ''' <summary>
    ''' Gets the Planning Level Type value
    ''' </summary>
    <Display(Name:="Planning Level Type", Description:="")>
  Public ReadOnly Property PlanningLevelType() As String
      Get
        Return GetProperty(PlanningLevelTypeProperty)
      End Get
    End Property

    Public Shared DefaultHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.DefaultHours, "Default Hours", CDec(0))
    ''' <summary>
    ''' Gets the Default Hours value
    ''' </summary>
    <Display(Name:="Default Hours", Description:="")>
  Public ReadOnly Property DefaultHours() As Decimal
      Get
        Return GetProperty(DefaultHoursProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PlanningLevelTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.PlanningLevelType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROPlanningLevelType(dr As SafeDataReader) As ROPlanningLevelType

      Dim r As New ROPlanningLevelType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(PlanningLevelTypeIDProperty, .GetInt32(0))
        LoadProperty(PlanningLevelTypeProperty, .GetString(1))
        LoadProperty(DefaultHoursProperty, .GetDecimal(2))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace