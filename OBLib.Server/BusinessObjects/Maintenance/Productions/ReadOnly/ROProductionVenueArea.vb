﻿' Generated 25 Jul 2014 19:18 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROProductionVenueArea
    Inherits SingularReadOnlyBase(Of ROProductionVenueArea)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVenueAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueAreaID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionVenueAreaID() As Integer
      Get
        Return GetProperty(ProductionVenueAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue", Nothing)
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenueArea, "Production Venue Area", "")
    ''' <summary>
    ''' Gets the Production Venue Area value
    ''' </summary>
    <Display(Name:="Production Venue Area", Description:="")>
    Public ReadOnly Property ProductionVenueArea() As String
      Get
        Return GetProperty(ProductionVenueAreaProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionVenueArea

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionVenueArea(dr As SafeDataReader) As ROProductionVenueArea

      Dim r As New ROProductionVenueArea()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionVenueAreaIDProperty, .GetInt32(0))
        LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionVenueAreaProperty, .GetString(2))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace