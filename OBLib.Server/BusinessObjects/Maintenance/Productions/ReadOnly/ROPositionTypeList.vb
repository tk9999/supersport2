﻿' Generated 05 Jul 2014 09:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROPositionTypeList
    Inherits SingularReadOnlyListBase(Of ROPositionTypeList, ROPositionType)

#Region " Business Methods "

    Public Function GetItem(PositionTypeID As Integer) As ROPositionType

      For Each child As ROPositionType In Me
        If child.PositionTypeID = PositionTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Position Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionTypeID As Integer? = Nothing

      <Display(Name:="PositionType", Description:=""), PrimarySearchField>
      Public Property PositionType() As String = ""

      Public Sub New(ProductionTypeID As Integer?)
        Me.ProductionTypeID = ProductionTypeID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROPositionTypeList() As ROPositionTypeList

      Return New ROPositionTypeList()

    End Function

    Public Shared Sub BeginGetROPositionTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROPositionTypeList)))

      Dim dp As New DataPortal(Of ROPositionTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    'Public Shared Sub BeginGetROPositionTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROPositionTypeList)))

    '  Dim dp As New DataPortal(Of ROPositionTypeList)()
    '  AddHandler dp.FetchCompleted, CallBack
    '  dp.BeginFetch(New Criteria())

    'End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROPositionTypeList(ProductionTypeID As Integer?) As ROPositionTypeList

      Return DataPortal.Fetch(Of ROPositionTypeList)(New Criteria(ProductionTypeID))

    End Function

    Public Shared Function GetROPositionTypeList() As ROPositionTypeList

      Return DataPortal.Fetch(Of ROPositionTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPositionType.GetROPositionType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROPositionTypeList"
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            'cm.Parameters.AddWithValue("@PositionTypeID", NothignDBNull(crit.PositionTypeID))
            cm.Parameters.AddWithValue("@PositionType", Singular.Strings.MakeEmptyDBNull(crit.PositionType))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace