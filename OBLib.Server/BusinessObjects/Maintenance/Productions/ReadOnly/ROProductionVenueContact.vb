﻿' Generated 25 Jul 2014 19:18 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROProductionVenueContact
    Inherits SingularReadOnlyBase(Of ROProductionVenueContact)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVenueContactIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueContactID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionVenueContactID() As Integer
      Get
        Return GetProperty(ProductionVenueContactIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue", Nothing)
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
    End Property

    Public Shared ContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactName, "Contact Name", "")
    ''' <summary>
    ''' Gets the Contact Name value
    ''' </summary>
    <Display(Name:="Contact Name", Description:="The name of the contact at the venue")>
    Public ReadOnly Property ContactName() As String
      Get
        Return GetProperty(ContactNameProperty)
      End Get
    End Property

    Public Shared ContactNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactNo, "Contact No", "")
    ''' <summary>
    ''' Gets the Contact No value
    ''' </summary>
    <Display(Name:="Contact No", Description:="The contact number of the contact person")>
    Public ReadOnly Property ContactNo() As String
      Get
        Return GetProperty(ContactNoProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueContactIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ContactName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionVenueContact(dr As SafeDataReader) As ROProductionVenueContact

      Dim r As New ROProductionVenueContact()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionVenueContactIDProperty, .GetInt32(0))
        LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ContactNameProperty, .GetString(2))
        LoadProperty(ContactNoProperty, .GetString(3))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace