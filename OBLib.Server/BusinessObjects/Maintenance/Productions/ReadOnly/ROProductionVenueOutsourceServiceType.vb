﻿' Generated 25 Jul 2014 19:18 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROProductionVenueOutsourceServiceType
    Inherits SingularReadOnlyBase(Of ROProductionVenueOutsourceServiceType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVenueOutsourceServiceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueOutsourceServiceTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionVenueOutsourceServiceTypeID() As Integer
      Get
        Return GetProperty(ProductionVenueOutsourceServiceTypeIDProperty)
      End Get
    End Property

    Public Shared OutsourceServiceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OutsourceServiceTypeID, "Outsource Service Type", Nothing)
    ''' <summary>
    ''' Gets the Outsource Service Type value
    ''' </summary>
    <Display(Name:="Outsource Service Type", Description:="The outsource service type that is required and the venue")>
    Public ReadOnly Property OutsourceServiceTypeID() As Integer?
      Get
        Return GetProperty(OutsourceServiceTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue", Nothing)
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="The Production Type the this outsource service type is for. If NULL then it applies to all for this production venue")>
    Public ReadOnly Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROProductionVenueOutsourceServiceTypeDetailListProperty As PropertyInfo(Of ROProductionVenueOutsourceServiceTypeDetailList) = RegisterProperty(Of ROProductionVenueOutsourceServiceTypeDetailList)(Function(c) c.ROProductionVenueOutsourceServiceTypeDetailList, "RO Production Venue Outsource Service Type Detail List")

    Public ReadOnly Property ROProductionVenueOutsourceServiceTypeDetailList() As ROProductionVenueOutsourceServiceTypeDetailList
      Get
        If GetProperty(ROProductionVenueOutsourceServiceTypeDetailListProperty) Is Nothing Then
          LoadProperty(ROProductionVenueOutsourceServiceTypeDetailListProperty, Maintenance.General.ReadOnly.ROProductionVenueOutsourceServiceTypeDetailList.NewROProductionVenueOutsourceServiceTypeDetailList())
        End If
        Return GetProperty(ROProductionVenueOutsourceServiceTypeDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueOutsourceServiceTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionVenueOutsourceServiceType(dr As SafeDataReader) As ROProductionVenueOutsourceServiceType

      Dim r As New ROProductionVenueOutsourceServiceType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionVenueOutsourceServiceTypeIDProperty, .GetInt32(0))
        LoadProperty(OutsourceServiceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace