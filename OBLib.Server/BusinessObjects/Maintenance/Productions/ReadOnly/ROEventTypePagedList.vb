﻿' Generated 14 Apr 2015 20:19 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.ReadOnly

  <Serializable()> _
  Public Class ROEventTypePagedList
    Inherits OBReadOnlyListBase(Of ROEventTypePagedList, ROEventTypePaged)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(EventTypeID As Integer) As ROEventTypePaged

      For Each child As ROEventTypePaged In Me
        If child.EventTypeID = EventTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Event Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits OBPagedCriteriaBase(Of Criteria)

      Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionTypeID, Nothing)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Production Type", Description:="")>
      Public Property ProductionTypeID() As Integer?
        Get
          Return ReadProperty(ProductionTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionTypeIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionType, "") _
                                                                        .AddSetExpression("ROEventTypePagedListCriteriaBO.ProductionTypeSet(self)", False, "250")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Genre"), TextField>
      Public Property ProductionType() As String
        Get
          Return ReadProperty(ProductionTypeProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionTypeProperty, Value)
        End Set
      End Property

      Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.EventType, "") _
                                                                   .AddSetExpression("ROEventTypePagedListCriteriaBO.EventTypeSet(self)", False, "250")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Series"), TextField>
      Public Property EventType() As String
        Get
          Return ReadProperty(EventTypeProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(EventTypeProperty, Value)
        End Set
      End Property

      Public Sub New(EventType As String)
        Me.EventType = EventType
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROEventTypePagedList() As ROEventTypePagedList

      Return New ROEventTypePagedList()

    End Function

    Public Shared Sub BeginGetROEventTypePagedList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROEventTypePagedList)))

      Dim dp As New DataPortal(Of ROEventTypePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    Public Shared Sub BeginGetROEventTypePagedList(CallBack As EventHandler(Of DataPortalResult(Of ROEventTypePagedList)))

      Dim dp As New DataPortal(Of ROEventTypePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROEventTypePagedList() As ROEventTypePagedList

      Return DataPortal.Fetch(Of ROEventTypePagedList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEventTypePaged.GetROEventTypePaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEventTypePagedList"
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@EventType", Strings.MakeEmptyDBNull(crit.EventType))
            cm.Parameters.AddWithValue("@ProductionType", Strings.MakeEmptyDBNull(crit.ProductionType))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace