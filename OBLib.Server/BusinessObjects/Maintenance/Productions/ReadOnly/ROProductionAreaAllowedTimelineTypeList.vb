﻿' Generated 26 Jun 2014 18:46 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Areas.ReadOnly

  <Serializable()> _
  Public Class ROProductionAreaAllowedTimelineTypeList
    Inherits SingularReadOnlyListBase(Of ROProductionAreaAllowedTimelineTypeList, ROProductionAreaAllowedTimelineType)

#Region " Business Methods "

    Public Function GetItem(AllowedTimelineTypeID As Integer) As ROProductionAreaAllowedTimelineType

      For Each child As ROProductionAreaAllowedTimelineType In Me
        If child.AllowedTimelineTypeID = AllowedTimelineTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Area Allowed Timeline Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionAreaID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing

      Public Sub New(ProductionAreaID As Integer?, SystemID As Integer?)
        Me.ProductionAreaID = ProductionAreaID
        Me.SystemID = SystemID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionAreaAllowedTimelineTypeList() As ROProductionAreaAllowedTimelineTypeList

      Return New ROProductionAreaAllowedTimelineTypeList()

    End Function

    Public Shared Sub BeginGetROProductionAreaAllowedTimelineTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionAreaAllowedTimelineTypeList)))

      Dim dp As New DataPortal(Of ROProductionAreaAllowedTimelineTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionAreaAllowedTimelineTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionAreaAllowedTimelineTypeList)))

      Dim dp As New DataPortal(Of ROProductionAreaAllowedTimelineTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionAreaAllowedTimelineTypeList() As ROProductionAreaAllowedTimelineTypeList

      Return DataPortal.Fetch(Of ROProductionAreaAllowedTimelineTypeList)(New Criteria())

    End Function

    Public Shared Function GetROProductionAreaAllowedTimelineTypeList(ProductionAreaID As Integer?, SystemID As Integer?) As ROProductionAreaAllowedTimelineTypeList

      Return DataPortal.Fetch(Of ROProductionAreaAllowedTimelineTypeList)(New Criteria(ProductionAreaID, SystemID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionAreaAllowedTimelineType.GetROProductionAreaAllowedTimelineType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionAreaAllowedTimelineTypeList"
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace