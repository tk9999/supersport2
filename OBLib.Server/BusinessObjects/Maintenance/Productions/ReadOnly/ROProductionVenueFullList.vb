﻿' Generated 25 Jul 2014 19:18 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROProductionVenueFullList
    Inherits SingularReadOnlyListBase(Of ROProductionVenueFullList, ROProductionVenueFull)

#Region " Business Methods "

    Public Function GetItem(ProductionVenueID As Integer) As ROProductionVenueFull

      For Each child As ROProductionVenueFull In Me
        If child.ProductionVenueID = ProductionVenueID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Venues"

    End Function

    Public Function GetROProductionVenueProductionType(ProductionVenueProductionTypeID As Integer) As ROProductionVenueProductionType

      Dim obj As ROProductionVenueProductionType = Nothing
      For Each parent As ROProductionVenueFull In Me
        obj = parent.ROProductionVenueProductionTypeList.GetItem(ProductionVenueProductionTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROProductionVenueOutsourceServiceType(ProductionVenueOutsourceServiceTypeID As Integer) As ROProductionVenueOutsourceServiceType

      Dim obj As ROProductionVenueOutsourceServiceType = Nothing
      For Each parent As ROProductionVenueFull In Me
        obj = parent.ROProductionVenueOutsourceServiceTypeList.GetItem(ProductionVenueOutsourceServiceTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROProductionVenueContact(ProductionVenueContactID As Integer) As ROProductionVenueContact

      Dim obj As ROProductionVenueContact = Nothing
      For Each parent As ROProductionVenueFull In Me
        obj = parent.ROProductionVenueContactList.GetItem(ProductionVenueContactID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROProductionVenueArea(ProductionVenueAreaID As Integer) As ROProductionVenueArea

      Dim obj As ROProductionVenueArea = Nothing
      For Each parent As ROProductionVenueFull In Me
        obj = parent.ROProductionVenueAreaList.GetItem(ProductionVenueAreaID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionTypeID As Integer?

      Public Sub New(ProductionTypeID As Integer?)
        Me.ProductionTypeID = ProductionTypeID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionVenueFullList() As ROProductionVenueFullList

      Return New ROProductionVenueFullList()

    End Function

    Public Shared Sub BeginGetROProductionVenueFullList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionVenueFullList)))

      Dim dp As New DataPortal(Of ROProductionVenueFullList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionVenueFullList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionVenueFullList)))

      Dim dp As New DataPortal(Of ROProductionVenueFullList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionVenueFullList() As ROProductionVenueFullList

      Return DataPortal.Fetch(Of ROProductionVenueFullList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionVenueFull.GetROProductionVenueFull(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROProductionVenueFull = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionVenueID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROProductionVenueProductionTypeList.RaiseListChangedEvents = False
          parent.ROProductionVenueProductionTypeList.Add(ROProductionVenueProductionType.GetROProductionVenueProductionType(sdr))
          parent.ROProductionVenueProductionTypeList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionVenueID <> sdr.GetInt32(2) Then
            parent = Me.GetItem(sdr.GetInt32(2))
          End If
          parent.ROProductionVenueOutsourceServiceTypeList.RaiseListChangedEvents = False
          parent.ROProductionVenueOutsourceServiceTypeList.Add(ROProductionVenueOutsourceServiceType.GetROProductionVenueOutsourceServiceType(sdr))
          parent.ROProductionVenueOutsourceServiceTypeList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As ROProductionVenueOutsourceServiceType = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.ProductionVenueOutsourceServiceTypeID <> sdr.GetInt32(1) Then
            parentChild = Me.GetROProductionVenueOutsourceServiceType(sdr.GetInt32(1))
          End If
          parentChild.ROProductionVenueOutsourceServiceTypeDetailList.RaiseListChangedEvents = False
          parentChild.ROProductionVenueOutsourceServiceTypeDetailList.Add(ROProductionVenueOutsourceServiceTypeDetail.GetROProductionVenueOutsourceServiceTypeDetail(sdr))
          parentChild.ROProductionVenueOutsourceServiceTypeDetailList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionVenueID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROProductionVenueContactList.RaiseListChangedEvents = False
          parent.ROProductionVenueContactList.Add(ROProductionVenueContact.GetROProductionVenueContact(sdr))
          parent.ROProductionVenueContactList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionVenueID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROProductionVenueAreaList.RaiseListChangedEvents = False
          parent.ROProductionVenueAreaList.Add(ROProductionVenueArea.GetROProductionVenueArea(sdr))
          parent.ROProductionVenueAreaList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionVenueFullList"
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace