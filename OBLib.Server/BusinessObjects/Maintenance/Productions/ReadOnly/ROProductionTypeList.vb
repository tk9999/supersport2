﻿' Generated 19 Feb 2014 17:18 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.Productions.ReadOnly

  <Serializable()> _
  Public Class ROProductionTypeList
    Inherits OBReadOnlyListBase(Of ROProductionTypeList, ROProductionType)

#Region " Business Methods "

    Public Function GetItem(ProductionTypeID As Integer) As ROProductionType

      For Each child As ROProductionType In Me
        If child.ProductionTypeID = ProductionTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Types"

    End Function

    Public Function GetROEventType(EventTypeID As Integer) As ROEventType

      Dim obj As ROEventType = Nothing
      For Each parent As ROProductionType In Me
        obj = parent.ROEventTypeList.GetItem(EventTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property ProductionType As String

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionTypeList() As ROProductionTypeList

      Return New ROProductionTypeList()

    End Function

    Public Shared Sub BeginGetROProductionTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionTypeList)))

      Dim dp As New DataPortal(Of ROProductionTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionTypeList)))

      Dim dp As New DataPortal(Of ROProductionTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionTypeList() As ROProductionTypeList

      Return DataPortal.Fetch(Of ROProductionTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionType.GetROProductionType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROProductionType = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionTypeID <> sdr.GetInt32(2) Then
            parent = Me.GetItem(sdr.GetInt32(2))
          End If
          parent.ROEventTypeList.RaiseListChangedEvents = False
          parent.ROEventTypeList.Add(ROEventType.GetROEventType(sdr))
          parent.ROEventTypeList.RaiseListChangedEvents = True
        End While
      End If

      'Dim parentChild As ROEventType = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentChild Is Nothing OrElse parentChild.EventTypeID <> sdr.GetInt32(1) Then
      '      parentChild = Me.GetROEventType(sdr.GetInt32(1))
      '    End If
      '    parentChild.ROEventTypeSystemList.RaiseListChangedEvents = False
      '    parentChild.ROEventTypeSystemList.Add(ROEventTypeSystem.GetROEventTypeSystem(sdr))
      '    parentChild.ROEventTypeSystemList.RaiseListChangedEvents = True
      '  End While
      'End If


    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionTypeList"
            cm.Parameters.AddWithValue("@ProductionType", Singular.Strings.MakeEmptyDBNull(crit.ProductionType))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace