﻿' Generated 26 Jun 2014 18:46 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Areas.ReadOnly

  <Serializable()> _
  Public Class ROProductionAreaAllowedTimelineType
    Inherits SingularReadOnlyBase(Of ROProductionAreaAllowedTimelineType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AllowedTimelineTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AllowedTimelineTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AllowedTimelineTypeID() As Integer
      Get
        Return GetProperty(AllowedTimelineTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionTimelineTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTimelineTypeID, "Production Timeline Type", Nothing)
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Production Timeline Type", Description:="")>
    Public ReadOnly Property ProductionTimelineTypeID() As Integer?
      Get
        Return GetProperty(ProductionTimelineTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionTimelineTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTimelineType, "Production Timeline Type", "")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Production Timeline Type", Description:="Description for the Production Timeline Type")>
    Public ReadOnly Property ProductionTimelineType() As String
      Get
        Return GetProperty(ProductionTimelineTypeProperty)
      End Get
    End Property

    Public Shared FreeIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FreeInd, "Free", False)
    ''' <summary>
    ''' Gets the Free value
    ''' </summary>
    <Display(Name:="Free", Description:="")>
    Public ReadOnly Property FreeInd() As Boolean
      Get
        Return GetProperty(FreeIndProperty)
      End Get
    End Property

    Public Shared OrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Order, "Order", 0)
    ''' <summary>
    ''' Gets the Order value
    ''' </summary>
    <Display(Name:="Order", Description:="")>
    Public ReadOnly Property Order() As Integer
      Get
        Return GetProperty(OrderProperty)
      End Get
    End Property

    Public Shared PreTransmissionIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.PreTransmissionInd, "Pre Transmission", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets the Pre Transmission value
    ''' </summary>
    <Display(Name:="Pre Transmission", Description:="")>
    Public ReadOnly Property PreTransmissionInd() As Boolean?
      Get
        Return GetProperty(PreTransmissionIndProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AllowedTimelineTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionAreaAllowedTimelineType(dr As SafeDataReader) As ROProductionAreaAllowedTimelineType

      Dim r As New ROProductionAreaAllowedTimelineType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AllowedTimelineTypeIDProperty, .GetInt32(0))
        LoadProperty(ProductionTimelineTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ProductionTimelineTypeProperty, .GetString(8))
        LoadProperty(FreeIndProperty, .GetBoolean(9))
        LoadProperty(OrderProperty, .GetInt32(10))
        LoadProperty(PreTransmissionIndProperty, .GetBoolean(11))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace