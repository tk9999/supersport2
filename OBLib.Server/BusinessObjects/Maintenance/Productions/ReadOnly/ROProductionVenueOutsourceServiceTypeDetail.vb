﻿' Generated 25 Jul 2014 19:18 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROProductionVenueOutsourceServiceTypeDetail
    Inherits SingularReadOnlyBase(Of ROProductionVenueOutsourceServiceTypeDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVenueOutsourceServiceTypeDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueOutsourceServiceTypeDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionVenueOutsourceServiceTypeDetailID() As Integer
      Get
        Return GetProperty(ProductionVenueOutsourceServiceTypeDetailIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueOutsourceServiceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueOutsourceServiceTypeID, "Production Venue Outsource Service Type", Nothing)
    ''' <summary>
    ''' Gets the Production Venue Outsource Service Type value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionVenueOutsourceServiceTypeID() As Integer?
      Get
        Return GetProperty(ProductionVenueOutsourceServiceTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueOutsourceServiceTypeDetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenueOutsourceServiceTypeDetail, "Production Venue Outsource Service Type Detail", "")
    ''' <summary>
    ''' Gets the Production Venue Outsource Service Type Detail value
    ''' </summary>
    <Display(Name:="Production Venue Outsource Service Type Detail", Description:="Additional details for the outsource service")>
    Public ReadOnly Property ProductionVenueOutsourceServiceTypeDetail() As String
      Get
        Return GetProperty(ProductionVenueOutsourceServiceTypeDetailProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueOutsourceServiceTypeDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionVenueOutsourceServiceTypeDetail

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionVenueOutsourceServiceTypeDetail(dr As SafeDataReader) As ROProductionVenueOutsourceServiceTypeDetail

      Dim r As New ROProductionVenueOutsourceServiceTypeDetail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionVenueOutsourceServiceTypeDetailIDProperty, .GetInt32(0))
        LoadProperty(ProductionVenueOutsourceServiceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionVenueOutsourceServiceTypeDetailProperty, .GetString(2))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace