﻿' Generated 20 Apr 2016 08:44 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.ReadOnly

  <Serializable()> _
  Public Class ROProductionTypeListReport
    Inherits OBReadOnlyListBase(Of ROProductionTypeListReport, ROProductionTypeReport)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionTypeID As Integer) As ROProductionTypeReport

      For Each child As ROProductionTypeReport In Me
        If child.ProductionTypeID = ProductionTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      <Display(Name:="Keyword"), SetExpression("ROProductionTypeCriteriaBO.KeywordSet(self)", , 250), TextField(, , , , )>
      Public Property KeyWord As String = ""

      Public Sub New()



      End Sub

      Public Sub New(KeyWord As String)

        Me.KeyWord = KeyWord

      End Sub

    End Class

    Public Shared Function NewROProductionTypeListReport() As ROProductionTypeListReport

      Return New ROProductionTypeListReport()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROProductionTypeListReport() As ROProductionTypeListReport

      Return DataPortal.Fetch(Of ROProductionTypeListReport)(New Criteria())

    End Function

    Public Shared Function GetROProductionTypeListReport(KeyWord As String) As ROProductionTypeListReport

      Return DataPortal.Fetch(Of ROProductionTypeListReport)(New Criteria(KeyWord))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionTypeReport.GetROProductionTypeReport(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionTypeListReport"
            cm.Parameters.AddWithValue("@KeyWord", Singular.Strings.MakeEmptyDBNull(crit.KeyWord))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class
End Namespace