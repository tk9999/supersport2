﻿' Generated 12 Feb 2015 12:13 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.Productions.ReadOnly

  <Serializable()> _
  Public Class ROProductionVenuePagedList
    Inherits SingularReadOnlyListBase(Of ROProductionVenuePagedList, ROProductionVenuePaged)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionVenueID As Integer) As ROProductionVenuePaged

      For Each child As ROProductionVenuePaged In Me
        If child.ProductionVenueID = ProductionVenueID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Venues"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits OBPagedCriteriaBase(Of Criteria)

      Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionVenue, "") _
                                                                         .AddSetExpression("ROProductionVenuePagedListCriteriaBO.ProductionVenueSet(self)", , 250)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Venue"), TextField>
      Public Property ProductionVenue() As String
        Get
          Return ReadProperty(ProductionVenueProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionVenueProperty, Value)
        End Set
      End Property

      Public Sub New(ProductionVenue As String)
        Me.ProductionVenue = ProductionVenue
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionVenuePagedList() As ROProductionVenuePagedList

      Return New ROProductionVenuePagedList()

    End Function

    Public Shared Sub BeginGetROProductionVenuePagedList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionVenuePagedList)))

      Dim dp As New DataPortal(Of ROProductionVenuePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionVenuePagedList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionVenuePagedList)))

      Dim dp As New DataPortal(Of ROProductionVenuePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionVenuePagedList() As ROProductionVenuePagedList

      Return DataPortal.Fetch(Of ROProductionVenuePagedList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionVenuePaged.GetROProductionVenuePaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionVenuePagedList"
            cm.Parameters.AddWithValue("@ProductionVenue", Singular.Strings.MakeEmptyDBNull(crit.ProductionVenue))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace