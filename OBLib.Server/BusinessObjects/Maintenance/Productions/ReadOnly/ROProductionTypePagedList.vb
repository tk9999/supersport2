﻿' Generated 19 Feb 2014 17:18 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.Productions.ReadOnly.Old

Namespace Maintenance.Productions.ReadOnly

  <Serializable()> _
  Public Class ROProductionTypePagedList
    Inherits SingularReadOnlyListBase(Of ROProductionTypePagedList, ROProductionTypePaged)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

    'Private mTotalPages As Integer = 0
    'Public ReadOnly Property TotalPages As Integer
    '  Get
    '    Return mTotalPages
    '  End Get
    'End Property

#Region " Business Methods "

    Public Function GetItem(ProductionTypeID As Integer) As ROProductionTypePaged

      For Each child As ROProductionTypePaged In Me
        If child.ProductionTypeID = ProductionTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Types"

    End Function

    Public Function GetROEventType(EventTypeID As Integer) As ROEventType

      Dim obj As ROEventType = Nothing
      For Each parent As ROProductionTypePaged In Me
        obj = parent.ROEventTypeList.GetItem(EventTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionType, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Production Type", Description:="")>
      Public Property ProductionType() As String
        Get
          Return ReadProperty(ProductionTypeProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionTypeProperty, Value)
        End Set
      End Property

      Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionTypeID, Nothing)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Production Type", Description:="")>
      Public Property ProductionTypeID() As Integer?
        Get
          Return ReadProperty(ProductionTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionTypeIDProperty, Value)
        End Set
      End Property

      Public Sub New(ProductionType As String)
        Me.ProductionType = ProductionType
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionTypePagedList() As ROProductionTypePagedList

      Return New ROProductionTypePagedList()

    End Function

    Public Shared Sub BeginGetROProductionTypePagedList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionTypePagedList)))

      Dim dp As New DataPortal(Of ROProductionTypePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionTypePagedList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionTypePagedList)))

      Dim dp As New DataPortal(Of ROProductionTypePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionTypePagedList() As ROProductionTypePagedList

      Return DataPortal.Fetch(Of ROProductionTypePagedList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionTypePaged.GetROProductionTypePaged(sdr))
      End While
      Me.IsReadOnly = True

      Me.RaiseListChangedEvents = True

      'Dim parent As ROProductionTypePaged = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.ProductionTypeID <> sdr.GetInt32(2) Then
      '      parent = Me.GetItem(sdr.GetInt32(2))
      '    End If
      '    parent.ROEventTypeList.RaiseListChangedEvents = False
      '    parent.ROEventTypeList.Add(ROEventType.GetROEventType(sdr))
      '    parent.ROEventTypeList.RaiseListChangedEvents = True
      '  End While
      'End If

      'Dim parentChild As ROEventType = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentChild Is Nothing OrElse parentChild.EventTypeID <> sdr.GetInt32(1) Then
      '      parentChild = Me.GetROEventType(sdr.GetInt32(1))
      '    End If
      '    parentChild.ROEventTypeSystemList.RaiseListChangedEvents = False
      '    parentChild.ROEventTypeSystemList.Add(ROEventTypeSystem.GetROEventTypeSystem(sdr))
      '    parentChild.ROEventTypeSystemList.RaiseListChangedEvents = True
      '  End While
      'End If


    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionTypePagedList"
            cm.Parameters.AddWithValue("@ProductionType", Singular.Strings.MakeEmptyDBNull(crit.ProductionType))
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace