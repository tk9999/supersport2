﻿' Generated 26 Jan 2015 20:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class ProductionVenueArea
    Inherits SingularBusinessBase(Of ProductionVenueArea)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVenueAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueAreaID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ProductionVenueAreaID() As Integer
      Get
        Return GetProperty(ProductionVenueAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="")>
  Public Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenueArea, "Production Venue Area", "")
    ''' <summary>
    ''' Gets and sets the Production Venue Area value
    ''' </summary>
    <Display(Name:="Production Venue Area", Description:=""),
    StringLength(100, ErrorMessage:="Production Venue Area cannot be more than 100 characters")>
  Public Property ProductionVenueArea() As String
      Get
        Return GetProperty(ProductionVenueAreaProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionVenueAreaProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionVenue

      Return CType(CType(Me.Parent, ProductionVenueAreaList).Parent, ProductionVenue)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionVenueArea.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Venue Area")
        Else
          Return String.Format("Blank {0}", "Production Venue Area")
        End If
      Else
        Return Me.ProductionVenueArea
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionVenueArea() method.

    End Sub

    Public Shared Function NewProductionVenueArea() As ProductionVenueArea

      Return DataPortal.CreateChild(Of ProductionVenueArea)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionVenueArea(dr As SafeDataReader) As ProductionVenueArea

      Dim p As New ProductionVenueArea()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionVenueAreaIDProperty, .GetInt32(0))
          LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionVenueAreaProperty, .GetString(2))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionVenueArea"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionVenueArea"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionVenueAreaID As SqlParameter = .Parameters.Add("@ProductionVenueAreaID", SqlDbType.Int)
          paramProductionVenueAreaID.Value = GetProperty(ProductionVenueAreaIDProperty)
          If Me.IsNew Then
            paramProductionVenueAreaID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionVenueID", Me.GetParent.ProductionVenueID)
          .Parameters.AddWithValue("@ProductionVenueArea", GetProperty(ProductionVenueAreaProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionVenueAreaIDProperty, paramProductionVenueAreaID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionVenueArea"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionVenueAreaID", GetProperty(ProductionVenueAreaIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace