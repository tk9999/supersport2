﻿' Generated 26 Jun 2014 17:54 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Areas

  <Serializable()> _
  Public Class ProductionAreaStatus
    Inherits SingularBusinessBase(Of ProductionAreaStatus)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaStatusID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionAreaStatusID() As Integer
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Production Area Status", "")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:=""),
    StringLength(50, ErrorMessage:="Production Area Status cannot be more than 50 characters")>
    Public Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaStatusProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionAreaStatusIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionAreaStatus.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Area Status")
        Else
          Return String.Format("Blank {0}", "Production Area Status")
        End If
      Else
        Return Me.ProductionAreaStatus
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionAreaStatus() method.

    End Sub

    Public Shared Function NewProductionAreaStatus() As ProductionAreaStatus

      Return DataPortal.CreateChild(Of ProductionAreaStatus)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionAreaStatus(dr As SafeDataReader) As ProductionAreaStatus

      Dim p As New ProductionAreaStatus()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionAreaStatusIDProperty, .GetInt32(0))
          LoadProperty(ProductionAreaStatusProperty, .GetString(1))
          LoadProperty(CreatedByProperty, .GetInt32(2))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
          LoadProperty(ModifiedByProperty, .GetInt32(4))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionAreaStatus"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionAreaStatus"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionAreaStatusID As SqlParameter = .Parameters.Add("@ProductionAreaStatusID", SqlDbType.Int)
          paramProductionAreaStatusID.Value = GetProperty(ProductionAreaStatusIDProperty)
          If Me.IsNew Then
            paramProductionAreaStatusID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionAreaStatus", GetProperty(ProductionAreaStatusProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionAreaStatusIDProperty, paramProductionAreaStatusID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionAreaStatus"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionAreaStatusID", GetProperty(ProductionAreaStatusIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace