﻿' Generated 26 Jan 2015 20:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class ProductionVenueContact
    Inherits SingularBusinessBase(Of ProductionVenueContact)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVenueContactIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueContactID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionVenueContactID() As Integer
      Get
        Return GetProperty(ProductionVenueContactIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="")>
    Public Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared ContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactName, "Contact Name", "")
    ''' <summary>
    ''' Gets and sets the Contact Name value
    ''' </summary>
    <Display(Name:="Contact Name", Description:="The name of the contact at the venue"),
    Required(ErrorMessage:="Contact Name required", AllowEmptyStrings:=False),
    StringLength(255, ErrorMessage:="Contact Name cannot be more than 255 characters")>
    Public Property ContactName() As String
      Get
        Return GetProperty(ContactNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContactNameProperty, Value)
      End Set
    End Property

    Public Shared ContactNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactNo, "Contact No", "")
    ''' <summary>
    ''' Gets and sets the Contact No value
    ''' </summary>
    <Display(Name:="Contact No", Description:="The contact number of the contact person"),
    Required(ErrorMessage:="Contact No required", AllowEmptyStrings:=False),
    StringLength(20, ErrorMessage:="Contact No cannot be more than 20 characters")>
    Public Property ContactNo() As String
      Get
        Return GetProperty(ContactNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContactNoProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueContactIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ContactName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Venue Contact")
        Else
          Return String.Format("Blank {0}", "Production Venue Contact")
        End If
      Else
        Return Me.ContactName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Public Function GetParent() As ProductionVenue

      Return CType(CType(Me.Parent, ProductionVenueContactList).Parent, ProductionVenue)

    End Function

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionVenueContact() method.

    End Sub

    Public Shared Function NewProductionVenueContact() As ProductionVenueContact

      Return DataPortal.CreateChild(Of ProductionVenueContact)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionVenueContact(dr As SafeDataReader) As ProductionVenueContact

      Dim p As New ProductionVenueContact()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionVenueContactIDProperty, .GetInt32(0))
          LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ContactNameProperty, .GetString(2))
          LoadProperty(ContactNoProperty, .GetString(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionVenueContact"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionVenueContact"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionVenueContactID As SqlParameter = .Parameters.Add("@ProductionVenueContactID", SqlDbType.Int)
          paramProductionVenueContactID.Value = GetProperty(ProductionVenueContactIDProperty)
          If Me.IsNew Then
            paramProductionVenueContactID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionVenueID", Me.GetParent.ProductionVenueID)
          .Parameters.AddWithValue("@ContactName", GetProperty(ContactNameProperty))
          .Parameters.AddWithValue("@ContactNo", GetProperty(ContactNoProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionVenueContactIDProperty, paramProductionVenueContactID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionVenueContact"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionVenueContactID", GetProperty(ProductionVenueContactIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace