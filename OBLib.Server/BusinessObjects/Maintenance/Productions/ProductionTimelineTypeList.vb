﻿' Generated 26 Jun 2014 18:33 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class ProductionTimelineTypeList
    Inherits SingularBusinessListBase(Of ProductionTimelineTypeList, ProductionTimelineType)

#Region " Business Methods "

    Public Function GetItem(ProductionTimelineTypeID As Integer) As ProductionTimelineType

      For Each child As ProductionTimelineType In Me
        If child.ProductionTimelineTypeID = ProductionTimelineTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Timeline Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionTimelineTypeList() As ProductionTimelineTypeList

      Return New ProductionTimelineTypeList()

    End Function

    Public Shared Sub BeginGetProductionTimelineTypeList(CallBack As EventHandler(Of DataPortalResult(Of ProductionTimelineTypeList)))

      Dim dp As New DataPortal(Of ProductionTimelineTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionTimelineTypeList() As ProductionTimelineTypeList

      Return DataPortal.Fetch(Of ProductionTimelineTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionTimelineType.GetProductionTimelineType(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ProductionTimelineType = Nothing
      'Disciplines
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionTimelineTypeID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionTimelineTypeDisciplineList.RaiseListChangedEvents = False
          parent.ProductionTimelineTypeDisciplineList.Add(ProductionTimelineTypeDiscipline.GetProductionTimelineTypeDiscipline(sdr))
          parent.ProductionTimelineTypeDisciplineList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionTimelineTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionTimelineType In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionTimelineType In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace