﻿' Generated 26 Jun 2014 18:33 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class ProductionTimelineType
    Inherits SingularBusinessBase(Of ProductionTimelineType)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared ProductionTimelineTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTimelineTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionTimelineTypeID() As Integer
      Get
        Return GetProperty(ProductionTimelineTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionTimelineTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTimelineType, "Timeline Type", "")
    ''' <summary>
    ''' Gets and sets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Production Timeline Type", Description:="Description for the Production Timeline Type"),
    Required(ErrorMessage:="Production Timeline Type required"),
    StringLength(50, ErrorMessage:="Production Timeline Type cannot be more than 50 characters")>
    Public Property ProductionTimelineType() As String
      Get
        Return GetProperty(ProductionTimelineTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTimelineTypeProperty, Value)
      End Set
    End Property

    Public Shared DefaultTimeRequiredProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DefaultTimeRequired, "Default Time Required", 0)
    ''' <summary>
    ''' Gets and sets the Default Time Required value
    ''' </summary>
    <Display(Name:="Default Time Required", Description:="The default number of minutes this task will take"),
    Browsable(False)>
    Public Property DefaultTimeRequired() As Integer
      Get
        Return GetProperty(DefaultTimeRequiredProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(DefaultTimeRequiredProperty, Value)
      End Set
    End Property

    Public Shared DefaultRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DefaultRequiredInd, "Default Required", True)
    ''' <summary>
    ''' Gets and sets the Default Required value
    ''' </summary>
    <Display(Name:="Default Required", Description:="Tick indicates that this timeline type will always be included when the system automatically calculates timelines"),
    Required(ErrorMessage:="Default Required required"),
    Browsable(False)>
    Public Property DefaultRequiredInd() As Boolean
      Get
        Return GetProperty(DefaultRequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DefaultRequiredIndProperty, Value)
      End Set
    End Property

    Public Shared OrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Order, "Order", 0)
    ''' <summary>
    ''' Gets and sets the Order value
    ''' </summary>
    <Display(Name:="Order", Description:=""),
    Required(ErrorMessage:="Order required")>
    Public Property Order() As Integer
      Get
        Return GetProperty(OrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OrderProperty, Value)
      End Set
    End Property

    Public Shared FreeIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FreeInd, "Free", False)
    ''' <summary>
    ''' Gets and sets the Free value
    ''' </summary>
    <Display(Name:="Free", Description:=""),
    Required(ErrorMessage:="Free required")>
    Public Property FreeInd() As Boolean
      Get
        Return GetProperty(FreeIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(FreeIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PreTransmissionIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.PreTransmissionInd, "Pre Transmission", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Pre Transmission value
    ''' </summary>
    <Display(Name:="Pre Transmission", Description:=""),
    Browsable(False)>
    Public Property PreTransmissionInd() As Boolean?
      Get
        Return GetProperty(PreTransmissionIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(PreTransmissionIndProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Browsable(False)>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionTimelineTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionTimelineType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Timeline Type")
        Else
          Return String.Format("Blank {0}", "Production Timeline Type")
        End If
      Else
        Return Me.ProductionTimelineType
      End If

    End Function

#End Region

#Region " Child Lists "

    Public Shared ProductionTimelineTypeDisciplineListProperty As PropertyInfo(Of ProductionTimelineTypeDisciplineList) = RegisterProperty(Of ProductionTimelineTypeDisciplineList)(Function(c) c.ProductionTimelineTypeDisciplineList, "Production Timeline Type Discipline List")

    Public ReadOnly Property ProductionTimelineTypeDisciplineList() As ProductionTimelineTypeDisciplineList
      Get
        If GetProperty(ProductionTimelineTypeDisciplineListProperty) Is Nothing Then
          LoadProperty(ProductionTimelineTypeDisciplineListProperty, Maintenance.Productions.ProductionTimelineTypeDisciplineList.NewProductionTimelineTypeDisciplineList)
        End If
        Return GetProperty(ProductionTimelineTypeDisciplineListProperty)
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionTimelineType() method.

    End Sub

    Public Shared Function NewProductionTimelineType() As ProductionTimelineType

      Return DataPortal.CreateChild(Of ProductionTimelineType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionTimelineType(dr As SafeDataReader) As ProductionTimelineType

      Dim p As New ProductionTimelineType()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionTimelineTypeIDProperty, .GetInt32(0))
          LoadProperty(ProductionTimelineTypeProperty, .GetString(1))
          LoadProperty(DefaultTimeRequiredProperty, .GetInt32(2))
          LoadProperty(DefaultRequiredIndProperty, .GetBoolean(3))
          LoadProperty(OrderProperty, .GetInt32(4))
          LoadProperty(FreeIndProperty, .GetBoolean(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(PreTransmissionIndProperty, .GetBoolean(10))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionTimelineType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionTimelineType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionTimelineTypeID As SqlParameter = .Parameters.Add("@ProductionTimelineTypeID", SqlDbType.Int)
          paramProductionTimelineTypeID.Value = GetProperty(ProductionTimelineTypeIDProperty)
          If Me.IsNew Then
            paramProductionTimelineTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionTimelineType", GetProperty(ProductionTimelineTypeProperty))
          .Parameters.AddWithValue("@DefaultTimeRequired", GetProperty(DefaultTimeRequiredProperty))
          .Parameters.AddWithValue("@DefaultRequiredInd", GetProperty(DefaultRequiredIndProperty))
          .Parameters.AddWithValue("@Order", GetProperty(OrderProperty))
          .Parameters.AddWithValue("@FreeInd", GetProperty(FreeIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@PreTransmissionInd", Singular.Misc.NothingDBNull(GetProperty(PreTransmissionIndProperty)))
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionTimelineTypeIDProperty, paramProductionTimelineTypeID.Value)
          End If
          ' update child objects
          If GetProperty(ProductionTimelineTypeDisciplineListProperty) IsNot Nothing Then
            Me.ProductionTimelineTypeDisciplineList.Update()
          End If
          MarkOld()
        End With
      Else
        If GetProperty(ProductionTimelineTypeDisciplineListProperty) IsNot Nothing Then
          Me.ProductionTimelineTypeDisciplineList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionTimelineType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionTimelineTypeID", GetProperty(ProductionTimelineTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace