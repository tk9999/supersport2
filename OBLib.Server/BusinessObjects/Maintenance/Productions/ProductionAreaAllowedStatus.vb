﻿' Generated 26 Jun 2014 17:54 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Areas

  <Serializable()> _
  Public Class ProductionAreaAllowedStatus
    Inherits SingularBusinessBase(Of ProductionAreaAllowedStatus)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AllowedStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AllowedStatusID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AllowedStatusID() As Integer
      Get
        Return GetProperty(AllowedStatusIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required(ErrorMessage:="System required"),
    DropDownWeb(GetType(ROSystemList))>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:=""),
    Required(ErrorMessage:="Production Area required"),
    DropDownWeb(GetType(ROProductionAreaList))>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "Production Area Status", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:=""),
    Required(ErrorMessage:="Production Area Status required"),
    DropDownWeb(GetType(ROProductionAreaStatusList))>
    Public Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared OrderNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNo, "Order No", 1)
    ''' <summary>
    ''' Gets and sets the Order No value
    ''' </summary>
    <Display(Name:="Order No", Description:=""),
    Required(ErrorMessage:="Order No required")>
    Public Property OrderNo() As Integer
      Get
        Return GetProperty(OrderNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OrderNoProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Status", "")
    ''' <summary>
    ''' Gets and sets the Order No value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaStatusProperty, Value)
      End Set
    End Property

    Public Shared BackColorStyleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BackColorStyle, "Back Color Style", "")
    ''' <summary>
    ''' Gets and sets the Back Color Style value
    ''' </summary>
    <Display(Name:="Back Color Style", Description:="")>
    Public Property BackColorStyle() As String
      Get
        Return GetProperty(BackColorStyleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BackColorStyleProperty, Value)
      End Set
    End Property

    Public Shared ForeColorStyleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ForeColorStyle, "Fore Color Style", "")
    ''' <summary>
    ''' Gets and sets the Fore Color Style value
    ''' </summary>
    <Display(Name:="Fore Color Style", Description:="")>
    Public Property ForeColorStyle() As String
      Get
        Return GetProperty(ForeColorStyleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ForeColorStyleProperty, Value)
      End Set
    End Property

    Public Shared CssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CssClass, "Css Class", "")
    ''' <summary>
    ''' Gets and sets the Css Class value
    ''' </summary>
    <Display(Name:="Css Class", Description:="")>
    Public Property CssClass() As String
      Get
        Return GetProperty(CssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CssClassProperty, Value)
      End Set
    End Property


#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AllowedStatusIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Area Allowed Status")
        Else
          Return String.Format("Blank {0}", "Production Area Allowed Status")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionAreaAllowedStatus() method.

    End Sub

    Public Shared Function NewProductionAreaAllowedStatus() As ProductionAreaAllowedStatus

      Return DataPortal.CreateChild(Of ProductionAreaAllowedStatus)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionAreaAllowedStatus(dr As SafeDataReader) As ProductionAreaAllowedStatus

      Dim p As New ProductionAreaAllowedStatus()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AllowedStatusIDProperty, .GetInt32(0))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(OrderNoProperty, .GetInt32(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ProductionAreaStatusProperty, .GetString(9))
          LoadProperty(BackColorStyleProperty, .GetString(10))
          LoadProperty(ForeColorStyleProperty, .GetString(11))
          LoadProperty(CssClassProperty, .GetString(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionAreaAllowedStatus"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionAreaAllowedStatus"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAllowedStatusID As SqlParameter = .Parameters.Add("@AllowedStatusID", SqlDbType.Int)
          paramAllowedStatusID.Value = GetProperty(AllowedStatusIDProperty)
          If Me.IsNew Then
            paramAllowedStatusID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@ProductionAreaStatusID", GetProperty(ProductionAreaStatusIDProperty))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@OrderNo", GetProperty(OrderNoProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@BackColorStyle", GetProperty(BackColorStyleProperty))
          .Parameters.AddWithValue("@ForeColorStyle", GetProperty(ForeColorStyleProperty))
          .Parameters.AddWithValue("@CssClass", GetProperty(CssClassProperty))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AllowedStatusIDProperty, paramAllowedStatusID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionAreaAllowedStatus"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AllowedStatusID", GetProperty(AllowedStatusIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace