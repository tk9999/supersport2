﻿' Generated 26 Jun 2014 17:54 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Areas

  <Serializable()> _
  Public Class ProductionAreaAllowedStatusList
    Inherits SingularBusinessListBase(Of ProductionAreaAllowedStatusList, ProductionAreaAllowedStatus)

#Region " Business Methods "

    Public Function GetItem(AllowedStatusID As Integer) As ProductionAreaAllowedStatus

      For Each child As ProductionAreaAllowedStatus In Me
        If child.AllowedStatusID = AllowedStatusID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Area Allowed Statuses"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionAreaID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing

      Public Sub New(ProductionAreaID As Integer?, SystemID As Integer?)
        Me.ProductionAreaID = ProductionAreaID
        Me.SystemID = SystemID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionAreaAllowedStatusList() As ProductionAreaAllowedStatusList

      Return New ProductionAreaAllowedStatusList()

    End Function

    Public Shared Sub BeginGetProductionAreaAllowedStatusList(CallBack As EventHandler(Of DataPortalResult(Of ProductionAreaAllowedStatusList)))

      Dim dp As New DataPortal(Of ProductionAreaAllowedStatusList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionAreaAllowedStatusList() As ProductionAreaAllowedStatusList

      Return DataPortal.Fetch(Of ProductionAreaAllowedStatusList)(New Criteria())

    End Function

    Public Shared Function GetProductionAreaAllowedStatusList(ProductionAreaID As Integer?, SystemID As Integer?) As ProductionAreaAllowedStatusList

      Return DataPortal.Fetch(Of ProductionAreaAllowedStatusList)(New Criteria(ProductionAreaID, SystemID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionAreaAllowedStatus.GetProductionAreaAllowedStatus(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionAreaAllowedStatusList"
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionAreaAllowedStatus In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionAreaAllowedStatus In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace