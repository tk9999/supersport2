﻿' Generated 26 Jan 2015 20:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class ProductionVenueOutsourceServiceTypeDetail
    Inherits SingularBusinessBase(Of ProductionVenueOutsourceServiceTypeDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVenueOutsourceServiceTypeDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueOutsourceServiceTypeDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ProductionVenueOutsourceServiceTypeDetailID() As Integer
      Get
        Return GetProperty(ProductionVenueOutsourceServiceTypeDetailIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueOutsourceServiceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueOutsourceServiceTypeID, "Production Venue Outsource Service Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue Outsource Service Type value
    ''' </summary>
    <Display(Name:="Production Venue Outsource Service Type", Description:="The parent outsource service type")>
  Public Property ProductionVenueOutsourceServiceTypeID() As Integer?
      Get
        Return GetProperty(ProductionVenueOutsourceServiceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueOutsourceServiceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueOutsourceServiceTypeDetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenueOutsourceServiceTypeDetail, "Production Venue Outsource Service Type Detail", "")
    ''' <summary>
    ''' Gets and sets the Production Venue Outsource Service Type Detail value
    ''' </summary>
    <Display(Name:="Production Venue Outsource Service Type Detail", Description:="Additional details for the outsource service"),
    Required(ErrorMessage:="Production Venue Outsource Service Type Detail required"),
    StringLength(100, ErrorMessage:="Production Venue Outsource Service Type Detail cannot be more than 100 characters")>
  Public Property ProductionVenueOutsourceServiceTypeDetail() As String
      Get
        Return GetProperty(ProductionVenueOutsourceServiceTypeDetailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionVenueOutsourceServiceTypeDetailProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionVenueOutsourceServiceType

      Return CType(CType(Me.Parent, ProductionVenueOutsourceServiceTypeDetailList).Parent, ProductionVenueOutsourceServiceType)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueOutsourceServiceTypeDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionVenueOutsourceServiceTypeDetail.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Venue Outsource Service Type Detail")
        Else
          Return String.Format("Blank {0}", "Production Venue Outsource Service Type Detail")
        End If
      Else
        Return Me.ProductionVenueOutsourceServiceTypeDetail
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionVenueOutsourceServiceTypeDetail() method.

    End Sub

    Public Shared Function NewProductionVenueOutsourceServiceTypeDetail() As ProductionVenueOutsourceServiceTypeDetail

      Return DataPortal.CreateChild(Of ProductionVenueOutsourceServiceTypeDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionVenueOutsourceServiceTypeDetail(dr As SafeDataReader) As ProductionVenueOutsourceServiceTypeDetail

      Dim p As New ProductionVenueOutsourceServiceTypeDetail()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionVenueOutsourceServiceTypeDetailIDProperty, .GetInt32(0))
          LoadProperty(ProductionVenueOutsourceServiceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionVenueOutsourceServiceTypeDetailProperty, .GetString(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionVenueOutsourceServiceTypeDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionVenueOutsourceServiceTypeDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionVenueOutsourceServiceTypeDetailID As SqlParameter = .Parameters.Add("@ProductionVenueOutsourceServiceTypeDetailID", SqlDbType.Int)
          paramProductionVenueOutsourceServiceTypeDetailID.Value = GetProperty(ProductionVenueOutsourceServiceTypeDetailIDProperty)
          If Me.IsNew Then
            paramProductionVenueOutsourceServiceTypeDetailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionVenueOutsourceServiceTypeID", Me.GetParent.ProductionVenueOutsourceServiceTypeID)
          .Parameters.AddWithValue("@ProductionVenueOutsourceServiceTypeDetail", GetProperty(ProductionVenueOutsourceServiceTypeDetailProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionVenueOutsourceServiceTypeDetailIDProperty, paramProductionVenueOutsourceServiceTypeDetailID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionVenueOutsourceServiceTypeDetail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionVenueOutsourceServiceTypeDetailID", GetProperty(ProductionVenueOutsourceServiceTypeDetailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace