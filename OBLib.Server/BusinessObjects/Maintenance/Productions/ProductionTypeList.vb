﻿' Generated 19 Oct 2014 22:25 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class ProductionTypeList
    Inherits SingularBusinessListBase(Of ProductionTypeList, ProductionType)
    'Implements Singular.Paging.IPagedList

    'Private mTotalRecords As Integer = 0
    'Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
    '  Get
    '    Return mTotalRecords
    '  End Get
    'End Property

    'Private mTotalPages As Integer = 0
    'Public ReadOnly Property TotalPages As Integer
    '  Get
    '    Return mTotalPages
    '  End Get
    'End Property

#Region " Business Methods "

    Public Function GetItem(ProductionTypeID As Integer) As ProductionType

      For Each child As ProductionType In Me
        If child.ProductionTypeID = ProductionTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits SingularCriteriaBase(Of Criteria)
      'Inherits Paging.PageCriteria(Of Criteria)

      Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionType, "") _
                                                                        .AddSetExpression("FilterProductionTypeList()", False)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Production Type", Description:="")>
      Public Property ProductionType() As String
        Get
          Return ReadProperty(ProductionTypeProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionTypeProperty, Value)
        End Set
      End Property

      Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type ID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production Type ID", Description:="")>
      Public Property ProductionTypeID() As Integer?
        Get
          Return ReadProperty(ProductionTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionTypeIDProperty, Value)
        End Set
      End Property

      Public Sub New(ProductionType As String)
        Me.ProductionType = ProductionType
        'Me.PageNo = PageNo
        'Me.PageSize = PageSize
        'Me.SortAsc = SortAsc
        'Me.SortColumn = SortColumn
        ',
        '             PageNo As Integer,
        '             PageSize As Integer,
        '             SortAsc As Boolean,
        '             SortColumn As String
      End Sub

      Public Sub New(ProductionTypeID As Integer?)
        Me.ProductionTypeID = ProductionTypeID
        'Me.PageNo = 1
        'Me.PageSize = 1
        'Me.SortColumn = "ProductionType"
        'Me.SortAsc = True
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionTypeList() As ProductionTypeList

      Return New ProductionTypeList()

    End Function

    Public Shared Sub BeginGetProductionTypeList(CallBack As EventHandler(Of DataPortalResult(Of ProductionTypeList)))

      Dim dp As New DataPortal(Of ProductionTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionTypeList() As ProductionTypeList

      Return DataPortal.Fetch(Of ProductionTypeList)(New Criteria())

    End Function

    Public Shared Function GetProductionTypeList(ProductionType As String) As ProductionTypeList

      ',
      '                                           PageNo As Integer,
      '                                           PageSize As Integer,
      '                                           SortAsc As Boolean,
      '                                           SortColumn As String

      Return DataPortal.Fetch(Of ProductionTypeList)(New Criteria(ProductionType))
      ', PageNo, PageSize, SortAsc, SortColumn

    End Function

    Public Shared Function GetProductionTypeList(ProductionTypeID As Integer?) As ProductionTypeList

      Return DataPortal.Fetch(Of ProductionTypeList)(New Criteria(ProductionTypeID))

    End Function

    Public Shared Function GetProductionTypeList(Criteria As Criteria) As ProductionTypeList

      Return DataPortal.Fetch(Of ProductionTypeList)(Criteria)

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      'sdr.Read()
      'mTotalRecords = sdr.GetInt32(0)
      ''mTotalPages = sdr.GetInt32(1)
      'sdr.NextResult()

      While sdr.Read
        Me.Add(ProductionType.GetProductionType(sdr))
      End While

      Dim parent As ProductionType = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionTypeID <> sdr.GetInt32(2) Then
            parent = Me.GetItem(sdr.GetInt32(2))
          End If
          parent.EventTypeList.RaiseListChangedEvents = False
          parent.EventTypeList.Add(EventType.GetEventType(sdr))
          parent.EventTypeList.RaiseListChangedEvents = True
        End While
      End If

      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionTypeList"
            cm.Parameters.AddWithValue("@ProductionType", Singular.Strings.MakeEmptyDBNull(crit.ProductionType))
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            'crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionType In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionType In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace