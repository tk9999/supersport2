﻿' Generated 26 Jan 2015 06:14 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class PlanningLevelType
    Inherits SingularBusinessBase(Of PlanningLevelType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PlanningLevelTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PlanningLevelTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property PlanningLevelTypeID() As Integer
      Get
        Return GetProperty(PlanningLevelTypeIDProperty)
      End Get
    End Property

    Public Shared PlanningLevelTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PlanningLevelType, "Planning Level Type", "")
    ''' <summary>
    ''' Gets and sets the Planning Level Type value
    ''' </summary>
    <Display(Name:="Planning Level Type", Description:=""),
    StringLength(50, ErrorMessage:="Planning Level Type cannot be more than 50 characters")>
  Public Property PlanningLevelType() As String
      Get
        Return GetProperty(PlanningLevelTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PlanningLevelTypeProperty, Value)
      End Set
    End Property

    Public Shared DefaultHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.DefaultHours, "Default Hours", CDec(0))
    ''' <summary>
    ''' Gets and sets the Default Hours value
    ''' </summary>
    <Display(Name:="Default Hours", Description:=""),
    Required(ErrorMessage:="Default Hours required")>
  Public Property DefaultHours() As Decimal
      Get
        Return GetProperty(DefaultHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(DefaultHoursProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PlanningLevelTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.PlanningLevelType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Planning Level Type")
        Else
          Return String.Format("Blank {0}", "Planning Level Type")
        End If
      Else
        Return Me.PlanningLevelType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPlanningLevelType() method.

    End Sub

    Public Shared Function NewPlanningLevelType() As PlanningLevelType

      Return DataPortal.CreateChild(Of PlanningLevelType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetPlanningLevelType(dr As SafeDataReader) As PlanningLevelType

      Dim p As New PlanningLevelType()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(PlanningLevelTypeIDProperty, .GetInt32(0))
          LoadProperty(PlanningLevelTypeProperty, .GetString(1))
          LoadProperty(DefaultHoursProperty, .GetDecimal(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insPlanningLevelType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updPlanningLevelType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramPlanningLevelTypeID As SqlParameter = .Parameters.Add("@PlanningLevelTypeID", SqlDbType.Int)
          paramPlanningLevelTypeID.Value = GetProperty(PlanningLevelTypeIDProperty)
          If Me.IsNew Then
            paramPlanningLevelTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@PlanningLevelType", GetProperty(PlanningLevelTypeProperty))
          .Parameters.AddWithValue("@DefaultHours", GetProperty(DefaultHoursProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(PlanningLevelTypeIDProperty, paramPlanningLevelTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delPlanningLevelType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@PlanningLevelTypeID", GetProperty(PlanningLevelTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace