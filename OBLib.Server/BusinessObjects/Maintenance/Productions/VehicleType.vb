﻿' Generated 19 Feb 2015 10:23 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class VehicleType
    Inherits OBBusinessBase(Of VehicleType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VehicleTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property VehicleTypeID() As Integer
      Get
        Return GetProperty(VehicleTypeIDProperty)
      End Get
    End Property

    Public Shared VehicleTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleType, "Vehicle Type", "")
    ''' <summary>
    ''' Gets and sets the Vehicle Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:=""),
    StringLength(50, ErrorMessage:="Vehicle Type cannot be more than 50 characters")>
    Public Property VehicleType() As String
      Get
        Return GetProperty(VehicleTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VehicleTypeProperty, Value)
      End Set
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", False)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required(ErrorMessage:="System required")>
    Public Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SystemIndProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VehicleTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.VehicleType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Vehicle Type")
        Else
          Return String.Format("Blank {0}", "Vehicle Type")
        End If
      Else
        Return Me.VehicleType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewVehicleType() method.

    End Sub

    Public Shared Function NewVehicleType() As VehicleType

      Return DataPortal.CreateChild(Of VehicleType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetVehicleType(dr As SafeDataReader) As VehicleType

      Dim v As New VehicleType()
      v.Fetch(dr)
      Return v

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(VehicleTypeIDProperty, .GetInt32(0))
          LoadProperty(VehicleTypeProperty, .GetString(1))
          LoadProperty(SystemIndProperty, .GetBoolean(2))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insVehicleType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updVehicleType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramVehicleTypeID As SqlParameter = .Parameters.Add("@VehicleTypeID", SqlDbType.Int)
          paramVehicleTypeID.Value = GetProperty(VehicleTypeIDProperty)
          If Me.IsNew Then
            paramVehicleTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@VehicleType", GetProperty(VehicleTypeProperty))
          .Parameters.AddWithValue("@SystemInd", GetProperty(SystemIndProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(VehicleTypeIDProperty, paramVehicleTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delVehicleType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@VehicleTypeID", GetProperty(VehicleTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace