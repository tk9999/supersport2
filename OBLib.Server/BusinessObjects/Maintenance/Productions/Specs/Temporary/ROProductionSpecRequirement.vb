﻿' Generated 11 Jun 2016 12:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.ProductionSpecs.ReadOnly

  <Serializable()> _
  Public Class ROProductionSpecRequirement
    Inherits OBReadOnlyBase(Of ROProductionSpecRequirement)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROProductionSpecRequirementBO.ROProductionSpecRequirementBOToString(self)")

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSpecRequirementID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionSpecRequirementID() As Integer
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionSpecRequirementName, "Production Spec Requirement Name", "")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Production Spec Requirement Name", Description:="Unique name for this spec requirement")>
    Public ReadOnly Property ProductionSpecRequirementName() As String
      Get
        Return GetProperty(ProductionSpecRequirementNameProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="The type of production")>
    Public ReadOnly Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventTypeID, "Event Type", Nothing)
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="The type of event that this production spec is part of (PSL Soccer, Super Rugby)")>
    Public ReadOnly Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date"),
    DateField(FormatString:="dd-MMM-yyyy")>
    Public ReadOnly Property StartDate As Date
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date"),
    DateField(FormatString:="dd-MMM-yyyy")>
    Public ReadOnly Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 0)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area", 0)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared SubDeptProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubDept, "Sub Dept", "")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Sub Dept")>
    Public ReadOnly Property SubDept() As String
      Get
        Return GetProperty(SubDeptProperty)
      End Get
    End Property

    Public Shared AreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Area, "Area", "")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Sub Dept")>
    Public ReadOnly Property Area() As String
      Get
        Return GetProperty(AreaProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room", "")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Room")>
    Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Production Type", "")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Production Type")>
    Public ReadOnly Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "Event Type", "")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Event Type")>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSpecRequirementIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionSpecRequirementName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROProductionSpecRequirement(dr As SafeDataReader) As ROProductionSpecRequirement

      Dim r As New ROProductionSpecRequirement()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionSpecRequirementIDProperty, .GetInt32(0))
        LoadProperty(ProductionSpecRequirementNameProperty, .GetString(1))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(StartDateProperty, .GetValue(4))
        LoadProperty(EndDateProperty, .GetValue(5))
        LoadProperty(SystemIDProperty, .GetInt32(6))
        LoadProperty(ProductionAreaIDProperty, .GetInt32(7))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(SubDeptProperty, .GetString(9))
        LoadProperty(AreaProperty, .GetString(10))
        LoadProperty(RoomProperty, .GetString(11))
        LoadProperty(ProductionTypeProperty, .GetString(12))
        LoadProperty(EventTypeProperty, .GetString(13))
      End With

    End Sub

#End Region

  End Class

End Namespace