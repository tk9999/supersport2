﻿' Generated 11 Jun 2016 12:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.ProductionSpecs.ReadOnly

  <Serializable()> _
  Public Class ROProductionSpecRequirementList
    Inherits OBReadOnlyListBase(Of ROProductionSpecRequirementList, ROProductionSpecRequirement)

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementID As Integer) As ROProductionSpecRequirement

      For Each child As ROProductionSpecRequirement In Me
        If child.ProductionSpecRequirementID = ProductionSpecRequirementID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirements"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property ProductionSpecRequirementName As String
      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?
      Public Property ProductionTypeID As Integer?
      Public Property EventTypeID As Integer?

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROProductionSpecRequirementList() As ROProductionSpecRequirementList

      Return New ROProductionSpecRequirementList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROProductionSpecRequirementList() As ROProductionSpecRequirementList

      Return DataPortal.Fetch(Of ROProductionSpecRequirementList)(New Criteria())

    End Function

    Public Shared Function GetROProductionSpecRequirementList(SystemID As Integer?, ProductionAreaID As Integer?) As ROProductionSpecRequirementList

      Return DataPortal.Fetch(Of ROProductionSpecRequirementList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionSpecRequirement.GetROProductionSpecRequirement(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionSpecRequirementList"
            cm.Parameters.AddWithValue("@ProductionSpecRequirementName", Strings.MakeEmptyDBNull(crit.ProductionSpecRequirementName))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@EventTypeID", NothingDBNull(crit.EventTypeID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace