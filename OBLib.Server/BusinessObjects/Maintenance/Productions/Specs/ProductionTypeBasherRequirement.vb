﻿' Generated 26 Jul 2014 22:58 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Specs

  <Serializable()> _
  Public Class ProductionTypeBasherRequirement
    Inherits SingularBusinessBase(Of ProductionTypeBasherRequirement)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionTypeBasherRequirementIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeBasherRequirementID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionTypeBasherRequirementID() As Integer
      Get
        Return GetProperty(ProductionTypeBasherRequirementIDProperty)
      End Get
    End Property

    Public Shared NoOfCamerasFromProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfCamerasFrom, "No Of Cameras From", 0)
    ''' <summary>
    ''' Gets and sets the No Of Cameras From value
    ''' </summary>
    <Display(Name:="No Of Cameras From", Description:="The minimum number of cameras in a production for this crew requirements to apply"),
    Required(ErrorMessage:="No Of Cameras From required")>
    Public Property NoOfCamerasFrom() As Integer
      Get
        Return GetProperty(NoOfCamerasFromProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(NoOfCamerasFromProperty, Value)
      End Set
    End Property

    Public Shared NoOfCamerasToProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfCamerasTo, "No Of Cameras To", 0)
    ''' <summary>
    ''' Gets and sets the No Of Cameras To value
    ''' </summary>
    <Display(Name:="No Of Cameras To", Description:="The maximum number of cameras in a production for this crew requirements to apply"),
    Required(ErrorMessage:="No Of Cameras To required")>
    Public Property NoOfCamerasTo() As Integer
      Get
        Return GetProperty(NoOfCamerasToProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(NoOfCamerasToProperty, Value)
      End Set
    End Property

    Public Shared NoOfBashersProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfBashers, "No Of Bashers", 1)
    ''' <summary>
    ''' Gets and sets the No Of Bashers value
    ''' </summary>
    <Display(Name:="No Of Bashers", Description:="The number of Bashers that the system will automically allocate to a production for the specified number of cameras"),
    Required(ErrorMessage:="No Of Bashers required")>
    Public Property NoOfBashers() As Integer
      Get
        Return GetProperty(NoOfBashersProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(NoOfBashersProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionTypeBasherRequirementIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Type Basher Requirement")
        Else
          Return String.Format("Blank {0}", "Production Type Basher Requirement")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      ' Me.AddMultiplePropertyRule(AddressOf Singular.CSLALib.Rules.CompareProperties, New Singular.CSLALib.Rules.Args.ComparePropertyArgs("NoOfCamerasFrom", "NoOfCamerasTo", "<"), New String() {"NoOfCamerasFrom", "NoOfCamerasTo"})

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionTypeBasherRequirement() method.

    End Sub

    Public Shared Function NewProductionTypeBasherRequirement() As ProductionTypeBasherRequirement

      Return DataPortal.CreateChild(Of ProductionTypeBasherRequirement)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionTypeBasherRequirement(dr As SafeDataReader) As ProductionTypeBasherRequirement

      Dim p As New ProductionTypeBasherRequirement()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionTypeBasherRequirementIDProperty, .GetInt32(0))
          LoadProperty(NoOfCamerasFromProperty, .GetInt32(1))
          LoadProperty(NoOfCamerasToProperty, .GetInt32(2))
          LoadProperty(NoOfBashersProperty, .GetInt32(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionTypeBasherRequirement"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionTypeBasherRequirement"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionTypeBasherRequirementID As SqlParameter = .Parameters.Add("@ProductionTypeBasherRequirementID", SqlDbType.Int)
          paramProductionTypeBasherRequirementID.Value = GetProperty(ProductionTypeBasherRequirementIDProperty)
          If Me.IsNew Then
            paramProductionTypeBasherRequirementID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@NoOfCamerasFrom", GetProperty(NoOfCamerasFromProperty))
          .Parameters.AddWithValue("@NoOfCamerasTo", GetProperty(NoOfCamerasToProperty))
          .Parameters.AddWithValue("@NoOfBashers", GetProperty(NoOfBashersProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionTypeBasherRequirementIDProperty, paramProductionTypeBasherRequirementID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionTypeBasherRequirement"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionTypeBasherRequirementID", GetProperty(ProductionTypeBasherRequirementIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace