﻿' Generated 02 Jul 2016 22:47 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class ProductionSpecRequirementPositionList
    Inherits OBBusinessListBase(Of ProductionSpecRequirementPositionList, ProductionSpecRequirementPosition)

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementPositionID As Integer) As ProductionSpecRequirementPosition

      For Each child As ProductionSpecRequirementPosition In Me
        If child.ProductionSpecRequirementPositionID = ProductionSpecRequirementPositionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirement Positions"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewProductionSpecRequirementPositionList() As ProductionSpecRequirementPositionList

      Return New ProductionSpecRequirementPositionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace