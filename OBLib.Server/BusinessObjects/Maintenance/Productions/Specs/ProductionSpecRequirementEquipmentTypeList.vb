﻿' Generated 02 Jul 2016 22:47 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class ProductionSpecRequirementEquipmentTypeList
    Inherits OBBusinessListBase(Of ProductionSpecRequirementEquipmentTypeList, ProductionSpecRequirementEquipmentType)

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementEquipmentTypeID As Integer) As ProductionSpecRequirementEquipmentType

      For Each child As ProductionSpecRequirementEquipmentType In Me
        If child.ProductionSpecRequirementEquipmentTypeID = ProductionSpecRequirementEquipmentTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirement Equipment Types"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewProductionSpecRequirementEquipmentTypeList() As ProductionSpecRequirementEquipmentTypeList

      Return New ProductionSpecRequirementEquipmentTypeList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace