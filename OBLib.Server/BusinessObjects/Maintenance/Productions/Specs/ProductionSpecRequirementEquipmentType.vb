﻿' Generated 02 Jul 2016 22:47 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Equipment.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class ProductionSpecRequirementEquipmentType
    Inherits OBBusinessBase(Of ProductionSpecRequirementEquipmentType)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ProductionSpecRequirementEquipmentTypeBO.ProductionSpecRequirementEquipmentTypeBOToString(self)")

    Public Shared ProductionSpecRequirementEquipmentTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSpecRequirementEquipmentTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ProductionSpecRequirementEquipmentTypeID() As Integer
      Get
        Return GetProperty(ProductionSpecRequirementEquipmentTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSpecRequirementID, "Production Spec Requirement", Nothing)
    ''' <summary>
    ''' Gets the Production Spec Requirement value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionSpecRequirementID() As Integer?
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
    End Property

    Public Shared EquipmentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentTypeID, "Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment Type value
    ''' </summary>
    <Display(Name:="Type", Description:="The general type of equipment that is required for the production spec"),
    Required(ErrorMessage:="Type required"),
    DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROEquipmentTypeList), DisplayMember:="EquipmentType", ValueMember:="EquipmentTypeID")>
    Public Property EquipmentTypeID() As Integer?
      Get
        Return GetProperty(EquipmentTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentSubTypeID, "Sub Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Sub Type", Description:="The specific type of equipment that is required for the production spec"),
    Required(ErrorMessage:="Sub Type required"),
    DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROEquipmentSubTypeList), DisplayMember:="EquipmentSubType", ValueMember:="EquipmentSubTypeID", ThisFilterMember:="EquipmentTypeID")>
    Public Property EquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(EquipmentSubTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentSubTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentQuantityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentQuantity, "Quantity", 0)
    ''' <summary>
    ''' Gets and sets the Equipment Quantity value
    ''' </summary>
    <Display(Name:="Quantity", Description:="Number of a specific equipment type used for this production spec"),
    Required(ErrorMessage:="Quantity required")>
    Public Property EquipmentQuantity() As Integer
      Get
        Return GetProperty(EquipmentQuantityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EquipmentQuantityProperty, Value)
      End Set
    End Property

    'Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Production value
    ' ''' </summary>
    '<Display(Name:="Production", Description:="The prodtion the the equipment type requirements relate to")>
    'Public Property ProductionID() As Integer?
    '  Get
    '    Return GetProperty(ProductionIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(ProductionIDProperty, Value)
    '  End Set
    'End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    'Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the System value
    ' ''' </summary>
    '<Display(Name:="System", Description:=""),
    'Required(ErrorMessage:="System required")>
    'Public Property SystemID() As Integer?
    '  Get
    '    Return GetProperty(SystemIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(SystemIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area", 0)
    ' ''' <summary>
    ' ''' Gets and sets the Production Area value
    ' ''' </summary>
    '<Display(Name:="Production Area", Description:=""),
    'Required(ErrorMessage:="Production Area required")>
    'Public Property ProductionAreaID() As Integer
    '  Get
    '    Return GetProperty(ProductionAreaIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(ProductionAreaIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Production System Area value
    ' ''' </summary>
    '<Display(Name:="Production System Area", Description:="")>
    'Public Property ProductionSystemAreaID() As Integer?
    '  Get
    '    Return GetProperty(ProductionSystemAreaIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(ProductionSystemAreaIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Room value
    ' ''' </summary>
    '<Display(Name:="Room", Description:="")>
    'Public Property RoomID() As Integer?
    '  Get
    '    Return GetProperty(RoomIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(RoomIDProperty, Value)
    '  End Set
    'End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionSpecRequirement

      Return CType(CType(Me.Parent, ProductionSpecRequirementEquipmentTypeList).Parent, ProductionSpecRequirement)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSpecRequirementEquipmentTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Spec Requirement Equipment Type")
        Else
          Return String.Format("Blank {0}", "Production Spec Requirement Equipment Type")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      'ValidationRules.AddRule(AddressOf Singular.CSLALib.Rules.CompareValue, New Singular.CSLALib.Rules.Args.CompareValueArgs("EquipmentQuantity", 0, ">"))

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionSpecRequirementEquipmentType() method.

    End Sub

    Public Shared Function NewProductionSpecRequirementEquipmentType() As ProductionSpecRequirementEquipmentType

      Return DataPortal.CreateChild(Of ProductionSpecRequirementEquipmentType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetProductionSpecRequirementEquipmentType(dr As SafeDataReader) As ProductionSpecRequirementEquipmentType

      Dim p As New ProductionSpecRequirementEquipmentType()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionSpecRequirementEquipmentTypeIDProperty, .GetInt32(0))
          LoadProperty(ProductionSpecRequirementIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EquipmentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(EquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(EquipmentQuantityProperty, .GetInt32(4))
          'LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          'LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          'LoadProperty(ProductionAreaIDProperty, .GetInt32(11))
          'LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          'LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionSpecRequirementEquipmentTypeIDProperty)

      cm.Parameters.AddWithValue("@ProductionSpecRequirementID", Me.GetParent.ProductionSpecRequirementID)
      cm.Parameters.AddWithValue("@EquipmentTypeID", GetProperty(EquipmentTypeIDProperty))
      cm.Parameters.AddWithValue("@EquipmentSubTypeID", GetProperty(EquipmentSubTypeIDProperty))
      cm.Parameters.AddWithValue("@EquipmentQuantity", GetProperty(EquipmentQuantityProperty))
      cm.Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(Nothing))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(Me.GetParent.SystemID))
      cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(Me.GetParent.ProductionAreaID))
      cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(Nothing))
      cm.Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(Me.GetParent.RoomID))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionSpecRequirementEquipmentTypeIDProperty, cm.Parameters("@ProductionSpecRequirementEquipmentTypeID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ProductionSpecRequirementEquipmentTypeID", GetProperty(ProductionSpecRequirementEquipmentTypeIDProperty))
    End Sub

#End Region

  End Class

End Namespace