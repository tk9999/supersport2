﻿' Generated 02 Jul 2016 22:47 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.Rooms.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class ProductionSpecRequirement
    Inherits OBBusinessBase(Of ProductionSpecRequirement)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ProductionSpecRequirementBO.ProductionSpecRequirementBOToString(self)")

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSpecRequirementID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ProductionSpecRequirementID() As Integer
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionSpecRequirementName, "Spec Name", "")
    ''' <summary>
    ''' Gets and sets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Spec Name", Description:="Unique name for this spec requirement"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Spec Name is required"),
    StringLength(100, ErrorMessage:="Spec Name cannot be more than 100 characters")>
    Public Property ProductionSpecRequirementName() As String
      Get
        Return GetProperty(ProductionSpecRequirementNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionSpecRequirementNameProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Production Type", Description:=""),
    DropDownWeb(GetType(ROProductionTypeList),
                BeforeFetchJS:="ProductionSpecRequirementBO.setProductionTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="ProductionSpecRequirementBO.triggerProductionTypeIDAutoPopulate",
                AfterFetchJS:="ProductionSpecRequirementBO.afterProductionTypeIDRefreshAjax",
                OnItemSelectJSFunction:="ProductionSpecRequirementBO.onProductionTypeIDSelected",
                LookupMember:="ProductionType", DisplayMember:="ProductionType", ValueMember:="ProductionTypeID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"ProductionType"}),
    SetExpression("ProductionSpecRequirementBO.ProductionTypeIDSet(self)")>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionType, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTypeProperty, Value)
      End Set
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EventTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Event Type", Description:=""),
    DropDownWeb(GetType(ROEventTypeList),
                BeforeFetchJS:="ProductionSpecRequirementBO.setEventTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="ProductionSpecRequirementBO.triggerEventTypeIDAutoPopulate",
                AfterFetchJS:="ProductionSpecRequirementBO.afterEventTypeIDRefreshAjax",
                OnItemSelectJSFunction:="ProductionSpecRequirementBO.onEventTypeIDSelected",
                LookupMember:="EventType", DisplayMember:="EventType", ValueMember:="EventTypeID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"EventType"}),
    SetExpression("ProductionSpecRequirementBO.EventTypeIDSet(self)")>
    Public Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EventTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.EventType, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventTypeProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="The date that this spec is valid from"),
    Required(ErrorMessage:="Start Date required"),
    DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="The date this spec is valid until. If left NULL then this spec is valid indefinately"),
    DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared HDRequiredIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.HDRequiredInd, "HD Required", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the HD Required value
    ''' </summary>
    <Display(Name:="HD Required", Description:="Tick indicates that HD is required. No tick indicates that HD is not required. NULL indicates that some event require HD")>
    Public Property HDRequiredInd() As Boolean?
      Get
        Return GetProperty(HDRequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(HDRequiredIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID"),
    SetExpression("ProductionSpecRequirementBO.SystemIDSet(self)")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionGradeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionGradeID, "Production Grade", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Grade value
    ''' </summary>
    <Display(Name:="Production Grade", Description:="")>
    Public Property ProductionGradeID() As Integer?
      Get
        Return GetProperty(ProductionGradeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionGradeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required"),
    DropDownWeb(GetType(ROUserSystemAreaList), ThisFilterMember:="SystemID", DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID"),
    SetExpression("ProductionSpecRequirementBO.ProductionAreaIDSet(self)")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
    DropDownWeb(GetType(RORoomList), DisplayMember:="Room", ValueMember:="RoomID"),
    SetExpression("ProductionSpecRequirementBO.RoomIDSet(self)")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Room, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionSpecRequirementPositionListProperty As PropertyInfo(Of ProductionSpecRequirementPositionList) = RegisterProperty(Of ProductionSpecRequirementPositionList)(Function(c) c.ProductionSpecRequirementPositionList, "Production Spec Requirement Position List")

    Public ReadOnly Property ProductionSpecRequirementPositionList() As ProductionSpecRequirementPositionList
      Get
        If GetProperty(ProductionSpecRequirementPositionListProperty) Is Nothing Then
          LoadProperty(ProductionSpecRequirementPositionListProperty, Maintenance.SystemManagement.ProductionSpecRequirementPositionList.NewProductionSpecRequirementPositionList())
        End If
        Return GetProperty(ProductionSpecRequirementPositionListProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementEquipmentTypeListProperty As PropertyInfo(Of ProductionSpecRequirementEquipmentTypeList) = RegisterProperty(Of ProductionSpecRequirementEquipmentTypeList)(Function(c) c.ProductionSpecRequirementEquipmentTypeList, "Production Spec Requirement Equipment Type List")

    Public ReadOnly Property ProductionSpecRequirementEquipmentTypeList() As ProductionSpecRequirementEquipmentTypeList
      Get
        If GetProperty(ProductionSpecRequirementEquipmentTypeListProperty) Is Nothing Then
          LoadProperty(ProductionSpecRequirementEquipmentTypeListProperty, Maintenance.SystemManagement.ProductionSpecRequirementEquipmentTypeList.NewProductionSpecRequirementEquipmentTypeList())
        End If
        Return GetProperty(ProductionSpecRequirementEquipmentTypeListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSpecRequirementIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionSpecRequirementName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Spec Requirement")
        Else
          Return String.Format("Blank {0}", "Production Spec Requirement")
        End If
      Else
        Return Me.ProductionSpecRequirementName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ProductionSpecRequirementPositions", "ProductionSpecRequirementEquipmentTypes"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      With AddWebRule(RoomIDProperty)
        .ServerRuleFunction = AddressOf RoomIDValid
        .JavascriptRuleFunctionName = "ProductionSpecRequirementBO.RoomIDValid"
        .AddTriggerProperties(ProductionAreaIDProperty, ProductionTypeIDProperty, EventTypeIDProperty)
        .AffectedProperties.AddRange({ProductionAreaIDProperty, ProductionTypeIDProperty, EventTypeIDProperty})
      End With

    End Sub

    Public Shared Function RoomIDValid(Spec As ProductionSpecRequirement) As String
      Return ""
    End Function


#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionSpecRequirement() method.

    End Sub

    Public Shared Function NewProductionSpecRequirement() As ProductionSpecRequirement

      Return DataPortal.CreateChild(Of ProductionSpecRequirement)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetProductionSpecRequirement(dr As SafeDataReader) As ProductionSpecRequirement

      Dim p As New ProductionSpecRequirement()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionSpecRequirementIDProperty, .GetInt32(0))
          LoadProperty(ProductionSpecRequirementNameProperty, .GetString(1))
          LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(StartDateProperty, .GetValue(4))
          LoadProperty(EndDateProperty, .GetValue(5))
          Dim tmpHDRequiredInd = .GetValue(6)
          If IsDBNull(tmpHDRequiredInd) Then
            LoadProperty(HDRequiredIndProperty, Nothing)
          Else
            LoadProperty(HDRequiredIndProperty, tmpHDRequiredInd)
          End If
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(SystemIDProperty, .GetInt32(11))
          LoadProperty(ProductionGradeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(ProductionAreaIDProperty, .GetInt32(13))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(ProductionTypeProperty, .GetString(15))
          LoadProperty(EventTypeProperty, .GetString(16))
          LoadProperty(RoomProperty, .GetString(17))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionSpecRequirementIDProperty)

      cm.Parameters.AddWithValue("@ProductionSpecRequirementName", GetProperty(ProductionSpecRequirementNameProperty))
      cm.Parameters.AddWithValue("@ProductionTypeID", Singular.Misc.NothingDBNull(GetProperty(ProductionTypeIDProperty)))
      cm.Parameters.AddWithValue("@EventTypeID", Singular.Misc.NothingDBNull(GetProperty(EventTypeIDProperty)))
      cm.Parameters.AddWithValue("@StartDate", StartDate)
      cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(EndDate))
      cm.Parameters.AddWithValue("@HDRequiredInd", Singular.Misc.NothingDBNull(GetProperty(HDRequiredIndProperty)))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      cm.Parameters.AddWithValue("@ProductionGradeID", Singular.Misc.NothingDBNull(GetProperty(ProductionGradeIDProperty)))
      cm.Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(GetProperty(RoomIDProperty)))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionSpecRequirementIDProperty, cm.Parameters("@ProductionSpecRequirementID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(ProductionSpecRequirementPositionListProperty))
      UpdateChild(GetProperty(ProductionSpecRequirementEquipmentTypeListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ProductionSpecRequirementID", GetProperty(ProductionSpecRequirementIDProperty))
    End Sub

#End Region

  End Class

End Namespace