﻿' Generated 02 Jul 2016 22:47 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class ProductionSpecRequirementList
    Inherits OBBusinessListBase(Of ProductionSpecRequirementList, ProductionSpecRequirement)

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementID As Integer) As ProductionSpecRequirement

      For Each child As ProductionSpecRequirement In Me
        If child.ProductionSpecRequirementID = ProductionSpecRequirementID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirements"

    End Function

    Public Function GetProductionSpecRequirementPosition(ProductionSpecRequirementPositionID As Integer) As ProductionSpecRequirementPosition

      Dim obj As ProductionSpecRequirementPosition = Nothing
      For Each parent As ProductionSpecRequirement In Me
        obj = parent.ProductionSpecRequirementPositionList.GetItem(ProductionSpecRequirementPositionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetProductionSpecRequirementEquipmentType(ProductionSpecRequirementEquipmentTypeID As Integer) As ProductionSpecRequirementEquipmentType

      Dim obj As ProductionSpecRequirementEquipmentType = Nothing
      For Each parent As ProductionSpecRequirement In Me
        obj = parent.ProductionSpecRequirementEquipmentTypeList.GetItem(ProductionSpecRequirementEquipmentTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionSpecRequirementID As Integer? = Nothing
      Public Property ProductionTypeID As Integer? = Nothing
      Public Property EventTypeID As Integer? = Nothing
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing

      Public Sub New(ProductionSpecRequirementID As Integer?, ProductionTypeID As Integer?, EventTypeID As Integer?, _
                     StartDate As DateTime?, EndDate As DateTime?, SystemID As Integer?, ProductionAreaID As Integer?)

        Me.ProductionSpecRequirementID = ProductionSpecRequirementID
        Me.ProductionTypeID = ProductionTypeID
        Me.EventTypeID = EventTypeID
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID

      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewProductionSpecRequirementList() As ProductionSpecRequirementList

      Return New ProductionSpecRequirementList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetProductionSpecRequirementList() As ProductionSpecRequirementList

      Return DataPortal.Fetch(Of ProductionSpecRequirementList)(New Criteria())

    End Function

    Public Shared Function GetProductionSpecRequirementList(ProductionSpecRequirementID As Integer?, ProductionTypeID As Integer?, EventTypeID As Integer?, _
                                                            StartDate As DateTime?, EndDate As DateTime?, SystemID As Integer?, ProductionAreaID As Integer?) As ProductionSpecRequirementList

      Return DataPortal.Fetch(Of ProductionSpecRequirementList)(New Criteria(ProductionSpecRequirementID, ProductionTypeID, EventTypeID, StartDate, EndDate, SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionSpecRequirement.GetProductionSpecRequirement(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ProductionSpecRequirement = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionSpecRequirementID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionSpecRequirementPositionList.RaiseListChangedEvents = False
          parent.ProductionSpecRequirementPositionList.Add(ProductionSpecRequirementPosition.GetProductionSpecRequirementPosition(sdr))
          parent.ProductionSpecRequirementPositionList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionSpecRequirementID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionSpecRequirementEquipmentTypeList.RaiseListChangedEvents = False
          parent.ProductionSpecRequirementEquipmentTypeList.Add(ProductionSpecRequirementEquipmentType.GetProductionSpecRequirementEquipmentType(sdr))
          parent.ProductionSpecRequirementEquipmentTypeList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As ProductionSpecRequirement In Me
        child.CheckRules()
        For Each ProductionSpecRequirementPosition As ProductionSpecRequirementPosition In child.ProductionSpecRequirementPositionList
          ProductionSpecRequirementPosition.CheckRules()
        Next
        For Each ProductionSpecRequirementEquipmentType As ProductionSpecRequirementEquipmentType In child.ProductionSpecRequirementEquipmentTypeList
          ProductionSpecRequirementEquipmentType.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionSpecRequirementList"
            cm.Parameters.AddWithValue("@ProductionSpecRequirementID", Singular.Misc.NothingDBNull(crit.ProductionSpecRequirementID))
            cm.Parameters.AddWithValue("@ProductionTypeID", Singular.Misc.NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@EventTypeID", Singular.Misc.NothingDBNull(crit.EventTypeID))
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace