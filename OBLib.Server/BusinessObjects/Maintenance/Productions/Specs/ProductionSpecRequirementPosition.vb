﻿' Generated 02 Jul 2016 22:47 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.Maintenance.Equipment.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class ProductionSpecRequirementPosition
    Inherits OBBusinessBase(Of ProductionSpecRequirementPosition)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ProductionSpecRequirementPositionBO.ProductionSpecRequirementPositionBOToString(self)")

    Public Shared ProductionSpecRequirementPositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSpecRequirementPositionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ProductionSpecRequirementPositionID() As Integer
      Get
        Return GetProperty(ProductionSpecRequirementPositionIDProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSpecRequirementID, "Production Spec Requirement", Nothing)
    ''' <summary>
    ''' Gets the Production Spec Requirement value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionSpecRequirementID() As Integer?
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position", Nothing)
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:=""),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplinePositionTypeListSelect),
                BeforeFetchJS:="ProductionSpecRequirementPositionBO.setPositionIDCriteriaBeforeRefresh",
                PreFindJSFunction:="ProductionSpecRequirementPositionBO.triggerPositionIDAutoPopulate",
                AfterFetchJS:="ProductionSpecRequirementPositionBO.afterPositionIDRefreshAjax",
                OnItemSelectJSFunction:="ProductionSpecRequirementPositionBO.onPositionIDSelected",
                LookupMember:="Position", DisplayMember:="Position", ValueMember:="PositionID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"PositionType", "Position"})>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position", "")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionProperty, Value)
      End Set
    End Property

    'DropDownWeb("ProductionSpecRequirementPositionBO.GetPositionList($data)", DisplayMember:="Position", ValueMember:="PositionID"),
    'SetExpressionBeforeChange("ProductionSpecRequirementPositionBO.PositionIDBeforeSet(self)"),
    'SetExpression("ProductionSpecRequirementPositionBO.PositionIDSet(self)")

    Public Shared EquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentSubTypeID, "Sub Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Sub Type"),
    DropDownWeb(GetType(ROEquipmentSubTypeList), DisplayMember:="EquipmentSubType", ValueMember:="EquipmentSubTypeID")>
    Public Property EquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(EquipmentSubTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentSubTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentQuantityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentQuantity, "Quantity", 0)
    ''' <summary>
    ''' Gets and sets the Equipment Quantity value
    ''' </summary>
    <Display(Name:="Quantity", Description:="Number of a specific equipment type used for this production spec"),
    Required(ErrorMessage:="Quantity required")>
    Public Property EquipmentQuantity() As Integer
      Get
        Return GetProperty(EquipmentQuantityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EquipmentQuantityProperty, Value)
      End Set
    End Property

    '  Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    '  ''' <summary>
    '  ''' Gets and sets the Production value
    '  ''' </summary>
    '  <Display(Name:="Production", Description:="The prodtion the the position requirements relate to")>
    'Public Property ProductionID() As Integer?
    '    Get
    '      Return GetProperty(ProductionIDProperty)
    '    End Get
    '    Set(ByVal Value As Integer?)
    '      SetProperty(ProductionIDProperty, Value)
    '    End Set
    '  End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    '  Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    '  ''' <summary>
    '  ''' Gets and sets the System value
    '  ''' </summary>
    '  <Display(Name:="System", Description:="system linked to"),
    '  Required(ErrorMessage:="System required")>
    'Public Property SystemID() As Integer?
    '    Get
    '      Return GetProperty(SystemIDProperty)
    '    End Get
    '    Set(ByVal Value As Integer?)
    '      SetProperty(SystemIDProperty, Value)
    '    End Set
    '  End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    Required(ErrorMessage:="Discipline required"),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplineListSelect),
                BeforeFetchJS:="ProductionSpecRequirementPositionBO.setDisciplineIDCriteriaBeforeRefresh",
                PreFindJSFunction:="ProductionSpecRequirementPositionBO.triggerDisciplineIDAutoPopulate",
                AfterFetchJS:="ProductionSpecRequirementPositionBO.afterDisciplineIDRefreshAjax",
                OnItemSelectJSFunction:="ProductionSpecRequirementPositionBO.onDisciplineIDSelected",
                LookupMember:="Discipline", DisplayMember:="Discipline", ValueMember:="DisciplineID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Discipline"}),
    SetExpression("ProductionSpecRequirementPositionBO.DisciplineIDSet(self)")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    '  Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    '  ''' <summary>
    '  ''' Gets and sets the Production Area value
    '  ''' </summary>
    '  <Display(Name:="Production Area", Description:=""),
    '  Required(ErrorMessage:="Production Area required")>
    'Public Property ProductionAreaID() As Integer?
    '    Get
    '      Return GetProperty(ProductionAreaIDProperty)
    '    End Get
    '    Set(ByVal Value As Integer?)
    '      SetProperty(ProductionAreaIDProperty, Value)
    '    End Set
    '  End Property

    '  Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    '  ''' <summary>
    '  ''' Gets and sets the Production System Area value
    '  ''' </summary>
    '  <Display(Name:="Production System Area", Description:="")>
    'Public Property ProductionSystemAreaID() As Integer?
    '    Get
    '      Return GetProperty(ProductionSystemAreaIDProperty)
    '    End Get
    '    Set(ByVal Value As Integer?)
    '      SetProperty(ProductionSystemAreaIDProperty, Value)
    '    End Set
    '  End Property

    '  Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    '  ''' <summary>
    '  ''' Gets and sets the Room value
    '  ''' </summary>
    '  <Display(Name:="Room", Description:="")>
    'Public Property RoomID() As Integer?
    '    Get
    '      Return GetProperty(RoomIDProperty)
    '    End Get
    '    Set(ByVal Value As Integer?)
    '      SetProperty(RoomIDProperty, Value)
    '    End Set
    '  End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionSpecRequirement

      Return CType(CType(Me.Parent, ProductionSpecRequirementPositionList).Parent, ProductionSpecRequirement)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSpecRequirementPositionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Spec Requirement Position")
        Else
          Return String.Format("Blank {0}", "Production Spec Requirement Position")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      'Me.AddMultiplePropertyRule(AddressOf Singular.CSLALib.Rules.CompareProperties, New Singular.CSLALib.Rules.Args.ComparePropertyArgs("EquipmentSubTypeID", "EquipmentQuantity", ">"), New String() {"EquipmentSubTypeID", "EquipmentQuantity"})

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionSpecRequirementPosition() method.

    End Sub

    Public Shared Function NewProductionSpecRequirementPosition() As ProductionSpecRequirementPosition

      Return DataPortal.CreateChild(Of ProductionSpecRequirementPosition)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetProductionSpecRequirementPosition(dr As SafeDataReader) As ProductionSpecRequirementPosition

      Dim p As New ProductionSpecRequirementPosition()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionSpecRequirementPositionIDProperty, .GetInt32(0))
          LoadProperty(ProductionSpecRequirementIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(EquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(EquipmentQuantityProperty, .GetInt32(4))
          'LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          'LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          'LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          'LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          'LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(DisciplineProperty, .GetString(15))
          LoadProperty(PositionProperty, .GetString(16))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionSpecRequirementPositionIDProperty)

      cm.Parameters.AddWithValue("@ProductionSpecRequirementID", Me.GetParent.ProductionSpecRequirementID)
      cm.Parameters.AddWithValue("@PositionID", Singular.Misc.NothingDBNull(GetProperty(PositionIDProperty)))
      cm.Parameters.AddWithValue("@EquipmentSubTypeID", Singular.Misc.NothingDBNull(GetProperty(EquipmentSubTypeIDProperty)))
      cm.Parameters.AddWithValue("@EquipmentQuantity", GetProperty(EquipmentQuantityProperty))
      cm.Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(Nothing))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(Me.GetParent.SystemID))
      cm.Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
      cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(Me.GetParent.ProductionAreaID))
      cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(Nothing))
      cm.Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(Me.GetParent.RoomID))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionSpecRequirementPositionIDProperty, cm.Parameters("@ProductionSpecRequirementPositionID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ProductionSpecRequirementPositionID", GetProperty(ProductionSpecRequirementPositionIDProperty))
    End Sub

#End Region

  End Class

End Namespace