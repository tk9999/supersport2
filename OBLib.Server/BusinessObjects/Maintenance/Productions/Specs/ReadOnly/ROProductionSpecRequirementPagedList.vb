﻿' Generated 02 Jul 2016 21:34 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROProductionSpecRequirementPagedList
    Inherits OBReadOnlyListBase(Of ROProductionSpecRequirementPagedList, ROProductionSpecRequirementPaged)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "


    ' ** Example Paging property declaration code to be implemented in the necessary VM **
    'Public Property ROProductionSpecRequirementPagedList As ROProductionSpecRequirementPagedList
    'Public Property ROProductionSpecRequirementPagedListCriteria As ROProductionSpecRequirementPagedList.Criteria
    'Public Property ROProductionSpecRequirementPagedListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    ' ** Example Paging definition code to be implemented in the necessary VM Setup Method **
    'ROProductionSpecRequirementPagedList = New ROProductionSpecRequirementPagedList
    'ROProductionSpecRequirementPagedListCriteria = New ROProductionSpecRequirementPagedList.Criteria
    'ROProductionSpecRequirementPagedListManager = New Singular.Web.Data.PagedDataManager(Of VMType)(Function(d) d.ROProductionSpecRequirementPagedList, Function(d) d.ROProductionSpecRequirementPagedListCriteria, "SortBy", PageSize, SortAsc)

    Public Function GetItem(ProductionSpecRequirementID As Integer) As ROProductionSpecRequirementPaged

      For Each child As ROProductionSpecRequirementPaged In Me
        If child.ProductionSpecRequirementID = ProductionSpecRequirementID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirements"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      <Display(Name:="Sub-Dept"),
      DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID"),
      SetExpression("ROProductionSpecRequirementPagedListCriteriaBO.SystemIDSet(self)")>
      Public Property SystemID As Integer? = Nothing

      <Display(Name:="Area"),
      DropDownWeb(GetType(ROUserSystemAreaList), ThisFilterMember:="SystemID", DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID"),
      SetExpression("ROProductionSpecRequirementPagedListCriteriaBO.ProductionAreaIDSet(self)")>
      Public Property ProductionAreaID As Integer? = Nothing

      <Display(Name:="Spec Name"), TextField(, , , , ),
      SetExpression("ROProductionSpecRequirementPagedListCriteriaBO.ProductionSpecRequirementNameSet(self)", , 250)>
      Public Property ProductionSpecRequirementName As String = ""

      Public Sub New(SystemID As Object, ProductionAreaID As Object)

        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID

      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROProductionSpecRequirementPagedList() As ROProductionSpecRequirementPagedList

      Return New ROProductionSpecRequirementPagedList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROProductionSpecRequirementPagedList() As ROProductionSpecRequirementPagedList

      Return DataPortal.Fetch(Of ROProductionSpecRequirementPagedList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionSpecRequirementPaged.GetROProductionSpecRequirementPaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionSpecRequirementPagedList"
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@ProductionSpecRequirementName", Strings.MakeEmptyDBNull(crit.ProductionSpecRequirementName))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace