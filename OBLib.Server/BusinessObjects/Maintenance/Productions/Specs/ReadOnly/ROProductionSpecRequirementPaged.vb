﻿' Generated 02 Jul 2016 21:35 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROProductionSpecRequirementPaged
    Inherits OBReadOnlyBase(Of ROProductionSpecRequirementPaged)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROProductionSpecRequirementPagedBO.ROProductionSpecRequirementPagedBOToString(self)")

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSpecRequirementID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ProductionSpecRequirementID() As Integer
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionSpecRequirementName, "Spec Name", "")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Spec Name", Description:="Unique name for this spec requirement")>
    Public ReadOnly Property ProductionSpecRequirementName() As String
      Get
        Return GetProperty(ProductionSpecRequirementNameProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="The type of production")>
    Public ReadOnly Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventTypeID, "Event Type", Nothing)
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="The type of event that this production spec is part of (PSL Soccer, Super Rugby)")>
    Public ReadOnly Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="The date that this spec is valid from")>
    Public ReadOnly Property StartDate As Date
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="The date this spec is valid until. If left NULL then this spec is valid indefinately")>
    Public ReadOnly Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    Public Shared HDRequiredIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.HDRequiredInd, "HD Required", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets the HD Required value
    ''' </summary>
    <Display(Name:="HD Required", Description:="Tick indicates that HD is required. No tick indicates that HD is not required. NULL indicates that some event require HD")>
    Public ReadOnly Property HDRequiredInd() As Boolean?
      Get
        Return GetProperty(HDRequiredIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 0)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionGradeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionGradeID, "Production Grade", Nothing)
    ''' <summary>
    ''' Gets the Production Grade value
    ''' </summary>
    <Display(Name:="Production Grade", Description:="")>
    Public ReadOnly Property ProductionGradeID() As Integer?
      Get
        Return GetProperty(ProductionGradeIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area", 0)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared SubDeptProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubDept, "Sub Dept", "")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Sub Dept")>
    Public ReadOnly Property SubDept() As String
      Get
        Return GetProperty(SubDeptProperty)
      End Get
    End Property

    Public Shared AreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Area, "Area", "")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Area")>
    Public ReadOnly Property Area() As String
      Get
        Return GetProperty(AreaProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room", "")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Room")>
    Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Production Type", "")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Production Type")>
    Public ReadOnly Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "Event Type", "")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Event Type")>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSpecRequirementIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionSpecRequirementName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROProductionSpecRequirementPaged(dr As SafeDataReader) As ROProductionSpecRequirementPaged

      Dim r As New ROProductionSpecRequirementPaged()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionSpecRequirementIDProperty, .GetInt32(0))
        LoadProperty(ProductionSpecRequirementNameProperty, .GetString(1))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(StartDateProperty, .GetValue(4))
        LoadProperty(EndDateProperty, .GetValue(5))
        'Dim tmpHDRequiredInd = .GetValue(6)
        'If IsDBNull(tmpHDRequiredInd) Then
        '  LoadProperty(HDRequiredIndProperty, Nothing)
        'Else
        '  LoadProperty(HDRequiredIndProperty, tmpHDRequiredInd)
        'End If
        LoadProperty(CreatedByProperty, .GetInt32(7))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(ModifiedByProperty, .GetInt32(9))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
        LoadProperty(SystemIDProperty, .GetInt32(11))
        LoadProperty(ProductionGradeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(ProductionAreaIDProperty, .GetInt32(13))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(SubDeptProperty, .GetString(15))
        LoadProperty(AreaProperty, .GetString(16))
        LoadProperty(RoomProperty, .GetString(17))
        LoadProperty(ProductionTypeProperty, .GetString(18))
        LoadProperty(EventTypeProperty, .GetString(19))
      End With

    End Sub

#End Region

  End Class

End Namespace