﻿' Generated 26 Jul 2014 22:59 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Specs.ReadOnly

  <Serializable()> _
  Public Class ROProductionTypeCrewRequirement
    Inherits SingularReadOnlyBase(Of ROProductionTypeCrewRequirement)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionTypeCrewRequirementIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeCrewRequirementID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionTypeCrewRequirementID() As Integer
      Get
        Return GetProperty(ProductionTypeCrewRequirementIDProperty)
      End Get
    End Property

    Public Shared NoOfCamerasFromProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfCamerasFrom, "No Of Cameras From", 0)
    ''' <summary>
    ''' Gets the No Of Cameras From value
    ''' </summary>
    <Display(Name:="No Of Cameras From", Description:="The minimum number of cameras in a production for this crew requirements to apply")>
    Public ReadOnly Property NoOfCamerasFrom() As Integer
      Get
        Return GetProperty(NoOfCamerasFromProperty)
      End Get
    End Property

    Public Shared NoOfCamerasToProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfCamerasTo, "No Of Cameras To", 0)
    ''' <summary>
    ''' Gets the No Of Cameras To value
    ''' </summary>
    <Display(Name:="No Of Cameras To", Description:="The maximum number of cameras in a production for this crew requirements to apply")>
    Public ReadOnly Property NoOfCamerasTo() As Integer
      Get
        Return GetProperty(NoOfCamerasToProperty)
      End Get
    End Property

    Public Shared NoOfEVSProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfEVS, "No Of EVS", 0)
    ''' <summary>
    ''' Gets the No Of EVS value
    ''' </summary>
    <Display(Name:="No Of EVS", Description:="The number of EVSs that the system will automically allocate to a production for the specified number of cameras")>
    Public ReadOnly Property NoOfEVS() As Integer
      Get
        Return GetProperty(NoOfEVSProperty)
      End Get
    End Property

    Public Shared NoOfVisionControllersProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfVisionControllers, "No Of Vision Controllers", 0)
    ''' <summary>
    ''' Gets the No Of Vision Controllers value
    ''' </summary>
    <Display(Name:="No Of Vision Controllers", Description:="The number of Vision Controllers that the system will automically allocate to a production for the specified number of cameras")>
    Public ReadOnly Property NoOfVisionControllers() As Integer
      Get
        Return GetProperty(NoOfVisionControllersProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionTypeCrewRequirementIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionTypeCrewRequirement(dr As SafeDataReader) As ROProductionTypeCrewRequirement

      Dim r As New ROProductionTypeCrewRequirement()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionTypeCrewRequirementIDProperty, .GetInt32(0))
        LoadProperty(NoOfCamerasFromProperty, .GetInt32(1))
        LoadProperty(NoOfCamerasToProperty, .GetInt32(2))
        LoadProperty(NoOfEVSProperty, .GetInt32(3))
        LoadProperty(NoOfVisionControllersProperty, .GetInt32(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace