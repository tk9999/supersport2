﻿' Generated 26 Jul 2014 22:58 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Specs.ReadOnly

  <Serializable()> _
  Public Class ROProductionTypeBasherRequirement
    Inherits SingularReadOnlyBase(Of ROProductionTypeBasherRequirement)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionTypeBasherRequirementIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeBasherRequirementID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionTypeBasherRequirementID() As Integer
      Get
        Return GetProperty(ProductionTypeBasherRequirementIDProperty)
      End Get
    End Property

    Public Shared NoOfCamerasFromProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfCamerasFrom, "No Of Cameras From", 0)
    ''' <summary>
    ''' Gets the No Of Cameras From value
    ''' </summary>
    <Display(Name:="No Of Cameras From", Description:="The minimum number of cameras in a production for this crew requirements to apply")>
    Public ReadOnly Property NoOfCamerasFrom() As Integer
      Get
        Return GetProperty(NoOfCamerasFromProperty)
      End Get
    End Property

    Public Shared NoOfCamerasToProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfCamerasTo, "No Of Cameras To", 0)
    ''' <summary>
    ''' Gets the No Of Cameras To value
    ''' </summary>
    <Display(Name:="No Of Cameras To", Description:="The maximum number of cameras in a production for this crew requirements to apply")>
    Public ReadOnly Property NoOfCamerasTo() As Integer
      Get
        Return GetProperty(NoOfCamerasToProperty)
      End Get
    End Property

    Public Shared NoOfBashersProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfBashers, "No Of Bashers", 1)
    ''' <summary>
    ''' Gets the No Of Bashers value
    ''' </summary>
    <Display(Name:="No Of Bashers", Description:="The number of Bashers that the system will automically allocate to a production for the specified number of cameras")>
    Public ReadOnly Property NoOfBashers() As Integer
      Get
        Return GetProperty(NoOfBashersProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionTypeBasherRequirementIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionTypeBasherRequirement(dr As SafeDataReader) As ROProductionTypeBasherRequirement

      Dim r As New ROProductionTypeBasherRequirement()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionTypeBasherRequirementIDProperty, .GetInt32(0))
        LoadProperty(NoOfCamerasFromProperty, .GetInt32(1))
        LoadProperty(NoOfCamerasToProperty, .GetInt32(2))
        LoadProperty(NoOfBashersProperty, .GetInt32(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace