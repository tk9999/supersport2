﻿' Generated 26 Jul 2014 22:58 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Specs.ReadOnly

  <Serializable()> _
  Public Class ROProductionTypeCrewRequirementList
    Inherits SingularReadOnlyListBase(Of ROProductionTypeCrewRequirementList, ROProductionTypeCrewRequirement)

#Region " Business Methods "

    Public Function GetItemForCameraCount(ByVal NoOfCameras As Integer) As ROProductionTypeCrewRequirement

      For Each child As ROProductionTypeCrewRequirement In Me
        If child.NoOfCamerasFrom <= NoOfCameras AndAlso child.NoOfCamerasTo >= NoOfCameras Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItem(ProductionTypeCrewRequirementID As Integer) As ROProductionTypeCrewRequirement

      For Each child As ROProductionTypeCrewRequirement In Me
        If child.ProductionTypeCrewRequirementID = ProductionTypeCrewRequirementID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Type Crew Requirements"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionTypeCrewRequirementList() As ROProductionTypeCrewRequirementList

      Return New ROProductionTypeCrewRequirementList()

    End Function

    Public Shared Sub BeginGetROProductionTypeCrewRequirementList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionTypeCrewRequirementList)))

      Dim dp As New DataPortal(Of ROProductionTypeCrewRequirementList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionTypeCrewRequirementList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionTypeCrewRequirementList)))

      Dim dp As New DataPortal(Of ROProductionTypeCrewRequirementList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionTypeCrewRequirementList() As ROProductionTypeCrewRequirementList

      Return DataPortal.Fetch(Of ROProductionTypeCrewRequirementList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionTypeCrewRequirement.GetROProductionTypeCrewRequirement(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionTypeCrewRequirementList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace