﻿' Generated 26 Jul 2014 22:58 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Specs.ReadOnly

  <Serializable()> _
  Public Class ROProductionTypeBasherRequirementList
    Inherits SingularReadOnlyListBase(Of ROProductionTypeBasherRequirementList, ROProductionTypeBasherRequirement)

#Region " Business Methods "

    Public Function GetItemForCameraCount(ByVal NoOfCameras As Integer) As ROProductionTypeBasherRequirement

      For Each child As ROProductionTypeBasherRequirement In Me
        If child.NoOfCamerasFrom <= NoOfCameras AndAlso child.NoOfCamerasTo >= NoOfCameras Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItem(ProductionTypeBasherRequirementID As Integer) As ROProductionTypeBasherRequirement

      For Each child As ROProductionTypeBasherRequirement In Me
        If child.ProductionTypeBasherRequirementID = ProductionTypeBasherRequirementID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Type Basher Requirements"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionTypeBasherRequirementList() As ROProductionTypeBasherRequirementList

      Return New ROProductionTypeBasherRequirementList()

    End Function

    Public Shared Sub BeginGetROProductionTypeBasherRequirementList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionTypeBasherRequirementList)))

      Dim dp As New DataPortal(Of ROProductionTypeBasherRequirementList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionTypeBasherRequirementList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionTypeBasherRequirementList)))

      Dim dp As New DataPortal(Of ROProductionTypeBasherRequirementList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionTypeBasherRequirementList() As ROProductionTypeBasherRequirementList

      Return DataPortal.Fetch(Of ROProductionTypeBasherRequirementList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionTypeBasherRequirement.GetROProductionTypeBasherRequirement(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionTypeBasherRequirementList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace