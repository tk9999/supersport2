﻿' Generated 31 May 2014 18:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Specs.ReadOnly.Old

  <Serializable()> _
  Public Class ROProductionSpecRequirementEquipmentType
    Inherits SingularReadOnlyBase(Of ROProductionSpecRequirementEquipmentType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionSpecRequirementEquipmentTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSpecRequirementEquipmentTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionSpecRequirementEquipmentTypeID() As Integer
      Get
        Return GetProperty(ProductionSpecRequirementEquipmentTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSpecRequirementID, "Production Spec Requirement", Nothing)
    ''' <summary>
    ''' Gets the Production Spec Requirement value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionSpecRequirementID() As Integer?
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
    End Property

    Public Shared EquipmentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentTypeID, "Equipment Type", Nothing)
    ''' <summary>
    ''' Gets the Equipment Type value
    ''' </summary>
    <Display(Name:="Equipment Type", Description:="The general type of equipment that is required for the production spec")>
    Public ReadOnly Property EquipmentTypeID() As Integer?
      Get
        Return GetProperty(EquipmentTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentSubTypeID, "Equipment Sub Type", Nothing)
    ''' <summary>
    ''' Gets the Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Equipment Sub Type", Description:="The specific type of equipment that is required for the production spec")>
    Public ReadOnly Property EquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(EquipmentSubTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentQuantityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentQuantity, "Equipment Quantity", 0)
    ''' <summary>
    ''' Gets the Equipment Quantity value
    ''' </summary>
    <Display(Name:="Equipment Quantity", Description:="Number of a specific equipment type used for this production spec")>
    Public ReadOnly Property EquipmentQuantity() As Integer
      Get
        Return GetProperty(EquipmentQuantityProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The prodtion the the equipment type requirements relate to")>
    Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 0)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSpecRequirementEquipmentTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionSpecRequirementEquipmentType(dr As SafeDataReader) As ROProductionSpecRequirementEquipmentType

      Dim r As New ROProductionSpecRequirementEquipmentType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionSpecRequirementEquipmentTypeIDProperty, .GetInt32(0))
        LoadProperty(ProductionSpecRequirementIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(EquipmentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(EquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(EquipmentQuantityProperty, .GetInt32(4))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(RoomIDProperty, ZeroNothing(.GetInt32(12)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace