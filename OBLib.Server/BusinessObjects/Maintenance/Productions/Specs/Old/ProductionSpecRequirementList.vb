﻿' Generated 31 May 2014 18:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Specs.Old

  <Serializable()> _
  Public Class ProductionSpecRequirementList
    Inherits SingularBusinessListBase(Of ProductionSpecRequirementList, ProductionSpecRequirement)

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementID As Integer) As ProductionSpecRequirement

      For Each child As ProductionSpecRequirement In Me
        If child.ProductionSpecRequirementID = ProductionSpecRequirementID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirements"

    End Function

    Public Function GetProductionSpecRequirementEquipmentType(ProductionSpecRequirementEquipmentTypeID As Integer) As ProductionSpecRequirementEquipmentType

      Dim obj As ProductionSpecRequirementEquipmentType = Nothing
      For Each parent As ProductionSpecRequirement In Me
        obj = parent.ProductionSpecRequirementEquipmentTypeList.GetItem(ProductionSpecRequirementEquipmentTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetProductionSpecRequirementPosition(ProductionSpecRequirementPositionID As Integer) As ProductionSpecRequirementPosition

      Dim obj As ProductionSpecRequirementPosition = Nothing
      For Each parent As ProductionSpecRequirement In Me
        obj = parent.ProductionSpecRequirementPositionList.GetItem(ProductionSpecRequirementPositionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionSpecRequirementID As Integer? = Nothing
      Public Property ProductionID As Integer? = Nothing
      Public Property ProductionTypeID As Integer? = Nothing
      Public Property EventTypeID As Integer? = Nothing

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", OBLib.Security.Settings.CurrentUser.SystemID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
      Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area is required")>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Property UnlinkedOnly As Boolean = True

      Public Sub New(ProductionSpecRequirementID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?,
                     UnlinkedOnly As Boolean)
        Me.ProductionSpecRequirementID = ProductionSpecRequirementID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.UnlinkedOnly = UnlinkedOnly
      End Sub

      Public Sub New(ProductionSpecRequirementID As Integer?, ProductionID As Integer?,
                     ProductionTypeID As Integer?, EventTypeID As Integer?,
                     SystemID As Integer?, ProductionAreaID As Integer?,
                     UnlinkedOnly As Boolean)
        Me.ProductionSpecRequirementID = ProductionSpecRequirementID
        Me.ProductionID = ProductionID
        Me.ProductionTypeID = ProductionTypeID
        Me.EventTypeID = EventTypeID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.UnlinkedOnly = UnlinkedOnly
      End Sub

      'Public Sub New(ProductionTypeID As Integer?, EventTypeID As Integer?)
      '  Me.ProductionTypeID = ProductionTypeID
      '  Me.EventTypeID = EventTypeID
      'End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionSpecRequirementList() As ProductionSpecRequirementList

      Return New ProductionSpecRequirementList()

    End Function

    Public Shared Sub BeginGetProductionSpecRequirementList(CallBack As EventHandler(Of DataPortalResult(Of ProductionSpecRequirementList)))

      Dim dp As New DataPortal(Of ProductionSpecRequirementList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionSpecRequirementList() As ProductionSpecRequirementList

      Return DataPortal.Fetch(Of ProductionSpecRequirementList)(New Criteria())

    End Function

    Public Shared Function GetProductionSpecRequirementList(ProductionSpecRequirementID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?, UnlinkedOnly As Boolean) As ProductionSpecRequirementList

      Return DataPortal.Fetch(Of ProductionSpecRequirementList)(New Criteria(ProductionSpecRequirementID, SystemID, ProductionAreaID, UnlinkedOnly))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionSpecRequirement.GetProductionSpecRequirement(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ProductionSpecRequirement = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionSpecRequirementID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionSpecRequirementEquipmentTypeList.RaiseListChangedEvents = False
          parent.ProductionSpecRequirementEquipmentTypeList.Add(ProductionSpecRequirementEquipmentType.GetProductionSpecRequirementEquipmentType(sdr))
          parent.ProductionSpecRequirementEquipmentTypeList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionSpecRequirementID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionSpecRequirementPositionList.RaiseListChangedEvents = False
          parent.ProductionSpecRequirementPositionList.Add(ProductionSpecRequirementPosition.GetProductionSpecRequirementPosition(sdr))
          parent.ProductionSpecRequirementPositionList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As ProductionSpecRequirement In Me
        child.CheckRules()
        For Each ProductionSpecRequirementEquipmentType As ProductionSpecRequirementEquipmentType In child.ProductionSpecRequirementEquipmentTypeList
          ProductionSpecRequirementEquipmentType.CheckRules()
        Next
        For Each ProductionSpecRequirementPosition As ProductionSpecRequirementPosition In child.ProductionSpecRequirementPositionList
          ProductionSpecRequirementPosition.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionSpecRequirementListOld"
            cm.Parameters.AddWithValue("@ProductionSpecRequirementID", NothingDBNull(crit.ProductionSpecRequirementID))
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@EventTypeID", NothingDBNull(crit.EventTypeID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(Nothing))
            cm.Parameters.AddWithValue("@UnlinkedOnly", crit.UnlinkedOnly)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionSpecRequirement In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionSpecRequirement In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace