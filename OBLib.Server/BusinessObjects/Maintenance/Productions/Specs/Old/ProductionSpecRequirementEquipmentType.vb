﻿' Generated 31 May 2014 18:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Productions.Studio
Imports OBLib.Productions.Areas
Imports OBLib.Maintenance.Rooms.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Specs.Old

  <Serializable()> _
  Public Class ProductionSpecRequirementEquipmentType
    Inherits SingularBusinessBase(Of ProductionSpecRequirementEquipmentType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionSpecRequirementEquipmentTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSpecRequirementEquipmentTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionSpecRequirementEquipmentTypeID() As Integer
      Get
        Return GetProperty(ProductionSpecRequirementEquipmentTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSpecRequirementID, "Production Spec Requirement", Nothing)
    ''' <summary>
    ''' Gets the Production Spec Requirement value
    ''' </summary>
    Public Property ProductionSpecRequirementID() As Integer?
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionSpecRequirementIDProperty, value)
      End Set
    End Property

    Public Shared EquipmentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EquipmentTypeID, Nothing) _
                                                                         .AddSetExpression("ProductionSpecRequirementEquipmentTypeBO.EquipmentTypeMeetsSpecRequirement(self)")
    ''' <summary>
    ''' Gets and sets the Equipment Type value
    ''' </summary>
    <Display(Name:="Type", Description:="The general type of equipment that is required for the production spec"),
    Required(ErrorMessage:="Type required"),
    DropDownWeb(GetType(ROEquipmentTypeList))>
    Public Property EquipmentTypeID() As Integer?
      Get
        Return GetProperty(EquipmentTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentTypeIDProperty, Value)
      End Set
    End Property
    ', Source:=DropDownWeb.SourceType.ViewModel

    Public Shared EquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EquipmentSubTypeID, Nothing) _
                                                                            .AddSetExpression("ProductionSpecRequirementEquipmentTypeBO.EquipmentTypeMeetsSpecRequirement(self)")
    ''' <summary>
    ''' Gets and sets the Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Sub Type", Description:="The specific type of equipment that is required for the production spec"),
    Required(ErrorMessage:="Sub Type required"),
    DropDownWeb(GetType(ROEquipmentSubTypeList), ThisFilterMember:="EquipmentTypeID")>
    Public Property EquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(EquipmentSubTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentSubTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentQuantityProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.EquipmentQuantity, 0) _
                                                                          .AddSetExpression("ProductionSpecRequirementEquipmentTypeBO.EquipmentTypeMeetsSpecRequirement(self)")
    ''' <summary>
    ''' Gets and sets the Equipment Quantity value
    ''' </summary>
    <Display(Name:="Quantity", Description:="Number of a specific equipment type used for this production spec"),
    Required(ErrorMessage:="Quantity required")>
    Public Property EquipmentQuantity() As Integer
      Get
        Return GetProperty(EquipmentQuantityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EquipmentQuantityProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The prodtion the the equipment type requirements relate to")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SystemID, Nothing) _
                                                                  .AddSetExpression("ProductionSpecRequirementEquipmentTypeBO.EquipmentTypeMeetsSpecRequirement(self)")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    DropDownWeb(GetType(ROSystemList), Source:=DropDownWeb.SourceType.CommonData),
    Required(ErrorMessage:="Sub-Dept is required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaID, Nothing) _
                                                                         .AddSetExpression("ProductionSpecRequirementEquipmentTypeBO.EquipmentTypeMeetsSpecRequirement(self)")
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROProductionAreaList)),
    Required(ErrorMessage:="Area is required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionAreaIDProperty, value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "ProductionSpecRequirementEquipmentTypeBO.ProductionEquipmentTypeToString")

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
    DropDownWeb(GetType(RORoomList), ThisFilterMember:="SystemID")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(RoomIDProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionSpecRequirement

      Dim pr = CType(Me.Parent, ProductionSpecRequirementEquipmentTypeList).Parent
      If TypeOf pr Is ProductionSpecRequirement Then
        Return CType(pr, ProductionSpecRequirement)
      End If
      Return Nothing

    End Function

    Public Function GetParentArea() As ProductionSystemArea

      Dim pr = CType(Me.Parent, ProductionSpecRequirementEquipmentTypeList).Parent
      If TypeOf pr Is ProductionSystemArea Then
        Return CType(pr, ProductionSystemArea)
      End If
      Return Nothing

    End Function

    'Public Function GetParentStudioArea() As StudioProductionArea

    '  Dim pr = CType(Me.Parent, ProductionSpecRequirementEquipmentTypeList).Parent
    '  If TypeOf pr Is StudioProductionArea Then
    '    Return CType(pr, StudioProductionArea)
    '  End If
    '  Return Nothing

    'End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSpecRequirementEquipmentTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Spec Requirement Equipment Type")
        Else
          Return String.Format("Blank {0}", "Production Spec Requirement Equipment Type")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Friend Function Copy() As ProductionSpecRequirementEquipmentType

      Dim obj As New ProductionSpecRequirementEquipmentType
      'obj.Parent = Parent
      'obj.mParentProductionSpec = mParentProductionSpec
      'obj.ProductionSpecRequirementEquipmentTypeID = ProductionSpecRequirementEquipmentTypeID
      obj.ProductionSpecRequirementID = Nothing
      obj.EquipmentTypeID = EquipmentTypeID
      obj.EquipmentSubTypeID = EquipmentSubTypeID
      obj.EquipmentQuantity = EquipmentQuantity
      obj.ProductionID = ProductionID
      obj.SystemID = SystemID
      obj.ProductionAreaID = ProductionAreaID
      obj.CheckRules()
      Return obj

    End Function

    'Friend Function CopyStudio() As StudioProductionAreaEquipment

    '  Dim obj As New StudioProductionAreaEquipment
    '  'obj.Parent = Parent
    '  'obj.mParentProductionSpec = mParentProductionSpec
    '  'obj.ProductionSpecRequirementEquipmentTypeID = ProductionSpecRequirementEquipmentTypeID
    '  obj.ProductionSpecRequirementID = Nothing
    '  obj.EquipmentTypeID = EquipmentTypeID
    '  obj.EquipmentSubTypeID = EquipmentSubTypeID
    '  obj.EquipmentQuantity = EquipmentQuantity
    '  obj.ProductionID = ProductionID
    '  obj.SystemID = SystemID
    '  obj.ProductionAreaID = ProductionAreaID
    '  obj.CheckRules()
    '  Return obj

    'End Function

    'Public Overrides Function GetBackColor() As Object

    '  If mParent Is Nothing Then
    '    Return MyBase.GetBackColor()
    '  Else
    '    If mParent.ROProductionEquipmentList.MeetsEquipmentRequirement(mEquipmentTypeID, mEquipmentSubTypeID, mEquipmentQuantity) Then
    '      Return Drawing.Color.LightSteelBlue
    '    Else
    '      Return Drawing.Color.LightSalmon
    '    End If
    '  End If

    'End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(RoomIDProperty)
        .JavascriptRuleFunctionName = "ProductionSpecRequirementEquipmentTypeBO.RoomIDValid"
        .ServerRuleFunction = AddressOf RoomIdValid
      End With

    End Sub

    Public Shared Function RoomIdValid(PSRET As ProductionSpecRequirementEquipmentType) As String
      Dim ErrorString = ""
      If PSRET.ProductionSpecRequirementID IsNot Nothing Then
        If PSRET.ProductionAreaID = 2 AndAlso Not PSRET.RoomID Then
          ErrorString = "Room is required"
        End If
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionSpecRequirementEquipmentType() method.

    End Sub

    Public Shared Function NewProductionSpecRequirementEquipmentType() As ProductionSpecRequirementEquipmentType

      Return DataPortal.CreateChild(Of ProductionSpecRequirementEquipmentType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionSpecRequirementEquipmentType(dr As SafeDataReader) As ProductionSpecRequirementEquipmentType

      Dim p As New ProductionSpecRequirementEquipmentType()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionSpecRequirementEquipmentTypeIDProperty, .GetInt32(0))
          LoadProperty(ProductionSpecRequirementIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EquipmentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(EquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(EquipmentQuantityProperty, .GetInt32(4))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(SystemIDProperty, .GetInt32(10))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(RoomIDProperty, ZeroNothing(.GetInt32(13)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionSpecRequirementEquipmentTypeOld"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionSpecRequirementEquipmentTypeOld"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionSpecRequirementEquipmentTypeID As SqlParameter = .Parameters.Add("@ProductionSpecRequirementEquipmentTypeID", SqlDbType.Int)
          paramProductionSpecRequirementEquipmentTypeID.Value = NothingDBNull(GetProperty(ProductionSpecRequirementEquipmentTypeIDProperty))
          If Me.IsNew Then
            paramProductionSpecRequirementEquipmentTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EquipmentTypeID", GetProperty(EquipmentTypeIDProperty))
          .Parameters.AddWithValue("@EquipmentSubTypeID", GetProperty(EquipmentSubTypeIDProperty))
          .Parameters.AddWithValue("@EquipmentQuantity", GetProperty(EquipmentQuantityProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUserID)
          If Me.GetParentArea() IsNot Nothing Then 'Production System Area is parent
            .Parameters.AddWithValue("@ProductionSpecRequirementID", Singular.Misc.NothingDBNull(Nothing))
            .Parameters.AddWithValue("@SystemID", Me.GetParentArea.SystemID)
            .Parameters.AddWithValue("@ProductionAreaID", Me.GetParentArea.ProductionAreaID)
            .Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(Me.GetParentArea.ProductionID))
            .Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParentArea.ProductionSystemAreaID)
            .Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
            'ElseIf Me.GetParentStudioArea IsNot Nothing Then 'Production System Area is parent
            '  .Parameters.AddWithValue("@ProductionSpecRequirementID", Singular.Misc.NothingDBNull(Nothing))
            '  .Parameters.AddWithValue("@SystemID", Me.GetParentStudioArea.SystemID)
            '  .Parameters.AddWithValue("@ProductionAreaID", Me.GetParentStudioArea.ProductionAreaID)
            '  .Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(Me.GetParentStudioArea.ProductionID))
            '  .Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParentStudioArea.ProductionSystemAreaID)
            '  .Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
          ElseIf Me.GetParent IsNot Nothing Then 'Spec Requirement is parent
            .Parameters.AddWithValue("@SystemID", Me.GetParent.SystemID)
            .Parameters.AddWithValue("@ProductionAreaID", Me.GetParent.ProductionAreaID)
            .Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(Nothing))
            .Parameters.AddWithValue("@ProductionSpecRequirementID", Singular.Misc.NothingDBNull(Me.GetParent.ProductionSpecRequirementID))
            .Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(Nothing))
            .Parameters.AddWithValue("@RoomID", NothingDBNull(Me.GetParent.RoomID))
          End If

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionSpecRequirementEquipmentTypeIDProperty, paramProductionSpecRequirementEquipmentTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionSpecRequirementEquipmentTypeOld"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionSpecRequirementEquipmentTypeID", GetProperty(ProductionSpecRequirementEquipmentTypeIDProperty))
        If Me.GetParentArea() IsNot Nothing Then 'Production System Area is parent
          cm.Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParentArea.ProductionSystemAreaID)
        Else
          cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(Nothing))
        End If
        cm.Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUserID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace