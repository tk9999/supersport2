﻿' Generated 31 May 2014 18:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Specs.Old

  <Serializable()> _
  Public Class ProductionSpecRequirementPositionList
    Inherits SingularBusinessListBase(Of ProductionSpecRequirementPositionList, ProductionSpecRequirementPosition)

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementPositionID As Integer) As ProductionSpecRequirementPosition

      For Each child As ProductionSpecRequirementPosition In Me
        If child.ProductionSpecRequirementPositionID = ProductionSpecRequirementPositionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetTotalCameraCount() As Integer

      'Dim cnt As Integer = 0
      'For Each child As ProductionSpecRequirementPosition In Me.Where(Function(d) CompareSafe(d.SystemID, CommonData.Enums.System.ProductionServices))
      '  If Not Singular.Misc.IsNullNothing(child.PositionID) AndAlso CompareSafe(CommonData.Lists.ROPositionList.GetItem(child.PositionID).DisciplineID, CommonData.Enums.Discipline.CameraOperator) Then
      '    cnt += IIf(child.EquipmentQuantity > 0, child.EquipmentQuantity, 1)
      '  End If
      'Next
      'Return cnt
      Dim cnt As Integer = Me.Where(Function(d) CompareSafe(d.SystemID, CType(CommonData.Enums.System.ProductionServices, Integer)) And CompareSafe(d.DisciplineID, CType(CommonData.Enums.Discipline.CameraOperator, Integer))).Count
      Return cnt

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirement Positions"

    End Function

    Public Sub PopulateForProduction(ByVal ProductionSpecRequirement As ProductionSpecRequirement, SystemID As Integer, ProductionAreaID As Integer)

      Dim list As ProductionSpecRequirementPositionList = ProductionSpecRequirement.ProductionSpecRequirementPositionList
      For Each child As ProductionSpecRequirementPosition In list
        If CompareSafe(child.SystemID, SystemID) And CompareSafe(child.ProductionAreaID, ProductionAreaID) Then
          Dim NewChild As ProductionSpecRequirementPosition = child.Copy
          Me.Add(NewChild)
        End If
      Next

    End Sub

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewProductionSpecRequirementPositionList() As ProductionSpecRequirementPositionList

      Return New ProductionSpecRequirementPositionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionSpecRequirementPosition In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionSpecRequirementPosition In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace