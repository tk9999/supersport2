﻿' Generated 31 May 2014 18:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Productions.Specs.Old

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.Productions.ReadOnly.Old

Namespace Productions.Specs.Old

  <Serializable()> _
  Public Class ProductionSpecRequirement
    Inherits SingularBusinessBase(Of ProductionSpecRequirement)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSpecRequirementID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property ProductionSpecRequirementID() As Integer
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionSpecRequirementName, "Production Spec Requirement Name", "")
    ''' <summary>
    ''' Gets and sets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Production Spec Requirement Name", Description:="Unique name for this spec requirement"),
    StringLength(100, ErrorMessage:="Production Spec Requirement Name cannot be more than 100 characters"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Spec Name is required")>
    Public Property ProductionSpecRequirementName() As String
      Get
        Return GetProperty(ProductionSpecRequirementNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionSpecRequirementNameProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="The type of production"),
    DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionTypeList))>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventTypeID, "Event Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="The type of event that this production spec is part of (PSL Soccer, Super Rugby)"),
    DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROEventTypeList), ThisFilterMember:="ProductionTypeID")>
    Public Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EventTypeIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="The date that this spec is valid from"),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="The date this spec is valid until. If left NULL then this spec is valid indefinately")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared HDRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HDRequiredInd, "HD Required", True)
    ''' <summary>
    ''' Gets and sets the HD Required value
    ''' </summary>
    <Display(Name:="HD Required", Description:="Tick indicates that HD is required. No tick indicates that HD is not required. NULL indicates that some event require HD")>
    Public Property HDRequiredInd() As Boolean
      Get
        Return GetProperty(HDRequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HDRequiredIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    DropDownWeb(GetType(ROSystemList)),
    Required(ErrorMessage:="Sub-Dept is required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionGradeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionGradeID, "Production Grade", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Grade value
    ''' </summary>
    <Display(Name:="Production Grade", Description:=""),
    Browsable(False)>
    Public Property ProductionGradeID() As Integer?
      Get
        Return GetProperty(ProductionGradeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionGradeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROProductionAreaList)),
    Required(ErrorMessage:="Area is required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionAreaIDProperty, value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
    DropDownWeb(GetType(RORoomList), ThisFilterMember:="SystemID")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(RoomIDProperty, value)
      End Set
    End Property

    Public Shared LinkCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.LinkCount, "Link Count", 0)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Link Count", Description:="")>
    Public Property LinkCount() As Integer
      Get
        Return GetProperty(LinkCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(LinkCountProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionSpecRequirementEquipmentTypeListProperty As PropertyInfo(Of ProductionSpecRequirementEquipmentTypeList) = RegisterProperty(Of ProductionSpecRequirementEquipmentTypeList)(Function(c) c.ProductionSpecRequirementEquipmentTypeList, "Production Spec Requirement Equipment Type List")

    Public ReadOnly Property ProductionSpecRequirementEquipmentTypeList() As ProductionSpecRequirementEquipmentTypeList
      Get
        If GetProperty(ProductionSpecRequirementEquipmentTypeListProperty) Is Nothing Then
          LoadProperty(ProductionSpecRequirementEquipmentTypeListProperty, ProductionSpecRequirementEquipmentTypeList.NewProductionSpecRequirementEquipmentTypeList())
        End If
        Return GetProperty(ProductionSpecRequirementEquipmentTypeListProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementPositionListProperty As PropertyInfo(Of ProductionSpecRequirementPositionList) = RegisterProperty(Of ProductionSpecRequirementPositionList)(Function(c) c.ProductionSpecRequirementPositionList, "Production Spec Requirement Position List")

    Public ReadOnly Property ProductionSpecRequirementPositionList() As ProductionSpecRequirementPositionList
      Get
        If GetProperty(ProductionSpecRequirementPositionListProperty) Is Nothing Then
          LoadProperty(ProductionSpecRequirementPositionListProperty, ProductionSpecRequirementPositionList.NewProductionSpecRequirementPositionList())
        End If
        Return GetProperty(ProductionSpecRequirementPositionListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSpecRequirementIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionSpecRequirementName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Spec Requirement")
        Else
          Return String.Format("Blank {0}", "Production Spec Requirement")
        End If
      Else
        Return Me.ProductionSpecRequirementName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ProductionSpecRequirementEquipmentTypes", "ProductionSpecRequirementPositions"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(ProductionTypeIDProperty)
        .JavascriptRuleFunctionName = "ProductionSpecRequirementBO.ProductionTypeIDValid"
        .ServerRuleFunction = AddressOf ProductionTypeIDValid
      End With

      With AddWebRule(EventTypeIDProperty)
        .JavascriptRuleFunctionName = "ProductionSpecRequirementBO.EventTypeIDValid"
        .ServerRuleFunction = AddressOf EventTypeIDValid
      End With

      With AddWebRule(RoomIDProperty)
        .JavascriptRuleFunctionName = "ProductionSpecRequirementBO.RoomIDValid"
        .ServerRuleFunction = AddressOf EventTypeIDValid
      End With

      With AddWebRule(ProductionSpecRequirementNameProperty,
                Function(c) c.ProductionSpecRequirementName = "",
                Function(c) "Spec Name is required")
        .ServerRuleFunction = AddressOf ProductionSpecRequirementNameValid
        .ASyncBusyText = "Checking if Spec Name exists..."
        .AddTriggerProperty(SystemIDProperty)
        .AddTriggerProperty(ProductionAreaIDProperty)
        .AddTriggerProperty(RoomIDProperty)
      End With

    End Sub

    Public Shared Function ProductionTypeIDValid(PSR As ProductionSpecRequirement) As String
      Dim ErrorString = ""
      If PSR.ProductionAreaID = 1 Then
        If PSR.ProductionTypeID Is Nothing Then
          ErrorString = "Production Type is required"
        End If
      End If
      Return ErrorString
    End Function

    Public Shared Function EventTypeIDValid(PSR As ProductionSpecRequirement) As String
      Dim ErrorString = ""
      If PSR.ProductionAreaID = 1 Then
        If PSR.EventTypeID Is Nothing Then
          ErrorString = "Event Type is required"
        End If
      End If
      Return ErrorString
    End Function

    Public Shared Function RoomIDValid(PSR As ProductionSpecRequirement) As String
      Dim ErrorString = ""
      If PSR.ProductionAreaID = 2 Then
        If PSR.RoomID Is Nothing Then
          ErrorString = "Room is required"
        End If
      End If
      Return ErrorString
    End Function

    Public Function ProductionSpecRequirementNameValid(ProductionSpecRequirement As ProductionSpecRequirement) As String
      Dim ErrorMsg As String = ""
      Dim cmd As New Singular.CommandProc("[cmdProcs].[cmdProductionSpecRequirementNameExists]",
                                          New String() {"ProductionSpecRequirementID",
                                                        "ProductionSpecRequirementName",
                                                        "SystemID",
                                                        "ProductionAreaID",
                                                        "RoomID"},
                                          New Object() {
                                                        NothingDBNull(ProductionSpecRequirement.ProductionSpecRequirementID),
                                                        ProductionSpecRequirement.ProductionSpecRequirementName,
                                                        NothingDBNull(ProductionSpecRequirement.SystemID),
                                                        NothingDBNull(ProductionSpecRequirement.ProductionAreaID),
                                                        NothingDBNull(ProductionSpecRequirement.RoomID)
                                                       })
      cmd.FetchType = CommandProc.FetchTypes.DataObject
      cmd = cmd.Execute
      Dim Count As Integer = cmd.DataObject
      If Count > 0 Then
        Return "A spec with this name already exists"
      End If
      Return ErrorMsg
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionSpecRequirement() method.

    End Sub

    Public Shared Function NewProductionSpecRequirement() As ProductionSpecRequirement

      Return DataPortal.CreateChild(Of ProductionSpecRequirement)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionSpecRequirement(dr As SafeDataReader) As ProductionSpecRequirement

      Dim p As New ProductionSpecRequirement()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionSpecRequirementIDProperty, .GetInt32(0))
          LoadProperty(ProductionSpecRequirementNameProperty, .GetString(1))
          LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(StartDateProperty, .GetValue(4))
          LoadProperty(EndDateProperty, .GetValue(5))
          LoadProperty(HDRequiredIndProperty, .GetBoolean(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(SystemIDProperty, .GetInt32(11))
          LoadProperty(ProductionGradeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(RoomIDProperty, ZeroNothing(.GetInt32(14)))
          LoadProperty(LinkCountProperty, .GetInt32(15))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionSpecRequirementOld"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionSpecRequirementOld"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionSpecRequirementID As SqlParameter = .Parameters.Add("@ProductionSpecRequirementID", SqlDbType.Int)
          paramProductionSpecRequirementID.Value = GetProperty(ProductionSpecRequirementIDProperty)
          If Me.IsNew Then
            paramProductionSpecRequirementID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionSpecRequirementName", GetProperty(ProductionSpecRequirementNameProperty))
          .Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(GetProperty(ProductionTypeIDProperty)))
          .Parameters.AddWithValue("@EventTypeID", NothingDBNull(GetProperty(EventTypeIDProperty)))
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@HDRequiredInd", Singular.Misc.NothingDBNull(GetProperty(HDRequiredIndProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionGradeID", Singular.Misc.NothingDBNull(GetProperty(ProductionGradeIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(GetProperty(RoomIDProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionSpecRequirementIDProperty, paramProductionSpecRequirementID.Value)
          End If
          ' update child objects
          If GetProperty(ProductionSpecRequirementEquipmentTypeListProperty) IsNot Nothing Then
            Me.ProductionSpecRequirementEquipmentTypeList.Update()
          End If
          If GetProperty(ProductionSpecRequirementPositionListProperty) IsNot Nothing Then
            Me.ProductionSpecRequirementPositionList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ProductionSpecRequirementEquipmentTypeListProperty) IsNot Nothing Then
          Me.ProductionSpecRequirementEquipmentTypeList.Update()
        End If
        If GetProperty(ProductionSpecRequirementPositionListProperty) IsNot Nothing Then
          Me.ProductionSpecRequirementPositionList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionSpecRequirementOld"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionSpecRequirementID", GetProperty(ProductionSpecRequirementIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace