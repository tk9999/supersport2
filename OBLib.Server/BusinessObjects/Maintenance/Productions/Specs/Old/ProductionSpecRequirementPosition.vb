﻿' Generated 31 May 2014 18:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Productions.Studio
Imports OBLib.Productions.Areas
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Productions.Old

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Specs.Old

  <Serializable()> _
  Public Class ProductionSpecRequirementPosition
    Inherits SingularBusinessBase(Of ProductionSpecRequirementPosition)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionSpecRequirementPositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSpecRequirementPositionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionSpecRequirementPositionID() As Integer
      Get
        Return GetProperty(ProductionSpecRequirementPositionIDProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSpecRequirementID, "Production Spec Requirement", Nothing)
    ''' <summary>
    ''' Gets the Production Spec Requirement value
    ''' </summary>
    Public Property ProductionSpecRequirementID() As Integer?
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionSpecRequirementIDProperty, value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.PositionID, Nothing) _
                                                                    .AddSetExpression("ProductionSpecRequirementPositionBO.PositionMeetsSpecRequirement(self)")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="The position that will be required"),
    DropDownWeb(GetType(ROPositionList), ThisFilterMember:="DisciplineID")>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property
    ', Source:=DropDownWeb.SourceType.ViewModel

    Public Shared EquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EquipmentSubTypeID, Nothing) _
                                                                            .AddSetExpression("ProductionSpecRequirementPositionBO.PositionMeetsSpecRequirement(self)")
    ''' <summary>
    ''' Gets and sets the Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Sub Type", Description:="The specific type of equipment that is required at this position"),
    DropDownWeb(GetType(ROEquipmentSubTypeList))>
    Public Property EquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(EquipmentSubTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentSubTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentQuantityProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.EquipmentQuantity, 0) _
                                                                          .AddSetExpression("ProductionSpecRequirementPositionBO.PositionMeetsSpecRequirement(self)")
    ''' <summary>
    ''' Gets and sets the Equipment Quantity value
    ''' </summary>
    <Display(Name:="Quantity", Description:="Number of a specific equipment type used for this production spec")>
    Public Property EquipmentQuantity() As Integer
      Get
        Return GetProperty(EquipmentQuantityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EquipmentQuantityProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The prodtion the the position requirements relate to")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SystemID, Nothing) _
                                                                  .AddSetExpression("ProductionSpecRequirementPositionBO.PositionMeetsSpecRequirement(self)")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="system linked to"),
    DropDownWeb(GetType(ROSystemList)),
    Required(ErrorMessage:="Sub-Dept is required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.DisciplineID, Nothing) _
                                                                      .AddSetExpression("ProductionSpecRequirementPositionBO.PositionMeetsSpecRequirement(self)")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="The Descipline of the Production Req. Position record"),
    DropDownWeb(GetType(RODisciplineList)),
    Required(ErrorMessage:="Discipline is required")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property
    ', Source:=DropDownWeb.SourceType.ViewModel

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaID, Nothing) _
                                                                          .AddSetExpression("ProductionSpecRequirementPositionBO.PositionMeetsSpecRequirement(self)")
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROProductionAreaList)),
    Required(ErrorMessage:="Area is required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionAreaIDProperty, value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "ProductionSpecRequirementPositionBO.ProductionSpecPositionToString")

    <Browsable(False)> _
    Public ReadOnly Property Priority() As Integer
      Get
        If PositionID Is Nothing Then
          Return 0
        Else
          Return CommonData.Lists.ROPositionList.GetItem(PositionID).Priority
        End If
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
    DropDownWeb(GetType(RORoomList), ThisFilterMember:="SystemID")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(RoomIDProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionSpecRequirement

      Dim pr = CType(Me.Parent, ProductionSpecRequirementPositionList).Parent
      If TypeOf pr Is ProductionSpecRequirement Then
        Return CType(pr, ProductionSpecRequirement)
      End If
      Return Nothing

    End Function

    Public Function GetParentArea() As ProductionSystemArea

      Dim pr = CType(Me.Parent, ProductionSpecRequirementPositionList).Parent
      If TypeOf pr Is ProductionSystemArea Then
        Return CType(pr, ProductionSystemArea)
      End If
      Return Nothing

    End Function

    'Public Function GetParentStudioArea() As StudioProductionArea

    '  Dim pr = CType(Me.Parent, ProductionSpecRequirementPositionList).Parent
    '  If TypeOf pr Is StudioProductionArea Then
    '    Return CType(pr, StudioProductionArea)
    '  End If
    '  Return Nothing

    'End Function

    Public Function GetParentProduction() As Production

      Dim pr = CType(Me.Parent, ProductionSpecRequirementPositionList).Parent
      If TypeOf pr Is Production Then
        Return CType(pr, Production)
      End If
      Return Nothing

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSpecRequirementPositionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Spec Requirement Position")
        Else
          Return String.Format("Blank {0}", "Production Spec Requirement Position")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Friend Function Copy() As ProductionSpecRequirementPosition

      Dim obj As New ProductionSpecRequirementPosition
      'obj.Parent = Parent
      'obj.ParentProductionSpec = ParentProductionSpec
      obj.ProductionSpecRequirementID = Nothing
      obj.PositionID = PositionID
      obj.EquipmentSubTypeID = EquipmentSubTypeID
      obj.EquipmentQuantity = EquipmentQuantity
      obj.ProductionID = ProductionID
      obj.SystemID = SystemID
      obj.DisciplineID = DisciplineID
      obj.ProductionAreaID = ProductionAreaID
      obj.CheckRules()
      Return obj

    End Function

    'Friend Function CopyStudio() As StudioProductionAreaPosition

    '  Dim obj As New StudioProductionAreaPosition
    '  'obj.Parent = Parent
    '  'obj.ParentProductionSpec = ParentProductionSpec
    '  obj.ProductionSpecRequirementID = Nothing
    '  obj.PositionID = PositionID
    '  obj.EquipmentSubTypeID = EquipmentSubTypeID
    '  obj.EquipmentQuantity = EquipmentQuantity
    '  obj.ProductionID = ProductionID
    '  obj.SystemID = SystemID
    '  obj.DisciplineID = DisciplineID
    '  obj.ProductionAreaID = ProductionAreaID
    '  obj.CheckRules()
    '  Return obj

    'End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(RoomIDProperty)
        .JavascriptRuleFunctionName = "ProductionSpecRequirementPositionBO.RoomIDValid"
        .ServerRuleFunction = AddressOf RoomIdValid
      End With

    End Sub

    Public Shared Function RoomIdValid(PSRP As ProductionSpecRequirementPosition) As String
      Dim ErrorString = ""
      If PSRP.ProductionSpecRequirementID IsNot Nothing Then
        If PSRP.ProductionAreaID = 2 AndAlso PSRP.RoomID IsNot Nothing Then
          ErrorString = "Room is required"
        End If
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionSpecRequirementPosition() method.

    End Sub

    Public Shared Function NewProductionSpecRequirementPosition() As ProductionSpecRequirementPosition

      Return DataPortal.CreateChild(Of ProductionSpecRequirementPosition)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionSpecRequirementPosition(dr As SafeDataReader) As ProductionSpecRequirementPosition

      Dim p As New ProductionSpecRequirementPosition()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionSpecRequirementPositionIDProperty, .GetInt32(0))
          LoadProperty(ProductionSpecRequirementIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(EquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(EquipmentQuantityProperty, .GetInt32(4))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(RoomIDProperty, ZeroNothing(.GetInt32(14)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionSpecRequirementPositionOld"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionSpecRequirementPositionOld"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionSpecRequirementPositionID As SqlParameter = .Parameters.Add("@ProductionSpecRequirementPositionID", SqlDbType.Int)
          paramProductionSpecRequirementPositionID.Value = NothingDBNull(GetProperty(ProductionSpecRequirementPositionIDProperty))
          If Me.IsNew Then
            paramProductionSpecRequirementPositionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@PositionID", Singular.Misc.NothingDBNull(GetProperty(PositionIDProperty)))
          .Parameters.AddWithValue("@EquipmentSubTypeID", Singular.Misc.NothingDBNull(GetProperty(EquipmentSubTypeIDProperty)))
          .Parameters.AddWithValue("@EquipmentQuantity", GetProperty(EquipmentQuantityProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@DisciplineID", Singular.Misc.NothingDBNull(GetProperty(DisciplineIDProperty)))
          .Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUserID)
          If Me.GetParentArea() IsNot Nothing Then 'Production System Area is parent
            .Parameters.AddWithValue("@ProductionSpecRequirementID", Singular.Misc.NothingDBNull(Nothing))
            .Parameters.AddWithValue("@SystemID", Me.GetParentArea.SystemID)
            .Parameters.AddWithValue("@ProductionAreaID", Me.GetParentArea.ProductionAreaID)
            .Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(Me.GetParentArea.ProductionID))
            .Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParentArea.ProductionSystemAreaID)
            .Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
            'ElseIf Me.GetParentStudioArea IsNot Nothing Then 'Production System Area is parent
            '  .Parameters.AddWithValue("@ProductionSpecRequirementID", Singular.Misc.NothingDBNull(Nothing))
            '  .Parameters.AddWithValue("@SystemID", Me.GetParentStudioArea.SystemID)
            '  .Parameters.AddWithValue("@ProductionAreaID", Me.GetParentStudioArea.ProductionAreaID)
            '  .Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(Me.GetParentStudioArea.ProductionID))
            '  .Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParentStudioArea.ProductionSystemAreaID)
            '  .Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
          ElseIf Me.GetParent IsNot Nothing Then 'Spec Requirement is parent
            .Parameters.AddWithValue("@SystemID", Me.GetParent.SystemID)
            .Parameters.AddWithValue("@ProductionAreaID", Me.GetParent.ProductionAreaID)
            .Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(Nothing))
            .Parameters.AddWithValue("@ProductionSpecRequirementID", Singular.Misc.NothingDBNull(Me.GetParent.ProductionSpecRequirementID))
            .Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(Nothing))
            .Parameters.AddWithValue("@RoomID", NothingDBNull(Me.GetParent.RoomID))
          End If
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionSpecRequirementPositionIDProperty, paramProductionSpecRequirementPositionID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionSpecRequirementPositionOld"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionSpecRequirementPositionID", GetProperty(ProductionSpecRequirementPositionIDProperty))
        If Me.GetParentArea() IsNot Nothing Then 'Production System Area is parent
          cm.Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParentArea.ProductionSystemAreaID)
        Else
          cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(Nothing))
        End If
        cm.Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUserID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace