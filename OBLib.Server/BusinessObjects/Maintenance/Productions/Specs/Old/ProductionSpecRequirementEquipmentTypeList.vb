﻿' Generated 31 May 2014 18:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Specs.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Specs.Old

  <Serializable()> _
  Public Class ProductionSpecRequirementEquipmentTypeList
    Inherits SingularBusinessListBase(Of ProductionSpecRequirementEquipmentTypeList, ProductionSpecRequirementEquipmentType)

#Region " Business Methods "

    Public Function GetTotalCameraCount() As Integer

      'Dim cnt As Integer = 0
      'For Each child As ProductionSpecRequirementEquipmentType In Me
      '  If Singular.Misc.CompareSafe(child.EquipmentTypeID, CommonData.Enums.EquipmentType.Camera) Then
      '    cnt += child.EquipmentQuantity
      '  End If
      'Next
      'Return cnt
      Dim cnt As Integer = Me.Where(Function(d) CompareSafe(d.EquipmentTypeID, CType(CommonData.Enums.EquipmentType.Camera, Integer))).Count
      Return cnt

    End Function

    Public Function GetItem(ProductionSpecRequirementEquipmentTypeID As Integer) As ProductionSpecRequirementEquipmentType

      For Each child As ProductionSpecRequirementEquipmentType In Me
        If child.ProductionSpecRequirementEquipmentTypeID = ProductionSpecRequirementEquipmentTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirement Equipment Types"

    End Function

    Public Sub PopulateForProduction(ByVal ProductionSpecRequirement As ProductionSpecRequirement, SystemID As Integer, ProductionAreaID As Integer)

      For Each child As ProductionSpecRequirementEquipmentType In ProductionSpecRequirement.ProductionSpecRequirementEquipmentTypeList
        If CompareSafe(child.SystemID, SystemID) And CompareSafe(child.ProductionAreaID, ProductionAreaID) Then
          Dim NewChild As ProductionSpecRequirementEquipmentType = child.Copy
          Me.Add(NewChild)
        End If
      Next

    End Sub

    'Friend Function MeetsEquipmentRequirement(ByVal EquipmentTypeID As Object, ByVal EquipmentSubTypeID As Object, ByVal EquipmentQuantity As Integer) As Boolean

    '  Dim cnt As Integer = 0
    '  For Each child As ROProductionEquipment In Me
    '    If Singular.Misc.CompareSafe(child.EquipmentTypeID, EquipmentTypeID) AndAlso _
    '      Singular.Misc.CompareSafe(child.EquipmentSubTypeID, EquipmentSubTypeID) Then
    '      cnt += 1
    '    End If
    '  Next
    '  If cnt >= EquipmentQuantity Then
    '    Return True
    '  Else
    '    Return False
    '  End If

    'End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewProductionSpecRequirementEquipmentTypeList() As ProductionSpecRequirementEquipmentTypeList

      Return New ProductionSpecRequirementEquipmentTypeList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionSpecRequirementEquipmentType In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionSpecRequirementEquipmentType In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace