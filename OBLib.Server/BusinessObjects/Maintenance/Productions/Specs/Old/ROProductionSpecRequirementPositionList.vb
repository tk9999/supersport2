﻿' Generated 31 May 2014 18:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Specs.ReadOnly.Old

  <Serializable()> _
  Public Class ROProductionSpecRequirementPositionList
    Inherits SingularReadOnlyListBase(Of ROProductionSpecRequirementPositionList, ROProductionSpecRequirementPosition)

#Region " Parent "

    <NotUndoable()> Private mParent As ROProductionSpecRequirementOld
#End Region

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementPositionID As Integer) As ROProductionSpecRequirementPosition

      For Each child As ROProductionSpecRequirementPosition In Me
        If child.ProductionSpecRequirementPositionID = ProductionSpecRequirementPositionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByPositionID(ByVal PositionID As Integer, ByVal EquipmentSubTypeID As Integer?, SystemID As Integer?) As ROProductionSpecRequirementPosition

      For Each child As ROProductionSpecRequirementPosition In Me
        If Singular.Misc.CompareSafe(child.PositionID, PositionID) _
           AndAlso Singular.Misc.CompareSafe(child.EquipmentSubTypeID, EquipmentSubTypeID) _
           AndAlso Singular.Misc.CompareSafe(child.SystemID, SystemID) Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function FindItem(DisciplineID As Integer?, ByVal PositionID As Integer?,
                             SystemID As Integer?, ProductionAreaID As Integer?) As ROProductionSpecRequirementPosition

      For Each child As ROProductionSpecRequirementPosition In Me
        If Singular.Misc.CompareSafe(child.DisciplineID, DisciplineID) _
           AndAlso Singular.Misc.CompareSafe(child.PositionID, PositionID) _
           AndAlso Singular.Misc.CompareSafe(child.SystemID, SystemID) _
           AndAlso Singular.Misc.CompareSafe(child.ProductionAreaID, ProductionAreaID) Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirement Positions"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROProductionSpecRequirementPositionList() As ROProductionSpecRequirementPositionList

      Return New ROProductionSpecRequirementPositionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace