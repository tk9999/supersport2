﻿' Generated 31 May 2014 18:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Specs.ReadOnly.Old

  <Serializable()> _
  Public Class ROProductionSpecRequirementListOld
    Inherits SingularReadOnlyListBase(Of ROProductionSpecRequirementListOld, ROProductionSpecRequirementOld)

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementID As Integer) As ROProductionSpecRequirementOld

      For Each child As ROProductionSpecRequirementOld In Me
        If child.ProductionSpecRequirementID = ProductionSpecRequirementID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItems(RoomID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?) As ROProductionSpecRequirementListOld

      Dim rs As New ROProductionSpecRequirementListOld

      For Each child As ROProductionSpecRequirementOld In Me
        If CompareSafe(child.RoomID, RoomID) AndAlso CompareSafe(child.SystemID, SystemID) AndAlso CompareSafe(child.ProductionAreaID, ProductionAreaID) Then
          rs.Add(child)
        End If
      Next
      Return rs

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirements"

    End Function

    Public Function GetROProductionSpecRequirementEquipmentType(ProductionSpecRequirementEquipmentTypeID As Integer) As ROProductionSpecRequirementEquipmentType

      Dim obj As ROProductionSpecRequirementEquipmentType = Nothing
      For Each parent As ROProductionSpecRequirementOld In Me
        obj = parent.ROProductionSpecRequirementEquipmentTypeList.GetItem(ProductionSpecRequirementEquipmentTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROProductionSpecRequirementPosition(ProductionSpecRequirementPositionID As Integer) As ROProductionSpecRequirementPosition

      Dim obj As ROProductionSpecRequirementPosition = Nothing
      For Each parent As ROProductionSpecRequirementOld In Me
        obj = parent.ROProductionSpecRequirementPositionList.GetItem(ProductionSpecRequirementPositionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionSpecRequirementID As Integer? = Nothing
      Public Property ProductionID As Integer? = Nothing
      Public Property ProductionTypeID As Integer? = Nothing
      Public Property EventTypeID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property UnlinkedOnly As Boolean = False

      Public Sub New(ProductionSpecRequirementID As Integer?, ProductionID As Integer?,
                     ProductionTypeID As Integer?, EventTypeID As Integer?,
                     SystemID As Integer?, ProductionAreaID As Integer?)
        Me.ProductionSpecRequirementID = ProductionSpecRequirementID
        Me.ProductionID = ProductionID
        Me.ProductionTypeID = ProductionTypeID
        Me.EventTypeID = EventTypeID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New(UnlinkedOnly As Boolean)
        Me.UnlinkedOnly = UnlinkedOnly
      End Sub

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionSpecRequirementListOld() As ROProductionSpecRequirementListOld

      Return New ROProductionSpecRequirementListOld()

    End Function

    Public Shared Sub BeginGetROProductionSpecRequirementListOld(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionSpecRequirementListOld)))

      Dim dp As New DataPortal(Of ROProductionSpecRequirementListOld)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionSpecRequirementListOld(CallBack As EventHandler(Of DataPortalResult(Of ROProductionSpecRequirementListOld)))

      Dim dp As New DataPortal(Of ROProductionSpecRequirementListOld)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionSpecRequirementListOld() As ROProductionSpecRequirementListOld

      Return DataPortal.Fetch(Of ROProductionSpecRequirementListOld)(New Criteria())

    End Function

    Public Shared Function GetROProductionSpecRequirementListOld(UnlinkedOnly As Boolean) As ROProductionSpecRequirementListOld

      Return DataPortal.Fetch(Of ROProductionSpecRequirementListOld)(New Criteria(UnlinkedOnly))

    End Function

    Public Shared Function GetROProductionSpecRequirementListOld(SystemID As Integer?, ProductionAreaID As Integer?) As ROProductionSpecRequirementListOld

      Return DataPortal.Fetch(Of ROProductionSpecRequirementListOld)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionSpecRequirementOld.GetROProductionSpecRequirementOld(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROProductionSpecRequirementOld = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionSpecRequirementID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          If parent IsNot Nothing Then
            parent.ROProductionSpecRequirementEquipmentTypeList.RaiseListChangedEvents = False
            parent.ROProductionSpecRequirementEquipmentTypeList.Add(ROProductionSpecRequirementEquipmentType.GetROProductionSpecRequirementEquipmentType(sdr))
            parent.ROProductionSpecRequirementEquipmentTypeList.RaiseListChangedEvents = True
          End If
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionSpecRequirementID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          If parent IsNot Nothing Then
            parent.ROProductionSpecRequirementPositionList.RaiseListChangedEvents = False
            parent.ROProductionSpecRequirementPositionList.Add(ROProductionSpecRequirementPosition.GetROProductionSpecRequirementPosition(sdr))
            parent.ROProductionSpecRequirementPositionList.RaiseListChangedEvents = True
          End If
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionSpecRequirementListOld"
            cm.Parameters.AddWithValue("@ProductionSpecRequirementID", NothingDBNull(crit.ProductionSpecRequirementID))
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@EventTypeID", NothingDBNull(crit.EventTypeID))
            cm.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID) 'NothingDBNull(crit.SystemID)
            cm.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID) 'NothingDBNull(crit.ProductionAreaID)
            'cm.Parameters.AddWithValue("@UnlinkedOnly", crit.UnlinkedOnly)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace