﻿' Generated 31 May 2014 18:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Specs.ReadOnly

  <Serializable()> _
  Public Class ROProductionSpecRequirementPagedEquipmentTypeList
    Inherits OBReadOnlyListBase(Of ROProductionSpecRequirementPagedEquipmentTypeList, ROProductionSpecRequirementPagedEquipmentType)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementEquipmentTypeID As Integer) As ROProductionSpecRequirementPagedEquipmentType

      For Each child As ROProductionSpecRequirementPagedEquipmentType In Me
        If child.ProductionSpecRequirementEquipmentTypeID = ProductionSpecRequirementEquipmentTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirement Equipment Types"

    End Function

    Public Function GetItemByEquipmentTypeID(ByVal EquipmentTypeID As Integer, ByVal EquipmentSubTypeID As Integer?, SystemID As Integer?) As ROProductionSpecRequirementPagedEquipmentType

      For Each child As ROProductionSpecRequirementPagedEquipmentType In Me
        If Singular.Misc.CompareSafe(child.EquipmentTypeID, EquipmentTypeID) _
           AndAlso Singular.Misc.CompareSafe(child.EquipmentSubTypeID, EquipmentSubTypeID) _
           AndAlso Singular.Misc.CompareSafe(child.SystemID, SystemID) Then
          Return child
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Common "

    <Serializable(), Singular.Web.WebFetchable()> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area ID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Spec Requirement ID", Description:="")>
      Public Property ProductionSystemAreaID() As Integer?
        Get
          Return ReadProperty(ProductionSystemAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionSystemAreaIDProperty, Value)
        End Set
      End Property

      Public Sub New(ProductionSystemAreaID As Integer?)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Data Access "

    Public Shared Function NewROProductionSpecRequirementPagedEquipmentTypeList() As ROProductionSpecRequirementPagedEquipmentTypeList

      Return New ROProductionSpecRequirementPagedEquipmentTypeList()

    End Function

    Public Shared Function GetROProductionSpecRequirementPagedEquipmentTypeList(ProductionSystemAreaID As Integer?) As ROProductionSpecRequirementPagedEquipmentTypeList

      Return DataPortal.Fetch(Of ROProductionSpecRequirementPagedEquipmentTypeList)(New Criteria(ProductionSystemAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionSpecRequirementPagedEquipmentType.GetROProductionSpecRequirementPagedEquipmentType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionSpecEquipmentTypePagedList"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace