﻿' Generated 31 May 2014 18:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Specs.ReadOnly

  <Serializable()> _
  Public Class ROProductionSpecRequirementPagedPositionList
    Inherits OBReadOnlyListBase(Of ROProductionSpecRequirementPagedPositionList, ROProductionSpecRequirementPagedPosition)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementPositionID As Integer) As ROProductionSpecRequirementPagedPosition

      For Each child As ROProductionSpecRequirementPagedPosition In Me
        If child.ProductionSpecRequirementPositionID = ProductionSpecRequirementPositionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByPositionID(ByVal PositionID As Integer, ByVal EquipmentSubTypeID As Integer?, SystemID As Integer?) As ROProductionSpecRequirementPagedPosition

      For Each child As ROProductionSpecRequirementPagedPosition In Me
        If Singular.Misc.CompareSafe(child.PositionID, PositionID) _
           AndAlso Singular.Misc.CompareSafe(child.EquipmentSubTypeID, EquipmentSubTypeID) _
           AndAlso Singular.Misc.CompareSafe(child.SystemID, SystemID) Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirement Positions"

    End Function

#End Region

#Region " Common "

    <Serializable(), Singular.Web.WebFetchable()> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area ID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Spec Requirement ID", Description:="")>
      Public Property ProductionSystemAreaID() As Integer?
        Get
          Return ReadProperty(ProductionSystemAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionSystemAreaIDProperty, Value)
        End Set
      End Property

      Public Sub New(ProductionSystemAreaID As Integer?)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Data Access "

    Public Shared Function NewROProductionSpecRequirementPagedPositionList() As ROProductionSpecRequirementPagedPositionList

      Return New ROProductionSpecRequirementPagedPositionList()

    End Function

    Public Shared Function GetROProductionSpecRequirementPagedList(ProductionSystemAreaID As Integer?) As ROProductionSpecRequirementPagedList

      Return DataPortal.Fetch(Of ROProductionSpecRequirementPagedList)(New Criteria(ProductionSystemAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionSpecRequirementPagedPosition.GetROProductionSpecRequirementPagedPosition(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionSpecPositionPagedList"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace