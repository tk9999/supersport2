﻿' Generated 31 May 2014 18:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Specs.ReadOnly

  <Serializable()> _
  Public Class ROProductionSpecRequirementPagedList
    Inherits SingularReadOnlyListBase(Of ROProductionSpecRequirementPagedList, ROProductionSpecRequirementPaged)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementID As Integer) As ROProductionSpecRequirementPaged

      For Each child As ROProductionSpecRequirementPaged In Me
        If child.ProductionSpecRequirementID = ProductionSpecRequirementID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItems(RoomID As Integer, SystemID As Integer?, ProductionAreaID As Integer?) As ROProductionSpecRequirementPagedList

      Dim rs As New ROProductionSpecRequirementPagedList

      For Each child As ROProductionSpecRequirementPaged In Me
        If CompareSafe(child.RoomID, RoomID) AndAlso CompareSafe(child.SystemID, SystemID) AndAlso CompareSafe(child.ProductionAreaID, ProductionAreaID) Then
          rs.Add(child)
        End If
      Next
      Return rs

    End Function

    Public Overrides Function ToString() As String

      Return "Production Spec Requirements"

    End Function

    Public Function GetROProductionSpecRequirementPagedEquipmentType(ProductionSpecRequirementEquipmentTypeID As Integer) As ROProductionSpecRequirementPagedEquipmentType

      Dim obj As ROProductionSpecRequirementPagedEquipmentType = Nothing
      For Each parent As ROProductionSpecRequirementPaged In Me
        obj = parent.ROProductionSpecRequirementPagedEquipmentTypeList.GetItem(ProductionSpecRequirementEquipmentTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROProductionSpecRequirementPagedPosition(ProductionSpecRequirementPositionID As Integer) As ROProductionSpecRequirementPagedPosition

      Dim obj As ROProductionSpecRequirementPagedPosition = Nothing
      For Each parent As ROProductionSpecRequirementPaged In Me
        obj = parent.ROProductionSpecRequirementPagedPositionList.GetItem(ProductionSpecRequirementPositionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable()> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared ProductionSpecRequirementNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionSpecRequirementName, "Name", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Name", Description:="")>
      Public Property ProductionSpecRequirementName() As String
        Get
          Return ReadProperty(ProductionSpecRequirementNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionSpecRequirementNameProperty, Value)
        End Set
      End Property

      Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSpecRequirementID, "Spec Requirement ID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Spec Requirement ID", Description:="")>
      Public Property ProductionSpecRequirementID() As Integer?
        Get
          Return ReadProperty(ProductionSpecRequirementIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionSpecRequirementIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production", Description:="")>
      Public Property ProductionID() As Integer?
        Get
          Return ReadProperty(ProductionIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production Type", Description:="")>
      Public Property ProductionTypeID() As Integer?
        Get
          Return ReadProperty(ProductionTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionTypeIDProperty, Value)
        End Set
      End Property

      Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventTypeID, "Event Type", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Event Type", Description:="")>
      Public Property EventTypeID() As Integer?
        Get
          Return ReadProperty(EventTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(EventTypeIDProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
      Required(ErrorMessage:="Sub-Dept is required"),
      DropDownWeb(GetType(ROSystemList))>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area is required"),
      DropDownWeb(GetType(ROProductionAreaList))>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Room", Description:="")>
      Public Property RoomID() As Integer?
        Get
          Return ReadProperty(RoomIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(RoomIDProperty, Value)
        End Set
      End Property

      Public Property UnlinkedOnly As Boolean = False

      Public Sub New(ProductionSpecRequirementID As Integer?, ProductionID As Integer?,
                     ProductionTypeID As Integer?, EventTypeID As Integer?,
                     SystemID As Integer?, ProductionAreaID As Integer?)
        Me.ProductionSpecRequirementID = ProductionSpecRequirementID
        Me.ProductionID = ProductionID
        Me.ProductionTypeID = ProductionTypeID
        Me.EventTypeID = EventTypeID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New(UnlinkedOnly As Boolean)
        Me.UnlinkedOnly = UnlinkedOnly
      End Sub

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?,
                     RoomID As Integer?,
                     PageNo As Integer, PageSize As Integer, SortAsc As Boolean, SortColumn As String)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.RoomID = RoomID
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionSpecRequirementPagedList() As ROProductionSpecRequirementPagedList

      Return New ROProductionSpecRequirementPagedList()

    End Function

    Public Shared Sub BeginGetROProductionSpecRequirementPagedList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionSpecRequirementPagedList)))

      Dim dp As New DataPortal(Of ROProductionSpecRequirementPagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionSpecRequirementPagedList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionSpecRequirementPagedList)))

      Dim dp As New DataPortal(Of ROProductionSpecRequirementPagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionSpecRequirementPagedList() As ROProductionSpecRequirementPagedList

      Return DataPortal.Fetch(Of ROProductionSpecRequirementPagedList)(New Criteria())

    End Function

    Public Shared Function GetROProductionSpecRequirementPagedList(UnlinkedOnly As Boolean) As ROProductionSpecRequirementPagedList

      Return DataPortal.Fetch(Of ROProductionSpecRequirementPagedList)(New Criteria(UnlinkedOnly))

    End Function

    Public Shared Function GetROProductionSpecRequirementPagedList(SystemID As Integer?, ProductionAreaID As Integer?) As ROProductionSpecRequirementPagedList

      Return DataPortal.Fetch(Of ROProductionSpecRequirementPagedList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Public Shared Function GetROProductionSpecRequirementPagedList(SystemID As Integer?, ProductionAreaID As Integer?, RoomID As Integer?,
                                                                   PageNo As Integer, PageSize As Integer, SortAsc As Boolean, SortColumn As String) As ROProductionSpecRequirementPagedList

      Return DataPortal.Fetch(Of ROProductionSpecRequirementPagedList)(New Criteria(SystemID, ProductionAreaID, RoomID,
                                                                                    PageNo, PageSize, SortAsc, SortColumn))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionSpecRequirementPaged.GetROProductionSpecRequirementPaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROProductionSpecRequirementPaged = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionSpecRequirementID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          If parent IsNot Nothing Then
            parent.ROProductionSpecRequirementPagedEquipmentTypeList.RaiseListChangedEvents = False
            parent.ROProductionSpecRequirementPagedEquipmentTypeList.Add(ROProductionSpecRequirementPagedEquipmentType.GetROProductionSpecRequirementPagedEquipmentType(sdr))
            parent.ROProductionSpecRequirementPagedEquipmentTypeList.RaiseListChangedEvents = True
          End If
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionSpecRequirementID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          If parent IsNot Nothing Then
            parent.ROProductionSpecRequirementPagedPositionList.RaiseListChangedEvents = False
            parent.ROProductionSpecRequirementPagedPositionList.Add(ROProductionSpecRequirementPagedPosition.GetROProductionSpecRequirementPagedPosition(sdr))
            parent.ROProductionSpecRequirementPagedPositionList.RaiseListChangedEvents = True
          End If
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionSpecRequirementPagedList"
            cm.Parameters.AddWithValue("@ProductionSpecRequirementID", NothingDBNull(crit.ProductionSpecRequirementID))
            cm.Parameters.AddWithValue("@ProductionSpecRequirementName", NothingDBNull(crit.ProductionSpecRequirementName))
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@EventTypeID", NothingDBNull(crit.EventTypeID))
            cm.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID) 'NothingDBNull(crit.SystemID)
            cm.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID) 'NothingDBNull(crit.ProductionAreaID)
            cm.Parameters.AddWithValue("@RoomID", NothingDBNull(crit.RoomID))
            crit.AddParameters(cm)
            'cm.Parameters.AddWithValue("@UnlinkedOnly", crit.UnlinkedOnly)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace