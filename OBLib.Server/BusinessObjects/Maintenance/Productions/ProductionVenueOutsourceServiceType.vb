﻿' Generated 26 Jan 2015 20:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.ReadOnly.Old

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class ProductionVenueOutsourceServiceType
    Inherits SingularBusinessBase(Of ProductionVenueOutsourceServiceType)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared ProductionVenueOutsourceServiceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueOutsourceServiceTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionVenueOutsourceServiceTypeID() As Integer
      Get
        Return GetProperty(ProductionVenueOutsourceServiceTypeIDProperty)
      End Get
    End Property

    Public Shared OutsourceServiceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OutsourceServiceTypeID, "Outsource Service Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Outsource Service Type value
    ''' </summary>
    <Display(Name:="Outsource Service Type", Description:="The outsource service type that is required and the venue"),
    Required(ErrorMessage:="Outsource Service Type required"),
    DropDownWeb(GetType(ROOutsourceServiceTypeList))>
    Public Property OutsourceServiceTypeID() As Integer?
      Get
        Return GetProperty(OutsourceServiceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OutsourceServiceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="The venue that this outsource service type is needed")>
    Public Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="The Production Type the this outsource service type is for. If NULL then it applies to all for this production venue"),
    DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionTypeList))>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionVenue

      Return CType(CType(Me.Parent, ProductionVenueOutsourceServiceTypeList).Parent, ProductionVenue)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueOutsourceServiceTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Venue Outsource Service Type")
        Else
          Return String.Format("Blank {0}", "Production Venue Outsource Service Type")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#Region " Child Lists "

    Public Shared ProductionVenueOutsourceServiceTypeDetailListProperty As PropertyInfo(Of ProductionVenueOutsourceServiceTypeDetailList) = RegisterProperty(Of ProductionVenueOutsourceServiceTypeDetailList)(Function(c) c.ProductionVenueOutsourceServiceTypeDetailList, "ProductionVenueOutsourceServiceTypeDetailList")
    Public ReadOnly Property ProductionVenueOutsourceServiceTypeDetailList() As ProductionVenueOutsourceServiceTypeDetailList
      Get
        If GetProperty(ProductionVenueOutsourceServiceTypeDetailListProperty) Is Nothing Then
          LoadProperty(ProductionVenueOutsourceServiceTypeDetailListProperty, Maintenance.Productions.ProductionVenueOutsourceServiceTypeDetailList.NewProductionVenueOutsourceServiceTypeDetailList())
        End If
        Return GetProperty(ProductionVenueOutsourceServiceTypeDetailListProperty)
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionVenueOutsourceServiceType() method.

    End Sub

    Public Shared Function NewProductionVenueOutsourceServiceType() As ProductionVenueOutsourceServiceType

      Return DataPortal.CreateChild(Of ProductionVenueOutsourceServiceType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionVenueOutsourceServiceType(dr As SafeDataReader) As ProductionVenueOutsourceServiceType

      Dim p As New ProductionVenueOutsourceServiceType()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionVenueOutsourceServiceTypeIDProperty, .GetInt32(0))
          LoadProperty(OutsourceServiceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionVenueOutsourceServiceType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionVenueOutsourceServiceType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionVenueOutsourceServiceTypeID As SqlParameter = .Parameters.Add("@ProductionVenueOutsourceServiceTypeID", SqlDbType.Int)
          paramProductionVenueOutsourceServiceTypeID.Value = GetProperty(ProductionVenueOutsourceServiceTypeIDProperty)
          If Me.IsNew Then
            paramProductionVenueOutsourceServiceTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@OutsourceServiceTypeID", GetProperty(OutsourceServiceTypeIDProperty))
          .Parameters.AddWithValue("@ProductionVenueID", Me.GetParent.ProductionVenueID)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@ProductionTypeID", Singular.Misc.NothingDBNull(GetProperty(ProductionTypeIDProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionVenueOutsourceServiceTypeIDProperty, paramProductionVenueOutsourceServiceTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionVenueOutsourceServiceType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionVenueOutsourceServiceTypeID", GetProperty(ProductionVenueOutsourceServiceTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace