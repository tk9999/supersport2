﻿' Generated 26 Jun 2014 18:34 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class ProductionTimelineTypeDisciplineList
    Inherits SingularBusinessListBase(Of ProductionTimelineTypeDisciplineList, ProductionTimelineTypeDiscipline)

#Region " Business Methods "

    Public Function GetItem(ProductionTimelineTypeDisciplineID As Integer) As ProductionTimelineTypeDiscipline

      For Each child As ProductionTimelineTypeDiscipline In Me
        If child.ProductionTimelineTypeDisciplineID = ProductionTimelineTypeDisciplineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Timeline Type Disciplines"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionTimelineTypeDisciplineList() As ProductionTimelineTypeDisciplineList

      Return New ProductionTimelineTypeDisciplineList()

    End Function

    Public Shared Sub BeginGetProductionTimelineTypeDisciplineList(CallBack As EventHandler(Of DataPortalResult(Of ProductionTimelineTypeDisciplineList)))

      Dim dp As New DataPortal(Of ProductionTimelineTypeDisciplineList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionTimelineTypeDisciplineList() As ProductionTimelineTypeDisciplineList

      Return DataPortal.Fetch(Of ProductionTimelineTypeDisciplineList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionTimelineTypeDiscipline.GetProductionTimelineTypeDiscipline(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionTimelineTypeDisciplineList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionTimelineTypeDiscipline In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionTimelineTypeDiscipline In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace