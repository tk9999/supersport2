﻿' Generated 19 Oct 2014 21:38 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class ProductionVenueList
    Inherits SingularBusinessListBase(Of ProductionVenueList, ProductionVenue)
    'Implements Singular.Paging.IPagedList
    'Implements Singular.Paging.IEditablePagedList

    'Private mTotalRecords As Integer = 0
    'Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords 'Implements Singular.Paging.IEditablePagedList.TotalRecords
    '  Get
    '    Return mTotalRecords
    '  End Get
    'End Property

#Region " Business Methods "

    Public Function GetItem(ProductionVenueID As Integer) As ProductionVenue

      For Each child As ProductionVenue In Me
        If child.ProductionVenueID = ProductionVenueID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetProductionVenueOutsourceServiceType(ProductionVenueOutsourceServiceTypeID As Integer) As ProductionVenueOutsourceServiceType

      For Each child As ProductionVenue In Me
        For Each grandchild As ProductionVenueOutsourceServiceType In child.ProductionVenueOutsourceServiceTypeList
          If grandchild.ProductionVenueOutsourceServiceTypeID = ProductionVenueOutsourceServiceTypeID Then
            Return grandchild
          End If
        Next
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits SingularCriteriaBase(Of Criteria)
      'Inherits Paging.EditablePageCriteria(Of Criteria)

      Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production Type", Description:="")>
      Public Property ProductionTypeID() As Integer?
        Get
          Return ReadProperty(ProductionTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionTypeIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue ID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production Venue", Description:="")>
      Public Property ProductionVenueID() As Integer?
        Get
          Return ReadProperty(ProductionVenueIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionVenueIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionVenue, "") '_
      '.AddSetExpression("FilterProductionVenueList()", False)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Production Venue", Description:="")>
      Public Property ProductionVenue() As String
        Get
          Return ReadProperty(ProductionVenueProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionVenueProperty, Value)
        End Set
      End Property

      Public Sub New(ProductionTypeID As Integer?, ProductionVenue As String)
        Me.ProductionTypeID = ProductionTypeID
        Me.ProductionVenue = ProductionVenue
      End Sub

      Public Sub New(ProductionVenueID As Integer?)
        Me.ProductionVenueID = ProductionVenueID
      End Sub

      Public Sub New(ProductionVenue As String)
        Me.ProductionVenue = ProductionVenue
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionVenueList() As ProductionVenueList

      Return New ProductionVenueList()

    End Function

    Public Shared Sub BeginGetProductionVenueList(CallBack As EventHandler(Of DataPortalResult(Of ProductionVenueList)))

      Dim dp As New DataPortal(Of ProductionVenueList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionVenueList() As ProductionVenueList

      Return DataPortal.Fetch(Of ProductionVenueList)(New Criteria())

    End Function

    Public Shared Function GetProductionVenueList(ProductionVenueID As Integer?) As ProductionVenueList

      Return DataPortal.Fetch(Of ProductionVenueList)(New Criteria(ProductionVenueID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      'sdr.Read()
      'mTotalRecords = sdr.GetInt32(0)
      'sdr.NextResult()

      While sdr.Read
        Me.Add(ProductionVenue.GetProductionVenue(sdr))
      End While

      Dim parent As ProductionVenue = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionVenueID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionVenueProductionTypeList.RaiseListChangedEvents = False
          parent.ProductionVenueProductionTypeList.Add(ProductionVenueProductionType.GetProductionVenueProductionType(sdr))
          parent.ProductionVenueProductionTypeList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionVenueID <> sdr.GetInt32(2) Then
            parent = Me.GetItem(sdr.GetInt32(2))
          End If
          parent.ProductionVenueOutsourceServiceTypeList.RaiseListChangedEvents = False
          parent.ProductionVenueOutsourceServiceTypeList.Add(ProductionVenueOutsourceServiceType.GetProductionVenueOutsourceServiceType(sdr))
          parent.ProductionVenueOutsourceServiceTypeList.RaiseListChangedEvents = True
        End While
      End If

      Dim po As ProductionVenueOutsourceServiceType = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If po Is Nothing OrElse po.ProductionVenueOutsourceServiceTypeID <> sdr.GetInt32(1) Then
            po = Me.GetProductionVenueOutsourceServiceType(sdr.GetInt32(1))
          End If
          po.ProductionVenueOutsourceServiceTypeDetailList.RaiseListChangedEvents = False
          po.ProductionVenueOutsourceServiceTypeDetailList.Add(ProductionVenueOutsourceServiceTypeDetail.GetProductionVenueOutsourceServiceTypeDetail(sdr))
          po.ProductionVenueOutsourceServiceTypeDetailList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionVenueID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionVenueContactList.RaiseListChangedEvents = False
          parent.ProductionVenueContactList.Add(ProductionVenueContact.GetProductionVenueContact(sdr))
          parent.ProductionVenueContactList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionVenueID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionVenueAreaList.RaiseListChangedEvents = False
          parent.ProductionVenueAreaList.Add(ProductionVenueArea.GetProductionVenueArea(sdr))
          parent.ProductionVenueAreaList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As ProductionVenue In Me
        child.CheckRules()
        For Each ProductionVenueProductionType As ProductionVenueProductionType In child.ProductionVenueProductionTypeList
          ProductionVenueProductionType.CheckRules()
        Next
        For Each ProductionVenueOutsourceServiceType As ProductionVenueOutsourceServiceType In child.ProductionVenueOutsourceServiceTypeList
          ProductionVenueOutsourceServiceType.CheckRules()
          For Each ProductionVenueOutsourceServiceTypeDetail As ProductionVenueOutsourceServiceTypeDetail In ProductionVenueOutsourceServiceType.ProductionVenueOutsourceServiceTypeDetailList
            ProductionVenueOutsourceServiceTypeDetail.CheckRules()
          Next
        Next
        For Each ProductionVenueContact As ProductionVenueContact In child.ProductionVenueContactList
          ProductionVenueContact.CheckRules()
        Next
        For Each ProductionVenueArea As ProductionVenueArea In child.ProductionVenueAreaList
          ProductionVenueArea.CheckRules()
        Next
      Next

      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionVenueList"
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@ProductionVenue", Strings.MakeEmptyDBNull(crit.ProductionVenue))
            cm.Parameters.AddWithValue("@ProductionVenueID", NothingDBNull(crit.ProductionVenueID))
            'crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionVenue In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionVenue In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace