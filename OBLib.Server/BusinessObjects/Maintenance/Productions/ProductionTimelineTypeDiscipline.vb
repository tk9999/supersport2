﻿' Generated 26 Jun 2014 18:34 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class ProductionTimelineTypeDiscipline
    Inherits SingularBusinessBase(Of ProductionTimelineTypeDiscipline)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionTimelineTypeDisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTimelineTypeDisciplineID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionTimelineTypeDisciplineID() As Integer
      Get
        Return GetProperty(ProductionTimelineTypeDisciplineIDProperty)
      End Get
    End Property

    Public Shared ProductionTimelineTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTimelineTypeID, "Production Timeline Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Production Timeline Type", Description:=""),
    Browsable(False)>
    Public Property ProductionTimelineTypeID() As Integer?
      Get
        Return GetProperty(ProductionTimelineTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTimelineTypeIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    Required(ErrorMessage:="Discipline required"),
    DropDownWeb(GetType(RODisciplineList))>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionTimelineType

      Return CType(CType(Me.Parent, ProductionTimelineTypeDisciplineList).Parent, ProductionTimelineType)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionTimelineTypeDisciplineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Timeline Type Discipline")
        Else
          Return String.Format("Blank {0}", "Production Timeline Type Discipline")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionTimelineTypeDiscipline() method.

    End Sub

    Public Shared Function NewProductionTimelineTypeDiscipline() As ProductionTimelineTypeDiscipline

      Return DataPortal.CreateChild(Of ProductionTimelineTypeDiscipline)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionTimelineTypeDiscipline(dr As SafeDataReader) As ProductionTimelineTypeDiscipline

      Dim p As New ProductionTimelineTypeDiscipline()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionTimelineTypeDisciplineIDProperty, .GetInt32(0))
          LoadProperty(ProductionTimelineTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionTimelineTypeDiscipline"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionTimelineTypeDiscipline"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionTimelineTypeDisciplineID As SqlParameter = .Parameters.Add("@ProductionTimelineTypeDisciplineID", SqlDbType.Int)
          paramProductionTimelineTypeDisciplineID.Value = GetProperty(ProductionTimelineTypeDisciplineIDProperty)
          If Me.IsNew Then
            paramProductionTimelineTypeDisciplineID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionTimelineTypeID", Me.GetParent.ProductionTimelineTypeID) 'GetProperty(ProductionTimelineTypeIDProperty)
          .Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionTimelineTypeDisciplineIDProperty, paramProductionTimelineTypeDisciplineID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionTimelineTypeDiscipline"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionTimelineTypeDisciplineID", GetProperty(ProductionTimelineTypeDisciplineIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace