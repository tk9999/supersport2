﻿' Generated 26 Jan 2015 06:14 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly.Old

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class PlanningLevelHour
    Inherits SingularBusinessBase(Of PlanningLevelHour)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PlanningLevelHourIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PlanningLevelHourID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property PlanningLevelHourID() As Integer
      Get
        Return GetProperty(PlanningLevelHourIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:=""),
    DropDownWeb(GetType(ROProductionVenueOldList))>
    Public Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:=""),
    Required(ErrorMessage:="Production Type required"),
    DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionTypeList))>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventTypeID, "Event Type", 0)
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:=""),
    DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROEventTypeList), ThisFilterMember:="ProductionTypeID")>
    Public Property EventTypeID() As Integer
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EventTypeIDProperty, Value)
      End Set
    End Property

    Public Shared PlanningHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PlanningHours, "Planning Hours", 0)
    ''' <summary>
    ''' Gets and sets the Planning Hours value
    ''' </summary>
    <Display(Name:="Planning Hours", Description:=""),
    Required(ErrorMessage:="Planning Hours required")>
    Public Property PlanningHours() As Integer
      Get
        Return GetProperty(PlanningHoursProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PlanningHoursProperty, Value)
      End Set
    End Property

    Public Shared ReconHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReconHours, "Recon Hours", 0)
    ''' <summary>
    ''' Gets and sets the Recon Hours value
    ''' </summary>
    <Display(Name:="Recon Hours", Description:=""),
    Required(ErrorMessage:="Recon Hours required")>
    Public Property ReconHours() As Integer
      Get
        Return GetProperty(ReconHoursProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ReconHoursProperty, Value)
      End Set
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets and sets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:=""),
    Required(ErrorMessage:="Effective Date required")>
    Public Property EffectiveDate As DateTime?
      Get
        Return GetProperty(EffectiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EffectiveDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PlanningLevelHourIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Planning Level Hour")
        Else
          Return String.Format("Blank {0}", "Planning Level Hour")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPlanningLevelHour() method.

    End Sub

    Public Shared Function NewPlanningLevelHour() As PlanningLevelHour

      Return DataPortal.CreateChild(Of PlanningLevelHour)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetPlanningLevelHour(dr As SafeDataReader) As PlanningLevelHour

      Dim p As New PlanningLevelHour()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(PlanningLevelHourIDProperty, .GetInt32(0))
          LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(EventTypeIDProperty, .GetInt32(3))
          LoadProperty(PlanningHoursProperty, .GetInt32(4))
          LoadProperty(ReconHoursProperty, .GetInt32(5))
          LoadProperty(EffectiveDateProperty, .GetValue(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insPlanningLevelHour"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updPlanningLevelHour"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramPlanningLevelHourID As SqlParameter = .Parameters.Add("@PlanningLevelHourID", SqlDbType.Int)
          paramPlanningLevelHourID.Value = GetProperty(PlanningLevelHourIDProperty)
          If Me.IsNew Then
            paramPlanningLevelHourID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionVenueID", Singular.Misc.NothingDBNull(GetProperty(ProductionVenueIDProperty)))
          .Parameters.AddWithValue("@ProductionTypeID", GetProperty(ProductionTypeIDProperty))
          .Parameters.AddWithValue("@EventTypeID", GetProperty(EventTypeIDProperty))
          .Parameters.AddWithValue("@PlanningHours", GetProperty(PlanningHoursProperty))
          .Parameters.AddWithValue("@ReconHours", GetProperty(ReconHoursProperty))
          .Parameters.AddWithValue("@EffectiveDate", (New SmartDate(GetProperty(EffectiveDateProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(PlanningLevelHourIDProperty, paramPlanningLevelHourID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delPlanningLevelHour"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@PlanningLevelHourID", GetProperty(PlanningLevelHourIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace