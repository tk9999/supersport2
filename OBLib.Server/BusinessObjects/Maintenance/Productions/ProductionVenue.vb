﻿' Generated 19 Oct 2014 21:38 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Locations.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class ProductionVenue
    Inherits SingularBusinessBase(Of ProductionVenue)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key()>
    Public Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Production Venue")
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:=""),
    StringLength(100, ErrorMessage:="Production Venue cannot be more than 100 characters")>
    Public Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionVenueProperty, Value)
      End Set
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets and sets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CityProperty, Value)
      End Set
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets and sets the City value
    ''' </summary>
    <Display(Name:="City", Description:=""),
    Required(ErrorMessage:="City required"),
    DropDownWeb(GetType(ROCityList), ThisFilterMember:="CountryID")>
    Public Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CityIDProperty, Value)
      End Set
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryID, "Country", Nothing)
    ''' <summary>
    ''' Gets and sets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:=""),
    Required(ErrorMessage:="Country required"),
    DropDownWeb(GetType(ROCountryList))>
    Public Property CountryID() As Integer?
      Get
        Return GetProperty(CountryIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CountryIDProperty, Value)
      End Set
    End Property

    Public Shared DirectionsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Directions, "Directions")
    ''' <summary>
    ''' Gets and sets the Directions value
    ''' </summary>
    <Display(Name:="Directions", Description:="")>
    Public Property Directions() As String
      Get
        Return GetProperty(DirectionsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DirectionsProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property


    'Public Shared RowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNo, "Row No")
    ' ''' <summary>
    ' ''' Gets and sets the Row No value
    ' ''' </summary>
    '<Display(Name:="Row No", Description:=""),
    'Required(ErrorMessage:="Row No required")>
    'Public Property RowNo() As Integer
    '  Get
    '    Return GetProperty(RowNoProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(RowNoProperty, Value)
    '  End Set
    'End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionVenueProductionTypeListProperty As PropertyInfo(Of ProductionVenueProductionTypeList) = RegisterProperty(Of ProductionVenueProductionTypeList)(Function(c) c.ProductionVenueProductionTypeList, "RO Production Venue Production Type List")
    Public ReadOnly Property ProductionVenueProductionTypeList() As ProductionVenueProductionTypeList
      Get
        If GetProperty(ProductionVenueProductionTypeListProperty) Is Nothing Then
          LoadProperty(ProductionVenueProductionTypeListProperty, Maintenance.Productions.ProductionVenueProductionTypeList.NewProductionVenueProductionTypeList())
        End If
        Return GetProperty(ProductionVenueProductionTypeListProperty)
      End Get
    End Property

    Public Shared ProductionVenueOutsourceServiceTypeListProperty As PropertyInfo(Of ProductionVenueOutsourceServiceTypeList) = RegisterProperty(Of ProductionVenueOutsourceServiceTypeList)(Function(c) c.ProductionVenueOutsourceServiceTypeList, "RO Production Venue Outsource Service Type List")
    Public ReadOnly Property ProductionVenueOutsourceServiceTypeList() As ProductionVenueOutsourceServiceTypeList
      Get
        If GetProperty(ProductionVenueOutsourceServiceTypeListProperty) Is Nothing Then
          LoadProperty(ProductionVenueOutsourceServiceTypeListProperty, Maintenance.Productions.ProductionVenueOutsourceServiceTypeList.NewProductionVenueOutsourceServiceTypeList())
        End If
        Return GetProperty(ProductionVenueOutsourceServiceTypeListProperty)
      End Get
    End Property

    Public Shared ProductionVenueContactListProperty As PropertyInfo(Of ProductionVenueContactList) = RegisterProperty(Of ProductionVenueContactList)(Function(c) c.ProductionVenueContactList, "RO Production Venue Contact List")
    Public ReadOnly Property ProductionVenueContactList() As ProductionVenueContactList
      Get
        If GetProperty(ProductionVenueContactListProperty) Is Nothing Then
          LoadProperty(ProductionVenueContactListProperty, Maintenance.Productions.ProductionVenueContactList.NewProductionVenueContactList())
        End If
        Return GetProperty(ProductionVenueContactListProperty)
      End Get
    End Property

    Public Shared ProductionVenueAreaListProperty As PropertyInfo(Of ProductionVenueAreaList) = RegisterProperty(Of ProductionVenueAreaList)(Function(c) c.ProductionVenueAreaList, "RO Production Venue Area List")
    Public ReadOnly Property ProductionVenueAreaList() As ProductionVenueAreaList
      Get
        If GetProperty(ProductionVenueAreaListProperty) Is Nothing Then
          LoadProperty(ProductionVenueAreaListProperty, Maintenance.Productions.ProductionVenueAreaList.NewProductionVenueAreaList())
        End If
        Return GetProperty(ProductionVenueAreaListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionVenue.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Venue")
        Else
          Return String.Format("Blank {0}", "Production Venue")
        End If
      Else
        Return Me.ProductionVenue
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(ProductionVenueProperty,
                      Function(c) c.ProductionVenue = "",
                      Function(c) "Production Venue is required")
        .ServerRuleFunction = AddressOf ProductionVenueValid
        .ASyncBusyText = "Checking if Production Venue exists..."
      End With

    End Sub

    Public Function ProductionVenueValid(ProductionVenue As ProductionVenue) As String
      Dim ErrorMsg As String = ""
      Dim cmd As New Singular.CommandProc("[cmdProcs].[cmdProductionVenueExists]",
                                          New String() {"ProductionVenueID", "ProductionVenue"},
                                          New Object() {NothingDBNull(ProductionVenue.ProductionVenueID), ProductionVenue.ProductionVenue})
      cmd.FetchType = CommandProc.FetchTypes.DataObject
      cmd = cmd.Execute
      Dim Count As Integer = cmd.DataObject
      If Count > 0 Then
        Return "An Production Venue with this name already exists"
      End If
      Return ErrorMsg
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionVenue() method.

    End Sub

    Public Shared Function NewProductionVenue() As ProductionVenue

      Return DataPortal.CreateChild(Of ProductionVenue)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionVenue(dr As SafeDataReader) As ProductionVenue

      Dim p As New ProductionVenue()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionVenueIDProperty, .GetInt32(0))
          LoadProperty(ProductionVenueProperty, .GetString(1))
          LoadProperty(CityProperty, .GetString(2))
          LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CountryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(DirectionsProperty, .GetString(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          'LoadProperty(RowNoProperty, .GetInt32(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionVenue"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionVenue"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionVenueID As SqlParameter = .Parameters.Add("@ProductionVenueID", SqlDbType.Int)
          paramProductionVenueID.Value = GetProperty(ProductionVenueIDProperty)
          If Me.IsNew Then
            paramProductionVenueID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionVenue", GetProperty(ProductionVenueProperty))
          '.Parameters.AddWithValue("@City", GetProperty(CityProperty))
          .Parameters.AddWithValue("@CityID", GetProperty(CityIDProperty))
          '.Parameters.AddWithValue("@CountryID", GetProperty(CountryIDProperty))
          .Parameters.AddWithValue("@Directions", GetProperty(DirectionsProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          '.Parameters.AddWithValue("@RowNo", GetProperty(RowNoProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionVenueIDProperty, paramProductionVenueID.Value)
          End If
          ' update child objects
          If GetProperty(ProductionVenueProductionTypeListProperty) IsNot Nothing Then
            Me.ProductionVenueProductionTypeList.Update()
          End If
          If GetProperty(ProductionVenueContactListProperty) IsNot Nothing Then
            Me.ProductionVenueContactList.Update()
          End If
          If GetProperty(ProductionVenueOutsourceServiceTypeListProperty) IsNot Nothing Then
            Me.ProductionVenueOutsourceServiceTypeList.Update()
          End If
          If GetProperty(ProductionVenueAreaListProperty) IsNot Nothing Then
            Me.ProductionVenueAreaList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ProductionVenueProductionTypeListProperty) IsNot Nothing Then
          Me.ProductionVenueProductionTypeList.Update()
        End If
        If GetProperty(ProductionVenueContactListProperty) IsNot Nothing Then
          Me.ProductionVenueContactList.Update()
        End If
        If GetProperty(ProductionVenueOutsourceServiceTypeListProperty) IsNot Nothing Then
          Me.ProductionVenueOutsourceServiceTypeList.Update()
        End If
        If GetProperty(ProductionVenueAreaListProperty) IsNot Nothing Then
          Me.ProductionVenueAreaList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionVenue"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionVenueID", GetProperty(ProductionVenueIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace