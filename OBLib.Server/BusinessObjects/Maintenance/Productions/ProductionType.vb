﻿' Generated 19 Oct 2014 22:26 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class ProductionType
    Inherits SingularBusinessBase(Of ProductionType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public Property ProductionTypeID() As Integer
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ProductionTypeIDProperty, value)
      End Set
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Production Type", "")
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="Description for the Production Type"),
    StringLength(100, ErrorMessage:="Production Type cannot be more than 100 characters")>
    Public Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTypeProperty, Value)
      End Set
    End Property

    Public Shared ReportingColourProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportingColour, "Reporting Colour", "")
    ''' <summary>
    ''' Gets and sets the Reporting Colour value
    ''' </summary>
    <Display(Name:="Reporting Colour", Description:="The colour to use on the Board Report"),
    StringLength(50, ErrorMessage:="Reporting Colour cannot be more than 50 characters"),
    Browsable(False)>
    Public Property ReportingColour() As String
      Get
        Return GetProperty(ReportingColourProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ReportingColourProperty, Value)
      End Set
    End Property

    Public Shared TextColourProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TextColour, "Text Colour", "Brown")
    ''' <summary>
    ''' Gets and sets the Text Colour value
    ''' </summary>
    <Display(Name:="Text Colour", Description:=""),
    StringLength(50, ErrorMessage:="Text Colour cannot be more than 50 characters"),
    Browsable(False)>
    Public Property TextColour() As String
      Get
        Return GetProperty(TextColourProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TextColourProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared OldIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OldInd, "Old", False)
    ''' <summary>
    ''' Gets and sets the Old value
    ''' </summary>
    <Display(Name:="Old?", Description:="")>
    Public Property OldInd() As Boolean
      Get
        Return GetProperty(OldIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OldIndProperty, Value)
      End Set
    End Property

    Public Shared ImportedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ImportedInd, "Imported", False)
    ''' <summary>
    ''' Gets and sets the Imported value
    ''' </summary>
    <Display(Name:="Imported?", Description:="")>
    Public Property ImportedInd() As Boolean
      Get
        Return GetProperty(ImportedIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ImportedIndProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Type")
        Else
          Return String.Format("Blank {0}", "Production Type")
        End If
      Else
        Return Me.ProductionType
      End If

    End Function

#End Region

#End Region

#Region " Child Lists "

    Public Shared EventTypeListProperty As PropertyInfo(Of EventTypeList) = RegisterProperty(Of EventTypeList)(Function(c) c.EventTypeList, "Event Type List")
    Public ReadOnly Property EventTypeList() As EventTypeList
      Get
        If GetProperty(EventTypeListProperty) Is Nothing Then
          LoadProperty(EventTypeListProperty, Maintenance.Productions.EventTypeList.NewEventTypeList)
        End If
        Return GetProperty(EventTypeListProperty)
      End Get
    End Property

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(ProductionTypeProperty,
                      Function(c) c.ProductionType = "",
                      Function(c) "Production Type is required")
        .ServerRuleFunction = AddressOf ProductionTypeValid
        .ASyncBusyText = "Checking if Production Type exists..."
      End With

    End Sub

    Public Function ProductionTypeValid(ProductionType As ProductionType) As String
      Dim ErrorMsg As String = ""
      Dim cmd As New Singular.CommandProc("[cmdProcs].[cmdProductionTypeExists]",
                                          New String() {"ProductionTypeID", "ProductionType"},
                                          New Object() {NothingDBNull(ProductionType.ProductionTypeID), ProductionType.ProductionType})
      cmd.FetchType = CommandProc.FetchTypes.DataObject
      cmd = cmd.Execute
      Dim Count As Integer = cmd.DataObject
      If Count > 0 Then
        Return "A Production Type with this name Already Exists"
      End If
      Return ErrorMsg
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionType() method.

    End Sub

    Public Shared Function NewProductionType() As ProductionType

      Return DataPortal.CreateChild(Of ProductionType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionType(dr As SafeDataReader) As ProductionType

      Dim p As New ProductionType()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionTypeIDProperty, .GetInt32(0))
          LoadProperty(ProductionTypeProperty, .GetString(1))
          LoadProperty(ReportingColourProperty, .GetString(2))
          LoadProperty(TextColourProperty, .GetString(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(OldIndProperty, .GetBoolean(8))
          LoadProperty(ImportedIndProperty, .GetBoolean(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionTypeID As SqlParameter = .Parameters.Add("@ProductionTypeID", SqlDbType.Int)
          paramProductionTypeID.Value = GetProperty(ProductionTypeIDProperty)
          If Me.IsNew Then
            paramProductionTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionType", GetProperty(ProductionTypeProperty))
          .Parameters.AddWithValue("@ReportingColour", GetProperty(ReportingColourProperty))
          .Parameters.AddWithValue("@TextColour", GetProperty(TextColourProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@OldInd", GetProperty(OldIndProperty))
          .Parameters.AddWithValue("@ImportedInd", GetProperty(ImportedIndProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionTypeIDProperty, paramProductionTypeID.Value)
          End If
          ' update child objects
          If GetProperty(EventTypeListProperty) IsNot Nothing Then
            Me.EventTypeList.Update()
          End If
          MarkOld()
        End With
      Else
        If GetProperty(EventTypeListProperty) IsNot Nothing Then
          Me.EventTypeList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionTypeID", GetProperty(ProductionTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace