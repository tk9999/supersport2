﻿' Generated 26 Mar 2014 13:06 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance

  <Serializable()> _
  Public Class ProductionChannelStatus
    Inherits SingularBusinessBase(Of ProductionChannelStatus)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionChannelStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionChannelStatusID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionChannelStatusID() As Integer
      Get
        Return GetProperty(ProductionChannelStatusIDProperty)
      End Get
    End Property

    Public Shared ProductionChannelStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionChannelStatus, "Production Channel Status", "")
    ''' <summary>
    ''' Gets and sets the Production Channel Status value
    ''' </summary>
    <Display(Name:="Production Channel Status", Description:=""),
    StringLength(20, ErrorMessage:="Production Channel Status cannot be more than 20 characters")>
    Public Property ProductionChannelStatus() As String
      Get
        Return GetProperty(ProductionChannelStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionChannelStatusProperty, Value)
      End Set
    End Property

    Public Shared ProductionChannelStatusCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionChannelStatusCode, "Production Channel Status Code", "")
    ''' <summary>
    ''' Gets and sets the Production Channel Status Code value
    ''' </summary>
    <Display(Name:="Production Channel Status Code", Description:=""),
    StringLength(5, ErrorMessage:="Production Channel Status Code cannot be more than 5 characters")>
    Public Property ProductionChannelStatusCode() As String
      Get
        Return GetProperty(ProductionChannelStatusCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionChannelStatusCodeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionChannelStatusIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionChannelStatus.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Channel Status")
        Else
          Return String.Format("Blank {0}", "Production Channel Status")
        End If
      Else
        Return Me.ProductionChannelStatus
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionChannelStatus() method.

    End Sub

    Public Shared Function NewProductionChannelStatus() As ProductionChannelStatus

      Return DataPortal.CreateChild(Of ProductionChannelStatus)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionChannelStatus(dr As SafeDataReader) As ProductionChannelStatus

      Dim p As New ProductionChannelStatus()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionChannelStatusIDProperty, .GetInt32(0))
          LoadProperty(ProductionChannelStatusProperty, .GetString(1))
          LoadProperty(ProductionChannelStatusCodeProperty, .GetString(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insProductionChannelStatus"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updProductionChannelStatus"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionChannelStatusID As SqlParameter = .Parameters.Add("@ProductionChannelStatusID", SqlDbType.Int)
          paramProductionChannelStatusID.Value = GetProperty(ProductionChannelStatusIDProperty)
          If Me.IsNew Then
            paramProductionChannelStatusID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionChannelStatus", GetProperty(ProductionChannelStatusProperty))
          .Parameters.AddWithValue("@ProductionChannelStatusCode", GetProperty(ProductionChannelStatusCodeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionChannelStatusIDProperty, paramProductionChannelStatusID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delProductionChannelStatus"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionChannelStatusID", GetProperty(ProductionChannelStatusIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace