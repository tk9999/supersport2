﻿' Generated 19 Oct 2014 22:26 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class EventTypeList
    Inherits SingularBusinessListBase(Of EventTypeList, EventType)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

    'Private mTotalPages As Integer = 0
    'Public ReadOnly Property TotalPages As Integer
    '  Get
    '    Return mTotalPages
    '  End Get
    'End Property

#Region " Business Methods "

    Public Function GetItem(EventTypeID As Integer) As EventType

      For Each child As EventType In Me
        If child.EventTypeID = EventTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Event Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      'Inherits CriteriaBase(Of Criteria)
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "ProductionTypeID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="ProductionTypeID", Description:="")>
      Public Property ProductionTypeID() As Integer?
        Get
          Return ReadProperty(ProductionTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionTypeIDProperty, Value)
        End Set
      End Property

      Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.EventType, "") _
                                                                  .AddSetExpression("FilterEventTypeList()", False)
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Event Type", Description:="")>
      Public Property EventType() As String
        Get
          Return ReadProperty(EventTypeProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(EventTypeProperty, Value)
        End Set
      End Property

      'Public Shared PageNoProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PageNo, "Page No", 1)
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="Page No", Description:="")>
      'Public Property PageNo() As Integer?
      '  Get
      '    Return ReadProperty(PageNoProperty)
      '  End Get
      '  Set(ByVal Value As Integer?)
      '    LoadProperty(PageNoProperty, Value)
      '  End Set
      'End Property

      'Public Shared PageSizeProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PageSize, "Page No", 25)
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="Page No", Description:="")>
      'Public Property PageSize() As Integer?
      '  Get
      '    Return ReadProperty(PageSizeProperty)
      '  End Get
      '  Set(ByVal Value As Integer?)
      '    LoadProperty(PageSizeProperty, Value)
      '  End Set
      'End Property

      'Public Shared SortAscProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SortAsc, "Sort Asc?", True)
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="Sort Asc?", Description:="")>
      'Public Property SortAsc() As Boolean
      '  Get
      '    Return ReadProperty(SortAscProperty)
      '  End Get
      '  Set(ByVal Value As Boolean)
      '    LoadProperty(SortAscProperty, Value)
      '  End Set
      'End Property

      'Public Shared SortColumnProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SortColumn, "Sort Column", "")
      ' ''' <summary>
      ' ''' Gets and sets the Genre value
      ' ''' </summary>
      '<Display(Name:="Sort Column", Description:="")>
      'Public Property SortColumn() As String
      '  Get
      '    Return ReadProperty(SortColumnProperty)
      '  End Get
      '  Set(ByVal Value As String)
      '    LoadProperty(SortColumnProperty, Value)
      '  End Set
      'End Property

      Public Sub New(ProductionTypeID As Integer?, EventType As String,
                     PageNo As Integer,
                     PageSize As Integer,
                     SortAsc As Boolean,
                     SortColumn As String)
        Me.ProductionTypeID = ProductionTypeID
        Me.EventType = EventType
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewEventTypeList() As EventTypeList

      Return New EventTypeList()

    End Function

    Public Shared Sub BeginGetEventTypeList(CallBack As EventHandler(Of DataPortalResult(Of EventTypeList)))

      Dim dp As New DataPortal(Of EventTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetEventTypeList() As EventTypeList

      Return DataPortal.Fetch(Of EventTypeList)(New Criteria())

    End Function

    Public Shared Function GetEventTypeList(Criteria As Criteria) As EventTypeList

      Return DataPortal.Fetch(Of EventTypeList)(Criteria)

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      'mTotalPages = sdr.GetInt32(1)
      sdr.NextResult()

      While sdr.Read
        Me.Add(EventType.GetEventType(sdr))
      End While

      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getEventTypeList"
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@EventType", Strings.MakeEmptyDBNull(crit.EventType))
            'cm.Parameters.AddWithValue("@PageNo", crit.PageNo)
            'cm.Parameters.AddWithValue("@PageSize", crit.PageSize)
            'cm.Parameters.AddWithValue("@SortColumn", crit.SortColumn)
            'cm.Parameters.AddWithValue("@SortAsc", crit.SortAsc)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As EventType In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As EventType In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace