﻿' Generated 19 Oct 2014 22:26 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions

  <Serializable()> _
  Public Class EventType
    Inherits SingularBusinessBase(Of EventType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public Property EventTypeID() As Integer
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(EventTypeIDProperty, value)
      End Set
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "Event Type", "")
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="Description for the Event Type"),
    StringLength(100, ErrorMessage:="Event Type cannot be more than 100 characters")>
    Public Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventTypeProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="The parent production type for this event type")>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 0)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Event Type", "")
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="Description for the Event Type")>
    Public Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTypeProperty, Value)
      End Set
    End Property

    Public Shared HighlightsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HighlightsInd, "Highlights?", False)
    ''' <summary>
    ''' Gets and sets the Position Required value
    ''' </summary>
    <Display(Name:="Highlights?", Description:="")>
    Public Property HighlightsInd() As Boolean
      Get
        Return GetProperty(HighlightsIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HighlightsIndProperty, Value)
      End Set
    End Property

    Public Shared ImportedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ImportedInd, "Imported?", False)
    ''' <summary>
    ''' Gets and sets the Position Required value
    ''' </summary>
    <Display(Name:="Imported?", Description:="")>
    Public Property ImportedInd() As Boolean
      Get
        Return GetProperty(ImportedIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ImportedIndProperty, Value)
      End Set
    End Property

    Public Shared MagazineShowIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MagazineShowInd, "Mag Show?", False)
    ''' <summary>
    ''' Gets and sets the Position Required value
    ''' </summary>
    <Display(Name:="Mag Show?", Description:="")>
    Public Property MagazineShowInd() As Boolean
      Get
        Return GetProperty(MagazineShowIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(MagazineShowIndProperty, Value)
      End Set
    End Property

    Public Shared OBIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBInd, "OB?", False)
    ''' <summary>
    ''' Gets and sets the Position Required value
    ''' </summary>
    <Display(Name:="OB?", Description:="")>
    Public Property OBInd() As Boolean
      Get
        Return GetProperty(OBIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OBIndProperty, Value)
      End Set
    End Property

    Public Shared PlayoutIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PlayoutInd, "Playout?", False)
    ''' <summary>
    ''' Gets and sets the Position Required value
    ''' </summary>
    <Display(Name:="Playout?", Description:="")>
    Public Property PlayoutInd() As Boolean
      Get
        Return GetProperty(PlayoutIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PlayoutIndProperty, Value)
      End Set
    End Property

    Public Shared StudioIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.StudioInd, "Studio?", False)
    ''' <summary>
    ''' Gets and sets the Position Required value
    ''' </summary>
    <Display(Name:="Studio?", Description:="")>
    Public Property StudioInd() As Boolean
      Get
        Return GetProperty(StudioIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(StudioIndProperty, Value)
      End Set
    End Property

    Public Shared ShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortName, "Short Name", "")
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Short Name", Description:="Brief Description for the Event Type, to be used in Smses"),
    StringLength(20, ErrorMessage:="Short Name cannot be more than 100 characters")>
    Public Property ShortName() As String
      Get
        Return GetProperty(ShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ShortNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object
      Return GetProperty(EventTypeIDProperty)
    End Function

    Public Overrides Function ToString() As String
      If Me.EventType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Event Type")
        Else
          Return String.Format("Blank {0}", "Event Type")
        End If
      Else
        Return Me.EventType
      End If
    End Function

    Public Function GetParent() As ProductionType
      If Me.Parent Is Nothing Then
        Return Nothing
      Else
        Return CType(CType(Me.Parent, EventTypeList).Parent, ProductionType)
      End If
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(EventTypeProperty,
                      Function(c) c.EventType = "",
                      Function(c) "Event Type is required")
        .ServerRuleFunction = AddressOf EventTypeValid
        .ASyncBusyText = "Checking if Event Type exists..."
      End With

    End Sub

    Public Function EventTypeValid(EventType As EventType) As String
      If EventType.GetParent IsNot Nothing Then
        Dim ErrorMsg As String = ""
        Dim cmd As New Singular.CommandProc("[cmdProcs].[cmdEventTypeExists]",
                                            New String() {"ProductionTypeID", "EventTypeID", "EventType"},
                                            New Object() {NothingDBNull(EventType.GetParent.ProductionTypeID), NothingDBNull(EventType.EventTypeID), EventType.EventType})
        cmd.FetchType = CommandProc.FetchTypes.DataObject
        cmd = cmd.Execute
        Dim Count As Integer = cmd.DataObject
        If Count > 0 Then
          Return "An Event Type with this name already exists"
        End If
        Return ErrorMsg
      End If
      Return ""
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEventType() method.

    End Sub

    Public Shared Function NewEventType() As EventType

      Return DataPortal.CreateChild(Of EventType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEventType(dr As SafeDataReader) As EventType

      Dim e As New EventType()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EventTypeIDProperty, .GetInt32(0))
          LoadProperty(EventTypeProperty, .GetString(1))
          LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(SystemIDProperty, .GetInt32(7))
          'If .IsDBNull(8) Then
          '  LoadProperty(DefaultDurationProperty, DateTime.MinValue)
          'Else
          '  LoadProperty(DefaultDurationProperty, (New DateTime(2000, 1, 1).Add(.GetValue(8))))
          'End If
          ''Row Number = 9
          LoadProperty(ProductionTypeProperty, .GetString(8))
          'LoadProperty(DefaultWeekDayProperty, ZeroNothing(.GetInt32(11)))
          'If .IsDBNull(12) Then
          '  LoadProperty(DefaultStartTimeProperty, DateTime.MinValue)
          'Else
          '  LoadProperty(DefaultStartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(12))))
          'End If
          'If .IsDBNull(13) Then
          '  LoadProperty(DefaultEndTimeProperty, DateTime.MinValue)
          'Else
          '  LoadProperty(DefaultEndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(13))))
          'End If
          'LoadProperty(DefaultRoomIDProperty, ZeroNothing(.GetInt32(14)))
          LoadProperty(HighlightsIndProperty, .GetBoolean(9))
          LoadProperty(ImportedIndProperty, .GetBoolean(10))
          LoadProperty(MagazineShowIndProperty, .GetBoolean(11))
          LoadProperty(OBIndProperty, .GetBoolean(12))
          LoadProperty(PlayoutIndProperty, .GetBoolean(13))
          LoadProperty(StudioIndProperty, .GetBoolean(14))
          LoadProperty(ShortNameProperty, .GetString(15))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEventType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEventType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramEventTypeID As SqlParameter = .Parameters.Add("@EventTypeID", SqlDbType.Int)
          paramEventTypeID.Value = GetProperty(EventTypeIDProperty)
          If Me.IsNew Then
            paramEventTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EventType", GetProperty(EventTypeProperty))
          .Parameters.AddWithValue("@ProductionTypeID", Me.GetParent.ProductionTypeID) 'GetProperty(ProductionTypeIDProperty)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          '.Parameters.AddWithValue("@DefaultDuration", (New SmartDate(GetProperty(DefaultDurationProperty))).DBValue)
          '.Parameters.AddWithValue("@DefaultWeekDay", NothingDBNull(GetProperty(DefaultWeekDayProperty)))
          '.Parameters.AddWithValue("@DefaultStartTime", (New SmartDate(GetProperty(DefaultStartTimeProperty))).DBValue)
          '.Parameters.AddWithValue("@DefaultEndTime", (New SmartDate(GetProperty(DefaultEndTimeProperty))).DBValue)
          '.Parameters.AddWithValue("@DefaultRoomID", NothingDBNull(GetProperty(DefaultRoomIDProperty)))
          .Parameters.AddWithValue("@HighlightsInd", GetProperty(HighlightsIndProperty))
          .Parameters.AddWithValue("@ImportedInd", GetProperty(ImportedIndProperty))
          .Parameters.AddWithValue("@MagazineShowInd", GetProperty(MagazineShowIndProperty))
          .Parameters.AddWithValue("@OBInd", GetProperty(OBIndProperty))
          .Parameters.AddWithValue("@PlayoutInd", GetProperty(PlayoutIndProperty))
          .Parameters.AddWithValue("@StudioInd", GetProperty(StudioIndProperty))
          .Parameters.AddWithValue("@ShortName", GetProperty(ShortNameProperty))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EventTypeIDProperty, paramEventTypeID.Value)
          End If
          ' update child objects
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEventType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EventTypeID", GetProperty(EventTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace