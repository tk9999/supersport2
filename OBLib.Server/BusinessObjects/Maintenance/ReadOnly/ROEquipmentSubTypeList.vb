﻿' Generated 02 Jun 2014 21:20 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROEquipmentSubTypeList
    Inherits OBReadOnlyListBase(Of ROEquipmentSubTypeList, ROEquipmentSubType)

#Region " Business Methods "

    Public Function GetItem(EquipmentSubTypeID As Integer) As ROEquipmentSubType

      For Each child As ROEquipmentSubType In Me
        If child.EquipmentSubTypeID = EquipmentSubTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Equipment Sub Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROEquipmentSubTypeList() As ROEquipmentSubTypeList

      Return New ROEquipmentSubTypeList()

    End Function

    Public Shared Sub BeginGetROEquipmentSubTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentSubTypeList)))

      Dim dp As New DataPortal(Of ROEquipmentSubTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROEquipmentSubTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentSubTypeList)))

      Dim dp As New DataPortal(Of ROEquipmentSubTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROEquipmentSubTypeList() As ROEquipmentSubTypeList

      Return DataPortal.Fetch(Of ROEquipmentSubTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEquipmentSubType.GetROEquipmentSubType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEquipmentSubTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace