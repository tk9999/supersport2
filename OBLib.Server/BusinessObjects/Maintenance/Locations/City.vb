﻿' Generated 24 Dec 2014 11:55 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Locations.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Locations

  <Serializable()> _
  Public Class City
    Inherits OBBusinessBase(Of City)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "CityBO.CityBOToString(self)")

    Public Shared CityIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CityID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property CityID() As Integer
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City", "")
    ''' <summary>
    ''' Gets and sets the City value
    ''' </summary>
    <Display(Name:="City", Description:="Name of the city"),
    Required(ErrorMessage:="City required"),
    StringLength(50, ErrorMessage:="City cannot be more than 50 characters")>
    Public Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CityProperty, Value)
      End Set
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City Code", "")
    ''' <summary>
    ''' Gets and sets the City Code value
    ''' </summary>
    <Display(Name:="City Code", Description:="A short code used to represent the city for diagrams"),
    Required(ErrorMessage:="City Code required"),
    StringLength(10, ErrorMessage:="City Code cannot be more than 10 characters")>
    Public Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CityCodeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryID, "Country", Nothing)
    ''' <summary>
    ''' Gets and sets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCountryList))>
    Public Property CountryID() As Integer?
      Get
        Return GetProperty(CountryIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CountryIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 0)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Browsable(False)>
    Public Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProvinceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProvinceID, "Province", Nothing)
    ''' <summary>
    ''' Gets and sets the Province value
    ''' </summary>
    <Display(Name:="Province", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROProvinceList))>
    Public Property ProvinceID() As Integer?
      Get
        Return GetProperty(ProvinceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProvinceIDProperty, Value)
      End Set
    End Property

    Public Shared ImportedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ImportedInd, "Imported", False)
    ''' <summary>
    ''' Gets and sets the Imported value
    ''' </summary>
    <Display(Name:="Imported", Description:=""),
    Browsable(False)>
    Public Property ImportedInd() As Boolean
      Get
        Return GetProperty(ImportedIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ImportedIndProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared CityToCityListProperty As PropertyInfo(Of CityToCityList) = RegisterProperty(Of CityToCityList)(Function(c) c.CityToCityList, "City To City List")

    Public ReadOnly Property CityToCityList() As CityToCityList
      Get
        If GetProperty(CityToCityListProperty) Is Nothing Then
          LoadProperty(CityToCityListProperty, Maintenance.Locations.CityToCityList.NewCityToCityList())
        End If
        Return GetProperty(CityToCityListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CityIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.City.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "City")
        Else
          Return String.Format("Blank {0}", "City")
        End If
      Else
        Return Me.City
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"CityToCity"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(CityToCityListProperty, Singular.Rules.RuleSeverity.Warning)
        .JavascriptRuleFunctionName = "CityBO.CheckCityIsNew"
        .ServerRuleFunction = AddressOf CheckCityIsNew
      End With

    End Sub

    Public Shared Function CheckCityIsNew(city As City) As String
      Dim ErrorString = ""
      If city.IsNew Then
        ErrorString = "Please save the new city before adding city to city records"
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCity() method.

    End Sub

    Public Shared Function NewCity() As City

      Return DataPortal.CreateChild(Of City)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCity(dr As SafeDataReader) As City

      Dim c As New City()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CityIDProperty, .GetInt32(0))
          LoadProperty(CityProperty, .GetString(1))
          LoadProperty(CityCodeProperty, .GetString(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(CountryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(SystemIDProperty, .GetInt32(8))
          LoadProperty(ProvinceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(ImportedIndProperty, .GetBoolean(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCity"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCity"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCityID As SqlParameter = .Parameters.Add("@CityID", SqlDbType.Int)
          paramCityID.Value = GetProperty(CityIDProperty)
          If Me.IsNew Then
            paramCityID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@City", GetProperty(CityProperty))
          .Parameters.AddWithValue("@CityCode", GetProperty(CityCodeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@CountryID", Singular.Misc.NothingDBNull(GetProperty(CountryIDProperty)))
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ProvinceID", Singular.Misc.NothingDBNull(GetProperty(ProvinceIDProperty)))
          .Parameters.AddWithValue("@ImportedInd", GetProperty(ImportedIndProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CityIDProperty, paramCityID.Value)
          End If
          ' update child objects
          If GetProperty(CityToCityListProperty) IsNot Nothing Then
            Me.CityToCityList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(CityToCityListProperty) IsNot Nothing Then
          Me.CityToCityList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCity"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CityID", GetProperty(CityIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace