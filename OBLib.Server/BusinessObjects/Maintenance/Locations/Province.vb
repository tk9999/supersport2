﻿' Generated 24 Dec 2014 11:49 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Locations

  <Serializable()> _
  Public Class Province
    Inherits OBBusinessBase(Of Province)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProvinceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProvinceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProvinceID() As Integer
      Get
        Return GetProperty(ProvinceIDProperty)
      End Get
    End Property

    Public Shared ProvinceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Province, "Province", "")
    ''' <summary>
    ''' Gets and sets the Province value
    ''' </summary>
    <Display(Name:="Province", Description:="Province name"),
    StringLength(100, ErrorMessage:="Province cannot be more than 100 characters"),
    Required(ErrorMessage:="Province Name Required")>
    Public Property Province() As String
      Get
        Return GetProperty(ProvinceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProvinceProperty, Value)
      End Set
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryID, "Country", Nothing)
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CountryID() As Integer?
      Get
        Return GetProperty(CountryIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Country

      Return CType(CType(Me.Parent, ProvinceList).Parent, Country)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProvinceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Province.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Province")
        Else
          Return String.Format("Blank {0}", "Province")
        End If
      Else
        Return Me.Province
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProvince() method.

    End Sub

    Public Shared Function NewProvince() As Province

      Return DataPortal.CreateChild(Of Province)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProvince(dr As SafeDataReader) As Province

      Dim p As New Province()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProvinceIDProperty, .GetInt32(0))
          LoadProperty(ProvinceProperty, .GetString(1))
          LoadProperty(CountryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProvince"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProvince"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProvinceID As SqlParameter = .Parameters.Add("@ProvinceID", SqlDbType.Int)
          paramProvinceID.Value = GetProperty(ProvinceIDProperty)
          If Me.IsNew Then
            paramProvinceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Province", GetProperty(ProvinceProperty))
          .Parameters.AddWithValue("@CountryID", Me.GetParent().CountryID)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProvinceIDProperty, paramProvinceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProvince"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProvinceID", GetProperty(ProvinceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace