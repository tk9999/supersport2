﻿' Generated 20 Feb 2014 16:21 - Singular Systems Object Generator Version 2.1.665
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Locations.ReadOnly

  <Serializable()> _
  Public Class ROCityPagedList
    Inherits OBReadOnlyListBase(Of ROCityPagedList, ROCityPaged)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Parent "

    '<NotUndoable()> Private mParent As ROProvince
#End Region

#Region " Business Methods "

    Public Function GetItem(CityID As Integer) As ROCityPaged

      For Each child As ROCityPaged In Me
        If child.CityID = CityID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Citys"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      'Inherits CriteriaBase(Of Criteria)
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City ID", Nothing)

      <Display(Name:="City ID", Description:="")>
      Public Property CityID() As Integer?
        Get
          Return ReadProperty(CityIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(CityIDProperty, Value)
        End Set
      End Property

      Public Shared CityProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.City, "")

      <Display(Name:="City", Description:="")>
      Public Property City() As String
        Get
          Return ReadProperty(CityProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(CityProperty, Value)
        End Set
      End Property

      Public Shared CountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryID, "City ID", Nothing)

      <Display(Name:="Country", Description:="")>
      Public Property CountryID() As Integer?
        Get
          Return ReadProperty(CountryIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(CountryIDProperty, Value)
        End Set
      End Property

      Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country", "")

      <Display(Name:="Country", Description:="")>
      Public Property Country() As String
        Get
          Return ReadProperty(CountryProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(CountryProperty, Value)
        End Set
      End Property

      Public Sub New(City As String)
        Me.City = City
      End Sub

      Public Sub New(PageNo As Integer, PageSize As Integer)
        Me.PageNo = PageNo
        Me.PageSize = PageSize
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROCityList() As ROCityPagedList

      Return New ROCityPagedList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROCityList() As ROCityPagedList

      Return DataPortal.Fetch(Of ROCityPagedList)(New Criteria())

    End Function

    Public Shared Function GetROCityListCommonData() As ROCityPagedList

      Return DataPortal.Fetch(Of ROCityPagedList)(New Criteria(1, 1000))

    End Function

    Public Shared Function GetROCityList(CityID As Integer?) As ROCityPagedList

      Return DataPortal.Fetch(Of ROCityPagedList)(New Criteria(CityID))

    End Function

    Public Shared Function GetROCityList(City As String) As ROCityPagedList

      Return DataPortal.Fetch(Of ROCityPagedList)(New Criteria(City))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROCityPaged.GetROCityPaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True
    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROCityListPaged"
            cm.Parameters.AddWithValue("@City", Singular.Strings.MakeEmptyDBNull(crit.City))
            cm.Parameters.AddWithValue("@CityID", NothingDBNull(crit.CityID))
            cm.Parameters.AddWithValue("@CountryID", NothingDBNull(crit.CountryID))
            cm.Parameters.AddWithValue("@Country", Strings.MakeEmptyDBNull(crit.Country))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace