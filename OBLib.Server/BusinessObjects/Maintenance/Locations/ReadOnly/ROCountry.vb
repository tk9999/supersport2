﻿' Generated 20 Feb 2014 16:21 - Singular Systems Object Generator Version 2.1.665
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Locations.ReadOnly

  <Serializable()> _
  Public Class ROCountry
    Inherits OBReadOnlyBase(Of ROCountry)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CountryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CountryID, "Country", 0)
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CountryID() As Integer
      Get
        Return GetProperty(CountryIDProperty)
      End Get
    End Property

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country", "")
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="The name of the Country")>
    Public ReadOnly Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
    End Property

    Public Shared CurrencyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CurrencyID, "Currency", Nothing)
    ''' <summary>
    ''' Gets the Currency value
    ''' </summary>
    <Display(Name:="Currency", Description:="The currency the country deals in")>
    Public ReadOnly Property CurrencyID() As Integer?
      Get
        Return GetProperty(CurrencyIDProperty)
      End Get
    End Property

    Public Shared DefaultDailySnTRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.DefaultDailySnTRate, "Default Daily Sn T Rate", 0)
    ''' <summary>
    ''' Gets the Default Daily Sn T Rate value
    ''' </summary>
    <Display(Name:="Default Daily Sn T Rate", Description:="A default daily S&T rate for a country")>
    Public ReadOnly Property DefaultDailySnTRate() As Decimal
      Get
        Return GetProperty(DefaultDailySnTRateProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="The system a country is linked to")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROProvinceListProperty As PropertyInfo(Of ROProvinceList) = RegisterProperty(Of ROProvinceList)(Function(c) c.ROProvinceList, "RO Province New List")

    Public ReadOnly Property ROProvinceList() As ROProvinceList
      Get
        If GetProperty(ROProvinceListProperty) Is Nothing Then
          LoadProperty(ROProvinceListProperty, Maintenance.Locations.ReadOnly.ROProvinceList.NewROProvinceList())
        End If
        Return GetProperty(ROProvinceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CountryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Country

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCountry(dr As SafeDataReader) As ROCountry

      Dim r As New ROCountry()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CountryIDProperty, .GetInt32(0))
        LoadProperty(CountryProperty, .GetString(1))
        LoadProperty(CurrencyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(DefaultDailySnTRateProperty, .GetDecimal(3))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace