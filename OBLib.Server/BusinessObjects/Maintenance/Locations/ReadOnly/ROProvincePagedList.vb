﻿' Generated 30 Dec 2014 13:22 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Locations.ReadOnly

  <Serializable()> _
  Public Class ROProvincePagedList
    Inherits OBReadOnlyListBase(Of ROProvincePagedList, ROProvincePaged)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProvinceID As Integer) As ROProvincePaged

      For Each child As ROProvincePaged In Me
        If child.ProvinceID = ProvinceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Provinces"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      'Inherits CriteriaBase(Of Criteria)
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared ProvinceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Province, "Province", "")

      <Display(Name:="Province", Description:="")>
      Public Property Province() As String
        Get
          Return ReadProperty(ProvinceProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProvinceProperty, Value)
        End Set
      End Property

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProvinceList() As ROProvincePagedList

      Return New ROProvincePagedList()

    End Function

    Public Shared Sub BeginGetROProvinceList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProvincePagedList)))

      Dim dp As New DataPortal(Of ROProvincePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProvinceList(CallBack As EventHandler(Of DataPortalResult(Of ROProvincePagedList)))

      Dim dp As New DataPortal(Of ROProvincePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProvinceList() As ROProvincePagedList

      Return DataPortal.Fetch(Of ROProvincePagedList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProvincePaged.GetROProvince(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProvincePagedList"
            cm.Parameters.AddWithValue("@Province", Strings.MakeEmptyDBNull(crit.Province))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace