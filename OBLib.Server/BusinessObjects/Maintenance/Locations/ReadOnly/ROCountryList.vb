﻿' Generated 20 Feb 2014 16:21 - Singular Systems Object Generator Version 2.1.665
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Locations.ReadOnly

  <Serializable()> _
  Public Class ROCountryList
    Inherits OBReadOnlyListBase(Of ROCountryList, ROCountry)

#Region " Business Methods "

    Public Function GetItem(CountryID As Integer) As ROCountry

      For Each child As ROCountry In Me
        If child.CountryID = CountryID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Countrys"

    End Function

    Public Function GetROProvince(ProvinceID As Integer) As ROProvince

      Dim obj As ROProvince = Nothing
      For Each parent As ROCountry In Me
        obj = parent.ROProvinceList.GetItem(ProvinceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Display(Name:="Country"), PrimarySearchField>
      Public Property Country As String

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROCountryList() As ROCountryList

      Return New ROCountryList()

    End Function

    Public Shared Sub BeginGetROCountryList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROCountryList)))

      Dim dp As New DataPortal(Of ROCountryList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROCountryList(CallBack As EventHandler(Of DataPortalResult(Of ROCountryList)))

      Dim dp As New DataPortal(Of ROCountryList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROCountryList() As ROCountryList

      Return DataPortal.Fetch(Of ROCountryList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROCountry.GetROCountry(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      'Dim parent As ROCountry = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.CountryID <> sdr.GetInt32(2) Then
      '      parent = Me.GetItem(sdr.GetInt32(2))
      '    End If
      '    parent.ROProvinceList.RaiseListChangedEvents = False
      '    parent.ROProvinceList.Add(ROProvince.GetROProvince(sdr))
      '    parent.ROProvinceList.RaiseListChangedEvents = True
      '  End While
      'End If

      'Dim parentChild As ROProvince = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentChild Is Nothing OrElse parentChild.ProvinceID <> sdr.GetInt32(9) Then
      '      parentChild = Me.GetROProvince(sdr.GetInt32(9))
      '    End If
      '    parentChild.ROCityList.RaiseListChangedEvents = False
      '    parentChild.ROCityList.Add(ROCity.GetROCity(sdr))
      '    parentChild.ROCityList.RaiseListChangedEvents = True
      '  End While
      'End If


    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROCountryList"
            cm.Parameters.AddWithValue("@Country", Strings.MakeEmptyDBNull(crit.Country))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace