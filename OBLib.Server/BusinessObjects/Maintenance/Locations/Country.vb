﻿' Generated 24 Dec 2014 11:49 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Locations

  <Serializable()> _
  Public Class Country
    Inherits OBBusinessBase(Of Country)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "CountryBO.CountryBOToString(self)")

    Public Shared CountryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CountryID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property CountryID() As Integer
      Get
        Return GetProperty(CountryIDProperty)
      End Get
    End Property

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country", "")
    ''' <summary>
    ''' Gets and sets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="Name of the country"),
    StringLength(50, ErrorMessage:="Country cannot be more than 50 characters"),
    Required(ErrorMessage:="Country Required")>
    Public Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CountryProperty, Value)
      End Set
    End Property

    Public Shared CurrencyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CurrencyID, "Currency", Nothing)
    ''' <summary>
    ''' Gets and sets the Currency value
    ''' </summary>
    <Display(Name:="Currency", Description:="The currency in which the country deals with"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCurrencyList))>
    Public Property CurrencyID() As Integer?
      Get
        Return GetProperty(CurrencyIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CurrencyIDProperty, Value)
      End Set
    End Property

    Public Shared DefaultDailySnTRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.DefaultDailySnTRate, "Default Daily S&T Rate", CDec(0))
    ''' <summary>
    ''' Gets and sets the Default Daily Sn T Rate value
    ''' </summary>
    <Display(Name:="Default Daily S&T Rate", Description:="The country's default daily S&T rate"),
    Required(ErrorMessage:="Default Daily Sn T Rate required")>
    Public Property DefaultDailySnTRate() As Decimal
      Get
        Return GetProperty(DefaultDailySnTRateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(DefaultDailySnTRateProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="The system to which the country belongs"),
    Browsable(False)>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ImportedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ImportedInd, "Imported", False)
    ''' <summary>
    ''' Gets and sets the Imported value
    ''' </summary>
    <Display(Name:="Imported", Description:=""),
    Browsable(False)>
    Public Property ImportedInd() As Boolean
      Get
        Return GetProperty(ImportedIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ImportedIndProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProvinceListProperty As PropertyInfo(Of ProvinceList) = RegisterProperty(Of ProvinceList)(Function(c) c.ProvinceList, "Province List")

    Public ReadOnly Property ProvinceList() As ProvinceList
      Get
        If GetProperty(ProvinceListProperty) Is Nothing Then
          LoadProperty(ProvinceListProperty, Maintenance.Locations.ProvinceList.NewProvinceList())
        End If
        Return GetProperty(ProvinceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CountryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Country.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Country")
        Else
          Return String.Format("Blank {0}", "Country")
        End If
      Else
        Return Me.Country
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"Provinces"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(DefaultDailySnTRateProperty)
        .JavascriptRuleFunctionName = "CountryBO.DefaultDailySnTRateValid"
        .ServerRuleFunction = AddressOf DefaultDailySnTRateValid
      End With

    End Sub

    Public Shared Function DefaultDailySnTRateValid(country As Country) As String
      Dim ErrorString = ""
      If country.DefaultDailySnTRate < 0 Then
        ErrorString = "Default Daily S&T Rate cannot be less then 0"
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCountry() method.

    End Sub

    Public Shared Function NewCountry() As Country

      Return DataPortal.CreateChild(Of Country)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCountry(dr As SafeDataReader) As Country

      Dim c As New Country()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CountryIDProperty, .GetInt32(0))
          LoadProperty(CountryProperty, .GetString(1))
          LoadProperty(CurrencyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DefaultDailySnTRateProperty, .GetDecimal(3))
          LoadProperty(SystemIDProperty, .GetInt32(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ImportedIndProperty, .GetBoolean(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCountry"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCountry"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCountryID As SqlParameter = .Parameters.Add("@CountryID", SqlDbType.Int)
          paramCountryID.Value = GetProperty(CountryIDProperty)
          If Me.IsNew Then
            paramCountryID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Country", GetProperty(CountryProperty))
          .Parameters.AddWithValue("@CurrencyID", Singular.Misc.NothingDBNull(GetProperty(CurrencyIDProperty)))
          .Parameters.AddWithValue("@DefaultDailySnTRate", GetProperty(DefaultDailySnTRateProperty))
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@ImportedInd", GetProperty(ImportedIndProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CountryIDProperty, paramCountryID.Value)
          End If
          ' update child objects
          If GetProperty(ProvinceListProperty) IsNot Nothing Then
            Me.ProvinceList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ProvinceListProperty) IsNot Nothing Then
          Me.ProvinceList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCountry"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CountryID", GetProperty(CountryIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace