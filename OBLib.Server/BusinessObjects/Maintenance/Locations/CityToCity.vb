﻿' Generated 24 Dec 2014 11:55 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Locations.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Locations

  <Serializable()> _
  Public Class CityToCity
    Inherits OBBusinessBase(Of CityToCity)

#Region " Properties and Methods "

    Private mManageMirror As Boolean = False

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "CityToCityBO.CityToCityBOToString(self)")

    Public Shared CityToCityIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CityToCityID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property CityToCityID() As Integer
      Get
        Return GetProperty(CityToCityIDProperty)
      End Get
    End Property

    Public Shared CityID1Property As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID1, "City ID 1", Nothing)
    ''' <summary>
    ''' Gets and sets the City ID 1 value
    ''' </summary>
    <Display(Name:="City ID 1", Description:="City from"),
    Browsable(False)>
    Public Property CityID1() As Integer?
      Get
        Return GetProperty(CityID1Property)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CityID1Property, Value)
      End Set
    End Property

    Public Shared CityID2Property As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID2, "City ID 2", Nothing)
    ''' <summary>
    ''' Gets and sets the City ID 2 value
    ''' </summary>
    <Display(Name:="Destination City", Description:="City to"),
    Required(ErrorMessage:="Destination City required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCityList))>
    Public Property CityID2() As Integer?
      Get
        Return GetProperty(CityID2Property)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CityID2Property, Value)
      End Set
    End Property

    Public Shared DistanceProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Distance, "Distance", 0)
    ''' <summary>
    ''' Gets and sets the Distance value
    ''' </summary>
    <Display(Name:="Distance", Description:="The number of KMs between cities"),
    Required(ErrorMessage:="Distance required")>
    Public Property Distance() As Integer
      Get
        Return GetProperty(DistanceProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(DistanceProperty, Value)
        ManageMirror()
      End Set
    End Property

    Public Shared TruckDriveTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.TruckDriveTime, "Truck Drive Time")
    ''' <summary>
    ''' Gets and sets the Truck Drive Time value
    ''' </summary>
    <Display(Name:="Truck Drive Time", Description:="The number of hours it takes for a truck to travel between cities")>
    Public Property TruckDriveTime As Object
      Get
        Dim value = GetProperty(TruckDriveTimeProperty)
        If value = DateTime.MinValue Or IsNullNothing(value) Then
          Return Nothing
        Else
          If DateDiff(DateInterval.Day, New DateTime(2000, 1, 1), value) = 0 Then
            Return CDate(value).ToString("HH:mm")
          Else
            Return "24:00"
          End If
        End If
        'Return GetProperty(TruckDriveTimeProperty)
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(TruckDriveTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(TruckDriveTimeProperty, Nothing)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(TruckDriveTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
            ManageMirror()
          End If
        End If
        'SetProperty(TruckDriveTimeProperty, Value)
      End Set
    End Property

    'Public Shared TruckDriveTimeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TruckDriveTime, "Truck Drive Time", 0)
    ' ''' <summary>
    ' ''' Gets and sets the Truck Drive Time value
    ' ''' </summary>
    '<Display(Name:="Truck Drive Time", Description:="The number of hours it takes for a truck to travel between cities"),
    'Required(ErrorMessage:="Truck Drive Time required")>
    'Public Property TruckDriveTime() As Integer
    '  Get
    '    Return GetProperty(TruckDriveTimeProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(TruckDriveTimeProperty, Value)
    '  End Set
    'End Property

    Public Shared CarDriveTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CarDriveTime, "Truck Drive Time")
    ''' <summary>
    ''' Gets and sets the Car Drive Time value
    ''' </summary>
    <Display(Name:="Car Drive Time", Description:="The number of hours it takes to drive between cities")>
    Public Property CarDriveTime As Object
      Get
        Dim value = GetProperty(CarDriveTimeProperty)
        If value = DateTime.MinValue Or IsNullNothing(value) Then
          Return Nothing
        Else
          If DateDiff(DateInterval.Day, New DateTime(2000, 1, 1), value) = 0 Then
            Return CDate(value).ToString("HH:mm")
          Else
            Return "24:00"
          End If
        End If
        'Return GetProperty(TruckDriveTimeProperty)
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(CarDriveTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(CarDriveTimeProperty, Nothing)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(CarDriveTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
            ManageMirror()
          End If
        End If
        'SetProperty(TruckDriveTimeProperty, Value)
      End Set
    End Property

    'Public Shared CarDriveTimeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CarDriveTime, "Car Drive Time", 0)
    ' ''' <summary>
    ' ''' Gets and sets the Car Drive Time value
    ' ''' </summary>
    '<Display(Name:="Car Drive Time", Description:="The number of hours it takes to drive from city1 to city2"),
    'Required(ErrorMessage:="Car Drive Time required")>
    'Public Property CarDriveTime() As Integer
    '  Get
    '    Return GetProperty(CarDriveTimeProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(CarDriveTimeProperty, Value)
    '  End Set
    'End Property

    Public Shared FlightTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.FlightTime, "Truck Drive Time")
    ''' <summary>
    ''' Gets and sets the Flight Time value
    ''' </summary>
    <Display(Name:="Flight Time", Description:="The number of hours it takes to fly between cities")>
    Public Property FlightTime As Object
      Get
        Dim value = GetProperty(FlightTimeProperty)
        If value = DateTime.MinValue Or IsNullNothing(value) Then
          Return Nothing
        Else
          If DateDiff(DateInterval.Day, New DateTime(2000, 1, 1), value) = 0 Then
            Return CDate(value).ToString("HH:mm")
          Else
            Return "24:00"
          End If
        End If
        'Return GetProperty(TruckDriveTimeProperty)
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(FlightTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(FlightTimeProperty, Nothing)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(FlightTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
            ManageMirror()
          End If
        End If
        'SetProperty(TruckDriveTimeProperty, Value)
      End Set
    End Property

    'Public Shared FlightTimeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightTime, "Flight Time", 0)
    ' ''' <summary>
    ' ''' Gets and sets the Flight Time value
    ' ''' </summary>
    '<Display(Name:="Flight Time", Description:="The number of hours it take to fly from airport to airport"),
    'Required(ErrorMessage:="Flight Time required")>
    'Public Property FlightTime() As Integer
    '  Get
    '    Return GetProperty(FlightTimeProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(FlightTimeProperty, Value)
    '  End Set
    'End Property

    Public Shared AirTravelTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.AirTravelTime, "Truck Drive Time")
    ''' <summary>
    ''' Gets and sets the Flight Time value
    ''' </summary>
    <Display(Name:="Air Travel Time", Description:="The number of hours it takes to drive to the airport, fly, collect luggage and drive to the venue (FlightTime + 3)")>
    Public Property AirTravelTime As Object
      Get
        Dim value = GetProperty(AirTravelTimeProperty)
        If value = DateTime.MinValue Or IsNullNothing(value) Then
          Return Nothing
        Else
          If DateDiff(DateInterval.Day, New DateTime(2000, 1, 1), value) = 0 Then
            Return CDate(value).ToString("HH:mm")
          Else
            Return "24:00"
          End If
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(AirTravelTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(AirTravelTimeProperty, Nothing)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(AirTravelTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
            ManageMirror()
          End If
        End If
      End Set
    End Property

    'Public Shared AirTravelTimeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AirTravelTime, "Air Travel Time", 0)
    ' ''' <summary>
    ' ''' Gets and sets the Air Travel Time value
    ' ''' </summary>
    '<Display(Name:="Air Travel Time", Description:="The number of hours it takes to drive to the airport, fly, collect luggage and drive to the venue (FlightTime + 3)"),
    'Required(ErrorMessage:="Air Travel Time required")>
    'Public Property AirTravelTime() As Integer
    '  Get
    '    Return GetProperty(AirTravelTimeProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(AirTravelTimeProperty, Value)
    '  End Set
    'End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As City

      Return CType(CType(Me.Parent, CityToCityList).Parent, City)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CityToCityIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Distance.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "City To City")
        Else
          Return String.Format("Blank {0}", "City To City")
        End If
      Else
        Return Me.Distance.ToString()
      End If

    End Function

    Private Function ConvertIntToTimeSpan(value As Integer) As TimeSpan
      If value = 0 Then
        Return TimeSpan.MinValue
      Else
        Dim ts As New TimeSpan(0, value, 0)
        Return ts 'Singular.Misc.TimeSpanToString(ts)
      End If
    End Function

    Private Function ConvertTimeSpanToInt(value As Object) As Integer

      'Dim ts As TimeSpan? = Nothing
      Dim TotalMinutes As Integer = 0
      If value IsNot Nothing Then
        Dim ts As TimeSpan = CDate(GetProperty(TruckDriveTimeProperty)).TimeOfDay
        TotalMinutes = ts.TotalMinutes()
      End If

      Return TotalMinutes

    End Function

    Private Sub ManageMirror()

      If mManageMirror Or GetProperty(CityID2Property) = 0 Then Exit Sub

      Dim c As City = CType(Me.GetParent.Parent, CityList).GetItem(GetProperty(CityID2Property))
      If c IsNot Nothing Then
        Dim ctcMirror As CityToCity = c.CityToCityList.GetItemByCityID2(Me.GetParent.CityID)
        If ctcMirror Is Nothing Then
          ctcMirror = c.CityToCityList.AddNew
          ctcMirror.CityID2 = Me.GetParent.CityID
        End If

        mManageMirror = True
        ctcMirror.mManageMirror = True

        ctcMirror.Distance = GetProperty(DistanceProperty)
        ctcMirror.TruckDriveTime = GetProperty(TruckDriveTimeProperty)
        ctcMirror.CarDriveTime = GetProperty(CarDriveTimeProperty)
        ctcMirror.FlightTime = GetProperty(FlightTimeProperty)
        ctcMirror.AirTravelTime = GetProperty(AirTravelTimeProperty)

        mManageMirror = False
        ctcMirror.mManageMirror = False
      End If

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(CityID2Property)
        .JavascriptRuleFunctionName = "CityToCityBO.CheckParentCityID"
        .ServerRuleFunction = AddressOf CheckParentCityID
      End With

    End Sub

    Public Shared Function CheckParentCityID(C2C As CityToCity) As String
      Dim ErrorString = ""
      If C2C.Parent IsNot Nothing Then
        If C2C.GetParent.CityID.Equals(C2C.CityID2) Then
          ErrorString = "Start and end cities cannot be the same"
        End If
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCityToCity() method.

    End Sub

    Public Shared Function NewCityToCity() As CityToCity

      Return DataPortal.CreateChild(Of CityToCity)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCityToCity(dr As SafeDataReader) As CityToCity

      Dim c As New CityToCity()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CityToCityIDProperty, .GetInt32(0))
          LoadProperty(CityID1Property, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(CityID2Property, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DistanceProperty, .GetInt32(3))

          If .IsDBNull(4) OrElse .GetInt32(4) = 0 Then
            LoadProperty(TruckDriveTimeProperty, Nothing)
          Else
            LoadProperty(TruckDriveTimeProperty, (New DateTime(2000, 1, 1).Add(ConvertIntToTimeSpan(.GetInt32(4)))))
          End If

          If .IsDBNull(5) OrElse .GetInt32(5) = 0 Then
            LoadProperty(CarDriveTimeProperty, Nothing)
          Else
            LoadProperty(CarDriveTimeProperty, (New DateTime(2000, 1, 1).Add(ConvertIntToTimeSpan(.GetInt32(5)))))
          End If

          If .IsDBNull(6) OrElse .GetInt32(6) = 0 Then
            LoadProperty(FlightTimeProperty, Nothing)
          Else
            LoadProperty(FlightTimeProperty, (New DateTime(2000, 1, 1).Add(ConvertIntToTimeSpan(.GetInt32(6)))))
          End If

          If .IsDBNull(7) OrElse .GetInt32(7) = 0 Then
            LoadProperty(AirTravelTimeProperty, Nothing)
          Else
            LoadProperty(AirTravelTimeProperty, (New DateTime(2000, 1, 1).Add(ConvertIntToTimeSpan(.GetInt32(7)))))
          End If

          'LoadProperty(TruckDriveTimeProperty, ConvertIntToTimeSpan(.GetInt32(4)))
          'LoadProperty(CarDriveTimeProperty, .GetInt32(5))
          'LoadProperty(FlightTimeProperty, .GetInt32(6))
          'LoadProperty(AirTravelTimeProperty, .GetInt32(7))

          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      'BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCityToCity"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCityToCity"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCityToCityID As SqlParameter = .Parameters.Add("@CityToCityID", SqlDbType.Int)
          paramCityToCityID.Value = GetProperty(CityToCityIDProperty)
          If Me.IsNew Then
            paramCityToCityID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@CityID1", Me.GetParent.CityID)
          .Parameters.AddWithValue("@CityID2", GetProperty(CityID2Property))
          .Parameters.AddWithValue("@Distance", GetProperty(DistanceProperty))

          ''Dim ts As TimeSpan? = Nothing
          'Dim TotalMinutes As Integer = 0
          'If (New SmartDate(GetProperty(TruckDriveTimeProperty))).DBValue IsNot Nothing Then
          '  Dim ts As TimeSpan = CDate(GetProperty(TruckDriveTimeProperty)).TimeOfDay
          '  TotalMinutes = ts.TotalMinutes()
          'End If

          .Parameters.AddWithValue("@TruckDriveTime", ConvertTimeSpanToInt((New SmartDate(GetProperty(TruckDriveTimeProperty))).DBValue))
          .Parameters.AddWithValue("@CarDriveTime", ConvertTimeSpanToInt((New SmartDate(GetProperty(CarDriveTimeProperty))).DBValue)) 'GetProperty(CarDriveTimeProperty))
          .Parameters.AddWithValue("@FlightTime", ConvertTimeSpanToInt((New SmartDate(GetProperty(FlightTimeProperty))).DBValue)) 'GetProperty(FlightTimeProperty))
          .Parameters.AddWithValue("@AirTravelTime", ConvertTimeSpanToInt((New SmartDate(GetProperty(AirTravelTimeProperty))).DBValue)) 'GetProperty(AirTravelTimeProperty))

          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CityToCityIDProperty, paramCityToCityID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCityToCity"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CityToCityID", GetProperty(CityToCityIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace