﻿' Generated 27 Jan 2015 09:45 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.HR

  <Serializable()> _
  Public Class OffReason
    Inherits OBBusinessBase(Of OffReason)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared OffReasonIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OffReasonID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property OffReasonID() As Integer
      Get
        Return GetProperty(OffReasonIDProperty)
      End Get
    End Property

    Public Shared OffReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OffReason, "Off Reason", "")
    ''' <summary>
    ''' Gets and sets the Off Reason value
    ''' </summary>
    <Display(Name:="Off Reason", Description:="Description for the Off Reason"),
    Required(ErrorMessage:="Off Reason required"),
    StringLength(50, ErrorMessage:="Off Reason cannot be more than 50 characters")>
    Public Property OffReason() As String
      Get
        Return GetProperty(OffReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OffReasonProperty, Value)
      End Set
    End Property

    Public Shared DetailRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DetailRequiredInd, "Detail Required", False)
    ''' <summary>
    ''' Gets and sets the Detail Required value
    ''' </summary>
    <Display(Name:="Detail Required", Description:="Tick indicates that extra detail must be captured if this Off Reason is selected"),
    Required(ErrorMessage:="Detail Required required")>
    Public Property DetailRequiredInd() As Boolean
      Get
        Return GetProperty(DetailRequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DetailRequiredIndProperty, Value)
      End Set
    End Property

    Public Shared FreelancerIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.FreelancerInd, "Freelancer", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Freelancer value
    ''' </summary>
    <Display(Name:="Freelancer", Description:="Tick indiicates that this off reason only applies to contract workers. NULL indicates that this reason applies to both contract and non-contract.")>
    Public Property FreelancerInd() As Boolean?
      Get
        Return GetProperty(FreelancerIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(FreelancerIndProperty, Value)
      End Set
    End Property

    Public Shared AuthorisationRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AuthorisationRequiredInd, "Authorisation Required", False)
    ''' <summary>
    ''' Gets and sets the Authorisation Required value
    ''' </summary>
    <Display(Name:="Authorisation Required", Description:="Tick indicates that the HR that is off with this reason required authorisation"),
    Required(ErrorMessage:="Authorisation Required required")>
    Public Property AuthorisationRequiredInd() As Boolean
      Get
        Return GetProperty(AuthorisationRequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(AuthorisationRequiredIndProperty, Value)
      End Set
    End Property

    Public Shared UnpaidLeaveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UnpaidLeaveInd, "Unpaid Leave", False)
    ''' <summary>
    ''' Gets and sets the Unpaid Leave value
    ''' </summary>
    <Display(Name:="Unpaid Leave", Description:="True if the leave is categorised as unpaid leave"),
    Required(ErrorMessage:="Unpaid Leave required")>
    Public Property UnpaidLeaveInd() As Boolean
      Get
        Return GetProperty(UnpaidLeaveIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(UnpaidLeaveIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared IsSelectableProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsSelectable, "Can Select Reason?", False)
    ''' <summary>
    ''' Gets the IsSelectable value
    ''' </summary>
    Public ReadOnly Property IsSelectable() As Boolean
      Get
        Return GetProperty(IsSelectableProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(OffReasonIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.OffReason.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Off Reason")
        Else
          Return String.Format("Blank {0}", "Off Reason")
        End If
      Else
        Return Me.OffReason
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewOffReason() method.

    End Sub

    Public Shared Function NewOffReason() As OffReason

      Return DataPortal.CreateChild(Of OffReason)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetOffReason(dr As SafeDataReader) As OffReason

      Dim o As New OffReason()
      o.Fetch(dr)
      Return o

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(OffReasonIDProperty, .GetInt32(0))
          LoadProperty(OffReasonProperty, .GetString(1))
          LoadProperty(DetailRequiredIndProperty, .GetBoolean(2))
          Dim tmpFreelancerInd = .GetValue(3)
          If IsDBNull(tmpFreelancerInd) Then
            LoadProperty(FreelancerIndProperty, Nothing)
          Else
            LoadProperty(FreelancerIndProperty, tmpFreelancerInd)
          End If
          LoadProperty(AuthorisationRequiredIndProperty, .GetBoolean(4))
          LoadProperty(UnpaidLeaveIndProperty, .GetBoolean(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(IsSelectableProperty, .GetBoolean(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insOffReason"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updOffReason"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramOffReasonID As SqlParameter = .Parameters.Add("@OffReasonID", SqlDbType.Int)
          paramOffReasonID.Value = GetProperty(OffReasonIDProperty)
          If Me.IsNew Then
            paramOffReasonID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@OffReason", GetProperty(OffReasonProperty))
          .Parameters.AddWithValue("@DetailRequiredInd", GetProperty(DetailRequiredIndProperty))
          .Parameters.AddWithValue("@FreelancerInd", Singular.Misc.NothingDBNull(GetProperty(FreelancerIndProperty)))
          .Parameters.AddWithValue("@AuthorisationRequiredInd", GetProperty(AuthorisationRequiredIndProperty))
          .Parameters.AddWithValue("@UnpaidLeaveInd", GetProperty(UnpaidLeaveIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(OffReasonIDProperty, paramOffReasonID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delOffReason"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@OffReasonID", GetProperty(OffReasonIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace