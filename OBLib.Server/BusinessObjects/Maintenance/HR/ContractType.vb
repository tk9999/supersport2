﻿' Generated 25 Jan 2015 12:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.HR

  <Serializable()> _
  Public Class ContractType
    Inherits SingularBusinessBase(Of ContractType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ContractTypeID() As Integer
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type", "")
    ''' <summary>
    ''' Gets and sets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="Description for the Contract Type"),
    Required(ErrorMessage:="Contract Type required"),
    StringLength(50, ErrorMessage:="Contract Type cannot be more than 50 characters")>
    Public Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContractTypeProperty, Value)
      End Set
    End Property

    Public Shared FreelancerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FreelancerInd, "Freelancer", True)
    ''' <summary>
    ''' Gets and sets the Freelancer value
    ''' </summary>
    <Display(Name:="Freelancer", Description:="True indicates that this contract type is a contract"),
    Required(ErrorMessage:="Freelancer required")>
    Public Property FreelancerInd() As Boolean
      Get
        Return GetProperty(FreelancerIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(FreelancerIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortName, "Short Name", "")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Short Name")>
    Public Property ShortName() As String
      Get
        Return GetProperty(ShortNameProperty)
      End Get
      Set(value As String)
        SetProperty(ShortNameProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ContractTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ContractType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Contract Type")
        Else
          Return String.Format("Blank {0}", "Contract Type")
        End If
      Else
        Return Me.ContractType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewContractType() method.

    End Sub

    Public Shared Function NewContractType() As ContractType

      Return DataPortal.CreateChild(Of ContractType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetContractType(dr As SafeDataReader) As ContractType

      Dim c As New ContractType()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ContractTypeIDProperty, .GetInt32(0))
          LoadProperty(ContractTypeProperty, .GetString(1))
          LoadProperty(FreelancerIndProperty, .GetBoolean(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ShortNameProperty, .GetString(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insContractType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updContractType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramContractTypeID As SqlParameter = .Parameters.Add("@ContractTypeID", SqlDbType.Int)
          paramContractTypeID.Value = GetProperty(ContractTypeIDProperty)
          If Me.IsNew Then
            paramContractTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ContractType", GetProperty(ContractTypeProperty))
          .Parameters.AddWithValue("@FreelancerInd", GetProperty(FreelancerIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@ShortName", GetProperty(ShortNameProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ContractTypeIDProperty, paramContractTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delContractType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ContractTypeID", GetProperty(ContractTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace