﻿' Generated 12 Aug 2014 13:50 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.HR.ReadOnly

  <Serializable()> _
  Public Class ROOffReason
    Inherits OBReadOnlyBase(Of ROOffReason)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared OffReasonIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OffReasonID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property OffReasonID() As Integer
      Get
        Return GetProperty(OffReasonIDProperty)
      End Get
    End Property

    Public Shared OffReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OffReason, "Off Reason", "")
    ''' <summary>
    ''' Gets the Off Reason value
    ''' </summary>
    <Display(Name:="Off Reason", Description:="Description for the Off Reason")>
    Public ReadOnly Property OffReason() As String
      Get
        Return GetProperty(OffReasonProperty)
      End Get
    End Property

    Public Shared DetailRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DetailRequiredInd, "Detail Required", False)
    ''' <summary>
    ''' Gets the Detail Required value
    ''' </summary>
    <Display(Name:="Detail Required", Description:="Tick indicates that extra detail must be captured if this Off Reason is selected")>
    Public ReadOnly Property DetailRequiredInd() As Boolean
      Get
        Return GetProperty(DetailRequiredIndProperty)
      End Get
    End Property

    Public Shared FreelancerIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.FreelancerInd, "Freelancer", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets the Freelancer value
    ''' </summary>
    <Display(Name:="Freelancer", Description:="Tick indiicates that this off reason only applies to contract workers. NULL indicates that this reason applies to both contract and non-contract.")>
    Public ReadOnly Property FreelancerInd() As Boolean?
      Get
        Return GetProperty(FreelancerIndProperty)
      End Get
    End Property

    Public Shared AuthorisationRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AuthorisationRequiredInd, "Authorisation Required", False)
    ''' <summary>
    ''' Gets the Authorisation Required value
    ''' </summary>
    <Display(Name:="Authorisation Required", Description:="Tick indicates that the HR that is off with this reason required authorisation")>
    Public ReadOnly Property AuthorisationRequiredInd() As Boolean
      Get
        Return GetProperty(AuthorisationRequiredIndProperty)
      End Get
    End Property

    Public Shared UnpaidLeaveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UnpaidLeaveInd, "Unpaid Leave", False)
    ''' <summary>
    ''' Gets the Unpaid Leave value
    ''' </summary>
    <Display(Name:="Unpaid Leave", Description:="")>
    Public ReadOnly Property UnpaidLeaveInd() As Boolean
      Get
        Return GetProperty(UnpaidLeaveIndProperty)
      End Get
    End Property

    Public Shared IsSelectableProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsSelectable, "Is Selectable?", True)
    ''' <summary>
    ''' Gets the IsSelectable value
    ''' </summary>
    <Display(Name:="", Description:="Selectable nature of the Off Reason")>
    Public ReadOnly Property IsSelectable() As Boolean
      Get
        Return GetProperty(IsSelectableProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(OffReasonIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.OffReason

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROOffReason(dr As SafeDataReader) As ROOffReason

      Dim r As New ROOffReason()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(OffReasonIDProperty, .GetInt32(0))
        LoadProperty(OffReasonProperty, .GetString(1))
        LoadProperty(DetailRequiredIndProperty, .GetBoolean(2))
        Dim tmpFreelancerInd = .GetValue(3)
        If IsDBNull(tmpFreelancerInd) Then
          LoadProperty(FreelancerIndProperty, Nothing)
        Else
          LoadProperty(FreelancerIndProperty, tmpFreelancerInd)
        End If
        LoadProperty(AuthorisationRequiredIndProperty, .GetBoolean(4))
        LoadProperty(UnpaidLeaveIndProperty, .GetBoolean(5))
        LoadProperty(IsSelectableProperty, .GetBoolean(6))
        LoadProperty(CreatedByProperty, .GetInt32(7))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(ModifiedByProperty, .GetInt32(9))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace