﻿' Generated 12 Aug 2014 09:20 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROGender
    Inherits OBReadOnlyBase(Of ROGender)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared GenderIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GenderID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property GenderID() As Integer
      Get
        Return GetProperty(GenderIDProperty)
      End Get
    End Property

    Public Shared GenderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Gender, "Gender", "")
    ''' <summary>
    ''' Gets the Gender value
    ''' </summary>
    <Display(Name:="Gender", Description:="Description for the Gender")>
    Public ReadOnly Property Gender() As String
      Get
        Return GetProperty(GenderProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(GenderIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Gender

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROGender(dr As SafeDataReader) As ROGender

      Dim r As New ROGender()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(GenderIDProperty, .GetInt32(0))
        LoadProperty(GenderProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace