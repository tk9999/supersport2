﻿' Generated 11 Aug 2014 14:56 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.HR.ReadOnly

  <Serializable()> _
  Public Class ROContractType
    Inherits OBReadOnlyBase(Of ROContractType)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared SelectedIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.SelectedInd, False) _
    '                                                                .AddSetExpression("ROContractTypeBO.SelectedIndSet(self)", False)
    ' ''' <summary>
    ' ''' Gets the Outside Broadcast Ind value
    ' ''' </summary>
    'Public Property SelectedInd() As Boolean
    '  Get
    '    Return GetProperty(SelectedIndProperty)
    '  End Get
    '  Set(value As Boolean)
    '    LoadProperty(SelectedIndProperty, value)
    '  End Set
    'End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ContractTypeID() As Integer
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type", "")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="Description for the Contract Type")>
    Public ReadOnly Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared FreelancerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FreelancerInd, "Freelancer", True)
    ''' <summary>
    ''' Gets the Freelancer value
    ''' </summary>
    <Display(Name:="Freelancer", Description:="True indicates that this contract type is a contract")>
    Public ReadOnly Property FreelancerInd() As Boolean
      Get
        Return GetProperty(FreelancerIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortName, "Short Name", "")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Short Name")>
    Public ReadOnly Property ShortName() As String
      Get
        Return GetProperty(ShortNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ContractTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ContractType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROContractType(dr As SafeDataReader) As ROContractType

      Dim r As New ROContractType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ContractTypeIDProperty, .GetInt32(0))
        LoadProperty(ContractTypeProperty, .GetString(1))
        LoadProperty(FreelancerIndProperty, .GetBoolean(2))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ShortNameProperty, .GetString(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace