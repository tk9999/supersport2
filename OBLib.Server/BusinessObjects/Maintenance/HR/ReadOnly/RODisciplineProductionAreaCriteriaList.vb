﻿' Generated 09 Jun 2015 14:07 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

<Serializable()> _
Public Class RODisciplineProductionAreaCriteriaList
  Inherits OBReadOnlyListBase(Of RODisciplineProductionAreaCriteriaList, RODisciplineProductionAreaCriteria)

#Region " Business Methods "

  Public Function GetItem(DisciplineProductionAreaCriteriaID As Integer) As RODisciplineProductionAreaCriteria

    For Each child As RODisciplineProductionAreaCriteria In Me
      If child.DisciplineProductionAreaCriteriaID = DisciplineProductionAreaCriteriaID Then
        Return child
      End If
    Next
    Return Nothing

  End Function

  Public Overrides Function ToString() As String

    Return "Discipline Production Area Criterias"

  End Function

#End Region

#Region " Data Access "

  <Serializable()> _
  Public Class Criteria
    Inherits CriteriaBase(Of Criteria)

    Public Property DisciplineID As Integer

    Public Property ProductionAreaID As Integer

    Public Property SystemID As Integer?

    Public Sub New()


    End Sub



  End Class

#Region " Common "

  Public Shared Function NewRODisciplineProductionAreaCriteriaList() As RODisciplineProductionAreaCriteriaList

    Return New RODisciplineProductionAreaCriteriaList()

  End Function

  Public Shared Sub BeginGetRODisciplineProductionAreaCriteriaList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RODisciplineProductionAreaCriteriaList)))

    Dim dp As New DataPortal(Of RODisciplineProductionAreaCriteriaList)()
    AddHandler dp.FetchCompleted, CallBack
    dp.BeginFetch(criteria)

  End Sub


  Public Shared Sub BeginGetRODisciplineProductionAreaCriteriaList(CallBack As EventHandler(Of DataPortalResult(Of RODisciplineProductionAreaCriteriaList)))

    Dim dp As New DataPortal(Of RODisciplineProductionAreaCriteriaList)()
    AddHandler dp.FetchCompleted, CallBack
    dp.BeginFetch(New Criteria())

  End Sub

  Public Sub New()

    ' must have parameter-less constructor

  End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

  Public Shared Function GetRODisciplineProductionAreaCriteriaList() As RODisciplineProductionAreaCriteriaList

    Return DataPortal.Fetch(Of RODisciplineProductionAreaCriteriaList)(New Criteria())

  End Function

  Public Shared Function GetRODisciplineProductionAreaCriteriaList(DisciplineID As Integer, ProductionAreaID As Integer, SystemID As Integer?) As RODisciplineProductionAreaCriteriaList

    Return DataPortal.Fetch(Of RODisciplineProductionAreaCriteriaList)(New Criteria() With {.DisciplineID = DisciplineID, .ProductionAreaID = ProductionAreaID, .SystemID = SystemID})

  End Function

  Private Sub Fetch(sdr As SafeDataReader)

    Me.RaiseListChangedEvents = False
    Me.IsReadOnly = False
    While sdr.Read
      Me.Add(RODisciplineProductionAreaCriteria.GetRODisciplineProductionAreaCriteria(sdr))
    End While
    Me.IsReadOnly = True
    Me.RaiseListChangedEvents = True

  End Sub

  Protected Overrides Sub DataPortal_Fetch(criteria As Object)

    Dim crit As Criteria = criteria
    Using cn As New SqlConnection(Singular.Settings.ConnectionString)
      cn.Open()
      Try
        Using cm As SqlCommand = cn.CreateCommand
          cm.CommandType = CommandType.StoredProcedure
          cm.CommandText = "GetProcsWeb.getRODisciplineProductionAreaCriteriaList"
          cm.Parameters.AddWithValue("@DisciplineID", Singular.Misc.ZeroDBNull(crit.DisciplineID))
          cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.ZeroDBNull(crit.ProductionAreaID))
          cm.Parameters.AddWithValue("@SystemID", Singular.Misc.ZeroDBNull(crit.SystemID))
          Using sdr As New SafeDataReader(cm.ExecuteReader)
            Fetch(sdr)
          End Using
        End Using
      Finally
        cn.Close()
      End Try
    End Using

  End Sub

#End If

#End Region

#End Region

End Class
