﻿' Generated 11 Aug 2014 14:56 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.HR.ReadOnly

  <Serializable()> _
  Public Class ROContractTypePagedList
    Inherits SingularReadOnlyListBase(Of ROContractTypePagedList, ROContractTypePaged)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ContractTypeID As Integer) As ROContractTypePaged

      For Each child As ROContractTypePaged In Me
        If child.ContractTypeID = ContractTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Contract Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      <Browsable(True)>
      Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ContractTypeID, Nothing)

      <Display(Name:="Contract Type ID")>
      Public Property ContractTypeID() As Integer?
        Get
          Return ReadProperty(ContractTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ContractTypeIDProperty, Value)
        End Set
      End Property

      <Browsable(True)>
      Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ContractType, "")

      <Display(Name:="Contract Type")>
      Public Property ContractType() As String
        Get
          Return ReadProperty(ContractTypeProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ContractTypeProperty, Value)
        End Set
      End Property

      Public Sub New(ContractType As String)
        Me.ContractType = ContractType
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROContractTypeList() As ROContractTypePagedList

      Return New ROContractTypePagedList()

    End Function

    Public Shared Sub BeginGetROContractTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROContractTypePagedList)))

      Dim dp As New DataPortal(Of ROContractTypePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROContractTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROContractTypePagedList)))

      Dim dp As New DataPortal(Of ROContractTypePagedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROContractTypePagedList() As ROContractTypePagedList

      Return DataPortal.Fetch(Of ROContractTypePagedList)(New Criteria())

    End Function

    'Public Shared Function GetROContractTypePagedList(ContractTypeID As Integer?) As ROContractTypePagedList

    '  Return DataPortal.Fetch(Of ROContractTypePagedList)(New Criteria(ContractTypeID))

    'End Function

    'Public Shared Function GetROContractTypePagedList(ContractType As String) As ROContractTypePagedList

    '  Return DataPortal.Fetch(Of ROContractTypePagedList)(New Criteria(ContractType))

    'End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROContractTypePaged.GetROContractTypePaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROContractTypePagedList"
            cm.Parameters.AddWithValue("@ContractType", Singular.Strings.MakeEmptyDBNull(crit.ContractType))
            cm.Parameters.AddWithValue("@ContractTypeID", NothingDBNull(crit.ContractTypeID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace