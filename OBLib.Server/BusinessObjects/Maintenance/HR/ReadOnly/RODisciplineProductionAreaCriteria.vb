﻿' Generated 09 Jun 2015 14:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

<Serializable()> _
Public Class RODisciplineProductionAreaCriteria
  Inherits OBReadOnlyBase(Of RODisciplineProductionAreaCriteria)

#Region " Properties and Methods "

#Region " Properties "

  Public Shared DisciplineProductionAreaCriteriaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineProductionAreaCriteriaID, "ID", 0)
  ''' <summary>
  ''' Gets the ID value
  ''' </summary>
  <Display(AutoGenerateField:=False), Key>
Public ReadOnly Property DisciplineProductionAreaCriteriaID() As Integer
    Get
      Return GetProperty(DisciplineProductionAreaCriteriaIDProperty)
    End Get
  End Property

  Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
  ''' <summary>
  ''' Gets the Discipline value
  ''' </summary>
  <Display(Name:="Discipline", Description:="")>
Public ReadOnly Property DisciplineID() As Integer?
    Get
      Return GetProperty(DisciplineIDProperty)
    End Get
  End Property

  Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area", 0)
  ''' <summary>
  ''' Gets the Production Area value
  ''' </summary>
  <Display(Name:="Production Area", Description:="")>
Public ReadOnly Property ProductionAreaID() As Integer
    Get
      Return GetProperty(ProductionAreaIDProperty)
    End Get
  End Property

  Public Shared PositionTypeRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PositionTypeRequiredInd, "Position Type Required", False)
  ''' <summary>
  ''' Gets the Position Type Required value
  ''' </summary>
  <Display(Name:="Position Type Required", Description:="")>
Public ReadOnly Property PositionTypeRequiredInd() As Boolean
    Get
      Return GetProperty(PositionTypeRequiredIndProperty)
    End Get
  End Property

  Public Shared RoomTypeRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomTypeRequiredInd, "Room Type Required", False)
  ''' <summary>
  ''' Gets the Room Type Required value
  ''' </summary>
  <Display(Name:="Room Type Required", Description:="")>
Public ReadOnly Property RoomTypeRequiredInd() As Boolean
    Get
      Return GetProperty(RoomTypeRequiredIndProperty)
    End Get
  End Property

  Public Shared ProductionTypeRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ProductionTypeRequiredInd, "Production Type Required", False)
  ''' <summary>
  ''' Gets the Production Type Required value
  ''' </summary>
  <Display(Name:="Production Type Required", Description:="")>
Public ReadOnly Property ProductionTypeRequiredInd() As Boolean
    Get
      Return GetProperty(ProductionTypeRequiredIndProperty)
    End Get
  End Property

  Public Shared RoomRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomRequiredInd, "Room Required", False)
  ''' <summary>
  ''' Gets the Room Required value
  ''' </summary>
  <Display(Name:="Room Required", Description:="")>
Public ReadOnly Property RoomRequiredInd() As Boolean
    Get
      Return GetProperty(RoomRequiredIndProperty)
    End Get
  End Property

  Public Shared PositionRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PositionRequiredInd, "Position Required", False)
  ''' <summary>
  ''' Gets the Position Required value
  ''' </summary>
  <Display(Name:="Position Required", Description:="")>
Public ReadOnly Property PositionRequiredInd() As Boolean
    Get
      Return GetProperty(PositionRequiredIndProperty)
    End Get
  End Property

  Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
  ''' <summary>
  ''' Gets the Created By value
  ''' </summary>
  <Display(AutoGenerateField:=False)>
Public ReadOnly Property CreatedBy() As Integer?
    Get
      Return GetProperty(CreatedByProperty)
    End Get
  End Property

  Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
  ''' <summary>
  ''' Gets the Created Date Time value
  ''' </summary>
  <Display(AutoGenerateField:=False)>
Public ReadOnly Property CreatedDateTime() As SmartDate
    Get
      Return GetProperty(CreatedDateTimeProperty)
    End Get
  End Property

  Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
  ''' <summary>
  ''' Gets the Modified By value
  ''' </summary>
  <Display(AutoGenerateField:=False)>
Public ReadOnly Property ModifiedBy() As Integer?
    Get
      Return GetProperty(ModifiedByProperty)
    End Get
  End Property

  Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
  ''' <summary>
  ''' Gets the Modified Date Time value
  ''' </summary>
  <Display(AutoGenerateField:=False)>
Public ReadOnly Property ModifiedDateTime() As SmartDate
    Get
      Return GetProperty(ModifiedDateTimeProperty)
    End Get
  End Property

#End Region

#Region " Methods "

  Protected Overrides Function GetIdValue() As Object

    Return GetProperty(DisciplineProductionAreaCriteriaIDProperty)

  End Function

  Public Overrides Function ToString() As String

    Return Me.CreatedDateTime.ToString()

  End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

  Friend Shared Function GetRODisciplineProductionAreaCriteria(dr As SafeDataReader) As RODisciplineProductionAreaCriteria

    Dim r As New RODisciplineProductionAreaCriteria()
    r.Fetch(dr)
    Return r

  End Function

  Protected Sub Fetch(sdr As SafeDataReader)

    With sdr
      LoadProperty(DisciplineProductionAreaCriteriaIDProperty, .GetInt32(0))
      LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
      LoadProperty(ProductionAreaIDProperty, .GetInt32(2))
      LoadProperty(PositionTypeRequiredIndProperty, .GetBoolean(3))
      LoadProperty(RoomTypeRequiredIndProperty, .GetBoolean(4))
      LoadProperty(ProductionTypeRequiredIndProperty, .GetBoolean(5))
      LoadProperty(RoomRequiredIndProperty, .GetBoolean(6))
      LoadProperty(PositionRequiredIndProperty, .GetBoolean(7))
      LoadProperty(CreatedByProperty, .GetInt32(8))
      LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
      LoadProperty(ModifiedByProperty, .GetInt32(10))
      LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
    End With

  End Sub

#End If

#End Region

#End Region

End Class
