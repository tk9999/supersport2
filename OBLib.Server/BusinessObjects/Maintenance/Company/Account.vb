﻿' Generated 14 Apr 2014 08:48 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class Account
    Inherits SingularBusinessBase(Of Account)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccountIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccountID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccountID() As Integer
      Get
        Return GetProperty(AccountIDProperty)
      End Get
    End Property

    Public Shared AccountCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccountCode, "Account Code", "")
    ''' <summary>
    ''' Gets and sets the Account Code value
    ''' </summary>
    <Display(Name:="Account Code", Description:=""),
    StringLength(10, ErrorMessage:="Account Code cannot be more than 10 characters"),
    Required(ErrorMessage:="Account Code is required")>
    Public Property AccountCode() As String
      Get
        Return GetProperty(AccountCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AccountCodeProperty, Value)
      End Set
    End Property

    Public Shared AccountProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Account, "Account", "")
    ''' <summary>
    ''' Gets and sets the Account value
    ''' </summary>
    <Display(Name:="Account", Description:=""),
    StringLength(50, ErrorMessage:="Account cannot be more than 50 characters"),
    Required(ErrorMessage:="Account Name is required")>
    Public Property Account() As String
      Get
        Return GetProperty(AccountProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AccountProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccountIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AccountCode.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Account")
        Else
          Return String.Format("Blank {0}", "Account")
        End If
      Else
        Return Me.AccountCode
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccount() method.

    End Sub

    Public Shared Function NewAccount() As Account

      Return DataPortal.CreateChild(Of Account)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccount(dr As SafeDataReader) As Account

      Dim a As New Account()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccountIDProperty, .GetInt32(0))
          LoadProperty(AccountCodeProperty, .GetString(1))
          LoadProperty(AccountProperty, .GetString(2))
          'LoadProperty(CostCentreIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insAccount"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updAccount"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccountID As SqlParameter = .Parameters.Add("@AccountID", SqlDbType.Int)
          paramAccountID.Value = GetProperty(AccountIDProperty)
          If Me.IsNew Then
            paramAccountID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccountCode", GetProperty(AccountCodeProperty))
          .Parameters.AddWithValue("@Account", GetProperty(AccountProperty))
          '.Parameters.AddWithValue("@CostCentreID", Me.GetParent().CostCentreID)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccountIDProperty, paramAccountID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delAccount"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccountID", GetProperty(AccountIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace