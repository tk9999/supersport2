﻿' Generated 02 Sep 2014 17:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Company.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class SystemEmail
    Inherits SingularBusinessBase(Of SystemEmail)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared SystemEmailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemEmailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemEmailID() As Integer
      Get
        Return GetProperty(SystemEmailIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared SystemEmailTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemEmailTypeID, "System Email Type", Nothing)
    ''' <summary>
    ''' Gets and sets the System Email Type value
    ''' </summary>
    <Display(Name:="System Email Type", Description:="Fuctionality within the system that needs to fire off an email"),
    Required(ErrorMessage:="System Email Type required"),
    DropDownWeb(GetType(ROSystemEmailTypeList))>
    Public Property SystemEmailTypeID() As Integer?
      Get
        Return GetProperty(SystemEmailTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemEmailTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared SystemEmailRecipientListProperty As PropertyInfo(Of SystemEmailRecipientList) = RegisterProperty(Of SystemEmailRecipientList)(Function(c) c.SystemEmailRecipientList, "System Email Recipient List")

    Public ReadOnly Property SystemEmailRecipientList() As SystemEmailRecipientList
      Get
        If GetProperty(SystemEmailRecipientListProperty) Is Nothing Then
          LoadProperty(SystemEmailRecipientListProperty, Maintenance.Company.SystemEmailRecipientList.NewSystemEmailRecipientList())
        End If
        Return GetProperty(SystemEmailRecipientListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As System

      Return CType(CType(Me.Parent, SystemEmailList).Parent, System)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemEmailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Email")
        Else
          Return String.Format("Blank {0}", "System Email")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"SystemEmailRecipients"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemEmail() method.

    End Sub

    Public Shared Function NewSystemEmail() As SystemEmail

      Return DataPortal.CreateChild(Of SystemEmail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSystemEmail(dr As SafeDataReader) As SystemEmail

      Dim s As New SystemEmail()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemEmailIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SystemEmailTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSystemEmail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSystemEmail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSystemEmailID As SqlParameter = .Parameters.Add("@SystemEmailID", SqlDbType.Int)
          paramSystemEmailID.Value = GetProperty(SystemEmailIDProperty)
          If Me.IsNew Then
            paramSystemEmailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SystemID", Me.GetParent().SystemID)
          .Parameters.AddWithValue("@SystemEmailTypeID", GetProperty(SystemEmailTypeIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SystemEmailIDProperty, paramSystemEmailID.Value)
          End If
          ' update child objects
          If GetProperty(SystemEmailRecipientListProperty) IsNot Nothing Then
            Me.SystemEmailRecipientList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(SystemEmailRecipientListProperty) IsNot Nothing Then
          Me.SystemEmailRecipientList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSystemEmail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SystemEmailID", GetProperty(SystemEmailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace