﻿' Generated 02 Sep 2014 17:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class SystemList
    Inherits SingularBusinessListBase(Of SystemList, System)

#Region " Business Methods "

    Public Function GetItem(SystemID As Integer) As System

      For Each child As System In Me
        If child.SystemID = SystemID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Systems"

    End Function

    Public Function GetClashCCEmail(ClashCCEmailID As Integer) As ClashCCEmail

      Dim obj As ClashCCEmail = Nothing
      For Each parent As System In Me
        obj = parent.ClashCCEmailList.GetItem(ClashCCEmailID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemEmail(SystemEmailID As Integer) As SystemEmail

      Dim obj As SystemEmail = Nothing
      For Each parent As System In Me
        obj = parent.SystemEmailList.GetItem(SystemEmailID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemAuthorisationSection(SystemAuthorisationSectionID As Integer) As SystemAuthorisationSection

      Dim obj As SystemAuthorisationSection = Nothing
      For Each parent As System In Me
        obj = parent.SystemAuthorisationSectionList.GetItem(SystemAuthorisationSectionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewSystemList() As SystemList

      Return New SystemList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As System In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As System In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace