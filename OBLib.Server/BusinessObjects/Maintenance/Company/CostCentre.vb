﻿' Generated 02 Sep 2014 17:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class CostCentre
    Inherits SingularBusinessBase(Of CostCentre)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CostCentreIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CostCentreID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CostCentreID() As Integer
      Get
        Return GetProperty(CostCentreIDProperty)
      End Get
    End Property

    Public Shared CostCentreProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CostCentre, "Cost Centre", "")
    ''' <summary>
    ''' Gets and sets the Cost Centre value
    ''' </summary>
    <Display(Name:="Cost Centre", Description:=""),
    StringLength(50, ErrorMessage:="Cost Centre cannot be more than 50 characters")>
    Public Property CostCentre() As String
      Get
        Return GetProperty(CostCentreProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CostCentreProperty, Value)
      End Set
    End Property

    Public Shared CostCentreCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CostCentreCode, "Cost Centre Code", "")
    ''' <summary>
    ''' Gets and sets the Cost Centre Code value
    ''' </summary>
    <Display(Name:="Cost Centre Code", Description:=""),
    StringLength(50, ErrorMessage:="Cost Centre Code cannot be more than 50 characters")>
    Public Property CostCentreCode() As String
      Get
        Return GetProperty(CostCentreCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CostCentreCodeProperty, Value)
      End Set
    End Property

    Public Shared CompanyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CompanyID, "Company", Nothing)
    ''' <summary>
    ''' Gets the Company value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CompanyID() As Integer?
      Get
        Return GetProperty(CompanyIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Company

      Return CType(CType(Me.Parent, CostCentreList).Parent, Company)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CostCentreIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CostCentre.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Cost Centre")
        Else
          Return String.Format("Blank {0}", "Cost Centre")
        End If
      Else
        Return Me.CostCentre
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCostCentre() method.

    End Sub

    Public Shared Function NewCostCentre() As CostCentre

      Return DataPortal.CreateChild(Of CostCentre)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCostCentre(dr As SafeDataReader) As CostCentre

      Dim c As New CostCentre()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CostCentreIDProperty, .GetInt32(0))
          LoadProperty(CostCentreProperty, .GetString(1))
          LoadProperty(CostCentreCodeProperty, .GetString(2))
          LoadProperty(CompanyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCostCentre"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCostCentre"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCostCentreID As SqlParameter = .Parameters.Add("@CostCentreID", SqlDbType.Int)
          paramCostCentreID.Value = GetProperty(CostCentreIDProperty)
          If Me.IsNew Then
            paramCostCentreID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@CostCentre", GetProperty(CostCentreProperty))
          .Parameters.AddWithValue("@CostCentreCode", GetProperty(CostCentreCodeProperty))
          .Parameters.AddWithValue("@CompanyID", Me.GetParent().CompanyID)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CostCentreIDProperty, paramCostCentreID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCostCentre"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CostCentreID", GetProperty(CostCentreIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace