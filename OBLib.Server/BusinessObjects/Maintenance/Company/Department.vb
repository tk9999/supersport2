﻿' Generated 02 Sep 2014 17:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class Department
    Inherits SingularBusinessBase(Of Department)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared DepartmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DepartmentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property DepartmentID() As Integer
      Get
        Return GetProperty(DepartmentIDProperty)
      End Get
    End Property

    Public Shared CompanyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CompanyID, "Company", Nothing)
    ''' <summary>
    ''' Gets the Company value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CompanyID() As Integer?
      Get
        Return GetProperty(CompanyIDProperty)
      End Get
    End Property

    Public Shared DepartmentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Department, "Department", "")
    ''' <summary>
    ''' Gets and sets the Department value
    ''' </summary>
    <Display(Name:="Department", Description:=""),
    StringLength(150, ErrorMessage:="Department cannot be more than 150 characters")>
    Public Property Department() As String
      Get
        Return GetProperty(DepartmentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DepartmentProperty, Value)
      End Set
    End Property

    Public Shared DepartmentCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DepartmentCode, "Department Code", "")
    ''' <summary>
    ''' Gets and sets the Department Code value
    ''' </summary>
    <Display(Name:="Department Code", Description:=""),
    StringLength(3, ErrorMessage:="Department Code cannot be more than 3 characters")>
    Public Property DepartmentCode() As String
      Get
        Return GetProperty(DepartmentCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DepartmentCodeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared SystemListProperty As PropertyInfo(Of SystemList) = RegisterProperty(Of SystemList)(Function(c) c.SystemList, "System List")

    Public ReadOnly Property SystemList() As SystemList
      Get
        If GetProperty(SystemListProperty) Is Nothing Then
          LoadProperty(SystemListProperty, Maintenance.Company.SystemList.NewSystemList())
        End If
        Return GetProperty(SystemListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Company

      Return CType(CType(Me.Parent, DepartmentList).Parent, Company)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DepartmentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Department.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Department")
        Else
          Return String.Format("Blank {0}", "Department")
        End If
      Else
        Return Me.Department
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"Systems"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewDepartment() method.

    End Sub

    Public Shared Function NewDepartment() As Department

      Return DataPortal.CreateChild(Of Department)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetDepartment(dr As SafeDataReader) As Department

      Dim d As New Department()
      d.Fetch(dr)
      Return d

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(DepartmentIDProperty, .GetInt32(0))
          LoadProperty(CompanyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DepartmentProperty, .GetString(2))
          LoadProperty(DepartmentCodeProperty, .GetString(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insDepartment"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updDepartment"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramDepartmentID As SqlParameter = .Parameters.Add("@DepartmentID", SqlDbType.Int)
          paramDepartmentID.Value = GetProperty(DepartmentIDProperty)
          If Me.IsNew Then
            paramDepartmentID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@CompanyID", Me.GetParent().CompanyID)
          .Parameters.AddWithValue("@Department", GetProperty(DepartmentProperty))
          .Parameters.AddWithValue("@DepartmentCode", GetProperty(DepartmentCodeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(DepartmentIDProperty, paramDepartmentID.Value)
          End If
          ' update child objects
          If GetProperty(SystemListProperty) IsNot Nothing Then
            Me.SystemList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(SystemListProperty) IsNot Nothing Then
          Me.SystemList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delDepartment"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@DepartmentID", GetProperty(DepartmentIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace