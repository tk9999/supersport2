﻿' Generated 02 Sep 2014 17:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class SystemEmailRecipient
    Inherits SingularBusinessBase(Of SystemEmailRecipient)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "SystemEmailRecipientBO.SystemEmailRecipientBOToString(self)")

    Public Shared SystemEmailRecipientIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemEmailRecipientID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemEmailRecipientID() As Integer
      Get
        Return GetProperty(SystemEmailRecipientIDProperty)
      End Get
    End Property

    Public Shared SystemEmailIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemEmailID, "System Email", Nothing)
    ''' <summary>
    ''' Gets the System Email value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemEmailID() As Integer?
      Get
        Return GetProperty(SystemEmailIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "User", Nothing)
    ''' <summary>
    ''' Gets and sets the User value
    ''' </summary>
    <Display(Name:="User", Description:="User that will receive the email for specific System Email Type"),
    DropDownWeb(GetType(ROUserList))>
    Public Property UserID() As Integer?
      Get
        Return GetProperty(UserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(UserIDProperty, Value)
      End Set
    End Property

    Public Shared CCIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CCInd, "CC", False)
    ''' <summary>
    ''' Gets and sets the CC value
    ''' </summary>
    <Display(Name:="CC", Description:="Must the User be 'CC'd' or 'To' in the email"),
    Required(ErrorMessage:="CC required")>
    Public Property CCInd() As Boolean
      Get
        Return GetProperty(CCIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CCIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets and sets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="Field is filled in when a user wants an email sent to an address and the person isn't in SOBER"),
    StringLength(100, ErrorMessage:="Email Address cannot be more than 100 characters")>
    Public Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmailAddressProperty, Value)
      End Set
    End Property

    <Browsable(False)>
    Public ReadOnly Property User() As ROUser
      Get
        Return If(IsNullNothing(UserID), Nothing, CommonData.Lists.ROUserList.GetItem(UserID))
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemEmail

      Return CType(CType(Me.Parent, SystemEmailRecipientList).Parent, SystemEmail)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemEmailRecipientIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EmailAddress.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Email Recipient")
        Else
          Return String.Format("Blank {0}", "System Email Recipient")
        End If
      Else
        Return Me.EmailAddress
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(UserIDProperty)
        .AddTriggerProperty(EmailAddressProperty)
        .JavascriptRuleFunctionName = "SystemEmailRecipientBO.UserIDAndEmailAddressValid"
        .ServerRuleFunction = AddressOf UserIDAndEmailAddressValid
      End With

    End Sub

    Public Shared Function UserIDAndEmailAddressValid(SER As SystemEmailRecipient) As String
      Dim ErrorString = ""
      Dim HasUser = False
      If SER.UserID <> 0 Then
        HasUser = True
      End If
      If SER.EmailAddress.Trim.Length > 0 Then
        If HasUser Then
          ErrorString = "Either User or Email Address must be specified, not both"
        End If
      ElseIf Not HasUser Then
        ErrorString = "Please specify either User or Email Address"
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemEmailRecipient() method.

    End Sub

    Public Shared Function NewSystemEmailRecipient() As SystemEmailRecipient

      Return DataPortal.CreateChild(Of SystemEmailRecipient)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSystemEmailRecipient(dr As SafeDataReader) As SystemEmailRecipient

      Dim s As New SystemEmailRecipient()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemEmailRecipientIDProperty, .GetInt32(0))
          LoadProperty(SystemEmailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CCIndProperty, .GetBoolean(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(EmailAddressProperty, .GetString(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSystemEmailRecipient"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSystemEmailRecipient"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSystemEmailRecipientID As SqlParameter = .Parameters.Add("@SystemEmailRecipientID", SqlDbType.Int)
          paramSystemEmailRecipientID.Value = GetProperty(SystemEmailRecipientIDProperty)
          If Me.IsNew Then
            paramSystemEmailRecipientID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SystemEmailID", Me.GetParent().SystemEmailID)
          .Parameters.AddWithValue("@UserID", Singular.Misc.NothingDBNull(GetProperty(UserIDProperty)))
          .Parameters.AddWithValue("@CCInd", GetProperty(CCIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@EmailAddress", GetProperty(EmailAddressProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SystemEmailRecipientIDProperty, paramSystemEmailRecipientID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSystemEmailRecipient"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SystemEmailRecipientID", GetProperty(SystemEmailRecipientIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace