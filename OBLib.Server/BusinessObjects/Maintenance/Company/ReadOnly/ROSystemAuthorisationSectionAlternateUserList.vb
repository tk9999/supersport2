﻿' Generated 02 Sep 2014 18:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROSystemAuthorisationSectionAlternateUserList
    Inherits SingularReadOnlyListBase(Of ROSystemAuthorisationSectionAlternateUserList, ROSystemAuthorisationSectionAlternateUser)

#Region " Parent "

    <NotUndoable()> Private mParent As ROSystemAuthorisationSection
#End Region

#Region " Business Methods "

    Public Function GetItem(SystemAuthorisationSectionAlternateUserID As Integer) As ROSystemAuthorisationSectionAlternateUser

      For Each child As ROSystemAuthorisationSectionAlternateUser In Me
        If child.SystemAuthorisationSectionAlternateUserID = SystemAuthorisationSectionAlternateUserID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Authorisation Section Alternate Users"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROSystemAuthorisationSectionAlternateUserList() As ROSystemAuthorisationSectionAlternateUserList

      Return New ROSystemAuthorisationSectionAlternateUserList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace