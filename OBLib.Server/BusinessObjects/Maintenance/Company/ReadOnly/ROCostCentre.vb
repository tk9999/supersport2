﻿' Generated 14 Apr 2014 09:41 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROCostCentre
    Inherits SingularReadOnlyBase(Of ROCostCentre)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CostCentreIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CostCentreID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CostCentreID() As Integer
      Get
        Return GetProperty(CostCentreIDProperty)
      End Get
    End Property

    Public Shared CostCentreProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CostCentre, "Cost Centre", "")
    ''' <summary>
    ''' Gets the Cost Centre value
    ''' </summary>
    <Display(Name:="Cost Centre", Description:="")>
    Public ReadOnly Property CostCentre() As String
      Get
        Return GetProperty(CostCentreProperty)
      End Get
    End Property

    Public Shared CostCentreCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CostCentreCode, "Cost Centre Code", "")
    ''' <summary>
    ''' Gets the Cost Centre Code value
    ''' </summary>
    <Display(Name:="Cost Centre Code", Description:="")>
    Public ReadOnly Property CostCentreCode() As String
      Get
        Return GetProperty(CostCentreCodeProperty)
      End Get
    End Property

    Public Shared CompanyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CompanyID, "Company", Nothing)
    ''' <summary>
    ''' Gets the Company value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CompanyID() As Integer?
      Get
        Return GetProperty(CompanyIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CostCentreIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CostCentre

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCostCentre(dr As SafeDataReader) As ROCostCentre

      Dim r As New ROCostCentre()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CostCentreIDProperty, .GetInt32(0))
        LoadProperty(CostCentreProperty, .GetString(1))
        LoadProperty(CostCentreCodeProperty, .GetString(2))
        LoadProperty(CompanyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace