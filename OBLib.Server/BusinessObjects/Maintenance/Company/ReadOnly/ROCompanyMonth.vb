﻿' Generated 17 Oct 2014 09:58 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc


Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROCompanyMonth
    Inherits OBReadOnlyBase(Of ROCompanyMonth)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CompanyMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CompanyMonthID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CompanyMonthID() As Integer
      Get
        Return GetProperty(CompanyMonthIDProperty)
      End Get
    End Property

    Public Shared MonthProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Month, "Month", 0)
    ''' <summary>
    ''' Gets the Month value
    ''' </summary>
    <Display(Name:="Month", Description:="Month of the year")>
    Public ReadOnly Property Month() As Integer
      Get
        Return GetProperty(MonthProperty)
      End Get
    End Property

    Public Shared YearProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Year, "Year", 0)
    ''' <summary>
    ''' Gets the Year value
    ''' </summary>
    <Display(Name:="Year", Description:="The year for the record"),
    Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.NoDecimals)>
    Public ReadOnly Property Year() As Integer
      Get
        Return GetProperty(YearProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="Start date of the month")>
    Public ReadOnly Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="End date of the month")>
    Public ReadOnly Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    Public Shared WorkHoursRequiredProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WorkHoursRequired, "Work Hours Required", 0)
    ''' <summary>
    ''' Gets the Work Hours Required value
    ''' </summary>
    <Display(Name:="Work Hours Required", Description:="Number of work hours required for the month")>
    Public ReadOnly Property WorkHoursRequired() As Integer
      Get
        Return GetProperty(WorkHoursRequiredProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="System for which the month belongs to")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="Production area for which the month belongs to")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared MonthNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MonthName, "Month", "")
    ''' <summary>
    ''' Gets the Month value
    ''' </summary>
    <Display(Name:="Month", Description:="The month name of the year")>
    Public ReadOnly Property MonthName() As String
      Get
        Select Case GetProperty(MonthProperty)
          Case 1
            Return "January"
          Case 2
            Return "February"
          Case 3
            Return "March"
          Case 4
            Return "April"
          Case 5
            Return "May"
          Case 6
            Return "June"
          Case 7
            Return "July"
          Case 8
            Return "August"
          Case 9
            Return "September"
          Case 10
            Return "October"
          Case 11
            Return "November"
          Case 12
            Return "Decemeber"

        End Select

        Return ""
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CompanyMonthIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Month.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCompanyMonth(dr As SafeDataReader) As ROCompanyMonth

      Dim r As New ROCompanyMonth()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CompanyMonthIDProperty, .GetInt32(0))
        LoadProperty(MonthProperty, .GetInt32(1))
        LoadProperty(YearProperty, .GetInt32(2))
        LoadProperty(StartDateProperty, .GetValue(3))
        LoadProperty(EndDateProperty, .GetValue(4))
        LoadProperty(WorkHoursRequiredProperty, .GetInt32(5))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace