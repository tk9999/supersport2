﻿' Generated 02 Sep 2014 18:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROSystemAuthorisationSection
    Inherits SingularReadOnlyBase(Of ROSystemAuthorisationSection)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemAuthorisationSectionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAuthorisationSectionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemAuthorisationSectionID() As Integer
      Get
        Return GetProperty(SystemAuthorisationSectionIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared AuthorisationSectionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisationSectionID, "Authorisation Section", Nothing)
    ''' <summary>
    ''' Gets the Authorisation Section value
    ''' </summary>
    <Display(Name:="Authorisation Section", Description:="")>
    Public ReadOnly Property AuthorisationSectionID() As Integer?
      Get
        Return GetProperty(AuthorisationSectionIDProperty)
      End Get
    End Property

    Public Shared AuthorisationSubSectionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisationSubSectionID, "Authorisation Sub Section", Nothing)
    ''' <summary>
    ''' Gets the Authorisation Sub Section value
    ''' </summary>
    <Display(Name:="Authorisation Sub Section", Description:="")>
    Public ReadOnly Property AuthorisationSubSectionID() As Integer?
      Get
        Return GetProperty(AuthorisationSubSectionIDProperty)
      End Get
    End Property

    Public Shared AuthorisingUserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuthorisingUserID, "Authorising User", 0)
    ''' <summary>
    ''' Gets the Authorising User value
    ''' </summary>
    <Display(Name:="Authorising User", Description:="")>
    Public ReadOnly Property AuthorisingUserID() As Integer
      Get
        Return GetProperty(AuthorisingUserIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROSystemAuthorisationSectionAlternateUserListProperty As PropertyInfo(Of ROSystemAuthorisationSectionAlternateUserList) = RegisterProperty(Of ROSystemAuthorisationSectionAlternateUserList)(Function(c) c.ROSystemAuthorisationSectionAlternateUserList, "RO System Authorisation Section Alternate User List")

    Public ReadOnly Property ROSystemAuthorisationSectionAlternateUserList() As ROSystemAuthorisationSectionAlternateUserList
      Get
        If GetProperty(ROSystemAuthorisationSectionAlternateUserListProperty) Is Nothing Then
          LoadProperty(ROSystemAuthorisationSectionAlternateUserListProperty, Maintenance.Company.ReadOnly.ROSystemAuthorisationSectionAlternateUserList.NewROSystemAuthorisationSectionAlternateUserList())
        End If
        Return GetProperty(ROSystemAuthorisationSectionAlternateUserListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAuthorisationSectionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemAuthorisationSection(dr As SafeDataReader) As ROSystemAuthorisationSection

      Dim r As New ROSystemAuthorisationSection()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemAuthorisationSectionIDProperty, .GetInt32(0))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(AuthorisationSectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(AuthorisationSubSectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(AuthorisingUserIDProperty, .GetInt32(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace