﻿' Generated 02 Sep 2014 18:12 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class RODepartmentList
    Inherits SingularReadOnlyListBase(Of RODepartmentList, RODepartment)

#Region " Parent "

    <NotUndoable()> Private mParent As Company
#End Region

#Region " Business Methods "

    Public Function GetItem(DepartmentID As Integer) As RODepartment

      For Each child As RODepartment In Me
        If child.DepartmentID = DepartmentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Departments"

    End Function

#End Region

#Region " Common "

    Public Shared Function NewRODepartmentList() As RODepartmentList

      Return New RODepartmentList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " .Net Data Access "

    Public Shared Function GetRODepartmentList() As RODepartmentList

      Return DataPortal.Fetch(Of RODepartmentList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RODepartment.GetRODepartment(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRODepartmentList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

#End Region

  End Class

End Namespace