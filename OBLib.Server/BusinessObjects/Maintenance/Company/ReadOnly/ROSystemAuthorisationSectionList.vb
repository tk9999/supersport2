﻿' Generated 02 Sep 2014 18:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROSystemAuthorisationSectionList
    Inherits SingularReadOnlyListBase(Of ROSystemAuthorisationSectionList, ROSystemAuthorisationSection)

#Region " Parent "

    <NotUndoable()> Private mParent As ROSystem
#End Region

#Region " Business Methods "

    Public Function GetItem(SystemAuthorisationSectionID As Integer) As ROSystemAuthorisationSection

      For Each child As ROSystemAuthorisationSection In Me
        If child.SystemAuthorisationSectionID = SystemAuthorisationSectionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Authorisation Sections"

    End Function

    Public Function GetROSystemAuthorisationSectionAlternateUser(SystemAuthorisationSectionAlternateUserID As Integer) As ROSystemAuthorisationSectionAlternateUser

      Dim obj As ROSystemAuthorisationSectionAlternateUser = Nothing
      For Each parent As ROSystemAuthorisationSection In Me
        obj = parent.ROSystemAuthorisationSectionAlternateUserList.GetItem(SystemAuthorisationSectionAlternateUserID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROSystemAuthorisationSectionList() As ROSystemAuthorisationSectionList

      Return New ROSystemAuthorisationSectionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace