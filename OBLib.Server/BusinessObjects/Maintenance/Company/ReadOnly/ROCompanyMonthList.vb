﻿' Generated 17 Oct 2014 09:58 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc


Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROCompanyMonthList
    Inherits OBReadOnlyListBase(Of ROCompanyMonthList, ROCompanyMonth)

#Region " Business Methods "

    Public Function GetItem(CompanyMonthID As Integer) As ROCompanyMonth

      For Each child As ROCompanyMonth In Me
        If child.CompanyMonthID = CompanyMonthID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Company Months"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROCompanyMonthList() As ROCompanyMonthList

      Return New ROCompanyMonthList()

    End Function

    Public Shared Sub BeginGetROCompanyMonthList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROCompanyMonthList)))

      Dim dp As New DataPortal(Of ROCompanyMonthList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROCompanyMonthList(CallBack As EventHandler(Of DataPortalResult(Of ROCompanyMonthList)))

      Dim dp As New DataPortal(Of ROCompanyMonthList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROCompanyMonthList() As ROCompanyMonthList

      Return DataPortal.Fetch(Of ROCompanyMonthList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROCompanyMonth.GetROCompanyMonth(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROCompanyMonthList"
            cm.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace