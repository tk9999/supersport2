﻿' Generated 02 Sep 2014 18:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROSystemEmailRecipient
    Inherits SingularReadOnlyBase(Of ROSystemEmailRecipient)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemEmailRecipientIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemEmailRecipientID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemEmailRecipientID() As Integer
      Get
        Return GetProperty(SystemEmailRecipientIDProperty)
      End Get
    End Property

    Public Shared SystemEmailIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemEmailID, "System Email", Nothing)
    ''' <summary>
    ''' Gets the System Email value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemEmailID() As Integer?
      Get
        Return GetProperty(SystemEmailIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "User", Nothing)
    ''' <summary>
    ''' Gets the User value
    ''' </summary>
    <Display(Name:="User", Description:="User that will receive the email for specific System Email Type")>
    Public ReadOnly Property UserID() As Integer?
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared CCIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CCInd, "CC", False)
    ''' <summary>
    ''' Gets the CC value
    ''' </summary>
    <Display(Name:="CC", Description:="Must the User be 'CC'd' or 'To' in the email")>
    Public ReadOnly Property CCInd() As Boolean
      Get
        Return GetProperty(CCIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="Field is filled in when a user wants an email sent to an address and the person isn't in SOBER")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemEmailRecipientIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EmailAddress

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemEmailRecipient(dr As SafeDataReader) As ROSystemEmailRecipient

      Dim r As New ROSystemEmailRecipient()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemEmailRecipientIDProperty, .GetInt32(0))
        LoadProperty(SystemEmailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CCIndProperty, .GetBoolean(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(EmailAddressProperty, .GetString(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace