﻿' Generated 02 Sep 2014 18:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROClashCCEmailList
    Inherits SingularReadOnlyListBase(Of ROClashCCEmailList, ROClashCCEmail)

#Region " Parent "

    <NotUndoable()> Private mParent As ROSystem
#End Region

#Region " Business Methods "

    Public Function GetItem(ClashCCEmailID As Integer) As ROClashCCEmail

      For Each child As ROClashCCEmail In Me
        If child.ClashCCEmailID = ClashCCEmailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Clash CC Emails"

    End Function

    Public Function GetROClashCCEmailUser(ClashCCEmailUserID As Integer) As ROClashCCEmailUser

      Dim obj As ROClashCCEmailUser = Nothing
      For Each parent As ROClashCCEmail In Me
        obj = parent.ROClashCCEmailUserList.GetItem(ClashCCEmailUserID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROClashCCEmailList() As ROClashCCEmailList

      Return New ROClashCCEmailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace