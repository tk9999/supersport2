﻿' Generated 02 Sep 2014 18:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROClashCCEmail
    Inherits SingularReadOnlyBase(Of ROClashCCEmail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ClashCCEmailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ClashCCEmailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ClashCCEmailID() As Integer
      Get
        Return GetProperty(ClashCCEmailIDProperty)
      End Get
    End Property

    Public Shared ClashTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ClashTypeID, "Clash Type", Nothing)
    ''' <summary>
    ''' Gets the Clash Type value
    ''' </summary>
    <Display(Name:="Clash Type", Description:="")>
    Public ReadOnly Property ClashTypeID() As Integer?
      Get
        Return GetProperty(ClashTypeIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 0)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROClashCCEmailUserListProperty As PropertyInfo(Of ROClashCCEmailUserList) = RegisterProperty(Of ROClashCCEmailUserList)(Function(c) c.ROClashCCEmailUserList, "RO Clash CC Email User List")

    Public ReadOnly Property ROClashCCEmailUserList() As ROClashCCEmailUserList
      Get
        If GetProperty(ROClashCCEmailUserListProperty) Is Nothing Then
          LoadProperty(ROClashCCEmailUserListProperty, Maintenance.Company.ReadOnly.ROClashCCEmailUserList.NewROClashCCEmailUserList())
        End If
        Return GetProperty(ROClashCCEmailUserListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ClashCCEmailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROClashCCEmail(dr As SafeDataReader) As ROClashCCEmail

      Dim r As New ROClashCCEmail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ClashCCEmailIDProperty, .GetInt32(0))
        LoadProperty(ClashTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SystemIDProperty, .GetInt32(2))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace