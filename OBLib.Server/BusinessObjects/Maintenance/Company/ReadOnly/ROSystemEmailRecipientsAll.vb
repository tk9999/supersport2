﻿' Generated 13 Sep 2014 10:47 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROSystemEmailRecipientsAll
    Inherits SingularReadOnlyBase(Of ROSystemEmailRecipientsAll)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemEmailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemEmailID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemEmailID() As Integer?
      Get
        Return GetProperty(SystemEmailIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared SystemEmailTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemEmailTypeID, "System Email Type", Nothing)
    ''' <summary>
    ''' Gets the System Email Type value
    ''' </summary>
    <Display(Name:="System Email Type", Description:="")>
    Public ReadOnly Property SystemEmailTypeID() As Integer?
      Get
        Return GetProperty(SystemEmailTypeIDProperty)
      End Get
    End Property

    Public Shared SystemEmailRecipientIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemEmailRecipientID, "System Email Recipient", Nothing)
    ''' <summary>
    ''' Gets the System Email Recipient value
    ''' </summary>
    <Display(Name:="System Email Recipient", Description:="")>
    Public ReadOnly Property SystemEmailRecipientID() As Integer?
      Get
        Return GetProperty(SystemEmailRecipientIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "User", Nothing)
    ''' <summary>
    ''' Gets the User value
    ''' </summary>
    <Display(Name:="User", Description:="")>
    Public ReadOnly Property UserID() As Integer?
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared UserEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.UserEmailAddress, "User Email Address")
    ''' <summary>
    ''' Gets the User Email Address value
    ''' </summary>
    <Display(Name:="User Email Address", Description:="")>
    Public ReadOnly Property UserEmailAddress() As String
      Get
        Return GetProperty(UserEmailAddressProperty)
      End Get
    End Property

    Public Shared CCIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CCInd, "CC", False)
    ''' <summary>
    ''' Gets the CC value
    ''' </summary>
    <Display(Name:="CC", Description:="")>
    Public ReadOnly Property CCInd() As Boolean
      Get
        Return GetProperty(CCIndProperty)
      End Get
    End Property

    Public Shared NoUserEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NoUserEmailAddress, "No User Email Address")
    ''' <summary>
    ''' Gets the No User Email Address value
    ''' </summary>
    <Display(Name:="No User Email Address", Description:="")>
    Public ReadOnly Property NoUserEmailAddress() As String
      Get
        Return GetProperty(NoUserEmailAddressProperty)
      End Get
    End Property

    Public Shared UserSystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserSystemID, "User System", Nothing)
    ''' <summary>
    ''' Gets the User System value
    ''' </summary>
    <Display(Name:="User System", Description:="")>
    Public ReadOnly Property UserSystemID() As Integer?
      Get
        Return GetProperty(UserSystemIDProperty)
      End Get
    End Property

    Public Shared UserProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserProductionAreaID, "User Production Area", Nothing)
    ''' <summary>
    ''' Gets the User Production Area value
    ''' </summary>
    <Display(Name:="User Production Area", Description:="")>
    Public ReadOnly Property UserProductionAreaID() As Integer?
      Get
        Return GetProperty(UserProductionAreaIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemEmailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.UserEmailAddress

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemEmailRecipientsAll(dr As SafeDataReader) As ROSystemEmailRecipientsAll

      Dim r As New ROSystemEmailRecipientsAll()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemEmailIDProperty, .GetInt32(0))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SystemEmailTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SystemEmailRecipientIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(UserEmailAddressProperty, .GetString(5))
        LoadProperty(CCIndProperty, .GetBoolean(6))
        LoadProperty(NoUserEmailAddressProperty, .GetString(7))
        LoadProperty(UserSystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(UserProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace