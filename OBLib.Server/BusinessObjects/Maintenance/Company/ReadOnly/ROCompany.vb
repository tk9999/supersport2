﻿' Generated 14 Apr 2014 09:41 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROCompany
    Inherits SingularReadOnlyBase(Of ROCompany)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CompanyIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CompanyID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CompanyID() As Integer
      Get
        Return GetProperty(CompanyIDProperty)
      End Get
    End Property

    Public Shared CompanyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Company, "Company", "")
    ''' <summary>
    ''' Gets the Company value
    ''' </summary>
    <Display(Name:="Company", Description:="")>
    Public ReadOnly Property Company() As String
      Get
        Return GetProperty(CompanyProperty)
      End Get
    End Property

    Public Shared CompanyCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CompanyCode, "Company Code", "")
    ''' <summary>
    ''' Gets the Company Code value
    ''' </summary>
    <Display(Name:="Company Code", Description:="")>
    Public ReadOnly Property CompanyCode() As String
      Get
        Return GetProperty(CompanyCodeProperty)
      End Get
    End Property

    Public Shared VatRegistrationNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VatRegistrationNo, "Vat Registration No", "")
    ''' <summary>
    ''' Gets the Vat Registration No value
    ''' </summary>
    <Display(Name:="Vat Registration No", Description:="")>
    Public ReadOnly Property VatRegistrationNo() As String
      Get
        Return GetProperty(VatRegistrationNoProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CompanyIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Company

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCompany(dr As SafeDataReader) As ROCompany

      Dim r As New ROCompany()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CompanyIDProperty, .GetInt32(0))
        LoadProperty(CompanyProperty, .GetString(1))
        LoadProperty(CompanyCodeProperty, .GetString(2))
        LoadProperty(VatRegistrationNoProperty, .GetString(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace