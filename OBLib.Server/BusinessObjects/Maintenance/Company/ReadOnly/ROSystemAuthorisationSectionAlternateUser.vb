﻿' Generated 02 Sep 2014 18:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROSystemAuthorisationSectionAlternateUser
    Inherits SingularReadOnlyBase(Of ROSystemAuthorisationSectionAlternateUser)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemAuthorisationSectionAlternateUserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAuthorisationSectionAlternateUserID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemAuthorisationSectionAlternateUserID() As Integer
      Get
        Return GetProperty(SystemAuthorisationSectionAlternateUserIDProperty)
      End Get
    End Property

    Public Shared SystemAuthorisationSectionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemAuthorisationSectionID, "System Authorisation Section", Nothing)
    ''' <summary>
    ''' Gets the System Authorisation Section value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemAuthorisationSectionID() As Integer?
      Get
        Return GetProperty(SystemAuthorisationSectionIDProperty)
      End Get
    End Property

    Public Shared AlternateUserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AlternateUserID, "Alternate User", 0)
    ''' <summary>
    ''' Gets the Alternate User value
    ''' </summary>
    <Display(Name:="Alternate User", Description:="")>
    Public ReadOnly Property AlternateUserID() As Integer
      Get
        Return GetProperty(AlternateUserIDProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public ReadOnly Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public ReadOnly Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAuthorisationSectionAlternateUserIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemAuthorisationSectionAlternateUser(dr As SafeDataReader) As ROSystemAuthorisationSectionAlternateUser

      Dim r As New ROSystemAuthorisationSectionAlternateUser()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemAuthorisationSectionAlternateUserIDProperty, .GetInt32(0))
        LoadProperty(SystemAuthorisationSectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(AlternateUserIDProperty, .GetInt32(2))
        LoadProperty(StartDateProperty, .GetValue(3))
        LoadProperty(EndDateProperty, .GetValue(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace