﻿' Generated 02 Sep 2014 18:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROSystemEmailRecipientList
    Inherits SingularReadOnlyListBase(Of ROSystemEmailRecipientList, ROSystemEmailRecipient)

#Region " Parent "

    <NotUndoable()> Private mParent As ROSystemEmail
#End Region

#Region " Business Methods "

    Public Function GetItem(SystemEmailRecipientID As Integer) As ROSystemEmailRecipient

      For Each child As ROSystemEmailRecipient In Me
        If child.SystemEmailRecipientID = SystemEmailRecipientID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Email Recipients"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROSystemEmailRecipientList() As ROSystemEmailRecipientList

      Return New ROSystemEmailRecipientList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace