﻿' Generated 02 Sep 2014 18:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class RODepartment
    Inherits SingularReadOnlyBase(Of RODepartment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared DepartmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DepartmentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property DepartmentID() As Integer
      Get
        Return GetProperty(DepartmentIDProperty)
      End Get
    End Property

    Public Shared CompanyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CompanyID, "Company", Nothing)
    ''' <summary>
    ''' Gets the Company value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CompanyID() As Integer?
      Get
        Return GetProperty(CompanyIDProperty)
      End Get
    End Property

    Public Shared DepartmentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Department, "Department", "")
    ''' <summary>
    ''' Gets the Department value
    ''' </summary>
    <Display(Name:="Department", Description:="")>
    Public ReadOnly Property Department() As String
      Get
        Return GetProperty(DepartmentProperty)
      End Get
    End Property

    Public Shared DepartmentCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DepartmentCode, "Department Code", "")
    ''' <summary>
    ''' Gets the Department Code value
    ''' </summary>
    <Display(Name:="Department Code", Description:="")>
    Public ReadOnly Property DepartmentCode() As String
      Get
        Return GetProperty(DepartmentCodeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DepartmentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Department

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRODepartment(dr As SafeDataReader) As RODepartment

      Dim r As New RODepartment()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(DepartmentIDProperty, .GetInt32(0))
        LoadProperty(CompanyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(DepartmentProperty, .GetString(2))
        LoadProperty(DepartmentCodeProperty, .GetString(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace