﻿' Generated 02 Sep 2014 18:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company.ReadOnly

  <Serializable()> _
  Public Class ROSystemEmailList
    Inherits SingularReadOnlyListBase(Of ROSystemEmailList, ROSystemEmail)

#Region " Parent "

    <NotUndoable()> Private mParent As ROSystem
#End Region

#Region " Business Methods "

    Public Function GetItem(SystemEmailID As Integer) As ROSystemEmail

      For Each child As ROSystemEmail In Me
        If child.SystemEmailID = SystemEmailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Emails"

    End Function

    Public Function GetROSystemEmailRecipient(SystemEmailRecipientID As Integer) As ROSystemEmailRecipient

      Dim obj As ROSystemEmailRecipient = Nothing
      For Each parent As ROSystemEmail In Me
        obj = parent.ROSystemEmailRecipientList.GetItem(SystemEmailRecipientID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROSystemEmailList() As ROSystemEmailList

      Return New ROSystemEmailList()

    End Function

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROCreditorList() As ROSystemEmailList

      Return New ROSystemEmailList()

    End Function

    Public Shared Sub BeginGetROCreditorList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSystemEmailList)))

      Dim dp As New DataPortal(Of ROSystemEmailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    Public Shared Sub BeginGetROCreditorList(CallBack As EventHandler(Of DataPortalResult(Of ROSystemEmailList)))

      Dim dp As New DataPortal(Of ROSystemEmailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSystemEmailList() As ROSystemEmailList

      Return DataPortal.Fetch(Of ROSystemEmailList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemEmail.GetROSystemEmail(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemEmailList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End If

#End Region

#End Region

  End Class

End Namespace