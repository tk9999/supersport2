﻿' Generated 02 Sep 2014 17:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class SystemAuthorisationSection
    Inherits SingularBusinessBase(Of SystemAuthorisationSection)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared SystemAuthorisationSectionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAuthorisationSectionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemAuthorisationSectionID() As Integer
      Get
        Return GetProperty(SystemAuthorisationSectionIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared AuthorisationSectionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisationSectionID, "Authorisation Section", Nothing)
    ''' <summary>
    ''' Gets and sets the Authorisation Section value
    ''' </summary>
    <Display(Name:="Authorisation Section", Description:=""),
    Required(ErrorMessage:="Authorisation Section required")>
    Public Property AuthorisationSectionID() As Integer?
      Get
        Return GetProperty(AuthorisationSectionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AuthorisationSectionIDProperty, Value)
      End Set
    End Property

    Public Shared AuthorisationSubSectionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisationSubSectionID, "Authorisation Sub Section", Nothing)
    ''' <summary>
    ''' Gets and sets the Authorisation Sub Section value
    ''' </summary>
    <Display(Name:="Authorisation Sub Section", Description:="")>
    Public Property AuthorisationSubSectionID() As Integer?
      Get
        Return GetProperty(AuthorisationSubSectionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AuthorisationSubSectionIDProperty, Value)
      End Set
    End Property

    Public Shared AuthorisingUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisingUserID, "Authorising User", Nothing)
    ''' <summary>
    ''' Gets and sets the Authorising User value
    ''' </summary>
    <Display(Name:="Authorising User", Description:=""),
    Required(ErrorMessage:="Authorising User required"),
    DropDownWeb(GetType(ROUserList))>
    Public Property AuthorisingUserID() As Integer?
      Get
        Return GetProperty(AuthorisingUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AuthorisingUserIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared SystemAuthorisationSectionAlternateUserListProperty As PropertyInfo(Of SystemAuthorisationSectionAlternateUserList) = RegisterProperty(Of SystemAuthorisationSectionAlternateUserList)(Function(c) c.SystemAuthorisationSectionAlternateUserList, "System Authorisation Section Alternate User List")

    Public ReadOnly Property SystemAuthorisationSectionAlternateUserList() As SystemAuthorisationSectionAlternateUserList
      Get
        If GetProperty(SystemAuthorisationSectionAlternateUserListProperty) Is Nothing Then
          LoadProperty(SystemAuthorisationSectionAlternateUserListProperty, Maintenance.Company.SystemAuthorisationSectionAlternateUserList.NewSystemAuthorisationSectionAlternateUserList())
        End If
        Return GetProperty(SystemAuthorisationSectionAlternateUserListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As System

      Return CType(CType(Me.Parent, SystemAuthorisationSectionList).Parent, System)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAuthorisationSectionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Authorisation Section")
        Else
          Return String.Format("Blank {0}", "System Authorisation Section")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"SystemAuthorisationSectionAlternateUsers"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemAuthorisationSection() method.

    End Sub

    Public Shared Function NewSystemAuthorisationSection() As SystemAuthorisationSection

      Return DataPortal.CreateChild(Of SystemAuthorisationSection)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSystemAuthorisationSection(dr As SafeDataReader) As SystemAuthorisationSection

      Dim s As New SystemAuthorisationSection()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemAuthorisationSectionIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AuthorisationSectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(AuthorisationSubSectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(AuthorisingUserIDProperty, .GetInt32(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSystemAuthorisationSection"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSystemAuthorisationSection"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSystemAuthorisationSectionID As SqlParameter = .Parameters.Add("@SystemAuthorisationSectionID", SqlDbType.Int)
          paramSystemAuthorisationSectionID.Value = GetProperty(SystemAuthorisationSectionIDProperty)
          If Me.IsNew Then
            paramSystemAuthorisationSectionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SystemID", Me.GetParent().SystemID)
          .Parameters.AddWithValue("@AuthorisationSectionID", GetProperty(AuthorisationSectionIDProperty))
          .Parameters.AddWithValue("@AuthorisationSubSectionID", Singular.Misc.NothingDBNull(GetProperty(AuthorisationSubSectionIDProperty)))
          .Parameters.AddWithValue("@AuthorisingUserID", GetProperty(AuthorisingUserIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SystemAuthorisationSectionIDProperty, paramSystemAuthorisationSectionID.Value)
          End If
          ' update child objects
          If GetProperty(SystemAuthorisationSectionAlternateUserListProperty) IsNot Nothing Then
            Me.SystemAuthorisationSectionAlternateUserList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(SystemAuthorisationSectionAlternateUserListProperty) IsNot Nothing Then
          Me.SystemAuthorisationSectionAlternateUserList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSystemAuthorisationSection"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SystemAuthorisationSectionID", GetProperty(SystemAuthorisationSectionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace