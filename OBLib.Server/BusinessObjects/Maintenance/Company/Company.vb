﻿' Generated 02 Sep 2014 17:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class Company
    Inherits SingularBusinessBase(Of Company)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared CompanyIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CompanyID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CompanyID() As Integer
      Get
        Return GetProperty(CompanyIDProperty)
      End Get
    End Property

    Public Shared CompanyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Company, "Company", "")
    ''' <summary>
    ''' Gets and sets the Company value
    ''' </summary>
    <Display(Name:="Company", Description:=""),
    StringLength(50, ErrorMessage:="Company cannot be more than 50 characters")>
    Public Property Company() As String
      Get
        Return GetProperty(CompanyProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CompanyProperty, Value)
      End Set
    End Property

    Public Shared CompanyCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CompanyCode, "Company Code", "")
    ''' <summary>
    ''' Gets and sets the Company Code value
    ''' </summary>
    <Display(Name:="Company Code", Description:=""),
    StringLength(2, ErrorMessage:="Company Code cannot be more than 2 characters")>
    Public Property CompanyCode() As String
      Get
        Return GetProperty(CompanyCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CompanyCodeProperty, Value)
      End Set
    End Property

    Public Shared VatRegistrationNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VatRegistrationNo, "Vat Registration No", "")
    ''' <summary>
    ''' Gets and sets the Vat Registration No value
    ''' </summary>
    <Display(Name:="Vat Registration No", Description:=""),
    StringLength(50, ErrorMessage:="Vat Registration No cannot be more than 50 characters")>
    Public Property VatRegistrationNo() As String
      Get
        Return GetProperty(VatRegistrationNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VatRegistrationNoProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared DepartmentListProperty As PropertyInfo(Of DepartmentList) = RegisterProperty(Of DepartmentList)(Function(c) c.DepartmentList, "Department List")

    Public ReadOnly Property DepartmentList() As DepartmentList
      Get
        If GetProperty(DepartmentListProperty) Is Nothing Then
          LoadProperty(DepartmentListProperty, Maintenance.Company.DepartmentList.NewDepartmentList())
        End If
        Return GetProperty(DepartmentListProperty)
      End Get
    End Property

    Public Shared CostCentreListProperty As PropertyInfo(Of CostCentreList) = RegisterProperty(Of CostCentreList)(Function(c) c.CostCentreList, "Cost Centre List")

    Public ReadOnly Property CostCentreList() As CostCentreList
      Get
        If GetProperty(CostCentreListProperty) Is Nothing Then
          LoadProperty(CostCentreListProperty, Maintenance.Company.CostCentreList.NewCostCentreList())
        End If
        Return GetProperty(CostCentreListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CompanyIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Company.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Company")
        Else
          Return String.Format("Blank {0}", "Company")
        End If
      Else
        Return Me.Company
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"Departments", "CostCentres"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCompany() method.

    End Sub

    Public Shared Function NewCompany() As Company

      Return DataPortal.CreateChild(Of Company)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCompany(dr As SafeDataReader) As Company

      Dim c As New Company()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CompanyIDProperty, .GetInt32(0))
          LoadProperty(CompanyProperty, .GetString(1))
          LoadProperty(CompanyCodeProperty, .GetString(2))
          LoadProperty(VatRegistrationNoProperty, .GetString(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCompany"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCompany"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCompanyID As SqlParameter = .Parameters.Add("@CompanyID", SqlDbType.Int)
          paramCompanyID.Value = GetProperty(CompanyIDProperty)
          If Me.IsNew Then
            paramCompanyID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Company", GetProperty(CompanyProperty))
          .Parameters.AddWithValue("@CompanyCode", GetProperty(CompanyCodeProperty))
          .Parameters.AddWithValue("@VatRegistrationNo", GetProperty(VatRegistrationNoProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CompanyIDProperty, paramCompanyID.Value)
          End If
          ' update child objects
          If GetProperty(DepartmentListProperty) IsNot Nothing Then
            Me.DepartmentList.Update()
          End If
          If GetProperty(CostCentreListProperty) IsNot Nothing Then
            Me.CostCentreList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(DepartmentListProperty) IsNot Nothing Then
          Me.DepartmentList.Update()
        End If
        If GetProperty(CostCentreListProperty) IsNot Nothing Then
          Me.CostCentreList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCompany"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CompanyID", GetProperty(CompanyIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace