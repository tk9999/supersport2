﻿' Generated 02 Sep 2014 17:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Company.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class ClashCCEmail
    Inherits SingularBusinessBase(Of ClashCCEmail)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared ClashCCEmailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ClashCCEmailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ClashCCEmailID() As Integer
      Get
        Return GetProperty(ClashCCEmailIDProperty)
      End Get
    End Property

    Public Shared ClashTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ClashTypeID, "Clash Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Clash Type value
    ''' </summary>
    <Display(Name:="Clash Type", Description:=""),
    Required(ErrorMessage:="Clash Type required"),
    DropDownWeb(GetType(ROClashTypeList))>
    Public Property ClashTypeID() As Integer?
      Get
        Return GetProperty(ClashTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ClashTypeIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 0)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ClashCCEmailUserListProperty As PropertyInfo(Of ClashCCEmailUserList) = RegisterProperty(Of ClashCCEmailUserList)(Function(c) c.ClashCCEmailUserList, "Clash CC Email User List")

    Public ReadOnly Property ClashCCEmailUserList() As ClashCCEmailUserList
      Get
        If GetProperty(ClashCCEmailUserListProperty) Is Nothing Then
          LoadProperty(ClashCCEmailUserListProperty, Maintenance.Company.ClashCCEmailUserList.NewClashCCEmailUserList())
        End If
        Return GetProperty(ClashCCEmailUserListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As System

      Return CType(CType(Me.Parent, ClashCCEmailList).Parent, System)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ClashCCEmailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Clash CC Email")
        Else
          Return String.Format("Blank {0}", "Clash CC Email")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ClashCCEmailUsers"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewClashCCEmail() method.

    End Sub

    Public Shared Function NewClashCCEmail() As ClashCCEmail

      Return DataPortal.CreateChild(Of ClashCCEmail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetClashCCEmail(dr As SafeDataReader) As ClashCCEmail

      Dim c As New ClashCCEmail()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ClashCCEmailIDProperty, .GetInt32(0))
          LoadProperty(ClashTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SystemIDProperty, .GetInt32(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insClashCCEmail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updClashCCEmail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramClashCCEmailID As SqlParameter = .Parameters.Add("@ClashCCEmailID", SqlDbType.Int)
          paramClashCCEmailID.Value = GetProperty(ClashCCEmailIDProperty)
          If Me.IsNew Then
            paramClashCCEmailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ClashTypeID", GetProperty(ClashTypeIDProperty))
          .Parameters.AddWithValue("@SystemID", Me.GetParent().SystemID)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ClashCCEmailIDProperty, paramClashCCEmailID.Value)
          End If
          ' update child objects
          If GetProperty(ClashCCEmailUserListProperty) IsNot Nothing Then
            Me.ClashCCEmailUserList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ClashCCEmailUserListProperty) IsNot Nothing Then
          Me.ClashCCEmailUserList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delClashCCEmail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ClashCCEmailID", GetProperty(ClashCCEmailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace