﻿' Generated 16 Oct 2014 12:24 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class CompanyMonthList
    Inherits OBBusinessListBase(Of CompanyMonthList, CompanyMonth)

#Region " Business Methods "

    Public Function GetItem(CompanyMonthID As Integer) As CompanyMonth

      For Each child As CompanyMonth In Me
        If child.CompanyMonthID = CompanyMonthID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Company Months"

    End Function

    Public Function GetCompanyMonthContractType(CompanyMonthContractTypeID As Integer) As CompanyMonthContractType

      Dim obj As CompanyMonthContractType = Nothing
      For Each parent As CompanyMonth In Me
        obj = parent.CompanyMonthContractTypeList.GetItem(CompanyMonthContractTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewCompanyMonthList() As CompanyMonthList

      Return New CompanyMonthList()

    End Function

    Public Shared Sub BeginGetCompanyMonthList(CallBack As EventHandler(Of DataPortalResult(Of CompanyMonthList)))

      Dim dp As New DataPortal(Of CompanyMonthList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetCompanyMonthList() As CompanyMonthList

      Return DataPortal.Fetch(Of CompanyMonthList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(CompanyMonth.GetCompanyMonth(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As CompanyMonth = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.CompanyMonthID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.CompanyMonthContractTypeList.RaiseListChangedEvents = False
          parent.CompanyMonthContractTypeList.Add(CompanyMonthContractType.GetCompanyMonthContractType(sdr))
          parent.CompanyMonthContractTypeList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As CompanyMonth In Me
        child.CheckRules()
        For Each CompanyMonthContractType As CompanyMonthContractType In child.CompanyMonthContractTypeList
          CompanyMonthContractType.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getCompanyMonthList"
            cm.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As CompanyMonth In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As CompanyMonth In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace