﻿' Generated 02 Sep 2014 17:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class System
    Inherits SingularBusinessBase(Of System)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System", "")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="The name of the system"),
    StringLength(50, ErrorMessage:="System cannot be more than 50 characters")>
    Public Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemProperty, Value)
      End Set
    End Property

    Public Shared SystemCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemCode, "System Code", "")
    ''' <summary>
    ''' Gets and sets the System Code value
    ''' </summary>
    <Display(Name:="System Code", Description:=""),
    StringLength(3, ErrorMessage:="System Code cannot be more than 3 characters")>
    Public Property SystemCode() As String
      Get
        Return GetProperty(SystemCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemCodeProperty, Value)
      End Set
    End Property

    Public Shared DepartmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DepartmentID, "Department", Nothing)
    ''' <summary>
    ''' Gets the Department value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property DepartmentID() As Integer?
      Get
        Return GetProperty(DepartmentIDProperty)
      End Get
    End Property

    Public Shared RequiresTeamsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresTeams, "RequiresTeams", False)
    ''' <summary>
    ''' Gets the Requires Teams value
    ''' </summary>
    Public Property RequiresTeams() As Boolean
      Get
        Return GetProperty(RequiresTeamsProperty)
      End Get
      Set(value As Boolean)
        SetProperty(RequiresTeamsProperty, value)
      End Set
    End Property

    'Public Shared DefaultLeaveDurationProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DefaultLeaveDuration, "Default Leave Duration", 12)
    ' ''' <summary>
    ' ''' Gets the Department value
    ' ''' </summary>
    'Public Property DefaultLeaveDuration() As Integer
    '  Get
    '    Return GetProperty(DefaultLeaveDurationProperty)
    '  End Get
    '  Set(value As Integer)
    '    SetProperty(DefaultLeaveDurationProperty, value)
    '  End Set
    'End Property

#End Region

#Region " Child Lists "

    Public Shared ClashCCEmailListProperty As PropertyInfo(Of ClashCCEmailList) = RegisterProperty(Of ClashCCEmailList)(Function(c) c.ClashCCEmailList, "Clash CC Email List")

    Public ReadOnly Property ClashCCEmailList() As ClashCCEmailList
      Get
        If GetProperty(ClashCCEmailListProperty) Is Nothing Then
          LoadProperty(ClashCCEmailListProperty, Maintenance.Company.ClashCCEmailList.NewClashCCEmailList())
        End If
        Return GetProperty(ClashCCEmailListProperty)
      End Get
    End Property

    Public Shared SystemEmailListProperty As PropertyInfo(Of SystemEmailList) = RegisterProperty(Of SystemEmailList)(Function(c) c.SystemEmailList, "System Email List")

    Public ReadOnly Property SystemEmailList() As SystemEmailList
      Get
        If GetProperty(SystemEmailListProperty) Is Nothing Then
          LoadProperty(SystemEmailListProperty, Maintenance.Company.SystemEmailList.NewSystemEmailList())
        End If
        Return GetProperty(SystemEmailListProperty)
      End Get
    End Property

    Public Shared SystemAuthorisationSectionListProperty As PropertyInfo(Of SystemAuthorisationSectionList) = RegisterProperty(Of SystemAuthorisationSectionList)(Function(c) c.SystemAuthorisationSectionList, "System Authorisation Section List")

    Public ReadOnly Property SystemAuthorisationSectionList() As SystemAuthorisationSectionList
      Get
        If GetProperty(SystemAuthorisationSectionListProperty) Is Nothing Then
          LoadProperty(SystemAuthorisationSectionListProperty, Maintenance.Company.SystemAuthorisationSectionList.NewSystemAuthorisationSectionList())
        End If
        Return GetProperty(SystemAuthorisationSectionListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Department

      Return CType(CType(Me.Parent, SystemList).Parent, Department)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.System.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System")
        Else
          Return String.Format("Blank {0}", "System")
        End If
      Else
        Return Me.System
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ClashCCEmails", "SystemEmails", "SystemAuthorisationSections"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystem() method.

    End Sub

    Public Shared Function NewSystem() As System

      Return DataPortal.CreateChild(Of System)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSystem(dr As SafeDataReader) As System

      Dim s As New System()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemIDProperty, .GetInt32(0))
          LoadProperty(SystemProperty, .GetString(1))
          LoadProperty(SystemCodeProperty, .GetString(2))
          LoadProperty(DepartmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSystem"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSystem"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSystemID As SqlParameter = .Parameters.Add("@SystemID", SqlDbType.Int)
          paramSystemID.Value = GetProperty(SystemIDProperty)
          If Me.IsNew Then
            paramSystemID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@System", GetProperty(SystemProperty))
          .Parameters.AddWithValue("@SystemCode", GetProperty(SystemCodeProperty))
          .Parameters.AddWithValue("@DepartmentID", Singular.Misc.NothingDBNull(GetProperty(DepartmentIDProperty)))
          .Parameters.AddWithValue("@RequiresTeams", GetProperty(RequiresTeamsProperty))
          '.Parameters.AddWithValue("@DefaultLeaveDuration", GetProperty(DefaultLeaveDurationProperty))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SystemIDProperty, paramSystemID.Value)
          End If
          ' update child objects
          If GetProperty(ClashCCEmailListProperty) IsNot Nothing Then
            Me.ClashCCEmailList.Update()
          End If
          If GetProperty(SystemEmailListProperty) IsNot Nothing Then
            Me.SystemEmailList.Update()
          End If
          If GetProperty(SystemAuthorisationSectionListProperty) IsNot Nothing Then
            Me.SystemAuthorisationSectionList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ClashCCEmailListProperty) IsNot Nothing Then
          Me.ClashCCEmailList.Update()
        End If
        If GetProperty(SystemEmailListProperty) IsNot Nothing Then
          Me.SystemEmailList.Update()
        End If
        If GetProperty(SystemAuthorisationSectionListProperty) IsNot Nothing Then
          Me.SystemAuthorisationSectionList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSystem"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace