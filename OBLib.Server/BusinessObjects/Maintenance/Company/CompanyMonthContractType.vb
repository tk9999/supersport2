﻿' Generated 16 Oct 2014 12:24 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class CompanyMonthContractType
    Inherits OBBusinessBase(Of CompanyMonthContractType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CompanyMonthContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CompanyMonthContractTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CompanyMonthContractTypeID() As Integer
      Get
        Return GetProperty(CompanyMonthContractTypeIDProperty)
      End Get
    End Property

    Public Shared CompanyMonthIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CompanyMonthID, "Company Month", Nothing)
    ''' <summary>
    ''' Gets the Company Month value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CompanyMonthID() As Integer?
      Get
        Return GetProperty(CompanyMonthIDProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "Contract Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="Contract type that is linked to the month"),
    Required(ErrorMessage:="Contract Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROContractTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ContractTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As CompanyMonth

      Return CType(CType(Me.Parent, CompanyMonthContractTypeList).Parent, CompanyMonth)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CompanyMonthContractTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ContractTypeID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Company Month Contract Type")
        Else
          Return String.Format("Blank {0}", "Company Month Contract Type")
        End If
      Else
        Return Me.ContractTypeID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCompanyMonthContractType() method.

    End Sub

    Public Shared Function NewCompanyMonthContractType() As CompanyMonthContractType

      Return DataPortal.CreateChild(Of CompanyMonthContractType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCompanyMonthContractType(dr As SafeDataReader) As CompanyMonthContractType

      Dim c As New CompanyMonthContractType()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CompanyMonthContractTypeIDProperty, .GetInt32(0))
          LoadProperty(CompanyMonthIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCompanyMonthContractType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCompanyMonthContractType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCompanyMonthContractTypeID As SqlParameter = .Parameters.Add("@CompanyMonthContractTypeID", SqlDbType.Int)
          paramCompanyMonthContractTypeID.Value = GetProperty(CompanyMonthContractTypeIDProperty)
          If Me.IsNew Then
            paramCompanyMonthContractTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@CompanyMonthID", Me.GetParent().CompanyMonthID)
          .Parameters.AddWithValue("@ContractTypeID", GetProperty(ContractTypeIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CompanyMonthContractTypeIDProperty, paramCompanyMonthContractTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCompanyMonthContractType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CompanyMonthContractTypeID", GetProperty(CompanyMonthContractTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace