﻿' Generated 16 Oct 2014 12:24 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class CompanyMonth
    Inherits OBBusinessBase(Of CompanyMonth)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "CompanyMonthBO.CompanyMonthBOToString(self)")

    Public Shared CompanyMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CompanyMonthID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CompanyMonthID() As Integer
      Get
        Return GetProperty(CompanyMonthIDProperty)
      End Get
    End Property

    Public Shared MonthProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Month, "Month", 0)
    ''' <summary>
    ''' Gets and sets the Month value
    ''' </summary>
    <Display(Name:="Month", Description:="Month of the year"),
    Required(ErrorMessage:="Month required"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.CommonData.Enums.Month), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property Month() As Integer
      Get
        Return GetProperty(MonthProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MonthProperty, Value)
      End Set
    End Property

    Public Shared YearProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Year, "Year", 0)
    ''' <summary>
    ''' Gets and sets the Year value
    ''' </summary>
    <Display(Name:="Year", Description:="The year for the record"),
    Required(ErrorMessage:="Year required")>
    Public Property Year() As Integer
      Get
        Return GetProperty(YearProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(YearProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="Start date of the month"),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate) _
                                                                  .AddSetExpression("CompanyMonthBO.SetMonthYear(self)")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="End date of the month"),
    Required(ErrorMessage:="End Date required")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared WorkHoursRequiredProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WorkHoursRequired, "Work Hours Required", 0)
    ''' <summary>
    ''' Gets and sets the Work Hours Required value
    ''' </summary>
    <Display(Name:="Work Hours Required", Description:="Number of work hours required for the month"),
    Required(ErrorMessage:="Work Hours Required required")>
    Public Property WorkHoursRequired() As Integer
      Get
        Return GetProperty(WorkHoursRequiredProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WorkHoursRequiredProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", OBLib.Security.Settings.CurrentUser.SystemID)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="System for which the month belongs to"),
    Required(ErrorMessage:="System required"),
    Browsable(False)>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="Production area for which the month belongs to"),
    Required(ErrorMessage:="Production Area required"),
    Browsable(False)>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared CompanyMonthContractTypeListProperty As PropertyInfo(Of CompanyMonthContractTypeList) = RegisterProperty(Of CompanyMonthContractTypeList)(Function(c) c.CompanyMonthContractTypeList, "Company Month Contract Type List")

    Public ReadOnly Property CompanyMonthContractTypeList() As CompanyMonthContractTypeList
      Get
        If GetProperty(CompanyMonthContractTypeListProperty) Is Nothing Then
          LoadProperty(CompanyMonthContractTypeListProperty, Maintenance.Company.CompanyMonthContractTypeList.NewCompanyMonthContractTypeList())
        End If
        Return GetProperty(CompanyMonthContractTypeListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CompanyMonthIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Month.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Company Month")
        Else
          Return String.Format("Blank {0}", "Company Month")
        End If
      Else
        Return Me.Month.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"CompanyMonthContractTypes"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(EndDateProperty)
        .JavascriptRuleFunctionName = "CompanyMonthBO.StartEndDateDifference"
        .ServerRuleFunction = AddressOf StartEndDateDifference
      End With
    End Sub

    Public Shared Function StartEndDateDifference(CM As CompanyMonth) As String
      Dim ErrorString = ""

      Dim startDate = CM.StartDate
      Dim endDate = CM.EndDate
      Dim difference As TimeSpan = endDate - startDate
      If difference.Days > 31 Then
        ErrorString = "Start and End Date Greater than 1 month"
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCompanyMonth() method.

    End Sub

    Public Shared Function NewCompanyMonth() As CompanyMonth

      Return DataPortal.CreateChild(Of CompanyMonth)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCompanyMonth(dr As SafeDataReader) As CompanyMonth

      Dim c As New CompanyMonth()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CompanyMonthIDProperty, .GetInt32(0))
          LoadProperty(MonthProperty, .GetInt32(1))
          LoadProperty(YearProperty, .GetInt32(2))
          LoadProperty(StartDateProperty, .GetValue(3))
          LoadProperty(EndDateProperty, .GetValue(4))
          LoadProperty(WorkHoursRequiredProperty, .GetInt32(5))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCompanyMonth"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCompanyMonth"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCompanyMonthID As SqlParameter = .Parameters.Add("@CompanyMonthID", SqlDbType.Int)
          paramCompanyMonthID.Value = GetProperty(CompanyMonthIDProperty)
          If Me.IsNew Then
            paramCompanyMonthID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Month", GetProperty(MonthProperty))
          .Parameters.AddWithValue("@Year", GetProperty(YearProperty))
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@WorkHoursRequired", GetProperty(WorkHoursRequiredProperty))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CompanyMonthIDProperty, paramCompanyMonthID.Value)
          End If
          ' update child objects
          If GetProperty(CompanyMonthContractTypeListProperty) IsNot Nothing Then
            Me.CompanyMonthContractTypeList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(CompanyMonthContractTypeListProperty) IsNot Nothing Then
          Me.CompanyMonthContractTypeList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCompanyMonth"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CompanyMonthID", GetProperty(CompanyMonthIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace