﻿' Generated 02 Sep 2014 17:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class CompanyList
    Inherits SingularBusinessListBase(Of CompanyList, Company)

#Region " Business Methods "

    Public Function GetItem(CompanyID As Integer) As Company

      For Each child As Company In Me
        If child.CompanyID = CompanyID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Companys"

    End Function

    Public Function GetDepartment(DepartmentID As Integer) As Department

      Dim obj As Department = Nothing
      For Each parent As Company In Me
        obj = parent.DepartmentList.GetItem(DepartmentID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystem(SystemID As Integer) As System

      Dim obj As System = Nothing
      For Each parent As Company In Me
        For Each dept As Department In parent.DepartmentList
          obj = dept.SystemList.GetItem(SystemID)
          If obj IsNot Nothing Then
            Return obj
          End If
        Next
      Next
      Return Nothing

    End Function

    Public Function GetSystemEmail(SystemEmailID As Integer) As SystemEmail

      Dim obj As SystemEmail = Nothing
      For Each parent As Company In Me
        For Each dept As Department In parent.DepartmentList
          For Each sys As System In dept.SystemList
            obj = sys.SystemEmailList.GetItem(SystemEmailID)
            If obj IsNot Nothing Then
              Return obj
            End If
          Next
        Next
      Next
      Return Nothing

    End Function

    Public Function GetClashCCEmail(ClashCCEmailID As Integer) As ClashCCEmail

      Dim obj As ClashCCEmail = Nothing
      For Each parent As Company In Me
        For Each dept As Department In parent.DepartmentList
          For Each sys As System In dept.SystemList
            obj = sys.ClashCCEmailList.GetItem(ClashCCEmailID)
            If obj IsNot Nothing Then
              Return obj
            End If
          Next
        Next
      Next
      Return Nothing

    End Function

    Public Function GetSystemAuthorisationSection(SystemAuthorisationSectionID As Integer) As SystemAuthorisationSection

      Dim obj As SystemAuthorisationSection = Nothing
      For Each parent As Company In Me
        For Each dept As Department In parent.DepartmentList
          For Each sys As System In dept.SystemList
            obj = sys.SystemAuthorisationSectionList.GetItem(SystemAuthorisationSectionID)
            If obj IsNot Nothing Then
              Return obj
            End If
          Next
        Next
      Next
      Return Nothing

    End Function

    Public Function GetCostCentre(CostCentreID As Integer) As CostCentre

      Dim obj As CostCentre = Nothing
      For Each parent As Company In Me
        obj = parent.CostCentreList.GetItem(CostCentreID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewCompanyList() As CompanyList

      Return New CompanyList()

    End Function

    Public Shared Sub BeginGetCompanyList(CallBack As EventHandler(Of DataPortalResult(Of CompanyList)))

      Dim dp As New DataPortal(Of CompanyList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetCompanyList() As CompanyList

      Return DataPortal.Fetch(Of CompanyList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(Company.GetCompany(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'Departments
      Dim parent As Company = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.CompanyID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.DepartmentList.RaiseListChangedEvents = False
          parent.DepartmentList.Add(Department.GetDepartment(sdr))
          parent.DepartmentList.RaiseListChangedEvents = True
        End While
      End If

      'Systems
      Dim parentChild As Department = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.DepartmentID <> sdr.GetInt32(3) Then
            parentChild = Me.GetDepartment(sdr.GetInt32(3))
          End If
          parentChild.SystemList.RaiseListChangedEvents = False
          parentChild.SystemList.Add(System.GetSystem(sdr))
          parentChild.SystemList.RaiseListChangedEvents = True
        End While
      End If

      'Clash CC Emails
      Dim parentSystem As System = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentSystem Is Nothing OrElse parentSystem.SystemID <> sdr.GetInt32(2) Then
            parentSystem = Me.GetSystem(sdr.GetInt32(2))
          End If
          parentSystem.ClashCCEmailList.RaiseListChangedEvents = False
          parentSystem.ClashCCEmailList.Add(ClashCCEmail.GetClashCCEmail(sdr))
          parentSystem.ClashCCEmailList.RaiseListChangedEvents = True
        End While
      End If

      'Clash CC Email Users
      Dim parentClashCC As ClashCCEmail = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentClashCC Is Nothing OrElse parentClashCC.ClashCCEmailID <> sdr.GetInt32(1) Then
            parentClashCC = Me.GetClashCCEmail(sdr.GetInt32(1))
          End If
          parentClashCC.ClashCCEmailUserList.RaiseListChangedEvents = False
          parentClashCC.ClashCCEmailUserList.Add(ClashCCEmailUser.GetClashCCEmailUser(sdr))
          parentClashCC.ClashCCEmailUserList.RaiseListChangedEvents = True
        End While
      End If

      'System Emails
      If sdr.NextResult() Then
        While sdr.Read
          If parentSystem Is Nothing OrElse parentSystem.SystemID <> sdr.GetInt32(1) Then
            parentSystem = Me.GetSystem(sdr.GetInt32(1))
          End If
          parentSystem.SystemEmailList.RaiseListChangedEvents = False
          parentSystem.SystemEmailList.Add(SystemEmail.GetSystemEmail(sdr))
          parentSystem.SystemEmailList.RaiseListChangedEvents = True
        End While
      End If

      'System Email Recipients
      Dim parentSystemEmail As SystemEmail = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentSystemEmail Is Nothing OrElse parentSystemEmail.SystemEmailID <> sdr.GetInt32(1) Then
            parentSystemEmail = Me.GetSystemEmail(sdr.GetInt32(1))
          End If
          parentSystemEmail.SystemEmailRecipientList.RaiseListChangedEvents = False
          parentSystemEmail.SystemEmailRecipientList.Add(SystemEmailRecipient.GetSystemEmailRecipient(sdr))
          parentSystemEmail.SystemEmailRecipientList.RaiseListChangedEvents = True
        End While
      End If

      'System Emails
      If sdr.NextResult() Then
        While sdr.Read
          If parentSystem Is Nothing OrElse parentSystem.SystemID <> sdr.GetInt32(1) Then
            parentSystem = Me.GetSystem(sdr.GetInt32(1))
          End If
          parentSystem.SystemAuthorisationSectionList.RaiseListChangedEvents = False
          parentSystem.SystemAuthorisationSectionList.Add(SystemAuthorisationSection.GetSystemAuthorisationSection(sdr))
          parentSystem.SystemAuthorisationSectionList.RaiseListChangedEvents = True
        End While
      End If

      'System Email Recipients
      Dim parentSystemAuthorisationSection As SystemAuthorisationSection = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentSystemAuthorisationSection Is Nothing OrElse parentSystemAuthorisationSection.SystemAuthorisationSectionID <> sdr.GetInt32(1) Then
            parentSystemAuthorisationSection = Me.GetSystemAuthorisationSection(sdr.GetInt32(1))
          End If
          parentSystemAuthorisationSection.SystemAuthorisationSectionAlternateUserList.RaiseListChangedEvents = False
          parentSystemAuthorisationSection.SystemAuthorisationSectionAlternateUserList.Add(SystemAuthorisationSectionAlternateUser.GetSystemAuthorisationSectionAlternateUser(sdr))
          parentSystemAuthorisationSection.SystemAuthorisationSectionAlternateUserList.RaiseListChangedEvents = True
        End While
      End If

      'Cost Centres
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.CompanyID <> sdr.GetInt32(3) Then
            parent = Me.GetItem(sdr.GetInt32(3))
          End If
          parent.CostCentreList.RaiseListChangedEvents = False
          parent.CostCentreList.Add(CostCentre.GetCostCentre(sdr))
          parent.CostCentreList.RaiseListChangedEvents = True
        End While
      End If

      ''Financial Years
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.CompanyID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.FinancialYearList.RaiseListChangedEvents = False
      '    parent.FinancialYearList.Add(FinancialYear.GetFinancialYear(sdr))
      '    parent.FinancialYearList.RaiseListChangedEvents = True
      '  End While
      'End If

      For Each child As Company In Me
        child.CheckRules()
        For Each Department As Department In child.DepartmentList
          Department.CheckRules()

          For Each System As System In Department.SystemList
            System.CheckRules()

            For Each ClashCCEmail As ClashCCEmail In System.ClashCCEmailList
              ClashCCEmail.CheckRules()

              For Each ClashCCEmailUser As ClashCCEmailUser In ClashCCEmail.ClashCCEmailUserList
                ClashCCEmailUser.CheckRules()
              Next
            Next
            For Each SystemEmail As SystemEmail In System.SystemEmailList
              SystemEmail.CheckRules()

              For Each SystemEmailRecipient As SystemEmailRecipient In SystemEmail.SystemEmailRecipientList
                SystemEmailRecipient.CheckRules()
              Next
            Next
            For Each SystemAuthorisationSection As SystemAuthorisationSection In System.SystemAuthorisationSectionList
              SystemAuthorisationSection.CheckRules()

              For Each SystemAuthorisationSectionAlternateUser As SystemAuthorisationSectionAlternateUser In SystemAuthorisationSection.SystemAuthorisationSectionAlternateUserList
                SystemAuthorisationSectionAlternateUser.CheckRules()
              Next
            Next
          Next
        Next
        For Each CostCentre As CostCentre In child.CostCentreList
          CostCentre.CheckRules()
        Next
        'For Each FinancialYear As FinancialYear In child.FinancialYearList
        '  FinancialYear.CheckRules()
        'Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getCompanyList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As Company In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As Company In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace