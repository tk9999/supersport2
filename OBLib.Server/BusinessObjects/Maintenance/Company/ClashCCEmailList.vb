﻿' Generated 02 Sep 2014 17:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class ClashCCEmailList
    Inherits SingularBusinessListBase(Of ClashCCEmailList, ClashCCEmail)

#Region " Business Methods "

    Public Function GetItem(ClashCCEmailID As Integer) As ClashCCEmail

      For Each child As ClashCCEmail In Me
        If child.ClashCCEmailID = ClashCCEmailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Clash CC Emails"

    End Function

    Public Function GetClashCCEmailUser(ClashCCEmailUserID As Integer) As ClashCCEmailUser

      Dim obj As ClashCCEmailUser = Nothing
      For Each parent As ClashCCEmail In Me
        obj = parent.ClashCCEmailUserList.GetItem(ClashCCEmailUserID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewClashCCEmailList() As ClashCCEmailList

      Return New ClashCCEmailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ClashCCEmail In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ClashCCEmail In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace