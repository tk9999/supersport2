﻿' Generated 02 Sep 2014 17:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Company

  <Serializable()> _
  Public Class SystemAuthorisationSectionAlternateUser
    Inherits SingularBusinessBase(Of SystemAuthorisationSectionAlternateUser)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemAuthorisationSectionAlternateUserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAuthorisationSectionAlternateUserID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemAuthorisationSectionAlternateUserID() As Integer
      Get
        Return GetProperty(SystemAuthorisationSectionAlternateUserIDProperty)
      End Get
    End Property

    Public Shared SystemAuthorisationSectionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemAuthorisationSectionID, "System Authorisation Section", Nothing)
    ''' <summary>
    ''' Gets the System Authorisation Section value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemAuthorisationSectionID() As Integer?
      Get
        Return GetProperty(SystemAuthorisationSectionIDProperty)
      End Get
    End Property

    Public Shared AlternateUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AlternateUserID, "Alternate User", Nothing)
    ''' <summary>
    ''' Gets and sets the Alternate User value
    ''' </summary>
    <Display(Name:="Alternate User", Description:=""),
    Required(ErrorMessage:="Alternate User required"),
    DropDownWeb(GetType(ROUserList))>
    Public Property AlternateUserID() As Integer?
      Get
        Return GetProperty(AlternateUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AlternateUserIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemAuthorisationSection

      Return CType(CType(Me.Parent, SystemAuthorisationSectionAlternateUserList).Parent, SystemAuthorisationSection)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAuthorisationSectionAlternateUserIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Authorisation Section Alternate User")
        Else
          Return String.Format("Blank {0}", "System Authorisation Section Alternate User")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemAuthorisationSectionAlternateUser() method.

    End Sub

    Public Shared Function NewSystemAuthorisationSectionAlternateUser() As SystemAuthorisationSectionAlternateUser

      Return DataPortal.CreateChild(Of SystemAuthorisationSectionAlternateUser)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSystemAuthorisationSectionAlternateUser(dr As SafeDataReader) As SystemAuthorisationSectionAlternateUser

      Dim s As New SystemAuthorisationSectionAlternateUser()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemAuthorisationSectionAlternateUserIDProperty, .GetInt32(0))
          LoadProperty(SystemAuthorisationSectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AlternateUserIDProperty, .GetInt32(2))
          LoadProperty(StartDateProperty, .GetValue(3))
          LoadProperty(EndDateProperty, .GetValue(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSystemAuthorisationSectionAlternateUser"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSystemAuthorisationSectionAlternateUser"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSystemAuthorisationSectionAlternateUserID As SqlParameter = .Parameters.Add("@SystemAuthorisationSectionAlternateUserID", SqlDbType.Int)
          paramSystemAuthorisationSectionAlternateUserID.Value = GetProperty(SystemAuthorisationSectionAlternateUserIDProperty)
          If Me.IsNew Then
            paramSystemAuthorisationSectionAlternateUserID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SystemAuthorisationSectionID", Me.GetParent().SystemAuthorisationSectionID)
          .Parameters.AddWithValue("@AlternateUserID", GetProperty(AlternateUserIDProperty))
          cm.Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          cm.Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SystemAuthorisationSectionAlternateUserIDProperty, paramSystemAuthorisationSectionAlternateUserID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSystemAuthorisationSectionAlternateUser"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SystemAuthorisationSectionAlternateUserID", GetProperty(SystemAuthorisationSectionAlternateUserIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace