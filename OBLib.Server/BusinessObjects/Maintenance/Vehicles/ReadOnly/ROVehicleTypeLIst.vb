﻿' Generated 20 Feb 2015 09:39 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleTypeList
    Inherits OBReadOnlyListBase(Of ROVehicleTypeList, ROVehicleType)

#Region " Business Methods "

    Public Function GetItem(VehicleTypeID As Integer) As ROVehicleType

      For Each child As ROVehicleType In Me
        If child.VehicleTypeID = VehicleTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Vehicle Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property VehicleType As String

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROVehicleTypeList() As ROVehicleTypeList

      Return New ROVehicleTypeList()

    End Function

    Public Shared Sub BeginGetROVehicleTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROVehicleTypeList)))

      Dim dp As New DataPortal(Of ROVehicleTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROVehicleTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROVehicleTypeList)))

      Dim dp As New DataPortal(Of ROVehicleTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROVehicleTypeList() As ROVehicleTypeList

      Return DataPortal.Fetch(Of ROVehicleTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROVehicleType.GetROVehicleType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROVehicleTypeList"
            cm.Parameters.AddWithValue("@VehicleType", Singular.Strings.MakeEmptyDBNull(crit.VehicleType))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace