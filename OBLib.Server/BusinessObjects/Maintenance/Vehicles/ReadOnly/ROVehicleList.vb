﻿' Generated 30 Jun 2014 13:59 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleList
    Inherits OBReadOnlyListBase(Of ROVehicleList, ROVehicle)

#Region " Business Methods "

    Public Function GetItem(VehicleID As Integer) As ROVehicle

      For Each child As ROVehicle In Me
        If child.VehicleID = VehicleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Vehicles"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property StartDateTime As DateTime? = Nothing
      Public Property EndDateTime As DateTime? = Nothing
      Public Property ProductionVenueID As Integer? = Nothing
      Public Property Keyword As String = Nothing

      <Display(AutoGenerateField:=True), Singular.DataAnnotations.PrimarySearchField, Browsable(False)>
      Public Property VehicleName As String = Nothing

      Public Sub New(StartDateTime As DateTime?, EndDateTime As DateTime?, ProductionVenueID As Integer?, Keyword As String)
        Me.StartDateTime = StartDateTime
        Me.EndDateTime = EndDateTime
        Me.ProductionVenueID = ProductionVenueID
        Me.Keyword = Keyword
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROVehicleList() As ROVehicleList

      Return New ROVehicleList()

    End Function

    Public Shared Sub BeginGetROVehicleList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROVehicleList)))

      Dim dp As New DataPortal(Of ROVehicleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROVehicleList(CallBack As EventHandler(Of DataPortalResult(Of ROVehicleList)))

      Dim dp As New DataPortal(Of ROVehicleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROVehicleList(StartDateTime As DateTime?, EndDateTime As DateTime?, ProductionVenueID As Integer?, Keyword As String) As ROVehicleList

      Return DataPortal.Fetch(Of ROVehicleList)(New Criteria(StartDateTime, EndDateTime, ProductionVenueID, Keyword))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROVehicle.GetROVehicle(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROVehicleList"
            cm.Parameters.AddWithValue("@StartDateTime", NothingDBNull(crit.StartDateTime))
            cm.Parameters.AddWithValue("@EndDateTime", NothingDBNull(crit.EndDateTime))
            cm.Parameters.AddWithValue("@CurrentProductionVenueID", NothingDBNull(crit.ProductionVenueID))
            cm.Parameters.AddWithValue("@Keyword", Singular.Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@VehicleName", Singular.Strings.MakeEmptyDBNull(crit.VehicleName))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace