﻿' Generated 26 Jul 2014 10:27 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleHumanResourceList
    Inherits SingularReadOnlyListBase(Of ROVehicleHumanResourceList, ROVehicleHumanResource)

#Region " Parent "

    <NotUndoable()> Private mParent As ROVehicleFull
#End Region

#Region " Business Methods "

    Public Function GetItem(VehicleHumanResourceID As Integer) As ROVehicleHumanResource

      For Each child As ROVehicleHumanResource In Me
        If child.VehicleHumanResourceID = VehicleHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Vehicle Human Resources"

    End Function

    Public Function GetROVehicleHumanResourceTempPosition(VehicleHumanResourceTempPositionID As Integer) As ROVehicleHumanResourceTempPosition

      Dim obj As ROVehicleHumanResourceTempPosition = Nothing
      For Each parent As ROVehicleHumanResource In Me
        obj = parent.ROVehicleHumanResourceTempPositionList.GetItem(VehicleHumanResourceTempPositionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROVehicleHumanResourceList() As ROVehicleHumanResourceList

      Return New ROVehicleHumanResourceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace