﻿' Generated 20 Feb 2015 09:35 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleType
    Inherits OBReadOnlyBase(Of ROVehicleType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VehicleTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property VehicleTypeID() As Integer
      Get
        Return GetProperty(VehicleTypeIDProperty)
      End Get
    End Property

    Public Shared VehicleTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleType, "Vehicle Type", "")
    ''' <summary>
    ''' Gets the Vehicle Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:="")>
  Public ReadOnly Property VehicleType() As String
      Get
        Return GetProperty(VehicleTypeProperty)
      End Get
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", False)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VehicleTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.VehicleType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROVehicleType(dr As SafeDataReader) As ROVehicleType

      Dim r As New ROVehicleType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(VehicleTypeIDProperty, .GetInt32(0))
        LoadProperty(VehicleTypeProperty, .GetString(1))
        LoadProperty(SystemIndProperty, .GetBoolean(2))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace