﻿' Generated 26 Jul 2014 10:27 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleServiceList
    Inherits SingularReadOnlyListBase(Of ROVehicleServiceList, ROVehicleService)

#Region " Parent "

    <NotUndoable()> Private mParent As ROVehicleFull
#End Region

#Region " Business Methods "

    Public Function GetItem(VehicleServiceID As Integer) As ROVehicleService

      For Each child As ROVehicleService In Me
        If child.VehicleServiceID = VehicleServiceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Vehicle Services"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROVehicleServiceList() As ROVehicleServiceList

      Return New ROVehicleServiceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace