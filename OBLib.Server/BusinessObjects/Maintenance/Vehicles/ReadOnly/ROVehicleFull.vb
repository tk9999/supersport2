﻿' Generated 26 Jul 2014 10:27 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleFull
    Inherits SingularReadOnlyBase(Of ROVehicleFull)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property VehicleID() As Integer
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared VehicleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleTypeID, "Vehicle Type", Nothing)
    ''' <summary>
    ''' Gets the Vehicle Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:="")>
    Public ReadOnly Property VehicleTypeID() As Integer?
      Get
        Return GetProperty(VehicleTypeIDProperty)
      End Get
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name", "")
    ''' <summary>
    ''' Gets the Vehicle Name value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="Name ofthe Vehicle")>
    Public ReadOnly Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
    End Property

    Public Shared VehicleDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleDescription, "Vehicle Description", "")
    ''' <summary>
    ''' Gets the Vehicle Description value
    ''' </summary>
    <Display(Name:="Vehicle Description", Description:="Description of the Vehicle")>
    Public ReadOnly Property VehicleDescription() As String
      Get
        Return GetProperty(VehicleDescriptionProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="Supplier that this Vehicle is source from. NULL implies the vehicle belongs to MIH")>
    Public ReadOnly Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared NoOfDriversProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfDrivers, "No Of Drivers", 1)
    ''' <summary>
    ''' Gets the No Of Drivers value
    ''' </summary>
    <Display(Name:="No Of Drivers", Description:="The minimum number of drivers required for this vehicle")>
    Public ReadOnly Property NoOfDrivers() As Integer
      Get
        Return GetProperty(NoOfDriversProperty)
      End Get
    End Property

    Public Shared OvernightNoOfDriversProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OvernightNoOfDrivers, "Overnight No Of Drivers", 1)
    ''' <summary>
    ''' Gets the Overnight No Of Drivers value
    ''' </summary>
    <Display(Name:="Overnight No Of Drivers", Description:="The minimum number of drivers required for this vehicle if the scheduled travel tim is overnight")>
    Public ReadOnly Property OvernightNoOfDrivers() As Integer
      Get
        Return GetProperty(OvernightNoOfDriversProperty)
      End Get
    End Property

    Public Shared HDCompatibleIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HDCompatibleInd, "HD Compatible", False)
    ''' <summary>
    ''' Gets the HD Compatible value
    ''' </summary>
    <Display(Name:="HD Compatible", Description:="Tick indicates that this vehicle can be used for HD productions")>
    Public ReadOnly Property HDCompatibleInd() As Boolean
      Get
        Return GetProperty(HDCompatibleIndProperty)
      End Get
    End Property

    Public Shared DecommissionedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DecommissionedDate, "Decommissioned Date")
    ''' <summary>
    ''' Gets the Decommissioned Date value
    ''' </summary>
    <Display(Name:="Decommissioned Date", Description:="This is the date that the vehicle must stop being used (it cannot be loaded on a production after this date)")>
    Public ReadOnly Property DecommissionedDate As DateTime?
      Get
        Return GetProperty(DecommissionedDateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROVehicleServiceListProperty As PropertyInfo(Of ROVehicleServiceList) = RegisterProperty(Of ROVehicleServiceList)(Function(c) c.ROVehicleServiceList, "RO Vehicle Service List")

    Public ReadOnly Property ROVehicleServiceList() As ROVehicleServiceList
      Get
        If GetProperty(ROVehicleServiceListProperty) Is Nothing Then
          LoadProperty(ROVehicleServiceListProperty, Maintenance.Vehicles.ReadOnly.ROVehicleServiceList.NewROVehicleServiceList())
        End If
        Return GetProperty(ROVehicleServiceListProperty)
      End Get
    End Property

    Public Shared ROVehicleHumanResourceListProperty As PropertyInfo(Of ROVehicleHumanResourceList) = RegisterProperty(Of ROVehicleHumanResourceList)(Function(c) c.ROVehicleHumanResourceList, "RO Vehicle Human Resource List")

    Public ReadOnly Property ROVehicleHumanResourceList() As ROVehicleHumanResourceList
      Get
        If GetProperty(ROVehicleHumanResourceListProperty) Is Nothing Then
          LoadProperty(ROVehicleHumanResourceListProperty, Maintenance.Vehicles.ReadOnly.ROVehicleHumanResourceList.NewROVehicleHumanResourceList())
        End If
        Return GetProperty(ROVehicleHumanResourceListProperty)
      End Get
    End Property

    Public Shared ROVehicleEquipmentListProperty As PropertyInfo(Of ROVehicleEquipmentList) = RegisterProperty(Of ROVehicleEquipmentList)(Function(c) c.ROVehicleEquipmentList, "RO Vehicle Equipment List")

    Public ReadOnly Property ROVehicleEquipmentList() As ROVehicleEquipmentList
      Get
        If GetProperty(ROVehicleEquipmentListProperty) Is Nothing Then
          LoadProperty(ROVehicleEquipmentListProperty, Maintenance.Vehicles.ReadOnly.ROVehicleEquipmentList.NewROVehicleEquipmentList())
        End If
        Return GetProperty(ROVehicleEquipmentListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VehicleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.VehicleName

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROVehicleFull(dr As SafeDataReader) As ROVehicleFull

      Dim r As New ROVehicleFull()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(VehicleIDProperty, .GetInt32(0))
        LoadProperty(VehicleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(VehicleNameProperty, .GetString(2))
        LoadProperty(VehicleDescriptionProperty, .GetString(3))
        LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(NoOfDriversProperty, .GetInt32(5))
        LoadProperty(OvernightNoOfDriversProperty, .GetInt32(6))
        LoadProperty(HDCompatibleIndProperty, .GetBoolean(7))
        LoadProperty(DecommissionedDateProperty, .GetValue(8))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace