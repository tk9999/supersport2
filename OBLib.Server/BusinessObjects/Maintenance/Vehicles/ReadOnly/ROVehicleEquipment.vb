﻿' Generated 26 Jul 2014 10:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleEquipment
    Inherits SingularReadOnlyBase(Of ROVehicleEquipment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VehicleEquipmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleEquipmentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property VehicleEquipmentID() As Integer
      Get
        Return GetProperty(VehicleEquipmentIDProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentID, "Equipment", Nothing)
    ''' <summary>
    ''' Gets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:="The piece of equipment assigned to the vehicle")>
    Public ReadOnly Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
    End Property

    Public Shared EquipmentDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentDescription, "Equipment", "")
    ''' <summary>
    ''' Gets and sets  
    ''' </summary>
    <Display(Name:="Equipment", Description:="")>
    Public ReadOnly Property EquipmentDescription() As String
      Get
        Return GetProperty(EquipmentDescriptionProperty)
      End Get
    End Property

    Public Shared EquipmentTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentType, "Equipment Type", "")
    ''' <summary>
    ''' Gets and sets  
    ''' </summary>
    <Display(Name:="Equipment Type", Description:="")>
    Public ReadOnly Property EquipmentType() As String
      Get
        Return GetProperty(EquipmentTypeProperty)
      End Get
    End Property

    Public Shared EquipmentSubTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentSubType, "Equipment Sub Type", "")
    ''' <summary>
    ''' Gets and sets  
    ''' </summary>
    <Display(Name:="Equipment Sub Type", Description:="")>
    Public ReadOnly Property EquipmentSubType() As String
      Get
        Return GetProperty(EquipmentSubTypeProperty)
      End Get
    End Property


    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    <Display(Name:="Select", Description:="")>
    Public Property SelectInd() As Boolean

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VehicleEquipmentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROVehicleEquipment(dr As SafeDataReader) As ROVehicleEquipment

      Dim r As New ROVehicleEquipment()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(VehicleEquipmentIDProperty, .GetInt32(0))
        LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(EquipmentDescriptionProperty, .GetString(3))
        LoadProperty(EquipmentTypeProperty, .GetString(4))
        LoadProperty(EquipmentSubTypeProperty, .GetString(5))

        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace