﻿' Generated 30 Jun 2014 13:59 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleOldList
    Inherits SingularReadOnlyListBase(Of ROVehicleOldList, ROVehicleOld)

#Region " Business Methods "

    Public Function GetItem(VehicleID As Integer) As ROVehicleOld

      For Each child As ROVehicleOld In Me
        If child.VehicleID = VehicleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Vehicles"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property StartDateTime As DateTime? = Nothing
      Public Property EndDateTime As DateTime? = Nothing
      Public Property ProductionVenueID As Integer? = Nothing

      Public Sub New(StartDateTime As DateTime?, EndDateTime As DateTime?, ProductionVenueID As Integer?)
        Me.StartDateTime = StartDateTime
        Me.EndDateTime = EndDateTime
        Me.ProductionVenueID = ProductionVenueID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROVehicleOldList() As ROVehicleOldList

      Return New ROVehicleOldList()

    End Function

    Public Shared Sub BeginGetROVehicleOldList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROVehicleOldList)))

      Dim dp As New DataPortal(Of ROVehicleOldList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROVehicleOldList(CallBack As EventHandler(Of DataPortalResult(Of ROVehicleOldList)))

      Dim dp As New DataPortal(Of ROVehicleOldList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROVehicleOldList(StartDateTime As DateTime?, EndDateTime As DateTime?, ProductionVenueID As Integer?) As ROVehicleOldList

      Return DataPortal.Fetch(Of ROVehicleOldList)(New Criteria(StartDateTime, EndDateTime, ProductionVenueID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROVehicleOld.GetROVehicleOld(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROVehicleOldList"
            cm.Parameters.AddWithValue("@StartDateTime", NothingDBNull(crit.StartDateTime))
            cm.Parameters.AddWithValue("@EndDateTime", NothingDBNull(crit.EndDateTime))
            cm.Parameters.AddWithValue("@CurrentProductionVenueID", NothingDBNull(crit.ProductionVenueID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace