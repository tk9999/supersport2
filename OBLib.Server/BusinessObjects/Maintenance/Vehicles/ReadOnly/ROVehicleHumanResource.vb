﻿' Generated 26 Jul 2014 10:27 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleHumanResource
    Inherits SingularReadOnlyBase(Of ROVehicleHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VehicleHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property VehicleHumanResourceID() As Integer
      Get
        Return GetProperty(VehicleHumanResourceIDProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline", 0)
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="The discipline of the human resource assigned to the vehicle")>
    Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource assigned to the vehicle")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared PreferenceOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PreferenceOrder, "Preference Order", 1)
    ''' <summary>
    ''' Gets the Preference Order value
    ''' </summary>
    <Display(Name:="Preference Order", Description:="The preference order that will be given to human resources when a vehicle is selected for a production")>
    Public ReadOnly Property PreferenceOrder() As Integer
      Get
        Return GetProperty(PreferenceOrderProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROVehicleHumanResourceTempPositionListProperty As PropertyInfo(Of ROVehicleHumanResourceTempPositionList) = RegisterProperty(Of ROVehicleHumanResourceTempPositionList)(Function(c) c.ROVehicleHumanResourceTempPositionList, "RO Vehicle Human Resource Temp Position List")

    Public ReadOnly Property ROVehicleHumanResourceTempPositionList() As ROVehicleHumanResourceTempPositionList
      Get
        If GetProperty(ROVehicleHumanResourceTempPositionListProperty) Is Nothing Then
          LoadProperty(ROVehicleHumanResourceTempPositionListProperty, Maintenance.Vehicles.ReadOnly.ROVehicleHumanResourceTempPositionList.NewROVehicleHumanResourceTempPositionList())
        End If
        Return GetProperty(ROVehicleHumanResourceTempPositionListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    'Public Function GetParent() As ROVehicleFull

    '  Return CType(CType(Me., ROVehicleHumanResourceList).Parent, ROVehicleFull)

    'End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VehicleHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Dim ROD As OBLib.Maintenance.General.ReadOnly.RODiscipline = OBLib.CommonData.Lists.RODisciplineList.GetItem(DisciplineID)
      Dim ROV As String = OBLib.CommonData.Lists.ROVehicleList.GetItem(VehicleID).VehicleName
      Dim HR As String = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(HumanResourceID).PreferredFirstSurname

      Return HR & " - " & ROV & " - " & ROD.Discipline

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROVehicleHumanResource(dr As SafeDataReader) As ROVehicleHumanResource

      Dim r As New ROVehicleHumanResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(VehicleHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(DisciplineIDProperty, .GetInt32(2))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(PreferenceOrderProperty, .GetInt32(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace