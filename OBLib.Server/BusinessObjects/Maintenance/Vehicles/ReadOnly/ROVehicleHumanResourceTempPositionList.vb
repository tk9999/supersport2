﻿' Generated 26 Jul 2014 10:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleHumanResourceTempPositionList
    Inherits SingularReadOnlyListBase(Of ROVehicleHumanResourceTempPositionList, ROVehicleHumanResourceTempPosition)

#Region " Parent "

    <NotUndoable()> Private mParent As ROVehicleHumanResource
#End Region

#Region " Business Methods "

    Public Function GetItem(VehicleHumanResourceTempPositionID As Integer) As ROVehicleHumanResourceTempPosition

      For Each child As ROVehicleHumanResourceTempPosition In Me
        If child.VehicleHumanResourceTempPositionID = VehicleHumanResourceTempPositionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Vehicle Human Resource Temp Positions"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROVehicleHumanResourceTempPositionList() As ROVehicleHumanResourceTempPositionList

      Return New ROVehicleHumanResourceTempPositionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace