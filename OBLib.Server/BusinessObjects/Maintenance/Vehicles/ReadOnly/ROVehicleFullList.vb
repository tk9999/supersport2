﻿' Generated 26 Jul 2014 10:27 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleFullList
    Inherits SingularReadOnlyListBase(Of ROVehicleFullList, ROVehicleFull)

#Region " Business Methods "

    Public Function GetItem(VehicleID As Integer) As ROVehicleFull

      For Each child As ROVehicleFull In Me
        If child.VehicleID = VehicleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Vehicles"

    End Function

    Public Function GetROVehicleService(VehicleServiceID As Integer) As ROVehicleService

      Dim obj As ROVehicleService = Nothing
      For Each parent As ROVehicleFull In Me
        obj = parent.ROVehicleServiceList.GetItem(VehicleServiceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROVehicleHumanResource(VehicleHumanResourceID As Integer) As ROVehicleHumanResource

      Dim obj As ROVehicleHumanResource = Nothing
      For Each parent As ROVehicleFull In Me
        obj = parent.ROVehicleHumanResourceList.GetItem(VehicleHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROVehicleEquipment(VehicleEquipmentID As Integer) As ROVehicleEquipment

      Dim obj As ROVehicleEquipment = Nothing
      For Each parent As ROVehicleFull In Me
        obj = parent.ROVehicleEquipmentList.GetItem(VehicleEquipmentID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROVehicleFullList() As ROVehicleFullList

      Return New ROVehicleFullList()

    End Function

    Public Shared Sub BeginGetROVehicleFullList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROVehicleFullList)))

      Dim dp As New DataPortal(Of ROVehicleFullList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROVehicleFullList(CallBack As EventHandler(Of DataPortalResult(Of ROVehicleFullList)))

      Dim dp As New DataPortal(Of ROVehicleFullList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROVehicleFullList() As ROVehicleFullList

      Return DataPortal.Fetch(Of ROVehicleFullList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROVehicleFull.GetROVehicleFull(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROVehicleFull = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.VehicleID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROVehicleServiceList.RaiseListChangedEvents = False
          parent.ROVehicleServiceList.Add(ROVehicleService.GetROVehicleService(sdr))
          parent.ROVehicleServiceList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.VehicleID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROVehicleHumanResourceList.RaiseListChangedEvents = False
          parent.ROVehicleHumanResourceList.Add(ROVehicleHumanResource.GetROVehicleHumanResource(sdr))
          parent.ROVehicleHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As ROVehicleHumanResource = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.VehicleHumanResourceID <> sdr.GetInt32(1) Then
            parentChild = Me.GetROVehicleHumanResource(sdr.GetInt32(1))
          End If
          parentChild.ROVehicleHumanResourceTempPositionList.RaiseListChangedEvents = False
          parentChild.ROVehicleHumanResourceTempPositionList.Add(ROVehicleHumanResourceTempPosition.GetROVehicleHumanResourceTempPosition(sdr))
          parentChild.ROVehicleHumanResourceTempPositionList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult Then
        'Paged Equipment Count
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.VehicleID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROVehicleEquipmentList.RaiseListChangedEvents = False
          parent.ROVehicleEquipmentList.Add(ROVehicleEquipment.GetROVehicleEquipment(sdr))
          parent.ROVehicleEquipmentList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROVehicleFullList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace