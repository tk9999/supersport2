﻿' Generated 30 Jun 2014 13:59 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleOld
    Inherits SingularReadOnlyBase(Of ROVehicleOld)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property VehicleID() As Integer
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared VehicleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleTypeID, "Vehicle Type", Nothing)
    ''' <summary>
    ''' Gets the Vehicle Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:="")>
    Public ReadOnly Property VehicleTypeID() As Integer?
      Get
        Return GetProperty(VehicleTypeIDProperty)
      End Get
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name", "")
    ''' <summary>
    ''' Gets the Vehicle Name value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="Name ofthe Vehicle")>
    Public ReadOnly Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
    End Property

    Public Shared VehicleDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleDescription, "Vehicle Description", "")
    ''' <summary>
    ''' Gets the Vehicle Description value
    ''' </summary>
    <Display(Name:="Vehicle Description", Description:="Description of the Vehicle")>
    Public ReadOnly Property VehicleDescription() As String
      Get
        Return GetProperty(VehicleDescriptionProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="Supplier that this Vehicle is source from. NULL implies the vehicle belongs to MIH")>
    Public ReadOnly Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared NoOfDriversProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfDrivers, "No Of Drivers", 1)
    ''' <summary>
    ''' Gets the No Of Drivers value
    ''' </summary>
    <Display(Name:="No Of Drivers", Description:="The minimum number of drivers required for this vehicle")>
    Public ReadOnly Property NoOfDrivers() As Integer
      Get
        Return GetProperty(NoOfDriversProperty)
      End Get
    End Property

    Public Shared OvernightNoOfDriversProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OvernightNoOfDrivers, "Overnight No Of Drivers", 1)
    ''' <summary>
    ''' Gets the Overnight No Of Drivers value
    ''' </summary>
    <Display(Name:="Overnight No Of Drivers", Description:="The minimum number of drivers required for this vehicle if the scheduled travel tim is overnight")>
    Public ReadOnly Property OvernightNoOfDrivers() As Integer
      Get
        Return GetProperty(OvernightNoOfDriversProperty)
      End Get
    End Property

    Public Shared HDCompatibleIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HDCompatibleInd, "HD Compatible", False)
    ''' <summary>
    ''' Gets the HD Compatible value
    ''' </summary>
    <Display(Name:="HD Compatible", Description:="Tick indicates that this vehicle can be used for HD productions")>
    Public ReadOnly Property HDCompatibleInd() As Boolean
      Get
        Return GetProperty(HDCompatibleIndProperty)
      End Get
    End Property

    Public Shared DecommissionedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DecommissionedDate, "Decommissioned Date")
    ''' <summary>
    ''' Gets the Decommissioned Date value
    ''' </summary>
    <Display(Name:="Decommissioned Date", Description:="This is the date that the vehicle must stop being used (it cannot be loaded on a production after this date)")>
    Public ReadOnly Property DecommissionedDate As DateTime?
      Get
        Return GetProperty(DecommissionedDateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PreviousProductionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreviousProduction, "Previous Production")
    ''' <summary>
    ''' Gets the Previous Production value
    ''' </summary>
    <Display(Name:="Previous Production", Description:="")>
    Public ReadOnly Property PreviousProduction() As String
      Get
        Return GetProperty(PreviousProductionProperty)
      End Get
    End Property

    Public Shared PreviousProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PreviousProductionVenueID, "Previous Production Venue", Nothing)
    ''' <summary>
    ''' Gets the Previous Production Venue value
    ''' </summary>
    <Display(Name:="Previous Production Venue", Description:="")>
    Public ReadOnly Property PreviousProductionVenueID() As Integer?
      Get
        Return GetProperty(PreviousProductionVenueIDProperty)
      End Get
    End Property

    Public Shared CurrentProductionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CurrentProduction, "Current Production")
    ''' <summary>
    ''' Gets the Current Production value
    ''' </summary>
    <Display(Name:="Current Production", Description:="")>
    Public ReadOnly Property CurrentProduction() As String
      Get
        Return GetProperty(CurrentProductionProperty)
      End Get
    End Property

    Public Shared CurrentProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CurrentProductionVenueID, "Current Production Venue", Nothing)
    ''' <summary>
    ''' Gets the Current Production Venue value
    ''' </summary>
    <Display(Name:="Current Production Venue", Description:="")>
    Public ReadOnly Property CurrentProductionVenueID() As Integer?
      Get
        Return GetProperty(CurrentProductionVenueIDProperty)
      End Get
    End Property

    Public Shared NextProductionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NextProduction, "Next Production")
    ''' <summary>
    ''' Gets the Next Production value
    ''' </summary>
    <Display(Name:="Next Production", Description:="")>
    Public ReadOnly Property NextProduction() As String
      Get
        Return GetProperty(NextProductionProperty)
      End Get
    End Property

    Public Shared NextProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NextProductionVenueID, "Next Production Venue", Nothing)
    ''' <summary>
    ''' Gets the Next Production Venue value
    ''' </summary>
    <Display(Name:="Next Production Venue", Description:="")>
    Public ReadOnly Property NextProductionVenueID() As Integer?
      Get
        Return GetProperty(NextProductionVenueIDProperty)
      End Get
    End Property

    Public Shared PrevCityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PrevCity, "Prev City")
    ''' <summary>
    ''' Gets the Current Production value
    ''' </summary>
    <Display(Name:="Prev City", Description:="")>
    Public ReadOnly Property PrevCity() As String
      Get
        Return GetProperty(PrevCityProperty)
      End Get
    End Property

    Public Shared PrevDistProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PrevDist, "Prev Dist")
    ''' <summary>
    ''' Gets the Current Production value
    ''' </summary>
    <Display(Name:="Prev Dist", Description:="")>
    Public ReadOnly Property PrevDist() As Integer?
      Get
        Return GetProperty(PrevDistProperty)
      End Get
    End Property

    Public Shared CurrCityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CurrCity, "Curr City")
    ''' <summary>
    ''' Gets the Current Production value
    ''' </summary>
    <Display(Name:="Current City", Description:="")>
    Public ReadOnly Property CurrCity() As String
      Get
        Return GetProperty(CurrCityProperty)
      End Get
    End Property

    Public Shared NextCityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NextCity, "Next City")
    ''' <summary>
    ''' Gets the Current Production value
    ''' </summary>
    <Display(Name:="Next City", Description:="")>
    Public ReadOnly Property NextCity() As String
      Get
        Return GetProperty(NextCityProperty)
      End Get
    End Property

    Public Shared NextDistProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NextDist, "Next Dist")
    ''' <summary>
    ''' Gets the Current Production value
    ''' </summary>
    <Display(Name:="Next Dist", Description:="")>
    Public ReadOnly Property NextDist() As Integer?
      Get
        Return GetProperty(NextDistProperty)
      End Get
    End Property

    <Display(Name:="Previous City")>
    Public ReadOnly Property PreviousCityDist As String
      Get
        Return PrevCity & If(PrevDist IsNot Nothing, " (" & PrevDist.ToString & " km" & ")", "")
      End Get
    End Property

    <Display(Name:="Next City")>
    Public ReadOnly Property NextCityDist As String
      Get
        Return NextCity & If(NextDist IsNot Nothing, " (" & NextDist.ToString & " km" & ")", "")
      End Get
    End Property

    Public Shared VehicleTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleType, "Vehicle Type")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:="The type of vehicle")>
    Public ReadOnly Property VehicleType() As String
      Get
        Return GetProperty(VehicleTypeProperty)
      End Get
    End Property

    Public Shared OBVanProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBVan, "OB Van")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="OB Van", Description:="The type of vehicle")>
    Public ReadOnly Property OBVan() As Boolean
      Get
        Return GetProperty(OBVanProperty)
      End Get
    End Property

    Public Shared JibProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Jib, "Jib")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Jib", Description:="The type of vehicle")>
    Public ReadOnly Property Jib() As Boolean
      Get
        Return GetProperty(JibProperty)
      End Get
    End Property

    <ClientOnly()>
    Public Property OnCurrentProduction As Boolean = False

    <ClientOnly()>
    Public Property Visible As Boolean = True

    Public Shared SuppliedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Supplied, "Supplied?", False)
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Supplied?")>
    Public ReadOnly Property Supplied() As Boolean
      Get
        Return GetProperty(SuppliedProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VehicleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.VehicleName

    End Function

    Public Shared Function IsAvailable(ByVal ProductionID As Integer, ByVal VehicleID As Integer, ByVal StartDate As Date, ByVal EndDate As Date, ByRef ErrorMessage As String) As Boolean
      Return OBLib.Helpers.RuleHelper.VehicleRules.IsAvailable(ProductionID, VehicleID, StartDate, EndDate, ErrorMessage)
    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROVehicleOld(dr As SafeDataReader) As ROVehicleOld

      Dim r As New ROVehicleOld()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(VehicleIDProperty, .GetInt32(0))
        LoadProperty(VehicleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(VehicleNameProperty, .GetString(2))
        LoadProperty(VehicleDescriptionProperty, .GetString(3))
        LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(NoOfDriversProperty, .GetInt32(5))
        LoadProperty(OvernightNoOfDriversProperty, .GetInt32(6))
        LoadProperty(HDCompatibleIndProperty, .GetBoolean(7))
        LoadProperty(DecommissionedDateProperty, .GetValue(8))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
        LoadProperty(PreviousProductionProperty, .GetString(13))
        LoadProperty(PreviousProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(CurrentProductionProperty, .GetString(15))
        LoadProperty(CurrentProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
        LoadProperty(NextProductionProperty, .GetString(17))
        LoadProperty(NextProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
        LoadProperty(PrevCityProperty, .GetString(19))
        LoadProperty(PrevDistProperty, Singular.Misc.ZeroNothing(.GetInt32(20)))
        LoadProperty(CurrCityProperty, .GetString(21))
        LoadProperty(NextCityProperty, .GetString(22))
        LoadProperty(NextDistProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
        LoadProperty(VehicleTypeProperty, .GetString(24))
        LoadProperty(OBVanProperty, .GetBoolean(25))
        LoadProperty(JibProperty, .GetBoolean(26))
        LoadProperty(SuppliedProperty, .GetBoolean(27))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace