﻿' Generated 26 Jul 2014 10:28 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROVehicleEquipmentList
    Inherits SingularReadOnlyListBase(Of ROVehicleEquipmentList, ROVehicleEquipment)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Parent "

    <NotUndoable()> Private mParent As ROVehicleFull
#End Region

#Region " Business Methods "

    Public Function GetItem(VehicleEquipmentID As Integer) As ROVehicleEquipment

      For Each child As ROVehicleEquipment In Me
        If child.VehicleEquipmentID = VehicleEquipmentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Vehicle Equipments"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROVehicleEquipmentList() As ROVehicleEquipmentList

      Return New ROVehicleEquipmentList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "


    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      'Inherits CriteriaBase(Of Criteria)
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "VehicleID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="VehicleID", Description:="")>
      Public Property VehicleID As Integer?
      

      Public Property FilterName As String = ""

      Public Sub New(VehicleID As Integer?,
                     PageNo As Integer,
                     PageSize As Integer,
                     SortAsc As Boolean,
                     SortColumn As String)
        Me.VehicleID = VehicleID
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn
      End Sub

      Public Sub New()


      End Sub

      Public Sub New(VehicleID As Integer)

        Me.VehicleID = VehicleID
      End Sub

    End Class


    Public Shared Function GetROVehicleEquipmentList() As ROVehicleEquipmentList

      Return DataPortal.Fetch(Of ROVehicleEquipmentList)(New Criteria())

    End Function

    Public Shared Function GetROVehicleEquipmentList(Criteria As Criteria) As ROVehicleEquipmentList

      Return DataPortal.Fetch(Of ROVehicleEquipmentList)(Criteria)

    End Function
    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROVehicleEquipment.GetROVehicleEquipment(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROVehicleEquipmentList"
            cm.Parameters.AddWithValue("@VehicleID", Singular.Misc.NothingDBNull(crit.VehicleID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub


#End If

#End Region

#End Region

  End Class

End Namespace