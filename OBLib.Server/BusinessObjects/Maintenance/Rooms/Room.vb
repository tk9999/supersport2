﻿' Generated 07 Nov 2014 04:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Rooms

  <Serializable()> _
  Public Class Room
    Inherits SingularBusinessBase(Of Room)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room", "")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="The name of the room"),
    StringLength(250, ErrorMessage:="Room cannot be more than 250 characters")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared RoomTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomTypeID, "Room Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="Room Type", Description:="The type of the room. i.e studio, gallery, sccr etc"),
    Required(ErrorMessage:="Room Type required"),
    DropDownWeb(GetType(RORoomTypeList))>
    Public Property RoomTypeID() As Integer?
      Get
        Return GetProperty(RoomTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", OBLib.Security.Settings.CurrentUser.UserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared RoomShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomShortName, "Room", "")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Short Name", Description:="The name of the room"),
    MaxLength(8, ErrorMessage:="Short Name cannot be more than 8 characters long")>
    Public Property RoomShortName() As String
      Get
        Return GetProperty(RoomShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomShortNameProperty, Value)
      End Set
    End Property

    Public Shared IgnoreClashesIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IgnoreClashesInd, "Ignore Clashes?", False)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Ignore Clashes?")>
    Public Property IgnoreClashesInd() As Boolean
      Get
        Return GetProperty(IgnoreClashesIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IgnoreClashesIndProperty, Value)
      End Set
    End Property

    Public Shared OwningProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OwningProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="Area"),
    Required(ErrorMessage:="Area is required"),
    DropDownWeb(GetType(ROProductionAreaList))>
    Public Property OwningProductionAreaID() As Integer?
      Get
        Return GetProperty(OwningProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OwningProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared OwningSystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OwningSystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="Sub-Dept"),
    Required(ErrorMessage:="Sub-Dept is required"),
    DropDownWeb(GetType(ROSystemList))>
    Public Property OwningSystemID() As Integer?
      Get
        Return GetProperty(OwningSystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OwningSystemIDProperty, Value)
      End Set
    End Property

    Public Shared SiteIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SiteID, "Site", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="Site"),
    Required(ErrorMessage:="Site is required"),
    DropDownWeb(GetType(ROSiteList))>
    Public Property SiteID() As Integer?
      Get
        Return GetProperty(SiteIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SiteIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared RoomTimelineSettingListProperty As PropertyInfo(Of RoomTimelineSettingList) = RegisterProperty(Of RoomTimelineSettingList)(Function(c) c.RoomTimelineSettingList, "Room Timeline Setting List")
    Public ReadOnly Property RoomTimelineSettingList() As RoomTimelineSettingList
      Get
        If GetProperty(RoomTimelineSettingListProperty) Is Nothing Then
          LoadProperty(RoomTimelineSettingListProperty, Maintenance.Rooms.RoomTimelineSettingList.NewRoomTimelineSettingList())
        End If
        Return GetProperty(RoomTimelineSettingListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Room.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Room")
        Else
          Return String.Format("Blank {0}", "Room")
        End If
      Else
        Return Me.Room
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"RoomTimelineSettings", "RoomSystems"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoom() method.

    End Sub

    Public Shared Function NewRoom() As Room

      Return DataPortal.CreateChild(Of Room)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRoom(dr As SafeDataReader) As Room

      Dim r As New Room()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RoomIDProperty, .GetInt32(0))
          LoadProperty(RoomProperty, .GetString(1))
          LoadProperty(RoomTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(RoomShortNameProperty, .GetString(7))
          LoadProperty(IgnoreClashesIndProperty, .GetBoolean(8))
          LoadProperty(OwningProductionAreaIDProperty, ZeroNothing(.GetInt32(9)))
          LoadProperty(OwningSystemIDProperty, ZeroNothing(.GetInt32(10)))
          LoadProperty(SiteIDProperty, ZeroNothing(.GetInt32(11)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insRoom"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updRoom"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramRoomID As SqlParameter = .Parameters.Add("@RoomID", SqlDbType.Int)
          paramRoomID.Value = GetProperty(RoomIDProperty)
          If Me.IsNew Then
            paramRoomID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Room", GetProperty(RoomProperty))
          .Parameters.AddWithValue("@RoomTypeID", GetProperty(RoomTypeIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@RoomShortName", GetProperty(RoomShortNameProperty))
          .Parameters.AddWithValue("@IgnoreClashesInd", GetProperty(IgnoreClashesIndProperty))
          .Parameters.AddWithValue("@OwningProductionAreaID", NothingDBNull(GetProperty(OwningProductionAreaIDProperty)))
          .Parameters.AddWithValue("@OwningSystemID", NothingDBNull(GetProperty(OwningSystemIDProperty)))
          .Parameters.AddWithValue("@SiteID", NothingDBNull(GetProperty(SiteIDProperty)))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(RoomIDProperty, paramRoomID.Value)
          End If
          ' update child objects
          If GetProperty(RoomTimelineSettingListProperty) IsNot Nothing Then
            Me.RoomTimelineSettingList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(RoomTimelineSettingListProperty) IsNot Nothing Then
          Me.RoomTimelineSettingList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delRoom"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace