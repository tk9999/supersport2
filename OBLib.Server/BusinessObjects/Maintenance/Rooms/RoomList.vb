﻿' Generated 07 Nov 2014 04:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Rooms

  <Serializable()> _
  Public Class RoomList
    Inherits SingularBusinessListBase(Of RoomList, Room)

#Region " Business Methods "

    Public Function GetItem(RoomID As Integer) As Room

      For Each child As Room In Me
        If child.RoomID = RoomID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Rooms"

    End Function

    Public Function GetRoomTimelineSetting(RoomTimlineSettingID As Integer) As RoomTimelineSetting

      Dim obj As RoomTimelineSetting = Nothing
      For Each parent As Room In Me
        obj = parent.RoomTimelineSettingList.GetItem(RoomTimlineSettingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRoomList() As RoomList

      Return New RoomList()

    End Function

    Public Shared Sub BeginGetRoomList(CallBack As EventHandler(Of DataPortalResult(Of RoomList)))

      Dim dp As New DataPortal(Of RoomList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRoomList() As RoomList

      Return DataPortal.Fetch(Of RoomList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(Room.GetRoom(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As Room = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.RoomID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.RoomTimelineSettingList.RaiseListChangedEvents = False
          parent.RoomTimelineSettingList.Add(RoomTimelineSetting.GetRoomTimelineSetting(sdr))
          parent.RoomTimelineSettingList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As Room In Me
        child.CheckRules()
        For Each RoomTimelineSetting As RoomTimelineSetting In child.RoomTimelineSettingList
          RoomTimelineSetting.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRoomList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As Room In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As Room In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace