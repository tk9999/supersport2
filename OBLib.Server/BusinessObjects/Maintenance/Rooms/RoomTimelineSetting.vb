﻿' Generated 07 Nov 2014 04:13 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Rooms

  <Serializable()> _
  Public Class RoomTimelineSetting
    Inherits SingularBusinessBase(Of RoomTimelineSetting)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RoomTimlineSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomTimlineSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property RoomTimlineSettingID() As Integer
      Get
        Return GetProperty(RoomTimlineSettingIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets and sets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:=""),
    Required(ErrorMessage:="Effective Date required")>
    Public Property EffectiveDate As DateTime?
      Get
        If Not FieldManager.FieldExists(EffectiveDateProperty) Then
          LoadProperty(EffectiveDateProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(EffectiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EffectiveDateProperty, Value)
      End Set
    End Property

    Public Shared DefaultCallTimeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DefaultCallTime, "Call Time (Minutes)", 0)
    ''' <summary>
    ''' Gets and sets the Default Call Time value
    ''' </summary>
    <Display(Name:="Call Time (Minutes)", Description:=""),
    Required(ErrorMessage:="Default Call Time required")>
    Public Property DefaultCallTime() As Integer
      Get
        Return GetProperty(DefaultCallTimeProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(DefaultCallTimeProperty, Value)
      End Set
    End Property

    Public Shared DefaultWrapTimeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DefaultWrapTime, "Wrap Time (Minutes)", 0)
    ''' <summary>
    ''' Gets and sets the Default Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time (Minutes)", Description:=""),
    Required(ErrorMessage:="Default Wrap Time required")>
    Public Property DefaultWrapTime() As Integer
      Get
        Return GetProperty(DefaultWrapTimeProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(DefaultWrapTimeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", OBLib.Security.Settings.CurrentUser.UserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Room

      Return CType(CType(Me.Parent, RoomTimelineSettingList).Parent, Room)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomTimlineSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Room Timeline Setting")
        Else
          Return String.Format("Blank {0}", "Room Timeline Setting")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomTimelineSetting() method.

    End Sub

    Public Shared Function NewRoomTimelineSetting() As RoomTimelineSetting

      Return DataPortal.CreateChild(Of RoomTimelineSetting)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRoomTimelineSetting(dr As SafeDataReader) As RoomTimelineSetting

      Dim r As New RoomTimelineSetting()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RoomTimlineSettingIDProperty, .GetInt32(0))
          LoadProperty(RoomIDProperty, .GetInt32(1))
          LoadProperty(EffectiveDateProperty, .GetValue(2))
          LoadProperty(DefaultCallTimeProperty, .GetInt32(3))
          LoadProperty(DefaultWrapTimeProperty, .GetInt32(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insRoomTimelineSetting"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updRoomTimelineSetting"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramRoomTimlineSettingID As SqlParameter = .Parameters.Add("@RoomTimlineSettingID", SqlDbType.Int)
          paramRoomTimlineSettingID.Value = GetProperty(RoomTimlineSettingIDProperty)
          If Me.IsNew Then
            paramRoomTimlineSettingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@RoomID", Me.GetParent().RoomID)
          cm.Parameters.AddWithValue("@EffectiveDate", (New SmartDate(GetProperty(EffectiveDateProperty))).DBValue)
          .Parameters.AddWithValue("@DefaultCallTime", GetProperty(DefaultCallTimeProperty))
          .Parameters.AddWithValue("@DefaultWrapTime", GetProperty(DefaultWrapTimeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(RoomTimlineSettingIDProperty, paramRoomTimlineSettingID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delRoomTimelineSetting"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@RoomTimlineSettingID", GetProperty(RoomTimlineSettingIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace