﻿' Generated 06 Aug 2014 09:22 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomScheduleTypeList
    Inherits SingularReadOnlyListBase(Of RORoomScheduleTypeList, RORoomScheduleType)

#Region " Business Methods "

    Public Function GetItem(RoomScheduleTypeID As Integer) As RORoomScheduleType

      For Each child As RORoomScheduleType In Me
        If child.RoomScheduleTypeID = RoomScheduleTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Room Schedule Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORoomScheduleTypeList() As RORoomScheduleTypeList

      Return New RORoomScheduleTypeList()

    End Function

    Public Shared Sub BeginGetRORoomScheduleTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORoomScheduleTypeList)))

      Dim dp As New DataPortal(Of RORoomScheduleTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRORoomScheduleTypeList(CallBack As EventHandler(Of DataPortalResult(Of RORoomScheduleTypeList)))

      Dim dp As New DataPortal(Of RORoomScheduleTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORoomScheduleTypeList() As RORoomScheduleTypeList

      Return DataPortal.Fetch(Of RORoomScheduleTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORoomScheduleType.GetRORoomScheduleType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORoomScheduleTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace