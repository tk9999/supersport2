﻿' Generated 07 Nov 2014 05:31 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomTimelineSetting
    Inherits SingularReadOnlyBase(Of RORoomTimelineSetting)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RoomTimlineSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomTimlineSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property RoomTimlineSettingID() As Integer
      Get
        Return GetProperty(RoomTimlineSettingIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:="")>
    Public ReadOnly Property EffectiveDate As DateTime?
      Get
        If Not FieldManager.FieldExists(EffectiveDateProperty) Then
          LoadProperty(EffectiveDateProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(EffectiveDateProperty)
      End Get
    End Property

    Public Shared DefaultCallTimeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DefaultCallTime, "Default Call Time", 0)
    ''' <summary>
    ''' Gets the Default Call Time value
    ''' </summary>
    <Display(Name:="Default Call Time", Description:="")>
    Public ReadOnly Property DefaultCallTime() As Integer
      Get
        Return GetProperty(DefaultCallTimeProperty)
      End Get
    End Property

    Public Shared DefaultWrapTimeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DefaultWrapTime, "Default Wrap Time", 0)
    ''' <summary>
    ''' Gets the Default Wrap Time value
    ''' </summary>
    <Display(Name:="Default Wrap Time", Description:="")>
    Public ReadOnly Property DefaultWrapTime() As Integer
      Get
        Return GetProperty(DefaultWrapTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomTimlineSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORoomTimelineSetting(dr As SafeDataReader) As RORoomTimelineSetting

      Dim r As New RORoomTimelineSetting()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RoomTimlineSettingIDProperty, .GetInt32(0))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(EffectiveDateProperty, .GetValue(2))
        LoadProperty(DefaultCallTimeProperty, .GetInt32(3))
        LoadProperty(DefaultWrapTimeProperty, .GetInt32(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace