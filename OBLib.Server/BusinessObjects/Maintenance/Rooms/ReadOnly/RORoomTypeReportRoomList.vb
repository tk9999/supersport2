﻿' Generated 09 Sep 2016 20:02 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomTypeReportRoomList
    Inherits OBReadOnlyListBase(Of RORoomTypeReportRoomList, RORoomTypeReportRoom)

#Region " Parent "

    <NotUndoable()> Private mParent As RORoomTypeReport
#End Region

#Region " Business Methods "

    Public Function GetItem(RoomID As Integer) As RORoomTypeReportRoom

      For Each child As RORoomTypeReportRoom In Me
        If child.RoomID = RoomID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewRORoomTypeReportRoomList() As RORoomTypeReportRoomList

      Return New RORoomTypeReportRoomList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace