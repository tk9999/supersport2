﻿' Generated 29 Jun 2014 16:41 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomList
    Inherits SingularReadOnlyListBase(Of RORoomList, RORoom)

#Region " Business Methods "

    Public Function GetItem(RoomID As Integer) As RORoom

      For Each child As RORoom In Me
        If child.RoomID = RoomID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Rooms"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property RoomID As Integer? = Nothing

      <Display(Name:="Room", Description:=""), PrimarySearchField>
      Public Property Room() As String

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORoomList() As RORoomList

      Return New RORoomList()

    End Function

    Public Shared Sub BeginGetRORoomList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORoomList)))

      Dim dp As New DataPortal(Of RORoomList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRORoomList(CallBack As EventHandler(Of DataPortalResult(Of RORoomList)))

      Dim dp As New DataPortal(Of RORoomList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORoomList() As RORoomList

      Return DataPortal.Fetch(Of RORoomList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORoom.GetRORoom(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORoomList"
            'cm.Parameters.AddWithValue("@Room", NothingDBNull(crit.Room))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace