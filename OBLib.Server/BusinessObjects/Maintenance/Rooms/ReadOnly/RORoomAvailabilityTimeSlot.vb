﻿' Generated 06 Jul 2016 20:09 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomAvailabilityTimeSlot
    Inherits OBReadOnlyBase(Of RORoomAvailabilityTimeSlot)

#Region " Properties and Methods "

#Region " Properties "


    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RORoomAvailabilityTimeSlotBO.RORoomAvailabilityTimeSlotBOToString(self)")

    Public Shared SlotIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SlotID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property SlotID() As Integer
      Get
        Return GetProperty(SlotIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared RoomDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomDescription, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property RoomDescription() As String
      Get
        Return GetProperty(RoomDescriptionProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "Room Schedule")
    ''' <summary>
    ''' Gets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public ReadOnly Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "Resource Booking")
    ''' <summary>
    ''' Gets the Resource Booking value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public ReadOnly Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
    End Property

    Public Shared BookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingDescription, "Slot Desc.")
    ''' <summary>
    ''' Gets the Booking Description value
    ''' </summary>
    <Display(Name:="Slot Desc.", Description:="")>
    Public ReadOnly Property BookingDescription() As String
      Get
        Return GetProperty(BookingDescriptionProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDateTime, "Start Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:="")>
    Public ReadOnly Property StartDateTime As Date
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDateTime, "End Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public ReadOnly Property EndDateTime As Date
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared IsGapProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsGap, "Is Gap", False)
    ''' <summary>
    ''' Gets the Is Gap value
    ''' </summary>
    <Display(Name:="Is Gap", Description:="")>
    Public ReadOnly Property IsGap() As Boolean
      Get
        Return GetProperty(IsGapProperty)
      End Get
    End Property

    Public Shared DurationProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Duration, "Duration")
    ''' <summary>
    ''' Gets the Duration value
    ''' </summary>
    <Display(Name:="Duration", Description:="")>
    Public ReadOnly Property Duration() As Decimal
      Get
        Return GetProperty(DurationProperty)
      End Get
    End Property

    Public Shared BookingDurationProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.BookingDuration, "Booking Duration")
    ''' <summary>
    ''' Gets the Booking Duration value
    ''' </summary>
    <Display(Name:="Booking Duration", Description:="")>
    Public ReadOnly Property BookingDuration() As Decimal
      Get
        Return GetProperty(BookingDurationProperty)
      End Get
    End Property

    Public Shared DurationDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DurationDescription, "Slot Dur.")
    ''' <summary>
    ''' Gets the Slot Dur. value
    ''' </summary>
    <Display(Name:="Slot Dur.", Description:="")>
    Public ReadOnly Property DurationDescription() As String
      Get
        Return GetProperty(DurationDescriptionProperty)
      End Get
    End Property

    Public Shared BookingDurationDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingDurationDescription, "Bkng Dur.")
    ''' <summary>
    ''' Gets the Bkng Dur. value
    ''' </summary>
    <Display(Name:="Bkng Dur.", Description:="")>
    Public ReadOnly Property BookingDurationDescription() As String
      Get
        Return GetProperty(BookingDurationDescriptionProperty)
      End Get
    End Property

    Public Shared IsSufficientTimeProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsSufficientTime, "Can Book", False)
    ''' <summary>
    ''' Gets the Is Sufficient Time value
    ''' </summary>
    <Display(Name:="Can Book", Description:="")>
    Public ReadOnly Property IsSufficientTime() As Boolean
      Get
        Return GetProperty(IsSufficientTimeProperty)
      End Get
    End Property

    Public Shared SlotTimeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SlotTimeDescription, "Slot Descr")
    ''' <summary>
    ''' Gets the Slot Descr value
    ''' </summary>
    <Display(Name:="Slot Desc.", Description:="")>
    Public ReadOnly Property SlotTimeDescription() As String
      Get
        Return GetProperty(SlotTimeDescriptionProperty)
      End Get
    End Property

    Public Shared RoomSlotRankProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.RoomSlotRank, "Room Slot Rank")
    ''' <summary>
    ''' Gets the Room Slot Rank value
    ''' </summary>
    <Display(Name:="Room Slot Rank", Description:="")>
    Public ReadOnly Property RoomSlotRank() As Int64
      Get
        Return GetProperty(RoomSlotRankProperty)
      End Get
    End Property

    Public Shared StartTimeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StartTimeDescription, "Start Time")
    ''' <summary>
    ''' Gets the Slot Descr value
    ''' </summary>
    <Display(Name:="Start Time")>
    Public ReadOnly Property StartTimeDescription() As String
      Get
        Return GetProperty(StartTimeDescriptionProperty)
      End Get
    End Property

    Public Shared EndTimeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EndTimeDescription, "End Time")
    ''' <summary>
    ''' Gets the Slot Descr value
    ''' </summary>
    <Display(Name:="End Time")>
    Public ReadOnly Property EndTimeDescription() As String
      Get
        Return GetProperty(EndTimeDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SlotIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Room

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetRORoomAvailabilityTimeSlot(dr As SafeDataReader) As RORoomAvailabilityTimeSlot

      Dim r As New RORoomAvailabilityTimeSlot()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SlotIDProperty, .GetInt32(0))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(RoomProperty, .GetString(2))
        LoadProperty(RoomDescriptionProperty, .GetString(3))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(BookingDescriptionProperty, .GetString(7))
        LoadProperty(StartDateTimeProperty, .GetValue(8))
        LoadProperty(EndDateTimeProperty, .GetValue(9))
        LoadProperty(IsGapProperty, .GetBoolean(10))
        LoadProperty(DurationProperty, .GetDecimal(11))
        LoadProperty(BookingDurationProperty, .GetDecimal(12))
        LoadProperty(DurationDescriptionProperty, .GetString(13))
        LoadProperty(BookingDurationDescriptionProperty, .GetString(14))
        LoadProperty(IsSufficientTimeProperty, .GetBoolean(15))
        LoadProperty(SlotTimeDescriptionProperty, .GetString(16))
        LoadProperty(RoomSlotRankProperty, .GetInt32(17))
        LoadProperty(StartTimeDescriptionProperty, .GetString(18))
        LoadProperty(EndTimeDescriptionProperty, .GetString(19))
      End With

    End Sub

#End Region

  End Class

End Namespace