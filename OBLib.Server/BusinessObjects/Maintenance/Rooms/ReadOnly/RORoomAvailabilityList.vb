﻿' Generated 29 Jun 2014 16:41 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomAvailabilityList
    Inherits OBReadOnlyListBase(Of RORoomAvailabilityList, RORoomAvailability)

#Region " Business Methods "

    Public Function GetItem(RoomID As Integer) As RORoomAvailability

      For Each child As RORoomAvailability In Me
        If child.RoomID = RoomID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Rooms"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Singular.DataAnnotations.PrimarySearchField>
      Public Property Room As String

      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?
      Public Property StartDateTime As DateTime?
      Public Property EndDateTime As DateTime?
      Public Property IgnoreAreas As Boolean = False

      Public Sub New(StartDateTime As DateTime?, EndDateTime As DateTime?)
        Me.StartDateTime = StartDateTime
        Me.EndDateTime = EndDateTime
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORoomAvailabilityList() As RORoomAvailabilityList

      Return New RORoomAvailabilityList()

    End Function

    Public Shared Sub BeginGetRORoomAvailabilityList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORoomAvailabilityList)))

      Dim dp As New DataPortal(Of RORoomAvailabilityList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRORoomAvailabilityList(CallBack As EventHandler(Of DataPortalResult(Of RORoomAvailabilityList)))

      Dim dp As New DataPortal(Of RORoomAvailabilityList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORoomAvailabilityList() As RORoomAvailabilityList

      Return DataPortal.Fetch(Of RORoomAvailabilityList)(New Criteria())

    End Function

    Public Shared Function GetRORoomAvailabilityList(StartDateTime As DateTime?, EndDateTime As DateTime?) As RORoomAvailabilityList

      Return DataPortal.Fetch(Of RORoomAvailabilityList)(New Criteria(StartDateTime, EndDateTime))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORoomAvailability.GetRORoomAvailability(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORoomAvailabilityList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@StartDateTime", NothingDBNull(crit.StartDateTime))
            cm.Parameters.AddWithValue("@EndDateTime", NothingDBNull(crit.EndDateTime))
            cm.Parameters.AddWithValue("@Room", Strings.MakeEmptyDBNull(crit.Room))
            cm.Parameters.AddWithValue("@IgnoreAreas", crit.IgnoreAreas)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace