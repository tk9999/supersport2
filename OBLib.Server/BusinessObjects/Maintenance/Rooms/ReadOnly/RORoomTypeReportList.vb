﻿' Generated 09 Sep 2016 20:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomTypeReportList
    Inherits OBReadOnlyListBase(Of RORoomTypeReportList, RORoomTypeReport)

#Region " Business Methods "

    Public Function GetItem(RoomTypeID As Integer) As RORoomTypeReport

      For Each child As RORoomTypeReport In Me
        If child.RoomTypeID = RoomTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "R Os"

    End Function

    Public Function GetRORoomTypeReportRoom(RoomID As Integer) As RORoomTypeReportRoom

      Dim obj As RORoomTypeReportRoom = Nothing
      For Each parent As RORoomTypeReport In Me
        obj = parent.RORoomTypeReportRoomList.GetItem(RoomID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewRORoomTypeReportList() As RORoomTypeReportList

      Return New RORoomTypeReportList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetRORoomTypeReportList() As RORoomTypeReportList

      Return DataPortal.Fetch(Of RORoomTypeReportList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORoomTypeReport.GetRORoomTypeReport(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As RORoomTypeReport = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.RoomTypeID <> sdr.GetInt32(2) Then
            parent = Me.GetItem(sdr.GetInt32(2))
          End If
          parent.RORoomTypeReportRoomList.RaiseListChangedEvents = False
          parent.RORoomTypeReportRoomList.Add(RORoomTypeReportRoom.GetRORoomTypeReportRoom(sdr))
          parent.RORoomTypeReportRoomList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORoomTypeListReport"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace