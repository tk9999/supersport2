﻿' Generated 06 Jul 2016 20:09 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomAvailabilityTimeSlotList
    Inherits OBReadOnlyListBase(Of RORoomAvailabilityTimeSlotList, RORoomAvailabilityTimeSlot)

#Region " Business Methods "

    Public Function GetItem(SlotID As Integer) As RORoomAvailabilityTimeSlot

      For Each child As RORoomAvailabilityTimeSlot In Me
        If child.SlotID = SlotID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Singular.DataAnnotations.PrimarySearchField>
      Public Property Room As String

      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?
      Public Property StartDateTime As DateTime?
      Public Property EndDateTime As DateTime?
      Public Property IncludeBookings As Boolean = True

      Public Sub New(StartDateTime As DateTime?, EndDateTime As DateTime?)
        Me.StartDateTime = StartDateTime
        Me.EndDateTime = EndDateTime
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewRORoomAvailabilityTimeSlotList() As RORoomAvailabilityTimeSlotList

      Return New RORoomAvailabilityTimeSlotList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetRORoomAvailabilityTimeSlotList() As RORoomAvailabilityTimeSlotList

      Return DataPortal.Fetch(Of RORoomAvailabilityTimeSlotList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORoomAvailabilityTimeSlot.GetRORoomAvailabilityTimeSlot(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORoomAvailabilityListTimeSlot"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@StartDateTime", NothingDBNull(crit.StartDateTime))
            cm.Parameters.AddWithValue("@EndDateTime", NothingDBNull(crit.EndDateTime))
            cm.Parameters.AddWithValue("@Room", Strings.MakeEmptyDBNull(crit.Room))
            cm.Parameters.AddWithValue("@IncludeBookings", crit.IncludeBookings)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace