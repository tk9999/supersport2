﻿' Generated 09 Sep 2016 20:02 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomTypeReportRoom
    Inherits OBReadOnlyBase(Of RORoomTypeReportRoom)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RORoomTypeReportRoomBO.RORoomTypeReportRoomBOToString(self)")

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared RoomTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomTypeID, "Room Type")
    ''' <summary>
    ''' Gets the Room Type value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property RoomTypeID() As Integer
      Get
        Return GetProperty(RoomTypeIDProperty)
      End Get
    End Property

    Public Shared RoomShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomShortName, "Room Short Name")
    ''' <summary>
    ''' Gets the Room Short Name value
    ''' </summary>
    <Display(Name:="Room Short Name", Description:="")>
    Public ReadOnly Property RoomShortName() As String
      Get
        Return GetProperty(RoomShortNameProperty)
      End Get
    End Property

    Public Shared OwningSystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OwningSystemID, "Owning System")
    ''' <summary>
    ''' Gets the Owning System value
    ''' </summary>
    <Display(Name:="Owning System", Description:="")>
    Public ReadOnly Property OwningSystemID() As Integer
      Get
        Return GetProperty(OwningSystemIDProperty)
      End Get
    End Property

    Public Shared OwningProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OwningProductionAreaID, "Owning Production Area")
    ''' <summary>
    ''' Gets the Owning Production Area value
    ''' </summary>
    <Display(Name:="Owning Production Area", Description:="")>
    Public ReadOnly Property OwningProductionAreaID() As Integer
      Get
        Return GetProperty(OwningProductionAreaIDProperty)
      End Get
    End Property

    Public Shared SiteIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SiteID, "Site")
    ''' <summary>
    ''' Gets the Site value
    ''' </summary>
    <Display(Name:="Site", Description:="")>
    Public ReadOnly Property SiteID() As Integer
      Get
        Return GetProperty(SiteIDProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Production Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

    Public Shared SiteNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SiteName, "Site Name")
    ''' <summary>
    ''' Gets the Site Name value
    ''' </summary>
    <Display(Name:="Site Name", Description:="")>
    Public ReadOnly Property SiteName() As String
      Get
        Return GetProperty(SiteNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Room

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetRORoomTypeReportRoom(dr As SafeDataReader) As RORoomTypeReportRoom

      Dim r As New RORoomTypeReportRoom()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RoomIDProperty, .GetInt32(0))
        LoadProperty(RoomProperty, .GetString(1))
        LoadProperty(RoomTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(RoomShortNameProperty, .GetString(3))
        LoadProperty(OwningSystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(OwningProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(SiteIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(SystemProperty, .GetString(7))
        LoadProperty(ProductionAreaProperty, .GetString(8))
        LoadProperty(SiteNameProperty, .GetString(9))
      End With

    End Sub

#End Region

  End Class

End Namespace