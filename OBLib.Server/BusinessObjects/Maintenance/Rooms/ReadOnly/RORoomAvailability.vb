﻿' Generated 29 Jun 2014 16:41 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomAvailability
    Inherits OBReadOnlyBase(Of RORoomAvailability)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room", "")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="The name of the room")>
    Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared RoomTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomTypeID, "Room Type", Nothing)
    ''' <summary>
    ''' Gets the Room Type value
    ''' </summary>
    <Display(Name:="Room Type", Description:="The type of the room. i.e studio, gallery, sccr etc")>
    Public ReadOnly Property RoomTypeID() As Integer?
      Get
        Return GetProperty(RoomTypeIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared RoomTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomType, "Room Type", "")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room Type", Description:="")>
    Public ReadOnly Property RoomType() As String
      Get
        Return GetProperty(RoomTypeProperty)
      End Get
    End Property

    Public Shared OwningSystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OwningSystemID, "OwningSystemID", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="OwningSystemID")>
    Public ReadOnly Property OwningSystemID() As Integer?
      Get
        Return GetProperty(OwningSystemIDProperty)
      End Get
    End Property

    Public Shared OwningProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OwningProductionAreaID, "OwningProductionAreaID", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="OwningProductionAreaID")>
    Public ReadOnly Property OwningProductionAreaID() As Integer?
      Get
        Return GetProperty(OwningProductionAreaIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource", Nothing)
    ''' <summary>
    ''' Gets the Room Type value
    ''' </summary>
    <Display(Name:="Resource")>
    Public ReadOnly Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared SiteIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SiteID, "Site", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="Site")>
    Public ReadOnly Property SiteID() As Integer?
      Get
        Return GetProperty(SiteIDProperty)
      End Get
    End Property

    Public Shared IsAvailableProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsAvailable, "Is Available", False)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Is Available", Description:="")>
    Public ReadOnly Property IsAvailable() As Boolean
      Get
        Return GetProperty(IsAvailableProperty)
      End Get
    End Property

    Public Shared IsAvailableStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsAvailableString, "Is Available", "")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Is Available", Description:="")>
    Public ReadOnly Property IsAvailableString() As String
      Get
        Return GetProperty(IsAvailableStringProperty)
      End Get
    End Property

    Public Shared OwningProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OwningProductionArea, "OwningProductionArea", "")
    ''' <summary>
    ''' Gets the Owning Production Area value
    ''' </summary>
    <Display(Name:="OwningProductionArea", Description:="The name of the Owning Production Area")>
    Public ReadOnly Property OwningProductionArea() As String
      Get
        Return GetProperty(OwningProductionAreaProperty)
      End Get
    End Property

    Public Shared ExpectedCallTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ExpectedCallTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:="")>
    Public ReadOnly Property ExpectedCallTime As DateTime?
      Get
        Return GetProperty(ExpectedCallTimeProperty)
      End Get
    End Property

    Public Shared ExpectedWrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ExpectedWrapTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:="")>
    Public ReadOnly Property ExpectedWrapTime As DateTime?
      Get
        Return GetProperty(ExpectedWrapTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Room

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORoomAvailability(dr As SafeDataReader) As RORoomAvailability

      Dim r As New RORoomAvailability()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RoomIDProperty, .GetInt32(0))
        LoadProperty(RoomProperty, .GetString(1))
        LoadProperty(RoomTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(RoomTypeProperty, .GetString(7))
        LoadProperty(OwningSystemIDProperty, ZeroNothing(.GetInt32(8)))
        LoadProperty(OwningProductionAreaIDProperty, ZeroNothing(.GetInt32(9)))
        LoadProperty(ResourceIDProperty, ZeroNothing(.GetInt32(10)))
        LoadProperty(SiteIDProperty, ZeroNothing(.GetInt32(11)))
        LoadProperty(IsAvailableProperty, .GetBoolean(12))
        LoadProperty(IsAvailableStringProperty, .GetString(13))
        LoadProperty(OwningProductionAreaProperty, .GetString(14))
        LoadProperty(ExpectedCallTimeProperty, .GetValue(15))
        LoadProperty(ExpectedWrapTimeProperty, .GetValue(16))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace