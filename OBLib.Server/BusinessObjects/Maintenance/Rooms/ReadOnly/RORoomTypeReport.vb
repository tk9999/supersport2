﻿' Generated 09 Sep 2016 20:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomTypeReport
    Inherits OBReadOnlyBase(Of RORoomTypeReport)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RORoomTypeReportBO.RORoomTypeReportBOToString(self)")

    Public Shared RoomTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomTypeID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property RoomTypeID() As Integer
      Get
        Return GetProperty(RoomTypeIDProperty)
      End Get
    End Property

    Public Shared RoomTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomType, "Room Type")
    ''' <summary>
    ''' Gets the Room Type value
    ''' </summary>
    <Display(Name:="Room Type", Description:="")>
  Public ReadOnly Property RoomType() As String
      Get
        Return GetProperty(RoomTypeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared RORoomTypeReportRoomListProperty As PropertyInfo(Of RORoomTypeReportRoomList) = RegisterProperty(Of RORoomTypeReportRoomList)(Function(c) c.RORoomTypeReportRoomList, "RO Room Type Report Room List")

    Public ReadOnly Property RORoomTypeReportRoomList() As RORoomTypeReportRoomList
      Get
        If GetProperty(RORoomTypeReportRoomListProperty) Is Nothing Then
          LoadProperty(RORoomTypeReportRoomListProperty, Maintenance.Rooms.ReadOnly.RORoomTypeReportRoomList.NewRORoomTypeReportRoomList())
        End If
        Return GetProperty(RORoomTypeReportRoomListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.RoomType

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetRORoomTypeReport(dr As SafeDataReader) As RORoomTypeReport

      Dim r As New RORoomTypeReport()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RoomTypeIDProperty, .GetInt32(0))
        LoadProperty(RoomTypeProperty, .GetString(1))
      End With

    End Sub

#End Region

  End Class

End Namespace