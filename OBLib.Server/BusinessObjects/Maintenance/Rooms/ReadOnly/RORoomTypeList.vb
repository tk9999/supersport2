﻿' Generated 29 Jun 2014 16:43 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomTypeList
    Inherits SingularReadOnlyListBase(Of RORoomTypeList, RORoomType)

#Region " Business Methods "

    Public Function GetItem(RoomTypeID As Integer) As RORoomType

      For Each child As RORoomType In Me
        If child.RoomTypeID = RoomTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Room Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORoomTypeList() As RORoomTypeList

      Return New RORoomTypeList()

    End Function

    Public Shared Sub BeginGetRORoomTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORoomTypeList)))

      Dim dp As New DataPortal(Of RORoomTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRORoomTypeList(CallBack As EventHandler(Of DataPortalResult(Of RORoomTypeList)))

      Dim dp As New DataPortal(Of RORoomTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORoomTypeList() As RORoomTypeList

      Return DataPortal.Fetch(Of RORoomTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORoomType.GetRORoomType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORoomTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace