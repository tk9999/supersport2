﻿' Generated 13 Oct 2014 11:09 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.PlayoutOps.ReadOnly

  <Serializable()> _
  Public Class ROLegend
    Inherits SingularReadOnlyBase(Of ROLegend)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared LegendIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.LegendID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property LegendID() As Integer
      Get
        Return GetProperty(LegendIDProperty)
      End Get
    End Property

    Public Shared LegendProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Legend, "Legend", "")
    ''' <summary>
    ''' Gets the Legend value
    ''' </summary>
    <Display(Name:="Legend", Description:="Legend name")>
    Public ReadOnly Property Legend() As String
      Get
        Return GetProperty(LegendProperty)
      End Get
    End Property

    Public Shared LegendKeyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LegendKey, "Legend Key", "")
    ''' <summary>
    ''' Gets the Legend Key value
    ''' </summary>
    <Display(Name:="Legend Key", Description:="Legend Key name")>
    Public ReadOnly Property LegendKey() As String
      Get
        Return GetProperty(LegendKeyProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(LegendIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Legend

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROLegend(dr As SafeDataReader) As ROLegend

      Dim r As New ROLegend()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(LegendIDProperty, .GetInt32(0))
        LoadProperty(LegendProperty, .GetString(1))
        LoadProperty(LegendKeyProperty, .GetString(2))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class


  '<Serializable()> _
  'Public Class ROLegendShiftType
  '  Inherits ROLegend

  'End Class

  'Public Class ROLegendStatus
  '  Inherits ROLegend

  'End Class

End Namespace
