﻿' Generated 13 Oct 2014 11:09 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.PlayoutOps.ReadOnly

  <Serializable()> _
  Public Class ROLegendShiftType
    Inherits SingularReadOnlyBase(Of ROLegendShiftType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared LegendShiftTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.LegendShiftTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property LegendShiftTypeID() As Integer
      Get
        Return GetProperty(LegendShiftTypeIDProperty)
      End Get
    End Property

    Public Shared LegendShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LegendShiftType, "LegendShiftType", "")
    ''' <summary>
    ''' Gets the LegendShiftType value
    ''' </summary>
    <Display(Name:="LegendShiftType", Description:="LegendShiftType name")>
    Public ReadOnly Property LegendShiftType() As String
      Get
        Return GetProperty(LegendShiftTypeProperty)
      End Get
    End Property

    Public Shared LegendShiftTypeKeyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LegendShiftTypeKey, "LegendShiftType Key", "")
    ''' <summary>
    ''' Gets the LegendShiftType Key value
    ''' </summary>
    <Display(Name:="LegendShiftType Key", Description:="LegendShiftType Key name")>
    Public ReadOnly Property LegendShiftTypeKey() As String
      Get
        Return GetProperty(LegendShiftTypeKeyProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(LegendShiftTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.LegendShiftType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROLegendShiftType(dr As SafeDataReader) As ROLegendShiftType

      Dim r As New ROLegendShiftType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(LegendShiftTypeIDProperty, .GetInt32(0))
        LoadProperty(LegendShiftTypeProperty, .GetString(1))
        LoadProperty(LegendShiftTypeKeyProperty, .GetString(2))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class


  '<Serializable()> _
  'Public Class ROLegendShiftTypeShiftType
  '  Inherits ROLegendShiftType

  'End Class

  'Public Class ROLegendShiftTypeStatus
  '  Inherits ROLegendShiftType

  'End Class

End Namespace
