﻿' Generated 13 Oct 2014 11:09 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.PlayoutOps.ReadOnly

  <Serializable()> _
  Public Class ROLegendStatus
    Inherits SingularReadOnlyBase(Of ROLegendStatus)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared LegendStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.LegendStatusID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property LegendStatusID() As Integer
      Get
        Return GetProperty(LegendStatusIDProperty)
      End Get
    End Property

    Public Shared LegendStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LegendStatus, "LegendStatus", "")
    ''' <summary>
    ''' Gets the LegendStatus value
    ''' </summary>
    <Display(Name:="LegendStatus", Description:="LegendStatus name")>
    Public ReadOnly Property LegendStatus() As String
      Get
        Return GetProperty(LegendStatusProperty)
      End Get
    End Property

    Public Shared LegendStatusKeyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LegendStatusKey, "LegendStatus Key", "")
    ''' <summary>
    ''' Gets the LegendStatus Key value
    ''' </summary>
    <Display(Name:="LegendStatus Key", Description:="LegendStatus Key name")>
    Public ReadOnly Property LegendStatusKey() As String
      Get
        Return GetProperty(LegendStatusKeyProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(LegendStatusIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.LegendStatus

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROLegendStatus(dr As SafeDataReader) As ROLegendStatus

      Dim r As New ROLegendStatus()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(LegendStatusIDProperty, .GetInt32(0))
        LoadProperty(LegendStatusProperty, .GetString(1))
        LoadProperty(LegendStatusKeyProperty, .GetString(2))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class


  '<Serializable()> _
  'Public Class ROLegendStatusShiftType
  '  Inherits ROLegendStatus

  'End Class

  'Public Class ROLegendStatusStatus
  '  Inherits ROLegendStatus

  'End Class

End Namespace
