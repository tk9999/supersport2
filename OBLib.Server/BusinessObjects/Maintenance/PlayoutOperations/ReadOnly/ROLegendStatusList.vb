﻿' Generated 13 Oct 2014 11:09 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.PlayoutOps.ReadOnly

  <Serializable()> _
  Public Class ROLegendStatusList
    Inherits SingularReadOnlyListBase(Of ROLegendStatusList, ROLegendStatus)

#Region " Business Methods "

    Public Function GetItem(LegendStatusID As Integer) As ROLegendStatus

      For Each child As ROLegendStatus In Me
        If child.LegendStatusID = LegendStatusID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "LegendStatuss"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROLegendStatusList() As ROLegendStatusList

      Return New ROLegendStatusList()

    End Function

    Public Shared Sub BeginGetROLegendStatusList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROLegendStatusList)))

      Dim dp As New DataPortal(Of ROLegendStatusList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROLegendStatusList(CallBack As EventHandler(Of DataPortalResult(Of ROLegendStatusList)))

      Dim dp As New DataPortal(Of ROLegendStatusList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROLegendStatusList() As ROLegendStatusList

      Return DataPortal.Fetch(Of ROLegendStatusList)(New Criteria())

    End Function

    Public Shared Function GetROLegendStatusList(SystemID As Integer?, ProductionAreaID As Integer?) As ROLegendStatusList

      Return DataPortal.Fetch(Of ROLegendStatusList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROLegendStatus.GetROLegendStatus(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = Criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROStatusLegendList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
