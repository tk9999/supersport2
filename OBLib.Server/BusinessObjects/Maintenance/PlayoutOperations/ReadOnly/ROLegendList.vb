﻿' Generated 13 Oct 2014 11:09 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.PlayoutOps.ReadOnly

  <Serializable()> _
  Public Class ROLegendList
    Inherits SingularReadOnlyListBase(Of ROLegendList, ROLegend)

#Region " Business Methods "

    Public Function GetItem(LegendID As Integer) As ROLegend

      For Each child As ROLegend In Me
        If child.LegendID = LegendID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Legends"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROLegendList() As ROLegendList

      Return New ROLegendList()

    End Function

    Public Shared Sub BeginGetROLegendList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROLegendList)))

      Dim dp As New DataPortal(Of ROLegendList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROLegendList(CallBack As EventHandler(Of DataPortalResult(Of ROLegendList)))

      Dim dp As New DataPortal(Of ROLegendList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROLegendList() As ROLegendList

      Return DataPortal.Fetch(Of ROLegendList)(New Criteria())

    End Function

    Public Shared Function GetROLegendList(SystemID As Integer?, ProductionAreaID As Integer?) As ROLegendList

      Return DataPortal.Fetch(Of ROLegendList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROLegend.GetROLegend(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = Criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROLegendList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

  '<Serializable()> _
  'Public Class ROLegendShiftTypeList
  '  Inherits ROLegendList

  '  Public Sub New()

  '    ' must have parameter-less constructor

  '  End Sub

  '  <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
  '  Public Shadows Class Criteria
  '    Inherits ROLegendList.Criteria

  '    'Public Property SystemID As Integer? = Nothing
  '    'Public Property ProductionAreaID As Integer? = Nothing

  '    Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
  '      Me.SystemID = SystemID
  '      Me.ProductionAreaID = ProductionAreaID
  '    End Sub

  '    Public Sub New()


  '    End Sub
  '  End Class

  '  Public Shared Function GetROLegendShiftTypeList() As ROLegendShiftTypeList

  '    Return DataPortal.Fetch(Of ROLegendShiftTypeList)(New Criteria())

  '  End Function

  '  Public Shared Function GetROLegendShiftTypeList(SystemID As Integer?, ProductionAreaID As Integer?) As ROLegendShiftTypeList

  '    Return DataPortal.Fetch(Of ROLegendShiftTypeList)(New Criteria(SystemID, ProductionAreaID))

  '  End Function

  '  Private Sub Fetch(sdr As SafeDataReader)

  '    Me.RaiseListChangedEvents = False
  '    Me.IsReadOnly = False
  '    While sdr.Read
  '      Me.Add(ROLegendShiftType.GetROLegend(sdr))
  '    End While
  '    Me.IsReadOnly = True
  '    Me.RaiseListChangedEvents = True

  '  End Sub

  '  Protected Overrides Sub DataPortal_Fetch(criteria As Object)

  '    Dim crit As Criteria = criteria
  '    Using cn As New SqlConnection(Singular.Settings.ConnectionString)
  '      cn.Open()
  '      Try
  '        Using cm As SqlCommand = cn.CreateCommand
  '          cm.CommandType = CommandType.StoredProcedure
  '          cm.CommandText = "GetProcsWeb.getROLegendShiftTypeList"
  '          cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
  '          cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
  '          Using sdr As New SafeDataReader(cm.ExecuteReader)
  '            Fetch(sdr)
  '          End Using
  '        End Using
  '      Finally
  '        cn.Close()
  '      End Try
  '    End Using
  '  End Sub
  'End Class

  '<Serializable()> _
  'Public Class ROLegendStatusList
  '  Inherits ROLegendList

  '  Public Sub New()

  '    ' must have parameter-less constructor

  '  End Sub

  '  <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
  '  Public Shadows Class Criteria
  '    Inherits ROLegendList.Criteria

  '    'Public Property SystemID As Integer? = Nothing
  '    'Public Property ProductionAreaID As Integer? = Nothing

  '    Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
  '      Me.SystemID = SystemID
  '      Me.ProductionAreaID = ProductionAreaID
  '    End Sub

  '    Public Sub New()


  '    End Sub
  '  End Class

  '  Public Shared Function GetROLegendStatusList() As ROLegendStatusList

  '    Return DataPortal.Fetch(Of ROLegendStatusList)(New Criteria())

  '  End Function

  '  Public Shared Function GetROLegendStatusList(SystemID As Integer?, ProductionAreaID As Integer?) As ROLegendStatusList

  '    Return DataPortal.Fetch(Of ROLegendStatusList)(New Criteria(SystemID, ProductionAreaID))

  '  End Function

  '  Private Sub Fetch(sdr As SafeDataReader)

  '    Me.RaiseListChangedEvents = False
  '    Me.IsReadOnly = False
  '    While sdr.Read
  '      Me.Add(ROLegendStatus.GetROLegend(sdr))
  '    End While
  '    Me.IsReadOnly = True
  '    Me.RaiseListChangedEvents = True

  '  End Sub

  '  Protected Overrides Sub DataPortal_Fetch(criteria As Object)

  '    Dim crit As Criteria = criteria
  '    Using cn As New SqlConnection(Singular.Settings.ConnectionString)
  '      cn.Open()
  '      Try
  '        Using cm As SqlCommand = cn.CreateCommand
  '          cm.CommandType = CommandType.StoredProcedure
  '          cm.CommandText = "GetProcsWeb.getROLegendStatusList"
  '          cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
  '          cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
  '          Using sdr As New SafeDataReader(cm.ExecuteReader)
  '            Fetch(sdr)
  '          End Using
  '        End Using
  '      Finally
  '        cn.Close()
  '      End Try
  '    End Using
  '  End Sub
  'End Class

End Namespace
