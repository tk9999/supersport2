﻿' Generated 15 May 2017 11:57 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class ROShiftAllowanceTypeList
    Inherits OBReadOnlyListBase(Of ROShiftAllowanceTypeList, ROShiftAllowanceType)

#Region " Business Methods "

    Public Function GetItem(ShiftAllowanceTypeID As Integer) As ROShiftAllowanceType

      For Each child As ROShiftAllowanceType In Me
        If child.ShiftAllowanceTypeID = ShiftAllowanceTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Shift Allowance Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemProductionAreaID As Integer?
      <Display(Name:="Shift Allowance Type", Description:=""), PrimarySearchField>
      Public Property ShiftAllowanceType() As String

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROShiftAllowanceTypeList() As ROShiftAllowanceTypeList

      Return New ROShiftAllowanceTypeList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROShiftAllowanceTypeList() As ROShiftAllowanceTypeList

      Return DataPortal.Fetch(Of ROShiftAllowanceTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROShiftAllowanceType.GetROShiftAllowanceType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROShiftAllowanceTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace