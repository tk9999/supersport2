﻿' Generated 09 May 2017 16:07 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaShiftAllowanceSettingList
    Inherits OBBusinessListBase(Of SystemProductionAreaShiftAllowanceSettingList, SystemProductionAreaShiftAllowanceSetting)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaShiftAllowanceSettingID As Integer) As SystemProductionAreaShiftAllowanceSetting

      For Each child As SystemProductionAreaShiftAllowanceSetting In Me
        If child.SystemProductionAreaShiftAllowanceSettingID = SystemProductionAreaShiftAllowanceSettingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Shift Allowance Settings"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSystemProductionAreaShiftAllowanceSettingList() As SystemProductionAreaShiftAllowanceSettingList

      Return New SystemProductionAreaShiftAllowanceSettingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace