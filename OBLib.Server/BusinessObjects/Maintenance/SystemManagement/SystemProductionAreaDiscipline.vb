﻿' Generated 10 May 2016 12:09 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaDiscipline
    Inherits OBBusinessBase(Of SystemProductionAreaDiscipline)

#Region " Properties and Methods "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Expanded"), AlwaysClean>
    Public Overridable Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemProductionAreaDisciplineBO.SystemProductionAreaDisciplineBOToString(self)")

    Public Shared SystemProductionAreaDisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaDisciplineID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemProductionAreaDisciplineID() As Integer
      Get
        Return GetProperty(SystemProductionAreaDisciplineIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaID, "System Production Area", Nothing)
    ''' <summary>
    ''' Gets the System Production Area value
    ''' </summary>
    <Display(Name:="System Production Area", Description:="")>
    Public Property SystemProductionAreaID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(SystemProductionAreaIDProperty, value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.RODisciplineList))>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(DisciplineIDProperty, value)
      End Set
    End Property

    Public Shared IsShiftBasedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsShiftBased, "Is Shift Based", False)
    ''' <summary>
    ''' Gets the Is Shift Based value
    ''' </summary>
    <Display(Name:="Is Shift Based", Description:="")>
    Public Property IsShiftBased() As Boolean
      Get
        Return GetProperty(IsShiftBasedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsShiftBasedProperty, value)
      End Set
    End Property

    Public Shared DisciplineOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineOrder, "Discipline Order", 0)
    ''' <summary>
    ''' Gets the Discipline Order value
    ''' </summary>
    <Display(Name:="Discipline Order", Description:="")>
    Public Property DisciplineOrder() As Integer
      Get
        Return GetProperty(DisciplineOrderProperty)
      End Get
      Set(value As Integer)
        SetProperty(DisciplineOrderProperty, value)
      End Set
    End Property

    Public Shared PositionTypeRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PositionTypeRequired, "Position Type Required", False)
    ''' <summary>
    ''' Gets the Position Type Required value
    ''' </summary>
    <Display(Name:="Position Type Required", Description:="")>
    Public Property PositionTypeRequired() As Boolean
      Get
        Return GetProperty(PositionTypeRequiredProperty)
      End Get
      Set(value As Boolean)
        SetProperty(PositionTypeRequiredProperty, value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Modified Date Time", Description:=""), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline Name", "")
    ''' <summary>
    ''' Gets the Discipline Order value
    ''' </summary>
    <Display(Name:="Discipline Name", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(value As String)
        SetProperty(DisciplineProperty, value)
      End Set
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "LoginName", "")
    ''' <summary>
    ''' Gets and sets the Discipline Name
    ''' </summary>
    <Display(Name:="Last Modified", Description:="Shows the last user to modify this record.")>
    Public Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LoginNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SystemProductionAreaDisciplinePositionTypeListProperty As PropertyInfo(Of SystemProductionAreaDisciplinePositionTypeList) = RegisterProperty(Of SystemProductionAreaDisciplinePositionTypeList)(Function(c) c.SystemProductionAreaDisciplinePositionTypeList, "System Production Area Discipline Position Type List")

    Public ReadOnly Property SystemProductionAreaDisciplinePositionTypeList() As SystemProductionAreaDisciplinePositionTypeList
      Get
        If GetProperty(SystemProductionAreaDisciplinePositionTypeListProperty) Is Nothing Then
          LoadProperty(SystemProductionAreaDisciplinePositionTypeListProperty, Maintenance.SystemManagement.SystemProductionAreaDisciplinePositionTypeList.NewSystemProductionAreaDisciplinePositionTypeList())
        End If
        Return GetProperty(SystemProductionAreaDisciplinePositionTypeListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemProductionArea

      Return CType(CType(Me.Parent, SystemProductionAreaDisciplineList).Parent, SystemProductionArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaDisciplineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area Discipline")
        Else
          Return String.Format("Blank {0}", "System Production Area Discipline")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"SystemProductionAreaDisciplinePositionTypes"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemProductionAreaDiscipline() method.

    End Sub

    Public Shared Function NewSystemProductionAreaDiscipline() As SystemProductionAreaDiscipline

      Return DataPortal.CreateChild(Of SystemProductionAreaDiscipline)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemProductionAreaDiscipline(dr As SafeDataReader) As SystemProductionAreaDiscipline

      Dim s As New SystemProductionAreaDiscipline()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaDisciplineIDProperty, .GetInt32(0))
          LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(IsShiftBasedProperty, .GetBoolean(3))
          LoadProperty(DisciplineOrderProperty, .GetInt32(4))
          LoadProperty(PositionTypeRequiredProperty, .GetBoolean(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(DisciplineProperty, .GetString(10))
          LoadProperty(LoginNameProperty, .GetString(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemProductionAreaDisciplineIDProperty)

      cm.Parameters.AddWithValue("@SystemProductionAreaID", Me.GetParent.SystemProductionAreaID)
      cm.Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
      cm.Parameters.AddWithValue("@IsShiftBased", GetProperty(IsShiftBasedProperty))
      cm.Parameters.AddWithValue("@DisciplineOrder", GetProperty(DisciplineOrderProperty))
      cm.Parameters.AddWithValue("@PositionTypeRequired", GetProperty(PositionTypeRequiredProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemProductionAreaDisciplineIDProperty, cm.Parameters("@SystemProductionAreaDisciplineID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(SystemProductionAreaDisciplinePositionTypeListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemProductionAreaDisciplineID", GetProperty(SystemProductionAreaDisciplineIDProperty))
    End Sub

#End Region

  End Class

End Namespace