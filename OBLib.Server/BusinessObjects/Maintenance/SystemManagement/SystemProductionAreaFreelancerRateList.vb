﻿' Generated 09 May 2017 16:07 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaFreelancerRateList
    Inherits OBBusinessListBase(Of SystemProductionAreaFreelancerRateList, SystemProductionAreaFreelancerRate)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaFreelancerRateID As Integer) As SystemProductionAreaFreelancerRate

      For Each child As SystemProductionAreaFreelancerRate In Me
        If child.SystemProductionAreaFreelancerRateID = SystemProductionAreaFreelancerRateID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Freelancer Rates"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSystemProductionAreaFreelancerRateList() As SystemProductionAreaFreelancerRateList

      Return New SystemProductionAreaFreelancerRateList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace