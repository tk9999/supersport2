﻿' Generated 02 Jun 2016 13:43 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.Productions.ReadOnly
Imports Singular.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaTimelineType
    Inherits OBBusinessBase(Of SystemProductionAreaTimelineType)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemProductionAreaTimelineTypeBO.SystemProductionAreaTimelineTypeBOToString(self)")

    Public Shared SystemProductionAreaTimelineTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaTimelineTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SystemProductionAreaTimelineTypeID() As Integer
      Get
        Return GetProperty(SystemProductionAreaTimelineTypeIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaID, "System Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the System Production Area value
    ''' </summary>
    <Display(Name:="System Production Area", Description:=""),
    Required(ErrorMessage:="System Production Area required")>
  Public Property SystemProductionAreaID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTimelineTypeID, "Production Timeline Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Production Timeline Type", Description:=""),
    Required(ErrorMessage:="Production Timeline Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROProductionTimelineTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SystemProductionAreaTimelineTypeBO.setTimelineCriteriaBeforeRefresh", PreFindJSFunction:="SystemProductionAreaTimelineTypeBO.triggerTimelineAutoPopulate", AfterFetchJS:="SystemProductionAreaTimelineTypeBO.afterTimelineRefreshAjax",
                OnItemSelectJSFunction:="SystemProductionAreaTimelineTypeBO.onTimelineSelected", LookupMember:="ProductionTimelineType", DisplayMember:="ProductionTimelineType", ValueMember:="ProductionTimelineTypeID",
                DropDownCssClass:="mcrc-dropdown", DropDownColumns:={"ProductionTimelineType"}, UnselectedText:="None")>
    Public Property ProductionTimelineTypeID() As Integer?
      Get
        Return GetProperty(ProductionTimelineTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTimelineTypeIDProperty, Value)
      End Set
    End Property

    Public Shared OrderNumProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNum, "Order Num", 99)
    ''' <summary>
    ''' Gets and sets the Order Num value
    ''' </summary>
    <Display(Name:="Order Num", Description:=""),
    Required(ErrorMessage:="Order Num required")>
  Public Property OrderNum() As Integer
      Get
        Return GetProperty(OrderNumProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OrderNumProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Modified Date Time", Description:=""), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "LoginName", "")
    ''' <summary>
    ''' Gets and sets the Discipline Name
    ''' </summary>
    <Display(Name:="Last Modified", Description:="Shows the last user to modify this record.")>
    Public Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LoginNameProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTimelineType, "Production Timeline Type", "")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Production Timeline Type", Description:="Description for the Production Timeline Type")>
    Public ReadOnly Property ProductionTimelineType() As String
      Get
        Return GetProperty(ProductionTimelineTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemProductionArea

      Return CType(CType(Me.Parent, SystemProductionAreaTimelineTypeList).Parent, SystemProductionArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaTimelineTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area Timeline Type")
        Else
          Return String.Format("Blank {0}", "System Production Area Timeline Type")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemProductionAreaTimelineType() method.

    End Sub

    Public Shared Function NewSystemProductionAreaTimelineType() As SystemProductionAreaTimelineType

      Return DataPortal.CreateChild(Of SystemProductionAreaTimelineType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemProductionAreaTimelineType(dr As SafeDataReader) As SystemProductionAreaTimelineType

      Dim s As New SystemProductionAreaTimelineType()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaTimelineTypeIDProperty, .GetInt32(0))
          LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionTimelineTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(OrderNumProperty, .GetInt32(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(LoginNameProperty, .GetString(8))
          LoadProperty(ProductionTimelineTypeProperty, .GetString(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemProductionAreaTimelineTypeIDProperty)

      cm.Parameters.AddWithValue("@SystemProductionAreaID", GetProperty(SystemProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@ProductionTimelineTypeID", GetProperty(ProductionTimelineTypeIDProperty))
      cm.Parameters.AddWithValue("@OrderNum", GetProperty(OrderNumProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemProductionAreaTimelineTypeIDProperty, cm.Parameters("@SystemProductionAreaTimelineTypeID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemProductionAreaTimelineTypeID", GetProperty(SystemProductionAreaTimelineTypeIDProperty))
    End Sub

#End Region

  End Class

End Namespace