﻿' Generated 10 May 2016 12:09 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaDisciplinePositionTypeList
    Inherits OBBusinessListBase(Of SystemProductionAreaDisciplinePositionTypeList, SystemProductionAreaDisciplinePositionType)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaDisciplinePositionTypeID As Integer) As SystemProductionAreaDisciplinePositionType

      For Each child As SystemProductionAreaDisciplinePositionType In Me
        If child.SystemProductionAreaDisciplinePositionTypeID = SystemProductionAreaDisciplinePositionTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Discipline Position Types"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSystemProductionAreaDisciplinePositionTypeList() As SystemProductionAreaDisciplinePositionTypeList

      Return New SystemProductionAreaDisciplinePositionTypeList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace