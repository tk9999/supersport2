﻿' Generated 16 May 2017 11:07 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemAreaDisciplineShiftRateList
    Inherits OBBusinessListBase(Of SystemAreaDisciplineShiftRateList, SystemAreaDisciplineShiftRate)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(SystemAreaDisciplineShiftRatesID As Integer) As SystemAreaDisciplineShiftRate

      For Each child As SystemAreaDisciplineShiftRate In Me
        If child.SystemAreaDisciplineShiftRatesID = SystemAreaDisciplineShiftRatesID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Area Discipline Shift Rates"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewSystemAreaDisciplineShiftRateList() As SystemAreaDisciplineShiftRateList

      Return New SystemAreaDisciplineShiftRateList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSystemAreaDisciplineShiftRateList() As SystemAreaDisciplineShiftRateList

      Return DataPortal.Fetch(Of SystemAreaDisciplineShiftRateList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemAreaDisciplineShiftRate.GetSystemAreaDisciplineShiftRate(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Public Sub SetTotalRecords(TotalRecords As Integer)
      mTotalRecords = TotalRecords
    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemAreaDisciplineShiftRateList"
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@PageNo", Singular.Misc.NothingDBNull(crit.PageNo))
            cm.Parameters.AddWithValue("@PageSize", Singular.Misc.NothingDBNull(crit.PageSize))
            cm.Parameters.AddWithValue("@SortColumn", Singular.Misc.NothingDBNull(crit.SortColumn))
            cm.Parameters.AddWithValue("@SortAsc", Singular.Misc.NothingDBNull(crit.SortAsc))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace