﻿' Generated 10 May 2016 15:20 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaDisciplineSelect
    Inherits OBBusinessBase(Of SystemProductionAreaDisciplineSelect)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemProductionAreaDisciplineSelectBO.SystemProductionAreaDisciplineSelectBOToString(self)")

    Public Shared SystemProductionAreaDisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaDisciplineID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
    Public Property SystemProductionAreaDisciplineID() As Integer
      Get
        Return GetProperty(SystemProductionAreaDisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemProductionAreaDisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required(ErrorMessage:="System required")>
    Public Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area")
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:=""),
    Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    Required(ErrorMessage:="Discipline required")>
    Public Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared PositionTypeRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PositionTypeRequired, "Position Type Required", False)
    ''' <summary>
    ''' Gets and sets the Position Type Required value
    ''' </summary>
    <Display(Name:="Position Type Required", Description:=""),
    Required(ErrorMessage:="Position Type Required required")>
    Public Property PositionTypeRequired() As Boolean
      Get
        Return GetProperty(PositionTypeRequiredProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PositionTypeRequiredProperty, Value)
      End Set
    End Property

    Public Shared DisciplineQuantityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineQuantity, "Discipline Quantity")
    ''' <summary>
    ''' Gets and sets the Discipline Quantity value
    ''' </summary>
    <Display(Name:="Discipline Quantity", Description:=""),
    Required(ErrorMessage:="Discipline Quantity required")>
    Public Property DisciplineQuantity() As Integer
      Get
        Return GetProperty(DisciplineQuantityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(DisciplineQuantityProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SystemProductionAreaDisciplinePositionTypeSelectListProperty As PropertyInfo(Of SystemProductionAreaDisciplinePositionTypeSelectList) = RegisterProperty(Of SystemProductionAreaDisciplinePositionTypeSelectList)(Function(c) c.SystemProductionAreaDisciplinePositionTypeSelectList, "System Production Area Discipline Position Type Select List")

    Public ReadOnly Property SystemProductionAreaDisciplinePositionTypeSelectList() As SystemProductionAreaDisciplinePositionTypeSelectList
      Get
        If GetProperty(SystemProductionAreaDisciplinePositionTypeSelectListProperty) Is Nothing Then
          LoadProperty(SystemProductionAreaDisciplinePositionTypeSelectListProperty, Maintenance.SystemManagement.SystemProductionAreaDisciplinePositionTypeSelectList.NewSystemProductionAreaDisciplinePositionTypeSelectList())
        End If
        Return GetProperty(SystemProductionAreaDisciplinePositionTypeSelectListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaDisciplineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SystemProductionAreaDisciplineID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area Discipline Select")
        Else
          Return String.Format("Blank {0}", "System Production Area Discipline Select")
        End If
      Else
        Return Me.SystemProductionAreaDisciplineID.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemProductionAreaDisciplineSelect() method.

    End Sub

    Public Shared Function NewSystemProductionAreaDisciplineSelect() As SystemProductionAreaDisciplineSelect

      Return DataPortal.CreateChild(Of SystemProductionAreaDisciplineSelect)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemProductionAreaDisciplineSelect(dr As SafeDataReader) As SystemProductionAreaDisciplineSelect

      Dim s As New SystemProductionAreaDisciplineSelect()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaDisciplineIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(PositionTypeRequiredProperty, .GetBoolean(4))
          LoadProperty(DisciplineQuantityProperty, .GetInt32(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemProductionAreaDisciplineIDProperty)

      cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      cm.Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
      cm.Parameters.AddWithValue("@PositionTypeRequired", GetProperty(PositionTypeRequiredProperty))
      cm.Parameters.AddWithValue("@DisciplineQuantity", GetProperty(DisciplineQuantityProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemProductionAreaDisciplineIDProperty, cm.Parameters("@SystemProductionAreaDisciplineID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(SystemProductionAreaDisciplinePositionTypeSelectListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemProductionAreaDisciplineID", GetProperty(SystemProductionAreaDisciplineIDProperty))
    End Sub

#End Region

  End Class

End Namespace