﻿' Generated 22 Mar 2016 12:50 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Synergy
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()>
  Public Class SystemProductionAreaList
    Inherits OBBusinessListBase(Of SystemProductionAreaList, SystemProductionArea)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaID As Integer) As SystemProductionArea

      For Each child As SystemProductionArea In Me
        If child.SystemProductionAreaID = SystemProductionAreaID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Areas"

    End Function

    Public Function GetSystemProductionAreaFreelancerRate(SystemProductionAreaFreelancerRateID As Integer) As SystemProductionAreaFreelancerRate

      Dim obj As SystemProductionAreaFreelancerRate = Nothing
      For Each parent As SystemProductionArea In Me
        obj = parent.SystemProductionAreaFreelancerRateList.GetItem(SystemProductionAreaFreelancerRateID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemProductionAreaShiftAllowanceSetting(SystemProductionAreaShiftAllowanceSettingID As Integer) As SystemProductionAreaShiftAllowanceSetting

      Dim obj As SystemProductionAreaShiftAllowanceSetting = Nothing
      For Each parent As SystemProductionArea In Me
        obj = parent.SystemProductionAreaShiftAllowanceSettingList.GetItem(SystemProductionAreaShiftAllowanceSettingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemAreaDisciplineShiftRate(SystemAreaDisciplineShiftRateID As Integer) As SystemAreaDisciplineShiftRate

      Dim obj As SystemAreaDisciplineShiftRate = Nothing
      For Each Parent As SystemProductionArea In Me
        obj = Parent.SystemAreaDisciplineShiftRateList.GetItem(SystemAreaDisciplineShiftRateID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemProductionAreaDiscipline(SystemProductionAreaDisciplineID As Integer) As SystemProductionAreaDiscipline

      Dim obj As SystemProductionAreaDiscipline = Nothing
      For Each Parent As SystemProductionArea In Me
        obj = Parent.SystemProductionAreaDisciplineList.GetItem(SystemProductionAreaDisciplineID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemProductionAreaCallTimeSetting(SystemProductionAreaCallTimeSettingID As Integer) As SystemProductionAreaCallTimeSetting

      Dim obj As SystemProductionAreaCallTimeSetting = Nothing
      For Each Parent As SystemProductionArea In Me
        obj = Parent.SystemProductionAreaCallTimeSettingList.GetItem(SystemProductionAreaCallTimeSettingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemProductionAreaChannelDefault(SystemProductionAreaChannelDefaultID As Integer) As SystemProductionAreaChannelDefault

      Dim obj As SystemProductionAreaChannelDefault = Nothing
      For Each Parent As SystemProductionArea In Me
        obj = Parent.SystemProductionAreaChannelDefaultList.GetItem(SystemProductionAreaChannelDefaultID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemProductionAreaSetting(SystemProductionAreaSettingID As Integer) As SystemProductionAreaSetting

      Dim obj As SystemProductionAreaSetting = Nothing
      For Each Parent As SystemProductionArea In Me
        obj = Parent.SystemProductionAreaSettingList.GetItem(SystemProductionAreaSettingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemProductionAreaStatus(SystemProductionAreaStatusID As Integer) As SystemProductionAreaStatus

      Dim obj As SystemProductionAreaStatus = Nothing
      For Each Parent As SystemProductionArea In Me
        obj = Parent.SystemProductionAreaStatusList.GetItem(SystemProductionAreaStatusID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemProductionAreaTimelineType(SystemProductionAreaTimelineTypeID As Integer) As SystemProductionAreaTimelineType

      Dim obj As SystemProductionAreaTimelineType = Nothing
      For Each Parent As SystemProductionArea In Me
        obj = Parent.SystemProductionAreaTimelineTypeList.GetItem(SystemProductionAreaTimelineTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetAutomaticImport(AutomaticImportID As Integer) As AutomaticImport

      Dim obj As AutomaticImport = Nothing
      For Each Parent As SystemProductionArea In Me
        obj = Parent.AutomaticImportList.GetItem(AutomaticImportID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSystemProductionAreaList() As SystemProductionAreaList

      Return New SystemProductionAreaList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Sub BeginGetSystemProductionAreaList(CallBack As EventHandler(Of DataPortalResult(Of SystemProductionAreaList)))

      Dim dp As New DataPortal(Of SystemProductionAreaList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSystemProductionAreaList() As SystemProductionAreaList

      Return DataPortal.Fetch(Of SystemProductionAreaList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemProductionArea.GetSystemProductionArea(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SystemProductionArea = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemProductionAreaID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SystemProductionAreaFreelancerRateList.RaiseListChangedEvents = False
          parent.SystemProductionAreaFreelancerRateList.Add(SystemProductionAreaFreelancerRate.GetSystemProductionAreaFreelancerRate(sdr))
          parent.SystemProductionAreaFreelancerRateList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemProductionAreaID <> sdr.GetInt32(3) Then
            parent = Me.GetItem(sdr.GetInt32(3))
          End If
          parent.SystemProductionAreaShiftAllowanceSettingList.RaiseListChangedEvents = False
          parent.SystemProductionAreaShiftAllowanceSettingList.Add(SystemProductionAreaShiftAllowanceSetting.GetSystemProductionAreaShiftAllowanceSetting(sdr))
          parent.SystemProductionAreaShiftAllowanceSettingList.RaiseListChangedEvents = True
        End While
      End If

      Dim mTotalRecords As Integer

      If sdr.NextResult() Then
        sdr.Read()
        mTotalRecords = sdr.GetInt32(0)
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemProductionAreaID <> sdr.GetInt32(17) Then
            parent = Me.GetItem(sdr.GetInt32(17))
            parent.SystemAreaDisciplineShiftRateList.SetTotalRecords(mTotalRecords)
          End If
          parent.SystemAreaDisciplineShiftRateList.RaiseListChangedEvents = False
          parent.SystemAreaDisciplineShiftRateList.Add(SystemAreaDisciplineShiftRate.GetSystemAreaDisciplineShiftRate(sdr))
          parent.SystemAreaDisciplineShiftRateList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemProductionAreaID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SystemProductionAreaDisciplineList.RaiseListChangedEvents = False
          parent.SystemProductionAreaDisciplineList.Add(SystemProductionAreaDiscipline.GetSystemProductionAreaDiscipline(sdr))
          parent.SystemProductionAreaDisciplineList.RaiseListChangedEvents = True
        End While
      End If

      Dim ParentDiscipline As SystemProductionAreaDiscipline = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If ParentDiscipline Is Nothing OrElse ParentDiscipline.SystemProductionAreaDisciplineID <> sdr.GetInt32(1) Then
            ParentDiscipline = Me.GetSystemProductionAreaDiscipline(sdr.GetInt32(1))
          End If
          ParentDiscipline.SystemProductionAreaDisciplinePositionTypeList.RaiseListChangedEvents = False
          ParentDiscipline.SystemProductionAreaDisciplinePositionTypeList.Add(SystemProductionAreaDisciplinePositionType.GetSystemProductionAreaDisciplinePositionType(sdr))
          ParentDiscipline.SystemProductionAreaDisciplinePositionTypeList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemProductionAreaID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SystemProductionAreaCallTimeSettingList.RaiseListChangedEvents = False
          parent.SystemProductionAreaCallTimeSettingList.Add(SystemProductionAreaCallTimeSetting.GetSystemProductionAreaCallTimeSetting(sdr))
          parent.SystemProductionAreaCallTimeSettingList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemProductionAreaID <> sdr.GetInt32(2) Then
            parent = Me.GetItem(sdr.GetInt32(2))
          End If
          parent.SystemProductionAreaChannelDefaultList.RaiseListChangedEvents = False
          parent.SystemProductionAreaChannelDefaultList.Add(SystemProductionAreaChannelDefault.GetSystemProductionAreaChannelDefault(sdr))
          parent.SystemProductionAreaChannelDefaultList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemProductionAreaID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SystemProductionAreaSettingList.RaiseListChangedEvents = False
          parent.SystemProductionAreaSettingList.Add(SystemProductionAreaSetting.GetSystemProductionAreaSetting(sdr))
          parent.SystemProductionAreaSettingList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemProductionAreaID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SystemProductionAreaStatusList.RaiseListChangedEvents = False
          parent.SystemProductionAreaStatusList.Add(SystemProductionAreaStatus.GetSystemProductionAreaStatus(sdr))
          parent.SystemProductionAreaStatusList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemProductionAreaID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SystemProductionAreaTimelineTypeList.RaiseListChangedEvents = False
          parent.SystemProductionAreaTimelineTypeList.Add(SystemProductionAreaTimelineType.GetSystemProductionAreaTimelineType(sdr))
          parent.SystemProductionAreaTimelineTypeList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemProductionAreaID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.AutomaticImportList.RaiseListChangedEvents = False
          parent.AutomaticImportList.Add(AutomaticImport.GetAutomaticImport(sdr))
          parent.AutomaticImportList.RaiseListChangedEvents = True
        End While
      End If

      Dim ParentAutomaticImport As AutomaticImport = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If ParentAutomaticImport Is Nothing OrElse ParentAutomaticImport.AutomaticImportID <> sdr.GetInt32(1) Then
            ParentAutomaticImport = Me.GetAutomaticImport(sdr.GetInt32(1))
          End If
          ParentAutomaticImport.AutomaticImportCrewList.RaiseListChangedEvents = False
          ParentAutomaticImport.AutomaticImportCrewList.Add(AutomaticImportCrew.GetAutomaticImportCrew(sdr))
          ParentAutomaticImport.AutomaticImportCrewList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As SystemProductionArea In Me
        child.CheckRules()
        For Each SystemProductionAreaFreelancerRate As SystemProductionAreaFreelancerRate In child.SystemProductionAreaFreelancerRateList
          SystemProductionAreaFreelancerRate.CheckRules()
        Next
        For Each SystemProductionAreaShiftAllowanceSetting As SystemProductionAreaShiftAllowanceSetting In child.SystemProductionAreaShiftAllowanceSettingList
          SystemProductionAreaShiftAllowanceSetting.CheckRules()
        Next
        For Each SystemAreaDisciplineShiftRate As SystemAreaDisciplineShiftRate In child.SystemAreaDisciplineShiftRateList
          SystemAreaDisciplineShiftRate.CheckRules()
        Next
        For Each SystemProductionAreaDiscipline As SystemProductionAreaDiscipline In child.SystemProductionAreaDisciplineList
          SystemProductionAreaDiscipline.CheckRules()
          For Each SystemProductionAreaDisciplinePositionType As SystemProductionAreaDisciplinePositionType In SystemProductionAreaDiscipline.SystemProductionAreaDisciplinePositionTypeList
            SystemProductionAreaDisciplinePositionType.CheckRules()
          Next
        Next
        For Each SystemProductionAreaCallTimeSetting As SystemProductionAreaCallTimeSetting In child.SystemProductionAreaCallTimeSettingList
          SystemProductionAreaCallTimeSetting.CheckRules()
        Next
        For Each SystemProductionAreaChannelDefault As SystemProductionAreaChannelDefault In child.SystemProductionAreaChannelDefaultList
          SystemProductionAreaChannelDefault.CheckRules()
        Next
        For Each SystemProductionAreaSetting As SystemProductionAreaSetting In child.SystemProductionAreaSettingList
          SystemProductionAreaSetting.CheckRules()
        Next
        For Each SystemProductionAreaStatus As SystemProductionAreaStatus In child.SystemProductionAreaStatusList
          SystemProductionAreaStatus.CheckRules()
        Next
        For Each SystemProductionAreaTimelineType As SystemProductionAreaTimelineType In child.SystemProductionAreaTimelineTypeList
          SystemProductionAreaTimelineType.CheckRules()
        Next
        For Each AutomaticImport As AutomaticImport In child.AutomaticImportList
          AutomaticImport.CheckRules()
          For Each AutomaticImportCrew As AutomaticImportCrew In AutomaticImport.AutomaticImportCrewList
            AutomaticImportCrew.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemProductionAreaList"
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

#End Region

  End Class

End Namespace