﻿' Generated 02 Jun 2016 13:43 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaTimelineTypeList
    Inherits OBBusinessListBase(Of SystemProductionAreaTimelineTypeList, SystemProductionAreaTimelineType)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaTimelineTypeID As Integer) As SystemProductionAreaTimelineType

      For Each child As SystemProductionAreaTimelineType In Me
        If child.SystemProductionAreaTimelineTypeID = SystemProductionAreaTimelineTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Timeline Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewSystemProductionAreaTimelineTypeList() As SystemProductionAreaTimelineTypeList

      Return New SystemProductionAreaTimelineTypeList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSystemProductionAreaTimelineTypeList() As SystemProductionAreaTimelineTypeList

      Return DataPortal.Fetch(Of SystemProductionAreaTimelineTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemProductionAreaTimelineType.GetSystemProductionAreaTimelineType(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemProductionAreaTimelineTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace