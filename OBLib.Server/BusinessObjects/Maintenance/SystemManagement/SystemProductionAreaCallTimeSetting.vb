﻿' Generated 26 May 2016 09:48 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaCallTimeSetting
    Inherits OBBusinessBase(Of SystemProductionAreaCallTimeSetting)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemProductionAreaCallTimeSettingBO.SystemProductionAreaCallTimeSettingBOToString(self)")

    Public Shared SystemProductionAreaCallTimeSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaCallTimeSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SystemProductionAreaCallTimeSettingID() As Integer
      Get
        Return GetProperty(SystemProductionAreaCallTimeSettingIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaID, "System Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the System Production Area value
    ''' </summary>
    <Display(Name:="System Production Area", Description:=""),
    Required(ErrorMessage:="System Production Area required")>
  Public Property SystemProductionAreaID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets and sets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:=""),
    Required(ErrorMessage:="Effective Date required")>
  Public Property EffectiveDate As Date
      Get
        Return GetProperty(EffectiveDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(EffectiveDateProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
    Required(ErrorMessage:="Room required"),
    Singular.DataAnnotations.DropDownWeb(GetType(RORoomList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SystemProductionAreaCallTimeSettingBO.setRoomCriteriaBeforeRefresh", PreFindJSFunction:="SystemProductionAreaCallTimeSettingBO.triggerRoomAutoPopulate", AfterFetchJS:="SystemProductionAreaCallTimeSettingBO.afterRoomRefreshAjax",
                OnItemSelectJSFunction:="SystemProductionAreaCallTimeSettingBO.onRoomSelected", LookupMember:="Room", DisplayMember:="Room", ValueMember:="RoomID",
                DropDownCssClass:="mcrc-dropdown", DropDownColumns:={"Room"}, UnselectedText:="None")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared CallTimeMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CallTimeMinutes, "Call Time Minutes", 0)
    ''' <summary>
    ''' Gets and sets the Call Time Minutes value
    ''' </summary>
    <Display(Name:="Call Time Minutes", Description:=""),
    Required(ErrorMessage:="Call Time Minutes required")>
  Public Property CallTimeMinutes() As Integer
      Get
        Return GetProperty(CallTimeMinutesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CallTimeMinutesProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WrapTimeMinutes, "Wrap Time Minutes", 0)
    ''' <summary>
    ''' Gets and sets the Wrap Time Minutes value
    ''' </summary>
    <Display(Name:="Wrap Time Minutes", Description:=""),
    Required(ErrorMessage:="Wrap Time Minutes required")>
  Public Property WrapTimeMinutes() As Integer
      Get
        Return GetProperty(WrapTimeMinutesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WrapTimeMinutesProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Modified Date Time", Description:=""), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "LoginName", "")
    ''' <summary>
    ''' Gets and sets the Discipline Name
    ''' </summary>
    <Display(Name:="Last Modified", Description:="Shows the last user to modify this record.")>
    Public Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LoginNameProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room", "")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="The name of the room")>
    Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemProductionArea

      Return CType(CType(Me.Parent, SystemProductionAreaCallTimeSettingList).Parent, SystemProductionArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaCallTimeSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area Call Time Setting")
        Else
          Return String.Format("Blank {0}", "System Production Area Call Time Setting")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemProductionAreaCallTimeSetting() method.

    End Sub

    Public Shared Function NewSystemProductionAreaCallTimeSetting() As SystemProductionAreaCallTimeSetting

      Return DataPortal.CreateChild(Of SystemProductionAreaCallTimeSetting)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemProductionAreaCallTimeSetting(dr As SafeDataReader) As SystemProductionAreaCallTimeSetting

      Dim s As New SystemProductionAreaCallTimeSetting()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaCallTimeSettingIDProperty, .GetInt32(0))
          LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EffectiveDateProperty, .GetValue(2))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CallTimeMinutesProperty, .GetInt32(4))
          LoadProperty(WrapTimeMinutesProperty, .GetInt32(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(LoginNameProperty, .GetString(10))
          LoadProperty(RoomProperty, .GetString(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemProductionAreaCallTimeSettingIDProperty)

      cm.Parameters.AddWithValue("@SystemProductionAreaID", GetProperty(SystemProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@EffectiveDate", EffectiveDate)
      cm.Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
      cm.Parameters.AddWithValue("@CallTimeMinutes", GetProperty(CallTimeMinutesProperty))
      cm.Parameters.AddWithValue("@WrapTimeMinutes", GetProperty(WrapTimeMinutesProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemProductionAreaCallTimeSettingIDProperty, cm.Parameters("@SystemProductionAreaCallTimeSettingID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemProductionAreaCallTimeSettingID", GetProperty(SystemProductionAreaCallTimeSettingIDProperty))
    End Sub

#End Region

  End Class

End Namespace