﻿' Generated 26 May 2016 09:48 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaCallTimeSettingList
    Inherits OBBusinessListBase(Of SystemProductionAreaCallTimeSettingList, SystemProductionAreaCallTimeSetting)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaCallTimeSettingID As Integer) As SystemProductionAreaCallTimeSetting

      For Each child As SystemProductionAreaCallTimeSetting In Me
        If child.SystemProductionAreaCallTimeSettingID = SystemProductionAreaCallTimeSettingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Call Time Settings"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewSystemProductionAreaCallTimeSettingList() As SystemProductionAreaCallTimeSettingList

      Return New SystemProductionAreaCallTimeSettingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSystemProductionAreaCallTimeSettingList() As SystemProductionAreaCallTimeSettingList

      Return DataPortal.Fetch(Of SystemProductionAreaCallTimeSettingList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemProductionAreaCallTimeSetting.GetSystemProductionAreaCallTimeSetting(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemProductionAreaCallTimeSettingList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace