﻿' Generated 10 May 2016 15:21 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaDisciplinePositionTypeSelect
    Inherits OBBusinessBase(Of SystemProductionAreaDisciplinePositionTypeSelect)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemProductionAreaDisciplinePositionTypeSelectBO.SystemProductionAreaDisciplinePositionTypeSelectBOToString(self)")

    Public Shared SystemProductionAreaDisciplinePositionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaDisciplinePositionTypeID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
  Public Property SystemProductionAreaDisciplinePositionTypeID() As Integer
      Get
        Return GetProperty(SystemProductionAreaDisciplinePositionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemProductionAreaDisciplinePositionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared SystemProductionAreaDisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaDisciplineID, "System Production Area Discipline")
    ''' <summary>
    ''' Gets the System Production Area Discipline value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property SystemProductionAreaDisciplineID() As Integer
      Get
        Return GetProperty(SystemProductionAreaDisciplineIDProperty)
      End Get
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionTypeID, "Position Type")
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:=""),
    Required(ErrorMessage:="Position Type required")>
  Public Property PositionTypeID() As Integer
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PositionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionID, "Position")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:=""),
    Required(ErrorMessage:="Position required")>
  Public Property PositionID() As Integer
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared PositionQuantityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionQuantity, "Position Quantity")
    ''' <summary>
    ''' Gets and sets the Position Quantity value
    ''' </summary>
    <Display(Name:="Position Quantity", Description:=""),
    Required(ErrorMessage:="Position Quantity required")>
  Public Property PositionQuantity() As Integer
      Get
        Return GetProperty(PositionQuantityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PositionQuantityProperty, Value)
      End Set
    End Property

    Public Shared IncludeInBookingDescriptionProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IncludeInBookingDescription, "Include In Booking Description", False)
    ''' <summary>
    ''' Gets the Position Order value
    ''' </summary>
    <Display(Name:="Include In Booking Description", Description:="")>
    Public Property IncludeInBookingDescription() As Boolean
      Get
        Return GetProperty(IncludeInBookingDescriptionProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IncludeInBookingDescriptionProperty, value)
      End Set
    End Property

    Public Shared PositionShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionShortName, "Short Name", "")
    ''' <summary>
    ''' Gets the Position Order value
    ''' </summary>
    <Display(Name:="Short Name", Description:="")>
    Public Property PositionShortName() As String
      Get
        Return GetProperty(PositionShortNameProperty)
      End Get
      Set(value As String)
        SetProperty(PositionShortNameProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemProductionAreaDisciplineSelect

      Return CType(CType(Me.Parent, SystemProductionAreaDisciplinePositionTypeSelectList).Parent, SystemProductionAreaDisciplineSelect)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaDisciplinePositionTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SystemProductionAreaDisciplinePositionTypeID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area Discipline Position Type Select")
        Else
          Return String.Format("Blank {0}", "System Production Area Discipline Position Type Select")
        End If
      Else
        Return Me.SystemProductionAreaDisciplinePositionTypeID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemProductionAreaDisciplinePositionTypeSelect() method.

    End Sub

    Public Shared Function NewSystemProductionAreaDisciplinePositionTypeSelect() As SystemProductionAreaDisciplinePositionTypeSelect

      Return DataPortal.CreateChild(Of SystemProductionAreaDisciplinePositionTypeSelect)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemProductionAreaDisciplinePositionTypeSelect(dr As SafeDataReader) As SystemProductionAreaDisciplinePositionTypeSelect

      Dim s As New SystemProductionAreaDisciplinePositionTypeSelect()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaDisciplinePositionTypeIDProperty, .GetInt32(0))
          LoadProperty(SystemProductionAreaDisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(PositionQuantityProperty, .GetInt32(4))
          LoadProperty(IncludeInBookingDescriptionProperty, .GetBoolean(5))
          LoadProperty(PositionShortNameProperty, .GetString(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemProductionAreaDisciplinePositionTypeIDProperty)

      cm.Parameters.AddWithValue("@SystemProductionAreaDisciplineID", Me.GetParent().SystemProductionAreaDisciplineID)
      cm.Parameters.AddWithValue("@PositionTypeID", GetProperty(PositionTypeIDProperty))
      cm.Parameters.AddWithValue("@PositionID", GetProperty(PositionIDProperty))
      cm.Parameters.AddWithValue("@PositionQuantity", GetProperty(PositionQuantityProperty))
      cm.Parameters.AddWithValue("@IncludeInBookingDescription", GetProperty(IncludeInBookingDescriptionProperty))
      cm.Parameters.AddWithValue("@PositionShortName", GetProperty(PositionShortNameProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemProductionAreaDisciplinePositionTypeIDProperty, cm.Parameters("@SystemProductionAreaDisciplinePositionTypeID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemProductionAreaDisciplinePositionTypeID", GetProperty(SystemProductionAreaDisciplinePositionTypeIDProperty))
    End Sub

#End Region

  End Class

End Namespace