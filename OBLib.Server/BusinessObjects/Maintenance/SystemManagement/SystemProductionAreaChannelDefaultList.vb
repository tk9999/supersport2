﻿' Generated 15 Jun 2017 11:42 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaChannelDefaultList
    Inherits OBBusinessListBase(Of SystemProductionAreaChannelDefaultList, SystemProductionAreaChannelDefault)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaChannelDefaultID As Integer) As SystemProductionAreaChannelDefault

      For Each child As SystemProductionAreaChannelDefault In Me
        If child.SystemProductionAreaChannelDefaultID = SystemProductionAreaChannelDefaultID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Channel Defaults"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewSystemProductionAreaChannelDefaultList() As SystemProductionAreaChannelDefaultList

      Return New SystemProductionAreaChannelDefaultList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSystemProductionAreaChannelDefaultList() As SystemProductionAreaChannelDefaultList

      Return DataPortal.Fetch(Of SystemProductionAreaChannelDefaultList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemProductionAreaChannelDefault.GetSystemProductionAreaChannelDefault(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemProductionAreaChannelDefaultList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace