﻿' Generated 22 Mar 2016 12:50 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaOld
    Inherits OBBusinessBase(Of SystemProductionAreaOld)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
    Public Property SystemProductionAreaID() As Integer
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required(ErrorMessage:="System required")>
    Public Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area")
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:=""),
    Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared RoomCallTimeRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomCallTimeRequired, "Room Call Time Required", False)
    ''' <summary>
    ''' Gets and sets the Room Call Time Required value
    ''' </summary>
    <Display(Name:="Room Call Time Required", Description:=""),
    Required(ErrorMessage:="Room Call Time Required required")>
    Public Property RoomCallTimeRequired() As Boolean
      Get
        Return GetProperty(RoomCallTimeRequiredProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RoomCallTimeRequiredProperty, Value)
      End Set
    End Property

    Public Shared RoomWrapTimeRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomWrapTimeRequired, "Room Wrap Time Required", False)
    ''' <summary>
    ''' Gets and sets the Room Wrap Time Required value
    ''' </summary>
    <Display(Name:="Room Wrap Time Required", Description:=""),
    Required(ErrorMessage:="Room Wrap Time Required required")>
    Public Property RoomWrapTimeRequired() As Boolean
      Get
        Return GetProperty(RoomWrapTimeRequiredProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RoomWrapTimeRequiredProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Production Area")
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaProperty, Value)
      End Set
    End Property

    Public Shared RequiresChannelsForRoomSchedulingProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresChannelsForRoomScheduling, "Requires Channel Details For Room Scheduling", False)
    ''' <summary>
    ''' Gets and sets the Room Wrap Time Required value
    ''' </summary>
    <Display(Name:="Requires Channel Details For Room Scheduling", Description:="")>
    Public Property RequiresChannelsForRoomScheduling() As Boolean
      Get
        Return GetProperty(RequiresChannelsForRoomSchedulingProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RequiresChannelsForRoomSchedulingProperty, Value)
      End Set
    End Property

    Public Shared ShiftBasedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShiftBased, "Shift Based", False)
    ''' <summary>
    ''' Gets and sets the Room Wrap Time Required value
    ''' </summary>
    <Display(Name:="Shift Based", Description:="")>
    Public Property ShiftBased() As Boolean
      Get
        Return GetProperty(ShiftBasedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ShiftBasedProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionArea.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area")
        Else
          Return String.Format("Blank {0}", "System Production Area")
        End If
      Else
        Return Me.ProductionArea
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemProductionArea() method.

    End Sub

    Public Shared Function NewSystemProductionArea() As SystemProductionAreaOld

      Return DataPortal.CreateChild(Of SystemProductionAreaOld)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSystemProductionArea(dr As SafeDataReader) As SystemProductionAreaOld

      Dim s As New SystemProductionAreaOld()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(RoomCallTimeRequiredProperty, .GetBoolean(3))
          LoadProperty(RoomWrapTimeRequiredProperty, .GetBoolean(4))
          LoadProperty(ProductionAreaProperty, .GetString(5))
          ''LoadProperty(DefaultLeaveDurationProperty, .GetInt32(6))
          ''If .IsDBNull(7) Then
          ''  LoadProperty(DefaultLeaveStartTimeProperty, DateTime.MinValue)
          ''Else
          ''  LoadProperty(DefaultLeaveStartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(7))))
          ''End If
          LoadProperty(RequiresChannelsForRoomSchedulingProperty, .GetBoolean(6))
          LoadProperty(ShiftBasedProperty, .GetBoolean(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSystemProductionArea"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSystemProductionArea"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSystemProductionAreaID As SqlParameter = .Parameters.Add("@SystemProductionAreaID", SqlDbType.Int)
          paramSystemProductionAreaID.Value = GetProperty(SystemProductionAreaIDProperty)
          If Me.IsNew Then
            paramSystemProductionAreaID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@RoomCallTimeRequired", GetProperty(RoomCallTimeRequiredProperty))
          .Parameters.AddWithValue("@RoomWrapTimeRequired", GetProperty(RoomWrapTimeRequiredProperty))
          .Parameters.AddWithValue("@ProductionArea", GetProperty(ProductionAreaProperty))
          '.Parameters.AddWithValue("@DefaultLeaveDuration", GetProperty(DefaultLeaveDurationProperty))
          '.Parameters.AddWithValue("@DefaultLeaveStartTime", GetProperty(DefaultLeaveStartTimeProperty))
          .Parameters.AddWithValue("@RequiresChannelsForRoomScheduling", GetProperty(RequiresChannelsForRoomSchedulingProperty))
          .Parameters.AddWithValue("@ShiftBased", GetProperty(ShiftBasedProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SystemProductionAreaIDProperty, paramSystemProductionAreaID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSystemProductionArea"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SystemProductionAreaID", GetProperty(SystemProductionAreaIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
