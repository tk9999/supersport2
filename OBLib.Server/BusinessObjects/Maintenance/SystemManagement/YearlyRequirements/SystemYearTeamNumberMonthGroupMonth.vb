﻿' Generated 27 Apr 2016 11:07 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemYearTeamNumberMonthGroupMonth
    Inherits OBBusinessBase(Of SystemYearTeamNumberMonthGroupMonth)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemYearTeamNumberMonthGroupMonthBO.SystemYearTeamNumberMonthGroupMonthBOToString(self)")

    Public Shared SystemTimesheetMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTimesheetMonthID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property SystemTimesheetMonthID() As Integer
      Get
        Return GetProperty(SystemTimesheetMonthIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemTimesheetMonthIDProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamNumberMonthGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTeamNumberMonthGroupID, "System Team Number Month Group")
    ''' <summary>
    ''' Gets the System Team Number Month Group value
    ''' </summary>
    Public Property SystemTeamNumberMonthGroupID() As Integer
      Get
        Return GetProperty(SystemTeamNumberMonthGroupIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(SystemTeamNumberMonthGroupIDProperty, value)
      End Set
    End Property

    Public Shared SystemYearMonthGroupMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemYearMonthGroupMonthID, "System Year Month Group Month")
    ''' <summary>
    ''' Gets and sets the System Year Month Group Month value
    ''' </summary>
    <Display(Name:="System Year Month Group Month", Description:=""),
    Required(ErrorMessage:="System Year Month Group Month required")>
    Public Property SystemYearMonthGroupMonthID() As Integer
      Get
        Return GetProperty(SystemYearMonthGroupMonthIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemYearMonthGroupMonthIDProperty, Value)
      End Set
    End Property

    Public Shared MonthStartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MonthStartDate, "Month Start Date")
    ''' <summary>
    ''' Gets and sets the Month Start Date value
    ''' </summary>
    <Display(Name:="Month Start Date", Description:=""),
    Required(ErrorMessage:="Month Start Date required")>
    Public Property MonthStartDate As Date
      Get
        Return GetProperty(MonthStartDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(MonthStartDateProperty, Value)
      End Set
    End Property

    Public Shared MonthEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MonthEndDate, "Month End Date")
    ''' <summary>
    ''' Gets and sets the Month End Date value
    ''' </summary>
    <Display(Name:="Month End Date", Description:=""),
    Required(ErrorMessage:="Month End Date required")>
    Public Property MonthEndDate As Date
      Get
        Return GetProperty(MonthEndDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(MonthEndDateProperty, Value)
      End Set
    End Property

    Public Shared SystemTimesheetMonthProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemTimesheetMonth, "System Timesheet Month")
    ''' <summary>
    ''' Gets and sets the System Timesheet Month value
    ''' </summary>
    <Display(Name:="System Timesheet Month", Description:="")>
    Public Property SystemTimesheetMonth() As String
      Get
        Return GetProperty(SystemTimesheetMonthProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemTimesheetMonthProperty, Value)
      End Set
    End Property

    Public Shared RequiredHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RequiredHours, "Required Hours")
    ''' <summary>
    ''' Gets and sets the Required Hours value
    ''' </summary>
    <Display(Name:="Required Hours", Description:=""),
    Required(ErrorMessage:="Required Hours required")>
    Public Property RequiredHours() As Decimal
      Get
        Return GetProperty(RequiredHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RequiredHoursProperty, Value)
      End Set
    End Property

    Public Shared RequiredShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RequiredShifts, "Required Shifts")
    ''' <summary>
    ''' Gets and sets the Required Shifts value
    ''' </summary>
    <Display(Name:="Required Shifts", Description:="")>
    Public Property RequiredShifts() As Integer
      Get
        Return GetProperty(RequiredShiftsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RequiredShiftsProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared RequiredHoursHRTotalProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RequiredHoursHRTotal, "Required Hours", CDec(0))
    ''' <summary>
    ''' Gets and sets the System Team Number Name value
    ''' </summary>
    <Display(Name:="Required Hours", Description:="")>
    Public Property RequiredHoursHRTotal() As Decimal
      Get
        Return GetProperty(RequiredHoursHRTotalProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RequiredHoursHRTotalProperty, Value)
      End Set
    End Property

    Public Shared TimesheetHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TimesheetHours, "Timesheet Hours", CDec(0))
    ''' <summary>
    ''' Gets and sets the System Team Number Name value
    ''' </summary>
    <Display(Name:="Timesheet Hours", Description:="")>
    Public Property TimesheetHours() As Decimal
      Get
        Return GetProperty(TimesheetHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TimesheetHoursProperty, Value)
      End Set
    End Property

    Public Shared TimesheetsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Timesheets, "Timesheets", 0)
    ''' <summary>
    ''' Gets and sets the System Team Number Name value
    ''' </summary>
    <Display(Name:="Timesheets", Description:="")>
    Public Property Timesheets() As Integer
      Get
        Return GetProperty(TimesheetsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TimesheetsProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemYearTeamNumberMonthGroup

      Return CType(CType(Me.Parent, SystemYearTeamNumberMonthGroupMonthList).Parent, SystemYearTeamNumberMonthGroup)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemTimesheetMonthIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SystemTimesheetMonth.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Year Team Number Month Group Month")
        Else
          Return String.Format("Blank {0}", "System Year Team Number Month Group Month")
        End If
      Else
        Return Me.SystemTimesheetMonth
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(RequiredHoursProperty)
        .ServerRuleFunction = AddressOf RequiredHoursValid
        .JavascriptRuleFunctionName = "SystemYearTeamNumberMonthGroupMonthBO.RequiredHoursValid"
      End With

    End Sub

    Public Shared Function RequiredHoursValid(MonthGroupMonth As SystemYearTeamNumberMonthGroupMonth) As String

      If Not MonthGroupMonth.RequiredHours > 0 Then
        Return "Required Hours must be greater than 0"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemYearTeamNumberMonthGroupMonth() method.

    End Sub

    Public Shared Function NewSystemYearTeamNumberMonthGroupMonth() As SystemYearTeamNumberMonthGroupMonth

      Return DataPortal.CreateChild(Of SystemYearTeamNumberMonthGroupMonth)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemYearTeamNumberMonthGroupMonth(dr As SafeDataReader) As SystemYearTeamNumberMonthGroupMonth

      Dim s As New SystemYearTeamNumberMonthGroupMonth()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemTimesheetMonthIDProperty, .GetInt32(0))
          LoadProperty(SystemTeamNumberMonthGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SystemYearMonthGroupMonthIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(MonthStartDateProperty, .GetValue(3))
          LoadProperty(MonthEndDateProperty, .GetValue(4))
          LoadProperty(SystemTimesheetMonthProperty, .GetString(5))
          LoadProperty(RequiredHoursProperty, .GetDecimal(6))
          LoadProperty(RequiredShiftsProperty, .GetInt32(7))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(11))
          LoadProperty(RequiredHoursHRTotalProperty, .GetDecimal(12))
          LoadProperty(TimesheetHoursProperty, .GetDecimal(13))
          LoadProperty(TimesheetsProperty, .GetInt32(14))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemTimesheetMonthIDProperty)

      cm.Parameters.AddWithValue("@SystemID", Me.GetParent().GetParent().SystemID)
      cm.Parameters.AddWithValue("@SystemYearMonthGroupMonthID", GetProperty(SystemYearMonthGroupMonthIDProperty))
      cm.Parameters.AddWithValue("@SystemTeamNumberMonthGroupID", Me.GetParent().SystemTeamNumberMonthGroupID)
      cm.Parameters.AddWithValue("@SystemTimesheetMonth", GetProperty(SystemTimesheetMonthProperty))
      cm.Parameters.AddWithValue("@MonthStartDate", MonthStartDate)
      cm.Parameters.AddWithValue("@MonthEndDate", MonthEndDate)
      cm.Parameters.AddWithValue("@RequiredHours", GetProperty(RequiredHoursProperty))
      cm.Parameters.AddWithValue("@RequiredShifts", GetProperty(RequiredShiftsProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@SystemTeamNumberID", Me.GetParent.GetParent.SystemTeamNumberID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemTimesheetMonthIDProperty, cm.Parameters("@SystemTimesheetMonthID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemTimesheetMonthID", GetProperty(SystemTimesheetMonthIDProperty))
    End Sub

#End Region

  End Class

End Namespace