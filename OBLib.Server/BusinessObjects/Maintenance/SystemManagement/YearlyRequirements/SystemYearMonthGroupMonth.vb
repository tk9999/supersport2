﻿' Generated 25 Apr 2016 14:23 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemYearMonthGroupMonth
    Inherits OBBusinessBase(Of SystemYearMonthGroupMonth)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemYearMonthGroupMonthBO.SystemYearMonthGroupMonthBOToString(self)")

    Public Shared SystemYearMonthGroupMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemYearMonthGroupMonthID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property SystemYearMonthGroupMonthID() As Integer
      Get
        Return GetProperty(SystemYearMonthGroupMonthIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(SystemYearMonthGroupMonthIDProperty, value)
      End Set
    End Property

    Public Shared SystemYearMonthGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemYearMonthGroupID, "System Year Month Group", Nothing)
    ''' <summary>
    ''' Gets the System Year Month Group value
    ''' </summary>
    Public Property SystemYearMonthGroupID() As Integer?
      Get
        Return GetProperty(SystemYearMonthGroupIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(SystemYearMonthGroupIDProperty, value)
      End Set
    End Property

    Public Shared MonthStartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MonthStartDate, "Month Start Date")
    ''' <summary>
    ''' Gets and sets the Month Start Date value
    ''' </summary>
    <Display(Name:="Month Start Date", Description:=""),
    Required(ErrorMessage:="Month Start Date required")>
    Public Property MonthStartDate As Date
      Get
        Return GetProperty(MonthStartDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(MonthStartDateProperty, Value)
      End Set
    End Property

    Public Shared MonthEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MonthEndDate, "Month End Date")
    ''' <summary>
    ''' Gets and sets the Month End Date value
    ''' </summary>
    <Display(Name:="Month End Date", Description:=""),
    Required(ErrorMessage:="Month End Date required")>
    Public Property MonthEndDate As Date
      Get
        Return GetProperty(MonthEndDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(MonthEndDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemYearMonthGroup

      Return CType(CType(Me.Parent, SystemYearMonthGroupMonthList).Parent, SystemYearMonthGroup)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemYearMonthGroupMonthIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Year Month Group Month")
        Else
          Return String.Format("Blank {0}", "System Year Month Group Month")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemYearMonthGroupMonth() method.

    End Sub

    Public Shared Function NewSystemYearMonthGroupMonth() As SystemYearMonthGroupMonth

      Return DataPortal.CreateChild(Of SystemYearMonthGroupMonth)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemYearMonthGroupMonth(dr As SafeDataReader) As SystemYearMonthGroupMonth

      Dim s As New SystemYearMonthGroupMonth()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemYearMonthGroupMonthIDProperty, .GetInt32(0))
          LoadProperty(SystemYearMonthGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(MonthStartDateProperty, .GetValue(2))
          LoadProperty(MonthEndDateProperty, .GetValue(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemYearMonthGroupMonthIDProperty)

      cm.Parameters.AddWithValue("@SystemYearMonthGroupID", Me.GetParent().SystemYearMonthGroupID)
      cm.Parameters.AddWithValue("@MonthStartDate", MonthStartDate)
      cm.Parameters.AddWithValue("@MonthEndDate", MonthEndDate)
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemYearMonthGroupMonthIDProperty, cm.Parameters("@SystemYearMonthGroupMonthID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemYearMonthGroupMonthID", GetProperty(SystemYearMonthGroupMonthIDProperty))
    End Sub

#End Region

  End Class

End Namespace