﻿' Generated 27 Apr 2016 11:07 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemYearTeamNumberMonthGroup
    Inherits OBBusinessBase(Of SystemYearTeamNumberMonthGroup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="IsExpanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemYearTeamNumberMonthGroupBO.SystemYearTeamNumberMonthGroupBOToString(self)")

    Public Shared SystemTeamNumberMonthGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTeamNumberMonthGroupID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public Property SystemTeamNumberMonthGroupID() As Integer
      Get
        Return GetProperty(SystemTeamNumberMonthGroupIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemTeamNumberMonthGroupIDProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamNumberIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTeamNumberID, "System Team Number")
    ''' <summary>
    ''' Gets the System Team Number value
    ''' </summary>
    Public Property SystemTeamNumberID() As Integer
      Get
        Return GetProperty(SystemTeamNumberIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(SystemTeamNumberIDProperty, value)
      End Set
    End Property

    Public Shared SystemYearMonthGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemYearMonthGroupID, "System Year Month Group")
    ''' <summary>
    ''' Gets and sets the System Year Month Group value
    ''' </summary>
    <Display(Name:="System Year Month Group", Description:=""),
    Required(ErrorMessage:="System Year Month Group required")>
    Public Property SystemYearMonthGroupID() As Integer
      Get
        Return GetProperty(SystemYearMonthGroupIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemYearMonthGroupIDProperty, Value)
      End Set
    End Property

    Public Shared RequiredHoursGroupProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RequiredHoursGroup, "Required Hours")
    ''' <summary>
    ''' Gets and sets the Required Hours Group value
    ''' </summary>
    <Display(Name:="Required Hours", Description:=""),
    Required(ErrorMessage:="Required Hours required")>
    Public Property RequiredHoursGroup() As Decimal
      Get
        Return GetProperty(RequiredHoursGroupProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RequiredHoursGroupProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared MonthGroupStartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MonthGroupStartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Month Group Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date required")>
    Public Property MonthGroupStartDate As Date
      Get
        Return GetProperty(MonthGroupStartDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(MonthGroupStartDateProperty, Value)
      End Set
    End Property

    Public Shared MonthGroupEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MonthGroupEndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the Month Group End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    Required(ErrorMessage:="End Date required")>
    Public Property MonthGroupEndDate As Date
      Get
        Return GetProperty(MonthGroupEndDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(MonthGroupEndDateProperty, Value)
      End Set
    End Property

    Public Shared RequiredHoursHRTotalProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RequiredHoursHRTotal, "Required Hours", CDec(0))
    ''' <summary>
    ''' Gets and sets the System Team Number Name value
    ''' </summary>
    <Display(Name:="Required Hours", Description:="")>
    Public Property RequiredHoursHRTotal() As Decimal
      Get
        Return GetProperty(RequiredHoursHRTotalProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RequiredHoursHRTotalProperty, Value)
      End Set
    End Property

    Public Shared TimesheetHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TimesheetHours, "Timesheet Hours", CDec(0))
    ''' <summary>
    ''' Gets and sets the System Team Number Name value
    ''' </summary>
    <Display(Name:="Timesheet Hours", Description:="")>
    Public Property TimesheetHours() As Decimal
      Get
        Return GetProperty(TimesheetHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TimesheetHoursProperty, Value)
      End Set
    End Property

    Public Shared TimesheetsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Timesheets, "Timesheets", 0)
    ''' <summary>
    ''' Gets and sets the System Team Number Name value
    ''' </summary>
    <Display(Name:="Timesheets", Description:="")>
    Public Property Timesheets() As Integer
      Get
        Return GetProperty(TimesheetsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TimesheetsProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SystemYearTeamNumberMonthGroupMonthListProperty As PropertyInfo(Of SystemYearTeamNumberMonthGroupMonthList) = RegisterProperty(Of SystemYearTeamNumberMonthGroupMonthList)(Function(c) c.SystemYearTeamNumberMonthGroupMonthList, "System Year Team Number Month Group Month List")

    Public ReadOnly Property SystemYearTeamNumberMonthGroupMonthList() As SystemYearTeamNumberMonthGroupMonthList
      Get
        If GetProperty(SystemYearTeamNumberMonthGroupMonthListProperty) Is Nothing Then
          LoadProperty(SystemYearTeamNumberMonthGroupMonthListProperty, Maintenance.SystemManagement.SystemYearTeamNumberMonthGroupMonthList.NewSystemYearTeamNumberMonthGroupMonthList())
        End If
        Return GetProperty(SystemYearTeamNumberMonthGroupMonthListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemYearTeamNumber

      Return CType(CType(Me.Parent, SystemYearTeamNumberMonthGroupList).Parent, SystemYearTeamNumber)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemTeamNumberMonthGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SystemTeamNumberMonthGroupID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Year Team Number Month Group")
        Else
          Return String.Format("Blank {0}", "System Year Team Number Month Group")
        End If
      Else
        Return Me.SystemTeamNumberMonthGroupID.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(RequiredHoursGroupProperty)
        .ServerRuleFunction = AddressOf RequiredHoursGroupValid
        .JavascriptRuleFunctionName = "SystemYearTeamNumberMonthGroupBO.RequiredHoursGroupValid"
      End With

    End Sub

    Public Shared Function RequiredHoursGroupValid(MonthGroup As SystemYearTeamNumberMonthGroup) As String

      If Not MonthGroup.RequiredHoursGroup > 0 Then
        Return "Required Hours must be greater than 0"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemYearTeamNumberMonthGroup() method.

    End Sub

    Public Shared Function NewSystemYearTeamNumberMonthGroup() As SystemYearTeamNumberMonthGroup

      Return DataPortal.CreateChild(Of SystemYearTeamNumberMonthGroup)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemYearTeamNumberMonthGroup(dr As SafeDataReader) As SystemYearTeamNumberMonthGroup

      Dim s As New SystemYearTeamNumberMonthGroup()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemTeamNumberMonthGroupIDProperty, .GetInt32(0))
          LoadProperty(SystemTeamNumberIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SystemYearMonthGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(RequiredHoursGroupProperty, .GetDecimal(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(7))
          LoadProperty(MonthGroupStartDateProperty, .GetDateTime(8))
          LoadProperty(MonthGroupEndDateProperty, .GetDateTime(9))
          LoadProperty(RequiredHoursHRTotalProperty, .GetDecimal(10))
          LoadProperty(TimesheetHoursProperty, .GetDecimal(11))
          LoadProperty(TimesheetsProperty, .GetInt32(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemTeamNumberMonthGroupIDProperty)

      cm.Parameters.AddWithValue("@SystemTeamNumberID", Me.GetParent().SystemTeamNumberID)
      cm.Parameters.AddWithValue("@SystemYearMonthGroupID", GetProperty(SystemYearMonthGroupIDProperty))
      cm.Parameters.AddWithValue("@RequiredHoursGroup", GetProperty(RequiredHoursGroupProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemTeamNumberMonthGroupIDProperty, cm.Parameters("@SystemTeamNumberMonthGroupID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(SystemYearTeamNumberMonthGroupMonthListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemTeamNumberMonthGroupID", GetProperty(SystemTeamNumberMonthGroupIDProperty))
    End Sub

#End Region

  End Class

End Namespace