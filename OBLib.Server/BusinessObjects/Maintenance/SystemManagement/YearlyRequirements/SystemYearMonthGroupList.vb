﻿' Generated 25 Apr 2016 14:23 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemYearMonthGroupList
    Inherits OBBusinessListBase(Of SystemYearMonthGroupList, SystemYearMonthGroup)

#Region " Business Methods "

    Public Function GetItem(SystemYearMonthGroupID As Integer) As SystemYearMonthGroup

      For Each child As SystemYearMonthGroup In Me
        If child.SystemYearMonthGroupID = SystemYearMonthGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Year Month Groups"

    End Function

    Public Function GetSystemYearMonthGroupMonth(SystemYearMonthGroupMonthID As Integer) As SystemYearMonthGroupMonth

      Dim obj As SystemYearMonthGroupMonth = Nothing
      For Each parent As SystemYearMonthGroup In Me
        obj = parent.SystemYearMonthGroupMonthList.GetItem(SystemYearMonthGroupMonthID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSystemYearMonthGroupList() As SystemYearMonthGroupList

      Return New SystemYearMonthGroupList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace