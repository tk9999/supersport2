﻿' Generated 27 Apr 2016 11:07 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemYearTeamNumberList
    Inherits OBBusinessListBase(Of SystemYearTeamNumberList, SystemYearTeamNumber)

#Region " Business Methods "

    Public Function GetItem(SystemTeamNumberID As Integer) As SystemYearTeamNumber

      For Each child As SystemYearTeamNumber In Me
        If child.SystemTeamNumberID = SystemTeamNumberID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetSystemYearTeamNumberMonthGroup(SystemTeamNumberMonthGroupID As Integer) As SystemYearTeamNumberMonthGroup

      Dim obj As SystemYearTeamNumberMonthGroup = Nothing
      For Each parent As SystemYearTeamNumber In Me
        obj = parent.SystemYearTeamNumberMonthGroupList.GetItem(SystemTeamNumberMonthGroupID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewSystemYearTeamNumberList() As SystemYearTeamNumberList

      Return New SystemYearTeamNumberList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSystemYearTeamNumberList() As SystemYearTeamNumberList

      Return DataPortal.Fetch(Of SystemYearTeamNumberList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemYearTeamNumber.GetSystemYearTeamNumber(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SystemYearTeamNumber = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemTeamNumberID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SystemYearTeamNumberMonthGroupList.RaiseListChangedEvents = False
          parent.SystemYearTeamNumberMonthGroupList.Add(SystemYearTeamNumberMonthGroup.GetSystemYearTeamNumberMonthGroup(sdr))
          parent.SystemYearTeamNumberMonthGroupList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As SystemYearTeamNumberMonthGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.SystemTeamNumberMonthGroupID <> sdr.GetInt32(1) Then
            parentChild = Me.GetSystemYearTeamNumberMonthGroup(sdr.GetInt32(1))
          End If
          parentChild.SystemYearTeamNumberMonthGroupMonthList.RaiseListChangedEvents = False
          parentChild.SystemYearTeamNumberMonthGroupMonthList.Add(SystemYearTeamNumberMonthGroupMonth.GetSystemYearTeamNumberMonthGroupMonth(sdr))
          parentChild.SystemYearTeamNumberMonthGroupMonthList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As SystemYearTeamNumber In Me
        child.CheckRules()
        For Each SystemYearTeamNumberMonthGroup As SystemYearTeamNumberMonthGroup In child.SystemYearTeamNumberMonthGroupList
          SystemYearTeamNumberMonthGroup.CheckRules()
          For Each SystemYearTeamNumberMonthGroupMonth As SystemYearTeamNumberMonthGroupMonth In SystemYearTeamNumberMonthGroup.SystemYearTeamNumberMonthGroupMonthList
            SystemYearTeamNumberMonthGroupMonth.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemYearTeamNumberList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace