﻿' Generated 26 Aug 2016 12:14 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemYearMonthGroupMonthList
    Inherits OBReadOnlyListBase(Of ROSystemYearMonthGroupMonthList, ROSystemYearMonthGroupMonth)

#Region " Business Methods "

    Public Function GetItem(SystemYearMonthGroupMonthID As Integer) As ROSystemYearMonthGroupMonth

      For Each child As ROSystemYearMonthGroupMonth In Me
        If child.SystemYearMonthGroupMonthID = SystemYearMonthGroupMonthID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO System Year Month Group Months"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROSystemYearMonthGroupMonthList() As ROSystemYearMonthGroupMonthList

      Return New ROSystemYearMonthGroupMonthList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemYearMonthGroupMonthList() As ROSystemYearMonthGroupMonthList

      Return DataPortal.Fetch(Of ROSystemYearMonthGroupMonthList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemYearMonthGroupMonth.GetROSystemYearMonthGroupMonth(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemYearMonthGroupMonthList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace