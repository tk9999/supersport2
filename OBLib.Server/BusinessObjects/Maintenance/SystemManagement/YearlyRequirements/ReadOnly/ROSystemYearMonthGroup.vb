﻿' Generated 26 Aug 2016 12:14 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemYearMonthGroup
    Inherits OBReadOnlyBase(Of ROSystemYearMonthGroup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSystemYearMonthGroupBO.ROSystemYearMonthGroupBOToString(self)")

    Public Shared SystemYearMonthGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemYearMonthGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemYearMonthGroupID() As Integer
      Get
        Return GetProperty(SystemYearMonthGroupIDProperty)
      End Get
    End Property

    Public Shared SystemYearIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemYearID, "System Year", Nothing)
    ''' <summary>
    ''' Gets the System Year value
    ''' </summary>
    <Display(Name:="System Year", Description:="")>
    Public ReadOnly Property SystemYearID() As Integer?
      Get
        Return GetProperty(SystemYearIDProperty)
      End Get
    End Property

    Public Shared GroupNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GroupName, "Group Name", "")
    ''' <summary>
    ''' Gets the Group Name value
    ''' </summary>
    <Display(Name:="Group Name", Description:="")>
    Public ReadOnly Property GroupName() As String
      Get
        Return GetProperty(GroupNameProperty)
      End Get
    End Property

    Public Shared MonthGroupStartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MonthGroupStartDate, "Month Group Start Date")
    ''' <summary>
    ''' Gets the Month Group Start Date value
    ''' </summary>
    <Display(Name:="Month Group Start Date", Description:="")>
    Public ReadOnly Property MonthGroupStartDate As Date
      Get
        Return GetProperty(MonthGroupStartDateProperty)
      End Get
    End Property

    Public Shared MonthGroupEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MonthGroupEndDate, "Month Group End Date")
    ''' <summary>
    ''' Gets the Month Group End Date value
    ''' </summary>
    <Display(Name:="Month Group End Date", Description:="")>
    Public ReadOnly Property MonthGroupEndDate As Date
      Get
        Return GetProperty(MonthGroupEndDateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemYearMonthGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.GroupName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSystemYearMonthGroup(dr As SafeDataReader) As ROSystemYearMonthGroup

      Dim r As New ROSystemYearMonthGroup()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemYearMonthGroupIDProperty, .GetInt32(0))
        LoadProperty(SystemYearIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(GroupNameProperty, .GetString(2))
        LoadProperty(MonthGroupStartDateProperty, .GetValue(3))
        LoadProperty(MonthGroupEndDateProperty, .GetValue(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
      End With

    End Sub

#End Region

  End Class

End Namespace