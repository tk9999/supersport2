﻿' Generated 26 Aug 2016 12:14 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemYearMonthGroupList
    Inherits OBReadOnlyListBase(Of ROSystemYearMonthGroupList, ROSystemYearMonthGroup)

#Region " Business Methods "

    Public Function GetItem(SystemYearMonthGroupID As Integer) As ROSystemYearMonthGroup

      For Each child As ROSystemYearMonthGroup In Me
        If child.SystemYearMonthGroupID = SystemYearMonthGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Year Month Groups"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField()>
      Public Property GroupName As String

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROSystemYearMonthGroupList() As ROSystemYearMonthGroupList

      Return New ROSystemYearMonthGroupList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemYearMonthGroupList() As ROSystemYearMonthGroupList

      Return DataPortal.Fetch(Of ROSystemYearMonthGroupList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemYearMonthGroup.GetROSystemYearMonthGroup(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemYearMonthGroupList"
            cm.Parameters.AddWithValue("@GroupName", Strings.MakeEmptyDBNull(crit.GroupName))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace