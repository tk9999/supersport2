﻿' Generated 26 Aug 2016 12:14 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemYearMonthGroupMonth
    Inherits OBReadOnlyBase(Of ROSystemYearMonthGroupMonth)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSystemYearMonthGroupMonthBO.ROSystemYearMonthGroupMonthBOToString(self)")

    Public Shared SystemYearMonthGroupMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemYearMonthGroupMonthID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SystemYearMonthGroupMonthID() As Integer
      Get
        Return GetProperty(SystemYearMonthGroupMonthIDProperty)
      End Get
    End Property

    Public Shared SystemYearMonthGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemYearMonthGroupID, "System Year Month Group", Nothing)
    ''' <summary>
    ''' Gets the System Year Month Group value
    ''' </summary>
    <Display(Name:="System Year Month Group", Description:="")>
  Public ReadOnly Property SystemYearMonthGroupID() As Integer?
      Get
        Return GetProperty(SystemYearMonthGroupIDProperty)
      End Get
    End Property

    Public Shared MonthStartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MonthStartDate, "Month Start Date")
    ''' <summary>
    ''' Gets the Month Start Date value
    ''' </summary>
    <Display(Name:="Month Start Date", Description:="")>
  Public ReadOnly Property MonthStartDate As Date
      Get
        Return GetProperty(MonthStartDateProperty)
      End Get
    End Property

    Public Shared MonthEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MonthEndDate, "Month End Date")
    ''' <summary>
    ''' Gets the Month End Date value
    ''' </summary>
    <Display(Name:="Month End Date", Description:="")>
  Public ReadOnly Property MonthEndDate As Date
      Get
        Return GetProperty(MonthEndDateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemYearMonthGroupMonthIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSystemYearMonthGroupMonth(dr As SafeDataReader) As ROSystemYearMonthGroupMonth

      Dim r As New ROSystemYearMonthGroupMonth()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemYearMonthGroupMonthIDProperty, .GetInt32(0))
        LoadProperty(SystemYearMonthGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(MonthStartDateProperty, .GetValue(2))
        LoadProperty(MonthEndDateProperty, .GetValue(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End Region

  End Class

End Namespace