﻿' Generated 27 Apr 2016 11:07 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemYearTeamNumber
    Inherits OBBusinessBase(Of SystemYearTeamNumber)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="IsExpanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemYearTeamNumberBO.SystemYearTeamNumberBOToString(self)")

    Public Shared SystemTeamNumberIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTeamNumberID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public Property SystemTeamNumberID() As Integer
      Get
        Return GetProperty(SystemTeamNumberIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemTeamNumberIDProperty, Value)
      End Set
    End Property

    Public Shared SystemYearIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemYearID, "SystemYear")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="SystemYear", Description:=""),
    Required(ErrorMessage:="SystemYear required")>
    Public Property SystemYearID() As Integer
      Get
        Return GetProperty(SystemYearIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemYearIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required(ErrorMessage:="System required")>
  Public Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTeamNumber, "System Team Number")
    ''' <summary>
    ''' Gets and sets the System Team Number value
    ''' </summary>
    <Display(Name:="System Team Number", Description:=""),
    Required(ErrorMessage:="System Team Number required")>
  Public Property SystemTeamNumber() As Integer
      Get
        Return GetProperty(SystemTeamNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemTeamNumberProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamNumberNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemTeamNumberName, "System Team Number Name")
    ''' <summary>
    ''' Gets and sets the System Team Number Name value
    ''' </summary>
    <Display(Name:="System Team Number Name", Description:="")>
  Public Property SystemTeamNumberName() As String
      Get
        Return GetProperty(SystemTeamNumberNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemTeamNumberNameProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared RequiredHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RequiredHours, "Required Hours", CDec(0))
    ''' <summary>
    ''' Gets and sets the System Team Number Name value
    ''' </summary>
    <Display(Name:="Required Hours", Description:="")>
    Public Property RequiredHours() As Decimal
      Get
        Return GetProperty(RequiredHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RequiredHoursProperty, Value)
      End Set
    End Property

    Public Shared TimesheetHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TimesheetHours, "Timesheet Hours", CDec(0))
    ''' <summary>
    ''' Gets and sets the System Team Number Name value
    ''' </summary>
    <Display(Name:="Timesheet Hours", Description:="")>
    Public Property TimesheetHours() As Decimal
      Get
        Return GetProperty(TimesheetHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TimesheetHoursProperty, Value)
      End Set
    End Property

    Public Shared TimesheetsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Timesheets, "Timesheets", 0)
    ''' <summary>
    ''' Gets and sets the System Team Number Name value
    ''' </summary>
    <Display(Name:="Timesheets", Description:="")>
    Public Property Timesheets() As Integer
      Get
        Return GetProperty(TimesheetsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TimesheetsProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SystemYearTeamNumberMonthGroupListProperty As PropertyInfo(Of SystemYearTeamNumberMonthGroupList) = RegisterProperty(Of SystemYearTeamNumberMonthGroupList)(Function(c) c.SystemYearTeamNumberMonthGroupList, "System Year Team Number Month Group List")

    Public ReadOnly Property SystemYearTeamNumberMonthGroupList() As SystemYearTeamNumberMonthGroupList
      Get
        If GetProperty(SystemYearTeamNumberMonthGroupListProperty) Is Nothing Then
          LoadProperty(SystemYearTeamNumberMonthGroupListProperty, Maintenance.SystemManagement.SystemYearTeamNumberMonthGroupList.NewSystemYearTeamNumberMonthGroupList())
        End If
        Return GetProperty(SystemYearTeamNumberMonthGroupListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemTeamNumberIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SystemTeamNumberName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Year Team Number")
        Else
          Return String.Format("Blank {0}", "System Year Team Number")
        End If
      Else
        Return Me.SystemTeamNumberName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemYearTeamNumber() method.

    End Sub

    Public Shared Function NewSystemYearTeamNumber() As SystemYearTeamNumber

      Return DataPortal.CreateChild(Of SystemYearTeamNumber)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemYearTeamNumber(dr As SafeDataReader) As SystemYearTeamNumber

      Dim s As New SystemYearTeamNumber()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemTeamNumberIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SystemTeamNumberProperty, .GetInt32(2))
          LoadProperty(SystemTeamNumberNameProperty, .GetString(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(7))
          LoadProperty(SystemYearIDProperty, .GetInt32(8))
          LoadProperty(RequiredHoursProperty, .GetDecimal(9))
          LoadProperty(TimesheetHoursProperty, .GetDecimal(10))
          LoadProperty(TimesheetsProperty, .GetInt32(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      'AddPrimaryKeyParam(cm, SystemTeamNumberIDProperty)

      'cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      'cm.Parameters.AddWithValue("@SystemTeamNumber", GetProperty(SystemTeamNumberProperty))
      'cm.Parameters.AddWithValue("@SystemTeamNumberName", GetProperty(SystemTeamNumberNameProperty))
      'cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      Return Sub()
               'Post Save
               'If Me.IsNew Then
               'LoadProperty(SystemTeamNumberIDProperty, cm.Parameters("@SystemTeamNumberID").Value)
               'End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(SystemYearTeamNumberMonthGroupListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemTeamNumberID", GetProperty(SystemTeamNumberIDProperty))
    End Sub

#End Region

  End Class

End Namespace