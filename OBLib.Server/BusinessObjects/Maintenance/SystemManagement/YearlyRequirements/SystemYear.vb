﻿' Generated 25 Apr 2016 14:23 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemYear
    Inherits OBBusinessBase(Of SystemYear)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="IsExpanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemYearBO.SystemYearBOToString(self)")

    Public Shared SystemYearIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemYearID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property SystemYearID() As Integer
      Get
        Return GetProperty(SystemYearIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(SystemYearIDProperty, value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required(ErrorMessage:="System required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared YearStartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.YearStartDate, "Year Start Date")
    ''' <summary>
    ''' Gets and sets the Year Start Date value
    ''' </summary>
    <Display(Name:="Year Start Date", Description:=""),
    Required(ErrorMessage:="Year Start Date required")>
    Public Property YearStartDate As Date
      Get
        Return GetProperty(YearStartDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(YearStartDateProperty, Value)
      End Set
    End Property

    Public Shared YearEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.YearEndDate, "Year End Date")
    ''' <summary>
    ''' Gets and sets the Year End Date value
    ''' </summary>
    <Display(Name:="Year End Date", Description:=""),
    Required(ErrorMessage:="Year End Date required")>
    Public Property YearEndDate As Date
      Get
        Return GetProperty(YearEndDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(YearEndDateProperty, Value)
      End Set
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared SystemYearMonthGroupListProperty As PropertyInfo(Of SystemYearMonthGroupList) = RegisterProperty(Of SystemYearMonthGroupList)(Function(c) c.SystemYearMonthGroupList, "System Year Month Group List")

    Public ReadOnly Property SystemYearMonthGroupList() As SystemYearMonthGroupList
      Get
        If GetProperty(SystemYearMonthGroupListProperty) Is Nothing Then
          LoadProperty(SystemYearMonthGroupListProperty, Maintenance.SystemManagement.SystemYearMonthGroupList.NewSystemYearMonthGroupList())
        End If
        Return GetProperty(SystemYearMonthGroupListProperty)
      End Get
    End Property

    Public Shared SystemYearTeamNumberListProperty As PropertyInfo(Of SystemYearTeamNumberList) = RegisterProperty(Of SystemYearTeamNumberList)(Function(c) c.SystemYearTeamNumberList, "System Year Team Number List")

    Public ReadOnly Property SystemYearTeamNumberList() As SystemYearTeamNumberList
      Get
        If GetProperty(SystemYearTeamNumberListProperty) Is Nothing Then
          LoadProperty(SystemYearTeamNumberListProperty, Maintenance.SystemManagement.SystemYearTeamNumberList.NewSystemYearTeamNumberList())
        End If
        Return GetProperty(SystemYearTeamNumberListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemYearIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ModifiedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Year")
        Else
          Return String.Format("Blank {0}", "System Year")
        End If
      Else
        Return Me.ModifiedDateTime.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"SystemYearMonthGroups"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemYear() method.

    End Sub

    Public Shared Function NewSystemYear() As SystemYear

      Return DataPortal.CreateChild(Of SystemYear)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemYear(dr As SafeDataReader) As SystemYear

      Dim s As New SystemYear()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemYearIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(YearStartDateProperty, .GetValue(2))
          LoadProperty(YearEndDateProperty, .GetValue(3))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemYearIDProperty)

      cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      cm.Parameters.AddWithValue("@YearStartDate", YearStartDate)
      cm.Parameters.AddWithValue("@YearEndDate", YearEndDate)
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemYearIDProperty, cm.Parameters("@SystemYearID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(SystemYearMonthGroupListProperty))
      UpdateChild(GetProperty(SystemYearTeamNumberListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemYearID", GetProperty(SystemYearIDProperty))
    End Sub

#End Region

  End Class

End Namespace