﻿' Generated 25 Apr 2016 14:23 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemYearMonthGroup
    Inherits OBBusinessBase(Of SystemYearMonthGroup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="IsExpanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemYearMonthGroupBO.SystemYearMonthGroupBOToString(self)")

    Public Shared SystemYearMonthGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemYearMonthGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property SystemYearMonthGroupID() As Integer
      Get
        Return GetProperty(SystemYearMonthGroupIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(SystemYearMonthGroupIDProperty, value)
      End Set
    End Property

    Public Shared SystemYearIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemYearID, "System Year", Nothing)
    ''' <summary>
    ''' Gets the System Year value
    ''' </summary>
    Public Property SystemYearID() As Integer?
      Get
        Return GetProperty(SystemYearIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(SystemYearIDProperty, value)
      End Set
    End Property

    Public Shared MonthGroupStartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MonthGroupStartDate, "Group Start Date")
    ''' <summary>
    ''' Gets and sets the Month Group Start Date value
    ''' </summary>
    <Display(Name:="Group Start Date", Description:=""),
    Required(ErrorMessage:="Group Start Date required")>
    Public Property MonthGroupStartDate As Date
      Get
        Return GetProperty(MonthGroupStartDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(MonthGroupStartDateProperty, Value)
      End Set
    End Property

    Public Shared MonthGroupEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MonthGroupEndDate, "Group End Date")
    ''' <summary>
    ''' Gets and sets the Month Group End Date value
    ''' </summary>
    <Display(Name:="Group End Date", Description:=""),
    Required(ErrorMessage:="Group End Date required")>
    Public Property MonthGroupEndDate As Date
      Get
        Return GetProperty(MonthGroupEndDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(MonthGroupEndDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared GroupNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GroupName, "Group Name", "")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Group Name", Description:=""),
    Required(AllowEmptyStrings:=True, ErrorMessage:="Group Name is required")>
    Public Property GroupName() As String
      Get
        Return GetProperty(GroupNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GroupNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SystemYearMonthGroupMonthListProperty As PropertyInfo(Of SystemYearMonthGroupMonthList) = RegisterProperty(Of SystemYearMonthGroupMonthList)(Function(c) c.SystemYearMonthGroupMonthList, "System Year Month Group Month List")
    Public ReadOnly Property SystemYearMonthGroupMonthList() As SystemYearMonthGroupMonthList
      Get
        If GetProperty(SystemYearMonthGroupMonthListProperty) Is Nothing Then
          LoadProperty(SystemYearMonthGroupMonthListProperty, Maintenance.SystemManagement.SystemYearMonthGroupMonthList.NewSystemYearMonthGroupMonthList())
        End If
        Return GetProperty(SystemYearMonthGroupMonthListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemYear

      Return CType(CType(Me.Parent, SystemYearMonthGroupList).Parent, SystemYear)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemYearMonthGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Year Month Group")
        Else
          Return String.Format("Blank {0}", "System Year Month Group")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"SystemYearMonthGroupMonths"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemYearMonthGroup() method.

    End Sub

    Public Shared Function NewSystemYearMonthGroup() As SystemYearMonthGroup

      Return DataPortal.CreateChild(Of SystemYearMonthGroup)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemYearMonthGroup(dr As SafeDataReader) As SystemYearMonthGroup

      Dim s As New SystemYearMonthGroup()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemYearMonthGroupIDProperty, .GetInt32(0))
          LoadProperty(SystemYearIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(MonthGroupStartDateProperty, .GetValue(2))
          LoadProperty(MonthGroupEndDateProperty, .GetValue(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(GroupNameProperty, .GetString(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemYearMonthGroupIDProperty)

      cm.Parameters.AddWithValue("@SystemYearID", Me.GetParent().SystemYearID)
      cm.Parameters.AddWithValue("@MonthGroupStartDate", MonthGroupStartDate)
      cm.Parameters.AddWithValue("@MonthGroupEndDate", MonthGroupEndDate)
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@GroupName", GetProperty(GroupNameProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemYearMonthGroupIDProperty, cm.Parameters("@SystemYearMonthGroupID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(SystemYearMonthGroupMonthListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemYearMonthGroupID", GetProperty(SystemYearMonthGroupIDProperty))
    End Sub

#End Region

  End Class

End Namespace