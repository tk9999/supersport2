﻿' Generated 25 Apr 2016 14:23 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemYearMonthGroupMonthList
    Inherits OBBusinessListBase(Of SystemYearMonthGroupMonthList, SystemYearMonthGroupMonth)

#Region " Business Methods "

    Public Function GetItem(SystemYearMonthGroupMonthID As Integer) As SystemYearMonthGroupMonth

      For Each child As SystemYearMonthGroupMonth In Me
        If child.SystemYearMonthGroupMonthID = SystemYearMonthGroupMonthID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Year Month Group Months"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSystemYearMonthGroupMonthList() As SystemYearMonthGroupMonthList

      Return New SystemYearMonthGroupMonthList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace