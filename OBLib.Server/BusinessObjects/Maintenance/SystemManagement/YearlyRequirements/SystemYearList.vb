﻿' Generated 25 Apr 2016 14:23 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemYearList
    Inherits OBBusinessListBase(Of SystemYearList, SystemYear)

#Region " Business Methods "

    Public Function GetItem(SystemYearID As Integer) As SystemYear

      For Each child As SystemYear In Me
        If child.SystemYearID = SystemYearID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Years"

    End Function

    Public Function GetSystemYearMonthGroup(SystemYearMonthGroupID As Integer) As SystemYearMonthGroup

      Dim obj As SystemYearMonthGroup = Nothing
      For Each parent As SystemYear In Me
        obj = parent.SystemYearMonthGroupList.GetItem(SystemYearMonthGroupID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemYearTeamNumber(SystemTeamNumberID As Integer) As SystemYearTeamNumber

      For Each child As SystemYear In Me
        For Each child2 As SystemYearTeamNumber In child.SystemYearTeamNumberList
          If child2.SystemTeamNumberID = SystemTeamNumberID Then
            Return child2
          End If
        Next
      Next
      Return Nothing

    End Function

    Public Function GetSystemYearTeamNumberMonthGroup(SystemTeamNumberMonthGroupID As Integer) As SystemYearTeamNumberMonthGroup

      Dim obj As SystemYearTeamNumberMonthGroup = Nothing
      For Each child As SystemYear In Me
        For Each child2 As SystemYearTeamNumber In child.SystemYearTeamNumberList
          obj = child2.SystemYearTeamNumberMonthGroupList.GetItem(SystemTeamNumberMonthGroupID)
          If obj IsNot Nothing Then
            Return obj
          End If
        Next
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer? = Nothing
      Public Property SystemYearID As Integer? = Nothing

      Public Sub New(SystemYearID As Integer?)
        Me.SystemYearID = SystemYearID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewSystemYearList() As SystemYearList

      Return New SystemYearList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSystemYearList() As SystemYearList

      Return DataPortal.Fetch(Of SystemYearList)(New Criteria())

    End Function

    Public Shared Function GetSystemYearList(SystemYearID As Integer?) As SystemYearList

      Return DataPortal.Fetch(Of SystemYearList)(New Criteria(SystemYearID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemYear.GetSystemYear(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SystemYear = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemYearID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SystemYearMonthGroupList.RaiseListChangedEvents = False
          parent.SystemYearMonthGroupList.Add(SystemYearMonthGroup.GetSystemYearMonthGroup(sdr))
          parent.SystemYearMonthGroupList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As SystemYearMonthGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.SystemYearMonthGroupID <> sdr.GetInt32(1) Then
            parentChild = Me.GetSystemYearMonthGroup(sdr.GetInt32(1))
          End If
          parentChild.SystemYearMonthGroupMonthList.RaiseListChangedEvents = False
          parentChild.SystemYearMonthGroupMonthList.Add(SystemYearMonthGroupMonth.GetSystemYearMonthGroupMonth(sdr))
          parentChild.SystemYearMonthGroupMonthList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemYearID <> sdr.GetInt32(8) Then
            parent = Me.GetItem(sdr.GetInt32(8))
          End If
          parent.SystemYearTeamNumberList.RaiseListChangedEvents = False
          parent.SystemYearTeamNumberList.Add(SystemYearTeamNumber.GetSystemYearTeamNumber(sdr))
          parent.SystemYearTeamNumberList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentTeamNumber As SystemYearTeamNumber = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentTeamNumber Is Nothing OrElse parentTeamNumber.SystemTeamNumberID <> sdr.GetInt32(1) Then
            parentTeamNumber = Me.GetSystemYearTeamNumber(sdr.GetInt32(1))
          End If
          parentTeamNumber.SystemYearTeamNumberMonthGroupList.RaiseListChangedEvents = False
          parentTeamNumber.SystemYearTeamNumberMonthGroupList.Add(SystemYearTeamNumberMonthGroup.GetSystemYearTeamNumberMonthGroup(sdr))
          parentTeamNumber.SystemYearTeamNumberMonthGroupList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentMonthGroup As SystemYearTeamNumberMonthGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentMonthGroup Is Nothing OrElse parentMonthGroup.SystemTeamNumberMonthGroupID <> sdr.GetInt32(1) Then
            parentMonthGroup = Me.GetSystemYearTeamNumberMonthGroup(sdr.GetInt32(1))
          End If
          parentMonthGroup.SystemYearTeamNumberMonthGroupMonthList.RaiseListChangedEvents = False
          parentMonthGroup.SystemYearTeamNumberMonthGroupMonthList.Add(SystemYearTeamNumberMonthGroupMonth.GetSystemYearTeamNumberMonthGroupMonth(sdr))
          parentMonthGroup.SystemYearTeamNumberMonthGroupMonthList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As SystemYear In Me
        child.CheckRules()
        For Each SystemYearMonthGroup As SystemYearMonthGroup In child.SystemYearMonthGroupList
          SystemYearMonthGroup.CheckRules()
          For Each SystemYearMonthGroupMonth As SystemYearMonthGroupMonth In SystemYearMonthGroup.SystemYearMonthGroupMonthList
            SystemYearMonthGroupMonth.CheckRules()
          Next
        Next
        For Each child2 As SystemYearTeamNumber In child.SystemYearTeamNumberList
          child2.CheckRules()
          For Each SystemYearTeamNumberMonthGroup As SystemYearTeamNumberMonthGroup In child2.SystemYearTeamNumberMonthGroupList
            SystemYearTeamNumberMonthGroup.CheckRules()
            For Each SystemYearTeamNumberMonthGroupMonth As SystemYearTeamNumberMonthGroupMonth In SystemYearTeamNumberMonthGroup.SystemYearTeamNumberMonthGroupMonthList
              SystemYearTeamNumberMonthGroupMonth.CheckRules()
            Next
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemYearList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@SystemYearID", NothingDBNull(crit.SystemYearID))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUser.UserID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace