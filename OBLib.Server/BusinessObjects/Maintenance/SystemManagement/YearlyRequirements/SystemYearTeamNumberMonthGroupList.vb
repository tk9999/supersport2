﻿' Generated 27 Apr 2016 11:07 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemYearTeamNumberMonthGroupList
    Inherits OBBusinessListBase(Of SystemYearTeamNumberMonthGroupList, SystemYearTeamNumberMonthGroup)

#Region " Business Methods "

    Public Function GetItem(SystemTeamNumberMonthGroupID As Integer) As SystemYearTeamNumberMonthGroup

      For Each child As SystemYearTeamNumberMonthGroup In Me
        If child.SystemTeamNumberMonthGroupID = SystemTeamNumberMonthGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetSystemYearTeamNumberMonthGroupMonth(SystemTimesheetMonthID As Integer) As SystemYearTeamNumberMonthGroupMonth

      Dim obj As SystemYearTeamNumberMonthGroupMonth = Nothing
      For Each parent As SystemYearTeamNumberMonthGroup In Me
        obj = parent.SystemYearTeamNumberMonthGroupMonthList.GetItem(SystemTimesheetMonthID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSystemYearTeamNumberMonthGroupList() As SystemYearTeamNumberMonthGroupList

      Return New SystemYearTeamNumberMonthGroupList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace