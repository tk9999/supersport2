﻿' Generated 27 Apr 2016 11:07 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemYearTeamNumberMonthGroupMonthList
    Inherits OBBusinessListBase(Of SystemYearTeamNumberMonthGroupMonthList, SystemYearTeamNumberMonthGroupMonth)

#Region " Business Methods "

    Public Function GetItem(SystemTimesheetMonthID As Integer) As SystemYearTeamNumberMonthGroupMonth

      For Each child As SystemYearTeamNumberMonthGroupMonth In Me
        If child.SystemTimesheetMonthID = SystemTimesheetMonthID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSystemYearTeamNumberMonthGroupMonthList() As SystemYearTeamNumberMonthGroupMonthList

      Return New SystemYearTeamNumberMonthGroupMonthList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace