﻿' Generated 16 May 2016 07:55 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaStatus
    Inherits OBBusinessBase(Of SystemProductionAreaStatus)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemProductionAreaStatusBO.SystemProductionAreaStatusBOToString(self)")

    Public Shared SystemProductionAreaStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaStatusID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemProductionAreaStatusID() As Integer
      Get
        Return GetProperty(SystemProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaID, "System Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the System Production Area value
    ''' </summary>
    <Display(Name:="System Production Area", Description:=""),
    Required(ErrorMessage:="System Production Area required")>
    Public Property SystemProductionAreaID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "Production Area Status", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:=""),
    Required(ErrorMessage:="Production Area Status required"),
        Singular.DataAnnotations.DropDownWeb(GetType(ROProductionAreaStatusList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SystemProductionAreaStatusBO.setStatusCriteriaBeforeRefresh", PreFindJSFunction:="SystemProductionAreaStatusBO.triggerStatusAutoPopulate", AfterFetchJS:="SystemProductionAreaStatusBO.afterStatusRefreshAjax",
                OnItemSelectJSFunction:="SystemProductionAreaStatusBO.onStatusSelected", LookupMember:="ProductionAreaStatus", DisplayMember:="ProductionAreaStatus", ValueMember:="ProductionAreaStatusID",
                DropDownCssClass:="mcrc-dropdown", DropDownColumns:={"ProductionAreaStatus"}, UnselectedText:="None")>
    Public Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared CssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CssClass, "Css Class", "")
    ''' <summary>
    ''' Gets and sets the Css Class value
    ''' </summary>
    <Display(Name:="Css Class", Description:=""),
    StringLength(50, ErrorMessage:="Css Class cannot be more than 50 characters")>
    Public Property CssClass() As String
      Get
        Return GetProperty(CssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CssClassProperty, Value)
      End Set
    End Property

    Public Shared OrderNumProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNum, "Order", 99)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Order")>
    Public Property OrderNum() As Integer?
      Get
        Return GetProperty(OrderNumProperty)
      End Get
      Set(value As Integer?)
        SetProperty(OrderNumProperty, value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Modified Date Time", Description:=""), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "LoginName", "")
    ''' <summary>
    ''' Gets and sets the Discipline Name
    ''' </summary>
    <Display(Name:="Last Modified", Description:="Shows the last user to modify this record.")>
    Public Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LoginNameProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "ProductionArea Status", "")
    ''' <summary>
    ''' Gets the ProductionAreaStatus value
    ''' </summary>
    <Display(Name:="ProductionAreaStatus", Description:="")>
    Public Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaStatusProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemProductionArea

      Return CType(CType(Me.Parent, SystemProductionAreaStatusList).Parent, SystemProductionArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaStatusIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CssClass.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area Status")
        Else
          Return String.Format("Blank {0}", "System Production Area Status")
        End If
      Else
        Return Me.CssClass
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemProductionAreaStatus() method.

    End Sub

    Public Shared Function NewSystemProductionAreaStatus() As SystemProductionAreaStatus

      Return DataPortal.CreateChild(Of SystemProductionAreaStatus)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemProductionAreaStatus(dr As SafeDataReader) As SystemProductionAreaStatus

      Dim s As New SystemProductionAreaStatus()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaStatusIDProperty, .GetInt32(0))
          LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CssClassProperty, .GetString(3))
          LoadProperty(OrderNumProperty, .GetInt32(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(LoginNameProperty, .GetString(9))
          LoadProperty(ProductionAreaStatusProperty, .GetString(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemProductionAreaStatusIDProperty)

      cm.Parameters.AddWithValue("@SystemProductionAreaID", GetProperty(SystemProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@ProductionAreaStatusID", GetProperty(ProductionAreaStatusIDProperty))
      cm.Parameters.AddWithValue("@CssClass", GetProperty(CssClassProperty))
      cm.Parameters.AddWithValue("@OrderNum", GetProperty(OrderNumProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemProductionAreaStatusIDProperty, cm.Parameters("@SystemProductionAreaStatusID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemProductionAreaStatusID", GetProperty(SystemProductionAreaStatusIDProperty))
    End Sub

#End Region

  End Class

End Namespace