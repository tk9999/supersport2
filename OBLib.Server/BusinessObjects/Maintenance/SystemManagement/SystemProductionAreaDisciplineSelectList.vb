﻿' Generated 10 May 2016 15:20 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaDisciplineSelectList
    Inherits OBBusinessListBase(Of SystemProductionAreaDisciplineSelectList, SystemProductionAreaDisciplineSelect)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaDisciplineID As Integer) As SystemProductionAreaDisciplineSelect

      For Each child As SystemProductionAreaDisciplineSelect In Me
        If child.SystemProductionAreaDisciplineID = SystemProductionAreaDisciplineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetSystemProductionAreaDisciplinePositionTypeSelect(SystemProductionAreaDisciplinePositionTypeID As Integer) As SystemProductionAreaDisciplinePositionTypeSelect

      Dim obj As SystemProductionAreaDisciplinePositionTypeSelect = Nothing
      For Each parent As SystemProductionAreaDisciplineSelect In Me
        obj = parent.SystemProductionAreaDisciplinePositionTypeSelectList.GetItem(SystemProductionAreaDisciplinePositionTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewSystemProductionAreaDisciplineSelectList() As SystemProductionAreaDisciplineSelectList

      Return New SystemProductionAreaDisciplineSelectList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSystemProductionAreaDisciplineSelectList() As SystemProductionAreaDisciplineSelectList

      Return DataPortal.Fetch(Of SystemProductionAreaDisciplineSelectList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemProductionAreaDisciplineSelect.GetSystemProductionAreaDisciplineSelect(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SystemProductionAreaDisciplineSelect = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemProductionAreaDisciplineID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SystemProductionAreaDisciplinePositionTypeSelectList.RaiseListChangedEvents = False
          parent.SystemProductionAreaDisciplinePositionTypeSelectList.Add(SystemProductionAreaDisciplinePositionTypeSelect.GetSystemProductionAreaDisciplinePositionTypeSelect(sdr))
          parent.SystemProductionAreaDisciplinePositionTypeSelectList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As SystemProductionAreaDisciplineSelect In Me
        child.CheckRules()
        For Each SystemProductionAreaDisciplinePositionTypeSelect As SystemProductionAreaDisciplinePositionTypeSelect In child.SystemProductionAreaDisciplinePositionTypeSelectList
          SystemProductionAreaDisciplinePositionTypeSelect.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemProductionAreaDisciplineSelectList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace