﻿' Generated 10 May 2016 12:09 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaDisciplineList
    Inherits OBBusinessListBase(Of SystemProductionAreaDisciplineList, SystemProductionAreaDiscipline)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaDisciplineID As Integer) As SystemProductionAreaDiscipline

      For Each child As SystemProductionAreaDiscipline In Me
        If child.SystemProductionAreaDisciplineID = SystemProductionAreaDisciplineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Disciplines"

    End Function

    Public Function GetSystemProductionAreaDisciplinePositionType(SystemProductionAreaDisciplinePositionTypeID As Integer) As SystemProductionAreaDisciplinePositionType

      Dim obj As SystemProductionAreaDisciplinePositionType = Nothing
      For Each parent As SystemProductionAreaDiscipline In Me
        obj = parent.SystemProductionAreaDisciplinePositionTypeList.GetItem(SystemProductionAreaDisciplinePositionTypeID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Singular.DataAnnotations.PrimarySearchField>
      Public Property Discipline As String = Nothing
      Public Property SystemProductionAreaID As Integer? = Nothing


      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewSystemProductionAreaDisciplineList() As SystemProductionAreaDisciplineList

      Return New SystemProductionAreaDisciplineList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSystemProductionAreaDisciplineList() As SystemProductionAreaDisciplineList

      Return DataPortal.Fetch(Of SystemProductionAreaDisciplineList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemProductionAreaDiscipline.GetSystemProductionAreaDiscipline(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SystemProductionAreaDiscipline = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemProductionAreaDisciplineID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SystemProductionAreaDisciplinePositionTypeList.RaiseListChangedEvents = False
          parent.SystemProductionAreaDisciplinePositionTypeList.Add(SystemProductionAreaDisciplinePositionType.GetSystemProductionAreaDisciplinePositionType(sdr))
          parent.SystemProductionAreaDisciplinePositionTypeList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As SystemProductionAreaDiscipline In Me
        child.CheckRules()
        For Each SystemProductionAreaDisciplinePositionType As SystemProductionAreaDisciplinePositionType In child.SystemProductionAreaDisciplinePositionTypeList
          SystemProductionAreaDisciplinePositionType.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemProductionAreaDisciplineList"
            cm.Parameters.AddWithValue("@SystemProductionAreaID", NothingDBNull(crit.SystemProductionAreaID))
            cm.Parameters.AddWithValue("@Discipline", Singular.Strings.MakeEmptyDBNull(crit.Discipline))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace