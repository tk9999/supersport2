﻿' Generated 16 May 2016 07:55 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaStatusList
    Inherits OBBusinessListBase(Of SystemProductionAreaStatusList, SystemProductionAreaStatus)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaStatusID As Integer) As SystemProductionAreaStatus

      For Each child As SystemProductionAreaStatus In Me
        If child.SystemProductionAreaStatusID = SystemProductionAreaStatusID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Statuses"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewSystemProductionAreaStatusList() As SystemProductionAreaStatusList

      Return New SystemProductionAreaStatusList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSystemProductionAreaStatusList() As SystemProductionAreaStatusList

      Return DataPortal.Fetch(Of SystemProductionAreaStatusList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemProductionAreaStatus.GetSystemProductionAreaStatus(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemProductionAreaStatusList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace