﻿' Generated 16 May 2017 11:08 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemAreaDisciplineShiftRate
    Inherits OBBusinessBase(Of SystemAreaDisciplineShiftRate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemAreaDisciplineShiftRatesIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaDisciplineShiftRatesID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemAreaDisciplineShiftRatesID() As Integer
      Get
        Return GetProperty(SystemAreaDisciplineShiftRatesIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="The system the rate applies to"),
    Required(ErrorMessage:="System required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="The production area the rate applies to"),
    Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="The discipline the rate applies for"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSystemProductionAreaDisciplineListSelect), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SystemAreaDisciplineShiftRateBO.setDisciplineCriteriaBeforeRefresh", PreFindJSFunction:="SystemAreaDisciplineShiftRateBO.triggerDisciplineAutoPopulate", AfterFetchJS:="SystemAreaDisciplineShiftRateBO.afterDisciplineRefreshAjax",
                OnItemSelectJSFunction:="SystemAreaDisciplineShiftRateBO.onDisciplineSelected", LookupMember:="Discipline", DisplayMember:="Discipline", ValueMember:="DisciplineID",
                DropDownCssClass:="mcrc-dropdown", DropDownColumns:={"Discipline"}, UnselectedText:="All Disciplines"),
    Required(ErrorMessage:="Discipline required")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared WeekendIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.WeekendInd, "Weekend", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Weekend value
    ''' </summary>
    <Display(Name:="Weekend", Description:="True if the rate for the discpline is for the weekend")>
    Public Property WeekendInd() As Boolean?
      Get
        Return GetProperty(WeekendIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(WeekendIndProperty, Value)
      End Set
    End Property

    Public Shared EffectiveStartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EffectiveStartDate, "Effective Start Date")
    ''' <summary>
    ''' Gets and sets the Effective Start Date value
    ''' </summary>
    <Display(Name:="Effective Start Date", Description:="The date the rate comes into effect"),
    Required(ErrorMessage:="Effective Start Date required")>
    Public Property EffectiveStartDate As Date
      Get
        Return GetProperty(EffectiveStartDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(EffectiveStartDateProperty, Value)
      End Set
    End Property

    Public Shared RateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Rate, "Rate", 0D)
    ''' <summary>
    ''' Gets and sets the Rate value
    ''' </summary>
    <Display(Name:="Rate", Description:="The rate paid"),
    Required(ErrorMessage:="Rate required")>
    Public Property Rate() As Decimal
      Get
        Return GetProperty(RateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Modified Date Time", Description:=""), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ExtraShiftIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.ExtraShiftInd, "Extra Shift", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Extra Shift value
    ''' </summary>
    <Display(Name:="Extra Shift", Description:="Indicates if the rate is applied to an extra shift")>
    Public Property ExtraShiftInd() As Boolean?
      Get
        Return GetProperty(ExtraShiftIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(ExtraShiftIndProperty, Value)
      End Set
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.EndTime, "End Time")
    ''' <summary>
    ''' Gets and sets the End Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
    TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Property EndTime() As Object
      Get
        Dim value = GetProperty(EndTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(EndTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(EndTimeProperty, DateTime.MinValue)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(EndTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.StartTime, "Start Time")
    ''' <summary>
    ''' Gets and sets the Start Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
    TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Property StartTime() As Object
      Get
        Dim value = GetProperty(StartTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(StartTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(StartTimeProperty, DateTime.MinValue)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(StartTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared HourlyRateIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HourlyRateInd, "Hourly Rate", False)
    ''' <summary>
    ''' Gets and sets the Hourly Rate value
    ''' </summary>
    <Display(Name:="Hourly Rate", Description:="True if the rate is applied hourly, false if rate is applied for entire shift"),
    Required(ErrorMessage:="Hourly Rate required")>
    Public Property HourlyRateInd() As Boolean
      Get
        Return GetProperty(HourlyRateIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HourlyRateIndProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROHumanResourceFindDropDownList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SystemAreaDisciplineShiftRateBO.setHRCriteriaBeforeRefresh", PreFindJSFunction:="SystemAreaDisciplineShiftRateBO.triggerHRAutoPopulate", AfterFetchJS:="SystemAreaDisciplineShiftRateBO.afterHRRefreshAjax",
                OnItemSelectJSFunction:="SystemAreaDisciplineShiftRateBO.onHRSelected", LookupMember:="FullName", DisplayMember:="FullName", ValueMember:="HumanResourceID",
                DropDownCssClass:="mcrc-dropdown", DropDownColumns:={"FullName"}, UnselectedText:="Missing Name"),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared TrainingIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TrainingInd, "Training", False)
    ''' <summary>
    ''' Gets and sets the Training value
    ''' </summary>
    <Display(Name:="Training", Description:=""),
    Required(ErrorMessage:="Training required")>
    Public Property TrainingInd() As Boolean
      Get
        Return GetProperty(TrainingIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(TrainingIndProperty, Value)
      End Set
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaID, "System Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the System Production Area value
    ''' </summary>
    <Display(Name:="System Production Area", Description:="")>
    Public Property SystemProductionAreaID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets and sets the Discipline Name
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared FullNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FullName, "FullName", "")
    ''' <summary>
    ''' Gets and sets the Discipline Name
    ''' </summary>
    <Display(Name:="FullName", Description:="")>
    Public Property FullName() As String
      Get
        Return GetProperty(FullNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FullNameProperty, Value)
      End Set
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "LoginName", "")
    ''' <summary>
    ''' Gets and sets the Discipline Name
    ''' </summary>
    <Display(Name:="Last Modified", Description:="Shows the last user to modify this record.")>
    Public Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LoginNameProperty, Value)
      End Set
    End Property

    Public Shared SortOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SortOrder, "SortOrder", 0)
    ''' <summary>
    ''' Gets and sets the System Production Area value
    ''' </summary>
    <Display(Name:="SortOrder", Description:="")>
    Public Property SortOrder() As Integer
      Get
        Return GetProperty(SortOrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SortOrderProperty, Value)
      End Set
    End Property


#End Region

#Region " Methods "

    Public Function GetParent() As SystemProductionArea

      Return CType(CType(Me.Parent, SystemAreaDisciplineShiftRateList).Parent, SystemProductionArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaDisciplineShiftRatesIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Area Discipline Shift Rate")
        Else
          Return String.Format("Blank {0}", "System Area Discipline Shift Rate")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemAreaDisciplineShiftRate() method.

    End Sub

    Public Shared Function NewSystemAreaDisciplineShiftRate() As SystemAreaDisciplineShiftRate

      Return DataPortal.CreateChild(Of SystemAreaDisciplineShiftRate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemAreaDisciplineShiftRate(dr As SafeDataReader) As SystemAreaDisciplineShiftRate

      Dim s As New SystemAreaDisciplineShiftRate()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemAreaDisciplineShiftRatesIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          Dim tmpWeekendInd = .GetValue(4)
          If IsDBNull(tmpWeekendInd) Then
            LoadProperty(WeekendIndProperty, Nothing)
          Else
            LoadProperty(WeekendIndProperty, tmpWeekendInd)
          End If
          LoadProperty(EffectiveStartDateProperty, .GetValue(5))
          LoadProperty(RateProperty, .GetDecimal(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          Dim tmpExtraShiftInd = .GetValue(11)
          If IsDBNull(tmpExtraShiftInd) Then
            LoadProperty(ExtraShiftIndProperty, Nothing)
          Else
            LoadProperty(ExtraShiftIndProperty, tmpExtraShiftInd)
          End If
          If .IsDBNull(12) Then
            LoadProperty(EndTimeProperty, DateTime.MinValue)
          Else
            LoadProperty(EndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(12))))
          End If
          If .IsDBNull(13) Then
            LoadProperty(StartTimeProperty, DateTime.MinValue)
          Else
            LoadProperty(StartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(13))))
          End If
          LoadProperty(HourlyRateIndProperty, .GetBoolean(14))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(TrainingIndProperty, .GetBoolean(16))
          LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(DisciplineProperty, .GetString(18))
          LoadProperty(FullNameProperty, .GetString(19))
          LoadProperty(LoginNameProperty, .GetString(20))
          LoadProperty(SortOrderProperty, .GetInt32(21))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemAreaDisciplineShiftRatesIDProperty)

      cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      cm.Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
      cm.Parameters.AddWithValue("@WeekendInd", Singular.Misc.NothingDBNull(GetProperty(WeekendIndProperty)))
      cm.Parameters.AddWithValue("@EffectiveStartDate", EffectiveStartDate)
      cm.Parameters.AddWithValue("@Rate", GetProperty(RateProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", GetProperty(ModifiedByProperty))
      cm.Parameters.AddWithValue("@ExtraShiftInd", Singular.Misc.NothingDBNull(GetProperty(ExtraShiftIndProperty)))
      cm.Parameters.AddWithValue("@EndTime", EndTime)
      cm.Parameters.AddWithValue("@StartTime", StartTime)
      cm.Parameters.AddWithValue("@HourlyRateInd", GetProperty(HourlyRateIndProperty))
      cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceIDProperty)))
      cm.Parameters.AddWithValue("@TrainingInd", GetProperty(TrainingIndProperty))
      cm.Parameters.AddWithValue("@SystemProductionAreaID", Me.GetParent().SystemProductionAreaID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemAreaDisciplineShiftRatesIDProperty, cm.Parameters("@SystemAreaDisciplineShiftRatesID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemAreaDisciplineShiftRatesID", GetProperty(SystemAreaDisciplineShiftRatesIDProperty))
    End Sub

#End Region

  End Class

End Namespace