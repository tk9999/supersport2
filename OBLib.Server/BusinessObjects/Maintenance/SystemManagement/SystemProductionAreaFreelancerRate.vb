﻿' Generated 09 May 2017 16:07 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaFreelancerRate
    Inherits OBBusinessBase(Of SystemProductionAreaFreelancerRate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemProductionAreaFreelancerRateIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaFreelancerRateID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemProductionAreaFreelancerRateID() As Integer
      Get
        Return GetProperty(SystemProductionAreaFreelancerRateIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaID, "System Production Area", Nothing)
    ''' <summary>
    ''' Gets the System Production Area value
    ''' </summary>
    <Display(Name:="System Production Area", Description:="")>
    Public ReadOnly Property SystemProductionAreaID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
        Singular.DataAnnotations.DropDownWeb(GetType(ROSystemProductionAreaDisciplineListSelect), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SystemProductionAreaFreelancerRateBO.setDisciplineCriteriaBeforeRefresh", PreFindJSFunction:="SystemProductionAreaFreelancerRateBO.triggerDisciplineAutoPopulate", AfterFetchJS:="SystemProductionAreaFreelancerRateBO.afterDisciplineRefreshAjax",
                OnItemSelectJSFunction:="SystemProductionAreaFreelancerRateBO.onDisciplineSelected", LookupMember:="Discipline", DisplayMember:="Discipline", ValueMember:="DisciplineID",
                DropDownCssClass:="mcrc-dropdown", DropDownColumns:={"Discipline"}, UnselectedText:="All Disciplines")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets and sets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:=""),
    Required(ErrorMessage:="Effective Date required")>
    Public Property EffectiveDate As Date
      Get
        Return GetProperty(EffectiveDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(EffectiveDateProperty, Value)
      End Set
    End Property

    Public Shared WeekdayRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.WeekdayRate, "Weekday Rate", 0D)
    ''' <summary>
    ''' Gets and sets the Weekday Rate value
    ''' </summary>
    <Display(Name:="Weekday Rate", Description:=""),
    Required(ErrorMessage:="Weekday Rate required")>
    Public Property WeekdayRate() As Decimal
      Get
        Return GetProperty(WeekdayRateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(WeekdayRateProperty, Value)
      End Set
    End Property

    Public Shared WeekendRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.WeekendRate, "Weekend Rate", 0D)
    ''' <summary>
    ''' Gets and sets the Weekend Rate value
    ''' </summary>
    <Display(Name:="Weekend Rate", Description:=""),
    Required(ErrorMessage:="Weekend Rate required")>
    Public Property WeekendRate() As Decimal
      Get
        Return GetProperty(WeekendRateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(WeekendRateProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets and sets the Discipline Name
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False), DateField(FormatString:="dd mmm yy HH:mm")>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Modified Date Time", Description:=""), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "LoginName", "")
    ''' <summary>
    ''' Gets and sets the Discipline Name
    ''' </summary>
    <Display(Name:="Last Modified", Description:="Shows the last user to modify this record.")>
    Public Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LoginNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemProductionArea

      Return CType(CType(Me.Parent, SystemProductionAreaFreelancerRateList).Parent, SystemProductionArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaFreelancerRateIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SystemProductionAreaFreelancerRateID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area Freelancer Rate")
        Else
          Return String.Format("Blank {0}", "System Production Area Freelancer Rate")
        End If
      Else
        Return Me.SystemProductionAreaFreelancerRateID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemProductionAreaFreelancerRate() method.

    End Sub

    Public Shared Function NewSystemProductionAreaFreelancerRate() As SystemProductionAreaFreelancerRate

      Return DataPortal.CreateChild(Of SystemProductionAreaFreelancerRate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemProductionAreaFreelancerRate(dr As SafeDataReader) As SystemProductionAreaFreelancerRate

      Dim s As New SystemProductionAreaFreelancerRate()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaFreelancerRateIDProperty, .GetInt32(0))
          LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(EffectiveDateProperty, .GetValue(3))
          LoadProperty(WeekdayRateProperty, .GetDecimal(4))
          LoadProperty(WeekendRateProperty, .GetDecimal(5))
          LoadProperty(DisciplineProperty, .GetString(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(LoginNameProperty, .GetString(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemProductionAreaFreelancerRateIDProperty)

      cm.Parameters.AddWithValue("@SystemProductionAreaID", Me.GetParent().SystemProductionAreaID)
      cm.Parameters.AddWithValue("@DisciplineID", Singular.Misc.NothingDBNull(GetProperty(DisciplineIDProperty)))
      cm.Parameters.AddWithValue("@EffectiveDate", EffectiveDate)
      cm.Parameters.AddWithValue("@WeekdayRate", GetProperty(WeekdayRateProperty))
      cm.Parameters.AddWithValue("@WeekendRate", GetProperty(WeekendRateProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemProductionAreaFreelancerRateIDProperty, cm.Parameters("@SystemProductionAreaFreelancerRateID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemProductionAreaFreelancerRateID", GetProperty(SystemProductionAreaFreelancerRateIDProperty))
    End Sub

#End Region

  End Class

End Namespace