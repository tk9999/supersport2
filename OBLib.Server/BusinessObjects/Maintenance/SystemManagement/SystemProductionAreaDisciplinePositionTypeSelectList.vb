﻿' Generated 10 May 2016 15:20 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaDisciplinePositionTypeSelectList
    Inherits OBBusinessListBase(Of SystemProductionAreaDisciplinePositionTypeSelectList, SystemProductionAreaDisciplinePositionTypeSelect)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaDisciplinePositionTypeID As Integer) As SystemProductionAreaDisciplinePositionTypeSelect

      For Each child As SystemProductionAreaDisciplinePositionTypeSelect In Me
        If child.SystemProductionAreaDisciplinePositionTypeID = SystemProductionAreaDisciplinePositionTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSystemProductionAreaDisciplinePositionTypeSelectList() As SystemProductionAreaDisciplinePositionTypeSelectList

      Return New SystemProductionAreaDisciplinePositionTypeSelectList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace