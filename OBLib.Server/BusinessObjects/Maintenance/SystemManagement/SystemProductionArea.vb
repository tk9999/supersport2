﻿' Generated 22 Mar 2016 12:50 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionArea
    Inherits OBBusinessBase(Of SystemProductionArea)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemProductionAreaID() As Integer
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required(ErrorMessage:="System required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:=""),
    Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared RoomCallTimeRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomCallTimeRequired, "Room Call Time Required", True)
    ''' <summary>
    ''' Gets and sets the Room Call Time Required value
    ''' </summary>
    <Display(Name:="Room Call Time Required", Description:=""),
    Required(ErrorMessage:="Room Call Time Required required")>
    Public Property RoomCallTimeRequired() As Boolean
      Get
        Return GetProperty(RoomCallTimeRequiredProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RoomCallTimeRequiredProperty, Value)
      End Set
    End Property

    Public Shared RoomWrapTimeRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomWrapTimeRequired, "Room Wrap Time Required", True)
    ''' <summary>
    ''' Gets and sets the Room Wrap Time Required value
    ''' </summary>
    <Display(Name:="Room Wrap Time Required", Description:=""),
    Required(ErrorMessage:="Room Wrap Time Required required")>
    Public Property RoomWrapTimeRequired() As Boolean
      Get
        Return GetProperty(RoomWrapTimeRequiredProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RoomWrapTimeRequiredProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Production Area")
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaProperty, Value)
      End Set
    End Property

    Public Shared MaxHoursBeforeApprovalRequiredProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxHoursBeforeApprovalRequired, "Max Hours Before Approval Required", 0)
    ''' <summary>
    ''' Gets and sets the Max Hours Before Approval Required value
    ''' </summary>
    <Display(Name:="Max Hours Before Approval Required", Description:="")>
    Public Property MaxHoursBeforeApprovalRequired() As Integer
      Get
        Return GetProperty(MaxHoursBeforeApprovalRequiredProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MaxHoursBeforeApprovalRequiredProperty, Value)
      End Set
    End Property

    Public Shared UsesSlugsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UsesSlugs, "Uses Slugs", False)
    ''' <summary>
    ''' Gets and sets the Uses Slugs value
    ''' </summary>
    <Display(Name:="Uses Slugs", Description:=""),
    Required(ErrorMessage:="Uses Slugs required")>
    Public Property UsesSlugs() As Boolean
      Get
        Return GetProperty(UsesSlugsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(UsesSlugsProperty, Value)
      End Set
    End Property

    Public Shared SlugLayoutInSchedulerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SlugLayoutInScheduler, "Slug Layout In Scheduler", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="Slug Layout In Scheduler", Description:=""),
    Required(ErrorMessage:="Slug Layout In Scheduler required")>
    Public Property SlugLayoutInScheduler() As Boolean
      Get
        Return GetProperty(SlugLayoutInSchedulerProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SlugLayoutInSchedulerProperty, Value)
      End Set
    End Property

    Public Shared CanViewOtherPersonnelSchedulesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanViewOtherPersonnelSchedules, "Can View Other Personnel Schedules", False)
    ''' <summary>
    ''' Gets and sets the Can View Other Personnel Schedules value
    ''' </summary>
    <Display(Name:="Can View Other Personnel Schedules", Description:=""),
    Required(ErrorMessage:="Can View Other Personnel Schedules required")>
    Public Property CanViewOtherPersonnelSchedules() As Boolean
      Get
        Return GetProperty(CanViewOtherPersonnelSchedulesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanViewOtherPersonnelSchedulesProperty, Value)
      End Set
    End Property

    Public Shared SwapsAllowedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SwapsAllowed, "Swaps Allowed", False)
    ''' <summary>
    ''' Gets and sets the Swaps Allowed value
    ''' </summary>
    <Display(Name:="Swaps Allowed", Description:=""),
    Required(ErrorMessage:="Swaps Allowed required")>
    Public Property SwapsAllowed() As Boolean
      Get
        Return GetProperty(SwapsAllowedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SwapsAllowedProperty, Value)
      End Set
    End Property

    Public Shared IsShiftBasedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsShiftBased, "Is Shift Based", True)
    ''' <summary>
    ''' Gets and sets the Is Shift Based value
    ''' </summary>
    <Display(Name:="Is Shift Based", Description:=""),
    Required(ErrorMessage:="Is Shift Based required")>
    Public Property IsShiftBased() As Boolean
      Get
        Return GetProperty(IsShiftBasedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsShiftBasedProperty, Value)
      End Set
    End Property

    Public Shared RequiresChannelsForRoomSchedulingProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresChannelsForRoomScheduling, "Requires Channels For Room Scheduling", False)
    ''' <summary>
    ''' Gets and sets the Requires Channels For Room Scheduling value
    ''' </summary>
    <Display(Name:="Requires Channels For Room Scheduling", Description:=""),
    Required(ErrorMessage:="Requires Channels For Room Scheduling required")>
    Public Property RequiresChannelsForRoomScheduling() As Boolean
      Get
        Return GetProperty(RequiresChannelsForRoomSchedulingProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RequiresChannelsForRoomSchedulingProperty, Value)
      End Set
    End Property

    Public Shared SynergyChangeEmailAddressesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SynergyChangeEmailAddresses, "Synergy Change Email Addresses", "")
    ''' <summary>
    ''' Gets and sets the Synergy Change Email Addresses value
    ''' </summary>
    <Display(Name:="Synergy Change Email Addresses", Description:=""),
    StringLength(1024, ErrorMessage:="Synergy Change Email Addresses cannot be more than 1024 characters")>
    Public Property SynergyChangeEmailAddresses() As String
      Get
        Return GetProperty(SynergyChangeEmailAddressesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SynergyChangeEmailAddressesProperty, Value)
      End Set
    End Property

    Public Shared ShiftDisputeEmailAddressesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftDisputeEmailAddresses, "Shift Dispute Email Addresses", "")
    ''' <summary>
    ''' Gets and sets the Shift Dispute Email Addresses value
    ''' </summary>
    <Display(Name:="Shift Dispute Email Addresses", Description:=""),
    StringLength(1024, ErrorMessage:="Shift Dispute Email Addresses cannot be more than 1024 characters")>
    Public Property ShiftDisputeEmailAddresses() As String
      Get
        Return GetProperty(ShiftDisputeEmailAddressesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ShiftDisputeEmailAddressesProperty, Value)
      End Set
    End Property

    Public Shared LeaveEmailAddressesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LeaveEmailAddresses, "Leave Email Addresses", "")
    ''' <summary>
    ''' Gets and sets the Leave Email Addresses value
    ''' </summary>
    <Display(Name:="Leave Email Addresses", Description:=""),
    StringLength(1024, ErrorMessage:="Leave Email Addresses cannot be more than 1024 characters")>
    Public Property LeaveEmailAddresses() As String
      Get
        Return GetProperty(LeaveEmailAddressesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LeaveEmailAddressesProperty, Value)
      End Set
    End Property

    Public Shared SwapEmailAddressesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SwapEmailAddresses, "Swap Email Addresses", "")
    ''' <summary>
    ''' Gets and sets the Swap Email Addresses value
    ''' </summary>
    <Display(Name:="Swap Email Addresses", Description:=""),
    StringLength(1024, ErrorMessage:="Swap Email Addresses cannot be more than 1024 characters")>
    Public Property SwapEmailAddresses() As String
      Get
        Return GetProperty(SwapEmailAddressesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SwapEmailAddressesProperty, Value)
      End Set
    End Property

    Public Shared AfterHoursCellNumProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AfterHoursCellNum, "After Hours Cell Num", "")
    ''' <summary>
    ''' Gets and sets the After Hours Cell Num value
    ''' </summary>
    <Display(Name:="After Hours Cell Num", Description:=""),
    StringLength(20, ErrorMessage:="After Hours Cell Num cannot be more than 20 characters")>
    Public Property AfterHoursCellNum() As String
      Get
        Return GetProperty(AfterHoursCellNumProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AfterHoursCellNumProperty, Value)
      End Set
    End Property

    Public Shared IncludeSendersEmailAddressInMonthlyScheduleProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IncludeSendersEmailAddressInMonthlySchedule, "Include Senders Email Address In Monthly Schedule", False)
    ''' <summary>
    ''' Gets and sets the Include Senders Email Address In Monthly Schedule value
    ''' </summary>
    <Display(Name:="Include Senders Email Address In Monthly Schedule", Description:=""),
    Required(ErrorMessage:="Include Senders Email Address In Monthly Schedule required")>
    Public Property IncludeSendersEmailAddressInMonthlySchedule() As Boolean
      Get
        Return GetProperty(IncludeSendersEmailAddressInMonthlyScheduleProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IncludeSendersEmailAddressInMonthlyScheduleProperty, Value)
      End Set
    End Property

    ''Properties used for paging the SystemAreaDisciplineShiftRates Table (TableFor)
    Public Property DisciplineRatePageNo As Integer = 1
    Public Property DisciplineRatePageSize As Integer = 50
    Public Property DisciplineRateSortColumn As String = "Discipline"
    Public Property DisciplineRateSortAsc As Boolean = True
    Public Property DisciplineRateTotalPages As Integer

#End Region

#Region " Child Lists "

    Public Shared SystemProductionAreaFreelancerRateListProperty As PropertyInfo(Of SystemProductionAreaFreelancerRateList) = RegisterProperty(Of SystemProductionAreaFreelancerRateList)(Function(c) c.SystemProductionAreaFreelancerRateList, "System Production Area Freelancer Rate List")

    Public ReadOnly Property SystemProductionAreaFreelancerRateList() As SystemProductionAreaFreelancerRateList
      Get
        If GetProperty(SystemProductionAreaFreelancerRateListProperty) Is Nothing Then
          LoadProperty(SystemProductionAreaFreelancerRateListProperty, Maintenance.SystemManagement.SystemProductionAreaFreelancerRateList.NewSystemProductionAreaFreelancerRateList())
        End If
        Return GetProperty(SystemProductionAreaFreelancerRateListProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaShiftAllowanceSettingListProperty As PropertyInfo(Of SystemProductionAreaShiftAllowanceSettingList) = RegisterProperty(Of SystemProductionAreaShiftAllowanceSettingList)(Function(c) c.SystemProductionAreaShiftAllowanceSettingList, "System Production Area Shift Allowance Setting List")

    Public ReadOnly Property SystemProductionAreaShiftAllowanceSettingList() As SystemProductionAreaShiftAllowanceSettingList
      Get
        If GetProperty(SystemProductionAreaShiftAllowanceSettingListProperty) Is Nothing Then
          LoadProperty(SystemProductionAreaShiftAllowanceSettingListProperty, Maintenance.SystemManagement.SystemProductionAreaShiftAllowanceSettingList.NewSystemProductionAreaShiftAllowanceSettingList())
        End If
        Return GetProperty(SystemProductionAreaShiftAllowanceSettingListProperty)
      End Get
    End Property

    Public Shared SystemAreaDisciplineShiftRateListProperty As PropertyInfo(Of SystemAreaDisciplineShiftRateList) = RegisterProperty(Of SystemAreaDisciplineShiftRateList)(Function(c) c.SystemAreaDisciplineShiftRateList, "System Area Discipline Shift Rate List")

    Public ReadOnly Property SystemAreaDisciplineShiftRateList() As SystemAreaDisciplineShiftRateList
      Get
        If GetProperty(SystemAreaDisciplineShiftRateListProperty) Is Nothing Then
          LoadProperty(SystemAreaDisciplineShiftRateListProperty, Maintenance.SystemManagement.SystemAreaDisciplineShiftRateList.NewSystemAreaDisciplineShiftRateList())
        End If
        Return GetProperty(SystemAreaDisciplineShiftRateListProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaDisciplineListProperty As PropertyInfo(Of SystemProductionAreaDisciplineList) = RegisterProperty(Of SystemProductionAreaDisciplineList)(Function(c) c.SystemProductionAreaDisciplineList, "System Production Area Discipline List")

    Public ReadOnly Property SystemProductionAreaDisciplineList() As SystemProductionAreaDisciplineList
      Get
        If GetProperty(SystemProductionAreaDisciplineListProperty) Is Nothing Then
          LoadProperty(SystemProductionAreaDisciplineListProperty, Maintenance.SystemManagement.SystemProductionAreaDisciplineList.NewSystemProductionAreaDisciplineList())
        End If
        Return GetProperty(SystemProductionAreaDisciplineListProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaCallTimeSettingListProperty As PropertyInfo(Of SystemProductionAreaCallTimeSettingList) = RegisterProperty(Of SystemProductionAreaCallTimeSettingList)(Function(c) c.SystemProductionAreaCallTimeSettingList, "System Production Area Call Time Setting List")

    Public ReadOnly Property SystemProductionAreaCallTimeSettingList() As SystemProductionAreaCallTimeSettingList
      Get
        If GetProperty(SystemProductionAreaCallTimeSettingListProperty) Is Nothing Then
          LoadProperty(SystemProductionAreaCallTimeSettingListProperty, Maintenance.SystemManagement.SystemProductionAreaCallTimeSettingList.NewSystemProductionAreaCallTimeSettingList())
        End If
        Return GetProperty(SystemProductionAreaCallTimeSettingListProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaChannelDefaultListProperty As PropertyInfo(Of SystemProductionAreaChannelDefaultList) = RegisterProperty(Of SystemProductionAreaChannelDefaultList)(Function(c) c.SystemProductionAreaChannelDefaultList, "System Production Area Channel Default List")

    Public ReadOnly Property SystemProductionAreaChannelDefaultList() As SystemProductionAreaChannelDefaultList
      Get
        If GetProperty(SystemProductionAreaChannelDefaultListProperty) Is Nothing Then
          LoadProperty(SystemProductionAreaChannelDefaultListProperty, Maintenance.SystemManagement.SystemProductionAreaChannelDefaultList.NewSystemProductionAreaChannelDefaultList())
        End If
        Return GetProperty(SystemProductionAreaChannelDefaultListProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaSettingListProperty As PropertyInfo(Of SystemProductionAreaSettingList) = RegisterProperty(Of SystemProductionAreaSettingList)(Function(c) c.SystemProductionAreaSettingList, "System Production Area Setting List")

    Public ReadOnly Property SystemProductionAreaSettingList() As SystemProductionAreaSettingList
      Get
        If GetProperty(SystemProductionAreaSettingListProperty) Is Nothing Then
          LoadProperty(SystemProductionAreaSettingListProperty, Maintenance.SystemManagement.SystemProductionAreaSettingList.NewSystemProductionAreaSettingList())
        End If
        Return GetProperty(SystemProductionAreaSettingListProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaStatusListProperty As PropertyInfo(Of SystemProductionAreaStatusList) = RegisterProperty(Of SystemProductionAreaStatusList)(Function(c) c.SystemProductionAreaStatusList, "System Production Area Status List")

    Public ReadOnly Property SystemProductionAreaStatusList() As SystemProductionAreaStatusList
      Get
        If GetProperty(SystemProductionAreaStatusListProperty) Is Nothing Then
          LoadProperty(SystemProductionAreaStatusListProperty, Maintenance.SystemManagement.SystemProductionAreaStatusList.NewSystemProductionAreaStatusList())
        End If
        Return GetProperty(SystemProductionAreaStatusListProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaTimelineTypeListProperty As PropertyInfo(Of SystemProductionAreaTimelineTypeList) = RegisterProperty(Of SystemProductionAreaTimelineTypeList)(Function(c) c.SystemProductionAreaTimelineTypeList, "System Production Area Timeline Type List")

    Public ReadOnly Property SystemProductionAreaTimelineTypeList() As SystemProductionAreaTimelineTypeList
      Get
        If GetProperty(SystemProductionAreaTimelineTypeListProperty) Is Nothing Then
          LoadProperty(SystemProductionAreaTimelineTypeListProperty, Maintenance.SystemManagement.SystemProductionAreaTimelineTypeList.NewSystemProductionAreaTimelineTypeList())
        End If
        Return GetProperty(SystemProductionAreaTimelineTypeListProperty)
      End Get
    End Property

    Public Shared AutomaticImportListProperty As PropertyInfo(Of OBLib.Synergy.AutomaticImportList) = RegisterProperty(Of OBLib.Synergy.AutomaticImportList)(Function(c) c.AutomaticImportList, "AutomaticImportList")

    Public ReadOnly Property AutomaticImportList() As OBLib.Synergy.AutomaticImportList
      Get
        If GetProperty(AutomaticImportListProperty) Is Nothing Then
          LoadProperty(AutomaticImportListProperty, OBLib.Synergy.AutomaticImportList.NewAutomaticImportList)
        End If
        Return GetProperty(AutomaticImportListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionArea.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area")
        Else
          Return String.Format("Blank {0}", "System Production Area")
        End If
      Else
        Return Me.ProductionArea
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"SystemProductionAreaFreelancerRates", "SystemProductionAreaShiftAllowanceSettings", "SystemAreaDisciplineShiftRate", "SystemProductionAreaDiscipline", "SystemProductionAreaCallTimeSetting", "SystemProductionAreaChannelDefault", "SystemProductionAreaSetting", "SystemProductionAreaStatus", "SystemProductionAreaTimelineType"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemProductionArea() method.

    End Sub

    Public Shared Function NewSystemProductionArea() As SystemProductionArea

      Return DataPortal.CreateChild(Of SystemProductionArea)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub


#Region " .Net Data Access "

    Friend Shared Function GetSystemProductionArea(dr As SafeDataReader) As SystemProductionArea

      Dim s As New SystemProductionArea()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(RoomCallTimeRequiredProperty, .GetBoolean(3))
          LoadProperty(RoomWrapTimeRequiredProperty, .GetBoolean(4))
          LoadProperty(ProductionAreaProperty, .GetString(5))
          ''LoadProperty(DefaultLeaveDurationProperty, .GetInt32(6))
          ''If .IsDBNull(7) Then
          ''  LoadProperty(DefaultLeaveStartTimeProperty, DateTime.MinValue)
          ''Else
          ''  LoadProperty(DefaultLeaveStartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(7))))
          ''End If
          LoadProperty(RequiresChannelsForRoomSchedulingProperty, .GetBoolean(6))
          LoadProperty(IsShiftBasedProperty, .GetBoolean(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemProductionAreaIDProperty)

      cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      cm.Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@RoomCallTimeRequired", GetProperty(RoomCallTimeRequiredProperty))
      cm.Parameters.AddWithValue("@RoomWrapTimeRequired", GetProperty(RoomWrapTimeRequiredProperty))
      cm.Parameters.AddWithValue("@MaxHoursBeforeApprovalRequired", GetProperty(MaxHoursBeforeApprovalRequiredProperty))
      cm.Parameters.AddWithValue("@UsesSlugs", GetProperty(UsesSlugsProperty))
      cm.Parameters.AddWithValue("@SlugLayoutInScheduler", GetProperty(SlugLayoutInSchedulerProperty))
      cm.Parameters.AddWithValue("@CanViewOtherPersonnelSchedules", GetProperty(CanViewOtherPersonnelSchedulesProperty))
      cm.Parameters.AddWithValue("@SwapsAllowed", GetProperty(SwapsAllowedProperty))
      cm.Parameters.AddWithValue("@IsShiftBased", GetProperty(IsShiftBasedProperty))
      cm.Parameters.AddWithValue("@RequiresChannelsForRoomScheduling", GetProperty(RequiresChannelsForRoomSchedulingProperty))
      cm.Parameters.AddWithValue("@SynergyChangeEmailAddresses", GetProperty(SynergyChangeEmailAddressesProperty))
      cm.Parameters.AddWithValue("@ShiftDisputeEmailAddresses", GetProperty(ShiftDisputeEmailAddressesProperty))
      cm.Parameters.AddWithValue("@LeaveEmailAddresses", GetProperty(LeaveEmailAddressesProperty))
      cm.Parameters.AddWithValue("@SwapEmailAddresses", GetProperty(SwapEmailAddressesProperty))
      cm.Parameters.AddWithValue("@AfterHoursCellNum", GetProperty(AfterHoursCellNumProperty))
      cm.Parameters.AddWithValue("@IncludeSendersEmailAddressInMonthlySchedule", GetProperty(IncludeSendersEmailAddressInMonthlyScheduleProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemProductionAreaIDProperty, cm.Parameters("@SystemProductionAreaID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(SystemProductionAreaFreelancerRateListProperty))
      UpdateChild(GetProperty(SystemProductionAreaShiftAllowanceSettingListProperty))
      UpdateChild(GetProperty(SystemAreaDisciplineShiftRateListProperty))
      UpdateChild(GetProperty(SystemProductionAreaDisciplineListProperty))
      UpdateChild(GetProperty(SystemProductionAreaCallTimeSettingListProperty))
      UpdateChild(GetProperty(SystemProductionAreaChannelDefaultListProperty))
      UpdateChild(GetProperty(SystemProductionAreaSettingListProperty))
      UpdateChild(GetProperty(SystemProductionAreaStatusListProperty))
      UpdateChild(GetProperty(SystemProductionAreaTimelineTypeListProperty))
      UpdateChild(GetProperty(AutomaticImportListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemProductionAreaID", GetProperty(SystemProductionAreaIDProperty))
    End Sub

#End Region

#End Region

  End Class

End Namespace