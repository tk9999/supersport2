﻿' Generated 10 May 2016 12:09 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaDisciplinePositionType
    Inherits OBBusinessBase(Of SystemProductionAreaDisciplinePositionType)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemProductionAreaDisciplinePositionTypeBO.SystemProductionAreaDisciplinePositionTypeBOToString(self)")

    Public Shared SystemProductionAreaDisciplinePositionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaDisciplinePositionTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemProductionAreaDisciplinePositionTypeID() As Integer
      Get
        Return GetProperty(SystemProductionAreaDisciplinePositionTypeIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaDisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaDisciplineID, "System Production Area Discipline", Nothing)
    ''' <summary>
    ''' Gets the System Production Area Discipline value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemProductionAreaDisciplineID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaDisciplineIDProperty)
      End Get
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionTypeID, "Position Type", Nothing)
    ''' <summary>
    ''' Gets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROPositionTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SystemProductionAreaDisciplinePositionTypeBO.setPositionTypeCriteriaBeforeRefresh", PreFindJSFunction:="SystemProductionAreaDisciplinePositionTypeBO.triggerPositionTypeAutoPopulate", AfterFetchJS:="SystemProductionAreaDisciplinePositionTypeBO.afterPositionTypeRefreshAjax",
                OnItemSelectJSFunction:="SystemProductionAreaDisciplinePositionTypeBO.onPositionTypeSelected", LookupMember:="PositionType", DisplayMember:="PositionType", ValueMember:="PositionTypeID",
                DropDownCssClass:="mcrc-dropdown", DropDownColumns:={"PositionType"}, UnselectedText:="No Position")>
    Public Property PositionTypeID() As Integer?
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(PositionTypeIDProperty, value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position", Nothing)
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROPositionList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SystemProductionAreaDisciplinePositionTypeBO.setPositionCriteriaBeforeRefresh", PreFindJSFunction:="SystemProductionAreaDisciplinePositionTypeBO.triggerPositionAutoPopulate", AfterFetchJS:="SystemProductionAreaDisciplinePositionTypeBO.afterPositionRefreshAjax",
                OnItemSelectJSFunction:="SystemProductionAreaDisciplinePositionTypeBO.onPositionSelected", LookupMember:="Position", DisplayMember:="Position", ValueMember:="PositionID",
                DropDownCssClass:="mcrc-dropdown", DropDownColumns:={"Position"}, UnselectedText:="No Position")>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(PositionIDProperty, value)
      End Set
    End Property

    Public Shared PositionOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionOrder, "Position Order", 999)
    ''' <summary>
    ''' Gets the Position Order value
    ''' </summary>
    <Display(Name:="Position Order", Description:="")>
    Public Property PositionOrder() As Integer
      Get
        Return GetProperty(PositionOrderProperty)
      End Get
      Set(value As Integer)
        SetProperty(PositionOrderProperty, value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Modified Date Time", Description:=""), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared IncludeInBookingDescriptionProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IncludeInBookingDescription, "Include In Booking Description", False)
    ''' <summary>
    ''' Gets the Position Order value
    ''' </summary>
    <Display(Name:="Include In Booking Description", Description:="")>
    Public Property IncludeInBookingDescription() As Boolean
      Get
        Return GetProperty(IncludeInBookingDescriptionProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IncludeInBookingDescriptionProperty, value)
      End Set
    End Property

    Public Shared PositionShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionShortName, "Short Name", "")
    ''' <summary>
    ''' Gets the Position Order value
    ''' </summary>
    <Display(Name:="Short Name", Description:="")>
    Public Property PositionShortName() As String
      Get
        Return GetProperty(PositionShortNameProperty)
      End Get
      Set(value As String)
        SetProperty(PositionShortNameProperty, value)
      End Set
    End Property

    Public Shared PositionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionType, "PositionType", "")
    ''' <summary>
    ''' Gets the PositionType value
    ''' </summary>
    <Display(Name:="PositionType", Description:="")>
    Public Property PositionType() As String
      Get
        Return GetProperty(PositionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionTypeProperty, Value)
      End Set
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position", "")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemProductionAreaDiscipline

      Return CType(CType(Me.Parent, SystemProductionAreaDisciplinePositionTypeList).Parent, SystemProductionAreaDiscipline)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaDisciplinePositionTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area Discipline Position Type")
        Else
          Return String.Format("Blank {0}", "System Production Area Discipline Position Type")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemProductionAreaDisciplinePositionType() method.

    End Sub

    Public Shared Function NewSystemProductionAreaDisciplinePositionType() As SystemProductionAreaDisciplinePositionType

      Return DataPortal.CreateChild(Of SystemProductionAreaDisciplinePositionType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemProductionAreaDisciplinePositionType(dr As SafeDataReader) As SystemProductionAreaDisciplinePositionType

      Dim s As New SystemProductionAreaDisciplinePositionType()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaDisciplinePositionTypeIDProperty, .GetInt32(0))
          LoadProperty(SystemProductionAreaDisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(PositionOrderProperty, .GetInt32(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(IncludeInBookingDescriptionProperty, .GetBoolean(9))
          LoadProperty(PositionShortNameProperty, .GetString(10))
          LoadProperty(PositionTypeProperty, .GetString(11))
          LoadProperty(PositionProperty, .GetString(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemProductionAreaDisciplinePositionTypeIDProperty)

      cm.Parameters.AddWithValue("@SystemProductionAreaDisciplineID", Me.GetParent().SystemProductionAreaDisciplineID)
      cm.Parameters.AddWithValue("@PositionTypeID", GetProperty(PositionTypeIDProperty))
      cm.Parameters.AddWithValue("@PositionID", GetProperty(PositionIDProperty))
      cm.Parameters.AddWithValue("@PositionOrder", GetProperty(PositionOrderProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@IncludeInBookingDescription", GetProperty(IncludeInBookingDescriptionProperty))
      cm.Parameters.AddWithValue("@PositionShortName", GetProperty(PositionShortNameProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemProductionAreaDisciplinePositionTypeIDProperty, cm.Parameters("@SystemProductionAreaDisciplinePositionTypeID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemProductionAreaDisciplinePositionTypeID", GetProperty(SystemProductionAreaDisciplinePositionTypeIDProperty))
    End Sub

#End Region

  End Class

End Namespace