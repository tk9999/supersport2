﻿' Generated 07 Jul 2016 07:53 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaSetting
    Inherits OBBusinessBase(Of SystemProductionAreaSetting)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemProductionAreaSettingBO.SystemProductionAreaSettingBOToString(self)")

    Public Shared SystemProductionAreaSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SystemProductionAreaSettingID() As Integer
      Get
        Return GetProperty(SystemProductionAreaSettingIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaID, "System Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the System Production Area value
    ''' </summary>
    <Display(Name:="System Production Area", Description:=""),
    Required(ErrorMessage:="System Production Area required")>
  Public Property SystemProductionAreaID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets and sets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:=""),
    Required(ErrorMessage:="Effective Date required")>
  Public Property EffectiveDate As Date
      Get
        Return GetProperty(EffectiveDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(EffectiveDateProperty, Value)
      End Set
    End Property

    Public Shared MaxHoursBeforeApprovalRequiredProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxHoursBeforeApprovalRequired, "Max Hours Before Approval Required", 0)
    ''' <summary>
    ''' Gets and sets the Max Hours Before Approval Required value
    ''' </summary>
    <Display(Name:="Max Hours Before Approval Required", Description:="")>
  Public Property MaxHoursBeforeApprovalRequired() As Integer
      Get
        Return GetProperty(MaxHoursBeforeApprovalRequiredProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MaxHoursBeforeApprovalRequiredProperty, Value)
      End Set
    End Property

    Public Shared MaxHoursPerShiftProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.MaxHoursPerShift, "Max Hours Per Shift", CDec(12))
    ''' <summary>
    ''' Gets and sets the Max Hours Per Shift value
    ''' </summary>
    <Display(Name:="Max Hours Per Shift", Description:=""),
    Required(ErrorMessage:="Max Hours Per Shift required")>
  Public Property MaxHoursPerShift() As Decimal
      Get
        Return GetProperty(MaxHoursPerShiftProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(MaxHoursPerShiftProperty, Value)
      End Set
    End Property

    Public Shared MinShortfallBetweenShiftsProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.MinShortfallBetweenShifts, "Min Shortfall Between Shifts", 0D)
    ''' <summary>
    ''' Gets and sets the Min Shortfall Between Shifts value
    ''' </summary>
    <Display(Name:="Min Shortfall Between Shifts", Description:=""),
    Required(ErrorMessage:="Min Shortfall Between Shifts required")>
  Public Property MinShortfallBetweenShifts() As Decimal
      Get
        Return GetProperty(MinShortfallBetweenShiftsProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(MinShortfallBetweenShiftsProperty, Value)
      End Set
    End Property

    Public Shared MaxBookingGroupTimeMinutesProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.MaxBookingGroupTimeMinutes, "Max Booking Group Time Minutes", 0D)
    ''' <summary>
    ''' Gets and sets the Max Booking Group Time Minutes
    ''' </summary>
    <Display(Name:="Max Booking Group Time Minutes", Description:=""),
    Required(ErrorMessage:="Max Booking Group Time Minutes required")>
    Public Property MaxBookingGroupTimeMinutes() As Decimal
      Get
        Return GetProperty(MaxBookingGroupTimeMinutesProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(MaxBookingGroupTimeMinutesProperty, Value)
      End Set
    End Property

    Public Shared MaxConsecutiveDaysBookedProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxConsecutiveDaysBooked, "Max Consecutive Days Booked", 0)
    ''' <summary>
    ''' Gets and sets Max Consecutive Days Booked
    ''' </summary>
    <Display(Name:="Max Consecutive Days Booked Required", Description:="")>
    Public Property MaxConsecutiveDaysBooked() As Integer
      Get
        Return GetProperty(MaxConsecutiveDaysBookedProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MaxConsecutiveDaysBookedProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Modified Date Time", Description:=""), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "LoginName", "")
    ''' <summary>
    ''' Gets and sets the Discipline Name
    ''' </summary>
    <Display(Name:="Last Modified", Description:="Shows the last user to modify this record.")>
    Public Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LoginNameProperty, Value)
      End Set
    End Property


#End Region

#Region " Methods "

    Public Function GetParent() As SystemProductionArea

      Return CType(CType(Me.Parent, SystemProductionAreaSettingList).Parent, SystemProductionArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area Setting")
        Else
          Return String.Format("Blank {0}", "System Production Area Setting")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemProductionAreaSetting() method.

    End Sub

    Public Shared Function NewSystemProductionAreaSetting() As SystemProductionAreaSetting

      Return DataPortal.CreateChild(Of SystemProductionAreaSetting)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemProductionAreaSetting(dr As SafeDataReader) As SystemProductionAreaSetting

      Dim s As New SystemProductionAreaSetting()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaSettingIDProperty, .GetInt32(0))
          LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EffectiveDateProperty, .GetValue(2))
          LoadProperty(MaxHoursBeforeApprovalRequiredProperty, .GetInt32(3))
          LoadProperty(MaxHoursPerShiftProperty, .GetDecimal(4))
          LoadProperty(MinShortfallBetweenShiftsProperty, .GetDecimal(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(LoginNameProperty, .GetString(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemProductionAreaSettingIDProperty)

      cm.Parameters.AddWithValue("@SystemProductionAreaID", GetProperty(SystemProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@EffectiveDate", EffectiveDate)
      cm.Parameters.AddWithValue("@MaxHoursBeforeApprovalRequired", GetProperty(MaxHoursBeforeApprovalRequiredProperty))
      cm.Parameters.AddWithValue("@MaxHoursPerShift", GetProperty(MaxHoursPerShiftProperty))
      cm.Parameters.AddWithValue("@MinShortfallBetweenShifts", GetProperty(MinShortfallBetweenShiftsProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemProductionAreaSettingIDProperty, cm.Parameters("@SystemProductionAreaSettingID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemProductionAreaSettingID", GetProperty(SystemProductionAreaSettingIDProperty))
    End Sub

#End Region

  End Class

End Namespace