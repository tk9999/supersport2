﻿' Generated 13 Aug 2016 15:50 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaOffReasonSetting
    Inherits OBReadOnlyBase(Of ROSystemProductionAreaOffReasonSetting)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSystemProductionAreaOffReasonSettingBO.ROSystemProductionAreaOffReasonSettingBOToString(self)")

    Public Shared SystemProductionAreaOffReasonSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaOffReasonSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SystemProductionAreaOffReasonSettingID() As Integer
      Get
        Return GetProperty(SystemProductionAreaOffReasonSettingIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaOffReasonIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaOffReasonID, "System Production Area Off Reason", Nothing)
    ''' <summary>
    ''' Gets the System Production Area Off Reason value
    ''' </summary>
    <Display(Name:="System Production Area Off Reason", Description:="")>
  Public ReadOnly Property SystemProductionAreaOffReasonID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaOffReasonIDProperty)
      End Get
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:="")>
  Public ReadOnly Property EffectiveDate As Date
      Get
        Return GetProperty(EffectiveDateProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
  Public ReadOnly Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared DayStartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DayStartTime, "Day Start Time")
    ''' <summary>
    ''' Gets the Default Start Time value
    ''' </summary>
    <Display(Name:="Default Start Time", Description:="")>
    Public ReadOnly Property DayStartTime() As Object
      Get
        Dim value = GetProperty(DayStartTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared DayEndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DayEndTime, "Day End Time")
    ''' <summary>
    ''' Gets the Default Start Time value
    ''' </summary>
    <Display(Name:="Default End Time", Description:="")>
    Public ReadOnly Property DayEndTime() As Object
      Get
        Dim value = GetProperty(DayEndTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared AccumulateToNormalHoursProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AccumulateToNormalHours, "Accumulate To Normal Hours", True)
    ''' <summary>
    ''' Gets the Accumulate To Normal Hours value
    ''' </summary>
    <Display(Name:="Accumulate To Normal Hours", Description:="")>
    Public ReadOnly Property AccumulateToNormalHours() As Boolean
      Get
        Return GetProperty(AccumulateToNormalHoursProperty)
      End Get
    End Property

    Public Shared AccumulateToOvertimeProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AccumulateToOvertime, "Accumulate To Overtime", True)
    ''' <summary>
    ''' Gets the Accumulate To Overtime value
    ''' </summary>
    <Display(Name:="Accumulate To Overtime", Description:="")>
    Public ReadOnly Property AccumulateToOvertime() As Boolean
      Get
        Return GetProperty(AccumulateToOvertimeProperty)
      End Get
    End Property

    Public Shared IsPaidLeaveProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsPaidLeave, "Is Paid Leave", False)
    ''' <summary>
    ''' Gets the Is Paid Leave value
    ''' </summary>
    <Display(Name:="Is Paid Leave", Description:="")>
    Public ReadOnly Property IsPaidLeave() As Boolean
      Get
        Return GetProperty(IsPaidLeaveProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared BookingStartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.BookingStartTime, "Booking Start Time")
    ''' <summary>
    ''' Gets the Default Start Time value
    ''' </summary>
    <Display(Name:="Booking Start Time", Description:="")>
    Public ReadOnly Property BookingStartTime() As Object
      Get
        Dim value = GetProperty(BookingStartTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared BookingEndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.BookingEndTime, "Booking End Time")
    ''' <summary>
    ''' Gets the Default Start Time value
    ''' </summary>
    <Display(Name:="Booking End Time", Description:="")>
    Public ReadOnly Property BookingEndTime() As Object
      Get
        Dim value = GetProperty(BookingEndTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaOffReasonSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSystemProductionAreaOffReasonSetting(dr As SafeDataReader) As ROSystemProductionAreaOffReasonSetting

      Dim r As New ROSystemProductionAreaOffReasonSetting()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemProductionAreaOffReasonSettingIDProperty, .GetInt32(0))
        LoadProperty(SystemProductionAreaOffReasonIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(EffectiveDateProperty, .GetValue(2))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        If .IsDBNull(4) Then
          LoadProperty(DayStartTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(DayStartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(4))))
        End If
        If .IsDBNull(5) Then
          LoadProperty(DayEndTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(DayEndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(5))))
        End If
        LoadProperty(AccumulateToNormalHoursProperty, .GetBoolean(6))
        LoadProperty(AccumulateToOvertimeProperty, .GetBoolean(7))
        LoadProperty(IsPaidLeaveProperty, .GetBoolean(8))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
        If .IsDBNull(8) Then
          LoadProperty(BookingStartTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(BookingStartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(8))))
        End If
        If .IsDBNull(9) Then
          LoadProperty(BookingEndTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(BookingEndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(9))))
        End If
      End With

    End Sub

#End Region

  End Class

End Namespace