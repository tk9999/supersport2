﻿' Generated 07 Jul 2016 07:56 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaSettingList
    Inherits OBReadOnlyListBase(Of ROSystemProductionAreaSettingList, ROSystemProductionAreaSetting)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaSettingID As Integer) As ROSystemProductionAreaSetting

      For Each child As ROSystemProductionAreaSetting In Me
        If child.SystemProductionAreaSettingID = SystemProductionAreaSettingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Settings"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROSystemProductionAreaSettingList() As ROSystemProductionAreaSettingList

      Return New ROSystemProductionAreaSettingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemProductionAreaSettingList() As ROSystemProductionAreaSettingList

      Return DataPortal.Fetch(Of ROSystemProductionAreaSettingList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemProductionAreaSetting.GetROSystemProductionAreaSetting(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemProductionAreaSettingList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace