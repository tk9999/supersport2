﻿' Generated 10 May 2016 12:05 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaDisciplinePositionTypeListSelect
    Inherits OBReadOnlyListBase(Of ROSystemProductionAreaDisciplinePositionTypeListSelect, ROSystemProductionAreaDisciplinePositionTypeSelect)

#Region " Parent "

    <NotUndoable()> Private mParent As ROSystemProductionAreaDiscipline
#End Region

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaDisciplinePositionTypeID As Integer) As ROSystemProductionAreaDisciplinePositionTypeSelect

      For Each child As ROSystemProductionAreaDisciplinePositionTypeSelect In Me
        If child.SystemProductionAreaDisciplinePositionTypeID = SystemProductionAreaDisciplinePositionTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Discipline Position Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?
      Public Property DisciplineID As Integer?
      Public Property HumanResourceID As Integer?
      Public Property ProductionSystemAreaID As Integer?

      <Display(Name:="Position Type", Description:=""), PrimarySearchField>
      Public Property PositionType() As String

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROSystemProductionAreaDisciplinePositionTypeListSelect() As ROSystemProductionAreaDisciplinePositionTypeListSelect

      Return New ROSystemProductionAreaDisciplinePositionTypeListSelect()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemProductionAreaDisciplinePositionTypeListSelect() As ROSystemProductionAreaDisciplinePositionTypeListSelect

      Return DataPortal.Fetch(Of ROSystemProductionAreaDisciplinePositionTypeListSelect)(New Criteria())

    End Function

    Public Shared Function GetROSystemProductionAreaDisciplinePositionTypeListSelect(SystemID As Integer?, ProductionAreaID As Integer?) As ROSystemProductionAreaDisciplinePositionTypeListSelect

      Return DataPortal.Fetch(Of ROSystemProductionAreaDisciplinePositionTypeListSelect)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemProductionAreaDisciplinePositionTypeSelect.GetROSystemProductionAreaDisciplinePositionTypeSelect(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemProductionAreaDisciplinePositionTypeListSelect"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@PositionType", Strings.MakeEmptyDBNull(crit.PositionType))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace