﻿' Generated 27 Jun 2016 15:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaDisciplinePositionTypeFlat
    Inherits OBReadOnlyBase(Of ROSystemProductionAreaDisciplinePositionTypeFlat)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSystemProductionAreaDisciplinePositionTypeFlatBO.ROSystemProductionAreaDisciplinePositionTypeFlatBOToString(self)")

    Public Shared SystemProductionAreaDisciplinePositionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaDisciplinePositionTypeID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SystemProductionAreaDisciplinePositionTypeID() As Integer
      Get
        Return GetProperty(SystemProductionAreaDisciplinePositionTypeIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaDisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaDisciplineID, "System Production Area Discipline")
    ''' <summary>
    ''' Gets the System Production Area Discipline value
    ''' </summary>
    <Display(Name:="System Production Area Discipline", Description:="")>
  Public ReadOnly Property SystemProductionAreaDisciplineID() As Integer
      Get
        Return GetProperty(SystemProductionAreaDisciplineIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
  Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionTypeID, "Position Type")
    ''' <summary>
    ''' Gets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
  Public ReadOnly Property PositionTypeID() As Integer
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionID, "Position")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
  Public ReadOnly Property PositionID() As Integer
      Get
        Return GetProperty(PositionIDProperty)
      End Get
    End Property

    Public Shared PositionOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionOrder, "Position Order")
    ''' <summary>
    ''' Gets the Position Order value
    ''' </summary>
    <Display(Name:="Position Order", Description:="")>
  Public ReadOnly Property PositionOrder() As Integer
      Get
        Return GetProperty(PositionOrderProperty)
      End Get
    End Property

    Public Shared PositionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionType, "Position Type")
    ''' <summary>
    ''' Gets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
  Public ReadOnly Property PositionType() As String
      Get
        Return GetProperty(PositionTypeProperty)
      End Get
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
  Public ReadOnly Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
  Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared IncludeInBookingDescriptionProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IncludeInBookingDescription, "Include In Booking Description", False)
    ''' <summary>
    ''' Gets the Include In Booking Description value
    ''' </summary>
    <Display(Name:="Include In Booking Description", Description:="")>
  Public ReadOnly Property IncludeInBookingDescription() As Boolean
      Get
        Return GetProperty(IncludeInBookingDescriptionProperty)
      End Get
    End Property

    Public Shared PositionShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionShortName, "Position Short Name")
    ''' <summary>
    ''' Gets the Position Short Name value
    ''' </summary>
    <Display(Name:="Position Short Name", Description:="")>
  Public ReadOnly Property PositionShortName() As String
      Get
        Return GetProperty(PositionShortNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaDisciplinePositionTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.PositionType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSystemProductionAreaDisciplinePositionTypeFlat(dr As SafeDataReader) As ROSystemProductionAreaDisciplinePositionTypeFlat

      Dim r As New ROSystemProductionAreaDisciplinePositionTypeFlat()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemProductionAreaDisciplinePositionTypeIDProperty, .GetInt32(0))
        LoadProperty(SystemProductionAreaDisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(PositionOrderProperty, .GetInt32(6))
        LoadProperty(PositionTypeProperty, .GetString(7))
        LoadProperty(PositionProperty, .GetString(8))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
        LoadProperty(IncludeInBookingDescriptionProperty, .GetBoolean(10))
        LoadProperty(PositionShortNameProperty, .GetString(11))
      End With

    End Sub

#End Region

  End Class

End Namespace