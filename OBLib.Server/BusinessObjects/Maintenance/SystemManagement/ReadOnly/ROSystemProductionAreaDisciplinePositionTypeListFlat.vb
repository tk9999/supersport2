﻿' Generated 27 Jun 2016 15:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaDisciplinePositionTypeListFlat
    Inherits OBReadOnlyListBase(Of ROSystemProductionAreaDisciplinePositionTypeListFlat, ROSystemProductionAreaDisciplinePositionTypeFlat)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaDisciplinePositionTypeID As Integer) As ROSystemProductionAreaDisciplinePositionTypeFlat

      For Each child As ROSystemProductionAreaDisciplinePositionTypeFlat In Me
        If child.SystemProductionAreaDisciplinePositionTypeID = SystemProductionAreaDisciplinePositionTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROSystemProductionAreaDisciplinePositionTypeListFlat() As ROSystemProductionAreaDisciplinePositionTypeListFlat

      Return New ROSystemProductionAreaDisciplinePositionTypeListFlat()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemProductionAreaDisciplinePositionTypeListFlat() As ROSystemProductionAreaDisciplinePositionTypeListFlat

      Return DataPortal.Fetch(Of ROSystemProductionAreaDisciplinePositionTypeListFlat)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemProductionAreaDisciplinePositionTypeFlat.GetROSystemProductionAreaDisciplinePositionTypeFlat(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemProductionAreaDisciplinePositionTypeListFlat"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace