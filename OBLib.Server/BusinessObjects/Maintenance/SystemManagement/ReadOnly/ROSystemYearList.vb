﻿' Generated 25 Apr 2016 14:29 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemYearList
    Inherits OBReadOnlyListBase(Of ROSystemYearList, ROSystemYear)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "


    ' ** Example Paging property declaration code to be implemented in the necessary VM **
    'Public Property ROSystemYearList As ROSystemYearList
    'Public Property ROSystemYearListCriteria As ROSystemYearList.Criteria
    'Public Property ROSystemYearListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    ' ** Example Paging definition code to be implemented in the necessary VM Setup Method **
    'ROSystemYearList = New ROSystemYearList
    'ROSystemYearListCriteria = New ROSystemYearList.Criteria
    'ROSystemYearListManager = New Singular.Web.Data.PagedDataManager(Of VMType)(Function(d) d.ROSystemYearList, Function(d) d.ROSystemYearListCriteria, "SortBy", PageSize, SortAsc)

    Public Function GetItem(SystemYearID As Integer) As ROSystemYear

      For Each child As ROSystemYear In Me
        If child.SystemYearID = SystemYearID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Years"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property SystemID As Integer? = Nothing
      Public Property SystemYearID As Integer? = Nothing
      <PrimarySearchField>
      Public Property SystemYear As String = ""

      Public Sub New(SystemYearID As Integer?)
        Me.SystemYearID = SystemYearID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROSystemYearList() As ROSystemYearList

      Return New ROSystemYearList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemYearList() As ROSystemYearList

      Return DataPortal.Fetch(Of ROSystemYearList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemYear.GetROSystemYear(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemYearList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@SystemYearID", NothingDBNull(crit.SystemYearID))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUser.UserID)
            cm.Parameters.AddWithValue("@SystemYear", Strings.MakeEmptyDBNull(crit.SystemYear))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace