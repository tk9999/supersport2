﻿' Generated 10 May 2016 12:05 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaDisciplineList
    Inherits OBReadOnlyListBase(Of ROSystemProductionAreaDisciplineList, ROSystemProductionAreaDiscipline)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaDisciplineID As Integer) As ROSystemProductionAreaDiscipline

      For Each child As ROSystemProductionAreaDiscipline In Me
        If child.SystemProductionAreaDisciplineID = SystemProductionAreaDisciplineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Disciplines"

    End Function

    'Public Function GetROSystemProductionAreaDisciplinePositionType(SystemProductionAreaDisciplinePositionTypeID As Integer) As ROSystemProductionAreaDisciplinePositionType

    '  Dim obj As ROSystemProductionAreaDisciplinePositionType = Nothing
    '  For Each parent As ROSystemProductionAreaDiscipline In Me
    '    obj = parent.ROSystemProductionAreaDisciplinePositionTypeList.GetItem(SystemProductionAreaDisciplinePositionTypeID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?
      Public Property CrewTypeID As Integer?
      <PrimarySearchField>
      Public Property Discipline As String
      Public Property HumanResourceID As Integer?


      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROSystemProductionAreaDisciplineList() As ROSystemProductionAreaDisciplineList

      Return New ROSystemProductionAreaDisciplineList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemProductionAreaDisciplineList() As ROSystemProductionAreaDisciplineList

      Return DataPortal.Fetch(Of ROSystemProductionAreaDisciplineList)(New Criteria())

    End Function

    Public Shared Function GetROSystemProductionAreaDisciplineList(SystemID As Integer?, ProductionAreaID As Integer?) As ROSystemProductionAreaDisciplineList

      Return DataPortal.Fetch(Of ROSystemProductionAreaDisciplineList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemProductionAreaDiscipline.GetROSystemProductionAreaDiscipline(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemProductionAreaDisciplineList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@CrewTypeID", NothingDBNull(crit.CrewTypeID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace