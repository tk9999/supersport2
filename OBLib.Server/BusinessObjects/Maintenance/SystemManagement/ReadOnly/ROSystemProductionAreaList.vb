﻿' Generated 27 Jan 2016 12:30 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaList
    Inherits OBReadOnlyListBase(Of ROSystemProductionAreaList, ROSystemProductionArea)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaID As Integer) As ROSystemProductionArea

      For Each child As ROSystemProductionArea In Me
        If child.SystemProductionAreaID = SystemProductionAreaID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Areas"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSystemProductionAreaList() As ROSystemProductionAreaList

      Return New ROSystemProductionAreaList()

    End Function

    Public Shared Sub BeginGetROSystemProductionAreaList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSystemProductionAreaList)))

      Dim dp As New DataPortal(Of ROSystemProductionAreaList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSystemProductionAreaList(CallBack As EventHandler(Of DataPortalResult(Of ROSystemProductionAreaList)))

      Dim dp As New DataPortal(Of ROSystemProductionAreaList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSystemProductionAreaList() As ROSystemProductionAreaList

      Return DataPortal.Fetch(Of ROSystemProductionAreaList)(New Criteria())

    End Function

    Public Shared Function GetROSystemProductionAreaList(SystemID As Integer?, ProductionAreaID As Integer?) As ROSystemProductionAreaList

      Return DataPortal.Fetch(Of ROSystemProductionAreaList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemProductionArea.GetROSystemProductionArea(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemProductionAreaList"
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace