﻿' Generated 26 May 2016 09:49 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaCallTimeSettingList
    Inherits OBReadOnlyListBase(Of ROSystemProductionAreaCallTimeSettingList, ROSystemProductionAreaCallTimeSetting)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaCallTimeSettingID As Integer) As ROSystemProductionAreaCallTimeSetting

      For Each child As ROSystemProductionAreaCallTimeSetting In Me
        If child.SystemProductionAreaCallTimeSettingID = SystemProductionAreaCallTimeSettingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Call Time Settings"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROSystemProductionAreaCallTimeSettingList() As ROSystemProductionAreaCallTimeSettingList

      Return New ROSystemProductionAreaCallTimeSettingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemProductionAreaCallTimeSettingList() As ROSystemProductionAreaCallTimeSettingList

      Return DataPortal.Fetch(Of ROSystemProductionAreaCallTimeSettingList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemProductionAreaCallTimeSetting.GetROSystemProductionAreaCallTimeSetting(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemProductionAreaCallTimeSettingList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace