﻿' Generated 14 Jul 2016 10:50 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Imports Singular
Imports Singular.Misc

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaDisciplineReportList
    Inherits OBReadOnlyListBase(Of ROSystemProductionAreaDisciplineReportList, ROSystemProductionAreaDisciplineReport)

    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaDisciplineID As Integer) As ROSystemProductionAreaDisciplineReport

      For Each child As ROSystemProductionAreaDisciplineReport In Me
        If child.SystemProductionAreaDisciplineID = SystemProductionAreaDisciplineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      <Display(Name:="Sub-Dept")>
      Public Property SystemID As Integer?

      <Display(Name:="Area")>
      Public Property ProductionAreaID As Integer?


      Public Sub New()


      End Sub

      Public Sub New(SystemID As Integer, ProductionAreaID As Integer)

        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID

      End Sub

    End Class

    Public Shared Function NewROSystemProductionAreaDisciplineReportList() As ROSystemProductionAreaDisciplineReportList

      Return New ROSystemProductionAreaDisciplineReportList()

    End Function

    Public Sub New()


      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemProductionAreaDisciplineReportList() As ROSystemProductionAreaDisciplineReportList

      Return DataPortal.Fetch(Of ROSystemProductionAreaDisciplineReportList)(New Criteria())

    End Function

    Public Shared Function GetROSystemProductionAreaDisciplineReportList(SystemID As Integer, ProductionAreaID As Integer) As ROSystemProductionAreaDisciplineReportList

      Return DataPortal.Fetch(Of ROSystemProductionAreaDisciplineReportList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemProductionAreaDisciplineReport.GetROSystemProductionAreaDisciplineReport(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemProductionAreaDisciplineReportList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace