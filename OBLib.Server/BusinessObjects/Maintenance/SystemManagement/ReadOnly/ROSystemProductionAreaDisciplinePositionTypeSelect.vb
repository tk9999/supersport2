﻿' Generated 10 May 2016 12:05 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaDisciplinePositionTypeSelect
    Inherits OBReadOnlyBase(Of ROSystemProductionAreaDisciplinePositionTypeSelect)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSystemProductionAreaDisciplinePositionTypeSelectBO.ROSystemProductionAreaDisciplinePositionTypeSelectBOToString(self)")

    Public Shared SystemProductionAreaDisciplinePositionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaDisciplinePositionTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemProductionAreaDisciplinePositionTypeID() As Integer
      Get
        Return GetProperty(SystemProductionAreaDisciplinePositionTypeIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaDisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaDisciplineID, "System Production Area Discipline", Nothing)
    ''' <summary>
    ''' Gets the System Production Area Discipline value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemProductionAreaDisciplineID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaDisciplineIDProperty)
      End Get
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionTypeID, "Position Type", Nothing)
    ''' <summary>
    ''' Gets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
    Public ReadOnly Property PositionTypeID() As Integer?
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionID, "Position", 0)
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property PositionID() As Integer
      Get
        Return GetProperty(PositionIDProperty)
      End Get
    End Property

    Public Shared PositionOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionOrder, "Position Order", 0)
    ''' <summary>
    ''' Gets the Position Order value
    ''' </summary>
    <Display(Name:="Position Order", Description:="")>
    Public ReadOnly Property PositionOrder() As Integer
      Get
        Return GetProperty(PositionOrderProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PositionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionType, "Position Type", "")
    ''' <summary>
    ''' Gets the Position Order value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
    Public ReadOnly Property PositionType() As String
      Get
        Return GetProperty(PositionTypeProperty)
      End Get
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position", "")
    ''' <summary>
    ''' Gets the Position Order value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets the System Production Area Discipline value
    ''' </summary>
    Public ReadOnly Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared IncludeInBookingDescriptionProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IncludeInBookingDescription, "Include In Booking Description", False)
    ''' <summary>
    ''' Gets the Position Order value
    ''' </summary>
    <Display(Name:="Include In Booking Description", Description:="")>
    Public ReadOnly Property IncludeInBookingDescription() As Boolean
      Get
        Return GetProperty(IncludeInBookingDescriptionProperty)
      End Get
    End Property

    Public Shared PositionShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionShortName, "Short Name", "")
    ''' <summary>
    ''' Gets the Position Order value
    ''' </summary>
    <Display(Name:="Short Name", Description:="")>
    Public ReadOnly Property PositionShortName() As String
      Get
        Return GetProperty(PositionShortNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaDisciplinePositionTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSystemProductionAreaDisciplinePositionTypeSelect(dr As SafeDataReader) As ROSystemProductionAreaDisciplinePositionTypeSelect

      Dim r As New ROSystemProductionAreaDisciplinePositionTypeSelect()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemProductionAreaDisciplinePositionTypeIDProperty, .GetInt32(0))
        LoadProperty(SystemProductionAreaDisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(PositionIDProperty, .GetInt32(3))
        LoadProperty(PositionOrderProperty, .GetInt32(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(PositionTypeProperty, .GetString(9))
        LoadProperty(PositionProperty, .GetString(10))
        LoadProperty(DisciplineIDProperty, ZeroNothing(.GetInt32(11)))
        LoadProperty(IncludeInBookingDescriptionProperty, .GetBoolean(12))
        LoadProperty(PositionShortNameProperty, .GetString(13))
      End With

    End Sub

#End Region

  End Class

End Namespace