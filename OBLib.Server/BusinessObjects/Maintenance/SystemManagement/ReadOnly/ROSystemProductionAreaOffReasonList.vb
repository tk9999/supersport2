﻿' Generated 13 Aug 2016 15:50 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaOffReasonList
    Inherits OBReadOnlyListBase(Of ROSystemProductionAreaOffReasonList, ROSystemProductionAreaOffReason)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaOffReasonID As Integer) As ROSystemProductionAreaOffReason

      For Each child As ROSystemProductionAreaOffReason In Me
        If child.SystemProductionAreaOffReasonID = SystemProductionAreaOffReasonID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO System Production Area Off Reasons"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?

      <Display(Name:="OffReason", Description:=""), PrimarySearchField>
      Public Property OffReason() As String

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROSystemProductionAreaOffReasonList() As ROSystemProductionAreaOffReasonList

      Return New ROSystemProductionAreaOffReasonList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemProductionAreaOffReasonList() As ROSystemProductionAreaOffReasonList

      Return DataPortal.Fetch(Of ROSystemProductionAreaOffReasonList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemProductionAreaOffReason.GetROSystemProductionAreaOffReason(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemProductionAreaOffReasonList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@OffReason", Strings.MakeEmptyDBNull(crit.OffReason))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace