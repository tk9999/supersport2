﻿' Generated 02 Jun 2016 13:22 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaTimelineTypeList
    Inherits OBReadOnlyListBase(Of ROSystemProductionAreaTimelineTypeList, ROSystemProductionAreaTimelineType)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaTimelineTypeID As Integer) As ROSystemProductionAreaTimelineType

      For Each child As ROSystemProductionAreaTimelineType In Me
        If child.SystemProductionAreaTimelineTypeID = SystemProductionAreaTimelineTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Production Area Timeline Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?
      <PrimarySearchField>
      Public Property ProductionTimelineType As String

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROSystemProductionAreaTimelineTypeList() As ROSystemProductionAreaTimelineTypeList

      Return New ROSystemProductionAreaTimelineTypeList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemProductionAreaTimelineTypeList() As ROSystemProductionAreaTimelineTypeList

      Return DataPortal.Fetch(Of ROSystemProductionAreaTimelineTypeList)(New Criteria())

    End Function

    Public Shared Function GetROSystemProductionAreaTimelineTypeList(SystemID As Integer?, ProductionAreaID As Integer?) As ROSystemProductionAreaTimelineTypeList

      Return DataPortal.Fetch(Of ROSystemProductionAreaTimelineTypeList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemProductionAreaTimelineType.GetROSystemProductionAreaTimelineType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemProductionAreaTimelineTypeList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@ProductionTimelineType", Strings.MakeEmptyDBNull(crit.ProductionTimelineType))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace