﻿' Generated 27 Jan 2016 12:30 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionArea
    Inherits OBReadOnlyBase(Of ROSystemProductionArea)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemProductionAreaID() As Integer
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared RoomCallTimeRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomCallTimeRequired, "Room Call Time Required", True)
    ''' <summary>
    ''' Gets the Room Call Time Required value
    ''' </summary>
    <Display(Name:="Room Call Time Required", Description:="")>
    Public ReadOnly Property RoomCallTimeRequired() As Boolean
      Get
        Return GetProperty(RoomCallTimeRequiredProperty)
      End Get
    End Property

    Public Shared RoomWrapTimeRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomWrapTimeRequired, "Room Wrap Time Required", True)
    ''' <summary>
    ''' Gets the Room Wrap Time Required value
    ''' </summary>
    <Display(Name:="Room Wrap Time Required", Description:="")>
    Public ReadOnly Property RoomWrapTimeRequired() As Boolean
      Get
        Return GetProperty(RoomWrapTimeRequiredProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area", "")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Area")>
    Public ReadOnly Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

    Public Shared DefaultLeaveDurationProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DefaultLeaveDuration, "Default Leave Duration", 12)
    ''' <summary>
    ''' Gets the Default Leave Duration value
    ''' </summary>
    Public ReadOnly Property DefaultLeaveDuration() As Integer
      Get
        Return GetProperty(DefaultLeaveDurationProperty)
      End Get
    End Property

    Public Shared DefaultLeaveStartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DefaultLeaveStartTime, "Default Leave Start Time")
    ''' <summary>
    ''' Gets the Default Leave Start Time value
    ''' </summary>
    Public ReadOnly Property DefaultLeaveStartTime() As Object
      Get
        'Return GetProperty(DefaultLeaveStartTimeProperty)
        Dim value = GetProperty(DefaultLeaveStartTimeProperty)
        Return CDate(value).ToString("HH:mm")
      End Get
    End Property

    Public Shared SiteIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SiteID, "Site", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Type value
    ''' </summary>
    <Display(Name:="Site")>
    Public ReadOnly Property SiteID() As Integer?
      Get
        Return GetProperty(SiteIDProperty)
      End Get
    End Property

    Public Shared RequiresChannelsForRoomSchedulingProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresChannelsForRoomScheduling, "Requires Channel Details For Room Scheduling", False)
    ''' <summary>
    ''' Gets and sets the Room Wrap Time Required value
    ''' </summary>
    <Display(Name:="Requires Channel Details For Room Scheduling", Description:="")>
    Public ReadOnly Property RequiresChannelsForRoomScheduling() As Boolean
      Get
        Return GetProperty(RequiresChannelsForRoomSchedulingProperty)
      End Get
    End Property

    Public Shared ShiftBasedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShiftBased, "Shift Based", False)
    ''' <summary>
    ''' Gets and sets the Room Wrap Time Required value
    ''' </summary>
    <Display(Name:="Shift Based", Description:="")>
    Public ReadOnly Property ShiftBased() As Boolean
      Get
        Return GetProperty(ShiftBasedProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SystemProductionAreaID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemProductionArea(dr As SafeDataReader) As ROSystemProductionArea

      Dim r As New ROSystemProductionArea()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemProductionAreaIDProperty, .GetInt32(0))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(RoomCallTimeRequiredProperty, .GetBoolean(3))
        LoadProperty(RoomWrapTimeRequiredProperty, .GetBoolean(4))
        LoadProperty(ProductionAreaProperty, .GetString(5))
        LoadProperty(SiteIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(RequiresChannelsForRoomSchedulingProperty, .GetBoolean(7))
        LoadProperty(ShiftBasedProperty, .GetBoolean(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace