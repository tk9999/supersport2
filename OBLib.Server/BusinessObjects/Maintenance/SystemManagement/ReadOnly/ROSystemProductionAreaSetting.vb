﻿' Generated 07 Jul 2016 07:56 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaSetting
    Inherits OBReadOnlyBase(Of ROSystemProductionAreaSetting)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSystemProductionAreaSettingBO.ROSystemProductionAreaSettingBOToString(self)")

    Public Shared SystemProductionAreaSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SystemProductionAreaSettingID() As Integer
      Get
        Return GetProperty(SystemProductionAreaSettingIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaID, "System Production Area", Nothing)
    ''' <summary>
    ''' Gets the System Production Area value
    ''' </summary>
    <Display(Name:="System Production Area", Description:="")>
  Public ReadOnly Property SystemProductionAreaID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:="")>
  Public ReadOnly Property EffectiveDate As Date
      Get
        Return GetProperty(EffectiveDateProperty)
      End Get
    End Property

    Public Shared MaxHoursBeforeApprovalRequiredProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxHoursBeforeApprovalRequired, "Max Hours Before Approval Required", 0)
    ''' <summary>
    ''' Gets the Max Hours Before Approval Required value
    ''' </summary>
    <Display(Name:="Max Hours Before Approval Required", Description:="")>
  Public ReadOnly Property MaxHoursBeforeApprovalRequired() As Integer
      Get
        Return GetProperty(MaxHoursBeforeApprovalRequiredProperty)
      End Get
    End Property

    Public Shared MaxHoursPerShiftProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.MaxHoursPerShift, "Max Hours Per Shift", CDec(12))
    ''' <summary>
    ''' Gets the Max Hours Per Shift value
    ''' </summary>
    <Display(Name:="Max Hours Per Shift", Description:="")>
  Public ReadOnly Property MaxHoursPerShift() As Decimal
      Get
        Return GetProperty(MaxHoursPerShiftProperty)
      End Get
    End Property

    Public Shared MinShortfallBetweenShiftsProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.MinShortfallBetweenShifts, "Min Shortfall Between Shifts", 0D)
    ''' <summary>
    ''' Gets the Min Shortfall Between Shifts value
    ''' </summary>
    <Display(Name:="Min Shortfall Between Shifts", Description:="")>
  Public ReadOnly Property MinShortfallBetweenShifts() As Decimal
      Get
        Return GetProperty(MinShortfallBetweenShiftsProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSystemProductionAreaSetting(dr As SafeDataReader) As ROSystemProductionAreaSetting

      Dim r As New ROSystemProductionAreaSetting()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemProductionAreaSettingIDProperty, .GetInt32(0))
        LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(EffectiveDateProperty, .GetValue(2))
        LoadProperty(MaxHoursBeforeApprovalRequiredProperty, .GetInt32(3))
        LoadProperty(MaxHoursPerShiftProperty, .GetDecimal(4))
        LoadProperty(MinShortfallBetweenShiftsProperty, .GetDecimal(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(10)))
        LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(11)))
      End With

    End Sub

#End Region

  End Class

End Namespace