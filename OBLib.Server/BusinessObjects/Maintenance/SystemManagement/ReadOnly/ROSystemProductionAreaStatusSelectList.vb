﻿' Generated 16 May 2016 08:01 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaStatusSelectList
    Inherits OBReadOnlyListBase(Of ROSystemProductionAreaStatusSelectList, ROSystemProductionAreaStatusSelect)

#Region " Business Methods "

    Public Function GetItem(SystemID As Integer) As ROSystemProductionAreaStatusSelect

      For Each child As ROSystemProductionAreaStatusSelect In Me
        If child.SystemID = SystemID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItem(SystemID As Integer, ProductionAreaID As Integer, ProductionAreaStatusID As Integer) As ROSystemProductionAreaStatusSelect

      For Each child As ROSystemProductionAreaStatusSelect In Me
        If child.SystemID = SystemID AndAlso child.ProductionAreaID = ProductionAreaID AndAlso child.ProductionAreaStatusID = ProductionAreaStatusID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property ProductionAreaStatus As String
      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROSystemProductionAreaStatusSelectList() As ROSystemProductionAreaStatusSelectList

      Return New ROSystemProductionAreaStatusSelectList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemProductionAreaStatusSelectList() As ROSystemProductionAreaStatusSelectList

      Return DataPortal.Fetch(Of ROSystemProductionAreaStatusSelectList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemProductionAreaStatusSelect.GetROSystemProductionAreaStatusSelect(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemProductionAreaStatusListSelect"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@ProductionAreaStatus", Strings.MakeEmptyDBNull(crit.ProductionAreaStatus))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace