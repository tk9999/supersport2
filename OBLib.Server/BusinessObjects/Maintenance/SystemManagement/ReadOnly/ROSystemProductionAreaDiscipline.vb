﻿' Generated 10 May 2016 12:05 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaDiscipline
    Inherits OBReadOnlyBase(Of ROSystemProductionAreaDiscipline)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSystemProductionAreaDisciplineBO.ROSystemProductionAreaDisciplineBOToString(self)")

    Public Shared SystemProductionAreaDisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaDisciplineID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SystemProductionAreaDisciplineID() As Integer
      Get
        Return GetProperty(SystemProductionAreaDisciplineIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaID, "System Production Area", Nothing)
    ''' <summary>
    ''' Gets the System Production Area value
    ''' </summary>
    <Display(Name:="System Production Area", Description:="")>
  Public ReadOnly Property SystemProductionAreaID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
  Public ReadOnly Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared IsShiftBasedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsShiftBased, "Is Shift Based", False)
    ''' <summary>
    ''' Gets the Is Shift Based value
    ''' </summary>
    <Display(Name:="Is Shift Based", Description:="")>
  Public ReadOnly Property IsShiftBased() As Boolean
      Get
        Return GetProperty(IsShiftBasedProperty)
      End Get
    End Property

    Public Shared DisciplineOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineOrder, "Discipline Order", 0)
    ''' <summary>
    ''' Gets the Discipline Order value
    ''' </summary>
    <Display(Name:="Discipline Order", Description:="")>
  Public ReadOnly Property DisciplineOrder() As Integer
      Get
        Return GetProperty(DisciplineOrderProperty)
      End Get
    End Property

    Public Shared PositionTypeRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PositionTypeRequired, "Position Type Required", False)
    ''' <summary>
    ''' Gets the Position Type Required value
    ''' </summary>
    <Display(Name:="Position Type Required", Description:="")>
  Public ReadOnly Property PositionTypeRequired() As Boolean
      Get
        Return GetProperty(PositionTypeRequiredProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets the Is Shift Based value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

#End Region

    '#Region " Child Lists "

    '    Public Shared ROSystemProductionAreaDisciplinePositionTypeListProperty As PropertyInfo(Of ROSystemProductionAreaDisciplinePositionTypeList) = RegisterProperty(Of ROSystemProductionAreaDisciplinePositionTypeList)(Function(c) c.ROSystemProductionAreaDisciplinePositionTypeList, "RO System Production Area Discipline Position Type List")

    '    Public ReadOnly Property ROSystemProductionAreaDisciplinePositionTypeList() As ROSystemProductionAreaDisciplinePositionTypeList
    '      Get
    '        If GetProperty(ROSystemProductionAreaDisciplinePositionTypeListProperty) Is Nothing Then
    '          LoadProperty(ROSystemProductionAreaDisciplinePositionTypeListProperty, Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplinePositionTypeList.NewROSystemProductionAreaDisciplinePositionTypeList())
    '        End If
    '        Return GetProperty(ROSystemProductionAreaDisciplinePositionTypeListProperty)
    '      End Get
    '    End Property

    '#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaDisciplineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSystemProductionAreaDiscipline(dr As SafeDataReader) As ROSystemProductionAreaDiscipline

      Dim r As New ROSystemProductionAreaDiscipline()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemProductionAreaDisciplineIDProperty, .GetInt32(0))
        LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(IsShiftBasedProperty, .GetBoolean(3))
        LoadProperty(DisciplineOrderProperty, .GetInt32(4))
        LoadProperty(PositionTypeRequiredProperty, .GetBoolean(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(DisciplineProperty, .GetString(10))
        LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(11)))
        LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(12)))
      End With

    End Sub

#End Region

  End Class

End Namespace