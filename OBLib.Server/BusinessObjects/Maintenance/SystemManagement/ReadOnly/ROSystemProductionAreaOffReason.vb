﻿' Generated 13 Aug 2016 15:50 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaOffReason
    Inherits OBReadOnlyBase(Of ROSystemProductionAreaOffReason)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSystemProductionAreaOffReasonBO.ROSystemProductionAreaOffReasonBOToString(self)")

    Public Shared SystemProductionAreaOffReasonIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaOffReasonID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemProductionAreaOffReasonID() As Integer
      Get
        Return GetProperty(SystemProductionAreaOffReasonIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaID, "System Production Area", 0)
    ''' <summary>
    ''' Gets the System Production Area value
    ''' </summary>
    <Display(Name:="System Production Area", Description:="")>
    Public ReadOnly Property SystemProductionAreaID() As Integer
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
    End Property

    Public Shared OffReasonIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OffReasonID, "Off Reason", 0)
    ''' <summary>
    ''' Gets the Off Reason value
    ''' </summary>
    <Display(Name:="Off Reason", Description:="")>
    Public ReadOnly Property OffReasonID() As Integer
      Get
        Return GetProperty(OffReasonIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared OffReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OffReason, "Off Reason", "")
    ''' <summary>
    ''' Gets the Off Reason value
    ''' </summary>
    <Display(Name:="Off Reason", Description:="")>
    Public ReadOnly Property OffReason() As String
      Get
        Return GetProperty(OffReasonProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaOffReasonIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSystemProductionAreaOffReason(dr As SafeDataReader) As ROSystemProductionAreaOffReason

      Dim r As New ROSystemProductionAreaOffReason()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemProductionAreaOffReasonIDProperty, .GetInt32(0))
        LoadProperty(SystemProductionAreaIDProperty, .GetInt32(1))
        LoadProperty(OffReasonIDProperty, .GetInt32(2))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(OffReasonProperty, .GetString(7))
      End With

    End Sub

#End Region

  End Class

End Namespace