﻿' Generated 29 May 2017 12:35 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaChannelDefaultList
    Inherits OBReadOnlyListBase(Of ROSystemProductionAreaChannelDefaultList, ROSystemProductionAreaChannelDefault)

#Region " Business Methods "

    Public Function GetItem(SystemProductionAreaChannelDefaultID As Integer) As ROSystemProductionAreaChannelDefault

      For Each child As ROSystemProductionAreaChannelDefault In Me
        If child.SystemProductionAreaChannelDefaultID = SystemProductionAreaChannelDefaultID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO System Production Area Channel Defaults"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ChannelID As Object = Nothing
      Public Property SystemProductionAreaID As Object = Nothing

      Public Sub New(ChannelID As Object, SystemProductionAreaID As Object)

        Me.ChannelID = ChannelID
        Me.SystemProductionAreaID = SystemProductionAreaID

      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROSystemProductionAreaChannelDefaultList() As ROSystemProductionAreaChannelDefaultList

      Return New ROSystemProductionAreaChannelDefaultList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSystemProductionAreaChannelDefaultList() As ROSystemProductionAreaChannelDefaultList

      Return DataPortal.Fetch(Of ROSystemProductionAreaChannelDefaultList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemProductionAreaChannelDefault.GetROSystemProductionAreaChannelDefault(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemProductionAreaChannelDefaultList"
            cm.Parameters.AddWithValue("@ChannelID", Singular.Misc.NothingDBNull(crit.ChannelID))
            cm.Parameters.AddWithValue("@SystemProductionAreaID", Singular.Misc.NothingDBNull(crit.SystemProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace