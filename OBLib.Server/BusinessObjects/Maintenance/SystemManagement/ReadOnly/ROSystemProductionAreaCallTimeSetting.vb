﻿' Generated 26 May 2016 09:49 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemProductionAreaCallTimeSetting
    Inherits OBReadOnlyBase(Of ROSystemProductionAreaCallTimeSetting)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSystemProductionAreaCallTimeSettingBO.ROSystemProductionAreaCallTimeSettingBOToString(self)")

    Public Shared SystemProductionAreaCallTimeSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaCallTimeSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemProductionAreaCallTimeSettingID() As Integer
      Get
        Return GetProperty(SystemProductionAreaCallTimeSettingIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaID, "System Production Area", Nothing)
    ''' <summary>
    ''' Gets the System Production Area value
    ''' </summary>
    <Display(Name:="System Production Area", Description:="")>
    Public ReadOnly Property SystemProductionAreaID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:="")>
    Public ReadOnly Property EffectiveDate As Date
      Get
        Return GetProperty(EffectiveDateProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared CallTimeMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CallTimeMinutes, "Call Time Minutes", 0)
    ''' <summary>
    ''' Gets the Call Time Minutes value
    ''' </summary>
    <Display(Name:="Call Time Minutes", Description:="")>
    Public ReadOnly Property CallTimeMinutes() As Integer
      Get
        Return GetProperty(CallTimeMinutesProperty)
      End Get
    End Property

    Public Shared WrapTimeMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WrapTimeMinutes, "Wrap Time Minutes", 0)
    ''' <summary>
    ''' Gets the Wrap Time Minutes value
    ''' </summary>
    <Display(Name:="Wrap Time Minutes", Description:="")>
    Public ReadOnly Property WrapTimeMinutes() As Integer
      Get
        Return GetProperty(WrapTimeMinutesProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared RoomCallTimeRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomCallTimeRequired, "Room Call Time Required", True)
    ''' <summary>
    ''' Gets the Room Call Time Required value
    ''' </summary>
    <Display(Name:="Room Call Time Required", Description:="")>
    Public ReadOnly Property RoomCallTimeRequired() As Boolean
      Get
        Return GetProperty(RoomCallTimeRequiredProperty)
      End Get
    End Property

    Public Shared RoomWrapTimeRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomWrapTimeRequired, "Room Wrap Time Required", True)
    ''' <summary>
    ''' Gets the Room Wrap Time Required value
    ''' </summary>
    <Display(Name:="Room Wrap Time Required", Description:="")>
    Public ReadOnly Property RoomWrapTimeRequired() As Boolean
      Get
        Return GetProperty(RoomWrapTimeRequiredProperty)
      End Get
    End Property

    Public Shared EffectiveEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EffectiveEndDate, "Effective End Date")
    ''' <summary>
    ''' Gets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:="")>
    Public ReadOnly Property EffectiveEndDate As Date
      Get
        Return GetProperty(EffectiveEndDateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaCallTimeSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSystemProductionAreaCallTimeSetting(dr As SafeDataReader) As ROSystemProductionAreaCallTimeSetting

      Dim r As New ROSystemProductionAreaCallTimeSetting()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemProductionAreaCallTimeSettingIDProperty, .GetInt32(0))
        LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(EffectiveDateProperty, .GetValue(2))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(CallTimeMinutesProperty, .GetInt32(4))
        LoadProperty(WrapTimeMinutesProperty, .GetInt32(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(RoomCallTimeRequiredProperty, .GetBoolean(12))
        LoadProperty(RoomWrapTimeRequiredProperty, .GetBoolean(13))
        LoadProperty(EffectiveEndDateProperty, .GetValue(14))
      End With

    End Sub

#End Region

  End Class

End Namespace