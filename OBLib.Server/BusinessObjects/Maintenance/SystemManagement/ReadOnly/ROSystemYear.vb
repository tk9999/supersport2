﻿' Generated 25 Apr 2016 14:29 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemYear
    Inherits OBReadOnlyBase(Of ROSystemYear)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSystemYearBO.ROSystemYearBOToString(self)")

    Public Shared SystemYearIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemYearID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemYearID() As Integer
      Get
        Return GetProperty(SystemYearIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared YearStartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.YearStartDate, "Year Start Date")
    ''' <summary>
    ''' Gets the Year Start Date value
    ''' </summary>
    <Display(Name:="Year Start Date", Description:="")>
    Public ReadOnly Property YearStartDate As Date
      Get
        Return GetProperty(YearStartDateProperty)
      End Get
    End Property

    Public Shared YearEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.YearEndDate, "Year End Date")
    ''' <summary>
    ''' Gets the Year End Date value
    ''' </summary>
    <Display(Name:="Year End Date", Description:="")>
    Public ReadOnly Property YearEndDate As Date
      Get
        Return GetProperty(YearEndDateProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared SubDeptProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.SubDept, "")
    <Display(Name:="Sub-Dept")>
    Public ReadOnly Property SubDept() As String
      Get
        Return GetProperty(SubDeptProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CreatedByName, "")
    <Display(Name:="Created By")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared ModifiedByNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ModifiedByName, "")
    <Display(Name:="Modified By")>
    Public ReadOnly Property ModifiedByName() As String
      Get
        Return GetProperty(ModifiedByNameProperty)
      End Get
    End Property

    <Display(Name:="Create Details")>
    Public ReadOnly Property CreateDetails() As String
      Get
        Return CreatedByName & " on " & CreatedDateTime.Date.ToString("ddd dd MMM yyyy HH:mm")
      End Get
    End Property

    <Display(Name:="Modify Details")>
    Public ReadOnly Property ModDetails() As String
      Get
        Return ModifiedByName & " on " & ModifiedDateTime.Date.ToString("ddd dd MMM yyyy HH:mm")
      End Get
    End Property

    Public Shared YearNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.YearName, "")
    <Display(Name:="Year")>
    Public ReadOnly Property YearName() As String
      Get
        Return GetProperty(YearNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemYearIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SubDept

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSystemYear(dr As SafeDataReader) As ROSystemYear

      Dim r As New ROSystemYear()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemYearIDProperty, .GetInt32(0))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(YearStartDateProperty, .GetValue(2))
        LoadProperty(YearEndDateProperty, .GetValue(3))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(SubDeptProperty, .GetString(8))
        LoadProperty(CreatedByNameProperty, .GetString(9))
        LoadProperty(ModifiedByNameProperty, .GetString(10))
        LoadProperty(YearNameProperty, .GetString(12))
      End With

    End Sub

#End Region

  End Class

End Namespace