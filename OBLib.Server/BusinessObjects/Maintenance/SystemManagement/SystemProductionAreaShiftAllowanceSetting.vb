﻿' Generated 09 May 2017 16:07 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.DataAnnotations
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class SystemProductionAreaShiftAllowanceSetting
    Inherits OBBusinessBase(Of SystemProductionAreaShiftAllowanceSetting)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemProductionAreaShiftAllowanceSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemProductionAreaShiftAllowanceSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SystemProductionAreaShiftAllowanceSettingID() As Integer
      Get
        Return GetProperty(SystemProductionAreaShiftAllowanceSettingIDProperty)
      End Get
    End Property

    Public Shared ShiftAllowanceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ShiftAllowanceTypeID, "Shift Allowance Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Shift Allowance Type value
    ''' </summary>
    <Display(Name:="Shift Allowance Type", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(ROShiftAllowanceTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property ShiftAllowanceTypeID() As Integer?
      Get
        Return GetProperty(ShiftAllowanceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ShiftAllowanceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets and sets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:=""),
    Required(ErrorMessage:="Effective Date required")>
  Public Property EffectiveDate As Date
      Get
        Return GetProperty(EffectiveDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(EffectiveDateProperty, Value)
      End Set
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaID, "System Production Area", Nothing)
    ''' <summary>
    ''' Gets the System Production Area value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property SystemProductionAreaID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
    End Property

    Public Shared QualifyingStartTimeStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QualifyingStartTimeStart, "Qualifying Start Time Start")
    ''' <summary>
    ''' Gets and sets the Qualifying Start Time Start value
    ''' </summary>
    <Display(Name:="Qualifying Start Time Start", Description:="")>
    Public Property QualifyingStartTimeStart() As DateTime?
      Get
        Dim value = GetProperty(QualifyingStartTimeStartProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value
        End If
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(QualifyingStartTimeStartProperty, Value)
      End Set
    End Property

    Public Shared QualifyingStartTimeEndProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QualifyingStartTimeEnd, "Qualifying Start Time End")
    ''' <summary>
    ''' Gets and sets the Qualifying Start Time End value
    ''' </summary>
    <Display(Name:="Qualifying Start Time End", Description:="")>
    Public Property QualifyingStartTimeEnd() As DateTime?
      Get
        Dim value = GetProperty(QualifyingStartTimeEndProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value
        End If
      End Get
      Set(ByVal Value As DateTime?)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(QualifyingStartTimeEndProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(QualifyingStartTimeEndProperty, DateTime.MinValue)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(QualifyingStartTimeEndProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared QualifyingEndTimeStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QualifyingEndTimeStart, "Qualifying End Time Start")
    ''' <summary>
    ''' Gets and sets the Qualifying End Time Start value
    ''' </summary>
    <Display(Name:="Qualifying End Time Start", Description:="")>
    Public Property QualifyingEndTimeStart() As DateTime?
      Get
        Dim value = GetProperty(QualifyingEndTimeStartProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value
        End If
      End Get
      Set(ByVal Value As DateTime?)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(QualifyingEndTimeStartProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(QualifyingEndTimeStartProperty, DateTime.MinValue)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(QualifyingEndTimeStartProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared QualifyingEndTimeEndProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QualifyingEndTimeEnd, "Qualifying End Time End")
    ''' <summary>
    ''' Gets and sets the Qualifying End Time End value
    ''' </summary>
    <Display(Name:="Qualifying End Time End", Description:="")>
    Public Property QualifyingEndTimeEnd() As DateTime?
      Get
        Dim value = GetProperty(QualifyingEndTimeEndProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value
        End If
      End Get
      Set(ByVal Value As DateTime?)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(QualifyingEndTimeEndProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(QualifyingEndTimeEndProperty, DateTime.MinValue)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(QualifyingEndTimeEndProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared RankingProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Ranking, "Ranking", 0)
    ''' <summary>
    ''' Gets and sets the Ranking value
    ''' </summary>
    <Display(Name:="Ranking", Description:=""),
    Required(ErrorMessage:="Ranking required")>
  Public Property Ranking() As Integer
      Get
        Return GetProperty(RankingProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RankingProperty, Value)
      End Set
    End Property

    Public Shared AllowanceAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AllowanceAmount, "Allowance Amount", CDec(0))
    ''' <summary>
    ''' Gets and sets the Allowance Amount value
    ''' </summary>
    <Display(Name:="Allowance Amount", Description:=""),
    Required(ErrorMessage:="Allowance Amount required")>
  Public Property AllowanceAmount() As Decimal
      Get
        Return GetProperty(AllowanceAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AllowanceAmountProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Modified Date Time", Description:=""), DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "LoginName", "")
    ''' <summary>
    ''' Gets and sets the Discipline Name
    ''' </summary>
    <Display(Name:="Last Modified", Description:="Shows the last user to modify this record.")>
    Public Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LoginNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemProductionArea

      Return CType(CType(Me.Parent, SystemProductionAreaShiftAllowanceSettingList).Parent, SystemProductionArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemProductionAreaShiftAllowanceSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SystemProductionAreaShiftAllowanceSettingID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area Shift Allowance Setting")
        Else
          Return String.Format("Blank {0}", "System Production Area Shift Allowance Setting")
        End If
      Else
        Return Me.SystemProductionAreaShiftAllowanceSettingID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      ' Unknown Rule - DB Constraint Text = "(([QualifyingStartTimeStart] IS NOT NULL OR [QualifyingStartTimeEnd] IS NOT NULL) AND NOT ([QualifyingEndTimeStart] IS NOT NULL OR [QualifyingEndTimeEnd] IS NOT NULL) OR NOT ([QualifyingStartTimeStart] IS NOT NULL OR [QualifyingStartTimeEnd] IS NOT NULL) AND ([QualifyingEndTimeStart] IS NOT NULL OR [QualifyingEndTimeEnd] IS NOT NULL))"

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemProductionAreaShiftAllowanceSetting() method.

    End Sub

    Public Shared Function NewSystemProductionAreaShiftAllowanceSetting() As SystemProductionAreaShiftAllowanceSetting

      Return DataPortal.CreateChild(Of SystemProductionAreaShiftAllowanceSetting)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSystemProductionAreaShiftAllowanceSetting(dr As SafeDataReader) As SystemProductionAreaShiftAllowanceSetting

      Dim s As New SystemProductionAreaShiftAllowanceSetting()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemProductionAreaShiftAllowanceSettingIDProperty, .GetInt32(0))
          LoadProperty(ShiftAllowanceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EffectiveDateProperty, .GetValue(2))
          LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          If .IsDBNull(4) Then
            LoadProperty(QualifyingStartTimeStartProperty, DateTime.MinValue)
          Else
            LoadProperty(QualifyingStartTimeStartProperty, (New DateTime(2000, 1, 1).Add(.GetValue(4))))
          End If
          If .IsDBNull(5) Then
            LoadProperty(QualifyingStartTimeEndProperty, DateTime.MinValue)
          Else
            LoadProperty(QualifyingStartTimeEndProperty, (New DateTime(2000, 1, 1).Add(.GetValue(5))))
          End If
          If .IsDBNull(6) Then
            LoadProperty(QualifyingEndTimeStartProperty, DateTime.MinValue)
          Else
            LoadProperty(QualifyingEndTimeStartProperty, (New DateTime(2000, 1, 1).Add(.GetValue(6))))
          End If
          If .IsDBNull(7) Then
            LoadProperty(QualifyingEndTimeEndProperty, DateTime.MinValue)
          Else
            LoadProperty(QualifyingEndTimeEndProperty, (New DateTime(2000, 1, 1).Add(.GetValue(7))))
          End If
          LoadProperty(RankingProperty, .GetInt32(8))
          LoadProperty(AllowanceAmountProperty, .GetDecimal(9))
          LoadProperty(CreatedByProperty, .GetInt32(10))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(11))
          LoadProperty(ModifiedByProperty, .GetInt32(12))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(13))
          LoadProperty(LoginNameProperty, .GetString(14))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemProductionAreaShiftAllowanceSettingIDProperty)

      cm.Parameters.AddWithValue("@ShiftAllowanceTypeID", GetProperty(ShiftAllowanceTypeIDProperty))
      cm.Parameters.AddWithValue("@EffectiveDate", EffectiveDate)
      cm.Parameters.AddWithValue("@SystemProductionAreaID", Me.GetParent().SystemProductionAreaID)
      cm.Parameters.AddWithValue("@QualifyingStartTimeStart", Singular.Misc.NothingDBNull(QualifyingStartTimeStart))
      cm.Parameters.AddWithValue("@QualifyingStartTimeEnd", Singular.Misc.NothingDBNull(QualifyingStartTimeEnd))
      cm.Parameters.AddWithValue("@QualifyingEndTimeStart", Singular.Misc.NothingDBNull(QualifyingEndTimeStart))
      cm.Parameters.AddWithValue("@QualifyingEndTimeEnd", Singular.Misc.NothingDBNull(QualifyingEndTimeEnd))
      cm.Parameters.AddWithValue("@Ranking", GetProperty(RankingProperty))
      cm.Parameters.AddWithValue("@AllowanceAmount", GetProperty(AllowanceAmountProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemProductionAreaShiftAllowanceSettingIDProperty, cm.Parameters("@SystemProductionAreaShiftAllowanceSettingID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemProductionAreaShiftAllowanceSettingID", GetProperty(SystemProductionAreaShiftAllowanceSettingIDProperty))
    End Sub

#End Region

  End Class

End Namespace