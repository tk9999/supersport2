﻿' Generated 10 May 2016 15:21 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class DisciplinePosition
    Inherits OBBusinessBase(Of DisciplinePosition)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets the System Production Area Discipline value
    ''' </summary>
    Public Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(DisciplineIDProperty, value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionID, "Position")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public Property PositionID() As Integer
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DisciplineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.DisciplineID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Production Area Discipline Position Type Select")
        Else
          Return String.Format("Blank {0}", "System Production Area Discipline Position Type Select")
        End If
      Else
        Return Me.DisciplineID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewDisciplinePosition() method.

    End Sub

    Public Shared Function NewDisciplinePosition() As DisciplinePosition

      Return DataPortal.CreateChild(Of DisciplinePosition)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetDisciplinePosition(dr As SafeDataReader) As DisciplinePosition

      Dim s As New DisciplinePosition()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(DisciplineIDProperty, .GetInt32(0))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      cm.Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
      cm.Parameters.AddWithValue("@PositionID", GetProperty(PositionIDProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(DisciplineIDProperty, cm.Parameters("@DisciplineID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
    End Sub

#End Region

  End Class

End Namespace