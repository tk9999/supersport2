﻿' Generated 15 May 2017 11:57 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.SystemManagement

  <Serializable()> _
  Public Class ROShiftAllowanceType
    Inherits OBReadOnlyBase(Of ROShiftAllowanceType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ShiftAllowanceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftAllowanceTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ShiftAllowanceTypeID() As Integer
      Get
        Return GetProperty(ShiftAllowanceTypeIDProperty)
      End Get
    End Property

    Public Shared ShiftAllowanceTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftAllowanceType, "Shift Allowance Type", "")
    ''' <summary>
    ''' Gets the Shift Allowance Type value
    ''' </summary>
    <Display(Name:="Shift Allowance Type", Description:="Description or Name of the allowance")>
  Public ReadOnly Property ShiftAllowanceType() As String
      Get
        Return GetProperty(ShiftAllowanceTypeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DisplayNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DisplayName, "Display Name", "")
    ''' <summary>
    ''' Gets the Display Name value
    ''' </summary>
    <Display(Name:="Display Name", Description:="")>
  Public ReadOnly Property DisplayName() As String
      Get
        Return GetProperty(DisplayNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ShiftAllowanceTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ShiftAllowanceType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROShiftAllowanceType(dr As SafeDataReader) As ROShiftAllowanceType

      Dim r As New ROShiftAllowanceType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ShiftAllowanceTypeIDProperty, .GetInt32(0))
        LoadProperty(ShiftAllowanceTypeProperty, .GetString(1))
        LoadProperty(CreatedByProperty, .GetInt32(2))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
        LoadProperty(ModifiedByProperty, .GetInt32(4))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(DisplayNameProperty, .GetString(6))
      End With

    End Sub

#End Region

  End Class

End Namespace