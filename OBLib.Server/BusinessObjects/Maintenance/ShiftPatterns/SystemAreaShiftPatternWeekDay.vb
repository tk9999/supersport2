﻿' Generated 18 Aug 2014 09:36 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ShiftPatterns.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.ShiftPatterns

  <Serializable()> _
  Public Class SystemAreaShiftPatternWeekDay
    Inherits OBBusinessBase(Of SystemAreaShiftPatternWeekDay)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemAreaShiftPatternWeekDayBO.SystemAreaShiftPatternWeekDayBOToString(self)")

    Public Shared SystemAreaShiftPatternWeekDayIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftPatternWeekDayID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemAreaShiftPatternWeekDayID() As Integer
      Get
        Return GetProperty(SystemAreaShiftPatternWeekDayIDProperty)
      End Get
    End Property

    Public Shared SystemAreaShiftPatternIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemAreaShiftPatternID, "System Area Shift Pattern", Nothing)
    ''' <summary>
    ''' Gets the System Area Shift Pattern value
    ''' </summary>
    Public Property SystemAreaShiftPatternID() As Integer?
      Get
        Return GetProperty(SystemAreaShiftPatternIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemAreaShiftPatternIDProperty, Value)
      End Set
    End Property

    Public Shared WeekNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WeekNo, "Week No", 0)
    ''' <summary>
    ''' Gets and sets the Week No value
    ''' </summary>
    <Display(Name:="Week No", Description:="The week of the pattern, i.e week 1, 2, 3 etc")>
    Public Property WeekNo() As Integer
      Get
        Return GetProperty(WeekNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WeekNoProperty, Value)
      End Set
    End Property

    Public Shared WeekDayProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.WeekDay, "Week Day", Nothing)
    ''' <summary>
    ''' Gets and sets the Week Day value
    ''' </summary>
    <Display(Name:="Week Day", Description:="The week day of the pattern, i.e. Mon,Tue,Wed"),
    Required(ErrorMessage:="Week Day required"),
    DropDownWeb(GetType(ROShiftPatternWeekDayList),
                DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
                FilterMethodName:="SystemAreaShiftPatternWeekDayBO.AllowedWeekDays",
                ValueMember:="WeekDay", DisplayMember:="WeekDayName")>
    Public Property WeekDay() As Integer?
      Get
        Return GetProperty(WeekDayProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(WeekDayProperty, Value)
      End Set
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.StartTime, "Start Time")
    ''' <summary>
    ''' Gets and sets the Start Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:="The start time of the shift"),
    TimeField(TimeFormat:=TimeFormats.ShortTime),
    Required(ErrorMessage:="Start Time is required")>
    Public Property StartTime() As Object
      Get
        Dim value = GetProperty(StartTimeProperty)
        If value = DateTime.MinValue Or IsNullNothing(value) Then
          Return Nothing
        Else
          Return CDate(value).ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(StartTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(StartTimeProperty, Nothing)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(StartTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.EndTime, "End Time")
    ''' <summary>
    ''' Gets and sets the End Time value
    ''' </summary>
    <Display(Name:="End Time", Description:="The end time of the shift"),
    TimeField(TimeFormat:=TimeFormats.ShortTime),
    Required(ErrorMessage:="End Time is required")>
    Public Property EndTime() As Object
      Get
        Dim value = GetProperty(EndTimeProperty)
        If value = DateTime.MinValue Or IsNullNothing(value) Then
          Return Nothing
        Else
          Return CDate(value).ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(EndTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(EndTimeProperty, Nothing)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(EndTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared OffDayProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OffDay, "Off Day", False)
    ''' <summary>
    ''' Gets and sets the Off Day value
    ''' </summary>
    <Display(Name:="Off Day", Description:="Is this an off day"),
    Required(ErrorMessage:="Off Day required")>
    Public Property OffDay() As Boolean
      Get
        Return GetProperty(OffDayProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OffDayProperty, Value)
      End Set
    End Property

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ShiftTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="Indicator for day shift, night shift or late night shift "),
    Required(ErrorMessage:="Shift Type required"), Browsable(True),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSystemAreaShiftTypeList),
      Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel,
      ValueMember:="ShiftTypeID", DisplayMember:="ShiftType",
      FilterMethodName:="SystemAreaShiftPatternWeekDayBO.GetAllowedSystemAreaShiftType"),
    SetExpression("SystemAreaShiftPatternWeekDayBO.SetStartEndTimeFromShiftType(self)")>
    Public Property ShiftTypeID() As Integer?
      Get
        Return GetProperty(ShiftTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ShiftTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared ModifiedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ModifiedByName, "Modified By", "")
    Public ReadOnly Property ModifiedByName() As String
      Get
        Return GetProperty(ModifiedByNameProperty)
      End Get
    End Property

    <Display(Name:="Creation Details")>
    Public ReadOnly Property CreateDetails() As String
      Get
        Return CreatedByName & " on " & CreatedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm")
      End Get
    End Property

    <Display(Name:="Modified Details")>
    Public ReadOnly Property ModifiedDetails() As String
      Get
        Return ModifiedByName & " on " & ModifiedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm")
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemAreaShiftPattern

      Return CType(CType(Me.Parent, SystemAreaShiftPatternWeekDayList).Parent, SystemAreaShiftPattern)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaShiftPatternWeekDayIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Area Shift Pattern Week Day")
        Else
          Return String.Format("Blank {0}", "System Area Shift Pattern Week Day")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemAreaShiftPatternWeekDay() method.

    End Sub

    Public Shared Function NewSystemAreaShiftPatternWeekDay() As SystemAreaShiftPatternWeekDay

      Return DataPortal.CreateChild(Of SystemAreaShiftPatternWeekDay)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSystemAreaShiftPatternWeekDay(dr As SafeDataReader) As SystemAreaShiftPatternWeekDay

      Dim s As New SystemAreaShiftPatternWeekDay()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemAreaShiftPatternWeekDayIDProperty, .GetInt32(0))
          LoadProperty(SystemAreaShiftPatternIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(WeekNoProperty, .GetInt32(2))
          LoadProperty(WeekDayProperty, .GetInt32(3))
          If .IsDBNull(4) Then
            LoadProperty(StartTimeProperty, DateTime.MinValue)
          Else
            LoadProperty(StartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(4))))
          End If
          If .IsDBNull(5) Then
            LoadProperty(EndTimeProperty, DateTime.MinValue)
          Else
            LoadProperty(EndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(5))))
          End If
          LoadProperty(OffDayProperty, .GetBoolean(6))
          LoadProperty(ShiftTypeIDProperty, .GetInt32(7))
          'LoadProperty(ExtraShiftIndProperty, .GetInt32(8))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetValue(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetValue(11))
          LoadProperty(CreatedByNameProperty, .GetString(12))
          LoadProperty(ModifiedByNameProperty, .GetString(13))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSystemAreaShiftPatternWeekDay"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSystemAreaShiftPatternWeekDay"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSystemAreaShiftPatternWeekDayID As SqlParameter = .Parameters.Add("@SystemAreaShiftPatternWeekDayID", SqlDbType.Int)
          paramSystemAreaShiftPatternWeekDayID.Value = GetProperty(SystemAreaShiftPatternWeekDayIDProperty)
          If Me.IsNew Then
            paramSystemAreaShiftPatternWeekDayID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SystemAreaShiftPatternID", Me.GetParent().SystemAreaShiftPatternID)
          .Parameters.AddWithValue("@WeekNo", GetProperty(WeekNoProperty))
          .Parameters.AddWithValue("@WeekDay", GetProperty(WeekDayProperty))
          .Parameters.AddWithValue("@StartTime", (New SmartDate(GetProperty(StartTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndTime", (New SmartDate(GetProperty(EndTimeProperty))).DBValue)
          .Parameters.AddWithValue("@OffDay", GetProperty(OffDayProperty))
          .Parameters.AddWithValue("@ShiftTypeID", GetProperty(ShiftTypeIDProperty))
          '.Parameters.AddWithValue("@ExtraShiftInd", GetProperty(ExtraShiftIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SystemAreaShiftPatternWeekDayIDProperty, paramSystemAreaShiftPatternWeekDayID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSystemAreaShiftPatternWeekDay"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SystemAreaShiftPatternWeekDayID", GetProperty(SystemAreaShiftPatternWeekDayIDProperty))
        cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace