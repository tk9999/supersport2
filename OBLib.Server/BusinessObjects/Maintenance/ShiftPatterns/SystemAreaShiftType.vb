﻿' Generated 13 Oct 2014 11:09 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ShiftPatterns

  <Serializable()> _
  Public Class SystemAreaShiftType
    Inherits SingularBusinessBase(Of SystemAreaShiftType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemAreaShiftTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemAreaShiftTypeID() As Integer
      Get
        Return GetProperty(SystemAreaShiftTypeIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required(ErrorMessage:="System required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:=""),
    Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ShiftTypeID, "Shift Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:=""),
    Required(ErrorMessage:="Shift Type required")>
    Public Property ShiftTypeID() As Integer?
      Get
        Return GetProperty(ShiftTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ShiftTypeIDProperty, Value)
      End Set
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartTime, "Start Time")
    ''' <summary>
    ''' Gets and sets the Start Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:="Start time of the shift type")>
    Public Property StartTime() As Object
      Get
        Dim value = GetProperty(StartTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(StartTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(StartTimeProperty, DBNull.Value)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(StartTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndTime, "End Time")
    ''' <summary>
    ''' Gets and sets the End Time value
    ''' </summary>
    <Display(Name:="End Time", Description:="End time of the shift type")>
    Public Property EndTime() As Object
      Get
        Dim value = GetProperty(EndTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(EndTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(EndTimeProperty, DBNull.Value)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(EndTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftType, "Shift Type", "")
    ''' <summary>
    ''' Gets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="Shift type name")>
    Public Property ShiftType() As String
      Get
        Return GetProperty(ShiftTypeProperty)
      End Get
      Set(value As String)
        SetProperty(ShiftTypeProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaShiftTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Area Shift Type")
        Else
          Return String.Format("Blank {0}", "System Area Shift Type")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemAreaShiftType() method.

    End Sub

    Public Shared Function NewSystemAreaShiftType() As SystemAreaShiftType

      Return DataPortal.CreateChild(Of SystemAreaShiftType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSystemAreaShiftType(dr As SafeDataReader) As SystemAreaShiftType

      Dim s As New SystemAreaShiftType()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemAreaShiftTypeIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ShiftTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          If .IsDBNull(4) Then
            LoadProperty(StartTimeProperty, DBNull.Value)
          Else
            LoadProperty(StartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(4))))
          End If
          If .IsDBNull(5) Then
            LoadProperty(EndTimeProperty, DBNull.Value)
          Else
            LoadProperty(EndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(5))))
          End If
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(ShiftTypeProperty, .GetString(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSystemAreaShiftType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSystemAreaShiftType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSystemAreaShiftTypeID As SqlParameter = .Parameters.Add("@SystemAreaShiftTypeID", SqlDbType.Int)
          paramSystemAreaShiftTypeID.Value = GetProperty(SystemAreaShiftTypeIDProperty)
          If Me.IsNew Then
            paramSystemAreaShiftTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@ShiftTypeID", GetProperty(ShiftTypeIDProperty))
          cm.Parameters.AddWithValue("@StartTime", (New SmartDate(GetProperty(StartTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@EndTime", (New SmartDate(GetProperty(EndTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SystemAreaShiftTypeIDProperty, paramSystemAreaShiftTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSystemAreaShiftType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SystemAreaShiftTypeID", GetProperty(SystemAreaShiftTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace