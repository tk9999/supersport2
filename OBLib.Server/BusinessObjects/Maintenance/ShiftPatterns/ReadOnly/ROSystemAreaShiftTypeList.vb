﻿' Generated 13 Oct 2014 11:09 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ShiftPatterns.ReadOnly

  <Serializable()> _
  Public Class ROSystemAreaShiftTypeList
    Inherits SingularReadOnlyListBase(Of ROSystemAreaShiftTypeList, ROSystemAreaShiftType)

#Region " Business Methods "

    Public Function GetItem(SystemAreaShiftTypeID As Integer) As ROSystemAreaShiftType

      For Each child As ROSystemAreaShiftType In Me
        If child.SystemAreaShiftTypeID = SystemAreaShiftTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Area Shift Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property ShiftType As String
      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSystemAreaShiftTypeList() As ROSystemAreaShiftTypeList

      Return New ROSystemAreaShiftTypeList()

    End Function

    Public Shared Sub BeginGetROSystemAreaShiftTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSystemAreaShiftTypeList)))

      Dim dp As New DataPortal(Of ROSystemAreaShiftTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSystemAreaShiftTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROSystemAreaShiftTypeList)))

      Dim dp As New DataPortal(Of ROSystemAreaShiftTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSystemAreaShiftTypeList() As ROSystemAreaShiftTypeList

      Return DataPortal.Fetch(Of ROSystemAreaShiftTypeList)(New Criteria())

    End Function

    Public Shared Function GetROSystemAreaShiftTypeList(SystemID As Integer?, ProductionAreaID As Integer?) As ROSystemAreaShiftTypeList

      Return DataPortal.Fetch(Of ROSystemAreaShiftTypeList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemAreaShiftType.GetROSystemAreaShiftType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemAreaShiftTypeList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@ShiftType", Strings.MakeEmptyDBNull(crit.ShiftType))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace