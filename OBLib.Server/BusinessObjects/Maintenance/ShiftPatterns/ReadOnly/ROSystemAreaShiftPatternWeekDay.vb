﻿' Generated 18 Aug 2014 09:36 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ShiftPatterns.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ShiftPatterns.ReadOnly

  <Serializable()> _
  Public Class ROSystemAreaShiftPatternWeekDay
    Inherits OBReadOnlyBase(Of ROSystemAreaShiftPatternWeekDay)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemAreaShiftPatternWeekDayIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftPatternWeekDayID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemAreaShiftPatternWeekDayID() As Integer
      Get
        Return GetProperty(SystemAreaShiftPatternWeekDayIDProperty)
      End Get
    End Property

    Public Shared SystemAreaShiftPatternIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemAreaShiftPatternID, "System Area Shift Pattern", Nothing)
    ''' <summary>
    ''' Gets the System Area Shift Pattern value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemAreaShiftPatternID() As Integer?
      Get
        Return GetProperty(SystemAreaShiftPatternIDProperty)
      End Get
    End Property

    Public Shared WeekNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WeekNo, "Week No", 0)
    ''' <summary>
    ''' Gets and sets the Week No value
    ''' </summary>
    <Display(Name:="Week No", Description:="The week of the pattern, i.e week 1, 2, 3 etc")>
    Public ReadOnly Property WeekNo() As Integer
      Get
        Return GetProperty(WeekNoProperty)
      End Get
    End Property

    Public Shared WeekDayProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.WeekDay, "Week Day", Nothing)
    ''' <summary>
    ''' Gets and sets the Week Day value
    ''' </summary>
    <Display(Name:="Week Day", Description:="The week day of the pattern, i.e. Mon,Tue,Wed")>
    Public ReadOnly Property WeekDay() As Integer?
      Get
        Return GetProperty(WeekDayProperty)
      End Get
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.StartTime, "Start Time")
    ''' <summary>
    ''' Gets and sets the Start Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:="The start time of the shift")>
    Public ReadOnly Property StartTime() As Object
      Get
        Dim value = GetProperty(StartTimeProperty)
        Return CDate(value).ToString("HH:mm")
      End Get
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.EndTime, "End Time")
    ''' <summary>
    ''' Gets and sets the End Time value
    ''' </summary>
    <Display(Name:="End Time", Description:="The end time of the shift")>
    Public ReadOnly Property EndTime() As Object
      Get
        Dim value = GetProperty(EndTimeProperty)
        Return CDate(value).ToString("HH:mm")
      End Get
    End Property

    Public Shared OffDayProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OffDay, "Off Day", False)
    ''' <summary>
    ''' Gets and sets the Off Day value
    ''' </summary>
    <Display(Name:="Off Day", Description:="Is this an off day")>
    Public ReadOnly Property OffDay() As Boolean
      Get
        Return GetProperty(OffDayProperty)
      End Get
    End Property

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ShiftTypeID, "Shift Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="Indicator for day shift, night shift or late night shift ")>
    Public ReadOnly Property ShiftTypeID() As Integer?
      Get
        Return GetProperty(ShiftTypeIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared ModifiedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ModifiedByName, "Modified By", "")
    Public ReadOnly Property ModifiedByName() As String
      Get
        Return GetProperty(ModifiedByNameProperty)
      End Get
    End Property

    <Display(Name:="Creation Details")>
    Public ReadOnly Property CreateDetails() As String
      Get
        Return CreatedByName & " on " & CreatedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm")
      End Get
    End Property

    <Display(Name:="Modified Details")>
    Public ReadOnly Property ModifiedDetails() As String
      Get
        Return ModifiedByName & " on " & ModifiedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm")
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaShiftPatternWeekDayIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemAreaShiftPatternWeekDay(dr As SafeDataReader) As ROSystemAreaShiftPatternWeekDay

      Dim s As New ROSystemAreaShiftPatternWeekDay()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)
      With sdr
        LoadProperty(SystemAreaShiftPatternWeekDayIDProperty, .GetInt32(0))
        LoadProperty(SystemAreaShiftPatternIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(WeekNoProperty, .GetInt32(2))
        LoadProperty(WeekDayProperty, .GetInt32(3))
        If .IsDBNull(4) Then
          LoadProperty(StartTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(StartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(4))))
        End If
        If .IsDBNull(5) Then
          LoadProperty(EndTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(EndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(5))))
        End If
        LoadProperty(OffDayProperty, .GetBoolean(6))
        LoadProperty(ShiftTypeIDProperty, .GetInt32(7))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateTimeProperty, .GetValue(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateTimeProperty, .GetValue(11))
        LoadProperty(CreatedByNameProperty, .GetString(12))
        LoadProperty(ModifiedByNameProperty, .GetString(13))
      End With
    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace