﻿' Generated 19 Aug 2014 10:05 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.ShiftPatterns.ReadOnly

  <Serializable()> _
  Public Class ROSystemAreaShiftPatternList
    Inherits OBReadOnlyListBase(Of ROSystemAreaShiftPatternList, ROSystemAreaShiftPattern)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(SystemAreaShiftPatternID As Integer) As ROSystemAreaShiftPattern

      For Each child As ROSystemAreaShiftPattern In Me
        If child.SystemAreaShiftPatternID = SystemAreaShiftPatternID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Area Shift Patterns"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property SystemAreaShiftPatternID As Integer? = Nothing

      <Display(Name:="Sub-Dept"),
      DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID"),
      SetExpression("ROSystemAreaShiftPatternListCrtieriaBO.SystemIDSet(self)")>
      Public Property SystemID As Integer? = Nothing

      <Display(Name:="Area"),
      DropDownWeb(GetType(ROUserSystemAreaList), ThisFilterMember:="SystemID", DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID"),
      SetExpression("ROSystemAreaShiftPatternListCrtieriaBO.ProductionAreaIDSet(self)")>
      Public Property ProductionAreaID As Integer? = Nothing

      <Display(Name:="Pattern Name", Description:=""),
      TextField(False, False, False, 1),
      SetExpression("TeamManagementPage.fetchSystemAreaShiftPatterns()", , 250)>
      Public Property KeyWord As String = Nothing
      Public Property FetchTemplates As Boolean? = Nothing
      Public Property SystemProductionAreaID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(SystemAreaShiftPatternID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?, KeyWord As String, FetchTemplates As Boolean?, SystemProductionAreaID As Integer?)
        Me.SystemAreaShiftPatternID = SystemAreaShiftPatternID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.KeyWord = KeyWord
        Me.FetchTemplates = FetchTemplates
        Me.SystemProductionAreaID = SystemProductionAreaID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSystemAreaShiftPatternList() As ROSystemAreaShiftPatternList

      Return New ROSystemAreaShiftPatternList()

    End Function

    Public Shared Sub BeginGetROSystemAreaShiftPatternList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSystemAreaShiftPatternList)))

      Dim dp As New DataPortal(Of ROSystemAreaShiftPatternList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSystemAreaShiftPatternList(CallBack As EventHandler(Of DataPortalResult(Of ROSystemAreaShiftPatternList)))

      Dim dp As New DataPortal(Of ROSystemAreaShiftPatternList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSystemAreaShiftPatternList() As ROSystemAreaShiftPatternList

      Return DataPortal.Fetch(Of ROSystemAreaShiftPatternList)(New Criteria())

    End Function

    Public Shared Function GetROSystemAreaShiftPatternList(SystemAreaShiftPatternID, SystemID, ProductionAreaID, KeyWord, FetchTemplates, SystemProductionAreaID) As ROSystemAreaShiftPatternList

      Return DataPortal.Fetch(Of ROSystemAreaShiftPatternList)(New Criteria(SystemAreaShiftPatternID, SystemID, ProductionAreaID, KeyWord, FetchTemplates, SystemProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemAreaShiftPattern.GetROSystemAreaShiftPattern(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROSystemAreaShiftPattern = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemAreaShiftPatternID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROSystemAreaShiftPatternWeekDayList.RaiseListChangedEvents = False
          parent.ROSystemAreaShiftPatternWeekDayList.Add(ROSystemAreaShiftPatternWeekDay.GetROSystemAreaShiftPatternWeekDay(sdr))
          parent.ROSystemAreaShiftPatternWeekDayList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemAreaShiftPatternList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID)) 'OBLib.Security.Settings.CurrentUser.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID)) '.Security.Settings.CurrentUser.ProductionAreaID))
            cm.Parameters.AddWithValue("@KeyWord", Singular.Strings.MakeEmptyDBNull(crit.KeyWord))
            cm.Parameters.AddWithValue("@SystemAreaShiftPatternID", NothingDBNull(crit.SystemAreaShiftPatternID))
            cm.Parameters.AddWithValue("@FetchTemplates", NothingDBNull(crit.FetchTemplates))
            'cm.Parameters.AddWithValue("@SystemProductionAreaID", NothingDBNull(crit.SystemProductionAreaID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace