﻿' Generated 19 Aug 2014 10:05 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ShiftPatterns.ReadOnly

  <Serializable()> _
  Public Class ROSystemAreaShiftPatternTemplate
    Inherits OBReadOnlyBase(Of ROSystemAreaShiftPatternTemplate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemAreaShiftPatternIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftPatternID, "ID", 0)

    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    '''   
    <Key()>
    Public ReadOnly Property SystemAreaShiftPatternID() As Integer
      Get
        Return GetProperty(SystemAreaShiftPatternIDProperty)
      End Get
    End Property

    Public Shared PatternNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PatternName, "Pattern Name", "")
    ''' <summary>
    ''' Gets the Pattern Name value
    ''' </summary>
    <Display(Name:="Pattern Name", Description:="a name which describes the pattern")>
    Public ReadOnly Property PatternName() As String
      Get
        Return GetProperty(PatternNameProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared SystemNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemName, "Sub-Dept", "")
    ''' <summary>
    ''' Gets and sets the System Name value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="Sub-Dept")>
    Public ReadOnly Property SystemName() As String
      Get
        Return GetProperty(SystemNameProperty)
      End Get
    End Property

    Public Shared ProductionAreaNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaName, "Production Area Name", "")
    ''' <summary>
    ''' Gets and sets the Production Area Name value
    ''' </summary>
    <Display(Name:="Area", Description:="Area")>
    Public ReadOnly Property ProductionAreaName() As String
      Get
        Return GetProperty(ProductionAreaNameProperty)
      End Get
    End Property

    'Public Shared HRUsingPatternIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HRUsingPatternInd, "HR Using Pattern")
    ' ''' <summary>
    ' ''' Gets the HR USing Pattern Ind value
    ' ''' </summary>
    'Public ReadOnly Property HRUsingPatternInd() As Boolean
    '  Get
    '    Return GetProperty(HRUsingPatternIndProperty)
    '  End Get
    'End Property

    Public Shared ShiftDurationProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftDuration, "Shift Duration")
    Public ReadOnly Property ShiftDuration() As Integer
      Get
        Return GetProperty(ShiftDurationProperty)
      End Get
    End Property

    'Public Shared IsTemplateProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTemplate, "is Template?")
    'Public ReadOnly Property IsTemplate() As Boolean
    '  Get
    '    Return GetProperty(IsTemplateProperty)
    '  End Get
    'End Property

    'Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    'Public ReadOnly Property CreatedByName() As String
    '  Get
    '    Return GetProperty(CreatedByNameProperty)
    '  End Get
    'End Property

    'Public Shared ModifiedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ModifiedByName, "Modified By", "")
    'Public ReadOnly Property ModifiedByName() As String
    '  Get
    '    Return GetProperty(ModifiedByNameProperty)
    '  End Get
    'End Property

    '<Display(Name:="Creation Details")>
    'Public ReadOnly Property CreateDetails() As String
    '  Get
    '    Return CreatedByName & " on " & CreatedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm")
    '  End Get
    'End Property

    '<Display(Name:="Modified Details")>
    'Public ReadOnly Property ModifiedDetails() As String
    '  Get
    '    Return ModifiedByName & " on " & ModifiedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm")
    '  End Get
    'End Property

    '#Region " Child Lists "

    '    Public Shared ROSystemAreaShiftPatternTemplateWeekDayListProperty As PropertyInfo(Of ROSystemAreaShiftPatternTemplateWeekDayList) = RegisterProperty(Of ROSystemAreaShiftPatternTemplateWeekDayList)(Function(c) c.ROSystemAreaShiftPatternTemplateWeekDayList, "RO System Area Shift Pattern Week Day List")

    '    Public ReadOnly Property ROSystemAreaShiftPatternTemplateWeekDayList() As ROSystemAreaShiftPatternTemplateWeekDayList
    '      Get
    '        If GetProperty(ROSystemAreaShiftPatternTemplateWeekDayListProperty) Is Nothing Then
    '          LoadProperty(ROSystemAreaShiftPatternTemplateWeekDayListProperty, OBLib.Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternTemplateWeekDayList.NewSystemAreaShiftPatternWeekDayList)
    '        End If
    '        Return GetProperty(ROSystemAreaShiftPatternTemplateWeekDayListProperty)
    '      End Get
    '    End Property

    '#End Region


#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaShiftPatternIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.PatternName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemAreaShiftPatternTemplate(dr As SafeDataReader) As ROSystemAreaShiftPatternTemplate

      Dim r As New ROSystemAreaShiftPatternTemplate()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemAreaShiftPatternIDProperty, .GetInt32(0))
        LoadProperty(PatternNameProperty, .GetString(1))
        LoadProperty(SystemIDProperty, .GetInt32(2))
        LoadProperty(ProductionAreaIDProperty, .GetInt32(3))
        'LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        'LoadProperty(StartDateProperty, .GetValue(3))
        'LoadProperty(EndDateProperty, .GetValue(4))
        'LoadProperty(CreatedByProperty, .GetInt32(4))
        'LoadProperty(CreatedDateTimeProperty, .GetValue(5))
        'LoadProperty(ModifiedByProperty, .GetInt32(6))
        'LoadProperty(ModifiedDateTimeProperty, .GetValue(7))
        'LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        'LoadProperty(HRUsingPatternIndProperty, .GetBoolean(9))
        LoadProperty(ShiftDurationProperty, .GetInt32(4))
        LoadProperty(SystemNameProperty, .GetString(5))
        LoadProperty(ProductionAreaNameProperty, .GetString(6))
        'LoadProperty(IsTemplateProperty, .GetBoolean(11))
        'LoadProperty(CreatedByNameProperty, .GetString(12))
        'LoadProperty(ModifiedByNameProperty, .GetString(13))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace