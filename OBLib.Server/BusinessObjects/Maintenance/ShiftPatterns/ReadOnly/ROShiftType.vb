﻿' Generated 22 Sep 2014 17:27 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ShiftPatterns

  <Serializable()> _
  Public Class ROShiftType
    Inherits OBReadOnlyBase(Of ROShiftType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ShiftTypeID() As Integer
      Get
        Return GetProperty(ShiftTypeIDProperty)
      End Get
    End Property

    Public Shared ShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftType, "Shift Type", "")
    ''' <summary>
    ''' Gets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="Shift type name")>
    Public ReadOnly Property ShiftType() As String
      Get
        Return GetProperty(ShiftTypeProperty)
      End Get
    End Property

    'Public Shared StartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.StartTime, "Start Time")
    ' ''' <summary>
    ' ''' Gets the Start Time value
    ' ''' </summary>
    '<Display(Name:="Start Time", Description:="Start time of the shift type")>
    'Public ReadOnly Property StartTime() As Object
    '  Get
    '    Dim value = GetProperty(StartTimeProperty)
    '    If value = DateTime.MinValue Then
    '      Return Nothing
    '    Else
    '      Return value.ToString("HH:mm")
    '    End If
    '  End Get
    'End Property

    'Public Shared EndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.EndTime, "End Time")
    ' ''' <summary>
    ' ''' Gets the End Time value
    ' ''' </summary>
    '<Display(Name:="End Time", Description:="End time of the shift type")>
    'Public ReadOnly Property EndTime() As Object
    '  Get
    '    Dim value = GetProperty(EndTimeProperty)
    '    If value = DateTime.MinValue Then
    '      Return Nothing
    '    Else
    '      Return value.ToString("HH:mm")
    '    End If
    '  End Get
    'End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ShiftTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ShiftType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROShiftType(dr As SafeDataReader) As ROShiftType

      Dim r As New ROShiftType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ShiftTypeIDProperty, .GetInt32(0))
        LoadProperty(ShiftTypeProperty, .GetString(1))
        'If .IsDBNull(2) Then
        '  LoadProperty(StartTimeProperty, DateTime.MinValue)
        'Else
        '  LoadProperty(StartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(2))))
        'End If
        'If .IsDBNull(3) Then
        '  LoadProperty(EndTimeProperty, DateTime.MinValue)
        'Else
        '  LoadProperty(EndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(3))))
        'End If
        LoadProperty(CreatedByProperty, .GetInt32(2))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
        LoadProperty(ModifiedByProperty, .GetInt32(4))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace