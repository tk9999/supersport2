﻿' Generated 18 Aug 2014 09:36 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ShiftPatterns.ReadOnly

  <Serializable()> _
  Public Class ROSystemAreaShiftPatternWeekDayList
    Inherits OBReadOnlyListBase(Of ROSystemAreaShiftPatternWeekDayList, ROSystemAreaShiftPatternWeekDay)

#Region " Business Methods "

    Public Function GetItem(SystemAreaShiftPatternWeekDayID As Integer) As ROSystemAreaShiftPatternWeekDay

      For Each child As ROSystemAreaShiftPatternWeekDay In Me
        If child.SystemAreaShiftPatternWeekDayID = SystemAreaShiftPatternWeekDayID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Area Shift Pattern Week Days"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSystemAreaShiftPatternWeekDayList() As ROSystemAreaShiftPatternWeekDayList

      Return New ROSystemAreaShiftPatternWeekDayList()

    End Function

    Public Shared Sub BeginGetROSystemAreaShiftPatternWeekDayList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSystemAreaShiftPatternWeekDayList)))

      Dim dp As New DataPortal(Of ROSystemAreaShiftPatternWeekDayList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSystemAreaShiftPatternWeekDayList(CallBack As EventHandler(Of DataPortalResult(Of ROSystemAreaShiftPatternWeekDayList)))

      Dim dp As New DataPortal(Of ROSystemAreaShiftPatternWeekDayList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace