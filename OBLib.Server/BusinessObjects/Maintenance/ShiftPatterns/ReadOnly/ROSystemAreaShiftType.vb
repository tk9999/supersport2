﻿' Generated 13 Oct 2014 11:09 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ShiftPatterns.ReadOnly

  <Serializable()> _
  Public Class ROSystemAreaShiftType
    Inherits SingularReadOnlyBase(Of ROSystemAreaShiftType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemAreaShiftTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemAreaShiftTypeID() As Integer
      Get
        Return GetProperty(SystemAreaShiftTypeIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ShiftTypeID, "Shift Type", Nothing)
    ''' <summary>
    ''' Gets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
    Public ReadOnly Property ShiftTypeID() As Integer?
      Get
        Return GetProperty(ShiftTypeIDProperty)
      End Get
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.StartTime, "Start Time")
    ''' <summary>
    ''' Gets the Start Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:="Start time of the shift type")>
    Public ReadOnly Property StartTime() As Object
      Get
        Dim value = GetProperty(StartTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.EndTime, "End Time")
    ''' <summary>
    ''' Gets the End Time value
    ''' </summary>
    <Display(Name:="End Time", Description:="End time of the shift type")>
    Public ReadOnly Property EndTime() As Object
      Get
        Dim value = GetProperty(EndTimeProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftType, "Shift Type", "")
    ''' <summary>
    ''' Gets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="Shift type name")>
    Public ReadOnly Property ShiftType() As String
      Get
        Return GetProperty(ShiftTypeProperty)
      End Get
    End Property

    Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "Status Css Class", "")
    ''' <summary>
    ''' Gets the Status Css Class value
    ''' </summary>
    <Display(Name:="Status Css", Description:="Status Css Class")>
    Public ReadOnly Property StatusCssClass() As String
      Get
        Return GetProperty(StatusCssClassProperty)
      End Get
    End Property

    Public Shared RoomBookingDetailRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomBookingDetailRequired, "RoomBookingDetailRequired", True)
    ''' <summary>
    ''' Gets the Status Css Class value
    ''' </summary>
    <Display(Name:="RoomBookingDetailRequired")>
    Public ReadOnly Property RoomBookingDetailRequired() As Boolean
      Get
        Return GetProperty(RoomBookingDetailRequiredProperty)
      End Get
    End Property

    Public Shared MealReimbursementProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MealReimbursement, "MealReimbursement", False)
    ''' <summary>
    ''' Gets the Status Css Class value
    ''' </summary>
    <Display(Name:="MealReimbursement")>
    Public ReadOnly Property MealReimbursement() As Boolean
      Get
        Return GetProperty(MealReimbursementProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaShiftTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemAreaShiftType(dr As SafeDataReader) As ROSystemAreaShiftType

      Dim r As New ROSystemAreaShiftType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemAreaShiftTypeIDProperty, .GetInt32(0))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ShiftTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        If .IsDBNull(4) Then
          LoadProperty(StartTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(StartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(4))))
        End If
        If .IsDBNull(5) Then
          LoadProperty(EndTimeProperty, DateTime.MinValue)
        Else
          LoadProperty(EndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(5))))
        End If
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(ShiftTypeProperty, .GetString(10))
        LoadProperty(StatusCssClassProperty, .GetString(11))
        LoadProperty(RoomBookingDetailRequiredProperty, .GetBoolean(12))
        LoadProperty(MealReimbursementProperty, .GetBoolean(13))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace