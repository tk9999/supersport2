﻿' Generated 19 Aug 2014 10:05 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.ShiftPatterns.ReadOnly

  <Serializable()> _
  Public Class ROSystemAreaShiftPatternTemplateList
    Inherits OBReadOnlyListBase(Of ROSystemAreaShiftPatternTemplateList, ROSystemAreaShiftPatternTemplate)

#Region " Business Methods "

    Public Function GetItem(SystemAreaShiftPatternID As Integer) As ROSystemAreaShiftPatternTemplate

      For Each child As ROSystemAreaShiftPatternTemplate In Me
        If child.SystemAreaShiftPatternID = SystemAreaShiftPatternID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Area Shift Patterns"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing

      <PrimarySearchField>
      Public Property PatternName As String = Nothing

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSystemAreaShiftPatternTemplateList() As ROSystemAreaShiftPatternTemplateList

      Return New ROSystemAreaShiftPatternTemplateList()

    End Function

    Public Shared Sub BeginGetROSystemAreaShiftPatternTemplateList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSystemAreaShiftPatternTemplateList)))

      Dim dp As New DataPortal(Of ROSystemAreaShiftPatternTemplateList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    Public Shared Sub BeginGetROSystemAreaShiftPatternTemplateList(CallBack As EventHandler(Of DataPortalResult(Of ROSystemAreaShiftPatternTemplateList)))

      Dim dp As New DataPortal(Of ROSystemAreaShiftPatternTemplateList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSystemAreaShiftPatternTemplateList() As ROSystemAreaShiftPatternTemplateList

      Return DataPortal.Fetch(Of ROSystemAreaShiftPatternTemplateList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemAreaShiftPatternTemplate.GetROSystemAreaShiftPatternTemplate(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      'Dim parent As ROSystemAreaShiftPatternTemplate = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.SystemAreaShiftPatternID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.ROSystemAreaShiftPatternTemplateWeekDayList.RaiseListChangedEvents = False
      '    parent.ROSystemAreaShiftPatternTemplateWeekDayList.Add(ROSystemAreaShiftPatternTemplateWeekDay.GetROSystemAreaShiftPatternTemplateWeekDay(sdr))
      '    parent.ROSystemAreaShiftPatternTemplateWeekDayList.RaiseListChangedEvents = True
      '  End While
      'End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemAreaShiftPatternTemplateList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID)) 'OBLib.Security.Settings.CurrentUser.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID)) '.Security.Settings.CurrentUser.ProductionAreaID))
            cm.Parameters.AddWithValue("@PatternName", Singular.Strings.MakeEmptyDBNull(crit.PatternName))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace