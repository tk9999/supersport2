﻿' Generated 18 Aug 2014 09:36 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.ShiftPatterns

  <Serializable()> _
  Public Class SystemAreaShiftPattern
    Inherits OBBusinessBase(Of SystemAreaShiftPattern)

#Region " Properties and Methods "

    <DefaultValue(False), AlwaysClean>
    Public Property IsExpanded As Boolean = False

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SystemAreaShiftPatternBO.SystemAreaShiftPatternBOToString(self)")

    Public Shared SystemAreaShiftPatternIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftPatternID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemAreaShiftPatternID() As Integer
      Get
        Return GetProperty(SystemAreaShiftPatternIDProperty)
      End Get
    End Property

    Public Shared PatternNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PatternName, "Pattern Name", "")
    ''' <summary>
    ''' Gets and sets the Pattern Name value
    ''' </summary>
    <Display(Name:="Pattern Name", Description:="a name which describes the pattern"),
    StringLength(50, ErrorMessage:="Pattern Name cannot be more than 50 characters"),
    Required(ErrorMessage:="Pattern Name Required")>
    Public Property PatternName() As String
      Get
        Return GetProperty(PatternNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PatternNameProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    DropDownWeb(GetType(ROUserSystemList),
                UnselectedText:="Select Sub-Dept...",
                DisplayMember:="System", ValueMember:="SystemID")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""), Required(ErrorMessage:="Area Required"),
    DropDownWeb(GetType(ROUserSystemAreaList),
                UnselectedText:="Select Area...",
                ThisFilterMember:="SystemID", DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROProductionAreaAllowedDisciplineList), FilterMethodName:="GetAllowedDisciplinesSASP", ValueMember:="DisciplineID", DisplayMember:="Discipline")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared ShiftDurationProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.ShiftDuration, 0)
    ''' <summary>
    ''' Gets and Sets the Number of Weeks value
    ''' </summary>
    <Display(Name:="Number of Weeks", Description:=""), Required(ErrorMessage:="Shift Duration is Required"),
    DropDownWeb(GetType(ROShiftDurationList),
                DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
                FilterMethodName:="SystemAreaShiftPatternBO.AllowedShiftDurations",
                ValueMember:="ShiftDuration", DisplayMember:="ShiftDurationName"),
    SetExpression("SystemAreaShiftPatternBO.UpdateNoWeekDays(self)")>
    Public Property ShiftDuration() As Integer
      Get
        Return GetProperty(ShiftDurationProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ShiftDurationProperty, Value)
      End Set
    End Property

    Public Shared HRUsingPatternIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HRUsingPatternInd, "HR Using Pattern")
    ''' <summary>
    ''' Gets the HR USing Pattern Ind value
    ''' </summary>
    Public ReadOnly Property HRUsingPatternInd() As Boolean
      Get
        Return GetProperty(HRUsingPatternIndProperty)
      End Get
    End Property

    Public Shared IsTemplateProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTemplate, "Is Template?")
    Public Property IsTemplate() As Boolean
      Get
        Return GetProperty(IsTemplateProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsTemplateProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SystemAreaShiftPatternWeekDayListProperty As PropertyInfo(Of SystemAreaShiftPatternWeekDayList) = RegisterProperty(Of SystemAreaShiftPatternWeekDayList)(Function(c) c.SystemAreaShiftPatternWeekDayList, "System Area Shift Pattern Week Day List")

    Public ReadOnly Property SystemAreaShiftPatternWeekDayList() As SystemAreaShiftPatternWeekDayList
      Get
        If GetProperty(SystemAreaShiftPatternWeekDayListProperty) Is Nothing Then
          LoadProperty(SystemAreaShiftPatternWeekDayListProperty, Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDayList.NewSystemAreaShiftPatternWeekDayList())
        End If
        Return GetProperty(SystemAreaShiftPatternWeekDayListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaShiftPatternIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.PatternName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Area Shift Pattern")
        Else
          Return String.Format("Blank {0}", "System Area Shift Pattern")
        End If
      Else
        Return Me.PatternName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"SystemAreaShiftPatternWeekDays"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(ShiftDurationProperty)
        .JavascriptRuleFunctionName = "SystemAreaShiftPatternBO.ShiftDurationValid"
        .ServerRuleFunction = AddressOf ShiftDurationValid
      End With

    End Sub

    Public Function ShiftDurationValid(SystemAreaShiftPattern As SystemAreaShiftPattern) As String
      Dim ErrString = ""
      If SystemAreaShiftPattern.ShiftDuration = 0 Then
        ErrString = "Shift Duration Required"
      End If
      Return ErrString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemAreaShiftPattern() method.

    End Sub

    Public Shared Function NewSystemAreaShiftPattern() As SystemAreaShiftPattern

      Return DataPortal.CreateChild(Of SystemAreaShiftPattern)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSystemAreaShiftPattern(dr As SafeDataReader) As SystemAreaShiftPattern

      Dim s As New SystemAreaShiftPattern()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemAreaShiftPatternIDProperty, .GetInt32(0))
          LoadProperty(PatternNameProperty, .GetString(1))
          LoadProperty(SystemIDProperty, .GetInt32(2))
          LoadProperty(ProductionAreaIDProperty, .GetInt32(3))
          'LoadProperty(SystemProductionAreaIDProperty, .GetInt32(2))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(ShiftDurationProperty, .GetInt32(9))
          LoadProperty(HRUsingPatternIndProperty, .GetBoolean(10))
          LoadProperty(IsTemplateProperty, .GetBoolean(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSystemAreaShiftPattern"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSystemAreaShiftPattern"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSystemAreaShiftPatternID As SqlParameter = .Parameters.Add("@SystemAreaShiftPatternID", SqlDbType.Int)
          paramSystemAreaShiftPatternID.Value = GetProperty(SystemAreaShiftPatternIDProperty)
          If Me.IsNew Then
            paramSystemAreaShiftPatternID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@PatternName", GetProperty(PatternNameProperty))
          .Parameters.AddWithValue("@ModifiedBy", Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@DisciplineID", NothingDBNull(GetProperty(DisciplineIDProperty)))
          .Parameters.AddWithValue("@SystemID", NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          '.Parameters.AddWithValue("@SystemProductionAreaID", NothingDBNull(GetProperty(SystemProductionAreaIDProperty)))
          .Parameters.AddWithValue("@ShiftDuration", GetProperty(ShiftDurationProperty))
          .Parameters.AddWithValue("@IsTemplate", GetProperty(IsTemplateProperty))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SystemAreaShiftPatternIDProperty, paramSystemAreaShiftPatternID.Value)
          End If
          ' update child objects
          If GetProperty(SystemAreaShiftPatternWeekDayListProperty) IsNot Nothing Then
            Me.SystemAreaShiftPatternWeekDayList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(SystemAreaShiftPatternWeekDayListProperty) IsNot Nothing Then
          Me.SystemAreaShiftPatternWeekDayList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSystemAreaShiftPattern"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SystemAreaShiftPatternID", GetProperty(SystemAreaShiftPatternIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace