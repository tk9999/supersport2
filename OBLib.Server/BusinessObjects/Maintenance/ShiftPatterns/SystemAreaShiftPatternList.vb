﻿' Generated 18 Aug 2014 09:36 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ShiftPatterns

  <Serializable()> _
  Public Class SystemAreaShiftPatternList
    Inherits OBBusinessListBase(Of SystemAreaShiftPatternList, SystemAreaShiftPattern)

#Region " Business Methods "

    Public Function GetItem(SystemAreaShiftPatternID As Integer) As SystemAreaShiftPattern

      For Each child As SystemAreaShiftPattern In Me
        If child.SystemAreaShiftPatternID = SystemAreaShiftPatternID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Area Shift Patterns"

    End Function

    Public Function GetSystemAreaShiftPatternWeekDay(SystemAreaShiftPatternWeekDayID As Integer) As SystemAreaShiftPatternWeekDay

      Dim obj As SystemAreaShiftPatternWeekDay = Nothing
      For Each parent As SystemAreaShiftPattern In Me
        obj = parent.SystemAreaShiftPatternWeekDayList.GetItem(SystemAreaShiftPatternWeekDayID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemAreaShiftPatternID As Integer? = Nothing

      <Display(Name:="Sub-Dept", Description:="", Order:=1),
       Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), UnselectedText:="Sub-Dept")>
      Public Property SystemID As Integer? = Nothing

      <Display(Name:="Area", Order:=2),
       Singular.DataAnnotations.DropDownWeb(GetType(Productions.Areas.ReadOnly.ROProductionAreaList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
                                                                                                      FilterMethodName:="FilterSystemAllowedAreasSASP",
                                                                                                      UnselectedText:="Area")>
      Public Property ProductionAreaID As Integer? = Nothing

      Public Property SystemProductionAreaID As Integer? = Nothing

      Public Sub New()

      End Sub

      Public Sub New(SystemAreaShiftPatternID As Integer?, SystemID As Integer? _
                     , ProductionAreaID As Integer?, SystemProductionAreaID As Integer?)
        Me.SystemAreaShiftPatternID = SystemAreaShiftPatternID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.SystemProductionAreaID = SystemProductionAreaID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSystemAreaShiftPatternList() As SystemAreaShiftPatternList

      Return New SystemAreaShiftPatternList()

    End Function

    Public Shared Sub BeginGetSystemAreaShiftPatternList(CallBack As EventHandler(Of DataPortalResult(Of SystemAreaShiftPatternList)))

      Dim dp As New DataPortal(Of SystemAreaShiftPatternList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSystemAreaShiftPatternList() As SystemAreaShiftPatternList

      Return DataPortal.Fetch(Of SystemAreaShiftPatternList)(New Criteria())

    End Function

    Public Shared Function GetSystemAreaShiftPatternList(SystemAreaShiftPatternID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?, _
                                                         SystemProductionAreaID As Integer?) As SystemAreaShiftPatternList

      Return DataPortal.Fetch(Of SystemAreaShiftPatternList)(New Criteria(SystemAreaShiftPatternID, SystemID, ProductionAreaID, SystemProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemAreaShiftPattern.GetSystemAreaShiftPattern(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SystemAreaShiftPattern = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemAreaShiftPatternID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SystemAreaShiftPatternWeekDayList.RaiseListChangedEvents = False
          parent.SystemAreaShiftPatternWeekDayList.Add(SystemAreaShiftPatternWeekDay.GetSystemAreaShiftPatternWeekDay(sdr))
          parent.SystemAreaShiftPatternWeekDayList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As SystemAreaShiftPattern In Me
        child.CheckRules()
        For Each SystemAreaShiftPatternWeekDay As SystemAreaShiftPatternWeekDay In child.SystemAreaShiftPatternWeekDayList
          SystemAreaShiftPatternWeekDay.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemAreaShiftPatternList"
            cm.Parameters.AddWithValue("@SystemAreaShiftPatternID", NothingDBNull(crit.SystemAreaShiftPatternID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SystemAreaShiftPattern In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SystemAreaShiftPattern In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace