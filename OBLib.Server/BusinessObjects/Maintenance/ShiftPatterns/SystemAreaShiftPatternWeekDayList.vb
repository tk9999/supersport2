﻿' Generated 18 Aug 2014 09:36 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.ShiftPatterns

  <Serializable()> _
  Public Class SystemAreaShiftPatternWeekDayList
    Inherits OBBusinessListBase(Of SystemAreaShiftPatternWeekDayList, SystemAreaShiftPatternWeekDay)

#Region " Business Methods "

    Public Function GetItem(SystemAreaShiftPatternWeekDayID As Integer) As SystemAreaShiftPatternWeekDay

      For Each child As SystemAreaShiftPatternWeekDay In Me
        If child.SystemAreaShiftPatternWeekDayID = SystemAreaShiftPatternWeekDayID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Area Shift Pattern Week Days"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewSystemAreaShiftPatternWeekDayList() As SystemAreaShiftPatternWeekDayList

      Return New SystemAreaShiftPatternWeekDayList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SystemAreaShiftPatternWeekDay In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SystemAreaShiftPatternWeekDay In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace