﻿' Generated 04 Nov 2014 13:30 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.AdHoc.ReadOnly

  <Serializable()> _
  Public Class ROSystemAreaAdHocBookingType
    Inherits OBReadOnlyBase(Of ROSystemAreaAdHocBookingType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemAreaAdHocBookingTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaAdHocBookingTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SystemAreaAdHocBookingTypeID() As Integer
      Get
        Return GetProperty(SystemAreaAdHocBookingTypeIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="System linked to the ad hoc booking type")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="Production Area linked to the ad hoc booking type")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared AdHocBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingTypeID, "Ad Hoc Booking Type", Nothing)
    ''' <summary>
    ''' Gets the Ad Hoc Booking Type value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking Type", Description:="Ad hoc booking type linked to the system and area")>
    Public ReadOnly Property AdHocBookingTypeID() As Integer?
      Get
        Return GetProperty(AdHocBookingTypeIDProperty)
      End Get
    End Property

    Public Shared DefaultGenRefNoProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.DefaultGenRefNo, "Default Gen Ref No.", 0)
    ''' <summary>
    ''' Gets the Default Gen Ref No for this booking type
    ''' </summary>
    <Display(Name:="Default Gen Ref No.", Description:="")>
    Public ReadOnly Property DefaultGenRefNo() As Int64
      Get
        Return GetProperty(DefaultGenRefNoProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DisplayNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DisplayName, "Display Name", "")
    ''' <summary>
    ''' Gets the Display Name value
    ''' </summary>
    <Display(Name:="Display Name", Description:="Display Name of Booking Type")>
    Public ReadOnly Property DisplayName() As String
      Get
        Return GetProperty(DisplayNameProperty)
      End Get
    End Property

    Public Shared AdHocBookingTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdHocBookingType, "Booking Type", "")
    ''' <summary>
    ''' Gets the Display Name value
    ''' </summary>
    <Display(Name:="Booking Type")>
    Public ReadOnly Property AdHocBookingType() As String
      Get
        Return GetProperty(AdHocBookingTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaAdHocBookingTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AdHocBookingTypeID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemAreaAdHocBookingType(dr As SafeDataReader) As ROSystemAreaAdHocBookingType

      Dim r As New ROSystemAreaAdHocBookingType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemAreaAdHocBookingTypeIDProperty, .GetInt32(0))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(AdHocBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(DefaultGenRefNoProperty, .GetInt64(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(DisplayNameProperty, .GetString(9))
        LoadProperty(AdHocBookingTypeProperty, .GetString(10))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace