﻿' Generated 17 Oct 2014 08:10 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.AdHoc.ReadOnly

  <Serializable()> _
  Public Class ROAdHocBookingType
    Inherits OBReadOnlyBase(Of ROAdHocBookingType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AdHocBookingTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdHocBookingTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AdHocBookingTypeID() As Integer
      Get
        Return GetProperty(AdHocBookingTypeIDProperty)
      End Get
    End Property

    Public Shared AdHocBookingTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdHocBookingType, "Ad Hoc Booking Type", "")
    ''' <summary>
    ''' Gets the Ad Hoc Booking Type value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking Type", Description:="")>
  Public ReadOnly Property AdHocBookingType() As String
      Get
        Return GetProperty(AdHocBookingTypeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AdHocBookingTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AdHocBookingType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAdHocBookingType(dr As SafeDataReader) As ROAdHocBookingType

      Dim r As New ROAdHocBookingType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AdHocBookingTypeIDProperty, .GetInt32(0))
        LoadProperty(AdHocBookingTypeProperty, .GetString(1))
        LoadProperty(CreatedByProperty, .GetInt32(2))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
        LoadProperty(ModifiedByProperty, .GetInt32(4))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace