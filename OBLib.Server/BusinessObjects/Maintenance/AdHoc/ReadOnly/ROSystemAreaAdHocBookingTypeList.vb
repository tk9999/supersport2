﻿' Generated 04 Nov 2014 13:30 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.AdHoc.ReadOnly

  <Serializable()> _
  Public Class ROSystemAreaAdHocBookingTypeList
    Inherits OBReadOnlyListBase(Of ROSystemAreaAdHocBookingTypeList, ROSystemAreaAdHocBookingType)

#Region " Business Methods "

    Public Function GetItem(SystemAreaAdHocBookingTypeID As Integer) As ROSystemAreaAdHocBookingType

      For Each child As ROSystemAreaAdHocBookingType In Me
        If child.SystemAreaAdHocBookingTypeID = SystemAreaAdHocBookingTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItem(SystemID As Integer?, ProductionAreaID As Integer?, AdHocBookingTypeID As Integer?) As ROSystemAreaAdHocBookingType

      For Each child As ROSystemAreaAdHocBookingType In Me
        If CompareSafe(child.SystemID, SystemID) AndAlso CompareSafe(child.ProductionAreaID, ProductionAreaID) AndAlso CompareSafe(child.AdHocBookingTypeID, AdHocBookingTypeID) Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Area Ad Hoc Booking Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer?
      <PrimarySearchField>
      Public Property AdHocBookingType As String = ""

      Public Sub New()

      End Sub

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?)

        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSystemAreaAdHocBookingTypeList() As ROSystemAreaAdHocBookingTypeList

      Return New ROSystemAreaAdHocBookingTypeList()

    End Function

    Public Shared Sub BeginGetROSystemAreaAdHocBookingTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSystemAreaAdHocBookingTypeList)))

      Dim dp As New DataPortal(Of ROSystemAreaAdHocBookingTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSystemAreaAdHocBookingTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROSystemAreaAdHocBookingTypeList)))

      Dim dp As New DataPortal(Of ROSystemAreaAdHocBookingTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSystemAreaAdHocBookingTypeList(SystemID As Integer?, ProductionAreaID As Integer?) As ROSystemAreaAdHocBookingTypeList

      Return DataPortal.Fetch(Of ROSystemAreaAdHocBookingTypeList)(New Criteria(SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemAreaAdHocBookingType.GetROSystemAreaAdHocBookingType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemAreaAdHocBookingTypeList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@AdHocBookingType", Strings.MakeEmptyDBNull(crit.AdHocBookingType))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace