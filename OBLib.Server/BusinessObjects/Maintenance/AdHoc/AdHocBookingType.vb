﻿' Generated 17 Oct 2014 08:10 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.AdHoc

  <Serializable()> _
  Public Class AdHocBookingType
    Inherits OBBusinessBase(Of AdHocBookingType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AdHocBookingTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdHocBookingTypeID, "ID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>

    <Display(Name:="ID", Description:=""), Key,
    Required(ErrorMessage:="Ad Hoc Booking Type is Required")>
    Public Property AdHocBookingTypeID() As Integer
      Get
        Return GetProperty(AdHocBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AdHocBookingTypeIDProperty, Value)
      End Set
    End Property

    Public Shared AdHocBookingTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdHocBookingType, "Ad Hoc Booking Type", "")
    ''' <summary>
    ''' Gets and sets the Ad Hoc Booking Type value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking Type", Description:=""),
    Required(ErrorMessage:="Ad Hoc Booking Type is Required"),
    StringLength(50, ErrorMessage:="Ad Hoc Booking Type cannot be more than 50 characters")>
    Public Property AdHocBookingType() As String
      Get
        Return GetProperty(AdHocBookingTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AdHocBookingTypeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AdHocBookingTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AdHocBookingType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Ad Hoc Booking Type")
        Else
          Return String.Format("Blank {0}", "Ad Hoc Booking Type")
        End If
      Else
        Return Me.AdHocBookingType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAdHocBookingType() method.

    End Sub

    Public Shared Function NewAdHocBookingType() As AdHocBookingType

      Return DataPortal.CreateChild(Of AdHocBookingType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAdHocBookingType(dr As SafeDataReader) As AdHocBookingType

      Dim a As New AdHocBookingType()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AdHocBookingTypeIDProperty, .GetInt32(0))
          LoadProperty(AdHocBookingTypeProperty, .GetString(1))
          LoadProperty(CreatedByProperty, .GetInt32(2))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
          LoadProperty(ModifiedByProperty, .GetInt32(4))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAdHocBookingType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAdHocBookingType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          'Dim paramAdHocBookingTypeID As SqlParameter = .Parameters.Add("@AdHocBookingTypeID", SqlDbType.Int)

          ' paramAdHocBookingTypeID.Value = GetProperty(AdHocBookingTypeIDProperty)
          'If Me.IsNew Then
          '  paramAdHocBookingTypeID.Direction = ParameterDirection.Output
          'End If
          .Parameters.AddWithValue("@AdHocBookingTypeID", GetProperty(AdHocBookingTypeIDProperty))
          .Parameters.AddWithValue("@AdHocBookingType", GetProperty(AdHocBookingTypeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          'If Me.IsNew Then
          '  LoadProperty(AdHocBookingTypeIDProperty, paramAdHocBookingTypeID.Value)
          'End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAdHocBookingType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AdHocBookingTypeID", GetProperty(AdHocBookingTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace