﻿' Generated 24 Dec 2014 12:03 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel

  <Serializable()> _
  Public Class RentalCarAgent
    Inherits OBBusinessBase(Of RentalCarAgent)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "RentalCarAgentBO.RentalCarAgentBOToString(self)")

    Public Shared RentalCarAgentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarAgentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property RentalCarAgentID() As Integer
      Get
        Return GetProperty(RentalCarAgentIDProperty)
      End Get
    End Property

    Public Shared RentalCarAgentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarAgent, "Rental Car Agent", "")
    ''' <summary>
    ''' Gets and sets the Rental Car Agent value
    ''' </summary>
    <Display(Name:="Rental Car Agent", Description:="Name of the Rental Car Agent"),
    StringLength(50, ErrorMessage:="Rental Car Agent cannot be more than 50 characters")>
    Public Property RentalCarAgent() As String
      Get
        Return GetProperty(RentalCarAgentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RentalCarAgentProperty, Value)
      End Set
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets and sets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="Email address for the Rental Car Agent"),
    StringLength(100, ErrorMessage:="Email Address cannot be more than 100 characters")>
    Public Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmailAddressProperty, Value)
      End Set
    End Property

    Public Shared ContactProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Contact, "Contact", "")
    ''' <summary>
    ''' Gets and sets the Contact value
    ''' </summary>
    <Display(Name:="Contact", Description:="Name of the contact person"),
    StringLength(50, ErrorMessage:="Contact cannot be more than 50 characters")>
    Public Property Contact() As String
      Get
        Return GetProperty(ContactProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContactProperty, Value)
      End Set
    End Property

    Public Shared TelNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TelNo, "Tel No", "")
    ''' <summary>
    ''' Gets and sets the Tel No value
    ''' </summary>
    <Display(Name:="Tel No", Description:="Telephone number for the Rental Car Agent"),
    StringLength(20, ErrorMessage:="Tel No cannot be more than 20 characters")>
    Public Property TelNo() As String
      Get
        Return GetProperty(TelNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TelNoProperty, Value)
      End Set
    End Property

    Public Shared FaxNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FaxNo, "Fax No", "")
    ''' <summary>
    ''' Gets and sets the Fax No value
    ''' </summary>
    <Display(Name:="Fax No", Description:="Fax number for the Rental Car Agent"),
    StringLength(20, ErrorMessage:="Fax No cannot be more than 20 characters")>
    Public Property FaxNo() As String
      Get
        Return GetProperty(FaxNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FaxNoProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared OldProductionCrewTravelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OldProductionCrewTravelID, "Old Production Crew Travel", 0)
    ''' <summary>
    ''' Gets and sets the Old Production Crew Travel value
    ''' </summary>
    <Display(Name:="Old Production Crew Travel", Description:="A reference to the old production crew travel record")>
    Public Property OldProductionCrewTravelID() As Integer
      Get
        Return GetProperty(OldProductionCrewTravelIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OldProductionCrewTravelIDProperty, Value)
      End Set
    End Property

    Public Shared OldIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.OldInd, False) _
                                                               .AddSetExpression("RentalCarAgentBO.OldIndSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Old value
    ''' </summary>
    <Display(Name:="Old", Description:="is and old branch and is no longer selectable")>
    Public Property OldInd() As Boolean
      Get
        Return GetProperty(OldIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OldIndProperty, Value)
      End Set
    End Property

    Public Shared OldDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.OldDate, Nothing) _
                                                                  .AddSetExpression("RentalCarAgentBO.OldDateSet(self)", False)
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Old Date", Description:="The date from which this branch is considered old")>
    Public Property OldDate() As DateTime?
      Get
        Return GetProperty(OldDateProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(OldDateProperty, value)
      End Set
    End Property

    Public Shared OldReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.OldReason, "") _
                                                                 .AddSetExpression("RentalCarAgentBO.OldReasonSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Rental Car Agent Branch value
    ''' </summary>
    <Display(Name:="Old Reason", Description:="Why is this branch considered old"),
    StringLength(200, ErrorMessage:="Old Reason cannot be more than 100 characters")>
    Public Property OldReason() As String
      Get
        Return GetProperty(OldReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OldReasonProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared RentalCarAgentBranchListProperty As PropertyInfo(Of RentalCarAgentBranchList) = RegisterProperty(Of RentalCarAgentBranchList)(Function(c) c.RentalCarAgentBranchList, "Rental Car Agent Branch List")

    Public ReadOnly Property RentalCarAgentBranchList() As RentalCarAgentBranchList
      Get
        If GetProperty(RentalCarAgentBranchListProperty) Is Nothing Then
          LoadProperty(RentalCarAgentBranchListProperty, Maintenance.Travel.RentalCarAgentBranchList.NewRentalCarAgentBranchList())
        End If
        Return GetProperty(RentalCarAgentBranchListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarAgentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.RentalCarAgent.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Rental Car Agent")
        Else
          Return String.Format("Blank {0}", "Rental Car Agent")
        End If
      Else
        Return Me.RentalCarAgent
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(OldDateProperty)
        .AddTriggerProperty(OldIndProperty)
        .JavascriptRuleFunctionName = "RentalCarAgentBO.OldDateValid"
        .ServerRuleFunction = AddressOf OldDateValid
      End With

      With AddWebRule(OldReasonProperty)
        .AddTriggerProperty(OldIndProperty)
        .JavascriptRuleFunctionName = "RentalCarAgentBO.OldReasonValid"
        .ServerRuleFunction = AddressOf OldReasonValid
      End With

    End Sub

    Public Shared Function OldDateValid(RentalCarAgent As RentalCarAgent) As String
      If RentalCarAgent.OldInd And RentalCarAgent.OldDate Is Nothing Then
        Return "Old Date is required"
      End If
      Return ""
    End Function

    Public Shared Function OldReasonValid(RentalCarAgent As RentalCarAgent) As String
      If RentalCarAgent.OldInd And RentalCarAgent.OldReason.Trim.Length = 0 Then
        Return "Old Reason is required"
      End If
      Return ""
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRentalCarAgent() method.

    End Sub

    Public Shared Function NewRentalCarAgent() As RentalCarAgent

      Return DataPortal.CreateChild(Of RentalCarAgent)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRentalCarAgent(dr As SafeDataReader) As RentalCarAgent

      Dim r As New RentalCarAgent()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RentalCarAgentIDProperty, .GetInt32(0))
          LoadProperty(RentalCarAgentProperty, .GetString(1))
          LoadProperty(EmailAddressProperty, .GetString(2))
          LoadProperty(ContactProperty, .GetString(3))
          LoadProperty(TelNoProperty, .GetString(4))
          LoadProperty(FaxNoProperty, .GetString(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(OldProductionCrewTravelIDProperty, .GetInt32(10))
          LoadProperty(OldIndProperty, .GetBoolean(11))
          LoadProperty(OldDateProperty, .GetValue(12))
          LoadProperty(OldReasonProperty, .GetString(13))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insRentalCarAgent"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updRentalCarAgent"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramRentalCarAgentID As SqlParameter = .Parameters.Add("@RentalCarAgentID", SqlDbType.Int)
          paramRentalCarAgentID.Value = GetProperty(RentalCarAgentIDProperty)
          If Me.IsNew Then
            paramRentalCarAgentID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@RentalCarAgent", GetProperty(RentalCarAgentProperty))
          .Parameters.AddWithValue("@EmailAddress", GetProperty(EmailAddressProperty))
          .Parameters.AddWithValue("@Contact", GetProperty(ContactProperty))
          .Parameters.AddWithValue("@TelNo", GetProperty(TelNoProperty))
          .Parameters.AddWithValue("@FaxNo", GetProperty(FaxNoProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@OldProductionCrewTravelID", GetProperty(OldProductionCrewTravelIDProperty))
          .Parameters.AddWithValue("@OldInd", GetProperty(OldIndProperty))
          .Parameters.AddWithValue("@OldDate", (New SmartDate(GetProperty(OldDateProperty)).DBValue))
          .Parameters.AddWithValue("@OldReason", GetProperty(OldReasonProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(RentalCarAgentIDProperty, paramRentalCarAgentID.Value)
          End If
          ' update child objects
          If GetProperty(RentalCarAgentBranchListProperty) IsNot Nothing Then
            Me.RentalCarAgentBranchList.Update()
          End If
          MarkOld()
        End With
      Else
        If GetProperty(RentalCarAgentBranchListProperty) IsNot Nothing Then
          Me.RentalCarAgentBranchList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delRentalCarAgent"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@RentalCarAgentID", GetProperty(RentalCarAgentIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace