﻿' Generated 02 Jul 2014 08:43 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel.ReadOnly

  <Serializable()> _
  Public Class ROAirportList
    Inherits OBReadOnlyListBase(Of ROAirportList, ROAirport)

#Region " Business Methods "

    Public Function GetItem(AirportID As Integer) As ROAirport

      For Each child As ROAirport In Me
        If child.AirportID = AirportID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Airports"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property Airport As String

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROAirportList() As ROAirportList

      Return New ROAirportList()

    End Function

    Public Shared Sub BeginGetROAirportList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROAirportList)))

      Dim dp As New DataPortal(Of ROAirportList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROAirportList(CallBack As EventHandler(Of DataPortalResult(Of ROAirportList)))

      Dim dp As New DataPortal(Of ROAirportList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROAirportList() As ROAirportList

      Return DataPortal.Fetch(Of ROAirportList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROAirport.GetROAirport(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROAirportList"
            cm.Parameters.AddWithValue("@Airport", Singular.Strings.MakeEmptyDBNull(crit.Airport))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace