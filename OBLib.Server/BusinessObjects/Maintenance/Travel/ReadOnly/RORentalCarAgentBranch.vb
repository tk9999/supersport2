﻿' Generated 03 Jul 2014 09:25 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel.ReadOnly

  <Serializable()> _
  Public Class RORentalCarAgentBranch
    Inherits OBReadOnlyBase(Of RORentalCarAgentBranch)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RentalCarAgentBranchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarAgentBranchID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property RentalCarAgentBranchID() As Integer
      Get
        Return GetProperty(RentalCarAgentBranchIDProperty)
      End Get
    End Property

    Public Shared RentalCarAgentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RentalCarAgentID, "Rental Car Agent", Nothing)
    ''' <summary>
    ''' Gets the Rental Car Agent value
    ''' </summary>
    <Display(Name:="Rental Car Agent", Description:="parent rental car agent")>
    Public ReadOnly Property RentalCarAgentID() As Integer?
      Get
        Return GetProperty(RentalCarAgentIDProperty)
      End Get
    End Property

    Public Shared RentalCarAgentBranchProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarAgentBranch, "Rental Car Agent Branch", "")
    ''' <summary>
    ''' Gets the Rental Car Agent Branch value
    ''' </summary>
    <Display(Name:="Rental Car Agent Branch", Description:="branch name")>
    Public ReadOnly Property RentalCarAgentBranch() As String
      Get
        Return GetProperty(RentalCarAgentBranchProperty)
      End Get
    End Property

    Public Shared InternationalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InternationalInd, "International", False)
    ''' <summary>
    ''' Gets the International value
    ''' </summary>
    <Display(Name:="International", Description:="is located internationally")>
    Public ReadOnly Property InternationalInd() As Boolean
      Get
        Return GetProperty(InternationalIndProperty)
      End Get
    End Property

    Public Shared LocationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LocationID, "Location", Nothing)
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="location in which the rental car agent is located")>
    Public ReadOnly Property LocationID() As Integer?
      Get
        Return GetProperty(LocationIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared OldIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OldInd, "Old", False)
    ''' <summary>
    ''' Gets and sets the Old value
    ''' </summary>
    <Display(Name:="Old", Description:="is and old branch and is no longer selectable")>
    Public ReadOnly Property OldInd() As Boolean
      Get
        Return GetProperty(OldIndProperty)
      End Get
    End Property

    Public Shared OldDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.OldDate, Nothing)
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Old Date", Description:="The date from which this branch is considered old")>
    Public ReadOnly Property OldDate() As DateTime?
      Get
        Return GetProperty(OldDateProperty)
      End Get
    End Property

    Public Shared OldReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OldReason, "Old Reason", "")
    ''' <summary>
    ''' Gets and sets the Rental Car Agent Branch value
    ''' </summary>
    <Display(Name:="Old Reason", Description:="Why is this branch considered old")>
    Public ReadOnly Property OldReason() As String
      Get
        Return GetProperty(OldReasonProperty)
      End Get
    End Property

    Public Shared RentalCarAgentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarAgent, "Agent", "")
    ''' <summary>
    ''' Gets and sets the Rental Car Agent Branch value
    ''' </summary>
    <Display(Name:="Agent")>
    Public ReadOnly Property RentalCarAgent() As String
      Get
        Return GetProperty(RentalCarAgentProperty)
      End Get
    End Property

    Public Shared RentalCarAgentBranchNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarAgentBranchName, "Agent Branch Name", "")
    ''' <summary>
    ''' Gets and sets the Rental Car Agent Branch value
    ''' </summary>
    <Display(Name:="Agent Branch Name")>
    Public ReadOnly Property RentalCarAgentBranchName() As String
      Get
        Return RentalCarAgentBranch & " (" & RentalCarAgent & ")"
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarAgentBranchIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.RentalCarAgentBranch

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORentalCarAgentBranch(dr As SafeDataReader) As RORentalCarAgentBranch

      Dim r As New RORentalCarAgentBranch()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RentalCarAgentBranchIDProperty, .GetInt32(0))
        LoadProperty(RentalCarAgentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(RentalCarAgentBranchProperty, .GetString(2))
        LoadProperty(InternationalIndProperty, .GetBoolean(3))
        LoadProperty(LocationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(OldIndProperty, .GetBoolean(9))
        LoadProperty(OldDateProperty, .GetValue(10))
        LoadProperty(OldReasonProperty, .GetString(11))
        LoadProperty(RentalCarAgentProperty, .GetString(12))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace