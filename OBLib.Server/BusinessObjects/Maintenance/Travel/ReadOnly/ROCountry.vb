﻿' Generated 06 Jul 2014 06:47 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROCountry
    Inherits OBReadOnlyBase(Of ROCountry)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CountryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CountryID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CountryID() As Integer
      Get
        Return GetProperty(CountryIDProperty)
      End Get
    End Property

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country", "")
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="Name of the country")>
    Public ReadOnly Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
    End Property

    Public Shared CurrencyIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CurrencyID, "Currency", 0)
    ''' <summary>
    ''' Gets the Currency value
    ''' </summary>
    <Display(Name:="Currency", Description:="The currency in which the country deals with")>
    Public ReadOnly Property CurrencyID() As Integer
      Get
        Return GetProperty(CurrencyIDProperty)
      End Get
    End Property

    Public Shared DefaultDailySnTRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.DefaultDailySnTRate, "Default Daily Sn T Rate", CDec(0))
    ''' <summary>
    ''' Gets the Default Daily Sn T Rate value
    ''' </summary>
    <Display(Name:="Default Daily Sn T Rate", Description:="The country's default daily S&T rate")>
    Public ReadOnly Property DefaultDailySnTRate() As Decimal
      Get
        Return GetProperty(DefaultDailySnTRateProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 0)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="The system to which the country belongs")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CountryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Country

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCountry(dr As SafeDataReader) As ROCountry

      Dim r As New ROCountry()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CountryIDProperty, .GetInt32(0))
        LoadProperty(CountryProperty, .GetString(1))
        LoadProperty(CurrencyIDProperty, .GetInt32(2))
        LoadProperty(DefaultDailySnTRateProperty, .GetDecimal(3))
        LoadProperty(SystemIDProperty, .GetInt32(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace