﻿' Generated 05 Nov 2014 15:03 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROTravelDirectionList
    Inherits OBReadOnlyListBase(Of ROTravelDirectionList, ROTravelDirection)

#Region " Business Methods "

    Public Function GetItem(TravelDirectionID As Integer) As ROTravelDirection

      For Each child As ROTravelDirection In Me
        If child.TravelDirectionID = TravelDirectionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Travel Directions"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROTravelDirectionList() As ROTravelDirectionList

      Return New ROTravelDirectionList()

    End Function

    Public Shared Sub BeginGetROTravelDirectionList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROTravelDirectionList)))

      Dim dp As New DataPortal(Of ROTravelDirectionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROTravelDirectionList(CallBack As EventHandler(Of DataPortalResult(Of ROTravelDirectionList)))

      Dim dp As New DataPortal(Of ROTravelDirectionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROTravelDirectionList() As ROTravelDirectionList

      Return DataPortal.Fetch(Of ROTravelDirectionList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTravelDirection.GetROTravelDirection(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravelDirectionList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace