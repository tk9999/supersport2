﻿' Generated 03 Jul 2014 09:25 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel.ReadOnly

  <Serializable()> _
  Public Class RORentalCarAgent
    Inherits OBReadOnlyBase(Of RORentalCarAgent)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RentalCarAgentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarAgentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property RentalCarAgentID() As Integer
      Get
        Return GetProperty(RentalCarAgentIDProperty)
      End Get
    End Property

    Public Shared RentalCarAgentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarAgent, "Rental Car Agent", "")
    ''' <summary>
    ''' Gets the Rental Car Agent value
    ''' </summary>
    <Display(Name:="Rental Car Agent", Description:="Name of the Rental Car Agent")>
    Public ReadOnly Property RentalCarAgent() As String
      Get
        Return GetProperty(RentalCarAgentProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="Email address for the Rental Car Agent")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared ContactProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Contact, "Contact", "")
    ''' <summary>
    ''' Gets the Contact value
    ''' </summary>
    <Display(Name:="Contact", Description:="Name of the contact person")>
    Public ReadOnly Property Contact() As String
      Get
        Return GetProperty(ContactProperty)
      End Get
    End Property

    Public Shared TelNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TelNo, "Tel No", "")
    ''' <summary>
    ''' Gets the Tel No value
    ''' </summary>
    <Display(Name:="Tel No", Description:="Telephone number for the Rental Car Agent")>
    Public ReadOnly Property TelNo() As String
      Get
        Return GetProperty(TelNoProperty)
      End Get
    End Property

    Public Shared FaxNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FaxNo, "Fax No", "")
    ''' <summary>
    ''' Gets the Fax No value
    ''' </summary>
    <Display(Name:="Fax No", Description:="Fax number for the Rental Car Agent")>
    Public ReadOnly Property FaxNo() As String
      Get
        Return GetProperty(FaxNoProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared OldProductionCrewTravelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OldProductionCrewTravelID, "Old Production Crew Travel", 0)
    ''' <summary>
    ''' Gets the Old Production Crew Travel value
    ''' </summary>
    <Display(Name:="Old Production Crew Travel", Description:="A reference to the old production crew travel record")>
    Public ReadOnly Property OldProductionCrewTravelID() As Integer
      Get
        Return GetProperty(OldProductionCrewTravelIDProperty)
      End Get
    End Property

    Public Shared OldIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OldInd, "Old", False)
    ''' <summary>
    ''' Gets and sets the Old value
    ''' </summary>
    <Display(Name:="Old", Description:="is and old branch and is no longer selectable")>
    Public ReadOnly Property OldInd() As Boolean
      Get
        Return GetProperty(OldIndProperty)
      End Get
    End Property

    Public Shared OldDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.OldDate, Nothing)
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Old Date", Description:="The date from which this branch is considered old")>
    Public ReadOnly Property OldDate() As DateTime?
      Get
        Return GetProperty(OldDateProperty)
      End Get
    End Property

    Public Shared OldReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OldReason, "Old Reason", "")
    ''' <summary>
    ''' Gets and sets the Rental Car Agent Branch value
    ''' </summary>
    <Display(Name:="Old Reason", Description:="Why is this branch considered old")>
    Public ReadOnly Property OldReason() As String
      Get
        Return GetProperty(OldReasonProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarAgentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.RentalCarAgent

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORentalCarAgent(dr As SafeDataReader) As RORentalCarAgent

      Dim r As New RORentalCarAgent()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RentalCarAgentIDProperty, .GetInt32(0))
        LoadProperty(RentalCarAgentProperty, .GetString(1))
        LoadProperty(EmailAddressProperty, .GetString(2))
        LoadProperty(ContactProperty, .GetString(3))
        LoadProperty(TelNoProperty, .GetString(4))
        LoadProperty(FaxNoProperty, .GetString(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(OldProductionCrewTravelIDProperty, .GetInt32(10))
        LoadProperty(OldIndProperty, .GetBoolean(11))
        LoadProperty(OldDateProperty, .GetValue(12))
        LoadProperty(OldReasonProperty, .GetString(13))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace