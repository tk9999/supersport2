﻿' Generated 03 Jul 2014 09:25 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.Travel.ReadOnly

  <Serializable()> _
  Public Class RORentalCarAgentList
    Inherits OBReadOnlyListBase(Of RORentalCarAgentList, RORentalCarAgent)

#Region " Business Methods "

    Public Function GetItem(RentalCarAgentID As Integer) As RORentalCarAgent

      For Each child As RORentalCarAgent In Me
        If child.RentalCarAgentID = RentalCarAgentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Rental Car Agents"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property RentalCarAgent As String = ""
      Public Property IncludeOld As Boolean = False

      Public Property BookingDate As Date? = Nothing

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORentalCarAgentList() As RORentalCarAgentList

      Return New RORentalCarAgentList()

    End Function

    Public Shared Sub BeginGetRORentalCarAgentList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORentalCarAgentList)))

      Dim dp As New DataPortal(Of RORentalCarAgentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRORentalCarAgentList(CallBack As EventHandler(Of DataPortalResult(Of RORentalCarAgentList)))

      Dim dp As New DataPortal(Of RORentalCarAgentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORentalCarAgentList() As RORentalCarAgentList

      Return DataPortal.Fetch(Of RORentalCarAgentList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORentalCarAgent.GetRORentalCarAgent(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORentalCarAgentList"
            cm.Parameters.AddWithValue("@RentalCarAgent", Strings.MakeEmptyDBNull(crit.RentalCarAgent))
            cm.Parameters.AddWithValue("@IncludeOld", crit.IncludeOld)
            cm.Parameters.AddWithValue("@BookingDate", NothingDBNull(crit.BookingDate))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace