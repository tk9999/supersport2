﻿' Generated 03 Jul 2014 09:25 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel.ReadOnly

  <Serializable()> _
  Public Class ROCarType
    Inherits OBReadOnlyBase(Of ROCarType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CarTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CarTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CarTypeID() As Integer
      Get
        Return GetProperty(CarTypeIDProperty)
      End Get
    End Property

    Public Shared CarTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CarType, "Car Type", "")
    ''' <summary>
    ''' Gets the Car Type value
    ''' </summary>
    <Display(Name:="Car Type", Description:="Description of the type of car")>
    Public ReadOnly Property CarType() As String
      Get
        Return GetProperty(CarTypeProperty)
      End Get
    End Property

    Public Shared CarTypeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CarTypeDescription, "Car Type Description", "")
    ''' <summary>
    ''' Gets the Car Type Description value
    ''' </summary>
    <Display(Name:="Car Type Description", Description:="Description of the type of car")>
    Public ReadOnly Property CarTypeDescription() As String
      Get
        Return GetProperty(CarTypeDescriptionProperty)
      End Get
    End Property

    Public Shared MaxNoOfOccupantsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxNoOfOccupants, "Max Seats", 0)
    ''' <summary>
    ''' Gets the Max No Of Occupants value
    ''' </summary>
    <Display(Name:="Max Seats", Description:="Maximum number of passengers this vehicle type can hold")>
    Public ReadOnly Property MaxNoOfOccupants() As Integer
      Get
        Return GetProperty(MaxNoOfOccupantsProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CarTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CarType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCarType(dr As SafeDataReader) As ROCarType

      Dim r As New ROCarType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CarTypeIDProperty, .GetInt32(0))
        LoadProperty(CarTypeProperty, .GetString(1))
        LoadProperty(CarTypeDescriptionProperty, .GetString(2))
        LoadProperty(MaxNoOfOccupantsProperty, .GetInt32(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace