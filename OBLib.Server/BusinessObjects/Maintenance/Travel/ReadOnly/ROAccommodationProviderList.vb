﻿' Generated 20 Jun 2014 14:49 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel.ReadOnly

  <Serializable()> _
  Public Class ROAccommodationProviderList
    Inherits OBReadOnlyListBase(Of ROAccommodationProviderList, ROAccommodationProvider)

#Region " Business Methods "

    Public Function GetItem(AccommodationProviderID As Integer) As ROAccommodationProvider

      For Each child As ROAccommodationProvider In Me
        If child.AccommodationProviderID = AccommodationProviderID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Accommodation Providers"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property AccommodationProvider As String

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROAccommodationProviderList() As ROAccommodationProviderList

      Return New ROAccommodationProviderList()

    End Function

    Public Shared Sub BeginGetROAccommodationProviderList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROAccommodationProviderList)))

      Dim dp As New DataPortal(Of ROAccommodationProviderList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROAccommodationProviderList(CallBack As EventHandler(Of DataPortalResult(Of ROAccommodationProviderList)))

      Dim dp As New DataPortal(Of ROAccommodationProviderList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROAccommodationProviderList() As ROAccommodationProviderList

      Return DataPortal.Fetch(Of ROAccommodationProviderList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROAccommodationProvider.GetROAccommodationProvider(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROAccommodationProviderList"
            cm.Parameters.AddWithValue("@AccommodationProvider", Singular.Strings.MakeEmptyDBNull(crit.AccommodationProvider))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace