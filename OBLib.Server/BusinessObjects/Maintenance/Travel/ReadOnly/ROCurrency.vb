﻿' Generated 06 Jul 2014 06:41 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROCurrency
    Inherits OBReadOnlyBase(Of ROCurrency)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CurrencyIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CurrencyID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CurrencyID() As Integer
      Get
        Return GetProperty(CurrencyIDProperty)
      End Get
    End Property

    Public Shared CurrencyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Currency, "Currency", "")
    ''' <summary>
    ''' Gets the Currency value
    ''' </summary>
    <Display(Name:="Currency", Description:="Name of the currency")>
    Public ReadOnly Property Currency() As String
      Get
        Return GetProperty(CurrencyProperty)
      End Get
    End Property

    Public Shared CurrencyCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CurrencyCode, "Currency Code", "")
    ''' <summary>
    ''' Gets the Currency Code value
    ''' </summary>
    <Display(Name:="Currency Code", Description:="The code of the currency")>
    Public ReadOnly Property CurrencyCode() As String
      Get
        Return GetProperty(CurrencyCodeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CurrencyIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Currency

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCurrency(dr As SafeDataReader) As ROCurrency

      Dim r As New ROCurrency()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CurrencyIDProperty, .GetInt32(0))
        LoadProperty(CurrencyProperty, .GetString(1))
        LoadProperty(CurrencyCodeProperty, .GetString(2))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace