﻿' Generated 20 Jun 2014 14:49 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel.ReadOnly

  <Serializable()> _
  Public Class ROAccommodationProvider
    Inherits OBReadOnlyBase(Of ROAccommodationProvider)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccommodationProviderIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationProviderID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccommodationProviderID() As Integer
      Get
        Return GetProperty(AccommodationProviderIDProperty)
      End Get
    End Property

    Public Shared AccommodationProviderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccommodationProvider, "Accommodation Provider", "")
    ''' <summary>
    ''' Gets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Accommodation Provider", Description:="Name of the accommodation provider")>
    Public ReadOnly Property AccommodationProvider() As String
      Get
        Return GetProperty(AccommodationProviderProperty)
      End Get
    End Property

    Public Shared ContactProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Contact, "Contact", "")
    ''' <summary>
    ''' Gets the Contact value
    ''' </summary>
    <Display(Name:="Contact", Description:="Name of the contact person")>
    Public ReadOnly Property Contact() As String
      Get
        Return GetProperty(ContactProperty)
      End Get
    End Property

    Public Shared TelNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TelNo, "Tel No", "")
    ''' <summary>
    ''' Gets the Tel No value
    ''' </summary>
    <Display(Name:="Tel No", Description:="Telephone number for the provider")>
    Public ReadOnly Property TelNo() As String
      Get
        Return GetProperty(TelNoProperty)
      End Get
    End Property

    Public Shared FaxNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FaxNo, "Fax No", "")
    ''' <summary>
    ''' Gets the Fax No value
    ''' </summary>
    <Display(Name:="Fax No", Description:="Fax number for the provider")>
    Public ReadOnly Property FaxNo() As String
      Get
        Return GetProperty(FaxNoProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="Email address for the provider")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CityID, "City", 0)
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="The city that the hotel/B&B is in")>
    Public ReadOnly Property CityID() As Integer
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared BnBIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.BnBInd, "Bn B", False)
    ''' <summary>
    ''' Gets the Bn B value
    ''' </summary>
    <Display(Name:="Bn B", Description:="Tick indicates that this is a B&B")>
    Public ReadOnly Property BnBInd() As Boolean
      Get
        Return GetProperty(BnBIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationProviderIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AccommodationProvider

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAccommodationProvider(dr As SafeDataReader) As ROAccommodationProvider

      Dim r As New ROAccommodationProvider()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AccommodationProviderIDProperty, .GetInt32(0))
        LoadProperty(AccommodationProviderProperty, .GetString(1))
        LoadProperty(ContactProperty, .GetString(2))
        LoadProperty(TelNoProperty, .GetString(3))
        LoadProperty(FaxNoProperty, .GetString(4))
        LoadProperty(EmailAddressProperty, .GetString(5))
        LoadProperty(CityIDProperty, .GetInt32(6))
        LoadProperty(BnBIndProperty, .GetBoolean(7))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace