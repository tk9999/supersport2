﻿' Generated 08 Jul 2014 16:01 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel.ReadOnly

  <Serializable()> _
  Public Class ROCommentTypeList
    Inherits OBReadOnlyListBase(Of ROCommentTypeList, ROCommentType)

#Region " Business Methods "

    Public Function GetItem(CommentTypeID As Integer) As ROCommentType

      For Each child As ROCommentType In Me
        If child.CommentTypeID = CommentTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Comment Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROCommentTypeList() As ROCommentTypeList

      Return New ROCommentTypeList()

    End Function

    Public Shared Sub BeginGetROCommentTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROCommentTypeList)))

      Dim dp As New DataPortal(Of ROCommentTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROCommentTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROCommentTypeList)))

      Dim dp As New DataPortal(Of ROCommentTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROCommentTypeList() As ROCommentTypeList

      Return DataPortal.Fetch(Of ROCommentTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROCommentType.GetROCommentType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROCommentTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace