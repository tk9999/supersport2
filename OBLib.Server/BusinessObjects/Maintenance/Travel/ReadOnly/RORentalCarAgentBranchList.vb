﻿' Generated 03 Jul 2014 09:25 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel.ReadOnly

  <Serializable()> _
  Public Class RORentalCarAgentBranchList
    Inherits OBReadOnlyListBase(Of RORentalCarAgentBranchList, RORentalCarAgentBranch)

#Region " Business Methods "

    Public Function GetItem(RentalCarAgentBranchID As Integer) As RORentalCarAgentBranch

      For Each child As RORentalCarAgentBranch In Me
        If child.RentalCarAgentBranchID = RentalCarAgentBranchID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Rental Car Agent Branches"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property RentalCarAgentID As Integer?
      Public Property IncludeOld As Boolean?
      <PrimarySearchField>
      Public Property AgentBranchName As String

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORentalCarAgentBranchList() As RORentalCarAgentBranchList

      Return New RORentalCarAgentBranchList()

    End Function

    Public Shared Sub BeginGetRORentalCarAgentBranchList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORentalCarAgentBranchList)))

      Dim dp As New DataPortal(Of RORentalCarAgentBranchList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRORentalCarAgentBranchList(CallBack As EventHandler(Of DataPortalResult(Of RORentalCarAgentBranchList)))

      Dim dp As New DataPortal(Of RORentalCarAgentBranchList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORentalCarAgentBranchList() As RORentalCarAgentBranchList

      Return DataPortal.Fetch(Of RORentalCarAgentBranchList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORentalCarAgentBranch.GetRORentalCarAgentBranch(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORentalCarAgentBranchList"
            cm.Parameters.AddWithValue("@RentalCarAgentID", Singular.Misc.NothingDBNull(crit.RentalCarAgentID))
            cm.Parameters.AddWithValue("@AgentBranchName", Singular.Strings.MakeEmptyDBNull(crit.AgentBranchName))
            cm.Parameters.AddWithValue("@IncludeOld", Singular.Misc.NothingDBNull(crit.IncludeOld))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace