﻿' Generated 24 Dec 2014 12:12 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Locations.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel

  <Serializable()> _
  Public Class AccommodationProvider
    Inherits OBBusinessBase(Of AccommodationProvider)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccommodationProviderIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationProviderID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AccommodationProviderID() As Integer
      Get
        Return GetProperty(AccommodationProviderIDProperty)
      End Get
    End Property

    Public Shared AccommodationProviderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccommodationProvider, "Accommodation Provider", "")
    ''' <summary>
    ''' Gets and sets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Accommodation Provider", Description:="Name of the accommodation provider"),
    StringLength(50, ErrorMessage:="Accommodation Provider cannot be more than 50 characters"),
    Required(ErrorMessage:="Accommodation Provider Name Required")>
    Public Property AccommodationProvider() As String
      Get
        Return GetProperty(AccommodationProviderProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AccommodationProviderProperty, Value)
      End Set
    End Property

    Public Shared ContactProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Contact, "Contact", "")
    ''' <summary>
    ''' Gets and sets the Contact value
    ''' </summary>
    <Display(Name:="Contact", Description:="Name of the contact person"),
    StringLength(50, ErrorMessage:="Contact cannot be more than 50 characters")>
    Public Property Contact() As String
      Get
        Return GetProperty(ContactProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContactProperty, Value)
      End Set
    End Property

    Public Shared TelNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TelNo, "Tel No", "")
    ''' <summary>
    ''' Gets and sets the Tel No value
    ''' </summary>
    <Display(Name:="Tel No", Description:="Telephone number for the provider"),
    StringLength(20, ErrorMessage:="Tel No cannot be more than 20 characters"),
    Required(ErrorMessage:="Tel No Required")>
    Public Property TelNo() As String
      Get
        Return GetProperty(TelNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TelNoProperty, Value)
      End Set
    End Property

    Public Shared FaxNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FaxNo, "Fax No", "")
    ''' <summary>
    ''' Gets and sets the Fax No value
    ''' </summary>
    <Display(Name:="Fax No", Description:="Fax number for the provider"),
    StringLength(20, ErrorMessage:="Fax No cannot be more than 20 characters")>
    Public Property FaxNo() As String
      Get
        Return GetProperty(FaxNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FaxNoProperty, Value)
      End Set
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets and sets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="Email address for the provider"),
    StringLength(100, ErrorMessage:="Email Address cannot be more than 100 characters")>
    Public Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmailAddressProperty, Value)
      End Set
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets and sets the City value
    ''' </summary>
    <Display(Name:="City", Description:="The city that the hotel/B&B is in"),
    Required(ErrorMessage:="City required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCityList))>
    Public Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CityIDProperty, Value)
      End Set
    End Property

    Public Shared BnBIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.BnBInd, "Bn B", False)
    ''' <summary>
    ''' Gets and sets the Bn B value
    ''' </summary>
    <Display(Name:="Bn B", Description:="Tick indicates that this is a B&B"),
    Required(ErrorMessage:="Bn B required")>
    Public Property BnBInd() As Boolean
      Get
        Return GetProperty(BnBIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(BnBIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationProviderIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AccommodationProvider.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Accommodation Provider")
        Else
          Return String.Format("Blank {0}", "Accommodation Provider")
        End If
      Else
        Return Me.AccommodationProvider
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccommodationProvider() method.

    End Sub

    Public Shared Function NewAccommodationProvider() As AccommodationProvider

      Return DataPortal.CreateChild(Of AccommodationProvider)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccommodationProvider(dr As SafeDataReader) As AccommodationProvider

      Dim a As New AccommodationProvider()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccommodationProviderIDProperty, .GetInt32(0))
          LoadProperty(AccommodationProviderProperty, .GetString(1))
          LoadProperty(ContactProperty, .GetString(2))
          LoadProperty(TelNoProperty, .GetString(3))
          LoadProperty(FaxNoProperty, .GetString(4))
          LoadProperty(EmailAddressProperty, .GetString(5))
          LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(BnBIndProperty, .GetBoolean(7))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccommodationProvider"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccommodationProvider"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccommodationProviderID As SqlParameter = .Parameters.Add("@AccommodationProviderID", SqlDbType.Int)
          paramAccommodationProviderID.Value = GetProperty(AccommodationProviderIDProperty)
          If Me.IsNew Then
            paramAccommodationProviderID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccommodationProvider", GetProperty(AccommodationProviderProperty))
          .Parameters.AddWithValue("@Contact", GetProperty(ContactProperty))
          .Parameters.AddWithValue("@TelNo", GetProperty(TelNoProperty))
          .Parameters.AddWithValue("@FaxNo", GetProperty(FaxNoProperty))
          .Parameters.AddWithValue("@EmailAddress", GetProperty(EmailAddressProperty))
          .Parameters.AddWithValue("@CityID", GetProperty(CityIDProperty))
          .Parameters.AddWithValue("@BnBInd", GetProperty(BnBIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccommodationProviderIDProperty, paramAccommodationProviderID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccommodationProvider"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccommodationProviderID", GetProperty(AccommodationProviderIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace