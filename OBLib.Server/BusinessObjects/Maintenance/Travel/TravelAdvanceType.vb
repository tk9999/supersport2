﻿' Generated 27 Jan 2015 09:03 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel

  <Serializable()> _
  Public Class TravelAdvanceType
    Inherits OBBusinessBase(Of TravelAdvanceType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TravelAdvanceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelAdvanceTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property TravelAdvanceTypeID() As Integer
      Get
        Return GetProperty(TravelAdvanceTypeIDProperty)
      End Get
    End Property

    Public Shared TravelAdvanceTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TravelAdvanceType, "Travel Advance Type", "")
    ''' <summary>
    ''' Gets and sets the Travel Advance Type value
    ''' </summary>
    <Display(Name:="Travel Advance Type", Description:="Travel advance type name"),
    Required(ErrorMessage:="Travel Advance Type Required"),
    StringLength(50, ErrorMessage:="Travel Advance Type cannot be more than 50 characters")>
    Public Property TravelAdvanceType() As String
      Get
        Return GetProperty(TravelAdvanceTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TravelAdvanceTypeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TravelAdvanceTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.TravelAdvanceType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Travel Advance Type")
        Else
          Return String.Format("Blank {0}", "Travel Advance Type")
        End If
      Else
        Return Me.TravelAdvanceType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTravelAdvanceType() method.

    End Sub

    Public Shared Function NewTravelAdvanceType() As TravelAdvanceType

      Return DataPortal.CreateChild(Of TravelAdvanceType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTravelAdvanceType(dr As SafeDataReader) As TravelAdvanceType

      Dim t As New TravelAdvanceType()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TravelAdvanceTypeIDProperty, .GetInt32(0))
          LoadProperty(TravelAdvanceTypeProperty, .GetString(1))
          LoadProperty(CreatedByProperty, .GetInt32(2))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
          LoadProperty(ModifiedByProperty, .GetInt32(4))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTravelAdvanceType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTravelAdvanceType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTravelAdvanceTypeID As SqlParameter = .Parameters.Add("@TravelAdvanceTypeID", SqlDbType.Int)
          paramTravelAdvanceTypeID.Value = GetProperty(TravelAdvanceTypeIDProperty)
          If Me.IsNew Then
            paramTravelAdvanceTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@TravelAdvanceType", GetProperty(TravelAdvanceTypeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(TravelAdvanceTypeIDProperty, paramTravelAdvanceTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTravelAdvanceType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TravelAdvanceTypeID", GetProperty(TravelAdvanceTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace