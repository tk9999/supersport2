﻿' Generated 24 Dec 2014 11:46 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.General

  <Serializable()> _
  Public Class Currency
    Inherits OBBusinessBase(Of Currency)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CurrencyIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CurrencyID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property CurrencyID() As Integer
      Get
        Return GetProperty(CurrencyIDProperty)
      End Get
    End Property

    Public Shared CurrencyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Currency, "Currency", "")
    ''' <summary>
    ''' Gets and sets the Currency value
    ''' </summary>
    <Display(Name:="Currency", Description:="Name of the currency"),
    StringLength(50, ErrorMessage:="Currency cannot be more than 50 characters"),
    Required(ErrorMessage:="Currency Required")>
    Public Property Currency() As String
      Get
        Return GetProperty(CurrencyProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CurrencyProperty, Value)
      End Set
    End Property

    Public Shared CurrencyCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CurrencyCode, "Currency Code", "")
    ''' <summary>
    ''' Gets and sets the Currency Code value
    ''' </summary>
    <Display(Name:="Currency Code", Description:="The code of the currency"),
    StringLength(10, ErrorMessage:="Currency Code cannot be more than 10 characters"),
    Required(ErrorMessage:="Currency Code Required")>
    Public Property CurrencyCode() As String
      Get
        Return GetProperty(CurrencyCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CurrencyCodeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CurrencyIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Currency.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Currency")
        Else
          Return String.Format("Blank {0}", "Currency")
        End If
      Else
        Return Me.Currency
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCurrency() method.

    End Sub

    Public Shared Function NewCurrency() As Currency

      Return DataPortal.CreateChild(Of Currency)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCurrency(dr As SafeDataReader) As Currency

      Dim c As New Currency()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CurrencyIDProperty, .GetInt32(0))
          LoadProperty(CurrencyProperty, .GetString(1))
          LoadProperty(CurrencyCodeProperty, .GetString(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCurrency"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCurrency"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCurrencyID As SqlParameter = .Parameters.Add("@CurrencyID", SqlDbType.Int)
          paramCurrencyID.Value = GetProperty(CurrencyIDProperty)
          If Me.IsNew Then
            paramCurrencyID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Currency", GetProperty(CurrencyProperty))
          .Parameters.AddWithValue("@CurrencyCode", GetProperty(CurrencyCodeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CurrencyIDProperty, paramCurrencyID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCurrency"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CurrencyID", GetProperty(CurrencyIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace