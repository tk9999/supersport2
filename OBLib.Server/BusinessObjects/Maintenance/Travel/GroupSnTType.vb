﻿' Generated 27 Jan 2015 09:32 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel

  <Serializable()> _
  Public Class GroupSnTType
    Inherits OBBusinessBase(Of GroupSnTType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared GroupSnTTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GroupSnTTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property GroupSnTTypeID() As Integer
      Get
        Return GetProperty(GroupSnTTypeIDProperty)
      End Get
    End Property

    Public Shared GroupSnTTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GroupSnTType, "Group S&T Type", "")
    ''' <summary>
    ''' Gets and sets the Group Sn T Type value
    ''' </summary>
    <Display(Name:="S&T Type", Description:="Group S&T Type"),
    Required(ErrorMessage:="S&T Type Required"),
    StringLength(50, ErrorMessage:="Group Sn T Type cannot be more than 50 characters")>
    Public Property GroupSnTType() As String
      Get
        Return GetProperty(GroupSnTTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GroupSnTTypeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(GroupSnTTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.GroupSnTType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Group S&T Type")
        Else
          Return String.Format("Blank {0}", "Group S&T Type")
        End If
      Else
        Return Me.GroupSnTType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewGroupSnTType() method.

    End Sub

    Public Shared Function NewGroupSnTType() As GroupSnTType

      Return DataPortal.CreateChild(Of GroupSnTType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetGroupSnTType(dr As SafeDataReader) As GroupSnTType

      Dim g As New GroupSnTType()
      g.Fetch(dr)
      Return g

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(GroupSnTTypeIDProperty, .GetInt32(0))
          LoadProperty(GroupSnTTypeProperty, .GetString(1))
          LoadProperty(CreatedByProperty, .GetInt32(2))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
          LoadProperty(ModifiedByProperty, .GetInt32(4))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insGroupSnTType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updGroupSnTType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramGroupSnTTypeID As SqlParameter = .Parameters.Add("@GroupSnTTypeID", SqlDbType.Int)
          paramGroupSnTTypeID.Value = GetProperty(GroupSnTTypeIDProperty)
          If Me.IsNew Then
            paramGroupSnTTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@GroupSnTType", GetProperty(GroupSnTTypeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(GroupSnTTypeIDProperty, paramGroupSnTTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delGroupSnTType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@GroupSnTTypeID", GetProperty(GroupSnTTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace