﻿' Generated 27 Jan 2015 07:50 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel

  <Serializable()> _
  Public Class CarType
    Inherits OBBusinessBase(Of CarType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CarTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CarTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property CarTypeID() As Integer
      Get
        Return GetProperty(CarTypeIDProperty)
      End Get
    End Property

    Public Shared CarTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CarType, "Car Type", "")
    ''' <summary>
    ''' Gets and sets the Car Type value
    ''' </summary>
    <Display(Name:="Car Type", Description:="Description of the type of car"),
    Required(ErrorMessage:="Car Type Required"),
    StringLength(50, ErrorMessage:="Car Type cannot be more than 50 characters")>
    Public Property CarType() As String
      Get
        Return GetProperty(CarTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CarTypeProperty, Value)
      End Set
    End Property

    Public Shared CarTypeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CarTypeDescription, "Car Type Description", "")
    ''' <summary>
    ''' Gets and sets the Car Type Description value
    ''' </summary>
    <Display(Name:="Car Type Description", Description:="Description of the type of car"),
    StringLength(50, ErrorMessage:="Car Type Description cannot be more than 50 characters")>
    Public Property CarTypeDescription() As String
      Get
        Return GetProperty(CarTypeDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CarTypeDescriptionProperty, Value)
      End Set
    End Property

    Public Shared MaxNoOfOccupantsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxNoOfOccupants, "Max No Of Occupants", 0)
    ''' <summary>
    ''' Gets and sets the Max No Of Occupants value
    ''' </summary>
    <Display(Name:="Max No Of Occupants", Description:="Maximum number of passengers this vehicle type can hold"),
    Required(ErrorMessage:="Max No Of Occupants required")>
    Public Property MaxNoOfOccupants() As Integer
      Get
        Return GetProperty(MaxNoOfOccupantsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MaxNoOfOccupantsProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CarTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CarType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Car Type")
        Else
          Return String.Format("Blank {0}", "Car Type")
        End If
      Else
        Return Me.CarType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCarType() method.

    End Sub

    Public Shared Function NewCarType() As CarType

      Return DataPortal.CreateChild(Of CarType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCarType(dr As SafeDataReader) As CarType

      Dim c As New CarType()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CarTypeIDProperty, .GetInt32(0))
          LoadProperty(CarTypeProperty, .GetString(1))
          LoadProperty(CarTypeDescriptionProperty, .GetString(2))
          LoadProperty(MaxNoOfOccupantsProperty, .GetInt32(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCarType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCarType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCarTypeID As SqlParameter = .Parameters.Add("@CarTypeID", SqlDbType.Int)
          paramCarTypeID.Value = GetProperty(CarTypeIDProperty)
          If Me.IsNew Then
            paramCarTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@CarType", GetProperty(CarTypeProperty))
          .Parameters.AddWithValue("@CarTypeDescription", GetProperty(CarTypeDescriptionProperty))
          .Parameters.AddWithValue("@MaxNoOfOccupants", GetProperty(MaxNoOfOccupantsProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CarTypeIDProperty, paramCarTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCarType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CarTypeID", GetProperty(CarTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace