﻿' Generated 24 Dec 2014 12:00 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Locations.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel

  <Serializable()> _
  Public Class Airport
    Inherits OBBusinessBase(Of Airport)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AirportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AirportID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AirportID() As Integer
      Get
        Return GetProperty(AirportIDProperty)
      End Get
    End Property

    Public Shared AirportProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Airport, "Airport", "")
    ''' <summary>
    ''' Gets and sets the Airport value
    ''' </summary>
    <Display(Name:="Airport", Description:="Airport Name"),
    Required(ErrorMessage:="Airport required"),
    StringLength(100, ErrorMessage:="Airport cannot be more than 100 characters")>
    Public Property Airport() As String
      Get
        Return GetProperty(AirportProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AirportProperty, Value)
      End Set
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets and sets the City value
    ''' </summary>
    <Display(Name:="City", Description:="City the airport resides in"),
    Required(ErrorMessage:="City required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCityList))>
    Public Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CityIDProperty, Value)
      End Set
    End Property

    Public Shared RedundantIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RedundantInd, "Redundant", False)
    ''' <summary>
    ''' Gets and sets the Redundant value
    ''' </summary>
    <Display(Name:="Redundant", Description:="True if the Aiport is redundant"),
    Required(ErrorMessage:="Redundant required")>
    Public Property RedundantInd() As Boolean
      Get
        Return GetProperty(RedundantIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RedundantIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AirportIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Airport.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Airport")
        Else
          Return String.Format("Blank {0}", "Airport")
        End If
      Else
        Return Me.Airport
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAirport() method.

    End Sub

    Public Shared Function NewAirport() As Airport

      Return DataPortal.CreateChild(Of Airport)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAirport(dr As SafeDataReader) As Airport

      Dim a As New Airport()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AirportIDProperty, .GetInt32(0))
          LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AirportProperty, .GetString(2))
          LoadProperty(RedundantIndProperty, .GetBoolean(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAirport"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAirport"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAirportID As SqlParameter = .Parameters.Add("@AirportID", SqlDbType.Int)
          paramAirportID.Value = GetProperty(AirportIDProperty)
          If Me.IsNew Then
            paramAirportID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@CityID", GetProperty(CityIDProperty))
          .Parameters.AddWithValue("@Airport", GetProperty(AirportProperty))
          .Parameters.AddWithValue("@RedundantInd", GetProperty(RedundantIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AirportIDProperty, paramAirportID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAirport"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AirportID", GetProperty(AirportIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace