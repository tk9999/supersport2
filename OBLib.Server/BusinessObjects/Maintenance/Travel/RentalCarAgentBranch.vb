﻿' Generated 29 Dec 2014 15:31 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Locations.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel

  <Serializable()> _
  Public Class RentalCarAgentBranch
    Inherits OBBusinessBase(Of RentalCarAgentBranch)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "RentalCarAgentBranchBO.RentalCarAgentBranchBOToString(self)")

    Public Shared RentalCarAgentBranchIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarAgentBranchID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property RentalCarAgentBranchID() As Integer
      Get
        Return GetProperty(RentalCarAgentBranchIDProperty)
      End Get
    End Property

    Public Shared RentalCarAgentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RentalCarAgentID, "Rental Car Agent", Nothing)
    ''' <summary>
    ''' Gets and sets the Rental Car Agent value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public Property RentalCarAgentID() As Integer?
      Get
        Return GetProperty(RentalCarAgentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RentalCarAgentIDProperty, Value)
      End Set
    End Property

    Public Shared RentalCarAgentBranchProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarAgentBranch, "Rental Car Agent Branch", "")
    ''' <summary>
    ''' Gets and sets the Rental Car Agent Branch value
    ''' </summary>
    <Display(Name:="Rental Car Agent Branch", Description:="branch name"),
    StringLength(200, ErrorMessage:="Rental Car Agent Branch cannot be more than 200 characters")>
    Public Property RentalCarAgentBranch() As String
      Get
        Return GetProperty(RentalCarAgentBranchProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RentalCarAgentBranchProperty, Value)
      End Set
    End Property

    Public Shared InternationalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InternationalInd, "International", False)
    ''' <summary>
    ''' Gets and sets the International value
    ''' </summary>
    <Display(Name:="International", Description:="is located internationally"),
    Required(ErrorMessage:="International required")>
    Public Property InternationalInd() As Boolean
      Get
        Return GetProperty(InternationalIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(InternationalIndProperty, Value)
      End Set
    End Property

    Public Shared LocationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LocationID, "Location", Nothing)
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="location in which the rental car agent is located"),
    Required(ErrorMessage:="Location required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROLocationList),
                                         DropDownColumns:={"Location", "City", "Country"})>
    Public Property LocationID() As Integer?
      Get
        Return GetProperty(LocationIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LocationIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared OldIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OldInd, "Old", False)
    ''' <summary>
    ''' Gets and sets the Old value
    ''' </summary>
    <Display(Name:="Old", Description:="is and old branch and is no longer selectable")>
    Public Property OldInd() As Boolean
      Get
        Return GetProperty(OldIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OldIndProperty, Value)
      End Set
    End Property

    Public Shared OldDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.OldDate, Nothing)
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Old Date", Description:="The date from which this branch is considered old")>
    Public Property OldDate() As DateTime?
      Get
        Return GetProperty(OldDateProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(OldDateProperty, value)
      End Set
    End Property

    Public Shared OldReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OldReason, "Old Reason", "")
    ''' <summary>
    ''' Gets and sets the Rental Car Agent Branch value
    ''' </summary>
    <Display(Name:="Old Reason", Description:="Why is this branch considered old"),
    StringLength(200, ErrorMessage:="Old Reason cannot be more than 100 characters")>
    Public Property OldReason() As String
      Get
        Return GetProperty(OldReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OldReasonProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As RentalCarAgent

      Return CType(CType(Me.Parent, RentalCarAgentBranchList).Parent, RentalCarAgent)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarAgentBranchIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.RentalCarAgentBranch.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Rental Car Agent Branch")
        Else
          Return String.Format("Blank {0}", "Rental Car Agent Branch")
        End If
      Else
        Return Me.RentalCarAgentBranch
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(OldDateProperty)
        .AddTriggerProperty(OldIndProperty)
        .JavascriptRuleFunctionName = "RentalCarAgentBranchBO.OldDateValid"
        .ServerRuleFunction = AddressOf OldDateValid
      End With

      With AddWebRule(OldReasonProperty)
        .AddTriggerProperty(OldIndProperty)
        .JavascriptRuleFunctionName = "RentalCarAgentBranchBO.OldReasonValid"
        .ServerRuleFunction = AddressOf OldReasonValid
      End With

    End Sub

    Public Shared Function OldDateValid(RentalCarAgentBranch As RentalCarAgentBranch) As String
      If RentalCarAgentBranch.OldInd And RentalCarAgentBranch.OldDate Is Nothing Then
        Return "Old Date is required"
      End If
      Return ""
    End Function

    Public Shared Function OldReasonValid(RentalCarAgentBranch As RentalCarAgentBranch) As String
      If RentalCarAgentBranch.OldInd And RentalCarAgentBranch.OldReason.Trim.Length = 0 Then
        Return "Old Reason is required"
      End If
      Return ""
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRentalCarAgentBranch() method.

    End Sub

    Public Shared Function NewRentalCarAgentBranch() As RentalCarAgentBranch

      Return DataPortal.CreateChild(Of RentalCarAgentBranch)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRentalCarAgentBranch(dr As SafeDataReader) As RentalCarAgentBranch

      Dim r As New RentalCarAgentBranch()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RentalCarAgentBranchIDProperty, .GetInt32(0))
          LoadProperty(RentalCarAgentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(RentalCarAgentBranchProperty, .GetString(2))
          LoadProperty(InternationalIndProperty, .GetBoolean(3))
          LoadProperty(LocationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(OldIndProperty, .GetBoolean(9))
          LoadProperty(OldDateProperty, .GetValue(10))
          LoadProperty(OldReasonProperty, .GetString(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insRentalCarAgentBranch"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updRentalCarAgentBranch"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramRentalCarAgentBranchID As SqlParameter = .Parameters.Add("@RentalCarAgentBranchID", SqlDbType.Int)
          paramRentalCarAgentBranchID.Value = GetProperty(RentalCarAgentBranchIDProperty)
          If Me.IsNew Then
            paramRentalCarAgentBranchID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@RentalCarAgentID", Me.GetParent.RentalCarAgentID)
          .Parameters.AddWithValue("@RentalCarAgentBranch", GetProperty(RentalCarAgentBranchProperty))
          .Parameters.AddWithValue("@InternationalInd", GetProperty(InternationalIndProperty))
          .Parameters.AddWithValue("@LocationID", GetProperty(LocationIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@OldInd", GetProperty(OldIndProperty))
          .Parameters.AddWithValue("@OldDate", (New SmartDate(GetProperty(OldDateProperty)).DBValue))
          .Parameters.AddWithValue("@OldReason", GetProperty(OldReasonProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(RentalCarAgentBranchIDProperty, paramRentalCarAgentBranchID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delRentalCarAgentBranch"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@RentalCarAgentBranchID", GetProperty(RentalCarAgentBranchIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace