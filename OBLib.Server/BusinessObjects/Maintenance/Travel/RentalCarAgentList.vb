﻿' Generated 24 Dec 2014 12:03 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Travel

  <Serializable()> _
  Public Class RentalCarAgentList
    Inherits OBBusinessListBase(Of RentalCarAgentList, RentalCarAgent)

#Region " Business Methods "

    Public Function GetItem(RentalCarAgentID As Integer) As RentalCarAgent

      For Each child As RentalCarAgent In Me
        If child.RentalCarAgentID = RentalCarAgentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Rental Car Agents"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRentalCarAgentList() As RentalCarAgentList

      Return New RentalCarAgentList()

    End Function

    Public Shared Sub BeginGetRentalCarAgentList(CallBack As EventHandler(Of DataPortalResult(Of RentalCarAgentList)))

      Dim dp As New DataPortal(Of RentalCarAgentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRentalCarAgentList() As RentalCarAgentList

      Return DataPortal.Fetch(Of RentalCarAgentList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(RentalCarAgent.GetRentalCarAgent(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As RentalCarAgent = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.RentalCarAgentID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.RentalCarAgentBranchList.RaiseListChangedEvents = False
          parent.RentalCarAgentBranchList.Add(RentalCarAgentBranch.GetRentalCarAgentBranch(sdr))
          parent.RentalCarAgentBranchList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As RentalCarAgent In Me
        child.CheckRules()
        For Each RentalCarAgentBranch As RentalCarAgentBranch In child.RentalCarAgentBranchList
          RentalCarAgentBranch.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRentalCarAgentList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As RentalCarAgent In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As RentalCarAgent In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace