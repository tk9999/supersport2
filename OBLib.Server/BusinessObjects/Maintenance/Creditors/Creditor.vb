﻿' Generated 28 Jan 2015 11:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Creditors

  <Serializable()> _
  Public Class Creditor
    Inherits OBBusinessBase(Of Creditor)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CreditorIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreditorID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property CreditorID() As Integer
      Get
        Return GetProperty(CreditorIDProperty)
      End Get
    End Property

    Public Shared CreditorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Creditor, "Creditor", "")
    ''' <summary>
    ''' Gets and sets the Creditor value
    ''' </summary>
    <Display(Name:="Creditor", Description:="Creditor Name"),
    Required(ErrorMessage:="Creditor Required"),
    StringLength(250, ErrorMessage:="Creditor cannot be more than 250 characters")>
    Public Property Creditor() As String
      Get
        Return GetProperty(CreditorProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CreditorProperty, Value)
      End Set
    End Property

    Public Shared VATRegNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VATRegNo, "VAT Reg No", "")
    ''' <summary>
    ''' Gets and sets the VAT Reg No value
    ''' </summary>
    <Display(Name:="VAT Reg No", Description:="VAT Registration Number for creditors registered"),
    StringLength(20, ErrorMessage:="VAT Reg No cannot be more than 20 characters")>
    Public Property VATRegNo() As String
      Get
        Return GetProperty(VATRegNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VATRegNoProperty, Value)
      End Set
    End Property

    Public Shared PhysicalAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PhysicalAddress, "Physical Address", "")
    ''' <summary>
    ''' Gets and sets the Physical Address value
    ''' </summary>
    <Display(Name:="Physical Address", Description:="Physical Address"),
    StringLength(255, ErrorMessage:="Physical Address cannot be more than 255 characters")>
    Public Property PhysicalAddress() As String
      Get
        Return GetProperty(PhysicalAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PhysicalAddressProperty, Value)
      End Set
    End Property

    Public Shared PostalAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PostalAddress, "Postal Address", "")
    ''' <summary>
    ''' Gets and sets the Postal Address value
    ''' </summary>
    <Display(Name:="Postal Address", Description:="Postal Address"),
    StringLength(255, ErrorMessage:="Postal Address cannot be more than 255 characters")>
    Public Property PostalAddress() As String
      Get
        Return GetProperty(PostalAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PostalAddressProperty, Value)
      End Set
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets and sets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="Email Address"),
    StringLength(255, ErrorMessage:="Email Address cannot be more than 255 characters")>
    Public Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmailAddressProperty, Value)
      End Set
    End Property

    Public Shared ContactPersonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactPerson, "Contact Person", "")
    ''' <summary>
    ''' Gets and sets the Contact Person value
    ''' </summary>
    <Display(Name:="Contact Person", Description:="The contact person of the creditor"),
    StringLength(255, ErrorMessage:="Contact Person cannot be more than 255 characters")>
    Public Property ContactPerson() As String
      Get
        Return GetProperty(ContactPersonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContactPersonProperty, Value)
      End Set
    End Property

    Public Shared ContactPersonNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactPersonNumber, "Contact Person Number", "")
    ''' <summary>
    ''' Gets and sets the Contact Person Number value
    ''' </summary>
    <Display(Name:="Contact Person Number", Description:="The contact number of the contact person for the Creditor"),
    StringLength(15, ErrorMessage:="Contact Person Number cannot be more than 15 characters")>
    Public Property ContactPersonNumber() As String
      Get
        Return GetProperty(ContactPersonNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContactPersonNumberProperty, Value)
      End Set
    End Property

    Public Shared CoRegNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CoRegNo, "Co Reg No", "")
    ''' <summary>
    ''' Gets and sets the Co Reg No value
    ''' </summary>
    <Display(Name:="Co Reg No", Description:="Company Registration Number"),
    StringLength(50, ErrorMessage:="Co Reg No cannot be more than 50 characters")>
    Public Property CoRegNo() As String
      Get
        Return GetProperty(CoRegNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CoRegNoProperty, Value)
      End Set
    End Property

    Public Shared TelNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TelNo, "Tel No", "")
    ''' <summary>
    ''' Gets and sets the Tel No value
    ''' </summary>
    <Display(Name:="Tel No", Description:="Telephone number"),
    StringLength(20, ErrorMessage:="Tel No cannot be more than 20 characters")>
    Public Property TelNo() As String
      Get
        Return GetProperty(TelNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TelNoProperty, Value)
      End Set
    End Property

    Public Shared FaxNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FaxNo, "Fax No", "")
    ''' <summary>
    ''' Gets and sets the Fax No value
    ''' </summary>
    <Display(Name:="Fax No", Description:="Facsimile number"),
    StringLength(20, ErrorMessage:="Fax No cannot be more than 20 characters")>
    Public Property FaxNo() As String
      Get
        Return GetProperty(FaxNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FaxNoProperty, Value)
      End Set
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="Supplier linked to the creditor"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSupplierList))>
    Public Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierIDProperty, Value)
      End Set
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code", "")
    ''' <summary>
    ''' Gets and sets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="Employee code of the creditor"),
    Required(ErrorMessage:="Employee Code Required"),
    StringLength(30, ErrorMessage:="Employee Code cannot be more than 30 characters")>
    Public Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmployeeCodeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CreditorIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Creditor.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Creditor")
        Else
          Return String.Format("Blank {0}", "Creditor")
        End If
      Else
        Return Me.Creditor
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCreditor() method.

    End Sub

    Public Shared Function NewCreditor() As Creditor

      Return DataPortal.CreateChild(Of Creditor)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCreditor(dr As SafeDataReader) As Creditor

      Dim c As New Creditor()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CreditorIDProperty, .GetInt32(0))
          LoadProperty(CreditorProperty, .GetString(1))
          LoadProperty(VATRegNoProperty, .GetString(2))
          LoadProperty(PhysicalAddressProperty, .GetString(3))
          LoadProperty(PostalAddressProperty, .GetString(4))
          LoadProperty(EmailAddressProperty, .GetString(5))
          LoadProperty(ContactPersonProperty, .GetString(6))
          LoadProperty(ContactPersonNumberProperty, .GetString(7))
          LoadProperty(CoRegNoProperty, .GetString(8))
          LoadProperty(TelNoProperty, .GetString(9))
          LoadProperty(FaxNoProperty, .GetString(10))
          LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(EmployeeCodeProperty, .GetString(12))
          LoadProperty(CreatedByProperty, .GetInt32(13))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(14))
          LoadProperty(ModifiedByProperty, .GetInt32(15))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(16))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCreditor"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCreditor"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCreditorID As SqlParameter = .Parameters.Add("@CreditorID", SqlDbType.Int)
          paramCreditorID.Value = GetProperty(CreditorIDProperty)
          If Me.IsNew Then
            paramCreditorID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Creditor", GetProperty(CreditorProperty))
          .Parameters.AddWithValue("@VATRegNo", GetProperty(VATRegNoProperty))
          .Parameters.AddWithValue("@PhysicalAddress", GetProperty(PhysicalAddressProperty))
          .Parameters.AddWithValue("@PostalAddress", GetProperty(PostalAddressProperty))
          .Parameters.AddWithValue("@EmailAddress", GetProperty(EmailAddressProperty))
          .Parameters.AddWithValue("@ContactPerson", GetProperty(ContactPersonProperty))
          .Parameters.AddWithValue("@ContactPersonNumber", GetProperty(ContactPersonNumberProperty))
          .Parameters.AddWithValue("@CoRegNo", GetProperty(CoRegNoProperty))
          .Parameters.AddWithValue("@TelNo", GetProperty(TelNoProperty))
          .Parameters.AddWithValue("@FaxNo", GetProperty(FaxNoProperty))
          .Parameters.AddWithValue("@SupplierID", Singular.Misc.NothingDBNull(GetProperty(SupplierIDProperty)))
          .Parameters.AddWithValue("@EmployeeCode", GetProperty(EmployeeCodeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CreditorIDProperty, paramCreditorID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCreditor"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CreditorID", GetProperty(CreditorIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace