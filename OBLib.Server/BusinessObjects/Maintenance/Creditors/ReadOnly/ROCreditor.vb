﻿' Generated 14 Apr 2014 09:35 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Creditors.ReadOnly

  <Serializable()> _
  Public Class ROCreditor
    Inherits SingularReadOnlyBase(Of ROCreditor)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CreditorIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreditorID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CreditorID() As Integer
      Get
        Return GetProperty(CreditorIDProperty)
      End Get
    End Property

    Public Shared CreditorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Creditor, "Creditor", "")
    ''' <summary>
    ''' Gets the Creditor value
    ''' </summary>
    <Display(Name:="Creditor", Description:="Creditor Name")>
    Public ReadOnly Property Creditor() As String
      Get
        Return GetProperty(CreditorProperty)
      End Get
    End Property

    Public Shared VATRegNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VATRegNo, "VAT Reg No", "")
    ''' <summary>
    ''' Gets the VAT Reg No value
    ''' </summary>
    <Display(Name:="VAT Reg No", Description:="VAT Registration Number for creditors registered")>
    Public ReadOnly Property VATRegNo() As String
      Get
        Return GetProperty(VATRegNoProperty)
      End Get
    End Property

    Public Shared PhysicalAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PhysicalAddress, "Physical Address", "")
    ''' <summary>
    ''' Gets the Physical Address value
    ''' </summary>
    <Display(Name:="Physical Address", Description:="Physical Address")>
    Public ReadOnly Property PhysicalAddress() As String
      Get
        Return GetProperty(PhysicalAddressProperty)
      End Get
    End Property

    Public Shared PostalAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PostalAddress, "Postal Address", "")
    ''' <summary>
    ''' Gets the Postal Address value
    ''' </summary>
    <Display(Name:="Postal Address", Description:="Postal Address")>
    Public ReadOnly Property PostalAddress() As String
      Get
        Return GetProperty(PostalAddressProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="Email Address")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared ContactPersonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactPerson, "Contact Person", "")
    ''' <summary>
    ''' Gets the Contact Person value
    ''' </summary>
    <Display(Name:="Contact Person", Description:="The contact person of the creditor")>
    Public ReadOnly Property ContactPerson() As String
      Get
        Return GetProperty(ContactPersonProperty)
      End Get
    End Property

    Public Shared ContactPersonNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactPersonNumber, "Contact Person Number", "")
    ''' <summary>
    ''' Gets the Contact Person Number value
    ''' </summary>
    <Display(Name:="Contact Person Number", Description:="The contact number of the contact person for the Creditor")>
    Public ReadOnly Property ContactPersonNumber() As String
      Get
        Return GetProperty(ContactPersonNumberProperty)
      End Get
    End Property

    Public Shared CoRegNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CoRegNo, "Co Reg No", "")
    ''' <summary>
    ''' Gets the Co Reg No value
    ''' </summary>
    <Display(Name:="Co Reg No", Description:="Company Registration Number")>
    Public ReadOnly Property CoRegNo() As String
      Get
        Return GetProperty(CoRegNoProperty)
      End Get
    End Property

    Public Shared TelNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TelNo, "Tel No", "")
    ''' <summary>
    ''' Gets the Tel No value
    ''' </summary>
    <Display(Name:="Tel No", Description:="Telephone number")>
    Public ReadOnly Property TelNo() As String
      Get
        Return GetProperty(TelNoProperty)
      End Get
    End Property

    Public Shared FaxNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FaxNo, "Fax No", "")
    ''' <summary>
    ''' Gets the Fax No value
    ''' </summary>
    <Display(Name:="Fax No", Description:="Facsimile number")>
    Public ReadOnly Property FaxNo() As String
      Get
        Return GetProperty(FaxNoProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code", "")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="The employee code from payroll")>
    Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public ReadOnly Property CreditorWithEmployeeCode As String
      Get
        Return Creditor & IIf(EmployeeCode.Trim.Length > 0, " (" & EmployeeCode & ")", " (No Employee Code)")
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CreditorIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Creditor

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCreditor(dr As SafeDataReader) As ROCreditor

      Dim r As New ROCreditor()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CreditorIDProperty, .GetInt32(0))
        LoadProperty(CreditorProperty, .GetString(1))
        LoadProperty(VATRegNoProperty, .GetString(2))
        LoadProperty(PhysicalAddressProperty, .GetString(3))
        LoadProperty(PostalAddressProperty, .GetString(4))
        LoadProperty(EmailAddressProperty, .GetString(5))
        LoadProperty(ContactPersonProperty, .GetString(6))
        LoadProperty(ContactPersonNumberProperty, .GetString(7))
        LoadProperty(CoRegNoProperty, .GetString(8))
        LoadProperty(TelNoProperty, .GetString(9))
        LoadProperty(FaxNoProperty, .GetString(10))
        LoadProperty(EmployeeCodeProperty, .GetString(11))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace