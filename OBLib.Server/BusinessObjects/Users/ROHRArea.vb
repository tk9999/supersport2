﻿' Generated 09 Sep 2015 05:53 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Users.ReadOnly

  <Serializable()> _
  Public Class ROHRArea
    Inherits OBReadOnlyBase(Of ROHRArea)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceSystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceSystemID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property HumanResourceSystemID() As Integer
      Get
        Return GetProperty(HumanResourceSystemIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared SubDeptProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubDept, "Sub Dept")
    ''' <summary>
    ''' Gets the Sub Dept value
    ''' </summary>
    <Display(Name:="Sub Dept", Description:="")>
  Public ReadOnly Property SubDept() As String
      Get
        Return GetProperty(SubDeptProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
  Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared AreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Area, "Area")
    ''' <summary>
    ''' Gets the Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
  Public ReadOnly Property Area() As String
      Get
        Return GetProperty(AreaProperty)
      End Get
    End Property

    Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryInd, "Primary", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="Primary", Description:="")>
  Public ReadOnly Property PrimaryInd() As Boolean
      Get
        Return GetProperty(PrimaryIndProperty)
      End Get
    End Property

    Public Shared SwapsAllowedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SwapsAllowed, "SwapsAllowed", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="Primary", Description:="")>
    Public ReadOnly Property SwapsAllowed() As Boolean
      Get
        Return GetProperty(SwapsAllowedProperty)
      End Get
    End Property

    Public Shared CanViewOtherPersonnelSchedulesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanViewOtherPersonnelSchedules, "Primary", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="CanViewOtherPersonnelSchedules", Description:="")>
    Public ReadOnly Property CanViewOtherPersonnelSchedules() As Boolean
      Get
        Return GetProperty(CanViewOtherPersonnelSchedulesProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceSystemIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SubDept

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHRArea(dr As SafeDataReader) As ROHRArea

      Dim r As New ROHRArea()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceSystemIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SubDeptProperty, .GetString(3))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(AreaProperty, .GetString(5))
        LoadProperty(PrimaryIndProperty, .GetBoolean(6))
        LoadProperty(CanViewOtherPersonnelSchedulesProperty, .GetBoolean(7))
        LoadProperty(SwapsAllowedProperty, .GetBoolean(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace