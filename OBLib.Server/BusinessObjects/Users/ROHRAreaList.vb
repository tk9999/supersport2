﻿' Generated 09 Sep 2015 05:53 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Users.ReadOnly

  <Serializable()> _
  Public Class ROHRAreaList
    Inherits OBReadOnlyListBase(Of ROHRAreaList, ROHRArea)

#Region " Business Methods "

    Public Function GetItem(HumanResourceSystemID As Integer) As ROHRArea

      For Each child As ROHRArea In Me
        If child.HumanResourceSystemID = HumanResourceSystemID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "R Os"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHRAreaList() As ROHRAreaList

      Return New ROHRAreaList()

    End Function

    Public Shared Sub BeginGetROHRAreaList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHRAreaList)))

      Dim dp As New DataPortal(Of ROHRAreaList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROHRAreaList(CallBack As EventHandler(Of DataPortalResult(Of ROHRAreaList)))

      Dim dp As New DataPortal(Of ROHRAreaList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHRAreaList() As ROHRAreaList

      Return DataPortal.Fetch(Of ROHRAreaList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHRArea.GetROHRArea(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHRAreaList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace