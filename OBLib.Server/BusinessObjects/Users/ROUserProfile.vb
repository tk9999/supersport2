﻿' Generated 09 Sep 2015 04:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Users.ReadOnly

  <Serializable()>
  Public Class ROUserProfile
    Inherits SingularReadOnlyBase(Of ROUserProfile)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared FirstnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Firstname, "Firstname")
    ''' <summary>
    ''' Gets the Firstname value
    ''' </summary>
    <Display(Name:="Firstname", Description:="")>
    Public ReadOnly Property Firstname() As String
      Get
        Return GetProperty(FirstnameProperty)
      End Get
    End Property

    Public Shared SecondNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SecondName, "Second Name")
    ''' <summary>
    ''' Gets the Second Name value
    ''' </summary>
    <Display(Name:="Second Name", Description:="")>
    Public ReadOnly Property SecondName() As String
      Get
        Return GetProperty(SecondNameProperty)
      End Get
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name")
    ''' <summary>
    ''' Gets the Preferred Name value
    ''' </summary>
    <Display(Name:="Preferred Name", Description:="")>
    Public ReadOnly Property PreferredName() As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="")>
    Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "SA ID Number")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
    Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "Contract Type", Nothing)
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared ContractDaysProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractDays, "Contract Days (only if Presenter/Commentator)", Nothing)
    ''' <summary>
    ''' Gets the Contract Days value
    ''' </summary>
    <Display(Name:="Contract Days (only if Presenter/Commentator)", Description:="")>
    Public ReadOnly Property ContractDays() As Integer?
      Get
        Return GetProperty(ContractDaysProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared AlternativeEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeEmailAddress, "Alternative Email Address")
    ''' <summary>
    ''' Gets the Alternative Email Address value
    ''' </summary>
    <Display(Name:="Alternative Email Address", Description:="")>
    Public ReadOnly Property AlternativeEmailAddress() As String
      Get
        Return GetProperty(AlternativeEmailAddressProperty)
      End Get
    End Property

    Public Shared PhoneEmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PhoneEmailAddress, "Phone Email Address")
    ''' <summary>
    ''' Gets the Phone Email Address value
    ''' </summary>
    <Display(Name:="Phone Email Address", Description:="")>
    Public ReadOnly Property PhoneEmailAddress() As String
      Get
        Return GetProperty(PhoneEmailAddressProperty)
      End Get
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number")
    ''' <summary>
    ''' Gets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Cell Phone Number", Description:="")>
    Public ReadOnly Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
    End Property

    Public Shared AlternativeContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AlternativeContactNumber, "Alternative Contact Number")
    ''' <summary>
    ''' Gets the Alternative Contact Number value
    ''' </summary>
    <Display(Name:="Alternative Contact Number", Description:="")>
    Public ReadOnly Property AlternativeContactNumber() As String
      Get
        Return GetProperty(AlternativeContactNumberProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "Home City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="Home City", Description:="")>
    Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public Shared RaceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RaceID, "Race", Nothing)
    ''' <summary>
    ''' Gets the Race value
    ''' </summary>
    <Display(Name:="Race", Description:="")>
    Public ReadOnly Property RaceID() As Integer?
      Get
        Return GetProperty(RaceIDProperty)
      End Get
    End Property

    Public Shared RaceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Race, "Race")
    ''' <summary>
    ''' Gets the Race value
    ''' </summary>
    <Display(Name:="Race", Description:="")>
    Public ReadOnly Property Race() As String
      Get
        Return GetProperty(RaceProperty)
      End Get
    End Property

    Public Shared GenderIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.GenderID, "Gender", Nothing)
    ''' <summary>
    ''' Gets the Gender value
    ''' </summary>
    <Display(Name:="Gender", Description:="")>
    Public ReadOnly Property GenderID() As Integer?
      Get
        Return GetProperty(GenderIDProperty)
      End Get
    End Property

    Public Shared GenderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Gender, "Gender")
    ''' <summary>
    ''' Gets the Gender value
    ''' </summary>
    <Display(Name:="Gender", Description:="")>
    Public ReadOnly Property Gender() As String
      Get
        Return GetProperty(GenderProperty)
      End Get
    End Property

    Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID, "Manager Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="Manager Human Resource", Description:="")>
    Public ReadOnly Property ManagerHumanResourceID() As Integer?
      Get
        Return GetProperty(ManagerHumanResourceIDProperty)
      End Get
    End Property

    Public Shared Manager1Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Manager1, "Primary Manager")
    ''' <summary>
    ''' Gets the Manager 1 value
    ''' </summary>
    <Display(Name:="Manager 1", Description:="")>
    Public ReadOnly Property Manager1() As String
      Get
        Return GetProperty(Manager1Property)
      End Get
    End Property

    Public Shared ManagerHumanResourceID2Property As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID2, "Manager Human Resource ID 2", Nothing)
    ''' <summary>
    ''' Gets the Manager Human Resource ID 2 value
    ''' </summary>
    <Display(Name:="Manager Human Resource ID 2", Description:="")>
    Public ReadOnly Property ManagerHumanResourceID2() As Integer?
      Get
        Return GetProperty(ManagerHumanResourceID2Property)
      End Get
    End Property

    Public Shared Manager2Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Manager2, "Manager 2")
    ''' <summary>
    ''' Gets the Manager 2 value
    ''' </summary>
    <Display(Name:="Manager 2", Description:="")>
    Public ReadOnly Property Manager2() As String
      Get
        Return GetProperty(Manager2Property)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PantSizeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PantSize, "Pant Size")
    ''' <summary>
    ''' Gets the Pant Size value
    ''' </summary>
    <Display(Name:="Pant Size", Description:="")>
    Public ReadOnly Property PantSize() As String
      Get
        Return GetProperty(PantSizeProperty)
      End Get
    End Property

    Public Shared ShirtSizeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShirtSize, "Shirt Size")
    ''' <summary>
    ''' Gets the Shirt Size value
    ''' </summary>
    <Display(Name:="Shirt Size", Description:="")>
    Public ReadOnly Property ShirtSize() As String
      Get
        Return GetProperty(ShirtSizeProperty)
      End Get
    End Property

    Public Shared JacketSizeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.JacketSize, "Jacket Size")
    ''' <summary>
    ''' Gets the Jacket Size value
    ''' </summary>
    <Display(Name:="Jacket Size", Description:="")>
    Public ReadOnly Property JacketSize() As String
      Get
        Return GetProperty(JacketSizeProperty)
      End Get
    End Property

    Public Shared PayHalfMonthIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PayHalfMonthInd, "Pay Half Month", False)
    ''' <summary>
    ''' Gets the Pay Half Month value
    ''' </summary>
    <Display(Name:="Pay Half Month", Description:="")>
    Public ReadOnly Property PayHalfMonthInd() As Boolean
      Get
        Return GetProperty(PayHalfMonthIndProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="")>
    Public ReadOnly Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared SupplierProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Supplier, "Employed by Supplier")
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Employed by Supplier", Description:="")>
    Public ReadOnly Property Supplier() As String
      Get
        Return GetProperty(SupplierProperty)
      End Get
    End Property

    Public Shared NationalityCountyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NationalityCountyID, "Nationality County", Nothing)
    ''' <summary>
    ''' Gets the Nationality County value
    ''' </summary>
    <Display(Name:="Nationality County", Description:="")>
    Public ReadOnly Property NationalityCountyID() As Integer?
      Get
        Return GetProperty(NationalityCountyIDProperty)
      End Get
    End Property

    Public Shared NationalityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Nationality, "Nationality")
    ''' <summary>
    ''' Gets the Nationality value
    ''' </summary>
    <Display(Name:="Nationality", Description:="")>
    Public ReadOnly Property Nationality() As String
      Get
        Return GetProperty(NationalityProperty)
      End Get
    End Property

    Public Shared AddressR1Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressR1, "Address R 1")
    ''' <summary>
    ''' Gets the Address R 1 value
    ''' </summary>
    <Display(Name:="Address R 1", Description:="")>
    Public ReadOnly Property AddressR1() As String
      Get
        Return GetProperty(AddressR1Property)
      End Get
    End Property

    Public Shared AddressR2Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressR2, "Address R 2")
    ''' <summary>
    ''' Gets the Address R 2 value
    ''' </summary>
    <Display(Name:="Address R 2", Description:="")>
    Public ReadOnly Property AddressR2() As String
      Get
        Return GetProperty(AddressR2Property)
      End Get
    End Property

    Public Shared AddressR3Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressR3, "Address R 3")
    ''' <summary>
    ''' Gets the Address R 3 value
    ''' </summary>
    <Display(Name:="Address R 3", Description:="")>
    Public ReadOnly Property AddressR3() As String
      Get
        Return GetProperty(AddressR3Property)
      End Get
    End Property

    Public Shared AddressR4Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressR4, "Address R 4")
    ''' <summary>
    ''' Gets the Address R 4 value
    ''' </summary>
    <Display(Name:="Address R 4", Description:="")>
    Public ReadOnly Property AddressR4() As String
      Get
        Return GetProperty(AddressR4Property)
      End Get
    End Property

    Public Shared PostalCodeRProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PostalCodeR, "Postal Code R")
    ''' <summary>
    ''' Gets the Postal Code R value
    ''' </summary>
    <Display(Name:="Postal Code R", Description:="")>
    Public ReadOnly Property PostalCodeR() As String
      Get
        Return GetProperty(PostalCodeRProperty)
      End Get
    End Property

    Public Shared AddressP1Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressP1, "Address P 1")
    ''' <summary>
    ''' Gets the Address P 1 value
    ''' </summary>
    <Display(Name:="Address P 1", Description:="")>
    Public ReadOnly Property AddressP1() As String
      Get
        Return GetProperty(AddressP1Property)
      End Get
    End Property

    Public Shared AddressP2Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressP2, "Address P 2")
    ''' <summary>
    ''' Gets the Address P 2 value
    ''' </summary>
    <Display(Name:="Address P 2", Description:="")>
    Public ReadOnly Property AddressP2() As String
      Get
        Return GetProperty(AddressP2Property)
      End Get
    End Property

    Public Shared AddressP3Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressP3, "Address P 3")
    ''' <summary>
    ''' Gets the Address P 3 value
    ''' </summary>
    <Display(Name:="Address P 3", Description:="")>
    Public ReadOnly Property AddressP3() As String
      Get
        Return GetProperty(AddressP3Property)
      End Get
    End Property

    Public Shared AddressP4Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AddressP4, "Address P 4")
    ''' <summary>
    ''' Gets the Address P 4 value
    ''' </summary>
    <Display(Name:="Address P 4", Description:="")>
    Public ReadOnly Property AddressP4() As String
      Get
        Return GetProperty(AddressP4Property)
      End Get
    End Property

    Public Shared PostalCodePProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PostalCodeP, "Postal Code P")
    ''' <summary>
    ''' Gets the Postal Code P value
    ''' </summary>
    <Display(Name:="Postal Code P", Description:="")>
    Public ReadOnly Property PostalCodeP() As String
      Get
        Return GetProperty(PostalCodePProperty)
      End Get
    End Property

    Public Shared TaxNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TaxNumber, "Tax Number")
    ''' <summary>
    ''' Gets the Tax Number value
    ''' </summary>
    <Display(Name:="Tax Number", Description:="")>
    Public ReadOnly Property TaxNumber() As String
      Get
        Return GetProperty(TaxNumberProperty)
      End Get
    End Property

    Public Shared InvoiceSupplierIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InvoiceSupplierInd, "Invoice Supplier", False)
    ''' <summary>
    ''' Gets the Invoice Supplier value
    ''' </summary>
    <Display(Name:="Invoice Supplier", Description:="")>
    Public ReadOnly Property InvoiceSupplierInd() As Boolean
      Get
        Return GetProperty(InvoiceSupplierIndProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
    ''' <summary>
    ''' Gets the Race value
    ''' </summary>
    <Display(Name:="ResourceID", Description:="")>
    Public ReadOnly Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared LicenceIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.LicenceInd, "Has Drivers Licence?", False)
    ''' <summary>
    ''' Gets the Invoice Supplier value
    ''' </summary>
    <Display(Name:="Has Drivers Licence?", Description:="")>
    Public ReadOnly Property LicenceInd() As Boolean
      Get
        Return GetProperty(LicenceIndProperty)
      End Get
    End Property

    Public Shared LicenceExpiryDateProperty As PropertyInfo(Of Date?) = RegisterSProperty(Of Date?)(Function(c) c.LicenceExpDate, Nothing)
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Licence Expires", Description:="")>
    Public ReadOnly Property LicenceExpDate() As Date?
      Get
        Return GetProperty(LicenceExpiryDateProperty)
      End Get
    End Property

    Public ReadOnly Property LicenceExpiryDateString
      Get
        If LicenceExpDate IsNot Nothing Then
          Return "Expires on " & LicenceExpDate.Value.ToString("ddd dd MMM yyyy")
        End If
        Return ""
      End Get
    End Property

    Public ReadOnly Property DisplayName As String
      Get
        Return Firstname & " " & Surname
      End Get
    End Property

    Public ReadOnly Property ContractDaysApplicable As Boolean
      Get
        If CompareSafe(ContractTypeID, CType(OBLib.CommonData.Enums.ContractType.ISP, Integer)) Then
          Return True
        End If
        Return False
      End Get
    End Property

    Public Shared UserProfileLargeImageIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserProfileLargeImageID, "Managed HR Count", Nothing)
    Public ReadOnly Property UserProfileLargeImageID As Integer?
      Get
        Return GetProperty(UserProfileLargeImageIDProperty)
      End Get
    End Property

    Public Shared UserProfileSmallImageIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserProfileSmallImageID, "Managed HR Count", Nothing)
    Public ReadOnly Property UserProfileSmallImageID As Integer?
      Get
        Return GetProperty(UserProfileSmallImageIDProperty)
      End Get
    End Property

    Private mProfileDescription As String = ""
    Public ReadOnly Property ProfileDescription As String
      Get
        Return mProfileDescription
      End Get
    End Property

    Public ReadOnly Property IsProductionServicesHR As Boolean
      Get
        Dim primaryAreas As List(Of ROHRArea) = Me.ROHRAreaList.Where(Function(d) d.PrimaryInd).ToList
        If primaryAreas.Count = 1 Then
          If primaryAreas(0).SystemID = CType(OBLib.CommonData.Enums.System.ProductionServices, Integer) Then
            Return True
          End If
        End If
        Return False
      End Get
    End Property

    Public ReadOnly Property IsICRHR As Boolean
      Get
        Dim primaryAreas As List(Of ROHRArea) = Me.ROHRAreaList.Where(Function(d) d.PrimaryInd).ToList
        If primaryAreas.Count = 1 Then
          If primaryAreas(0).SystemID = CType(OBLib.CommonData.Enums.System.ICR, Integer) Then
            Return True
          End If
        End If
        Return False
      End Get
    End Property

    Public ReadOnly Property IsPlayoutOpsHR As Boolean
      Get
        Dim primaryAreas As List(Of ROHRArea) = Me.ROHRAreaList.Where(Function(d) d.PrimaryInd).ToList
        If primaryAreas.Count = 1 Then
          If primaryAreas(0).SystemID = CType(OBLib.CommonData.Enums.System.PlayoutOperations, Integer) Then
            Return True
          End If
        End If
        Return False
      End Get
    End Property

    Public ReadOnly Property PrimarySystemID As Integer?
      Get
        Dim primaryAreas As List(Of ROHRArea) = Me.ROHRAreaList.Where(Function(d) d.PrimaryInd).ToList
        If primaryAreas.Count = 1 Then
          Return primaryAreas(0).SystemID
        End If
        Return Nothing
      End Get
    End Property

    Public ReadOnly Property PrimaryProductionAreaID As Integer?
      Get
        Dim primaryAreas As List(Of ROHRArea) = Me.ROHRAreaList.Where(Function(d) d.PrimaryInd).ToList
        If primaryAreas.Count = 1 Then
          Return primaryAreas(0).ProductionAreaID
        End If
        Return Nothing
      End Get
    End Property

    Public Shared CanViewOtherPersonnelSchedulesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanViewOtherPersonnelSchedules, "Primary", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="CanViewOtherPersonnelSchedules", Description:="")>
    Public ReadOnly Property CanViewOtherPersonnelSchedules() As Boolean
      Get
        Return GetProperty(CanViewOtherPersonnelSchedulesProperty)
      End Get
    End Property

    Public Shared SwapsAllowedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SwapsAllowed, "SwapsAllowed", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="Primary", Description:="")>
    Public ReadOnly Property SwapsAllowed() As Boolean
      Get
        Return GetProperty(SwapsAllowedProperty)
      End Get
    End Property

    Public Shared IsManagingHumanResourcesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsManagingHumanResources, "IsManagingHumanResources", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="IsManagingHumanResources", Description:="")>
    Public ReadOnly Property IsManagingHumanResources() As Boolean
      Get
        Return GetProperty(IsManagingHumanResourcesProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROHRSkillListProperty As PropertyInfo(Of ROHRSkillList) = RegisterProperty(Of ROHRSkillList)(Function(c) c.ROHRSkillList, "RO Human Resource Skill List")
    Public ReadOnly Property ROHRSkillList() As ROHRSkillList
      Get
        If GetProperty(ROHRSkillListProperty) Is Nothing Then
          LoadProperty(ROHRSkillListProperty, Users.ReadOnly.ROHRSkillList.NewROHRSkillList)
        End If
        Return GetProperty(ROHRSkillListProperty)
      End Get
    End Property

    Public Shared ROHRAreaListProperty As PropertyInfo(Of ROHRAreaList) = RegisterProperty(Of ROHRAreaList)(Function(c) c.ROHRAreaList, "RO Human Resource Off Period List")
    Public ReadOnly Property ROHRAreaList() As ROHRAreaList
      Get
        If GetProperty(ROHRAreaListProperty) Is Nothing Then
          LoadProperty(ROHRAreaListProperty, Users.ReadOnly.ROHRAreaList.NewROHRAreaList)
        End If
        Return GetProperty(ROHRAreaListProperty)
      End Get
    End Property

    Public Shared ROUserManagedHumanResourceListProperty As PropertyInfo(Of ROUserManagedHumanResourceList) = RegisterProperty(Of ROUserManagedHumanResourceList)(Function(c) c.ROUserManagedHumanResourceList, "RO User Profile List")
    Public ReadOnly Property ROUserManagedHumanResourceList() As ROUserManagedHumanResourceList
      Get
        If GetProperty(ROUserManagedHumanResourceListProperty) Is Nothing Then
          LoadProperty(ROUserManagedHumanResourceListProperty, Users.ReadOnly.ROUserManagedHumanResourceList.NewROUserManagedHumanResourceList)
        End If
        Return GetProperty(ROUserManagedHumanResourceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Firstname

    End Function

    Public Sub SetProfileDescription()

      Dim mContractTypeDescription As String = ""
      Select Case ContractTypeID
        Case OBLib.CommonData.Enums.ContractType.Permanent
          mContractTypeDescription = "Permanent"
        Case OBLib.CommonData.Enums.ContractType.Contract
          mContractTypeDescription = "Fixted-Term Contractor"
        Case OBLib.CommonData.Enums.ContractType.Freeleancer
          mContractTypeDescription = "Freelance"
        Case OBLib.CommonData.Enums.ContractType.Trainee
          mContractTypeDescription = "Trainee"
        Case OBLib.CommonData.Enums.ContractType.ISP
          mContractTypeDescription = "Independant Service Provider"
      End Select
      'mProfileDescription = mContractTypeDescription

      Dim PrimarySKill As ROHRSkill = Me.ROHRSkillList.Where(Function(d) d.PrimaryInd).FirstOrDefault
      If PrimarySKill IsNot Nothing Then
        mProfileDescription &= mContractTypeDescription & " " & PrimarySKill.Discipline
      Else
        mProfileDescription = ContractType
      End If

      Dim PrimaryArea As ROHRArea = Me.ROHRAreaList.Where(Function(d) d.PrimaryInd).FirstOrDefault
      If PrimaryArea IsNot Nothing Then
        mProfileDescription &= " working primarily for " + PrimaryArea.SubDept & " in the " & PrimaryArea.Area & " environment"
      End If

    End Sub

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserProfile(dr As SafeDataReader) As ROUserProfile

      Dim r As New ROUserProfile()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(FirstnameProperty, .GetString(1))
        LoadProperty(SecondNameProperty, .GetString(2))
        LoadProperty(PreferredNameProperty, .GetString(3))
        LoadProperty(SurnameProperty, .GetString(4))
        LoadProperty(EmployeeCodeProperty, .GetString(5))
        LoadProperty(IDNoProperty, .GetString(6))
        LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(ContractTypeProperty, .GetString(8))
        LoadProperty(ContractDaysProperty, .GetInt32(9))
        LoadProperty(EmailAddressProperty, .GetString(10))
        LoadProperty(AlternativeEmailAddressProperty, .GetString(11))
        LoadProperty(PhoneEmailAddressProperty, .GetString(12))
        LoadProperty(CellPhoneNumberProperty, .GetString(13))
        LoadProperty(AlternativeContactNumberProperty, .GetString(14))
        LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
        LoadProperty(CityProperty, .GetString(16))
        LoadProperty(RaceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(RaceProperty, .GetString(18))
        LoadProperty(GenderIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
        LoadProperty(GenderProperty, .GetString(20))
        LoadProperty(ManagerHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
        LoadProperty(Manager1Property, .GetString(22))
        LoadProperty(ManagerHumanResourceID2Property, Singular.Misc.ZeroNothing(.GetInt32(23)))
        LoadProperty(Manager2Property, .GetString(24))
        LoadProperty(CreatedByProperty, .GetInt32(25))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(26))
        LoadProperty(ModifiedByProperty, .GetInt32(27))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(28))
        LoadProperty(PantSizeProperty, .GetString(29))
        LoadProperty(ShirtSizeProperty, .GetString(30))
        LoadProperty(JacketSizeProperty, .GetString(31))
        LoadProperty(PayHalfMonthIndProperty, .GetBoolean(32))
        LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(33)))
        LoadProperty(SupplierProperty, .GetString(34))
        LoadProperty(NationalityCountyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(35)))
        LoadProperty(NationalityProperty, .GetString(36))
        LoadProperty(AddressR1Property, .GetString(37))
        LoadProperty(AddressR2Property, .GetString(38))
        LoadProperty(AddressR3Property, .GetString(39))
        LoadProperty(AddressR4Property, .GetString(40))
        LoadProperty(PostalCodeRProperty, .GetString(41))
        LoadProperty(AddressP1Property, .GetString(42))
        LoadProperty(AddressP2Property, .GetString(43))
        LoadProperty(AddressP3Property, .GetString(44))
        LoadProperty(AddressP4Property, .GetString(45))
        LoadProperty(PostalCodePProperty, .GetString(46))
        LoadProperty(TaxNumberProperty, .GetString(47))
        LoadProperty(InvoiceSupplierIndProperty, .GetBoolean(48))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(49)))
        LoadProperty(LicenceIndProperty, .GetBoolean(50))
        LoadProperty(LicenceExpiryDateProperty, .GetValue(51))
        LoadProperty(UserProfileLargeImageIDProperty, ZeroNothing(.GetInt32(52)))
        LoadProperty(UserProfileSmallImageIDProperty, ZeroNothing(.GetInt32(53)))
        LoadProperty(CanViewOtherPersonnelSchedulesProperty, .GetBoolean(54))
        LoadProperty(SwapsAllowedProperty, .GetBoolean(55))
        LoadProperty(IsManagingHumanResourcesProperty, .GetBoolean(56))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace