﻿' Generated 28 Jan 2016 20:05 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Users

  <Serializable()> _
  Public Class UserProfileShift
    Inherits OBBusinessBase(Of UserProfileShift)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceShiftID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required"),
    Browsable(True)>
    Public Property HumanResourceShiftID() As Integer
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftTypeID, "Shift Type")
    ''' <summary>
    ''' Gets and sets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
    Public Property ShiftTypeID() As Integer
      Get
        Return GetProperty(ShiftTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ShiftTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftType, "Shift Type")
    ''' <summary>
    ''' Gets and sets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
    Public Property ShiftType() As String
      Get
        Return GetProperty(ShiftTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ShiftTypeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleDate, "Schedule Date")
    ''' <summary>
    ''' Gets and sets the Schedule Date value
    ''' </summary>
    <Display(Name:="Schedule Date", Description:="")>
    Public Property ScheduleDate As DateTime?
      Get
        Return GetProperty(ScheduleDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleDateProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleDateStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ScheduleDateString, "Date")
    ''' <summary>
    ''' Gets and sets the Schedule Date value
    ''' </summary>
    <Display(Name:="Schedule Date", Description:="")>
    Public Property ScheduleDateString As String
      Get
        If ScheduleDate IsNot Nothing Then
          Return ScheduleDate.Value.ToString("ddd dd MMM")
        Else
          Return ""
        End If
        'Return GetProperty(ScheduleDateStringProperty)
      End Get
      Set(ByVal Value As String)
        'SetProperty(ScheduleDateStringProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StartDateTimeString, "Start Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:="")>
    Public Property StartDateTimeString As String
      Get
        If StartDateTime IsNot Nothing Then
          Return StartDateTime.Value.ToString("HH:mm")
        Else
          Return ""
        End If
        'Return GetProperty(StartDateTimeStringProperty)
      End Get
      Set(ByVal Value As String)
        'SetProperty(StartDateTimeStringProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EndDateTimeString, "End Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public Property EndDateTimeString As String
      Get
        If EndDateTime IsNot Nothing Then
          Return EndDateTime.Value.ToString("HH:mm")
        Else
          Return ""
        End If
        'Return GetProperty(EndDateTimeStringProperty)
      End Get
      Set(ByVal Value As String)
        'SetProperty(EndDateTimeStringProperty, Value)
      End Set
    End Property

    Public Shared ExtraShiftIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ExtraShiftInd, "Extra Shift", False)
    ''' <summary>
    ''' Gets and sets the Extra Shift value
    ''' </summary>
    <Display(Name:="Extra Shift", Description:="")>
    Public Property ExtraShiftInd() As Boolean
      Get
        Return GetProperty(ExtraShiftIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ExtraShiftIndProperty, Value)
      End Set
    End Property

    Public Shared StaffAcknowledgeIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.StaffAcknowledgeInd, False) _
                                                                            .AddSetExpression("UserProfileShiftBO.AcknowledgeSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Staff Acknowledge value
    ''' </summary>
    <Display(Name:="Staff Acknowledge", Description:="")>
    Public Property StaffAcknowledgeInd() As Boolean
      Get
        Return GetProperty(StaffAcknowledgeIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(StaffAcknowledgeIndProperty, Value)
      End Set
    End Property

    Public Shared StaffAcknowledgeByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.StaffAcknowledgeBy, "Staff Acknowledge By", Nothing)
    ''' <summary>
    ''' Gets and sets the Staff Acknowledge By value
    ''' </summary>
    <Display(Name:="Staff Acknowledge By", Description:="")>
    Public Property StaffAcknowledgeBy() As Integer?
      Get
        Return GetProperty(StaffAcknowledgeByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(StaffAcknowledgeByProperty, Value)
      End Set
    End Property

    Public Shared StaffAcknowledgeDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StaffAcknowledgeDateTime, "Staff Acknowledge Date Time")
    ''' <summary>
    ''' Gets and sets the Staff Acknowledge Date Time value
    ''' </summary>
    <Display(Name:="Staff Acknowledge Date Time", Description:="")>
    Public Property StaffAcknowledgeDateTime As DateTime?
      Get
        Return GetProperty(StaffAcknowledgeDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StaffAcknowledgeDateTimeProperty, Value)
      End Set
    End Property

    Public Shared SupervisorAuthIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SupervisorAuthInd, "Supervisor Auth", False)
    ''' <summary>
    ''' Gets and sets the Supervisor Auth value
    ''' </summary>
    <Display(Name:="Supervisor Auth", Description:="")>
    Public Property SupervisorAuthInd() As Boolean
      Get
        Return GetProperty(SupervisorAuthIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SupervisorAuthIndProperty, Value)
      End Set
    End Property

    Public Shared SupervisorAuthByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupervisorAuthBy, "Supervisor Auth By")
    ''' <summary>
    ''' Gets and sets the Supervisor Auth By value
    ''' </summary>
    <Display(Name:="Supervisor Auth By", Description:="")>
    Public Property SupervisorAuthBy() As Integer
      Get
        Return GetProperty(SupervisorAuthByProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SupervisorAuthByProperty, Value)
      End Set
    End Property

    Public Shared SuperAuthDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SuperAuthDateTime, "Super Auth Date Time")
    ''' <summary>
    ''' Gets and sets the Super Auth Date Time value
    ''' </summary>
    <Display(Name:="Super Auth Date Time", Description:="")>
    Public Property SuperAuthDateTime As DateTime?
      Get
        Return GetProperty(SuperAuthDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SuperAuthDateTimeProperty, Value)
      End Set
    End Property

    Public Shared SupervisorRejectedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupervisorRejectedReason, "Supervisor Rejected Reason")
    ''' <summary>
    ''' Gets and sets the Supervisor Rejected Reason value
    ''' </summary>
    <Display(Name:="Supervisor Rejected Reason", Description:="")>
    Public Property SupervisorRejectedReason() As String
      Get
        Return GetProperty(SupervisorRejectedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SupervisorRejectedReasonProperty, Value)
      End Set
    End Property

    Public Shared ManagerAuthIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ManagerAuthInd, "Manager Auth", False)
    ''' <summary>
    ''' Gets and sets the Manager Auth value
    ''' </summary>
    <Display(Name:="Manager Auth", Description:="")>
    Public Property ManagerAuthInd() As Boolean
      Get
        Return GetProperty(ManagerAuthIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ManagerAuthIndProperty, Value)
      End Set
    End Property

    Public Shared ManagerAuthByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ManagerAuthBy, "Manager Auth By")
    ''' <summary>
    ''' Gets and sets the Manager Auth By value
    ''' </summary>
    <Display(Name:="Manager Auth By", Description:="")>
    Public Property ManagerAuthBy() As Integer
      Get
        Return GetProperty(ManagerAuthByProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ManagerAuthByProperty, Value)
      End Set
    End Property

    Public Shared ManagerAuthDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ManagerAuthDateTime, "Manager Auth Date Time")
    ''' <summary>
    ''' Gets and sets the Manager Auth Date Time value
    ''' </summary>
    <Display(Name:="Manager Auth Date Time", Description:="")>
    Public Property ManagerAuthDateTime As DateTime?
      Get
        Return GetProperty(ManagerAuthDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ManagerAuthDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ManagerRejectedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerRejectedReason, "Manager Rejected Reason")
    ''' <summary>
    ''' Gets and sets the Manager Rejected Reason value
    ''' </summary>
    <Display(Name:="Manager Rejected Reason", Description:="")>
    Public Property ManagerRejectedReason() As String
      Get
        Return GetProperty(ManagerRejectedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ManagerRejectedReasonProperty, Value)
      End Set
    End Property

    Public Shared ShiftDurationProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShiftDuration, "Shift Duration")
    ''' <summary>
    ''' Gets and sets the Shift Duration value
    ''' </summary>
    <Display(Name:="Shift Duration", Description:="")>
    Public Property ShiftDuration() As Decimal
      Get
        Return GetProperty(ShiftDurationProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(ShiftDurationProperty, Value)
      End Set
    End Property

    <Display(Name:="Duration")>
    Public Property ShiftDurationString As String
      Get
        If ShiftDuration <> 0 Then
          Return ShiftDuration.ToString("#,#.00#;(#,#.00#)") & " hours"
        End If
        Return ""
      End Get
      Set(value As String)

      End Set
    End Property

    Public Shared RunningTotalProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RunningTotal, "Running Total")
    ''' <summary>
    ''' Gets and sets the Running Total value
    ''' </summary>
    <Display(Name:="Total", Description:="")>
    Public Property RunningTotal() As Decimal
      Get
        Return GetProperty(RunningTotalProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RunningTotalProperty, Value)
      End Set
    End Property

    <Display(Name:="Total")>
    Public Property RunningTotalString As String
      Get
        If RunningTotal <> 0 Then
          Return RunningTotal.ToString("#,#.00#;(#,#.00#)") & " hrs"
        End If
        Return ""
      End Get
      Set(value As String)

      End Set
    End Property

    Public Shared PublicHolidayIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PublicHolidayInd, "Public Holiday", False)
    ''' <summary>
    ''' Gets and sets the Public Holiday value
    ''' </summary>
    <Display(Name:="Public Holiday", Description:="")>
    Public Property PublicHolidayInd() As Boolean
      Get
        Return GetProperty(PublicHolidayIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PublicHolidayIndProperty, Value)
      End Set
    End Property

    Public Shared AllowanceCategoryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AllowanceCategory, "Allowance")
    ''' <summary>
    ''' Gets and sets the Manager Rejected Reason value
    ''' </summary>
    <Display(Name:="Allowance", Description:="")>
    Public Property AllowanceCategory() As String
      Get
        Return GetProperty(AllowanceCategoryProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AllowanceCategoryProperty, Value)
      End Set
    End Property

    'Public Shared AllowanceAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AllowanceAmount, "Allowance Amnt")
    ' ''' <summary>
    ' ''' Gets and sets the Allowance Amount value
    ' ''' </summary>
    '<Display(Name:="Allowance Amnt", Description:=""),
    'Required(ErrorMessage:="Allowance required")>
    'Public Property AllowanceAmount() As Decimal
    '  Get
    '    Return GetProperty(AllowanceAmountProperty)
    '  End Get
    '  Set(ByVal Value As Decimal)
    '    SetProperty(AllowanceAmountProperty, Value)
    '  End Set
    'End Property

    Public Shared AuthorisedNoOfMealsProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.AuthorisedNoOfMeals, 0)
    ''' <summary>
    ''' Gets and sets the Authorised Meal Vouchers value
    ''' </summary>
    <Display(Name:="Num. Meals", Description:="")>
    Public ReadOnly Property AuthorisedNoOfMeals() As Integer
      Get
        Return GetProperty(AuthorisedNoOfMealsProperty)
      End Get
    End Property

    Public Shared AuthorisedNoOfMealsStringProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.AuthorisedNoOfMealsString, "")
    ''' <summary>
    ''' Gets and sets the Authorised Meal Vouchers value
    ''' </summary>
    <Display(Name:="Num. Meals")>
    Public ReadOnly Property AuthorisedNoOfMealsString() As String
      Get
        If AuthorisedNoOfMeals > 0 Then
          Return AuthorisedNoOfMeals.ToString
        End If
        Return ""
      End Get
    End Property

    'Public Shared AuthorisedMealAmountProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.AuthorisedMealAmount, 0)
    ' ''' <summary>
    ' ''' Gets and sets the Authorised Reimbursement Amount value
    ' ''' </summary>
    '<Display(Name:="Meal Amnt", Description:="")>
    'Public ReadOnly Property AuthorisedMealAmount() As Decimal
    '  Get
    '    Return GetProperty(AuthorisedMealAmountProperty)
    '  End Get
    'End Property

    <Display(Name:="Meal Amnt", Description:="")>
    Public Property MealAmount As String
      Get
        If AuthorisedNoOfMeals <> 0 Then
          Return AuthorisedNoOfMeals.ToString() '("#,#.00#;(#,#.00#)")
        End If
        Return ""
      End Get
      Set(value As String)

      End Set
    End Property

    '<Display(Name:="Allowance Amnt", Description:="")>
    'Public Property AllowanceAmt As String
    '  Get
    '    If AllowanceAmount <> 0 Then
    '      Return AllowanceAmount.ToString("#,#.00#;(#,#.00#)")
    '    End If
    '    Return ""
    '  End Get
    '  Set(value As String)

    '  End Set
    'End Property

    Public Shared StaffDisputeReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StaffDisputeReason, "Query Reason", "")
    ''' <summary>
    ''' Gets and sets the Staff Dispute Reason value
    ''' </summary>
    <Display(Name:="Query Reason", Description:=""),
    StringLength(1024, ErrorMessage:="Query Reason cannot be more than 1024 characters"),
    TextField(True, True, False, 10)>
    Public Property StaffDisputeReason() As String
      Get
        Return GetProperty(StaffDisputeReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StaffDisputeReasonProperty, Value)
      End Set
    End Property

    Public Shared OriginalStaffAcknowledgeIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OriginalStaffAcknowledgeInd, "Staff Acknowledge", False)
    ''' <summary>
    ''' Gets and sets the Staff Acknowledge value
    ''' </summary>
    <Display(Name:="Staff Acknowledge")>
    Public Property OriginalStaffAcknowledgeInd() As Boolean
      Get
        Return GetProperty(OriginalStaffAcknowledgeIndProperty)
      End Get
      Set(value As Boolean)
        'SetProperty(OriginalStaffAcknowledgeIndProperty, value)
      End Set
    End Property

    Public Sub CheckAcknowledgementValid()
      'If acknowledgement status is different
      If Not (CanAcknowledgedShift() And StaffAckStatusChanged()) Then
        SetProperty(StaffAcknowledgeIndProperty, OriginalStaffAcknowledgeInd)
      End If
    End Sub

    Public Function StaffAckStatusChanged() As Boolean
      Return (Not CompareSafe(OriginalStaffAcknowledgeInd, StaffAcknowledgeInd))
    End Function

    Public Function CanAcknowledgedShift() As Boolean
      If ScheduleDate.Value.Date < Now.Date Then
        Return True
      ElseIf ScheduleDate.Value.Date = Now.Date Then
        'If the current time is later than the shift end time
        If Now.Subtract(EndDateTime).TotalMinutes > 0 Then
          Return True
        End If
      End If
      Return False
    End Function

    Public Shared IsDisputingProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsDisputing, "Is Disputing", False)
    ''' <summary>
    ''' Gets and sets the Staff Dispute Reason value
    ''' </summary>
    <Display(Name:="Is Disputing"), AlwaysClean>
    Public Property IsDisputing() As Boolean
      Get
        Return GetProperty(IsDisputingProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsDisputingProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceShiftIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ShiftType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "User Profile Shift")
        Else
          Return String.Format("Blank {0}", "User Profile Shift")
        End If
      Else
        Return Me.ShiftType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(StaffDisputeReasonProperty)
        .ServerRuleFunction = AddressOf StaffDisputeReasonValid
        .JavascriptRuleFunctionName = "UserProfileShiftBO.StaffDisputeReasonValid"
        .AddTriggerProperty(IsDisputingProperty)
      End With

    End Sub

    Public Shared Function StaffDisputeReasonValid(ByVal Instance As UserProfileShift) As String

      If Instance.IsDisputing AndAlso Instance.StaffDisputeReason.Trim.Length = 0 Then
        Return "Dispute Reason is required"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewUserProfileShift() method.

    End Sub

    Public Shared Function NewUserProfileShift() As UserProfileShift

      Return DataPortal.CreateChild(Of UserProfileShift)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetUserProfileShift(dr As SafeDataReader) As UserProfileShift

      Dim u As New UserProfileShift()
      u.Fetch(dr)
      Return u

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceShiftIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ShiftTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ShiftTypeProperty, .GetString(3))
          LoadProperty(ScheduleDateProperty, .GetValue(4))
          LoadProperty(StartDateTimeProperty, .GetValue(5))
          LoadProperty(EndDateTimeProperty, .GetValue(6))
          LoadProperty(ExtraShiftIndProperty, .GetBoolean(7))
          LoadProperty(StaffAcknowledgeIndProperty, .GetBoolean(8))
          LoadProperty(StaffAcknowledgeByProperty, ZeroNothing(.GetInt32(9)))
          LoadProperty(StaffAcknowledgeDateTimeProperty, .GetValue(10))
          LoadProperty(SupervisorAuthIndProperty, .GetBoolean(11))
          LoadProperty(SupervisorAuthByProperty, ZeroNothing(.GetInt32(12)))
          LoadProperty(SuperAuthDateTimeProperty, .GetValue(13))
          LoadProperty(SupervisorRejectedReasonProperty, .GetString(14))
          LoadProperty(ManagerAuthIndProperty, .GetBoolean(15))
          LoadProperty(ManagerAuthByProperty, .GetInt32(16))
          LoadProperty(ManagerAuthDateTimeProperty, .GetValue(17))
          LoadProperty(ManagerRejectedReasonProperty, .GetString(18))
          LoadProperty(PublicHolidayIndProperty, .GetBoolean(19))
          LoadProperty(ShiftDurationProperty, .GetDecimal(20))
          LoadProperty(RunningTotalProperty, .GetDecimal(21))
          'LoadProperty(AllowanceAmountProperty, .GetDecimal(22))
          LoadProperty(AuthorisedNoOfMealsProperty, .GetInt32(23))
          'LoadProperty(AuthorisedMealAmountProperty, .GetDecimal(24))
          LoadProperty(AllowanceCategoryProperty, .GetString(25))
          LoadProperty(OriginalStaffAcknowledgeIndProperty, .GetBoolean(8))
          LoadProperty(StaffDisputeReasonProperty, .GetString(26))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      '' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcsWeb.insUserProfileShift"
      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updUserProfileShift"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceShiftID As SqlParameter = .Parameters.Add("@HumanResourceShiftID", SqlDbType.Int)
          paramHumanResourceShiftID.Value = GetProperty(HumanResourceShiftIDProperty)
          If Me.IsNew Then
            paramHumanResourceShiftID.Direction = ParameterDirection.Output
          End If
          '.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          '.Parameters.AddWithValue("@ShiftTypeID", GetProperty(ShiftTypeIDProperty))
          '.Parameters.AddWithValue("@ShiftType", GetProperty(ShiftTypeProperty))
          '.Parameters.AddWithValue("@ScheduleDate", (New SmartDate(GetProperty(ScheduleDateProperty))).DBValue)
          '.Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          '.Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          '.Parameters.AddWithValue("@ExtraShiftInd", GetProperty(ExtraShiftIndProperty))
          .Parameters.AddWithValue("@StaffAcknowledgeInd", GetProperty(StaffAcknowledgeIndProperty))
          .Parameters.AddWithValue("@StaffAcknowledgeBy", NothingDBNull(GetProperty(StaffAcknowledgeByProperty)))
          .Parameters.AddWithValue("@StaffDisputeReason", GetProperty(StaffDisputeReasonProperty))
          .Parameters.AddWithValue("@StaffAcknowledgeDateTime", NothingDBNull(GetProperty(StaffAcknowledgeDateTimeProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          '.Parameters.AddWithValue("@ManagerRejectedReason", GetProperty(ManagerRejectedReasonProperty))
          '.Parameters.AddWithValue("@SupervisorAuthInd", GetProperty(SupervisorAuthIndProperty))
          '.Parameters.AddWithValue("@SupervisorAuthBy", GetProperty(SupervisorAuthByProperty))
          '.Parameters.AddWithValue("@SuperAuthDateTime", (New SmartDate(GetProperty(SuperAuthDateTimeProperty))).DBValue)
          '.Parameters.AddWithValue("@SupervisorRejectedReason", GetProperty(SupervisorRejectedReasonProperty))
          '.Parameters.AddWithValue("@ManagerAuthInd", GetProperty(ManagerAuthIndProperty))
          '.Parameters.AddWithValue("@ManagerAuthBy", GetProperty(ManagerAuthByProperty))
          '.Parameters.AddWithValue("@ManagerAuthDateTime", (New SmartDate(GetProperty(ManagerAuthDateTimeProperty))).DBValue)
          '.Parameters.AddWithValue("@ManagerRejectedReason", GetProperty(ManagerRejectedReasonProperty))
          '.Parameters.AddWithValue("@ShiftDuration", GetProperty(ShiftDurationProperty))
          '.Parameters.AddWithValue("@PublicHolidayInd", GetProperty(PublicHolidayIndProperty))
          '.Parameters.AddWithValue("@AllowanceAmount", GetProperty(AllowanceAmountProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceShiftIDProperty, paramHumanResourceShiftID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      '' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delUserProfileShift"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@HumanResourceShiftID", GetProperty(HumanResourceShiftIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      'If Me.IsNew Then Exit Sub

      'cm.ExecuteNonQuery()
      'MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace