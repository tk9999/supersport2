﻿' Generated 28 Jan 2016 20:05 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Users

  <Serializable()> _
  Public Class UserProfileShiftList
    Inherits OBBusinessListBase(Of UserProfileShiftList, UserProfileShift)

#Region " Business Methods "

    Public Function GetItem(HumanResourceShiftID As Integer) As UserProfileShift

      For Each child As UserProfileShift In Me
        If child.HumanResourceShiftID = HumanResourceShiftID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Display(Name:="Human Resource"), Required>
      Public Property HumanResourceID As Integer?

      <Display(Name:="Start Date"), Required>
      Public Property StartDate As DateTime?

      <Display(Name:="End Date"), Required>
      Public Property EndDate As DateTime?

      Public Property IsProcessing As Boolean = False

      Public Sub New(HumanResourceID As Integer?, StartDate As DateTime?, EndDate As DateTime?)
        Me.HumanResourceID = HumanResourceID
        Me.StartDate = StartDate
        Me.EndDate = EndDate
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewUserProfileShiftList() As UserProfileShiftList

      Return New UserProfileShiftList()

    End Function

    Public Shared Sub BeginGetUserProfileShiftList(CallBack As EventHandler(Of DataPortalResult(Of UserProfileShiftList)))

      Dim dp As New DataPortal(Of UserProfileShiftList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetUserProfileShiftList() As UserProfileShiftList

      Return DataPortal.Fetch(Of UserProfileShiftList)(New Criteria())

    End Function

    Public Shared Function GetUserProfileShiftList(HumanResourceID As Integer?, StartDate As DateTime?, EndDate As DateTime?) As UserProfileShiftList

      Return DataPortal.Fetch(Of UserProfileShiftList)(New Criteria(HumanResourceID, StartDate, EndDate))

    End Function

    Public Shared Function GetUserProfileShiftList(crit As Criteria) As UserProfileShiftList

      Return DataPortal.Fetch(Of UserProfileShiftList)(crit)

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(UserProfileShift.GetUserProfileShift(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getUserProfileShiftList"
            cm.Parameters.AddWithValue("@HumanResourceID", crit.HumanResourceID)
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        'For Each Child As UserProfileShift In DeletedList
        '  Child.DeleteSelf()
        'Next

        ' Then clear the list of deleted objects because they are truly gone now.
        'DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As UserProfileShift In Me
          If Child.IsNew Then
            'Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace