﻿' Generated 09 Sep 2015 04:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Users.ReadOnly

  <Serializable()> _
  Public Class ROUserProfileList
    Inherits SingularReadOnlyListBase(Of ROUserProfileList, ROUserProfile)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROUserProfile

      For Each child As ROUserProfile In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "R Os"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property UserID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New(UserID As Integer?, HumanResourceID As Integer?)
        Me.UserID = UserID
        Me.HumanResourceID = HumanResourceID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROUserProfileList() As ROUserProfileList

      Return New ROUserProfileList()

    End Function

    Public Shared Sub BeginGetROUserProfileList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROUserProfileList)))

      Dim dp As New DataPortal(Of ROUserProfileList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROUserProfileList(CallBack As EventHandler(Of DataPortalResult(Of ROUserProfileList)))

      Dim dp As New DataPortal(Of ROUserProfileList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROUserProfileList(UserID As Integer?, HumanResourceID As Integer?) As ROUserProfileList

      Return DataPortal.Fetch(Of ROUserProfileList)(New Criteria(UserID, HumanResourceID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUserProfile.GetROUserProfile(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROUserProfile = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROHRSkillList.RaiseListChangedEvents = False
          parent.ROHRSkillList.Add(ROHRSkill.GetROHRSkill(sdr))
          parent.ROHRSkillList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROHRAreaList.RaiseListChangedEvents = False
          parent.ROHRAreaList.Add(ROHRArea.GetROHRArea(sdr))
          parent.ROHRAreaList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.ROUserManagedHumanResourceList.RaiseListChangedEvents = False
          parent.ROUserManagedHumanResourceList.Add(ROUserManagedHumanResource.GetROUserManagedHumanResource(sdr))
          parent.ROUserManagedHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      For Each up As ROUserProfile In Me
        up.SetProfileDescription()
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROUserProfileList"
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(crit.UserID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace