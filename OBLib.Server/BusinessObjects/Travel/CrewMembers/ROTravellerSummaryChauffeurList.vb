﻿' Generated 16 Apr 2017 23:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.ReadOnly

  <Serializable()> _
  Public Class ROTravellerSummaryChauffeurList
    Inherits OBReadOnlyListBase(Of ROTravellerSummaryChauffeurList, ROTravellerSummaryChauffeur)

#Region " Parent "

    <NotUndoable()> Private mParent As ROTravellerSummary
#End Region

#Region " Business Methods "

    Public Function GetItem(ChauffeurDriverHumanResourceID As Integer) As ROTravellerSummaryChauffeur

      For Each child As ROTravellerSummaryChauffeur In Me
        If child.ChauffeurDriverHumanResourceID = ChauffeurDriverHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewROTravellerSummaryChauffeurList() As ROTravellerSummaryChauffeurList

      Return New ROTravellerSummaryChauffeurList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace