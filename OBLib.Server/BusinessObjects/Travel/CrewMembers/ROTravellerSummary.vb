﻿' Generated 16 Apr 2017 23:02 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.ReadOnly

  <Serializable()> _
  Public Class ROTravellerSummary
    Inherits OBReadOnlyBase(Of ROTravellerSummary)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROTravellerSummaryBO.ROTravellerSummaryBOToString(self)")

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRName, "HR Name")
    ''' <summary>
    ''' Gets the HR Name value
    ''' </summary>
    <Display(Name:="HR Name", Description:="")>
  Public ReadOnly Property HRName() As String
      Get
        Return GetProperty(HRNameProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROTravellerSummaryFlightListProperty As PropertyInfo(Of ROTravellerSummaryFlightList) = RegisterProperty(Of ROTravellerSummaryFlightList)(Function(c) c.ROTravellerSummaryFlightList, "RO Traveller Summary Flight List")

    Public ReadOnly Property ROTravellerSummaryFlightList() As ROTravellerSummaryFlightList
      Get
        If GetProperty(ROTravellerSummaryFlightListProperty) Is Nothing Then
          LoadProperty(ROTravellerSummaryFlightListProperty, Travel.ReadOnly.ROTravellerSummaryFlightList.NewROTravellerSummaryFlightList())
        End If
        Return GetProperty(ROTravellerSummaryFlightListProperty)
      End Get
    End Property

    Public Shared ROTravellerSummaryRentalCarListProperty As PropertyInfo(Of ROTravellerSummaryRentalCarList) = RegisterProperty(Of ROTravellerSummaryRentalCarList)(Function(c) c.ROTravellerSummaryRentalCarList, "RO Traveller Summary Rental Car List")

    Public ReadOnly Property ROTravellerSummaryRentalCarList() As ROTravellerSummaryRentalCarList
      Get
        If GetProperty(ROTravellerSummaryRentalCarListProperty) Is Nothing Then
          LoadProperty(ROTravellerSummaryRentalCarListProperty, Travel.ReadOnly.ROTravellerSummaryRentalCarList.NewROTravellerSummaryRentalCarList())
        End If
        Return GetProperty(ROTravellerSummaryRentalCarListProperty)
      End Get
    End Property

    Public Shared ROTravellerSummaryAccommodationListProperty As PropertyInfo(Of ROTravellerSummaryAccommodationList) = RegisterProperty(Of ROTravellerSummaryAccommodationList)(Function(c) c.ROTravellerSummaryAccommodationList, "RO Traveller Summary Accommodation List")

    Public ReadOnly Property ROTravellerSummaryAccommodationList() As ROTravellerSummaryAccommodationList
      Get
        If GetProperty(ROTravellerSummaryAccommodationListProperty) Is Nothing Then
          LoadProperty(ROTravellerSummaryAccommodationListProperty, Travel.ReadOnly.ROTravellerSummaryAccommodationList.NewROTravellerSummaryAccommodationList())
        End If
        Return GetProperty(ROTravellerSummaryAccommodationListProperty)
      End Get
    End Property

    Public Shared ROTravellerSummaryChauffeurListProperty As PropertyInfo(Of ROTravellerSummaryChauffeurList) = RegisterProperty(Of ROTravellerSummaryChauffeurList)(Function(c) c.ROTravellerSummaryChauffeurList, "RO Traveller Summary Chauffeur List")

    Public ReadOnly Property ROTravellerSummaryChauffeurList() As ROTravellerSummaryChauffeurList
      Get
        If GetProperty(ROTravellerSummaryChauffeurListProperty) Is Nothing Then
          LoadProperty(ROTravellerSummaryChauffeurListProperty, Travel.ReadOnly.ROTravellerSummaryChauffeurList.NewROTravellerSummaryChauffeurList())
        End If
        Return GetProperty(ROTravellerSummaryChauffeurListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HRName

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerSummary(dr As SafeDataReader) As ROTravellerSummary

      Dim r As New ROTravellerSummary()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HRNameProperty, .GetString(1))
      End With

    End Sub

#End Region

  End Class

End Namespace