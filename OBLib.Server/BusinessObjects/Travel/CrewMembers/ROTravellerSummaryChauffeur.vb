﻿' Generated 16 Apr 2017 23:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.ReadOnly

  <Serializable()> _
  Public Class ROTravellerSummaryChauffeur
    Inherits OBReadOnlyBase(Of ROTravellerSummaryChauffeur)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROTravellerSummaryChauffeurBO.ROTravellerSummaryChauffeurBOToString(self)")

    Public Shared ChauffeurDriverHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChauffeurDriverHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ChauffeurDriverHumanResourceID() As Integer
      Get
        Return GetProperty(ChauffeurDriverHumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared ChauffeurDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChauffeurDescription, "Chauffeur Description")
    ''' <summary>
    ''' Gets the Chauffeur Description value
    ''' </summary>
    <Display(Name:="Chauffeur Description", Description:="")>
  Public ReadOnly Property ChauffeurDescription() As String
      Get
        Return GetProperty(ChauffeurDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ChauffeurDriverHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ChauffeurDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerSummaryChauffeur(dr As SafeDataReader) As ROTravellerSummaryChauffeur

      Dim r As New ROTravellerSummaryChauffeur()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ChauffeurDriverHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ChauffeurDescriptionProperty, .GetString(2))
      End With

    End Sub

#End Region

  End Class

End Namespace