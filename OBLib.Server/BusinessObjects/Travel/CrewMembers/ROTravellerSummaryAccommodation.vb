﻿' Generated 16 Apr 2017 23:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.ReadOnly

  <Serializable()> _
  Public Class ROTravellerSummaryAccommodation
    Inherits OBReadOnlyBase(Of ROTravellerSummaryAccommodation)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROTravellerSummaryAccommodationBO.ROTravellerSummaryAccommodationBOToString(self)")

    Public Shared AccommodationHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AccommodationHumanResourceID() As Integer
      Get
        Return GetProperty(AccommodationHumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared AccommodationDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccommodationDescription, "Accommodation Description")
    ''' <summary>
    ''' Gets the Accommodation Description value
    ''' </summary>
    <Display(Name:="Accommodation Description", Description:="")>
  Public ReadOnly Property AccommodationDescription() As String
      Get
        Return GetProperty(AccommodationDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AccommodationDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerSummaryAccommodation(dr As SafeDataReader) As ROTravellerSummaryAccommodation

      Dim r As New ROTravellerSummaryAccommodation()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AccommodationHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(AccommodationDescriptionProperty, .GetString(2))
      End With

    End Sub

#End Region

  End Class

End Namespace