﻿' Generated 16 Apr 2017 23:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.ReadOnly

  <Serializable()> _
  Public Class ROTravellerSummaryAccommodationList
    Inherits OBReadOnlyListBase(Of ROTravellerSummaryAccommodationList, ROTravellerSummaryAccommodation)

#Region " Parent "

    <NotUndoable()> Private mParent As ROTravellerSummary
#End Region

#Region " Business Methods "

    Public Function GetItem(AccommodationHumanResourceID As Integer) As ROTravellerSummaryAccommodation

      For Each child As ROTravellerSummaryAccommodation In Me
        If child.AccommodationHumanResourceID = AccommodationHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewROTravellerSummaryAccommodationList() As ROTravellerSummaryAccommodationList

      Return New ROTravellerSummaryAccommodationList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace