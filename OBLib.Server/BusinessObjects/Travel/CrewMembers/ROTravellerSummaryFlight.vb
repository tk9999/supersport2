﻿' Generated 16 Apr 2017 23:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.ReadOnly

  <Serializable()> _
  Public Class ROTravellerSummaryFlight
    Inherits OBReadOnlyBase(Of ROTravellerSummaryFlight)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROTravellerSummaryFlightBO.ROTravellerSummaryFlightBOToString(self)")

    Public Shared FlightHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FlightHumanResourceID() As Integer
      Get
        Return GetProperty(FlightHumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared FlightDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlightDescription, "Flight Description")
    ''' <summary>
    ''' Gets the Flight Description value
    ''' </summary>
    <Display(Name:="Flight Description", Description:="")>
  Public ReadOnly Property FlightDescription() As String
      Get
        Return GetProperty(FlightDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FlightHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FlightDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerSummaryFlight(dr As SafeDataReader) As ROTravellerSummaryFlight

      Dim r As New ROTravellerSummaryFlight()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FlightHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(FlightDescriptionProperty, .GetString(2))
      End With

    End Sub

#End Region

  End Class

End Namespace