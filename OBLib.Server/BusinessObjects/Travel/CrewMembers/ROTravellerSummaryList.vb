﻿' Generated 16 Apr 2017 23:02 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.ReadOnly

  <Serializable()> _
  Public Class ROTravellerSummaryList
    Inherits OBReadOnlyListBase(Of ROTravellerSummaryList, ROTravellerSummary)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROTravellerSummary

      For Each child As ROTravellerSummary In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROTravellerSummaryFlight(FlightHumanResourceID As Integer) As ROTravellerSummaryFlight

      Dim obj As ROTravellerSummaryFlight = Nothing
      For Each parent As ROTravellerSummary In Me
        obj = parent.ROTravellerSummaryFlightList.GetItem(FlightHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROTravellerSummaryRentalCar(RentalCarHumanResourceID As Integer) As ROTravellerSummaryRentalCar

      Dim obj As ROTravellerSummaryRentalCar = Nothing
      For Each parent As ROTravellerSummary In Me
        obj = parent.ROTravellerSummaryRentalCarList.GetItem(RentalCarHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROTravellerSummaryAccommodation(AccommodationHumanResourceID As Integer) As ROTravellerSummaryAccommodation

      Dim obj As ROTravellerSummaryAccommodation = Nothing
      For Each parent As ROTravellerSummary In Me
        obj = parent.ROTravellerSummaryAccommodationList.GetItem(AccommodationHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROTravellerSummaryChauffeur(ChauffeurDriverHumanResourceID As Integer) As ROTravellerSummaryChauffeur

      Dim obj As ROTravellerSummaryChauffeur = Nothing
      For Each parent As ROTravellerSummary In Me
        obj = parent.ROTravellerSummaryChauffeurList.GetItem(ChauffeurDriverHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer?
      Public Property TravelRequisitionID As Integer?

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROTravellerSummaryList() As ROTravellerSummaryList

      Return New ROTravellerSummaryList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTravellerSummaryList() As ROTravellerSummaryList

      Return DataPortal.Fetch(Of ROTravellerSummaryList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTravellerSummary.GetROTravellerSummary(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROTravellerSummary = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROTravellerSummaryFlightList.RaiseListChangedEvents = False
          parent.ROTravellerSummaryFlightList.Add(ROTravellerSummaryFlight.GetROTravellerSummaryFlight(sdr))
          parent.ROTravellerSummaryFlightList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROTravellerSummaryRentalCarList.RaiseListChangedEvents = False
          parent.ROTravellerSummaryRentalCarList.Add(ROTravellerSummaryRentalCar.GetROTravellerSummaryRentalCar(sdr))
          parent.ROTravellerSummaryRentalCarList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROTravellerSummaryAccommodationList.RaiseListChangedEvents = False
          parent.ROTravellerSummaryAccommodationList.Add(ROTravellerSummaryAccommodation.GetROTravellerSummaryAccommodation(sdr))
          parent.ROTravellerSummaryAccommodationList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROTravellerSummaryChauffeurList.RaiseListChangedEvents = False
          parent.ROTravellerSummaryChauffeurList.Add(ROTravellerSummaryChauffeur.GetROTravellerSummaryChauffeur(sdr))
          parent.ROTravellerSummaryChauffeurList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravellerSummaryList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace