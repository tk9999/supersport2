﻿' Generated 16 Apr 2017 23:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.ReadOnly

  <Serializable()> _
  Public Class ROTravellerSummaryRentalCarList
    Inherits OBReadOnlyListBase(Of ROTravellerSummaryRentalCarList, ROTravellerSummaryRentalCar)

#Region " Parent "

    <NotUndoable()> Private mParent As ROTravellerSummary
#End Region

#Region " Business Methods "

    Public Function GetItem(RentalCarHumanResourceID As Integer) As ROTravellerSummaryRentalCar

      For Each child As ROTravellerSummaryRentalCar In Me
        If child.RentalCarHumanResourceID = RentalCarHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewROTravellerSummaryRentalCarList() As ROTravellerSummaryRentalCarList

      Return New ROTravellerSummaryRentalCarList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace