﻿' Generated 04 Apr 2016 20:07 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.CrewMembers.ReadOnly

  <Serializable()> _
  Public Class ROTravelAvailableHumanResourceList
    Inherits OBReadOnlyListBase(Of ROTravelAvailableHumanResourceList, ROTravelAvailableHumanResource)

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROTravelAvailableHumanResource

      For Each child As ROTravelAvailableHumanResource In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TravelRequisitionID As Integer?

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROTravelAvailableHumanResourceList() As ROTravelAvailableHumanResourceList

      Return New ROTravelAvailableHumanResourceList()

    End Function

    Public Shared Sub BeginGetROTravelAvailableHumanResourceList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROTravelAvailableHumanResourceList)))

      Dim dp As New DataPortal(Of ROTravelAvailableHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROTravelAvailableHumanResourceList(CallBack As EventHandler(Of DataPortalResult(Of ROTravelAvailableHumanResourceList)))

      Dim dp As New DataPortal(Of ROTravelAvailableHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROTravelAvailableHumanResourceList() As ROTravelAvailableHumanResourceList

      Return DataPortal.Fetch(Of ROTravelAvailableHumanResourceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTravelAvailableHumanResource.GetROTravelAvailableHumanResource(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravelAvailableHumanResourceList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace