﻿' Generated 04 Apr 2016 20:07 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.CrewMembers.ReadOnly

  <Serializable()> _
  Public Class ROTravelAvailableHumanResource
    Inherits OBReadOnlyBase(Of ROTravelAvailableHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared DisciplinesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Disciplines, "Disciplines")
    ''' <summary>
    ''' Gets the Disciplines value
    ''' </summary>
    <Display(Name:="Disciplines", Description:="")>
    Public ReadOnly Property Disciplines() As String
      Get
        Return GetProperty(DisciplinesProperty)
      End Get
    End Property

    Public Shared IsDriverIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsDriverInd, "Is Driver", False)
    ''' <summary>
    ''' Gets the Is Driver value
    ''' </summary>
    <Display(Name:="Is Driver", Description:="")>
    Public ReadOnly Property IsDriverInd() As Boolean
      Get
        Return GetProperty(IsDriverIndProperty)
      End Get
    End Property

    Public Shared BaseCityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BaseCityCode, "City")
    ''' <summary>
    ''' Gets the Base City Code value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property BaseCityCode() As String
      Get
        Return GetProperty(BaseCityCodeProperty)
      End Get
    End Property

    Public Shared CrewTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CrewType, "Crew Type")
    ''' <summary>
    ''' Gets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="")>
    Public ReadOnly Property CrewType() As String
      Get
        Return GetProperty(CrewTypeProperty)
      End Get
    End Property

    Public Shared PreviousProductionNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreviousProductionName, "Prev Prd")
    ''' <summary>
    ''' Gets the Previous Production Name value
    ''' </summary>
    <Display(Name:="Prev Prd", Description:="")>
    Public ReadOnly Property PreviousProductionName() As String
      Get
        Return GetProperty(PreviousProductionNameProperty)
      End Get
    End Property

    Public Shared NextProductionNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NextProductionName, "Nxt Prd")
    ''' <summary>
    ''' Gets the Next Production Name value
    ''' </summary>
    <Display(Name:="Nxt Prd", Description:="")>
    Public ReadOnly Property NextProductionName() As String
      Get
        Return GetProperty(NextProductionNameProperty)
      End Get
    End Property

    Public Shared PreviousProductionCityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreviousProductionCity, "Prev Prd Cty")
    ''' <summary>
    ''' Gets the Previous Production City value
    ''' </summary>
    <Display(Name:="Prev Prd Cty", Description:="")>
    Public ReadOnly Property PreviousProductionCity() As String
      Get
        Return GetProperty(PreviousProductionCityProperty)
      End Get
    End Property

    Public Shared NextProductionCityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NextProductionCity, "Nxt Prd Cty")
    ''' <summary>
    ''' Gets the Next Production City value
    ''' </summary>
    <Display(Name:="Nxt Prd Cty", Description:="")>
    Public ReadOnly Property NextProductionCity() As String
      Get
        Return GetProperty(NextProductionCityProperty)
      End Get
    End Property

    Public Shared PreviousProductionCallTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PreviousProductionCallTime, "Prv Prd Time")
    ''' <summary>
    ''' Gets the Previous Production Call Time value
    ''' </summary>
    <Display(Name:="Prv Prd Time", Description:="")>
    Public ReadOnly Property PreviousProductionCallTime As DateTime?
      Get
        Return GetProperty(PreviousProductionCallTimeProperty)
      End Get
    End Property

    <Display(Name:="Prv Prd Time", Description:="")>
    Public ReadOnly Property PreviousProductionCallTimeString As String
      Get
        If PreviousProductionCallTime IsNot Nothing Then
          Return PreviousProductionCallTime.Value.ToString("dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared NextProductionCallTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.NextProductionCallTime, "Nxt Prd Time")
    ''' <summary>
    ''' Gets the Next Production Call Time value
    ''' </summary>
    <Display(Name:="Nxt Prd Time", Description:="")>
    Public ReadOnly Property NextProductionCallTime As DateTime?
      Get
        Return GetProperty(NextProductionCallTimeProperty)
      End Get
    End Property

    <Display(Name:="Nxt Prd Time", Description:="")>
    Public ReadOnly Property NxtProductionCallTimeString As String
      Get
        If NextProductionCallTime IsNot Nothing Then
          Return NextProductionCallTime.Value.ToString("dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared DriverNeedsPreTravelProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DriverNeedsPreTravel, "Driver Needs Pre Travel", False)
    ''' <summary>
    ''' Gets the Driver Needs Pre Travel value
    ''' </summary>
    <Display(Name:="Driver Needs Pre Travel", Description:="")>
    Public ReadOnly Property DriverNeedsPreTravel() As Boolean
      Get
        Return GetProperty(DriverNeedsPreTravelProperty)
      End Get
    End Property

    Public Shared DriverNeedsPostTravelProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DriverNeedsPostTravel, "Driver Needs Post Travel", False)
    ''' <summary>
    ''' Gets the Driver Needs Post Travel value
    ''' </summary>
    <Display(Name:="Driver Needs Post Travel", Description:="")>
    Public ReadOnly Property DriverNeedsPostTravel() As Boolean
      Get
        Return GetProperty(DriverNeedsPostTravelProperty)
      End Get
    End Property

    Public Shared SortOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SortOrder, "Sort Order")
    ''' <summary>
    ''' Gets the Sort Order value
    ''' </summary>
    <Display(Name:="Sort Order", Description:="")>
    Public ReadOnly Property SortOrder() As Integer
      Get
        Return GetProperty(SortOrderProperty)
      End Get
    End Property

    Public Shared DriversLicenceIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DriversLicenceInd, "Drivers Licence", False)
    ''' <summary>
    ''' Gets the Drivers Licence value
    ''' </summary>
    <Display(Name:="Drivers Licence", Description:="")>
    Public ReadOnly Property DriversLicenceInd() As Boolean
      Get
        Return GetProperty(DriversLicenceIndProperty)
      End Get
    End Property

    Public Shared LicenseExpieryDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LicenseExpieryDate, "Drv Lic Exp")
    ''' <summary>
    ''' Gets the License Expiery Date value
    ''' </summary>
    <Display(Name:="Drv Lic Exp", Description:="")>
    Public ReadOnly Property LicenseExpieryDate As DateTime?
      Get
        Return GetProperty(LicenseExpieryDateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROTravelAvailableHumanResource(dr As SafeDataReader) As ROTravelAvailableHumanResource

      Dim r As New ROTravelAvailableHumanResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(HumanResourceProperty, .GetString(2))
        LoadProperty(DisciplinesProperty, .GetString(3))
        LoadProperty(IsDriverIndProperty, .GetBoolean(4))
        LoadProperty(BaseCityCodeProperty, .GetString(5))
        LoadProperty(CrewTypeProperty, .GetString(6))
        LoadProperty(PreviousProductionNameProperty, .GetString(7))
        LoadProperty(NextProductionNameProperty, .GetString(8))
        LoadProperty(PreviousProductionCityProperty, .GetString(9))
        LoadProperty(NextProductionCityProperty, .GetString(10))
        LoadProperty(PreviousProductionCallTimeProperty, .GetValue(11))
        LoadProperty(NextProductionCallTimeProperty, .GetValue(12))
        LoadProperty(DriverNeedsPreTravelProperty, .GetBoolean(13))
        LoadProperty(DriverNeedsPostTravelProperty, .GetBoolean(14))
        LoadProperty(SortOrderProperty, .GetInt32(15))
        LoadProperty(DriversLicenceIndProperty, .GetBoolean(16))
        LoadProperty(LicenseExpieryDateProperty, .GetValue(17))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace