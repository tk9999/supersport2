﻿' Generated 16 Apr 2017 23:03 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.ReadOnly

  <Serializable()> _
  Public Class ROTravellerSummaryRentalCar
    Inherits OBReadOnlyBase(Of ROTravellerSummaryRentalCar)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROTravellerSummaryRentalCarBO.ROTravellerSummaryRentalCarBOToString(self)")

    Public Shared RentalCarHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property RentalCarHumanResourceID() As Integer
      Get
        Return GetProperty(RentalCarHumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared RentalCarDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarDescription, "Rental Car Description")
    ''' <summary>
    ''' Gets the Rental Car Description value
    ''' </summary>
    <Display(Name:="Rental Car Description", Description:="")>
  Public ReadOnly Property RentalCarDescription() As String
      Get
        Return GetProperty(RentalCarDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.RentalCarDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerSummaryRentalCar(dr As SafeDataReader) As ROTravellerSummaryRentalCar

      Dim r As New ROTravellerSummaryRentalCar()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RentalCarHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(RentalCarDescriptionProperty, .GetString(2))
      End With

    End Sub

#End Region

  End Class

End Namespace