﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.BulkRentalCars

  <Serializable()> _
  Public Class BulkRentalCarList
    Inherits OBBusinessListBase(Of BulkRentalCarList, BulkRentalCar)

#Region " Business Methods "

    Public Function GetItem(BulkRentalCarID As Integer) As BulkRentalCar

      For Each child As BulkRentalCar In Me
        If child.BulkRentalCarID = BulkRentalCarID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Rental Cars"

    End Function

    'Public Function GetBulkRentalCarHumanResource(BulkRentalCarHumanResourceID As Integer) As BulkRentalCarHumanResource

    '  Dim obj As BulkRentalCarHumanResource = Nothing
    '  For Each parent As BulkRentalCar In Me
    '    obj = parent.BulkRentalCarHumanResourceList.GetItem(BulkRentalCarHumanResourceID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    Public Sub UpdateTravelRequisitionID(TravelRequisitionID As Integer)

      Me.ToList.ForEach(Sub(z)
                          If z.IsNew Then
                            z.TravelRequisitionID = TravelRequisitionID
                          End If
                        End Sub)

    End Sub

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property BulkRentalCarID As Integer? = Nothing

      Public Sub New(TravelRequisitionID As Integer?, BulkRentalCarID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        Me.BulkRentalCarID = BulkRentalCarID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewBulkRentalCarList() As BulkRentalCarList

      Return New BulkRentalCarList()

    End Function

    Public Shared Sub BeginGetBulkRentalCarList(CallBack As EventHandler(Of DataPortalResult(Of BulkRentalCarList)))

      Dim dp As New DataPortal(Of BulkRentalCarList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetBulkRentalCarList() As BulkRentalCarList

      Return DataPortal.Fetch(Of BulkRentalCarList)(New Criteria())

    End Function

    Public Shared Function GetBulkRentalCarList(TravelRequisitionID As Integer?, BulkRentalCarID As Integer?) As BulkRentalCarList

      Return DataPortal.Fetch(Of BulkRentalCarList)(New Criteria(TravelRequisitionID, BulkRentalCarID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(BulkRentalCar.GetBulkRentalCar(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'Dim parent As BulkRentalCar = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.BulkRentalCarID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.BulkRentalCarHumanResourceList.RaiseListChangedEvents = False
      '    parent.BulkRentalCarHumanResourceList.Add(BulkRentalCarHumanResource.GetBulkRentalCarHumanResource(sdr))
      '    parent.BulkRentalCarHumanResourceList.RaiseListChangedEvents = True
      '  End While
      'End If

      For Each child As BulkRentalCar In Me
        child.CheckRules()
        'For Each rchr As BulkRentalCarHumanResource In child.BulkRentalCarHumanResourceList
        '  rchr.CheckRules()
        'Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getBulkRentalCarList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@BulkRentalCarID", NothingDBNull(crit.BulkRentalCarID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As BulkRentalCar In DeletedList
          Child.DeleteSelf()
        Next
        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As BulkRentalCar In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

    'Private Sub DeleteBulk()

    '  'Dim dt As DataTable = CreateCrewScheduleTableParameter()
    '  Using cm As SqlCommand = New SqlCommand
    '    cm.CommandText = "DelProcsWeb.delBulkRentalCarListBulk"
    '    cm.CommandType = CommandType.StoredProcedure
    '    Dim sqlP As SqlClient.SqlParameter = New SqlClient.SqlParameter("@BulkRentalCarIDs", SqlDbType.VarChar)
    '    sqlP.Value = OBMisc.IntegerListToXML(Me.DeletedList.Select(Function(d) d.BulkRentalCarID).ToList)
    '    cm.Connection = CType(Csla.ApplicationContext.LocalContext("cn"), SqlClient.SqlConnection)
    '    cm.Parameters.Add(sqlP)
    '    cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
    '    cm.Transaction = Csla.ApplicationContext.LocalContext("tr")
    '    Singular.CSLALib.ContextInfo.SetContextInfoOnConnection(cm.Connection, cm.Transaction)
    '    cm.ExecuteNonQuery()
    '  End Using

    'End Sub

#End If

#End Region

#End Region

  End Class

End Namespace