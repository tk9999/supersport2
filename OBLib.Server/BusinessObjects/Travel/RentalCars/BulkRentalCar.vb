﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Travel.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.BulkRentalCars

  <Serializable()> _
  Public Class BulkRentalCar
    Inherits OBBusinessBase(Of BulkRentalCar)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return BulkRentalCarBO.toString(self)")

    Public Shared BulkRentalCarIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BulkRentalCarID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property BulkRentalCarID() As Integer
      Get
        Return GetProperty(BulkRentalCarIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelRequisitionID, "Travel Requisition", Nothing)
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    Public Property TravelRequisitionID() As Integer?
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(TravelRequisitionIDProperty, value)
      End Set
    End Property

    Public Shared RentalCarAgentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RentalCarAgentID, "Agent", OBLib.CommonData.Enums.RentalCarAgents.Bidvest)
    ''' <summary>
    ''' Gets and sets the Rental Car Agent value
    ''' </summary>
    <Display(Name:="Agent", Description:="Rental Car Agent that is being booked"),
    Required(ErrorMessage:="Agent required"),
    DropDownWeb(GetType(RORentalCarAgentList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="BulkRentalCarBO.setRentalCarAgentIDCriteriaBeforeRefresh",
                 PreFindJSFunction:="BulkRentalCarBO.triggerRentalCarAgentIDAutoPopulate",
                 AfterFetchJS:="BulkRentalCarBO.afterRentalCarAgentIDRefreshAjax",
                 LookupMember:="RentalCarAgent", ValueMember:="RentalCarAgentID", DisplayMember:="RentalCarAgent", DropDownColumns:={"RentalCarAgent"},
                 OnItemSelectJSFunction:="BulkRentalCarBO.onRentalCarAgentIDSelected", DropDownCssClass:="room-dropdown"),
    Singular.DataAnnotations.SetExpression("BulkRentalCarBO.AgentBranchIDSet(self)")>
    Public Property RentalCarAgentID() As Integer?
      Get
        Return GetProperty(RentalCarAgentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RentalCarAgentIDProperty, Value)
      End Set
    End Property
    'Singular.DataAnnotations.DropDownWeb(GetType(RORentalCarAgentList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo),

    Public Shared RentalCarAgentProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.RentalCarAgent, "Bidvest")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Agent", Description:="Agency used for the rental")>
    Public Property RentalCarAgent() As String
      Get
        Return GetProperty(RentalCarAgentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RentalCarAgentProperty, Value)
      End Set
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "Crew Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="The type of crew this travel is booked for"),
    Required(ErrorMessage:="Agent required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCrewTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
                                         FilterMethodName:="BulkRentalCarBO.filterROCrewTypes", DisplayMember:="TravelScreenDisplayName")>
    Public Property CrewTypeID() As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CarTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.CarTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Car Type value
    ''' </summary>
    <Display(Name:="Car Type", Description:="The type of car. Used to determine the number of passengers this car can hold"),
    Required(ErrorMessage:="Car Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCarTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo),
    SetExpression("BulkRentalCarBO.CarTypeIDSet(self)")>
    Public Property CarTypeID() As Integer?
      Get
        Return GetProperty(CarTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CarTypeIDProperty, Value)
      End Set
    End Property

    Public Shared BulkRentalCarRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BulkRentalCarRefNo, "Rental Car Ref No", "")
    ''' <summary>
    ''' Gets and sets the Rental Car Ref No value
    ''' </summary>
    <Display(Name:="Rental Car Ref No", Description:="Ref No for the vehicle, most likely the reference number provided by the agency"),
    StringLength(20, ErrorMessage:="Rental Car Ref No cannot be more than 20 characters")>
    Public Property BulkRentalCarRefNo() As String
      Get
        Return GetProperty(BulkRentalCarRefNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BulkRentalCarRefNoProperty, Value)
      End Set
    End Property

    Public Shared AgentBranchIDFromProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AgentBranchIDFrom, "From Branch", Nothing)
    ''' <summary>
    ''' Gets and sets the Agent Branch ID From value
    ''' </summary>
    <Display(Name:="From Branch", Description:="Agency Branch at which the rental car will be collected"),
     DropDownWeb(GetType(RORentalCarAgentBranchList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="BulkRentalCarBO.setAgentBranchIDFromCriteriaBeforeRefresh",
                 PreFindJSFunction:="BulkRentalCarBO.triggerAgentBranchIDFromAutoPopulate",
                 AfterFetchJS:="BulkRentalCarBO.afterAgentBranchIDFromRefreshAjax",
                 LookupMember:="BranchFrom", ValueMember:="RentalCarAgentBranchID", DisplayMember:="RentalCarAgentBranchName", DropDownColumns:={"RentalCarAgentBranchName"},
                 OnItemSelectJSFunction:="BulkRentalCarBO.onAgentBranchIDFromSelected", DropDownCssClass:="room-dropdown")>
    Public Property AgentBranchIDFrom() As Integer?
      Get
        Return GetProperty(AgentBranchIDFromProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AgentBranchIDFromProperty, Value)
      End Set
    End Property
    'Singular.DataAnnotations.DropDownWeb(GetType(ROBulkRentalCarAgentBranchList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
    '                                     DropDownColumns:={"BulkRentalCarAgent", "BulkRentalCarAgentBranch"},
    '                                     DisplayMember:="RentalCarAgentBranchName", ValueMember:="RentalCarAgentBranchID")
    '    Required(ErrorMessage:="Agent Branch ID From required"),

    Public Shared BranchFromProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.BranchFrom, "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="From Branch", Description:="Branch being picked up at")>
    Public Property BranchFrom() As String
      Get
        Return GetProperty(BranchFromProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BranchFromProperty, Value)
      End Set
    End Property

    Public Shared PickupDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PickupDateTime, "Pickup Date")
    ''' <summary>
    ''' Gets and sets the Pickup Date Time value
    ''' </summary>
    <Display(Name:="Pickup Date", Description:="The date and time the rental car is to be collected"),
    Required(ErrorMessage:="Pickup Date required"), Singular.DataAnnotations.DateField(InitialDateFunction:="BulkRentalCarBO.InitDate($data)"),
    SetExpression("BulkRentalCarBO.PickupDateTimeSet(self)")>
    Public Property PickupDateTime As DateTime?
      Get
        Return GetProperty(PickupDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(PickupDateTimeProperty, Value)
      End Set
    End Property

    Public Shared AgentBranchIDToProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AgentBranchIDTo, "To Branch", Nothing)
    ''' <summary>
    ''' Gets and sets the Agent Branch ID To value
    ''' </summary>
    <Display(Name:="To Branch", Description:="The agency branch that the rental car will be dropped off at"),
    DropDownWeb(GetType(RORentalCarAgentBranchList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="BulkRentalCarBO.setAgentBranchIDToCriteriaBeforeRefresh",
                PreFindJSFunction:="BulkRentalCarBO.triggerAgentBranchIDToAutoPopulate",
                AfterFetchJS:="BulkRentalCarBO.afterAgentBranchIDToRefreshAjax",
                LookupMember:="BranchTo", ValueMember:="RentalCarAgentBranchID", DisplayMember:="RentalCarAgentBranchName", DropDownColumns:={"RentalCarAgentBranchName"},
                OnItemSelectJSFunction:="BulkRentalCarBO.onAgentBranchIDToSelected", DropDownCssClass:="room-dropdown")>
    Public Property AgentBranchIDTo() As Integer?
      Get
        Return GetProperty(AgentBranchIDToProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AgentBranchIDToProperty, Value)
      End Set
    End Property
    'Singular.DataAnnotations.DropDownWeb(GetType(ROBulkRentalCarAgentBranchList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
    '                                     DropDownColumns:={"BulkRentalCarAgent", "BulkRentalCarAgentBranch"},
    '                                     DisplayMember:="RentalCarAgentBranchName", ValueMember:="RentalCarAgentBranchID")
    '    Required(ErrorMessage:="Agent Branch ID To required"),

    Public Shared BranchToProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.BranchTo, "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="To Branch", Description:="Branch being dropped off at")>
    Public Property BranchTo() As String
      Get
        Return GetProperty(BranchToProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BranchToProperty, Value)
      End Set
    End Property

    Public Shared DropoffDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DropoffDateTime, "Dropoff Date")
    ''' <summary>
    ''' Gets and sets the Dropoff Date Time value
    ''' </summary>
    <Display(Name:="Dropoff Date", Description:="The date and time the rental car is to be dropped off"),
    Required(ErrorMessage:="Dropoff Date required"), Singular.DataAnnotations.DateField(InitialDateFunction:="BulkRentalCarBO.InitDate($data)"),
    SetExpression("BulkRentalCarBO.DropoffDateTimeSet(self)")>
    Public Property DropoffDateTime As DateTime?
      Get
        Return GetProperty(DropoffDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(DropoffDateTimeProperty, Value)
      End Set
    End Property

    'Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ' ''' <summary>
    ' ''' Gets and sets the Cancelled Date Time value
    ' ''' </summary>
    '<Display(Name:="Cancelled Date Time", Description:="When the booking was cancelled")>
    'Public Property CancelledDateTime As DateTime?
    '  Get
    '    Return GetProperty(CancelledDateTimeProperty)
    '  End Get
    '  Set(ByVal Value As DateTime?)
    '    SetProperty(CancelledDateTimeProperty, Value)
    '  End Set
    'End Property

    'Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Cancelled By User value
    ' ''' </summary>
    '<Display(Name:="Cancelled By User", Description:="Who cancelled the booking")>
    'Public Property CancelledByUserID() As Integer?
    '  Get
    '    Return GetProperty(CancelledByUserIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(CancelledByUserIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CancelledReason, "")
    ' ''' <summary>
    ' ''' Gets and sets the Cancelled Reason value
    ' ''' </summary>
    '<Display(Name:="Cancelled Reason", Description:="Why the booking was cancelled"),
    'StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters"),
    'SetExpression("BulkRentalCarBO.CancelledReasonSet(self)")>
    'Public Property CancelledReason() As String
    '  Get
    '    Return GetProperty(CancelledReasonProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(CancelledReasonProperty, Value)
    '    UpdateCancelledReasonRCHRList(Value)
    '  End Set
    'End Property

    'Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ' ''' <summary>
    ' ''' Gets the Created By value
    ' ''' </summary>
    '<Display(AutoGenerateField:=False)>
    'Public ReadOnly Property CreatedBy() As Integer?
    '  Get
    '    Return GetProperty(CreatedByProperty)
    '  End Get
    'End Property

    'Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ' ''' <summary>
    ' ''' Gets the Created Date Time value
    ' ''' </summary>
    '<Display(AutoGenerateField:=False)>
    'Public ReadOnly Property CreatedDateTime() As SmartDate
    '  Get
    '    Return GetProperty(CreatedDateTimeProperty)
    '  End Get
    'End Property

    'Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ' ''' <summary>
    ' ''' Gets the Modified By value
    ' ''' </summary>
    '<Display(AutoGenerateField:=False)>
    'Public ReadOnly Property ModifiedBy() As Integer?
    '  Get
    '    Return GetProperty(ModifiedByProperty)
    '  End Get
    'End Property

    'Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ' ''' <summary>
    ' ''' Gets the Modified Date Time value
    ' ''' </summary>
    '<Display(AutoGenerateField:=False)>
    'Public ReadOnly Property ModifiedDateTime() As SmartDate
    '  Get
    '    Return GetProperty(ModifiedDateTimeProperty)
    '  End Get
    'End Property

    'Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Cancelled?", False)
    ' ''' <summary>
    ' ''' Gets and sets the Cancelled value
    ' ''' </summary>
    '<Display(Name:="Cancelled Indicator", Description:=""),
    'SetExpression("BulkRentalCarBO.IsCancelledSet(self)")>
    'Public Property IsCancelled() As Boolean
    '  Get
    '    Return GetProperty(IsCancelledProperty)
    '  End Get
    '  Set(ByVal Value As Boolean)
    '    SetProperty(IsCancelledProperty, Value)
    '    UpdateCancelledDetails(Value)
    '    UpdateIsCancelledRCHRList(Value)
    '  End Set
    'End Property

    'Public Shared RemainingSeatsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RemainingSeats, "Seats", "")
    ' ''' <summary>
    ' ''' Gets and sets the Remaining Seats value
    ' ''' </summary>
    '<Display(Name:="Seats", Description:="Remaining Seats")>
    'Public Property RemainingSeats() As String
    '  Get
    '    Return GetProperty(RemainingSeatsProperty)
    '  End Get
    '  Set(value As String)
    '    SetProperty(RemainingSeatsProperty, value)
    '  End Set
    'End Property

    'Public Shared PassengerCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PassengerCount, "Passenger Count", 0)
    ' ''' <summary>
    ' ''' Gets and sets the Accommodation Provider value
    ' ''' </summary>
    '<Display(Name:="Passenger Count", Description:="Passenger Count")>
    'Public ReadOnly Property PassengerCount() As Integer
    '  Get
    '    Return GetProperty(PassengerCountProperty)
    '  End Get
    'End Property

    'Public Shared Property PassengerCountErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PassengerCountError, "Passenger Count Error", "")
    ' ''' <summary>
    ' ''' Gets and sets the Create By value
    ' ''' </summary>
    '<Display(Name:="Passenger Count Error", Description:="")>
    'Public Property PassengerCountError() As String
    '  Get
    '    Return GetProperty(PassengerCountErrorProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(PassengerCountErrorProperty, Value)
    '  End Set
    'End Property

    'Public Shared Property BulkRentalCarDriverErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BulkRentalCarDriverError, "Driver Error", "")
    ' ''' <summary>
    ' ''' Gets and sets the Create By value
    ' ''' </summary>
    '<Display(Name:="Error", Description:="")>
    'Public Property BulkRentalCarDriverError() As String
    '  Get
    '    Return GetProperty(BulkRentalCarDriverErrorProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(BulkRentalCarDriverErrorProperty, Value)
    '  End Set
    'End Property

    Public Shared Property DriverProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Driver, "Driver", "")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Driver", Description:="")>
    Public Property Driver() As String
      Get
        Return GetProperty(DriverProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DriverProperty, Value)
      End Set
    End Property
    'Required(AllowEmptyStrings:=False, ErrorMessage:="Driver is required")

    Public Shared QuantityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Quantity, "Quantity", 0)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="Quantity", Description:="")>
    Public Property Quantity() As Integer
      Get
        Return GetProperty(QuantityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(QuantityProperty, Value)
      End Set
    End Property

    Public Shared HRClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HRClashCount, "HR Clash Count", 0)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="HR Clash Count", Description:="")>
    Public Property HRClashCount() As Integer
      Get
        Return GetProperty(HRClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HRClashCountProperty, Value)
      End Set
    End Property



#End Region

    '#Region " Child Lists "

    '    Public Shared BulkRentalCarHumanResourceListProperty As PropertyInfo(Of BulkRentalCarHumanResourceList) = RegisterProperty(Of BulkRentalCarHumanResourceList)(Function(c) c.BulkRentalCarHumanResourceList, "Rental Car Human Resource List")

    '    Public ReadOnly Property BulkRentalCarHumanResourceList() As BulkRentalCarHumanResourceList
    '      Get
    '        If GetProperty(BulkRentalCarHumanResourceListProperty) Is Nothing Then
    '          LoadProperty(BulkRentalCarHumanResourceListProperty, OBLib.Travel.BulkRentalCars.BulkRentalCarHumanResourceList.NewBulkRentalCarHumanResourceList())
    '        End If
    '        Return GetProperty(BulkRentalCarHumanResourceListProperty)
    '      End Get
    '    End Property

    '#End Region

#Region " Methods "

    Public Overrides Sub CanDelete(CallBack As System.EventHandler(Of Singular.CanDeleteArgs))
      MyBase.CanDelete(CallBack)

    End Sub

    Public Function GetParent() As OBLib.Travel.TravelRequisition

      Return CType(CType(Me.Parent, BulkRentalCarList).Parent, OBLib.Travel.TravelRequisition)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(BulkRentalCarIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.RentalCarAgentID = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Rental Car")
        Else
          Return String.Format("Blank {0}", "Rental Car: " & CDate(PickupDateTime).ToString("dd MMM yy HH:mm") & " - " & CDate(DropoffDateTime).ToString("dd MMM yy HH:mm"))
        End If
      Else
        Return Me.RentalCarAgentID
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"BulkRentalCarHumanResources"}
      End Get
    End Property

    'Private Sub UpdateCancelledDetails(Value As Boolean)

    '  If Value = True Then
    '    SetProperty(CancelledByUserIDProperty, Security.Settings.CurrentUser.UserID)
    '    SetProperty(CancelledDateTimeProperty, Date.Now)
    '  Else
    '    SetProperty(CancelledByUserIDProperty, Nothing)
    '    SetProperty(CancelledDateTimeProperty, Nothing)
    '  End If

    'End Sub

    'Private Sub UpdateIsCancelledRCHRList(Value As Boolean)

    '  BulkRentalCarHumanResourceList.ToList.ForEach(Sub(z)
    '                                                  z.IsCancelled = Value
    '                                                End Sub)

    'End Sub

    'Private Sub UpdateCancelledReasonRCHRList(Reason As String)

    '  BulkRentalCarHumanResourceList.ToList.ForEach(Sub(z)
    '                                                  z.CancelledReason = Reason
    '                                                End Sub)

    'End Sub

    'Public Sub SetupRemainingSeats(MaxNoOfOccupants As Integer)

    '  RemainingSeats = "(" & MaxNoOfOccupants - Me.BulkRentalCarHumanResourceList.Where(Function(d) d.CancelledByUserID Is Nothing).Count & " remain)"

    'End Sub

    'Public Sub SetPassengerCount()

    '  LoadProperty(PassengerCountProperty, Me.BulkRentalCarHumanResourceList.Where(Function(d) d.CancelledByUserID Is Nothing _
    '                                                                           AndAlso d.BulkRentalCarHumanResourceID <> 0).Count)

    'End Sub

    'Private Shared Function CreateBulkRentalCarClashesTableParameter(BulkRentalCarHumanResourceList As BulkRentalCarHumanResourceList) As DataTable

    '  Dim RBTable As New DataTable
    '  RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("TravelRequisitionID", GetType(Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("BulkRentalCarID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("BulkRentalCarHumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("RentalStartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("RentalEndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("IsCancelled", GetType(System.Boolean)).ColumnMapping = MappingType.Attribute

    '  Dim rowList As New List(Of DataRow)
    '  For Each rb As BulkRentalCarHumanResource In BulkRentalCarHumanResourceList
    '    Dim row As DataRow = RBTable.NewRow
    '    row("ObjectGuid") = rb.Guid
    '    row("TravelRequisitionID") = rb.GetParent.TravelRequisitionID
    '    row("BulkRentalCarID") = NothingDBNull(rb.BulkRentalCarID)
    '    row("BulkRentalCarHumanResourceID") = rb.BulkRentalCarHumanResourceID
    '    row("HumanResourceID") = rb.HumanResourceID
    '    row("RentalStartDateTime") = rb.GetParent.PickupDateTime
    '    row("RentalEndDateTime") = rb.GetParent.DropoffDateTime
    '    row("IsCancelled") = rb.IsCancelled
    '    RBTable.Rows.Add(row)
    '  Next

    '  Return RBTable

    'End Function

    'Private Shared Function GetBulkRentalCarHRClashesDataSet(BulkRentalCarHumanResourceList As BulkRentalCarHumanResourceList) As DataSet

    '  Dim RBTable As DataTable = CreateBulkRentalCarClashesTableParameter(BulkRentalCarHumanResourceList)
    '  Dim cmd As New Singular.CommandProc("GetProcsWeb.getROBulkRentalCarClashList")
    '  Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
    '  ResourceBookingsParam.Name = "@BulkRentalCarHumanResources"
    '  ResourceBookingsParam.SqlType = SqlDbType.Structured
    '  ResourceBookingsParam.Value = RBTable
    '  cmd.Parameters.Add(ResourceBookingsParam)
    '  cmd.FetchType = CommandProc.FetchTypes.DataSet
    '  cmd = cmd.Execute(0)
    '  Return cmd.Dataset

    'End Function

    'Public Function UpdateClashes() As Singular.Web.Result

    '  Try
    '    Dim ds As DataSet = GetBulkRentalCarHRClashesDataSet(Me.BulkRentalCarHumanResourceList)
    '    Me.HRClashCount = 0
    '    Me.BulkRentalCarHumanResourceList.ToList.ForEach(Sub(c As BulkRentalCarHumanResource)
    '                                                       c.HasClashes = False
    '                                                       c.OtherBulkRentalCarsShort = ""
    '                                                       c.OtherBulkRentalCars = ""
    '                                                     End Sub)

    '    Me.HRClashCount = ds.Tables(0).Rows.Count

    '    For Each dr As DataRow In ds.Tables(0).Rows
    '      Dim rb As BulkRentalCarHumanResource = Me.BulkRentalCarHumanResourceList.Where(Function(d) d.Guid = dr(0)).FirstOrDefault
    '      If rb IsNot Nothing Then
    '        rb.HasClashes = True
    '        rb.OtherBulkRentalCarsShort = dr(1)
    '        rb.OtherBulkRentalCars = dr(2)
    '      End If
    '    Next

    '    Return New Singular.Web.Result(True)

    '  Catch ex As Exception
    '    Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    '  End Try

    'End Function


#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(PickupDateTimeProperty)
        .AddTriggerProperty(DropoffDateTimeProperty)
        .JavascriptRuleFunctionName = "BulkRentalCarBO.TravelDatesValid"
        .ServerRuleFunction = AddressOf TravelDatesValid
      End With

      With AddWebRule(RentalCarAgentIDProperty)
        .AddTriggerProperty(PickupDateTimeProperty)
        .AddTriggerProperty(DropoffDateTimeProperty)
        .AddTriggerProperty(AgentBranchIDFromProperty)
        .AddTriggerProperty(AgentBranchIDToProperty)
        .JavascriptRuleFunctionName = "BulkRentalCarBO.RentalCarAgentValid"
        .ServerRuleFunction = AddressOf BulkRentalCarAgentValid
      End With

      With AddWebRule(AgentBranchIDFromProperty)
        .AddTriggerProperty(PickupDateTimeProperty)
        .AddTriggerProperty(DropoffDateTimeProperty)
        .JavascriptRuleFunctionName = "BulkRentalCarBO.AgentBranchIDFromValid"
        .ServerRuleFunction = AddressOf AgentBranchIDFromValid
      End With

      With AddWebRule(AgentBranchIDToProperty)
        .AddTriggerProperty(PickupDateTimeProperty)
        .AddTriggerProperty(DropoffDateTimeProperty)
        .JavascriptRuleFunctionName = "BulkRentalCarBO.AgentBranchIDToValid"
        .ServerRuleFunction = AddressOf AgentBranchIDToValid
      End With

      'With AddWebRule(CancelledReasonProperty)
      '  .JavascriptRuleFunctionName = "BulkRentalCarBO.CancelledReasonRequired"
      '  .ServerRuleFunction = AddressOf CancelledReasonRequired
      'End With

      'taken care of in data annotation
      'With AddWebRule(DriverProperty)
      '  .JavascriptRuleFunctionName = "BulkRentalCarBO.DriverValid"
      '  .ServerRuleFunction = AddressOf PrimaryDriverValid
      'End With

      With AddWebRule(HRClashCountProperty)
        .JavascriptRuleFunctionName = "BulkRentalCarBO.HRClashCountValid"
        .ServerRuleFunction = AddressOf HRClashCountValid
      End With

      'With AddWebRule(BulkRentalCarHumanResourceListProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.BulkRentalCarBO.DriverValid
      'End With

      'With AddWebRule(BulkRentalCarIDProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.BulkRentalCarBO.DriverValid
      'End With

      'With AddWebRule(BulkRentalCarDriverErrorProperty,
      '           Function(d) d.BulkRentalCarDriverError <> "",
      '           Function(d) d.BulkRentalCarDriverError)
      'End With

    End Sub

    Public Function HRClashCountValid(BulkRentalCar As BulkRentalCar) As String

      Dim ErrorMessage As String = ""
      If BulkRentalCar.HRClashCount > 0 Then
        ErrorMessage = "There are passenger clashes on this rental car"
      End If

      Return ErrorMessage

    End Function

    'Public Function PrimaryDriverValid(BulkRentalCar As BulkRentalCar) As String

    '  Dim ErrorMessage As String = ""
    '  If BulkRentalCarHumanResourceList.Count > 0 Then
    '    If BulkRentalCarHumanResourceList.Where(Function(c) c.DriverInd).Count = 0 Then
    '      ErrorMessage = "Driver Required"
    '    End If
    '  End If

    '  Return ErrorMessage

    'End Function

    'Public Shared Function CancelledReasonRequired(BulkRentalCar As BulkRentalCar) As String
    '  Dim ErrorString = ""
    '  If BulkRentalCar.IsCancelled AndAlso BulkRentalCar.CancelledReason.Trim = "" Then
    '    ErrorString = "Cancelled Reason Required"
    '  End If
    '  Return ErrorString
    'End Function

    Public Shared Function TravelDatesValid(BulkRentalCar As BulkRentalCar) As String
      Dim ErrorString = ""
      If BulkRentalCar.PickupDateTime IsNot Nothing AndAlso BulkRentalCar.DropoffDateTime IsNot Nothing Then
        Dim CheckInDate = BulkRentalCar.PickupDateTime
        Dim CheckOutDate = BulkRentalCar.DropoffDateTime
        Dim result As Integer = DateTime.Compare(CheckInDate, CheckOutDate)
        If result >= 0 Then
          ErrorString = "Pickup Date must be before Dropoff Date"
        End If
      End If
      Return ErrorString
    End Function

    Public Shared Function BulkRentalCarAgentValid(BulkRentalCar As BulkRentalCar) As String
      If BulkRentalCar.RentalCarAgentID IsNot Nothing Then
        Dim ROBulkRentalCarAgent As RORentalCarAgent = OBLib.CommonData.Lists.RORentalCarAgentList.GetItem(BulkRentalCar.RentalCarAgentID)
        If ROBulkRentalCarAgent.OldInd Then
          If BulkRentalCar.PickupDateTime IsNot Nothing AndAlso BulkRentalCar.PickupDateTime > ROBulkRentalCarAgent.OldDate Then
            Return "This branch can no longer be selected: " & ROBulkRentalCarAgent.OldReason
          End If
        End If
      Else
        Return "Rental Car Agent is required"
      End If
      Return ""
    End Function

    Public Shared Function AgentBranchIDFromValid(BulkRentalCar As BulkRentalCar) As String
      If BulkRentalCar.AgentBranchIDFrom IsNot Nothing Then
        Dim ROBulkRentalCarAgentBranch As RORentalCarAgentBranch = OBLib.CommonData.Lists.RORentalCarAgentBranchList.GetItem(BulkRentalCar.AgentBranchIDFrom)
        If ROBulkRentalCarAgentBranch.OldInd Then
          If BulkRentalCar.PickupDateTime IsNot Nothing AndAlso BulkRentalCar.PickupDateTime > ROBulkRentalCarAgentBranch.OldDate Then
            Return "This branch can no longer be selected: " & ROBulkRentalCarAgentBranch.OldReason
            'Else
            '  Return "This branch can no longer be selected: " & ROBulkRentalCarAgentBranch.OldReason
          End If
        End If
      Else
        Return "Branch From is required"
      End If
      Return ""
    End Function

    Public Shared Function AgentBranchIDToValid(BulkRentalCar As BulkRentalCar) As String
      If BulkRentalCar.AgentBranchIDTo IsNot Nothing Then
        Dim ROBulkRentalCarAgentBranch As RORentalCarAgentBranch = OBLib.CommonData.Lists.RORentalCarAgentBranchList.GetItem(BulkRentalCar.AgentBranchIDTo)
        If ROBulkRentalCarAgentBranch.OldInd Then
          If BulkRentalCar.PickupDateTime IsNot Nothing AndAlso BulkRentalCar.PickupDateTime > ROBulkRentalCarAgentBranch.OldDate Then
            Return "This branch can no longer be selected: " & ROBulkRentalCarAgentBranch.OldReason
            'Else
            '  Return "This branch can no longer be selected: " & ROBulkRentalCarAgentBranch.OldReason
          End If
        End If
      Else
        Return "Branch To is required"
      End If
      Return ""
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewBulkRentalCar() method.

    End Sub

    Public Shared Function NewBulkRentalCar() As BulkRentalCar

      Return DataPortal.CreateChild(Of BulkRentalCar)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetBulkRentalCar(dr As SafeDataReader) As BulkRentalCar

      Dim r As New BulkRentalCar()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(BulkRentalCarIDProperty, .GetInt32(0))
          LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          'LoadProperty(BulkRentalCarAgentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CarTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(BulkRentalCarRefNoProperty, .GetString(5))
          LoadProperty(AgentBranchIDFromProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(PickupDateTimeProperty, .GetValue(7))
          LoadProperty(AgentBranchIDToProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(DropoffDateTimeProperty, .GetValue(9))
          'LoadProperty(CancelledDateTimeProperty, .GetValue(10))
          'LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          'LoadProperty(CancelledReasonProperty, .GetString(12))
          'LoadProperty(CreatedByProperty, .GetInt32(13))
          'LoadProperty(CreatedDateTimeProperty, .GetSmartDate(14))
          'LoadProperty(ModifiedByProperty, .GetInt32(15))
          'LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(16))
          LoadProperty(DriverProperty, .GetString(17))
          'LoadProperty(RemainingSeatsProperty, .GetString(18))
          LoadProperty(BranchFromProperty, .GetString(19))
          LoadProperty(BranchToProperty, .GetString(20))
          'LoadProperty(PassengerCountProperty, .GetInt32(21))
          'LoadProperty(IsCancelledProperty, IIf(CancelledByUserID Is Nothing, False, True))
          LoadProperty(HRClashCountProperty, .GetInt32(22))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insBulkRentalCar"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updBulkRentalCar"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramBulkRentalCarID As SqlParameter = .Parameters.Add("@BulkRentalCarID", SqlDbType.Int)
          paramBulkRentalCarID.Value = GetProperty(BulkRentalCarIDProperty)
          If Me.IsNew Then
            paramBulkRentalCarID.Direction = ParameterDirection.Output
          End If
          If Me.GetParent IsNot Nothing Then
            SetProperty(TravelRequisitionIDProperty, Me.GetParent.TravelRequisitionID)
            .Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
          Else
            .Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(GetProperty(TravelRequisitionIDProperty)))
          End If
          '.Parameters.AddWithValue("@BulkRentalCarAgentID", GetProperty(BulkRentalCarAgentIDProperty))
          .Parameters.AddWithValue("@CrewTypeID", Singular.Misc.NothingDBNull(GetProperty(CrewTypeIDProperty)))
          .Parameters.AddWithValue("@CarTypeID", GetProperty(CarTypeIDProperty))
          .Parameters.AddWithValue("@BulkRentalCarRefNo", GetProperty(BulkRentalCarRefNoProperty))
          .Parameters.AddWithValue("@AgentBranchIDFrom", GetProperty(AgentBranchIDFromProperty))
          .Parameters.AddWithValue("@PickupDateTime", (New SmartDate(GetProperty(PickupDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@AgentBranchIDTo", GetProperty(AgentBranchIDToProperty))
          .Parameters.AddWithValue("@DropoffDateTime", (New SmartDate(GetProperty(DropoffDateTimeProperty))).DBValue)
          '.Parameters.AddWithValue("@CancelledDateTime", (New SmartDate(GetProperty(CancelledDateTimeProperty))).DBValue)
          '.Parameters.AddWithValue("@CancelledByUserID", Singular.Misc.NothingDBNull(GetProperty(CancelledByUserIDProperty)))
          '.Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@ModifiedBy", ZeroDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(BulkRentalCarIDProperty, paramBulkRentalCarID.Value)
          End If
          '' update child objects
          'If GetProperty(BulkRentalCarHumanResourceListProperty) IsNot Nothing Then
          '  Me.BulkRentalCarHumanResourceList.Update()
          'End If
          'SetPassengerCount()
          MarkOld()
        End With
      Else
        '' update child objects
        'If GetProperty(BulkRentalCarHumanResourceListProperty) IsNot Nothing Then
        '  Me.BulkRentalCarHumanResourceList.Update()
        'End If
        'SetPassengerCount()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delBulkRentalCar"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@BulkRentalCarID", GetProperty(BulkRentalCarIDProperty))
        cm.Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
        cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace