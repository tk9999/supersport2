﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.RentalCars

  <Serializable()> _
  Public Class RentalCarHumanResource
    Inherits OBBusinessBase(Of RentalCarHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RentalCarPassengerBO.toString(self)")

    Public Shared RentalCarHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public Property RentalCarHumanResourceID() As Integer
      Get
        Return GetProperty(RentalCarHumanResourceIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(RentalCarHumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared RentalCarIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RentalCarID, "Rental Car", Nothing)
    ''' <summary>
    ''' Gets the Rental Car value
    ''' </summary>
    Public Property RentalCarID() As Integer?
      Get
        Return GetProperty(RentalCarIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(RentalCarIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource ID", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource booked on this rental car")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared DriverIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DriverInd, "Driver", False)
    ''' <summary>
    ''' Gets and sets the Driver value
    ''' </summary>
    <Display(Name:="Driver", Description:="Tick indicates that this person is a primary driver of the rental car"),
    SetExpression("RentalCarPassengerBO.DriverIndSet(self)")>
    Public Property DriverInd() As Boolean
      Get
        Return GetProperty(DriverIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DriverIndProperty, Value)
      End Set
    End Property

    Public Shared CoDriverIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CoDriverInd, "Co Driver", False)
    ''' <summary>
    ''' Gets and sets the Co Driver value
    ''' </summary>
    <Display(Name:="Co Driver", Description:="Tick indicates that this person is a secondary driver of the rental car"),
    SetExpression("RentalCarPassengerBO.CoDriverIndSet(self)")>
    Public Property CoDriverInd() As Boolean
      Get
        Return GetProperty(CoDriverIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CoDriverIndProperty, Value)
      End Set
    End Property

    Public Shared BookingReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingReason, "Booking Reason", "")
    ''' <summary>
    ''' Gets and sets the Booking Reason value
    ''' </summary>
    <Display(Name:="Booking Reason", Description:="Why was the person booked. (only if required)"),
    StringLength(200, ErrorMessage:="Booking Reason cannot be more than 200 characters")>
    Public Property BookingReason() As String
      Get
        Return GetProperty(BookingReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BookingReasonProperty, Value)
      End Set
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets and sets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="When the booking was cancelled")>
    Public Property CancelledDateTime As DateTime?
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(CancelledDateTimeProperty, value)
      End Set
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="Who cancelled the booking")>
    Public Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(CancelledByUserIDProperty, value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="Why the booking was cancelled"),
    StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters"),
    SetExpression("RentalCarPassengerBO.CancelledReasonSet(self)")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Cancelled Indicator", False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:=""),
    SetExpression("RentalCarPassengerBO.IsCancelledSet(self)")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceName, "Human Resource Name", "")
    ''' <summary>
    ''' Gets and sets the Booking Reason value
    ''' </summary>
    <Display(Name:="Human Resource Name", Description:="Human Resource Name")>
    Public Property HumanResourceName() As String
      Get
        Return GetProperty(HumanResourceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceNameProperty, Value)
      End Set
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City Code", "")
    ''' <summary>
    ''' Gets and sets the City Code value
    ''' </summary>
    <Display(Name:="City Code", Description:="")>
    Public Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CityCodeProperty, Value)
      End Set
    End Property

    Public Shared OtherRentalCarsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OtherRentalCars, "Other Rental Cars", "")
    ''' <summary>
    ''' Gets and sets the Other Accommodations value
    ''' </summary>
    <Display(Name:="Other Rental Cars", Description:="")>
    Public Property OtherRentalCars() As String
      Get
        Return GetProperty(OtherRentalCarsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OtherRentalCarsProperty, Value)
      End Set
    End Property

    Public Shared OtherRentalCarsShortProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OtherRentalCarsShort, "Other Rental Cars", "")
    ''' <summary>
    ''' Gets and sets the Other Accommodations value
    ''' </summary>
    <Display(Name:="Other Rental Cars", Description:="")>
    Public Property OtherRentalCarsShort() As String
      Get
        Return GetProperty(OtherRentalCarsShortProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OtherRentalCarsShortProperty, Value)
      End Set
    End Property

    Public Shared DriverLicenseIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DriverLicenseInd, "Drivers License", False)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="Drivers License", Description:="")>
    Public Property DriverLicenseInd() As Boolean
      Get
        Return GetProperty(DriverLicenseIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DriverLicenseIndProperty, Value)
      End Set
    End Property

    Public Shared OnRentalCarIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnRentalCarInd, "Passenger Indicator", False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="Passenger Indicator", Description:=""),
    SetExpression("RentalCarPassengerBO.OnRentalCarIndSet(self)")>
    Public Property OnRentalCarInd() As Boolean
      Get
        Return GetProperty(OnRentalCarIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OnRentalCarIndProperty, Value)
      End Set
    End Property

    Public Shared HasClashesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasClashes, "Has Clashes", False)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="Has Clashes", Description:="")>
    Public Property HasClashes() As Boolean
      Get
        Return GetProperty(HasClashesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasClashesProperty, Value)
      End Set
    End Property

    Public Shared ReAddedToRentalCarProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ReAddedToRentalCar, "ReAddedToRentalCar", False)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="ReAddedToFlight", Description:="")>
    Public Property ReAddedToRentalCar() As Boolean
      Get
        Return GetProperty(ReAddedToRentalCarProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ReAddedToRentalCarProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As RentalCar

      Return CType(CType(Me.Parent, RentalCarHumanResourceList).Parent, RentalCar)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResourceID = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Rental Car Human Resource")
        Else
          Return String.Format(Me.HumanResourceName)
        End If
      Else
        Return Me.HumanResourceID
      End If

    End Function

    Private Sub UpdateCancelledDetails(Value As Boolean)

      If Value = True Then
        SetProperty(CancelledDateTimeProperty, Date.Now)
        SetProperty(CancelledByUserIDProperty, Security.Settings.CurrentUser.UserID)
      Else
        SetProperty(CancelledDateTimeProperty, Nothing)
        SetProperty(CancelledByUserIDProperty, Nothing)
      End If

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(OnRentalCarIndProperty)
        .AddTriggerProperty(OnRentalCarIndProperty)
        .JavascriptRuleFunctionName = "RentalCarPassengerBO.OnRentalCarIndValid"
        .ServerRuleFunction = AddressOf OnRentalCarIndValid
      End With

      With AddWebRule(CancelledReasonProperty)
        .AddTriggerProperty(IsCancelledProperty)
        .JavascriptRuleFunctionName = "RentalCarPassengerBO.CancelledReasonValid"
        .ServerRuleFunction = AddressOf CancelledReasonRequired
      End With

    End Sub

    Public Shared Function CancelledReasonRequired(RentalCarHumanResource As RentalCarHumanResource) As String
      Dim ErrorString = ""
      If RentalCarHumanResource.IsCancelled AndAlso RentalCarHumanResource.CancelledReason.Trim = "" Then
        ErrorString = "Cancelled Reason Required"
      End If
      Return ErrorString
    End Function

    Public Shared Function OnRentalCarIndValid(RentalCarHumanResource As RentalCarHumanResource) As String
      Dim ErrorString = ""
      If RentalCarHumanResource.OnRentalCarInd AndAlso Not RentalCarHumanResource.OtherRentalCars.Trim = "" Then
        ErrorString = RentalCarHumanResource.HumanResourceName & " has flight clashes"
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRentalCarHumanResource() method.

    End Sub

    Public Shared Function NewRentalCarHumanResource() As RentalCarHumanResource

      Return DataPortal.CreateChild(Of RentalCarHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRentalCarHumanResource(dr As SafeDataReader) As RentalCarHumanResource

      Dim r As New RentalCarHumanResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RentalCarHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(RentalCarIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DriverIndProperty, .GetBoolean(3))
          LoadProperty(CoDriverIndProperty, .GetBoolean(4))
          LoadProperty(BookingReasonProperty, .GetString(5))
          LoadProperty(CancelledDateTimeProperty, .GetValue(6))
          LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(CancelledReasonProperty, .GetString(8))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ModifiedByProperty, .GetInt32(11))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
          LoadProperty(IsCancelledProperty, .GetBoolean(13))
          LoadProperty(HumanResourceNameProperty, .GetString(14))
          LoadProperty(CityCodeProperty, .GetString(15))
          LoadProperty(OtherRentalCarsProperty, .GetString(16))
          LoadProperty(DriverLicenseIndProperty, .GetBoolean(17))
          'LoadProperty(IsSelectedProperty, .GetBoolean(18))
          LoadProperty(OnRentalCarIndProperty, .GetBoolean(18))
          'Disciplines = 19
          LoadProperty(OtherRentalCarsShortProperty, .GetString(20))
          LoadProperty(HasClashesProperty, .GetBoolean(21))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insRentalCarHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updRentalCarHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramRentalCarHumanResourceID As SqlParameter = .Parameters.Add("@RentalCarHumanResourceID", SqlDbType.Int)
          paramRentalCarHumanResourceID.Value = GetProperty(RentalCarHumanResourceIDProperty)
          'If Me.IsNew Then
          paramRentalCarHumanResourceID.Direction = ParameterDirection.InputOutput
          'End If
          .Parameters.AddWithValue("@IsSelected", GetProperty(OnRentalCarIndProperty))
          .Parameters.AddWithValue("@RentalCarID", Me.GetParent().RentalCarID)
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@DriverInd", GetProperty(DriverIndProperty))
          .Parameters.AddWithValue("@CoDriverInd", GetProperty(CoDriverIndProperty))
          .Parameters.AddWithValue("@BookingReason", GetProperty(BookingReasonProperty))
          .Parameters.AddWithValue("@CancelledDateTime", (New SmartDate(GetProperty(CancelledDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledByUserID", Singular.Misc.NothingDBNull(GetProperty(CancelledByUserIDProperty)))
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@ModifiedBy", ZeroDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@TravelRequisitionID", Me.GetParent().TravelRequisitionID)

          .ExecuteNonQuery()

          LoadProperty(RentalCarIDProperty, Me.GetParent.RentalCarID)
          'If Me.IsNew Then
          LoadProperty(RentalCarHumanResourceIDProperty, IIf(IsNullNothing(paramRentalCarHumanResourceID.Value), 0, paramRentalCarHumanResourceID.Value))
          'End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delRentalCarHumanResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@RentalCarHumanResourceID", GetProperty(RentalCarHumanResourceIDProperty))
        cm.Parameters.AddWithValue("@TravelRequisitionID", Me.GetParent().TravelRequisitionID)
        cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace