﻿' Generated 15 May 2016 21:10 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.RentalCars.ReadOnly

  <Serializable()> _
  Public Class ROTravellerRentalCarList
    Inherits OBReadOnlyListBase(Of ROTravellerRentalCarList, ROTravellerRentalCar)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "

    Public Function GetItem(RentalCarID As Integer) As ROTravellerRentalCar

      For Each child As ROTravellerRentalCar In Me
        If child.RentalCarID = RentalCarID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROTravellerRentalCarPassenger(RentalCarHumanResourceID As Integer) As ROTravellerRentalCarPassenger

      Dim obj As ROTravellerRentalCarPassenger = Nothing
      For Each parent As ROTravellerRentalCar In Me
        obj = parent.ROTravellerRentalCarPassengerList.GetItem(RentalCarHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New(TravelRequisitionID As Integer?, HumanResourceID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        Me.HumanResourceID = HumanResourceID
      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROTravellerRentalCarList() As ROTravellerRentalCarList

      Return New ROTravellerRentalCarList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTravellerRentalCarList() As ROTravellerRentalCarList

      Return DataPortal.Fetch(Of ROTravellerRentalCarList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTravellerRentalCar.GetROTravellerRentalCar(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROTravellerRentalCar = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.RentalCarID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROTravellerRentalCarPassengerList.RaiseListChangedEvents = False
          parent.ROTravellerRentalCarPassengerList.Add(ROTravellerRentalCarPassenger.GetROTravellerRentalCarPassenger(sdr))
          parent.ROTravellerRentalCarPassengerList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravellerRentalCarList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace
