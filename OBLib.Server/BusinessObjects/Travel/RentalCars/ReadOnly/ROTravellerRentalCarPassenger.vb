﻿' Generated 15 May 2016 21:11 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.RentalCars.ReadOnly

  <Serializable()> _
  Public Class ROTravellerRentalCarPassenger
    Inherits OBReadOnlyBase(Of ROTravellerRentalCarPassenger)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RentalCarHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property RentalCarHumanResourceID() As Integer
      Get
        Return GetProperty(RentalCarHumanResourceIDProperty)
      End Get
    End Property

    Public Shared RentalCarIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarID, "Rental Car")
    ''' <summary>
    ''' Gets the Rental Car value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property RentalCarID() As Integer
      Get
        Return GetProperty(RentalCarIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared DriverIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DriverInd, "Driver", False)
    ''' <summary>
    ''' Gets the Driver value
    ''' </summary>
    <Display(Name:="Driver", Description:="")>
    Public ReadOnly Property DriverInd() As Boolean
      Get
        Return GetProperty(DriverIndProperty)
      End Get
    End Property

    Public Shared CoDriverIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CoDriverInd, "Co Driver", False)
    ''' <summary>
    ''' Gets the Co Driver value
    ''' </summary>
    <Display(Name:="Co Driver", Description:="")>
    Public ReadOnly Property CoDriverInd() As Boolean
      Get
        Return GetProperty(CoDriverIndProperty)
      End Get
    End Property

    Public Shared BookingReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingReason, "Booking Reason")
    ''' <summary>
    ''' Gets the Booking Reason value
    ''' </summary>
    <Display(Name:="Booking Reason", Description:="")>
    Public ReadOnly Property BookingReason() As String
      Get
        Return GetProperty(BookingReasonProperty)
      End Get
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="")>
    Public ReadOnly Property CancelledDateTime As Date
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User")
    ''' <summary>
    ''' Gets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="")>
    Public ReadOnly Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="")>
    Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CancelledIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CancelledInd, "Cancelled?", False)
    ''' <summary>
    ''' Gets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled?", Description:="")>
    Public ReadOnly Property CancelledInd() As Boolean
      Get
        If CancelledByUserID IsNot Nothing Then
          Return True
        Else
          Return False
        End If
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared OldProductionCrewTravelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OldProductionCrewTravelID, "Old Production Crew Travel")
    ''' <summary>
    ''' Gets the Old Production Crew Travel value
    ''' </summary>
    <Display(Name:="Old Production Crew Travel", Description:="")>
    Public ReadOnly Property OldProductionCrewTravelID() As Integer
      Get
        Return GetProperty(OldProductionCrewTravelIDProperty)
      End Get
    End Property

    Public Shared OldPCTHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OldPCTHumanResourceID, "Old PCT Human Resource")
    ''' <summary>
    ''' Gets the Old PCT Human Resource value
    ''' </summary>
    <Display(Name:="Old PCT Human Resource", Description:="")>
    Public ReadOnly Property OldPCTHumanResourceID() As Integer
      Get
        Return GetProperty(OldPCTHumanResourceIDProperty)
      End Get
    End Property

    Public Shared AllocatedProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AllocatedProductionID, "Allocated Production")
    ''' <summary>
    ''' Gets the Allocated Production value
    ''' </summary>
    <Display(Name:="Allocated Production", Description:="")>
    Public ReadOnly Property AllocatedProductionID() As Integer
      Get
        Return GetProperty(AllocatedProductionIDProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared CommentUpdatedDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CommentUpdatedDateTime, "Comment Updated Date Time")
    ''' <summary>
    ''' Gets the Comment Updated Date Time value
    ''' </summary>
    <Display(Name:="Comment Updated Date Time", Description:="")>
    Public ReadOnly Property CommentUpdatedDateTime As Date
      Get
        Return GetProperty(CommentUpdatedDateTimeProperty)
      End Get
    End Property

    Public Shared ReplacementHRIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReplacementHRID, "Replacement HR")
    ''' <summary>
    ''' Gets the Replacement HR value
    ''' </summary>
    <Display(Name:="Replacement HR", Description:="")>
    Public ReadOnly Property ReplacementHRID() As Integer
      Get
        Return GetProperty(ReplacementHRIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.BookingReason

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerRentalCarPassenger(dr As SafeDataReader) As ROTravellerRentalCarPassenger

      Dim r As New ROTravellerRentalCarPassenger()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RentalCarHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(RentalCarIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(DriverIndProperty, .GetBoolean(3))
        LoadProperty(CoDriverIndProperty, .GetBoolean(4))
        LoadProperty(BookingReasonProperty, .GetString(5))
        LoadProperty(CancelledDateTimeProperty, .GetValue(6))
        LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(CancelledReasonProperty, .GetString(8))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(12))
        LoadProperty(OldProductionCrewTravelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(OldPCTHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(AllocatedProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
        LoadProperty(CommentsProperty, .GetString(16))
        LoadProperty(CommentUpdatedDateTimeProperty, .GetValue(17))
        LoadProperty(ReplacementHRIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
      End With

    End Sub

#End Region

  End Class

End Namespace
