﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.RentalCars.ReadOnly

  <Serializable()> _
  Public Class RORentalCarDriverSelect
    Inherits OBReadOnlyBase(Of RORentalCarDriverSelect)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource ID", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource booked on this rental car"), Key()>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceName, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Booking Reason value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="Human Resource")>
    Public ReadOnly Property HumanResourceName() As String
      Get
        Return GetProperty(HumanResourceNameProperty)
      End Get
    End Property

    Public Shared OtherRentalCarsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OtherRentalCars, "Other Rental Cars", "")
    ''' <summary>
    ''' Gets and sets the Other Accommodations value
    ''' </summary>
    <Display(Name:="Other Rental Cars", Description:="")>
    Public ReadOnly Property OtherRentalCars() As String
      Get
        Return GetProperty(OtherRentalCarsProperty)
      End Get
    End Property

    Public Shared OtherRentalCarsShortProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OtherRentalCarsShort, "Other Rental Cars", "")
    ''' <summary>
    ''' Gets and sets the Other Accommodations value
    ''' </summary>
    <Display(Name:="Other Rental Cars", Description:="")>
    Public ReadOnly Property OtherRentalCarsShort() As String
      Get
        Return GetProperty(OtherRentalCarsShortProperty)
      End Get
    End Property

    Public Shared DriverLicenseIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DriverLicenseInd, "Drivers License", False)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="Drivers License", Description:="")>
    Public ReadOnly Property DriverLicenseInd() As Boolean
      Get
        Return GetProperty(DriverLicenseIndProperty)
      End Get
    End Property

    Public Shared HasClashesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasClashes, "Has Clashes", False)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="Has Clashes", Description:="")>
    Public ReadOnly Property HasClashes() As Boolean
      Get
        Return GetProperty(HasClashesProperty)
      End Get
    End Property

    Public Shared RentalCarIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RentalCarID, "RentalCarID", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource booked on this rental car")>
    Public ReadOnly Property RentalCarID() As Integer?
      Get
        Return GetProperty(RentalCarIDProperty)
      End Get
    End Property

    Public Shared RentalCarHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RentalCarHumanResourceID, "Human Resource ID", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource booked on this rental car")>
    Public ReadOnly Property RentalCarHumanResourceID() As Integer?
      Get
        Return GetProperty(RentalCarHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ImageFileNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImageFileName, "ImageFileName", "")
    ''' <summary>
    ''' Gets and sets the Other Accommodations value
    ''' </summary>
    <Display(Name:="ImageFileName", Description:="")>
    Public ReadOnly Property ImageFileName() As String
      Get
        Return GetProperty(ImageFileNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResourceID = 0 Then
        Return String.Format(Me.HumanResourceName)
      Else
        Return Me.HumanResourceID
      End If

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Public Shared Function NewRORentalCarDriverSelect() As RORentalCarDriverSelect

      Return DataPortal.CreateChild(Of RORentalCarDriverSelect)()

    End Function

    Public Sub New()

    End Sub

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORentalCarDriverSelect(dr As SafeDataReader) As RORentalCarDriverSelect

      Dim r As New RORentalCarDriverSelect()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(0)))
        LoadProperty(HumanResourceNameProperty, .GetString(1))
        LoadProperty(OtherRentalCarsProperty, .GetString(2))
        LoadProperty(OtherRentalCarsShortProperty, .GetString(3))
        LoadProperty(DriverLicenseIndProperty, .GetBoolean(4))
        LoadProperty(HasClashesProperty, .GetBoolean(5))
        LoadProperty(RentalCarIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(RentalCarHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(ImageFileNameProperty, .GetString(8))
      End With

    End Sub

#End Region

#End Region

  End Class

End Namespace