﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.RentalCars.ReadOnly

  <Serializable()>
  Public Class RORentalCarCoDriverSelectList
    Inherits OBReadOnlyListBase(Of RORentalCarCoDriverSelectList, RORentalCarCoDriverSelect)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As RORentalCarCoDriverSelect

      For Each child As RORentalCarCoDriverSelect In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Rental Car Human Resources"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property CoDriver As String = ""
      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property RentalCarID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing
      Public Property PickupDateTime As DateTime? = Nothing
      Public Property DropoffDateTime As DateTime? = Nothing

      Public Sub New()

      End Sub

      Public Sub New(TravelRequisitionID As Integer?, RentalCarID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        Me.RentalCarID = RentalCarID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORentalCarCoDriverSelectList() As RORentalCarCoDriverSelectList

      Return New RORentalCarCoDriverSelectList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(RORentalCarCoDriverSelect.GetRORentalCarCoDriverSelect(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORentalCarCoDriverSelectList"
            cm.Parameters.AddWithValue("@CoDriver", Strings.MakeEmptyDBNull(crit.CoDriver))
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@RentalCarID", NothingDBNull(crit.RentalCarID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@PickupDateTime", NothingDBNull(crit.PickupDateTime))
            cm.Parameters.AddWithValue("@DropoffDateTime", NothingDBNull(crit.DropoffDateTime))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace