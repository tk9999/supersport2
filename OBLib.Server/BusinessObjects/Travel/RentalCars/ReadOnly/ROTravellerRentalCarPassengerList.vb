﻿' Generated 15 May 2016 21:10 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.RentalCars.ReadOnly

  <Serializable()> _
  Public Class ROTravellerRentalCarPassengerList
    Inherits OBReadOnlyListBase(Of ROTravellerRentalCarPassengerList, ROTravellerRentalCarPassenger)
    Implements Singular.Paging.IPagedList

#Region " Parent "

    <NotUndoable()> Private mParent As ROTravellerRentalCar
#End Region

#Region " Business Methods "

    Public Function GetItem(RentalCarHumanResourceID As Integer) As ROTravellerRentalCarPassenger

      For Each child As ROTravellerRentalCarPassenger In Me
        If child.RentalCarHumanResourceID = RentalCarHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    Public Shared Function NewROTravellerRentalCarPassengerList() As ROTravellerRentalCarPassengerList

      Return New ROTravellerRentalCarPassengerList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace
