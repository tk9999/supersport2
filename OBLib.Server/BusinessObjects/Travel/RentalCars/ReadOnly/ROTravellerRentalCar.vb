﻿' Generated 15 May 2016 21:10 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.RentalCars.ReadOnly

  <Serializable()> _
  Public Class ROTravellerRentalCar
    Inherits OBReadOnlyBase(Of ROTravellerRentalCar)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RentalCarIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property RentalCarID() As Integer
      Get
        Return GetProperty(RentalCarIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionID, "Travel Requisition")
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="")>
    Public ReadOnly Property TravelRequisitionID() As Integer
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared RentalCarAgentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarAgentID, "Rental Car Agent")
    ''' <summary>
    ''' Gets the Rental Car Agent value
    ''' </summary>
    <Display(Name:="Agent", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Travel.ReadOnly.RORentalCarAgentList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel)>
    Public ReadOnly Property RentalCarAgentID() As Integer
      Get
        Return GetProperty(RentalCarAgentIDProperty)
      End Get
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewTypeID, "Crew Type")
    ''' <summary>
    ''' Gets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="")>
    Public ReadOnly Property CrewTypeID() As Integer
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
    End Property

    Public Shared CarTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CarTypeID, "Car Type")
    ''' <summary>
    ''' Gets the Car Type value
    ''' </summary>
    <Display(Name:="Type", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Travel.ReadOnly.ROCarTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property CarTypeID() As Integer
      Get
        Return GetProperty(CarTypeIDProperty)
      End Get
    End Property

    Public Shared RentalCarRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarRefNo, "Rental Car Ref No")
    ''' <summary>
    ''' Gets the Rental Car Ref No value
    ''' </summary>
    <Display(Name:="Rental Car Ref No", Description:="")>
    Public ReadOnly Property RentalCarRefNo() As String
      Get
        Return GetProperty(RentalCarRefNoProperty)
      End Get
    End Property

    Public Shared AgentBranchIDFromProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AgentBranchIDFrom, "Agent Branch ID From")
    ''' <summary>
    ''' Gets the Agent Branch ID From value
    ''' </summary>
    <Display(Name:="Branch From", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Travel.ReadOnly.RORentalCarAgentBranchList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, ThisFilterMember:="RentalCarAgentID", Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel)>
    Public ReadOnly Property AgentBranchIDFrom() As Integer
      Get
        Return GetProperty(AgentBranchIDFromProperty)
      End Get
    End Property

    Public Shared PickupDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.PickupDateTime, "Pickup Date Time")
    ''' <summary>
    ''' Gets the Pickup Date Time value
    ''' </summary>
    <Display(Name:="PickUp Date", Description:="")>
    Public ReadOnly Property PickupDateTime As DateTime
      Get
        Return GetProperty(PickupDateTimeProperty)
      End Get
    End Property

    Public Shared PickupDateTimeSProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PickupDateTimeS, "Pickup Date Time")
    ''' <summary>
    ''' Gets the Pickup Date Time value
    ''' </summary>
    <Display(Name:="PickUp Date", Description:="")>
    Public ReadOnly Property PickupDateTimeS As String
      Get
        Return GetProperty(PickupDateTimeSProperty)
      End Get
    End Property

    Public Shared AgentBranchIDToProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AgentBranchIDTo, "Agent Branch ID To")
    ''' <summary>
    ''' Gets the Agent Branch ID To value
    ''' </summary>
    <Display(Name:="Branch To", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Travel.ReadOnly.RORentalCarAgentBranchList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, ThisFilterMember:="RentalCarAgentID", Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel)>
    Public ReadOnly Property AgentBranchIDTo() As Integer
      Get
        Return GetProperty(AgentBranchIDToProperty)
      End Get
    End Property

    Public Shared DropoffDateTimeSProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DropoffDateTimeS, "Dropoff Date Time")
    ''' <summary>
    ''' Gets the Dropoff Date Time value
    ''' </summary>
    <Display(Name:="DropOff Date", Description:="")>
    Public ReadOnly Property DropoffDateTimeS As String
      Get
        Return GetProperty(DropoffDateTimeSProperty)
      End Get
    End Property

    Public Shared DropoffDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DropoffDateTime, "Dropoff Date Time")
    ''' <summary>
    ''' Gets the Dropoff Date Time value
    ''' </summary>
    <Display(Name:="DropOff Date", Description:="")>
    Public ReadOnly Property DropoffDateTime As DateTime
      Get
        Return GetProperty(DropoffDateTimeProperty)
      End Get
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="")>
    Public ReadOnly Property CancelledDateTime As Date
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User")
    ''' <summary>
    ''' Gets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="")>
    Public ReadOnly Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="")>
    Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DriverNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Driver, "Driver Name")
    ''' <summary>
    ''' Gets the Driver Name value
    ''' </summary>
    <Display(Name:="Driver", Description:="")>
    Public ReadOnly Property Driver() As String
      Get
        Return GetProperty(DriverNameProperty)
      End Get
    End Property

    Public Shared SeatsRemainingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RemainingSeats, "Remaining Seats")
    ''' <summary>
    ''' Gets the Seats Remaining value
    ''' </summary>
    <Display(Name:="Seats", Description:="")>
    Public ReadOnly Property RemainingSeats() As String
      Get
        Return GetProperty(SeatsRemainingProperty)
      End Get
    End Property

    Public Shared PassengerCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PassengerCount, "Passenger Count")
    ''' <summary>
    ''' Gets the Passenger Count value
    ''' </summary>
    <Display(Name:="Passenger Count", Description:="")>
    Public ReadOnly Property PassengerCount() As Integer
      Get
        Return GetProperty(PassengerCountProperty)
      End Get
    End Property

    Public Shared CancelledIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CancelledInd, "Cancelled Indicator", False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:="Is Flight Cancelled?")>
    Public ReadOnly Property CancelledInd() As Boolean
      Get
        If CancelledByUserID IsNot Nothing Then
          Return True
        End If
        Return False
      End Get
    End Property

    Public Shared Property RentalCarDriverErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarDriverError, "Driver Error", "")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Error", Description:="")>
    Public ReadOnly Property RentalCarDriverError() As String
      Get
        Return GetProperty(RentalCarDriverErrorProperty)
      End Get
    End Property

    Public Shared RentalCarAgentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarAgent, "Agent", "")
    ''' <summary>
    ''' Gets the Agent Branch ID From value
    ''' </summary>
    <Display(Name:="Agent", Description:="")>
    Public ReadOnly Property RentalCarAgent() As String
      Get
        Return GetProperty(RentalCarAgentProperty)
      End Get
    End Property

    Public Shared AgentBranchFromProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AgentBranchFrom, "Branch From", "")
    ''' <summary>
    ''' Gets the Agent Branch ID From value
    ''' </summary>
    <Display(Name:="Branch From", Description:="")>
    Public ReadOnly Property AgentBranchFrom() As String
      Get
        Return GetProperty(AgentBranchFromProperty)
      End Get
    End Property

    Public Shared AgentBranchToProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AgentBranchTo, "Branch To", "")
    ''' <summary>
    ''' Gets the Agent Branch ID To value
    ''' </summary>
    <Display(Name:="Branch To", Description:="")>
    Public ReadOnly Property AgentBranchTo() As String
      Get
        Return GetProperty(AgentBranchToProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROTravellerRentalCarPassengerListProperty As PropertyInfo(Of ROTravellerRentalCarPassengerList) = RegisterProperty(Of ROTravellerRentalCarPassengerList)(Function(c) c.ROTravellerRentalCarPassengerList, "RO Traveller Rental Car Passenger List")

    Public ReadOnly Property ROTravellerRentalCarPassengerList() As ROTravellerRentalCarPassengerList
      Get
        If GetProperty(ROTravellerRentalCarPassengerListProperty) Is Nothing Then
          LoadProperty(ROTravellerRentalCarPassengerListProperty, OBLib.Travel.RentalCars.ReadOnly.ROTravellerRentalCarPassengerList.NewROTravellerRentalCarPassengerList())
        End If
        Return GetProperty(ROTravellerRentalCarPassengerListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.RentalCarRefNo

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerRentalCar(dr As SafeDataReader) As ROTravellerRentalCar

      Dim r As New ROTravellerRentalCar()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RentalCarIDProperty, .GetInt32(0))
        LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(RentalCarAgentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(CarTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(RentalCarRefNoProperty, .GetString(5))
        LoadProperty(AgentBranchIDFromProperty, .GetInt32(6))
        LoadProperty(PickupDateTimeSProperty, .GetString(7))
        LoadProperty(AgentBranchIDToProperty, .GetInt32(8))
        LoadProperty(DropoffDateTimeSProperty, .GetString(9))
        LoadProperty(PickupDateTimeProperty, .GetValue(10))
        LoadProperty(DropoffDateTimeProperty, .GetValue(11))
        LoadProperty(CancelledDateTimeProperty, .GetValue(12))
        LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(CancelledReasonProperty, .GetString(14))
        LoadProperty(CreatedByProperty, .GetInt32(15))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(16))
        LoadProperty(ModifiedByProperty, .GetInt32(17))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(18))
        LoadProperty(DriverNameProperty, .GetString(19))
        LoadProperty(SeatsRemainingProperty, .GetString(20))
        LoadProperty(PassengerCountProperty, .GetInt32(21))
        'RowNo = 22
        LoadProperty(RentalCarAgentProperty, .GetString(23))
        LoadProperty(AgentBranchFromProperty, .GetString(24))
        LoadProperty(AgentBranchToProperty, .GetString(25))
      End With

    End Sub

#End Region

  End Class

End Namespace
