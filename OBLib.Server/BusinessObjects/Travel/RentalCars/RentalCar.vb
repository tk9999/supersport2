﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Travel.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Travel.RentalCars.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.RentalCars

  <Serializable()> _
  Public Class RentalCar
    Inherits OBBusinessBase(Of RentalCar)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RentalCarBO.toString(self)")

    Public Shared RentalCarIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property RentalCarID() As Integer
      Get
        Return GetProperty(RentalCarIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelRequisitionID, "Travel Requisition", Nothing)
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    Public Property TravelRequisitionID() As Integer?
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(TravelRequisitionIDProperty, value)
      End Set
    End Property

    Public Shared RentalCarAgentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RentalCarAgentID, "Agent", OBLib.CommonData.Enums.RentalCarAgents.Bidvest)
    ''' <summary>
    ''' Gets and sets the Rental Car Agent value
    ''' </summary>
    <Display(Name:="Agent", Description:="Rental Car Agent that is being booked"),
    Required(ErrorMessage:="Agent required"),
    DropDownWeb(GetType(RORentalCarAgentList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="RentalCarBO.setRentalCarAgentCriteriaBeforeRefresh",
                PreFindJSFunction:="RentalCarBO.triggerRentalCarAgentAutoPopulate",
                AfterFetchJS:="RentalCarBO.afterRentalCarAgentRefreshAjax",
                LookupMember:="RentalCarAgent", ValueMember:="RentalCarAgentID", DisplayMember:="RentalCarAgent", DropDownColumns:={"RentalCarAgent"},
                OnItemSelectJSFunction:="RentalCarBO.onRentalCarAgentSelected", DropDownCssClass:="room-dropdown"),
    Singular.DataAnnotations.SetExpression("RentalCarBO.RentalCarAgentSet(self)")>
    Public Property RentalCarAgentID() As Integer?
      Get
        Return GetProperty(RentalCarAgentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RentalCarAgentIDProperty, Value)
      End Set
    End Property

    Public Shared RentalCarAgentProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.RentalCarAgent, "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Agent", Description:="Agency being used")>
    Public Property RentalCarAgent() As String
      Get
        Return GetProperty(RentalCarAgentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RentalCarAgentProperty, Value)
      End Set
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "Crew Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="The type of crew this travel is booked for"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCrewTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
                                         FilterMethodName:="RentalCarBO.filterROCrewTypes", DisplayMember:="TravelScreenDisplayName")>
    Public Property CrewTypeID() As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CarTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.CarTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Car Type value
    ''' </summary>
    <Display(Name:="Car Type", Description:="The type of car. Used to determine the number of passengers this car can hold"),
    Required(ErrorMessage:="Car Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCarTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, DropDownColumns:={"CarType", "MaxNoOfOccupants"}),
    SetExpression("RentalCarBO.CarTypeIDSet(self)")>
    Public Property CarTypeID() As Integer?
      Get
        Return GetProperty(CarTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CarTypeIDProperty, Value)
      End Set
    End Property

    Public Shared RentalCarRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarRefNo, "Rental Car Ref No", "")
    ''' <summary>
    ''' Gets and sets the Rental Car Ref No value
    ''' </summary>
    <Display(Name:="Rental Car Ref No", Description:="Ref No for the vehicle, most likely the reference number provided by the agency"),
    StringLength(20, ErrorMessage:="Rental Car Ref No cannot be more than 20 characters")>
    Public Property RentalCarRefNo() As String
      Get
        Return GetProperty(RentalCarRefNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RentalCarRefNoProperty, Value)
      End Set
    End Property

    Public Shared AgentBranchIDFromProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AgentBranchIDFrom, "From Branch", Nothing)
    ''' <summary>
    ''' Gets and sets the Agent Branch ID From value
    ''' </summary>
    <Display(Name:="From Branch", Description:="Agency Branch at which the rental car will be collected"),
     DropDownWeb(GetType(RORentalCarAgentBranchList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="RentalCarBO.setAgentBranchIDFromCriteriaBeforeRefresh",
                 PreFindJSFunction:="RentalCarBO.triggerAgentBranchIDFromAutoPopulate",
                 AfterFetchJS:="RentalCarBO.afterAgentBranchIDFromRefreshAjax",
                 LookupMember:="BranchFrom", ValueMember:="RentalCarAgentBranchID", DisplayMember:="RentalCarAgentBranchName", DropDownColumns:={"RentalCarAgentBranchName"},
                 OnItemSelectJSFunction:="RentalCarBO.onAgentBranchIDFromSelected", DropDownCssClass:="room-dropdown")>
    Public Property AgentBranchIDFrom() As Integer?
      Get
        Return GetProperty(AgentBranchIDFromProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AgentBranchIDFromProperty, Value)
      End Set
    End Property
    'Singular.DataAnnotations.DropDownWeb(GetType(RORentalCarAgentBranchList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
    '                                     DropDownColumns:={"RentalCarAgent", "RentalCarAgentBranch"},
    '                                     DisplayMember:="RentalCarAgentBranchName", ValueMember:="RentalCarAgentBranchID")
    '    Required(ErrorMessage:="Agent Branch ID From required"),

    Public Shared BranchFromProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.BranchFrom, "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="From Branch", Description:="Branch being picked up at")>
    Public Property BranchFrom() As String
      Get
        Return GetProperty(BranchFromProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BranchFromProperty, Value)
      End Set
    End Property

    Public Shared PickupDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PickupDateTime, "Pickup Date")
    ''' <summary>
    ''' Gets and sets the Pickup Date Time value
    ''' </summary>
    <Display(Name:="Pickup Date", Description:="The date and time the rental car is to be collected"),
    Required(ErrorMessage:="Pickup Date required"), Singular.DataAnnotations.DateField(InitialDateFunction:="RentalCarBO.InitDate($data)"),
    SetExpression("RentalCarBO.PickupDateTimeSet(self)")>
    Public Property PickupDateTime As DateTime?
      Get
        Return GetProperty(PickupDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(PickupDateTimeProperty, Value)
      End Set
    End Property

    Public Shared AgentBranchIDToProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AgentBranchIDTo, "To Branch", Nothing)
    ''' <summary>
    ''' Gets and sets the Agent Branch ID To value
    ''' </summary>
    <Display(Name:="To Branch", Description:="The agency branch that the rental car will be dropped off at"),
    DropDownWeb(GetType(RORentalCarAgentBranchList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="RentalCarBO.setAgentBranchIDToCriteriaBeforeRefresh",
                PreFindJSFunction:="RentalCarBO.triggerAgentBranchIDToAutoPopulate",
                AfterFetchJS:="RentalCarBO.afterAgentBranchIDToRefreshAjax",
                LookupMember:="BranchTo", ValueMember:="RentalCarAgentBranchID", DisplayMember:="RentalCarAgentBranchName", DropDownColumns:={"RentalCarAgentBranchName"},
                OnItemSelectJSFunction:="RentalCarBO.onAgentBranchIDToSelected", DropDownCssClass:="room-dropdown")>
    Public Property AgentBranchIDTo() As Integer?
      Get
        Return GetProperty(AgentBranchIDToProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AgentBranchIDToProperty, Value)
      End Set
    End Property
    'Singular.DataAnnotations.DropDownWeb(GetType(RORentalCarAgentBranchList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
    '                                     DropDownColumns:={"RentalCarAgent", "RentalCarAgentBranch"},
    '                                     DisplayMember:="RentalCarAgentBranchName", ValueMember:="RentalCarAgentBranchID")
    '    Required(ErrorMessage:="Agent Branch ID To required"),

    Public Shared BranchToProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.BranchTo, "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="To Branch", Description:="Branch being dropped off at")>
    Public Property BranchTo() As String
      Get
        Return GetProperty(BranchToProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BranchToProperty, Value)
      End Set
    End Property

    Public Shared DropoffDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DropoffDateTime, "Dropoff Date")
    ''' <summary>
    ''' Gets and sets the Dropoff Date Time value
    ''' </summary>
    <Display(Name:="Dropoff Date", Description:="The date and time the rental car is to be dropped off"),
    Required(ErrorMessage:="Dropoff Date required"), Singular.DataAnnotations.DateField(InitialDateFunction:="RentalCarBO.InitDate($data)"),
    SetExpression("RentalCarBO.DropoffDateTimeSet(self)")>
    Public Property DropoffDateTime As DateTime?
      Get
        Return GetProperty(DropoffDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(DropoffDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.CancelledDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="When the booking was cancelled")>
    Public Property CancelledDateTime As DateTime?
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancelledDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="Who cancelled the booking")>
    Public Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CancelledByUserIDProperty, Value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CancelledReason, "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="Why the booking was cancelled"),
    StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters"),
    SetExpression("RentalCarBO.CancelledReasonSet(self)")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
        UpdateCancelledReasonRCHRList(Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Cancelled?", False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:=""),
    SetExpression("RentalCarBO.IsCancelledSet(self)")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
        UpdateCancelledDetails(Value)
        UpdateIsCancelledRCHRList(Value)
      End Set
    End Property

    Public Shared RemainingSeatsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RemainingSeats, "Seats", "")
    ''' <summary>
    ''' Gets and sets the Remaining Seats value
    ''' </summary>
    <Display(Name:="Seats", Description:="Remaining Seats")>
    Public Property RemainingSeats() As String
      Get
        Return GetProperty(RemainingSeatsProperty)
      End Get
      Set(value As String)
        SetProperty(RemainingSeatsProperty, value)
      End Set
    End Property

    Public Shared PassengerCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PassengerCount, "Passengers", Nothing)
    ''' <summary>
    ''' Gets and sets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Passengers", Description:="Passengers"), AlwaysClean>
    Public Property PassengerCount() As Integer
      Get
        Return GetProperty(PassengerCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(PassengerCountProperty, value)
      End Set
    End Property

    Public Shared Property PassengerCountErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PassengerCountError, "Passenger Count Error", "")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Passenger Count Error", Description:="")>
    Public Property PassengerCountError() As String
      Get
        Return GetProperty(PassengerCountErrorProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PassengerCountErrorProperty, Value)
      End Set
    End Property

    'Public Shared Property RentalCarDriverErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarDriverError, "Driver Error", "")
    ' ''' <summary>
    ' ''' Gets and sets the Create By value
    ' ''' </summary>
    '<Display(Name:="Error", Description:="")>
    'Public Property RentalCarDriverError() As String
    '  Get
    '    Return GetProperty(RentalCarDriverErrorProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(RentalCarDriverErrorProperty, Value)
    '  End Set
    'End Property

    Public Shared Property DriverHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DriverHumanResourceID, "Driver", Nothing)
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Driver", Description:=""),
    DropDownWeb(GetType(RORentalCarDriverSelectList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="RentalCarBO.setDriverCriteriaBeforeRefresh",
                 PreFindJSFunction:="RentalCarBO.triggerDriverAutoPopulate",
                 AfterFetchJS:="RentalCarBO.afterDriverRefreshAjax",
                 LookupMember:="Driver", ValueMember:="HumanResourceID", DisplayMember:="HumanResourceName", DropDownColumns:={"HumanResourceName"},
                 OnItemSelectJSFunction:="RentalCarBO.onDriverSelected", DropDownCssClass:="room-dropdown", OnCellCreateFunction:="RentalCarBO.drawDriverCellContent")>
    Public Property DriverHumanResourceID() As Integer?
      Get
        Return GetProperty(DriverHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DriverHumanResourceIDProperty, Value)
      End Set
    End Property
    ', "OtherRentalCarsShort"

    Public Shared Property DriverRentalCarHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DriverRentalCarHumanResourceID, "Driver", Nothing)
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Driver", Description:="")>
    Public Property DriverRentalCarHumanResourceID() As Integer?
      Get
        Return GetProperty(DriverRentalCarHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DriverRentalCarHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared Property DriverProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Driver, "Driver", "")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Driver", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Driver is required")>
    Public Property Driver() As String
      Get
        Return GetProperty(DriverProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DriverProperty, Value)
      End Set
    End Property

    Public Shared HRClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HRClashCount, "HR Clash Count", 0)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="HR Clash Count", Description:="")>
    Public Property HRClashCount() As Integer
      Get
        Return GetProperty(HRClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HRClashCountProperty, Value)
      End Set
    End Property

    Public Shared MaxSeatsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxSeats, "Max Seats", 0)
    ''' <summary>
    ''' Gets and sets the Rental Car Ref No value
    ''' </summary>
    <Display(Name:="Max Seats", Description:="Maximum number of seats for this car type")>
    Public Property MaxSeats() As Integer
      Get
        Return GetProperty(MaxSeatsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MaxSeatsProperty, Value)
      End Set
    End Property

    Public Shared Property CoDriverHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CoDriverHumanResourceID, "Co Driver", Nothing)
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Co Driver", Description:=""),
    DropDownWeb(GetType(RORentalCarCoDriverSelectList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="RentalCarBO.setCoDriverCriteriaBeforeRefresh",
                 PreFindJSFunction:="RentalCarBO.triggerCoDriverAutoPopulate",
                 AfterFetchJS:="RentalCarBO.afterCoDriverRefreshAjax",
                 LookupMember:="CoDriver", ValueMember:="HumanResourceID", DisplayMember:="HumanResourceName", DropDownColumns:={"HumanResourceName"},
                 OnItemSelectJSFunction:="RentalCarBO.onCoDriverSelected", DropDownCssClass:="room-dropdown", OnCellCreateFunction:="RentalCarBO.drawCoDriverCellContent")>
    Public Property CoDriverHumanResourceID() As Integer?
      Get
        Return GetProperty(CoDriverHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CoDriverHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared Property CoDriverProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CoDriver, "Co Driver", "")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Co Driver", Description:="")>
    Public Property CoDriver() As String
      Get
        Return GetProperty(CoDriverProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CoDriverProperty, Value)
      End Set
    End Property
    'Required(AllowEmptyStrings:=False, ErrorMessage:="Co Driver is required")

    Public Shared Property CoDriverRentalCarHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CoDriverRentalCarHumanResourceID, "Co Driver", Nothing)
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Co Driver", Description:="")>
    Public Property CoDriverRentalCarHumanResourceID() As Integer?
      Get
        Return GetProperty(CoDriverRentalCarHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CoDriverRentalCarHumanResourceIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared RentalCarHumanResourceListProperty As PropertyInfo(Of RentalCarHumanResourceList) = RegisterProperty(Of RentalCarHumanResourceList)(Function(c) c.RentalCarHumanResourceList, "Rental Car Human Resource List")

    Public ReadOnly Property RentalCarHumanResourceList() As RentalCarHumanResourceList
      Get
        If GetProperty(RentalCarHumanResourceListProperty) Is Nothing Then
          LoadProperty(RentalCarHumanResourceListProperty, OBLib.Travel.RentalCars.RentalCarHumanResourceList.NewRentalCarHumanResourceList())
        End If
        Return GetProperty(RentalCarHumanResourceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Overrides Sub CanDelete(CallBack As System.EventHandler(Of Singular.CanDeleteArgs))
      MyBase.CanDelete(CallBack)

    End Sub

    Public Function GetParent() As OBLib.Travel.TravelRequisition

      Try
        Return CType(CType(Me.Parent, RentalCarList).Parent, OBLib.Travel.TravelRequisition)
      Catch ex As Exception
        Return Nothing
      End Try

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.RentalCarAgentID = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Rental Car")
        Else
          Return String.Format("Blank {0}", "Rental Car: " & CDate(PickupDateTime).ToString("dd MMM yy HH:mm") & " - " & CDate(DropoffDateTime).ToString("dd MMM yy HH:mm"))
        End If
      Else
        Return Me.RentalCarAgentID
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"RentalCarHumanResources"}
      End Get
    End Property

    Private Sub UpdateCancelledDetails(Value As Boolean)

      If Value = True Then
        SetProperty(CancelledByUserIDProperty, Security.Settings.CurrentUser.UserID)
        SetProperty(CancelledDateTimeProperty, Date.Now)
      Else
        SetProperty(CancelledByUserIDProperty, Nothing)
        SetProperty(CancelledDateTimeProperty, Nothing)
      End If

    End Sub

    Private Sub UpdateIsCancelledRCHRList(Value As Boolean)

      RentalCarHumanResourceList.ToList.ForEach(Sub(z)
                                                  z.IsCancelled = Value
                                                End Sub)

    End Sub

    Private Sub UpdateCancelledReasonRCHRList(Reason As String)

      RentalCarHumanResourceList.ToList.ForEach(Sub(z)
                                                  z.CancelledReason = Reason
                                                End Sub)

    End Sub

    Public Sub SetupRemainingSeats(MaxNoOfOccupants As Integer)

      RemainingSeats = "(" & MaxNoOfOccupants - Me.RentalCarHumanResourceList.Where(Function(d) d.CancelledByUserID Is Nothing).Count & " remain)"

    End Sub

    Public Sub SetPassengerCount()

      If Me.RentalCarHumanResourceList.Count > 0 Then
        LoadProperty(PassengerCountProperty, Me.RentalCarHumanResourceList.Where(Function(d) d.CancelledByUserID Is Nothing _
                                                                                 AndAlso d.RentalCarHumanResourceID <> 0).Count)
      End If

    End Sub

    Private Shared Function CreateRentalCarClashesTableParameter(RentalCarHumanResourceList As RentalCarHumanResourceList) As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("TravelRequisitionID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("RentalCarID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("RentalCarHumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("RentalStartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("RentalEndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IsCancelled", GetType(System.Boolean)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As RentalCarHumanResource In RentalCarHumanResourceList
        Dim row As DataRow = RBTable.NewRow
        row("ObjectGuid") = rb.Guid
        row("TravelRequisitionID") = rb.GetParent.TravelRequisitionID
        row("RentalCarID") = NothingDBNull(rb.RentalCarID)
        row("RentalCarHumanResourceID") = ZeroDBNull(rb.RentalCarHumanResourceID)
        row("HumanResourceID") = rb.HumanResourceID
        row("RentalStartDateTime") = rb.GetParent.PickupDateTime
        row("RentalEndDateTime") = rb.GetParent.DropoffDateTime
        row("IsCancelled") = rb.IsCancelled
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

    Private Function GetRentalCarHRClashesDataSet() As DataSet

      Dim RBTable As DataTable = CreateRentalCarClashesTableParameter(Me.RentalCarHumanResourceList)
      Dim cmd As New Singular.CommandProc("GetProcsWeb.getRORentalCarClashList")
      Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      ResourceBookingsParam.Name = "@RentalCarHumanResources"
      ResourceBookingsParam.SqlType = SqlDbType.Structured
      ResourceBookingsParam.Value = RBTable
      cmd.Parameters.Add(ResourceBookingsParam)
      cmd.Parameters.AddWithValue("@PassengersLoaded", (Me.RentalCarHumanResourceList.Count > 0))
      cmd.Parameters.AddWithValue("@PickupDateTime", NothingDBNull(Me.PickupDateTime))
      cmd.Parameters.AddWithValue("@DropoffDateTime", NothingDBNull(Me.DropoffDateTime))
      cmd.Parameters.AddWithValue("@IsCancelled", Me.IsCancelled)
      cmd.Parameters.AddWithValue("@RentalCarID", ZeroDBNull(Me.RentalCarID))
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute(0)
      Return cmd.Dataset

    End Function

    Public Function UpdateClashes() As Singular.Web.Result

      Try
        'get clash data
        Dim ds As DataSet = GetRentalCarHRClashesDataSet()

        'reset clashes
        Me.HRClashCount = 0
        Me.RentalCarHumanResourceList.ToList.ForEach(Sub(c As RentalCarHumanResource)
                                                       c.HasClashes = False
                                                       c.OtherRentalCarsShort = ""
                                                       c.OtherRentalCars = ""
                                                     End Sub)

        'update rentalcar humanresources
        If Me.RentalCarHumanResourceList.Count > 0 Then
          For Each dr As DataRow In ds.Tables(0).Rows
            Dim rb As RentalCarHumanResource = Me.RentalCarHumanResourceList.Where(Function(d) d.Guid = dr(0)).FirstOrDefault
            If rb IsNot Nothing Then
              rb.HasClashes = True
              rb.OtherRentalCarsShort = dr(1)
              rb.OtherRentalCars = dr(2)
            End If
            If rb.OnRentalCarInd Then
              Me.HRClashCount += 1
            End If
          Next
        Else
          Me.HRClashCount = ds.Tables(0).Rows.Count
        End If

        Return New Singular.Web.Result(True)

      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

    End Function


#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(PickupDateTimeProperty)
        .AddTriggerProperty(DropoffDateTimeProperty)
        .JavascriptRuleFunctionName = "RentalCarBO.TravelDatesValid"
        .ServerRuleFunction = AddressOf TravelDatesValid
      End With

      With AddWebRule(RentalCarAgentIDProperty)
        .AddTriggerProperty(PickupDateTimeProperty)
        .AddTriggerProperty(DropoffDateTimeProperty)
        .AddTriggerProperty(AgentBranchIDFromProperty)
        .AddTriggerProperty(AgentBranchIDToProperty)
        .JavascriptRuleFunctionName = "RentalCarBO.RentalCarAgentValid"
        .ServerRuleFunction = AddressOf RentalCarAgentValid
      End With

      With AddWebRule(AgentBranchIDFromProperty)
        .AddTriggerProperty(PickupDateTimeProperty)
        .AddTriggerProperty(DropoffDateTimeProperty)
        .JavascriptRuleFunctionName = "RentalCarBO.AgentBranchIDFromValid"
        .ServerRuleFunction = AddressOf AgentBranchIDFromValid
      End With

      With AddWebRule(AgentBranchIDToProperty)
        .AddTriggerProperty(PickupDateTimeProperty)
        .AddTriggerProperty(DropoffDateTimeProperty)
        .JavascriptRuleFunctionName = "RentalCarBO.AgentBranchIDToValid"
        .ServerRuleFunction = AddressOf AgentBranchIDToValid
      End With

      With AddWebRule(CancelledReasonProperty)
        .JavascriptRuleFunctionName = "RentalCarBO.CancelledReasonRequired"
        .ServerRuleFunction = AddressOf CancelledReasonRequired
      End With

      'taken care of in data annotation
      'With AddWebRule(DriverProperty)
      '  .JavascriptRuleFunctionName = "RentalCarBO.DriverValid"
      '  .ServerRuleFunction = AddressOf PrimaryDriverValid
      'End With

      With AddWebRule(HRClashCountProperty)
        .JavascriptRuleFunctionName = "RentalCarBO.HRClashCountValid"
        .ServerRuleFunction = AddressOf HRClashCountValid
      End With

      With AddWebRule(CrewTypeIDProperty)
        .JavascriptRuleFunctionName = "RentalCarBO.CrewTypeIDValid"
        .ServerRuleFunction = AddressOf CrewTypeIDValid
      End With

      'With AddWebRule(RentalCarHumanResourceListProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.RentalCarBO.DriverValid
      'End With

      'With AddWebRule(RentalCarIDProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.RentalCarBO.DriverValid
      'End With

      'With AddWebRule(RentalCarDriverErrorProperty,
      '           Function(d) d.RentalCarDriverError <> "",
      '           Function(d) d.RentalCarDriverError)
      'End With

    End Sub

    Public Function HRClashCountValid(RentalCar As RentalCar) As String

      Dim ErrorMessage As String = ""
      If RentalCar.HRClashCount > 0 Then
        ErrorMessage = "There are passenger clashes on this rental car"
      End If

      Return ErrorMessage

    End Function

    Public Function PrimaryDriverValid(RentalCar As RentalCar) As String

      Dim ErrorMessage As String = ""
      If RentalCarHumanResourceList.Count > 0 Then
        If RentalCarHumanResourceList.Where(Function(c) c.DriverInd).Count = 0 Then
          ErrorMessage = "Driver Required"
        End If
      End If

      Return ErrorMessage

    End Function

    Public Shared Function CancelledReasonRequired(RentalCar As RentalCar) As String
      Dim ErrorString = ""
      If RentalCar.IsCancelled AndAlso RentalCar.CancelledReason.Trim = "" Then
        ErrorString = "Cancelled Reason Required"
      End If
      Return ErrorString
    End Function

    Public Shared Function TravelDatesValid(RentalCar As RentalCar) As String
      Dim ErrorString = ""
      If RentalCar.PickupDateTime IsNot Nothing AndAlso RentalCar.DropoffDateTime IsNot Nothing Then
        Dim CheckInDate = RentalCar.PickupDateTime
        Dim CheckOutDate = RentalCar.DropoffDateTime
        Dim result As Integer = DateTime.Compare(CheckInDate, CheckOutDate)
        If result >= 0 Then
          ErrorString = "Pickup Date must be before Dropoff Date"
        End If
      End If
      Return ErrorString
    End Function

    Public Shared Function RentalCarAgentValid(RentalCar As RentalCar) As String
      If RentalCar.RentalCarAgentID IsNot Nothing Then
        Dim RORentalCarAgent As RORentalCarAgent = OBLib.CommonData.Lists.RORentalCarAgentList.GetItem(RentalCar.RentalCarAgentID)
        If RORentalCarAgent.OldInd Then
          If RentalCar.PickupDateTime IsNot Nothing AndAlso RentalCar.PickupDateTime > RORentalCarAgent.OldDate Then
            Return "This branch can no longer be selected: " & RORentalCarAgent.OldReason
          End If
        End If
      Else
        Return "Rental Car Agent is required"
      End If
      Return ""
    End Function

    Public Shared Function AgentBranchIDFromValid(RentalCar As RentalCar) As String
      If RentalCar.AgentBranchIDFrom IsNot Nothing Then
        Dim RORentalCarAgentBranch As RORentalCarAgentBranch = OBLib.CommonData.Lists.RORentalCarAgentBranchList.GetItem(RentalCar.AgentBranchIDFrom)
        If RORentalCarAgentBranch.OldInd Then
          If RentalCar.PickupDateTime IsNot Nothing AndAlso RentalCar.PickupDateTime > RORentalCarAgentBranch.OldDate Then
            Return "This branch can no longer be selected: " & RORentalCarAgentBranch.OldReason
            'Else
            '  Return "This branch can no longer be selected: " & RORentalCarAgentBranch.OldReason
          End If
        End If
      Else
        Return "Branch From is required"
      End If
      Return ""
    End Function

    Public Shared Function AgentBranchIDToValid(RentalCar As RentalCar) As String
      If RentalCar.AgentBranchIDTo IsNot Nothing Then
        Dim RORentalCarAgentBranch As RORentalCarAgentBranch = OBLib.CommonData.Lists.RORentalCarAgentBranchList.GetItem(RentalCar.AgentBranchIDTo)
        If RORentalCarAgentBranch.OldInd Then
          If RentalCar.PickupDateTime IsNot Nothing AndAlso RentalCar.PickupDateTime > RORentalCarAgentBranch.OldDate Then
            Return "This branch can no longer be selected: " & RORentalCarAgentBranch.OldReason
            'Else
            '  Return "This branch can no longer be selected: " & RORentalCarAgentBranch.OldReason
          End If
        End If
      Else
        Return "Branch To is required"
      End If
      Return ""
    End Function

    Public Shared Function CrewTypeIDValid(RentalCar As RentalCar) As String
      If RentalCar.CrewTypeID Is Nothing AndAlso RentalCar.Parent IsNot Nothing AndAlso RentalCar.GetParent IsNot Nothing AndAlso CompareSafe(RentalCar.GetParent.SystemID, CType(OBLib.CommonData.Enums.System.ProductionServices, Integer)) Then
        Return "Crew Type is required"
      End If
      Return ""
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRentalCar() method.

    End Sub

    Public Shared Function NewRentalCar() As RentalCar

      Return DataPortal.CreateChild(Of RentalCar)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRentalCar(dr As SafeDataReader) As RentalCar

      Dim r As New RentalCar()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RentalCarIDProperty, .GetInt32(0))
          LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(RentalCarAgentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CarTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(RentalCarRefNoProperty, .GetString(5))
          LoadProperty(AgentBranchIDFromProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(PickupDateTimeProperty, .GetValue(7))
          LoadProperty(AgentBranchIDToProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(DropoffDateTimeProperty, .GetValue(9))
          LoadProperty(CancelledDateTimeProperty, .GetValue(10))
          LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(CancelledReasonProperty, .GetString(12))
          LoadProperty(CreatedByProperty, .GetInt32(13))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(14))
          LoadProperty(ModifiedByProperty, .GetInt32(15))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(16))
          LoadProperty(DriverProperty, .GetString(17))
          LoadProperty(RemainingSeatsProperty, .GetString(18))
          LoadProperty(BranchFromProperty, .GetString(19))
          LoadProperty(BranchToProperty, .GetString(20))
          LoadProperty(PassengerCountProperty, .GetInt32(21))
          LoadProperty(IsCancelledProperty, IIf(CancelledByUserID Is Nothing, False, True))
          LoadProperty(HRClashCountProperty, .GetInt32(22))
          LoadProperty(DriverRentalCarHumanResourceIDProperty, NothingDBNull(sdr.GetInt32(23)))
          LoadProperty(MaxSeatsProperty, .GetInt32(24))
          LoadProperty(CoDriverRentalCarHumanResourceIDProperty, NothingDBNull(sdr.GetInt32(25)))
          LoadProperty(CoDriverProperty, .GetString(26))
          LoadProperty(RentalCarAgentProperty, .GetString(27))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insRentalCar"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updRentalCar"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      Dim WasNew = Me.IsNew

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramRentalCarID As SqlParameter = .Parameters.Add("@RentalCarID", SqlDbType.Int)
          paramRentalCarID.Value = GetProperty(RentalCarIDProperty)
          If Me.IsNew Then
            paramRentalCarID.Direction = ParameterDirection.Output
          End If

          If Me.GetParent IsNot Nothing Then
            SetProperty(TravelRequisitionIDProperty, Me.GetParent.TravelRequisitionID)
            .Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
          Else
            .Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(GetProperty(TravelRequisitionIDProperty)))
          End If

          Dim paramDriverRentalCarHumanResourceID As SqlParameter = .Parameters.Add("@DriverRentalCarHumanResourceID", SqlDbType.Int)
          paramDriverRentalCarHumanResourceID.Value = Singular.Misc.NothingDBNull(GetProperty(DriverRentalCarHumanResourceIDProperty))
          paramDriverRentalCarHumanResourceID.Direction = ParameterDirection.InputOutput

          Dim paramCoDriverRentalCarHumanResourceID As SqlParameter = .Parameters.Add("@CoDriverRentalCarHumanResourceID", SqlDbType.Int)
          paramCoDriverRentalCarHumanResourceID.Value = Singular.Misc.NothingDBNull(GetProperty(CoDriverRentalCarHumanResourceIDProperty))
          paramCoDriverRentalCarHumanResourceID.Direction = ParameterDirection.InputOutput

          .Parameters.AddWithValue("@RentalCarAgentID", GetProperty(RentalCarAgentIDProperty))
          .Parameters.AddWithValue("@CrewTypeID", Singular.Misc.NothingDBNull(GetProperty(CrewTypeIDProperty)))
          .Parameters.AddWithValue("@CarTypeID", GetProperty(CarTypeIDProperty))
          .Parameters.AddWithValue("@RentalCarRefNo", GetProperty(RentalCarRefNoProperty))
          .Parameters.AddWithValue("@AgentBranchIDFrom", GetProperty(AgentBranchIDFromProperty))
          .Parameters.AddWithValue("@PickupDateTime", (New SmartDate(GetProperty(PickupDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@AgentBranchIDTo", GetProperty(AgentBranchIDToProperty))
          .Parameters.AddWithValue("@DropoffDateTime", (New SmartDate(GetProperty(DropoffDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledDateTime", (New SmartDate(GetProperty(CancelledDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledByUserID", Singular.Misc.NothingDBNull(GetProperty(CancelledByUserIDProperty)))
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@ModifiedBy", ZeroDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@DriverHumanResourceID", Singular.Misc.NothingDBNull(GetProperty(DriverHumanResourceIDProperty)))
          .Parameters.AddWithValue("@Driver", GetProperty(DriverProperty))
          .Parameters.AddWithValue("@CoDriverHumanResourceID", Singular.Misc.NothingDBNull(GetProperty(CoDriverHumanResourceIDProperty)))
          .Parameters.AddWithValue("@CoDriver", GetProperty(DriverProperty))
          .Parameters.AddWithValue("@PassengersLoaded", (Me.RentalCarHumanResourceList.Count > 0))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(RentalCarIDProperty, paramRentalCarID.Value)
          End If

          ' update child objects
          If GetProperty(RentalCarHumanResourceListProperty) IsNot Nothing Then
            Me.RentalCarHumanResourceList.Update()
          End If
          SetPassengerCount()
          MarkOld()
          If WasNew Then
            Me.SetDriverProps()
          End If
        End With
      Else
        ' update child objects
        If GetProperty(RentalCarHumanResourceListProperty) IsNot Nothing Then
          Me.RentalCarHumanResourceList.Update()
        End If
        SetPassengerCount()
        If WasNew Then
          Me.SetDriverProps()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delRentalCar"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@RentalCarID", GetProperty(RentalCarIDProperty))
        cm.Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
        cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Sub SetDriverProps()

      Dim driver As RentalCarHumanResource = Me.RentalCarHumanResourceList.Where(Function(d) d.DriverInd).FirstOrDefault
      If driver IsNot Nothing Then
        LoadProperty(DriverRentalCarHumanResourceIDProperty, driver.RentalCarHumanResourceID)
        LoadProperty(DriverHumanResourceIDProperty, driver.HumanResourceID)
        LoadProperty(DriverProperty, driver.HumanResourceName)
      End If

    End Sub

  End Class

End Namespace