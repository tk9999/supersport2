﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.RentalCars

  <Serializable()> _
  Public Class RentalCarList
    Inherits OBBusinessListBase(Of RentalCarList, RentalCar)

#Region " Business Methods "

    Public Function GetItem(RentalCarID As Integer) As RentalCar

      For Each child As RentalCar In Me
        If child.RentalCarID = RentalCarID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Rental Cars"

    End Function

    Public Function GetRentalCarHumanResource(RentalCarHumanResourceID As Integer) As RentalCarHumanResource

      Dim obj As RentalCarHumanResource = Nothing
      For Each parent As RentalCar In Me
        obj = parent.RentalCarHumanResourceList.GetItem(RentalCarHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Sub UpdateTravelRequisitionID(TravelRequisitionID As Integer)

      Me.ToList.ForEach(Sub(z)
                          If z.IsNew Then
                            z.TravelRequisitionID = TravelRequisitionID
                          End If
                        End Sub)

    End Sub

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property RentalCarID As Integer? = Nothing

      Public Sub New(TravelRequisitionID As Integer?, RentalCarID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        Me.RentalCarID = RentalCarID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRentalCarList() As RentalCarList

      Return New RentalCarList()

    End Function

    Public Shared Sub BeginGetRentalCarList(CallBack As EventHandler(Of DataPortalResult(Of RentalCarList)))

      Dim dp As New DataPortal(Of RentalCarList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRentalCarList() As RentalCarList

      Return DataPortal.Fetch(Of RentalCarList)(New Criteria())

    End Function

    Public Shared Function GetRentalCarList(TravelRequisitionID As Integer?, RentalCarID As Integer?) As RentalCarList

      Return DataPortal.Fetch(Of RentalCarList)(New Criteria(TravelRequisitionID, RentalCarID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(RentalCar.GetRentalCar(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As RentalCar = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.RentalCarID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.RentalCarHumanResourceList.RaiseListChangedEvents = False
          parent.RentalCarHumanResourceList.Add(RentalCarHumanResource.GetRentalCarHumanResource(sdr))
          parent.RentalCarHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As RentalCar In Me
        child.CheckRules()
        child.SetDriverProps()
        For Each rchr As RentalCarHumanResource In child.RentalCarHumanResourceList
          rchr.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRentalCarList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@RentalCarID", NothingDBNull(crit.RentalCarID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        If Me.DeletedList.Count > 0 Then
          DeleteBulk()
          ' Then clear the list of deleted objects because they are truly gone now.
          DeletedList.Clear()
        End If

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As RentalCar In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

    Private Sub DeleteBulk()

      Dim cm As New Singular.CommandProc("DelProcsWeb.delRentalCarListBulk")
      cm.Parameters.AddWithValue("@RentalCarIDs", Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(Me.DeletedList.Select(Function(d) d.RentalCarID).ToList)))
      cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      cm.UseTransaction = True
      cm.Execute()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace