﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Travel.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.Flights

  <Serializable()>
  Public Class Flight
    Inherits OBBusinessBase(Of Flight)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return FlightBO.toString(self)")

    Public Shared FlightIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property FlightID() As Integer
      Get
        Return GetProperty(FlightIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelRequisitionID, "Travel Requisition", Nothing)
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    Public Property TravelRequisitionID() As Integer?
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(TravelRequisitionIDProperty, value)
      End Set
    End Property

    Public Shared FlightNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlightNo, "Flight No", "")
    ''' <summary>
    ''' Gets and sets the Flight No value
    ''' </summary>
    <Display(Name:="Flight No", Description:="Flight Number of the flight"),
    StringLength(10, ErrorMessage:="Flight No cannot be more than 10 characters")>
    Public Property FlightNo() As String
      Get
        Return GetProperty(FlightNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FlightNoProperty, Value)
      End Set
    End Property

    Public Shared FlightTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FlightTypeID, "Flight Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Flight Type value
    ''' </summary>
    <Display(Name:="Flight Type", Description:="Type of flight"),
    Required(ErrorMessage:="Flight Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROFlightTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property FlightTypeID() As Integer?
      Get
        Return GetProperty(FlightTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FlightTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "Crew Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="Type of crew"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCrewTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
                                         FilterMethodName:="FlightBO.filterROCrewTypes", DisplayMember:="TravelScreenDisplayName")>
    Public Property CrewTypeID() As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTypeIDProperty, Value)
      End Set
    End Property

    Public Shared TravelDirectionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelDirectionID, "Travel Direction", Nothing)
    ''' <summary>
    ''' Gets and sets the Travel Direction value
    ''' </summary>
    <Display(Name:="Travel Direction", Description:="Travel direction of the flight"),
    Required(ErrorMessage:="Travel Direction required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROTravelDirectionList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property TravelDirectionID() As Integer?
      Get
        Return GetProperty(TravelDirectionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TravelDirectionIDProperty, Value)
      End Set
    End Property

    Public Shared AirportIDFromProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AirportIDFrom, "Airport ID From", Nothing)
    ''' <summary>
    ''' Gets and sets the Airport ID From value
    ''' </summary>
    <Display(Name:="From Airport", Description:="Departure Airport"),
    Required(ErrorMessage:="Airport From required"),
    DropDownWeb(GetType(ROAirportList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="FlightBO.setAirportIDFromCriteriaBeforeRefresh",
                PreFindJSFunction:="FlightBO.triggerAirportIDFromAutoPopulate",
                AfterFetchJS:="FlightBO.afterAirportIDFromRefreshAjax",
                LookupMember:="AirportFrom", ValueMember:="AirportID", DisplayMember:="Airport", DropDownColumns:={"Airport"},
                OnItemSelectJSFunction:="FlightBO.onAirportIDFromSelected", DropDownCssClass:="room-dropdown")>
    Public Property AirportIDFrom() As Integer?
      Get
        Return GetProperty(AirportIDFromProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AirportIDFromProperty, Value)
      End Set
    End Property
    'Singular.DataAnnotations.DropDownWeb(GetType(ROAirportList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, FilterMethodName:="TravelVM.SaAirports"),

    Public Shared AirportFromProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.AirportFrom, "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="From", Description:="Airport departing from")>
    Public Property AirportFrom() As String
      Get
        Return GetProperty(AirportFromProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AirportFromProperty, Value)
      End Set
    End Property

    Public Shared DepartureDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.DepartureDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Departure Date Time value
    ''' </summary>
    <Display(Name:="Departure Date", Description:="Date and time of departure"),
    Required(ErrorMessage:="Departure Date Time required"), Singular.DataAnnotations.DateField(InitialDateFunction:="FlightBO.InitDate($data)"),
    SetExpression("FlightBO.DepartureDateTimeSet(self)")>
    Public Property DepartureDateTime As DateTime?
      Get
        Return GetProperty(DepartureDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(DepartureDateTimeProperty, Value)
      End Set
    End Property
    '.Add

    Public Shared AirportIDToProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AirportIDTo, "Airport To", Nothing)
    ''' <summary>
    ''' Gets and sets the Airport ID To value
    ''' </summary>
    <Display(Name:="To Airport", Description:="Arrival airport"),
    Required(ErrorMessage:="Airport To required"),
    DropDownWeb(GetType(ROAirportList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="FlightBO.setAirportIDToCriteriaBeforeRefresh",
                PreFindJSFunction:="FlightBO.triggerAirportIDToAutoPopulate",
                AfterFetchJS:="FlightBO.afterAirportIDToRefreshAjax",
                LookupMember:="AirportTo", ValueMember:="AirportID", DisplayMember:="Airport", DropDownColumns:={"Airport"},
                OnItemSelectJSFunction:="FlightBO.onAirportIDToSelected", DropDownCssClass:="room-dropdown")>
    Public Property AirportIDTo() As Integer?
      Get
        Return GetProperty(AirportIDToProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AirportIDToProperty, Value)
      End Set
    End Property
    'Singular.DataAnnotations.DropDownWeb(GetType(ROAirportList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, FilterMethodName:="TravelVM.SaAirports"),

    Public Shared AirportToProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.AirportTo, "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="To", Description:="Airport arriving at")>
    Public Property AirportTo() As String
      Get
        Return GetProperty(AirportToProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AirportToProperty, Value)
      End Set
    End Property

    Public Shared ArrivalDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ArrivalDateTime, "Arrival Date")
    ''' <summary>
    ''' Gets and sets the Arrival Date Time value
    ''' </summary>
    <Display(Name:="Arrival Date", Description:="Date and time of arrival"),
    Required(ErrorMessage:="Arrival Date Time required"), Singular.DataAnnotations.DateField(InitialDateFunction:="FlightBO.InitDate($data)"),
    SetExpression("FlightBO.ArrivalDateTimeSet(self)")>
    Public Property ArrivalDateTime As DateTime?
      Get
        Return GetProperty(ArrivalDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ArrivalDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets and sets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="Date and time of cancellation")>
    Public Property CancelledDateTime As DateTime?
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancelledDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="User who cancelled the flight")>
    Public Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CancelledByUserIDProperty, Value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CancelledReason, "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancel Reason", Description:="Reason for cancelling the flight"),
    StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters"),
    SetExpression("FlightBO.CancelledReasonSet(self)")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
        'UpdateCancelledReasonFlightHRList(Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsCancelled, False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:="Is Flight Cancelled?"),
    SetExpression("FlightBO.IsCancelledSet(self)")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
        'UpdateCancelledDetails(Value)
        'UpdateCancelledIndFlightHRList(Value)
      End Set
    End Property

    Public Shared PassengerCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PassengerCount, "Passengers", Nothing)
    ''' <summary>
    ''' Gets and sets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Passengers", Description:="Passengers"), AlwaysClean>
    Public Property PassengerCount() As Integer
      Get
        Return GetProperty(PassengerCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(PassengerCountProperty, value)
      End Set
    End Property

    Public Shared HRClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HRClashCount, "HR Clash Count", 0)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="HR Clash Count", Description:="")>
    Public Property HRClashCount() As Integer
      Get
        Return GetProperty(HRClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HRClashCountProperty, Value)
      End Set
    End Property

    Public Shared CountryIDFromProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryIDFrom, "Country From", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Country From", Description:="")>
    Public Property CountryIDFrom() As Integer?
      Get
        Return GetProperty(CountryIDFromProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CountryIDFromProperty, Value)
      End Set
    End Property

    Public Shared CountryIDToProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryIDTo, "Country To", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Country To", Description:="")>
    Public Property CountryIDTo() As Integer?
      Get
        Return GetProperty(CountryIDToProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CountryIDToProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared FlightHumanResourceListProperty As PropertyInfo(Of FlightHumanResourceList) = RegisterProperty(Of FlightHumanResourceList)(Function(c) c.FlightHumanResourceList, "Flight Human Resource List")

    Public ReadOnly Property FlightHumanResourceList() As FlightHumanResourceList
      Get
        If GetProperty(FlightHumanResourceListProperty) Is Nothing Then
          LoadProperty(FlightHumanResourceListProperty, OBLib.Travel.Flights.FlightHumanResourceList.NewFlightHumanResourceList())
        End If
        Return GetProperty(FlightHumanResourceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As OBLib.Travel.TravelRequisition

      Try
        Return CType(CType(Me.Parent, FlightList).Parent, OBLib.Travel.TravelRequisition)
      Catch ex As Exception
        Return Nothing
      End Try

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FlightIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AirportIDFrom = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Flight")
        Else
          Return String.Format("Blank {0}", "Flight")
        End If
      Else
        Return Me.AirportIDFrom
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"FlightHumanResources"}
      End Get
    End Property

    Private Sub UpdateCancelledDetails(Value As Boolean)

      If Value = True Then
        SetProperty(CancelledByUserIDProperty, Security.Settings.CurrentUser.UserID)
        SetProperty(CancelledDateTimeProperty, Date.Now)
      Else
        SetProperty(CancelledByUserIDProperty, Nothing)
        SetProperty(CancelledDateTimeProperty, Nothing)
      End If

    End Sub

    Private Sub UpdateCancelledIndFlightHRList(Value As Boolean)

      FlightHumanResourceList.ToList.ForEach(Sub(z)
                                               z.IsCancelled = Value
                                             End Sub)

    End Sub

    Private Sub UpdateCancelledReasonFlightHRList(Reason As String)

      FlightHumanResourceList.ToList.ForEach(Sub(z)
                                               z.CancelledReason = Reason
                                             End Sub)

    End Sub

    Public Sub SetPassengerCount()

      If Me.FlightHumanResourceList.Count > 0 Then
        LoadProperty(PassengerCountProperty, Me.FlightHumanResourceList.Where(Function(d) d.CancelledByUserID Is Nothing _
                                                                                          AndAlso d.FlightHumanResourceID <> 0).Count)
      End If

    End Sub

    Sub CleanPassengerList()

      LoadProperty(FlightHumanResourceListProperty, New OBLib.Travel.Flights.FlightHumanResourceList)

    End Sub

    Private Shared Function CreateFlightClashesTableParameter(FlightHumanResourceList As FlightHumanResourceList) As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("TravelRequisitionID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("FlightID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("FlightHumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("DepartureDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ArrivalDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IsCancelled", GetType(System.Boolean)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As FlightHumanResource In FlightHumanResourceList
        Dim row As DataRow = RBTable.NewRow
        row("ObjectGuid") = rb.Guid
        row("TravelRequisitionID") = rb.GetParent.TravelRequisitionID
        row("FlightID") = NothingDBNull(rb.FlightID)
        row("FlightHumanResourceID") = rb.FlightHumanResourceID
        row("HumanResourceID") = rb.HumanResourceID
        row("DepartureDateTime") = rb.GetParent.DepartureDateTime
        row("ArrivalDateTime") = rb.GetParent.ArrivalDateTime
        row("IsCancelled") = rb.IsCancelled
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

    Private Function GetFlightClashesDataSet() As DataSet

      Dim RBTable As DataTable = CreateFlightClashesTableParameter(Me.FlightHumanResourceList)
      Dim cmd As New Singular.CommandProc("GetProcsWeb.getROFlightClashList")
      Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      ResourceBookingsParam.Name = "@FlightHumanResources"
      ResourceBookingsParam.SqlType = SqlDbType.Structured
      ResourceBookingsParam.Value = RBTable
      cmd.Parameters.Add(ResourceBookingsParam)
      cmd.Parameters.AddWithValue("@PassengersLoaded", (Me.FlightHumanResourceList.Count > 0))
      cmd.Parameters.AddWithValue("@DepartureDateTime", NothingDBNull(Me.DepartureDateTime))
      cmd.Parameters.AddWithValue("@ArrivalDateTime", NothingDBNull(Me.ArrivalDateTime))
      cmd.Parameters.AddWithValue("@IsCancelled", Me.IsCancelled)
      cmd.Parameters.AddWithValue("@FlightID", ZeroDBNull(Me.FlightID))
      '@DepartureDateTime Datetime,
      '@ArrivalDateTime DateTime, 
      '@IsCancelled Bit,
      '@FlightID Int
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute(0)
      Return cmd.Dataset

    End Function

    Public Function UpdateClashes() As Singular.Web.Result

      Try
        'get clash data
        Dim ds As DataSet = GetFlightClashesDataSet()

        'reset clashes
        Me.HRClashCount = 0
        Me.FlightHumanResourceList.ToList.ForEach(Sub(c As FlightHumanResource)
                                                    c.HasClashes = False
                                                    c.OtherFlightsShort = ""
                                                    c.OtherFlights = ""
                                                  End Sub)

        'update flight humanresources
        If Me.FlightHumanResourceList.Count > 0 Then
          For Each dr As DataRow In ds.Tables(0).Rows
            Dim rb As FlightHumanResource = Me.FlightHumanResourceList.Where(Function(d) d.Guid = dr(0)).FirstOrDefault
            If rb IsNot Nothing Then
              rb.HasClashes = True
              rb.OtherFlightsShort = dr(1)
              rb.OtherFlights = dr(2)
            End If
            If rb.IsOnFlight Then
              Me.HRClashCount += 1
            End If
          Next
        Else
          Me.HRClashCount = ds.Tables(0).Rows.Count
        End If

        'SetProperty(PassengerCountProperty, Me.FlightHumanResourceList.Count)

        Return New Singular.Web.Result(True)

      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(AirportIDFromProperty)
        .AddTriggerProperty(AirportIDToProperty)
        .AffectedProperties.Add(AirportIDToProperty)
        .JavascriptRuleFunctionName = "FlightBO.AirportsValid"
        .ServerRuleFunction = AddressOf AirportsValid
      End With

      With AddWebRule(DepartureDateTimeProperty)
        .AddTriggerProperty(ArrivalDateTimeProperty)
        .AffectedProperties.Add(ArrivalDateTimeProperty)
        .JavascriptRuleFunctionName = "FlightBO.TravelDatesValid"
        .ServerRuleFunction = AddressOf TravelDatesValid
      End With

      With AddWebRule(IsCancelledProperty)
        .JavascriptRuleFunctionName = "FlightBO.CancelledReasonValid"
        .ServerRuleFunction = AddressOf CancelledReasonValid
        .AddTriggerProperty(CancelledReasonProperty)
        .AffectedProperties.Add(CancelledReasonProperty)
      End With

      With AddWebRule(HRClashCountProperty)
        .JavascriptRuleFunctionName = "FlightBO.HRClashCountValid"
        .ServerRuleFunction = AddressOf HRClashCountValid
      End With

      With AddWebRule(CrewTypeIDProperty)
        .JavascriptRuleFunctionName = "FlightBO.CrewTypeIDValid"
        .ServerRuleFunction = AddressOf CrewTypeIDValid
      End With

      'With AddWebRule(IsCancelledProperty)
      '  .AddTriggerProperty(CancelledReasonProperty)
      '  .JavascriptRuleFunctionName = "FlightBO.FlightCancelledReasonRequired"
      'End With

    End Sub

    Public Function HRClashCountValid(Flight As Flight) As String

      Dim ErrorMessage As String = ""
      If Flight.HRClashCount > 0 Then
        ErrorMessage = "There are passenger clashes on this flight"
      End If

      Return ErrorMessage

    End Function

    Public Shared Function CancelledReasonValid(flt As Flight) As String
      Dim ErrorString = ""
      If flt.IsCancelled AndAlso flt.CancelledReason.Trim = "" Then
        ErrorString = "Cancelled Reason Required"
      End If
      Return ErrorString
    End Function

    Public Shared Function AirportsValid(flt As Flight) As String
      Dim ErrorString = ""
      If CompareSafe(flt.AirportIDFrom, flt.AirportIDTo) Then
        ErrorString = "Airport From and To cannot be the same"
      End If
      Return ErrorString
    End Function

    Public Shared Function TravelDatesValid(flt As Flight) As String
      Dim ErrorString = ""
      If flt.DepartureDateTime IsNot Nothing AndAlso flt.ArrivalDateTime IsNot Nothing Then
        Dim CheckInDate = flt.DepartureDateTime
        Dim CheckOutDate = flt.ArrivalDateTime
        Dim result As Integer = DateTime.Compare(CheckInDate, CheckOutDate)
        If result >= 0 Then
          ErrorString = "Departure Date must be before Arrival Date"
        End If
      End If
      Return ErrorString
    End Function
    Public Shared Function CrewTypeIDValid(flt As Flight) As String
      If flt.CrewTypeID Is Nothing AndAlso flt.GetParent IsNot Nothing AndAlso CompareSafe(flt.GetParent.SystemID, CType(OBLib.CommonData.Enums.System.ProductionServices, Integer)) Then
        Return "Crew Type is required"
      End If
      Return ""
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFlight() method.

    End Sub

    Public Shared Function NewFlight() As Flight

      Return DataPortal.CreateChild(Of Flight)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFlight(dr As SafeDataReader) As Flight

      Dim f As New Flight()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FlightIDProperty, .GetInt32(0))
          LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(FlightNoProperty, .GetString(2))
          LoadProperty(FlightTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(TravelDirectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(AirportIDFromProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(DepartureDateTimeProperty, .GetValue(7))
          LoadProperty(AirportIDToProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(ArrivalDateTimeProperty, .GetValue(9))
          LoadProperty(CancelledDateTimeProperty, .GetValue(10))
          LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(CancelledReasonProperty, .GetString(12))
          LoadProperty(CreatedByProperty, .GetInt32(13))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(14))
          LoadProperty(ModifiedByProperty, .GetInt32(15))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(16))
          LoadProperty(AirportFromProperty, .GetString(17))
          LoadProperty(AirportToProperty, .GetString(18))
          LoadProperty(PassengerCountProperty, .GetInt32(19))
          LoadProperty(IsCancelledProperty, IIf(CancelledByUserID Is Nothing, False, True))
          LoadProperty(HRClashCountProperty, .GetInt32(20))
          LoadProperty(CountryIDFromProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
          LoadProperty(CountryIDToProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insFlight"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFlight"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramFlightID As SqlParameter = .Parameters.Add("@FlightID", SqlDbType.Int)
          paramFlightID.Value = GetProperty(FlightIDProperty)
          If Me.IsNew Then
            paramFlightID.Direction = ParameterDirection.Output
          End If
          If Me.GetParent IsNot Nothing Then
            SetProperty(TravelRequisitionIDProperty, Me.GetParent.TravelRequisitionID)
            .Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
          Else
            .Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(GetProperty(TravelRequisitionIDProperty)))
          End If
          .Parameters.AddWithValue("@FlightNo", GetProperty(FlightNoProperty))
          .Parameters.AddWithValue("@FlightTypeID", GetProperty(FlightTypeIDProperty))
          .Parameters.AddWithValue("@CrewTypeID", Singular.Misc.NothingDBNull(GetProperty(CrewTypeIDProperty)))
          .Parameters.AddWithValue("@TravelDirectionID", GetProperty(TravelDirectionIDProperty))
          .Parameters.AddWithValue("@AirportIDFrom", GetProperty(AirportIDFromProperty))
          .Parameters.AddWithValue("@DepartureDateTime", (New SmartDate(GetProperty(DepartureDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@AirportIDTo", GetProperty(AirportIDToProperty))
          .Parameters.AddWithValue("@ArrivalDateTime", (New SmartDate(GetProperty(ArrivalDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledDateTime", (New SmartDate(GetProperty(CancelledDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledByUserID", Singular.Misc.NothingDBNull(GetProperty(CancelledByUserIDProperty)))
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@ModifiedBy", ZeroDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@CountryIDFrom", Singular.Misc.NothingDBNull(GetProperty(CountryIDFromProperty)))
          .Parameters.AddWithValue("@CountryIDTo", Singular.Misc.NothingDBNull(GetProperty(CountryIDToProperty)))
          .Parameters.AddWithValue("@PassengersLoaded", (Me.FlightHumanResourceList.Count > 0))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(FlightIDProperty, paramFlightID.Value)
          End If
          ' update child objects
          If GetProperty(FlightHumanResourceListProperty) IsNot Nothing Then
            Me.FlightHumanResourceList.Update()
          End If
          SetPassengerCount()
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(FlightHumanResourceListProperty) IsNot Nothing Then
          Me.FlightHumanResourceList.Update()
        End If
        SetPassengerCount()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delFlight"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@FlightID", GetProperty(FlightIDProperty))
        cm.Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
        cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace