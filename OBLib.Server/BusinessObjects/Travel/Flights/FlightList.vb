﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.Flights

  <Serializable()> _
  Public Class FlightList
    Inherits OBBusinessListBase(Of FlightList, Flight)

#Region " Business Methods "

    Public Function GetItem(FlightID As Integer) As Flight

      For Each child As Flight In Me
        If child.FlightID = FlightID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Flights"

    End Function

    Public Function GetFlightHumanResource(FlightHumanResourceID As Integer) As FlightHumanResource

      Dim obj As FlightHumanResource = Nothing
      For Each parent As Flight In Me
        obj = parent.FlightHumanResourceList.GetItem(FlightHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    'Public Sub UpdateCancelledFlightHumanResources()

    '  For Each Flight As Flight In Me
    '    If Not IsNullNothing(Flight) Then
    '      For Each FlightHumanResource As FlightHumanResource In Flight.FlightHumanResourceList
    '        If FlightHumanResource.CancelledInd Then
    '          FlightHumanResource.CancelledDateTime = Date.Now
    '          FlightHumanResource.CancelledByUserID = OBLib.Security.Settings.CurrentUserID
    '        End If
    '      Next
    '    End If
    '  Next

    'End Sub

    Public Sub UpdateTravelRequisitionID(TravelRequisitionID As Integer)

      Me.ToList.ForEach(Sub(z)
                          If z.IsNew Then
                            z.TravelRequisitionID = TravelRequisitionID
                          End If
                        End Sub)

    End Sub

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property FlightID As Integer? = Nothing

      Public Sub New(TravelRequisitionID As Integer?, FlightID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        Me.FlightID = FlightID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewFlightList() As FlightList

      Return New FlightList()

    End Function

    Public Shared Sub BeginGetFlightList(CallBack As EventHandler(Of DataPortalResult(Of FlightList)))

      Dim dp As New DataPortal(Of FlightList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetFlightList() As FlightList

      Return DataPortal.Fetch(Of FlightList)(New Criteria())

    End Function

    Public Shared Function GetFlightList(TravelRequisitionID As Integer?, FlightID As Integer?) As FlightList

      Return DataPortal.Fetch(Of FlightList)(New Criteria(TravelRequisitionID, FlightID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(Flight.GetFlight(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As Flight = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.FlightID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          If parent IsNot Nothing Then
            parent.FlightHumanResourceList.RaiseListChangedEvents = False
            parent.FlightHumanResourceList.Add(FlightHumanResource.GetFlightHumanResource(sdr))
            parent.FlightHumanResourceList.RaiseListChangedEvents = True
          End If
        End While
      End If

      For Each child As Flight In Me
        child.CheckRules()
        For Each fhr As FlightHumanResource In child.FlightHumanResourceList
          fhr.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getFlightList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@FlightID", NothingDBNull(crit.FlightID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        If Me.DeletedList.Count > 0 Then
          DeleteBulk()
          ' Then clear the list of deleted objects because they are truly gone now.
          DeletedList.Clear()
        End If

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As Flight In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

    Private Sub DeleteBulk()

      ''Dim dt As DataTable = CreateCrewScheduleTableParameter()
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delFlightListBulk"
      '  cm.CommandType = CommandType.StoredProcedure
      '  Dim sqlP As SqlClient.SqlParameter = New SqlClient.SqlParameter("@FlightIDs", SqlDbType.VarChar)
      '  sqlP.Value = OBMisc.IntegerListToXML(Me.DeletedList.Select(Function(d) d.FlightID).ToList)
      '  cm.Connection = CType(Csla.ApplicationContext.LocalContext("cn"), SqlClient.SqlConnection)
      '  cm.Parameters.Add(sqlP)
      '  cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      '  cm.Transaction = Csla.ApplicationContext.LocalContext("tr")
      '  Singular.CSLALib.ContextInfo.SetContextInfoOnConnection(cm.Connection, cm.Transaction)
      '  cm.ExecuteNonQuery()
      'End Using

      Dim cm As New Singular.CommandProc("DelProcsWeb.delFlightListBulk")
      cm.Parameters.AddWithValue("@FlightIDs", Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(Me.DeletedList.Select(Function(d) d.FlightID).ToList)))
      cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUser.UserID)
      cm.UseTransaction = True
      cm.Execute()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace