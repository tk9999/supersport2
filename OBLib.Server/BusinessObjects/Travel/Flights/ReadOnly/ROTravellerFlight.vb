﻿' Generated 13 May 2016 11:47 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Flights.ReadOnly

  <Serializable()> _
  Public Class ROTravellerFlight
    Inherits OBReadOnlyBase(Of ROTravellerFlight)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FlightIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FlightID() As Integer
      Get
        Return GetProperty(FlightIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionID, "Travel Requisition")
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="")>
    Public ReadOnly Property TravelRequisitionID() As Integer
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared FlightNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlightNo, "Flight No")
    ''' <summary>
    ''' Gets the Flight No value
    ''' </summary>
    <Display(Name:="Flight No", Description:="")>
    Public ReadOnly Property FlightNo() As String
      Get
        Return GetProperty(FlightNoProperty)
      End Get
    End Property

    Public Shared FlightTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightTypeID, "Flight Type")
    ''' <summary>
    ''' Gets the Flight Type value
    ''' </summary>
    <Display(Name:="Flight Type", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROFlightTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property FlightTypeID() As Integer
      Get
        Return GetProperty(FlightTypeIDProperty)
      End Get
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewTypeID, "Crew Type")
    ''' <summary>
    ''' Gets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="")>
    Public ReadOnly Property CrewTypeID() As Integer
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
    End Property

    Public Shared TravelDirectionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelDirectionID, "Travel Direction")
    ''' <summary>
    ''' Gets the Travel Direction value
    ''' </summary>
    <Display(Name:="Travel Direction", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROTravelDirectionList), Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel)>
    Public ReadOnly Property TravelDirectionID() As Integer
      Get
        Return GetProperty(TravelDirectionIDProperty)
      End Get
    End Property

    Public Shared AirportIDFromProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AirportIDFrom, "Airport From")
    ''' <summary>
    ''' Gets the Airport ID From value
    ''' </summary>
    <Display(Name:="Airport From", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Travel.ReadOnly.ROAirportList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property AirportIDFrom() As Integer
      Get
        Return GetProperty(AirportIDFromProperty)
      End Get
    End Property

    Public Shared DepartureDateTimeSProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DepartureDateTimeS, "Departure Date Time")
    ''' <summary>
    ''' Gets the Departure Date Time value
    ''' </summary>
    <Display(Name:="Departure Date Time", Description:="")>
    Public ReadOnly Property DepartureDateTimeS As String
      Get
        Return GetProperty(DepartureDateTimeSProperty)
      End Get
    End Property

    Public Shared DepartureDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DepartureDateTime, "Departure Date Time")
    ''' <summary>
    ''' Gets the Departure Date Time value
    ''' </summary>
    <Display(Name:="Departure Date Time", Description:="")>
    Public ReadOnly Property DepartureDateTime As DateTime
      Get
        Return GetProperty(DepartureDateTimeProperty)
      End Get
    End Property

    Public Shared AirportIDToProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AirportIDTo, "Airport To")
    ''' <summary>
    ''' Gets the Airport ID To value
    ''' </summary>
    <Display(Name:="Airport To", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Travel.ReadOnly.ROAirportList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property AirportIDTo() As Integer
      Get
        Return GetProperty(AirportIDToProperty)
      End Get
    End Property

    Public Shared ArrivaldateTimeSProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ArrivalDateTimeS, "Arrivaldate Time")
    ''' <summary>
    ''' Gets the Arrivaldate Time value
    ''' </summary>
    <Display(Name:="Arrivaldate Time", Description:="")>
    Public ReadOnly Property ArrivalDateTimeS As String
      Get
        Return GetProperty(ArrivaldateTimeSProperty)
      End Get
    End Property

    Public Shared ArrivaldateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ArrivalDateTime, "Arrivaldate Time")
    ''' <summary>
    ''' Gets the Arrivaldate Time value
    ''' </summary>
    <Display(Name:="Arrivaldate Time", Description:="")>
    Public ReadOnly Property ArrivalDateTime As DateTime
      Get
        Return GetProperty(ArrivaldateTimeProperty)
      End Get
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="")>
    Public ReadOnly Property CancelledDateTime As Date
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User")
    ''' <summary>
    ''' Gets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="")>
    Public ReadOnly Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="")>
    Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PassengerCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PassengerCount, "Passenger Count", Nothing)
    ''' <summary>
    ''' Gets and sets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Passenger Count", Description:="Passenger Count")>
    Public ReadOnly Property PassengerCount() As Integer
      Get
        Return GetProperty(PassengerCountProperty)
      End Get
    End Property

    Public Shared CancelledIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CancelledInd, "Cancelled Indicator", False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:="Is Flight Cancelled?")>
    Public ReadOnly Property CancelledInd() As Boolean
      Get
        If CancelledByUserID IsNot Nothing Then
          Return True
        End If
        Return False
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROTravellerFlightPassengerListProperty As PropertyInfo(Of ROTravellerFlightPassengerList) = RegisterProperty(Of ROTravellerFlightPassengerList)(Function(c) c.ROTravellerFlightPassengerList, "RO Traveller Flight Passenger List")

    Public ReadOnly Property ROTravellerFlightPassengerList() As ROTravellerFlightPassengerList
      Get
        If GetProperty(ROTravellerFlightPassengerListProperty) Is Nothing Then
          LoadProperty(ROTravellerFlightPassengerListProperty, OBLib.Travel.Flights.ReadOnly.ROTravellerFlightPassengerList.NewROTravellerFlightPassengerList)
        End If
        Return GetProperty(ROTravellerFlightPassengerListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FlightIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FlightNo

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerFlight(dr As SafeDataReader) As ROTravellerFlight

      Dim r As New ROTravellerFlight()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FlightIDProperty, .GetInt32(0))
        LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(FlightNoProperty, .GetString(2))
        LoadProperty(FlightTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(TravelDirectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(AirportIDFromProperty, .GetInt32(6))
        LoadProperty(DepartureDateTimeSProperty, .GetString(7))
        LoadProperty(AirportIDToProperty, .GetInt32(8))
        LoadProperty(ArrivaldateTimeSProperty, .GetString(9))
        LoadProperty(DepartureDateTimeProperty, .GetValue(10))
        LoadProperty(ArrivaldateTimeProperty, .GetValue(11))
        LoadProperty(CancelledDateTimeProperty, .GetValue(12))
        LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(CancelledReasonProperty, .GetString(14))
        LoadProperty(CreatedByProperty, .GetInt32(15))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(16))
        LoadProperty(ModifiedByProperty, .GetInt32(17))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(18))
        LoadProperty(PassengerCountProperty, .GetInt32(19))
      End With

    End Sub

#End Region

  End Class

End Namespace
