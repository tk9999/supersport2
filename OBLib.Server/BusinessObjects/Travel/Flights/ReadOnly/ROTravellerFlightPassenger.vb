﻿' Generated 13 May 2016 11:47 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Flights.ReadOnly

  <Serializable()> _
  Public Class ROTravellerFlightPassenger
    Inherits OBReadOnlyBase(Of ROTravellerFlightPassenger)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FlightHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FlightHumanResourceID() As Integer
      Get
        Return GetProperty(FlightHumanResourceIDProperty)
      End Get
    End Property

    Public Shared FlightIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property FlightID() As Integer
      Get
        Return GetProperty(FlightIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource ID")
    ''' <summary>
    ''' Gets the Human Resource ID value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared FlightClassIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightClassID, "Flight Class")
    ''' <summary>
    ''' Gets the Flight Class value
    ''' </summary>
    Public ReadOnly Property FlightClassID() As Integer
      Get
        Return GetProperty(FlightClassIDProperty)
      End Get
    End Property

    Public Shared BookingReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingReason, "Booking Reason")
    ''' <summary>
    ''' Gets the Booking Reason value
    ''' </summary>
    <Display(Name:="Booking Reason", Description:="")>
    Public ReadOnly Property BookingReason() As String
      Get
        Return GetProperty(BookingReasonProperty)
      End Get
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="")>
    Public ReadOnly Property CancelledDateTime As Date
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CancelledByUserID, "Cancelled By User")
    ''' <summary>
    ''' Gets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="")>
    Public ReadOnly Property CancelledByUserID() As Integer
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="")>
    Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared TicketPriceProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TicketPrice, "Ticket Price", 0)
    ''' <summary>
    ''' Gets and sets the Ticket Price value
    ''' </summary>
    <Display(Name:="Ticket Price", Description:="")>
    Public ReadOnly Property TicketPrice() As Decimal
      Get
        Return GetProperty(TicketPriceProperty)
      End Get
    End Property

    Public Shared ServiceFeeProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ServiceFee, "Service Fee", 0)
    ''' <summary>
    ''' Gets and sets the Service Fee value
    ''' </summary>
    <Display(Name:="Service Fee", Description:="")>
    Public ReadOnly Property ServiceFee() As Decimal
      Get
        Return GetProperty(ServiceFeeProperty)
      End Get
    End Property

    Public Shared CancellationFeeProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.CancellationFee, "Cancellation Fee", 0)
    ''' <summary>
    ''' Gets and sets the Cancellation Fee value
    ''' </summary>
    <Display(Name:="Cancellation Fee", Description:="")>
    Public ReadOnly Property CancellationFee() As Decimal
      Get
        Return GetProperty(CancellationFeeProperty)
      End Get
    End Property

    Public Shared AllocatedProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AllocatedProductionID, "Allocated Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Allocated Production value
    ''' </summary>
    <Display(Name:="Allocated Production", Description:="")>
    Public ReadOnly Property AllocatedProductionID() As Integer?
      Get
        Return GetProperty(AllocatedProductionIDProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared CommentUpdatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CommentUpdatedDateTime, "Comment Updated Date Time")
    ''' <summary>
    ''' Gets and sets the Comment Updated Date Time value
    ''' </summary>
    <Display(Name:="Comment Updated Date Time", Description:="")>
    Public ReadOnly Property CommentUpdatedDateTime As DateTime?
      Get
        Return GetProperty(CommentUpdatedDateTimeProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Booking Reason value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared CancelledIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CancelledInd, "Cancelled Indicator", False)
    ''' <summary>
    ''' Gets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:="")>
    Public ReadOnly Property CancelledInd() As Boolean
      Get
        Return IIf(Me.CancelledByUserID = Nothing, False, True)
      End Get
    End Property


#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FlightIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResourceID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerFlightPassenger(dr As SafeDataReader) As ROTravellerFlightPassenger

      Dim r As New ROTravellerFlightPassenger()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FlightHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(FlightIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(FlightClassIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(BookingReasonProperty, .GetString(4))
        LoadProperty(CancelledDateTimeProperty, .GetValue(5))
        LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(CancelledReasonProperty, .GetString(7))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(11))
        LoadProperty(TicketPriceProperty, .GetDecimal(12))
        LoadProperty(ServiceFeeProperty, .GetDecimal(13))
        LoadProperty(CancellationFeeProperty, .GetDecimal(14))
        LoadProperty(AllocatedProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
        LoadProperty(CommentsProperty, .GetString(16))
        LoadProperty(CommentUpdatedDateTimeProperty, .GetValue(17))
        LoadProperty(HumanResourceProperty, .GetString(18))
      End With

    End Sub

#End Region

  End Class

End Namespace

