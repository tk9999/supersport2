﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.Travel.ReadOnly

Namespace Travel.Flights

  <Serializable()> _
  Public Class FlightHumanResource
    Inherits OBBusinessBase(Of FlightHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return FlightPassengerBO.toString(self)")
    'Public Shared OnFlightIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.OnFlightInd, False)
    ' ''' <summary>
    ' ''' Gets and sets the Flight Class value
    ' ''' </summary>
    '<Display(Name:="On Flight Indicator", Description:=""),
    'SetExpression("FlightPassengerBO.SetFlightClass(self)")>
    'Public Property OnFlightInd() As Boolean
    '  Get
    '    Return GetProperty(OnFlightIndProperty)
    '  End Get
    '  Set(ByVal Value As Boolean)
    '    SetProperty(OnFlightIndProperty, Value)
    '  End Set
    'End Property

    Public Shared IsOnFlightProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsOnFlight, "", False)
    ''' <summary>
    ''' Gets and sets the Car Class value
    ''' </summary>
    <Display(Name:="On Flight Indicator", Description:=""),
    SetExpression("FlightPassengerBO.IsOnFlightSet(self)")>
    Public Property IsOnFlight() As Boolean
      Get
        Return GetProperty(IsOnFlightProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsOnFlightProperty, Value)
      End Set
    End Property

    Public Shared FlightHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property FlightHumanResourceID() As Integer
      Get
        Return GetProperty(FlightHumanResourceIDProperty)
      End Get
    End Property

    Public Shared FlightIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FlightID, "Flight", Nothing)
    ''' <summary>
    ''' Gets the Flight value
    ''' </summary>
    Public Property FlightID() As Integer?
      Get
        Return GetProperty(FlightIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(FlightIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="Human resource booked for the flight"),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared FlightClassIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FlightClassID, "Flight Class", Nothing)
    ''' <summary>
    ''' Gets and sets the Flight Class value
    ''' </summary>
    <Display(Name:="Flight Class", Description:="Flight class fo the flight"),
    Required(ErrorMessage:="Flight Class required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROFlightClassList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property FlightClassID() As Integer?
      Get
        Return GetProperty(FlightClassIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FlightClassIDProperty, Value)
      End Set
    End Property

    Public Shared BookingReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingReason, "Booking Reason", "")
    ''' <summary>
    ''' Gets and sets the Booking Reason value
    ''' </summary>
    <Display(Name:="Booking Reason", Description:="Why was the person booked. (only if required)"),
    StringLength(200, ErrorMessage:="Booking Reason cannot be more than 200 characters")>
    Public Property BookingReason() As String
      Get
        Return GetProperty(BookingReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BookingReasonProperty, Value)
      End Set
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets and sets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="Date and time of the cancellation")>
    Public Property CancelledDateTime As DateTime?
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancelledDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="User who made the cancellation")>
    Public Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(CancelledByUserIDProperty, value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="Reason for the cancellation"),
    StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters"),
    SetExpression("FlightPassengerBO.CancelledReasonSet(self)")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Cancelled Indicator", False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:=""),
    SetExpression("FlightPassengerBO.IsCancelledSet(self)")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceName, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the HR value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="Human Resource")>
    Public Property HumanResourceName() As String
      Get
        Return GetProperty(HumanResourceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceNameProperty, Value)
      End Set
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City Code")
    ''' <summary>
    ''' Gets and sets the City Code value
    ''' </summary>
    <Display(Name:="City Code", Description:="")>
    Public Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CityCodeProperty, Value)
      End Set
    End Property

    Public Shared OtherFlightsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OtherFlights, "Other Flights", "")
    ''' <summary>
    ''' Gets and sets the Other Flights value
    ''' </summary>
    <Display(Name:="Other Flights", Description:="")>
    Public Property OtherFlights() As String
      Get
        Return GetProperty(OtherFlightsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OtherFlightsProperty, Value)
      End Set
    End Property

    Public Shared DisciplinesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Disciplines, "Disciplines", "")
    ''' <summary>
    ''' Gets and sets the Booking Reason value
    ''' </summary>
    <Display(Name:="Disciplines", Description:="")>
    Public Property Disciplines() As String
      Get
        Return GetProperty(DisciplinesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplinesProperty, Value)
      End Set
    End Property

    Public Shared OtherFlightsShortProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OtherFlightsShort, "Other Flights", "")
    ''' <summary>
    ''' Gets and sets the City Code value
    ''' </summary>
    <Display(Name:="Other Flights", Description:="")>
    Public Property OtherFlightsShort() As String
      Get
        Return GetProperty(OtherFlightsShortProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OtherFlightsShortProperty, Value)
      End Set
    End Property

    Public Shared HasClashesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasClashes, "Has Clashes", False)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="Has Clashes", Description:="")>
    Public Property HasClashes() As Boolean
      Get
        Return GetProperty(HasClashesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasClashesProperty, Value)
      End Set
    End Property

    Public Shared ReAddedToFlightProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ReAddedToFlight, "ReAddToFlight", False)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="ReAddedToFlight", Description:="")>
    Public Property ReAddedToFlight() As Boolean
      Get
        Return GetProperty(ReAddedToFlightProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ReAddedToFlightProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Flight

      Return CType(CType(Me.Parent, FlightHumanResourceList).Parent, Flight)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FlightHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.BookingReason.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Flight Human Resource")
        Else
          Return String.Format(Me.HumanResourceName)
        End If
      Else
        Return Me.BookingReason
      End If

    End Function

    Private Sub UpdateCancelledDetails(Value As Boolean)

      If Value = True Then
        SetProperty(CancelledByUserIDProperty, Security.Settings.CurrentUser.UserID)
        SetProperty(CancelledDateTimeProperty, Date.Now)
      Else
        SetProperty(CancelledByUserIDProperty, Nothing)
        SetProperty(CancelledDateTimeProperty, Nothing)
      End If

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(IsOnFlightProperty)
        .AddTriggerProperty(OtherFlightsProperty)
        .JavascriptRuleFunctionName = "FlightPassengerBO.IsOnFlightValid"
        .ServerRuleFunction = AddressOf IsOnFlightValid
      End With

      With AddWebRule(CancelledReasonProperty)
        .JavascriptRuleFunctionName = "FlightPassengerBO.CancelledReasonRequired"
        .ServerRuleFunction = AddressOf CancelledReasonRequired
      End With

    End Sub

    Public Shared Function CancelledReasonRequired(FlightHumanResource As FlightHumanResource) As String
      Dim ErrorString = ""
      If FlightHumanResource.IsCancelled AndAlso FlightHumanResource.CancelledReason.Trim = "" Then
        ErrorString = "Cancelled Reason Required"
      End If
      Return ErrorString
    End Function

    Public Shared Function IsOnFlightValid(FlightHumanResource As FlightHumanResource) As String
      Dim ErrorString = ""
      If FlightHumanResource.IsOnFlight AndAlso Not FlightHumanResource.OtherFlights.Trim = "" Then
        ErrorString = FlightHumanResource.HumanResourceName & " has flight clashes"
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFlightHumanResource() method.

    End Sub

    Public Shared Function NewFlightHumanResource() As FlightHumanResource

      Return DataPortal.CreateChild(Of FlightHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFlightHumanResource(dr As SafeDataReader) As FlightHumanResource

      Dim f As New FlightHumanResource()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FlightHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(FlightIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(FlightClassIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(BookingReasonProperty, .GetString(4))
          LoadProperty(CancelledDateTimeProperty, .GetValue(5))
          LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(CancelledReasonProperty, .GetString(7))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
          LoadProperty(HumanResourceNameProperty, .GetString(12))
          LoadProperty(IsCancelledProperty, .GetBoolean(13))
          LoadProperty(CityCodeProperty, .GetString(14))
          LoadProperty(OtherFlightsProperty, .GetString(15))
          LoadProperty(DisciplinesProperty, .GetString(16))
          LoadProperty(IsSelectedProperty, .GetBoolean(17))
          LoadProperty(IsOnFlightProperty, .GetBoolean(17))
          LoadProperty(OtherFlightsShortProperty, .GetString(18))
          LoadProperty(HasClashesProperty, .GetBoolean(19))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insFlightHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFlightHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramFlightHumanResourceID As SqlParameter = .Parameters.Add("@FlightHumanResourceID", SqlDbType.Int)
          paramFlightHumanResourceID.Value = GetProperty(FlightHumanResourceIDProperty)
          'If Me.IsNew Then
          paramFlightHumanResourceID.Direction = ParameterDirection.InputOutput
          'End If
          .Parameters.AddWithValue("@IsSelected", GetProperty(IsOnFlightProperty))
          .Parameters.AddWithValue("@FlightID", Me.GetParent().FlightID)
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@FlightClassID", GetProperty(FlightClassIDProperty))
          .Parameters.AddWithValue("@BookingReason", GetProperty(BookingReasonProperty))
          .Parameters.AddWithValue("@CancelledDateTime", (New SmartDate(GetProperty(CancelledDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledByUserID", Singular.Misc.NothingDBNull(GetProperty(CancelledByUserIDProperty)))
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@ModifiedBy", ZeroDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@TravelRequisitionID", Me.GetParent().TravelRequisitionID)

          .ExecuteNonQuery()

          'If Me.IsNew Then
          LoadProperty(FlightHumanResourceIDProperty, IIf(IsNullNothing(paramFlightHumanResourceID.Value), 0, paramFlightHumanResourceID.Value))
          'End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delFlightHumanResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@FlightHumanResourceID", GetProperty(FlightHumanResourceIDProperty))
        cm.Parameters.AddWithValue("@TravelRequisitionID", Me.GetParent().TravelRequisitionID)
        cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace