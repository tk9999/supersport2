﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Travel.CrewMembers

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel

  <Serializable()> _
  Public Class TravelRequisitionList
    Inherits OBBusinessListBase(Of TravelRequisitionList, TravelRequisition)

#Region " Business Methods "

    Public Function GetItem(TravelRequisitionID As Integer) As TravelRequisition

      For Each child As TravelRequisition In Me
        If child.TravelRequisitionID = TravelRequisitionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetAdHocSnT(HumanResourceID As Integer) As OBLib.Travel.Travellers.TravelRequisitionTraveller

      Dim obj As OBLib.Travel.Travellers.TravelRequisitionTraveller = Nothing
      For Each parent As TravelRequisition In Me
        obj = parent.TravellerList.GetItem(HumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    'Public Function GetCrewMember(HumanResourceID As Integer) As OBLib.Travel.CrewMembers.CrewMember

    '  Dim obj As OBLib.Travel.CrewMembers.CrewMember = Nothing
    '  For Each parent As TravelRequisition In Me
    '    obj = parent.CrewMemberList.GetItem(HumanResourceID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    Public Overrides Function ToString() As String

      Return "Travel Requisitions"

    End Function

    Public Function GetFlight(FlightID As Integer) As OBLib.Travel.Flights.Flight

      Dim obj As OBLib.Travel.Flights.Flight = Nothing
      For Each parent As TravelRequisition In Me
        obj = parent.FlightList.GetItem(FlightID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetRentalCar(RentalCarID As Integer) As OBLib.Travel.RentalCars.RentalCar

      Dim obj As OBLib.Travel.RentalCars.RentalCar = Nothing
      For Each parent As TravelRequisition In Me
        obj = parent.RentalCarList.GetItem(RentalCarID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetChauffeurDriver(ChauffeurDriverID As Integer) As OBLib.Travel.Chauffeurs.ChauffeurDriver

      Dim obj As OBLib.Travel.Chauffeurs.ChauffeurDriver = Nothing
      For Each parent As TravelRequisition In Me
        obj = parent.ChauffeurDriverList.GetItem(ChauffeurDriverID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetAccommodation(AccommodationID As Integer) As OBLib.Travel.Accommodation.Accommodation

      Dim obj As OBLib.Travel.Accommodation.Accommodation = Nothing
      For Each parent As TravelRequisition In Me
        obj = parent.AccommodationList.GetItem(AccommodationID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    'Public Function GetCrewMember(HumanResourceID As Integer) As OBLib.Travel.CrewMembers.CrewMember

    '  Dim obj As OBLib.Travel.CrewMembers.CrewMember = Nothing
    '  For Each parent As TravelRequisition In Me
    '    obj = parent.CrewMemberList.GetItem(HumanResourceID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TravelRequisitionID As Integer?
      'Public Property ProductionID As Integer?
      'Public Property AdHocBookingID As Integer?
      'Public Property StartDate As Date?
      'Public Property EndDate As Date?
      'Public Property FetchForProduction As Boolean

      Public Sub New()

      End Sub

      ', ProductionID As Integer?, AdHocBookingID As Integer?, StartDate As Date?, EndDate As Date?, FetchForProduction As Boolean
      Public Sub New(TravelRequisitionID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        'Me.ProductionID = ProductionID
        'Me.AdHocBookingID = AdHocBookingID
        'Me.StartDate = StartDate
        'Me.EndDate = EndDate
        'Me.FetchForProduction = FetchForProduction
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewTravelRequisitionList() As TravelRequisitionList

      Return New TravelRequisitionList()

    End Function

    Public Shared Sub BeginGetTravelRequisitionList(CallBack As EventHandler(Of DataPortalResult(Of TravelRequisitionList)))

      Dim dp As New DataPortal(Of TravelRequisitionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetTravelRequisitionList() As TravelRequisitionList

      Return DataPortal.Fetch(Of TravelRequisitionList)(New Criteria())

    End Function

    Public Shared Function GetTravelRequisitionList(TravelRequisitionID As Integer?) As TravelRequisitionList
      ', ProductionID As Integer?, AdHocBookingID As Integer?, StartDate As Date?, EndDate As Date?, FetchForProduction As Boolean
      ', ProductionID, AdHocBookingID, StartDate, EndDate, FetchForProduction
      Return DataPortal.Fetch(Of TravelRequisitionList)(New Criteria(TravelRequisitionID))
    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(TravelRequisition.GetTravelRequisition(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As TravelRequisition = Nothing

      ''''''''''''''''''''''''''Flights''''''''''''''''''''''''''
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.TravelRequisitionID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    If parent IsNot Nothing Then
      '      parent.FlightList.RaiseListChangedEvents = False
      '      parent.FlightList.Add(Flight.GetFlight(sdr))
      '      parent.FlightList.RaiseListChangedEvents = True
      '    End If
      '  End While
      'End If

      ''''''''''''''''''''''''''Flight HumanResources''''''''''''''''''''''''''
      'Dim parentFlightChild As OBLib.Travel.Flights.Flight = Nothing
      'If sdr.NextResult() Then
      'While sdr.Read
      '  If parentFlightChild Is Nothing OrElse parentFlightChild.FlightID <> sdr.GetInt32(1) Then
      '    parentFlightChild = Me.GetFlight(sdr.GetInt32(1))
      '  End If
      '  If parentFlightChild IsNot Nothing Then
      '    parentFlightChild.FlightHumanResourceList.RaiseListChangedEvents = False
      '    parentFlightChild.FlightHumanResourceList.Add(FlightHumanResource.GetFlightHumanResource(sdr))
      '    parentFlightChild.FlightHumanResourceList.RaiseListChangedEvents = True
      '  End If
      'End While
      'End If

      ''''''''''''''''''''''''''Rental Cars''''''''''''''''''''''''''
      'If sdr.NextResult() Then
      'While sdr.Read
      '  If parent Is Nothing OrElse parent.TravelRequisitionID <> sdr.GetInt32(1) Then
      '    parent = Me.GetItem(sdr.GetInt32(1))
      '  End If
      '  If parent IsNot Nothing Then
      '    parent.RentalCarList.RaiseListChangedEvents = False
      '    parent.RentalCarList.Add(RentalCar.GetRentalCar(sdr))
      '    parent.RentalCarList.RaiseListChangedEvents = True
      '  End If
      'End While
      'End If

      ''''''''''''''''''''''''''Rental Cars Human Resources''''''''''''''''''''''''''
      'Dim parentRentalCarChild As OBLib.Travel.RentalCars.RentalCar = Nothing
      'If sdr.NextResult() Then
      'While sdr.Read
      '  If parentRentalCarChild Is Nothing OrElse parentRentalCarChild.RentalCarID <> sdr.GetInt32(1) Then
      '    parentRentalCarChild = Me.GetRentalCar(sdr.GetInt32(1))
      '  End If
      '  If parentRentalCarChild IsNot Nothing Then
      '    parentRentalCarChild.RentalCarHumanResourceList.RaiseListChangedEvents = False
      '    parentRentalCarChild.RentalCarHumanResourceList.Add(RentalCarHumanResource.GetRentalCarHumanResource(sdr))
      '    parentRentalCarChild.RentalCarHumanResourceList.RaiseListChangedEvents = True
      '  End If
      'End While
      'End If

      ''''''''''''''''''''''''''Accommodation''''''''''''''''''''''''''
      'If sdr.NextResult() Then
      'While sdr.Read
      '  If parent Is Nothing OrElse parent.TravelRequisitionID <> sdr.GetInt32(1) Then
      '    parent = Me.GetItem(sdr.GetInt32(1))
      '  End If
      '  If parent IsNot Nothing Then
      '    parent.AccommodationList.RaiseListChangedEvents = False
      '    parent.AccommodationList.Add(Accommodation.GetAccommodation(sdr))
      '    parent.AccommodationList.RaiseListChangedEvents = True
      '  End If
      'End While
      'End If

      ''''''''''''''''''''''''''Accommodation Human Resources''''''''''''''''''''''''''
      'Dim parentAccommodationChild As OBLib.Travel.Accommodation.Accommodation = Nothing
      'If sdr.NextResult() Then
      'While sdr.Read
      '  If parentAccommodationChild Is Nothing OrElse parentAccommodationChild.AccommodationID <> sdr.GetInt32(1) Then
      '    parentAccommodationChild = Me.GetAccommodation(sdr.GetInt32(1))
      '  End If
      '  If parentAccommodationChild IsNot Nothing Then
      '    parentAccommodationChild.AccommodationHumanResourceList.RaiseListChangedEvents = False
      '    parentAccommodationChild.AccommodationHumanResourceList.Add(AccommodationHumanResource.GetAccommodationHumanResource(sdr))
      '    parentAccommodationChild.AccommodationHumanResourceList.RaiseListChangedEvents = True
      '  End If
      'End While
      'End If

      ''''''''''''''''''''''''''Accommodation Supplier Human Resources''''''''''''''''''''''''''
      'If sdr.NextResult() Then
      'While sdr.Read
      '  If parentAccommodationChild Is Nothing OrElse parentAccommodationChild.AccommodationID <> sdr.GetInt32(1) Then
      '    parentAccommodationChild = Me.GetAccommodation(sdr.GetInt32(1))
      '  End If
      '  If parentAccommodationChild IsNot Nothing Then
      '    parentAccommodationChild.AccommodationSupplierHumanResourceList.RaiseListChangedEvents = False
      '    parentAccommodationChild.AccommodationSupplierHumanResourceList.Add(AccommodationSupplierHumanResource.GetAccommodationSupplierHumanResource(sdr))
      '    parentAccommodationChild.AccommodationSupplierHumanResourceList.RaiseListChangedEvents = True
      '  End If
      'End While
      'End If

      ''''''''''''''''''''''''''Travel Req Comments''''''''''''''''''''''''''
      ' If sdr.NextResult() Then
      'While sdr.Read
      '  If parent Is Nothing OrElse parent.TravelRequisitionID <> sdr.GetInt32(1) Then
      '    parent = Me.GetItem(sdr.GetInt32(1))
      '  End If
      '  parent.TravelReqCommentList.RaiseListChangedEvents = False
      '  parent.TravelReqCommentList.Add(TravelReqComment.GetTravelReqComment(sdr))
      '  parent.TravelReqCommentList.RaiseListChangedEvents = True
      'End While
      'End If

      ''''''''''''''''''''''''''Production Travel Advance Details''''''''''''''''''''''''''
      'If sdr.NextResult() Then
      'While sdr.Read
      '  If parent Is Nothing OrElse parent.TravelRequisitionID <> sdr.GetInt32(14) Then
      '    parent = Me.GetItem(sdr.GetInt32(14))
      '  End If
      '  parent.ProductionTravelAdvanceDetailList.RaiseListChangedEvents = False
      '  parent.ProductionTravelAdvanceDetailList.Add(ProductionTravelAdvanceDetail.GetProductionTravelAdvanceDetail(sdr))
      '  parent.ProductionTravelAdvanceDetailList.RaiseListChangedEvents = True
      'End While
      ' End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.TravelRequisitionID <> sdr.GetInt32(7) Then
      '      parent = Me.GetItem(sdr.GetInt32(7))
      '    End If
      '    If parent IsNot Nothing Then
      '      parent.TravellerList.RaiseListChangedEvents = False
      '      parent.TravellerList.Add(OBLib.Travel.Travellers.TravelRequisitionTraveller.GetTravelRequisitionTraveller(sdr))
      '      parent.TravellerList.RaiseListChangedEvents = True
      '    End If
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.TravelRequisitionID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.CrewMemberList.RaiseListChangedEvents = False
      '    parent.CrewMemberList.Add(OBLib.Travel.CrewMembers.CrewMember.GetCrewMember(sdr))
      '    parent.CrewMemberList.RaiseListChangedEvents = True
      '  End While
      'End If

      ' ''''''''''''''''''''''''''Crew Members (Flights)''''''''''''''''''''''''''
      'Dim parentCrewMemberChild As CrewMember = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentCrewMemberChild Is Nothing OrElse parentCrewMemberChild.HumanResourceID <> sdr.GetInt32(0) Then
      '      parentCrewMemberChild = Me.GetCrewMember(sdr.GetInt32(0))
      '    End If
      '    parentCrewMemberChild.CrewMemberFlightList.RaiseListChangedEvents = False
      '    parentCrewMemberChild.CrewMemberFlightList.Add(CrewMemberFlight.GetCrewMemberFlight(sdr))
      '    parentCrewMemberChild.CrewMemberFlightList.RaiseListChangedEvents = True
      '  End While
      'End If

      ' ''''''''''''''''''''''''''Crew Members (Rental Cars)''''''''''''''''''''''''''
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentCrewMemberChild Is Nothing OrElse parentCrewMemberChild.HumanResourceID <> sdr.GetInt32(0) Then
      '      parentCrewMemberChild = Me.GetCrewMember(sdr.GetInt32(0))
      '    End If
      '    parentCrewMemberChild.CrewMemberRentalCarList.RaiseListChangedEvents = False
      '    parentCrewMemberChild.CrewMemberRentalCarList.Add(CrewMemberRentalCar.GetCrewMemberRentalCar(sdr))
      '    parentCrewMemberChild.CrewMemberRentalCarList.RaiseListChangedEvents = True
      '  End While
      'End If

      ' ''''''''''''''''''''''''''Crew Members (Accommodation)''''''''''''''''''''''''''
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentCrewMemberChild Is Nothing OrElse parentCrewMemberChild.HumanResourceID <> sdr.GetInt32(0) Then
      '      parentCrewMemberChild = Me.GetCrewMember(sdr.GetInt32(0))
      '    End If
      '    parentCrewMemberChild.CrewMemberAccommodationList.RaiseListChangedEvents = False
      '    parentCrewMemberChild.CrewMemberAccommodationList.Add(CrewMemberAccommodation.GetCrewMemberAccommodation(sdr))
      '    parentCrewMemberChild.CrewMemberAccommodationList.RaiseListChangedEvents = True
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    'If paren Is Nothing OrElse parentChauffeurDriverChild.ChauffeurDriverID <> sdr.GetInt32(1) Then
      '    '  parentChauffeurDriverChild = Me.GetChauffeurDriver(sdr.GetInt32(1))
      '    'End If
      '    parent = Me(0)
      '    parent.ROTravelAvailableHumanResourceList.RaiseListChangedEvents = False
      '    parent.ROTravelAvailableHumanResourceList.Add(OBLib.Travel.CrewMembers.ReadOnly.ROTravelAvailableHumanResource.GetROTravelAvailableHumanResource(sdr))
      '    parent.ROTravelAvailableHumanResourceList.RaiseListChangedEvents = True
      '  End While
      'End If

      ''''''''''''''''''''''''''Chauffeur Drivers'''''''''''''''''''''''''
      'If sdr.NextResult() Then
      'While sdr.Read
      '  If parent Is Nothing OrElse parent.TravelRequisitionID <> sdr.GetInt32(1) Then
      '    parent = Me.GetItem(sdr.GetInt32(1))
      '  End If
      '  parent.ChauffeurDriverList.RaiseListChangedEvents = False
      '  parent.ChauffeurDriverList.Add(ChauffeurDriver.GetChauffeurDriver(sdr))
      '  parent.ChauffeurDriverList.RaiseListChangedEvents = True
      'End While
      'End If

      ' ''''''''''''''''''''''''''Chauffeur Drivers Human Resources''''''''''''''''''''''''''
      'Dim parentChauffeurDriverChild As OBLib.Travel.Chauffeurs.ChauffeurDriver = Nothing
      ' If sdr.NextResult() Then
      'While sdr.Read
      '  If parentChauffeurDriverChild Is Nothing OrElse parentChauffeurDriverChild.ChauffeurDriverID <> sdr.GetInt32(1) Then
      '    parentChauffeurDriverChild = Me.GetChauffeurDriver(sdr.GetInt32(1))
      '  End If
      '  parentChauffeurDriverChild.ChauffeurDriverHumanResourceList.RaiseListChangedEvents = False
      '  parentChauffeurDriverChild.ChauffeurDriverHumanResourceList.Add(ChauffeurDriverHumanResource.GetChauffeurDriverHumanResource(sdr))
      '  parentChauffeurDriverChild.ChauffeurDriverHumanResourceList.RaiseListChangedEvents = True
      'End While
      ' End If

      'Check Rules
      'For Each child As TravelRequisition In Me
      'child.CheckRules()
      'For Each Flight As Flight In child.FlightList
      '  Flight.CheckRules()
      '  For Each FlightHumanResource As FlightHumanResource In Flight.FlightHumanResourceList
      '    FlightHumanResource.CheckRules()
      '  Next
      'Next

      'For Each RentalCar As RentalCar In child.RentalCarList
      '  RentalCar.CheckRules()
      '  For Each RentalCarHumanResource As RentalCarHumanResource In RentalCar.RentalCarHumanResourceList
      '    RentalCarHumanResource.CheckRules()
      '  Next
      'Next

      'For Each Accommodation As Accommodation In child.AccommodationList
      '  Accommodation.CheckRules()
      '  For Each AccommodationHumanResource As AccommodationHumanResource In Accommodation.AccommodationHumanResourceList
      '    AccommodationHumanResource.CheckRules()
      '  Next
      '  For Each AccommodationSupplierHumanResource As AccommodationSupplierHumanResource In Accommodation.AccommodationSupplierHumanResourceList
      '    AccommodationSupplierHumanResource.CheckRules()
      '  Next
      'Next

      'For Each TravelReqComment As TravelReqComment In child.TravelReqCommentList
      '  TravelReqComment.CheckRules()
      'Next

      'For Each ChauffeurDriver As ChauffeurDriver In child.ChauffeurDriverList
      '  ChauffeurDriver.CheckRules()
      '  For Each ChauffeurDriverHumanResource As ChauffeurDriverHumanResource In ChauffeurDriver.ChauffeurDriverHumanResourceList
      '    ChauffeurDriverHumanResource.CheckRules()
      '  Next
      'Next

      'Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getTravelRequisitionList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As TravelRequisition In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As TravelRequisition In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace