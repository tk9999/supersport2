﻿' Generated 09 Jul 2014 09:16 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.TravelAdvances

  <Serializable()> _
  Public Class ProductionTravelAdvanceDetailList
    Inherits OBBusinessListBase(Of ProductionTravelAdvanceDetailList, ProductionTravelAdvanceDetail)

#Region " Business Methods "

    Public Function GetItem(ProductionTravelAdvanceDetailID As Integer) As ProductionTravelAdvanceDetail

      For Each child As ProductionTravelAdvanceDetail In Me
        If child.ProductionTravelAdvanceDetailID = ProductionTravelAdvanceDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Travel Advance Details"

    End Function

    Public Sub UpdateTravelRequisitionID(TravelRequisitionID As Integer)

      Me.ToList.ForEach(Sub(z)
                          If z.IsNew Then
                            z.TravelRequisitionID = TravelRequisitionID
                          End If
                        End Sub)

    End Sub

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(TravelRequisitionID As Integer?)

        Me.TravelRequisitionID = TravelRequisitionID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionTravelAdvanceDetailList() As ProductionTravelAdvanceDetailList

      Return New ProductionTravelAdvanceDetailList()

    End Function

    Public Shared Sub BeginGetProductionTravelAdvanceDetailList(CallBack As EventHandler(Of DataPortalResult(Of ProductionTravelAdvanceDetailList)))

      Dim dp As New DataPortal(Of ProductionTravelAdvanceDetailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionTravelAdvanceDetailList(TravelRequisitionID As Integer?) As ProductionTravelAdvanceDetailList

      Return DataPortal.Fetch(Of ProductionTravelAdvanceDetailList)(New Criteria(TravelRequisitionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionTravelAdvanceDetail.GetProductionTravelAdvanceDetail(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionTravelAdvanceDetailList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        '' Loop through each deleted child object and call its Update() method
        'For Each Child As Flight In DeletedList
        '  Child.DeleteSelf()
        'Next
        DeleteBulk()

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionTravelAdvanceDetail In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

    Private Sub DeleteBulk()

      Dim cm As New Singular.CommandProc("DelProcsWeb.delTravelAdvanceListBulk")
      cm.Parameters.AddWithValue("@ProductionTravelAdvanceDetailIDs", Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(Me.DeletedList.Select(Function(d) d.ProductionTravelAdvanceDetailID).ToList)))
      cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      cm.UseTransaction = True
      cm.Execute()

      ''Dim dt As DataTable = CreateCrewScheduleTableParameter()
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delTravelAdvanceListBulk"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Connection = CType(Csla.ApplicationContext.LocalContext("cn"), SqlClient.SqlConnection)
      '  cm.Transaction = Csla.ApplicationContext.LocalContext("tr")
      '  Singular.CSLALib.ContextInfo.SetContextInfoOnConnection(cm.Connection, cm.Transaction)
      '  cm.Parameters.AddWithValue("@ProductionTravelAdvanceDetailIDs", OBMisc.IntegerListToXML(Me.DeletedList.Select(Function(d) d.ProductionTravelAdvanceDetailID).ToList))
      '  cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      '  cm.ExecuteNonQuery()
      'End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace