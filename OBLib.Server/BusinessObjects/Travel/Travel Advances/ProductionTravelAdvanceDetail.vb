﻿' Generated 09 Jul 2014 09:16 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Productions.ReadOnly
Imports OBLib.Maintenance.Travel.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.TravelAdvances

  <Serializable()> _
  Public Class ProductionTravelAdvanceDetail
    Inherits OBBusinessBase(Of ProductionTravelAdvanceDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionTravelAdvanceDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTravelAdvanceDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property ProductionTravelAdvanceDetailID() As Integer
      Get
        Return GetProperty(ProductionTravelAdvanceDetailIDProperty)
      End Get
    End Property

    Public Shared ProductionTravelAdvanceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTravelAdvanceID, "Production Travel Advance", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Travel Advance value
    ''' </summary>
    <Display(Name:="Production Travel Advance", Description:="Reference to the Production Travel record")>
    Public Property ProductionTravelAdvanceID() As Integer?
      Get
        Return GetProperty(ProductionTravelAdvanceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTravelAdvanceIDProperty, Value)
      End Set
    End Property

    Public Shared CurrencyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CurrencyID, "Currency", Nothing)
    ''' <summary>
    ''' Gets and sets the Currency value
    ''' </summary>
    <Display(Name:="Currency", Description:="Currency in which the Huma Resource will be paid in"),
    Required(ErrorMessage:="Currency required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCurrencyList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property CurrencyID() As Integer?
      Get
        Return GetProperty(CurrencyIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CurrencyIDProperty, Value)
      End Set
    End Property

    Public Shared DenomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Denom, "Denom", "")
    ''' <summary>
    ''' Gets and sets the Denom value
    ''' </summary>
    <Display(Name:="Denom", Description:="Denom of the production travel advance type"),
    StringLength(20, ErrorMessage:="Denom cannot be more than 20 characters")>
    Public Property Denom() As String
      Get
        Return GetProperty(DenomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DenomProperty, Value)
      End Set
    End Property

    Public Shared TravelAdvanceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelAdvanceTypeID, "Advance Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Travel Advance Type value
    ''' </summary>
    <Display(Name:="Advance Type", Description:="The advance type for the human resource"),
    Required(ErrorMessage:="Advance Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROTravelAdvanceTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property TravelAdvanceTypeID() As Integer?
      Get
        Return GetProperty(TravelAdvanceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TravelAdvanceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared PayTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PayTypeID, "Pay Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Pay Type value
    ''' </summary>
    <Display(Name:="Pay Type", Description:="How the money will be paid"),
    Required(ErrorMessage:="Pay Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROPayTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property PayTypeID() As Integer?
      Get
        Return GetProperty(PayTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PayTypeIDProperty, Value)
      End Set
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Amount, "Amount", CDec(0.00))
    ''' <summary>
    ''' Gets and sets the Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:="The amount to be paid"),
    Required(ErrorMessage:="Amount required")>
    Public Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountProperty, Value)
      End Set
    End Property

    Public Shared AmountZARProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AmountZAR, "Amount ZAR", 0)
    ''' <summary>
    ''' Gets and sets the Amount ZAR value
    ''' </summary>
    <Display(Name:="Amount ZAR", Description:="The amount in ZAR that will be paid"),
    Required(ErrorMessage:="Amount ZAR required")>
    Public Property AmountZAR() As Decimal
      Get
        Return GetProperty(AmountZARProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountZARProperty, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="A comment made about the production travel advance detail"),
    StringLength(400, ErrorMessage:="Comments cannot be more than 400 characters")>
    Public Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Traveller", Description:=""), Required(ErrorMessage:="Human Resource Required"),
    Singular.DataAnnotations.DropDownWeb(GetType(Travel.Travellers.ReadOnly.ROTravellerList),
                                     DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete,
                                     DisplayMember:="HumanResource", LookupMember:="HumanResource", ValueMember:="HumanResourceID",
                                     DropDownColumns:={"HumanResource"},
                                     BeforeFetchJS:="TravelAdvanceDetailBO.setHumanResourceCriteriaBeforeRefresh",
                                     PreFindJSFunction:="TravelAdvanceDetailBO.triggerHumanResourceAutoPopulate",
                                     AfterFetchJS:="TravelAdvanceDetailBO.afterHumanResourceRefreshAjax",
                                     OnItemSelectJSFunction:="TravelAdvanceDetailBO.onHumanResourceSelected",
                                     DropDownCssClass:="room-dropdown")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "HumanResource", "")
    ''' <summary>
    ''' Gets and sets the HumanResource value
    ''' </summary>
    <Display(Name:="Human Resource")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelRequisitionID, "Travel Requisition", Nothing)
    ''' <summary>
    ''' Gets and sets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="")>
    Public Property TravelRequisitionID() As Integer?
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TravelRequisitionIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As OBLib.Travel.TravelRequisition

      Return CType(CType(Me.Parent, ProductionTravelAdvanceDetailList).Parent, OBLib.Travel.TravelRequisition)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionTravelAdvanceDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Denom.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Travel Advance Detail")
        Else
          Return String.Format("Blank {0}", "Production Travel Advance Detail")
        End If
      Else
        Return Me.Denom
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionTravelAdvanceDetail() method.

    End Sub

    Public Shared Function NewProductionTravelAdvanceDetail() As ProductionTravelAdvanceDetail

      Return DataPortal.CreateChild(Of ProductionTravelAdvanceDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionTravelAdvanceDetail(dr As SafeDataReader) As ProductionTravelAdvanceDetail

      Dim p As New ProductionTravelAdvanceDetail()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionTravelAdvanceDetailIDProperty, .GetInt32(0))
          LoadProperty(ProductionTravelAdvanceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(CurrencyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DenomProperty, .GetString(3))
          LoadProperty(TravelAdvanceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(PayTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(AmountProperty, .GetDecimal(6))
          LoadProperty(AmountZARProperty, .GetDecimal(7))
          LoadProperty(CommentsProperty, .GetString(8))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ModifiedByProperty, .GetInt32(11))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(HumanResourceProperty, .GetString(15))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionTravelAdvanceDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionTravelAdvanceDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionTravelAdvanceDetailID As SqlParameter = .Parameters.Add("@ProductionTravelAdvanceDetailID", SqlDbType.Int)
          paramProductionTravelAdvanceDetailID.Value = GetProperty(ProductionTravelAdvanceDetailIDProperty)
          If Me.IsNew Then
            paramProductionTravelAdvanceDetailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionTravelAdvanceID", NothingDBNull(GetProperty(ProductionTravelAdvanceIDProperty)))
          .Parameters.AddWithValue("@CurrencyID", GetProperty(CurrencyIDProperty))
          .Parameters.AddWithValue("@Denom", GetProperty(DenomProperty))
          .Parameters.AddWithValue("@TravelAdvanceTypeID", GetProperty(TravelAdvanceTypeIDProperty))
          .Parameters.AddWithValue("@PayTypeID", GetProperty(PayTypeIDProperty))
          .Parameters.AddWithValue("@Amount", GetProperty(AmountProperty))
          .Parameters.AddWithValue("@AmountZAR", GetProperty(AmountZARProperty))
          .Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
          .Parameters.AddWithValue("@ModifiedBy", ZeroDBNull(OBLib.Security.Settings.CurrentUser.UserID))
          .Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
          .Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionTravelAdvanceDetailIDProperty, paramProductionTravelAdvanceDetailID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionTravelAdvanceDetail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionTravelAdvanceDetailID", GetProperty(ProductionTravelAdvanceDetailIDProperty))
        cm.Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
        cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace