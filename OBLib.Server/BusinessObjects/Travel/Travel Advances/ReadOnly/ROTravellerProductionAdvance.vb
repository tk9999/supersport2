﻿' Generated 15 May 2016 21:33 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.TravelAdvances.ReadOnly

  <Serializable()> _
  Public Class ROTravellerProductionAdvance
    Inherits OBReadOnlyBase(Of ROTravellerProductionAdvance)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionTravelAdvanceDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTravelAdvanceDetailID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionTravelAdvanceDetailID() As Integer
      Get
        Return GetProperty(ProductionTravelAdvanceDetailIDProperty)
      End Get
    End Property

    Public Shared CurrencyIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CurrencyID, "Currency")
    ''' <summary>
    ''' Gets the Currency value
    ''' </summary>
    <Display(Name:="Currency", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROCurrencyList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property CurrencyID() As Integer
      Get
        Return GetProperty(CurrencyIDProperty)
      End Get
    End Property

    Public Shared DenomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Denom, "Denom")
    ''' <summary>
    ''' Gets the Denom value
    ''' </summary>
    <Display(Name:="Denom", Description:="")>
    Public ReadOnly Property Denom() As String
      Get
        Return GetProperty(DenomProperty)
      End Get
    End Property

    Public Shared TravelAdvanceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelAdvanceTypeID, "Travel Advance Type")
    ''' <summary>
    ''' Gets the Travel Advance Type value
    ''' </summary>
    <Display(Name:="Advance Type", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Travel.ReadOnly.ROTravelAdvanceTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property TravelAdvanceTypeID() As Integer
      Get
        Return GetProperty(TravelAdvanceTypeIDProperty)
      End Get
    End Property

    Public Shared PayTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PayTypeID, "Pay Type")
    ''' <summary>
    ''' Gets the Pay Type value
    ''' </summary>
    <Display(Name:="Pay Type", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROPayTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property PayTypeID() As Integer
      Get
        Return GetProperty(PayTypeIDProperty)
      End Get
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Amount, "Amount")
    ''' <summary>
    ''' Gets the Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:="")>
    Public ReadOnly Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
    End Property

    Public Shared AmountZARProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AmountZAR, "Amount ZAR")
    ''' <summary>
    ''' Gets the Amount ZAR value
    ''' </summary>
    <Display(Name:="Amount ZAR", Description:="")>
    Public ReadOnly Property AmountZAR() As Decimal
      Get
        Return GetProperty(AmountZARProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Traveller", Description:=""),
    Singular.DataAnnotations.DropDownWeb("TravellerBO.GetTravellerDropDown($parent)",
                                         Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel,
                                         UnselectedText:="Select Traveller",
                                         ValueMember:="HumanResourceID",
                                         DisplayMember:="HumanResource")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionID, "Travel Requisition")
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="")>
    Public ReadOnly Property TravelRequisitionID() As Integer
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionTravelAdvanceDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Denom

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerProductionAdvance(dr As SafeDataReader) As ROTravellerProductionAdvance

      Dim r As New ROTravellerProductionAdvance()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionTravelAdvanceDetailIDProperty, .GetInt32(0))
        LoadProperty(CurrencyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(DenomProperty, .GetString(2))
        LoadProperty(TravelAdvanceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(PayTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(AmountProperty, .GetDecimal(5))
        LoadProperty(AmountZARProperty, .GetDecimal(6))
        LoadProperty(CommentsProperty, .GetString(7))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(11))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(HumanResourceProperty, .GetString(14))
      End With

    End Sub

#End Region

  End Class

End Namespace
