﻿' Generated 15 May 2016 21:33 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.TravelAdvances.ReadOnly

  <Serializable()> _
  Public Class ROTravellerProductionAdvanceList
    Inherits OBReadOnlyListBase(Of ROTravellerProductionAdvanceList, ROTravellerProductionAdvance)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "

    Public Function GetItem(ProductionTravelAdvanceDetailID As Integer) As ROTravellerProductionAdvance

      For Each child As ROTravellerProductionAdvance In Me
        If child.ProductionTravelAdvanceDetailID = ProductionTravelAdvanceDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New(TravelRequisitionID As Integer?, HumanResourceID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        Me.HumanResourceID = HumanResourceID
      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROTravellerProductionAdvanceList() As ROTravellerProductionAdvanceList

      Return New ROTravellerProductionAdvanceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTravellerProductionAdvanceList() As ROTravellerProductionAdvanceList

      Return DataPortal.Fetch(Of ROTravellerProductionAdvanceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTravellerProductionAdvance.GetROTravellerProductionAdvance(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravellerProductionAdvanceList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace