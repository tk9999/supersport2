﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Travel.CrewMembers.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel

  <Serializable()> _
  Public Class TravelRequisition
    Inherits OBBusinessBase(Of TravelRequisition)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return TravelRequisitionBO.toString(self)")

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property TravelRequisitionID() As Integer
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="A refereance to a Production")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingID, "Ad Hoc Booking", Nothing)
    ''' <summary>
    ''' Gets and sets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking", Description:="")>
    Public Property AdHocBookingID() As Integer?
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AdHocBookingIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="Reference to the Sub-Dept"),
    Required(ErrorMessage:="Sub-Dept required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSystemList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:="Reference to the Area")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared VersionNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VersionNo, "Version No", 0)
    ''' <summary>
    ''' Gets and sets the Version No value
    ''' </summary>
    <Display(Name:="Version No", Description:="Version numbe rof the Travel Requisition"),
    Required(ErrorMessage:="Version No required")>
    Public Property VersionNo() As Integer
      Get
        Return GetProperty(VersionNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(VersionNoProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", OBLib.Security.Settings.CurrentUser.UserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Created By User"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROUserList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, DisplayMember:="FirstSurname")>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Check In Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the Check In Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared EventTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventTitle, "Title", "")
    ''' <summary>
    ''' Gets and sets the Crew Type value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property EventTitle() As String
      Get
        Return GetProperty(EventTitleProperty)
      End Get
    End Property

    Public Shared EventReferenceNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventReferenceNumber, "RefNum", "")
    ''' <summary>
    ''' Gets and sets the Crew Type value
    ''' </summary>
    <Display(Name:="RefNum", Description:="")>
    Public ReadOnly Property EventReferenceNumber() As String
      Get
        Return GetProperty(EventReferenceNumberProperty)
      End Get
    End Property

    Public Shared StartTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StartTimeString, "Start Time", "")
    ''' <summary>
    ''' Gets and sets the Crew Type value
    ''' </summary>
    <Display(Name:="Start Time")>
    Public ReadOnly Property StartTimeString() As String
      Get
        Return GetProperty(StartTimeStringProperty)
      End Get
    End Property

    Public Shared EndTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EndTimeString, "End Time", "")
    ''' <summary>
    ''' Gets and sets the Crew Type value
    ''' </summary>
    <Display(Name:="End Time")>
    Public ReadOnly Property EndTimeString() As String
      Get
        Return GetProperty(EndTimeStringProperty)
      End Get
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "Crew Type", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="The type of crew to which the requisition applies"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCrewTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property CrewTypeID() As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Created By")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "Sub-Dept", "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="Sub-Dept")>
    Public ReadOnly Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area", "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Area", Description:="Area")>
    Public ReadOnly Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

    ''Public Shared CreatedByUserProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByUser, "Created By User", "")
    ' ''' <summary>
    ' ''' Gets and sets the Cancelled Reason value
    ' ''' </summary>
    '<Display(Name:="Created By User", Description:="Created By User")>
    'Public ReadOnly Property CreatedByUser() As String
    '  Get
    '    'If CreatedBy IsNot Nothing Then
    '    '  Dim ROUser As ROUser = CommonData.Lists.ROUserList.GetItem(CreatedBy)
    '    '  If ROUser IsNot Nothing Then
    '    '    Return ROUser.FirstSurname
    '    '  End If
    '    'End If
    '    Return ""
    '  End Get
    'End Property

    'Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area ID", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Production System Area ID value
    ' ''' </summary>
    '<Display(Name:="Production", Description:="A refereance to a Production")>
    'Public Property ProductionSystemAreaID() As Integer?
    '  Get
    '    Return GetProperty(ProductionSystemAreaIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(ProductionSystemAreaIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared AdHocBookingStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.AdHocBookingStartDateTime)
    ' ''' <summary>
    ' ''' Gets and sets the Start Date Time value
    ' ''' </summary>
    '<Display(Name:="Start Date Time", Description:="")>
    'Public Property AdHocBookingStartDateTime As DateTime?
    '  Get
    '    Return GetProperty(AdHocBookingStartDateTimeProperty)
    '  End Get
    '  Set(ByVal Value As DateTime?)
    '    SetProperty(AdHocBookingStartDateTimeProperty, Value)
    '  End Set
    'End Property

    'Public Shared AdHocBookingEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.AdHocBookingEndDateTime)
    ' ''' <summary>
    ' ''' Gets and sets the End Date Time value
    ' ''' </summary>
    '<Display(Name:="End Date Time", Description:="")>
    'Public Property AdHocBookingEndDateTime As DateTime?
    '  Get
    '    Return GetProperty(AdHocBookingEndDateTimeProperty)
    '  End Get
    '  Set(ByVal Value As DateTime?)
    '    SetProperty(AdHocBookingEndDateTimeProperty, Value)
    '  End Set
    'End Property

    'This is left in so things dont break, needs to be removed at some point
    Public Shared CountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryID, "CountryID", Nothing)
    Public ReadOnly Property CountryID As Integer?
      Get
        Return GetProperty(CountryIDProperty)
      End Get
    End Property

    Public Shared IsAdHocProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsAdHoc, "Is AdHoc", False)
    ''' <summary>
    ''' Gets and sets the Version No value
    ''' </summary>
    <Display(Name:="Is AdHoc", Description:="Is AdHoc")>
    Public Property IsAdHoc() As Boolean
      Get
        Return GetProperty(IsAdHocProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsAdHocProperty, Value)
      End Set
    End Property

    Public Shared CanBookChauffersProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanBookChauffers, "CanBookChauffers", False)
    ''' <summary>
    ''' Gets and sets the Version No value
    ''' </summary>
    <Display(Name:="CanBookChauffers", Description:="CanBookChauffersc")>
    Public Property CanBookChauffers() As Boolean
      Get
        Return GetProperty(CanBookChauffersProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanBookChauffersProperty, Value)
      End Set
    End Property

    Public Shared RentalCarsBusyProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RentalCarsBusy, "RentalCarsBusy", False)
    ''' <summary>
    ''' Gets and sets the Version No value
    ''' </summary>
    <Display(Name:="RentalCarsBusy", Description:="RentalCarsBusy")>
    Public Property RentalCarsBusy() As Boolean
      Get
        Return GetProperty(RentalCarsBusyProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RentalCarsBusyProperty, Value)
      End Set
    End Property

#End Region

#Region " Other Properties "

    Public Shared TravellersFetchedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TravellersFetched, "TravellersFetched", False)
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="TravellersFetched")>
    Public Property TravellersFetched() As Boolean
      Get
        Return GetProperty(TravellersFetchedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(TravellersFetchedProperty, value)
      End Set
    End Property

    Public Shared FlightsFetchedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FlightsFetched, "FlightsFetched", False)
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="FlightsFetched")>
    Public Property FlightsFetched() As Boolean
      Get
        Return GetProperty(FlightsFetchedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(FlightsFetchedProperty, value)
      End Set
    End Property

    Public Shared RentalCarsFetchedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RentalCarsFetched, "RentalCarsFetched", False)
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="RentalCarsFetched")>
    Public Property RentalCarsFetched() As Boolean
      Get
        Return GetProperty(RentalCarsFetchedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(RentalCarsFetchedProperty, value)
      End Set
    End Property

    Public Shared AccommodationFetchedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AccommodationFetched, "AccommodationFetched", False)
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="AccommodationFetched")>
    Public Property AccommodationFetched() As Boolean
      Get
        Return GetProperty(AccommodationFetchedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(AccommodationFetchedProperty, value)
      End Set
    End Property

    Public Shared ChaufferDrivesFetchedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ChaufferDrivesFetched, "ChaufferDrivesFetched", False)
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="ChaufferDrivesFetched")>
    Public Property ChaufferDrivesFetched() As Boolean
      Get
        Return GetProperty(ChaufferDrivesFetchedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(ChaufferDrivesFetchedProperty, value)
      End Set
    End Property

    Public Shared TravelAdvancesFetchedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TravelAdvancesFetched, "TravelAdvancesFetched", False)
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="TravelAdvancesFetched")>
    Public Property TravelAdvancesFetched() As Boolean
      Get
        Return GetProperty(TravelAdvancesFetchedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(TravelAdvancesFetchedProperty, value)
      End Set
    End Property

    Public Shared CommentsFetchedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CommentsFetched, "CommentsFetched", False)
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="CommentsFetched")>
    Public Property CommentsFetched() As Boolean
      Get
        Return GetProperty(CommentsFetchedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(CommentsFetchedProperty, value)
      End Set
    End Property

    Public Shared TravellerSnTFetchedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TravellerSnTFetched, "TravellerSnTFetched", False)
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="TravellerSnTFetched")>
    Public Property TravellerSnTFetched() As Boolean
      Get
        Return GetProperty(TravellerSnTFetchedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(TravellerSnTFetchedProperty, value)
      End Set
    End Property

    Public Shared TravelAvailabilityFetchedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TravelAvailabilityFetched, "TravelAvailabilityFetched", False)
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="TravelAvailabilityFetched")>
    Public Property TravelAvailabilityFetched() As Boolean
      Get
        Return GetProperty(TravelAvailabilityFetchedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(TravelAvailabilityFetchedProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared FlightListProperty As PropertyInfo(Of OBLib.Travel.Flights.FlightList) = RegisterProperty(Of OBLib.Travel.Flights.FlightList)(Function(c) c.FlightList, "Flight List")
    Public ReadOnly Property FlightList() As OBLib.Travel.Flights.FlightList
      Get
        If GetProperty(FlightListProperty) Is Nothing Then
          LoadProperty(FlightListProperty, OBLib.Travel.Flights.FlightList.NewFlightList())
        End If
        Return GetProperty(FlightListProperty)
      End Get
    End Property

    Public Shared RentalCarListProperty As PropertyInfo(Of OBLib.Travel.RentalCars.RentalCarList) = RegisterProperty(Of OBLib.Travel.RentalCars.RentalCarList)(Function(c) c.RentalCarList, "Rental Car List")
    Public ReadOnly Property RentalCarList() As OBLib.Travel.RentalCars.RentalCarList
      Get
        If GetProperty(RentalCarListProperty) Is Nothing Then
          LoadProperty(RentalCarListProperty, OBLib.Travel.RentalCars.RentalCarList.NewRentalCarList())
        End If
        Return GetProperty(RentalCarListProperty)
      End Get
    End Property

    Public Shared AccommodationListProperty As PropertyInfo(Of OBLib.Travel.Accommodation.AccommodationList) = RegisterProperty(Of OBLib.Travel.Accommodation.AccommodationList)(Function(c) c.AccommodationList, "Accommodation List")
    Public ReadOnly Property AccommodationList() As OBLib.Travel.Accommodation.AccommodationList
      Get
        If GetProperty(AccommodationListProperty) Is Nothing Then
          LoadProperty(AccommodationListProperty, OBLib.Travel.Accommodation.AccommodationList.NewAccommodationList())
        End If
        Return GetProperty(AccommodationListProperty)
      End Get
    End Property

    Public Shared TravelReqCommentListProperty As PropertyInfo(Of OBLib.Travel.Comments.TravelReqCommentList) = RegisterProperty(Of OBLib.Travel.Comments.TravelReqCommentList)(Function(c) c.TravelReqCommentList, "Travel Req Comment List")
    Public ReadOnly Property TravelReqCommentList() As OBLib.Travel.Comments.TravelReqCommentList
      Get
        If GetProperty(TravelReqCommentListProperty) Is Nothing Then
          LoadProperty(TravelReqCommentListProperty, OBLib.Travel.Comments.TravelReqCommentList.NewTravelReqCommentList())
        End If
        Return GetProperty(TravelReqCommentListProperty)
      End Get
    End Property

    Public Shared ProductionTravelAdvanceDetailListProperty As PropertyInfo(Of OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetailList) = RegisterProperty(Of OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetailList)(Function(c) c.ProductionTravelAdvanceDetailList, "Production Travel Advance Detail List")
    Public ReadOnly Property ProductionTravelAdvanceDetailList() As OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetailList
      Get
        If GetProperty(ProductionTravelAdvanceDetailListProperty) Is Nothing Then
          LoadProperty(ProductionTravelAdvanceDetailListProperty, OBLib.Travel.TravelAdvances.ProductionTravelAdvanceDetailList.NewProductionTravelAdvanceDetailList())
        End If
        Return GetProperty(ProductionTravelAdvanceDetailListProperty)
      End Get
    End Property

    'Public Shared CrewMemberListProperty As PropertyInfo(Of OBLib.Travel.CrewMembers.CrewMemberList) = RegisterProperty(Of OBLib.Travel.CrewMembers.CrewMemberList)(Function(c) c.CrewMemberList, "Crew Member List")
    'Public ReadOnly Property CrewMemberList() As OBLib.Travel.CrewMembers.CrewMemberList
    '  Get
    '    If GetProperty(CrewMemberListProperty) Is Nothing Then
    '      LoadProperty(CrewMemberListProperty, OBLib.Travel.CrewMembers.CrewMemberList.NewCrewMemberList())
    '    End If
    '    Return GetProperty(CrewMemberListProperty)
    '  End Get
    'End Property

    Public Shared ROTravelAvailableHumanResourceListProperty As PropertyInfo(Of ROTravelAvailableHumanResourceList) = RegisterProperty(Of ROTravelAvailableHumanResourceList)(Function(c) c.ROTravelAvailableHumanResourceList, "Available HR List")
    Public ReadOnly Property ROTravelAvailableHumanResourceList() As ROTravelAvailableHumanResourceList
      Get
        If GetProperty(ROTravelAvailableHumanResourceListProperty) Is Nothing Then
          LoadProperty(ROTravelAvailableHumanResourceListProperty, ROTravelAvailableHumanResourceList.NewROTravelAvailableHumanResourceList())
        End If
        Return GetProperty(ROTravelAvailableHumanResourceListProperty)
      End Get
    End Property

    'Public Shared ProductionHumanResourceSnTListProperty As PropertyInfo(Of OBLib.Travel.OB.SnTs.ProductionHumanResourceSnTList) = RegisterProperty(Of OBLib.Travel.OB.SnTs.ProductionHumanResourceSnTList)(Function(c) c.ProductionHumanResourceSnTList, "Production Human Resource SnT List")
    'Public ReadOnly Property ProductionHumanResourceSnTList() As OBLib.Travel.OB.SnTs.ProductionHumanResourceSnTList
    '  Get
    '    If GetProperty(ProductionHumanResourceSnTListProperty) Is Nothing Then
    '      LoadProperty(ProductionHumanResourceSnTListProperty, OBLib.Travel.OB.SnTs.ProductionHumanResourceSnTList.NewProductionHumanResourceSnTList())
    '    End If
    '    Return GetProperty(ProductionHumanResourceSnTListProperty)
    '  End Get
    'End Property

    Public Shared ChauffeurDriverListProperty As PropertyInfo(Of OBLib.Travel.Chauffeurs.ChauffeurDriverList) = RegisterProperty(Of OBLib.Travel.Chauffeurs.ChauffeurDriverList)(Function(c) c.ChauffeurDriverList, "Chauffeur driver List")
    Public ReadOnly Property ChauffeurDriverList() As OBLib.Travel.Chauffeurs.ChauffeurDriverList
      Get
        If GetProperty(ChauffeurDriverListProperty) Is Nothing Then
          LoadProperty(ChauffeurDriverListProperty, OBLib.Travel.Chauffeurs.ChauffeurDriverList.NewChauffeurDriverList())
        End If
        Return GetProperty(ChauffeurDriverListProperty)
      End Get
    End Property

    Public Shared TravellerListProperty As PropertyInfo(Of OBLib.Travel.Travellers.TravelRequisitionTravellerList) = RegisterProperty(Of OBLib.Travel.Travellers.TravelRequisitionTravellerList)(Function(c) c.TravellerList, "Traveller List")
    Public ReadOnly Property TravellerList() As OBLib.Travel.Travellers.TravelRequisitionTravellerList
      Get
        If GetProperty(TravellerListProperty) Is Nothing Then
          LoadProperty(TravellerListProperty, OBLib.Travel.Travellers.TravelRequisitionTravellerList.NewTravelRequisitionTravellerList())
        End If
        Return GetProperty(TravellerListProperty)
      End Get
    End Property

    Public Shared TravellerSnTListProperty As PropertyInfo(Of OBLib.Travel.Travellers.TravellerSnTList) = RegisterProperty(Of OBLib.Travel.Travellers.TravellerSnTList)(Function(c) c.TravellerSnTList, "Traveller S&T List")
    Public ReadOnly Property TravellerSnTList() As OBLib.Travel.Travellers.TravellerSnTList
      Get
        If GetProperty(TravellerSnTListProperty) Is Nothing Then
          LoadProperty(TravellerSnTListProperty, OBLib.Travel.Travellers.TravellerSnTList.NewTravellerSnTList())
        End If
        Return GetProperty(TravellerSnTListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TravelRequisitionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Travel Requisition")
        Else
          Return String.Format("Blank {0}", "Travel Requisition")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"Flights", "RentalCars", "Accommodation"}
      End Get
    End Property

    Public Function GetParent() As OBLib.Productions.Production

      Return CType(CType(Me.Parent, TravelRequisitionList).Parent, OBLib.Productions.Production)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTravelRequisition() method.

    End Sub

    Public Shared Function NewTravelRequisition() As TravelRequisition

      Return DataPortal.CreateChild(Of TravelRequisition)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTravelRequisition(dr As SafeDataReader) As TravelRequisition

      Dim t As New TravelRequisition()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TravelRequisitionIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(VersionNoProperty, .GetInt32(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(StartDateProperty, .GetValue(10))
          LoadProperty(EndDateProperty, .GetValue(11))
          LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(EventTitleProperty, .GetString(13))
          LoadProperty(EventReferenceNumberProperty, .GetString(14))
          LoadProperty(StartTimeStringProperty, .GetString(15))
          LoadProperty(EndTimeStringProperty, .GetString(16))
          LoadProperty(CreatedByNameProperty, .GetString(17))
          LoadProperty(SystemProperty, .GetString(18))
          LoadProperty(ProductionAreaProperty, .GetString(19))
          LoadProperty(IsAdHocProperty, .GetBoolean(20))
          LoadProperty(CanBookChauffersProperty, .GetBoolean(21))
          'LoadProperty(ProductionDescriptionProperty, .GetString(10))
          'LoadProperty(SystemProperty, .GetString(11))
          'LoadProperty(CreatedByUserProperty, .GetString(12))       
          'LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          'LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          'LoadProperty(AdHocBookingStartDateTimeProperty, .GetDateTime(16))
          'LoadProperty(AdHocBookingEndDateTimeProperty, .GetDateTime(17))
          'LoadProperty(CountryIDProperty, .GetInt32(18))
          'LoadProperty(AdHocBookingTitleProperty, .GetString(19))
          'LoadProperty(AdHocBookingDescriptionProperty, .GetString(20))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTravelRequisition"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTravelRequisition"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTravelRequisitionID As SqlParameter = .Parameters.Add("@TravelRequisitionID", SqlDbType.Int)
          paramTravelRequisitionID.Value = GetProperty(TravelRequisitionIDProperty)
          If Me.IsNew Then
            paramTravelRequisitionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionID", NothingDBNull(GetProperty(ProductionIDProperty)))
          .Parameters.AddWithValue("@AdHocBookingID", NothingDBNull(GetProperty(AdHocBookingIDProperty)))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@VersionNo", GetProperty(VersionNoProperty))
          .Parameters.AddWithValue("@ModifiedBy", ZeroDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@CrewTypeID", NothingDBNull(GetProperty(CrewTypeIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(TravelRequisitionIDProperty, paramTravelRequisitionID.Value)
          End If
          If GetProperty(TravellerListProperty) IsNot Nothing Then
            Me.TravellerList.Update()
          End If
          ' update child objects
          If GetProperty(FlightListProperty) IsNot Nothing Then
            Me.FlightList.Update()
          End If
          If GetProperty(RentalCarListProperty) IsNot Nothing Then
            Me.RentalCarList.Update()
          End If
          If GetProperty(AccommodationListProperty) IsNot Nothing Then
            Me.AccommodationList.Update()
          End If
          If GetProperty(TravelReqCommentListProperty) IsNot Nothing Then
            Me.TravelReqCommentList.Update()
          End If
          If GetProperty(ProductionTravelAdvanceDetailListProperty) IsNot Nothing Then
            Me.ProductionTravelAdvanceDetailList.Update()
          End If
          If GetProperty(TravellerSnTListProperty) IsNot Nothing Then
            Me.TravellerSnTList.Update()
          End If
          If GetProperty(ChauffeurDriverListProperty) IsNot Nothing Then
            Me.ChauffeurDriverList.Update()
          End If
          'If GetProperty(CrewMemberListProperty) IsNot Nothing Then
          '  Me.CrewMemberList.Update()
          'End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(TravellerListProperty) IsNot Nothing Then
          Me.TravellerList.Update()
        End If
        If GetProperty(FlightListProperty) IsNot Nothing Then
          Me.FlightList.Update()
        End If
        If GetProperty(RentalCarListProperty) IsNot Nothing Then
          Me.RentalCarList.Update()
        End If
        If GetProperty(AccommodationListProperty) IsNot Nothing Then
          Me.AccommodationList.Update()
        End If
        If GetProperty(TravelReqCommentListProperty) IsNot Nothing Then
          Me.TravelReqCommentList.Update()
        End If
        If GetProperty(ProductionTravelAdvanceDetailListProperty) IsNot Nothing Then
          Me.ProductionTravelAdvanceDetailList.Update()
        End If
        If GetProperty(TravellerSnTListProperty) IsNot Nothing Then
          Me.TravellerSnTList.Update()
        End If
        If GetProperty(ChauffeurDriverListProperty) IsNot Nothing Then
          Me.ChauffeurDriverList.Update()
        End If
        'If GetProperty(CrewMemberListProperty) IsNot Nothing Then
        '  Me.CrewMemberList.Update()
        'End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTravelRequisition"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TravelRequisitionID", GetProperty(TravelRequisitionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace