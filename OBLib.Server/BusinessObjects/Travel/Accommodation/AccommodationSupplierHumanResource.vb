﻿' Generated 05 Aug 2014 22:36 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.Accommodation

  <Serializable()> _
  Public Class AccommodationSupplierHumanResource
    Inherits OBBusinessBase(Of AccommodationSupplierHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccommodationSupplierHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationSupplierHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccommodationSupplierHumanResourceID() As Integer
      Get
        Return GetProperty(AccommodationSupplierHumanResourceIDProperty)
      End Get
    End Property

    Public Shared AccommodationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccommodationID, "Accommodation", Nothing)
    ''' <summary>
    ''' Gets and sets the Accommodation value
    ''' </summary>
    <Display(Name:="Accommodation", Description:="A reference to the accommodation record")>
    Public Property AccommodationID() As Integer?
      Get
        Return GetProperty(AccommodationIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccommodationIDProperty, Value)
      End Set
    End Property

    Public Shared SupplierHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierHumanResourceID, "Supplier Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Supplier Human Resource value
    ''' </summary>
    <Display(Name:="Supplier Human Resource", Description:="Person who is booked"),
    Required(ErrorMessage:="Supplier Human Resource required")>
    Public Property SupplierHumanResourceID() As Integer?
      Get
        Return GetProperty(SupplierHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared RoomCostProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RoomCost, "Room Cost", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Room Cost value
    ''' </summary>
    <Display(Name:="Room Cost", Description:="Cost of the room"),
    Required(ErrorMessage:="Room Cost required")>
    Public Property RoomCost() As Decimal
      Get
        Return GetProperty(RoomCostProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RoomCostProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    'Public Shared OldProductionAccomodationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OldProductionAccomodationID, "Old Production Accomodation", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Old Production Accomodation value
    ' ''' </summary>
    '<Display(Name:="Old Production Accomodation", Description:="")>
    'Public Property OldProductionAccomodationID() As Integer?
    '  Get
    '    Return GetProperty(OldProductionAccomodationIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(OldProductionAccomodationIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared OldPASHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OldPASHumanResourceID, "Old PAS Human Resource", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Old PAS Human Resource value
    ' ''' </summary>
    '<Display(Name:="Old PAS Human Resource", Description:="")>
    'Public Property OldPASHumanResourceID() As Integer?
    '  Get
    '    Return GetProperty(OldPASHumanResourceIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(OldPASHumanResourceIDProperty, Value)
    '  End Set
    'End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Accommodation

      Return CType(CType(Me.Parent, AccommodationSupplierHumanResourceList).Parent, Accommodation)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationSupplierHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SupplierHumanResourceID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Accommodation Supplier Human Resource")
        Else
          Return String.Format("Blank {0}", "Accommodation Supplier Human Resource")
        End If
      Else
        Return Me.SupplierHumanResourceID.ToString()
      End If

    End Function

    Public Sub SetAccommodationCost(PerPersonPerNightCost As Decimal, NightsAway As Decimal)

      RoomCost = PerPersonPerNightCost * NightsAway

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccommodationSupplierHumanResource() method.

    End Sub

    Public Shared Function NewAccommodationSupplierHumanResource() As AccommodationSupplierHumanResource

      Return DataPortal.CreateChild(Of AccommodationSupplierHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccommodationSupplierHumanResource(dr As SafeDataReader) As AccommodationSupplierHumanResource

      Dim a As New AccommodationSupplierHumanResource()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccommodationSupplierHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(AccommodationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SupplierHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(RoomCostProperty, .GetDecimal(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccommodationSupplierHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccommodationSupplierHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccommodationSupplierHumanResourceID As SqlParameter = .Parameters.Add("@AccommodationSupplierHumanResourceID", SqlDbType.Int)
          paramAccommodationSupplierHumanResourceID.Value = GetProperty(AccommodationSupplierHumanResourceIDProperty)
          If Me.IsNew Then
            paramAccommodationSupplierHumanResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccommodationID", Me.GetParent().AccommodationID)
          .Parameters.AddWithValue("@SupplierHumanResourceID", GetProperty(SupplierHumanResourceIDProperty))
          .Parameters.AddWithValue("@RoomCost", GetProperty(RoomCostProperty))
          .Parameters.AddWithValue("@ModifiedBy", ZeroDBNull(OBLib.Security.Settings.CurrentUserID))
          '.Parameters.AddWithValue("@OldProductionAccomodationID", Singular.Misc.NothingDBNull(GetProperty(OldProductionAccomodationIDProperty)))
          '.Parameters.AddWithValue("@OldPASHumanResourceID", Singular.Misc.NothingDBNull(GetProperty(OldPASHumanResourceIDProperty)))
          .Parameters.AddWithValue("@TravelRequisitionID", Me.GetParent().GetParent().TravelRequisitionID)
          .Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccommodationSupplierHumanResourceIDProperty, paramAccommodationSupplierHumanResourceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccommodationSupplierHumanResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccommodationSupplierHumanResourceID", GetProperty(AccommodationSupplierHumanResourceIDProperty))
        cm.Parameters.AddWithValue("@TravelRequisitionID", Me.GetParent().GetParent().TravelRequisitionID)
        cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace