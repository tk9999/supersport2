﻿' Generated 15 May 2016 20:40 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Accommodation.ReadOnly

  <Serializable()> _
  Public Class ROTravellerAccommodationGuestList
    Inherits OBReadOnlyListBase(Of ROTravellerAccommodationGuestList, ROTravellerAccommodationGuest)
    Implements Singular.Paging.IPagedList

#Region " Parent "

    <NotUndoable()> Private mParent As ROTravellerAccommodation
#End Region

#Region " Business Methods "

    Public Function GetItem(AccommodationHumanResourceID As Integer) As ROTravellerAccommodationGuest

      For Each child As ROTravellerAccommodationGuest In Me
        If child.AccommodationHumanResourceID = AccommodationHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    Public Shared Function NewROTravellerAccommodationGuestList() As ROTravellerAccommodationGuestList

      Return New ROTravellerAccommodationGuestList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace
