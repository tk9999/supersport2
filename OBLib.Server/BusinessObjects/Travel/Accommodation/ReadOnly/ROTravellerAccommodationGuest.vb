﻿' Generated 15 May 2016 20:40 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Accommodation.ReadOnly

  <Serializable()> _
  Public Class ROTravellerAccommodationGuest
    Inherits OBReadOnlyBase(Of ROTravellerAccommodationGuest)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccommodationHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AccommodationHumanResourceID() As Integer
      Get
        Return GetProperty(AccommodationHumanResourceIDProperty)
      End Get
    End Property

    Public Shared AccommodationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationID, "Accommodation")
    ''' <summary>
    ''' Gets the Accommodation value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property AccommodationID() As Integer
      Get
        Return GetProperty(AccommodationIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared BookingReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingReason, "Booking Reason")
    ''' <summary>
    ''' Gets the Booking Reason value
    ''' </summary>
    <Display(Name:="Booking Reason", Description:="")>
    Public ReadOnly Property BookingReason() As String
      Get
        Return GetProperty(BookingReasonProperty)
      End Get
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="")>
    Public ReadOnly Property CancelledDateTime As Date
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CancelledByUserID, "Cancelled By User")
    ''' <summary>
    ''' Gets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="")>
    Public ReadOnly Property CancelledByUserID() As Integer
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="")>
    Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CancelledIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CancelledInd, "Cancelled?", False)
    ''' <summary>
    ''' Gets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:="")>
    Public ReadOnly Property CancelledInd() As Boolean
      Get
        Return GetProperty(CancelledIndProperty)
      End Get
    End Property

    Public Shared RoomCostProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RoomCost, "Room Cost")
    ''' <summary>
    ''' Gets the Room Cost value
    ''' </summary>
    <Display(Name:="Room Cost", Description:="")>
    Public ReadOnly Property RoomCost() As Decimal
      Get
        Return GetProperty(RoomCostProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared OldProductionAccomodationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OldProductionAccomodationID, "Old Production Accomodation")
    ''' <summary>
    ''' Gets the Old Production Accomodation value
    ''' </summary>
    <Display(Name:="Old Production Accomodation", Description:="")>
    Public ReadOnly Property OldProductionAccomodationID() As Integer
      Get
        Return GetProperty(OldProductionAccomodationIDProperty)
      End Get
    End Property

    Public Shared OldPAHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OldPAHumanResourceID, "Old PA Human Resource")
    ''' <summary>
    ''' Gets the Old PA Human Resource value
    ''' </summary>
    <Display(Name:="Old PA Human Resource", Description:="")>
    Public ReadOnly Property OldPAHumanResourceID() As Integer
      Get
        Return GetProperty(OldPAHumanResourceIDProperty)
      End Get
    End Property

    Public Shared AllocatedProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AllocatedProductionID, "Allocated Production")
    ''' <summary>
    ''' Gets the Allocated Production value
    ''' </summary>
    <Display(Name:="Allocated Production", Description:="")>
    Public ReadOnly Property AllocatedProductionID() As Integer
      Get
        Return GetProperty(AllocatedProductionIDProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared CommentUpdatedDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CommentUpdatedDateTime, "Comment Updated Date Time")
    ''' <summary>
    ''' Gets the Comment Updated Date Time value
    ''' </summary>
    <Display(Name:="Comment Updated Date Time", Description:="")>
    Public ReadOnly Property CommentUpdatedDateTime As Date
      Get
        Return GetProperty(CommentUpdatedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.BookingReason

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerAccommodationGuest(dr As SafeDataReader) As ROTravellerAccommodationGuest

      Dim r As New ROTravellerAccommodationGuest()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AccommodationHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(AccommodationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(BookingReasonProperty, .GetString(3))
        LoadProperty(CancelledDateTimeProperty, .GetValue(4))
        LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(CancelledReasonProperty, .GetString(6))
        LoadProperty(RoomCostProperty, .GetDecimal(7))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(11))
        LoadProperty(OldProductionAccomodationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(OldPAHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(AllocatedProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(CommentsProperty, .GetString(15))
        LoadProperty(CommentUpdatedDateTimeProperty, .GetValue(16))
      End With

    End Sub

#End Region

  End Class

End Namespace
