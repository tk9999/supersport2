﻿' Generated 15 May 2016 20:40 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Accommodation.ReadOnly

  <Serializable()> _
  Public Class ROTravellerAccommodationList
    Inherits OBReadOnlyListBase(Of ROTravellerAccommodationList, ROTravellerAccommodation)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "

    Public Function GetItem(AccommodationID As Integer) As ROTravellerAccommodation

      For Each child As ROTravellerAccommodation In Me
        If child.AccommodationID = AccommodationID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROTravellerAccommodationGuest(AccommodationHumanResourceID As Integer) As ROTravellerAccommodationGuest

      Dim obj As ROTravellerAccommodationGuest = Nothing
      For Each parent As ROTravellerAccommodation In Me
        obj = parent.ROTravellerAccommodationGuestList.GetItem(AccommodationHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New(TravelRequisitionID As Integer?, HumanResourceID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        Me.HumanResourceID = HumanResourceID
      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROTravellerAccommodationList() As ROTravellerAccommodationList

      Return New ROTravellerAccommodationList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTravellerAccommodationList() As ROTravellerAccommodationList

      Return DataPortal.Fetch(Of ROTravellerAccommodationList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTravellerAccommodation.GetROTravellerAccommodation(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROTravellerAccommodation = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AccommodationID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          If parent IsNot Nothing Then
            parent.ROTravellerAccommodationGuestList.RaiseListChangedEvents = False
            parent.ROTravellerAccommodationGuestList.Add(ROTravellerAccommodationGuest.GetROTravellerAccommodationGuest(sdr))
            parent.ROTravellerAccommodationGuestList.RaiseListChangedEvents = True
          End If
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravellerAccommodationList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace
