﻿' Generated 15 May 2016 20:40 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Accommodation.ReadOnly

  <Serializable()> _
  Public Class ROTravellerAccommodation
    Inherits OBReadOnlyBase(Of ROTravellerAccommodation)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccommodationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AccommodationID() As Integer
      Get
        Return GetProperty(AccommodationIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionID, "Travel Requisition")
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="")>
    Public ReadOnly Property TravelRequisitionID() As Integer
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared AccommodationProviderIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationProviderID, "Accommodation Provider")
    ''' <summary>
    ''' Gets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Accommodation Provider", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Travel.ReadOnly.ROAccommodationProviderList))>
    Public ReadOnly Property AccommodationProviderID() As Integer
      Get
        Return GetProperty(AccommodationProviderIDProperty)
      End Get
    End Property

    Public Shared CheckInDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CheckInDate, "Check In Date")
    ''' <summary>
    ''' Gets the Check In Date value
    ''' </summary>
    <Display(Name:="Check In Date", Description:="")>
    Public ReadOnly Property CheckInDate As Date
      Get
        Return GetProperty(CheckInDateProperty)
      End Get
    End Property

    Public Shared CheckOutDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CheckOutDate, "Check Out Date")
    ''' <summary>
    ''' Gets the Check Out Date value
    ''' </summary>
    <Display(Name:="Check Out Date", Description:="")>
    Public ReadOnly Property CheckOutDate As Date
      Get
        Return GetProperty(CheckOutDateProperty)
      End Get
    End Property

    Public Shared AccommodationRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccommodationRefNo, "Accommodation Ref No")
    ''' <summary>
    ''' Gets the Accommodation Ref No value
    ''' </summary>
    <Display(Name:="Accommodation Ref No", Description:="")>
    Public ReadOnly Property AccommodationRefNo() As String
      Get
        Return GetProperty(AccommodationRefNoProperty)
      End Get
    End Property

    Public Shared BookingReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingReason, "Booking Reason")
    ''' <summary>
    ''' Gets the Booking Reason value
    ''' </summary>
    <Display(Name:="Booking Reason", Description:="")>
    Public ReadOnly Property BookingReason() As String
      Get
        Return GetProperty(BookingReasonProperty)
      End Get
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="")>
    Public ReadOnly Property CancelledDateTime As Date
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User")
    ''' <summary>
    ''' Gets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="")>
    Public ReadOnly Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="")>
    Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierID, "Supplier")
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="")>
    Public ReadOnly Property SupplierID() As Integer
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared GuestCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GuestCount, "Guest Count")
    ''' <summary>
    ''' Gets the Guest Count value
    ''' </summary>
    <Display(Name:="Guest Count", Description:="")>
    Public ReadOnly Property GuestCount() As Integer
      Get
        Return GetProperty(GuestCountProperty)
      End Get
    End Property

    Public Shared NightsAwayProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NightsAway, "Nights Away", Nothing)
    ''' <summary>
    ''' Gets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Nights Away", Description:="Nights Away")>
    Public ReadOnly Property NightsAway() As Integer
      Get
        Return DateDiff(DateInterval.Day, CheckInDate, CheckOutDate)
      End Get
    End Property

    Public Shared CancelledIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CancelledInd, "Cancelled Indicator", False)
    ''' <summary>
    ''' Gets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:="")>
    Public ReadOnly Property CancelledInd() As Boolean
      Get
        If CancelledByUserID IsNot Nothing Then
          Return True
        End If
        Return False
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROTravellerAccommodationGuestListProperty As PropertyInfo(Of ROTravellerAccommodationGuestList) = RegisterProperty(Of ROTravellerAccommodationGuestList)(Function(c) c.ROTravellerAccommodationGuestList, "RO Traveller Accommodation Guest List")

    Public ReadOnly Property ROTravellerAccommodationGuestList() As ROTravellerAccommodationGuestList
      Get
        If GetProperty(ROTravellerAccommodationGuestListProperty) Is Nothing Then
          LoadProperty(ROTravellerAccommodationGuestListProperty, Travel.Accommodation.ReadOnly.ROTravellerAccommodationGuestList.NewROTravellerAccommodationGuestList())
        End If
        Return GetProperty(ROTravellerAccommodationGuestListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AccommodationRefNo

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerAccommodation(dr As SafeDataReader) As ROTravellerAccommodation

      Dim r As New ROTravellerAccommodation()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AccommodationIDProperty, .GetInt32(0))
        LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(AccommodationProviderIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CheckInDateProperty, .GetValue(3))
        LoadProperty(CheckOutDateProperty, .GetValue(4))
        LoadProperty(AccommodationRefNoProperty, .GetString(5))
        LoadProperty(BookingReasonProperty, .GetString(6))
        LoadProperty(CancelledDateTimeProperty, .GetValue(7))
        LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(CancelledReasonProperty, .GetString(9))
        LoadProperty(CreatedByProperty, .GetInt32(10))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(11))
        LoadProperty(ModifiedByProperty, .GetInt32(12))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(13))
        LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(GuestCountProperty, .GetInt32(15))
      End With

    End Sub

#End Region

  End Class

End Namespace
