﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.Accommodation

  <Serializable()> _
  Public Class AccommodationList
    Inherits OBBusinessListBase(Of AccommodationList, Accommodation)

#Region " Business Methods "

    Public Function GetItem(AccommodationID As Integer) As Accommodation

      For Each child As Accommodation In Me
        If child.AccommodationID = AccommodationID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Accommodations"

    End Function

    Public Function GetAccommodationHumanResource(AccommodationHumanResourceID As Integer) As AccommodationHumanResource

      Dim obj As AccommodationHumanResource = Nothing
      For Each parent As Accommodation In Me
        obj = parent.AccommodationHumanResourceList.GetItem(AccommodationHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Sub UpdateTravelRequisitionID(TravelRequisitionID As Integer)

      Me.ToList.ForEach(Sub(z)
                          If z.IsNew Then
                            z.TravelRequisitionID = TravelRequisitionID
                          End If
                        End Sub)

    End Sub

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property AccommodationID As Integer? = Nothing

      Public Sub New(TravelRequisitionID As Integer?, AccommodationID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        Me.AccommodationID = AccommodationID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewAccommodationList() As AccommodationList

      Return New AccommodationList()

    End Function

    Public Shared Sub BeginGetAccommodationList(CallBack As EventHandler(Of DataPortalResult(Of AccommodationList)))

      Dim dp As New DataPortal(Of AccommodationList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAccommodationList() As AccommodationList

      Return DataPortal.Fetch(Of AccommodationList)(New Criteria())

    End Function

    Public Shared Function GetAccommodationList(TravelRequisitionID As Integer?, AccommodationID As Integer?) As AccommodationList

      Return DataPortal.Fetch(Of AccommodationList)(New Criteria(TravelRequisitionID, AccommodationID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(Accommodation.GetAccommodation(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As Accommodation = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AccommodationID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.AccommodationHumanResourceList.RaiseListChangedEvents = False
          parent.AccommodationHumanResourceList.Add(AccommodationHumanResource.GetAccommodationHumanResource(sdr))
          parent.AccommodationHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AccommodationID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.AccommodationSupplierHumanResourceList.RaiseListChangedEvents = False
          parent.AccommodationSupplierHumanResourceList.Add(AccommodationSupplierHumanResource.GetAccommodationSupplierHumanResource(sdr))
          parent.AccommodationSupplierHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As Accommodation In Me
        child.CheckRules()
        For Each ahr As AccommodationHumanResource In child.AccommodationHumanResourceList
          ahr.CheckRules()
        Next

        For Each ashr As AccommodationSupplierHumanResource In child.AccommodationSupplierHumanResourceList
          ashr.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAccommodationList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@AccommodationID", NothingDBNull(crit.AccommodationID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As Accommodation In DeletedList
          Child.DeleteSelf()
        Next
        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As Accommodation In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

    'Private Sub DeleteBulk()

    '  'Dim dt As DataTable = CreateCrewScheduleTableParameter()
    '  Using cm As SqlCommand = New SqlCommand
    '    cm.CommandText = "DelProcsWeb.delAccommodationListBulk"
    '    cm.CommandType = CommandType.StoredProcedure
    '    Dim sqlP As SqlClient.SqlParameter = New SqlClient.SqlParameter("@AccommodationIDs", SqlDbType.VarChar)
    '    sqlP.Value = OBMisc.IntegerListToXML(Me.DeletedList.Select(Function(d) d.AccommodationID).ToList)
    '    cm.Connection = CType(Csla.ApplicationContext.LocalContext("cn"), SqlClient.SqlConnection)
    '    cm.Parameters.Add(sqlP)
    '    cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
    '    cm.Transaction = Csla.ApplicationContext.LocalContext("tr")
    '    Singular.CSLALib.ContextInfo.SetContextInfoOnConnection(cm.Connection, cm.Transaction)
    '    cm.ExecuteNonQuery()
    '  End Using

    'End Sub


#End If

#End Region

#End Region

  End Class

End Namespace