﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Travel.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.Web

Namespace Travel.Accommodation

  <Serializable()> _
  Public Class Accommodation
    Inherits OBBusinessBase(Of Accommodation)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return AccommodationBO.toString(self)")

    Public Shared AccommodationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property AccommodationID() As Integer
      Get
        Return GetProperty(AccommodationIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelRequisitionID, "Travel Requisition", Nothing)
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    Public Property TravelRequisitionID() As Integer?
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(TravelRequisitionIDProperty, value)
      End Set
    End Property

    Public Shared AccommodationProviderIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccommodationProviderID, "Accommodation Provider", Nothing)
    ''' <summary>
    ''' Gets and sets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Provider", Description:="The hotel/provider of the accommodation"),
    Required(ErrorMessage:="Accommodation Provider required"),
    DropDownWeb(GetType(ROAccommodationProviderList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="AccommodationBO.setAccommodationProviderIDCriteriaBeforeRefresh",
                PreFindJSFunction:="AccommodationBO.triggerAccommodationProviderIDAutoPopulate",
                AfterFetchJS:="AccommodationBO.afterAccommodationProviderIDRefreshAjax",
                LookupMember:="AccommodationProvider", ValueMember:="AccommodationProviderID", DisplayMember:="AccommodationProvider", DropDownColumns:={"AccommodationProvider"},
                OnItemSelectJSFunction:="AccommodationBO.onAccommodationProviderSelected", DropDownCssClass:="room-dropdown")>
    Public Property AccommodationProviderID() As Integer?
      Get
        Return GetProperty(AccommodationProviderIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccommodationProviderIDProperty, Value)
      End Set
    End Property

    Public Shared AccommodationProviderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccommodationProvider, "Accommodation Provider", "")
    ''' <summary>
    ''' Gets and sets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Provider", Description:="The hotel/provider of the accommodation")>
    Public Property AccommodationProvider() As String
      Get
        Return GetProperty(AccommodationProviderProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AccommodationProviderProperty, Value)
      End Set
    End Property

    'Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Travel.ReadOnly.ROAccommodationProviderList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)

    Public Shared CheckInDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.CheckInDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Check In Date value
    ''' </summary>
    <Display(Name:="Check In Date", Description:="Date of check in"),
    Required(ErrorMessage:="Check In Date required"), Singular.DataAnnotations.DateField(InitialDateFunction:="AccommodationBO.InitDate($data)"),
    SetExpression("AccommodationBO.CheckInDateSet(self)")>
    Public Property CheckInDate As DateTime?
      Get
        Return GetProperty(CheckInDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CheckInDateProperty, Value)
        SetNightsAway()
      End Set
    End Property

    Public Shared CheckOutDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.CheckOutDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Check Out Date value
    ''' </summary>
    <Display(Name:="Check Out Date", Description:="Date of check out"),
    Required(ErrorMessage:="Check Out Date required"), Singular.DataAnnotations.DateField(InitialDateFunction:="AccommodationBO.InitDate($data)"),
    SetExpression("AccommodationBO.CheckOutDateSet(self)")>
    Public Property CheckOutDate As DateTime?
      Get
        Return GetProperty(CheckOutDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CheckOutDateProperty, Value)
        SetNightsAway()
      End Set
    End Property

    Public Shared AccommodationRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccommodationRefNo, "Accommodation Ref No", "")
    ''' <summary>
    ''' Gets and sets the Accommodation Ref No value
    ''' </summary>
    <Display(Name:="Ref No", Description:="Reference Number to identify the accommodation"),
    StringLength(15, ErrorMessage:="Accommodation Ref No cannot be more than 15 characters")>
    Public Property AccommodationRefNo() As String
      Get
        Return GetProperty(AccommodationRefNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AccommodationRefNoProperty, Value)
      End Set
    End Property

    Public Shared BookingReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingReason, "Booking Reason", "")
    ''' <summary>
    ''' Gets and sets the Booking Reason value
    ''' </summary>
    <Display(Name:="Booking Reason", Description:="Reason required when Human Resource is booked accommodation in base city"),
    StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters")>
    Public Property BookingReason() As String
      Get
        Return GetProperty(BookingReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BookingReasonProperty, Value)
      End Set
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets and sets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="When the booking was cancelled")>
    Public Property CancelledDateTime As DateTime?
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancelledDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="Who cancelled the booking")>
    Public Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CancelledByUserIDProperty, Value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="Why the booking was cancelled"),
    StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters"),
    SetExpression("AccommodationBO.CancelledReasonSet(self)")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
        UpdateCancelledReasonAHRList(Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Cancelled Indicator", False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:=""),
    SetExpression("AccommodationBO.IsCancelledSet(self)")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
        UpdateCancelledDetails(Value)
        UpdateIsCancelledAHRList(Value)
      End Set
    End Property

    Public Shared NightsAwayProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NightsAway, "Nights Away", 0)
    ''' <summary>
    ''' Gets and sets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Nights Away", Description:="Nights Away")>
    Public Property NightsAway() As Integer
      Get
        Return GetProperty(NightsAwayProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(NightsAwayProperty, Value)
      End Set
    End Property

    Public Shared GuestCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GuestCount, "Guest Count", 0)
    ''' <summary>
    ''' Gets and sets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Guest Count", Description:="Guest Count")>
    Public Property GuestCount() As Integer
      Get
        Return GetProperty(GuestCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(GuestCountProperty, value)
      End Set
    End Property

    Public Shared PerPersonPerNightCostProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.PerPersonPerNightCost, "Per Person Per Night Cost", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Room Cost value
    ''' </summary>
    <Display(Name:="Per Person Per Night Cost", Description:="Per Person Per Night Cost"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public Property PerPersonPerNightCost() As Decimal
      Get
        If AccommodationHumanResourceList.Count > 0 Then
          Dim Divisor As Integer = AccommodationHumanResourceList.Count
          If Divisor = 0 Or NightsAway = 0 Then
            Return 0
          End If
          Return (AccommodationHumanResourceList.Sum(Function(d) d.RoomCost)) / (Divisor * NightsAway)

        ElseIf AccommodationSupplierHumanResourceList.Count > 0 Then
          Dim Divisor As Integer = AccommodationSupplierHumanResourceList.Count
          If Divisor = 0 Or NightsAway = 0 Then
            Return 0
          End If
          Return (AccommodationSupplierHumanResourceList.Sum(Function(d) d.RoomCost)) / (Divisor * NightsAway)

        Else
          Return 0.0
        End If
        Return GetProperty(PerPersonPerNightCostProperty)
      End Get
      Set(ByVal Value As Decimal)
        CalculateRoomCosts(Value)
        SetProperty(PerPersonPerNightCostProperty, Value)
      End Set
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets and sets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Supplier", Description:="The supplier that needs accommodation booked for"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSupplierList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierIDProperty, Value)
      End Set
    End Property

    Public Shared AccomProviderCityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccomProviderCityID, "Accomm. Provider City ID", Nothing)
    ''' <summary>
    ''' Gets the Accomm. Provider City ID value
    ''' </summary>
    <Display(Name:="Accomm. Provider City ID", Description:="")>
    Public ReadOnly Property AccomProviderCityID() As Integer?
      Get
        Return GetProperty(AccomProviderCityIDProperty)
      End Get
    End Property

    Public Shared HRClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HRClashCount, "HR Clash Count", 0)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="HR Clash Count", Description:="")>
    Public Property HRClashCount() As Integer
      Get
        Return GetProperty(HRClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HRClashCountProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared AccommodationHumanResourceListProperty As PropertyInfo(Of AccommodationHumanResourceList) = RegisterProperty(Of AccommodationHumanResourceList)(Function(c) c.AccommodationHumanResourceList, "Accommodation Human Resource List")

    Public ReadOnly Property AccommodationHumanResourceList() As AccommodationHumanResourceList
      Get
        If GetProperty(AccommodationHumanResourceListProperty) Is Nothing Then
          LoadProperty(AccommodationHumanResourceListProperty, OBLib.Travel.Accommodation.AccommodationHumanResourceList.NewAccommodationHumanResourceList())
        End If
        Return GetProperty(AccommodationHumanResourceListProperty)
      End Get
    End Property

    Public Shared AccommodationSupplierHumanResourceListProperty As PropertyInfo(Of AccommodationSupplierHumanResourceList) = RegisterProperty(Of AccommodationSupplierHumanResourceList)(Function(c) c.AccommodationSupplierHumanResourceList, "Accommodation Supplier Human Resource List")

    Public ReadOnly Property AccommodationSupplierHumanResourceList() As AccommodationSupplierHumanResourceList
      Get
        If GetProperty(AccommodationSupplierHumanResourceListProperty) Is Nothing Then
          LoadProperty(AccommodationSupplierHumanResourceListProperty, OBLib.Travel.Accommodation.AccommodationSupplierHumanResourceList.NewAccommodationSupplierHumanResourceList())
        End If
        Return GetProperty(AccommodationSupplierHumanResourceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As OBLib.Travel.TravelRequisition

      Return CType(CType(Me.Parent, AccommodationList).Parent, OBLib.Travel.TravelRequisition)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AccommodationID = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Accommodation")
        Else
          Return String.Format("Blank {0}", "Accommodation")
        End If
      Else
        Return Me.AccommodationID
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"AccommodationHumanResources"}
      End Get
    End Property

    Private Sub UpdateCancelledDetails(Value As Boolean)

      If Value = True Then
        SetProperty(CancelledByUserIDProperty, Security.Settings.CurrentUser.UserID)
        SetProperty(CancelledDateTimeProperty, Date.Now)
      Else
        SetProperty(CancelledByUserIDProperty, Nothing)
        SetProperty(CancelledDateTimeProperty, Nothing)
        SetProperty(CancelledReasonProperty, "")
      End If

    End Sub

    Private Sub UpdateIsCancelledAHRList(Value As Boolean)

      AccommodationHumanResourceList.ToList.ForEach(Sub(z)
                                                      z.IsCancelled = Value
                                                    End Sub)

    End Sub

    Private Sub UpdateCancelledReasonAHRList(Reason As String)

      AccommodationHumanResourceList.ToList.ForEach(Sub(z)
                                                      z.CancelledReason = Reason
                                                    End Sub)

    End Sub

    Private Sub SetNightsAway()

      If Me.IsValid AndAlso CheckInDate IsNot Nothing AndAlso CheckOutDate IsNot Nothing Then
        Dim DayCount As Integer = DateDiff(DateInterval.Day, CheckInDate.Value, CheckOutDate.Value)
        DayCount = If(DayCount <> 0, DayCount, 1)
        NightsAway = If(DayCount < 0, 0, DayCount)
      Else
        NightsAway = 0
      End If

    End Sub

    Public Sub CalculateRoomCosts(value As Decimal)

      'Parallel.ForEach(mAccommodationGuestList, Sub(guest As AccommodationGuest)
      '                                            guest.SetAccommodationCost(value, NightsAway)
      '                                          End Sub)
      If SupplierID Is Nothing Then
        AccommodationHumanResourceList.ToList.ForEach(Sub(z)
                                                        z.SetAccommodationCost(value, NightsAway)
                                                      End Sub)

      Else
        AccommodationSupplierHumanResourceList.ToList.ForEach(Sub(z)
                                                                z.SetAccommodationCost(value, NightsAway)
                                                              End Sub)

      End If


    End Sub

    Public Sub SetGuestCount()
      If Me.AccommodationHumanResourceList.Count > 0 Then
        LoadProperty(GuestCountProperty, Me.AccommodationHumanResourceList.Where(Function(c) c.CancelledByUserID Is Nothing _
                                                                         AndAlso c.AccommodationHumanResourceID <> 0).Count)
      ElseIf Me.AccommodationSupplierHumanResourceList.Count > 0 Then
        LoadProperty(GuestCountProperty, Me.AccommodationSupplierHumanResourceList.Count)
      End If
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(CancelledReasonProperty)
        .JavascriptRuleFunctionName = "AccommodationBO.CancelledReasonValid"
        .ServerRuleFunction = AddressOf CancelledReasonRequired
        .AddTriggerProperty(IsCancelledProperty)
      End With

      With AddWebRule(CheckInDateProperty)
        .AddTriggerProperty(CheckOutDateProperty)
        .AffectedProperties.Add(CheckOutDateProperty)
        .JavascriptRuleFunctionName = "AccommodationBO.AccommodationDatesValid"
        .ServerRuleFunction = AddressOf AccommodationDatesValid
      End With

      With AddWebRule(HRClashCountProperty)
        .JavascriptRuleFunctionName = "AccommodationBO.HRClashCountValid"
        .ServerRuleFunction = AddressOf HRClashCountValid
      End With

    End Sub

    Public Shared Function CancelledReasonRequired(acc As Accommodation) As String
      Dim ErrorString = ""
      If acc.IsCancelled AndAlso acc.CancelledReason.Trim = "" Then
        ErrorString = "Cancelled Reason Required"
      End If
      Return ErrorString
    End Function

    Public Shared Function AccommodationDatesValid(acc As Accommodation) As String
      Dim ErrorString = ""
      If acc.CheckInDate IsNot Nothing AndAlso acc.CheckOutDate IsNot Nothing Then
        Dim CheckInDate = acc.CheckInDate
        Dim CheckOutDate = acc.CheckOutDate
        Dim result As Integer = DateTime.Compare(CheckInDate, CheckOutDate)
        If result > 0 Then
          ErrorString = "Check In Date must be before Check Out Date"
        End If
      End If
      Return ErrorString
    End Function

    Public Function HRClashCountValid(Accommodation As Accommodation) As String

      Dim ErrorMessage As String = ""
      If Accommodation.HRClashCount > 0 Then
        ErrorMessage = "There are guest clashes on this accommodation"
      End If

      Return ErrorMessage

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccommodation() method.

    End Sub

    Public Shared Function NewAccommodation() As Accommodation

      Return DataPortal.CreateChild(Of Accommodation)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccommodation(dr As SafeDataReader) As Accommodation

      Dim a As New Accommodation()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccommodationIDProperty, .GetInt32(0))
          LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AccommodationProviderIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CheckInDateProperty, .GetValue(3))
          LoadProperty(CheckOutDateProperty, .GetValue(4))
          LoadProperty(AccommodationRefNoProperty, .GetString(5))
          LoadProperty(BookingReasonProperty, .GetString(6))
          LoadProperty(CancelledDateTimeProperty, .GetValue(7))
          LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(CancelledReasonProperty, .GetString(9))
          LoadProperty(CreatedByProperty, .GetInt32(10))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(11))
          LoadProperty(ModifiedByProperty, .GetInt32(12))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(13))
          LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(AccommodationProviderProperty, .GetString(15))
          LoadProperty(GuestCountProperty, .GetInt32(16))
          LoadProperty(IsCancelledProperty, IIf(CancelledByUserID Is Nothing, False, True))
          LoadProperty(HRClashCountProperty, .GetInt32(17))
        End With
      End Using

      SetNightsAway()
      SetGuestCount()
      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccommodation"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccommodation"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccommodationID As SqlParameter = .Parameters.Add("@AccommodationID", SqlDbType.Int)
          paramAccommodationID.Value = GetProperty(AccommodationIDProperty)
          If Me.IsNew Then
            paramAccommodationID.Direction = ParameterDirection.Output
          End If
          If Me.GetParent IsNot Nothing Then
            SetProperty(TravelRequisitionIDProperty, Me.GetParent.TravelRequisitionID)
            .Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
          Else
            .Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(GetProperty(TravelRequisitionIDProperty)))
          End If
          .Parameters.AddWithValue("@AccommodationProviderID", GetProperty(AccommodationProviderIDProperty))
          .Parameters.AddWithValue("@CheckInDate", (New SmartDate(GetProperty(CheckInDateProperty))).DBValue)
          .Parameters.AddWithValue("@CheckOutDate", (New SmartDate(GetProperty(CheckOutDateProperty))).DBValue)
          .Parameters.AddWithValue("@AccommodationRefNo", GetProperty(AccommodationRefNoProperty))
          .Parameters.AddWithValue("@BookingReason", GetProperty(BookingReasonProperty))
          .Parameters.AddWithValue("@CancelledDateTime", (New SmartDate(GetProperty(CancelledDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledByUserID", Singular.Misc.NothingDBNull(GetProperty(CancelledByUserIDProperty)))
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@ModifiedBy", ZeroDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@SupplierID", NothingDBNull(GetProperty(SupplierIDProperty)))
          .Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@GuestsLoaded", (Me.AccommodationHumanResourceList.Count > 0))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccommodationIDProperty, paramAccommodationID.Value)
          End If
          ' update child objects
          If GetProperty(AccommodationHumanResourceListProperty) IsNot Nothing Then
            Me.AccommodationHumanResourceList.Update()
          End If
          If GetProperty(AccommodationSupplierHumanResourceListProperty) IsNot Nothing Then
            Me.AccommodationSupplierHumanResourceList.Update()
          End If
          SetGuestCount()
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(AccommodationHumanResourceListProperty) IsNot Nothing Then
          Me.AccommodationHumanResourceList.Update()
        End If
        If GetProperty(AccommodationSupplierHumanResourceListProperty) IsNot Nothing Then
          Me.AccommodationSupplierHumanResourceList.Update()
        End If
        SetGuestCount()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccommodation"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccommodationID", GetProperty(AccommodationIDProperty))
        cm.Parameters.AddWithValue("@TravelRequisitionID", GetProperty(TravelRequisitionIDProperty))
        cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub



    Private Shared Function CreateRentalCarClashesTableParameter(AccommodationHumanResourceList As AccommodationHumanResourceList) As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("TravelRequisitionID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("AccommodationID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("AccommodationHumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("CheckInDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("CheckOutDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IsCancelled", GetType(System.Boolean)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As AccommodationHumanResource In AccommodationHumanResourceList
        Dim row As DataRow = RBTable.NewRow
        row("ObjectGuid") = rb.Guid
        row("TravelRequisitionID") = rb.GetParent.TravelRequisitionID
        row("AccommodationID") = NothingDBNull(rb.AccommodationID)
        row("AccommodationHumanResourceID") = ZeroDBNull(rb.AccommodationHumanResourceID)
        row("HumanResourceID") = rb.HumanResourceID
        row("CheckInDate") = rb.GetParent.CheckInDate
        row("CheckOutDate") = rb.GetParent.CheckOutDate
        row("IsCancelled") = rb.IsCancelled
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

    Private Function GetRentalCarHRClashesDataSet() As DataSet

      Dim RBTable As DataTable = CreateRentalCarClashesTableParameter(Me.AccommodationHumanResourceList)
      Dim cmd As New Singular.CommandProc("GetProcsWeb.getROAccommodationClashList")
      Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      ResourceBookingsParam.Name = "@AccommodationHumanResources"
      ResourceBookingsParam.SqlType = SqlDbType.Structured
      ResourceBookingsParam.Value = RBTable
      cmd.Parameters.Add(ResourceBookingsParam)
      cmd.Parameters.AddWithValue("@PassengersLoaded", (Me.AccommodationHumanResourceList.Count > 0))
      cmd.Parameters.AddWithValue("@CheckInDate", NothingDBNull(Me.CheckInDate))
      cmd.Parameters.AddWithValue("@CheckOutDate", NothingDBNull(Me.CheckOutDate))
      cmd.Parameters.AddWithValue("@IsCancelled", Me.IsCancelled)
      cmd.Parameters.AddWithValue("@AccommodationID", ZeroDBNull(Me.AccommodationID))
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute(0)
      Return cmd.Dataset

    End Function

    Public Function UpdateClashes() As Singular.Web.Result

      Try
        'get clash data
        Dim ds As DataSet = GetRentalCarHRClashesDataSet()

        'reset clashes
        Me.HRClashCount = 0
        Me.AccommodationHumanResourceList.ToList.ForEach(Sub(c As AccommodationHumanResource)
                                                           c.HasClashes = False
                                                           c.OtherAccommodationShort = ""
                                                           c.OtherAccommodation = ""
                                                         End Sub)

        'update rental car

        'update rentalcar humanresources
        If Me.AccommodationHumanResourceList.Count > 0 Then
          For Each dr As DataRow In ds.Tables(0).Rows
            Dim rb As AccommodationHumanResource = Me.AccommodationHumanResourceList.Where(Function(d) d.Guid = dr(0)).FirstOrDefault
            If rb IsNot Nothing Then
              rb.HasClashes = True
              rb.OtherAccommodationShort = dr(1)
              rb.OtherAccommodation = dr(2)
            End If
            If rb.OnAccommodationInd Then
              Me.HRClashCount += 1
            End If
          Next
        Else
          Me.HRClashCount = ds.Tables(0).Rows.Count
        End If

        Return New Singular.Web.Result(True)

      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

    End Function

#End If

#End Region

#End Region

  End Class

End Namespace