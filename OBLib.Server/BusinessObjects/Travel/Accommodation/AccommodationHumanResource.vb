﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.Accommodation

  <Serializable()> _
  Public Class AccommodationHumanResource
    Inherits OBBusinessBase(Of AccommodationHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return AccommodationGuestBO.toString(self)")

    <SetExpression("AccommodationGuestBO.IsSelectedSet(self)")>
    Public Overrides Property IsSelected() As Boolean
      Get
        Return GetProperty(IsSelectedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsSelectedProperty, value)
      End Set
    End Property

    Public Shared AccommodationHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property AccommodationHumanResourceID() As Integer
      Get
        Return GetProperty(AccommodationHumanResourceIDProperty)
      End Get
    End Property

    Public Shared AccommodationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccommodationID, "Accommodation", Nothing)
    ''' <summary>
    ''' Gets the Accommodation value
    ''' </summary>
    Public Property AccommodationID() As Integer?
      Get
        Return GetProperty(AccommodationIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(AccommodationIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The link to the human resource on the accommodation"),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared BookingReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingReason, "Booking Reason", "")
    ''' <summary>
    ''' Gets and sets the Booking Reason value
    ''' </summary>
    <Display(Name:="Booking Reason", Description:="Reason for booking this HR on the accommodation. If required."),
    StringLength(200, ErrorMessage:="Booking Reason cannot be more than 200 characters")>
    Public Property BookingReason() As String
      Get
        Return GetProperty(BookingReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BookingReasonProperty, Value)
      End Set
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets and sets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="When the booking was cancelled")>
    Public Property CancelledDateTime As DateTime?
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancelledDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="Who cancelled the booking")>
    Public Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CancelledByUserIDProperty, Value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="Why the booking was cancelled"),
    StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters"),
    SetExpression("AccommodationGuestBO.CancelledReasonSet(self)")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
      End Set
    End Property

    Public Shared RoomCostProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RoomCost, "Room Cost", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Room Cost value
    ''' </summary>
    <Display(Name:="Room Cost", Description:="Cost of the room for the Human Resource"),
    Required(ErrorMessage:="Room Cost required")>
    Public Property RoomCost() As Decimal
      Get
        Return GetProperty(RoomCostProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RoomCostProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Cancelled Indicator", False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:=""),
    SetExpression("AccommodationGuestBO.IsCancelledSet(self)")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
        'UpdateCancelledDetails(Value)
      End Set
    End Property

    Public Shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceName, "Human Resource Name", "")
    ''' <summary>
    ''' Gets and sets the HR Name value
    ''' </summary>
    <Display(Name:="Human Resource Name", Description:="Human Resource Name")>
    Public Property HumanResourceName() As String
      Get
        Return GetProperty(HumanResourceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceNameProperty, Value)
      End Set
    End Property

    Public Shared BaseCityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.BaseCityID, "BaseCityID", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="BaseCityID")>
    Public Property BaseCityID() As Integer?
      Get
        Return GetProperty(BaseCityIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(BaseCityIDProperty, Value)
      End Set
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City Code", "")
    ''' <summary>
    ''' Gets and sets the City Code value
    ''' </summary>
    <Display(Name:="City Code", Description:="")>
    Public Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CityCodeProperty, Value)
      End Set
    End Property

    Public Shared OtherAccommodationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OtherAccommodation, "Other Accommodation", "")
    ''' <summary>
    ''' Gets and sets the City Code value
    ''' </summary>
    <Display(Name:="Other Accommodation", Description:="")>
    Public Property OtherAccommodation() As String
      Get
        Return GetProperty(OtherAccommodationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OtherAccommodationProperty, Value)
      End Set
    End Property

    Public Shared DisciplinesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Disciplines, "Disciplines", "")
    ''' <summary>
    ''' Gets and sets the Booking Reason value
    ''' </summary>
    <Display(Name:="Disciplines", Description:="")>
    Public Property Disciplines() As String
      Get
        Return GetProperty(DisciplinesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplinesProperty, Value)
      End Set
    End Property

    Public Shared OnAccommodationIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnAccommodationInd, "OnAccommodationInd", False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="OnAccommodationInd", Description:=""),
    SetExpression("AccommodationGuestBO.OnAccommodationIndSet(self)")>
    Public Property OnAccommodationInd() As Boolean
      Get
        Return GetProperty(OnAccommodationIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OnAccommodationIndProperty, Value)
        'UpdateCancelledDetails(Value)
      End Set
    End Property

    Public Shared OtherAccommodationShortProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OtherAccommodationShort, "Other Accommodation", "")
    ''' <summary>
    ''' Gets and sets the City Code value
    ''' </summary>
    <Display(Name:="Other Accommodation", Description:="")>
    Public Property OtherAccommodationShort() As String
      Get
        Return GetProperty(OtherAccommodationShortProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OtherAccommodationShortProperty, Value)
      End Set
    End Property

    Public Shared HasClashesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasClashes, "Has Clashes", False)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="Has Clashes", Description:="")>
    Public Property HasClashes() As Boolean
      Get
        Return GetProperty(HasClashesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasClashesProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Accommodation

      If Me.Parent Is Nothing Then
        Return Nothing
      Else
        Return CType(CType(Me.Parent, AccommodationHumanResourceList).Parent, Accommodation)
      End If

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AccommodationHumanResourceID = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Accommodation Human Resource")
        Else
          Return String.Format(Me.HumanResourceName)
        End If
      Else
        Return Me.AccommodationHumanResourceID
      End If

    End Function

    Private Sub UpdateCancelledDetails(Value As Boolean)

      If Value = True Then
        SetProperty(CancelledByUserIDProperty, Security.Settings.CurrentUser.UserID)
        SetProperty(CancelledDateTimeProperty, Date.Now)
      Else
        SetProperty(CancelledDateTimeProperty, Nothing)
        SetProperty(CancelledByUserIDProperty, Nothing)
        SetProperty(CancelledReasonProperty, "")
      End If

    End Sub

    Public Sub SetAccommodationCost(PerPersonPerNightCost As Decimal, NightsAway As Decimal)

      RoomCost = PerPersonPerNightCost * NightsAway

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(CancelledReasonProperty)
        .JavascriptRuleFunctionName = "AccommodationGuestBO.CancelledReasonValid"
        .ServerRuleFunction = AddressOf CancelledReasonValid
      End With

      With AddWebRule(BookingReasonProperty)
        .AddTriggerProperty(BaseCityIDProperty)
        .JavascriptRuleFunctionName = "AccommodationGuestBO.BookingReasonValid"
        .ServerRuleFunction = AddressOf BookingReasonValid
      End With

      With AddWebRule(OnAccommodationIndProperty)
        .AddTriggerProperty(OnAccommodationIndProperty)
        .JavascriptRuleFunctionName = "AccommodationGuestBO.IsOnAccommodationValid"
        .ServerRuleFunction = AddressOf IsOnAccommodationValid
      End With

    End Sub

    Public Shared Function CancelledReasonValid(AccommodationHumanResource As AccommodationHumanResource) As String
      Dim ErrorString = ""
      If AccommodationHumanResource.IsCancelled AndAlso AccommodationHumanResource.CancelledReason.Trim = "" Then
        ErrorString = "Cancelled Reason Required"
      End If
      Return ErrorString
    End Function

    Public Shared Function BookingReasonValid(AccommodationHumanResource As AccommodationHumanResource) As String
      Dim ErrorString = ""
      If AccommodationHumanResource.GetParent IsNot Nothing AndAlso CompareSafe(AccommodationHumanResource.BaseCityID, AccommodationHumanResource.GetParent.AccomProviderCityID) Then
        ErrorString = "Booking Reason Required: " + AccommodationHumanResource.HumanResourceName() + "'s Base City is the same as Accommodation Provider's City"
      End If
      Return ErrorString
    End Function

    Public Shared Function IsOnAccommodationValid(AccommodationHumanResource As AccommodationHumanResource) As String
      Dim ErrorString = ""
      If AccommodationHumanResource.OnAccommodationInd AndAlso Not AccommodationHumanResource.OtherAccommodation.Trim = "" Then
        ErrorString = AccommodationHumanResource.HumanResourceName & " has accommodation clashes"
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccommodationHumanResource() method.

    End Sub

    Public Shared Function NewAccommodationHumanResource() As AccommodationHumanResource

      Return DataPortal.CreateChild(Of AccommodationHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccommodationHumanResource(dr As SafeDataReader) As AccommodationHumanResource

      Dim a As New AccommodationHumanResource()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccommodationHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(AccommodationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(BookingReasonProperty, .GetString(3))
          LoadProperty(CancelledDateTimeProperty, .GetValue(4))
          LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(CancelledReasonProperty, .GetString(6))
          LoadProperty(RoomCostProperty, .GetDecimal(7))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
          LoadProperty(HumanResourceNameProperty, .GetString(12))
          LoadProperty(IsCancelledProperty, .GetBoolean(13))
          LoadProperty(CityCodeProperty, .GetString(14))
          LoadProperty(OtherAccommodationProperty, .GetString(15))
          LoadProperty(DisciplinesProperty, .GetString(16))
          LoadProperty(IsSelectedProperty, .GetBoolean(17))
          LoadProperty(OnAccommodationIndProperty, .GetBoolean(17))
          LoadProperty(BaseCityIDProperty, NothingDBNull(.GetInt32(18)))
          LoadProperty(OtherAccommodationShortProperty, .GetString(19))
          LoadProperty(HasClashesProperty, .GetBoolean(20))
          LoadProperty(OnAccommodationIndProperty, IsSelected)
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccommodationHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccommodationHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccommodationHumanResourceID As SqlParameter = .Parameters.Add("@AccommodationHumanResourceID", SqlDbType.Int)
          paramAccommodationHumanResourceID.Value = GetProperty(AccommodationHumanResourceIDProperty)
          'If Me.IsNew Then
          paramAccommodationHumanResourceID.Direction = ParameterDirection.InputOutput
          'End If
          .Parameters.AddWithValue("@IsSelected", GetProperty(IsSelectedProperty))
          .Parameters.AddWithValue("@AccommodationID", Me.GetParent().AccommodationID)
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@BookingReason", GetProperty(BookingReasonProperty))
          .Parameters.AddWithValue("@CancelledDateTime", (New SmartDate(GetProperty(CancelledDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledByUserID", Singular.Misc.NothingDBNull(GetProperty(CancelledByUserIDProperty)))
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@RoomCost", GetProperty(RoomCostProperty))
          .Parameters.AddWithValue("@ModifiedBy", ZeroDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@TravelRequisitionID", Me.GetParent().TravelRequisitionID)

          .ExecuteNonQuery()

          'If Me.IsNew Then
          LoadProperty(AccommodationHumanResourceIDProperty, IIf(IsNullNothing(paramAccommodationHumanResourceID.Value), 0, paramAccommodationHumanResourceID.Value))
          'End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccommodationHumanResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccommodationHumanResourceID", GetProperty(AccommodationHumanResourceIDProperty))
        cm.Parameters.AddWithValue("@TravelRequisitionID", Me.GetParent().TravelRequisitionID)
        cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace