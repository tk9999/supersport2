﻿' Generated 05 Aug 2014 22:36 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.Accommodation

  <Serializable()> _
  Public Class AccommodationSupplierHumanResourceList
    Inherits OBBusinessListBase(Of AccommodationSupplierHumanResourceList, AccommodationSupplierHumanResource)

#Region " Business Methods "

    Public Function GetItem(AccommodationSupplierHumanResourceID As Integer) As AccommodationSupplierHumanResource

      For Each child As AccommodationSupplierHumanResource In Me
        If child.AccommodationSupplierHumanResourceID = AccommodationSupplierHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Accommodation Supplier Human Resources"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property AccommodationID As Integer? = Nothing

      Public Sub New()

      End Sub

      Public Sub New(TravelRequisitionID As Integer?, AccommodationID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        Me.AccommodationID = AccommodationID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewAccommodationSupplierHumanResourceList() As AccommodationSupplierHumanResourceList

      Return New AccommodationSupplierHumanResourceList()

    End Function

    Public Shared Sub BeginGetAccommodationSupplierHumanResourceList(CallBack As EventHandler(Of DataPortalResult(Of AccommodationSupplierHumanResourceList)))

      Dim dp As New DataPortal(Of AccommodationSupplierHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAccommodationSupplierHumanResourceList() As AccommodationSupplierHumanResourceList

      Return DataPortal.Fetch(Of AccommodationSupplierHumanResourceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AccommodationSupplierHumanResource.GetAccommodationSupplierHumanResource(sdr))
      End While
      Me.RaiseListChangedEvents = True

      For Each AccommodationSupplierHumanResource As AccommodationSupplierHumanResource In Me
        AccommodationSupplierHumanResource.CheckAllRules()
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAccommodationSupplierHumanResourceList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@AccommodationID", NothingDBNull(crit.AccommodationID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AccommodationSupplierHumanResource In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AccommodationSupplierHumanResource In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace