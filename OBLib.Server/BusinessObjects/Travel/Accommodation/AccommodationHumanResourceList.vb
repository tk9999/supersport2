﻿' Generated 20 Jun 2014 14:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.Accommodation

  <Serializable()> _
  Public Class AccommodationHumanResourceList
    Inherits OBBusinessListBase(Of AccommodationHumanResourceList, AccommodationHumanResource)

#Region " Business Methods "

    Public Function GetItem(AccommodationHumanResourceID As Integer) As AccommodationHumanResource

      For Each child As AccommodationHumanResource In Me
        If child.AccommodationHumanResourceID = AccommodationHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Accommodation Human Resources"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property AccommodationID As Integer? = Nothing

      Public Sub New()

      End Sub

      Public Sub New(TravelRequisitionID As Integer?, AccommodationID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        Me.AccommodationID = AccommodationID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewAccommodationHumanResourceList() As AccommodationHumanResourceList

      Return New AccommodationHumanResourceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AccommodationHumanResource.GetAccommodationHumanResource(sdr))
      End While
      Me.RaiseListChangedEvents = True

      For Each AccommodationHumanResource As AccommodationHumanResource In Me
        AccommodationHumanResource.CheckAllRules()
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAccommodationHumanResourceList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@AccommodationID", NothingDBNull(crit.AccommodationID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AccommodationHumanResource In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AccommodationHumanResource In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace