﻿' Generated 08 Jul 2014 15:54 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Productions.ReadOnly
Imports OBLib.Maintenance.Travel.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.Comments

  <Serializable()> _
  Public Class TravelReqComment
    Inherits OBBusinessBase(Of TravelReqComment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TravelReqCommentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelReqCommentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property TravelReqCommentID() As Integer
      Get
        Return GetProperty(TravelReqCommentIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelRequisitionID, "Travel Requisition", Nothing)
    ''' <summary>
    ''' Gets and sets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="A reference to the travel requisition")>
    Public Property TravelRequisitionID() As Integer?
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TravelRequisitionIDProperty, Value)
      End Set
    End Property

    Public Shared CommentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CommentTypeID, "Comment Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Comment Type value
    ''' </summary>
    <Display(Name:="Comment Type", Description:="Comment type name"),
    Required(ErrorMessage:="Comment Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCommentTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property CommentTypeID() As Integer?
      Get
        Return GetProperty(CommentTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CommentTypeIDProperty, Value)
      End Set
    End Property

    Public Shared RentalCarIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RentalCarID, "Rental Car", Nothing)
    ''' <summary>
    ''' Gets and sets the Rental Car value
    ''' </summary>
    <Display(Name:="Rental Car", Description:="A reference to the rental car"),
    Singular.DataAnnotations.DropDownWeb(GetType(RORentalCarList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel, DisplayMember:="RentalCarRefNo")>
    Public Property RentalCarID() As Integer?
      Get
        Return GetProperty(RentalCarIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RentalCarIDProperty, Value)
      End Set
    End Property

    Public Shared AccommodationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccommodationID, "Accommodation", Nothing)
    ''' <summary>
    ''' Gets and sets the Accommodation value
    ''' </summary>
    <Display(Name:="Accommodation", Description:="A reference to the accommodation"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROAccommodationList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel, DisplayMember:="AccommodationRefNo")>
    Public Property AccommodationID() As Integer?
      Get
        Return GetProperty(AccommodationIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccommodationIDProperty, Value)
      End Set
    End Property

    Public Shared CommentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comment, "Comment", "")
    ''' <summary>
    ''' Gets and sets the Comment value
    ''' </summary>
    <Display(Name:="Comment", Description:="Comment the user needs to make"),
    StringLength(200, ErrorMessage:="Comment cannot be more than 200 characters"),
    Required(ErrorMessage:="Comment Required")>
    Public Property Comment() As String
      Get
        Return GetProperty(CommentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As TravelRequisition

      Return CType(CType(Me.Parent, TravelReqCommentList).Parent, TravelRequisition)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TravelReqCommentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Comment.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Travel Req Comment")
        Else
          Return String.Format("Blank {0}", "Travel Req Comment")
        End If
      Else
        Return Me.Comment
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTravelReqComment() method.

    End Sub

    Public Shared Function NewTravelReqComment() As TravelReqComment

      Return DataPortal.CreateChild(Of TravelReqComment)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTravelReqComment(dr As SafeDataReader) As TravelReqComment

      Dim t As New TravelReqComment()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TravelReqCommentIDProperty, .GetInt32(0))
          LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(CommentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(RentalCarIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(AccommodationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(CommentProperty, .GetString(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTravelReqComment"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTravelReqComment"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTravelReqCommentID As SqlParameter = .Parameters.Add("@TravelReqCommentID", SqlDbType.Int)
          paramTravelReqCommentID.Value = GetProperty(TravelReqCommentIDProperty)
          If Me.IsNew Then
            paramTravelReqCommentID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
          .Parameters.AddWithValue("@CommentTypeID", GetProperty(CommentTypeIDProperty))
          .Parameters.AddWithValue("@RentalCarID", Singular.Misc.NothingDBNull(GetProperty(RentalCarIDProperty)))
          .Parameters.AddWithValue("@AccommodationID", Singular.Misc.NothingDBNull(GetProperty(AccommodationIDProperty)))
          .Parameters.AddWithValue("@Comment", GetProperty(CommentProperty))
          .Parameters.AddWithValue("@ModifiedBy", ZeroDBNull(OBLib.Security.Settings.CurrentUser.UserID))
          .Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(TravelReqCommentIDProperty, paramTravelReqCommentID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTravelReqComment"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TravelReqCommentID", GetProperty(TravelReqCommentIDProperty))
        cm.Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
        cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace