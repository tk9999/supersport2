﻿' Generated 20 May 2016 12:36 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Comments.ReadOnly

  <Serializable()> _
  Public Class ROTravellerTravelReqCommentList
    Inherits OBReadOnlyListBase(Of ROTravellerTravelReqCommentList, ROTravellerTravelReqComment)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "

    Public Function GetItem(TravelReqCommentID As Integer) As ROTravellerTravelReqComment

      For Each child As ROTravellerTravelReqComment In Me
        If child.TravelReqCommentID = TravelReqCommentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing

      Public Sub New(TravelRequisitionID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID

      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROTravellerTravelReqCommentList() As ROTravellerTravelReqCommentList

      Return New ROTravellerTravelReqCommentList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTravellerTravelReqCommentList() As ROTravellerTravelReqCommentList

      Return DataPortal.Fetch(Of ROTravellerTravelReqCommentList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTravellerTravelReqComment.GetROTravellerTravelReqComment(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravellerTravelReqCommentList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace
