﻿' Generated 20 May 2016 12:36 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Comments.ReadOnly

  <Serializable()> _
  Public Class ROTravellerTravelReqComment
    Inherits OBReadOnlyBase(Of ROTravellerTravelReqComment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TravelReqCommentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelReqCommentID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property TravelReqCommentID() As Integer
      Get
        Return GetProperty(TravelReqCommentIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionID, "Travel Requisition")
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="")>
    Public ReadOnly Property TravelRequisitionID() As Integer
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared CommentTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CommentTypeID, "Comment Type")
    ''' <summary>
    ''' Gets the Comment Type value
    ''' </summary>
    <Display(Name:="Comment Type", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Travel.ReadOnly.ROCommentTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public ReadOnly Property CommentTypeID() As Integer
      Get
        Return GetProperty(CommentTypeIDProperty)
      End Get
    End Property

    Public Shared RentalCarIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarID, "Rental Car")
    ''' <summary>
    ''' Gets the Rental Car value
    ''' </summary>
    <Display(Name:="Rental Car", Description:="")>
    Public ReadOnly Property RentalCarID() As Integer
      Get
        Return GetProperty(RentalCarIDProperty)
      End Get
    End Property

    Public Shared AccommodationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationID, "Accommodation")
    ''' <summary>
    ''' Gets the Accommodation value
    ''' </summary>
    <Display(Name:="Accommodation", Description:="")>
    Public ReadOnly Property AccommodationID() As Integer
      Get
        Return GetProperty(AccommodationIDProperty)
      End Get
    End Property

    Public Shared CommentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comment, "Comment")
    ''' <summary>
    ''' Gets the Comment value
    ''' </summary>
    <Display(Name:="Comment", Description:="")>
    Public ReadOnly Property Comment() As String
      Get
        Return GetProperty(CommentProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TravelReqCommentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Comment

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerTravelReqComment(dr As SafeDataReader) As ROTravellerTravelReqComment

      Dim r As New ROTravellerTravelReqComment()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TravelReqCommentIDProperty, .GetInt32(0))
        LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(CommentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(RentalCarIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(AccommodationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(CommentProperty, .GetString(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(9))
      End With

    End Sub

#End Region

  End Class

End Namespace
