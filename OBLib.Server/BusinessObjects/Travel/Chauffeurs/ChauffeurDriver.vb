﻿' Generated 05 May 2015 10:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.Misc

Namespace Travel.Chauffeurs

  <Serializable()> _
  Public Class ChauffeurDriver
    Inherits OBBusinessBase(Of ChauffeurDriver)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ChauffeurDriverBO.toString(self)")

    Public Shared ChauffeurDriverIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChauffeurDriverID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Chauffeur Name"), Key()>
    Public Property ChauffeurDriverID() As Integer
      Get
        Return GetProperty(ChauffeurDriverIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ChauffeurDriverIDProperty, value)
      End Set
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelRequisitionID, "Travel Requisition", Nothing)
    ''' <summary>
    ''' Gets and sets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="Travel requsition ID")>
    Public Property TravelRequisitionID() As Integer?
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(TravelRequisitionIDProperty, value)
      End Set
    End Property

    Public Shared PickUpLocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PickUpLocation, "Pick Up Location", "")
    ''' <summary>
    ''' Gets and sets the Pick Up Location value
    ''' </summary>
    <Display(Name:="Pick Up Location", Description:="Location of the pick up point"),
    Required(ErrorMessage:="Pick Up Location Required"),
    StringLength(255, ErrorMessage:="Pick Up Location cannot be more than 255 characters")>
    Public Property PickUpLocation() As String
      Get
        Return GetProperty(PickUpLocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PickUpLocationProperty, Value)
      End Set
    End Property

    Public Shared DropOffLocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DropOffLocation, "Drop Off Location", "")
    ''' <summary>
    ''' Gets and sets the Drop Off Location value
    ''' </summary>
    <Display(Name:="Drop Off Location", Description:="Location of the drop off point"),
    Required(ErrorMessage:="Drop Off Location Required"),
    StringLength(255, ErrorMessage:="Drop Off Location cannot be more than 255 characters")>
    Public Property DropOffLocation() As String
      Get
        Return GetProperty(DropOffLocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DropOffLocationProperty, Value)
      End Set
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="User ID of the User which cancelled the journey")>
    Public Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CancelledByUserIDProperty, Value)
      End Set
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets and sets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="Date the journey was cancelled")>
    Public Property CancelledDateTime As DateTime?
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancelledDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CancelledReason, "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="Reason the journey was cancelled"),
    StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters"),
    SetExpression("ChauffeurDriverBO.CancelledReasonSet(self)")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
      End Set
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsCancelled, False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:=""),
    SetExpression("ChauffeurDriverBO.IsCancelledSet(self)")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As SmartDate
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDate, "Modified Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDate() As SmartDate
      Get
        Return GetProperty(ModifiedDateProperty)
      End Get
    End Property

    Public Shared PassengerCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PassengerCount, "Passengers", 0)
    ''' <summary>
    ''' Gets the Passenger count value
    ''' </summary>
    <Display(Name:="Passengers", Description:="Passengers")>
    Public Property PassengerCount() As Integer
      Get
        Return GetProperty(PassengerCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(PassengerCountProperty, value)
      End Set
    End Property

    Public Shared PickUpDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.PickUpDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Pick Up Date Time value
    ''' </summary>
    <Display(Name:="PickUp Date ", Description:="Pick Up Date and Time"),
    Required(ErrorMessage:="PickUp Date Time required"), Singular.DataAnnotations.DateField(InitialDateFunction:="ChauffeurDriverBO.InitDate($data)"),
    SetExpression("ChauffeurDriverBO.PickUpDateTimeSet(self)")>
    Public Property PickUpDateTime As DateTime?
      Get
        Return GetProperty(PickUpDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(PickUpDateTimeProperty, Value)
      End Set
    End Property

    Public Shared DropOffDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.DropOffDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Drop Off Date Time value
    ''' </summary>
    <Display(Name:="Drop Off Date", Description:="Drop Off Date and Time"),
    Required(ErrorMessage:="Drop Off Date Time required"), Singular.DataAnnotations.DateField(InitialDateFunction:="ChauffeurDriverBO.InitDate($data)"),
    SetExpression("ChauffeurDriverBO.DropOffDateTimeSet(self)")>
    Public Property DropOffDateTime As DateTime?
      Get
        Return GetProperty(DropOffDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(DropOffDateTimeProperty, Value)
      End Set
    End Property

    Public Shared Property MainPassengerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MainPassenger, "Main Passenger", "")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Main Passenger", Description:=""),
    Required(ErrorMessage:="Main Passenger is required")>
    Public Property MainPassenger() As String
      Get
        Return GetProperty(MainPassengerProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MainPassengerProperty, Value)
      End Set
    End Property

    Public Shared HRClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HRClashCount, "HR Clash Count", 0)
    ''' <summary>
    ''' Gets and sets the On Rental Car value
    ''' </summary>
    <Display(Name:="HR Clash Count", Description:="")>
    Public Property HRClashCount() As Integer
      Get
        Return GetProperty(HRClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HRClashCountProperty, Value)
      End Set
    End Property

    Public Shared MainPassengerChauffeurDriverHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.MainPassengerChauffeurDriverHumanResourceID, "Main Passenger", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Main Passenger", Description:="")>
    Public Property MainPassengerChauffeurDriverHumanResourceID() As Integer?
      Get
        Return GetProperty(MainPassengerChauffeurDriverHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(MainPassengerChauffeurDriverHumanResourceIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ChauffeurDriverHumanResourceListProperty As PropertyInfo(Of ChauffeurDriverHumanResourceList) = RegisterProperty(Of ChauffeurDriverHumanResourceList)(Function(c) c.ChauffeurDriverHumanResourceList, "Chauffeur Driver Human Resource List")
    Public ReadOnly Property ChauffeurDriverHumanResourceList() As ChauffeurDriverHumanResourceList
      Get
        If GetProperty(ChauffeurDriverHumanResourceListProperty) Is Nothing Then
          LoadProperty(ChauffeurDriverHumanResourceListProperty, OBLib.Travel.Chauffeurs.ChauffeurDriverHumanResourceList.NewChauffeurDriverHumanResourceList())
        End If
        Return GetProperty(ChauffeurDriverHumanResourceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As OBLib.Travel.TravelRequisition

      Return CType(CType(Me.Parent, ChauffeurDriverList).Parent, TravelRequisition)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ChauffeurDriverIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ChauffeurDriverID = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Chauffeur Driver")
        Else
          Return String.Format("Blank {0}", "Chauffeur Driver")
        End If
      Else
        Return Me.PickUpLocation
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ChauffeurDriverHumanResources"}
      End Get
    End Property

    Public Sub SetPassengerCount()

      If Me.ChauffeurDriverHumanResourceList.Count > 0 Then
        LoadProperty(PassengerCountProperty, ChauffeurDriverHumanResourceList.Where(Function(d) d.CancelledByUserID Is Nothing _
                                                                                                AndAlso d.ChauffeurDriverHumanResourceID <> 0).Count)


      End If

    End Sub

    Private Shared Function CreateChauffeurClashesTableParameter(ChauffeurDriverHumanResourceList As ChauffeurDriverHumanResourceList) As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("TravelRequisitionID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ChauffeurDriverID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ChauffeurDriverHumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("PickUpDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("DropOffDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IsCancelled", GetType(System.Boolean)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As ChauffeurDriverHumanResource In ChauffeurDriverHumanResourceList
        Dim row As DataRow = RBTable.NewRow
        row("ObjectGuid") = rb.Guid
        row("TravelRequisitionID") = rb.GetParent.TravelRequisitionID
        row("ChauffeurDriverID") = NothingDBNull(rb.ChauffeurDriverID)
        row("ChauffeurDriverHumanResourceID") = ZeroDBNull(rb.ChauffeurDriverHumanResourceID)
        row("HumanResourceID") = rb.HumanResourceID
        row("PickUpDateTime") = rb.GetParent.PickUpDateTime
        row("DropOffDateTime") = rb.GetParent.DropOffDateTime
        row("IsCancelled") = rb.IsCancelled
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

    Private Function GetChauffeurClashesDataSet() As DataSet

      Dim RBTable As DataTable = CreateChauffeurClashesTableParameter(Me.ChauffeurDriverHumanResourceList)
      Dim cmd As New Singular.CommandProc("GetProcsWeb.getROChauffeurClashList")
      Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      ResourceBookingsParam.Name = "@ChauffeurHumanResources"
      ResourceBookingsParam.SqlType = SqlDbType.Structured
      ResourceBookingsParam.Value = RBTable
      cmd.Parameters.Add(ResourceBookingsParam)
      cmd.Parameters.AddWithValue("@PassengersLoaded", (Me.ChauffeurDriverHumanResourceList.Count > 0))
      cmd.Parameters.AddWithValue("@PickUpDateTime", NothingDBNull(Me.PickUpDateTime))
      cmd.Parameters.AddWithValue("@DropOffDateTime", NothingDBNull(Me.DropOffDateTime))
      cmd.Parameters.AddWithValue("@IsCancelled", Me.IsCancelled)
      cmd.Parameters.AddWithValue("@ChauffeurDriverID", ZeroDBNull(Me.ChauffeurDriverID))
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute(0)
      Return cmd.Dataset

    End Function

    Public Function UpdateClashes() As Singular.Web.Result

      Try
        Dim ds As DataSet = GetChauffeurClashesDataSet()
        Me.HRClashCount = 0
        Me.ChauffeurDriverHumanResourceList.ToList.ForEach(Sub(c As ChauffeurDriverHumanResource)
                                                             c.HasClashes = False
                                                             c.OtherChauffeursShort = ""
                                                             c.OtherChauffeurs = ""
                                                           End Sub)

        If Me.ChauffeurDriverHumanResourceList.Count > 0 Then
          For Each dr As DataRow In ds.Tables(0).Rows
            Dim rb As ChauffeurDriverHumanResource = Me.ChauffeurDriverHumanResourceList.Where(Function(d) d.Guid = dr(0)).FirstOrDefault
            If rb IsNot Nothing Then
              rb.HasClashes = True
              rb.OtherChauffeursShort = dr(1)
              rb.OtherChauffeurs = dr(2)
            End If
            If rb.OnCarInd Then
              Me.HRClashCount += 1
            End If
          Next
        Else
          Me.HRClashCount = ds.Tables(0).Rows.Count
        End If

        Return New Singular.Web.Result(True)

      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(DropOffDateTimeProperty)
        .ServerRuleFunction = AddressOf TimesValid
        .AffectedProperties.Add(PickUpDateTimeProperty)
        .AddTriggerProperty(PickUpDateTimeProperty)
        .JavascriptRuleFunctionName = "ChauffeurDriverBO.TimesValid"
      End With

      With AddWebRule(CancelledReasonProperty)
        .AddTriggerProperty(IsCancelledProperty)
        .JavascriptRuleFunctionName = "ChauffeurDriverBO.CancelledReasonValid"
        .ServerRuleFunction = AddressOf CancelledReasonValid
      End With

      With AddWebRule(MainPassengerProperty)
        .AddTriggerProperty(CancelledReasonProperty)
        .AddTriggerProperty(IsCancelledProperty)
        .JavascriptRuleFunctionName = "ChauffeurDriverBO.MainPassengerValid"
        .ServerRuleFunction = AddressOf MainPassengerValid
      End With

      With AddWebRule(HRClashCountProperty)
        .JavascriptRuleFunctionName = "ChauffeurDriverBO.HRClashCountValid"
        .ServerRuleFunction = AddressOf HRClashCountValid
      End With

      'With AddWebRule(DropOffDateTimeProperty)
      '  .ServerRuleFunction = AddressOf MainPassengerValid
      'End With

    End Sub

    Public Function HRClashCountValid(ChauffeurDriver As ChauffeurDriver) As String

      Dim ErrorMessage As String = ""
      If ChauffeurDriver.HRClashCount > 0 Then
        ErrorMessage = "There are passenger clashes on this chauffeur drive"
      End If

      Return ErrorMessage

    End Function

    Private Shared Function CancelledReasonValid(bag As ChauffeurDriver) As String

      Dim ErrorString = ""
      If bag.IsCancelled AndAlso bag.CancelledReason.Trim = "" Then
        ErrorString = "Cancelled Reason Required"
      End If
      Return ErrorString

    End Function

    Private Shared Function MainPassengerValid(bag As ChauffeurDriver) As String

      Dim ErrorString = ""
      If Not bag.IsCancelled AndAlso bag.MainPassenger.Trim = "" Then
        ErrorString = "Main Passenger is required"
      End If
      Return ErrorString

    End Function

    Private Shared Function TimesValid(bag As ChauffeurDriver) As String

      If bag.PickUpDateTime > bag.DropOffDateTime Then
        Return "Pickup Time must be before Dropoff Time"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewChauffeurDriver() method.

    End Sub

    Public Shared Function NewChauffeurDriver() As ChauffeurDriver

      Return DataPortal.CreateChild(Of ChauffeurDriver)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetChauffeurDriver(dr As SafeDataReader) As ChauffeurDriver

      Dim c As New ChauffeurDriver()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ChauffeurDriverIDProperty, .GetInt32(0))
          LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(PickUpLocationProperty, .GetString(2))
          LoadProperty(DropOffLocationProperty, .GetString(3))
          LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(CancelledDateTimeProperty, .GetValue(5))
          LoadProperty(CancelledReasonProperty, .GetString(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateProperty, .GetSmartDate(10))
          LoadProperty(PickUpDateTimeProperty, .GetValue(11))
          LoadProperty(DropOffDateTimeProperty, .GetValue(12))
          LoadProperty(PassengerCountProperty, .GetInt32(13))
          LoadProperty(IsCancelledProperty, IIf(CancelledByUserID Is Nothing, False, True))
          LoadProperty(HRClashCountProperty, .GetInt32(14))
          LoadProperty(MainPassengerProperty, .GetString(15))
          LoadProperty(MainPassengerChauffeurDriverHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insChauffeurDriver"

        'DoInsertUpdateParent(cm)
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updChauffeurDriver"

        'DoInsertUpdateParent(cm)
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramChauffeurDriverID As SqlParameter = .Parameters.Add("@ChauffeurDriverID", SqlDbType.Int)
          paramChauffeurDriverID.Value = GetProperty(ChauffeurDriverIDProperty)
          If Me.IsNew Then
            paramChauffeurDriverID.Direction = ParameterDirection.Output
          End If
          If Me.GetParent IsNot Nothing Then
            SetProperty(TravelRequisitionIDProperty, Me.GetParent.TravelRequisitionID)
            .Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
          Else
            .Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(GetProperty(TravelRequisitionIDProperty)))
          End If
          .Parameters.AddWithValue("@PickUpLocation", GetProperty(PickUpLocationProperty))
          .Parameters.AddWithValue("@DropOffLocation", GetProperty(DropOffLocationProperty))
          .Parameters.AddWithValue("@CancelledByUserID", Singular.Misc.NothingDBNull(GetProperty(CancelledByUserIDProperty)))
          .Parameters.AddWithValue("@CancelledDateTime", (New SmartDate(GetProperty(CancelledDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@PickUpDateTime", (New SmartDate(GetProperty(PickUpDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@DropOffDateTime", (New SmartDate(GetProperty(DropOffDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@PassengersLoaded", (Me.ChauffeurDriverHumanResourceList.Count > 0))

          .ExecuteNonQuery()

          If Me.IsNew Then
            'Me.ChauffeurDriverHumanResourceList.AddNew()
            LoadProperty(ChauffeurDriverIDProperty, paramChauffeurDriverID.Value)
          End If
          ' update child objects
          If GetProperty(ChauffeurDriverHumanResourceListProperty) IsNot Nothing Then
            Me.ChauffeurDriverHumanResourceList.Update()
          End If
          SetPassengerCount()
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ChauffeurDriverHumanResourceListProperty) IsNot Nothing Then
          Me.ChauffeurDriverHumanResourceList.Update()
        End If
        SetPassengerCount()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delChauffeurDriver"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ChauffeurDriverID", GetProperty(ChauffeurDriverIDProperty))
        cm.Parameters.AddWithValue("@TravelRequisitionID", GetProperty(TravelRequisitionIDProperty))
        cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
        'DoDeleteParent(cm)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
