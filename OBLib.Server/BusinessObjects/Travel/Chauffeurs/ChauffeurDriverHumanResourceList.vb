﻿' Generated 05 May 2015 10:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Chauffeurs

  <Serializable()> _
  Public Class ChauffeurDriverHumanResourceList
    Inherits OBBusinessListBase(Of ChauffeurDriverHumanResourceList, ChauffeurDriverHumanResource)

#Region " Business Methods "

    Public Function GetItem(ChauffeurDriverHumanResourceID As Integer) As ChauffeurDriverHumanResource

      For Each child As ChauffeurDriverHumanResource In Me
        If child.ChauffeurDriverHumanResourceID = ChauffeurDriverHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Chauffeur Driver Human Resources"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property ChauffeurDriverID As Integer? = Nothing

      Public Sub New()

      End Sub

      Public Sub New(ChauffeurDriverID As Integer?)

        Me.ChauffeurDriverID = ChauffeurDriverID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewChauffeurDriverHumanResourceList() As ChauffeurDriverHumanResourceList

      Return New ChauffeurDriverHumanResourceList()

    End Function

    Public Shared Sub BeginGetChauffeurDriverHumanResourceList(CallBack As EventHandler(Of DataPortalResult(Of ChauffeurDriverHumanResourceList)))

      Dim dp As New DataPortal(Of ChauffeurDriverHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetChauffeurDriverHumanResourceList(ChauffeurDriverID As Integer?) As ChauffeurDriverHumanResourceList

      Return DataPortal.Fetch(Of ChauffeurDriverHumanResourceList)(New Criteria(ChauffeurDriverID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ChauffeurDriverHumanResource.GetChauffeurDriverHumanResource(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getChauffeurDriverHumanResourceList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@ChauffeurDriverID", NothingDBNull(crit.ChauffeurDriverID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ChauffeurDriverHumanResource In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ChauffeurDriverHumanResource In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
