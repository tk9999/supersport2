﻿' Generated 15 May 2016 21:27 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Chauffeurs.ReadOnly

  <Serializable()> _
  Public Class ROTravellerChauffeurPassengerList
    Inherits OBReadOnlyListBase(Of ROTravellerChauffeurPassengerList, ROTravellerChauffeurPassenger)
    Implements Singular.Paging.IPagedList

#Region " Parent "

    <NotUndoable()> Private mParent As ROTravellerChauffeur
#End Region

#Region " Business Methods "


    ' ** Example Paging property declaration code to be implemented in the necessary VM **
    'Public Property ROTravellerChauffeurPassengerList As ROTravellerChauffeurPassengerList
    'Public Property ROTravellerChauffeurPassengerListCriteria As ROTravellerChauffeurPassengerList.Criteria
    'Public Property ROTravellerChauffeurPassengerListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    ' ** Example Paging definition code to be implemented in the necessary VM Setup Method **
    'ROTravellerChauffeurPassengerList = New ROTravellerChauffeurPassengerList
    'ROTravellerChauffeurPassengerListCriteria = New ROTravellerChauffeurPassengerList.Criteria
    'ROTravellerChauffeurPassengerListManager = New Singular.Web.Data.PagedDataManager(Of VMType)(Function(d) d.ROTravellerChauffeurPassengerList, Function(d) d.ROTravellerChauffeurPassengerListCriteria, "SortBy", PageSize, SortAsc)

    Public Function GetItem(ChauffeurDriverHumanResourceID As Integer) As ROTravellerChauffeurPassenger

      For Each child As ROTravellerChauffeurPassenger In Me
        If child.ChauffeurDriverHumanResourceID = ChauffeurDriverHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    Public Shared Function NewROTravellerChauffeurPassengerList() As ROTravellerChauffeurPassengerList

      Return New ROTravellerChauffeurPassengerList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace