﻿' Generated 15 May 2016 21:27 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Chauffeurs.ReadOnly

  <Serializable()> _
  Public Class ROTravellerChauffeurList
    Inherits OBReadOnlyListBase(Of ROTravellerChauffeurList, ROTravellerChauffeur)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "

    Public Function GetItem(ChauffeurDriverID As Integer) As ROTravellerChauffeur

      For Each child As ROTravellerChauffeur In Me
        If child.ChauffeurDriverID = ChauffeurDriverID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROTravellerChauffeurPassenger(ChauffeurDriverHumanResourceID As Integer) As ROTravellerChauffeurPassenger

      Dim obj As ROTravellerChauffeurPassenger = Nothing
      For Each parent As ROTravellerChauffeur In Me
        obj = parent.ROTravellerChauffeurPassengerList.GetItem(ChauffeurDriverHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New(TravelRequisitionID As Integer?, HumanResourceID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        Me.HumanResourceID = HumanResourceID
      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROTravellerChauffeurList() As ROTravellerChauffeurList

      Return New ROTravellerChauffeurList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTravellerChauffeurList() As ROTravellerChauffeurList

      Return DataPortal.Fetch(Of ROTravellerChauffeurList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTravellerChauffeur.GetROTravellerChauffeur(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROTravellerChauffeur = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ChauffeurDriverID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROTravellerChauffeurPassengerList.RaiseListChangedEvents = False
          parent.ROTravellerChauffeurPassengerList.Add(ROTravellerChauffeurPassenger.GetROTravellerChauffeurPassenger(sdr))
          parent.ROTravellerChauffeurPassengerList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravellerChauffeurList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace
