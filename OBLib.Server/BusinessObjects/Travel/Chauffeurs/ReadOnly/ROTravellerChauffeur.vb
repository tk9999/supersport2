﻿' Generated 15 May 2016 21:27 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Chauffeurs.ReadOnly

  <Serializable()> _
  Public Class ROTravellerChauffeur
    Inherits OBReadOnlyBase(Of ROTravellerChauffeur)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ChauffeurDriverIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChauffeurDriverID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ChauffeurDriverID() As Integer
      Get
        Return GetProperty(ChauffeurDriverIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionID, "Travel Requisition")
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="")>
    Public ReadOnly Property TravelRequisitionID() As Integer
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared PickupLocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PickupLocation, "Pickup Location")
    ''' <summary>
    ''' Gets the Pickup Location value
    ''' </summary>
    <Display(Name:="Pickup Location", Description:="")>
    Public ReadOnly Property PickupLocation() As String
      Get
        Return GetProperty(PickupLocationProperty)
      End Get
    End Property

    Public Shared DropOffLocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DropOffLocation, "Drop Off Location")
    ''' <summary>
    ''' Gets the Drop Off Location value
    ''' </summary>
    <Display(Name:="Drop Off Location", Description:="")>
    Public ReadOnly Property DropOffLocation() As String
      Get
        Return GetProperty(DropOffLocationProperty)
      End Get
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User")
    ''' <summary>
    ''' Gets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="")>
    Public ReadOnly Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="")>
    Public ReadOnly Property CancelledDateTime As Date
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="")>
    Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As DateTime
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDate, "Modified Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDate() As DateTime
      Get
        Return GetProperty(ModifiedDateProperty)
      End Get
    End Property

    Public Shared PickUpDateTimeSProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PickUpDateTimeS, "Pick Up Date Time")
    ''' <summary>
    ''' Gets the Pick Up Date Time value
    ''' </summary>
    <Display(Name:="Pick Up Date Time", Description:="")>
    Public ReadOnly Property PickUpDateTimeS As String
      Get
        Return GetProperty(PickUpDateTimeSProperty)
      End Get
    End Property

    Public Shared DropOffDateTimeSProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DropOffDateTimeS, "Drop Off Date Time")
    ''' <summary>
    ''' Gets the Drop Off Date Time value
    ''' </summary>
    <Display(Name:="Drop Off Date Time", Description:="")>
    Public ReadOnly Property DropOffDateTimeS As String
      Get
        Return GetProperty(DropOffDateTimeSProperty)
      End Get
    End Property

    Public Shared PickUpDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.PickUpDateTime, "Pick Up Date Time")
    ''' <summary>
    ''' Gets the Pick Up Date Time value
    ''' </summary>
    <Display(Name:="Pick Up Date Time", Description:="")>
    Public ReadOnly Property PickUpDateTime As DateTime
      Get
        Return GetProperty(PickUpDateTimeProperty)
      End Get
    End Property

    Public Shared DropOffDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.DropOffDateTime, "Drop Off Date Time")
    ''' <summary>
    ''' Gets the Drop Off Date Time value
    ''' </summary>
    <Display(Name:="Drop Off Date Time", Description:="")>
    Public ReadOnly Property DropOffDateTime As DateTime
      Get
        Return GetProperty(DropOffDateTimeProperty)
      End Get
    End Property

    Public Shared PassengerCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PassengerCount, "Passenger Count")
    ''' <summary>
    ''' Gets the Passenger Count value
    ''' </summary>
    <Display(Name:="Passenger Count", Description:="")>
    Public ReadOnly Property PassengerCount() As Integer
      Get
        Return GetProperty(PassengerCountProperty)
      End Get
    End Property

    Public Shared CancelledIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CancelledInd, "Cancelled Indicator", False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:="Is Flight Cancelled?")>
    Public ReadOnly Property CancelledInd() As Boolean
      Get
        If CancelledByUserID IsNot Nothing Then
          Return True
        End If
        Return False
      End Get
    End Property

    Public Shared Property MainPassengerErrorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MainPassengerError, "Driver Error", "")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Error", Description:="")>
    Public ReadOnly Property MainPassengerError() As String
      Get
        Return GetProperty(MainPassengerErrorProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROTravellerChauffeurPassengerListProperty As PropertyInfo(Of ROTravellerChauffeurPassengerList) = RegisterProperty(Of ROTravellerChauffeurPassengerList)(Function(c) c.ROTravellerChauffeurPassengerList, "RO Traveller Chauffeur Passenger List")

    Public ReadOnly Property ROTravellerChauffeurPassengerList() As ROTravellerChauffeurPassengerList
      Get
        If GetProperty(ROTravellerChauffeurPassengerListProperty) Is Nothing Then
          LoadProperty(ROTravellerChauffeurPassengerListProperty, OBLib.Travel.Chauffeurs.ReadOnly.ROTravellerChauffeurPassengerList.NewROTravellerChauffeurPassengerList())
        End If
        Return GetProperty(ROTravellerChauffeurPassengerListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ChauffeurDriverIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.PickupLocation

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerChauffeur(dr As SafeDataReader) As ROTravellerChauffeur

      Dim r As New ROTravellerChauffeur()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ChauffeurDriverIDProperty, .GetInt32(0))
        LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(PickupLocationProperty, .GetString(2))
        LoadProperty(DropOffLocationProperty, .GetString(3))
        LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(CancelledDateTimeProperty, .GetValue(5))
        LoadProperty(CancelledReasonProperty, .GetString(6))
        LoadProperty(CreatedByProperty, .GetInt32(7))
        LoadProperty(CreatedDateProperty, .GetDateTime(8))
        LoadProperty(ModifiedByProperty, .GetInt32(9))
        LoadProperty(ModifiedDateProperty, .GetDateTime(10))
        LoadProperty(PickUpDateTimeSProperty, .GetString(11))
        LoadProperty(DropOffDateTimeSProperty, .GetString(12))
        LoadProperty(PickUpDateTimeProperty, .GetValue(13))
        LoadProperty(DropOffDateTimeProperty, .GetValue(14))
        LoadProperty(PassengerCountProperty, .GetValue(15))
      End With

    End Sub

#End Region

  End Class

End Namespace