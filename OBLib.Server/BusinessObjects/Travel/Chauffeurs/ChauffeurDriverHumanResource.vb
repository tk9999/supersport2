﻿' Generated 05 May 2015 10:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.Misc

Namespace Travel.Chauffeurs

  <Serializable()> _
  Public Class ChauffeurDriverHumanResource
    Inherits OBBusinessBase(Of ChauffeurDriverHumanResource)

#Region " Properties and Methods "

#Region " Properties "
    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ChauffeurPassengerBO.toString(self)")

    Public Shared ChauffeurDriverHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChauffeurDriverHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property ChauffeurDriverHumanResourceID() As Integer
      Get
        Return GetProperty(ChauffeurDriverHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ChauffeurDriverIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChauffeurDriverID, "Chauffeur Driver", Nothing)
    ''' <summary>
    ''' Gets the Chauffeur Driver value
    ''' </summary>
    Public Property ChauffeurDriverID() As Integer
      Get
        Return GetProperty(ChauffeurDriverIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ChauffeurDriverIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="Human Resource ID"),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="User which cancelled the Human Resource")>
    Public Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CancelledByUserIDProperty, Value)
      End Set
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets and sets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="Date which the Human Resource was cancelled")>
    Public Property CancelledDateTime As DateTime?
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancelledDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="Reason why the Human Resource was cancelled"),
    StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
      End Set
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Cancelled Indicator", False)
    ''' <summary>
    ''' Gets and sets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled Indicator", Description:=""),
    SetExpression("ChauffeurPassengerBO.IsCancelledSet(self)")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared OnCarIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnCarInd, "", False)
    ''' <summary>
    ''' Gets and sets the Car Class value
    ''' </summary>
    <Display(Name:="On Car Indicator", Description:=""),
    SetExpression("ChauffeurPassengerBO.OnCarIndSet(self)")>
    Public Property OnCarInd() As Boolean
      Get
        Return GetProperty(OnCarIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OnCarIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As SmartDate
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDate, "Modified Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDate() As SmartDate
      Get
        Return GetProperty(ModifiedDateProperty)
      End Get
    End Property

    Public Shared MainPassengerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MainPassengerInd, "", False)
    ''' <summary>
    ''' Gets and sets the Main Passenger value
    ''' </summary>
    <Display(Name:="Main Passenger?", Description:="Main Passenger the Chauffeur is booked for"),
    SetExpression("ChauffeurPassengerBO.MainPassengerIndSet(self)")>
    Public Property MainPassengerInd() As Boolean
      Get
        Return GetProperty(MainPassengerIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(MainPassengerIndProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City Code")
    ''' <summary>
    ''' Gets and sets the City Code value
    ''' </summary>
    <Display(Name:="City Code", Description:="")>
    Public Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CityCodeProperty, Value)
      End Set
    End Property

    Public Shared OtherChauffeursProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OtherChauffeurs, "Other Chauffeurs", "")
    ''' <summary>
    ''' Gets and sets the Other Flights value
    ''' </summary>
    <Display(Name:="Other Chauffeurs", Description:="")>
    Public Property OtherChauffeurs() As String
      Get
        Return GetProperty(OtherChauffeursProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OtherChauffeursProperty, Value)
      End Set
    End Property

    Public Shared OtherChauffeursShortProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OtherChauffeursShort, "Other Chauffeurs", "")
    ''' <summary>
    ''' Gets and sets the City Code value
    ''' </summary>
    <Display(Name:="Other Chauffeurs", Description:="")>
    Public Property OtherChauffeursShort() As String
      Get
        Return GetProperty(OtherChauffeursShortProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OtherChauffeursShortProperty, Value)
      End Set
    End Property

    Public Shared HasClashesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasClashes, "", False)
    ''' <summary>
    ''' Gets and sets the Car Class value
    ''' </summary>
    <Display(Name:="HasClashes", Description:="")>
    Public Property HasClashes() As Boolean
      Get
        Return GetProperty(HasClashesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasClashesProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ChauffeurDriver

      Return CType(CType(Me.Parent, ChauffeurDriverHumanResourceList).Parent, ChauffeurDriver)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ChauffeurDriverHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ChauffeurDriverHumanResourceID = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Chauffeur Driver Human Resource")
        Else
          Return String.Format("Blank {0}", "Chauffeur Driver Human Resource")
        End If
      Else
        Return Me.CancelledReason
      End If

    End Function

    Private Sub UpdateCancelledDetails(Value As Boolean)

      If Value = True Then
        SetProperty(CancelledByUserIDProperty, Security.Settings.CurrentUser.UserID)
        SetProperty(CancelledDateTimeProperty, Date.Now)
      Else
        SetProperty(CancelledByUserIDProperty, Nothing)
        SetProperty(CancelledDateTimeProperty, Nothing)
        SetProperty(CancelledReasonProperty, "")
      End If

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(OnCarIndProperty)
        .AddTriggerProperty(OtherChauffeursProperty)
        .JavascriptRuleFunctionName = "ChauffeurPassengerBO.OnCarIndValid"
        .ServerRuleFunction = AddressOf OnCarIndValid
      End With

      With AddWebRule(CancelledReasonProperty)
        .AddTriggerProperty(IsCancelledProperty)
        .JavascriptRuleFunctionName = "ChauffeurPassengerBO.CancelledReasonValid"
        .ServerRuleFunction = AddressOf CancelledReasonValid
      End With

    End Sub

    Private Shared Function CancelledReasonValid(bag As ChauffeurDriverHumanResource) As String

      Dim ErrorString = ""
      If bag.IsCancelled AndAlso bag.CancelledReason.Trim = "" Then
        ErrorString = "Cancelled Reason Required"
      End If
      Return ErrorString

    End Function

    Public Shared Function OnCarIndValid(bag As ChauffeurDriverHumanResource) As String
      Dim ErrorString = ""
      If bag.OnCarInd AndAlso bag.HasClashes Then
        ErrorString = bag.HumanResource & " has chauffeur clashes"
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewChauffeurDriverHumanResource() method.

    End Sub

    Public Shared Function NewChauffeurDriverHumanResource() As ChauffeurDriverHumanResource

      Return DataPortal.CreateChild(Of ChauffeurDriverHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetChauffeurDriverHumanResource(dr As SafeDataReader) As ChauffeurDriverHumanResource

      Dim c As New ChauffeurDriverHumanResource()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ChauffeurDriverHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(ChauffeurDriverIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CancelledDateTimeProperty, .GetValue(4))
          LoadProperty(CancelledReasonProperty, .GetString(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateProperty, .GetSmartDate(9))
          LoadProperty(MainPassengerIndProperty, .GetBoolean(10))
          LoadProperty(HumanResourceProperty, .GetString(11))
          LoadProperty(CityCodeProperty, .GetString(12))
          LoadProperty(IsSelectedProperty, .GetBoolean(13))
          LoadProperty(OnCarIndProperty, IsSelected)
          LoadProperty(OtherChauffeursProperty, .GetString(14))
          LoadProperty(OtherChauffeursShortProperty, .GetString(15))
          LoadProperty(HasClashesProperty, .GetBoolean(16))
          LoadProperty(IsCancelledProperty, IIf(CancelledByUserID Is Nothing, False, True))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insChauffeurDriverHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updChauffeurDriverHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramChauffeurDriverHumanResourceID As SqlParameter = .Parameters.Add("@ChauffeurDriverHumanResourceID", SqlDbType.Int)
          paramChauffeurDriverHumanResourceID.Value = GetProperty(ChauffeurDriverHumanResourceIDProperty)
          'If Me.IsNew Then
          paramChauffeurDriverHumanResourceID.Direction = ParameterDirection.InputOutput
          'End If
          .Parameters.AddWithValue("@IsSelected", GetProperty(IsSelectedProperty))
          .Parameters.AddWithValue("@ChauffeurDriverID", Me.GetParent().ChauffeurDriverID)
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@CancelledByUserID", Singular.Misc.ZeroDBNull(GetProperty(CancelledByUserIDProperty)))
          .Parameters.AddWithValue("@CancelledDateTime", (New SmartDate(GetProperty(CancelledDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@MainPassengerInd", GetProperty(MainPassengerIndProperty))
          .Parameters.AddWithValue("@TravelRequisitionID", Me.GetParent.TravelRequisitionID)

          .ExecuteNonQuery()

          'If Me.IsNew Then
          LoadProperty(ChauffeurDriverHumanResourceIDProperty, IIf(IsNullNothing(paramChauffeurDriverHumanResourceID.Value), 0, paramChauffeurDriverHumanResourceID.Value))
          'End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delChauffeurDriverHumanResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ChauffeurDriverHumanResourceID", GetProperty(ChauffeurDriverHumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
