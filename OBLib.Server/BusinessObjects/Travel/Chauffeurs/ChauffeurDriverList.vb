﻿' Generated 05 May 2015 10:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Chauffeurs

  <Serializable()> _
  Public Class ChauffeurDriverList
    Inherits OBBusinessListBase(Of ChauffeurDriverList, ChauffeurDriver)

#Region " Business Methods "

    Public Function GetItem(ChauffeurDriverID As Integer) As ChauffeurDriver

      For Each child As ChauffeurDriver In Me
        If child.ChauffeurDriverID = ChauffeurDriverID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Chauffeur Drivers"

    End Function

    Public Function GetChauffeurDriverHumanResource(HumanResourceID As Integer) As ChauffeurDriverHumanResource

      Dim obj As ChauffeurDriverHumanResource = Nothing
      For Each parent As ChauffeurDriver In Me
        obj = parent.ChauffeurDriverHumanResourceList.GetItem(HumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Sub UpdateTravelRequisitionID(TravelRequisitionID As Integer)

      Me.ToList.ForEach(Sub(z)
                          If z.IsNew Then
                            z.TravelRequisitionID = TravelRequisitionID
                          End If
                        End Sub)

    End Sub

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property ChauffeurDriverID As Integer? = Nothing

      Public Sub New(TravelRequisitionID As Integer?, ChauffeurDriverID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
        Me.ChauffeurDriverID = ChauffeurDriverID
      End Sub

      Public Sub New()

      End Sub

    End Class


#Region " Common "

    Public Shared Function NewChauffeurDriverList() As ChauffeurDriverList

      Return New ChauffeurDriverList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetChauffeurDriverList() As ChauffeurDriverList

      Return DataPortal.Fetch(Of ChauffeurDriverList)(New Criteria())

    End Function

    Public Shared Function GetChauffeurDriverList(TravelRequisitionID As Integer?, ChauffeurDriverID As Integer?) As ChauffeurDriverList

      Return DataPortal.Fetch(Of ChauffeurDriverList)(New Criteria(TravelRequisitionID, ChauffeurDriverID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ChauffeurDriver.GetChauffeurDriver(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ChauffeurDriver = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ChauffeurDriverID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ChauffeurDriverHumanResourceList.RaiseListChangedEvents = False
          parent.ChauffeurDriverHumanResourceList.Add(ChauffeurDriverHumanResource.GetChauffeurDriverHumanResource(sdr))
          parent.ChauffeurDriverHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As ChauffeurDriver In Me
        child.CheckRules()
        For Each ahr As ChauffeurDriverHumanResource In child.ChauffeurDriverHumanResourceList
          ahr.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getChauffeurDriverList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@ChauffeurDriverID", NothingDBNull(crit.ChauffeurDriverID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        If Me.DeletedList.Count > 0 Then
          DeleteBulk()
          ' Then clear the list of deleted objects because they are truly gone now.
          DeletedList.Clear()
        End If

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ChauffeurDriver In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

    Private Sub DeleteBulk()

      Dim cm As New Singular.CommandProc("DelProcsWeb.delChauffeurDriverListBulk")
      cm.Parameters.AddWithValue("@ChauffeurDriverIDs", Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(Me.DeletedList.Select(Function(d) d.ChauffeurDriverID).ToList)))
      cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      cm.UseTransaction = True
      cm.Execute()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
