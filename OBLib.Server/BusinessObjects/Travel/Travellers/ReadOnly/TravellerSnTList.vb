﻿' Generated 13 May 2016 08:16 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Travellers

  <Serializable()> _
  Public Class TravellerSnTList
    Inherits OBBusinessListBase(Of TravellerSnTList, TravellerSnT)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As TravellerSnT

      For Each child As TravellerSnT In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property TravelRequisitionID As Integer?
      Public Property HumanResourceID As Integer?
      Public Property FetchSnTDetails As Boolean = False
      Public Property SnTStartDateOverride As Date? = Nothing
      Public Property SnTEndDateOverride As Date? = Nothing

      Public Sub New()
      End Sub

      Public Sub New(TravelRequisitionID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
      End Sub

    End Class

    Public Shared Function NewTravellerSnTList() As TravellerSnTList

      Return New TravellerSnTList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetTravellerSnTList(TravelRequisitionID As Integer?) As TravellerSnTList

      Return DataPortal.Fetch(Of TravellerSnTList)(New Criteria(TravelRequisitionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(TravellerSnT.GetTravellerSnT(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parentAdHocSnT As TravellerSnT = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentAdHocSnT Is Nothing OrElse parentAdHocSnT.HumanResourceID <> sdr.GetInt32(1) Then
            parentAdHocSnT = Me.GetItem(sdr.GetInt32(1))
          End If
          If parentAdHocSnT IsNot Nothing Then
            parentAdHocSnT.SnTDetailList.RaiseListChangedEvents = False
            parentAdHocSnT.SnTDetailList.Add(OBLib.Travel.SnT.SnTDetail.GetSnTDetail(sdr))
            parentAdHocSnT.SnTDetailList.RaiseListChangedEvents = True
          End If
        End While
      End If

      For Each child As TravellerSnT In Me
        For Each SnTDetail As OBLib.Travel.SnT.SnTDetail In child.SnTDetailList
          SnTDetail.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getTravellerSnTList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@FetchSnTDetails", crit.FetchSnTDetails)
            cm.Parameters.AddWithValue("@SnTStartDateOverride", NothingDBNull(crit.SnTStartDateOverride))
            cm.Parameters.AddWithValue("@SnTEndDateOverride", NothingDBNull(crit.SnTEndDateOverride))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Private Function CreateTravellersTableParameter() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("TravelRequisitionTravellerID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("TravelRequisitionID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SnTStartDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SnTEndDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each tr As OBLib.Travel.Travellers.TravellerSnT In Me
        Dim row As DataRow = RBTable.NewRow
        row("ObjectGuid") = NothingDBNull(tr.Guid)
        row("TravelRequisitionTravellerID") = NothingDBNull(tr.TravelRequisitionTravellerID)
        row("TravelRequisitionID") = NothingDBNull(tr.TravelRequisitionID)
        row("HumanResourceID") = NothingDBNull(tr.HumanResourceID)
        row("SnTStartDate") = tr.SnTStartDate.ToString
        row("SnTEndDate") = tr.SnTEndDate.ToString
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

    Private Function CreateSnTDetailsTableParameter() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ParentObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("GroupHumanResourceSnTID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("GroupSnTID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("DestinationCountryID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("CurrencyID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SnTDay", GetType(Date)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("BreakfastAmount", GetType(Decimal)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("BreakfastProductionID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("BreakfastAdHocBookingID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("BreakfastSystemID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("BreakfastProductionAreaID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("LunchAmount", GetType(Decimal)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("LunchProductionID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("LunchAdHocBookingID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("LunchSystemID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("LunchProductionAreaID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("DinnerAmount", GetType(Decimal)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("DinnerProductionID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("DinnerAdHocBookingID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("DinnerSystemID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("DinnerProductionAreaID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IncidentalAmount", GetType(Decimal)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IncidentalProductionID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IncidentalAdHocBookingID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IncidentalSystemID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IncidentalProductionAreaID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Comments", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ChangedReason", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IsBreakfastCurrentEvent", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IsLunchCurrentEvent", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IsDinnerCurrentEvent", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IsIncidentalCurrentEvent", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HasBreakfast", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HasLunch", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HasDinner", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HasIncidental", GetType(Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("CanEditRecord", GetType(Boolean)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each trd As OBLib.Travel.Travellers.TravellerSnT In Me
        If trd.IsDirty Then
          For Each tr As OBLib.Travel.SnT.SnTDetail In trd.SnTDetailList
            If tr.IsDirty Then
              Dim row As DataRow = RBTable.NewRow
              row("ObjectGuid") = NothingDBNull(tr.Guid)
              row("ParentObjectGuid") = NothingDBNull(trd.Guid)
              row("GroupHumanResourceSnTID") = NothingDBNull(tr.GroupHumanResourceSnTID)
              row("GroupSnTID") = NothingDBNull(tr.GroupSnTID)
              row("DestinationCountryID") = NothingDBNull(tr.DestinationCountryID)
              row("CurrencyID") = NothingDBNull(tr.CurrencyID)
              row("SnTDay") = NothingDBNull(tr.SnTDay)
              row("BreakfastAmount") = NothingDBNull(tr.BreakfastAmount)
              row("BreakfastProductionID") = NothingDBNull(tr.BreakfastProductionID)
              row("BreakfastAdHocBookingID") = NothingDBNull(tr.BreakfastAdHocBookingID)
              row("BreakfastSystemID") = NothingDBNull(tr.BreakfastSystemID)
              row("BreakfastProductionAreaID") = NothingDBNull(tr.BreakfastProductionAreaID)
              row("LunchAmount") = NothingDBNull(tr.LunchAmount)
              row("LunchProductionID") = NothingDBNull(tr.LunchProductionID)
              row("LunchAdHocBookingID") = NothingDBNull(tr.LunchAdHocBookingID)
              row("LunchSystemID") = NothingDBNull(tr.LunchSystemID)
              row("LunchProductionAreaID") = NothingDBNull(tr.LunchProductionAreaID)
              row("DinnerAmount") = NothingDBNull(tr.DinnerAmount)
              row("DinnerProductionID") = NothingDBNull(tr.DinnerProductionID)
              row("DinnerAdHocBookingID") = NothingDBNull(tr.DinnerAdHocBookingID)
              row("DinnerSystemID") = NothingDBNull(tr.DinnerSystemID)
              row("DinnerProductionAreaID") = NothingDBNull(tr.DinnerProductionAreaID)
              row("IncidentalAmount") = NothingDBNull(tr.IncidentalAmount)
              row("IncidentalProductionID") = NothingDBNull(tr.IncidentalProductionID)
              row("IncidentalAdHocBookingID") = NothingDBNull(tr.IncidentalAdHocBookingID)
              row("IncidentalSystemID") = NothingDBNull(tr.IncidentalSystemID)
              row("IncidentalProductionAreaID") = NothingDBNull(tr.IncidentalProductionAreaID)
              row("Comments") = tr.Comments
              row("ChangedReason") = tr.ChangedReason
              row("IsBreakfastCurrentEvent") = NothingDBNull(tr.IsBreakfastCurrentEvent)
              row("IsLunchCurrentEvent") = NothingDBNull(tr.IsLunchCurrentEvent)
              row("IsDinnerCurrentEvent") = NothingDBNull(tr.IsDinnerCurrentEvent)
              row("IsIncidentalCurrentEvent") = NothingDBNull(tr.IsIncidentalCurrentEvent)
              row("HasBreakfast") = NothingDBNull(tr.HasBreakfast)
              row("HasLunch") = NothingDBNull(tr.HasLunch)
              row("HasDinner") = NothingDBNull(tr.HasDinner)
              row("HasIncidental") = NothingDBNull(tr.HasIncidental)
              row("CanEditRecord") = NothingDBNull(tr.CanEditRecord)
              RBTable.Rows.Add(row)
            End If
          Next
        End If
      Next

      Return RBTable

    End Function

    Private Sub UpdateBulk()

      Dim dt As DataTable = CreateTravellersTableParameter()
      Dim dtr As DataTable = CreateSnTDetailsTableParameter()

      Dim cm As New Singular.CommandProc("UpdProcsWeb.updTravellerSnTBulk")
      cm.Parameters.AddWithValue("@TravellerSnT", dt, SqlDbType.Structured)
      cm.Parameters.AddWithValue("@TravellerSnTDetail", dtr, SqlDbType.Structured)
      cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      cm.UseTransaction = True
      cm.Execute()

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "UpdProcsWeb.updTravellerSnTBulk"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@TravellerSnT", dt)
      '  cm.Parameters.AddWithValue("@TravellerSnTDetail", dtr)
      '  cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      '  cm.Connection = CType(Csla.ApplicationContext.LocalContext("cn"), SqlClient.SqlConnection)
      '  cm.Transaction = Csla.ApplicationContext.LocalContext("tr")
      '  Singular.CSLALib.ContextInfo.SetContextInfoOnConnection(cm.Connection, cm.Transaction)
      '  cm.ExecuteNonQuery()
      'End Using

    End Sub

    'Private Function GetTravellerSnTDataSet() As DataSet

    '  Dim RBTable As DataTable = CreateResourceBookingTableParameter()
    '  Using cm As SqlCommand = New SqlCommand
    '    cm.CommandText = "DelProcsWeb.delProductionHRBulk"
    '    cm.CommandType = CommandType.StoredProcedure
    '    'Dim sqlP As SqlClient.SqlParameter = New SqlClient.SqlParameter("@ProductionHRBulk", dt)
    '    'sqlP.SqlDbType = SqlDbType.Structured
    '    cm.Parameters.AddWithValue("@ProductionHRBulk", RBTable)
    '    cm.Connection = CType(Csla.ApplicationContext.LocalContext("cn"), SqlClient.SqlConnection)
    '    cm.Transaction = Csla.ApplicationContext.LocalContext("tr")
    '    Singular.CSLALib.ContextInfo.SetContextInfoOnConnection(cm.Connection, cm.Transaction)
    '    cm.ExecuteNonQuery()
    '  End Using
    '  'Dim cmd As New Singular.CommandProc("RuleProcs.ruleResourcesAvailable")
    '  'Dim TravellersParam As System.Data.SqlClient.SqlParameter = New System.Data.SqlClient.SqlParameter()
    '  'TravellersParam.ParameterName = "@Travellers"
    '  'TravellersParam.SqlDbType = SqlDbType.Structured
    '  'TravellersParam.Value = RBTable
    '  'cmd.Parameters.Add(TravellersParam)
    '  'cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
    '  'cmd.Execute(0)
    '  'Return cmd.Dataset

    'End Function

    'Private Function CreateResourceBookingTableParameter() As DataTable

    '  Dim RBTable As New DataTable
    '  RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("ResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("ResourceBookingID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("StartDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("EndDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("RoomScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("ProductionHRID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("EquipmentScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("HumanResourceShiftID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("HumanResourceOffPeriodID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("HumanResourceSecondmentID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("ProductionSystemAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

    '  Dim rowList As New List(Of DataRow)
    '  For Each ph As OBLib.Travel.Travellers.TravellerSnT In Me
    '    'If CompareSafe(rb.ResourceBookingID, 0) Then
    '    '  rb.ResourceBookingID = Nothing
    '    'End If
    '    Dim row As DataRow = RBTable.NewRow
    '    row("ObjectGuid") = NothingDBNull(Nothing)
    '    row("ResourceID") = NothingDBNull(rb.ResourceID)
    '    If CompareSafe(rb.ResourceBookingID, 0) Then
    '      row("ResourceBookingID") = ZeroDBNull(rb.ResourceBookingID)
    '    Else
    '      row("ResourceBookingID") = NothingDBNull(rb.ResourceBookingID)
    '    End If
    '    row("StartDateTimeBuffer") = NothingDBNull(Nothing)
    '    row("StartDateTime") = rb.StartDateTime.ToString
    '    row("EndDateTime") = rb.EndDateTime.ToString
    '    row("EndDateTimeBuffer") = NothingDBNull(Nothing)
    '    row("RoomScheduleID") = NothingDBNull(Nothing)
    '    If CompareSafe(ph.ProductionHRID, 0) Then
    '      row("ProductionHRID") = ZeroDBNull(ph.ProductionHRID)
    '    Else
    '      row("ProductionHRID") = NothingDBNull(ph.ProductionHRID)
    '    End If
    '    row("EquipmentScheduleID") = NothingDBNull(Nothing)
    '    row("HumanResourceShiftID") = NothingDBNull(Nothing)
    '    row("HumanResourceOffPeriodID") = NothingDBNull(Nothing)
    '    row("HumanResourceSecondmentID") = NothingDBNull(Nothing)
    '    row("ProductionSystemAreaID") = NothingDBNull(mProductionSystemAreaID)
    '    RBTable.Rows.Add(row)
    '  Next

    '  Return RBTable

    'End Function

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try

        UpdateBulk()
        '' Loop through each deleted child object and call its Update() method
        'For Each Child As TravellerSnT In DeletedList
        '  Child.DeleteSelf()
        'Next
        '' Then clear the list of deleted objects because they are truly gone now.
        'DeletedList.Clear()

        'Me.BulkUpdate(BulkUpdateMethod.AllRecords, , Sub(d)
        '                                               d.CommandText = "InsProcsWeb.insTravellerSnTBulk"
        '                                               d.CommandTimeout = 0
        '                                               d.CommandType = CommandType.StoredProcedure
        '                                             End Sub)

        '' Loop through each non-deleted child object and call its Update() method
        'For Each Child As TravellerSnT In Me
        '  If Child.IsNew Then
        '    Child.Insert()
        '  Else
        '    Child.Update()
        '  End If
        'Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End Region

    Function Reset() As Singular.Web.Result

      Try
        Dim dt As DataTable = CreateTravellersTableParameter()
        Dim cmdProc As New Singular.CommandProc("cmdProcs.cmdTravelResetTravellerSnT")
        cmdProc.UseTransaction = True
        cmdProc.Parameters.AddWithValue("@TravellerSnT", dt)
        cmdProc.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
        cmdProc = cmdProc.Execute()
        Return New Singular.Web.Result(True)
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

    End Function

  End Class

End Namespace
