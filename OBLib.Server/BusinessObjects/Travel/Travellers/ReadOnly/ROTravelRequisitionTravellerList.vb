﻿' Generated 03 Aug 2015 10:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Travellers.ReadOnly

  <Serializable()> _
  Public Class ROTravelRequisitionTravellerList
    Inherits OBReadOnlyListBase(Of ROTravelRequisitionTravellerList, ROTravelRequisitionTraveller)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROTravelRequisitionTraveller

      For Each child As ROTravelRequisitionTraveller In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Travel Requision Travellers"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property TravelRequisitionID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(TravelRequisitionID As Integer)
        Me.TravelRequisitionID = TravelRequisitionID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROTravelRequisionTravellerList() As ROTravelRequisitionTravellerList

      Return New ROTravelRequisitionTravellerList()

    End Function

    Public Shared Sub BeginGetROTravelRequisionTravellerList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROTravelRequisitionTravellerList)))

      Dim dp As New DataPortal(Of ROTravelRequisitionTravellerList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROTravelRequisionTravellerList(CallBack As EventHandler(Of DataPortalResult(Of ROTravelRequisitionTravellerList)))

      Dim dp As New DataPortal(Of ROTravelRequisitionTravellerList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROTravelRequisitionTravellerList() As ROTravelRequisitionTravellerList

      Return DataPortal.Fetch(Of ROTravelRequisitionTravellerList)(New Criteria())

    End Function

    Public Shared Function GetROTravelRequisitionTravellerList(TravelRequisitionID As Integer?) As ROTravelRequisitionTravellerList

      Return DataPortal.Fetch(Of ROTravelRequisitionTravellerList)(New Criteria(TravelRequisitionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTravelRequisitionTraveller.GetROTravelRequisitionTraveller(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      'Dim parentAdHocSnT As ROTravelRequisitionTraveller = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentAdHocSnT Is Nothing OrElse parentAdHocSnT.HumanResourceID <> sdr.GetInt32(1) Then
      '      parentAdHocSnT = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parentAdHocSnT.ROSnTDetailList.RaiseListChangedEvents = False
      '    parentAdHocSnT.ROSnTDetailList.Add(OBLib.Travel.AdHoc.SnTs.ReadOnly.ROAdHocSnTDetail.GetAdHocSnTDetail(sdr))
      '    parentAdHocSnT.ROSnTDetailList.RaiseListChangedEvents = True
      '  End While
      'End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravelRequisitionTravellerList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", crit.TravelRequisitionID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace