﻿' Generated 13 May 2016 08:16 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Travellers.ReadOnly

  <Serializable()> _
  Public Class ROTravellerList
    Inherits OBReadOnlyListBase(Of ROTravellerList, ROTraveller)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROTraveller

      For Each child As ROTraveller In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property TravelRequisitionID As Integer?

      <Singular.DataAnnotations.PrimarySearchField>
      Public Property HumanResource As String

      Public Sub New()
      End Sub

      Public Sub New(TravelRequisitionID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
      End Sub

    End Class

    Public Shared Function NewROTravellerList() As ROTravellerList

      Return New ROTravellerList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTravellerList(TravelRequisitionID As Integer?) As ROTravellerList

      Return DataPortal.Fetch(Of ROTravellerList)(New Criteria(TravelRequisitionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTraveller.GetROTraveller(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravellerList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@HumanResource", Singular.Strings.MakeEmptyDBNull(crit.HumanResource))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace
