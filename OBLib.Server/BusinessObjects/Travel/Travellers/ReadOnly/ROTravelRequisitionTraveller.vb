﻿' Generated 03 Aug 2015 10:23 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Travellers.ReadOnly

  <Serializable()> _
  Public Class ROTravelRequisitionTraveller
    Inherits OBReadOnlyBase(Of ROTravelRequisitionTraveller)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TravelRequisitionTravellerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionTravellerID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property TravelRequisitionTravellerID() As Integer
      Get
        Return GetProperty(TravelRequisitionTravellerIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelRequisitionID, "Travel Requisition", Nothing)
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="ID of the associated Travel Requisition")>
    Public ReadOnly Property TravelRequisitionID() As Integer?
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="ID of the Human Resource which this S&T is allocated to")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SnTStartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SnTStartDate, "Sn T Start Date")
    ''' <summary>
    ''' Gets the Sn T Start Date value
    ''' </summary>
    <Display(Name:="Sn T Start Date", Description:="Date of the first SnT allocated to this Traveller")>
    Public ReadOnly Property SnTStartDate As DateTime?
      Get
        Return GetProperty(SnTStartDateProperty)
      End Get
    End Property

    Public Shared SnTEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SnTEndDate, "Sn T End Date")
    ''' <summary>
    ''' Gets the Sn T End Date value
    ''' </summary>
    <Display(Name:="Sn T End Date", Description:="Date of the last SnT allocated to this Traveller")>
    Public ReadOnly Property SnTEndDate As DateTime?
      Get
        Return GetProperty(SnTEndDateProperty)
      End Get
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Is Cancelled", False)
    ''' <summary>
    ''' Gets the Is Cancelled value
    ''' </summary>
    <Display(Name:="Is Cancelled", Description:="Tick indicates thether or not the Traveller has been cancelled")>
    Public ReadOnly Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="Reason for the Traveller to be Cancelled")>
    Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="ID of the User who cancelled the Traveller")>
    Public ReadOnly Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    'Public Shared ROSnTDetailListProperty As PropertyInfo(Of OBLib.Travel.AdHoc.SnTs.ReadOnly.ROAdHocSnTDetailList) = RegisterProperty(Of OBLib.Travel.AdHoc.SnTs.ReadOnly.ROAdHocSnTDetailList)(Function(c) c.ROSnTDetailList, "SnT Detail List")
    'Public ReadOnly Property ROSnTDetailList() As OBLib.Travel.AdHoc.SnTs.ReadOnly.ROAdHocSnTDetailList
    '  Get
    '    If GetProperty(ROSnTDetailListProperty) Is Nothing Then
    '      LoadProperty(ROSnTDetailListProperty, OBLib.Travel.AdHoc.SnTs.ReadOnly.ROAdHocSnTDetailList.NewAdHocSnTDetailList())
    '    End If
    '    Return GetProperty(ROSnTDetailListProperty)
    '  End Get
    'End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TravelRequisitionTravellerIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CancelledReason

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROTravelRequisitionTraveller(dr As SafeDataReader) As ROTravelRequisitionTraveller

      Dim r As New ROTravelRequisitionTraveller()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TravelRequisitionTravellerIDProperty, .GetInt32(0))
        LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SnTStartDateProperty, .GetValue(3))
        LoadProperty(SnTEndDateProperty, .GetValue(4))
        LoadProperty(IsCancelledProperty, .GetBoolean(5))
        LoadProperty(CancelledReasonProperty, .GetString(6))
        LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
        LoadProperty(CreatedByProperty, .GetInt32(13))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(14))
        LoadProperty(ModifiedByProperty, .GetInt32(15))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace