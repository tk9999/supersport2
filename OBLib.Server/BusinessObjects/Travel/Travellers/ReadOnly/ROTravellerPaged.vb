﻿' Generated 13 May 2016 08:16 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Travellers.ReadOnly

  <Serializable()> _
  Public Class ROTravellerPaged
    Inherits OBReadOnlyBase(Of ROTravellerPaged)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Traveller", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City Code")
    ''' <summary>
    ''' Gets the City Code value
    ''' </summary>
    <Display(Name:="City Code", Description:="")>
    Public ReadOnly Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CityID, "City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property CityID() As Integer
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
    Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number")
    ''' <summary>
    ''' Gets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Cell Phone Number", Description:="")>
    Public ReadOnly Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionID, "Travel Requisition")
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="")>
    Public ReadOnly Property TravelRequisitionID() As Integer
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared SnTStartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.SnTStartDate, "Sn T Start Date")
    ''' <summary>
    ''' Gets the SnT Start Date value
    ''' </summary>
    <Display(Name:="SnT Start Date", Description:="")>
    Public ReadOnly Property SnTStartDate As Date
      Get
        Return GetProperty(SnTStartDateProperty)
      End Get
    End Property

    Public Shared SnTEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.SnTEndDate, "Sn T End Date")
    ''' <summary>
    ''' Gets the SnT End Date value
    ''' </summary>
    <Display(Name:="SnT End Date", Description:="")>
    Public ReadOnly Property SnTEndDate As Date
      Get
        Return GetProperty(SnTEndDateProperty)
      End Get
    End Property

    Public Shared HasFlightProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasFlight, "Has Flight")
    ''' <summary>
    ''' Gets the HasFlight value
    ''' </summary>
    Public ReadOnly Property HasFlight() As Boolean
      Get
        Return GetProperty(HasFlightProperty)
      End Get
    End Property

    Public Shared HasRentalCarProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasRentalCar, "Has Rental Car")
    ''' <summary>
    ''' Gets the HasRentalCar value
    ''' </summary>
    Public ReadOnly Property HasRentalCar() As Boolean
      Get
        Return GetProperty(HasRentalCarProperty)
      End Get
    End Property

    Public Shared HasAccommodationProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasAccommodation, "Has Accommodation")
    ''' <summary>
    ''' Gets the HasAccommodation value
    ''' </summary>
    Public ReadOnly Property HasAccommodation() As Boolean
      Get
        Return GetProperty(HasAccommodationProperty)
      End Get
    End Property

    Public Shared HasTravelAdvanceProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasTravelAdvance, "Has Travel Advance")
    ''' <summary>
    ''' Gets the HasTravelAdvance value
    ''' </summary>
    Public ReadOnly Property HasTravelAdvance() As Boolean
      Get
        Return GetProperty(HasTravelAdvanceProperty)
      End Get
    End Property

    Public Shared HasChauffeurDriveProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasChauffeurDrive, "Has Chauffeur Drive")
    ''' <summary>
    ''' Gets the HasChauffeurDrive value
    ''' </summary>
    Public ReadOnly Property HasChauffeurDrive() As Boolean
      Get
        Return GetProperty(HasChauffeurDriveProperty)
      End Get
    End Property

    Public Shared TotalBreakfastAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalBreakfastAmount, "Total Breakfast Amount", 0)
    ''' <summary>
    ''' Gets the Total Breakfast Amount value
    ''' </summary>
    <Display(Name:="Breakfast Amount", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property TotalBreakfastAmount() As Decimal
      Get
        Return SnTDetailList.Sum(Function(c) c.BreakfastAmount) 'GetProperty(TotalBreakfastAmountProperty)
      End Get
    End Property

    Public Shared TotalLunchAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalLunchAmount, "Total Lunch Amount", 0)
    ''' <summary>
    ''' Gets the Total Lunch Amount value
    ''' </summary>
    <Display(Name:="Lunch Amount", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property TotalLunchAmount() As Decimal
      Get
        Return SnTDetailList.Sum(Function(c) c.LunchAmount) 'GetProperty(TotalLunchAmountProperty)
      End Get
    End Property

    Public Shared TotalDinnerAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalDinnerAmount, "Total Dinner Amount", 0)
    ''' <summary>
    ''' Gets the Total Dinner Amount value
    ''' </summary>
    <Display(Name:="Dinner Amount", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property TotalDinnerAmount() As Decimal
      Get
        Return SnTDetailList.Sum(Function(c) c.DinnerAmount) 'GetProperty(TotalDinnerAmountProperty)
      End Get
    End Property

    Public Shared TotalIncidentalAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalIncidentalAmount, "Total Incidental Amount", 0)
    ''' <summary>
    ''' Gets the Total Incidental Amount value
    ''' </summary>
    <Display(Name:="Incidental Amount", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property TotalIncidentalAmount() As Decimal
      Get
        Return SnTDetailList.Sum(Function(c) c.IncidentalAmount) 'GetProperty(TotalIncidentalAmountProperty)
      End Get
    End Property

    Public Shared TotalSnTAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalSnTAmount, "Total Amount", 0)
    ''' <summary>
    ''' Gets and sets the Total Incidental Amount value
    ''' </summary>
    <Display(Name:="Total", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property TotalSnTAmount() As Decimal
      Get
        Return TotalBreakfastAmount + TotalLunchAmount + TotalDinnerAmount + TotalIncidentalAmount
      End Get
    End Property

    Public Shared HasSnTsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasSnTs, "HR Has SnTs", False)
    <Display(Name:="S&T Select Ind", Description:="")>
    Public ReadOnly Property HasSnTs() As Boolean
      Get
        Return Me.SnTDetailList.Count > 0
      End Get
    End Property

    Public Shared CancelledOnAllProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CancelledOnAll, "Cancelled On All")
    Public ReadOnly Property CancelledOnAll() As Boolean
      Get
        Return GetProperty(CancelledOnAllProperty)
      End Get
    End Property

#Region "Child Lists"

    Public Shared SnTDetailListProperty As PropertyInfo(Of OBLib.Travel.SnT.SnTDetailList) = RegisterProperty(Of OBLib.Travel.SnT.SnTDetailList)(Function(c) c.SnTDetailList, "SnT Detail List")

    Public ReadOnly Property SnTDetailList() As OBLib.Travel.SnT.SnTDetailList
      Get
        If GetProperty(SnTDetailListProperty) Is Nothing Then
          LoadProperty(SnTDetailListProperty, OBLib.Travel.SnT.SnTDetailList.NewAdHocSnTDetailList())
        End If
        Return GetProperty(SnTDetailListProperty)
      End Get
    End Property

#End Region

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravellerPaged(dr As SafeDataReader) As ROTravellerPaged

      Dim r As New ROTravellerPaged()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceProperty, .GetString(1))
        LoadProperty(CityCodeProperty, .GetString(2))
        LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(IDNoProperty, .GetString(4))
        LoadProperty(CellPhoneNumberProperty, .GetString(5))
        LoadProperty(EmailAddressProperty, .GetString(6))
        LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(SnTStartDateProperty, .GetValue(8))
        LoadProperty(SnTEndDateProperty, .GetValue(9))
        LoadProperty(HasFlightProperty, .GetBoolean(10))
        LoadProperty(HasRentalCarProperty, .GetBoolean(11))
        LoadProperty(HasAccommodationProperty, .GetBoolean(12))
        LoadProperty(HasTravelAdvanceProperty, .GetBoolean(13))
        LoadProperty(HasChauffeurDriveProperty, .GetBoolean(14))
        LoadProperty(CancelledOnAllProperty, .GetBoolean(15))
      End With

    End Sub

#End Region

  End Class

End Namespace
