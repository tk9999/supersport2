﻿' Generated 13 May 2016 08:16 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports OBLib.Travel.SnT

Namespace Travel.Travellers

  <Serializable()> _
  Public Class TravellerSnT
    Inherits OBBusinessBase(Of TravellerSnT)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Expanded"), AlwaysClean,
    SetExpression("TravellerSnTBO.IsExpandedSet(self)")>
    Public Overridable Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared TravelRequisitionTravellerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionTravellerID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property TravelRequisitionTravellerID() As Integer
      Get
        Return GetProperty(TravelRequisitionTravellerIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(TravelRequisitionTravellerIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(HumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Traveller", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City Code")
    ''' <summary>
    ''' Gets the City Code value
    ''' </summary>
    <Display(Name:="City Code", Description:="")>
    Public ReadOnly Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CityID, "City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property CityID() As Integer
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
    Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number")
    ''' <summary>
    ''' Gets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Cell Phone Number", Description:="")>
    Public ReadOnly Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionID, "Travel Requisition")
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="")>
    Public Property TravelRequisitionID() As Integer
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(TravelRequisitionIDProperty, value)
      End Set
    End Property

    Public Shared SnTStartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.SnTStartDate, "S&T Start Date")
    ''' <summary>
    ''' Gets the SnT Start Date value
    ''' </summary>
    <Display(Name:="S&T Start Date", Description:="")>
    Public Property SnTStartDate As Date
      Get
        Return GetProperty(SnTStartDateProperty)
      End Get
      Set(value As Date)
        SetProperty(SnTStartDateProperty, value)
      End Set
    End Property

    Public Shared SnTEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.SnTEndDate, "S&T End Date")
    ''' <summary>
    ''' Gets the SnT End Date value
    ''' </summary>
    <Display(Name:="S&T End Date", Description:="")>
    Public Property SnTEndDate As Date
      Get
        Return GetProperty(SnTEndDateProperty)
      End Get
      Set(value As Date)
        SetProperty(SnTEndDateProperty, value)
      End Set
    End Property

    'Public Shared HasFlightProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasFlight, "Has Flight")
    ' ''' <summary>
    ' ''' Gets the HasFlight value
    ' ''' </summary>
    'Public ReadOnly Property HasFlight() As Boolean
    '  Get
    '    Return GetProperty(HasFlightProperty)
    '  End Get
    'End Property

    'Public Shared HasRentalCarProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasRentalCar, "Has Rental Car")
    ' ''' <summary>
    ' ''' Gets the HasRentalCar value
    ' ''' </summary>
    'Public ReadOnly Property HasRentalCar() As Boolean
    '  Get
    '    Return GetProperty(HasRentalCarProperty)
    '  End Get
    'End Property

    'Public Shared HasAccommodationProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasAccommodation, "Has Accommodation")
    ' ''' <summary>
    ' ''' Gets the HasAccommodation value
    ' ''' </summary>
    'Public ReadOnly Property HasAccommodation() As Boolean
    '  Get
    '    Return GetProperty(HasAccommodationProperty)
    '  End Get
    'End Property

    'Public Shared HasTravelAdvanceProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasTravelAdvance, "Has Travel Advance")
    ' ''' <summary>
    ' ''' Gets the HasTravelAdvance value
    ' ''' </summary>
    'Public ReadOnly Property HasTravelAdvance() As Boolean
    '  Get
    '    Return GetProperty(HasTravelAdvanceProperty)
    '  End Get
    'End Property

    'Public Shared HasChauffeurDriveProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasChauffeurDrive, "Has Chauffeur Drive")
    ' ''' <summary>
    ' ''' Gets the HasChauffeurDrive value
    ' ''' </summary>
    'Public ReadOnly Property HasChauffeurDrive() As Boolean
    '  Get
    '    Return GetProperty(HasChauffeurDriveProperty)
    '  End Get
    'End Property

    Public Shared TotalBreakfastAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalBreakfastAmount, "Breakfast", 0)
    ''' <summary>
    ''' Gets the Total Breakfast Amount value
    ''' </summary>
    <Display(Name:="Breakfast", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property TotalBreakfastAmount() As Decimal
      Get
        Return GetProperty(TotalBreakfastAmountProperty)
      End Get
    End Property

    Public Shared TotalLunchAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalLunchAmount, "Lunch", 0)
    ''' <summary>
    ''' Gets the Total Lunch Amount value
    ''' </summary>
    <Display(Name:="Lunch", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property TotalLunchAmount() As Decimal
      Get
        Return GetProperty(TotalLunchAmountProperty)
      End Get
    End Property

    Public Shared TotalDinnerAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalDinnerAmount, "Dinner", 0)
    ''' <summary>
    ''' Gets the Total Dinner Amount value
    ''' </summary>
    <Display(Name:="Dinner", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property TotalDinnerAmount() As Decimal
      Get
        Return GetProperty(TotalDinnerAmountProperty)
      End Get
    End Property

    Public Shared TotalIncidentalAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalIncidentalAmount, "Incidental", 0)
    ''' <summary>
    ''' Gets the Total Incidental Amount value
    ''' </summary>
    <Display(Name:="Incidental", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property TotalIncidentalAmount() As Decimal
      Get
        Return GetProperty(TotalIncidentalAmountProperty)
      End Get
    End Property

    Public Shared TotalSnTAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalSnTAmount, "Total", 0)
    ''' <summary>
    ''' Gets and sets the Total Incidental Amount value
    ''' </summary>
    <Display(Name:="Total", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property TotalSnTAmount() As Decimal
      Get
        Return GetProperty(TotalSnTAmountProperty)
      End Get
    End Property

    Public Shared DefaultGroupSnTIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.DefaultGroupSnTID, Nothing)
    ''' <summary>
    ''' Gets and sets the Group SnT value
    ''' </summary>
    <Display(Name:="Bulk Policy", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(Travel.SnTs.ReadOnly.ROGroupSnTList)),
    SetExpression("TravellerSnTBO.DefaultGroupSnTIDSet(self)")>
    Public Property DefaultGroupSnTID() As Integer?
      Get
        Return GetProperty(DefaultGroupSnTIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DefaultGroupSnTIDProperty, Value)
      End Set
    End Property

    Public Shared SnTFetchedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.SnTFetched, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="SnTFetched"), AlwaysClean,
    SetExpression("TravellerSnTBO.SnTFetchedSet(self)")>
    Public Overridable Property SnTFetched() As Boolean
      Get
        Return GetProperty(SnTFetchedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SnTFetchedProperty, Value)
      End Set
    End Property

    'Public Shared HasSnTsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasSnTs, "HR Has SnTs", False)
    '<Display(Name:="S&T Select Ind", Description:="")>
    'Public ReadOnly Property HasSnTs() As Boolean
    '  Get
    '    Return Me.SnTDetailList.Count > 0
    '  End Get
    'End Property

    'Public Shared CancelledOnAllProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CancelledOnAll, "Cancelled On All")
    'Public ReadOnly Property CancelledOnAll() As Boolean
    '  Get
    '    Return GetProperty(CancelledOnAllProperty)
    '  End Get
    'End Property

#Region "Child Lists"

    Public Shared SnTDetailListProperty As PropertyInfo(Of OBLib.Travel.SnT.SnTDetailList) = RegisterProperty(Of OBLib.Travel.SnT.SnTDetailList)(Function(c) c.SnTDetailList, "SnT Detail List")

    Public ReadOnly Property SnTDetailList() As OBLib.Travel.SnT.SnTDetailList
      Get
        If GetProperty(SnTDetailListProperty) Is Nothing Then
          LoadProperty(SnTDetailListProperty, OBLib.Travel.SnT.SnTDetailList.NewAdHocSnTDetailList())
        End If
        Return GetProperty(SnTDetailListProperty)
      End Get
    End Property

#End Region

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TravelRequisitionTravellerIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetTravellerSnT(dr As SafeDataReader) As TravellerSnT

      Dim r As New TravellerSnT()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TravelRequisitionTravellerIDProperty, .GetInt32(0))
        LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(HumanResourceIDProperty, .GetInt32(2))
        LoadProperty(HumanResourceProperty, .GetString(3))
        LoadProperty(CityCodeProperty, .GetString(4))
        LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(IDNoProperty, .GetString(6))
        LoadProperty(CellPhoneNumberProperty, .GetString(7))
        LoadProperty(EmailAddressProperty, .GetString(8))
        LoadProperty(SnTStartDateProperty, .GetValue(9))
        LoadProperty(SnTEndDateProperty, .GetValue(10))
        LoadProperty(TotalBreakfastAmountProperty, .GetDecimal(11))
        LoadProperty(TotalLunchAmountProperty, .GetDecimal(12))
        LoadProperty(TotalDinnerAmountProperty, .GetDecimal(13))
        LoadProperty(TotalIncidentalAmountProperty, .GetDecimal(14))
        LoadProperty(TotalSnTAmountProperty, .GetDecimal(15))
        'LoadProperty(HasFlightProperty, .GetBoolean(10))
        'LoadProperty(HasRentalCarProperty, .GetBoolean(11))
        'LoadProperty(HasAccommodationProperty, .GetBoolean(12))
        'LoadProperty(HasTravelAdvanceProperty, .GetBoolean(13))
        'LoadProperty(HasChauffeurDriveProperty, .GetBoolean(14))
        'LoadProperty(CancelledOnAllProperty, .GetBoolean(15))
      End With

    End Sub

    Friend Sub Insert()

      '' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcsWeb.insTravelRequisitionTraveller"
      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTravellerSnT"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, TravelRequisitionTravellerIDProperty)

      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@SnTStartDate", (New SmartDate(GetProperty(SnTStartDateProperty)).DBValue))
      cm.Parameters.AddWithValue("@SnTEndDate", (New SmartDate(GetProperty(SnTEndDateProperty))).DBValue)
      'cm.Parameters.AddWithValue("@IsCancelled", GetProperty(IsCancelledProperty))
      'cm.Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
      'cm.Parameters.AddWithValue("@CancelledByUserID", Singular.Misc.NothingDBNull(GetProperty(CancelledByUserIDProperty)))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
      cm.Parameters.AddWithValue("@CreatedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(TravelRequisitionTravellerIDProperty, cm.Parameters("@TravelRequisitionTravellerID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then

      '  With cm
      '    .CommandType = CommandType.StoredProcedure

      '    Dim paramTravelRequisitionTravellerID As SqlParameter = .Parameters.Add("@TravelRequisitionTravellerID", SqlDbType.Int)
      '    paramTravelRequisitionTravellerID.Value = GetProperty(TravelRequisitionTravellerIDProperty)
      '    If Me.IsNew Then
      '      paramTravelRequisitionTravellerID.Direction = ParameterDirection.Output
      '    End If
      '    If Me.GetParent IsNot Nothing Then
      '      .Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
      '    Else
      '      .Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(GetProperty(TravelRequisitionIDProperty)))
      '    End If
      '    .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      '    .Parameters.AddWithValue("@SnTStartDate", (New SmartDate(If(GetProperty(SnTStartDateProperty) Is Nothing, Me.GetParent.StartDate, GetProperty(SnTStartDateProperty)))).DBValue)
      '    .Parameters.AddWithValue("@SnTEndDate", (New SmartDate(If(GetProperty(SnTEndDateProperty) Is Nothing, Me.GetParent.EndDate, GetProperty(SnTEndDateProperty)))).DBValue)
      '    .Parameters.AddWithValue("@IsCancelled", GetProperty(IsCancelledProperty))
      '    .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
      '    .Parameters.AddWithValue("@CancelledByUserID", Singular.Misc.NothingDBNull(GetProperty(CancelledByUserIDProperty)))
      '    .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      '    .ExecuteNonQuery()

      '    If Me.IsNew Then
      '      LoadProperty(TravelRequisitionTravellerIDProperty, paramTravelRequisitionTravellerID.Value)
      '    End If
      '    ' update child objects
      '    ' mChildList.Update()
      '    MarkOld()
      '  End With
      '  'Else
      'End If
      'If GetProperty(SnTDetailListProperty) IsNot Nothing Then
      '  Me.SnTDetailList.Update()
      'End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delTravelRequisitionTraveller"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@TravelRequisitionTravellerID", GetProperty(TravelRequisitionTravellerIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End Region

  End Class

End Namespace
