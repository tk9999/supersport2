﻿' Generated 13 May 2016 08:16 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Travellers.ReadOnly

  <Serializable()> _
  Public Class ROTravellerPagedList
    Inherits OBReadOnlyListBase(Of ROTravellerPagedList, ROTravellerPaged)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROTravellerPaged

      For Each child As ROTravellerPaged In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property TravelRequisitionID As Integer?

      <Singular.DataAnnotations.PrimarySearchField>
      Public Property HumanResource As String

      Public Sub New()
      End Sub

      Public Sub New(TravelRequisitionID As Integer?)
        Me.TravelRequisitionID = TravelRequisitionID
      End Sub

    End Class

    Public Shared Function NewROTravellerPagedList() As ROTravellerPagedList

      Return New ROTravellerPagedList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTravellerPagedList(TravelRequisitionID As Integer?) As ROTravellerPagedList

      Return DataPortal.Fetch(Of ROTravellerPagedList)(New Criteria(TravelRequisitionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTravellerPaged.GetROTravellerPaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parentAdHocSnT As ROTravellerPaged = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentAdHocSnT Is Nothing OrElse parentAdHocSnT.HumanResourceID <> sdr.GetInt32(1) Then
            parentAdHocSnT = Me.GetItem(sdr.GetInt32(1))
          End If
          If parentAdHocSnT IsNot Nothing Then
            parentAdHocSnT.SnTDetailList.RaiseListChangedEvents = False
            parentAdHocSnT.SnTDetailList.Add(OBLib.Travel.SnT.SnTDetail.GetSnTDetail(sdr))
            parentAdHocSnT.SnTDetailList.RaiseListChangedEvents = True
          End If
        End While
      End If

      For Each child As ROTravellerPaged In Me
        For Each SnTDetail As OBLib.Travel.SnT.SnTDetail In child.SnTDetailList
          SnTDetail.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravellerPagedList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            cm.Parameters.AddWithValue("@HumanResource", Singular.Strings.MakeEmptyDBNull(crit.HumanResource))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace
