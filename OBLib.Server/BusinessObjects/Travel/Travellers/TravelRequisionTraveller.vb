﻿' Generated 03 Aug 2015 10:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports OBLib.Productions
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.Misc

Namespace Travel.Travellers

  <Serializable()> _
  Public Class TravelRequisitionTraveller
    Inherits OBBusinessBase(Of TravelRequisitionTraveller)

#Region " Properties and Methods "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Expanded"), AlwaysClean>
    Public Overridable Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return TravelRequisitionTravellerBO.toString(self)")

    Public Shared TravelRequisitionTravellerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionTravellerID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property TravelRequisitionTravellerID() As Integer
      Get
        Return GetProperty(TravelRequisitionTravellerIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionID, "Travel Requisition", 0)
    ''' <summary>
    ''' Gets and sets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="ID of the associated Travel Requisition"),
    Required(ErrorMessage:="Travel Requisition required")>
    Public Property TravelRequisitionID() As Integer
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TravelRequisitionIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource", 0)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="ID of the Human Resource which this S&T is allocated to"),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared SnTStartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SnTStartDate, "S&T Start Date")
    ''' <summary>
    ''' Gets and sets the S&T Start Date value
    ''' </summary>
    <Display(Name:="S&T Start Date", Description:="Date of the first S&T allocated to this Traveller")>
    Public Property SnTStartDate As DateTime?
      Get
        Return GetProperty(SnTStartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SnTStartDateProperty, Value)
      End Set
    End Property

    Public Shared SnTEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SnTEndDate, "S&T End Date")
    ''' <summary>
    ''' Gets and sets the S&T End Date value
    ''' </summary>
    <Display(Name:="S&T End Date", Description:="Date of the last S&T allocated to this Traveller")>
    Public Property SnTEndDate As DateTime?
      Get
        Return GetProperty(SnTEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SnTEndDateProperty, Value)
      End Set
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Is Cancelled value
    ''' </summary>
    <Display(Name:="Is Cancelled", Description:="Tick indicates thether or not the Traveller has been cancelled")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="Reason for the Traveller to be Cancelled"),
    StringLength(500, ErrorMessage:="Cancelled Reason cannot be more than 500 characters")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
      End Set
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="ID of the User who cancelled the Traveller")>
    Public Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CancelledByUserIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No", "")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No")>
    Public Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IDNoProperty, Value)
      End Set
    End Property

    Public Shared CellPhoneNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CellPhoneNumber, "Cell Phone Number", "")
    ''' <summary>
    ''' Gets the Cell Phone Number value
    ''' </summary>
    <Display(Name:="Cell Phone Number")>
    Public Property CellPhoneNumber() As String
      Get
        Return GetProperty(CellPhoneNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CellPhoneNumberProperty, Value)
      End Set
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="")>
    Public Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmailAddressProperty, Value)
      End Set
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City Code", "")
    ''' <summary>
    ''' Gets the City Code value
    ''' </summary>
    <Display(Name:="City Code", Description:="")>
    Public ReadOnly Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared FlightCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FlightCount, "Flights", 0)
    ''' <summary>
    ''' Gets the Total Breakfast Amount value
    ''' </summary>
    <Display(Name:="Flights", Description:="")>
    Public Property FlightCount() As Integer
      Get
        Return GetProperty(FlightCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(FlightCountProperty, value)
      End Set
    End Property

    Public Shared RentalCarCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarCount, "Rental Cars", 0)
    ''' <summary>
    ''' Gets the Total Breakfast Amount value
    ''' </summary>
    <Display(Name:="Rental Cars", Description:="")>
    Public Property RentalCarCount() As Integer
      Get
        Return GetProperty(RentalCarCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(RentalCarCountProperty, value)
      End Set
    End Property

    Public Shared AccommodationCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationCount, "Accommodation", 0)
    ''' <summary>
    ''' Gets the Total Breakfast Amount value
    ''' </summary>
    <Display(Name:="Accommodation", Description:="")>
    Public Property AccommodationCount() As Integer
      Get
        Return GetProperty(AccommodationCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(AccommodationCountProperty, value)
      End Set
    End Property

    Public Shared ChauffeurCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChauffeurCount, "Chauffeur", 0)
    ''' <summary>
    ''' Gets the Total Breakfast Amount value
    ''' </summary>
    <Display(Name:="Chauffeur", Description:="")>
    Public Property ChauffeurCount() As Integer
      Get
        Return GetProperty(ChauffeurCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(ChauffeurCountProperty, value)
      End Set
    End Property

    Public Shared IsCancelledOriginalProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelledOriginal, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Is Cancelled value
    ''' </summary>
    <Display(Name:="Is Cancelled", Description:="Tick indicates thether or not the Traveller has been cancelled")>
    Public Property IsCancelledOriginal() As Boolean
      Get
        Return GetProperty(IsCancelledOriginalProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledOriginalProperty, Value)
      End Set
    End Property

    'Public Shared TotalBreakfastAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalBreakfastAmount, "Total Breakfast Amount", 0)
    ' ''' <summary>
    ' ''' Gets the Total Breakfast Amount value
    ' ''' </summary>
    '<Display(Name:="Total Breakfast Amount", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    'Public ReadOnly Property TotalBreakfastAmount() As Decimal
    '  Get
    '    Return SnTDetailList.Sum(Function(c) c.BreakfastAmount) 'GetProperty(TotalBreakfastAmountProperty)
    '  End Get
    'End Property

    'Public Shared TotalLunchAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalLunchAmount, "Total Lunch Amount", 0)
    ' ''' <summary>
    ' ''' Gets the Total Lunch Amount value
    ' ''' </summary>
    '<Display(Name:="Total Lunch Amount", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    'Public ReadOnly Property TotalLunchAmount() As Decimal
    '  Get
    '    Return SnTDetailList.Sum(Function(c) c.LunchAmount) 'GetProperty(TotalLunchAmountProperty)
    '  End Get
    'End Property

    'Public Shared TotalDinnerAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalDinnerAmount, "Total Dinner Amount", 0)
    ' ''' <summary>
    ' ''' Gets the Total Dinner Amount value
    ' ''' </summary>
    '<Display(Name:="Total Dinner Amount", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    'Public ReadOnly Property TotalDinnerAmount() As Decimal
    '  Get
    '    Return SnTDetailList.Sum(Function(c) c.DinnerAmount) 'GetProperty(TotalDinnerAmountProperty)
    '  End Get
    'End Property

    'Public Shared TotalIncidentalAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalIncidentalAmount, "Total Incidental Amount", 0)
    ' ''' <summary>
    ' ''' Gets the Total Incidental Amount value
    ' ''' </summary>
    '<Display(Name:="Total Incidental Amount", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    'Public ReadOnly Property TotalIncidentalAmount() As Decimal
    '  Get
    '    Return SnTDetailList.Sum(Function(c) c.IncidentalAmount) 'GetProperty(TotalIncidentalAmountProperty)
    '  End Get
    'End Property

    'Public Shared TotalSnTAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalSnTAmount, "Total Amount", 0)
    ' ''' <summary>
    ' ''' Gets and sets the Total Incidental Amount value
    ' ''' </summary>
    '<Display(Name:="Total Amount", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    'Public ReadOnly Property TotalSnTAmount() As Decimal
    '  Get
    '    Return TotalBreakfastAmount + TotalLunchAmount + TotalDinnerAmount + TotalIncidentalAmount
    '  End Get
    'End Property

    'Public Shared HasSnTsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasSnTs, "HR Has SnTs", False)
    ' ''' <summary>
    ' ''' Gets and sets the S&T Select Ind value
    ' ''' </summary>
    '<Display(Name:="S&T Select Ind", Description:="")>
    'Public ReadOnly Property HasSnTs() As Boolean
    '  Get
    '    Return Me.SnTDetailList.Count > 0
    '  End Get
    'End Property

    'Public Shared SnTSelectIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SnTSelectInd, "S&T Select Ind", False)
    ' ''' <summary>
    ' ''' Gets and sets the S&T Select Ind value
    ' ''' </summary>
    '<Display(Name:="S&T Select Ind", Description:="")>
    'Public Property SnTSelectInd() As Boolean
    '  Get
    '    Return GetProperty(SnTSelectIndProperty)
    '  End Get
    '  Set(ByVal Value As Boolean)
    '    SetProperty(SnTSelectIndProperty, Value)
    '  End Set
    'End Property

    'Public Shared GroupPolicyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.GroupPolicyID, "Group Policy", CInt(OBLib.CommonData.Enums.GroupSnTPolicies.SouthAfricaDomestic))
    ' ''' <summary>
    ' ''' Gets and sets the Group Policy value
    ' ''' </summary>
    '<Display(Name:="Policy", Description:=""),
    'Singular.DataAnnotations.DropDownWeb(GetType(Travel.SnTs.ReadOnly.ROGroupSnTList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, DisplayMember:="DestinationCountry")>
    'Public Property GroupPolicyID() As Integer?
    '  Get
    '    Dim SADomesticNewSnTPolicy As OBLib.Travel.SnTs.ReadOnly.ROGroupSnT = OBLib.CommonData.Lists.ROGroupSnTList.Where(Function(c) Singular.Misc.CompareSafe(c.GroupSnTID, 17))(0)
    '    If Singular.Misc.CompareSafe(GetProperty(GroupPolicyIDProperty).Value, 13) Then
    '      If Me.SnTStartDate >= SADomesticNewSnTPolicy.EffectiveDate Then
    '        Return SADomesticNewSnTPolicy.GroupSnTID
    '      End If
    '    End If
    '    Return GetProperty(GroupPolicyIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(GroupPolicyIDProperty, Value)
    '  End Set
    'End Property

#End Region

    '#Region "Child Lists"

    '    Public Shared ROTravellerFlightListProperty As PropertyInfo(Of OBLib.Travel.Flights.ReadOnly.ROTravellerFlightList) = RegisterProperty(Of OBLib.Travel.Flights.ReadOnly.ROTravellerFlightList)(Function(c) c.ROTravellerFlightList, "RO Traveller Flight List")
    '    Public ReadOnly Property ROTravellerFlightList() As OBLib.Travel.Flights.ReadOnly.ROTravellerFlightList
    '      Get
    '        If GetProperty(ROTravellerFlightListProperty) Is Nothing Then
    '          LoadProperty(ROTravellerFlightListProperty, OBLib.Travel.Flights.ReadOnly.ROTravellerFlightList.NewROTravellerFlightList())
    '        End If
    '        Return GetProperty(ROTravellerFlightListProperty)
    '      End Get
    '    End Property

    '    Public Shared ROTravellerRentalCarListProperty As PropertyInfo(Of OBLib.Travel.RentalCars.ReadOnly.ROTravellerRentalCarList) = RegisterProperty(Of OBLib.Travel.RentalCars.ReadOnly.ROTravellerRentalCarList)(Function(c) c.ROTravellerRentalCarList, "RO Traveller Rental Car List")
    '    Public ReadOnly Property ROTravellerRentalCarList() As OBLib.Travel.RentalCars.ReadOnly.ROTravellerRentalCarList
    '      Get
    '        If GetProperty(ROTravellerRentalCarListProperty) Is Nothing Then
    '          LoadProperty(ROTravellerRentalCarListProperty, OBLib.Travel.RentalCars.ReadOnly.ROTravellerRentalCarList.NewROTravellerRentalCarList())
    '        End If
    '        Return GetProperty(ROTravellerRentalCarListProperty)
    '      End Get
    '    End Property

    '    Public Shared ROTravellerAccommodationListProperty As PropertyInfo(Of OBLib.Travel.Accommodation.ReadOnly.ROTravellerAccommodationList) = RegisterProperty(Of OBLib.Travel.Accommodation.ReadOnly.ROTravellerAccommodationList)(Function(c) c.ROTravellerAccommodationList, "RO Traveller Accommodation List")
    '    Public ReadOnly Property ROTravellerAccommodationList() As OBLib.Travel.Accommodation.ReadOnly.ROTravellerAccommodationList
    '      Get
    '        If GetProperty(ROTravellerAccommodationListProperty) Is Nothing Then
    '          LoadProperty(ROTravellerAccommodationListProperty, OBLib.Travel.Accommodation.ReadOnly.ROTravellerAccommodationList.NewROTravellerAccommodationList())
    '        End If
    '        Return GetProperty(ROTravellerAccommodationListProperty)
    '      End Get
    '    End Property

    '    Public Shared ROTravellerTravelAdvanceDetailListProperty As PropertyInfo(Of OBLib.Travel.TravelAdvances.ReadOnly.ROTravellerProductionAdvanceList) = RegisterProperty(Of OBLib.Travel.TravelAdvances.ReadOnly.ROTravellerProductionAdvanceList)(Function(c) c.ROTravellerProductionAdvanceList, "RO Traveller Travel Advance Detail List")
    '    Public ReadOnly Property ROTravellerProductionAdvanceList() As OBLib.Travel.TravelAdvances.ReadOnly.ROTravellerProductionAdvanceList
    '      Get
    '        If GetProperty(ROTravellerTravelAdvanceDetailListProperty) Is Nothing Then
    '          LoadProperty(ROTravellerTravelAdvanceDetailListProperty, OBLib.Travel.TravelAdvances.ReadOnly.ROTravellerProductionAdvanceList.NewROTravellerProductionAdvanceList())
    '        End If
    '        Return GetProperty(ROTravellerTravelAdvanceDetailListProperty)
    '      End Get
    '    End Property

    '    Public Shared ROTravellerChauffeurListProperty As PropertyInfo(Of OBLib.Travel.Chauffeurs.ReadOnly.ROTravellerChauffeurList) = RegisterProperty(Of OBLib.Travel.Chauffeurs.ReadOnly.ROTravellerChauffeurList)(Function(c) c.ROTravellerChauffeurList, "RO Traveller Travel Chauffeur List")
    '    Public ReadOnly Property ROTravellerChauffeurList() As OBLib.Travel.Chauffeurs.ReadOnly.ROTravellerChauffeurList
    '      Get
    '        If GetProperty(ROTravellerChauffeurListProperty) Is Nothing Then
    '          LoadProperty(ROTravellerChauffeurListProperty, OBLib.Travel.Chauffeurs.ReadOnly.ROTravellerChauffeurList.NewROTravellerChauffeurList())
    '        End If
    '        Return GetProperty(ROTravellerChauffeurListProperty)
    '      End Get
    '    End Property

    '    Public Shared SnTDetailListProperty As PropertyInfo(Of OBLib.Travel.SnT.SnTDetailList) = RegisterProperty(Of OBLib.Travel.SnT.SnTDetailList)(Function(c) c.SnTDetailList, "Ad Hoc SnT Detail List")
    '    Public ReadOnly Property SnTDetailList() As OBLib.Travel.SnT.SnTDetailList
    '      Get
    '        If GetProperty(SnTDetailListProperty) Is Nothing Then
    '          LoadProperty(SnTDetailListProperty, OBLib.Travel.SnT.SnTDetailList.NewAdHocSnTDetailList())
    '        End If
    '        Return GetProperty(SnTDetailListProperty)
    '      End Get
    '    End Property

    '#End Region

#Region " Methods "

    Public Function GetParent() As OBLib.Travel.TravelRequisition

      Return CType(CType(Me.Parent, TravelRequisitionTravellerList).Parent, OBLib.Travel.TravelRequisition)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TravelRequisitionTravellerIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CancelledReason.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Travel Requision Traveller")
        Else
          Return String.Format("Blank {0}", "Travel Requision Traveller")
        End If
      Else
        Return Me.CancelledReason
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

    Public Function CancelAll() As Singular.Web.Result
      Me.IsCancelled = True
      Me.CancelledReason = "Cancelled from travellers tab"
      'Me.CancelledDateTime = Now
      Dim sh As Singular.SaveHelper = TrySave(GetType(OBLib.Travel.Travellers.TravelRequisitionTravellerList))
      If sh.Success Then
        Return New Singular.Web.Result(True) With {.Data = Me}
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = sh.ErrorText}
      End If
    End Function

    'Public Sub UpdateSnTDetails(SnTDay As Date, GroupSnTPolicyID As Integer, BreakfastInd As Boolean, LunchInd As Boolean, DinnerInd As Boolean, IncidentalInd As Boolean,
    '                        ChangeReason As String, AdHocBookingID As Integer?, ProductionID As Integer?, CountryID As Integer)

    '  If ProductionID Is Nothing Then
    '    Me.SnTDetailList.UpdateAdHocSnTAmounts(SnTDay, GroupSnTPolicyID, BreakfastInd, LunchInd, DinnerInd, IncidentalInd, ChangeReason, AdHocBookingID, CountryID)
    '  Else
    '    Me.SnTDetailList.UpdateOBSnTAmounts(SnTDay, GroupSnTPolicyID, BreakfastInd, LunchInd, DinnerInd, IncidentalInd, ChangeReason, ProductionID, CountryID)
    '  End If
    'End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(SnTEndDateProperty)
        .AddTriggerProperty(SnTStartDateProperty)
        .JavascriptRuleFunctionName = "TravelRequisitionTravellerBO.CheckSnTDates"
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTravelRequisionTraveller() method.

    End Sub

    Public Shared Function NewTravelRequisitionTraveller() As TravelRequisitionTraveller

      Return DataPortal.CreateChild(Of TravelRequisitionTraveller)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTravelRequisitionTraveller(dr As SafeDataReader) As TravelRequisitionTraveller

      Dim t As New TravelRequisitionTraveller()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TravelRequisitionTravellerIDProperty, .GetInt32(0))
          LoadProperty(TravelRequisitionIDProperty, .GetInt32(1))
          LoadProperty(HumanResourceIDProperty, .GetInt32(2))
          LoadProperty(SnTStartDateProperty, .GetDateTime(3))
          LoadProperty(SnTEndDateProperty, .GetDateTime(4))
          LoadProperty(IsCancelledProperty, .GetBoolean(5))
          LoadProperty(CancelledReasonProperty, .GetString(6))
          LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(HumanResourceProperty, .GetString(8))
          LoadProperty(IDNoProperty, .GetString(9))
          LoadProperty(CellPhoneNumberProperty, .GetString(10))
          LoadProperty(EmailAddressProperty, .GetString(11))
          LoadProperty(CityCodeProperty, .GetString(12))
          LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(CreatedByProperty, .GetInt32(14))
          LoadProperty(CreatedDateTimeProperty, .GetValue(15))
          LoadProperty(ModifiedByProperty, .GetInt32(16))
          LoadProperty(ModifiedDateTimeProperty, .GetValue(17))
          LoadProperty(FlightCountProperty, .GetInt32(18))
          LoadProperty(RentalCarCountProperty, .GetInt32(18))
          LoadProperty(AccommodationCountProperty, .GetInt32(18))
          LoadProperty(ChauffeurCountProperty, .GetInt32(18))
          LoadProperty(IsCancelledOriginalProperty, IsCancelled)
          'LoadProperty(TotalBreakfastAmountProperty, .GetDecimal(18))
          'LoadProperty(TotalLunchAmountProperty, .GetDecimal(19))
          'LoadProperty(TotalDinnerAmountProperty, .GetDecimal(20))
          'LoadProperty(TotalIncidentalAmountProperty, .GetDecimal(21))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTravelRequisitionTraveller"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTravelRequisitionTraveller"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          If Not IsCancelledOriginal AndAlso IsCancelled Then
            CancelledByUserID = OBLib.Security.Settings.CurrentUserID
            CancelledReason = "Cancelled from Travellers tab"
          End If

          Dim paramTravelRequisitionTravellerID As SqlParameter = .Parameters.Add("@TravelRequisitionTravellerID", SqlDbType.Int)
          paramTravelRequisitionTravellerID.Value = GetProperty(TravelRequisitionTravellerIDProperty)
          If Me.IsNew Then
            paramTravelRequisitionTravellerID.Direction = ParameterDirection.Output
          End If
          If Me.GetParent IsNot Nothing Then
            .Parameters.AddWithValue("@TravelRequisitionID", Singular.Misc.NothingDBNull(Me.GetParent.TravelRequisitionID))
          Else
            .Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(GetProperty(TravelRequisitionIDProperty)))
          End If
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@SnTStartDate", (New SmartDate(If(GetProperty(SnTStartDateProperty) Is Nothing, Me.GetParent.StartDate, GetProperty(SnTStartDateProperty)))).DBValue)
          .Parameters.AddWithValue("@SnTEndDate", (New SmartDate(If(GetProperty(SnTEndDateProperty) Is Nothing, Me.GetParent.EndDate, GetProperty(SnTEndDateProperty)))).DBValue)
          .Parameters.AddWithValue("@IsCancelled", GetProperty(IsCancelledProperty))
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@CancelledByUserID", Singular.Misc.NothingDBNull(GetProperty(CancelledByUserIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@FlightCount", GetProperty(FlightCountProperty))
          .Parameters.AddWithValue("@RentalCarCount", GetProperty(RentalCarCountProperty))
          .Parameters.AddWithValue("@AccommodationCount", GetProperty(AccommodationCountProperty))
          .Parameters.AddWithValue("@ChauffeurCount", GetProperty(ChauffeurCountProperty))
          .Parameters.AddWithValue("@IsCancelledOriginal", GetProperty(IsCancelledOriginalProperty))

          .ExecuteNonQuery()

          LoadProperty(IsCancelledOriginalProperty, IsCancelled)

          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
        'Else
      End If
      'If GetProperty(SnTDetailListProperty) IsNot Nothing Then
      '  Me.SnTDetailList.Update()
      'End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTravelRequisitionTraveller"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TravelRequisitionTravellerID", GetProperty(TravelRequisitionTravellerIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace