﻿' Generated 03 Aug 2015 10:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Travel.Travellers

  <Serializable()> _
  Public Class TravelRequisitionTravellerList
    Inherits OBBusinessListBase(Of TravelRequisitionTravellerList, TravelRequisitionTraveller)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As TravelRequisitionTraveller

      For Each child As TravelRequisitionTraveller In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Travel Requision Travellers"

    End Function

    Public Overloads Function GetXmlIDs() As String

      Dim HumanResourceIDs As String = OBLib.Helpers.MiscHelper.IntegerArrayToXML(Me.Select(Of Integer)(Function(c) c.HumanResourceID).ToArray)

      Return HumanResourceIDs

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      'Public Property StartDate As DateTime? = Nothing
      'Public Property EndDate As DateTime? = Nothing
      'Public Property HumanResourceID As Integer?
      'Public Property AdHocBookingID As Integer?
      Public Property TravelRequisitionID As Integer?

      Public Sub New()

      End Sub

      Public Sub New(TravelRequisitionID As Integer?)
        'StartDate As DateTime?, EndDate As DateTime?, HumanResourceID As Integer?, AdHocBookingID As Integer?, 
        'Me.StartDate = StartDate
        'Me.EndDate = EndDate
        'Me.HumanResourceID = HumanResourceID
        'Me.AdHocBookingID = AdHocBookingID
        Me.TravelRequisitionID = TravelRequisitionID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewTravelRequisitionTravellerList() As TravelRequisitionTravellerList

      Return New TravelRequisitionTravellerList()

    End Function

    Public Shared Sub BeginGetTravelRequisitionTravellerList(CallBack As EventHandler(Of DataPortalResult(Of TravelRequisitionTravellerList)))

      Dim dp As New DataPortal(Of TravelRequisitionTravellerList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetTravelRequisitionTravellerList() As TravelRequisitionTravellerList

      Return DataPortal.Fetch(Of TravelRequisitionTravellerList)(New Criteria())

    End Function

    Public Shared Function GetTravelRequisitionTravellerList(TravelRequisitionID As Integer?) As TravelRequisitionTravellerList
      'StartDate As DateTime?, EndDate As DateTime?, HumanResourceID As Integer, AdHocBookingID As Integer?, 
      'StartDate, EndDate, HumanResourceID, AdHocBookingID, 
      Return DataPortal.Fetch(Of TravelRequisitionTravellerList)(New Criteria(TravelRequisitionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(TravelRequisitionTraveller.GetTravelRequisitionTraveller(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'Dim parent As TravelRequisitionTraveller = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If

      '    parent.SnTDetailList.RaiseListChangedEvents = False
      '    parent.SnTDetailList.Add(OBLib.Travel.SnT.SnTDetail.GetSnTDetail(sdr))
      '    parent.SnTDetailList.RaiseListChangedEvents = True

      '  End While
      'End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getTravelRequisitionTravellerList"
            'cm.Parameters.AddWithValue("@StartDate", crit.StartDate)
            'cm.Parameters.AddWithValue("@EndDate", crit.EndDate)
            'cm.Parameters.AddWithValue("@HumanResourceID", crit.HumanResourceID)
            'cm.Parameters.AddWithValue("@AdHocBookingID", crit.AdHocBookingID)
            cm.Parameters.AddWithValue("@TravelRequisitionID", crit.TravelRequisitionID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As TravelRequisitionTraveller In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As TravelRequisitionTraveller In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace