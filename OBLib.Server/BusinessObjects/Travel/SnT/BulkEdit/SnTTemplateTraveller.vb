﻿' Generated 27 Aug 2014 08:22 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.SnT

  <Serializable()> _
  Public Class SnTTemplateTraveller
    Inherits OBBusinessBase(Of SnTTemplateTraveller)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TravelRequisitionTravellerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionTravellerID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property TravelRequisitionTravellerID() As Integer
      Get
        Return GetProperty(TravelRequisitionTravellerIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(TravelRequisitionTravellerIDProperty, value)
      End Set
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property TravelRequisitionID() As Integer
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(TravelRequisitionIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared SnTStartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SnTStartDate, "From Date")
    <Display(Name:="From Date")>
    Public Property SnTStartDate As DateTime?
      Get
        Return GetProperty(SnTStartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SnTStartDateProperty, Value)
      End Set
    End Property

    Public Shared SnTEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SnTEndDate, "To Date")
    <Display(Name:="To Date")>
    Public Property SnTEndDate As DateTime?
      Get
        Return GetProperty(SnTEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SnTEndDateProperty, Value)
      End Set
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City Code", "")
    ''' <summary>
    ''' Gets and sets the City Code value
    ''' </summary>
    <Display(Name:="City Code", Description:="")>
    Public Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CityCodeProperty, Value)
      End Set
    End Property

    Public Shared CrewTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CrewType, "Crew Type", "")
    ''' <summary>
    ''' Gets and sets the CrewType value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="")>
    Public Property CrewType() As String
      Get
        Return GetProperty(CrewTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CrewTypeProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TravelRequisitionTravellerIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Group Sn T Bulk Crew")
        Else
          Return String.Format("Blank {0}", "Group Sn T Bulk Crew")
        End If
      Else
        Return Me.HumanResource
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSnTTemplateTraveller() method.

    End Sub

    Public Shared Function NewSnTTemplateTraveller() As SnTTemplateTraveller

      Return DataPortal.CreateChild(Of SnTTemplateTraveller)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSnTTemplateTraveller(dr As SafeDataReader) As SnTTemplateTraveller

      Dim g As New SnTTemplateTraveller()
      g.Fetch(dr)
      Return g

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TravelRequisitionTravellerIDProperty, .GetInt32(0))
          LoadProperty(TravelRequisitionIDProperty, .GetInt32(1))
          LoadProperty(HumanResourceIDProperty, .GetInt32(2))
          LoadProperty(HumanResourceProperty, .GetString(3))
          LoadProperty(SnTStartDateProperty, .GetDateTime(4))
          LoadProperty(SnTEndDateProperty, .GetDateTime(5))
          LoadProperty(CityIDProperty, ZeroNothing(.GetInt32(6)))
          LoadProperty(CityCodeProperty, .GetString(7))
          LoadProperty(CrewTypeProperty, .GetString(8))
          LoadProperty(DisciplineProperty, .GetString(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      '' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcsWeb.insSnTTemplateTraveller"
      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      '' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "UpdProcsWeb.updSnTTemplateTraveller"
      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then
      '  With cm
      '    .CommandType = CommandType.StoredProcedure
      '    Dim paramSnTInd As SqlParameter = .Parameters.Add("@SnTInd", SqlDbType.Int)
      '    paramSnTInd.Value = GetProperty(SnTIndProperty)
      '    If Me.IsNew Then
      '      paramSnTInd.Direction = ParameterDirection.Output
      '    End If
      '    .Parameters.AddWithValue("@HumanResource", GetProperty(HumanResourceProperty))
      '    .Parameters.AddWithValue("@CityCode", GetProperty(CityCodeProperty))
      '    .Parameters.AddWithValue("@Discipline", GetProperty(DisciplineProperty))
      '    .ExecuteNonQuery()
      '    If Me.IsNew Then
      '      LoadProperty(SnTIndProperty, paramSnTInd.Value)
      '    End If
      '    ' update child objects
      '    ' mChildList.Update()
      '    MarkOld()
      '  End With
      'Else
      'End If

    End Sub

    Friend Sub DeleteSelf()

      '' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delSnTTemplateTraveller"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@SnTInd", GetProperty(SnTIndProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub
      'cm.ExecuteNonQuery()
      'MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace