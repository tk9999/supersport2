﻿' Generated 02 Sep 2014 19:33 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.SnT

  <Serializable()> _
  Public Class SnTTemplate
    Inherits OBBusinessBase(Of SnTTemplate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "SnTTemplateBO.SnTTemplateBOToString(self)")

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelRequisitionID, "Travel Requisition", Nothing)
    <Display(Name:="Travel Requisition", Description:=""),
    Required(ErrorMessage:="Travel Requisition required")>
    Public Property TravelRequisitionID() As Integer?
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TravelRequisitionIDProperty, Value)
      End Set
    End Property

    Public Shared GroupSnTIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.GroupSnTID, "Group Policy", CInt(OBLib.CommonData.Enums.GroupSnTPolicies.SouthAfricaDomesticNew))
    ''' <summary>
    ''' Gets and sets the Group Policy value
    ''' </summary>
    <Display(Name:="Policy", Description:=""),
    Required(ErrorMessage:="Group Policy required"),
    Singular.DataAnnotations.DropDownWeb(GetType(Travel.SnTs.ReadOnly.ROGroupSnTList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, DisplayMember:="DestinationCountry"),
    SetExpression("SnTTemplateBO.GroupSnTIDSet(self)")>
    Public Property GroupSnTID() As Integer?
      Get
        Return GetProperty(GroupSnTIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(GroupSnTIDProperty, Value)
      End Set
    End Property

    Public Shared CurrencyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CurrencyID, "Currency", CInt(OBLib.CommonData.Enums.Currency.SouthAfricanRand))
    ''' <summary>
    ''' Gets and sets the Group Policy value
    ''' </summary>
    <Display(Name:="Currency", Description:=""),
    Required(ErrorMessage:="Currency required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCurrencyList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, DisplayMember:="Currency", valueMember:="CurrencyID")>
    Public Property CurrencyID() As Integer?
      Get
        Return GetProperty(CurrencyIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CurrencyIDProperty, Value)
      End Set
    End Property

    Public Shared BreakfastIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.BreakfastInd, "Breakfast", False)
    ''' <summary>
    ''' Gets and sets the Breakfast value
    ''' </summary>
    <Display(Name:="Breakfast", Description:=""),
    Required(ErrorMessage:="Breakfast required")>
    Public Property BreakfastInd() As Boolean
      Get
        Return GetProperty(BreakfastIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(BreakfastIndProperty, Value)
      End Set
    End Property

    Public Shared LunchIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.LunchInd, "Lunch", False)
    ''' <summary>
    ''' Gets and sets the Lunch value
    ''' </summary>
    <Display(Name:="Lunch", Description:=""),
    Required(ErrorMessage:="Lunch required")>
    Public Property LunchInd() As Boolean
      Get
        Return GetProperty(LunchIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(LunchIndProperty, Value)
      End Set
    End Property

    Public Shared DinnerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DinnerInd, "Dinner", False)
    ''' <summary>
    ''' Gets and sets the Dinner value
    ''' </summary>
    <Display(Name:="Dinner", Description:=""),
    Required(ErrorMessage:="Dinner required")>
    Public Property DinnerInd() As Boolean
      Get
        Return GetProperty(DinnerIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DinnerIndProperty, Value)
      End Set
    End Property

    Public Shared IncidentalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IncidentalInd, "Incidental", False)
    ''' <summary>
    ''' Gets and sets the Incidental value
    ''' </summary>
    <Display(Name:="Incidental", Description:=""),
    Required(ErrorMessage:="Incidental required")>
    Public Property IncidentalInd() As Boolean
      Get
        Return GetProperty(IncidentalIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IncidentalIndProperty, Value)
      End Set
    End Property

    Public Shared ChangedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangedReason, "Change Reason", "")
    ''' <summary>
    ''' Gets and sets the Change Reason value
    ''' </summary>
    <Display(Name:="Change Reason", Description:="")>
    Public Property ChangedReason() As String
      Get
        Return GetProperty(ChangedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChangedReasonProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Private Property GroupPolicyID As Object

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(GroupSnTIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ChangedReason.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Bulk Group Policy Detail")
        Else
          Return String.Format("Blank {0}", "Bulk Group Policy Detail")
        End If
      Else
        Return Me.ChangedReason
      End If

    End Function

#End Region

#Region " Child Lists "

    Public Shared SnTTemplateTravellerListProperty As PropertyInfo(Of OBLib.Travel.SnT.SnTTemplateTravellerList) = RegisterProperty(Of OBLib.Travel.SnT.SnTTemplateTravellerList)(Function(c) c.SnTTemplateTravellerList, "Traveller List")
    Public ReadOnly Property SnTTemplateTravellerList() As OBLib.Travel.SnT.SnTTemplateTravellerList
      Get
        If GetProperty(SnTTemplateTravellerListProperty) Is Nothing Then
          LoadProperty(SnTTemplateTravellerListProperty, New OBLib.Travel.SnT.SnTTemplateTravellerList())
        End If
        Return GetProperty(SnTTemplateTravellerListProperty)
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(ChangedReasonProperty)
        .JavascriptRuleFunctionName = "SnTTemplateBO.TotalAmountValid"
        .ServerRuleFunction = AddressOf TotalAmountValid
      End With

    End Sub

    Public Shared Function TotalAmountValid(BGPD As SnTTemplate) As String
      Dim ErrorString = ""
      If BGPD IsNot Nothing Then
        If BGPD.GroupSnTID IsNot Nothing Then
          Dim ROGroupSnT = CommonData.Lists.ROGroupSnTList.GetItem(BGPD.GroupSnTID)
          If Not ROGroupSnT.InternationalInd Then
            Dim total = 0
            If BGPD.BreakfastInd Then
              total += ROGroupSnT.BreakfastAmount
            End If
            If BGPD.LunchInd Then
              total += ROGroupSnT.LunchAmount
            End If
            If BGPD.DinnerInd Then
              total += ROGroupSnT.DinnerAmount
            End If
            If BGPD.IncidentalInd Then
              total += ROGroupSnT.IncidentalAmount
            End If
            If total > ROGroupSnT.TotalAmount AndAlso BGPD.ChangedReason.Trim = "" Then
              ErrorString = "Total S&T is greater than the allowed maximum, please specify a reason why"
            End If
          End If
        End If
      End If
      Return ErrorString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSnTTemplate() method.

    End Sub

    Public Shared Function NewSnTTemplate() As SnTTemplate

      Return DataPortal.CreateChild(Of SnTTemplate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSnTTemplate(dr As SafeDataReader) As SnTTemplate

      Dim b As New SnTTemplate()
      b.Fetch(dr)
      Return b

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(GroupSnTIDProperty, .GetInt32(0))
          LoadProperty(BreakfastIndProperty, .GetBoolean(1))
          LoadProperty(LunchIndProperty, .GetBoolean(2))
          LoadProperty(DinnerIndProperty, .GetBoolean(3))
          LoadProperty(IncidentalIndProperty, .GetBoolean(4))
          LoadProperty(ChangedReasonProperty, .GetString(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSnTTemplate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSnTTemplate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then

      '  With cm
      '    .CommandType = CommandType.StoredProcedure

      '    Dim paramGroupPolicyID As SqlParameter = .Parameters.Add("@GroupPolicyID", SqlDbType.Int)
      '    paramGroupPolicyID.Value = GetProperty(GroupPolicyIDProperty)
      '    If Me.IsNew Then
      '      paramGroupPolicyID.Direction = ParameterDirection.Output
      '    End If
      '    .Parameters.AddWithValue("@BreakfastInd", GetProperty(BreakfastIndProperty))
      '    .Parameters.AddWithValue("@LunchInd", GetProperty(LunchIndProperty))
      '    .Parameters.AddWithValue("@DinnerInd", GetProperty(DinnerIndProperty))
      '    .Parameters.AddWithValue("@IncidentalInd", GetProperty(IncidentalIndProperty))
      '    .Parameters.AddWithValue("@ChangeReason", GetProperty(ChangedReasonProperty))

      '    .ExecuteNonQuery()

      '    If Me.IsNew Then
      '      LoadProperty(GroupSnTIDProperty, paramGroupPolicyID.Value)
      '    End If
      '    ' update child objects
      '    ' mChildList.Update()
      '    MarkOld()
      '  End With
      'Else
      'End If

    End Sub

    Friend Sub DeleteSelf()

      '' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delSnTTemplate"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@GroupPolicyID", GetProperty(GroupPolicyIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Private Function CreateTravellersTableParameter() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("TravelRequisitionTravellerID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("TravelRequisitionID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResource", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SnTStartDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SnTEndDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("CityID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("CityCode", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("CrewType", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Discipline", GetType(System.String)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each tr As OBLib.Travel.SnT.SnTTemplateTraveller In Me.SnTTemplateTravellerList
        If tr.IsSelected Then
          Dim row As DataRow = RBTable.NewRow
          row("ObjectGuid") = NothingDBNull(tr.Guid)
          row("TravelRequisitionTravellerID") = NothingDBNull(tr.TravelRequisitionTravellerID)
          row("TravelRequisitionID") = NothingDBNull(tr.TravelRequisitionID)
          row("HumanResourceID") = NothingDBNull(tr.HumanResourceID)
          row("HumanResource") = tr.HumanResource
          row("SnTStartDate") = tr.SnTStartDate.ToString
          row("SnTEndDate") = tr.SnTEndDate.ToString
          row("CityID") = NothingDBNull(tr.CityID)
          row("CityCode") = tr.CityCode
          row("CrewType") = tr.CrewType
          row("Discipline") = tr.Discipline
          RBTable.Rows.Add(row)
        End If
      Next

      Return RBTable

    End Function

    Function Apply() As Singular.Web.Result
      Try
        Dim dt As DataTable = CreateTravellersTableParameter()
        Dim cmdProc As New Singular.CommandProc("cmdProcs.cmdTravelApplySnTTemplate")
        cmdProc.UseTransaction = True
        cmdProc.Parameters.AddWithValue("@GroupSnTID", GetProperty(GroupSnTIDProperty))
        cmdProc.Parameters.AddWithValue("@BreakfastInd", GetProperty(BreakfastIndProperty))
        cmdProc.Parameters.AddWithValue("@LunchInd", GetProperty(LunchIndProperty))
        cmdProc.Parameters.AddWithValue("@DinnerInd", GetProperty(DinnerIndProperty))
        cmdProc.Parameters.AddWithValue("@IncidentalInd", GetProperty(IncidentalIndProperty))
        cmdProc.Parameters.AddWithValue("@ChangedReason", GetProperty(ChangedReasonProperty))
        cmdProc.Parameters.AddWithValue("@Travellers", dt)
        cmdProc.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
        cmdProc.Parameters.AddWithValue("@TravelRequisitionID", GetProperty(TravelRequisitionIDProperty))
        cmdProc = cmdProc.Execute()
        Return New Singular.Web.Result(True)
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try
    End Function

  End Class

End Namespace