﻿' Generated 15 Oct 2014 12:46 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.OB.SnTs.ReadOnly

  <Serializable()> _
  Public Class ROCalculatedSnTResultList
    Inherits SingularReadOnlyListBase(Of ROCalculatedSnTResultList, ROCalculatedSnTResult)

#Region " Business Methods "

    Public Function GetItem(ID As Integer) As ROCalculatedSnTResult

      For Each child As ROCalculatedSnTResult In Me
        If child.ID = ID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItem(SntDay As Date) As ROCalculatedSnTResult

      For Each child As ROCalculatedSnTResult In Me
        If child.ProductionDay = SntDay Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing
      Public Property MinStart As DateTime? = Nothing
      Public Property MaxEnd As DateTime? = Nothing
      Public Property ProductionID As Integer? = Nothing

      Public Sub New(HumanResourceID As Integer?, MinStart As DateTime?, MaxEnd As DateTime?, ProductionID As Integer?)

        Me.HumanResourceID = HumanResourceID
        Me.MinStart = MinStart
        Me.MaxEnd = MaxEnd
        Me.ProductionID = ProductionID

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROCalculatedSnTResultList() As ROCalculatedSnTResultList

      Return New ROCalculatedSnTResultList()

    End Function

    Public Shared Sub BeginGetROCalculatedSnTResultList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROCalculatedSnTResultList)))

      Dim dp As New DataPortal(Of ROCalculatedSnTResultList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROCalculatedSnTResultList(CallBack As EventHandler(Of DataPortalResult(Of ROCalculatedSnTResultList)))

      Dim dp As New DataPortal(Of ROCalculatedSnTResultList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROCalculatedSnTResultList() As ROCalculatedSnTResultList

      Return DataPortal.Fetch(Of ROCalculatedSnTResultList)(New Criteria())

    End Function

    Public Shared Function GetROCalculatedSnTResultList(HumanResourceID As Integer?, MinStart As DateTime?, MaxEnd As DateTime?, ProductionID As Integer?) As ROCalculatedSnTResultList

      Return DataPortal.Fetch(Of ROCalculatedSnTResultList)(New Criteria(HumanResourceID, MinStart, MaxEnd, ProductionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROCalculatedSnTResult.GetROCalculatedSnTResult(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcs.getROCalculatedSnTResultList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.MinStart))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.MaxEnd))
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(OBLib.Security.Settings.CurrentUser.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(OBLib.Security.Settings.CurrentUser.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace