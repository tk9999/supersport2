﻿' Generated 17 Mar 2014 15:09 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.SnTs.ReadOnly

  <Serializable()> _
  Public Class ROGroupSnTList
    Inherits OBReadOnlyListBase(Of ROGroupSnTList, ROGroupSnT)

#Region " Business Methods "

    Public Function GetItem(GroupSnTID As Integer) As ROGroupSnT

      For Each child As ROGroupSnT In Me
        If child.GroupSnTID = GroupSnTID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Group Sn Ts"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROGroupSnTList() As ROGroupSnTList

      Return New ROGroupSnTList()

    End Function

    Public Shared Sub BeginGetROGroupSnTList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROGroupSnTList)))

      Dim dp As New DataPortal(Of ROGroupSnTList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROGroupSnTList(CallBack As EventHandler(Of DataPortalResult(Of ROGroupSnTList)))

      Dim dp As New DataPortal(Of ROGroupSnTList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROGroupSnTList() As ROGroupSnTList

      Return DataPortal.Fetch(Of ROGroupSnTList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROGroupSnT.GetROGroupSnT(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROGroupSnTList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace