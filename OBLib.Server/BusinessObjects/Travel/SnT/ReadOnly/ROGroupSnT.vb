﻿' Generated 17 Mar 2014 15:09 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.SnTs.ReadOnly

  <Serializable()> _
  Public Class ROGroupSnT
    Inherits OBReadOnlyBase(Of ROGroupSnT)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared GroupSnTIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GroupSnTID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property GroupSnTID() As Integer
      Get
        Return GetProperty(GroupSnTIDProperty)
      End Get
    End Property

    Public Shared GroupSnTTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GroupSnTTypeID, "Group Sn T Type", 0)
    ''' <summary>
    ''' Gets the Group Sn T Type value
    ''' </summary>
    <Display(Name:="Group Sn T Type", Description:="foreign key link to group snt types, normal, breakfast, lunch, dinner etc")>
    Public ReadOnly Property GroupSnTTypeID() As Integer
      Get
        Return GetProperty(GroupSnTTypeIDProperty)
      End Get
    End Property

    Public Shared DestinationCountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DestinationCountryID, "Destination Country", Nothing)
    ''' <summary>
    ''' Gets the Destination Country value
    ''' </summary>
    <Display(Name:="Destination Country", Description:="Destination country to which this rate will apply")>
    Public ReadOnly Property DestinationCountryID() As Integer?
      Get
        Return GetProperty(DestinationCountryIDProperty)
      End Get
    End Property

    Public Shared CurrencyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CurrencyID, "Currency", Nothing)
    ''' <summary>
    ''' Gets the Currency value
    ''' </summary>
    <Display(Name:="Currency", Description:="The currency in which the rate will be paid")>
    Public ReadOnly Property CurrencyID() As Integer?
      Get
        Return GetProperty(CurrencyIDProperty)
      End Get
    End Property

    Public Shared InternationalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InternationalInd, "International", False)
    ''' <summary>
    ''' Gets the International value
    ''' </summary>
    <Display(Name:="International", Description:="International or Local Travel")>
    Public ReadOnly Property InternationalInd() As Boolean
      Get
        Return GetProperty(InternationalIndProperty)
      End Get
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:="the date from which this rate applies")>
    Public ReadOnly Property EffectiveDate As DateTime?
      Get
        If Not FieldManager.FieldExists(EffectiveDateProperty) Then
          LoadProperty(EffectiveDateProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(EffectiveDateProperty)
      End Get
    End Property

    Public Shared BreakfastAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.BreakfastAmount, "Breakfast Amount", CDec(0))
    ''' <summary>
    ''' Gets the Breakfast Amount value
    ''' </summary>
    <Display(Name:="Breakfast Amount", Description:="")>
    Public ReadOnly Property BreakfastAmount() As Decimal
      Get
        Return GetProperty(BreakfastAmountProperty)
      End Get
    End Property

    Public Shared LunchAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.LunchAmount, "Lunch Amount", CDec(0))
    ''' <summary>
    ''' Gets the Lunch Amount value
    ''' </summary>
    <Display(Name:="Lunch Amount", Description:="")>
    Public ReadOnly Property LunchAmount() As Decimal
      Get
        Return GetProperty(LunchAmountProperty)
      End Get
    End Property

    Public Shared DinnerAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.DinnerAmount, "Dinner Amount", CDec(0))
    ''' <summary>
    ''' Gets the Dinner Amount value
    ''' </summary>
    <Display(Name:="Dinner Amount", Description:="")>
    Public ReadOnly Property DinnerAmount() As Decimal
      Get
        Return GetProperty(DinnerAmountProperty)
      End Get
    End Property

    Public Shared IncidentalAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.IncidentalAmount, "Incidental Amount", CDec(0))
    ''' <summary>
    ''' Gets the Incidental Amount value
    ''' </summary>
    <Display(Name:="Incidental Amount", Description:="")>
    Public ReadOnly Property IncidentalAmount() As Decimal
      Get
        Return GetProperty(IncidentalAmountProperty)
      End Get
    End Property

    Public Shared TotalAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalAmount, "Total Amount", CDec(0))
    ''' <summary>
    ''' Gets the Total Amount value
    ''' </summary>
    <Display(Name:="Total Amount", Description:="")>
    Public ReadOnly Property TotalAmount() As Decimal
      Get
        Return GetProperty(TotalAmountProperty)
      End Get
    End Property

    Public Shared ConferenceAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ConferenceAmount, "Conference Amount", CDec(0))
    ''' <summary>
    ''' Gets the Conference Amount value
    ''' </summary>
    <Display(Name:="Conference Amount", Description:="")>
    Public ReadOnly Property ConferenceAmount() As Decimal
      Get
        Return GetProperty(ConferenceAmountProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DestinationCountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DestinationCountry, "Destination Country")
    ''' <summary>
    ''' Gets the Destination Country value
    ''' </summary>
    <Display(Name:="Destination Country", Description:="Destination country to which this rate will apply")>
    Public ReadOnly Property DestinationCountry() As String
      Get
        Return GetProperty(DestinationCountryProperty)
      End Get
    End Property

    Public Shared PolicyRuleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PolicyRule, "Policy")
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Policy", Description:="")>
    Public ReadOnly Property PolicyRule() As String
      Get
        Return If(InternationalInd, DestinationCountry & " - " & "International", DestinationCountry & " - " & "Domestic")
      End Get
    End Property

    Public ReadOnly Property InternationalDescription() As String
      Get
        If InternationalInd Then
          Return "International"
        Else
          Return "Domestic"
        End If
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(GroupSnTIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.GroupSnTTypeID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROGroupSnT(dr As SafeDataReader) As ROGroupSnT

      Dim r As New ROGroupSnT()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(GroupSnTIDProperty, .GetInt32(0))
        LoadProperty(GroupSnTTypeIDProperty, .GetInt32(1))
        LoadProperty(DestinationCountryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CurrencyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(InternationalIndProperty, .GetBoolean(4))
        LoadProperty(EffectiveDateProperty, .GetValue(5))
        LoadProperty(BreakfastAmountProperty, .GetDecimal(6))
        LoadProperty(LunchAmountProperty, .GetDecimal(7))
        LoadProperty(DinnerAmountProperty, .GetDecimal(8))
        LoadProperty(IncidentalAmountProperty, .GetDecimal(9))
        LoadProperty(TotalAmountProperty, .GetDecimal(10))
        LoadProperty(ConferenceAmountProperty, .GetDecimal(11))
        LoadProperty(CreatedByProperty, .GetInt32(12))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(13))
        LoadProperty(ModifiedByProperty, .GetInt32(14))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(15))
        LoadProperty(DestinationCountryProperty, .GetString(16))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace