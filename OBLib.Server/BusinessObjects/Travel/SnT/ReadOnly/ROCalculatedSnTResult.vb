﻿' Generated 15 Oct 2014 12:46 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.OB.SnTs.ReadOnly

  <Serializable()> _
  Public Class ROCalculatedSnTResult
    Inherits SingularReadOnlyBase(Of ROCalculatedSnTResult)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ID() As Integer?
      Get
        Return GetProperty(IDProperty)
      End Get
    End Property

    Public Shared ProductionDayProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ProductionDay, "Production Day")
    ''' <summary>
    ''' Gets the Production Day value
    ''' </summary>
    <Display(Name:="Production Day", Description:="")>
    Public ReadOnly Property ProductionDay As DateTime?
      Get
        Return GetProperty(ProductionDayProperty)
      End Get
    End Property

    Public Shared SnTFieldsToUseProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SnTFieldsToUse, "Sn T Fields To Use")
    ''' <summary>
    ''' Gets the Sn T Fields To Use value
    ''' </summary>
    <Display(Name:="Sn T Fields To Use", Description:="")>
    Public ReadOnly Property SnTFieldsToUse() As String
      Get
        Return GetProperty(SnTFieldsToUseProperty)
      End Get
    End Property

    Public Shared InternationalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InternationalInd, "International", False)
    ''' <summary>
    ''' Gets the International value
    ''' </summary>
    <Display(Name:="International", Description:="")>
    Public ReadOnly Property InternationalInd() As Boolean
      Get
        Return GetProperty(InternationalIndProperty)
      End Get
    End Property

    Public Shared DestinationCountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DestinationCountryID, "Destination Country", Nothing)
    ''' <summary>
    ''' Gets the Destination Country value
    ''' </summary>
    <Display(Name:="Destination Country", Description:="")>
    Public ReadOnly Property DestinationCountryID() As Integer?
      Get
        Return GetProperty(DestinationCountryIDProperty)
      End Get
    End Property

    Public Shared GroupSnTIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.GroupSnTID, "Group Sn T", Nothing)
    ''' <summary>
    ''' Gets the Group Sn T value
    ''' </summary>
    <Display(Name:="Group Sn T", Description:="")>
    Public ReadOnly Property GroupSnTID() As Integer?
      Get
        Return GetProperty(GroupSnTIDProperty)
      End Get
    End Property

    Public Shared BreakfastIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.BreakfastInd, "Breakfast", False)
    ''' <summary>
    ''' Gets the Breakfast value
    ''' </summary>
    <Display(Name:="Breakfast", Description:="")>
    Public ReadOnly Property BreakfastInd() As Boolean
      Get
        Return GetProperty(BreakfastIndProperty)
      End Get
    End Property

    Public Shared LunchIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.LunchInd, "Lunch", False)
    ''' <summary>
    ''' Gets the Lunch value
    ''' </summary>
    <Display(Name:="Lunch", Description:="")>
    Public ReadOnly Property LunchInd() As Boolean
      Get
        Return GetProperty(LunchIndProperty)
      End Get
    End Property

    Public Shared DinnerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DinnerInd, "Dinner", False)
    ''' <summary>
    ''' Gets the Dinner value
    ''' </summary>
    <Display(Name:="Dinner", Description:="")>
    Public ReadOnly Property DinnerInd() As Boolean
      Get
        Return GetProperty(DinnerIndProperty)
      End Get
    End Property

    Public Shared IncidentalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IncidentalInd, "Incidental", False)
    ''' <summary>
    ''' Gets the Incidental value
    ''' </summary>
    <Display(Name:="Incidental", Description:="")>
    Public ReadOnly Property IncidentalInd() As Boolean
      Get
        Return GetProperty(IncidentalIndProperty)
      End Get
    End Property

    Public Shared Column1Property As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Column1, "Column 1", False)
    ''' <summary>
    ''' Gets the Column 1 value
    ''' </summary>
    <Display(Name:="Column 1", Description:="")>
    Public ReadOnly Property Column1() As Boolean
      Get
        Return GetProperty(Column1Property)
      End Get
    End Property

    Public Shared BreakfastProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.BreakfastProductionID, "Breakfast Production", Nothing)
    ''' <summary>
    ''' Gets the Breakfast Production value
    ''' </summary>
    <Display(Name:="Breakfast Production", Description:="")>
    Public ReadOnly Property BreakfastProductionID() As Integer?
      Get
        Return GetProperty(BreakfastProductionIDProperty)
      End Get
    End Property

    Public Shared LunchProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LunchProductionID, "Lunch Production", Nothing)
    ''' <summary>
    ''' Gets the Lunch Production value
    ''' </summary>
    <Display(Name:="Lunch Production", Description:="")>
    Public ReadOnly Property LunchProductionID() As Integer?
      Get
        Return GetProperty(LunchProductionIDProperty)
      End Get
    End Property

    Public Shared DinnerProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DinnerProductionID, "Dinner Production", Nothing)
    ''' <summary>
    ''' Gets the Dinner Production value
    ''' </summary>
    <Display(Name:="Dinner Production", Description:="")>
    Public ReadOnly Property DinnerProductionID() As Integer?
      Get
        Return GetProperty(DinnerProductionIDProperty)
      End Get
    End Property

    Public Shared IncidentalProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IncidentalProductionID, "Incidental Production", Nothing)
    ''' <summary>
    ''' Gets the Incidental Production value
    ''' </summary>
    <Display(Name:="Incidental Production", Description:="")>
    Public ReadOnly Property IncidentalProductionID() As Integer?
      Get
        Return GetProperty(IncidentalProductionIDProperty)
      End Get
    End Property

    Public Shared ConferenceProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ConferenceProductionID, "Conference Production", Nothing)
    ''' <summary>
    ''' Gets the Conference Production value
    ''' </summary>
    <Display(Name:="Conference Production", Description:="")>
    Public ReadOnly Property ConferenceProductionID() As Integer?
      Get
        Return GetProperty(ConferenceProductionIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(IDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SnTFieldsToUse

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCalculatedSnTResult(dr As SafeDataReader) As ROCalculatedSnTResult

      Dim r As New ROCalculatedSnTResult()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(IDProperty, .GetInt32(0))
        LoadProperty(ProductionDayProperty, .GetValue(1))
        LoadProperty(SnTFieldsToUseProperty, .GetString(2))
        LoadProperty(InternationalIndProperty, .GetBoolean(3))
        LoadProperty(DestinationCountryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(GroupSnTIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(BreakfastIndProperty, .GetBoolean(6))
        LoadProperty(LunchIndProperty, .GetBoolean(7))
        LoadProperty(DinnerIndProperty, .GetBoolean(8))
        LoadProperty(IncidentalIndProperty, .GetBoolean(9))
        LoadProperty(Column1Property, .GetBoolean(10))
        LoadProperty(BreakfastProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(LunchProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(DinnerProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(IncidentalProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(ConferenceProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace