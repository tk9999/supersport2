﻿' Generated 19 Nov 2014 12:18 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.SnT

  <Serializable()> _
  Public Class SnTDetailList
    Inherits OBBusinessListBase(Of SnTDetailList, SnTDetail)

#Region " Business Methods "

    Public Function GetItem(GroupHumanResourceSnTID As Integer) As SnTDetail

      For Each child As SnTDetail In Me
        If child.GroupHumanResourceSnTID = GroupHumanResourceSnTID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

    Public Sub UpdateOBSnTAmounts(SnTDay As Date, GroupSnTPolicyID As Integer, BreakfastInd As Boolean, LunchInd As Boolean, DinnerInd As Boolean, _
                            IncidentalInd As Boolean, ChangeReason As String, mProductionID As Integer, CountryID As Integer)

      Dim ROGroupSnT As OBLib.Travel.SnTs.ReadOnly.ROGroupSnT = OBLib.CommonData.Lists.ROGroupSnTList.Where(Function(c) Singular.Misc.CompareSafe(c.GroupSnTID, GroupSnTPolicyID)).FirstOrDefault()
      For Each ahsd As SnTDetail In Me
        If ahsd.SnTDay = SnTDay Then
          If ROGroupSnT IsNot Nothing AndAlso (ahsd.BreakfastAdHocBookingID Is Nothing AndAlso
                                               ahsd.LunchAdHocBookingID Is Nothing AndAlso
                                               ahsd.DinnerAdHocBookingID Is Nothing AndAlso
                                               ahsd.IncidentalAdHocBookingID Is Nothing) Then
            Dim ProductionID As Integer = mProductionID
            If ahsd.BreakfastProductionID <> ProductionID OrElse
              ahsd.LunchProductionID <> ProductionID OrElse
              ahsd.DinnerProductionID <> ProductionID OrElse
              ahsd.IncidentalProductionID <> ProductionID Then
              Continue For 'S&T already given on another booking, Skip to next S&T Detail
            Else
              ahsd.GroupSnTID = GroupSnTPolicyID
              ahsd.DestinationCountryID = ROGroupSnT.DestinationCountryID
              If ahsd.DestinationCountryID Is Nothing Then
                ahsd.DestinationCountryID = CountryID
              End If
              ahsd.CurrencyID = ROGroupSnT.CurrencyID
              ahsd.InternationalInd = ROGroupSnT.InternationalInd
              If BreakfastInd = True AndAlso (ahsd.BreakfastAdHocBookingID Is Nothing OrElse ahsd.BreakfastProductionID = ProductionID) Then
                ahsd.HasBreakfast = True
                ahsd.BreakfastAmount = ROGroupSnT.BreakfastAmount
                ahsd.BreakfastProductionID = mProductionID
                ahsd.BreakfastSystemID = OBLib.Security.Settings.CurrentUser.SystemID
                ahsd.BreakfastProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
              End If

              If LunchInd = True AndAlso (ahsd.LunchAdHocBookingID Is Nothing OrElse ahsd.LunchProductionID = ProductionID) Then
                ahsd.HasLunch = True
                ahsd.LunchAmount = ROGroupSnT.LunchAmount
                ahsd.LunchProductionID = mProductionID
                ahsd.LunchSystemID = OBLib.Security.Settings.CurrentUser.SystemID
                ahsd.LunchProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
              End If

              If DinnerInd = True AndAlso (ahsd.DinnerAdHocBookingID Is Nothing OrElse ahsd.DinnerProductionID = ProductionID) Then
                ahsd.HasDinner = True
                ahsd.DinnerAmount = ROGroupSnT.DinnerAmount
                ahsd.DinnerProductionID = mProductionID
                ahsd.DinnerSystemID = OBLib.Security.Settings.CurrentUser.SystemID
                ahsd.DinnerProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
              End If

              If IncidentalInd = True AndAlso (ahsd.IncidentalAdHocBookingID Is Nothing OrElse ahsd.IncidentalProductionID = ProductionID) Then
                ahsd.HasIncidental = True
                ahsd.IncidentalAmount = ROGroupSnT.IncidentalAmount
                ahsd.IncidentalProductionID = mProductionID
                ahsd.IncidentalSystemID = OBLib.Security.Settings.CurrentUser.SystemID
                ahsd.IncidentalProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
              End If

              If ChangeReason.Trim <> "" Then

                If ahsd.ChangedReason.Trim = "" Then
                  ahsd.ChangedReason = ChangeReason
                Else
                  ahsd.ChangedReason += ", " + ChangeReason
                End If

              End If
            End If
          End If
        End If
      Next
    End Sub

    Public Sub UpdateAdHocSnTAmounts(SnTDay As Date, GroupSnTPolicyID As Integer, BreakfastInd As Boolean, LunchInd As Boolean, DinnerInd As Boolean, _
                                IncidentalInd As Boolean, ChangeReason As String, mAdHocBookingID As Integer, CountryID As Integer)

      Dim ROGroupSnT As OBLib.Travel.SnTs.ReadOnly.ROGroupSnT = OBLib.CommonData.Lists.ROGroupSnTList.Where(Function(c) Singular.Misc.CompareSafe(c.GroupSnTID, GroupSnTPolicyID)).FirstOrDefault()
      For Each ahsd As SnTDetail In Me
        If ahsd.SnTDay = SnTDay Then
          If ROGroupSnT IsNot Nothing AndAlso (ahsd.BreakfastProductionID Is Nothing AndAlso
                                               ahsd.LunchProductionID Is Nothing AndAlso
                                               ahsd.DinnerProductionID Is Nothing AndAlso
                                               ahsd.IncidentalProductionID Is Nothing) Then
            Dim AdHocBookingID As Integer = mAdHocBookingID
            If ahsd.BreakfastAdHocBookingID <> AdHocBookingID OrElse
              ahsd.LunchAdHocBookingID <> AdHocBookingID OrElse
              ahsd.DinnerAdHocBookingID <> AdHocBookingID OrElse
              ahsd.IncidentalAdHocBookingID <> AdHocBookingID Then
              Continue For 'S&T already given on another booking, Skip to next S&T Detail
            Else
              ahsd.GroupSnTID = GroupSnTPolicyID
              ahsd.DestinationCountryID = ROGroupSnT.DestinationCountryID
              If ahsd.DestinationCountryID Is Nothing Then
                ahsd.DestinationCountryID = CountryID
              End If
              ahsd.CurrencyID = ROGroupSnT.CurrencyID
              ahsd.InternationalInd = ROGroupSnT.InternationalInd
              If BreakfastInd = True AndAlso (ahsd.BreakfastAdHocBookingID Is Nothing OrElse ahsd.BreakfastAdHocBookingID = AdHocBookingID) Then
                ahsd.HasBreakfast = True
                ahsd.BreakfastAmount = ROGroupSnT.BreakfastAmount
                ahsd.BreakfastAdHocBookingID = mAdHocBookingID
                ahsd.BreakfastSystemID = OBLib.Security.Settings.CurrentUser.SystemID
                ahsd.BreakfastProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
              End If

              If LunchInd = True AndAlso (ahsd.LunchAdHocBookingID Is Nothing OrElse ahsd.LunchAdHocBookingID = AdHocBookingID) Then
                ahsd.HasLunch = True
                ahsd.LunchAmount = ROGroupSnT.LunchAmount
                ahsd.LunchAdHocBookingID = mAdHocBookingID
                ahsd.LunchSystemID = OBLib.Security.Settings.CurrentUser.SystemID
                ahsd.LunchProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
              End If

              If DinnerInd = True AndAlso (ahsd.DinnerAdHocBookingID Is Nothing OrElse ahsd.DinnerAdHocBookingID = AdHocBookingID) Then
                ahsd.HasDinner = True
                ahsd.DinnerAmount = ROGroupSnT.DinnerAmount
                ahsd.DinnerAdHocBookingID = mAdHocBookingID
                ahsd.DinnerSystemID = OBLib.Security.Settings.CurrentUser.SystemID
                ahsd.DinnerProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
              End If

              If IncidentalInd = True AndAlso (ahsd.IncidentalAdHocBookingID Is Nothing OrElse ahsd.IncidentalAdHocBookingID = AdHocBookingID) Then
                ahsd.HasIncidental = True
                ahsd.IncidentalAmount = ROGroupSnT.IncidentalAmount
                ahsd.IncidentalAdHocBookingID = mAdHocBookingID
                ahsd.IncidentalSystemID = OBLib.Security.Settings.CurrentUser.SystemID
                ahsd.IncidentalProductionAreaID = OBLib.Security.Settings.CurrentUser.ProductionAreaID
              End If

              If ChangeReason.Trim <> "" Then

                If ahsd.ChangedReason.Trim = "" Then
                  ahsd.ChangedReason = ChangeReason
                Else
                  ahsd.ChangedReason += ", " + ChangeReason
                End If

              End If
            End If
          End If
        End If
      Next
    End Sub

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer? = Nothing
      Public Property TravelRequisitionID As Integer? = Nothing

      Public Sub New()

      End Sub

      Public Sub New(HumanResourceID As Integer?, TravelRequisitionID As Integer?)

        Me.HumanResourceID = HumanResourceID
        Me.TravelRequisitionID = TravelRequisitionID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewAdHocSnTDetailList() As SnTDetailList

      Return New SnTDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAdHocSnTDetailList() As SnTDetailList

      Return DataPortal.Fetch(Of SnTDetailList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SnTDetail.GetSnTDetail(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAdHocSnTDetailList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SnTDetail In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SnTDetail In Me
          If Child.IsNew Then
            'Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace