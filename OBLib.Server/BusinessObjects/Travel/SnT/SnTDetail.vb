﻿' Generated 19 Nov 2014 12:18 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.SnT

  <Serializable()> _
  Public Class SnTDetail
    Inherits OBBusinessBase(Of SnTDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SnTDetailBO.SnTDetailBOToString(self)")

    Public Shared GroupHumanResourceSnTIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GroupHumanResourceSnTID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property GroupHumanResourceSnTID() As Integer
      Get
        Return GetProperty(GroupHumanResourceSnTIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(GroupHumanResourceSnTIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    '''Display(AutoGenerateField:=False)
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared GroupSnTIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.GroupSnTID, Nothing)
    ''' <summary>
    ''' Gets and sets the Group SnT value
    ''' </summary>
    <Display(Name:="Policy", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(Travel.SnTs.ReadOnly.ROGroupSnTList)),
    SetExpression("SnTDetailBO.GroupSnTIDSet(self)"),
    Required(ErrorMessage:="Policy is required")>
    Public Property GroupSnTID() As Integer?
      Get
        Return GetProperty(GroupSnTIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(GroupSnTIDProperty, Value)
      End Set
    End Property
    '    Required(ErrorMessage:="Policy required"),

    Public Shared InternationalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InternationalInd, "International", False)
    ''' <summary>
    ''' Gets and sets the International value
    ''' </summary>
    <Display(Name:="International", Description:=""),
    Required(ErrorMessage:="International required")>
    Public Property InternationalInd() As Boolean
      Get
        Return GetProperty(InternationalIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(InternationalIndProperty, Value)
      End Set
    End Property

    Public Shared DestinationCountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DestinationCountryID, "Destination Country", Nothing)
    ''' <summary>
    ''' Gets and sets the Destination Country value
    '''Required(ErrorMessage:="Destination Country required")
    ''' </summary>
    <Display(Name:="Destination Country", Description:="")>
    Public Property DestinationCountryID() As Integer?
      Get
        Return GetProperty(DestinationCountryIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DestinationCountryIDProperty, Value)
      End Set
    End Property
    'Required(ErrorMessage:="Destination Country required")

    Public Shared CurrencyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CurrencyID, "Currency", Nothing)
    ''' <summary>
    ''' Gets and sets the Currency value
    ''' </summary>
    <Display(Name:="Currency", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROCurrencyList), ValueMember:="CurrencyID", DisplayMember:="Currency")>
    Public Property CurrencyID() As Integer?
      Get
        Return GetProperty(CurrencyIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CurrencyIDProperty, Value)
      End Set
    End Property
    'Required(ErrorMessage:="Currency required"), 

    Public Shared SnTDayProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SnTDay, "Day", Date.Today)
    ''' <summary>
    ''' Gets and sets the SnT Day value
    ''' </summary>
    <Display(Name:="S&T Day", Description:="")>
    Public Property SnTDay As DateTime?
      Get
        Return GetProperty(SnTDayProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SnTDayProperty, Value)
      End Set
    End Property

    Public Shared BreakfastProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.BreakfastProductionID, "Breakfast Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Breakfast Production value
    ''' </summary>
    <Display(Name:="Breakfast Production", Description:="")>
    Public Property BreakfastProductionID() As Integer?
      Get
        Return GetProperty(BreakfastProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(BreakfastProductionIDProperty, Value)
      End Set
    End Property

    Public Shared BreakfastAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.BreakfastAmount, "Breakfast Amount", 0)
    ''' <summary>
    ''' Gets and sets the Breakfast Amount value
    ''' </summary>
    <Display(Name:="Breakfast Amount", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.NotSet, FormatStringPropertyName:="SnTDetailBO.CurrencyFormat($data)")>
    Public Property BreakfastAmount() As Decimal
      Get
        Return GetProperty(BreakfastAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(BreakfastAmountProperty, Value)
      End Set
    End Property

    Public Shared LunchProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LunchProductionID, "Lunch Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Lunch Production value
    ''' </summary>
    <Display(Name:="Lunch Production", Description:="")>
    Public Property LunchProductionID() As Integer?
      Get
        Return GetProperty(LunchProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LunchProductionIDProperty, Value)
      End Set
    End Property

    Public Shared LunchAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.LunchAmount, "Lunch Amount", 0)
    ''' <summary>
    ''' Gets and sets the Lunch Amount value
    ''' </summary>
    <Display(Name:="Lunch Amount", Description:="")>
    Public Property LunchAmount() As Decimal
      Get
        Return GetProperty(LunchAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(LunchAmountProperty, Value)
      End Set
    End Property

    Public Shared DinnerProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DinnerProductionID, "Dinner Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Dinner Production value
    ''' </summary>
    <Display(Name:="Dinner Production", Description:="")>
    Public Property DinnerProductionID() As Integer?
      Get
        Return GetProperty(DinnerProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DinnerProductionIDProperty, Value)
      End Set
    End Property

    Public Shared DinnerAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.DinnerAmount, "Dinner Amount", 0)
    ''' <summary>
    ''' Gets and sets the Dinner Amount value
    ''' </summary>
    <Display(Name:="Dinner Amount", Description:="")>
    Public Property DinnerAmount() As Decimal
      Get
        Return GetProperty(DinnerAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(DinnerAmountProperty, Value)
      End Set
    End Property

    Public Shared IncidentalProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IncidentalProductionID, "Incidental Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Incidental Production value
    ''' </summary>
    <Display(Name:="Incidental Production", Description:="")>
    Public Property IncidentalProductionID() As Integer?
      Get
        Return GetProperty(IncidentalProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(IncidentalProductionIDProperty, Value)
      End Set
    End Property

    Public Shared IncidentalAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.IncidentalAmount, "Incidental Amount", 0)
    ''' <summary>
    ''' Gets and sets the Incidental Amount value
    ''' </summary>
    <Display(Name:="Incidental Amount", Description:="")>
    Public Property IncidentalAmount() As Decimal
      Get
        Return GetProperty(IncidentalAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(IncidentalAmountProperty, Value)
      End Set
    End Property

    Public Shared ConferenceProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ConferenceProductionID, "Conference Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Conference Production value
    ''' </summary>
    <Display(Name:="Conference Production", Description:="")>
    Public Property ConferenceProductionID() As Integer?
      Get
        Return GetProperty(ConferenceProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ConferenceProductionIDProperty, Value)
      End Set
    End Property

    Public Shared ConferenceAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ConferenceAmount, "Conference Amount", 0)
    ''' <summary>
    ''' Gets and sets the Conference Amount value
    ''' </summary>
    <Display(Name:="Conference Amount", Description:="")>
    Public Property ConferenceAmount() As Decimal
      Get
        Return GetProperty(ConferenceAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(ConferenceAmountProperty, Value)
      End Set
    End Property

    Public Shared AccommodationIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AccommodationInd, "Accommodation", False)
    ''' <summary>
    ''' Gets and sets the Accommodation value
    ''' </summary>
    <Display(Name:="Accommodation", Description:="")>
    Public Property AccommodationInd() As Boolean
      Get
        Return GetProperty(AccommodationIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(AccommodationIndProperty, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ChangedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangedReason, "Changed Reason", "")
    ''' <summary>
    ''' Gets and sets the Changed Reason value
    ''' </summary>
    <Display(Name:="Changed Reason", Description:="")>
    Public Property ChangedReason() As String
      Get
        Return GetProperty(ChangedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChangedReasonProperty, Value)
      End Set
    End Property

    Public Shared BreakfastSystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.BreakfastSystemID, "Breakfast System", Nothing)
    ''' <summary>
    ''' Gets and sets the Breakfast System value
    ''' </summary>
    <Display(Name:="Breakfast System", Description:="")>
    Public Property BreakfastSystemID() As Integer?
      Get
        Return GetProperty(BreakfastSystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(BreakfastSystemIDProperty, Value)
      End Set
    End Property

    Public Shared BreakfastProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.BreakfastProductionAreaID, "Breakfast Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Breakfast Production Area value
    ''' </summary>
    <Display(Name:="Breakfast Production Area", Description:="")>
    Public Property BreakfastProductionAreaID() As Integer?
      Get
        Return GetProperty(BreakfastProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(BreakfastProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared BreakfastAdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.BreakfastAdHocBookingID, "Breakfast Ad Hoc Booking", Nothing)
    ''' <summary>
    ''' Gets and sets the Breakfast Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Breakfast Ad Hoc Booking", Description:="")>
    Public Property BreakfastAdHocBookingID() As Integer?
      Get
        Return GetProperty(BreakfastAdHocBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(BreakfastAdHocBookingIDProperty, Value)
      End Set
    End Property

    Public Shared LunchSystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LunchSystemID, "Lunch System", Nothing)
    ''' <summary>
    ''' Gets and sets the Lunch System value
    ''' </summary>
    <Display(Name:="Lunch System", Description:="")>
    Public Property LunchSystemID() As Integer?
      Get
        Return GetProperty(LunchSystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LunchSystemIDProperty, Value)
      End Set
    End Property

    Public Shared LunchProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LunchProductionAreaID, "Lunch Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Lunch Production Area value
    ''' </summary>
    <Display(Name:="Lunch Production Area", Description:="")>
    Public Property LunchProductionAreaID() As Integer?
      Get
        Return GetProperty(LunchProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LunchProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared LunchAdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LunchAdHocBookingID, "Lunch Ad Hoc Booking", Nothing)
    ''' <summary>
    ''' Gets and sets the Lunch Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Lunch Ad Hoc Booking", Description:="")>
    Public Property LunchAdHocBookingID() As Integer?
      Get
        Return GetProperty(LunchAdHocBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LunchAdHocBookingIDProperty, Value)
      End Set
    End Property

    Public Shared DinnerSystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DinnerSystemID, "Dinner System", Nothing)
    ''' <summary>
    ''' Gets and sets the Dinner System value
    ''' </summary>
    <Display(Name:="Dinner System", Description:="")>
    Public Property DinnerSystemID() As Integer?
      Get
        Return GetProperty(DinnerSystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DinnerSystemIDProperty, Value)
      End Set
    End Property

    Public Shared DinnerProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DinnerProductionAreaID, "Dinner Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Dinner Production Area value
    ''' </summary>
    <Display(Name:="Dinner Production Area", Description:="")>
    Public Property DinnerProductionAreaID() As Integer?
      Get
        Return GetProperty(DinnerProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DinnerProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared DinnerAdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DinnerAdHocBookingID, "Dinner Ad Hoc Booking", Nothing)
    ''' <summary>
    ''' Gets and sets the Dinner Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Dinner Ad Hoc Booking", Description:="")>
    Public Property DinnerAdHocBookingID() As Integer?
      Get
        Return GetProperty(DinnerAdHocBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DinnerAdHocBookingIDProperty, Value)
      End Set
    End Property

    Public Shared IncidentalSystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IncidentalSystemID, "Incidental System", Nothing)
    ''' <summary>
    ''' Gets and sets the Incidental System value
    ''' </summary>
    <Display(Name:="Incidental System", Description:="")>
    Public Property IncidentalSystemID() As Integer?
      Get
        Return GetProperty(IncidentalSystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(IncidentalSystemIDProperty, Value)
      End Set
    End Property

    Public Shared IncidentalProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IncidentalProductionAreaID, "Incidental Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Incidental Production Area value
    ''' </summary>
    <Display(Name:="Incidental Production Area", Description:="")>
    Public Property IncidentalProductionAreaID() As Integer?
      Get
        Return GetProperty(IncidentalProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(IncidentalProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared IncidentalAdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IncidentalAdHocBookingID, "Incidental Ad Hoc Booking", Nothing)
    ''' <summary>
    ''' Gets and sets the Incidental Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Incidental Ad Hoc Booking", Description:="")>
    Public Property IncidentalAdHocBookingID() As Integer?
      Get
        Return GetProperty(IncidentalAdHocBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(IncidentalAdHocBookingIDProperty, Value)
      End Set
    End Property

    'Public Shared ConferenceSystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ConferenceSystemID, "Conference System", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Conference System value
    ' ''' </summary>
    '<Display(Name:="Conference System", Description:="")>
    'Public Property ConferenceSystemID() As Integer?
    '  Get
    '    Return GetProperty(ConferenceSystemIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(ConferenceSystemIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared ConferenceProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ConferenceProductionAreaID, "Conference Production Area", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Conference Production Area value
    ' ''' </summary>
    '<Display(Name:="Conference Production Area", Description:="")>
    'Public Property ConferenceProductionAreaID() As Integer?
    '  Get
    '    Return GetProperty(ConferenceProductionAreaIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(ConferenceProductionAreaIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared ConferenceAdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ConferenceAdHocBookingID, "Conference Ad Hoc Booking", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Conference Ad Hoc Booking value
    ' ''' </summary>
    '<Display(Name:="Conference Ad Hoc Booking", Description:="")>
    'Public Property ConferenceAdHocBookingID() As Integer?
    '  Get
    '    Return GetProperty(ConferenceAdHocBookingIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(ConferenceAdHocBookingIDProperty, Value)
    '  End Set
    'End Property

    Public Shared IsBreakfastCurrentEventProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsBreakfastCurrentEvent, "Breakfast icator", False)
    ''' <summary>
    ''' Gets and sets the Breakfast value
    ''' </summary>
    <Display(Name:="Breakfast icator", Description:="")>
    Public Property IsBreakfastCurrentEvent() As Boolean
      Get
        Return GetProperty(IsBreakfastCurrentEventProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsBreakfastCurrentEventProperty, Value)
      End Set
    End Property

    Public Shared IsLunchCurrentEventProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsLunchCurrentEvent, "Lunch icator", False)
    ''' <summary>
    ''' Gets and sets the Lunch value
    ''' </summary>
    <Display(Name:="Lunch icator", Description:="")>
    Public Property IsLunchCurrentEvent() As Boolean
      Get
        Return GetProperty(IsLunchCurrentEventProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsLunchCurrentEventProperty, Value)
      End Set
    End Property

    Public Shared IsDinnerCurrentEventProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsDinnerCurrentEvent, "Dinner icator", False)
    ''' <summary>
    ''' Gets and sets the Dinner value
    ''' </summary>
    <Display(Name:="Dinner icator", Description:="")>
    Public Property IsDinnerCurrentEvent() As Boolean
      Get
        Return GetProperty(IsDinnerCurrentEventProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsDinnerCurrentEventProperty, Value)
      End Set
    End Property

    Public Shared IsIncidentalCurrentEventProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsIncidentalCurrentEvent, "Incidental icator", False)
    ''' <summary>
    ''' Gets and sets the Incidental value
    ''' </summary>
    <Display(Name:="Incidental icator", Description:="")>
    Public Property IsIncidentalCurrentEvent() As Boolean
      Get
        Return GetProperty(IsIncidentalCurrentEventProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsIncidentalCurrentEventProperty, Value)
      End Set
    End Property

    Public Shared HasBreakfastProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasBreakfast, "Breakfast icator", False)
    ''' <summary>
    ''' Gets and sets the Breakfast value
    ''' </summary>
    <Display(Name:="Breakfast icator", Description:=""),
    SetExpression("SnTDetailBO.HasBreakfastSet(self)")>
    Public Property HasBreakfast() As Boolean
      Get
        Return GetProperty(HasBreakfastProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasBreakfastProperty, Value)
      End Set
    End Property

    Public Shared HasLunchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasLunch, "Lunch icator", False)
    ''' <summary>
    ''' Gets and sets the Lunch value
    ''' </summary>
    <Display(Name:="Lunch icator", Description:=""),
    SetExpression("SnTDetailBO.HasLunchSet(self)")>
    Public Property HasLunch() As Boolean
      Get
        Return GetProperty(HasLunchProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasLunchProperty, Value)
      End Set
    End Property

    Public Shared HasDinnerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasDinner, "Dinner icator", False)
    ''' <summary>
    ''' Gets and sets the Dinner value
    ''' </summary>
    <Display(Name:="Dinner icator", Description:=""),
    SetExpression("SnTDetailBO.HasDinnerSet(self)")>
    Public Property HasDinner() As Boolean
      Get
        Return GetProperty(HasDinnerProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasDinnerProperty, Value)
      End Set
    End Property

    Public Shared HasIncidentalProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasIncidental, "Incidental icator", False)
    ''' <summary>
    ''' Gets and sets the Incidental value
    ''' </summary>
    <Display(Name:="Incidental icator", Description:=""),
    SetExpression("SnTDetailBO.HasIncidentalSet(self)")>
    Public Property HasIncidental() As Boolean
      Get
        Return GetProperty(HasIncidentalProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasIncidentalProperty, Value)
      End Set
    End Property

    Public Shared TotalAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalAmount, "Incidental Amount", 0)
    ''' <summary>
    ''' Gets and sets the Incidental Amount value
    ''' </summary>
    <Display(Name:="Total Amount", Description:=""), Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property TotalAmount() As Decimal
      Get
        Return BreakfastAmount + LunchAmount + DinnerAmount + IncidentalAmount 'GetProperty(TotalAmountProperty)
      End Get
    End Property

    'Public Shared CalculatedPolicyProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.CalculatedPolicy, Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Group SnT value
    ' ''' </summary>
    '<Display(Name:="Policy", Description:=""),
    'Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Travel.SnTs.ReadOnly.ROGroupSnTList))>
    'Public Property CalculatedPolicy() As Integer?
    '  Get
    '    If (Singular.Misc.CompareSafe(Me.BreakfastAdHocBookingID, Nothing) AndAlso _
    '      Singular.Misc.CompareSafe(Me.LunchAdHocBookingID, Nothing) AndAlso _
    '      Singular.Misc.CompareSafe(Me.DinnerAdHocBookingID, Nothing) AndAlso _
    '      Singular.Misc.CompareSafe(Me.IncidentalAdHocBookingID, Nothing)) AndAlso Not Me.IsDirty Then
    '      Dim SnTHR As OBLib.HR.ReadOnly.ROHumanResource = OBLib.CommonData.Lists.ROHumanResourceList.Where(Function(c) Singular.Misc.CompareSafe(c.HumanResourceID, HumanResourceID))(0)
    '      If SnTHR IsNot Nothing Then
    '        Dim HRCity As OBLib.Maintenance.Locations.ReadOnly.ROCity = OBLib.CommonData.Lists.ROCityList.Where(Function(c) Singular.Misc.CompareSafe(c.CityID, SnTHR.CityID))(0)
    '        Dim SADomesticNewSnTPolicy As OBLib.Travel.SnTs.ReadOnly.ROGroupSnT = OBLib.CommonData.Lists.ROGroupSnTList.Where(Function(c) Singular.Misc.CompareSafe(c.GroupSnTID, 17))(0)
    '        Dim SnTPolicies As List(Of OBLib.Travel.SnTs.ReadOnly.ROGroupSnT) = New List(Of OBLib.Travel.SnTs.ReadOnly.ROGroupSnT)
    '        If HRCity.CountryID <> Me.DestinationCountryID Then
    '          'HR is foreign to this country
    '          For Each item In OBLib.CommonData.Lists.ROGroupSnTList
    '            If Singular.Misc.CompareSafe(item.DestinationCountryID, Me.DestinationCountryID) AndAlso item.InternationalInd AndAlso Not Singular.Misc.CompareSafe(item.DestinationCountryID, 1) Then
    '              SnTPolicies.Add(item)
    '              Exit For
    '            End If
    '          Next
    '        Else
    '          'If not International must be domestic
    '          For Each item In OBLib.CommonData.Lists.ROGroupSnTList
    '            If Singular.Misc.CompareSafe(item.DestinationCountryID, Me.DestinationCountryID) AndAlso Not item.InternationalInd AndAlso Not Singular.Misc.CompareSafe(item.DestinationCountryID, 1) Then
    '              SnTPolicies.Add(item)
    '            End If
    '          Next
    '        End If
    '        If SnTPolicies.Count = 0 Then
    '          ' Destination Country is NULL, either SA int or domestic
    '          If Not Singular.Misc.CompareSafe(Me.DestinationCountryID, 1) AndAlso Singular.Misc.CompareSafe(HRCity.CountryID, Me.DestinationCountryID) Then
    '            ' In South Africa
    '            SetProperty(GroupSnTIDProperty, 12)
    '          ElseIf Singular.Misc.CompareSafe(Me.DestinationCountryID, 1) AndAlso Singular.Misc.CompareSafe(HRCity.CountryID, 1) Then
    '            'HR is South African and event is in SA
    '            If Me.SnTDay >= SADomesticNewSnTPolicy.EffectiveDate Then
    '              SetProperty(GroupSnTIDProperty, SADomesticNewSnTPolicy.GroupSnTID)
    '            Else
    '              SetProperty(GroupSnTIDProperty, 13)
    '            End If
    '          ElseIf Not Singular.Misc.CompareSafe(Me.DestinationCountryID, 1) AndAlso Singular.Misc.CompareSafe(HRCity.CountryID, 1) Then
    '            'HR is South African but event International
    '            SetProperty(GroupSnTIDProperty, 11)
    '          Else
    '            SetProperty(GroupSnTIDProperty, 5)
    '          End If
    '        Else
    '          SetProperty(GroupSnTIDProperty, SnTPolicies(0).GroupSnTID)
    '        End If
    '      End If
    '      Me.MarkClean()
    '      Return GetProperty(CalculatedPolicyProperty)
    '    Else
    '      Return Nothing
    '    End If
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(CalculatedPolicyProperty, Value)
    '  End Set
    'End Property

    Public Shared CanEditRecordProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanEditRecord, "Can Edit", False)
    ''' <summary>
    ''' Gets and sets the International value
    ''' </summary>
    <Display(Name:="Can Edit")>
    Public Property CanEditRecord() As Boolean
      Get
        Return GetProperty(CanEditRecordProperty)
      End Get
      Set(value As Boolean)
        SetProperty(CanEditRecordProperty, value)
      End Set
    End Property

    Public Shared BreakfastPopoverProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BreakfastPopover, "BreakfastPopover", "")
    ''' <summary>
    ''' Gets and sets the Other Booking value
    ''' </summary>
    <Display(Name:="BreakfastPopover", Description:="")>
    Public ReadOnly Property BreakfastPopover() As String
      Get
        Return GetProperty(BreakfastPopoverProperty)
      End Get
    End Property

    Public Shared LunchPopoverProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LunchPopover, "LunchPopover", "")
    ''' <summary>
    ''' Gets and sets the Other Booking value
    ''' </summary>
    <Display(Name:="LunchPopover", Description:="")>
    Public ReadOnly Property LunchPopover() As String
      Get
        Return GetProperty(LunchPopoverProperty)
      End Get
    End Property

    Public Shared DinnerPopoverProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DinnerPopover, "DinnerPopover", "")
    ''' <summary>
    ''' Gets and sets the Other Booking value
    ''' </summary>
    <Display(Name:="DinnerPopover", Description:="")>
    Public ReadOnly Property DinnerPopover() As String
      Get
        Return GetProperty(DinnerPopoverProperty)
      End Get
    End Property

    Public Shared IncidentalPopoverProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IncidentalPopover, "IncidentalPopover", "")
    ''' <summary>
    ''' Gets and sets the Other Booking value
    ''' </summary>
    <Display(Name:="IncidentalPopover", Description:="")>
    Public ReadOnly Property IncidentalPopover() As String
      Get
        Return GetProperty(IncidentalPopoverProperty)
      End Get
    End Property

    Public Shared GroupSnTIDCalculatedProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.GroupSnTIDCalculated, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Suggested Policy", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(Travel.SnTs.ReadOnly.ROGroupSnTList)),
    SetExpression("SnTDetailBO.GroupSnTIDCalculatedSet(self)")>
    Public Property GroupSnTIDCalculated() As Integer?
      Get
        Return GetProperty(GroupSnTIDCalculatedProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(GroupSnTIDCalculatedProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged 'TravelRequisitionTraveller

      Return CType(CType(Me.Parent, SnTDetailList).Parent, OBLib.Travel.Travellers.ReadOnly.ROTravellerPaged)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(GroupHumanResourceSnTIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Comments.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Ad Hoc SnT Detail")
        Else
          Return String.Format("Blank {0}", "Ad Hoc SnT Detail")
        End If
      Else
        Return Me.Comments
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(ChangedReasonProperty)
        .AddTriggerProperties(HasBreakfastProperty, HasLunchProperty, HasDinnerProperty, HasIncidentalProperty)
        .JavascriptRuleFunctionName = "SnTDetailBO.TotalAmountValid"
        .ServerRuleFunction = AddressOf TotalAmountValid
      End With

      'With AddWebRule(DestinationCountryIDProperty)
      '  .JavascriptRuleFunctionName = "SnTDetailBO.CheckDestinationCountryBooking"
      '  .ServerRuleFunction = AddressOf CheckDestinationCountryBooking
      'End With

    End Sub

    Public Shared Function TotalAmountValid(SnT As SnTDetail) As String
      Dim ErrorString = ""
      If SnT.GroupSnTID IsNot Nothing Then
        Dim groupSnT As OBLib.Travel.SnTs.ReadOnly.ROGroupSnT = CommonData.Lists.ROGroupSnTList.GetItem(SnT.GroupSnTID)
        If groupSnT IsNot Nothing AndAlso groupSnT.InternationalInd = False Then
          Dim Total As Decimal = SnT.BreakfastAmount + SnT.LunchAmount + SnT.DinnerAmount + SnT.IncidentalAmount
          If Total > groupSnT.TotalAmount AndAlso SnT.ChangedReason.Trim = "" Then
            ErrorString = "Total S&T is greater than the allowed maximum, please specify a reason why"
          End If
        End If
      End If
      Return ErrorString
    End Function

    'Public Shared Function CheckDestinationCountryBooking(SnT As SnTDetail) As String
    '  Dim ErrorString = ""

    '  Return ErrorString
    'End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAdHocSnTDetail() method.

    End Sub

    Public Shared Function NewAdHocSnTDetail() As SnTDetail

      Return DataPortal.CreateChild(Of SnTDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSnTDetail(dr As SafeDataReader) As SnTDetail

      Dim a As New SnTDetail()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(GroupHumanResourceSnTIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(GroupSnTIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(InternationalIndProperty, .GetBoolean(3))
          LoadProperty(DestinationCountryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(CurrencyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(SnTDayProperty, .GetValue(6))
          LoadProperty(BreakfastProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(BreakfastAmountProperty, .GetDecimal(8))
          LoadProperty(LunchProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(LunchAmountProperty, .GetDecimal(10))
          LoadProperty(DinnerProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(DinnerAmountProperty, .GetDecimal(12))
          LoadProperty(IncidentalProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(IncidentalAmountProperty, .GetDecimal(14))
          LoadProperty(ConferenceProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(ConferenceAmountProperty, .GetDecimal(16))
          LoadProperty(AccommodationIndProperty, .GetBoolean(17))
          LoadProperty(CommentsProperty, .GetString(18))
          LoadProperty(CreatedByProperty, .GetInt32(19))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(20))
          LoadProperty(ModifiedByProperty, .GetInt32(21))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(22))
          LoadProperty(ChangedReasonProperty, .GetString(23))
          LoadProperty(BreakfastSystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
          LoadProperty(BreakfastProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(25)))
          LoadProperty(BreakfastAdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(26)))
          LoadProperty(LunchSystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(27)))
          LoadProperty(LunchProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(28)))
          LoadProperty(LunchAdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(29)))
          LoadProperty(DinnerSystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(30)))
          LoadProperty(DinnerProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(31)))
          LoadProperty(DinnerAdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(32)))
          LoadProperty(IncidentalSystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(33)))
          LoadProperty(IncidentalProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(34)))
          LoadProperty(IncidentalAdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(35)))
          LoadProperty(IsBreakfastCurrentEventProperty, .GetBoolean(36))
          LoadProperty(IsLunchCurrentEventProperty, .GetBoolean(37))
          LoadProperty(IsDinnerCurrentEventProperty, .GetBoolean(38))
          LoadProperty(IsIncidentalCurrentEventProperty, .GetBoolean(39))
          'LoadProperty(ConferenceSystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(36)))
          'LoadProperty(ConferenceProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(37)))
          'LoadProperty(ConferenceAdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(38)))
          LoadProperty(HasBreakfastProperty, .GetBoolean(40))
          LoadProperty(HasLunchProperty, .GetBoolean(41))
          LoadProperty(HasDinnerProperty, .GetBoolean(42))
          LoadProperty(HasIncidentalProperty, .GetBoolean(43))
          LoadProperty(CanEditRecordProperty, .GetBoolean(44))
          LoadProperty(BreakfastPopoverProperty, .GetString(45))
          LoadProperty(LunchPopoverProperty, .GetString(46))
          LoadProperty(DinnerPopoverProperty, .GetString(47))
          LoadProperty(IncidentalPopoverProperty, .GetString(48))
          'LoadProperty(AdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(43)))
          LoadProperty(GroupSnTIDCalculatedProperty, Singular.Misc.ZeroNothing(.GetInt32(49)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAdHocSnTDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramGroupHumanResourceSnTID As SqlParameter = .Parameters.Add("@GroupHumanResourceSnTID", SqlDbType.Int)
          paramGroupHumanResourceSnTID.Value = GetProperty(GroupHumanResourceSnTIDProperty)
          If Me.IsNew Then
            paramGroupHumanResourceSnTID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@GroupSnTID", GetProperty(GroupSnTIDProperty))
          .Parameters.AddWithValue("@InternationalInd", GetProperty(InternationalIndProperty))
          .Parameters.AddWithValue("@CountryID", GetProperty(DestinationCountryIDProperty))
          .Parameters.AddWithValue("@CurrencyID", GetProperty(CurrencyIDProperty))
          .Parameters.AddWithValue("@SnTDay", (New SmartDate(GetProperty(SnTDayProperty))).DBValue)
          .Parameters.AddWithValue("@BreakfastAmount", GetProperty(BreakfastAmountProperty))
          .Parameters.AddWithValue("@LunchAmount", GetProperty(LunchAmountProperty))
          .Parameters.AddWithValue("@DinnerAmount", GetProperty(DinnerAmountProperty))
          .Parameters.AddWithValue("@IncidentalAmount", GetProperty(IncidentalAmountProperty))
          .Parameters.AddWithValue("@ConferenceAmount", GetProperty(ConferenceAmountProperty))
          .Parameters.AddWithValue("@AccommodationInd", GetProperty(AccommodationIndProperty))
          .Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@ChangedReason", GetProperty(ChangedReasonProperty))
          .Parameters.AddWithValue("@BreakfastSystemID", Singular.Misc.NothingDBNull(GetProperty(BreakfastSystemIDProperty)))
          .Parameters.AddWithValue("@BreakfastProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(BreakfastProductionAreaIDProperty)))
          .Parameters.AddWithValue("@BreakfastProductionID", Singular.Misc.NothingDBNull(GetProperty(BreakfastProductionIDProperty)))
          .Parameters.AddWithValue("@BreakfastAdHocBookingID", Singular.Misc.NothingDBNull(GetProperty(BreakfastAdHocBookingIDProperty)))
          .Parameters.AddWithValue("@LunchSystemID", Singular.Misc.NothingDBNull(GetProperty(LunchSystemIDProperty)))
          .Parameters.AddWithValue("@LunchProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(LunchProductionAreaIDProperty)))
          .Parameters.AddWithValue("@LunchProductionID", Singular.Misc.NothingDBNull(GetProperty(LunchProductionIDProperty)))
          .Parameters.AddWithValue("@LunchAdHocBookingID", Singular.Misc.NothingDBNull(GetProperty(LunchAdHocBookingIDProperty)))
          .Parameters.AddWithValue("@DinnerSystemID", Singular.Misc.NothingDBNull(GetProperty(DinnerSystemIDProperty)))
          .Parameters.AddWithValue("@DinnerProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(DinnerProductionAreaIDProperty)))
          .Parameters.AddWithValue("@DinnerProductionID", Singular.Misc.NothingDBNull(GetProperty(DinnerProductionIDProperty)))
          .Parameters.AddWithValue("@DinnerAdHocBookingID", Singular.Misc.NothingDBNull(GetProperty(DinnerAdHocBookingIDProperty)))
          .Parameters.AddWithValue("@IncidentalSystemID", Singular.Misc.NothingDBNull(GetProperty(IncidentalSystemIDProperty)))
          .Parameters.AddWithValue("@IncidentalProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(IncidentalProductionAreaIDProperty)))
          .Parameters.AddWithValue("@IncidentalProductionID", Singular.Misc.NothingDBNull(GetProperty(IncidentalProductionIDProperty)))
          .Parameters.AddWithValue("@IncidentalAdHocBookingID", Singular.Misc.NothingDBNull(GetProperty(IncidentalAdHocBookingIDProperty)))
          '.Parameters.AddWithValue("@ConferenceSystemID", Singular.Misc.NothingDBNull(GetProperty(ConferenceSystemIDProperty)))
          '.Parameters.AddWithValue("@ConferenceProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ConferenceProductionAreaIDProperty)))
          '.Parameters.AddWithValue("@ConferenceProductionID", Singular.Misc.NothingDBNull(GetProperty(ConferenceProductionIDProperty)))
          '.Parameters.AddWithValue("@ConferenceAdHocBookingID", Singular.Misc.NothingDBNull(GetProperty(ConferenceAdHocBookingIDProperty)))
          '.Parameters.AddWithValue("@AdHocBookingID", Singular.Misc.NothingDBNull(GetProperty(AdHocBookingIDProperty)))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(GroupHumanResourceSnTIDProperty, paramGroupHumanResourceSnTID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      '' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delAdHocSnTDetail"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@GroupHumanResourceSnTID", GetProperty(GroupHumanResourceSnTIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace