﻿' Generated 08 Jul 2014 16:05 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.ReadOnly

  <Serializable()> _
  Public Class RORentalCarList
    Inherits OBReadOnlyListBase(Of RORentalCarList, RORentalCar)

#Region " Business Methods "

    Public Function GetItem(RentalCarID As Integer) As RORentalCar

      For Each child As RORentalCar In Me
        If child.RentalCarID = RentalCarID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Rental Cars"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public TravelRequisitionID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(TravelRequisitionID As Integer?)

        Me.TravelRequisitionID = TravelRequisitionID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORentalCarList() As RORentalCarList

      Return New RORentalCarList()

    End Function

    Public Shared Sub BeginGetRORentalCarList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORentalCarList)))

      Dim dp As New DataPortal(Of RORentalCarList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRORentalCarList(CallBack As EventHandler(Of DataPortalResult(Of RORentalCarList)))

      Dim dp As New DataPortal(Of RORentalCarList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORentalCarList(TravelRequisitionID As Integer?) As RORentalCarList

      Return DataPortal.Fetch(Of RORentalCarList)(New Criteria(TravelRequisitionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORentalCar.GetRORentalCar(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORentalCarList"
            cm.Parameters.AddWithValue("@TravelRequisitionID", NothingDBNull(crit.TravelRequisitionID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace