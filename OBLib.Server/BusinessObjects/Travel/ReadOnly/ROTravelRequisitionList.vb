﻿' Generated 02 Jun 2016 12:07 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Travel.ReadOnly

  <Serializable()> _
  Public Class ROTravelRequisitionList
    Inherits OBReadOnlyListBase(Of ROTravelRequisitionList, ROTravelRequisition)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "


    ' ** Example Paging property declaration code to be implemented in the necessary VM **
    'Public Property ROTravelRequisitionList As ROTravelRequisitionList
    'Public Property ROTravelRequisitionListCriteria As ROTravelRequisitionList.Criteria
    'Public Property ROTravelRequisitionListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    ' ** Example Paging definition code to be implemented in the necessary VM Setup Method **
    'ROTravelRequisitionList = New ROTravelRequisitionList
    'ROTravelRequisitionListCriteria = New ROTravelRequisitionList.Criteria
    'ROTravelRequisitionListManager = New Singular.Web.Data.PagedDataManager(Of VMType)(Function(d) d.ROTravelRequisitionList, Function(d) d.ROTravelRequisitionListCriteria, "SortBy", PageSize, SortAsc)

    Public Function GetItem(TravelRequisitionID As Integer) As ROTravelRequisition

      For Each child As ROTravelRequisition In Me
        If child.TravelRequisitionID = TravelRequisitionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Travel Requisitions"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property ProductionID As Integer?
      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROTravelRequisitionList() As ROTravelRequisitionList

      Return New ROTravelRequisitionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTravelRequisitionList() As ROTravelRequisitionList

      Return DataPortal.Fetch(Of ROTravelRequisitionList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTravelRequisition.GetROTravelRequisition(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravelRequisitionList"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace