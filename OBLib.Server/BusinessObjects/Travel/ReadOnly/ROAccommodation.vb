﻿' Generated 08 Jul 2014 16:05 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.ReadOnly

  <Serializable()> _
  Public Class ROAccommodation
    Inherits OBReadOnlyBase(Of ROAccommodation)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccommodationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccommodationID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccommodationID() As Integer
      Get
        Return GetProperty(AccommodationIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelRequisitionID, "Travel Requisition", Nothing)
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="Refereance to the Travel Req")>
    Public ReadOnly Property TravelRequisitionID() As Integer?
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared AccommodationProviderIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccommodationProviderID, "Accommodation Provider", Nothing)
    ''' <summary>
    ''' Gets the Accommodation Provider value
    ''' </summary>
    <Display(Name:="Accommodation Provider", Description:="The hotel/provider of the accommodation")>
    Public ReadOnly Property AccommodationProviderID() As Integer?
      Get
        Return GetProperty(AccommodationProviderIDProperty)
      End Get
    End Property

    Public Shared CheckInDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CheckInDate, "Check In Date")
    ''' <summary>
    ''' Gets the Check In Date value
    ''' </summary>
    <Display(Name:="Check In Date", Description:="Date of check in")>
    Public ReadOnly Property CheckInDate As DateTime?
      Get
        Return GetProperty(CheckInDateProperty)
      End Get
    End Property

    Public Shared CheckOutDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CheckOutDate, "Check Out Date")
    ''' <summary>
    ''' Gets the Check Out Date value
    ''' </summary>
    <Display(Name:="Check Out Date", Description:="Date of check out")>
    Public ReadOnly Property CheckOutDate As DateTime?
      Get
        Return GetProperty(CheckOutDateProperty)
      End Get
    End Property

    Public Shared AccommodationRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccommodationRefNo, "Accommodation Ref No", "")
    ''' <summary>
    ''' Gets the Accommodation Ref No value
    ''' </summary>
    <Display(Name:="Accommodation Ref No", Description:="Reference Number to identify the accommodation")>
    Public ReadOnly Property AccommodationRefNo() As String
      Get
        Return GetProperty(AccommodationRefNoProperty)
      End Get
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="When the booking was cancelled")>
    Public ReadOnly Property CancelledDateTime As DateTime?
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="Who cancelled the booking")>
    Public ReadOnly Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="Why the booking was cancelled")>
    Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared OldProductionAccomodationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OldProductionAccomodationID, "Old Production Accomodation", 0)
    ''' <summary>
    ''' Gets the Old Production Accomodation value
    ''' </summary>
    <Display(Name:="Old Production Accomodation", Description:="")>
    Public ReadOnly Property OldProductionAccomodationID() As Integer
      Get
        Return GetProperty(OldProductionAccomodationIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccommodationIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AccommodationRefNo

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAccommodation(dr As SafeDataReader) As ROAccommodation

      Dim r As New ROAccommodation()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AccommodationIDProperty, .GetInt32(0))
        LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(AccommodationProviderIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CheckInDateProperty, .GetValue(3))
        LoadProperty(CheckOutDateProperty, .GetValue(4))
        LoadProperty(AccommodationRefNoProperty, .GetString(5))
        LoadProperty(CancelledDateTimeProperty, .GetValue(6))
        LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(CancelledReasonProperty, .GetString(8))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
        LoadProperty(OldProductionAccomodationIDProperty, .GetInt32(13))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace