﻿' Generated 08 Jul 2014 16:05 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.ReadOnly

  <Serializable()> _
  Public Class RORentalCar
    Inherits OBReadOnlyBase(Of RORentalCar)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RentalCarIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RentalCarID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property RentalCarID() As Integer
      Get
        Return GetProperty(RentalCarIDProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelRequisitionID, "Travel Requisition", Nothing)
    ''' <summary>
    ''' Gets the Travel Requisition value
    ''' </summary>
    <Display(Name:="Travel Requisition", Description:="Refereance to the Travel Req")>
    Public ReadOnly Property TravelRequisitionID() As Integer?
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared RentalCarAgentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RentalCarAgentID, "Rental Car Agent", Nothing)
    ''' <summary>
    ''' Gets the Rental Car Agent value
    ''' </summary>
    <Display(Name:="Rental Car Agent", Description:="Rental Car Agent that is being booked")>
    Public ReadOnly Property RentalCarAgentID() As Integer?
      Get
        Return GetProperty(RentalCarAgentIDProperty)
      End Get
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "Crew Type", Nothing)
    ''' <summary>
    ''' Gets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="The type of crew this travel is booked for")>
    Public ReadOnly Property CrewTypeID() As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
    End Property

    Public Shared CarTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CarTypeID, "Car Type", Nothing)
    ''' <summary>
    ''' Gets the Car Type value
    ''' </summary>
    <Display(Name:="Car Type", Description:="The type of car. Used to determine the number of passengers this car can hold")>
    Public ReadOnly Property CarTypeID() As Integer?
      Get
        Return GetProperty(CarTypeIDProperty)
      End Get
    End Property

    Public Shared RentalCarRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RentalCarRefNo, "Rental Car Ref No", "")
    ''' <summary>
    ''' Gets the Rental Car Ref No value
    ''' </summary>
    <Display(Name:="Rental Car Ref No", Description:="Ref No for the vehicle, most likely the reference number provided by the agency")>
    Public ReadOnly Property RentalCarRefNo() As String
      Get
        Return GetProperty(RentalCarRefNoProperty)
      End Get
    End Property

    Public Shared AgentBranchIDFromProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AgentBranchIDFrom, "Agent Branch ID From", Nothing)
    ''' <summary>
    ''' Gets the Agent Branch ID From value
    ''' </summary>
    <Display(Name:="Agent Branch ID From", Description:="Agency Branch at which the rental car will be collected")>
    Public ReadOnly Property AgentBranchIDFrom() As Integer?
      Get
        Return GetProperty(AgentBranchIDFromProperty)
      End Get
    End Property

    Public Shared PickupDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PickupDateTime, "Pickup Date Time")
    ''' <summary>
    ''' Gets the Pickup Date Time value
    ''' </summary>
    <Display(Name:="Pickup Date Time", Description:="The date and time the rental car is to be collected")>
    Public ReadOnly Property PickupDateTime As DateTime?
      Get
        Return GetProperty(PickupDateTimeProperty)
      End Get
    End Property

    Public Shared AgentBranchIDToProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AgentBranchIDTo, "Agent Branch ID To", Nothing)
    ''' <summary>
    ''' Gets the Agent Branch ID To value
    ''' </summary>
    <Display(Name:="Agent Branch ID To", Description:="The agency branch that the rental car will be dropped off at")>
    Public ReadOnly Property AgentBranchIDTo() As Integer?
      Get
        Return GetProperty(AgentBranchIDToProperty)
      End Get
    End Property

    Public Shared DropoffDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.DropoffDateTime, "Dropoff Date Time")
    ''' <summary>
    ''' Gets the Dropoff Date Time value
    ''' </summary>
    <Display(Name:="Dropoff Date Time", Description:="The date and time the rental car is to be dropped off")>
    Public ReadOnly Property DropoffDateTime As DateTime?
      Get
        Return GetProperty(DropoffDateTimeProperty)
      End Get
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDateTime, "Cancelled Date Time")
    ''' <summary>
    ''' Gets the Cancelled Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="When the booking was cancelled")>
    Public ReadOnly Property CancelledDateTime As DateTime?
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
    End Property

    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CancelledByUserID, "Cancelled By User", Nothing)
    ''' <summary>
    ''' Gets the Cancelled By User value
    ''' </summary>
    <Display(Name:="Cancelled By User", Description:="Who cancelled the booking")>
    Public ReadOnly Property CancelledByUserID() As Integer?
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="Why the booking was cancelled")>
    Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared OldProductionCrewTravelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OldProductionCrewTravelID, "Old Production Crew Travel", 0)
    ''' <summary>
    ''' Gets the Old Production Crew Travel value
    ''' </summary>
    <Display(Name:="Old Production Crew Travel", Description:="")>
    Public ReadOnly Property OldProductionCrewTravelID() As Integer
      Get
        Return GetProperty(OldProductionCrewTravelIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RentalCarIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.RentalCarRefNo

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORentalCar(dr As SafeDataReader) As RORentalCar

      Dim r As New RORentalCar()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RentalCarIDProperty, .GetInt32(0))
        LoadProperty(TravelRequisitionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(RentalCarAgentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(CarTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(RentalCarRefNoProperty, .GetString(5))
        LoadProperty(AgentBranchIDFromProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(PickupDateTimeProperty, .GetValue(7))
        LoadProperty(AgentBranchIDToProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(DropoffDateTimeProperty, .GetValue(9))
        LoadProperty(CancelledDateTimeProperty, .GetValue(10))
        LoadProperty(CancelledByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(CancelledReasonProperty, .GetString(12))
        LoadProperty(CreatedByProperty, .GetInt32(13))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(14))
        LoadProperty(ModifiedByProperty, .GetInt32(15))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(16))
        LoadProperty(OldProductionCrewTravelIDProperty, .GetInt32(17))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace