﻿' Generated 02 Jun 2016 12:07 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Travel.ReadOnly

  <Serializable()> _
  Public Class ROTravelRequisitionListPaged
    Inherits OBReadOnlyListBase(Of ROTravelRequisitionListPaged, ROTravelRequisitionPaged)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "


    ' ** Example Paging property declaration code to be implemented in the necessary VM **
    'Public Property ROTravelRequisitionList As ROTravelRequisitionList
    'Public Property ROTravelRequisitionListCriteria As ROTravelRequisitionList.Criteria
    'Public Property ROTravelRequisitionListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    ' ** Example Paging definition code to be implemented in the necessary VM Setup Method **
    'ROTravelRequisitionList = New ROTravelRequisitionList
    'ROTravelRequisitionListCriteria = New ROTravelRequisitionList.Criteria
    'ROTravelRequisitionListManager = New Singular.Web.Data.PagedDataManager(Of VMType)(Function(d) d.ROTravelRequisitionList, Function(d) d.ROTravelRequisitionListCriteria, "SortBy", PageSize, SortAsc)

    Public Function GetItem(TravelRequisitionID As Integer) As ROTravelRequisitionPaged

      For Each child As ROTravelRequisitionPaged In Me
        If child.TravelRequisitionID = TravelRequisitionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Travel Requisitions"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList)),
     SetExpression("ROTravelRequisitionListPagedCriteriaBO.SystemIDSet(self)")>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaList)),
     SetExpression("ROTravelRequisitionListPagedCriteriaBO.ProductionAreaIDSet(self)")>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Title", Description:=""),
      SetExpression("ROTravelRequisitionListPagedCriteriaBO.TitleSet(self)", , 250), TextField>
      Public Property Title() As String
        Get
          Return ReadProperty(TitleProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(TitleProperty, Value)
        End Set
      End Property

      Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
      ''' <summary>
      ''' Gets and sets the Human Resource value
      ''' </summary>
      <Display(Name:="Human Resource", Description:=""),
      DropDownWeb(GetType(OBLib.HR.ReadOnly.ROHumanResourceFindDropDownList),
                   BeforeFetchJS:="ROTravelRequisitionListPagedCriteriaBO.setHumanResourceIDCriteriaBeforeRefresh",
                   PreFindJSFunction:="ROTravelRequisitionListPagedCriteriaBO.triggerHumanResourceIDAutoPopulate",
                   AfterFetchJS:="ROTravelRequisitionListPagedCriteriaBO.afterHumanResourceIDRefreshAjax",
                   OnItemSelectJSFunction:="ROTravelRequisitionListPagedCriteriaBO.onHumanResourceIDSelected",
                   LookupMember:="HumanResource", DisplayMember:="FullName", ValueMember:="HumanResourceID",
                   DropDownCssClass:="circuit-dropdown", DropDownColumns:={"FullName", "IDNo"}),
      SetExpression("ROTravelRequisitionListPagedCriteriaBO.HumanResourceIDSet(self)", , 250)>
      Public Property HumanResourceID() As Integer?
        Get
          Return ReadProperty(HumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(HumanResourceIDProperty, Value)
        End Set
      End Property

      Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
      ''' <summary>
      ''' Gets and sets the Human Resource value
      ''' </summary>
      <Display(Name:="Human Resource", Description:=""),
      SetExpression("ROTravelRequisitionListPagedCriteriaBO.HumanResourceSet(self)", , 250), TextField>
      Public Property HumanResource() As String
        Get
          Return ReadProperty(HumanResourceProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(HumanResourceProperty, Value)
        End Set
      End Property

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Start Date", Description:=""),
      SetExpression("ROTravelRequisitionListPagedCriteriaBO.StartDateSet(self)", , 250), TextField>
      Public Property StartDate() As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="End Date", Description:=""),
      SetExpression("ROTravelRequisitionListPagedCriteriaBO.EndDateSet(self)", , 250), TextField>
      Public Property EndDate() As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
      ''' <summary>
      ''' Gets and sets the Human Resource value
      ''' </summary>
      <Display(Name:="Production", Description:="")>
      Public Property ProductionID() As Integer?
        Get
          Return ReadProperty(ProductionIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTitle, "Production", "")
      ''' <summary>
      ''' Gets and sets the Human Resource value
      ''' </summary>
      <Display(Name:="Production", Description:="")>
      Public Property ProductionTitle() As String
        Get
          Return ReadProperty(ProductionTitleProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionTitleProperty, Value)
        End Set
      End Property

      Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingID, "AdHoc Booking", Nothing)
      ''' <summary>
      ''' Gets and sets the Human Resource value
      ''' </summary>
      <Display(Name:="AdHoc Booking", Description:="")>
      Public Property AdHocBookingID() As Integer?
        Get
          Return ReadProperty(AdHocBookingIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(AdHocBookingIDProperty, Value)
        End Set
      End Property

      Public Shared AdHocBookingTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdHocBookingTitle, "AdHoc Booking", "")
      ''' <summary>
      ''' Gets and sets the Human Resource value
      ''' </summary>
      <Display(Name:="AdHoc Booking", Description:="")>
      Public Property AdHocBookingTitle() As String
        Get
          Return ReadProperty(AdHocBookingTitleProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(AdHocBookingTitleProperty, Value)
        End Set
      End Property

      Public Shared AdHocBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingTypeID, "Booking Type", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Booking Type", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.AdHoc.ReadOnly.ROAdHocBookingTypeList)),
     SetExpression("ROTravelRequisitionListPagedCriteriaBO.AdHocBookingTypeIDSet(self)")>
      Public Property AdHocBookingTypeID() As Integer?
        Get
          Return ReadProperty(AdHocBookingTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(AdHocBookingTypeIDProperty, Value)
        End Set
      End Property

      <SetExpression("ROTravelRequisitionListPagedCriteriaBO.CreatedBySet(self)", , 250), TextField>
      Public Property CreatedBy As String = ""

      <SetExpression("ROTravelRequisitionListPagedCriteriaBO.RefNoSet(self)", , 250), TextField>
      Public Property RefNo As Integer? = Nothing

      Public Property PreventRefresh As Boolean = False

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROTravelRequisitionListPaged() As ROTravelRequisitionListPaged

      Return New ROTravelRequisitionListPaged()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROTravelRequisitionListPaged() As ROTravelRequisitionListPaged

      Return DataPortal.Fetch(Of ROTravelRequisitionListPaged)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTravelRequisitionPaged.GetROTravelRequisitionPaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTravelRequisitionListPaged"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@AdHocBookingID", NothingDBNull(crit.AdHocBookingID))
            cm.Parameters.AddWithValue("@AdHocBookingTypeID", NothingDBNull(crit.AdHocBookingTypeID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@Title", NothingDBNull(crit.Title))
            cm.Parameters.AddWithValue("@StartDate", (New SmartDate(crit.StartDate).DBValue))
            cm.Parameters.AddWithValue("@EndDate", (New SmartDate(crit.EndDate).DBValue))
            cm.Parameters.AddWithValue("@CreatedBy", NothingDBNull(crit.CreatedBy))
            cm.Parameters.AddWithValue("@RefNo", NothingDBNull(crit.RefNo))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace