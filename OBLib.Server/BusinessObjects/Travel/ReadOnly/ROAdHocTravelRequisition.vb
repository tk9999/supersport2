﻿' Generated 04 Nov 2014 15:17 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AdHoc.ReadOnly

  <Serializable()> _
  Public Class ROAdHocTravelRequisition
    Inherits SingularReadOnlyBase(Of ROAdHocTravelRequisition)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdHocBookingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AdHocBookingID() As Integer
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
    End Property

    Public Shared AdHocBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingTypeID, "Ad Hoc Booking Type", Nothing)
    ''' <summary>
    ''' Gets the Ad Hoc Booking Type value
    ''' </summary>
    <Display(Name:="Booking Type", Description:="")>
    Public ReadOnly Property AdHocBookingTypeID() As Integer?
      Get
        Return GetProperty(AdHocBookingTypeIDProperty)
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared AdHocBookingTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdHocBookingType, "Booking Type", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Booking Type", Description:="")>
    Public ReadOnly Property AdHocBookingType() As String
      Get
        Return GetProperty(AdHocBookingTypeProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
    Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Starts")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "Ends")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    <Display(Name:="Starts", Description:="")>
    Public ReadOnly Property StartDateTimeString As String
      Get
        If StartDateTime Is Nothing Then
          Return ""
        Else
          Return StartDateTime.Value.ToString("dd MMM yy HH:mm")
        End If
      End Get
    End Property

    <Display(Name:="Ends", Description:="")>
    Public ReadOnly Property EndDateTimeString As String
      Get
        If EndDateTime Is Nothing Then
          Return ""
        Else
          Return EndDateTime.Value.ToString("dd MMM yy HH:mm")
        End If
      End Get
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CountryID, "Country", Nothing)
    ''' <summary>
    ''' Gets the Country ID value
    ''' </summary>
    <Display(Name:="Country", Description:="")>
    Public ReadOnly Property CountryID() As Integer?
      Get
        Return GetProperty(CountryIDProperty)
      End Get
    End Property

    Public Shared RequiresTravelProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresTravel, "Requires Travel?", False)
    ''' <summary>
    ''' Gets the Requires Travel value
    ''' </summary>
    <Display(Name:="Requires Travel?", Description:="")>
    Public ReadOnly Property RequiresTravel() As Boolean
      Get
        Return GetProperty(RequiresTravelProperty)
      End Get
    End Property

    Public Shared BookingInRoomProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.BookingInRoom, "Booking In Room?", False)
    ''' <summary>
    ''' Gets the Booking In Room
    ''' </summary>
    <Display(Name:="Booking In Room?", Description:="")>
    Public ReadOnly Property BookingInRoom() As Boolean
      Get
        Return GetProperty(BookingInRoomProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared LocationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.LocationID, "Location", 0)
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public ReadOnly Property LocationID() As Integer
      Get
        Return GetProperty(LocationIDProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    ''' <summary>
    ''' Gets the Created By Name value
    ''' </summary>
    <Display(Name:="Created By", Description:="")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelRequisitionID, "TravelRequisitionID", Nothing)
    ''' <summary>
    ''' Gets the Country ID value
    ''' </summary>
    <Display(Name:="TravelRequisitionID", Description:="")>
    Public ReadOnly Property TravelRequisitionID() As Integer?
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared RequisitionReferenceNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RequisitionReferenceNumber, "RequisitionReferenceNumber", "")
    ''' <summary>
    ''' Gets the Created By Name value
    ''' </summary>
    <Display(Name:="RequisitionReferenceNumber", Description:="")>
    Public ReadOnly Property RequisitionReferenceNumber() As String
      Get
        Return GetProperty(RequisitionReferenceNumberProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AdHocBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Title

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAdHocTravelRequisition(dr As SafeDataReader) As ROAdHocTravelRequisition

      Dim r As New ROAdHocTravelRequisition()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AdHocBookingIDProperty, .GetInt32(0))
        LoadProperty(AdHocBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(TitleProperty, .GetString(2))
        LoadProperty(DescriptionProperty, .GetString(3))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(StartDateTimeProperty, .GetValue(6))
        LoadProperty(EndDateTimeProperty, .GetValue(7))
        LoadProperty(CountryIDProperty, .GetValue(8))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
        ' LoadProperty(CreatedDateTimeProperty, .GetDateTime(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
        'LoadProperty(ModifiedDateTimeProperty, .GetDateTime(12))
        LoadProperty(LocationIDProperty, .GetInt32(13))
        LoadProperty(CreatedByNameProperty, .GetString(15))
        LoadProperty(AdHocBookingTypeProperty, .GetString(16))
        LoadProperty(RequiresTravelProperty, .GetBoolean(17))
        LoadProperty(BookingInRoomProperty, .GetBoolean(18))
        LoadProperty(TravelRequisitionIDProperty, ZeroNothing(.GetInt32(19)))
        LoadProperty(RequisitionReferenceNumberProperty, .GetString(20))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace