﻿' Generated 02 Jun 2016 12:08 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Travel.ReadOnly

  <Serializable()> _
  Public Class ROTravelRequisitionPaged
    Inherits OBReadOnlyBase(Of ROTravelRequisitionPaged)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROTravelRequisitionPagedBO.ROTravelRequisitionPagedBOToString(self)")

    Public Shared TravelRequisitionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRequisitionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property TravelRequisitionID() As Integer
      Get
        Return GetProperty(TravelRequisitionIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="A refereance to a Production")>
    Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdHocBookingID, "Ad Hoc Booking", 0)
    ''' <summary>
    ''' Gets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking", Description:="")>
    Public ReadOnly Property AdHocBookingID() As Integer
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="Reference to the System")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared VersionNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VersionNo, "Version No", 0)
    ''' <summary>
    ''' Gets the Version No value
    ''' </summary>
    <Display(Name:="Version No", Description:="Version numbe rof the Travel Requisition")>
    Public ReadOnly Property VersionNo() As Integer
      Get
        Return GetProperty(VersionNoProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "Crew Type", Nothing)
    ''' <summary>
    ''' Gets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="")>
    Public ReadOnly Property CrewTypeID() As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
    End Property

    Public Shared SubDeptProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubDept, "Sub Dept", "")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Sub Dept", Description:="")>
    Public ReadOnly Property SubDept() As String
      Get
        Return GetProperty(SubDeptProperty)
      End Get
    End Property

    Public Shared AreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Area, "Area", "")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property Area() As String
      Get
        Return GetProperty(AreaProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    ''' <summary>
    ''' Gets the Created By Name value
    ''' </summary>
    <Display(Name:="Created By", Description:="")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets the Production Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared SubTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubTitle, "Sub Title", "")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Sub Title", Description:="")>
    Public ReadOnly Property SubTitle() As String
      Get
        Return GetProperty(SubTitleProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Starts")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "Ends")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared TravelModuleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TravelModule, "Module", "")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Module", Description:="")>
    Public ReadOnly Property TravelModule() As String
      Get
        Return GetProperty(TravelModuleProperty)
      End Get
    End Property

    <Display(Name:="Req Num")>
    Public ReadOnly Property RequisitionNumber() As String
      Get
        Return GetProperty(TravelRequisitionIDProperty).ToString
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TravelRequisitionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROTravelRequisitionPaged(dr As SafeDataReader) As ROTravelRequisitionPaged

      Dim r As New ROTravelRequisitionPaged()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TravelRequisitionIDProperty, .GetInt32(0))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(AdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(VersionNoProperty, .GetInt32(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(SubDeptProperty, .GetString(11))
        LoadProperty(AreaProperty, .GetString(12))
        LoadProperty(CreatedByNameProperty, .GetString(13))
        LoadProperty(TitleProperty, .GetString(14))
        LoadProperty(SubTitleProperty, .GetString(15))
        LoadProperty(StartDateTimeProperty, .GetValue(16))
        LoadProperty(EndDateTimeProperty, .GetValue(17))
        LoadProperty(TravelModuleProperty, .GetString(18))
        'RowNo = 11
      End With

    End Sub

#End Region

  End Class

End Namespace