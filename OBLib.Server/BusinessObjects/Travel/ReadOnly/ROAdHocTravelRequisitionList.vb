﻿' Generated 04 Nov 2014 15:17 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace AdHoc.ReadOnly

  <Serializable()> _
  Public Class ROAdHocTravelRequisitionList
    Inherits SingularReadOnlyListBase(Of ROAdHocTravelRequisitionList, ROAdHocTravelRequisition)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(AdHocBookingID As Integer) As ROAdHocTravelRequisition

      For Each child As ROAdHocTravelRequisition In Me
        If child.AdHocBookingID = AdHocBookingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Ad Hoc Bookings"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared AdHocBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingTypeID, "Booking Type", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Booking Type", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.AdHoc.ReadOnly.ROAdHocBookingTypeList)),
     SetExpression("ROAdHocTravelRequisitionCriteriaBO.AdHocBookingTypeIDSet(self)")>
      Public Property AdHocBookingTypeID() As Integer?
        Get
          Return ReadProperty(AdHocBookingTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(AdHocBookingTypeIDProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList)),
     SetExpression("ROAdHocTravelRequisitionCriteriaBO.SystemIDSet(self)")>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaList)),
     SetExpression("ROAdHocTravelRequisitionCriteriaBO.ProductionAreaIDSet(self)")>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Title", Description:=""),
      SetExpression("ROAdHocTravelRequisitionCriteriaBO.TitleSet(self)", , 250), TextField>
      Public Property Title() As String
        Get
          Return ReadProperty(TitleProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(TitleProperty, Value)
        End Set
      End Property

      Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
      ''' <summary>
      ''' Gets and sets the Human Resource value
      ''' </summary>
      <Display(Name:="Human Resource", Description:=""),
      SetExpression("ROAdHocTravelRequisitionCriteriaBO.HumanResourceIDSet(self)", , 250)>
      Public Property HumanResourceID() As Integer?
        Get
          Return ReadProperty(HumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(HumanResourceIDProperty, Value)
        End Set
      End Property

      Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
      ''' <summary>
      ''' Gets and sets the Human Resource value
      ''' </summary>
      <Display(Name:="Human Resource", Description:="")>
      Public Property HumanResource() As String
        Get
          Return ReadProperty(HumanResourceProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(HumanResourceProperty, Value)
        End Set
      End Property

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Start Date", Description:=""),
      SetExpression("ROAdHocTravelRequisitionCriteriaBO.StartDateSet(self)", , 250), TextField>
      Public Property StartDate() As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="End Date", Description:=""),
      SetExpression("ROAdHocTravelRequisitionCriteriaBO.EndDateSet(self)", , 250), TextField>
      Public Property EndDate() As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Property AdHocBookingID As Integer? = Nothing
      Public Property FilterName As String = ""

      <SetExpression("ROAdHocTravelRequisitionCriteriaBO.CreatedBySet(self)", , 250), TextField>
      Public Property CreatedBy As String = ""

      <SetExpression("ROAdHocTravelRequisitionCriteriaBO.RefNoSet(self)", , 250), TextField>
      Public Property RefNo As Integer? = Nothing

      Public Sub New()

      End Sub

      Public Sub New(AdHocBookingTypeID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?)

        Me.AdHocBookingTypeID = AdHocBookingTypeID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID

      End Sub

      Public Sub New(AdHocBookingID As Integer?)

        Me.AdHocBookingID = AdHocBookingID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROAdHocTravelRequisitionList() As ROAdHocTravelRequisitionList

      Return New ROAdHocTravelRequisitionList()

    End Function

    Public Shared Sub BeginGetROAdHocTravelRequisitionList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROAdHocTravelRequisitionList)))

      Dim dp As New DataPortal(Of ROAdHocTravelRequisitionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROAdHocTravelRequisitionList(CallBack As EventHandler(Of DataPortalResult(Of ROAdHocTravelRequisitionList)))

      Dim dp As New DataPortal(Of ROAdHocTravelRequisitionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROAdHocTravelRequisitionList(AdHocBookingTypeID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?) As ROAdHocTravelRequisitionList

      Return DataPortal.Fetch(Of ROAdHocTravelRequisitionList)(New Criteria(AdHocBookingTypeID, SystemID, ProductionAreaID))

    End Function

    Public Shared Function GetROAdHocTravelRequisitionList(AdHocBookingID As Integer?) As ROAdHocTravelRequisitionList

      Return DataPortal.Fetch(Of ROAdHocTravelRequisitionList)(New Criteria(AdHocBookingID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROAdHocTravelRequisition.GetROAdHocTravelRequisition(sdr))
      End While
      Me.IsReadOnly = True

      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROAdHocTravelRequisitionList"
            cm.Parameters.AddWithValue("@AdHocBookingTypeID", NothingDBNull(crit.AdHocBookingTypeID))
            cm.Parameters.AddWithValue("@AdHocBookingID", NothingDBNull(crit.AdHocBookingID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@Title", NothingDBNull(crit.Title))
            cm.Parameters.AddWithValue("@StartDate", (New SmartDate(crit.StartDate).DBValue))
            cm.Parameters.AddWithValue("@EndDate", (New SmartDate(crit.EndDate).DBValue))
            cm.Parameters.AddWithValue("@CreatedBy", NothingDBNull(crit.CreatedBy))
            cm.Parameters.AddWithValue("@RefNo", NothingDBNull(crit.RefNo))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            ' cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace