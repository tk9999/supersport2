﻿' Generated 01 May 2017 13:57 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Audit

  <Serializable()> _
  Public Class ROObjectAuditTrail
    Inherits OBReadOnlyBase(Of ROObjectAuditTrail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsExpanded, "Is Expanded", False)
    ''' <summary>
    ''' Gets and sets the Is TBC value
    ''' </summary>
    <Display(Name:="Is Expanded", Description:="")>
    Public ReadOnly Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROObjectAuditTrailBO.ROObjectAuditTrailBOToString(self)")

    Public Shared AuditTrailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property AuditTrailID() As Integer
      Get
        Return GetProperty(AuditTrailIDProperty)
      End Get
    End Property

    Public Shared SectionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Section, "Section")
    ''' <summary>
    ''' Gets the Section value
    ''' </summary>
    <Display(Name:="Section", Description:="")>
    Public ReadOnly Property Section() As String
      Get
        Return GetProperty(SectionProperty)
      End Get
    End Property

    Public Shared ChangeDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ChangeDateTime, "Change Time")
    ''' <summary>
    ''' Gets the Change Date Time value
    ''' </summary>
    <Display(Name:="Change Time", Description:=""),
    DateField(FormatString:="dd MMM yy HH:mm:ss.l")>
    Public ReadOnly Property ChangeDateTime As DateTime
      Get
        Return GetProperty(ChangeDateTimeProperty)
      End Get
    End Property

    Public Shared ChangeTypeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangeTypeDescription, "Change Type")
    ''' <summary>
    ''' Gets the Change Type Description value
    ''' </summary>
    <Display(Name:="Change Type", Description:="")>
    Public ReadOnly Property ChangeTypeDescription() As String
      Get
        Return GetProperty(ChangeTypeDescriptionProperty)
      End Get
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "Login Name")
    ''' <summary>
    ''' Gets the Login Name value
    ''' </summary>
    <Display(Name:="Login Name", Description:="")>
    Public ReadOnly Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
    End Property

    Public Shared MachineNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MachineName, "Machine Name")
    ''' <summary>
    ''' Gets the Machine Name value
    ''' </summary>
    <Display(Name:="Machine Name", Description:="")>
    Public ReadOnly Property MachineName() As String
      Get
        Return GetProperty(MachineNameProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROObjectAuditTrailDetailListProperty As PropertyInfo(Of ROObjectAuditTrailDetailList) = RegisterProperty(Of ROObjectAuditTrailDetailList)(Function(c) c.ROObjectAuditTrailDetailList, "RO Object Audit Trail Detail List")

    Public ReadOnly Property ROObjectAuditTrailDetailList() As ROObjectAuditTrailDetailList
      Get
        If GetProperty(ROObjectAuditTrailDetailListProperty) Is Nothing Then
          LoadProperty(ROObjectAuditTrailDetailListProperty, Audit.ROObjectAuditTrailDetailList.NewROObjectAuditTrailDetailList())
        End If
        Return GetProperty(ROObjectAuditTrailDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AuditTrailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Section

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROObjectAuditTrail(dr As SafeDataReader) As ROObjectAuditTrail

      Dim r As New ROObjectAuditTrail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AuditTrailIDProperty, .GetInt32(0))
        LoadProperty(SectionProperty, .GetString(1))
        LoadProperty(ChangeDateTimeProperty, .GetValue(2))
        LoadProperty(ChangeTypeDescriptionProperty, .GetString(3))
        LoadProperty(LoginNameProperty, .GetString(4))
        LoadProperty(MachineNameProperty, .GetString(5))
      End With

    End Sub

#End Region

  End Class

End Namespace