﻿' Generated 01 May 2017 13:57 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Audit

  <Serializable()> _
  Public Class ROObjectAuditTrailDetailList
    Inherits OBReadOnlyListBase(Of ROObjectAuditTrailDetailList, ROObjectAuditTrailDetail)

#Region " Parent "

    <NotUndoable()> Private mParent As ROObjectAuditTrail
#End Region

#Region " Business Methods "

    Public Function GetItem(AuditTrailDetailID As Integer) As ROObjectAuditTrailDetail

      For Each child As ROObjectAuditTrailDetail In Me
        If child.AuditTrailDetailID = AuditTrailDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewROObjectAuditTrailDetailList() As ROObjectAuditTrailDetailList

      Return New ROObjectAuditTrailDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace