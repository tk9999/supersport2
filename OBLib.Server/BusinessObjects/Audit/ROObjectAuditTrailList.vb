﻿' Generated 01 May 2017 13:57 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Audit

  <Serializable()> _
  Public Class ROObjectAuditTrailList
    Inherits OBReadOnlyListBase(Of ROObjectAuditTrailList, ROObjectAuditTrail)

#Region " Business Methods "

    Public Function GetItem(AuditTrailID As Integer) As ROObjectAuditTrail

      For Each child As ROObjectAuditTrail In Me
        If child.AuditTrailID = AuditTrailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROObjectAuditTrailDetail(AuditTrailDetailID As Integer) As ROObjectAuditTrailDetail

      Dim obj As ROObjectAuditTrailDetail = Nothing
      For Each parent As ROObjectAuditTrail In Me
        obj = parent.ROObjectAuditTrailDetailList.GetItem(AuditTrailDetailID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ForScenario As String
      Public Property RoomScheduleID As Integer?
      Public Property ProductionSystemAreaID As Integer?
      Public Property HumanResourceShiftID As Integer?

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROObjectAuditTrailList() As ROObjectAuditTrailList

      Return New ROObjectAuditTrailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROObjectAuditTrailList() As ROObjectAuditTrailList

      Return DataPortal.Fetch(Of ROObjectAuditTrailList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROObjectAuditTrail.GetROObjectAuditTrail(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROObjectAuditTrail = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AuditTrailID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROObjectAuditTrailDetailList.RaiseListChangedEvents = False
          parent.ROObjectAuditTrailDetailList.Add(ROObjectAuditTrailDetail.GetROObjectAuditTrailDetail(sdr))
          parent.ROObjectAuditTrailDetailList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROObjectAuditTrailList"
            cm.Parameters.AddWithValue("@ForScenario", NothingDBNull(crit.ForScenario))
            cm.Parameters.AddWithValue("@RoomScheduleID", NothingDBNull(crit.RoomScheduleID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@HumanResourceShiftID", NothingDBNull(crit.HumanResourceShiftID))
            cm.CommandTimeout = 30
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace