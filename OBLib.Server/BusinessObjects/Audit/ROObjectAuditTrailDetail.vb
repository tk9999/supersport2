﻿' Generated 01 May 2017 13:58 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Audit

  <Serializable()> _
  Public Class ROObjectAuditTrailDetail
    Inherits OBReadOnlyBase(Of ROObjectAuditTrailDetail)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROObjectAuditTrailDetailBO.ROObjectAuditTrailDetailBOToString(self)")

    Public Shared AuditTrailDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailDetailID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AuditTrailDetailID() As Integer
      Get
        Return GetProperty(AuditTrailDetailIDProperty)
      End Get
    End Property

    Public Shared AuditTrailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuditTrailID, "Audit Trail")
    ''' <summary>
    ''' Gets the Audit Trail value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property AuditTrailID() As Integer
      Get
        Return GetProperty(AuditTrailIDProperty)
      End Get
    End Property

    Public Shared ChangeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangeDescription, "Field")
    ''' <summary>
    ''' Gets the Change Description value
    ''' </summary>
    <Display(Name:="Field", Description:="")>
    Public ReadOnly Property ChangeDescription() As String
      Get
        Return GetProperty(ChangeDescriptionProperty)
      End Get
    End Property

    Public Shared PreviousValueDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreviousValueDescription, "Previous Value")
    ''' <summary>
    ''' Gets the Previous Value Description value
    ''' </summary>
    <Display(Name:="Previous Value", Description:="")>
    Public ReadOnly Property PreviousValueDescription() As String
      Get
        Return GetProperty(PreviousValueDescriptionProperty)
      End Get
    End Property

    Public Shared NewValueDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NewValueDescription, "New Value")
    ''' <summary>
    ''' Gets the New Value Description value
    ''' </summary>
    <Display(Name:="New Value", Description:="")>
    Public ReadOnly Property NewValueDescription() As String
      Get
        Return GetProperty(NewValueDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AuditTrailDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ChangeDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROObjectAuditTrailDetail(dr As SafeDataReader) As ROObjectAuditTrailDetail

      Dim r As New ROObjectAuditTrailDetail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AuditTrailDetailIDProperty, .GetInt32(0))
        LoadProperty(AuditTrailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ChangeDescriptionProperty, .GetString(2))
        LoadProperty(PreviousValueDescriptionProperty, .GetString(3))
        LoadProperty(NewValueDescriptionProperty, .GetString(4))
      End With

    End Sub

#End Region

  End Class

End Namespace