﻿Imports System.Drawing


Namespace PositionLayouts

  <Serializable()> _
  Public Class Vector

    Private X, Y, XPos, YPos As Double
    Public Xtheta As Double = 0.0
    Private Length As Double
    Public Ori As Point

    Public Sub New(ByVal xt As Double, ByVal yt As Double, ByVal Origin As Point)
      Ori = Origin
      X = xt
      Y = yt
      Length = Math.Sqrt((X * X) + (Y * Y))
      Xtheta = Math.Asin(Math.Abs(Y) / Length)

      If X < 0 Then
        If Y < 0 Then
          Xtheta = Math.PI + Xtheta ' Both Negative
        Else
          Xtheta = Math.PI - Xtheta 'Only X Negative
        End If
      Else
        If Y < 0 Then
          Xtheta = -Xtheta ' Only Y negative
        Else
          Xtheta = Xtheta ' Both Positive
        End If
      End If

      YPos = Y
      XPos = X
    End Sub

    Public Function getPoint(ByVal XMag As Double, ByVal YMag As Double)
      Dim p As Point = New Point((Ori.X + XPos) * XMag / 100, (Ori.Y + YPos) * YMag / 100)
      Return p
    End Function

    Public Function getPoint(ByVal XMag As Double, ByVal YMag As Double, ByVal ex As Double)
      Dim p As Point = New Point((Ori.X + (XPos * ex)) * XMag / 100, (Ori.Y + (YPos * ex)) * YMag / 100)
      Return p
    End Function

    Public Sub MoveLeft(ByVal deg As Double)
      Xtheta = Xtheta + deg
      YPos = Length * Math.Sin(Xtheta)
      XPos = Length * Math.Cos(Xtheta)
    End Sub

    Public Sub MoveRight(ByVal deg As Double)
      Xtheta = Xtheta - deg
      XPos = YPos / Math.Tan(Xtheta)
      YPos = XPos * Math.Tan(Xtheta)
    End Sub

  End Class

End Namespace