﻿' Generated 09 Mar 2015 16:30 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace PositionLayouts.ReadOnly

  <Serializable()> _
  Public Class ROPositionLayoutList
    Inherits OBReadOnlyListBase(Of ROPositionLayoutList, ROPositionLayout)
    Implements Singular.Paging.IPagedList


    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(PositionLayoutID As Integer) As ROPositionLayout

      For Each child As ROPositionLayout In Me
        If child.PositionLayoutID = PositionLayoutID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Position Layouts"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property LayoutName As String = ""
      Public Property ProductionVenueImageID As Integer = 0
      Public Property ProductionID As Integer = 0
      Public Property CreatedBy As Integer = 0
      Public Property ProductionTypeID As Integer = 0
      Public Property ProductionVenueID As Integer = 0

      <Display(Name:="Templates Only?", Description:="Filter for Templates only")>
      Public Property TemplateInd As Boolean? = False
      Public Property PositionLayoutType As Integer = 0

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROPositionLayoutList() As ROPositionLayoutList

      Return New ROPositionLayoutList()

    End Function

    Public Shared Sub BeginGetROPositionLayoutList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROPositionLayoutList)))

      Dim dp As New DataPortal(Of ROPositionLayoutList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROPositionLayoutList(CallBack As EventHandler(Of DataPortalResult(Of ROPositionLayoutList)))

      Dim dp As New DataPortal(Of ROPositionLayoutList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROPositionLayoutList() As ROPositionLayoutList

      Return DataPortal.Fetch(Of ROPositionLayoutList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPositionLayout.GetROPositionLayout(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROPositionLayoutList"
            cm.Parameters.AddWithValue("@LayoutName", crit.LayoutName)
            cm.Parameters.AddWithValue("@ProductionVenueImageID", Singular.Misc.ZeroDBNull(crit.ProductionVenueImageID))
            cm.Parameters.AddWithValue("@ProductionID", Singular.Misc.ZeroDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@CreatedBy", Singular.Misc.ZeroDBNull(crit.CreatedBy))
            cm.Parameters.AddWithValue("@ProductionTypeID", Singular.Misc.ZeroDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@ProductionVenueID", Singular.Misc.ZeroDBNull(crit.ProductionVenueID))
            cm.Parameters.AddWithValue("@TemplateInd", crit.TemplateInd)
            cm.Parameters.AddWithValue("@PositionLayoutTypeID", Singular.Misc.ZeroDBNull(crit.PositionLayoutType))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region


  End Class

End Namespace