﻿' Generated 02 Jun 2014 21:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace PositionLayouts.ReadOnly

  <Serializable()> _
  Public Class ROProductionVenueImage
    Inherits OBReadOnlyBase(Of ROProductionVenueImage)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVenueImageIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueImageID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionVenueImageID() As Integer
      Get
        Return GetProperty(ProductionVenueImageIDProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeID, "Production Type", 0)
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="The production type that this image is for")>
    Public ReadOnly Property ProductionTypeID() As Integer
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueID, "Production Venue", 0)
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="The Venue that this image corresponds to. If left NULL then this is a template image that can be used for all production of this production type.")>
    Public ReadOnly Property ProductionVenueID() As Integer
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Production Venue", 0)
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="The Venue that this image corresponds to. If left NULL then this is a template image that can be used for all production of this production type.")>
    Public ReadOnly Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
    End Property

    Public Shared VenueImageProperty As PropertyInfo(Of System.Drawing.Image) = RegisterProperty(Of System.Drawing.Image)(Function(c) c.VenueImage)
    ''' <summary>
    ''' Gets the Venue Image value
    ''' </summary>
    <Display(Name:="Venue Image", Description:="The image of the venue")>
    Public ReadOnly Property VenueImage() As System.Drawing.Image
      Get
        Return GetProperty(VenueImageProperty)
      End Get
    End Property

    Public Shared VenueImageThumbnailProperty As PropertyInfo(Of System.Drawing.Image) = RegisterProperty(Of System.Drawing.Image)(Function(c) c.VenueImageThumbnail)
    ''' <summary>
    ''' Gets the Venue Image Thumbnail value
    ''' </summary>
    <Display(Name:="Venue Image Thumbnail", Description:="")>
        Public ReadOnly Property VenueImageThumbnail() As System.Drawing.Image
      Get
        Return GetProperty(VenueImageThumbnailProperty)
      End Get
    End Property

    Public Shared ImageNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImageName, "Image Name", "")
    ''' <summary>
    ''' Gets the Image Name value
    ''' </summary>
    <Display(Name:="Image Name", Description:="A name for the image")>
    Public ReadOnly Property ImageName() As String
      Get
        Return GetProperty(ImageNameProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    'Public Sub SetVenueImageFromFile(ByVal FileName As String)

    '  Dim bmp As System.Drawing.Bitmap = System.Drawing.Bitmap.FromFile(FileName)
    '  SetProperty(VenueImageProperty, bmp)

    'End Sub

    'Public Sub SetVenueImageThumbnailFromFile(ByVal FileName As String)

    '  Dim bmp As System.Drawing.Bitmap = System.Drawing.Bitmap.FromFile(FileName)
    '  SetProperty(VenueImageThumbnailProperty, bmp)

    'End Sub

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueImageIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ImageName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionVenueImage(dr As SafeDataReader) As ROProductionVenueImage

      Dim r As New ROProductionVenueImage()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionVenueImageIDProperty, .GetInt32(0))
        LoadProperty(ProductionTypeIDProperty, .GetInt32(1))
        LoadProperty(ProductionVenueIDProperty, .GetInt32(2))
        If Not IsNothing(.GetValue(3)) Then
          Dim bytes(.GetBytes(3, 0, Nothing, 0, Integer.MaxValue)) As Byte
          .GetBytes(3, 0, bytes, 0, Integer.MaxValue)
          LoadProperty(VenueImageProperty, System.Drawing.Image.FromStream(New IO.MemoryStream(bytes)))
        End If
        If Not IsNothing(.GetValue(4)) Then
          Dim bytes(.GetBytes(4, 0, Nothing, 0, Integer.MaxValue)) As Byte
          .GetBytes(4, 0, bytes, 0, Integer.MaxValue)
          LoadProperty(VenueImageThumbnailProperty, System.Drawing.Image.FromStream(New IO.MemoryStream(bytes)))
        End If
        LoadProperty(ImageNameProperty, .GetString(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(ProductionVenueProperty, .GetString(10))
      End With

    End Sub

    'Protected Sub Fetch(ByRef sdr As SafeDataReader)

    '  With sdr
    '    LoadProperty(ProductionVenueImageIDProperty, .GetInt32(0))
    '    LoadProperty(ProductionTypeIDProperty, .GetInt32(1))
    '    LoadProperty(ProductionVenueIDProperty, .GetInt32(2))

    '    Dim bytes() As Byte = .GetValue(3)
    '    If Not IsNothing(bytes) Then
    '      Using imageStream As New IO.MemoryStream(bytes)
    '        LoadProperty(VenueImageProperty, System.Drawing.Image.FromStream(New IO.MemoryStream(bytes)))
    '      End Using
    '      bytes = Nothing
    '    End If

    '    bytes = .GetValue(4)
    '    If Not IsNothing(bytes) Then
    '      Using imageStream As New IO.MemoryStream(bytes)
    '        LoadProperty(VenueImageThumbnailProperty, System.Drawing.Image.FromStream(New IO.MemoryStream(bytes)))
    '      End Using
    '      bytes = Nothing
    '    End If

    '    LoadProperty(ImageNameProperty, .GetString(5))
    '    LoadProperty(CreatedByProperty, .GetInt32(6))
    '    LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
    '    LoadProperty(ModifiedByProperty, .GetInt32(8))
    '    LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
    '  End With

    'End Sub

#End If

#End Region

#End Region

  End Class

End Namespace