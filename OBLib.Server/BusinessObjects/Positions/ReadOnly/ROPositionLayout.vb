﻿' Generated 09 Mar 2015 16:31 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace PositionLayouts.ReadOnly

  <Serializable()> _
  Public Class ROPositionLayout
    Inherits OBReadOnlyBase(Of ROPositionLayout)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PositionLayoutIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionLayoutID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property PositionLayoutID() As Integer
      Get
        Return GetProperty(PositionLayoutIDProperty)
      End Get
    End Property

    Public Shared LayoutNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LayoutName, "Layout Name", "")
    ''' <summary>
    ''' Gets the Layout Name value
    ''' </summary>
    <Display(Name:="Layout Name", Description:="The name of this layout")>
    Public ReadOnly Property LayoutName() As String
      Get
        Return GetProperty(LayoutNameProperty)
      End Get
    End Property

    Public Shared ProductionVenueImageIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueImageID, "Production Venue Image", Nothing)
    ''' <summary>
    ''' Gets the Production Venue Image value
    ''' </summary>
    <Display(Name:="Production Venue Image", Description:="The template image for this position layout")>
    Public ReadOnly Property ProductionVenueImageID() As Integer?
      Get
        Return GetProperty(ProductionVenueImageIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The position structure for a production. If NULL then this is a template structure")>
    Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared PositionLayoutTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionLayoutTypeID, "Position Layout Type", Nothing)
    ''' <summary>
    ''' Gets the Position Layout Type value
    ''' </summary>
    <Display(Name:="Position Layout Type", Description:="")>
    Public ReadOnly Property PositionLayoutTypeID() As Integer?
      Get
        Return GetProperty(PositionLayoutTypeIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ImageNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImageName, "Image Name", "")
    ''' <summary>
    ''' Gets the Image Name value
    ''' </summary>
    <Display(Name:="Image Name", Description:="")>
    Public ReadOnly Property ImageName() As String
      Get
        Return GetProperty(ImageNameProperty)
      End Get
    End Property

    Public Shared ProductionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Production, "Production", "")
    ''' <summary>
    ''' Gets the Image Name value
    ''' </summary>
    <Display(Name:="Production Name", Description:="")>
    Public ReadOnly Property Production() As String
      Get
        Return GetProperty(ProductionProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "CreatedByName", "")
    ''' <summary>
    ''' Gets the Image Name value
    ''' </summary>
    <Display(Name:="Created By", Description:="")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PositionLayoutIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.LayoutName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROPositionLayout(dr As SafeDataReader) As ROPositionLayout

      Dim r As New ROPositionLayout()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(PositionLayoutIDProperty, .GetInt32(0))
        LoadProperty(LayoutNameProperty, .GetString(1))
        LoadProperty(ProductionVenueImageIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(PositionLayoutTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(ImageNameProperty, .GetString(9))
        LoadProperty(ProductionProperty, .GetString(10))
        LoadProperty(CreatedByNameProperty, .GetString(11))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace