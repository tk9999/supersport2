﻿' Generated 02 Jun 2014 21:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace PositionLayouts.ReadOnly

  <Serializable()> _
  Public Class ROProductionVenueImageList
    Inherits OBReadOnlyListBase(Of ROProductionVenueImageList, ROProductionVenueImage)
    Implements Singular.Paging.IPagedList


    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionVenueImageID As Integer) As ROProductionVenueImage

      For Each child As ROProductionVenueImage In Me
        If child.ProductionVenueImageID = ProductionVenueImageID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Venue Images"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property ImageName As String = ""
      Public ProductionVenueImageID As Integer? = Nothing
      Public LoadImageInd As Boolean

      Public Sub New()

      End Sub

      Public Sub New(ProductionVenueImageID As Integer?)

        Me.ProductionVenueImageID = ProductionVenueImageID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionVenueImageList() As ROProductionVenueImageList

      Return New ROProductionVenueImageList()

    End Function

    Public Shared Sub BeginGetROProductionVenueImageList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionVenueImageList)))

      Dim dp As New DataPortal(Of ROProductionVenueImageList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionVenueImageList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionVenueImageList)))

      Dim dp As New DataPortal(Of ROProductionVenueImageList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionVenueImageList() As ROProductionVenueImageList

      Return DataPortal.Fetch(Of ROProductionVenueImageList)(New Criteria())

    End Function

    Public Shared Function GetROProductionVenueImageList(ProductionVenueImageID As Integer?, LoadImageInd As Boolean) As ROProductionVenueImageList

      Return DataPortal.Fetch(Of ROProductionVenueImageList)(New Criteria() With {.ProductionVenueImageID = ProductionVenueImageID, .LoadImageInd = LoadImageInd})

    End Function

    Public Shared Function GetROProductionVenueImageList(ProductionVenueImageID As Integer?) As ROProductionVenueImageList

      Return DataPortal.Fetch(Of ROProductionVenueImageList)(New Criteria(ProductionVenueImageID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionVenueImage.GetROProductionVenueImage(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionVenueImageList"
            cm.Parameters.AddWithValue("@ProductionVenueImageID", crit.ProductionVenueImageID)
            cm.Parameters.AddWithValue("@ImageName", crit.ImageName)
            cm.Parameters.AddWithValue("LoadImageInd", crit.LoadImageInd)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace