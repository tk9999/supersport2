﻿' Generated 02 Jun 2014 21:30 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Drawing 
Imports OBLib.PositionLayouts.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace PositionLayouts

  <Serializable()> _
  Public Class PositionLayout
    Inherits OBBusinessBase(Of PositionLayout)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PositionLayoutIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionLayoutID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property PositionLayoutID() As Integer
      Get
        Return GetProperty(PositionLayoutIDProperty)
      End Get
    End Property

    Public Shared LayoutNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LayoutName, "Layout Name", "")
    ''' <summary>
    ''' Gets and sets the Layout Name value
    ''' </summary>
    <Display(Name:="Layout Name", Description:="The name of this layout"),
    StringLength(200, ErrorMessage:="Layout Name cannot be more than 200 characters"),
    Required(ErrorMessage:="Layout Name is required")>
    Public Property LayoutName() As String
      Get
        Return GetProperty(LayoutNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LayoutNameProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueImageIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueImageID, "Production Venue Image", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue Image value  
    ''' </summary>
    <Display(Name:="Venue Image", Description:="The template image for this position layout"),
    Required(ErrorMessage:="Production Venue Image required") >
    Public Property ProductionVenueImageID() As Integer?
      Get
        Return GetProperty(ProductionVenueImageIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueImageIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The position structure for a production. If NULL then this is a template structure"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Productions.ReadOnly.ROProductionList), UnselectedText:="Production ")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared PositionLayoutTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionLayoutTypeID, "Position Layout Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Position Layout Type value
    ''' </summary>
    <Display(Name:="Position Layout Type", Description:=""),
    Required(ErrorMessage:="Position Layout Type required")>
    Public Property PositionLayoutTypeID() As Integer?
      Get
        Return GetProperty(PositionLayoutTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionLayoutTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", Settings.CurrentUserID)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    <Browsable(False)> _
    Public ReadOnly Property Img() As System.Drawing.Image
      Get
        Return ROProductionVenueImage.VenueImage
      End Get
    End Property

    Public ReadOnly Property ImgString As String
      Get
        If Img Is Nothing Or ProductionVenueImageName Is Nothing Then
          Return ""
        End If

        Dim type As String = "png"
        'If ProductionVenueImageName.LastIndexOf(".") > 0 Then
        '  type = ProductionVenueImageName.Substring(ProductionVenueImageName.LastIndexOf(".") + 1)
        'End If
        Dim b() As Byte = ImagetoArray(Img, ProductionVenueImageName)
        Dim str = Convert.ToBase64String(b)
        Return "data:image/" & type & ";base64," & str
        Return ""
      End Get
    End Property

    Public Property ProductionVenueImageName() As String

    Public ReadOnly Property Width As String
      Get
        If Img IsNot Nothing Then
          Return Img.Width.ToString
        End If
        Return ""
      End Get
    End Property

    Public ReadOnly Property Height As String
      Get
        If Img IsNot Nothing Then
          Return Img.Height.ToString
        End If
        Return ""
      End Get
    End Property

    Public Shared NewImageProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.NewImage, "New Image", False)
    ''' <summary>
    ''' Gets and sets the Position Layout Type value
    ''' </summary>
    <Display(Name:="New Image", Description:="")>
    Public Property NewImage() As Boolean
      Get
        Return GetProperty(NewImageProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(NewImageProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared PositionLayoutPositionListProperty As PropertyInfo(Of PositionLayoutPositionList) = RegisterProperty(Of PositionLayoutPositionList)(Function(c) c.PositionLayoutPositionList, "Position Layout Position List")

    Public ReadOnly Property PositionLayoutPositionList() As PositionLayoutPositionList
      Get
        If GetProperty(PositionLayoutPositionListProperty) Is Nothing Then
          LoadProperty(PositionLayoutPositionListProperty, PositionLayouts.PositionLayoutPositionList.NewPositionLayoutPositionList())
        End If
        Return GetProperty(PositionLayoutPositionListProperty)
      End Get
    End Property

    <NotUndoable(), NonSerialized()>
    Private mROProductionVenueImage As PositionLayouts.ReadOnly.ROProductionVenueImage

    Public ReadOnly Property ROProductionVenueImage() As PositionLayouts.ReadOnly.ROProductionVenueImage
      Get
        If mROProductionVenueImage Is Nothing OrElse mROProductionVenueImage.ProductionVenueImageID <> GetProperty(ProductionVenueImageIDProperty) Then
          If GetProperty(ProductionVenueImageIDProperty) = 0 Then
            mROProductionVenueImage = Nothing
          Else
            Dim ROProductionVenueImageList As PositionLayouts.ReadOnly.ROProductionVenueImageList = PositionLayouts.ReadOnly.ROProductionVenueImageList.GetROProductionVenueImageList(GetProperty(ProductionVenueImageIDProperty), True)
            mROProductionVenueImage = ROProductionVenueImageList.FirstOrDefault()
          End If
        End If
        Return mROProductionVenueImage
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PositionLayoutIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.LayoutName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Position Layout")
        Else
          Return String.Format("Blank {0}", "Position Layout")
        End If
      Else
        Return Me.LayoutName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"PositionLayoutPositions"}
      End Get
    End Property

    Public Function ImagetoArray(Img As Drawing.Image, ImageName As String) As Byte()

      Dim ms As New IO.MemoryStream
      Dim type As String = ImageName.Substring(ImageName.LastIndexOf(".") + 1)
      Dim format As Drawing.Imaging.ImageFormat = Drawing.Imaging.ImageFormat.Png

      Select Case type.ToLower()
        Case "jpg", "jpeg"
          format = Drawing.Imaging.ImageFormat.Jpeg
        Case "png"
          format = Drawing.Imaging.ImageFormat.Png
      End Select

      Img.Save(ms, format)

      Img.Save("C:\Clients\Test.png")

      Return ms.ToArray

    End Function

    Public Shared Function GetTemplateDataset()

      Dim ds As New DataSet("DataSet")

      Dim tbl As New DataTable("Image")

      Dim clm As New DataColumn("Image")
      clm.DataType = GetType(Byte())
      tbl.Columns.Add(clm)
      clm = New DataColumn("LayoutName")
      clm.DataType = GetType(String)
      tbl.Columns.Add(clm)
      clm = New DataColumn("PositionLayoutTypeID")
      clm.DataType = GetType(Integer)
      tbl.Columns.Add(clm)

      ds.Tables.Add(tbl)

      tbl = New DataTable("PositionLayoutPositionList")

      clm = New DataColumn("PositionLayoutPositionID")
      clm.DataType = GetType(Integer)
      tbl.Columns.Add(clm)
      clm = New DataColumn("PositionID")
      clm.DataType = GetType(Integer)
      tbl.Columns.Add(clm)
      clm = New DataColumn("CreatedBy")
      clm.DataType = GetType(Integer)
      tbl.Columns.Add(clm)
      clm = New DataColumn("EquipmentSubType")
      clm.DataType = GetType(String)
      tbl.Columns.Add(clm)
      clm = New DataColumn("LensEquipmentSubType")
      clm.DataType = GetType(String)
      tbl.Columns.Add(clm)
      clm = New DataColumn("TripodEquipmentSubType")
      clm.DataType = GetType(String)
      tbl.Columns.Add(clm)
      clm = New DataColumn("EVSPosition")
      clm.DataType = GetType(String)
      tbl.Columns.Add(clm)
      clm = New DataColumn("Notes")
      clm.DataType = GetType(String)
      tbl.Columns.Add(clm)
      clm = New DataColumn("PositionCode")
      clm.DataType = GetType(String)
      tbl.Columns.Add(clm)
      clm = New DataColumn("PositionLayoutTypeID")
      clm.DataType = GetType(Integer)
      tbl.Columns.Add(clm)

      ds.Tables.Add(tbl)

      tbl = New DataTable("PositionLayoutSubPositionList")

      clm = New DataColumn("PositionLayoutSubPositionID")
      clm.DataType = GetType(Integer)
      tbl.Columns.Add(clm)
      clm = New DataColumn("PositionLayoutPositionID")
      clm.DataType = GetType(Integer)
      tbl.Columns.Add(clm)
      clm = New DataColumn("TripodEquipmentSubType")
      clm.DataType = GetType(String)
      tbl.Columns.Add(clm)
      clm = New DataColumn("Notes")
      clm.DataType = GetType(String)
      tbl.Columns.Add(clm)
      clm = New DataColumn("PositionCode")
      clm.DataType = GetType(String)
      tbl.Columns.Add(clm)

      ds.Tables.Add(tbl)

      Return ds

    End Function

    Public Function GetDataSet(ByVal TemplateDataset As DataSet) As System.Data.DataSet

      Dim ds As DataSet = TemplateDataset
      If ds Is Nothing Then
        ds = GetTemplateDataset()
      End If

      Dim drw As DataRow

      drw = ds.Tables("Image").NewRow
      drw(0) = BmpToBytes(GetImage)
      drw(1) = GetProperty(LayoutNameProperty)
      drw(2) = GetProperty(PositionLayoutTypeIDProperty)
      ds.Tables("Image").Rows.Add(drw)

      Dim ROEquipmentSubTypeList As Maintenance.General.ReadOnly.ROEquipmentSubTypeList = Maintenance.General.ReadOnly.ROEquipmentSubTypeList.GetROEquipmentSubTypeList()
      Dim ROPositionList As Maintenance.General.ReadOnly.ROPositionList = Maintenance.General.ReadOnly.ROPositionList.GetROPositionList()

      For Each cp As PositionLayouts.PositionLayoutPosition In Me.PositionLayoutPositionList

        If cp.ShowInd Then
          drw = ds.Tables("PositionLayoutPositionList").NewRow
          drw(0) = cp.PositionLayoutPositionID
          drw(1) = cp.PositionID
          drw(2) = CInt(cp.CreatedBy)

          If Not IsNullNothing(cp.MainEquipmentSubTypeID) Then
            drw(3) = ROEquipmentSubTypeList.GetItem(cp.MainEquipmentSubTypeID).EquipmentSubType 'CommonData.Lists.ROEquipmentSubTypeList.GetItem(cp.MainEquipmentSubTypeID).EquipmentSubType
          End If

          If Not IsNullNothing(cp.LensEquipmentSubTypeID) Then
            drw(4) = ROEquipmentSubTypeList.GetItem(cp.LensEquipmentSubTypeID).EquipmentSubType 'CommonData.Lists.ROEquipmentSubTypeList.GetItem(cp.LensEquipmentSubTypeID).EquipmentSubType
          End If

          If Not IsNullNothing(cp.TripodEquipmentSubTypeID) Then
            drw(5) = ROEquipmentSubTypeList.GetItem(cp.TripodEquipmentSubTypeID).EquipmentSubType 'CommonData.Lists.ROEquipmentSubTypeList.GetItem(cp.TripodEquipmentSubTypeID).EquipmentSubType
          End If

          If Not IsNullNothing(cp.EVSPositionID) Then
            drw(6) = ROPositionList.GetItem(cp.EVSPositionID).ToString 'CommonData.Lists.PositionList.GetItem(cp.EVSPositionID).ToString
          End If
          drw(7) = cp.Notes
          'drw(8) = cp.PositionCode
          drw(8) = ROPositionList.GetItem(cp.PositionID).ToString() 'CommonData.Lists.PositionList.GetItem(cp.PositionID).ToString()


          drw(9) = GetProperty(PositionLayoutTypeIDProperty)
          ds.Tables("PositionLayoutPositionList").Rows.Add(drw)
        End If

      Next

      For Each cp As PositionLayouts.PositionLayoutPosition In Me.PositionLayoutPositionList
        For Each csp As PositionLayouts.PositionLayoutSubPosition In cp.PositionLayoutSubPositionList

          If csp.ShowInd Then
            drw = ds.Tables("PositionLayoutSubPositionList").NewRow
            drw(0) = csp.PositionLayoutSubPositionID
            drw(1) = csp.PositionLayoutPositionID

            If Not IsNullNothing(csp.TripodEquipmentSubTypeID) Then
              drw(2) = ROEquipmentSubTypeList.GetItem(csp.TripodEquipmentSubTypeID).EquipmentSubType 'CommonData.Lists.ROEquipmentSubTypeList.GetItem(csp.TripodEquipmentSubTypeID).EquipmentSubType
            End If

            drw(3) = csp.Notes
            drw(4) = csp.PositionCode
            ds.Tables("PositionLayoutSubPositionList").Rows.Add(drw)
          End If

        Next
      Next

      Return ds

    End Function

    Private Function BmpToBytes(ByVal bmp As Bitmap) As Byte()

      Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()

      bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
      Dim bmpBytes As Byte() = ms.GetBuffer()
      bmp.Dispose()
      ms.Close()

      Return bmpBytes

    End Function

    Public Function GetImage() As Image

      Try

        'Dim width As Integer = 1024
        'Dim height As Integer = 768

        Dim width As Integer = 1188
        Dim height As Integer = 840
        Dim XMag As Double = width / ROProductionVenueImage().VenueImage.Width * 100
        Dim YMag As Double = height / ROProductionVenueImage().VenueImage.Height * 100

        Dim g As Graphics
        Dim PaintImage As New Bitmap(width, height)
        g = Graphics.FromImage(PaintImage)
        g.Clear(Color.White)
        Dim Rec As New Drawing.Rectangle(0, 0, width, height)
        g.DrawImage(Img, Rec)
        For Each t As PositionLayoutPosition In GetProperty(PositionLayoutPositionListProperty)
          t.Draw(PaintImage, XMag, YMag, GetProperty(PositionLayoutTypeIDProperty))
          For Each subpos As PositionLayoutSubPosition In t.PositionLayoutSubPositionList
            subpos.Draw(PaintImage, XMag, YMag, GetProperty(PositionLayoutTypeIDProperty))
          Next
        Next

        Return PaintImage

      Catch ex As Exception
        Return Nothing
      End Try

    End Function

    Public Sub SetupNewPositionLayoutFromTemplate(ByVal ProductionID As Integer?, ProductionName As String, ByVal PositionLayoutTemplate As PositionLayout)

      Me.ProductionID = ProductionID
      Me.LayoutName = ProductionName
      Me.ProductionVenueImageID = PositionLayoutTemplate.ProductionVenueImageID
      For Each childTemplate As PositionLayoutPosition In PositionLayoutTemplate.PositionLayoutPositionList
        Dim newItem As PositionLayoutPosition = PositionLayoutPosition.NewPositionLayoutPosition(childTemplate)
        Me.PositionLayoutPositionList.Add(newItem)
      Next
      Me.CheckRules()

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPositionLayout() method.

    End Sub

    Public Shared Function NewPositionLayout() As PositionLayout

      Return DataPortal.CreateChild(Of PositionLayout)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetPositionLayout(dr As SafeDataReader) As PositionLayout

      Dim p As New PositionLayout()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(PositionLayoutIDProperty, .GetInt32(0))
          LoadProperty(LayoutNameProperty, .GetString(1))
          LoadProperty(ProductionVenueImageIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(PositionLayoutTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          ProductionVenueImageName = ROProductionVenueImage.ImageName
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insPositionLayout"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updPositionLayout"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramPositionLayoutID As SqlParameter = .Parameters.Add("@PositionLayoutID", SqlDbType.Int)
          paramPositionLayoutID.Value = GetProperty(PositionLayoutIDProperty)
          If Me.IsNew Then
            paramPositionLayoutID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@LayoutName", GetProperty(LayoutNameProperty))
          .Parameters.AddWithValue("@ProductionVenueImageID", GetProperty(ProductionVenueImageIDProperty))
          .Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(GetProperty(ProductionIDProperty)))
          .Parameters.AddWithValue("@PositionLayoutTypeID", GetProperty(PositionLayoutTypeIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(PositionLayoutIDProperty, paramPositionLayoutID.Value)
          End If
          ' update child objects
          If GetProperty(PositionLayoutPositionListProperty) IsNot Nothing Then
            Me.PositionLayoutPositionList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(PositionLayoutPositionListProperty) IsNot Nothing Then
          Me.PositionLayoutPositionList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delPositionLayout"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@PositionLayoutID", GetProperty(PositionLayoutIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace