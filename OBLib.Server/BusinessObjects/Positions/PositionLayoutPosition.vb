﻿' Generated 02 Jun 2014 21:30 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace PositionLayouts

  <Serializable()> _
  Public Class PositionLayoutPosition
    Inherits Pos(Of PositionLayoutPosition)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PositionLayoutPositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionLayoutPositionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property PositionLayoutPositionID() As Integer
      Get
        Return GetProperty(PositionLayoutPositionIDProperty)
      End Get
    End Property

    Public Shared PositionLayoutIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionLayoutID, "Position Layout", Nothing)
    ''' <summary>
    ''' Gets the Position Layout value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property PositionLayoutID() As Integer?
      Get
        Return GetProperty(PositionLayoutIDProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position", Nothing)
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="The camera position on the layout"),
    Required(ErrorMessage:="Position required"),
    Singular.DataAnnotations.DropDownWeb("ClientData.TypePositionlist", Source:=Singular.DataAnnotations.DropDownWeb.SourceType.None, DisplayMember:="Position", ValueMember:="PositionID")>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared MainEquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.MainEquipmentSubTypeID, "Main Equipment Sub Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Main Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Equipment", Description:="The camera that will be used at this position"),
    Singular.DataAnnotations.DropDownWeb("ClientData.MainEquipmentSubType", Source:=Singular.DataAnnotations.DropDownWeb.SourceType.None, DisplayMember:="EquipmentSubType", ValueMember:="EquipmentSubTypeID")>
    Public Property MainEquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(MainEquipmentSubTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(MainEquipmentSubTypeIDProperty, Value)
      End Set
    End Property

    Public Shared LensEquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LensEquipmentSubTypeID, "Lens Equipment Sub Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Lens Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Lens Equipment Sub Type", Description:="The lens that will be used at this position"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROEquipmentSubTypeList), ThisFilterMember:="EquipmentTypeID", FilterConstant:=CommonData.Enums.EquipmentType.Lens)>
    Public Property LensEquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(LensEquipmentSubTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LensEquipmentSubTypeIDProperty, Value)
      End Set
    End Property

    Public Shared TripodEquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TripodEquipmentSubTypeID, "Tripod Equipment Sub Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Tripod Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Tripod Equipment Sub Type", Description:="The tripod that will be used at this position"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROEquipmentSubTypeList), ThisFilterMember:="EquipmentTypeID", FilterConstant:=CommonData.Enums.EquipmentType.CameraSupports)>
    Public Property TripodEquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(TripodEquipmentSubTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TripodEquipmentSubTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EVSPositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EVSPositionID, "EVS Position", Nothing)
    ''' <summary>
    ''' Gets and sets the EVS Position value
    ''' </summary>
    <Display(Name:="EVS Position", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROPositionList), ThisFilterMember:="DisciplineID", FilterConstant:=CommonData.Enums.Discipline.EVSOperator)>
    Public Property EVSPositionID() As Integer?
      Get
        Return GetProperty(EVSPositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EVSPositionIDProperty, Value)
      End Set
    End Property

    Public Shared NotesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Notes, "Notes", "")
    ''' <summary>
    ''' Gets and sets the Notes value
    ''' </summary>
    <Display(Name:="Notes", Description:="Additional notes for this position"),
    StringLength(200, ErrorMessage:="Notes cannot be more than 200 characters")>
    Public Property Notes() As String
      Get
        Return GetProperty(NotesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotesProperty, Value)
      End Set
    End Property

    Public Shared XPosProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.XPosServer, "X Pos", CDec(0))
    ''' <summary>
    ''' Gets and sets the X Pos value
    ''' </summary>
    <Display(Name:="X Pos", Description:="The X coordinate of the camera on the image"),
    Required(ErrorMessage:="X Pos required")>
    Public Property XPosServer() As Decimal
      Get
        Return GetProperty(XPosProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(XPosProperty, Value)
      End Set
    End Property

    Public Shared YPosProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.YPosServer, "Y Pos", CDec(0))
    ''' <summary>
    ''' Gets and sets the Y Pos value
    ''' </summary>
    <Display(Name:="Y Pos", Description:="The Y coordinate of the camera on the image"),
    Required(ErrorMessage:="Y Pos required")>
    Public Property YPosServer() As Decimal
      Get
        Return GetProperty(YPosProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(YPosProperty, Value)
      End Set
    End Property

    Public Shared RotationProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RotationServer, "Rotation", 0)
    ''' <summary>
    ''' Gets and sets the Rotation value
    ''' </summary>
    <Display(Name:="Rotation", Description:="The rotation of the camera on the image"),
    Required(ErrorMessage:="Rotation required")>
    Public Property RotationServer() As Integer
      Get
        Return GetProperty(RotationProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RotationProperty, Value)
      End Set
    End Property

    Public Shared ShowIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShowInd, "Show", True)
    ''' <summary>
    ''' Gets and sets the Show value
    ''' </summary>
    <Display(Name:="Show", Description:="Indicator if shown on image"),
    Required(ErrorMessage:="Show required")>
    Public Property ShowInd() As Boolean
      Get
        Return GetProperty(ShowIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ShowIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared AngleProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Angle, "Angle", 0)
    ''' <summary>
    ''' Gets and sets the Rotation value
    ''' </summary>
    <Display(Name:="Rotation", Description:="The rotation of the camera on the image"),
    Required(ErrorMessage:="Rotation required")>
    Public Property Angle() As Integer
      Get
        Return GetProperty(RotationProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RotationProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared PositionLayoutSubPositionListProperty As PropertyInfo(Of PositionLayoutSubPositionList) = RegisterProperty(Of PositionLayoutSubPositionList)(Function(c) c.PositionLayoutSubPositionList, "Position Layout Sub Position List")

    Public ReadOnly Property PositionLayoutSubPositionList() As PositionLayoutSubPositionList
      Get
        If GetProperty(PositionLayoutSubPositionListProperty) Is Nothing Then
          LoadProperty(PositionLayoutSubPositionListProperty, PositionLayouts.PositionLayoutSubPositionList.NewPositionLayoutSubPositionList())
        End If
        Return GetProperty(PositionLayoutSubPositionListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As PositionLayout

      Return CType(CType(Me.Parent, PositionLayoutPositionList).Parent, PositionLayout)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PositionLayoutPositionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.PositionID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Position Layout Position")
        Else
          Return String.Format("Blank {0}", "Position Layout Position")
        End If
      Else
        Return Me.PositionID.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"PositionLayoutSubPositions"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      'Me.AddMultiplePropertyRule(AddressOf Singular.CSLALib.Rules.CompareProperties, New Singular.CSLALib.Rules.Args.ComparePropertyArgs("Rotation", "Rotation", "<="), New String() {"Rotation", "Rotation"})

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPositionLayoutPosition() method.

    End Sub

    Public Shared Function NewPositionLayoutPosition() As PositionLayoutPosition

      Return DataPortal.CreateChild(Of PositionLayoutPosition)()

    End Function

    Public Shared Function NewPositionLayoutPosition(ByVal PositionLayoutPositionTemplate As PositionLayoutPosition) As PositionLayoutPosition

      Dim plc As New PositionLayoutPosition()
      plc.PositionID = PositionLayoutPositionTemplate.PositionID
      plc.MainEquipmentSubTypeID = PositionLayoutPositionTemplate.MainEquipmentSubTypeID
      plc.LensEquipmentSubTypeID = PositionLayoutPositionTemplate.LensEquipmentSubTypeID
      plc.TripodEquipmentSubTypeID = PositionLayoutPositionTemplate.TripodEquipmentSubTypeID
      plc.EVSPositionID = PositionLayoutPositionTemplate.EVSPositionID
      plc.Notes = PositionLayoutPositionTemplate.Notes
      plc.Origin = PositionLayoutPositionTemplate.Origin
      plc.Rotation = PositionLayoutPositionTemplate.Rotation
      plc.mPositionCode = PositionLayoutPositionTemplate.mPositionCode
      For Each childTemplate As PositionLayoutSubPosition In PositionLayoutPositionTemplate.PositionLayoutSubPositionList
        Dim newItem As PositionLayoutSubPosition = PositionLayoutSubPosition.NewPositionLayoutSubPosition(plc, childTemplate)
        plc.PositionLayoutSubPositionList.Add(newItem)
      Next
      plc.CheckRules()
      Return plc

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetPositionLayoutPosition(dr As SafeDataReader) As PositionLayoutPosition

      Dim p As New PositionLayoutPosition()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(PositionLayoutPositionIDProperty, .GetInt32(0))
          LoadProperty(PositionLayoutIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(MainEquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(LensEquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(TripodEquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(EVSPositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(NotesProperty, .GetString(7))
          LoadProperty(XPosProperty, .GetDecimal(8))
          LoadProperty(YPosProperty, .GetDecimal(9))
          LoadProperty(RotationProperty, .GetInt32(10))
          LoadProperty(ShowIndProperty, .GetBoolean(11))
          LoadProperty(CreatedByProperty, .GetInt32(12))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(13))
          LoadProperty(ModifiedByProperty, .GetInt32(14))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(15))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insPositionLayoutPosition"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updPositionLayoutPosition"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramPositionLayoutPositionID As SqlParameter = .Parameters.Add("@PositionLayoutPositionID", SqlDbType.Int)
          paramPositionLayoutPositionID.Value = GetProperty(PositionLayoutPositionIDProperty)
          If Me.IsNew Then
            paramPositionLayoutPositionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@PositionLayoutID", Me.GetParent().PositionLayoutID)
          .Parameters.AddWithValue("@PositionID", GetProperty(PositionIDProperty))
          .Parameters.AddWithValue("@MainEquipmentSubTypeID", Singular.Misc.NothingDBNull(GetProperty(MainEquipmentSubTypeIDProperty)))
          .Parameters.AddWithValue("@LensEquipmentSubTypeID", Singular.Misc.NothingDBNull(GetProperty(LensEquipmentSubTypeIDProperty)))
          .Parameters.AddWithValue("@TripodEquipmentSubTypeID", Singular.Misc.NothingDBNull(GetProperty(TripodEquipmentSubTypeIDProperty)))
          .Parameters.AddWithValue("@EVSPositionID", Singular.Misc.NothingDBNull(GetProperty(EVSPositionIDProperty)))
          .Parameters.AddWithValue("@Notes", GetProperty(NotesProperty))
          .Parameters.AddWithValue("@XPos", GetProperty(XPosProperty))
          .Parameters.AddWithValue("@YPos", GetProperty(YPosProperty))
          .Parameters.AddWithValue("@Rotation", GetProperty(RotationProperty))
          .Parameters.AddWithValue("@ShowInd", GetProperty(ShowIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(PositionLayoutPositionIDProperty, paramPositionLayoutPositionID.Value)
          End If
          ' update child objects
          If GetProperty(PositionLayoutSubPositionListProperty) IsNot Nothing Then
            Me.PositionLayoutSubPositionList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(PositionLayoutSubPositionListProperty) IsNot Nothing Then
          Me.PositionLayoutSubPositionList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delPositionLayoutPosition"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@PositionLayoutPositionID", GetProperty(PositionLayoutPositionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace