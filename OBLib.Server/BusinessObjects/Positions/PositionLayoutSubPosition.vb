﻿' Generated 02 Jun 2014 21:30 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace PositionLayouts

  <Serializable()> _
  Public Class PositionLayoutSubPosition
    Inherits Pos(Of PositionLayoutSubPosition)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PositionLayoutSubPositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionLayoutSubPositionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property PositionLayoutSubPositionID() As Integer
      Get
        Return GetProperty(PositionLayoutSubPositionIDProperty)
      End Get
    End Property

    Public Shared PositionLayoutPositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionLayoutPositionID, "Position Layout Position", Nothing)
    ''' <summary>
    ''' Gets the Position Layout Position value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property PositionLayoutPositionID() As Integer?
      Get
        Return GetProperty(PositionLayoutPositionIDProperty)
      End Get
    End Property

    Public Shared PositionCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionCode, "Position Code", "")
    ''' <summary>
    ''' Gets and sets the Position Code value
    ''' </summary>
    <Display(Name:="Position Code", Description:="One character code used to identify this sub position on the image"),
    Required(ErrorMessage:="Position Code required"),
    StringLength(1, ErrorMessage:="Position Code cannot be more than 1 characters")>
    Public Property PositionCode() As String
      Get
        Return GetProperty(PositionCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionCodeProperty, Value)
      End Set
    End Property

    Public Shared NotesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Notes, "Notes", "")
    ''' <summary>
    ''' Gets and sets the Notes value
    ''' </summary>
    <Display(Name:="Notes", Description:="Additional notes for this sub position"),
    StringLength(200, ErrorMessage:="Notes cannot be more than 200 characters")>
    Public Property Notes() As String
      Get
        Return GetProperty(NotesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotesProperty, Value)
      End Set
    End Property

    Public Shared TripodEquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TripodEquipmentSubTypeID, "Tripod Equipment Sub Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Tripod Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Tripod Equipment Sub Type", Description:="The specific tripod equipment that will be used at this position"),
       Singular.DataAnnotations.DropDownWeb(GetType(ROEquipmentSubTypeList))>
    Public Property TripodEquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(TripodEquipmentSubTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TripodEquipmentSubTypeIDProperty, Value)
      End Set
    End Property

    Public Shared XPosProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.XPos, "X Pos", 0)
    ''' <summary>
    ''' Gets and sets the X Pos value
    ''' </summary>
    <Display(Name:="X Pos", Description:="The X coordinate of the camera on the image"),
    Required(ErrorMessage:="X Pos required")>
    Public Property XPos() As Decimal
      Get
        Return GetProperty(XPosProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(XPosProperty, Value)
      End Set
    End Property

    Public Shared YPosProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.YPos, "Y Pos", 0)
    ''' <summary>
    ''' Gets and sets the Y Pos value
    ''' </summary>
    <Display(Name:="Y Pos", Description:="The Y coordinate of the camera on the image"),
    Required(ErrorMessage:="Y Pos required")>
    Public Property YPos() As Decimal
      Get
        Return GetProperty(YPosProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(YPosProperty, Value)
      End Set
    End Property

    Public Shared RotationProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Rotation, "Rotation", 0)
    ''' <summary>
    ''' Gets and sets the Rotation value
    ''' </summary>
    <Display(Name:="Rotation", Description:="The rotation of the camera on the image"),
    Required(ErrorMessage:="Rotation required")>
    Public Property Rotation() As Integer
      Get
        Return GetProperty(RotationProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RotationProperty, Value)
      End Set
    End Property

    Public Shared ShowIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShowInd, "Show", True)
    ''' <summary>
    ''' Gets and sets the Show value
    ''' </summary>
    <Display(Name:="Show", Description:="Indicator if shown on image"),
    Required(ErrorMessage:="Show required")>
    Public Property ShowInd() As Boolean
      Get
        Return GetProperty(ShowIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ShowIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As PositionLayoutPosition

      Return CType(CType(Me.Parent, PositionLayoutSubPositionList).Parent, PositionLayoutPosition)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PositionLayoutSubPositionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.PositionCode.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Position Layout Sub Position")
        Else
          Return String.Format("Blank {0}", "Position Layout Sub Position")
        End If
      Else
        Return Me.PositionCode
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPositionLayoutSubPosition() method.

    End Sub

    Public Shared Function NewPositionLayoutSubPosition() As PositionLayoutSubPosition

      Return DataPortal.CreateChild(Of PositionLayoutSubPosition)()

    End Function

    Friend Shared Function NewPositionLayoutSubPosition(ByVal Parent As PositionLayoutPosition, ByVal PositionLayoutSubPositionTemplate As PositionLayoutSubPosition) As PositionLayoutSubPosition

      Dim plcs As New PositionLayoutSubPosition()
      plcs.Notes = PositionLayoutSubPositionTemplate.Notes
      plcs.TripodEquipmentSubTypeID = PositionLayoutSubPositionTemplate.TripodEquipmentSubTypeID
      plcs.Origin = PositionLayoutSubPositionTemplate.Origin
      plcs.Rotation = PositionLayoutSubPositionTemplate.Rotation
      plcs.PositionCode = PositionLayoutSubPositionTemplate.PositionCode
      plcs.PositionCode = PositionLayoutSubPositionTemplate.PositionCode
      plcs.CheckRules()
      Return plcs

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetPositionLayoutSubPosition(dr As SafeDataReader) As PositionLayoutSubPosition

      Dim p As New PositionLayoutSubPosition()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(PositionLayoutSubPositionIDProperty, .GetInt32(0))
          LoadProperty(PositionLayoutPositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(PositionCodeProperty, .GetString(2))
          LoadProperty(NotesProperty, .GetString(3))
          LoadProperty(TripodEquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(XPosProperty, .GetDecimal(5))
          LoadProperty(YPosProperty, .GetDecimal(6))
          LoadProperty(RotationProperty, .GetInt32(7))
          LoadProperty(ShowIndProperty, .GetBoolean(8))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ModifiedByProperty, .GetInt32(11))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insPositionLayoutSubPosition"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updPositionLayoutSubPosition"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramPositionLayoutSubPositionID As SqlParameter = .Parameters.Add("@PositionLayoutSubPositionID", SqlDbType.Int)
          paramPositionLayoutSubPositionID.Value = GetProperty(PositionLayoutSubPositionIDProperty)
          If Me.IsNew Then
            paramPositionLayoutSubPositionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@PositionLayoutPositionID", Me.GetParent().PositionLayoutPositionID)
          .Parameters.AddWithValue("@PositionCode", GetProperty(PositionCodeProperty))
          .Parameters.AddWithValue("@Notes", GetProperty(NotesProperty))
          .Parameters.AddWithValue("@TripodEquipmentSubTypeID", Singular.Misc.NothingDBNull(GetProperty(TripodEquipmentSubTypeIDProperty)))
          .Parameters.AddWithValue("@XPos", GetProperty(XPosProperty))
          .Parameters.AddWithValue("@YPos", GetProperty(YPosProperty))
          .Parameters.AddWithValue("@Rotation", GetProperty(RotationProperty))
          .Parameters.AddWithValue("@ShowInd", GetProperty(ShowIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(PositionLayoutSubPositionIDProperty, paramPositionLayoutSubPositionID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delPositionLayoutSubPosition"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@PositionLayoutSubPositionID", GetProperty(PositionLayoutSubPositionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace