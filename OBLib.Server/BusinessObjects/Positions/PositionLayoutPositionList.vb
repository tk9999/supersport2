﻿' Generated 02 Jun 2014 21:30 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace PositionLayouts

  <Serializable()> _
  Public Class PositionLayoutPositionList
    Inherits OBBusinessListBase(Of PositionLayoutPositionList, PositionLayoutPosition)

#Region " Business Methods "

    Public Function GetItem(PositionLayoutPositionID As Integer) As PositionLayoutPosition

      For Each child As PositionLayoutPosition In Me
        If child.PositionLayoutPositionID = PositionLayoutPositionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Position Layout Positions"

    End Function

    Public Function GetPositionLayoutSubPosition(PositionLayoutSubPositionID As Integer) As PositionLayoutSubPosition

      Dim obj As PositionLayoutSubPosition = Nothing
      For Each parent As PositionLayoutPosition In Me
        obj = parent.PositionLayoutSubPositionList.GetItem(PositionLayoutSubPositionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewPositionLayoutPositionList() As PositionLayoutPositionList

      Return New PositionLayoutPositionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As PositionLayoutPosition In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As PositionLayoutPosition In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace