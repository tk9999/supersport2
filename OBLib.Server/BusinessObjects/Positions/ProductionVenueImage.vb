﻿' Generated 08 Apr 2015 12:07 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.IO

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports OBLib.Maintenance.Productions.ReadOnly.Old

Namespace ProductionLayouts

  <Serializable()> _
  Public Class ProductionVenueImage
    Inherits OBBusinessBase(Of ProductionVenueImage)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVenueImageIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueImageID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionVenueImageID() As Integer
      Get
        Return GetProperty(ProductionVenueImageIDProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="The production type that this image is for"),
    Required(ErrorMessage:="Production Type required"), Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Productions.ReadOnly.Old.ROProductionTypeList), UnselectedText:="Production Type")>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="The Venue that this image corresponds to. If left NULL then this is a template image that can be used for all production of this production type."), Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.Productions.ReadOnly.ROProductionVenueOldList), UnselectedText:="Production Venue")>
    Public Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared VenueImageProperty As PropertyInfo(Of Singular.Documents.TemporaryDocument) = RegisterProperty(Of Singular.Documents.TemporaryDocument)(Function(c) c.VenueImage)
    ''' <summary>
    ''' Gets and sets the Venue Image value
    ''' </summary>

    <Display(Name:="Venue Image", Description:="The image of the venue"),
    Required(ErrorMessage:="Venue Image required")>
    Public Property VenueImage() As Singular.Documents.TemporaryDocument
      Get
        Return GetProperty(VenueImageProperty)
      End Get
      Set(ByVal Value As Singular.Documents.TemporaryDocument)
        SetProperty(VenueImageProperty, Value)
      End Set
    End Property

    Public Shared VenueImageThumbnailProperty As PropertyInfo(Of System.Drawing.Image) = RegisterProperty(Of System.Drawing.Image)(Function(c) c.VenueImageThumbnail)
    ''' <summary>
    ''' Gets and sets the Venue Image Thumbnail value
    ''' </summary>

    <Display(Name:="Venue Image Thumbnail", Description:="")>
    Public Property VenueImageThumbnail() As System.Drawing.Image
      Get
        Return GetProperty(VenueImageThumbnailProperty)
      End Get
      Set(ByVal Value As System.Drawing.Image)
        SetProperty(VenueImageThumbnailProperty, Value)
      End Set
    End Property

    Public Shared ImageNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImageName, "Image Name", "")
    ''' <summary>
    ''' Gets and sets the Image Name value
    ''' </summary>
    <Display(Name:="Image Name", Description:="A name for the image"),
    StringLength(50, ErrorMessage:="Image Name cannot be more than 50 characters")>
    Public Property ImageName() As String
      Get
        Return GetProperty(ImageNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ImageNameProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Sub SetVenueImageFromFile(ByVal FileName As String)

      Dim bmp As System.Drawing.Bitmap = System.Drawing.Bitmap.FromFile(FileName)
      SetProperty(VenueImageProperty, bmp)

    End Sub

    Public Sub SetVenueImageThumbnailFromFile(ByVal FileName As String)

      Dim bmp As System.Drawing.Bitmap = System.Drawing.Bitmap.FromFile(FileName)
      SetProperty(VenueImageThumbnailProperty, bmp)

    End Sub

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVenueImageIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ImageName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Venue Image")
        Else
          Return String.Format("Blank {0}", "Production Venue Image")
        End If
      Else
        Return Me.ImageName
      End If

    End Function

    Public Function byteArrayToImage(ByVal byteArrayIn As Byte()) As System.Drawing.Image
      Using mStream As New MemoryStream(byteArrayIn)
        Return System.Drawing.Image.FromStream(mStream)
      End Using
    End Function

    Public Sub SetThumbnail()

      Dim TempImage As System.Drawing.Image = byteArrayToImage(GetProperty(VenueImageProperty).Document.Document())

      If VenueImageThumbnail IsNot Nothing Then SetProperty(VenueImageThumbnailProperty, Nothing)
      VenueImageThumbnail = Nothing
      If VenueImage IsNot Nothing Then
        Dim width As Integer = 100
        Dim height As Integer = CInt(CDec(width) / CDec(TempImage.Width) * CDec(TempImage.Height))
        Dim del As System.Drawing.Image.GetThumbnailImageAbort = Function() False
        VenueImageThumbnail = TempImage.GetThumbnailImage(width, height, del, System.IntPtr.Zero)
      End If

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionVenueImage() method.

    End Sub

    Public Shared Function NewProductionVenueImage() As ProductionVenueImage

      Return DataPortal.CreateChild(Of ProductionVenueImage)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionVenueImage(dr As SafeDataReader) As ProductionVenueImage

      Dim p As New ProductionVenueImage()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionVenueImageIDProperty, .GetInt32(0))
          LoadProperty(ProductionTypeIDProperty, .GetInt32(1))
          LoadProperty(ProductionVenueIDProperty, .GetInt32(2))
          If Not IsNothing(.GetValue(3)) Then
            LoadProperty(VenueImage, New Singular.Documents.TemporaryDocument(.GetString(5), .GetValue(3)))
          Else
            LoadProperty(VenueImageProperty, New Singular.Documents.TemporaryDocument())
          End If
          If Not IsNothing(.GetValue(4)) Then
            Dim bytes(.GetBytes(4, 0, Nothing, 0, Integer.MaxValue)) As Byte
            .GetBytes(4, 0, bytes, 0, Integer.MaxValue)
            LoadProperty(VenueImageThumbnail, System.Drawing.Image.FromStream(New IO.MemoryStream(bytes)))
          End If
          LoadProperty(ImageNameProperty, .GetString(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionVenueImage"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionVenueImage"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        If Not IsNothing(GetProperty(VenueImageProperty)) AndAlso IsNothing(GetProperty(VenueImageThumbnailProperty)) Then
          Me.SetThumbnail()
        End If

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionVenueImageID As SqlParameter = .Parameters.Add("@ProductionVenueImageID", SqlDbType.Int)
          paramProductionVenueImageID.Value = GetProperty(ProductionVenueImageIDProperty)
          If Me.IsNew Then
            paramProductionVenueImageID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionTypeID", GetProperty(ProductionTypeIDProperty))
          .Parameters.AddWithValue("@ProductionVenueID", Singular.Misc.NothingDBNull(GetProperty(ProductionVenueIDProperty)))
          .Parameters.AddWithValue("@VenueImage", GetProperty(VenueImageProperty).Document.Document)

          Dim ms = New MemoryStream()
          GetProperty(VenueImageThumbnailProperty).Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg) ' Use appropriate format here
          Dim bytes = ms.ToArray()
          .Parameters.AddWithValue("@VenueImageThumbnail", bytes)
          .Parameters.AddWithValue("@ImageName", GetProperty(VenueImageProperty).Document.DocumentName)
          .Parameters.AddWithValue("@ModifiedBy", Security.Settings.CurrentUser.UserID)
          .ExecuteNonQuery()
          If Me.IsNew Then
            LoadProperty(ProductionVenueImageIDProperty, paramProductionVenueImageID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionVenueImage"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionVenueImageID", GetProperty(ProductionVenueImageIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace