﻿' Generated 02 Jun 2014 21:30 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace PositionLayouts

  <Serializable()> _
  Public Class PositionLayoutList
    Inherits OBBusinessListBase(Of PositionLayoutList, PositionLayout)

#Region " Business Methods "

    Public Function GetItem(PositionLayoutID As Integer) As PositionLayout

      For Each child As PositionLayout In Me
        If child.PositionLayoutID = PositionLayoutID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Position Layouts"

    End Function

    Public Function GetPositionLayoutPosition(PositionLayoutPositionID As Integer) As PositionLayoutPosition

      Dim obj As PositionLayoutPosition = Nothing
      For Each parent As PositionLayout In Me
        obj = parent.PositionLayoutPositionList.GetItem(PositionLayoutPositionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public ProductionID As Integer? = Nothing
      Public Property PositionLayoutID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(ProductionID As Integer?, PositionLayoutID As Integer?)

        Me.ProductionID = ProductionID
        Me.PositionLayoutID = PositionLayoutID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewPositionLayoutList() As PositionLayoutList

      Return New PositionLayoutList()

    End Function

    Public Shared Sub BeginGetPositionLayoutList(CallBack As EventHandler(Of DataPortalResult(Of PositionLayoutList)))

      Dim dp As New DataPortal(Of PositionLayoutList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetPositionLayoutList() As PositionLayoutList

      Return DataPortal.Fetch(Of PositionLayoutList)(New Criteria())

    End Function

    Public Shared Function GetPositionLayoutList(ProductionID As Integer?, Optional PositionLayoutID As Integer? = Nothing) As PositionLayoutList

      Return DataPortal.Fetch(Of PositionLayoutList)(New Criteria(ProductionID, PositionLayoutID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(PositionLayout.GetPositionLayout(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As PositionLayout = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.PositionLayoutID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.PositionLayoutPositionList.RaiseListChangedEvents = False
          parent.PositionLayoutPositionList.Add(PositionLayoutPosition.GetPositionLayoutPosition(sdr))
          parent.PositionLayoutPositionList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As PositionLayoutPosition = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.PositionLayoutPositionID <> sdr.GetInt32(1) Then
            parentChild = Me.GetPositionLayoutPosition(sdr.GetInt32(1))
          End If
          parentChild.PositionLayoutSubPositionList.RaiseListChangedEvents = False
          parentChild.PositionLayoutSubPositionList.Add(PositionLayoutSubPosition.GetPositionLayoutSubPosition(sdr))
          parentChild.PositionLayoutSubPositionList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As PositionLayout In Me
        child.CheckRules()
        For Each PositionLayoutPosition As PositionLayoutPosition In child.PositionLayoutPositionList
          PositionLayoutPosition.CheckRules()

          For Each PositionLayoutSubPosition As PositionLayoutSubPosition In PositionLayoutPosition.PositionLayoutSubPositionList
            PositionLayoutSubPosition.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getPositionLayoutList"
            cm.Parameters.AddWithValue("@ProductionID", ZeroNothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@PositionLayoutID", ZeroNothingDBNull(crit.PositionLayoutID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As PositionLayout In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As PositionLayout In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace