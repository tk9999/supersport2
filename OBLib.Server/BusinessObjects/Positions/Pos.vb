﻿Imports System.ComponentModel
Imports Csla
Imports Csla.Data
Imports Csla.Core
Imports System.Drawing
Imports OBLib.Security

Namespace PositionLayouts

  Public Interface IPos

    'Function MouseIn(ByVal XMag As Double, ByVal YMag As Double, ByVal e As System.Windows.Forms.MouseEventArgs) As Boolean
    'Sub Move(ByVal e As System.Windows.Forms.MouseEventArgs, ByVal XMag As Double, ByVal YMag As Double, ByVal MaxX As Double, ByVal MaxY As Double)
    'Sub Move(ByVal p As Point, ByVal XMag As Double, ByVal YMag As Double, ByVal MaxX As Double, ByVal MaxY As Double)
    'Sub Rotate(ByVal e As System.Windows.Forms.MouseEventArgs)
    'Sub StartRotation(ByVal e As System.Windows.Forms.MouseEventArgs)
    'Sub SetVisibility(ByVal value As Boolean)

  End Interface

  <Serializable()> _
  Public Class Pos(Of C As Pos(Of C))
    Inherits OBBusinessBase(Of C)
    Implements IPos

    Private P1 As Vector
    Private P2 As Vector
    Private P3 As Vector
    Private P4 As Vector
    Protected mPositionCode As String = ""
    Protected mXPos As Decimal = 0
    Protected mYPos As Decimal = 0
    Protected mRotation As Integer = 0
    Protected mShowInd As Boolean = True
    Protected mCreatedBy As Integer = Settings.CurrentUserID
    Protected mCreatedDateTime As SmartDate = New SmartDate(Now)
    Protected mModifiedBy As Integer = Settings.CurrentUserID
    Protected mModifiedDateTime As SmartDate = New SmartDate(Now)

#Region " Properties "

    <DisplayNameAttribute("X Pos")> _
    Public Property XPos() As Decimal
      Get
        Return mXPos
      End Get
      Set(ByVal Value As Decimal)
        If mXPos <> Value Then
          mXPos = Value
          P1.Ori = New Point(mXPos, mYPos)
          P2.Ori = New Point(mXPos, mYPos)
          P3.Ori = New Point(mXPos, mYPos)
          P4.Ori = New Point(mXPos, mYPos)
          If Not Me.IsDirty Then
            PropertyHasChanged("XPos")
          End If
        End If
      End Set
    End Property

    <DisplayNameAttribute("Y Pos")> _
    Public Property YPos() As Decimal
      Get
        Return mYPos
      End Get
      Set(ByVal Value As Decimal)
        If mYPos <> Value Then
          mYPos = Value
          P1.Ori = New Point(mXPos, mYPos)
          P2.Ori = New Point(mXPos, mYPos)
          P3.Ori = New Point(mXPos, mYPos)
          P4.Ori = New Point(mXPos, mYPos)
          If Not Me.IsDirty Then
            PropertyHasChanged("YPos")
          End If
        End If
      End Set
    End Property

    <Browsable(False)> _
    Public Property Origin() As Point
      Get
        Return New Point(mXPos, mYPos)
      End Get
      Set(ByVal Value As Point)
        If New Point(XPos, YPos) <> Value Then
          XPos = Value.X
          YPos = Value.Y
          P1.Ori = New Point(mXPos, mYPos)
          P2.Ori = New Point(mXPos, mYPos)
          P3.Ori = New Point(mXPos, mYPos)
          P4.Ori = New Point(mXPos, mYPos)
          If Not Me.IsDirty Then
            PropertyHasChanged("XPos")
            PropertyHasChanged("YPos")
          End If
        End If
      End Set
    End Property

    <DisplayNameAttribute("Rotation")> _
    Public Property Rotation() As Integer
      Get
        Return mRotation
      End Get
      Set(ByVal Value As Integer)
        If mRotation <> Value Mod 360 Then
          mRotation = Value Mod 360
          'If mRotation Mod 45 <> 0 Then
          '  Dim degdelta = CInt(mRotation / 45)
          '  Dim diff = Math.Abs(degdelta * 45 - mRotation)

          '  If diff < 23 Then
          '    mRotation -= diff
          '  Else
          '    mRotation -= diff + 45
          '  End If
          'End If
          'If mRotation < 45 Then
          '  mRotation = 0
          'ElseIf mRotation < 90 Then
          '  mRotation = 45
          'ElseIf mRotation < 135 Then
          '  mRotation = 90
          'ElseIf mRotation < 180 Then
          '  mRotation = 135
          'ElseIf mRotation < 225 Then
          '  mRotation = 180
          'ElseIf mRotation < 270 Then
          '  mRotation = 225
          'ElseIf mRotation < 315 Then
          '  mRotation = 270
          'ElseIf mRotation < 360 Then
          '  mRotation = 315
          'End If
          Rotate(mRotation * ((Math.PI * 2) / 360) - P4.Xtheta)
          If Not Me.IsDirty Then
            PropertyHasChanged("Rotation")
          End If
        End If
      End Set
    End Property

    <Browsable(False)> _
    Public ReadOnly Property CreatedBy() As Object
      Get
        Return Singular.Misc.ZeroDBNull(mCreatedBy)
      End Get
    End Property

    <Browsable(False)> _
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return mCreatedDateTime
      End Get
    End Property

    <Browsable(False)> _
    Public ReadOnly Property ModifiedBy() As Object
      Get
        Return Singular.Misc.ZeroDBNull(mModifiedBy)
      End Get
    End Property

    <Browsable(False)> _
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return mModifiedDateTime
      End Get
    End Property

    <DisplayNameAttribute("Position Code")> _
    Public ReadOnly Property PositionCode() As String
      Get
        Return (mPositionCode)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Sub Rotate(ByVal deg As Decimal)
      P1.MoveLeft(deg)
      P2.MoveLeft(deg)
      P3.MoveLeft(deg)
      P4.MoveLeft(deg)
    End Sub

    Public Sub Draw(ByRef img As Bitmap, ByVal XMag As Double, ByVal YMag As Double, ByVal PositionLayoutType As CommonData.Enums.PositionLayoutType)

      Try

        If Me.mShowInd Then

          Dim g As Graphics
          g = Graphics.FromImage(img)

          Dim hb As New System.Drawing.Drawing2D.HatchBrush(System.Drawing.Drawing2D.HatchStyle.Plaid, Color.Red, Color.Blue)


          Dim pen As New Pen(hb)
          g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias

          'Dim points() As Point = {P1.getPoint(XMag, YMag), P2.getPoint(XMag, YMag), P3.getPoint(XMag, YMag)}
          'g.FillPolygon(New SolidBrush(Color.FromArgb(100, 1, 36, 165)), points)

          'points = {P1.getPoint(XMag, YMag, 0.9), P2.getPoint(XMag, YMag, 0.9), P3.getPoint(XMag, YMag, 0.9)}
          'g.FillPolygon(New SolidBrush(Color.FromArgb(50, 255, 255, 255)), points)

          'points = {P1.getPoint(XMag, YMag, 0.8), P2.getPoint(XMag, YMag, 0.8), P3.getPoint(XMag, YMag, 0.8)}
          'g.FillPolygon(New SolidBrush(Color.FromArgb(50, 255, 255, 255)), points)

          'points = {P1.getPoint(XMag, YMag, 0.7), P2.getPoint(XMag, YMag, 0.7), P3.getPoint(XMag, YMag, 0.7)}
          'g.FillPolygon(New SolidBrush(Color.FromArgb(50, 255, 255, 255)), points)

          'points = {P1.getPoint(XMag, YMag, 0.6), P2.getPoint(XMag, YMag, 0.6), P3.getPoint(XMag, YMag, 0.6)}
          'g.FillPolygon(New SolidBrush(Color.FromArgb(50, 255, 255, 255)), points)

          'points = {P1.getPoint(XMag, YMag, 0.5), P2.getPoint(XMag, YMag, 0.5), P3.getPoint(XMag, YMag, 0.5)}
          'g.FillPolygon(New SolidBrush(Color.FromArgb(50, 255, 255, 255)), points)

          'g.DrawLine(Pens.Black, P1.getPoint(XMag, YMag), P2.getPoint(XMag, YMag))
          'g.DrawLine(Pens.Black, P1.getPoint(XMag, YMag), P3.getPoint(XMag, YMag))
          'g.DrawLine(Pens.Black, P2.getPoint(XMag, YMag), P3.getPoint(XMag, YMag))

          Dim points1() As Point = {P2.getPoint(XMag, YMag), P3.getPoint(XMag, YMag), P1.getPoint(XMag, YMag)}

          If PositionLayoutType = CommonData.Enums.PositionLayoutType.Camera Then
            g.DrawImage(My.Resources.CameraImageSmall, points1)
          Else
            g.DrawImage(My.Resources.MicImageSmall, points1)
          End If

          Dim FontSize As Single = CSng(20 * ((XMag + YMag) / 2) / 100)
          'Dim FontXPos As Integer = (Me.Origin.X - Me.PositionCode.Length * 8.5) * XMag / 100
          'Dim FontYPos As Integer = (Me.Origin.Y - 11.5) * YMag / 100
          Dim FontXPos As Integer = (Me.Origin.X - Me.PositionCode.Length * 8.5) * XMag / 100
          Dim FontYPos As Integer = (Me.Origin.Y - 19) * YMag / 100

          g.DrawString(Me.PositionCode, New System.Drawing.Font("Calibri", FontSize, FontStyle.Bold), Brushes.Black, New Point(FontXPos, FontYPos))

        End If

      Catch ex As Exception

      End Try

    End Sub

    'Public Function MouseIn(ByVal XMag As Double, ByVal YMag As Double, ByVal e As System.Windows.Forms.MouseEventArgs) As Boolean Implements IPos.MouseIn

    '  If Math.Abs((Me.Origin.X * XMag / 100) - e.X) <= (30 * XMag / 100) AndAlso Math.Abs((Me.Origin.Y * YMag / 100) - e.Y) <= (30 * YMag / 100) Then
    '    Return True
    '  Else
    '    Return False
    '  End If

    'End Function

    'Public Sub Move(ByVal e As System.Windows.Forms.MouseEventArgs, ByVal XMag As Double, ByVal YMag As Double, ByVal MaxX As Double, ByVal MaxY As Double) Implements IPos.Move

    '  Try
    '    Dim tempX As Double = e.X
    '    Dim tempY As Double = e.Y

    '    If tempX < 0 Then
    '      tempX = 0
    '    ElseIf tempX > MaxX Then
    '      tempX = MaxX
    '    End If

    '    If tempY < 0 Then
    '      tempY = 0
    '    ElseIf tempY > MaxY Then
    '      tempY = MaxY
    '    End If

    '    Me.Origin = New Point(tempX / XMag * 100, tempY / YMag * 100)
    '  Catch ex As Exception

    '  End Try

    'End Sub

    'Public Sub Move(ByVal p As Point, ByVal XMag As Double, ByVal YMag As Double, ByVal MaxX As Double, ByVal MaxY As Double) Implements IPos.Move

    '  Try
    '    Dim tempX As Double = p.X
    '    Dim tempY As Double = p.Y

    '    If tempX < 0 Then
    '      tempX = 0
    '    ElseIf tempX > MaxX Then
    '      tempX = MaxX
    '    End If

    '    If tempY < 0 Then
    '      tempY = 0
    '    ElseIf tempY > MaxY Then
    '      tempY = MaxY
    '    End If

    '    Me.Origin = New Point(tempX / XMag * 100, tempY / YMag * 100)
    '  Catch ex As Exception

    '  End Try

    'End Sub

    'Private mStartRotation As Integer = 0
    'Private mStartRotationMousePos As Point

    'Public Sub StartRotation(ByVal e As System.Windows.Forms.MouseEventArgs) Implements IPos.StartRotation

    '  mStartRotation = Me.Rotation
    '  mStartRotationMousePos = e.Location

    'End Sub

    'Public Sub SetVisibility(ByVal value As Boolean) Implements IPos.SetVisibility
    '  mShowInd = value
    'End Sub

    'Public Sub Rotate(ByVal e As System.Windows.Forms.MouseEventArgs) Implements IPos.Rotate

    '  Try
    '    Dim x As Decimal = e.X - mStartRotationMousePos.X
    '    Dim y As Decimal = e.Y - mStartRotationMousePos.Y
    '    Dim Rotation As Decimal = 0.0
    '    If x = 0 AndAlso y > 0 Then
    '      Rotation = Math.PI / 2
    '    ElseIf x = 0 AndAlso y < 0 Then
    '      Rotation = -Math.PI / 2
    '    ElseIf x = 0 AndAlso y = 0 Then
    '      Rotation = 0.0
    '    Else
    '      Rotation = Math.Atan(y / x)
    '      If x < 0 AndAlso y >= 0 Then
    '        Rotation = Rotation + Math.PI
    '      ElseIf x < 0 And y < 0 Then
    '        Rotation = Rotation - Math.PI
    '      End If
    '    End If
    '    'Me.Rotation = Math.Round((Rotation * 180 / Math.PI), 0) Mod 360

    '    Dim Rot = Math.Round((Rotation * 180 / Math.PI), 0) Mod 360

    '    If Rot < 0 Then
    '      Rot = 360 + Rot
    '    End If

    '    If Rot > 22 AndAlso Rot < 67 Then
    '      Rot = 45
    '    ElseIf Rot > 67 AndAlso Rot < 112 Then
    '      Rot = 90
    '    ElseIf Rot > 112 AndAlso Rot < 157 Then
    '      Rot = 135
    '    ElseIf Rot > 157 AndAlso Rot < 202 Then
    '      Rot = 180
    '    ElseIf Rot > 202 AndAlso Rot < 247 Then
    '      Rot = 225
    '    ElseIf Rot > 247 AndAlso Rot < 292 Then
    '      Rot = 270
    '    ElseIf Rot > 292 AndAlso Rot < 337 Then
    '      Rot = 315
    '    ElseIf (Rot > 337 AndAlso Rot < 360) OrElse (Rot < 22 AndAlso Rot > 0) Then
    '      Rot = 0
    '    End If

    '    Me.Rotation = Rot

    '  Catch ex As Exception
    '  End Try

    'End Sub

#End Region

#Region " Constructor "

    Protected Sub New()

      'P1 = New Vector(20, 20, Origin)
      'P2 = New Vector(-20, 20, Origin)
      'P3 = New Vector(0, -40, Origin)

      'P1 = New Vector(-30, 20, Origin)
      'P2 = New Vector(-30, -40, Origin)
      'P3 = New Vector(30, -40, Origin)
      'P4 = New Vector(0, -40, Origin)

      P1 = New Vector(-30, 30, Origin)
      P2 = New Vector(-30, -30, Origin)
      P3 = New Vector(30, -30, Origin)
      P4 = New Vector(0, -30, Origin)

      XPos = 100
      YPos = 100
      Rotation = 90

    End Sub

#End Region

  End Class

End Namespace
