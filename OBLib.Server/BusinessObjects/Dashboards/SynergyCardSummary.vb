﻿' Generated 23 Oct 2014 11:42 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Dashboards

  <Serializable()> _
  Public Class SynergyCardSummary
    Inherits SingularBusinessBase(Of SynergyCardSummary)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SynergyCardSummaryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SynergyCardSummaryID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key()>
    Public Property SynergyCardSummaryID() As Integer
      Get
        Return GetProperty(SynergyCardSummaryIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SynergyCardSummaryIDProperty, Value)
      End Set
    End Property

    Public Shared NewEventsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NewEvents, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="New Events", Description:="")>
    Public Property NewEvents() As Integer
      Get
        Return GetProperty(NewEventsProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(NewEventsProperty, Value)
      End Set
    End Property

    Public Shared EventsUpdatedProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventsUpdated, "Events Updated")
    ''' <summary>
    ''' Gets and sets the Events Updated value
    ''' </summary>
    <Display(Name:="Events Updated", Description:="")>
    Public Property EventsUpdated() As Integer
      Get
        Return GetProperty(EventsUpdatedProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EventsUpdatedProperty, Value)
      End Set
    End Property

    Public Shared EventsRemovedProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventsRemoved, "Events Removed")
    ''' <summary>
    ''' Gets and sets the Events Removed value
    ''' </summary>
    <Display(Name:="Events Removed", Description:="")>
    Public Property EventsRemoved() As Integer
      Get
        Return GetProperty(EventsRemovedProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EventsRemovedProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(NewEventsProperty)

    End Function

    Public Overrides Function ToString() As String

      'If Me.NewEvents.ToString().Length = 0 Then
      '  If Me.IsNew Then
      '    Return String.Format("New {0}", "Synergy Card Summary")
      '  Else
      '    Return String.Format("Blank {0}", "Synergy Card Summary")
      '  End If
      'Else
      '  Return Me.NewEvents.ToString()
      'End If
      Return "Synergy Event Summary"

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSynergyCardSummary() method.

    End Sub

    Public Shared Function NewSynergyCardSummary() As SynergyCardSummary

      Return DataPortal.CreateChild(Of SynergyCardSummary)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSynergyCardSummary(dr As SafeDataReader) As SynergyCardSummary

      Dim s As New SynergyCardSummary()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(NewEventsProperty, .GetInt32(0))
          LoadProperty(EventsUpdatedProperty, .GetInt32(1))
          LoadProperty(EventsRemovedProperty, .GetInt32(2))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSynergyCardSummary"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSynergyCardSummary"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramNewEvents As SqlParameter = .Parameters.Add("@NewEvents", SqlDbType.Int)
          paramNewEvents.Value = GetProperty(NewEventsProperty)
          If Me.IsNew Then
            paramNewEvents.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EventsUpdated", GetProperty(EventsUpdatedProperty))
          .Parameters.AddWithValue("@EventsRemoved", GetProperty(EventsRemovedProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(NewEventsProperty, paramNewEvents.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSynergyCardSummary"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@NewEvents", GetProperty(NewEventsProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace