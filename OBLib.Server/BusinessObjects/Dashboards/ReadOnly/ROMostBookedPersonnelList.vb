﻿' Generated 19 Jan 2015 19:49 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace HR.ReadOnly

  <Serializable()> _
  Public Class ROMostBookedPersonnelList
    Inherits SingularReadOnlyListBase(Of ROMostBookedPersonnelList, ROMostBookedPersonnel)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROMostBookedPersonnel

      For Each child As ROMostBookedPersonnel In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "R Os"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Start Date", Description:=""),
      Required(ErrorMessage:="Start Date required"),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property StartDate As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate)
      ''' <summary>
      ''' Gets and sets the End Date value
      ''' </summary>
      <Display(Name:="End Date", Description:=""),
      Required(ErrorMessage:="End Date required"),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property EndDate As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", OBLib.Security.Settings.CurrentUser.SystemID)
      ''' <summary>
      ''' Gets and sets the System value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
      Required(ErrorMessage:="Sub-Dept required"),
      DropDownWeb(GetType(ROSystemList))>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ''' <summary>
      ''' Gets and sets the Production Area value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area required"),
      DropDownWeb(GetType(ROProductionAreaList))>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
      ''' <summary>
      ''' Gets and sets the System value
      ''' </summary>
      <Display(Name:="Discipline", Description:=""),
      DropDownWeb(GetType(RODisciplineList))>
      Public Property DisciplineID() As Integer?
        Get
          Return ReadProperty(DisciplineIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(DisciplineIDProperty, Value)
        End Set
      End Property

      Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "Contract Type", Nothing)
      ''' <summary>
      ''' Gets and sets the System value
      ''' </summary>
      <Display(Name:="Contract Type", Description:=""),
      DropDownWeb(GetType(ROContractTypeList))>
      Public Property ContractTypeID() As Integer?
        Get
          Return ReadProperty(ContractTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ContractTypeIDProperty, Value)
        End Set
      End Property

      Public Sub New(StartDate As DateTime?, EndDate As DateTime?,
                     SystemID As Integer?, ProductionAreaID As Integer?,
                     PageNo As Integer, PageSize As Integer,
                     SortAsc As Boolean, SortColumn As String,
                     DisciplineID As Integer?, ContractTypeID As Integer?)
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn
        Me.DisciplineID = DisciplineID
        Me.ContractTypeID = ContractTypeID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROMostBookedPersonnelList() As ROMostBookedPersonnelList

      Return New ROMostBookedPersonnelList()

    End Function

    Public Shared Sub BeginGetROMostBookedPersonnelList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROMostBookedPersonnelList)))

      Dim dp As New DataPortal(Of ROMostBookedPersonnelList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROMostBookedPersonnelList(CallBack As EventHandler(Of DataPortalResult(Of ROMostBookedPersonnelList)))

      Dim dp As New DataPortal(Of ROMostBookedPersonnelList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROMostBookedPersonnelList() As ROMostBookedPersonnelList

      Return DataPortal.Fetch(Of ROMostBookedPersonnelList)(New Criteria())

    End Function

    Public Shared Function GetROMostBookedPersonnelList(StartDate As DateTime?, EndDate As DateTime?,
                                                        SystemID As Integer?, ProductionAreaID As Integer?,
                                                        PageNo As Integer, PageSize As Integer,
                                                        SortAsc As Boolean, SortColumn As String,
                                                        DisciplineID As Integer?, ContractTypeID As Integer?) As ROMostBookedPersonnelList

      Return DataPortal.Fetch(Of ROMostBookedPersonnelList)(New Criteria(StartDate, EndDate,
                                                                         SystemID, ProductionAreaID,
                                                                          PageNo, PageSize,
                                                                          SortAsc, SortColumn,
                                                                          DisciplineID, ContractTypeID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      'mTotalPages = sdr.GetInt32(1)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROMostBookedPersonnel.GetROMostBookedPersonnel(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROMostBookedPersonnelList"
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@ContractTypeID", NothingDBNull(crit.ContractTypeID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace