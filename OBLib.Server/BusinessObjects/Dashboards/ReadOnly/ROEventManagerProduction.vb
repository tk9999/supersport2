﻿' Generated 21 Jul 2014 07:17 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Dashboards.ReadOnly

  <Serializable()> _
  Public Class ROEventManagerProduction
    Inherits SingularReadOnlyBase(Of ROEventManagerProduction)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionRefNo, "Ref Num")
    ''' <summary>
    ''' Gets the Production Ref No value
    ''' </summary>
    <Display(Name:="Ref Num", Description:="")>
    Public ReadOnly Property ProductionRefNo() As String
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared SynergyGenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.SynergyGenRefNo, "Gen Ref", Nothing)
    ''' <summary>
    ''' Gets the Synergy Gen Ref No value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:="")>
    Public ReadOnly Property SynergyGenRefNo() As Int64?
      Get
        Return GetProperty(SynergyGenRefNoProperty)
      End Get
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Description")
    ''' <summary>
    ''' Gets the Production Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
    Public ReadOnly Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
    End Property

    Public Shared TeamsPlayingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamsPlaying, "Teams")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Teams", Description:="")>
    Public ReadOnly Property TeamsPlaying() As String
      Get
        Return GetProperty(TeamsPlayingProperty)
      End Get
    End Property

    Public Shared PlayStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlayStartDateTime, "Start")
    ''' <summary>
    ''' Gets the Play Start Date Time value
    ''' </summary>
    <Display(Name:="Start", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property PlayStartDateTime As DateTime?
      Get
        Return GetProperty(PlayStartDateTimeProperty)
      End Get
    End Property

    Public Shared PlayEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlayEndDateTime, "End")
    ''' <summary>
    ''' Gets the Play End Date Time value
    ''' </summary>
    <Display(Name:="End", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property PlayEndDateTime As DateTime?
      Get
        Return GetProperty(PlayEndDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Production Type")
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "Event Type")
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Venue")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public ReadOnly Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
    End Property

    Public Shared ProductionGradeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionGrade, "Grade")
    ''' <summary>
    ''' Gets the Production Grade value
    ''' </summary>
    <Display(Name:="Grade", Description:="")>
    Public ReadOnly Property ProductionGrade() As String
      Get
        Return GetProperty(ProductionGradeProperty)
      End Get
    End Property

    Public Shared TxStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxStartDateTime, "Tx Start Date Time")
    ''' <summary>
    ''' Gets the Tx Start Date Time value
    ''' </summary>
    <Display(Name:="Tx Start Date Time", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property TxStartDateTime As DateTime?
      Get
        Return GetProperty(TxStartDateTimeProperty)
      End Get
    End Property

    Public Shared TxEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxEndDateTime, "Tx End Date Time")
    ''' <summary>
    ''' Gets the Tx End Date Time value
    ''' </summary>
    <Display(Name:="Tx End Date Time", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property TxEndDateTime As DateTime?
      Get
        Return GetProperty(TxEndDateTimeProperty)
      End Get
    End Property

    Public Shared CrewFinalisedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CrewFinalised, "Crew Finalised", False)
    ''' <summary>
    ''' Gets the Crew Finalised value
    ''' </summary>
    <Display(Name:="Crew Finalised", Description:="")>
    Public ReadOnly Property CrewFinalised() As Boolean
      Get
        Return GetProperty(CrewFinalisedProperty)
      End Get
    End Property

    Public Shared PlanningFinalisedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PlanningFinalised, "Planning Finalised", False)
    ''' <summary>
    ''' Gets the Planning Finalised value
    ''' </summary>
    <Display(Name:="Planning Finalised", Description:="")>
    Public ReadOnly Property PlanningFinalised() As Boolean
      Get
        Return GetProperty(PlanningFinalisedProperty)
      End Get
    End Property

    Public Shared ReconciledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Reconciled, "Reconciled", False)
    ''' <summary>
    ''' Gets the Reconciled value
    ''' </summary>
    <Display(Name:="Reconciled", Description:="")>
    Public ReadOnly Property Reconciled() As Boolean
      Get
        Return GetProperty(ReconciledProperty)
      End Get
    End Property

    Public Shared CancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Cancelled, "Cancelled", False)
    ''' <summary>
    ''' Gets the Cancelled value
    ''' </summary>
    <Display(Name:="Cancelled", Description:="")>
    Public ReadOnly Property Cancelled() As Boolean
      Get
        Return GetProperty(CancelledProperty)
      End Get
    End Property

    <ClientOnly()>
    Public Property Visible() As Boolean = True

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Title

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEventManagerProduction(dr As SafeDataReader) As ROEventManagerProduction

      Dim r As New ROEventManagerProduction()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionIDProperty, .GetInt32(0))
        LoadProperty(ProductionRefNoProperty, .GetString(1))
        LoadProperty(TitleProperty, .GetString(2))
        LoadProperty(SynergyGenRefNoProperty, ZeroNothing(.GetInt64(3)))
        LoadProperty(ProductionDescriptionProperty, .GetString(4))
        LoadProperty(TeamsPlayingProperty, .GetString(5))
        LoadProperty(PlayStartDateTimeProperty, .GetValue(6))
        LoadProperty(PlayEndDateTimeProperty, .GetValue(7))
        LoadProperty(ProductionTypeProperty, .GetString(8))
        LoadProperty(EventTypeProperty, .GetString(9))
        LoadProperty(ProductionVenueProperty, .GetString(10))
        LoadProperty(ProductionGradeProperty, .GetString(11))
        LoadProperty(TxStartDateTimeProperty, .GetValue(12))
        LoadProperty(TxEndDateTimeProperty, .GetValue(13))
        LoadProperty(CrewFinalisedProperty, .GetBoolean(14))
        LoadProperty(PlanningFinalisedProperty, .GetBoolean(15))
        LoadProperty(ReconciledProperty, .GetBoolean(16))
        LoadProperty(CancelledProperty, .GetBoolean(17))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace