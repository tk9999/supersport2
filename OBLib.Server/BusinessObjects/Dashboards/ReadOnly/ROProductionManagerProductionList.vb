﻿' Generated 28 Jul 2014 15:22 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Dashboards.ReadOnly

  <Serializable()> _
  Public Class ROProductionManagerProductionList
    Inherits SingularReadOnlyListBase(Of ROProductionManagerProductionList, ROProductionManagerProduction)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROProductionManagerProduction

      For Each child As ROProductionManagerProduction In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared ProductionManagerHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionManagerHumanResourceID, "PM", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Area value
      ''' </summary>
      <Display(Name:="PM", Description:="")>
      Public Property ProductionManagerHumanResourceID() As Integer?
        Get
          Return ReadProperty(ProductionManagerHumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionManagerHumanResourceIDProperty, Value)
        End Set
      End Property

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Start Date", Description:=""),
      Required(ErrorMessage:="Start Date required"),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property StartDate As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate)
      ''' <summary>
      ''' Gets and sets the End Date value
      ''' </summary>
      <Display(Name:="End Date", Description:=""),
      Required(ErrorMessage:="End Date required"),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property EndDate As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", OBLib.Security.Settings.CurrentUser.SystemID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
      Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area is required")>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Sub New(ProductionManagerHumanResourceID As Integer?, StartDate As DateTime?, EndDate As DateTime?)
        ProductionManagerHumanResourceID = ProductionManagerHumanResourceID
        Me.StartDate = StartDate
        Me.EndDate = EndDate
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionManagerProductionList() As ROProductionManagerProductionList

      Return New ROProductionManagerProductionList()

    End Function

    Public Shared Sub BeginGetROProductionManagerProductionList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionManagerProductionList)))

      Dim dp As New DataPortal(Of ROProductionManagerProductionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionManagerProductionList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionManagerProductionList)))

      Dim dp As New DataPortal(Of ROProductionManagerProductionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionManagerProductionList() As ROProductionManagerProductionList

      Return DataPortal.Fetch(Of ROProductionManagerProductionList)(New Criteria())

    End Function

    Public Shared Function GetROProductionManagerProductionList(ProductionManagerHumanResourceID As Integer?, StartDate As DateTime?, EndDate As DateTime?) As ROProductionManagerProductionList

      Return DataPortal.Fetch(Of ROProductionManagerProductionList)(New Criteria(ProductionManagerHumanResourceID, StartDate, EndDate))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      'mTotalPages = sdr.GetInt32(1)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionManagerProduction.GetROProductionManagerProduction(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionManagerProductionList"
            cm.Parameters.AddWithValue("@ProductionManagerHumanresourceID", NothingDBNull(crit.ProductionManagerHumanResourceID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace