﻿' Generated 23 Feb 2015 00:19 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Dashboards.ReadOnly

  <Serializable()> _
  Public Class ROPositionBookingList
    Inherits SingularReadOnlyListBase(Of ROPositionBookingList, ROPositionBooking)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROPositionBooking

      For Each child As ROPositionBooking In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared TxDateFromProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxDateFrom, "")
      <Display(Name:="Start Date", Description:=""), Required()>
      Public Property TxDateFrom As DateTime?
        Get
          Return ReadProperty(TxDateFromProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(TxDateFromProperty, Value)
        End Set
      End Property

      Public Shared TxDateToProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxDateTo, "")
      <Display(Name:="End Date", Description:=""), Required()>
      Public Property TxDateTo As DateTime?
        Get
          Return ReadProperty(TxDateToProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(TxDateToProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", OBLib.Security.Settings.CurrentUser.SystemID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
      Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area is required")>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Keyword, "")
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="Keyword", Description:="")>
      Public Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Shared PositionIDsProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.PositionIDs, "")
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="PositionIDs", Description:="")>
      Public Property PositionIDs() As String
        Get
          Return ReadProperty(PositionIDsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(PositionIDsProperty, Value)
        End Set
      End Property

      ''<DataSet>
      '		<Table ID="1" />
      '		<Table ID="2" />
      '		<Table ID="3" />
      '</DataSet>'

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROPositionBookingList() As ROPositionBookingList

      Return New ROPositionBookingList()

    End Function

    Public Shared Sub BeginGetROPositionBookingList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROPositionBookingList)))

      Dim dp As New DataPortal(Of ROPositionBookingList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROPositionBookingList(CallBack As EventHandler(Of DataPortalResult(Of ROPositionBookingList)))

      Dim dp As New DataPortal(Of ROPositionBookingList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROPositionBookingList() As ROPositionBookingList

      Return DataPortal.Fetch(Of ROPositionBookingList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPositionBooking.GetROPositionBooking(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROPositionBookingList"
            cm.Parameters.AddWithValue("@TxDateFrom", NothingDBNull(crit.TxDateFrom))
            cm.Parameters.AddWithValue("@TxDateTo", NothingDBNull(crit.TxDateTo))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@Keyword", Singular.Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@PositionIDs", Singular.Strings.MakeEmptyDBNull(crit.PositionIDs))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace