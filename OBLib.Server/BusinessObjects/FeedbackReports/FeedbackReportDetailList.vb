﻿' Generated 12 Nov 2015 08:41 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class FeedbackReportDetailList
    Inherits OBBusinessListBase(Of FeedbackReportDetailList, FeedbackReportDetail)

#Region " Business Methods "

    Public Function GetItem(FeedbackReportDetailID As Integer) As FeedbackReportDetail

      For Each child As FeedbackReportDetail In Me
        If child.FeedbackReportDetailID = FeedbackReportDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Feedback Report Details"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewFeedbackReportDetailList() As FeedbackReportDetailList

      Return New FeedbackReportDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

 

#End Region

  End Class

End Namespace