﻿' Generated 11 Nov 2015 14:01 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportSectionSettingList
    Inherits OBReadOnlyListBase(Of ROFeedbackReportSectionSettingList, ROFeedbackReportSectionSetting)

#Region " Parent "

    <NotUndoable()> Private mParent As ROFeedbackReportSetting
#End Region

#Region " Business Methods "

    Public Function GetItem(FeedbackReportSectionSettingID As Integer) As ROFeedbackReportSectionSetting

      For Each child As ROFeedbackReportSectionSetting In Me
        If child.FeedbackReportSectionSettingID = FeedbackReportSectionSettingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Feedback Report Section Settings"

    End Function

    Public Function GetROFeedbackReportSubSectionSetting(FeedbackReportSubSectionSettingID As Integer) As ROFeedbackReportSubSectionSetting

      Dim obj As ROFeedbackReportSubSectionSetting = Nothing
      For Each parent As ROFeedbackReportSectionSetting In Me
        obj = parent.ROFeedbackReportSubSectionSettingList.GetItem(FeedbackReportSubSectionSettingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewROFeedbackReportSectionSettingList() As ROFeedbackReportSectionSettingList

      Return New ROFeedbackReportSectionSettingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace