﻿' Generated 23 Nov 2016 10:46 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class StudioFeedbackReportList
    Inherits OBBusinessListBase(Of StudioFeedbackReportList, StudioFeedbackReport)

#Region " Business Methods "

    Public Function GetItem(FeedbackReportID As Integer) As StudioFeedbackReport

      For Each child As StudioFeedbackReport In Me
        If child.FeedbackReportID = FeedbackReportID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "StudioFeedbackReport"

    End Function

    Public Function GetStudioFeedbackSectionArea(RoomID As Integer) As StudioFeedbackSectionArea

      Dim obj As StudioFeedbackSectionArea = Nothing
      For Each parent As StudioFeedbackReport In Me
        obj = parent.StudioFeedbackSectionAreaList.GetItem(RoomID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetStudioFeedbackSection(FeedbackReportID As Integer) As StudioFeedbackSection

      Dim obj As StudioFeedbackSection = Nothing

      For Each grandParent As StudioFeedbackReport In Me
        For Each parent As StudioFeedbackSectionArea In grandParent.StudioFeedbackSectionAreaList

          obj = parent.StudioFeedbackSectionList.GetItem(FeedbackReportID)

          If obj IsNot Nothing Then
            Return obj
          End If
        Next
      Next

      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)
      Public Property ReportDate As Date
      ' Public Property HumanResourceID As Object
      Public Sub New()


      End Sub

      Public Sub New(ReportDate As Date)
        Me.ReportDate = ReportDate
      End Sub

    End Class

    Public Shared Function NewStudioFeedbackReportList() As StudioFeedbackReportList

      Return New StudioFeedbackReportList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetStudioFeedbackReportList() As StudioFeedbackReportList

      Return DataPortal.Fetch(Of StudioFeedbackReportList)(New Criteria())

    End Function

    Public Shared Function GetStudioFeedbackReportList(ReportDate As Date) As StudioFeedbackReportList

      Return DataPortal.Fetch(Of StudioFeedbackReportList)(New Criteria(ReportDate))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(StudioFeedbackReport.GetStudioFeedbackReport(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As StudioFeedbackReport = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.FeedbackReportID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.StudioFeedbackSectionAreaList.RaiseListChangedEvents = False
          parent.StudioFeedbackSectionAreaList.Add(StudioFeedbackSectionArea.GetStudioFeedbackSectionArea(sdr))
          parent.StudioFeedbackSectionAreaList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As StudioFeedbackSectionArea = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.RoomID <> sdr.GetInt32(1) Then
            parentChild = Me.GetStudioFeedbackSectionArea(sdr.GetInt32(1))
          End If
          parentChild.StudioFeedbackSectionList.RaiseListChangedEvents = False
          parentChild.StudioFeedbackSectionList.Add(StudioFeedbackSection.GetStudioFeedbackSection(sdr))
          parentChild.StudioFeedbackSectionList.RaiseListChangedEvents = True
        End While
      End If

      'Dim parentGrandChild As StudioFeedbackSection = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentGrandChild Is Nothing OrElse parentGrandChild.FeedbackReportID <> sdr.GetInt32(1) Then
      '      parentGrandChild = Me.GetStudioFeedbackSection(sdr.GetInt32(1))
      '    End If
      '    parentGrandChild.StudioFeedbackSubSectionList.RaiseListChangedEvents = False
      '    parentGrandChild.StudioFeedbackSubSectionList.Add(StudioFeedbackSubSection.GetStudioFeedbackSubSection(sdr))
      '    parentGrandChild.StudioFeedbackSubSectionList.RaiseListChangedEvents = True
      '  End While
      'End If

      For Each child As StudioFeedbackReport In Me
        child.CheckRules()
        For Each StudioFeedbackSectionArea As StudioFeedbackSectionArea In child.StudioFeedbackSectionAreaList
          StudioFeedbackSectionArea.CheckRules()
          For Each StudioFeedbackSection As StudioFeedbackSection In StudioFeedbackSectionArea.StudioFeedbackSectionList
            StudioFeedbackSection.CheckRules()
            'For Each StudioFeedbackSubSection As StudioFeedbackSubSection In StudioFeedbackSection.StudioFeedbackSubSectionList
            '  StudioFeedbackSubSection.CheckRules()
            'Next
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getStudioFeedbackReportList"
            cm.Parameters.AddWithValue("@ReportDate", crit.ReportDate)
            cm.Parameters.AddWithValue("@UserID", Security.Settings.CurrentUser.UserID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace