﻿' Generated 11 Nov 2015 14:02 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportSubSectionSetting
    Inherits OBReadOnlyBase(Of ROFeedbackReportSubSectionSetting)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportSubSectionSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportSubSectionSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedbackReportSubSectionSettingID() As Integer
      Get
        Return GetProperty(FeedbackReportSubSectionSettingIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportSectionSettingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportSectionSettingID, "Feedback Report Section Setting", Nothing)
    ''' <summary>
    ''' Gets the Feedback Report Section Setting value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property FeedbackReportSectionSettingID() As Integer?
      Get
        Return GetProperty(FeedbackReportSectionSettingIDProperty)
      End Get
    End Property

    Public Shared FeedbackSystemDetailIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackSystemDetailID, "Feedback System Detail", Nothing)
    ''' <summary>
    ''' Gets the Feedback System Detail value
    ''' </summary>
    <Display(Name:="Feedback System Detail", Description:="")>
    Public ReadOnly Property FeedbackSystemDetailID() As Integer?
      Get
        Return GetProperty(FeedbackSystemDetailIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline", 0)
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="If section is regarding feedback for specific disciplines, this should be completed")>
    Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared ReportSubSectionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportSubSection, "Report Sub Section", "")
    ''' <summary>
    ''' Gets the Report Sub Section value
    ''' </summary>
    <Display(Name:="Report Sub Section", Description:="The name of sub section")>
    Public ReadOnly Property ReportSubSection() As String
      Get
        Return GetProperty(ReportSubSectionProperty)
      End Get
    End Property

    Public Shared FeedbackReportSubSectionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportSubSectionTypeID, "Feedback Report Sub Section Type", Nothing)
    ''' <summary>
    ''' Gets the Feedback Report Sub Section Type value
    ''' </summary>
    <Display(Name:="Feedback Report Sub Section Type", Description:="The type of the sub section (E.g. Free Text, Yes/No, Drop Down)")>
    Public ReadOnly Property FeedbackReportSubSectionTypeID() As Integer?
      Get
        Return GetProperty(FeedbackReportSubSectionTypeIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportDropDownIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportDropDownID, "Feedback Report Drop Down", 0)
    ''' <summary>
    ''' Gets the Feedback Report Drop Down value
    ''' </summary>
    <Display(Name:="Feedback Report Drop Down", Description:="If Type is Drop Down or Double Drop Down this field must be specified")>
    Public ReadOnly Property FeedbackReportDropDownID() As Integer
      Get
        Return GetProperty(FeedbackReportDropDownIDProperty)
      End Get
    End Property

    Public Shared CommentsAllowedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CommentsAllowed, "Comments Allowed", False)
    ''' <summary>
    ''' Gets the Comments Allowed value
    ''' </summary>
    <Display(Name:="Comments Allowed", Description:="Tick indicates that comments field will be enabled for this Sub Section")>
    Public ReadOnly Property CommentsAllowed() As Boolean
      Get
        Return GetProperty(CommentsAllowedProperty)
      End Get
    End Property

    Public Shared SubsectionOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SubsectionOrder, "Subsection Order", 0)
    ''' <summary>
    ''' Gets the Subsection Order value
    ''' </summary>
    <Display(Name:="Subsection Order", Description:="The order of the sub section")>
    Public ReadOnly Property SubsectionOrder() As Integer
      Get
        Return GetProperty(SubsectionOrderProperty)
      End Get
    End Property

    Public Shared Column1HeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column1Header, "Column 1Header", "")
    ''' <summary>
    ''' Gets the Column 1Header value
    ''' </summary>
    <Display(Name:="Column 1Header", Description:="")>
    Public ReadOnly Property Column1Header() As String
      Get
        Return GetProperty(Column1HeaderProperty)
      End Get
    End Property

    Public Shared Column2HeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column2Header, "Column 2Header", "")
    ''' <summary>
    ''' Gets the Column 2Header value
    ''' </summary>
    <Display(Name:="Column 2Header", Description:="")>
    Public ReadOnly Property Column2Header() As String
      Get
        Return GetProperty(Column2HeaderProperty)
      End Get
    End Property

    Public Shared Column3HeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column3Header, "Column 3Header", "")
    ''' <summary>
    ''' Gets the Column 3Header value
    ''' </summary>
    <Display(Name:="Column 3Header", Description:="")>
    Public ReadOnly Property Column3Header() As String
      Get
        Return GetProperty(Column3HeaderProperty)
      End Get
    End Property

    Public Shared AnswerHeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AnswerHeader, "Answer Header", "")
    ''' <summary>
    ''' Gets the Answer Header value
    ''' </summary>
    <Display(Name:="Answer Header", Description:="")>
    Public ReadOnly Property AnswerHeader() As String
      Get
        Return GetProperty(AnswerHeaderProperty)
      End Get
    End Property

    Public Shared Answer2HeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Answer2Header, "Answer 2Header", "")
    ''' <summary>
    ''' Gets the Answer 2Header value
    ''' </summary>
    <Display(Name:="Answer 2Header", Description:="")>
    Public ReadOnly Property Answer2Header() As String
      Get
        Return GetProperty(Answer2HeaderProperty)
      End Get
    End Property

    Public Shared DefaultAnswerValueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DefaultAnswerValue, "Default Answer Value", "")
    ''' <summary>
    ''' Gets the Default Answer Value value
    ''' </summary>
    <Display(Name:="Default Answer Value", Description:="This is the default answer (if available)")>
    Public ReadOnly Property DefaultAnswerValue() As String
      Get
        Return GetProperty(DefaultAnswerValueProperty)
      End Get
    End Property

    Public Shared DefaultAnswer1FeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DefaultAnswer1FeedbackReportDropDownItemID, "Default Answer 1Feedback Report Drop Down Item", Nothing)
    ''' <summary>
    ''' Gets the Default Answer 1Feedback Report Drop Down Item value
    ''' </summary>
    <Display(Name:="Default Answer 1Feedback Report Drop Down Item", Description:="This is the default feedback section option")>
    Public ReadOnly Property DefaultAnswer1FeedbackReportDropDownItemID() As Integer?
      Get
        Return GetProperty(DefaultAnswer1FeedbackReportDropDownItemIDProperty)
      End Get
    End Property

    Public Shared DefaultAnswer2FeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DefaultAnswer2FeedbackReportDropDownItemID, "Default Answer 2Feedback Report Drop Down Item", Nothing)
    ''' <summary>
    ''' Gets the Default Answer 2Feedback Report Drop Down Item value
    ''' </summary>
    <Display(Name:="Default Answer 2Feedback Report Drop Down Item", Description:="")>
    Public ReadOnly Property DefaultAnswer2FeedbackReportDropDownItemID() As Integer?
      Get
        Return GetProperty(DefaultAnswer2FeedbackReportDropDownItemIDProperty)
      End Get
    End Property

    Public Shared CanSpecifyMultipleAnswersProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanSpecifyMultipleAnswers, "Can Specify Multiple Answers", False)
    ''' <summary>
    ''' Gets the Can Specify Multiple Answers value
    ''' </summary>
    <Display(Name:="Can Specify Multiple Answers", Description:="Tick indicates that multiple answers can be specified (only valid for Drop Down answers)")>
    Public ReadOnly Property CanSpecifyMultipleAnswers() As Boolean
      Get
        Return GetProperty(CanSpecifyMultipleAnswersProperty)
      End Get
    End Property

    Public Shared StopCompletionIfBlankProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.StopCompletionIfBlank, "Stop Completion If Blank", False)
    ''' <summary>
    ''' Gets the Stop Completion If Blank value
    ''' </summary>
    <Display(Name:="Stop Completion If Blank", Description:="Do not allow reconciliation of this report if the answer for this section is left blank")>
    Public ReadOnly Property StopCompletionIfBlank() As Boolean
      Get
        Return GetProperty(StopCompletionIfBlankProperty)
      End Get
    End Property

    Public Shared AutoPopulatedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AutoPopulated, "Auto Populated", False)
    ''' <summary>
    ''' Gets the Auto Populated value
    ''' </summary>
    <Display(Name:="Auto Populated", Description:="Tick indicates that this sub section will be auto-populated with an answer")>
    Public ReadOnly Property AutoPopulated() As Boolean
      Get
        Return GetProperty(AutoPopulatedProperty)
      End Get
    End Property

    Public Shared UserCanEditProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UserCanEdit, "User Can Edit", False)
    ''' <summary>
    ''' Gets the User Can Edit value
    ''' </summary>
    <Display(Name:="User Can Edit", Description:="Tick indicates that the user can edit the answer to this question")>
    Public ReadOnly Property UserCanEdit() As Boolean
      Get
        Return GetProperty(UserCanEditProperty)
      End Get
    End Property

    Public Shared UserCanAddProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UserCanAdd, "User Can Add")
    ''' <summary>
    ''' Gets the User Can Add value
    ''' </summary>
    <Display(Name:="User Can Add", Description:="Tick indicates that the user can add new lines to this section")>
    Public ReadOnly Property UserCanAdd() As Boolean
      Get
        Return GetProperty(UserCanAddProperty)
      End Get
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", False)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROFeedbackReportSubSectionSettingQuestionListProperty As PropertyInfo(Of ROFeedbackReportSubSectionSettingQuestionList) = RegisterProperty(Of ROFeedbackReportSubSectionSettingQuestionList)(Function(c) c.ROFeedbackReportSubSectionSettingQuestionList, "RO Feedback Report Sub Section Setting Question List")

    <Display(AutoGenerateField:=False)> _
    Public ReadOnly Property ROFeedbackReportSubSectionSettingQuestionList() As ROFeedbackReportSubSectionSettingQuestionList
      Get
        If GetProperty(ROFeedbackReportSubSectionSettingQuestionListProperty) Is Nothing Then
          LoadProperty(ROFeedbackReportSubSectionSettingQuestionListProperty, FeedbackReports.ROFeedbackReportSubSectionSettingQuestionList.NewROFeedbackReportSubSectionSettingQuestionList())
        End If
        Return GetProperty(ROFeedbackReportSubSectionSettingQuestionListProperty)
      End Get
    End Property

    Public Shared ROFeedbackReportFlagCriteriaListProperty As PropertyInfo(Of ROFeedbackReportFlagCriteriaList) = RegisterProperty(Of ROFeedbackReportFlagCriteriaList)(Function(c) c.ROFeedbackReportFlagCriteriaList, "RO Feedback Report Flag Criteria List")

    '<Display(AutoGenerateField:=False)> _
    Public ReadOnly Property ROFeedbackReportFlagCriteriaList() As ROFeedbackReportFlagCriteriaList
      Get
        If GetProperty(ROFeedbackReportFlagCriteriaListProperty) Is Nothing Then
          LoadProperty(ROFeedbackReportFlagCriteriaListProperty, FeedbackReports.ROFeedbackReportFlagCriteriaList.NewROFeedbackReportFlagCriteriaList())
        End If
        Return GetProperty(ROFeedbackReportFlagCriteriaListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportSubSectionSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ReportSubSection

    End Function

    Public Function GetFlagCriteriaItem(FeedbackReportSubSectionSettingID As Integer) As ROFeedbackReportFlagCriteria

      Dim FlagCriteria As ROFeedbackReportFlagCriteria = (From r In Me.ROFeedbackReportFlagCriteriaList
                                                       Where r.FeedbackReportSubSectionSettingID = FeedbackReportSubSectionSettingID
                                                       Select r).FirstOrDefault

      Return FlagCriteria

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROFeedbackReportSubSectionSetting(dr As SafeDataReader) As ROFeedbackReportSubSectionSetting

      Dim r As New ROFeedbackReportSubSectionSetting
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedbackReportSubSectionSettingIDProperty, .GetInt32(0))
        LoadProperty(FeedbackReportSectionSettingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(FeedbackSystemDetailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(DisciplineIDProperty, .GetInt32(3))
        LoadProperty(ReportSubSectionProperty, .GetString(4))
        LoadProperty(FeedbackReportSubSectionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(FeedbackReportDropDownIDProperty, .GetInt32(6))
        LoadProperty(CommentsAllowedProperty, .GetBoolean(7))
        LoadProperty(SubsectionOrderProperty, .GetInt32(8))
        LoadProperty(Column1HeaderProperty, .GetString(9))
        LoadProperty(Column2HeaderProperty, .GetString(10))
        LoadProperty(Column3HeaderProperty, .GetString(11))
        LoadProperty(AnswerHeaderProperty, .GetString(12))
        LoadProperty(Answer2HeaderProperty, .GetString(13))
        LoadProperty(DefaultAnswerValueProperty, .GetString(14))
        LoadProperty(DefaultAnswer1FeedbackReportDropDownItemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
        LoadProperty(DefaultAnswer2FeedbackReportDropDownItemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
        LoadProperty(CanSpecifyMultipleAnswersProperty, .GetBoolean(17))
        LoadProperty(StopCompletionIfBlankProperty, .GetBoolean(18))
        LoadProperty(AutoPopulatedProperty, .GetBoolean(19))
        LoadProperty(UserCanEditProperty, .GetBoolean(20))
        LoadProperty(UserCanAddProperty, .GetBoolean(21))
        LoadProperty(SystemIndProperty, .GetBoolean(22))
      End With

    End Sub

#End Region

  End Class

End Namespace