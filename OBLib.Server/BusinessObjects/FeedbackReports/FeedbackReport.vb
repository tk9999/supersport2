﻿' Generated 12 Nov 2015 08:41 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class FeedbackReport
    Inherits OBBusinessBase(Of FeedbackReport)

#Region " Properties and Methods "

#Region " Feedback Settings "

    Private mFeedbackReportSettings As ROFeedbackReportSetting

    Public ReadOnly Property ReportSettings() As ROFeedbackReportSetting
      Get 
        If mFeedbackReportSettings Is Nothing AndAlso FeedbackReportSettingID IsNot Nothing Then
          mFeedbackReportSettings = CommonData.Lists.ROFeedbackReportSettingList.GetItem(FeedbackReportSettingID)
        End If
        Return mFeedbackReportSettings
      End Get
    End Property

#End Region

#Region " Properties "

    Public Shared FeedbackReportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedbackReportID() As Integer
      Get
        Return GetProperty(FeedbackReportIDProperty)
      End Get
    End Property


    Public Shared FeedbackReportSettingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportSettingID, "Feedback Report Setting", Nothing)
    ''' <summary>
    ''' Gets and sets the Feedback Report Setting value
    ''' </summary>
    <Display(Name:="Feedback Report Setting", Description:="Links to Report settings"),
    Required(ErrorMessage:="Feedback Template required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROFeedbackReportSettingList), FilterMethodName:="FeedbackReportBO.FilterAdHocSettings")>
    Public Property FeedbackReportSettingID() As Integer?
      Get
        Return GetProperty(FeedbackReportSettingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedbackReportSettingIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="Link to production")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="Link to the production system area of this production")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property


    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Department", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Department", Description:=""),
    DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), DropDownType:=DropDownWeb.SelectType.Combo,
                Source:=DropDownWeb.SourceType.ViewModel)>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROProductionAreaList), DropDownType:=DropDownWeb.SelectType.Combo,
                Source:=DropDownWeb.SourceType.ViewModel)>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property


    Public Shared ReportDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ReportDate, "Report Date")
    ''' <summary>
    ''' Gets and sets the Report Date value
    ''' </summary>
    <Display(Name:="Report Date", Description:=""),
    Required(ErrorMessage:="Report Date required")>
    Public Property ReportDate As Date
      Get
        Return GetProperty(ReportDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(ReportDateProperty, Value)
      End Set
    End Property

    Public Shared ReportCompletedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ReportCompletedDateTime)
    ''' <summary>
    ''' "Report Completed Date Time" - Default...
    ''' Gets and sets the Report Completed Date Time value
    ''' RegisterSProperty(Of DateTime?)(Function(c) c.CompletedDate, Nothing) _
    ''' </summary>
    <Display(Name:="Report Completed Date Time", Description:="Date and time the report was marked as completed"), Singular.DataAnnotations.DateField(MinDateProperty:="MinDate")>
    Public Property ReportCompletedDateTime As DateTime?
      Get
        Return GetProperty(ReportCompletedDateTimeProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(ReportCompletedDateTimeProperty, value)
      End Set
    End Property

    Public Shared ReportCompletedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ReportCompletedBy)
    ''' <summary>
    '''  "Report Completed By",
    ''' Gets and sets the Report Completed By value
    ''' </summary>
    <Display(Name:="Report Completed By", Description:="User who completed the report"), Singular.DataAnnotations.DropDownWeb(GetType(ROUserList), DisplayMember:="HumanResourceName")>
    Public Property ReportCompletedBy() As Integer?
      Get
        Return GetProperty(ReportCompletedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ReportCompletedByProperty, Value)
      End Set
    End Property

    Public Shared MinDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MinDate, "Min Date")
    Public Property MinDate As Date = Now

    Public Shared ReportPublishedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ReportPublishedDateTime, RelationshipTypes.None)

    ''' <summary>
    ''' SetReportPublishedBy
    ''' Gets and sets the Report Published Date Time value
    ''' </summary>
    <Display(Name:="Report Published Date Time", Description:=""), Singular.DataAnnotations.DateField(MinDateProperty:="MinDate")>
    Public Property ReportPublishedDateTime As DateTime?
      Get
        Return GetProperty(ReportPublishedDateTimeProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(ReportPublishedDateTimeProperty, value)
      End Set
    End Property

    Public Shared ReportPublishedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ReportPublishedBy, 0)
    ''' <summary>
    ''' Gets and sets the Report Published By value
    '''  "Report Published By",
    ''' </summary>
    <Display(Name:="Report Published By", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(ROUserList), DisplayMember:="HumanResourceName")>
    Public Property ReportPublishedBy() As Integer?
      Get
        Return GetProperty(ReportPublishedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ReportPublishedByProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' Display(AutoGenerateField:=False)
    ''' </summary>
    <Singular.DataAnnotations.DropDownWeb(GetType(ROUserList), DisplayMember:="HumanResourceName")>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared IsManagerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsManager, "", False)

    <Display(Name:="IsManager", Description:="")>
    Public ReadOnly Property IsManager() As Boolean
      Get
        Return GetProperty(IsManagerProperty)
      End Get
    End Property

    Public Shared IsCompletedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsCompleted, False)
    <Display(Name:="IsCompleted", Description:=""),
     SetExpression("FeedbackReportBO.IsCompletedSet(self)")>
    Public Property IsCompleted() As Boolean
      Get
        Return GetProperty(IsCompletedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsCompletedProperty, value)
      End Set
    End Property

    Public Shared ReportCompletedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportCompletedByName, "Completed By")
    <Display(Name:="Completed by", Description:="")>
    Public Property ReportCompletedByName() As String
      Get
        Return GetProperty(ReportCompletedByNameProperty)
      End Get
      Set(value As String)
        SetProperty(ReportCompletedByNameProperty, value)
      End Set
    End Property

    Public Shared IsPublishedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsPublished, False)
    <Display(Name:="IsPublished", Description:=""),
     SetExpression("FeedbackReportBO.IsPublishedSet(self)")>
    Public Property IsPublished() As Boolean
      Get
        Return GetProperty(IsPublishedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsPublishedProperty, value)
      End Set
    End Property

    Public Shared ReportPublishedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportPublishedByName, "Published By")
    <Display(Name:="Published by", Description:="")>
    Public Property ReportPublishedByName() As String
      Get
        Return GetProperty(ReportPublishedByNameProperty)
      End Get
      Set(value As String)
        SetProperty(ReportPublishedByNameProperty, value)
      End Set
    End Property

    Public Shared ReportNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportName, "Report Name", "")
    ''' <summary>
    ''' Gets the Report Name value
    ''' </summary>
    <Display(Name:="Report Name", Description:="")>
    Public ReadOnly Property ReportName() As String
      Get
        If ReportSettings IsNot Nothing Then
          Return ReportSettings.ReportName + " " + ReportDate.ToString("dd-MMM-yyyy")
        End If
        Return ""
      End Get
    End Property

    Public Shared OriginalReportCompletedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.OriginalReportCompletedDateTime, Nothing)
    ''' <summary>
    ''' "Report Completed Date Time" - Default...
    ''' Gets and sets the Report Completed Date Time value
    ''' RegisterSProperty(Of DateTime?)(Function(c) c.CompletedDate, Nothing) _
    ''' </summary>
    <Display(Name:="Report Completed Date Time", Description:="Date and time the report was marked as completed"), Singular.DataAnnotations.DateField(MinDateProperty:="MinDate")>
    Public Property OriginalReportCompletedDateTime As DateTime?
      Get
        Return GetProperty(OriginalReportCompletedDateTimeProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(OriginalReportCompletedDateTimeProperty, value)
      End Set
    End Property

    Public Shared OriginalReportPublishDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.OriginalReportPublishDateTime, Nothing)
    ''' <summary>
    ''' "Report Completed Date Time" - Default...
    ''' Gets and sets the Report Completed Date Time value
    ''' RegisterSProperty(Of DateTime?)(Function(c) c.CompletedDate, Nothing) _
    ''' </summary>
    Public Property OriginalReportPublishDateTime As DateTime?
      Get
        Return GetProperty(OriginalReportPublishDateTimeProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(OriginalReportPublishDateTimeProperty, value)
      End Set
    End Property

    Public Shared HasFeedbackProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasFeedback, "Has Feedack")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Has Feedack", Description:="")>
    Public Property HasFeedback() As Boolean
      Get
        Return GetProperty(HasFeedbackProperty)
      End Get
      Set(value As Boolean)
        SetProperty(HasFeedbackProperty, value)
      End Set
    End Property

    Public Shared IsSubReportProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsSubReport, "Is Sub Report")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Has Feedack", Description:="")>
    Public Property IsSubReport() As Boolean
      Get
        Return GetProperty(IsSubReportProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsSubReportProperty, value)
      End Set
    End Property

    Public Shared IsCompletedParentProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCompletedParent, "Is Completed Parent")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Has Feedack", Description:="")>
    Public Property IsCompletedParent() As Boolean
      Get
        Return GetProperty(IsCompletedParentProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsCompletedParentProperty, value)
      End Set
    End Property

#End Region

#Region "Expanded"
    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#End Region

#Region " Child Lists "

    Public Shared FeedbackReportSectionListProperty As PropertyInfo(Of FeedbackReportSectionList) = RegisterProperty(Of FeedbackReportSectionList)(Function(c) c.FeedbackReportSectionList, "Feedback Report Section List")

    '<Display(AutoGenerateField:=False)> _
    Public ReadOnly Property FeedbackReportSectionList() As FeedbackReportSectionList
      Get
        If GetProperty(FeedbackReportSectionListProperty) Is Nothing Then
          LoadProperty(FeedbackReportSectionListProperty, FeedbackReports.FeedbackReportSectionList.NewFeedbackReportSectionList())
        End If
        Return GetProperty(FeedbackReportSectionListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feedback Report")
        Else
          Return String.Format("Blank {0}", "Feedback Report")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"FeedbackReportSections"}
      End Get
    End Property

    Public Sub SetHasFeedack()
      Dim feedbackCount As Integer = 0
      For Each frs As FeedbackReportSection In Me.FeedbackReportSectionList
        For Each rss As FeedbackReportSubSection In frs.FeedbackReportSubSectionList
          For Each fbd As FeedbackReportDetail In rss.FeedbackReportDetailList
            If fbd.TextAnswer <> "" Or fbd.Comments <> "" Then
              feedbackCount += 1
            End If
          Next
        Next
      Next
      If feedbackCount > 0 Then
        LoadProperty(HasFeedbackProperty, True)
      Else
        LoadProperty(HasFeedbackProperty, False)
      End If
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      'AddWebRule(ReportPublishedDateTimeProperty, Function(c) Not c.ReportPublishedDateTime Is Nothing And c.ReportCompletedDateTime Is Nothing, Function(c) "The Report Must Be Completed Before Published")
      'AddWebRule(ReportCompletedDateTimeProperty, Function(c) Not c.ReportPublishedDateTime Is Nothing And c.ReportCompletedDateTime Is Nothing, Function(c) "The Report Must Be Completed Before Published")
    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedbackReport() method.

    End Sub

    Public Shared Function NewFeedbackReport() As FeedbackReport

      Return DataPortal.CreateChild(Of FeedbackReport)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetFeedbackReport(dr As SafeDataReader) As FeedbackReport

      Dim f As New FeedbackReport()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedbackReportIDProperty, .GetInt32(0))
          LoadProperty(FeedbackReportSettingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ReportDateProperty, .GetValue(4))
          LoadProperty(ReportCompletedDateTimeProperty, .GetValue(5))
          LoadProperty(ReportCompletedByProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(ReportPublishedDateTimeProperty, .GetValue(7))
          LoadProperty(ReportPublishedByProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(IsManagerProperty, .GetBoolean(11))
          LoadProperty(IsCompletedProperty, .GetBoolean(12))
          LoadProperty(ReportCompletedByNameProperty, .GetString(13))
          LoadProperty(IsPublishedProperty, .GetBoolean(14))
          LoadProperty(ReportPublishedByNameProperty, .GetString(15))
          LoadProperty(IsSubReportProperty, .GetBoolean(16))
          LoadProperty(IsCompletedParentProperty, .GetBoolean(17))
          OriginalReportCompletedDateTime = ReportCompletedDateTime
          OriginalReportPublishDateTime = ReportPublishedDateTime
        End With
      End Using
      SetHasFeedack()

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, FeedbackReportIDProperty)
      cm.CommandTimeout = 3600
      cm.Parameters.AddWithValue("@FeedbackReportSettingID", GetProperty(FeedbackReportSettingIDProperty))
      cm.Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(GetProperty(ProductionIDProperty)))
      cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
      cm.Parameters.AddWithValue("@ReportDate", ReportDate)
      cm.Parameters.AddWithValue("@ReportCompletedDateTime", Singular.Misc.NothingDBNull(ReportCompletedDateTime))
      cm.Parameters.AddWithValue("@ReportCompletedBy", Singular.Misc.NothingDBNull(GetProperty(ReportCompletedByProperty)))
      cm.Parameters.AddWithValue("@ReportPublishedDateTime", Singular.Misc.NothingDBNull(ReportPublishedDateTime))
      cm.Parameters.AddWithValue("@ReportPublishedBy", Singular.Misc.NothingDBNull(GetProperty(ReportPublishedByProperty)))
      cm.Parameters.AddWithValue("@CreatedBy", CurrentUser.UserID)
      cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
      cm.Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(FeedbackReportIDProperty, cm.Parameters("@FeedbackReportID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(FeedbackReportSectionListProperty))
      'SetupReportCommpletedEmail()
      SetHasFeedack()

    End Sub

    Private Sub SetupReportCommpletedEmail()


      If Me.ReportSettings.ReportCompletionEmailGroupID IsNot Nothing Then
        If Not Me.ReportCompletedDateTime Is Nothing And OriginalReportCompletedDateTime Is Nothing Then

          Dim ToEmailAddress As String = ""
          Dim Sections As String = ""


          Dim ProductionID As Integer = ProductionID
          '

          Dim ToEmailGroup As ROEmailGroup = CommonData.Lists.ROEmailGroupList.GetItem(Me.ReportSettings.ReportCompletionEmailGroupID)

          If ToEmailGroup IsNot Nothing AndAlso ToEmailGroup.ROEmailGroupMemberList.Count > 0 Then
            Dim ToUserList As ROUserList = CommonData.Lists.ROUserList
            Dim Emaillist As Singular.Emails.EmailList = Singular.Emails.EmailList.NewEmailList
            Dim Email As Singular.Emails.Email = Emaillist.AddNew
            '
            Dim ToEmailAddressList As List(Of String) = New List(Of String)

            ToEmailAddress = BuildRecipientEmail(ToUserList, ToEmailGroup)
            '
            With Email
              .Subject = "Feedback Report Completed : " & mFeedbackReportSettings.ReportName
              .Body = "Dear" & vbCrLf & "All" &
                       " " & vbCrLf &
                       "please note that the " + mFeedbackReportSettings.ReportName & " - " & ReportDate & " has been Completed by " & ToUserList.GetItem(ReportCompletedBy).LoginName & "." & vbCrLf &
                       "This email was sent to inform you about the Completion of the Section" & vbCrLf &
                       " " & vbCrLf &
                       "Reqards, " & vbCrLf &
                       "SOBER"
              .ToEmailAddress = ToEmailAddress
              .FromEmailAddress = OBLib.Security.Settings.OutgoingEmailAddress
              .DateToSend = Now
            End With

            Me.UpdateChild(Emaillist)
          End If
        End If
      End If
    End Sub

    Public Function BuildRecipientEmail(ToUserList As ROUserList, ToEmailGroup As ROEmailGroup)

      Dim EmailToMember As String = ""

      If ToEmailGroup IsNot Nothing Then
        For Each MemberGroup As ROEmailGroupMember In ToEmailGroup.ROEmailGroupMemberList

          If EmailToMember = "" Then
            EmailToMember = ToUserList.GetItem(MemberGroup.UserID).EmailAddress
          ElseIf EmailToMember <> "" Then
            EmailToMember += ";" & ToUserList.GetItem(MemberGroup.UserID).EmailAddress
          End If
        Next

      End If
      Return EmailToMember

    End Function

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@FeedbackReportID", GetProperty(FeedbackReportIDProperty))
    End Sub

#End Region

  End Class

End Namespace