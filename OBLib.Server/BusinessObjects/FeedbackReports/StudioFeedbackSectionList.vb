﻿' Generated 23 Nov 2016 10:47 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class StudioFeedbackSectionList
    Inherits OBBusinessListBase(Of StudioFeedbackSectionList, StudioFeedbackSection)

#Region " Business Methods "

    Public Function GetItem(FeedbackReportID As Integer) As StudioFeedbackSection

      For Each child As StudioFeedbackSection In Me
        If child.FeedbackReportID = FeedbackReportID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewStudioFeedbackSectionList() As StudioFeedbackSectionList

      Return New StudioFeedbackSectionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace