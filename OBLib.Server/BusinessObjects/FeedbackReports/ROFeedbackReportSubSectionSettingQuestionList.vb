﻿' Generated 11 Nov 2015 14:02 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportSubSectionSettingQuestionList
    Inherits OBReadOnlyListBase(Of ROFeedbackReportSubSectionSettingQuestionList, ROFeedbackReportSubSectionSettingQuestion)

#Region " Parent "

    <NotUndoable()> Private mParent As ROFeedbackReportSubSectionSetting
#End Region

#Region " Business Methods "

    Public Function GetItem(FeedbackReportSubSectionSettingQuestionID As Integer) As ROFeedbackReportSubSectionSettingQuestion

      For Each child As ROFeedbackReportSubSectionSettingQuestion In Me
        If child.FeedbackReportSubSectionSettingQuestionID = FeedbackReportSubSectionSettingQuestionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Feedback Report Sub Section Setting Questions"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewROFeedbackReportSubSectionSettingQuestionList() As ROFeedbackReportSubSectionSettingQuestionList

      Return New ROFeedbackReportSubSectionSettingQuestionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace