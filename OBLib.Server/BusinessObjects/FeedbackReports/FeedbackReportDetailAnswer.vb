﻿' Generated 12 Nov 2015 11:22 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

<Serializable()> _
Public Class FeedbackReportDetailAnswer
Inherits OBBusinessBase(Of FeedbackReportDetailAnswer)

#Region " Properties and Methods "

#Region " Properties "

Public Shared FeedbackReportDetailAnswerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportDetailAnswerID, "ID", 0)
''' <summary>
''' Gets the ID value
''' </summary>
		<Display(AutoGenerateField:=False), Key>
Public ReadOnly Property FeedbackReportDetailAnswerID() As Integer
Get
Return GetProperty(FeedbackReportDetailAnswerIDProperty)
End Get
End Property

Public Shared FeedbackReportDetailIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportDetailID, "Feedback Report Detail", Nothing)
''' <summary>
''' Gets and sets the Feedback Report Detail value
''' </summary>
		<Display(Name:="Feedback Report Detail", Description:=""),
		Required(ErrorMessage:="Feedback Report Detail required")>
Public Property FeedbackReportDetailID() As Integer?
Get
Return GetProperty(FeedbackReportDetailIDProperty)
End Get
Set(ByVal Value As Integer?)
SetProperty(FeedbackReportDetailIDProperty, Value)
End Set
End Property

Public Shared TextAnswerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TextAnswer, "Text Answer", "")
''' <summary>
''' Gets and sets the Text Answer value
''' </summary>
		<Display(Name:="Text Answer", Description:="")>
Public Property TextAnswer() As String
Get
Return GetProperty(TextAnswerProperty)
End Get
Set(ByVal Value As String)
SetProperty(TextAnswerProperty, Value)
End Set
End Property

Public Shared Answer1FeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.Answer1FeedbackReportDropDownItemID, "Answer 1Feedback Report Drop Down Item", Nothing)
''' <summary>
''' Gets and sets the Answer 1Feedback Report Drop Down Item value
''' </summary>
		<Display(Name:="Answer 1Feedback Report Drop Down Item", Description:="Selected Drop Down Option")>
Public Property Answer1FeedbackReportDropDownItemID() As Integer?
Get
Return GetProperty(Answer1FeedbackReportDropDownItemIDProperty)
End Get
Set(ByVal Value As Integer?)
SetProperty(Answer1FeedbackReportDropDownItemIDProperty, Value)
End Set
End Property

Public Shared Answer2FeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.Answer2FeedbackReportDropDownItemID, "Answer 2Feedback Report Drop Down Item", Nothing)
''' <summary>
''' Gets and sets the Answer 2Feedback Report Drop Down Item value
''' </summary>
		<Display(Name:="Answer 2Feedback Report Drop Down Item", Description:="Selected Drop Down Option")>
Public Property Answer2FeedbackReportDropDownItemID() As Integer?
Get
Return GetProperty(Answer2FeedbackReportDropDownItemIDProperty)
End Get
Set(ByVal Value As Integer?)
SetProperty(Answer2FeedbackReportDropDownItemIDProperty, Value)
End Set
End Property

Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
''' <summary>
''' Gets and sets the Comments value
''' </summary>
		<Display(Name:="Comments", Description:="Free Text, or Yes/No answer")>
Public Property Comments() As String
Get
Return GetProperty(CommentsProperty)
End Get
Set(ByVal Value As String)
SetProperty(CommentsProperty, Value)
End Set
End Property

Public Shared ConfidentialProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Confidential, "Confidential", False)
''' <summary>
''' Gets and sets the Confidential value
''' </summary>
		<Display(Name:="Confidential", Description:="Comments are confidential and therefore must not appear in the published report (printable version)"),
		Required(ErrorMessage:="Confidential required")>
Public Property Confidential() As Boolean
Get
Return GetProperty(ConfidentialProperty)
End Get
Set(ByVal Value As Boolean)
SetProperty(ConfidentialProperty, Value)
End Set
End Property

Public Shared MarkedConfidentialByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.MarkedConfidentialBy, "Marked Confidential By", Nothing)
''' <summary>
''' Gets and sets the Marked Confidential By value
''' </summary>
		<Display(Name:="Marked Confidential By", Description:="User that marked these comments as confidential (requires security right)")>
Public Property MarkedConfidentialBy() As Integer?
Get
Return GetProperty(MarkedConfidentialByProperty)
End Get
Set(ByVal Value As Integer?)
SetProperty(MarkedConfidentialByProperty, Value)
End Set
End Property

Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
''' <summary>
''' Gets the Created By value
''' </summary>
		<Display(AutoGenerateField:=False)>
Public ReadOnly Property CreatedBy() As Integer?
Get
Return GetProperty(CreatedByProperty)
End Get
End Property

Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
''' <summary>
''' Gets the Created Date Time value
''' </summary>
		<Display(AutoGenerateField:=False)>
Public ReadOnly Property CreatedDateTime() As SmartDate
Get
Return GetProperty(CreatedDateTimeProperty)
End Get
End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
''' <summary>
''' Gets the Modified By value
''' </summary>
		<Display(AutoGenerateField:=False)>
Public ReadOnly Property ModifiedBy() As Integer?
Get
Return GetProperty(ModifiedByProperty)
End Get
End Property

Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
''' <summary>
''' Gets the Modified Date Time value
''' </summary>
		<Display(AutoGenerateField:=False)>
Public ReadOnly Property ModifiedDateTime() As SmartDate
Get
Return GetProperty(ModifiedDateTimeProperty)
End Get
End Property

Public Shared RaisedFeedbackReportFlagTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RaisedFeedbackReportFlagTypeID, "Raised Feedback Report Flag Type", Nothing)
''' <summary>
''' Gets and sets the Raised Feedback Report Flag Type value
''' </summary>
		<Display(Name:="Raised Feedback Report Flag Type", Description:="")>
Public Property RaisedFeedbackReportFlagTypeID() As Integer?
Get
Return GetProperty(RaisedFeedbackReportFlagTypeIDProperty)
End Get
Set(ByVal Value As Integer?)
SetProperty(RaisedFeedbackReportFlagTypeIDProperty, Value)
End Set
End Property

#End Region

#Region " Methods "

Protected Overrides Function GetIdValue() As Object

Return GetProperty(FeedbackReportDetailAnswerIDProperty)

End Function

Public Overrides Function ToString() As String

If Me.TextAnswer.Length = 0 Then
If Me.IsNew Then
		Return String.Format("New {0}", "Feedback Report Detail Answer")
 Else
		Return String.Format("Blank {0}", "Feedback Report Detail Answer")
End If
 Else
	Return Me.TextAnswer
End If

End Function

#End Region

#End Region

#Region " Validation Rules "

Protected Overrides Sub AddBusinessRules()

MyBase.AddBusinessRules()

End Sub

#End Region

#Region " Data Access & Factory Methods "

Protected Overrides Sub OnCreate()

'This is called when a new object is created
'Set any variables here, not in the constructor or NewFeedbackReportDetailAnswer() method.

End Sub

Public Shared Function NewFeedbackReportDetailAnswer() As FeedbackReportDetailAnswer

Return DataPortal.CreateChild(Of FeedbackReportDetailAnswer)()

End Function

Public Sub New()

MarkAsChild()

End Sub

 Friend Shared Function GetFeedbackReportDetailAnswer(dr As SafeDataReader) As FeedbackReportDetailAnswer

Dim f As New FeedbackReportDetailAnswer()
f.Fetch(dr)
Return f

End Function

Protected Sub Fetch(sdr As SafeDataReader)

Using BypassPropertyChecks
With sdr
LoadProperty(FeedbackReportDetailAnswerIDProperty, .GetInt32(0))
LoadProperty(FeedbackReportDetailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
LoadProperty(TextAnswerProperty, .GetString(2))
LoadProperty(Answer1FeedbackReportDropDownItemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
LoadProperty(Answer2FeedbackReportDropDownItemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
LoadProperty(CommentsProperty, .GetString(5))
LoadProperty(ConfidentialProperty, .GetBoolean(6))
LoadProperty(MarkedConfidentialByProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
LoadProperty(CreatedByProperty, .GetInt32(8))
LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
LoadProperty(ModifiedByProperty, .GetInt32(10))
LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
LoadProperty(RaisedFeedbackReportFlagTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
End With
End Using

MarkAsChild()
MarkOld()
BusinessRules.CheckRules()

End Sub

Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, FeedbackReportDetailAnswerIDProperty)

      'cm.Parameters.AddWithValue("FeedbackReportDetailAnswerID", GetProperty(FeedbackReportDetailAnswerIDProperty))
      cm.Parameters.AddWithValue("@FeedbackReportDetailID", GetProperty(FeedbackReportDetailIDProperty))
      cm.Parameters.AddWithValue("@TextAnswer", GetProperty(TextAnswerProperty))
      cm.Parameters.AddWithValue("@Answer1FeedbackReportDropDownItemID", Singular.Misc.NothingDBNull(GetProperty(Answer1FeedbackReportDropDownItemIDProperty)))
      cm.Parameters.AddWithValue("@Answer2FeedbackReportDropDownItemID", Singular.Misc.NothingDBNull(GetProperty(Answer2FeedbackReportDropDownItemIDProperty)))
      cm.Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
      cm.Parameters.AddWithValue("@Confidential", GetProperty(ConfidentialProperty))
      cm.Parameters.AddWithValue("@MarkedConfidentialBy", Singular.Misc.NothingDBNull(GetProperty(MarkedConfidentialByProperty)))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
      cm.Parameters.AddWithValue("@RaisedFeedbackReportFlagTypeID", Singular.Misc.NothingDBNull(GetProperty(RaisedFeedbackReportFlagTypeIDProperty)))

Return Sub()
'Post Save
If Me.IsNew Then
LoadProperty(FeedbackReportDetailAnswerIDProperty, cm.Parameters("@FeedbackReportDetailAnswerID").Value)
End If
End Sub

End Function

Protected Overrides Sub SaveChildren()
'No Children
End Sub

Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
cm.Parameters.AddWithValue("@FeedbackReportDetailAnswerID", GetProperty(FeedbackReportDetailAnswerIDProperty))
End Sub

#End Region

End Class

End Namespace