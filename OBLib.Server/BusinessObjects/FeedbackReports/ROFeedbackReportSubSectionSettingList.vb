﻿' Generated 11 Nov 2015 14:01 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportSubSectionSettingList
    Inherits OBReadOnlyListBase(Of ROFeedbackReportSubSectionSettingList, ROFeedbackReportSubSectionSetting)

#Region " Parent "

    <NotUndoable()> Private mParent As ROFeedbackReportSectionSetting
#End Region

#Region " Business Methods "

    Public Function GetItem(FeedbackReportSubSectionSettingID As Integer) As ROFeedbackReportSubSectionSetting

      For Each child As ROFeedbackReportSubSectionSetting In Me
        If child.FeedbackReportSubSectionSettingID = FeedbackReportSubSectionSettingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Feedback Report Sub Section Settings"

    End Function

    Public Function GetROFeedbackReportSubSectionSettingQuestion(FeedbackReportSubSectionSettingQuestionID As Integer) As ROFeedbackReportSubSectionSettingQuestion

      Dim obj As ROFeedbackReportSubSectionSettingQuestion = Nothing
      For Each parent As ROFeedbackReportSubSectionSetting In Me
        obj = parent.ROFeedbackReportSubSectionSettingQuestionList.GetItem(FeedbackReportSubSectionSettingQuestionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewROFeedbackReportSubSectionSettingList() As ROFeedbackReportSubSectionSettingList

      Return New ROFeedbackReportSubSectionSettingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace