﻿' Generated 12 Nov 2015 08:41 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class FeedbackReportDetail
    Inherits OBBusinessBase(Of FeedbackReportDetail)

    <NotUndoable()> Private mParent As FeedbackReportSubSection = Nothing

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedbackReportDetailID() As Integer
      Get
        Return GetProperty(FeedbackReportDetailIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportSectionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportSectionID, "Feedback Report Section", Nothing)
    ''' <summary>
    ''' Gets the Feedback Report Section value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property FeedbackReportSectionID() As Integer?
      Get
        Return GetProperty(FeedbackReportSectionIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportSubSectionSettingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportSubSectionSettingID, "Feedback Report Sub Section Setting", Nothing)
    ''' <summary>
    ''' Gets and sets the Feedback Report Sub Section Setting value
    ''' </summary>
    <Display(Name:="Feedback Report Sub Section Setting", Description:="")>
    Public Property FeedbackReportSubSectionSettingID() As Integer?
      Get
        Return GetProperty(FeedbackReportSubSectionSettingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedbackReportSubSectionSettingIDProperty, Value)
        'Required(ErrorMessage:="Feedback Report Sub Section Setting required")
      End Set
    End Property

    Public Shared Column1Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column1, "Column 1", "")
    ''' <summary>
    ''' RegisterProperty(Of String)(Function(c) c.Column1, "Column 1", "")
    ''' Gets and sets the Column 1 value
    ''' </summary>
    <Display(Name:="Column 1", Description:="Column 1 Detail")>
    Public Property Column1() As String
      Get
        Return GetProperty(Column1Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(Column1Property, Value)
      End Set
    End Property

    Public Shared Column2Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column2, "Column 2", "")
    ''' <summary>
    ''' Gets and sets the Column 2 value
    ''' </summary>
    <Display(Name:="Column 2", Description:="Column 2 Detail (if specifics are required)")>
    Public Property Column2() As String
      Get
        Return GetProperty(Column2Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(Column2Property, Value)
      End Set
    End Property

    Public Shared Column3Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column3, "Column 3", "")
    ''' <summary>
    ''' Gets and sets the Column 3 value
    ''' </summary>
    <Display(Name:="Column 3", Description:="")>
    Public Property Column3() As String
      Get
        Return GetProperty(Column3Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(Column3Property, Value)
      End Set
    End Property

    Public Shared ReadOnly HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource", 0)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="(System Only) If the answer is related to a Human Resource then this should be completed by the system"), Singular.DataAnnotations.DropDownWeb(GetType(ROHumanResourceList))>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared FeedbackReportDetailAnswerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportDetailAnswerID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>

    Public ReadOnly Property FeedbackReportDetailAnswerID() As Integer
      Get
        Return GetProperty(FeedbackReportDetailAnswerIDProperty)
      End Get
    End Property

    Public Shared TextAnswerProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.TextAnswer, "") _
                                                                  .AddSetExpression("FeedbackReportDetailBO.SetTextAnswerFlag(self)")
    ''RegisterProperty(Of String)(Function(c) c.TextAnswer, "Text Answer", "")
    ''' <summary>
    ''' Gets and sets the Text Answer value
    ''' </summary>
    <Display(Name:="Text Answer", Description:="")>
    Public Property TextAnswer() As String
      Get
        Return GetProperty(TextAnswerProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TextAnswerProperty, Value)
      End Set
    End Property

    Public Shared Answer1FeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.Answer1FeedbackReportDropDownItemID, Nothing) _
                                                                                         .AddSetExpression("FeedbackReportDetailBO.SetAnswer1DropDownFlag(self)")
    'RegisterProperty(Of Integer?)(Function(c) c.Answer1FeedbackReportDropDownItemID, "Answer 1Feedback Report Drop Down Item", Nothing
    ''' <summary> 
    ''' Gets and sets the Answer 1Feedback Report Drop Down Item value
    ''' , FilterMethodName:="FilterDropDown1"
    ''' </summary>
    <Display(Name:="Answer 1Feedback Report Drop Down Item", Description:="Selected Drop Down Option: 1"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROFeedbackReportDropDownItemList), FilterMethodName:="FeedbackReportDetailBO.FilterDropDown1")>
    Public Property Answer1FeedbackReportDropDownItemID() As Integer?
      Get
        Return GetProperty(Answer1FeedbackReportDropDownItemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(Answer1FeedbackReportDropDownItemIDProperty, Value)
      End Set
    End Property

    Public Shared Answer2FeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.Answer2FeedbackReportDropDownItemID, Nothing) _
                                                                                         .AddSetExpression("FeedbackReportDetailBO.SetAnswer2DropDownFlag(self)")
    ''' <summary>
    ''' Gets and sets the Answer 2Feedback Report Drop Down Item value
    ''' </summary>
    <Display(Name:="Answer 2Feedback Report Drop Down Item", Description:="Selected Drop Down Option: 2"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROFeedbackReportDropDownItemList), FilterMethodName:="FeedbackReportDetailBO.FilterDropDown2")>
    Public Property Answer2FeedbackReportDropDownItemID() As Integer?
      Get
        Return GetProperty(Answer2FeedbackReportDropDownItemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(Answer2FeedbackReportDropDownItemIDProperty, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Comments, "") _
                                                                  .AddSetExpression("FeedbackReportDetailBO.SetCommentFlag(self)")
    'RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="Free Text, or Yes/No answer")>
    Public Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

    Public Shared ConfidentialProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.Confidential, False) _
                                                                                         .AddSetExpression("FeedbackReportDetailBO.SetMarkedConfidentialBy(self)")
    '"Confidential"
    ''' <summary>
    ''' Gets and sets the Confidential value
    ''' </summary>
    <Display(Name:="Confidential", Description:="Comments are confidential and therefore must not appear in the published report (printable version)"),
    Required(ErrorMessage:="Confidential required")>
    Public Property Confidential() As Boolean
      Get
        Return GetProperty(ConfidentialProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ConfidentialProperty, Value)
        If Value Then
          LoadProperty(MarkedConfidentialByProperty, CurrentUser.UserID)
        Else
          LoadProperty(MarkedConfidentialByProperty, Nothing)
        End If
      End Set
    End Property

    Public Shared ReadOnly MarkedConfidentialByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.MarkedConfidentialBy, "Marked Confidential By", Nothing)
    ''' <summary>
    ''' Gets and sets the Marked Confidential By value
    ''' 
    ''' , Singular.DataAnnotations.DropDownWeb(GetType(ROHumanResourceList))>
    ''' </summary>
    <Display(Name:="Marked Confidential By", Description:="User that marked these comments as confidential (requires security right)"),
                                                                                         Singular.DataAnnotations.DropDownWeb(GetType(ROUserList), DisplayMember:="HumanResourceName")>
    Public Property MarkedConfidentialBy() As Integer?
      Get
        Return GetProperty(MarkedConfidentialByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(MarkedConfidentialByProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared RaisedFeedbackReportFlagTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.RaisedFeedbackReportFlagTypeID, 0)
    ''.AddSetExpression("GetFlagDetailsHTML(self)")
    ''' <summary>
    ''' Gets and sets the Raised Feedback Report Flag Type value
    ''' </summary>
    <Display(Name:="Flag", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROFeedbackReportFlagTypeList))>
    Public Property RaisedFeedbackReportFlagTypeID() As Integer?

      Get
        Return GetProperty(RaisedFeedbackReportFlagTypeIDProperty)
      End Get
      Set(ByVal value As Integer?)
        SetProperty(RaisedFeedbackReportFlagTypeIDProperty, value)
      End Set
    End Property

    Public Shared FeedbackReportActionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportActionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property FeedbackReportActionID() As Integer
      Get
        Return GetProperty(FeedbackReportActionIDProperty)
      End Get
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "", False)

    <Display(Name:="System Ind", Description:="")>
    Public ReadOnly Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
    End Property

    Public ReadOnly Property FeedbackReportSubSectionTypeID() As Integer
      Get
        Return GetParent.FeedbackReportSubSectionTypeID
      End Get
    End Property


    Public Shared DefaultAnswerValueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DefaultAnswerValue, "")
    ''RegisterProperty(Of String)(Function(c) c.TextAnswer, "Text Answer", "")
    ''' <summary>
    ''' Gets and sets the Text Answer value
    ''' </summary>
    <Display(Name:="Default AnswerValue Answer", Description:="")>
    Public Property DefaultAnswerValue() As String
      Get
        Return GetProperty(DefaultAnswerValueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DefaultAnswerValueProperty, Value)
      End Set
    End Property

    Public Shared DefaultAnswer1FeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DefaultAnswer1FeedbackReportDropDownItemID, 0)
    ''' <summary> 
    ''' Gets and sets the Answer 1Feedback Report Drop Down Item value
    ''' , FilterMethodName:="FilterDropDown1"
    ''' </summary>
    <Display(Name:="Default Answer 1 Feedback Report Drop Down Item", Description:="Selected Drop Down Option: 1")>
    Public Property DefaultAnswer1FeedbackReportDropDownItemID() As Integer?
      Get
        Return GetProperty(DefaultAnswer1FeedbackReportDropDownItemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DefaultAnswer1FeedbackReportDropDownItemIDProperty, Value)
      End Set
    End Property

    Public Shared DefaultAnswer2FeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DefaultAnswer2FeedbackReportDropDownItemID, 0)
    ''' <summary> 
    ''' Gets and sets the Answer 1Feedback Report Drop Down Item value
    ''' , FilterMethodName:="FilterDropDown1"
    ''' </summary>
    <Display(Name:="Default Answer 2 Feedback Report Drop Down Item", Description:="Selected Drop Down Option: 2")>
    Public Property DefaultAnswer2FeedbackReportDropDownItemID() As Integer?
      Get
        Return GetProperty(DefaultAnswer2FeedbackReportDropDownItemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DefaultAnswer2FeedbackReportDropDownItemIDProperty, Value)
      End Set
    End Property

    Public Shared ActionsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Actions, "Actions", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>

    Public ReadOnly Property Actions() As Integer
      Get
        Return FeedbackReportActionList.Count
      End Get
    End Property

    'Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime)
    '''' <summary>
    '''' Gets and Sets value of Start date/time
    '''' </summary>    
    '<Singular.DataAnnotations.DateAndTimeField(FormatString:="DD MMM YYYY HH:mm")>
    'Public Property StartDateTime() As DateTime?
    '  Get
    '    Return GetProperty(StartDateTimeProperty)
    '  End Get
    '  Set(value As DateTime?)
    '    SetProperty(StartDateTimeProperty, value)
    '  End Set
    'End Property


    'Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime)
    '''' <summary>
    '''' Get and Sets value of End date/time
    '''' </summary>  
    '<Singular.DataAnnotations.DateAndTimeField(FormatString:="ddd DD MMM YYYY HH:mm")>
    'Public Property EndDateTime() As DateTime?
    '  Get
    '    Return GetProperty(EndDateTimeProperty)
    '  End Get
    '  Set(value As DateTime?)
    '    SetProperty(EndDateTimeProperty, value)
    '  End Set
    'End Property


    'Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer)
    '''' <summary>
    '''' Get and Sets values of Start date/time buffer
    '''' </summary>
    '<Singular.DataAnnotations.DateAndTimeField(FormatString:="DD MMM YYYY")>
    'Public Property StartDateTimeBuffer() As DateTime?
    '  Get
    '    Return GetProperty(StartDateTimeBufferProperty)
    '  End Get
    '  Set(value As DateTime?)
    '    SetProperty(StartDateTimeBufferProperty, value)
    '  End Set
    'End Property


    'Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer)
    '''' <summary>
    '''' Get and Sets the value of End date/time buffer
    '''' </summary>
    '<Singular.DataAnnotations.DateAndTimeField()>
    'Public Property EndDateTimeBuffer() As DateTime?
    '  Get
    '    Return GetProperty(EndDateTimeBufferProperty)
    '  End Get
    '  Set(value As DateTime?)
    '    SetProperty(EndDateTimeBufferProperty, value)
    '  End Set
    'End Property

#End Region

#Region " Methods "

    Public Function GetParent() As FeedbackReportSubSection

      Return CType(CType(Me.Parent, FeedbackReportDetailList).Parent, FeedbackReportSubSection)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Column1.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feedback Report Detail")
        Else
          Return String.Format("Blank {0}", "Feedback Report Detail")
        End If
      Else
        Return Me.Column1
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"FeedbackReportDetailAnswers"}
      End Get
    End Property

#End Region

#Region "Expanded"
    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#End Region

#End Region

#Region " Child Lists "

    Public Shared FeedbackReportActionListProperty As PropertyInfo(Of FeedbackReportActionList) = RegisterProperty(Of FeedbackReportActionList)(Function(c) c.FeedbackReportActionList, "Feedback Report Action List")

    '<Display(AutoGenerateField:=False)> _
    Public ReadOnly Property FeedbackReportActionList() As FeedbackReportActionList
      Get
        If GetProperty(FeedbackReportActionListProperty) Is Nothing Then
          LoadProperty(FeedbackReportActionListProperty, FeedbackReports.FeedbackReportActionList.NewFeedbackReportActionList())
        End If
        Return GetProperty(FeedbackReportActionListProperty)
      End Get
    End Property

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedbackReportDetail() method.

    End Sub

    Public Shared Function NewFeedbackReportDetail() As FeedbackReportDetail

      Return DataPortal.CreateChild(Of FeedbackReportDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetFeedbackReportDetail(dr As SafeDataReader) As FeedbackReportDetail

      Dim f As New FeedbackReportDetail()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr

          LoadProperty(FeedbackReportDetailIDProperty, .GetInt32(0))
          LoadProperty(FeedbackReportSectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(FeedbackReportSubSectionSettingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(Column1Property, .GetString(3))
          LoadProperty(Column2Property, .GetString(4))
          LoadProperty(Column3Property, .GetString(5))
          LoadProperty(HumanResourceIDProperty, .GetInt32(6))
          LoadProperty(SystemIndProperty, .GetBoolean(7))
          LoadProperty(FeedbackReportDetailAnswerIDProperty, .GetInt32(8))
          LoadProperty(TextAnswerProperty, .GetString(9))
          LoadProperty(Answer1FeedbackReportDropDownItemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(Answer2FeedbackReportDropDownItemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(CommentsProperty, .GetString(12))
          LoadProperty(ConfidentialProperty, .GetBoolean(13))
          LoadProperty(MarkedConfidentialByProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(CreatedByProperty, .GetInt32(15))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(16))
          LoadProperty(ModifiedByProperty, .GetInt32(17))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(18))
          LoadProperty(RaisedFeedbackReportFlagTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
          LoadProperty(DefaultAnswerValueProperty, .GetString(20))
          LoadProperty(DefaultAnswer1FeedbackReportDropDownItemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
          LoadProperty(DefaultAnswer2FeedbackReportDropDownItemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
          'LoadProperty(StartDateTimeProperty, .GetValue(23))
          'LoadProperty(EndDateTimeProperty, .GetValue(24))
          'LoadProperty(StartDateTimeBufferProperty, .GetValue(25))
          'LoadProperty(EndDateTimeBufferProperty, .GetValue(26))


        End With
      End Using

      MarkAsChild()
      MarkOld()
      SetDefaultValues()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, FeedbackReportDetailIDProperty)
      cm.Parameters.AddWithValue("@FeedbackReportDetailAnswerID", Singular.Misc.ZeroNothingDBNull(GetProperty(FeedbackReportDetailAnswerIDProperty)))
      cm.Parameters.AddWithValue("@TextAnswer", GetProperty(TextAnswerProperty))
      cm.Parameters.AddWithValue("@Answer1FeedbackReportDropDownItemID", Singular.Misc.ZeroNothingDBNull(GetProperty(Answer1FeedbackReportDropDownItemIDProperty)))
      cm.Parameters.AddWithValue("@Answer2FeedbackReportDropDownItemID", Singular.Misc.ZeroNothingDBNull(GetProperty(Answer2FeedbackReportDropDownItemIDProperty)))
      cm.Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
      cm.Parameters.AddWithValue("@Confidential", GetProperty(ConfidentialProperty))
      cm.Parameters.AddWithValue("@MarkedConfidentialBy", Singular.Misc.ZeroNothingDBNull(GetProperty(ConfidentialProperty)))
      cm.Parameters.AddWithValue("@ModifiedBy", CurrentUser.UserID)
      cm.Parameters.AddWithValue("@RaisedFeedbackReportFlagTypeID", Singular.Misc.ZeroNothingDBNull(GetProperty(RaisedFeedbackReportFlagTypeIDProperty)))
      cm.Parameters.AddWithValue("@Column1", GetProperty(Column1Property))
      cm.Parameters.AddWithValue("@Column2", GetProperty(Column2Property))
      cm.Parameters.AddWithValue("@Column3", GetProperty(Column3Property))
      cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.ZeroNothingDBNull(GetProperty(HumanResourceIDProperty)))
      cm.Parameters.AddWithValue("@FeedbackReportSectionID", Me.GetParent().FeedbackReportSectionID)
      cm.Parameters.AddWithValue("@FeedbackReportSubSectionSettingID", Me.GetParent().FeedbackReportSubSectionSettingID) 'GetProperty(FeedbackReportSubSectionSettingIDProperty)
      'cm.Parameters.AddWithValue("@StartDateTime", Singular.Misc.NothingDBNull(GetProperty(StartDateTimeProperty)))
      'cm.Parameters.AddWithValue("@StartDateTimeBuffer", Singular.Misc.NothingDBNull(GetProperty(StartDateTimeBufferProperty)))
      'cm.Parameters.AddWithValue("@EndDateTime", Singular.Misc.NothingDBNull(GetProperty(EndDateTimeProperty)))
      'cm.Parameters.AddWithValue("@EndDateTimeBuffer", Singular.Misc.NothingDBNull(GetProperty(EndDateTimeBufferProperty)))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(FeedbackReportDetailIDProperty, cm.Parameters("@FeedbackReportDetailID").Value)
                 '   updateFeedbackReportDetailAnswer()
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      UpdateChild(GetProperty(FeedbackReportActionListProperty))
    End Sub

    Private Sub SetDefaultValues()

      If TextAnswer = "" AndAlso DefaultAnswerValue <> "" Then
        TextAnswer = DefaultAnswerValue
      End If
      If Answer1FeedbackReportDropDownItemID Is Nothing AndAlso _
        DefaultAnswer1FeedbackReportDropDownItemID IsNot Nothing Then
        Answer1FeedbackReportDropDownItemID = DefaultAnswer1FeedbackReportDropDownItemID
      End If
      If Answer2FeedbackReportDropDownItemID Is Nothing AndAlso _
        DefaultAnswer2FeedbackReportDropDownItemID IsNot Nothing Then
        Answer2FeedbackReportDropDownItemID = DefaultAnswer2FeedbackReportDropDownItemID
      End If
    End Sub

    'Private Sub updateFeedbackReportDetailAnswer()

    '  Dim AnswerList As New FeedbackReportDetailAnswerList
    '  Dim Answers As FeedbackReportDetailAnswer = AnswerList.AddNew

    '  Answers.FeedbackReportDetailID = FeedbackReportDetailID
    '  Answers.TextAnswer = TextAnswer
    '  Answers.Answer1FeedbackReportDropDownItemID = Answer1FeedbackReportDropDownItemID
    '  Answers.Answer2FeedbackReportDropDownItemID = Answer2FeedbackReportDropDownItemID
    '  Answers.Comments = Comments
    '  Answers.Confidential = Confidential
    '  Answers.MarkedConfidentialBy = MarkedConfidentialBy
    '  Answers.RaisedFeedbackReportFlagTypeID = RaisedFeedbackReportFlagTypeID

    '  AnswerList.Save()
    'End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@FeedbackReportDetailID", GetProperty(FeedbackReportDetailIDProperty))
    End Sub

#End Region

  End Class

End Namespace
