﻿' Generated 11 Nov 2015 14:01 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportSettingList
    Inherits OBReadOnlyListBase(Of ROFeedbackReportSettingList, ROFeedbackReportSetting)

#Region " Business Methods "

    Public Function GetItem(FeedbackReportSettingID As Integer) As ROFeedbackReportSetting

      For Each child As ROFeedbackReportSetting In Me
        If child.FeedbackReportSettingID = FeedbackReportSettingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function



    Public Overrides Function ToString() As String

      Return "Feedback Report Settings"

    End Function

    Public Function GetROFeedbackReportSectionSetting(FeedbackReportSectionSettingID As Integer) As ROFeedbackReportSectionSetting

      Dim obj As ROFeedbackReportSectionSetting = Nothing
      For Each parent As ROFeedbackReportSetting In Me
        obj = parent.ROFeedbackReportSectionSettingList.GetItem(FeedbackReportSectionSettingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROFeedbackReportSubSectionSetting(FeedbackReportSubSectionSettingID As Integer) As ROFeedbackReportSubSectionSetting

      'GetObject.ROFeedbackReportSubSectionSettingList
      Dim obj As ROFeedbackReportSubSectionSetting = Nothing

      For Each parent As ROFeedbackReportSetting In Me
        For Each child As ROFeedbackReportSectionSetting In parent.ROFeedbackReportSectionSettingList
          obj = child.ROFeedbackReportSubSectionSettingList.GetItem(FeedbackReportSubSectionSettingID)
          If obj IsNot Nothing Then
            Return obj
          End If
        Next
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROFeedbackReportSettingList() As ROFeedbackReportSettingList

      Return New ROFeedbackReportSettingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROFeedbackReportSettingList() As ROFeedbackReportSettingList

      Return DataPortal.Fetch(Of ROFeedbackReportSettingList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROFeedbackReportSetting.GetROFeedbackReportSetting(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROFeedbackReportSetting = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.FeedbackReportSettingID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROFeedbackReportSectionSettingList.RaiseListChangedEvents = False
          parent.ROFeedbackReportSectionSettingList.Add(ROFeedbackReportSectionSetting.GetROFeedbackReportSectionSetting(sdr))
          parent.ROFeedbackReportSectionSettingList.RaiseListChangedEvents = True
        End While
      End If

      Dim parenttChild As ROFeedbackReportSectionSetting = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parenttChild Is Nothing OrElse parenttChild.FeedbackReportSectionSettingID <> sdr.GetInt32(1) Then
            parenttChild = Me.GetROFeedbackReportSectionSetting(sdr.GetInt32(1))
          End If
          parenttChild.ROFeedbackReportSubSectionSettingList.RaiseListChangedEvents = False
          parenttChild.ROFeedbackReportSubSectionSettingList.Add(ROFeedbackReportSubSectionSetting.GetROFeedbackReportSubSectionSetting(sdr))
          parenttChild.ROFeedbackReportSubSectionSettingList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As ROFeedbackReportSubSectionSetting = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.FeedbackReportSubSectionSettingID <> sdr.GetInt32(1) Then
            parentChild = Me.GetROFeedbackReportSubSectionSetting(sdr.GetInt32(1))
          End If
          parentChild.ROFeedbackReportFlagCriteriaList.RaiseListChangedEvents = False
          parentChild.ROFeedbackReportFlagCriteriaList.Add(ROFeedbackReportFlagCriteria.GetROFeedbackReportFlagCriteria(sdr))
          parentChild.ROFeedbackReportFlagCriteriaList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROFeedbackReportSettingList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace