﻿' Generated 31 Mar 2017 15:15 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class StudioFeedbackSubSection
    Inherits OBBusinessBase(Of StudioFeedbackSubSection)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportSectionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportSectionID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
  Public Property FeedbackReportSectionID() As Integer
      Get
        Return GetProperty(FeedbackReportSectionIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(FeedbackReportSectionIDProperty, value)
      End Set
    End Property

    Public Shared FeedbackReportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportID, "Feedback Report")
    ''' <summary>
    ''' Gets and sets the Feedback Report value
    ''' </summary>
    <Display(Name:="Feedback Report", Description:=""),
    Required(ErrorMessage:="Feedback Report required")>
  Public Property FeedbackReportID() As Integer
      Get
        Return GetProperty(FeedbackReportIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(FeedbackReportIDProperty, value)
      End Set
    End Property

    Public Shared ReportSectionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportSection, "Report Section")
    ''' <summary>
    ''' Gets and sets the Report Section value
    ''' </summary>
    <Display(Name:="Report Section", Description:="")>
  Public Property ReportSection() As String
      Get
        Return GetProperty(ReportSectionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ReportSectionProperty, value)
      End Set
    End Property

    Public Shared CompletedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CompletedBy, "Completed By")
    ''' <summary>
    ''' Gets and sets the Completed By value
    ''' </summary>
    <Display(Name:="Completed By", Description:="")>
    Public Property CompletedBy() As Integer?
      Get
        Return GetProperty(CompletedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CompletedByProperty, value)
      End Set
    End Property

    Public Shared CompletedDateProperty As PropertyInfo(Of Date?) = RegisterProperty(Of Date?)(Function(c) c.CompletedDate, "Completed Date")
    ''' <summary>
    ''' Gets and sets the Completed Date value
    ''' </summary>
    <Display(Name:="Completed Date", Description:="")>
    Public Property CompletedDate As Date?
      Get
        Return GetProperty(CompletedDateProperty)
      End Get
      Set(ByVal Value As Date?)
        SetProperty(CompletedDateProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportSectionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ReportSection.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Studio Feedback Sub Section")
        Else
          Return String.Format("Blank {0}", "Studio Feedback Sub Section")
        End If
      Else
        Return Me.ReportSection
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewStudioFeedbackSubSection() method.

    End Sub

    Public Shared Function NewStudioFeedbackSubSection() As StudioFeedbackSubSection

      Return DataPortal.CreateChild(Of StudioFeedbackSubSection)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetStudioFeedbackSubSection(dr As SafeDataReader) As StudioFeedbackSubSection

      Dim s As New StudioFeedbackSubSection()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedbackReportSectionIDProperty, .GetInt32(0))
          LoadProperty(FeedbackReportIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ReportSectionProperty, .GetString(2))
          LoadProperty(CompletedByProperty, .GetInt32(3))
          LoadProperty(CompletedDateProperty, .GetValue(4))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, FeedbackReportSectionIDProperty)

      cm.Parameters.AddWithValue("@FeedbackReportID", GetProperty(FeedbackReportIDProperty))
      cm.Parameters.AddWithValue("@ReportSection", GetProperty(ReportSectionProperty))
      cm.Parameters.AddWithValue("@CompletedBy", GetProperty(CompletedByProperty))
      cm.Parameters.AddWithValue("@CompletedDate", CompletedDate)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(FeedbackReportSectionIDProperty, cm.Parameters("@FeedbackReportSectionID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@FeedbackReportSectionID", GetProperty(FeedbackReportSectionIDProperty))
    End Sub

#End Region

  End Class

End Namespace