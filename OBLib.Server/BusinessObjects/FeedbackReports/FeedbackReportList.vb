﻿' Generated 12 Nov 2015 08:41 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class FeedbackReportList
    Inherits OBBusinessListBase(Of FeedbackReportList, FeedbackReport)

#Region " Business Methods "

    Public Function GetItem(FeedbackReportID As Integer) As FeedbackReport

      For Each child As FeedbackReport In Me
        If child.FeedbackReportID = FeedbackReportID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Feedback Reports"

    End Function

    Public Function GetFeedbackReportSection(FeedbackReportSectionID As Integer) As FeedbackReportSection

      Dim obj As FeedbackReportSection = Nothing
      For Each parent As FeedbackReport In Me
        obj = parent.FeedbackReportSectionList.GetItem(FeedbackReportSectionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetFeedbackReportSubSection(FeedbackReportSubSectionID As Integer) As FeedbackReportSubSection

      Dim obj As FeedbackReportSubSection = Nothing

      For Each parent As FeedbackReport In Me
        For Each child As FeedbackReportSection In parent.FeedbackReportSectionList
          obj = child.FeedbackReportSubSectionList.GetItem(FeedbackReportSubSectionID)

          If obj IsNot Nothing Then
            Return obj
          End If

        Next
      Next
      Return Nothing

    End Function

    Public Function GetFeedbackReportDetail(FeedbackReportDetailID As Integer) As FeedbackReportDetail

      Dim obj As FeedbackReportDetail = Nothing

      For Each parent As FeedbackReport In Me
        For Each parentChild As FeedbackReportSection In parent.FeedbackReportSectionList
          For Each childParent As FeedbackReportSubSection In parentChild.FeedbackReportSubSectionList
            obj = childParent.FeedbackReportDetailList.GetItem(FeedbackReportDetailID)
            If obj IsNot Nothing Then
              Return obj
            End If
          Next
        Next
      Next
      Return Nothing

    End Function

    'Public Function GetFeedbackReportFlagCriteria(FeedbackReportFlagCriteriaID As Integer) As ROFeedbackReportFlagCriteria

    '  Dim obj As ROFeedbackReportFlagCriteria = Nothing

    '  For Each parent As FeedbackReport In Me
    '    For Each parentChild As FeedbackReportSection In parent.FeedbackReportSectionList
    '      For Each childParent As FeedbackReportSubSection In parentChild.FeedbackReportSubSectionList
    '        obj = childParent.ROFeedbackReportFlagCriteriaList.GetItem(FeedbackReportFlagCriteriaID)
    '        If obj IsNot Nothing Then
    '          Return obj
    '        End If
    '      Next
    '    Next
    '  Next
    '  Return Nothing

    'End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Object
      Public Property ProductionSystemAreaID As Object
      Public Property HumanResourceID As Object
      Public Property FeedbackReportID As Object


      Public Sub New()


      End Sub
      Public Sub New(FeedbackReportID As Integer)

        Me.FeedbackReportID = FeedbackReportID

      End Sub


      Public Sub New(ProductionID As Object, ProductionSystemAreaID As Object)

        Me.ProductionID = ProductionID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
        Me.HumanResourceID = HumanResourceID
      End Sub

    End Class

    Public Shared Function NewFeedbackReportList() As FeedbackReportList

      Return New FeedbackReportList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetFeedbackReportList(ProductionID As Object, ProductionSystemAreaID As Object) As FeedbackReportList

      Return DataPortal.Fetch(Of FeedbackReportList)(New Criteria(ProductionID, ProductionSystemAreaID))

    End Function

    Public Shared Function GetFeedbackReportList(FeedbackReportID As Integer) As FeedbackReportList

      Return DataPortal.Fetch(Of FeedbackReportList)(New Criteria(FeedbackReportID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read

        Me.Add(FeedbackReport.GetFeedbackReport(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As FeedbackReport = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.FeedbackReportID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.FeedbackReportSectionList.RaiseListChangedEvents = False
          parent.FeedbackReportSectionList.Add(FeedbackReportSection.GetFeedbackReportSection(sdr))
          parent.FeedbackReportSectionList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentSection As FeedbackReportSection = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentSection Is Nothing OrElse parentSection.FeedbackReportSectionID <> sdr.GetInt32(1) Then
            parentSection = Me.GetFeedbackReportSection(sdr.GetInt32(1))
          End If
          parentSection.FeedbackReportSubSectionList.RaiseListChangedEvents = False
          parentSection.FeedbackReportSubSectionList.Add(FeedbackReportSubSection.GetFeedbackReportSubSection(sdr))
          parentSection.FeedbackReportSubSectionList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentSubSection As FeedbackReportSubSection = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentSubSection Is Nothing OrElse parentSubSection.FeedbackReportSubSectionID <> sdr.GetInt32(1) Then
            parentSubSection = Me.GetFeedbackReportSubSection(sdr.GetInt32(1))
          End If
          parentSubSection.FeedbackReportDetailList.RaiseListChangedEvents = False
          parentSubSection.FeedbackReportDetailList.Add(FeedbackReportDetail.GetFeedbackReportDetail(sdr))
          parentSubSection.FeedbackReportDetailList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentDetail As FeedbackReportDetail = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentDetail Is Nothing OrElse parentDetail.FeedbackReportDetailID <> sdr.GetInt32(1) Then
            parentDetail = GetFeedbackReportDetail(sdr.GetInt32(1))
          End If
          parentDetail.FeedbackReportActionList.RaiseListChangedEvents = False
          parentDetail.FeedbackReportActionList.Add(FeedbackReportAction.GetFeedbackReportAction(sdr))
          parentDetail.FeedbackReportActionList.RaiseListChangedEvents = True
        End While
      End If

      'Dim parentSubSectionFC As FeedbackReportSubSection = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentSubSectionFC Is Nothing OrElse parentSubSectionFC.FeedbackReportSubSectionSettingID <> sdr.GetInt32(1) Then
      '      parentSubSectionFC = Me.GetFeedbackReportSubSection(sdr.GetInt32(1))
      '    End If
      '    If parentSubSectionFC IsNot Nothing Then
      '      parentSubSectionFC.ROFeedbackReportFlagCriteriaList.RaiseListChangedEvents = False
      '      parentSubSectionFC.ROFeedbackReportFlagCriteriaList.Add(ROFeedbackReportFlagCriteria.GetROFeedbackReportFlagCriteria(sdr))
      '      parentSubSectionFC.ROFeedbackReportFlagCriteriaList.RaiseListChangedEvents = True
      '    End If
      '  End While
      'End If

      For Each child As FeedbackReport In Me
        child.CheckRules()
        For Each FeedbackReportSection As FeedbackReportSection In child.FeedbackReportSectionList
          FeedbackReportSection.CheckRules()

          'For Each FeedbackReportDetail As FeedbackReportDetail In FeedbackReportSection.FeedbackReportDetailList
          '  FeedbackReportDetail.CheckRules()
          ' FeedbackReportDetail.LinkSettings()
          'Next
          For Each FeedbackReportSubSection As FeedbackReportSubSection In FeedbackReportSection.FeedbackReportSubSectionList
            FeedbackReportSubSection.CheckRules()
            If FeedbackReportSubSection.FeedbackReportDetailList.Count > 0 Then
              FeedbackReportSubSection.FeedbackReportSubSectionSettingID = FeedbackReportSubSection.FeedbackReportDetailList(0).FeedbackReportSubSectionSettingID
            End If
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getFeedbackReportList"
            cm.Parameters.AddWithValue("@ProductionID", Singular.Misc.ZeroDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@FeedbackReportID", Singular.Misc.ZeroDBNull(crit.FeedbackReportID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.ZeroDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.ZeroDBNull(OBLib.Security.Settings.CurrentUser.HumanResourceID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace