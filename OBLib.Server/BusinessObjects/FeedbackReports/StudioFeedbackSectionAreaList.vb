﻿' Generated 23 Nov 2016 10:46 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class StudioFeedbackSectionAreaList
    Inherits OBBusinessListBase(Of StudioFeedbackSectionAreaList, StudioFeedbackSectionArea)

#Region " Business Methods "

    Public Function GetItem(RoomID As Integer) As StudioFeedbackSectionArea

      For Each child As StudioFeedbackSectionArea In Me
        If child.RoomID = RoomID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetStudioFeedbackSection(FeedbackReportID As Integer) As StudioFeedbackSection

      Dim obj As StudioFeedbackSection = Nothing
      For Each parent As StudioFeedbackSectionArea In Me
        obj = parent.StudioFeedbackSectionList.GetItem(FeedbackReportID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewStudioFeedbackSectionAreaList() As StudioFeedbackSectionAreaList

      Return New StudioFeedbackSectionAreaList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace