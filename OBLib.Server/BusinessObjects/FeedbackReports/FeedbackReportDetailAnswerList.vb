﻿' Generated 12 Nov 2015 11:22 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

<Serializable()> _
Public Class FeedbackReportDetailAnswerList
Inherits OBBusinessListBase(Of FeedbackReportDetailAnswerList, FeedbackReportDetailAnswer)

#Region " Business Methods "

Public Function GetItem(FeedbackReportDetailAnswerID As Integer) As FeedbackReportDetailAnswer

For Each child As FeedbackReportDetailAnswer In Me
If child.FeedbackReportDetailAnswerID = FeedbackReportDetailAnswerID Then
Return child
End If
Next
Return Nothing

End Function

Public Overrides Function ToString() As String

Return "Feedback Report Detail Answers"

End Function

#End Region

#Region " Data Access "

<Serializable()> _
Public Class Criteria
Inherits CriteriaBase(Of Criteria)

Public Sub New()


End Sub

End Class

Public Shared Function NewFeedbackReportDetailAnswerList() As FeedbackReportDetailAnswerList

Return New FeedbackReportDetailAnswerList()

End Function

Public Sub New()

' must have parameter-less constructor

End Sub

Public Shared Function GetFeedbackReportDetailAnswerList() As FeedbackReportDetailAnswerList

Return DataPortal.Fetch(Of FeedbackReportDetailAnswerList)(New Criteria())

End Function

Private Sub Fetch(sdr As SafeDataReader)

Me.RaiseListChangedEvents = False
While sdr.Read
Me.Add(FeedbackReportDetailAnswer.GetFeedbackReportDetailAnswer(sdr))
End While
Me.RaiseListChangedEvents = True

End Sub

Protected Overrides Sub DataPortal_Fetch(criteria As Object)

Dim crit As Criteria = criteria
Using cn As New SqlConnection(Singular.Settings.ConnectionString)
cn.Open()
Try
Using cm As SqlCommand = cn.CreateCommand
cm.CommandType = CommandType.StoredProcedure
cm.CommandText = "GetProcsWeb.getFeedbackReportDetailAnswerList"
Using sdr As New SafeDataReader(cm.ExecuteReader)
Fetch(sdr)
End Using
End Using
Finally
cn.Close()
End Try
End Using

End Sub

#End Region

End Class

End Namespace