﻿' Generated 23 Nov 2016 10:47 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()>
  Public Class StudioFeedbackSection
    Inherits OBBusinessBase(Of StudioFeedbackSection)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key, Browsable(True)>
    Public Property FeedbackReportID() As Integer
      Get
        Return GetProperty(FeedbackReportIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(FeedbackReportIDProperty, value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared ParentReportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ParentReportID, "Parent Report")
    ''' <summary>
    ''' Gets and sets the Parent Report value
    ''' </summary>
    <Display(Name:="Parent Report", Description:="")>
    Public ReadOnly Property ParentReportID() As Integer
      Get
        Return GetProperty(ParentReportIDProperty)
      End Get
    End Property

    Public Shared RoomScheduleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomSchedule, "Room Schedule")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public ReadOnly Property RoomSchedule() As String
      Get
        Return GetProperty(RoomScheduleProperty)
      End Get
    End Property

    Public Shared RoomScheduleTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomScheduleTime, "Room Schedule")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Schedule Time", Description:="")>
    Public ReadOnly Property RoomScheduleTime() As String
      Get
        Return GetProperty(RoomScheduleTimeProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets and Sets the Production value
    ''' </summary>    
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionIDProperty, value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area")
    ''' <summary>
    ''' 
    ''' </summary>    
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "RoomScheduleID")
    ''' <summary>
    ''' 
    ''' </summary>    
    Public Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(RoomScheduleIDProperty, value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingID, "ResourceBookingID")
    ''' <summary>
    ''' 
    ''' </summary>    
    Public Property ResourceBookingID() As Integer?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ResourceBookingIDProperty, value)
      End Set
    End Property

    Public Shared HasFeedbackProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasFeedback, "Has Feedback")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Has Feedback", Description:="")>
    Public Property HasFeedback() As Boolean
      Get
        Return GetProperty(HasFeedbackProperty)
      End Get
      Set(value As Boolean)
        SetProperty(HasFeedbackProperty, value)
      End Set
    End Property

    Public Shared IsCompletedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCompleted, "Is Completed")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Is Completed", Description:="")>
    Public Property IsCompleted() As Boolean
      Get
        Return GetProperty(IsCompletedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsCompletedProperty, value)
      End Set
    End Property

    Public Shared IsReconciledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsReconciled, "Is Reconciled")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Is Reconciled", Description:=""),
      SetExpression("StudioFeedbackSectionBO.IsReconciledSet(self)")>
    Public Property IsReconciled() As Boolean
      Get
        Return GetProperty(IsReconciledProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsReconciledProperty, value)
      End Set
    End Property

#End Region

    '#Region "Child List"

    '    Public Shared StudioFeedbackSubSectionListProperty As PropertyInfo(Of StudioFeedbackSubSectionList) = RegisterProperty(Of StudioFeedbackSubSectionList)(Function(c) c.StudioFeedbackSubSectionList)

    '    Public ReadOnly Property StudioFeedbackSubSectionList() As StudioFeedbackSubSectionList
    '      Get
    '        If GetProperty(StudioFeedbackSubSectionListProperty) Is Nothing Then
    '          LoadProperty(StudioFeedbackSubSectionListProperty, FeedbackReports.StudioFeedbackSubSectionList.NewStudioFeedbackSubSectionList())
    '        End If
    '        Return GetProperty(StudioFeedbackSubSectionListProperty)
    '      End Get
    '    End Property

    '#End Region

#Region " Methods "

    Public Function GetParent() As StudioFeedbackSectionArea

      Return CType(CType(Me.Parent, StudioFeedbackSectionList).Parent, StudioFeedbackSectionArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.RoomSchedule.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Studio Feedback Section")
        Else
          Return String.Format("Blank {0}", "Studio Feedback Section")
        End If
      Else
        Return Me.RoomSchedule
      End If

    End Function

    'Public Sub SetHasFeedack()
    '  Dim feedbackCount As Integer = 0
    '  For Each rss As FeedbackReportSubSection In Me.FeedbackReportSubSectionList
    '    For Each fbd As FeedbackReportDetail In rss.FeedbackReportDetailList
    '      If fbd.TextAnswer <> "" Or fbd.Comments <> "" Then
    '        feedbackCount += 1
    '      End If
    '    Next
    '  Next
    '  If feedbackCount > 0 Then
    '    LoadProperty(HasFeedbackProperty, True)
    '  Else
    '    LoadProperty(HasFeedbackProperty, False)
    '  End If
    'End Sub

#End Region


    'Public Shared FeedbackReportSubSectionListProperty As PropertyInfo(Of FeedbackReportSubSectionList) = RegisterProperty(Of FeedbackReportSubSectionList)(Function(c) c.FeedbackReportSubSectionList, "Feedback Report Sub Section List")

    ''<Display(AutoGenerateField:=False)> _
    'Public ReadOnly Property FeedbackReportSubSectionList() As FeedbackReportSubSectionList
    '  Get
    '    If GetProperty(FeedbackReportSubSectionListProperty) Is Nothing Then
    '      LoadProperty(FeedbackReportSubSectionListProperty, FeedbackReports.FeedbackReportSubSectionList.NewFeedbackReportSubSectionList())
    '    End If
    '    Return GetProperty(FeedbackReportSubSectionListProperty)
    '  End Get
    'End Property

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewStudioFeedbackSection() method.

    End Sub

    Public Shared Function NewStudioFeedbackSection() As StudioFeedbackSection

      Return DataPortal.CreateChild(Of StudioFeedbackSection)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetStudioFeedbackSection(dr As SafeDataReader) As StudioFeedbackSection

      Dim s As New StudioFeedbackSection()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedbackReportIDProperty, .GetInt32(0))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ParentReportIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(RoomScheduleProperty, .GetString(3))
          LoadProperty(RoomScheduleTimeProperty, .GetString(4))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(HasFeedbackProperty, .GetBoolean(9))
          LoadProperty(IsCompletedProperty, .GetBoolean(10))
          LoadProperty(IsReconciledProperty, .GetBoolean(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, FeedbackReportIDProperty)

      cm.Parameters.AddWithValue("@RoomID", Me.GetParent().RoomID)
      cm.Parameters.AddWithValue("@ParentReportID", GetProperty(ParentReportIDProperty))
      cm.Parameters.AddWithValue("@RoomSchedule", GetProperty(RoomScheduleProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(FeedbackReportIDProperty, cm.Parameters("@FeedbackReportID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@FeedbackReportID", GetProperty(FeedbackReportIDProperty))
    End Sub

#End Region

  End Class

End Namespace