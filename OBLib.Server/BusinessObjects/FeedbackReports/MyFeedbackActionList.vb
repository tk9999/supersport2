﻿' Generated 14 Mar 2016 10:19 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class MyFeedbackActionList
    Inherits OBBusinessListBase(Of MyFeedbackActionList, MyFeedbackAction)

#Region " Business Methods "

    Public Function GetItem(FeedbackReportActionID As Integer) As MyFeedbackAction

      For Each child As MyFeedbackAction In Me
        If child.FeedbackReportActionID = FeedbackReportActionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "My Feedback Actions"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property UserID As Object

      Public Sub New()

      End Sub

      Public Sub New(UserID As Integer)

        Me.UserID = UserID
      End Sub

    End Class

    Public Shared Function NewMyFeedbackActionList() As MyFeedbackActionList

      Return New MyFeedbackActionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetMyFeedbackActionList(UserID As Integer) As MyFeedbackActionList

      Return DataPortal.Fetch(Of MyFeedbackActionList)(New Criteria(UserID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(MyFeedbackAction.GetMyFeedbackAction(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getMyFeedbackActionList"
            cm.Parameters.AddWithValue("@UserID", crit.UserID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace