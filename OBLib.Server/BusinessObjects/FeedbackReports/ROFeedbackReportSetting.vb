﻿' Generated 11 Nov 2015 14:01 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportSetting
    Inherits OBReadOnlyBase(Of ROFeedbackReportSetting)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedbackReportSettingID() As Integer
      Get
        Return GetProperty(FeedbackReportSettingIDProperty)
      End Get
    End Property

    Public Shared ReportNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportName, "Report Name", "")
    ''' <summary>
    ''' Gets the Report Name value
    ''' </summary>
    <Display(Name:="Report Name", Description:="")>
    Public ReadOnly Property ReportName() As String
      Get
        Return GetProperty(ReportNameProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="System these report settings apply to")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' , Singular.DataAnnotations.DropDownWeb(GetType(ROProductionAreaList)
    ''' </summary>
    <Display(Name:="Production Area", Description:="The area these report settings apply to"), Singular.DataAnnotations.DropDownWeb(GetType(ROProductionAreaList))>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportAdHocTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportAdHocTypeID, "Feedback Report Ad Hoc Type", Nothing)
    ''' <summary>
    ''' Gets the Feedback Report Ad Hoc Type value
    ''' </summary>
    <Display(Name:="Feedback Report Ad Hoc Type", Description:="If specified, this means that this feedback report can be created on an Ad Hoc basis")>
    Public ReadOnly Property FeedbackReportAdHocTypeID() As Integer?
      Get
        Return GetProperty(FeedbackReportAdHocTypeIDProperty)
      End Get
    End Property

    Public Shared ReportOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReportOrder, "Report Order", 0)
    ''' <summary>
    ''' Gets the Report Order value
    ''' </summary>
    <Display(Name:="Report Order", Description:="The order in which this Report will appear in the Feedback Report if run for multiple System Areas")>
    Public ReadOnly Property ReportOrder() As Integer
      Get
        Return GetProperty(ReportOrderProperty)
      End Get
    End Property

    Public Shared CanPublishProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanPublish, "Can Publish", False)
    ''' <summary>
    ''' Gets the Can Publish value
    ''' </summary>
    <Display(Name:="Can Publish", Description:="Published Reports can be viewed by users in other system areas")>
    Public ReadOnly Property CanPublish() As Boolean
      Get
        Return GetProperty(CanPublishProperty)
      End Get
    End Property

    Public Shared FeebackReportCompleteByTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeebackReportCompleteByTypeID, "Feeback Report Complete By Type", Nothing)
    ''' <summary>
    ''' Gets the Feeback Report Complete By Type value
    ''' ROFeebackReportCompleteByTypeList --- 
    ''' </summary>
    <Display(Name:="Feeback Report Complete By Type", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(ROFeebackReportCompleteByTypeList))>
    Public ReadOnly Property FeebackReportCompleteByTypeID() As Integer?
      Get
        Return GetProperty(FeebackReportCompleteByTypeIDProperty)
      End Get
    End Property

    Public Shared FeebackReportCompleteBySpecificDisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeebackReportCompleteBySpecificDisciplineID, "Feeback Report Complete By Specific Discipline", Nothing)
    ''' <summary>
    ''' Gets the Feeback Report Complete By Specific Discipline value
    ''' </summary>
    <Display(Name:="Feeback Report Complete By Specific Discipline", Description:="")>
    Public ReadOnly Property FeebackReportCompleteBySpecificDisciplineID() As Integer?
      Get
        Return GetProperty(FeebackReportCompleteBySpecificDisciplineIDProperty)
      End Get
    End Property

    Public Shared ReportCompletionEmailGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ReportCompletionEmailGroupID, "Report Completion Email Group", Nothing)
    ''' <summary>
    ''' Gets the Report Completion Email Group value
    ''' </summary>
    <Display(Name:="Report Completion Email Group", Description:="If specified, this group will be emailed the report on Completion")>
    Public ReadOnly Property ReportCompletionEmailGroupID() As Integer?
      Get
        Return GetProperty(ReportCompletionEmailGroupIDProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROFeedbackReportSectionSettingListProperty As PropertyInfo(Of ROFeedbackReportSectionSettingList) = RegisterProperty(Of ROFeedbackReportSectionSettingList)(Function(c) c.ROFeedbackReportSectionSettingList, "RO Feedback Report Section Setting List")

    <Display(AutoGenerateField:=False)> _
    Public ReadOnly Property ROFeedbackReportSectionSettingList() As ROFeedbackReportSectionSettingList
      Get
        If GetProperty(ROFeedbackReportSectionSettingListProperty) Is Nothing Then
          LoadProperty(ROFeedbackReportSectionSettingListProperty, FeedbackReports.ROFeedbackReportSectionSettingList.NewROFeedbackReportSectionSettingList())
        End If
        Return GetProperty(ROFeedbackReportSectionSettingListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ReportName

    End Function

    Public Function GetSectionItem(FeedbackReportSectionSettingID As Integer) As ROFeedbackReportSectionSetting

      Dim Section As ROFeedbackReportSectionSetting = (From r In Me.ROFeedbackReportSectionSettingList
                                                       Where r.FeedbackReportSectionSettingID = FeedbackReportSectionSettingID
                                                       Select r).FirstOrDefault

      Return Section

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROFeedbackReportSetting(dr As SafeDataReader) As ROFeedbackReportSetting

      Dim r As New ROFeedbackReportSetting()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedbackReportSettingIDProperty, .GetInt32(0))
        LoadProperty(ReportNameProperty, .GetString(1))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(FeedbackReportAdHocTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(ReportOrderProperty, .GetInt32(5))
        LoadProperty(CanPublishProperty, .GetBoolean(6))
        LoadProperty(FeebackReportCompleteByTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(FeebackReportCompleteBySpecificDisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(ReportCompletionEmailGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
      End With

    End Sub

#End Region

  End Class

End Namespace