﻿' Generated 07 Dec 2015 12:14 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Emails
Imports System.Web.Mail

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class FeedbackReportAction
    Inherits OBBusinessBase(Of FeedbackReportAction)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportActionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportActionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedbackReportActionID() As Integer
      Get
        Return GetProperty(FeedbackReportActionIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportDetailIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportDetailID, "Feedback Report Detail", Nothing)
    ''' <summary>
    ''' Gets and sets the Feedback Report Detail value
    ''' ,Required(ErrorMessage:="Feedback Report Detail required")
    ''' </summary>
    <Display(Name:="Feedback Report Detail", Description:="")>
    Public Property FeedbackReportDetailID() As Integer?
      Get
        Return GetProperty(FeedbackReportDetailIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedbackReportDetailIDProperty, Value)
      End Set
    End Property

    Public Shared DelegateActionProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.DelegateAction, False) _
                                                                                         .AddSetExpression("FeedbackReportActionBO.SetDelegatedBy(self)")
    ''' <summary>
    ''' Gets and sets the Delegate Action value
    ''' </summary>
    <Display(Name:="Delegate", Description:="Tick indicates that the action will be delegated to"),
    Required(ErrorMessage:="Delegate Action required")>
    Public Property DelegateAction() As Boolean
      Get
        Return GetProperty(DelegateActionProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DelegateActionProperty, Value)
      End Set
    End Property

    Public Shared DelegatedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DelegatedBy, "Delegated By", Nothing)
    ''' <summary>
    ''' Gets and sets the Delegated By value
    ''' </summary>
    <Display(Name:="Delegated By", Description:="This is the user that delegated the action"),
                                                                                        Singular.DataAnnotations.DropDownWeb(GetType(ROUserList), DisplayMember:="HumanResourceName")>
    Public Property DelegatedBy() As Integer?
      Get
        Return GetProperty(DelegatedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DelegatedByProperty, Value)
      End Set
    End Property

    Public Shared ActionByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ActionBy, "Action By", RelationshipTypes.None)
    ''' <summary>
    ''' Gets and sets the Action By value
    ''' 'ROHumanResourceList
    ''' </summary>
    <Display(Name:="Action By", Description:="This is the user that took the action (or was delegated to do so)"),
                                                    Singular.DataAnnotations.DropDownWeb(GetType(ROUserList), DisplayMember:="HumanResourceName")>
    Public Property ActionBy() As Integer?
      Get
        Return GetProperty(ActionByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ActionByProperty, Value)
      End Set
    End Property

    Public Shared InstructionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Instruction, "Instruction", "")
    ''' <summary>
    ''' Gets and sets the Instruction value
    ''' </summary>
    <Display(Name:="Instruction", Description:="")>
    Public Property Instruction() As String
      Get
        Return GetProperty(InstructionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(InstructionProperty, Value)
      End Set
    End Property
    'Mini Date
    Public Shared MinDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MinDate, "Min Date")
    Public Property MinDate As Date = Now

    Public Shared ActionByDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ActionByDate, "Action By Date")
    ''' <summary>
    ''' Gets and sets the Action By Date value
    ''' RegisterProperty(Of Date)(Function(c) c.ActionByDate, "Action By Date")
    ''' </summary>
    <Display(Name:="Action Date", Description:=""), Singular.DataAnnotations.DateField(MinDateProperty:="MinDate"), Required(ErrorMessage:="Action  Date required")>
    Public Property ActionByDate As Date
      Get
        Return GetProperty(ActionByDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(ActionByDateProperty, Value)
      End Set
    End Property
    ''' <summary>
    ''' Creating A Property For Restricting The Date To Start From The Current Date...Add This Code : Singular.DataAnnotations.DateField(MinDateProperty:="MinDate")>
    ''' </summary>
    ''' <remarks></remarks>

    Public Shared ActionCommentsProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ActionComments, "") _
    ''' <summary>
    ''' </summary>
    <Display(Name:="Action Taken", Description:="")>
    Public Property ActionComments() As String
      Get
        Return GetProperty(ActionCommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ActionCommentsProperty, Value)
      End Set
    End Property

    Public Shared ActionTakenDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ActionTakenDate, "Action Taken Date")
    ''' <summary>
    ''',Singular.DataAnnotations.DateField(MinDateProperty:="MinDate")>
    ''' Gets and sets the Action Taken Date value
    ''' </summary>

    <Display(Name:="Action Taken Date", Description:="")>
    Public Property ActionTakenDate As DateTime?
      Get
        Return GetProperty(ActionTakenDateProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(ActionTakenDateProperty, value)
      End Set
    End Property

    Public Shared ConfidentialProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.Confidential, False) _
                                                                                           .AddSetExpression("FeedbackReportActionBO.SetMarkedConfidentialByML(self)") 'RegisterProperty(Of Boolean)(Function(c) c.Confidential, "Confidential", False)
    ''' <summary>
    ''' Gets and sets the Confidential value
    ''' </summary>
    <Display(Name:="Confidential", Description:="Action is confidential and therefore must not appear in the published report (printable version)"),
    Required(ErrorMessage:="Confidential required")>
    Public Property Confidential() As Boolean
      Get
        Return GetProperty(ConfidentialProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ConfidentialProperty, Value)
      End Set
    End Property

    Public Shared MarkedConfidentialByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.MarkedConfidentialBy, "Marked Confidential By", Nothing)
    ''' <summary>
    ''' Gets and sets the Marked Confidential By value
    ''' , Singular.DataAnnotations.DropDownWeb(GetType(ROUserList))>
    ''' </summary>
    <Display(Name:="Marked Confidential By", Description:="User that marked these action as confidential (requires security right)"),
                                                                                                       Singular.DataAnnotations.DropDownWeb(GetType(ROUserList), DisplayMember:="HumanResourceName")>
    Public Property MarkedConfidentialBy() As Integer?
      Get
        Return GetProperty(MarkedConfidentialByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(MarkedConfidentialByProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared OriginalInstructionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OriginalInstruction, "OriginalInstruction Instruction", "")
    ''' <summary>
    ''' Gets and sets the Instruction value
    ''' </summary>
    <Display(Name:="OriginalInstruction", Description:="")>
    Public Property OriginalInstruction() As String
      Get
        Return GetProperty(OriginalInstructionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OriginalInstructionProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As FeedbackReportDetail

      Return CType(CType(Me.Parent, FeedbackReportActionList).Parent, FeedbackReportDetail)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportActionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Instruction.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feedback Report Action")
        Else
          Return String.Format("Blank {0}", "Feedback Report Action")
        End If
      Else
        Return Me.Instruction
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      AddWebRule(ActionByProperty, Function(c) c.DelegateAction AndAlso c.ActionBy Is Nothing, Function(c) "Assign A Person To An Action")
      AddWebRule(InstructionProperty, Function(c) c.DelegateAction AndAlso c.ActionBy <> c.DelegatedBy AndAlso c.Instruction = "", Function(c) "An Instruction is required")
      AddWebRule(ActionCommentsProperty, Function(c) Not c.DelegateAction AndAlso c.ActionComments = "" Or c.DelegateAction And c.ActionBy = c.DelegatedBy And c.ActionComments = "", Function(c) "An Action Taken is required")
      'AddWebRule(ActionCommentsProperty, Function(c) c.DelegateAction And c.ActionBy = c.DelegatedBy And c.ActionComments = "", Function(c) "An Action Taken is required")
      AddWebRule(ActionTakenDateProperty, Function(c) Not c.DelegateAction And c.ActionTakenDate Is Nothing Or c.DelegateAction AndAlso c.ActionBy = c.DelegatedBy AndAlso c.ActionTakenDate Is Nothing, Function(c) "Action Taken Date is required")
      'AddWebRule(ActionByProperty, Function(c) c.DelegateAction AndAlso c.ActionBy = c.DelegatedBy, Function(c) "Action by  must be a different person")
    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedbackReportAction() method.

    End Sub

    Public Shared Function NewFeedbackReportAction() As FeedbackReportAction

      Return DataPortal.CreateChild(Of FeedbackReportAction)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetFeedbackReportAction(dr As SafeDataReader) As FeedbackReportAction

      Dim f As New FeedbackReportAction()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedbackReportActionIDProperty, .GetInt32(0))
          LoadProperty(FeedbackReportDetailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DelegateActionProperty, .GetBoolean(2))
          LoadProperty(DelegatedByProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ActionByProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(InstructionProperty, .GetString(5))
          LoadProperty(ActionByDateProperty, .GetValue(6))
          LoadProperty(ActionCommentsProperty, .GetString(7))
          LoadProperty(ActionTakenDateProperty, .GetValue(8))
          LoadProperty(ConfidentialProperty, .GetBoolean(9))
          LoadProperty(MarkedConfidentialByProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(CreatedByProperty, .GetInt32(11))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(12))
          OriginalInstruction = Instruction
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, FeedbackReportActionIDProperty)

      cm.Parameters.AddWithValue("@FeedbackReportDetailID", Me.GetParent().FeedbackReportDetailID) 'GetProperty(FeedbackReportDetailIDProperty)
      cm.Parameters.AddWithValue("@DelegateAction", GetProperty(DelegateActionProperty))
      cm.Parameters.AddWithValue("@DelegatedBy", Singular.Misc.NothingDBNull(GetProperty(DelegatedByProperty)))
      cm.Parameters.AddWithValue("@ActionBy", GetProperty(ActionByProperty))
      cm.Parameters.AddWithValue("@Instruction", GetProperty(InstructionProperty))
      cm.Parameters.AddWithValue("@ActionByDate", ActionByDate)
      cm.Parameters.AddWithValue("@ActionComments", GetProperty(ActionCommentsProperty))
      cm.Parameters.AddWithValue("@ActionTakenDate", Singular.Misc.NothingDBNull(ActionTakenDate))
      cm.Parameters.AddWithValue("@Confidential", GetProperty(ConfidentialProperty))
      cm.Parameters.AddWithValue("@MarkedConfidentialBy", Singular.Misc.NothingDBNull(GetProperty(MarkedConfidentialByProperty)))
      cm.Parameters.AddWithValue("@CreatedBy", CurrentUser.UserID)


      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(FeedbackReportActionIDProperty, cm.Parameters("@FeedbackReportActionID").Value)

               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
      SetupDelegateActionEmail()
    End Sub

    Private Sub SetupDelegateActionEmail()

      If DelegateAction And OriginalInstruction.Length = 0 Then

        Dim FeedbackReport = GetParent.GetParent.GetParent.GetParent
        Dim Emaillist As Singular.Emails.EmailList = Singular.Emails.EmailList.NewEmailList
        Dim Email As Singular.Emails.Email = Emaillist.AddNew

        Dim ToEmailAddress As String = CommonData.Lists.ROUserList.GetItem(ActionBy).EmailAddress
        'ROHumanResourceFullList

        With Email
          .Subject = "Delegate Feedback Action "
          .Body = "Please note a feedback instruction has been delegated to you." &
                  " " & vbCrLf &
                  "Report: " & FeedbackReport.ReportName & vbCrLf &
                  "Instruction: " & Instruction & vbCrLf &
                  "Action Date: " & ActionByDate.ToString("dd MMM yyyy") & vbCrLf &
                  " " & vbCrLf &
                  "Please visit My Feedback Action Tab Under Feedback Module For More Information." & vbCrLf &
                  " " & vbCrLf &
                  "Ragards," & vbCrLf &
                  "SOBER"
          .ToEmailAddress = ToEmailAddress
          .FromEmailAddress = OBLib.Security.Settings.OutgoingEmailAddress
          .DateToSend = Now
        End With

        Me.UpdateChild(Emaillist)
      End If
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@FeedbackReportActionID", GetProperty(FeedbackReportActionIDProperty))
    End Sub

#End Region

  End Class

End Namespace