﻿' Generated 11 Nov 2015 14:01 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportSectionSetting
    Inherits OBReadOnlyBase(Of ROFeedbackReportSectionSetting)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportSectionSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportSectionSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeedbackReportSectionSettingID() As Integer
      Get
        Return GetProperty(FeedbackReportSectionSettingIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportSettingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportSettingID, "Feedback Report Setting", Nothing)
    ''' <summary>
    ''' Gets the Feedback Report Setting value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property FeedbackReportSettingID() As Integer?
      Get
        Return GetProperty(FeedbackReportSettingIDProperty)
      End Get
    End Property

    Public Shared ReportSectionGroupProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportSectionGroup, "Report Section Group", "")
    ''' <summary>
    ''' Gets the Report Section Group value
    ''' </summary>
    <Display(Name:="Report Section Group", Description:="Not Required!  If blank, only Section name will be used.  If specified, all Sections with the same group will fall under that Section Group name")>
  Public ReadOnly Property ReportSectionGroup() As String
      Get
        Return GetProperty(ReportSectionGroupProperty)
      End Get
    End Property

    Public Shared ReportSectionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportSection, "Report Section", "")
    ''' <summary>
    ''' Gets the Report Section value
    ''' </summary>
    <Display(Name:="Report Section", Description:="Name of the Report Section")>
  Public ReadOnly Property ReportSection() As String
      Get
        Return GetProperty(ReportSectionProperty)
      End Get
    End Property

    Public Shared SectionOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SectionOrder, "Section Order", 0)
    ''' <summary>
    ''' Gets the Section Order value
    ''' </summary>
    <Display(Name:="Section Order", Description:="The order of the section")>
  Public ReadOnly Property SectionOrder() As Integer
      Get
        Return GetProperty(SectionOrderProperty)
      End Get
    End Property

    Public Shared OverridingFeebackReportCompleteBySpecificDisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OverridingFeebackReportCompleteBySpecificDisciplineID, "Overriding Feeback Report Complete By Specific Discipline", Nothing)
    ''' <summary>
    ''' Gets the Overriding Feeback Report Complete By Specific Discipline value
    ''' </summary>
    <Display(Name:="Overriding Feeback Report Complete By Specific Discipline", Description:="If specified, the person fulfilling this discipline will be required to complete the report")>
  Public ReadOnly Property OverridingFeebackReportCompleteBySpecificDisciplineID() As Integer?
      Get
        Return GetProperty(OverridingFeebackReportCompleteBySpecificDisciplineIDProperty)
      End Get
    End Property

    Public Shared SectionCompletedEmailGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SectionCompletedEmailGroupID, "Section Completed Email Group", Nothing)
    ''' <summary>
    ''' Gets the Section Completed Email Group value
    ''' </summary>
    <Display(Name:="Section Completed Email Group", Description:="If specified, this group will be emailed the details of this section upon completion")>
  Public ReadOnly Property SectionCompletedEmailGroupID() As Integer?
      Get
        Return GetProperty(SectionCompletedEmailGroupIDProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROFeedbackReportSubSectionSettingListProperty As PropertyInfo(Of ROFeedbackReportSubSectionSettingList) = RegisterProperty(Of ROFeedbackReportSubSectionSettingList)(Function(c) c.ROFeedbackReportSubSectionSettingList, "RO Feedback Report Sub Section Setting List")

    <Display(AutoGenerateField:=False)> _
    Public ReadOnly Property ROFeedbackReportSubSectionSettingList() As ROFeedbackReportSubSectionSettingList
      Get
        If GetProperty(ROFeedbackReportSubSectionSettingListProperty) Is Nothing Then
          LoadProperty(ROFeedbackReportSubSectionSettingListProperty, FeedbackReports.ROFeedbackReportSubSectionSettingList.NewROFeedbackReportSubSectionSettingList())
        End If
        Return GetProperty(ROFeedbackReportSubSectionSettingListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportSectionSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ReportSectionGroup

    End Function

    Public Function GetSubSectionItem(FeedbackReportSubSectionSettingID As Integer) As ROFeedbackReportSubSectionSetting

      Dim SubSection As ROFeedbackReportSubSectionSetting = (From r In Me.ROFeedbackReportSubSectionSettingList
                                                       Where r.FeedbackReportSubSectionSettingID = FeedbackReportSubSectionSettingID
                                                       Select r).FirstOrDefault

      Return SubSection

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROFeedbackReportSectionSetting(dr As SafeDataReader) As ROFeedbackReportSectionSetting

      Dim r As New ROFeedbackReportSectionSetting()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedbackReportSectionSettingIDProperty, .GetInt32(0))
        LoadProperty(FeedbackReportSettingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ReportSectionGroupProperty, .GetString(2))
        LoadProperty(ReportSectionProperty, .GetString(3))
        LoadProperty(SectionOrderProperty, .GetInt32(4))
        LoadProperty(OverridingFeebackReportCompleteBySpecificDisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(SectionCompletedEmailGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
      End With

    End Sub

#End Region

  End Class

End Namespace