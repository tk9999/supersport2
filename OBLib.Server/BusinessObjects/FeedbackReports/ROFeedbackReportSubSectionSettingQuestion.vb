﻿' Generated 11 Nov 2015 14:02 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportSubSectionSettingQuestion
    Inherits OBReadOnlyBase(Of ROFeedbackReportSubSectionSettingQuestion)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportSubSectionSettingQuestionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportSubSectionSettingQuestionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeedbackReportSubSectionSettingQuestionID() As Integer
      Get
        Return GetProperty(FeedbackReportSubSectionSettingQuestionIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportSubSectionSettingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportSubSectionSettingID, "Feedback Report Sub Section Setting", Nothing)
    ''' <summary>
    ''' Gets the Feedback Report Sub Section Setting value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property FeedbackReportSubSectionSettingID() As Integer?
      Get
        Return GetProperty(FeedbackReportSubSectionSettingIDProperty)
      End Get
    End Property

    Public Shared Column1Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column1, "Column 1", "")
    ''' <summary>
    ''' Gets the Column 1 value
    ''' </summary>
    <Display(Name:="Column 1", Description:="")>
  Public ReadOnly Property Column1() As String
      Get
        Return GetProperty(Column1Property)
      End Get
    End Property

    Public Shared Column2Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column2, "Column 2", "")
    ''' <summary>
    ''' Gets the Column 2 value
    ''' </summary>
    <Display(Name:="Column 2", Description:="")>
  Public ReadOnly Property Column2() As String
      Get
        Return GetProperty(Column2Property)
      End Get
    End Property

    Public Shared Column3Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column3, "Column 3", "")
    ''' <summary>
    ''' Gets the Column 3 value
    ''' </summary>
    <Display(Name:="Column 3", Description:="")>
  Public ReadOnly Property Column3() As String
      Get
        Return GetProperty(Column3Property)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportSubSectionSettingQuestionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Column1

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROFeedbackReportSubSectionSettingQuestion(dr As SafeDataReader) As ROFeedbackReportSubSectionSettingQuestion

      Dim r As New ROFeedbackReportSubSectionSettingQuestion()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedbackReportSubSectionSettingQuestionIDProperty, .GetInt32(0))
        LoadProperty(FeedbackReportSubSectionSettingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(Column1Property, .GetString(2))
        LoadProperty(Column2Property, .GetString(3))
        LoadProperty(Column3Property, .GetString(4))
      End With

    End Sub

#End Region

  End Class

End Namespace