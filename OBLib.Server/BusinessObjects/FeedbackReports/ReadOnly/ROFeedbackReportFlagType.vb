﻿' Generated 19 Jul 2016 09:46 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportFlagType
    Inherits OBReadOnlyBase(Of ROFeedbackReportFlagType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportFlagTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportFlagTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedbackReportFlagTypeID() As Integer
      Get
        Return GetProperty(FeedbackReportFlagTypeIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportFlagTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FeedbackReportFlagType, "Feedback Report Flag Type", "")
    ''' <summary>
    ''' Gets the Feedback Report Flag Type value
    ''' </summary>
    <Display(Name:="Feedback Report Flag Type", Description:="")>
    Public ReadOnly Property FeedbackReportFlagType() As String
      Get
        Return GetProperty(FeedbackReportFlagTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportFlagTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FeedbackReportFlagType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROFeedbackReportFlagType(dr As SafeDataReader) As ROFeedbackReportFlagType

      Dim r As New ROFeedbackReportFlagType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedbackReportFlagTypeIDProperty, .GetInt32(0))
        LoadProperty(FeedbackReportFlagTypeProperty, .GetString(1))
      End With

    End Sub

#End Region

  End Class

End Namespace