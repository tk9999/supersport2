﻿' Generated 04 Feb 2016 10:57 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROEmailGroupList
    Inherits OBReadOnlyListBase(Of ROEmailGroupList, ROEmailGroup)

#Region " Business Methods "

    Public Function GetItem(EmailGroupID As Integer) As ROEmailGroup

      For Each child As ROEmailGroup In Me
        If child.EmailGroupID = EmailGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Email Groups"

    End Function

    Public Function GetROEmailGroupMember(EmailGroupMemberID As Integer) As ROEmailGroupMember

      Dim obj As ROEmailGroupMember = Nothing
      For Each parent As ROEmailGroup In Me
        obj = parent.ROEmailGroupMemberList.GetItem(EmailGroupMemberID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
       Inherits CriteriaBase(Of Criteria)

      Public Property EmailGroupID As Object 'SectionCompletedEmailGroupID

      Public Sub New(EmailGroupID As Integer) 'SectionCompletedEmailGroupID
        Me.EmailGroupID = EmailGroupID 'SectionCompletedEmailGroupID...SectionCompletedEmailGroupID
      End Sub

      Public Sub New() 
      End Sub

    End Class

    Public Shared Function NewROEmailGroupList() As ROEmailGroupList

      Return New ROEmailGroupList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub
    Public Shared Function GetROEmailGroupList() As ROEmailGroupList

      Return DataPortal.Fetch(Of ROEmailGroupList)(New Criteria())

    End Function

    Public Shared Function GetROEmailGroupList(EmailGroupID As Integer) As ROEmailGroupList

      Return DataPortal.Fetch(Of ROEmailGroupList)(New Criteria(EmailGroupID)) 'SectionCompletedEmailGroupID

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEmailGroup.GetROEmailGroup(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROEmailGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.EmailGroupID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROEmailGroupMemberList.RaiseListChangedEvents = False
          parent.ROEmailGroupMemberList.Add(ROEmailGroupMember.GetROEmailGroupMember(sdr))
          parent.ROEmailGroupMemberList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEmailGroupList"
            cm.Parameters.AddWithValue("@EmailGroupID", crit.EmailGroupID) 'SectionCompletedEmailGroupID
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace