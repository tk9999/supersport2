﻿' Generated 23 Nov 2015 09:43 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportFlagCriteria
    Inherits OBReadOnlyBase(Of ROFeedbackReportFlagCriteria)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportFlagCriteriaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportFlagCriteriaID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    '<Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedbackReportFlagCriteriaID() As Integer
      Get
        Return GetProperty(FeedbackReportFlagCriteriaIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportSubSectionSettingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportSubSectionSettingID, "Feedback Report Sub Section Setting", Nothing)
    ''' <summary>
    ''' Gets the Feedback Report Sub Section Setting value
    ''' </summary>
    <Display(Name:="Feedback Report Sub Section Setting", Description:="")>
  Public ReadOnly Property FeedbackReportSubSectionSettingID() As Integer?
      Get
        Return GetProperty(FeedbackReportSubSectionSettingIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportFlagTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportFlagTypeID, "Feedback Report Flag Type", Nothing)
    ''' <summary>
    ''' Gets the Feedback Report Flag Type value
    ''' </summary>
    <Display(Name:="Feedback Report Flag Type", Description:="Type of flag answers that match this criteria will create")>
  Public ReadOnly Property FeedbackReportFlagTypeID() As Integer?
      Get
        Return GetProperty(FeedbackReportFlagTypeIDProperty)
      End Get
    End Property

    Public Shared FlagRaisedWhenAnyAnswerGivenProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FlagRaisedWhenAnyAnswerGiven, "Flag Raised When Any Answer Given", False)
    ''' <summary>
    ''' Gets the Flag Raised When Any Answer Given value
    ''' </summary>
    <Display(Name:="Flag Raised When Any Answer Given", Description:="Tick indicates that if an answer is specified then a flag will be raised")>
  Public ReadOnly Property FlagRaisedWhenAnyAnswerGiven() As Boolean
      Get
        Return GetProperty(FlagRaisedWhenAnyAnswerGivenProperty)
      End Get
    End Property

    Public Shared FlagRaisedIfAnswer1IsFeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FlagRaisedIfAnswer1IsFeedbackReportDropDownItemID, "Flag Raised If Answer 1Is Feedback Report Drop Down Item", Nothing)
    ''' <summary>
    ''' Gets the Flag Raised If Answer 1Is Feedback Report Drop Down Item value
    ''' </summary>
    <Display(Name:="Flag Raised If Answer 1Is Feedback Report Drop Down Item", Description:="If Can Raise Flag and the selected drop down answer is this, then raise the flag")>
  Public ReadOnly Property FlagRaisedIfAnswer1IsFeedbackReportDropDownItemID() As Integer?
      Get
        Return GetProperty(FlagRaisedIfAnswer1IsFeedbackReportDropDownItemIDProperty)
      End Get
    End Property

    Public Shared FlagRaisedIfAnswer2IsFeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FlagRaisedIfAnswer2IsFeedbackReportDropDownItemID, "Flag Raised If Answer 2Is Feedback Report Drop Down Item", Nothing)
    ''' <summary>
    ''' Gets the Flag Raised If Answer 2Is Feedback Report Drop Down Item value
    ''' </summary>
    <Display(Name:="Flag Raised If Answer 2Is Feedback Report Drop Down Item", Description:="")>
  Public ReadOnly Property FlagRaisedIfAnswer2IsFeedbackReportDropDownItemID() As Integer?
      Get
        Return GetProperty(FlagRaisedIfAnswer2IsFeedbackReportDropDownItemIDProperty)
      End Get
    End Property

    Public Shared FlagRaisedIfAnswerContainsTextProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlagRaisedIfAnswerContainsText, "Flag Raised If Answer Contains Text", "")
    ''' <summary>
    ''' Gets the Flag Raised If Answer Contains Text value
    ''' </summary>
    <Display(Name:="Flag Raised If Answer Contains Text", Description:="If Can Raise Flag and the Comment contains the specified text")>
  Public ReadOnly Property FlagRaisedIfAnswerContainsText() As String
      Get
        Return GetProperty(FlagRaisedIfAnswerContainsTextProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportFlagCriteriaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FlagRaisedIfAnswerContainsText

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROFeedbackReportFlagCriteria(dr As SafeDataReader) As ROFeedbackReportFlagCriteria

      Dim r As New ROFeedbackReportFlagCriteria()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedbackReportFlagCriteriaIDProperty, .GetInt32(0))
        LoadProperty(FeedbackReportSubSectionSettingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(FeedbackReportFlagTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(FlagRaisedWhenAnyAnswerGivenProperty, .GetBoolean(3))
        LoadProperty(FlagRaisedIfAnswer1IsFeedbackReportDropDownItemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(FlagRaisedIfAnswer2IsFeedbackReportDropDownItemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(FlagRaisedIfAnswerContainsTextProperty, .GetString(6))
      End With

    End Sub

#End Region

  End Class

End Namespace