﻿' Generated 04 Feb 2016 10:57 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROEmailGroupMemberList
    Inherits OBReadOnlyListBase(Of ROEmailGroupMemberList, ROEmailGroupMember)

#Region " Parent "

    <NotUndoable()> Private mParent As ROEmailGroup
#End Region

#Region " Business Methods "

    Public Function GetItem(EmailGroupMemberID As Integer) As ROEmailGroupMember

      For Each child As ROEmailGroupMember In Me
        If child.EmailGroupMemberID = EmailGroupMemberID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Email Group Members"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewROEmailGroupMemberList() As ROEmailGroupMemberList

      Return New ROEmailGroupMemberList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace