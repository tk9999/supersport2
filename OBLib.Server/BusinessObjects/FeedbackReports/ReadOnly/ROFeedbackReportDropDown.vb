﻿
' Generated 13 Nov 2015 14:19 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportDropDown
    Inherits OBReadOnlyBase(Of ROFeedbackReportDropDown)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportDropDownIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportDropDownID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedbackReportDropDownID() As Integer
      Get
        Return GetProperty(FeedbackReportDropDownIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportDropDownProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FeedbackReportDropDown, "Feedback Report Drop Down", "")
    ''' <summary>
    ''' Gets the Feedback Report Drop Down value
    ''' </summary>
    <Display(Name:="Feedback Report Drop Down", Description:="Textual Description of the Drop Down")>
    Public ReadOnly Property FeedbackReportDropDown() As String
      Get
        Return GetProperty(FeedbackReportDropDownProperty)
      End Get
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", False)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="Tick indicates that this is a system drop down and cannot be modified")>
    Public ReadOnly Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportDropDownIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FeedbackReportDropDown

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROFeedbackReportDropDown(dr As SafeDataReader) As ROFeedbackReportDropDown

      Dim r As New ROFeedbackReportDropDown()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedbackReportDropDownIDProperty, .GetInt32(0))
        LoadProperty(FeedbackReportDropDownProperty, .GetString(1))
        LoadProperty(SystemIndProperty, .GetBoolean(2))
      End With

    End Sub

#End Region

  End Class
End Namespace