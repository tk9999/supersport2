﻿' Generated 13 Nov 2015 14:19 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If


Namespace FeedbackReports


  <Serializable()> _
  Public Class ROFeedbackReportDropDownItemList
    Inherits OBReadOnlyListBase(Of ROFeedbackReportDropDownItemList, ROFeedbackReportDropDownItem)

#Region " Business Methods "

    Public Function GetItem(FeedbackReportDropDownItemID As Integer) As ROFeedbackReportDropDownItem

      For Each child As ROFeedbackReportDropDownItem In Me
        If child.FeedbackReportDropDownItemID = FeedbackReportDropDownItemID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function RequiresDropDown2(FeedbackReportDropDownID As Integer) As boolean

      For Each child As ROFeedbackReportDropDownItem In Me
        If child.FeedbackReportDropDownID = FeedbackReportDropDownID AndAlso child.DropDown2FeedbackReportDropDownID IsNot Nothing Then
          Return True
        End If
      Next
      Return False

    End Function

    Public Overrides Function ToString() As String

      Return "Feedback Report Drop Down Items"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROFeedbackReportDropDownItemList() As ROFeedbackReportDropDownItemList

      Return New ROFeedbackReportDropDownItemList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROFeedbackReportDropDownItemList() As ROFeedbackReportDropDownItemList

      Return DataPortal.Fetch(Of ROFeedbackReportDropDownItemList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROFeedbackReportDropDownItem.GetROFeedbackReportDropDownItem(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROFeedbackReportDropDownItemList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace