﻿' Generated 04 Feb 2016 10:57 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROEmailGroupMember
    Inherits OBReadOnlyBase(Of ROEmailGroupMember)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EmailGroupMemberIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailGroupMemberID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property EmailGroupMemberID() As Integer
      Get
        Return GetProperty(EmailGroupMemberIDProperty)
      End Get
    End Property

    Public Shared EmailGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EmailGroupID, "Email Group", Nothing)
    ''' <summary>
    ''' Gets the Email Group value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property EmailGroupID() As Integer?
      Get
        Return GetProperty(EmailGroupIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "User", Nothing)
    ''' <summary>
    ''' Gets the User value
    ''' </summary>
    <Display(Name:="User", Description:="")>
  Public ReadOnly Property UserID() As Integer?
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailGroupMemberIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EmailGroupMemberID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROEmailGroupMember(dr As SafeDataReader) As ROEmailGroupMember

      Dim r As New ROEmailGroupMember()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EmailGroupMemberIDProperty, .GetInt32(0))
        LoadProperty(EmailGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
      End With

    End Sub

#End Region

  End Class

End Namespace