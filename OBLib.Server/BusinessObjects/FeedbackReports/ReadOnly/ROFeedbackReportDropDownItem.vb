﻿' Generated 13 Nov 2015 14:19 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If


Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportDropDownItem
    Inherits OBReadOnlyBase(Of ROFeedbackReportDropDownItem)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportDropDownItemID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedbackReportDropDownItemID() As Integer
      Get
        Return GetProperty(FeedbackReportDropDownItemIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportDropDownIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportDropDownID, "Feedback Report Drop Down", Nothing)
    ''' <summary>
    ''' Gets the Feedback Report Drop Down value
    ''' </summary>
    <Display(Name:="Feedback Report Drop Down", Description:="")>
    Public ReadOnly Property FeedbackReportDropDownID() As Integer?
      Get
        Return GetProperty(FeedbackReportDropDownIDProperty)
      End Get
    End Property

    Public Shared ItemTextProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ItemText, "Item Text", "")
    ''' <summary>
    ''' Gets the Item Text value
    ''' </summary>
    <Display(Name:="Item Text", Description:="")>
    Public ReadOnly Property ItemText() As String
      Get
        Return GetProperty(ItemTextProperty)
      End Get
    End Property

    Public Shared DropDown2FeedbackReportDropDownIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DropDown2FeedbackReportDropDownID, "Drop Down 2Feedback Report Drop Down", Nothing)
    ''' <summary>
    ''' Gets the Drop Down 2Feedback Report Drop Down value
    ''' </summary>
    <Display(Name:="Drop Down 2Feedback Report Drop Down", Description:="If this drop down item is selected, then Drop Down 2 will display this drop down")>
    Public ReadOnly Property DropDown2FeedbackReportDropDownID() As Integer?
      Get
        Return GetProperty(DropDown2FeedbackReportDropDownIDProperty)
      End Get
    End Property

    Public Shared AllowFreeTextProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AllowFreeText, "Allow Free Text", False)
    ''' <summary>
    ''' Gets the Allow Free Text value
    ''' </summary>
    <Display(Name:="Allow Free Text", Description:="Tick indicates that the user can enter free text")>
    Public ReadOnly Property AllowFreeText() As Boolean
      Get
        Return GetProperty(AllowFreeTextProperty)
      End Get
    End Property

    Public Shared ForceFreeTextProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ForceFreeText, "Force Free Text", False)
    ''' <summary>
    ''' Gets the Force Free Text value
    ''' </summary>
    <Display(Name:="Force Free Text", Description:="Tick indicates that the user must enter free text when selecting this option")>
    Public ReadOnly Property ForceFreeText() As Boolean
      Get
        Return GetProperty(ForceFreeTextProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportDropDownItemIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ItemText

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROFeedbackReportDropDownItem(dr As SafeDataReader) As ROFeedbackReportDropDownItem

      Dim r As New ROFeedbackReportDropDownItem()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedbackReportDropDownItemIDProperty, .GetInt32(0))
        LoadProperty(FeedbackReportDropDownIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ItemTextProperty, .GetString(2))
        LoadProperty(DropDown2FeedbackReportDropDownIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(AllowFreeTextProperty, .GetBoolean(4))
        LoadProperty(ForceFreeTextProperty, .GetBoolean(5))
      End With

    End Sub

#End Region

  End Class

End Namespace