﻿' Generated 21 Dec 2015 08:14 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeebackReportCompleteByType
    Inherits OBReadOnlyBase(Of ROFeebackReportCompleteByType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeebackReportCompleteByTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeebackReportCompleteByTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeebackReportCompleteByTypeID() As Integer
      Get
        Return GetProperty(FeebackReportCompleteByTypeIDProperty)
      End Get
    End Property

    Public Shared FeebackReportCompleteByTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FeebackReportCompleteByType, "Feeback Report Complete By Type", "")
    ''' <summary>
    ''' Gets the Feeback Report Complete By Type value
    ''' </summary>
    <Display(Name:="Feeback Report Complete By Type", Description:="")>
  Public ReadOnly Property FeebackReportCompleteByType() As String
      Get
        Return GetProperty(FeebackReportCompleteByTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeebackReportCompleteByTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FeebackReportCompleteByType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROFeebackReportCompleteByType(dr As SafeDataReader) As ROFeebackReportCompleteByType

      Dim r As New ROFeebackReportCompleteByType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeebackReportCompleteByTypeIDProperty, .GetInt32(0))
        LoadProperty(FeebackReportCompleteByTypeProperty, .GetString(1))
      End With

    End Sub

#End Region

  End Class

End Namespace