﻿' Generated 13 Nov 2015 14:19 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportDropDownList
    Inherits OBReadOnlyListBase(Of ROFeedbackReportDropDownList, ROFeedbackReportDropDown)

#Region " Business Methods "

    Public Function GetItem(FeedbackReportDropDownID As Integer) As ROFeedbackReportDropDown

      For Each child As ROFeedbackReportDropDown In Me
        If child.FeedbackReportDropDownID = FeedbackReportDropDownID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Feedback Report Drop Downs"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROFeedbackReportDropDownList() As ROFeedbackReportDropDownList

      Return New ROFeedbackReportDropDownList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROFeedbackReportDropDownList() As ROFeedbackReportDropDownList

      Return DataPortal.Fetch(Of ROFeedbackReportDropDownList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROFeedbackReportDropDown.GetROFeedbackReportDropDown(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROFeedbackReportDropDownList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class
End Namespace