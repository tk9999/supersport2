﻿' Generated 20 May 2014 16:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace FeedbackReports.ReadOnly

  <Serializable()> _
  Public Class ROProductionFB
    Inherits SingularReadOnlyBase(Of ROProductionFB)


#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="ID"), Key()>
    Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Production Description", "")
    ''' <summary>
    ''' Gets the Production Description value
    ''' </summary>
    <Display(Name:="Production Description", Description:="")>
    Public ReadOnly Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
    End Property

    Public Shared TeamsPlayingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamsPlaying, "Teams Playing", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Teams", Description:="")>
    Public ReadOnly Property TeamsPlaying() As String
      Get
        Return GetProperty(TeamsPlayingProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="The type of production")>
    Public ReadOnly Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventTypeID, "Event Type", Nothing)
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="The type of event that this production is part of (PSL Soccer, Super Rugby)")>
    Public ReadOnly Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
    End Property
     
    Public Shared ShortProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShortProductionDescription, "Production Description", "")
    ''' <summary>
    ''' Gets the Production Description value
    ''' </summary>
    <Display(Name:="Short Production Description", Description:="")>
    Public ReadOnly Property ShortProductionDescription() As String
      Get
        Return GetProperty(ShortProductionDescriptionProperty)
      End Get
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue", Nothing)
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="The venue that this production will be held at")>
    Public ReadOnly Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
    End Property

    Public Shared VenueConfirmedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.VenueConfirmedDate, "Venue Confirmed Date")
    ''' <summary>
    ''' Gets the Venue Confirmed Date value
    ''' </summary>
    <Display(Name:="Venue Confirmed Date", Description:="Tick indicates that the venue has been confirmed. NULL indicates that the venue has not been confirmed")>
    Public ReadOnly Property VenueConfirmedDate As DateTime?
      Get
        Return GetProperty(VenueConfirmedDateProperty)
      End Get
    End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionRefNo, "Ref", "")
    ''' <summary>
    ''' Gets the Production Ref No value
    ''' </summary>
    <Display(Name:="Ref", Description:="")>
    Public ReadOnly Property ProductionRefNo() As String
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
    End Property

    Public Shared ParentProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentProductionID, "Parent Production", Nothing)
    ''' <summary>
    ''' Gets the Parent Production value
    ''' </summary>
    <Display(Name:="Parent Production", Description:="")>
    Public ReadOnly Property ParentProductionID() As Integer?
      Get
        Return GetProperty(ParentProductionIDProperty)
      End Get
    End Property


    Public Shared ReportCompletedByToStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CompletedBy, "Completed By")
    ''' <summary>
    ''' 
    ''' </summary> 
    <Display(Name:="Report Completed by", Description:="")>
    Public ReadOnly Property CompletedBy() As String
      Get
        Return GetProperty(ReportCompletedByToStringProperty)
      End Get
    End Property

    Public Shared ReconciledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ReconciledDate, "Reconciled Date Time")
    ''' <summary> 
    ''' </summary> 
    Public ReadOnly Property ReconciledDate() As DateTime?
      Get
        Return GetProperty(ReconciledDateProperty)
      End Get
    End Property

    <Display(Name:="Reconciled Date", Description:="")>
    Public ReadOnly Property ReconciledDateString As String
      Get
        If ReconciledDate.HasValue Then
          Return ReconciledDate.Value.ToString("dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property


    Public Shared ReportPublishedToStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportPublishedToString, "Report Published By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary> 
    <Display(Name:="Report Published by", Description:="")>
    Public ReadOnly Property ReportPublishedToString() As String
      Get
        Return GetProperty(ReportPublishedToStringProperty)
      End Get
    End Property

    Public Shared ReportPublishedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ReportPublishedDateTime, "Report Published Date Time", RelationshipTypes.None)
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary> 
    Public ReadOnly Property ReportPublishedDateTime() As DateTime?
      Get
        Return GetProperty(ReportPublishedDateTimeProperty)
      End Get
    End Property


    Public Shared SynergyGenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.SynergyGenRefNo, "Gen Ref", Nothing)
    ''' <summary>
    ''' Gets the Synergy Gen Ref No value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:="")>
    Public ReadOnly Property SynergyGenRefNo() As Int64?
      Get
        Return GetProperty(SynergyGenRefNoProperty)
      End Get
    End Property

    Public Shared PlayStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlayStartDateTime, "Play Start Date Time")
    ''' <summary>
    ''' Gets the Play Start Date Time value
    ''' </summary>
    <Display(Name:="Starts", Description:="")>
    Public ReadOnly Property PlayStartDateTime As DateTime?
      Get
        Return GetProperty(PlayStartDateTimeProperty)
      End Get
    End Property

    Public Shared PlayEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlayEndDateTime, "Play End Date Time")
    ''' <summary>
    ''' Gets the Play End Date Time value
    ''' </summary>
    <Display(Name:="Ends", Description:="")>
    Public ReadOnly Property PlayEndDateTime As DateTime?
      Get
        Return GetProperty(PlayEndDateTimeProperty)
      End Get
    End Property

    <Display(Name:="Starts")>
    Public ReadOnly Property PlayStartsString As String
      Get
        If PlayStartDateTime IsNot Nothing Then
          Return PlayStartDateTime.Value.ToString("dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Ends")>
    Public ReadOnly Property PlayEndsString As String
      Get
        If PlayEndDateTime IsNot Nothing Then
          Return PlayEndDateTime.Value.ToString("dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "ProductionType", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "EventType", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Venue/Stadium", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Venue/Stadium", Description:="")>
    Public ReadOnly Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
    End Property


    Public Shared TxStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxStartDateTime, "Tx Start Date Time")
    ''' <summary>
    ''' Gets the Tx Start Date Time value
    ''' </summary>
    <Display(Name:="Tx Start", Description:="Tx Start Date Time")>
    Public ReadOnly Property TxStartDateTime As DateTime?
      Get
        Return GetProperty(TxStartDateTimeProperty)
      End Get
    End Property

    Public Shared TxEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxEndDateTime, "Tx End Date Time")
    ''' <summary>
    ''' Gets the Tx End Date Time value
    ''' </summary>
    <Display(Name:="Tx End", Description:="Tx End Date Time")>
    Public ReadOnly Property TxEndDateTime As DateTime?
      Get
        Return GetProperty(TxEndDateTimeProperty)
      End Get
    End Property

    Public Shared EventManagerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventManager, "Event Manager", "")
    ''' <summary>
    ''' Gets the Event Manager value
    ''' </summary>
    <Display(Name:="Event Manager", Description:="Event Manager of the Production")>
    Public ReadOnly Property EventManager() As String
      Get
        Return GetProperty(EventManagerProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "Production Area Status", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Status", "")
    ''' <summary>
    ''' Gets the ProductionAreaStatus value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
    End Property

    Public Shared ReportCreatedByToStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportCreatedByToString, "Report Created By", "")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Report Created By", Description:="")>
    Public ReadOnly Property ReportCreatedByToString() As String
      Get
        Return GetProperty(ReportCreatedByToStringProperty)
      End Get
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name", "")
    ''' <summary>
    ''' Gets the Vehicle Name value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="")>
    Public ReadOnly Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "ProductionSystemAreaID", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="ProductionSystemAreaID", Description:="")>
    Public ReadOnly Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property



    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Department", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Department", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property


    Public Shared CreationTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreationTypeID, "CreationTypeID", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="CreationTypeID", Description:="")>
    Public ReadOnly Property CreationTypeID() As Integer
      Get
        Return GetProperty(CreationTypeIDProperty)
      End Get
    End Property

    Public Shared PlaceHolderIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.PlaceHolderInd, "PlaceHolder", False)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="PlaceHolder", Description:="")>
    Public ReadOnly Property PlaceHolderInd() As Boolean?
      Get
        Return GetProperty(PlaceHolderIndProperty)
      End Get
    End Property

    Public Shared VisionViewIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.VisionViewInd, "VisionView", False)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="VisionView", Description:="")>
    Public ReadOnly Property VisionViewInd() As Boolean?
      Get
        Return GetProperty(VisionViewIndProperty)
      End Get
    End Property

    Public Shared GraphicSuppliersProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GraphicSuppliers, "Graphics", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Graphics", Description:="")>
    Public ReadOnly Property GraphicSuppliers() As String
      Get
        Return GetProperty(GraphicSuppliersProperty)
      End Get
    End Property

    Public Shared OBFacilitySuppliersProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OBFacilitySuppliers, "OBFacility Suppliers")
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="OBFacility Suppliers", Description:="")>
    Public ReadOnly Property OBFacilitySuppliers() As String
      Get
        Return GetProperty(OBFacilitySuppliersProperty)
      End Get
    End Property

    Public Shared CreatedByUserProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByUser, "Created By User", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Created By", Description:="")>
    Public ReadOnly Property CreatedByUser() As String
      Get
        Return GetProperty(CreatedByUserProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public Shared SystemAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemArea, "Production Area", "")
    ''' <summary>
    ''' Gets the Production Description value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property SystemArea() As String
      Get
        Return GetProperty(SystemAreaProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionFB(dr As SafeDataReader) As ROProductionFB

      Dim r As New ROProductionFB()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionIDProperty, .GetInt32(0))
        LoadProperty(ProductionDescriptionProperty, .GetString(1))
        LoadProperty(TeamsPlayingProperty, .GetString(2))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(VenueConfirmedDateProperty, .GetValue(6))
        LoadProperty(ProductionRefNoProperty, .GetString(7))
        LoadProperty(ParentProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(ReportCompletedByToStringProperty, .GetString(9))
        LoadProperty(ReconciledDateProperty, .GetValue(10))
        LoadProperty(ReportPublishedToStringProperty, .GetString(11))
        LoadProperty(ReportPublishedDateTimeProperty, .GetValue(12))
        LoadProperty(SynergyGenRefNoProperty, ZeroNothing(.GetInt64(13)))
        LoadProperty(TitleProperty, .GetString(14))
        LoadProperty(ProductionTypeProperty, .GetString(15))
        LoadProperty(EventTypeProperty, .GetString(16))
        LoadProperty(ProductionVenueProperty, .GetString(17))
        LoadProperty(EventManagerProperty, .GetString(18))
        LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
        LoadProperty(PlayStartDateTimeProperty, .GetValue(20))
        LoadProperty(PlayEndDateTimeProperty, .GetValue(21))
        LoadProperty(TxStartDateTimeProperty, .GetValue(22))
        LoadProperty(TxEndDateTimeProperty, .GetValue(23))
        LoadProperty(ProductionAreaStatusProperty, .GetString(24))
        LoadProperty(ReportCreatedByToStringProperty, .GetString(25))
        LoadProperty(ProductionSystemAreaIDProperty, ZeroNothing(.GetInt32(27)))
        LoadProperty(SystemIDProperty, .GetInt32(28))
        LoadProperty(ProductionAreaIDProperty, .GetInt32(29))

        LoadProperty(ShortProductionDescriptionProperty, .GetString(30))
        LoadProperty(SystemAreaProperty, .GetString(31)) 


      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace