﻿' Generated 20 May 2014 16:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly

Namespace FeedbackReports.ReadOnly

  <Serializable()> _
  Public Class ROProductionFBList
    Inherits SingularReadOnlyListBase(Of ROProductionFBList, ROProductionFB)
    Implements Singular.Paging.IPagedList


    Private mTotalRecords As Integer = 0
    Public Property StudioInd As Boolean
    Public Property IsManager As Boolean

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property


#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROProductionFB

      For Each child As ROProductionFB In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Productions"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property ProductionServicesInd As Boolean = False
      Public Property ProductionContentInd As Boolean = False

      Public Property OutsideBroadcastInd As Boolean = False
      Public Property StudioInd As Boolean = False

      Public Property OBVan As Boolean = False
      Public Property Externalvan As Boolean = False

      Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "")
      <Display(Name:="Start Date", Description:="")>
      Public Overridable Property StartDateTime As DateTime?
        Get
          Return ReadProperty(StartDateTimeProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateTimeProperty, Value)
        End Set
      End Property

      Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "")
      <Display(Name:="End Date", Description:="")>
      Public Overridable Property EndDateTime As DateTime?
        Get
          Return ReadProperty(EndDateTimeProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateTimeProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", OBLib.Security.Settings.CurrentUser.SystemID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:="") >
      Public Overridable Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Sub-Dept", OBLib.Security.Settings.CurrentUser.HumanResourceID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:="")>
      Public Overridable Property HumanResourceID() As Integer?
        Get
          Return ReadProperty(HumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(HumanResourceIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area is required")>
      Public Overridable Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property



      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Keyword, "")
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="Keyword", Description:="")>
      Public Overridable Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production", Description:="")>
      Public Overridable Property ProductionID() As Integer?
        Get
          Return ReadProperty(ProductionIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionIDProperty, Value)
        End Set
      End Property

      Public Shared FeedbackFilterTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackFilterTypeID, "Feedback Filter", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production", Description:="")>
      Public Overridable Property FeedbackFilterTypeID() As Integer?
        Get
          Return ReadProperty(FeedbackFilterTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(FeedbackFilterTypeIDProperty, Value)
        End Set
      End Property





      Public Shared ProductionRefNoProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionRefNo, "Ref Num", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Ref Num", Description:="")>
      Public Overridable Property ProductionRefNo() As Integer?
        Get
          Return ReadProperty(ProductionRefNoProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionRefNoProperty, Value)
        End Set
      End Property



      <Display(AutoGenerateField:=False),
      Browsable(True)>
      Public Property LoggedInSystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID

      Public Sub New(ProductionTypeID As Integer?, FeedbackFilterTypeID As Integer?,
                     PlayStartDateTime As DateTime?, PlayEndDateTime As DateTime?, SystemID As Integer?,
                     TxDateFrom As DateTime?, TxDateTo As DateTime?, EventManagerID As Integer?, ProductionRefNo As Integer?,
                     NotProductionAreaStatusID As Integer?, ProductionAreaID As Integer?,
                     PageNo As Integer, PageSize As Integer,
                     SortAsc As Boolean, SortColumn As String)

        Me.ProductionID = ProductionID
        Me.FeedbackFilterTypeID = FeedbackFilterTypeID

        Me.StartDateTime = PlayStartDateTime
        Me.EndDateTime = PlayEndDateTime
        Me.SystemID = SystemID
        Me.ProductionRefNo = ProductionRefNo
        Me.ProductionAreaID = ProductionAreaID
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn

      End Sub

      Public Sub New(ProductionID As Integer?)

        Me.ProductionID = ProductionID

      End Sub

      Public Sub New()


      End Sub

    End Class

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class ProductionFBReportCriteria
      Inherits Criteria


      <Browsable(False)>
      Public Overrides Property SystemID() As Integer? = OBLib.Security.Settings.CurrentUser.SystemID

      <Browsable(False)>
      Public Overrides Property ProductionAreaID() As Integer? = OBLib.Security.Settings.CurrentUser.ProductionAreaID

      <Browsable(False)>
      Public Overrides Property ProductionID() As Integer? = Nothing



      Public Sub New(ProductionTypeID As Integer?, FeedbackFilterTypeID As Integer?,
                     PlayStartDateTime As DateTime?, PlayEndDateTime As DateTime?, SystemID As Integer?,
                     TxDateFrom As DateTime?, TxDateTo As DateTime?, EventManagerID As Integer?, ProductionRefNo As Integer?,
                     NotProductionAreaStatusID As Integer?, ProductionAreaID As Integer?,
                     PageNo As Integer, PageSize As Integer,
                     SortAsc As Boolean, SortColumn As String)

        Me.ProductionID = ProductionID

        Me.StartDateTime = PlayStartDateTime
        Me.EndDateTime = PlayEndDateTime
        Me.SystemID = SystemID
        Me.FeedbackFilterTypeID = FeedbackFilterTypeID
        Me.ProductionRefNo = ProductionRefNo
        Me.ProductionAreaID = ProductionAreaID
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn

      End Sub

      Public Sub New(ProductionID As Integer?)

        Me.ProductionID = ProductionID

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionFBList() As ROProductionFBList

      Return New ROProductionFBList()

    End Function

    Public Shared Sub BeginGetROProductionFBList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionFBList)))

      Dim dp As New DataPortal(Of ROProductionFBList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionFBList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionFBList)))

      Dim dp As New DataPortal(Of ROProductionFBList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionFBList(Optional StartDateTime As DateTime? = Nothing, Optional EndDateTime As DateTime? = Nothing) As ROProductionFBList
      Dim Crit As Criteria = New Criteria
      Crit.PageNo = 1
      Crit.PageSize = 20
      Crit.SortAsc = True
      Crit.SortColumn = "TxStartDateTime"
      Crit.StartDateTime = StartDateTime
      Crit.EndDateTime = EndDateTime
      Return DataPortal.Fetch(Of ROProductionFBList)(Crit)

    End Function

    'Public Shared Function GetROProductionFBList() As ROProductionFBList

    '  Return DataPortal.Fetch(Of ROProductionFBList)(New Criteria())

    'End Function

    Public Shared Function GetROProductionFBList(ProductionID As Integer) As ROProductionFBList

      Return DataPortal.Fetch(Of ROProductionFBList)(New Criteria(ProductionID))

    End Function

    'Public Shared Function GetROProductionFBList(ProductionTypeID As Integer?, EventTypeID As Integer?,
    '                                           ProductionVenueID As Integer?, SynergyGenRefNo As Int64?, ProductionGradeID As Integer?,
    '                                           PlayStartDateTime As DateTime?, PlayEndDateTime As DateTime?, SystemID As Integer?,
    '                                           TxDateFrom As DateTime?, TxDateTo As DateTime?, EventManagerID As Integer?,
    '                                           ProductionRefNo As Integer?, NotProductionAreaStatusID As Integer?, ProductionAreaID As Integer?,
    '                                           PageNo As Integer, PageSize As Integer,
    '                                           SortAsc As Boolean, SortColumn As String) As ROProductionFBList

    '  Return DataPortal.Fetch(Of ROProductionFBList)(New Criteria(ProductionTypeID, EventTypeID, ProductionVenueID, SynergyGenRefNo,
    '                                                            ProductionGradeID, PlayStartDateTime, PlayEndDateTime, SystemID,
    '                                                            TxDateFrom, TxDateTo, EventManagerID, ProductionRefNo, NotProductionAreaStatusID, ProductionAreaID,
    '                                                            PageNo, PageSize,
    '                                                            SortAsc, SortColumn))

    'End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      StudioInd = sdr.GetBoolean(1)
      IsManager = sdr.GetBoolean(2)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionFB.GetROProductionFB(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionFBList"

            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@StartDateTime", NothingDBNull(crit.StartDateTime))
            cm.Parameters.AddWithValue("@EndDateTime", NothingDBNull(crit.EndDateTime))
            cm.Parameters.AddWithValue("@FeedbackFilterTypeID", NothingDBNull(crit.FeedbackFilterTypeID))
            cm.Parameters.AddWithValue("@ProductionRefNo", NothingDBNull(crit.ProductionRefNo))
            cm.Parameters.AddWithValue("@Keyword", Singular.Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUser.UserID)
            cm.Parameters.AddWithValue("@HumanResourceID", OBLib.Security.Settings.CurrentUser.HumanResourceID)

            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace