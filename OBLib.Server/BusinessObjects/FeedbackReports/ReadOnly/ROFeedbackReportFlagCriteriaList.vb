﻿' Generated 23 Nov 2015 09:43 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROFeedbackReportFlagCriteriaList
    Inherits OBReadOnlyListBase(Of ROFeedbackReportFlagCriteriaList, ROFeedbackReportFlagCriteria)

#Region " Business Methods "

    Public Function GetItem(FeedbackReportFlagCriteriaID As Integer) As ROFeedbackReportFlagCriteria

      For Each child As ROFeedbackReportFlagCriteria In Me
        If child.FeedbackReportFlagCriteriaID = FeedbackReportFlagCriteriaID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    'Public Function GetROFeedbackReportFlagCriteria(FeedbackReportFlagCriteriaID As Integer) As ROFeedbackReportFlagCriteria

    '  Dim obj As ROFeedbackReportFlagCriteria = Nothing
    '  For Each parent As ROFeedbackReportSubSectionSetting In GetObject.ROFeedbackReporty
    '    obj = parent.ROFeedbackReportFlagCriteriaList.GetItem(FeedbackReportFlagCriteriaID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    Public Overrides Function ToString() As String

      Return "Feedback Report Flag Criterias"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Object

      Public Sub New()


      End Sub

      Public Sub New(ProductionID As Integer)

        Me.ProductionID = ProductionID

      End Sub

    End Class

    Public Shared Function NewROFeedbackReportFlagCriteriaList() As ROFeedbackReportFlagCriteriaList

      Return New ROFeedbackReportFlagCriteriaList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROFeedbackReportFlagCriteriaList(ProductionID As Integer) As ROFeedbackReportFlagCriteriaList

      Return DataPortal.Fetch(Of ROFeedbackReportFlagCriteriaList)(New Criteria(ProductionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROFeedbackReportFlagCriteria.GetROFeedbackReportFlagCriteria(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROFeedbackReportFlagCriteriaList"
            cm.Parameters.AddWithValue("@ProductionID", crit.ProductionID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace