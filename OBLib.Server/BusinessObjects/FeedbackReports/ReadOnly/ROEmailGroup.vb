﻿' Generated 04 Feb 2016 10:57 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class ROEmailGroup
    Inherits OBReadOnlyBase(Of ROEmailGroup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EmailGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EmailGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property EmailGroupID() As Integer
      Get
        Return GetProperty(EmailGroupIDProperty)
      End Get
    End Property

    Public Shared EmailGroupProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailGroup, "Email Group", "")
    ''' <summary>
    ''' Gets the Email Group value
    ''' </summary>
    <Display(Name:="Email Group", Description:="Name of email group")>
  Public ReadOnly Property EmailGroup() As String
      Get
        Return GetProperty(EmailGroupProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROEmailGroupMemberListProperty As PropertyInfo(Of ROEmailGroupMemberList) = RegisterProperty(Of ROEmailGroupMemberList)(Function(c) c.ROEmailGroupMemberList, "RO Email Group Member List")

    Public ReadOnly Property ROEmailGroupMemberList() As ROEmailGroupMemberList
      Get
        If GetProperty(ROEmailGroupMemberListProperty) Is Nothing Then
          LoadProperty(ROEmailGroupMemberListProperty, FeedbackReports.ROEmailGroupMemberList.NewROEmailGroupMemberList())
        End If
        Return GetProperty(ROEmailGroupMemberListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EmailGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EmailGroup

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROEmailGroup(dr As SafeDataReader) As ROEmailGroup

      Dim r As New ROEmailGroup()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EmailGroupIDProperty, .GetInt32(0))
        LoadProperty(EmailGroupProperty, .GetString(1))
      End With

    End Sub

#End Region

  End Class

End Namespace