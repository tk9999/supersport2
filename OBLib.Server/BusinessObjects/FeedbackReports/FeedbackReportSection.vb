﻿
' Generated 12 Nov 2015 08:41 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Security
Imports OBLib.Emails
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib
Imports OBLib.FeedbackReports
Imports OBLib.Productions.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class FeedbackReportSection
    Inherits OBBusinessBase(Of FeedbackReportSection)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportSectionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportSectionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedbackReportSectionID() As Integer
      Get
        Return GetProperty(FeedbackReportSectionIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportID, "Feedback Report", Nothing)
    ''' <summary>
    ''' Gets the Feedback Report value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property FeedbackReportID() As Integer?
      Get
        Return GetProperty(FeedbackReportIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportSectionSettingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportSectionSettingID, "Feedback Report Section Setting", Nothing)
    ''' <summary>
    ''' Gets and sets the Feedback Report Section Setting value
    ''' </summary>
    <Display(Name:="Feedback Report Section Setting", Description:=""),
    Required(ErrorMessage:="Feedback Report Section Setting required")>
    Public Property FeedbackReportSectionSettingID() As Integer?
      Get
        Return GetProperty(FeedbackReportSectionSettingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedbackReportSectionSettingIDProperty, Value)
      End Set
    End Property

    ''' <summary>
    ''' Creating A Property, To Restrict The Date, From Where The User Can Choose From.
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared MinDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MinDate, "Min Date") '"Completed Date"
    Public Property MinDate As Date = Now

    Public Shared CompletedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CompletedDate, "Completed Date")
    ''' <summary>
    ''' Gets and sets the Completed Date value ''RegisterProperty(Of DateTime?)(Function(c) c.CompletedDate, "Completed Date")
    ''' RegisterProperty(Of Integer?)(Function(c) c.CompletedBy, Nothing)
    ''' RegisterSProperty(Of DateTime?)(Function(c) c.CompletedDate, Nothing) _.AddSetExpression("SetCompletedBy(self)")
    ''' RegisterSProperty(Of DateTime?)(Function(c) c.CompletedDate, "Completed Date").AddSetExpression("SetCompletedBy(self)")
    ''' </summary>
    <Display(Name:="Completed Date", Description:=""),
    Singular.DataAnnotations.DateField(MinDateProperty:="MinDate")>
    Public Property CompletedDate As DateTime?
      Get
        Return GetProperty(CompletedDateProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(CompletedDateProperty, value)
      End Set
    End Property

    Public Shared CompletedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CompletedBy, 0)
    ''' <summary>
    ''' Gets and sets the Completed By value
    ''' RegisterSProperty(Of Integer?)(Function(c) c.CompletedBy, Nothing) _.AddSetExpression("SetCompletedBy(self)")
    ''' </summary>
    <Display(Name:="Completed By", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(ROUserList))>
    Public Property CompletedBy() As Integer?
      Get
        Return GetProperty(CompletedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CompletedByProperty, Value)
      End Set
    End Property

    Public Shared SectionCompletedEmailGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SectionCompletedEmailGroupID, 0)
    Public ReadOnly Property SectionCompletedEmailGroupID() As Integer?
      Get
        Return GetProperty(SectionCompletedEmailGroupIDProperty)
      End Get

    End Property

    Public Shared OriginalDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalDate, "Original Date")

    <Display(Name:="Original Date", Description:="")>
    Public Property OriginalDate As DateTime?
      Get
        Return GetProperty(OriginalDateProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(OriginalDateProperty, value)
      End Set
    End Property
#End Region

#Region "Expanded"
    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#End Region

#Region " SectionSettings "
    Private mFeedbackReportSectionSettings As ROFeedbackReportSectionSetting

    Public ReadOnly Property SectionSettings() As ROFeedbackReportSectionSetting
      Get
        If mFeedbackReportSectionSettings Is Nothing Then
          mFeedbackReportSectionSettings = GetParent.ReportSettings.GetSectionItem(FeedbackReportSectionSettingID)
        End If
        Return mFeedbackReportSectionSettings
      End Get
    End Property


    'Public FeedbackReportSubSectionSettingID As Integer
    'Private mFeedbackReportSubSectionSettings As ROFeedbackReportSubSectionSetting
    'Public ReadOnly Property SubSectionSettings() As ROFeedbackReportSubSectionSetting
    '  Get
    '    If mFeedbackReportSubSectionSettings Is Nothing OrElse FeedbackReportSubSectionSettingID <> mFeedbackReportSubSectionSettings.FeedbackReportSubSectionSettingID Then
    '      mFeedbackReportSubSectionSettings = SectionSettings.ROFeedbackReportSubSectionSettingList.GetItem(FeedbackReportSubSectionSettingID)
    '    End If
    '    Return mFeedbackReportSubSectionSettings
    '  End Get
    'End Property





    Public Shared ReportSectionGroupProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportSectionGroup, "Report Section Group", "")
    ''' <summary>
    ''' Gets the Report Section Group value
    ''' </summary>
    <Display(Name:="Section Group", Description:="Not Required!  If blank, only Section name will be used.  If specified, all Sections with the same group will fall under that Section Group name")>
    Public ReadOnly Property ReportSectionGroup() As String
      Get
        Return SectionSettings.ReportSectionGroup
      End Get
    End Property

    Public Shared ReportSectionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportSection, "Report Section", "")
    ''' <summary>
    ''' Gets the Report Section value
    ''' </summary>
    <Display(Name:="Section", Description:="Name of the Report Section")>
    Public ReadOnly Property ReportSection() As String
      Get
        Return SectionSettings.ReportSection
      End Get
    End Property


#End Region

#Region " Child Lists "

    Public Shared FeedbackReportDetailListProperty As PropertyInfo(Of FeedbackReportDetailList) = RegisterProperty(Of FeedbackReportDetailList)(Function(c) c.FeedbackReportDetailList, "Feedback Report Detail List")

    '<Display(AutoGenerateField:=False)> _
    Public ReadOnly Property FeedbackReportDetailList() As FeedbackReportDetailList
      Get
        If GetProperty(FeedbackReportDetailListProperty) Is Nothing Then
          LoadProperty(FeedbackReportDetailListProperty, FeedbackReports.FeedbackReportDetailList.NewFeedbackReportDetailList())
        End If
        Return GetProperty(FeedbackReportDetailListProperty)
      End Get
    End Property

    Public Shared FeedbackReportSubSectionListProperty As PropertyInfo(Of FeedbackReportSubSectionList) = RegisterProperty(Of FeedbackReportSubSectionList)(Function(c) c.FeedbackReportSubSectionList, "Feedback Report Sub Section List")

    '<Display(AutoGenerateField:=False)> _
    Public ReadOnly Property FeedbackReportSubSectionList() As FeedbackReportSubSectionList
      Get
        If GetProperty(FeedbackReportSubSectionListProperty) Is Nothing Then
          LoadProperty(FeedbackReportSubSectionListProperty, FeedbackReports.FeedbackReportSubSectionList.NewFeedbackReportSubSectionList())
        End If
        Return GetProperty(FeedbackReportSubSectionListProperty)
      End Get
    End Property
#End Region

#Region " Methods "

    Public Function GetParent() As FeedbackReport

      Return CType(CType(Me.Parent, FeedbackReportSectionList).Parent, FeedbackReport)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportSectionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.FeedbackReportSectionID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feedback Report Section")
        Else
          Return String.Format("Blank {0}", "Feedback Report Section")
        End If
      Else
        Return Me.FeedbackReportSectionID.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"FeedbackReportDetails"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedbackReportSection() method.

    End Sub

    Public Shared Function NewFeedbackReportSection() As FeedbackReportSection

      Return DataPortal.CreateChild(Of FeedbackReportSection)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetFeedbackReportSection(dr As SafeDataReader) As FeedbackReportSection

      Dim f As New FeedbackReportSection()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedbackReportSectionIDProperty, .GetInt32(0))
          LoadProperty(FeedbackReportIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(FeedbackReportSectionSettingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CompletedDateProperty, .GetValue(3))
          LoadProperty(CompletedByProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(SectionCompletedEmailGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          OriginalDate = CompletedDate
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, FeedbackReportSectionIDProperty)

      cm.Parameters.AddWithValue("@FeedbackReportID", Me.GetParent().FeedbackReportID)
      cm.Parameters.AddWithValue("@FeedbackReportSectionSettingID", GetProperty(FeedbackReportSectionSettingIDProperty))
      cm.Parameters.AddWithValue("@CompletedDate", Singular.Misc.NothingDBNull(CompletedDate))
      cm.Parameters.AddWithValue("@CompletedBy", Singular.Misc.NothingDBNull(GetProperty(CompletedByProperty)))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(FeedbackReportSectionIDProperty, cm.Parameters("@FeedbackReportSectionID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      UpdateChild(GetProperty(FeedbackReportSubSectionListProperty))


    End Sub

    Private Sub SetupSectionCommpletedEmail()

      '' check for Recipient
      If Me.SectionCompletedEmailGroupID IsNot Nothing Then
        If Not CompletedDate Is Nothing And OriginalDate Is Nothing Then
          Dim ToEmailAddress As String = ""
          Dim Sections As String = ""

          Dim ProductionID As Integer = GetParent.ProductionID
          Dim ToEmailGroup As ROEmailGroup = CommonData.Lists.ROEmailGroupList.GetItem(Me.SectionCompletedEmailGroupID)

          If ToEmailGroup IsNot Nothing AndAlso ToEmailGroup.ROEmailGroupMemberList.Count > 0 Then
            Dim ToUserList As ROUserList = CommonData.Lists.ROUserList
            Dim Emaillist As Singular.Emails.EmailList = Singular.Emails.EmailList.NewEmailList
            Dim Email As Singular.Emails.Email = Emaillist.AddNew()
            '
            Dim ToEmailAddressList As List(Of String) = New List(Of String)

            ToEmailAddress = GetParent.BuildRecipientEmail(ToUserList, ToEmailGroup)
            '
            With Email
              .Subject = "Feedback Report Section Completed : " & Me.ReportSectionGroup
              .Body = "Dear" & vbCrLf & "All" &
                       " " & vbCrLf &
                       "Please note that The " &
                       Me.ReportSectionGroup & " Report Section has been Completed by " & ToUserList.GetItem(CompletedBy).LoginName & "." & vbCrLf &
                       "This email was sent to inform you about the Completion of the Section" & vbCrLf &
                       " " & vbCrLf &
                       "Reqards, " & vbCrLf &
                       "SOBER"
              .ToEmailAddress = ToEmailAddress
              .FromEmailAddress = OBLib.Security.Settings.OutgoingEmailAddress
              .DateToSend = Now
            End With

            Me.UpdateChild(Emaillist)
          End If
        End If
      End If
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@FeedbackReportSectionID", GetProperty(FeedbackReportSectionIDProperty))
    End Sub

#End Region

  End Class

End Namespace