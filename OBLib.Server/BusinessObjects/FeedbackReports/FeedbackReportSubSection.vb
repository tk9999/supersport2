﻿' Generated 16 Nov 2015 17:36 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class FeedbackReportSubSection
    Inherits OBBusinessBase(Of FeedbackReportSubSection)

#Region " Properties and Methods "

#Region " SubSectionSettings "
    Private mFeedbackReportSubSectionSettings As ROFeedbackReportSubSectionSetting

    Public ReadOnly Property SubSectionSettings() As ROFeedbackReportSubSectionSetting
      Get
        If mFeedbackReportSubSectionSettings Is Nothing Then
          mFeedbackReportSubSectionSettings = GetParent.SectionSettings.GetSubSectionItem(FeedbackReportSubSectionSettingID)
        End If
        Return mFeedbackReportSubSectionSettings
      End Get
    End Property

#End Region

#Region " Properties "

    Public Shared FeedbackReportSubSectionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportSubSectionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedbackReportSubSectionID() As Integer
      Get
        Return GetProperty(FeedbackReportSubSectionIDProperty)
      End Get
    End Property


    Public Shared FeedbackReportSubSectionSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportSubSectionSettingID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>

    Public Property FeedbackReportSubSectionSettingID() As Integer
      Get
        Return GetProperty(FeedbackReportSubSectionSettingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(FeedbackReportSubSectionSettingIDProperty, Value)
      End Set
    End Property

    Public Shared FeedbackReportSectionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportSectionID, "Feedback Report Section")
    ''' <summary>
    ''' Gets the Feedback Report Section value
    ''' </summary>
    <Display(Name:="Feedback Report Section", Description:="")>
    Public Property FeedbackReportSectionID() As Integer
      Get
        Return GetProperty(FeedbackReportSectionIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(FeedbackReportSectionIDProperty, Value)
      End Set
    End Property

    Public Shared DetailCountProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.DetailCount, "") '_
    ''.GetExpression("GetFlagDetailsSubSection(self)")
    ''' <summary>
    ''' Gets the Detail Count value
    ''' </summary>
    <Display(Name:="Details", Description:="")>
    Public Property DetailCount() As String
      Get
        Return GetProperty(DetailCountProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DetailCountProperty, Value)
      End Set
    End Property

 

    'Public Shared DetailStringProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.DetailString, "")

    ' ''' <summary>
    ' ''' Gets the Detail Count value
    ' ''' </summary>
    '<Display(Name:="DetailsHTML", Description:="")>
    'Public Property DetailString() As String
    '  Get
    '    Return GetProperty(DetailStringProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(DetailStringProperty, Value)
    '  End Set
    'End Property


    Public Shared FeedbackSystemDetailIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackSystemDetailID, "Feedback System Detail")
    ''' <summary>
    ''' Gets the Feedback System Detail value
    ''' </summary>
    <Display(Name:="Feedback System Detail", Description:="")>
    Public ReadOnly Property FeedbackSystemDetailID() As Integer?
      Get
        Return SubSectionSettings.FeedbackSystemDetailID
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared ReportSubSectionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportSubSection, "Report Sub Section")
    ''' <summary>
    ''' Gets the Report Sub Section value
    ''' </summary>
    <Display(Name:="Sub Section", Description:="")>
    Public ReadOnly Property ReportSubSection() As String
      Get
        Return GetProperty(ReportSubSectionProperty)
      End Get
    End Property

    Public Shared FeedbackReportSubSectionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportSubSectionTypeID, "Feedback Report Sub Section Type")
    ''' <summary>
    ''' Gets the Feedback Report Sub Section Type value
    ''' </summary>
    <Display(Name:="Feedback Report Sub Section Type", Description:="")>
    Public ReadOnly Property FeedbackReportSubSectionTypeID() As Integer
      Get
        Return GetProperty(FeedbackReportSubSectionTypeIDProperty)
      End Get
    End Property

    Public Shared FeedbackReportDropDownIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportDropDownID, "Feedback Report Drop Down")
    ''' <summary>
    ''' Gets the Feedback Report Drop Down value
    ''' </summary>
    <Display(Name:="Feedback Report Drop Down", Description:="")>
    Public ReadOnly Property FeedbackReportDropDownID() As Integer
      Get
        Return GetProperty(FeedbackReportDropDownIDProperty)
      End Get
    End Property

    Public Shared CommentsAllowedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CommentsAllowed, "Comments Allowed", False)
    ''' <summary>
    ''' Gets the Comments Allowed value
    ''' </summary>
    <Display(Name:="Comments Allowed", Description:="")>
    Public ReadOnly Property CommentsAllowed() As Boolean
      Get
        Return GetProperty(CommentsAllowedProperty)
      End Get
    End Property

    Public Shared SubsectionOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SubsectionOrder, "Subsection Order")
    ''' <summary>
    ''' Gets the Subsection Order value
    ''' </summary>
    <Display(Name:="Subsection Order", Description:="")>
    Public ReadOnly Property SubsectionOrder() As Integer
      Get
        Return GetProperty(SubsectionOrderProperty)
      End Get
    End Property

    Public Shared Column1HeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column1Header, "Column 1Header")
    ''' <summary>
    ''' Gets the Column 1Header value
    ''' </summary>
    <Display(Name:="Column 1Header", Description:="")>
    Public ReadOnly Property Column1Header() As String
      Get
        Return GetProperty(Column1HeaderProperty)
      End Get
    End Property

    Public Shared Column2HeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column2Header, "Column 2Header")
    ''' <summary>
    ''' Gets the Column 2Header value
    ''' </summary>
    <Display(Name:="Column 2Header", Description:="")>
    Public ReadOnly Property Column2Header() As String
      Get
        Return GetProperty(Column2HeaderProperty)
      End Get
    End Property

    Public Shared Column3HeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column3Header, "Column 3Header")
    ''' <summary>
    ''' Gets the Column 3Header value
    ''' </summary>
    <Display(Name:="Column 3Header", Description:="")>
    Public ReadOnly Property Column3Header() As String
      Get
        Return GetProperty(Column3HeaderProperty)
      End Get
    End Property

    Public Shared AnswerHeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AnswerHeader, "Answer Header")
    ''' <summary>
    ''' Gets the Answer Header value
    ''' </summary>
    <Display(Name:="Answer Header", Description:="")>
    Public ReadOnly Property AnswerHeader() As String
      Get
        Return GetProperty(AnswerHeaderProperty)
      End Get
    End Property

    Public Shared Answer2HeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Answer2Header, "Answer 2Header")
    ''' <summary>
    ''' Gets the Answer 2Header value
    ''' </summary>
    <Display(Name:="Answer 2Header", Description:="")>
    Public ReadOnly Property Answer2Header() As String
      Get
        If GetProperty(Answer2HeaderProperty) = "" Then
          Return AnswerHeader + " " + "Description"
        End If
        Return GetProperty(Answer2HeaderProperty)
      End Get
    End Property

    Public Shared DefaultAnswerValueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DefaultAnswerValue, "Default Answer Value")
    ''' <summary>
    ''' Gets the Default Answer Value value
    ''' </summary>
    <Display(Name:="Default Answer Value", Description:="")>
    Public ReadOnly Property DefaultAnswerValue() As String
      Get
        Return GetProperty(DefaultAnswerValueProperty)
      End Get
    End Property

    Public Shared DefaultAnswer1FeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DefaultAnswer1FeedbackReportDropDownItemID, "Default Answer 1Feedback Report Drop Down Item")
    ''' <summary>
    ''' Gets the Default Answer 1Feedback Report Drop Down Item value
    ''' </summary>
    <Display(Name:="Default Answer 1Feedback Report Drop Down Item", Description:="")>
    Public ReadOnly Property DefaultAnswer1FeedbackReportDropDownItemID() As Integer
      Get
        Return GetProperty(DefaultAnswer1FeedbackReportDropDownItemIDProperty)
      End Get
    End Property

    Public Shared DefaultAnswer2FeedbackReportDropDownItemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DefaultAnswer2FeedbackReportDropDownItemID, "Default Answer 2Feedback Report Drop Down Item")
    ''' <summary>
    ''' Gets the Default Answer 2Feedback Report Drop Down Item value
    ''' </summary>
    <Display(Name:="Default Answer 2Feedback Report Drop Down Item", Description:="")>
    Public ReadOnly Property DefaultAnswer2FeedbackReportDropDownItemID() As Integer
      Get
        Return GetProperty(DefaultAnswer2FeedbackReportDropDownItemIDProperty)
      End Get
    End Property

    Public Shared CanSpecifyMultipleAnswersProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanSpecifyMultipleAnswers, "Can Specify Multiple Answers", False)
    ''' <summary>
    ''' Gets the Can Specify Multiple Answers value
    ''' </summary>
    <Display(Name:="Can Specify Multiple Answers", Description:="")>
    Public ReadOnly Property CanSpecifyMultipleAnswers() As Boolean
      Get
        Return GetProperty(CanSpecifyMultipleAnswersProperty)
      End Get
    End Property

    Public Shared StopCompletionIfBlankProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.StopCompletionIfBlank, "Stop Completion If Blank", False)
    ''' <summary>
    ''' Gets the Stop Completion If Blank value
    ''' </summary>
    <Display(Name:="Stop Completion If Blank", Description:="")>
    Public ReadOnly Property StopCompletionIfBlank() As Boolean
      Get
        Return GetProperty(StopCompletionIfBlankProperty)
      End Get
    End Property

    Public Shared AutoPopulatedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AutoPopulated, "Auto Populated", False)
    ''' <summary>
    ''' Gets the Auto Populated value
    ''' </summary>
    <Display(Name:="Auto Populated", Description:="")>
    Public ReadOnly Property AutoPopulated() As Boolean
      Get
        Return GetProperty(AutoPopulatedProperty)
      End Get
    End Property

    Public Shared UserCanEditProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UserCanEdit, "User Can Edit", False)
    ''' <summary>
    ''' Gets the User Can Edit value
    ''' </summary>
    <Display(Name:="User Can Edit", Description:="")>
    Public ReadOnly Property UserCanEdit() As Boolean
      Get
        Return GetProperty(UserCanEditProperty)
      End Get
    End Property

    Public Shared UserCanAddProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UserCanAdd, "User Can Add", False)
    ''' <summary>
    ''' Gets the User Can Add value
    ''' </summary>
    <Display(Name:="User Can Add", Description:="")>
    Public ReadOnly Property UserCanAdd() As Boolean
      Get
        Return GetProperty(UserCanAddProperty)
      End Get
    End Property

    Public Shared SystemIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemInd, "System", False)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemInd() As Boolean
      Get
        Return GetProperty(SystemIndProperty)
      End Get
    End Property

    Public Shared isTypeFreeTextProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.isTypeFreeText, "isTypeFreeText", False)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="isTypeFreeText", Description:="")>
    Public ReadOnly Property isTypeFreeText() As Boolean
      Get
        If FeedbackReportSubSectionTypeID = CommonData.Enums.FeedbackReportSubSectionType.FreeText Then
          Return True
        End If
        Return False
      End Get
    End Property

    Public ReadOnly Property isSystemDetail() As Boolean
      Get
        Return FeedbackSystemDetailID IsNot Nothing
  
      End Get
    End Property

    Public Shared RequiresDropDown2Property As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresDropDown2, "RequiresDropDown2", False)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property RequiresDropDown2 As Boolean
      Get
        Return CommonData.Lists.ROFeedbackReportDropDownItemList.RequiresDropDown2(FeedbackReportDropDownID)

      End Get
    End Property

    Public Shared RequiresDropDownProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresDropDown, "RequiresDropDown", False)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="RequiresDropDown", Description:="")>
    Public ReadOnly Property RequiresDropDown As Boolean
      Get
        Return FeedbackReportSubSectionTypeID = CommonData.Enums.FeedbackReportSubSectionType.DropDown

      End Get
    End Property

#End Region

#Region "Expanded"
    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#End Region

#Region " Methods "

    Public Function GetParent() As FeedbackReportSection

      Return CType(CType(Me.Parent, FeedbackReportSubSectionList).Parent, FeedbackReportSection)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportSubSectionSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ReportSubSection.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feedback Report Sub Section")
        Else
          Return String.Format("Blank {0}", "Feedback Report Sub Section")
        End If
      Else
        Return Me.ReportSubSection
      End If

    End Function

#End Region

#End Region

#Region " Child Lists "

    Public Shared FeedbackReportDetailListProperty As PropertyInfo(Of FeedbackReportDetailList) = RegisterProperty(Of FeedbackReportDetailList)(Function(c) c.FeedbackReportDetailList, "Feedback Report Detail List")

    '<Display(AutoGenerateField:=False)> _
    Public ReadOnly Property FeedbackReportDetailList() As FeedbackReportDetailList
      Get
        If GetProperty(FeedbackReportDetailListProperty) Is Nothing Then
          LoadProperty(FeedbackReportDetailListProperty, FeedbackReports.FeedbackReportDetailList.NewFeedbackReportDetailList())
        End If
        Return GetProperty(FeedbackReportDetailListProperty)
      End Get
    End Property

    Public Shared ROFeedbackReportFlagCriteriaListProperty As PropertyInfo(Of ROFeedbackReportFlagCriteriaList) = RegisterProperty(Of ROFeedbackReportFlagCriteriaList)(Function(c) c.ROFeedbackReportFlagCriteriaList, "Feedback Report Flag Criteria List")

    Public ReadOnly Property ROFeedbackReportFlagCriteriaList() As ROFeedbackReportFlagCriteriaList
      Get
        If GetProperty(ROFeedbackReportFlagCriteriaListProperty) Is Nothing Then
          LoadProperty(ROFeedbackReportFlagCriteriaListProperty, FeedbackReports.ROFeedbackReportFlagCriteriaList.NewROFeedbackReportFlagCriteriaList())
        End If
        Return GetProperty(ROFeedbackReportFlagCriteriaListProperty)
      End Get
    End Property

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      'AddWebRule(FeedbackReport.ReportCompletedDateTimeProperty, Function(c) c.StopCompletionIfBlank AndAlso c.AnswerHeader = "" AndAlso c.Answer2Header = "" AndAlso c.DefaultAnswerValue = "" AndAlso c.DefaultAnswer1FeedbackReportDropDownItemID = 0 AndAlso c.DefaultAnswer2FeedbackReportDropDownItemID = 0, Function(c) "Fill in The Blank Fields")

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedbackReportSubSection() method.

    End Sub

    Public Shared Function NewFeedbackReportSubSection() As FeedbackReportSubSection

      Return DataPortal.CreateChild(Of FeedbackReportSubSection)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

  #End Region

  #Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFeedbackReportSubSection(dr As SafeDataReader) As FeedbackReportSubSection

      Dim f As New FeedbackReportSubSection()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedbackReportSubSectionIDProperty, .GetInt32(0))
          LoadProperty(FeedbackReportSectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DetailCountProperty, .GetString(2))
          LoadProperty(ReportSubSectionProperty, .GetString(3))
          LoadProperty(FeedbackReportSubSectionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(FeedbackReportDropDownIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(CommentsAllowedProperty, .GetBoolean(6))
          LoadProperty(SubsectionOrderProperty, .GetInt32(7))
          LoadProperty(Column1HeaderProperty, .GetString(8))
          LoadProperty(Column2HeaderProperty, .GetString(9))
          LoadProperty(Column3HeaderProperty, .GetString(10))
          LoadProperty(AnswerHeaderProperty, .GetString(11))
          LoadProperty(Answer2HeaderProperty, .GetString(12))
          LoadProperty(DefaultAnswerValueProperty, .GetString(13))
          LoadProperty(DefaultAnswer1FeedbackReportDropDownItemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(DefaultAnswer2FeedbackReportDropDownItemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(CanSpecifyMultipleAnswersProperty, .GetBoolean(16))
          LoadProperty(StopCompletionIfBlankProperty, .GetBoolean(17))
          LoadProperty(AutoPopulatedProperty, .GetBoolean(18))
          LoadProperty(UserCanEditProperty, .GetBoolean(19))
          LoadProperty(UserCanAddProperty, .GetBoolean(20))
          LoadProperty(SystemIndProperty, .GetBoolean(21))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insFeedbackReportSubSection"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFeedbackReportSubSection"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)
      ' Sub Section is only for setting
      SaveChildren()


    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delFeedbackReportSubSection"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@FeedbackReportSubSectionSettingID", GetProperty(FeedbackReportSubSectionSettingIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(FeedbackReportDetailListProperty))
      'FeedbackReportDetailList.Save()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace