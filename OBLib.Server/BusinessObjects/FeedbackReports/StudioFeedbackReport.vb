﻿' Generated 23 Nov 2016 10:46 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()>
  Public Class StudioFeedbackReport
    Inherits OBBusinessBase(Of StudioFeedbackReport)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property FeedbackReportID() As Integer
      Get
        Return GetProperty(FeedbackReportIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(FeedbackReportIDProperty, Value)
      End Set
    End Property

    Public Shared FeedbackReportSettingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedbackReportSettingID, "Feedback Report Setting", Nothing)
    ''' <summary>
    ''' Gets and sets the Feedback Report Setting value
    ''' </summary>
    <Display(Name:="Feedback Report Setting", Description:="Links to Report settings"),
    Required(ErrorMessage:="Feedback Template required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROFeedbackReportSettingList))>
    Public Property FeedbackReportSettingID() As Integer?
      Get
        Return GetProperty(FeedbackReportSettingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedbackReportSettingIDProperty, Value)
      End Set
    End Property

    Public Shared ReportDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ReportDate, "Report Date")
    ''' <summary>
    ''' Gets and sets the Report Date value
    ''' </summary>
    <Display(Name:="Report Date", Description:=""),
    Required(ErrorMessage:="Report Date required")>
    Public Property ReportDate As Date
      Get
        Return GetProperty(ReportDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(ReportDateProperty, Value)
      End Set
    End Property

    Public Shared ReportNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportName, "Report Name", "")
    ''' <summary>
    ''' Gets the Report Name value
    ''' </summary>
    <Display(Name:="Report Name", Description:="")>
    Public ReadOnly Property ReportName() As String
      Get
        Return "Studio's Daily Feedback :" + ReportDate.ToString("dd-MMM-yyyy")
      End Get
    End Property

    Public Shared ReportCompletedDateTimeProperty As PropertyInfo(Of Date?) = RegisterProperty(Of Date?)(Function(c) c.ReportCompletedDateTime, "Report Completed Date Time")
    ''' <summary>
    ''' Gets and sets the Report Completed Date Time value
    ''' </summary>
    <Display(Name:="Report Completed Date Time", Description:="")>
    Public Property ReportCompletedDateTime As Date?
      Get
        Return GetProperty(ReportCompletedDateTimeProperty)
      End Get
      Set(ByVal Value As Date?)
        SetProperty(ReportCompletedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ReportCompletedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ReportCompletedBy, "Report Completed By", Nothing)
    ''' <summary>
    ''' Gets and sets the Report Completed By value
    ''' </summary>
    <Display(Name:="Report Completed By", Description:="")>
    Public Property ReportCompletedBy() As Integer?
      Get
        Return GetProperty(ReportCompletedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ReportCompletedByProperty, Value)
      End Set
    End Property

    Public Shared ReportPublishedDateTimeProperty As PropertyInfo(Of Date?) = RegisterProperty(Of Date?)(Function(c) c.ReportPublishedDateTime, "Report Published Date Time")
    ''' <summary>
    ''' Gets and sets the Report Published Date Time value
    ''' </summary>
    <Display(Name:="Report Published Date Time", Description:="")>
    Public Property ReportPublishedDateTime As Date?
      Get
        Return GetProperty(ReportPublishedDateTimeProperty)
      End Get
      Set(ByVal Value As Date?)
        SetProperty(ReportPublishedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ReportPublishedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ReportPublishedBy, "Report Published By", Nothing)
    ''' <summary>
    ''' Gets and sets the Report Published By value
    ''' </summary>
    <Display(Name:="Report Published By", Description:="")>
    Public Property ReportPublishedBy() As Integer?
      Get
        Return GetProperty(ReportPublishedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ReportPublishedByProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Department", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Department", Description:=""),
     Required(ErrorMessage:="Sub-Dept is required"),
     DropDownWeb(GetType(Maintenance.General.ReadOnly.ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID", DropDownType:=DropDownWeb.SelectType.Combo)>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
     Required(ErrorMessage:="Area is required"),
     DropDownWeb(GetType(Maintenance.General.ReadOnly.ROUserSystemAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", ThisFilterMember:="SystemID", DropDownType:=DropDownWeb.SelectType.Combo)>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared IsCompletedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsCompleted, False)
    <Display(Name:="IsCompleted", Description:=""),
     SetExpression("FeedbackReportBO.IsCompletedSet(self)")>
    Public Property IsCompleted() As Boolean
      Get
        Return GetProperty(IsCompletedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsCompletedProperty, value)
      End Set
    End Property

    Public Shared ReportCompletedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportCompletedByName, "Completed By")
    <Display(Name:="Completed by", Description:="")>
    Public Property ReportCompletedByName() As String
      Get
        Return GetProperty(ReportCompletedByNameProperty)
      End Get
      Set(value As String)
        SetProperty(ReportCompletedByNameProperty, value)
      End Set
    End Property

    Public Shared IsPublishedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsPublished, False)
    <Display(Name:="IsPublished", Description:=""),
     SetExpression("FeedbackReportBO.IsPublishedSet(self)")>
    Public Property IsPublished() As Boolean
      Get
        Return GetProperty(IsPublishedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsPublishedProperty, value)
      End Set
    End Property

    Public Shared ReportPublishedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportPublishedByName, "Published By")
    <Display(Name:="Published by", Description:="")>
    Public Property ReportPublishedByName() As String
      Get
        Return GetProperty(ReportPublishedByNameProperty)
      End Get
      Set(value As String)
        SetProperty(ReportPublishedByNameProperty, value)
      End Set
    End Property

    Public Shared IsManagerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsManager, "Is Manager")
    ''' <summary>
    ''' Gets and sets the Is Manager value
    ''' </summary>
    <Display(Name:="Is Manager", Description:=""),
    Required(ErrorMessage:="Is Manager required")>
    Public Property IsManager() As Boolean
      Get
        Return GetProperty(IsManagerProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsManagerProperty, Value)
      End Set
    End Property

    Public Shared SupervisorsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Supervisors, "Supervisors")
    ''' <summary>
    ''' Gets and sets the Supervisors value
    ''' </summary>
    <Display(Name:="Supervisors", Description:="")>
    Public Property Supervisors() As String
      Get
        Return GetProperty(SupervisorsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SupervisorsProperty, Value)
      End Set
    End Property

    Public Shared ReportCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReportCount, "Report Count", 0)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Report Count", Description:="")>
    Public Property ReportCount() As Integer
      Get
        Return GetProperty(ReportCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ReportCountProperty, Value)
      End Set
    End Property

    Public Shared ReconciledCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReconciledCount, "Reconciled Count", 0)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Reconciled Count", Description:="")>
    Public Property ReconciledCount() As Integer
      Get
        Return GetProperty(ReconciledCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ReconciledCountProperty, Value)
      End Set
    End Property

    Public Shared OriginalReportCompletedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.OriginalReportCompletedDateTime, Nothing)
    ''' <summary>
    ''' "Report Completed Date Time" - Default...
    ''' Gets and sets the Report Completed Date Time value
    ''' RegisterSProperty(Of DateTime?)(Function(c) c.CompletedDate, Nothing) _
    ''' </summary>
    <Display(Name:="Report Completed Date Time", Description:="Date and time the report was marked as completed"), Singular.DataAnnotations.DateField(MinDateProperty:="MinDate")>
    Public Property OriginalReportCompletedDateTime As DateTime?
      Get
        Return GetProperty(OriginalReportCompletedDateTimeProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(OriginalReportCompletedDateTimeProperty, value)
      End Set
    End Property

    Public Shared OriginalReportPublishDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.OriginalReportPublishDateTime, Nothing)
    ''' <summary>
    ''' "Report Completed Date Time" - Default...
    ''' Gets and sets the Report Completed Date Time value
    ''' RegisterSProperty(Of DateTime?)(Function(c) c.CompletedDate, Nothing) _
    ''' </summary>
    Public Property OriginalReportPublishDateTime As DateTime?
      Get
        Return GetProperty(OriginalReportPublishDateTimeProperty)
      End Get
      Set(value As DateTime?)
        SetProperty(OriginalReportPublishDateTimeProperty, value)
      End Set
    End Property

#End Region

#Region "Expanded"
    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#End Region

#Region " Child Lists "

    Public Shared StudioFeedbackSectionAreaListProperty As PropertyInfo(Of StudioFeedbackSectionAreaList) = RegisterProperty(Of StudioFeedbackSectionAreaList)(Function(c) c.StudioFeedbackSectionAreaList, "Studio Feedback Section Area List")

    Public ReadOnly Property StudioFeedbackSectionAreaList() As StudioFeedbackSectionAreaList
      Get
        If GetProperty(StudioFeedbackSectionAreaListProperty) Is Nothing Then
          LoadProperty(StudioFeedbackSectionAreaListProperty, FeedbackReports.StudioFeedbackSectionAreaList.NewStudioFeedbackSectionAreaList())
        End If
        Return GetProperty(StudioFeedbackSectionAreaListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Supervisors.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Studio Feedback Report")
        Else
          Return String.Format("Blank {0}", "Studio Feedback Report")
        End If
      Else
        Return Me.Supervisors
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(ReconciledCountProperty)
        .AddTriggerProperties({ReportCountProperty, IsCompletedProperty})
        .AffectedProperties.AddRange({ReportCountProperty, IsCompletedProperty})
        .JavascriptRuleFunctionName = "StudioFeedbackReportBO.ReconciledCountValid"
        .ServerRuleFunction = AddressOf ReconciledCountValid
      End With

    End Sub

    Public Shared Function ReconciledCountValid(FeedbackReport As StudioFeedbackReport) As String

      If FeedbackReport.ReportCount <> FeedbackReport.ReconciledCount Then
        Return "All bookings need to be reconciled"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewStudioFeedbackReport() method.

    End Sub

    Public Shared Function NewStudioFeedbackReport() As StudioFeedbackReport

      Return DataPortal.CreateChild(Of StudioFeedbackReport)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetStudioFeedbackReport(dr As SafeDataReader) As StudioFeedbackReport

      Dim s As New StudioFeedbackReport()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedbackReportIDProperty, .GetInt32(0))
          LoadProperty(ReportDateProperty, .GetValue(1))
          LoadProperty(ReportCompletedDateTimeProperty, .GetValue(2))
          LoadProperty(ReportCompletedByProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ReportPublishedDateTimeProperty, .GetValue(4))
          LoadProperty(ReportPublishedByProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(IsManagerProperty, .GetBoolean(6))
          LoadProperty(SupervisorsProperty, .GetString(7))
          LoadProperty(SystemIDProperty, .GetInt32(8))
          LoadProperty(ProductionAreaIDProperty, .GetInt32(9))
          LoadProperty(FeedbackReportSettingIDProperty, .GetInt32(10))
          LoadProperty(ReportCompletedByNameProperty, .GetString(11))
          LoadProperty(ReportPublishedByNameProperty, .GetString(12))
          LoadProperty(IsCompletedProperty, .GetBoolean(13))
          LoadProperty(IsPublishedProperty, .GetBoolean(14))
          LoadProperty(ReportCountProperty, .GetInt32(15))
          LoadProperty(ReconciledCountProperty, .GetInt32(16))
          OriginalReportCompletedDateTime = ReportCompletedDateTime
          OriginalReportPublishDateTime = ReportPublishedDateTime
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, FeedbackReportIDProperty)

      cm.Parameters.AddWithValue("@ReportDate", ReportDate)
      cm.Parameters.AddWithValue("@ReportCompletedDateTime", Singular.Misc.NothingDBNull(ReportCompletedDateTime))
      cm.Parameters.AddWithValue("@ReportCompletedBy", Singular.Misc.NothingDBNull(GetProperty(ReportCompletedByProperty)))
      cm.Parameters.AddWithValue("@ReportPublishedDateTime", Singular.Misc.NothingDBNull(ReportPublishedDateTime))
      cm.Parameters.AddWithValue("@ReportPublishedBy", Singular.Misc.NothingDBNull(GetProperty(ReportPublishedByProperty)))
      cm.Parameters.AddWithValue("@IsManager", GetProperty(IsManagerProperty))
      cm.Parameters.AddWithValue("@Supervisors", GetProperty(SupervisorsProperty))
      cm.Parameters.AddWithValue("@OriginalReportCompletedDateTime", Singular.Misc.NothingDBNull(OriginalReportCompletedDateTime))
      cm.Parameters.AddWithValue("@OriginalReportPublishedDateTime", Singular.Misc.NothingDBNull(OriginalReportPublishDateTime))
      cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(FeedbackReportIDProperty, cm.Parameters("@FeedbackReportID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(StudioFeedbackSectionAreaListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@FeedbackReportID", GetProperty(FeedbackReportIDProperty))
    End Sub

#End Region

  End Class

End Namespace