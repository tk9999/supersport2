﻿' Generated 23 Nov 2016 10:47 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()>
  Public Class StudioFeedbackSectionArea
    Inherits OBBusinessBase(Of StudioFeedbackSectionArea)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared ParentReportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ParentReportID, "Parent Report")
    ''' <summary>
    ''' Gets and sets the Parent Report value
    ''' </summary>
    <Display(Name:="Parent Report", Description:="")>
    Public Property ParentReportID() As Integer
      Get
        Return GetProperty(ParentReportIDProperty)
      End Get
      Set(ByVal value As Integer)
        SetProperty(ParentReportIDProperty, value)
      End Set
    End Property

    'Public Shared ReportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReportID, "Parent Report")
    ' ''' <summary>
    ' ''' Gets and sets the Parent Report value
    ' ''' </summary>
    '<Display(Name:="Section Report", Description:="")>
    'Public ReadOnly Property ReportID() As Integer
    '  Get
    '    Return GetProperty(ReportIDProperty)
    '  End Get
    'End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal value As String)
        SetProperty(RoomProperty, value)
      End Set
    End Property

    Public Shared BookingsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Bookings, "Bookings")
    ''' <summary>
    ''' Gets and sets the Bookings value
    ''' </summary>
    <Display(Name:="Bookings", Description:="")>
    Public Property Bookings() As Integer
      Get
        Return GetProperty(BookingsProperty)
      End Get
      Set(ByVal value As Integer)
        SetProperty(BookingsProperty, value)
      End Set
    End Property

    Public Shared RowNumProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNum)
    ''' <summary>
    ''' 
    ''' </summary>    
    Public Property RowNum() As Integer
      Get
        Return GetProperty(RowNumProperty)
      End Get
      Set(value As Integer)
        SetProperty(RowNumProperty, value)
      End Set
    End Property

#End Region

#Region "Expanded"
    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#End Region

#Region " Child Lists "

    Public Shared StudioFeedbackSectionListProperty As PropertyInfo(Of StudioFeedbackSectionList) = RegisterProperty(Of StudioFeedbackSectionList)(Function(c) c.StudioFeedbackSectionList, "Studio Feedback Section List")

    Public ReadOnly Property StudioFeedbackSectionList() As StudioFeedbackSectionList
      Get
        If GetProperty(StudioFeedbackSectionListProperty) Is Nothing Then
          LoadProperty(StudioFeedbackSectionListProperty, FeedbackReports.StudioFeedbackSectionList.NewStudioFeedbackSectionList())
        End If
        Return GetProperty(StudioFeedbackSectionListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As StudioFeedbackReport

      Return CType(CType(Me.Parent, StudioFeedbackSectionAreaList).Parent, StudioFeedbackReport)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Room.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Studio Feedback Section Area")
        Else
          Return String.Format("Blank {0}", "Studio Feedback Section Area")
        End If
      Else
        Return Me.Room
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewStudioFeedbackSectionArea() method.

    End Sub

    Public Shared Function NewStudioFeedbackSectionArea() As StudioFeedbackSectionArea

      Return DataPortal.CreateChild(Of StudioFeedbackSectionArea)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetStudioFeedbackSectionArea(dr As SafeDataReader) As StudioFeedbackSectionArea

      Dim s As New StudioFeedbackSectionArea()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RoomIDProperty, .GetInt32(0))
          LoadProperty(ParentReportIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(RoomProperty, .GetString(2))
          LoadProperty(BookingsProperty, .GetInt32(3))
          LoadProperty(RowNumProperty, .GetInt32(4))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, RoomIDProperty)

      cm.Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
      cm.Parameters.AddWithValue("@ParentReportID", GetProperty(ParentReportIDProperty))
      cm.Parameters.AddWithValue("@Room", GetProperty(RoomProperty))
      cm.Parameters.AddWithValue("@Bookings", GetProperty(BookingsProperty))

      Return Sub()
               ''Post Save
               'If Me.IsNew Then
               '  LoadProperty(ProductionSystemAreaIDProperty, cm.Parameters("@ProductionSystemAreaID").Value)
               'End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(StudioFeedbackSectionListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
    End Sub

#End Region

  End Class

End Namespace
