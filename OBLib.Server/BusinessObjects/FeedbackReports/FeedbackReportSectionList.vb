﻿' Generated 12 Nov 2015 08:41 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class FeedbackReportSectionList
    Inherits OBBusinessListBase(Of FeedbackReportSectionList, FeedbackReportSection)

#Region " Business Methods "

    Public Function GetItem(FeedbackReportSectionID As Integer) As FeedbackReportSection

      For Each child As FeedbackReportSection In Me
        If child.FeedbackReportSectionID = FeedbackReportSectionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Feedback Report Sections"

    End Function

    'Public Function GetFeedbackReportDetail(FeedbackReportDetailID As Integer) As FeedbackReportDetail

    '  Dim obj As FeedbackReportDetail = Nothing
    '  For Each parent As FeedbackReportSection In Me
    '    obj = parent.FeedbackReportDetailList.GetItem(FeedbackReportDetailID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

#End Region

#Region " Data Access "

    Public Shared Function NewFeedbackReportSectionList() As FeedbackReportSectionList

      Return New FeedbackReportSectionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace