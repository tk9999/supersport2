﻿' Generated 31 Mar 2017 15:15 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class StudioFeedbackSubSectionList
    Inherits OBBusinessListBase(Of StudioFeedbackSubSectionList, StudioFeedbackSubSection)

#Region " Business Methods "

    Public Function GetItem(FeedbackReportSectionID As Integer) As StudioFeedbackSubSection

      For Each child As StudioFeedbackSubSection In Me
        If child.FeedbackReportSectionID = FeedbackReportSectionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewStudioFeedbackSubSectionList() As StudioFeedbackSubSectionList

      Return New StudioFeedbackSubSectionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetStudioFeedbackSubSectionList() As StudioFeedbackSubSectionList

      Return DataPortal.Fetch(Of StudioFeedbackSubSectionList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(StudioFeedbackSubSection.GetStudioFeedbackSubSection(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcs.getStudioFeedbackSubSectionList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace