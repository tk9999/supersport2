﻿' Generated 14 Mar 2016 10:19 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace FeedbackReports

  <Serializable()> _
  Public Class MyFeedbackAction
    Inherits OBBusinessBase(Of MyFeedbackAction)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedbackReportActionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedbackReportActionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedbackReportActionID() As Integer
      Get
        Return GetProperty(FeedbackReportActionIDProperty)
      End Get
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Production Description")
    ''' <summary>
    ''' Gets the Production Description value
    ''' </summary>
    <Display(Name:="Production Description", Description:="")>
    Public ReadOnly Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
    End Property

    Public Shared ReportDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ReportDate, "Report Date")
    ''' <summary>
    ''' Gets the Report Date value
    ''' </summary>
    <Display(Name:="Report Date", Description:="")>
    Public ReadOnly Property ReportDate As Date
      Get
        Return GetProperty(ReportDateProperty)
      End Get
    End Property

    Public Shared ReportSectionGroupProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportSectionGroup, "Section Group")
    ''' <summary>
    ''' Gets the Report Section Group value
    ''' </summary>
    <Display(Name:="Section Group", Description:="")>
    Public ReadOnly Property ReportSectionGroup() As String
      Get
        Return GetProperty(ReportSectionGroupProperty)
      End Get
    End Property

    Public Shared ReportSubSectionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReportSubSection, "Sub Section")
    ''' <summary>
    ''' Gets the Report Sub Section value
    ''' </summary>
    <Display(Name:="Sub Section", Description:="")>
    Public ReadOnly Property ReportSubSection() As String
      Get
        Return GetProperty(ReportSubSectionProperty)
      End Get
    End Property

    Public Shared DelegatedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DelegatedBy, "Delegated By")
    ''' <summary>
    ''' Gets the Delegated By value
    ''' </summary>
    <Display(Name:="Delegated By", Description:="")>
    Public ReadOnly Property DelegatedBy() As String
      Get
        Return GetProperty(DelegatedByProperty)
      End Get
    End Property

    Public Shared InstructionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Instruction, "Instruction")
    ''' <summary>
    ''' Gets the Instruction value
    ''' </summary>
    <Display(Name:="Instruction", Description:="")>
    Public ReadOnly Property Instruction() As String
      Get
        Return GetProperty(InstructionProperty)
      End Get
    End Property

    Public Shared ActionByDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ActionByDate, "Action Date")
    ''' <summary>
    ''' Gets the Action By Date value
    ''' </summary>
    <Display(Name:="Action Date", Description:="")>
    Public ReadOnly Property ActionByDate As Date
      Get
        Return GetProperty(ActionByDateProperty)
      End Get
    End Property

    Public Shared ActionCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ActionComments, "Action Taken")
    ''' <summary>
    ''' Gets and sets the Action Comments value
    ''' </summary>
    <Display(Name:="Action Taken", Description:="")>
    Public Property ActionComments() As String
      Get
        Return GetProperty(ActionCommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ActionCommentsProperty, Value)
      End Set
    End Property

    Public Shared MinDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.MinDate, "Min Date")
    Public Property MinDate As Date = Now.Date

    Public Shared ActionTakenDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ActionTakenDate, "Action Date")
    ''' <summary>
    ''' Gets the Action Taken Date value
    ''' </summary>
    <Display(Name:="Action Taken Date", Description:=""), Singular.DataAnnotations.DateField(MinDateProperty:="MinDate")>
    Public Property ActionTakenDate As DateTime?
      Get
        Return GetProperty(ActionTakenDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ActionTakenDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(Name:="Created By", Description:="Created By")>
    Public ReadOnly Property CreatedBy() As String
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(Name:="Created Date", Description:="Created Date")>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedbackReportActionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "My Feedback Action")
        Else
          Return String.Format("Blank {0}", "My Feedback Action")
        End If
      Else
        Return Me.ProductionDescription
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewMyFeedbackAction() method.

    End Sub

    Public Shared Function NewMyFeedbackAction() As MyFeedbackAction

      Return DataPortal.CreateChild(Of MyFeedbackAction)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetMyFeedbackAction(dr As SafeDataReader) As MyFeedbackAction

      Dim m As New MyFeedbackAction()
      m.Fetch(dr)
      Return m

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedbackReportActionIDProperty, .GetInt32(0))
          LoadProperty(ProductionDescriptionProperty, .GetString(1))
          LoadProperty(ReportDateProperty, .GetValue(2))
          LoadProperty(ReportSectionGroupProperty, .GetString(3))
          LoadProperty(ReportSubSectionProperty, .GetString(4))
          LoadProperty(DelegatedByProperty, .GetString(5))
          LoadProperty(InstructionProperty, .GetString(6))
          LoadProperty(ActionByDateProperty, .GetValue(7))
          LoadProperty(ActionCommentsProperty, .GetString(8))
          LoadProperty(ActionTakenDateProperty, .GetValue(9))
          LoadProperty(CreatedByProperty, .GetString(10))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, FeedbackReportActionIDProperty)

      'cm.Parameters.AddWithValue("@ProductionDescription", GetProperty(ProductionDescriptionProperty))
      'cm.Parameters.AddWithValue("@ReportDate", ReportDate)
      'cm.Parameters.AddWithValue("@ReportSectionGroup", GetProperty(ReportSectionGroupProperty))
      'cm.Parameters.AddWithValue("@ReportSubSection", GetProperty(ReportSubSectionProperty))
      'cm.Parameters.AddWithValue("@DelegatedBy", GetProperty(DelegatedByProperty))
      'cm.Parameters.AddWithValue("@Instruction", GetProperty(InstructionProperty))
      'cm.Parameters.AddWithValue("@ActionByDate", ActionByDate)
      cm.Parameters.AddWithValue("@ActionComments", GetProperty(ActionCommentsProperty))
      cm.Parameters.AddWithValue("@ActionTakenDate", Singular.Misc.NothingDBNull(ActionTakenDate))
      'cm.Parameters.AddWithValue("@CreatedBy", GetProperty(CreatedByProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(FeedbackReportActionIDProperty, cm.Parameters("@FeedbackReportActionID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@FeedbackReportActionID", GetProperty(FeedbackReportActionIDProperty))
    End Sub

#End Region

  End Class

End Namespace