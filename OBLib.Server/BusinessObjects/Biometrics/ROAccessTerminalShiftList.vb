﻿' Generated 05 Jun 2015 17:17 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class ROAccessTerminalShiftList
    Inherits OBReadOnlyListBase(Of ROAccessTerminalShiftList, ROAccessTerminalShift)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(AccessTransactionID As Integer) As ROAccessTerminalShift

      For Each child As ROAccessTerminalShift In Me
        If child.AccessTransactionID = AccessTransactionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property AccessTerminalGroupID As Integer?
      Public Property HumanResourceShiftID As Integer?

      Public Sub New()


      End Sub

      Public Sub New(AccessTerminalGroupID As Integer, HumanResourceShiftID As Integer?)

        Me.HumanResourceShiftID = HumanResourceShiftID
        Me.AccessTerminalGroupID = AccessTerminalGroupID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROAccessTerminalShiftList() As ROAccessTerminalShiftList

      Return New ROAccessTerminalShiftList()

    End Function

    Public Shared Sub BeginGetROAccessTerminalShiftList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROAccessTerminalShiftList)))

      Dim dp As New DataPortal(Of ROAccessTerminalShiftList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROAccessTerminalShiftList(CallBack As EventHandler(Of DataPortalResult(Of ROAccessTerminalShiftList)))

      Dim dp As New DataPortal(Of ROAccessTerminalShiftList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROAccessTerminalShiftList() As ROAccessTerminalShiftList

      Return DataPortal.Fetch(Of ROAccessTerminalShiftList)(New Criteria())

    End Function

    Public Shared Function GetROAccessTerminalShiftList(AccessTerminalGroupID As Integer, HumanResourceShiftID As Object) As ROAccessTerminalShiftList

      Return DataPortal.Fetch(Of ROAccessTerminalShiftList)(New Criteria(AccessTerminalGroupID, HumanResourceShiftID))

    End Function


    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False


      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROAccessTerminalShift.GetROAccessTerminalShift(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROAccessTerminalShiftList"
            cm.Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.NothingDBNull(crit.HumanResourceShiftID))
            cm.Parameters.AddWithValue("@AccessTerminalGroupID", Singular.Misc.NothingDBNull(crit.AccessTerminalGroupID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

 
  End Class

End Namespace