﻿' Generated 25 Nov 2015 15:04 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class AccessRoomScheduleList
    Inherits OBBusinessListBase(Of AccessRoomScheduleList, AccessRoomSchedule)

#Region " Parent "


#End Region

#Region " Business Methods "

    Public Function GetItem(ProductionSystemAreaID As Integer) As AccessRoomSchedule

      For Each child As AccessRoomSchedule In Me
        If child.ProductionSystemAreaID = ProductionSystemAreaID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "AccessRoomSchedule"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewAccessRoomSchedulelist() As AccessRoomScheduleList

      Return New AccessRoomScheduleList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

  #End Region

  #Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace