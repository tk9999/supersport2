﻿' Generated 22 Jun 2015 16:04 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Shifts.ICR

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class AccessShiftList
    Inherits OBBusinessListBase(Of AccessShiftList, AccessShift)

#Region " Properties "



#End Region

#Region " Business Methods "

    Public ReadOnly Property Length As Integer
      Get
        Return Me.Count
      End Get
    End Property

    Public Function GetItem(AccessShiftID As Integer, ShiftDate As Date) As AccessShift

      If AccessShiftID = 0 Then
        For Each child As AccessShift In Me
          If child.ShiftDate = ShiftDate Then
            Return child
          End If
        Next
      End If

      For Each child As AccessShift In Me

        If child.AccessShiftID = AccessShiftID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItem(ShiftDate As Date) As AccessShift

      For Each child As AccessShift In Me
        If child.ShiftDate = ShiftDate Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Sub CreateHRShifts(SystemTeamID As Integer)
      Try
        For Each child As AccessShift In Me
          'If Singular.Misc.IsNullNothing(child.HumanResourceShiftID, True) Then
          '  If child.IsDirty AndAlso child.IsValid Then
          '    Dim HRShftList As New HumanResourceShiftList
          '    Dim HRShft As HumanResourceShift = HRShftList.AddNew
          '    If child.AccessShiftOptionInd Then
          '      HRShft.HumanResourceID = child.HumanResourceID
          '      HRShft.SystemID = CurrentUser.SystemID
          '      HRShft.ProductionAreaID = CurrentUser.ProductionAreaID
          '      HRShft.ShiftTypeID = child.ShiftTypeID
          '      HRShft.ScheduleDate = child.ShiftStartDateTime
          '      HRShft.StartDateTime = child.ShiftStartDateTime
          '      HRShft.EndDateTime = child.ShiftEndDateTime
          '      ' HRShft.SystemTeamID = SystemTeamID
          '      HRShft.DisciplineID = child.DisciplineID
          '      HRShftList = HRShftList.Save()
          '    End If
          '    child.HumanResourceShiftID = HRShft.HumanResourceShiftID
          '  End If
          'End If
        Next
        Update()
      Catch ex As Exception
        Throw New Exception(ex.Message)
      End Try




    End Sub

    Public Sub AssignMissingShift(accs As AccessShift)
      Dim MissingDataFlag = Me.Where(Function(c) c.AccessFlagID = CommonData.Enums.AccessFlag.Biometrics_Missing AndAlso c.ShiftDate = accs.StartDateTime.Date).FirstOrDefault
      Me.Remove(MissingDataFlag)
      Dim existingShift = Me.Where(Function(c) c.ShiftDate = accs.StartDateTime.Date).FirstOrDefault
      If existingShift Is Nothing Then
        Me.Add(accs)
      End If
    End Sub

    Public Overrides Function ToString() As String

      Return "Biometrics Shifts"

    End Function

    Public ReadOnly Property IsInformational() As Boolean
      Get
        ' Check If all the Shifts are informational
        Dim Total As Integer = Me.Sum(Function(c) c.AccessFlagID)
        Dim TotalSlightly As Integer = CommonData.Enums.AccessFlag.Arrived_Slightly_Late + CommonData.Enums.AccessFlag.Left_Slightly_Early


        If Total = CommonData.Enums.AccessFlag.Arrived_Slightly_Late OrElse _
          Total = CommonData.Enums.AccessFlag.Arrived_Slightly_Late OrElse _
          Total = TotalSlightly Then
          Return True
        End If

        Return False

      End Get
    End Property


#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property AccessGroupID As Integer = OBLib.CommonData.Enums.AccessTerminalGroup.ICR
      Public Property MissingShiftsInd As Boolean = Nothing

      Public Sub New()


      End Sub

      Public Sub New(StartDate As DateTime?, EndDate As DateTime?, SystemID As Integer?, MissingShiftsInd As Boolean)

        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.SystemID = SystemID
        Me.MissingShiftsInd = MissingShiftsInd

      End Sub

    End Class

    <Serializable()> _
    Public Class FlagValues
      Property AccessFlagID As Integer = 0
      Property FlagDescription As String = ""
      Property SyncID As Integer
    End Class

    <Serializable()> _
    Public Class ScheduleValidityCheck
      Property TotalCrew As Integer = 0
      Property StartedEarlyText As String = ""
      Property StartedLateText As String = ""
      Property EndedEarlyText As String = ""
      Property EndedLateText As String = ""
      Property TimeSheetDateToString As String = ""
      Public ReadOnly Property ValidityString()
        Get
          Dim text As String = ""
          text = TimeSheetDateToString + ":" + StartedEarlyText

          If StartedLateText <> "" Then
            If StartedEarlyText <> "" Then
              text = text + "; " + StartedLateText
            Else
              text = text + StartedLateText
            End If
          End If

          If EndedEarlyText <> "" Then
            If StartedEarlyText <> "" OrElse StartedLateText <> "" Then
              text = text + "; " + EndedEarlyText
            Else
              text = text + EndedEarlyText
            End If
          End If

          If EndedLateText <> "" Then
            If EndedEarlyText <> "" OrElse StartedEarlyText <> "" OrElse StartedLateText <> "" Then
              text = text + "; " + EndedLateText
            Else
              text = text + EndedLateText
            End If
          End If

        

          Return text
        End Get
      End Property



    End Class

    Public Shared Function GetShiftFlagValues(AccessShiftID As Integer, StartDate As DateTime?, EndDate As DateTime?, SyncID As Integer, AccessGroupID As Integer) As FlagValues
      Dim mResults As New FlagValues

      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.[GetAccessShiftFlagValues]"
            cm.Parameters.AddWithValue("@AccessShiftID", AccessShiftID)
            cm.Parameters.AddWithValue("@StartDateTime", Singular.Misc.NothingDBNull(StartDate))
            cm.Parameters.AddWithValue("@EndDateTime", Singular.Misc.NothingDBNull(EndDate))
            cm.Parameters.AddWithValue("@AccessGroupID", AccessGroupID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              If sdr.Read Then
                mResults.AccessFlagID = sdr.GetInt32(0)
                mResults.FlagDescription = sdr.GetString(1)

              End If
            End Using
          End Using
        Finally
          cn.Close()
          mResults.SyncID = SyncID
        End Try

      End Using

      Return mResults
    End Function

    Public Shared Function ScheduleTimeCorrect(ProductionID As Integer) As String

      Dim Message As String = ""
      Dim ScheduleValidityCheckList As New List(Of ScheduleValidityCheck)

      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[cmdProcs].[cmdScheduleTimeCorrect]"
            cm.Parameters.AddWithValue("@ProductionID", ProductionID)
            cm.Parameters.AddWithValue("@SystemID", CommonData.Enums.System.ProductionServices)
            cm.Parameters.AddWithValue("@ProductionAreaID", CommonData.Enums.ProductionArea.OB)
            Using sdr As New SafeDataReader(cm.ExecuteReader)

              While sdr.Read
                Dim ScheduleValidityCheck = New ScheduleValidityCheck
                ScheduleValidityCheck.TotalCrew = sdr.GetInt32(0)
                ScheduleValidityCheck.StartedEarlyText = sdr.GetString(1)
                ScheduleValidityCheck.StartedLateText = sdr.GetString(2)
                ScheduleValidityCheck.EndedEarlyText = sdr.GetString(3)
                ScheduleValidityCheck.EndedLateText = sdr.GetString(4)
                ScheduleValidityCheck.TimeSheetDateToString = sdr.GetString(5)

                ScheduleValidityCheckList.Add(ScheduleValidityCheck)
              End While
            End Using
          End Using
          ScheduleValidityCheckList.ForEach(Sub(svc)
                                              Message = Message + "; " + svc.ValidityString
                                            End Sub)


          Message = Message.Trim("; ")
          If ScheduleValidityCheckList.Count > 0 Then
            Message = Message + "; Recommendation: Adjust the schedule accordingly."
          End If
          Return Message
        Finally
          cn.Close()
        End Try
      End Using


    End Function

#Region " Common "

    Public Shared Function NewAccessShiftList() As AccessShiftList

      Return New AccessShiftList()

    End Function

    Public Shared Sub BeginGetAccessShiftList(CallBack As EventHandler(Of DataPortalResult(Of AccessShiftList)))

      Dim dp As New DataPortal(Of AccessShiftList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Sub LoadOBMissingShiftList(StartDate As DateTime, EndDate As DateTime)

      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAccessMissingShiftList"
            cm.Parameters.AddWithValue("@StartDate", StartDate)
            cm.Parameters.AddWithValue("@EndDate", EndDate)
            cm.Parameters.AddWithValue("@SystemID", CommonData.Enums.System.ProductionServices)
            cm.Parameters.AddWithValue("@ProductionAreaID", CommonData.Enums.ProductionArea.OB)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr, CommonData.Enums.AccessTerminalGroup.Outside_Broadcast)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using
    End Sub

    Public Shared Function GetAccessShiftList() As AccessShiftList

      Return DataPortal.Fetch(Of AccessShiftList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader, AccessGroupID As Integer)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AccessShift.GetAccessShift(sdr, AccessGroupID))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAccessShiftList"
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@MissingShiftsInd", crit.MissingShiftsInd)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr, crit.AccessGroupID)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()


      If Singular.Security.HasAccess("ICR", "Can Authorise HR Shift - Supervisor") OrElse _
        Singular.Security.HasAccess("ICR", "Can Authorise HR Shift - Manager") Then

        Me.RaiseListChangedEvents = False
        Try

          ' Then clear the list of deleted objects because they are truly gone now.
          DeletedList.Clear()

          ' Loop through each non-deleted child object and call its Update() method
          For Each Child As AccessShift In Me
            If Child.IsNew Then
              If Child.PlannedTimeChangedReason <> "" And Child.AccessFlagID = CommonData.Enums.AccessFlag.Biometrics_Missing Then
                Child.AccessShiftOptionInd = True

                Child.Insert()
              End If
            Else
              Child.Update()
            End If
          Next
        Finally
          Me.RaiseListChangedEvents = True
        End Try
      End If
    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region



  End Class

End Namespace