﻿' Generated 04 Nov 2015 09:41 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Schedules

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class AccessHRBooking
    Inherits OBBusinessBase(Of AccessHRBooking)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared KeyValueProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.KeyValue, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property KeyValue() As Integer
      Get
        Return GetProperty(KeyValueProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HRBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HRBookingID, "HRBooking")
    ''' <summary>
    ''' This ID Could be FlightID, AccommodationID or RentalCarID, Depending on the Booking Type
    ''' </summary>
    <Display(Name:="HRBooking", Description:="")>
    Public ReadOnly Property HRBookingID() As Integer
      Get
        Return GetProperty(HRBookingIDProperty)
      End Get
    End Property

    Public Shared HRBookingTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HRBookingTypeID, "HR Booking Type")
    ''' <summary>
    ''' Gets and sets the HR Booking Type value
    ''' </summary>
    <Display(Name:="HR Booking Type", Description:="")>
    Public ReadOnly Property HRBookingTypeID() As Integer
      Get
        Return GetProperty(HRBookingTypeIDProperty)
      End Get
    End Property

    Public Shared HRBookingTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRBookingType, "HR Booking Type")
    ''' <summary>
    ''' Gets and sets the HR Booking Type value
    ''' </summary>
    <Display(Name:="HR Booking Type", Description:="")>
    Public Property HRBookingType() As String
      Get
        Return GetProperty(HRBookingTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HRBookingTypeProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
    Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared DriverIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DriverInd, "Driver")
    ''' <summary>
    ''' Gets and sets the Driver value
    ''' </summary>
    <Display(Name:="Driver", Description:="")>
    Public ReadOnly Property DriverInd() As Boolean
      Get
        Return GetProperty(DriverIndProperty)
      End Get
    End Property

    Public Shared CoDriverIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CoDriverInd, "Co Driver")
    ''' <summary>
    ''' Gets and sets the Co Driver value
    ''' </summary>
    <Display(Name:="Co Driver", Description:="")>
    Public ReadOnly Property CoDriverInd() As Boolean
      Get
        Return GetProperty(CoDriverIndProperty)
      End Get
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Cancelled")
    ''' <summary>
    ''' Gets and sets the Co Driver value
    ''' </summary> 
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
        If Value Then
          SetProperty(CancelledByUserIDProperty, CurrentUser.UserID)
          SetProperty(CancelledDateTimeProperty, Now)
        Else
          SetProperty(CancelledByUserIDProperty, Nothing)
          SetProperty(CancelledDateTimeProperty, Nothing)
        End If
      End Set
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDateTime, "Cancelled Date")
    ''' <summary>
    ''' Gets and sets the Flagged Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="")>
    Public Property CancelledDateTime() As DateTime?
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancelledDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
      End Set
    End Property


    Public Shared CancelledByUserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CancelledByUserID, "Access Shift Option By", 0)
    ''' <summary>
    ''' Gets and sets the Access Shift Option By value
    ''' </summary>
    <Display(Name:="Cancelled By", Description:="links to the person who actioned the option")>
    Public Property CancelledByUserID() As Integer
      Get
        Return GetProperty(CancelledByUserIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CancelledByUserIDProperty, Value)
      End Set
    End Property

    Public Shared RentalCarReplacementHRIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RentalCarReplacementHRID, "Rental Car Replacement")
    ''' <summary>
    ''' Gets and sets the HR Booking Type value
    ''' </summary>
    <Display(Name:="Rental Car Replacement", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(HRProductionScheduleList), ValueMember:="HumanResourceID", DisplayMember:="HumanResource")>
    Public Property RentalCarReplacementHRID() As Integer?
      Get
        Return GetProperty(RentalCarReplacementHRIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RentalCarReplacementHRIDProperty, Value)
      End Set
    End Property



#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(KeyValueProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HRBookingType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access HR Booking")
        Else
          Return String.Format("Blank {0}", "Access HR Booking")
        End If
      Else
        Return Me.HRBookingType
      End If

    End Function

    Public Function GetParent() As HRProductionSchedule

      Return CType(CType(Me.Parent, AccessHRBookingList).Parent, HRProductionSchedule)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(CancelledReasonProperty)
        .JavascriptRuleFunctionName = "CheckCancelledReason"
        .ServerRuleFunction = Function(ts)
                                If ts.IsCancelled Then
                                  If ts.CancelledReason = "" Then
                                    Return "Cancelled Reason"
                                  End If
                                End If
                                Return ""
                              End Function

      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessHRBooking() method.

    End Sub

    Public Shared Function NewAccessHRBooking() As AccessHRBooking

      Return DataPortal.CreateChild(Of AccessHRBooking)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessHRBooking(dr As SafeDataReader) As AccessHRBooking

      Dim a As New AccessHRBooking()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(KeyValueProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HRBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(HRBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(HRBookingTypeProperty, .GetString(4))
          LoadProperty(DescriptionProperty, .GetString(5))
          LoadProperty(DriverIndProperty, .GetBoolean(6))
          LoadProperty(CoDriverIndProperty, .GetBoolean(7))
          LoadProperty(CancelledDateTimeProperty, .GetValue(8))
          LoadProperty(CancelledReasonProperty, .GetString(9))
          LoadProperty(CancelledByUserIDProperty, .GetInt32(10))
          LoadProperty(RentalCarReplacementHRIDProperty, .GetInt32(11))

          If CancelledDateTime IsNot Nothing Then
            LoadProperty(IsCancelledProperty, True)
          End If

        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessHRBooking"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessHRBooking"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramKeyValue As SqlParameter = .Parameters.Add("@KeyValue", SqlDbType.Int)
          paramKeyValue.Value = GetProperty(KeyValueProperty)
          If Me.IsNew Then
            paramKeyValue.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@HRBookingID", GetProperty(HRBookingIDProperty))
          .Parameters.AddWithValue("@HRBookingTypeID", GetProperty(HRBookingTypeIDProperty))
          .Parameters.AddWithValue("@HRBookingType", GetProperty(HRBookingTypeProperty))
          .Parameters.AddWithValue("@CancelledDateTime", GetProperty(CancelledDateTimeProperty))
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@CancelledByUserID", GetProperty(CancelledByUserIDProperty))
          .Parameters.AddWithValue("@RentalCarReplacementHRID", GetProperty(RentalCarReplacementHRIDProperty))
          .Parameters.AddWithValue("@ProductionID", GetParent.ProductionID)
          .Parameters.AddWithValue("@SystemID", GetParent.SystemID)
          .Parameters.AddWithValue("@ProductionAreaID", GetParent.ProductionAreaID)


          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(KeyValueProperty, paramKeyValue.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessHRBooking"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@KeyValue", GetProperty(KeyValueProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace