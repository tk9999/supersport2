﻿' Generated 30 May 2016 14:30 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class AccessStudioSupervisor
    Inherits OBBusinessBase(Of AccessStudioSupervisor)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceShiftID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Human Resource")>
    Public ReadOnly Property HumanResourceShiftID() As Integer
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Human Resource")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SupervisorNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupervisorName, "Name")
    ''' <summary>
    ''' Gets the Name value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SupervisorName() As String
      Get
        Return GetProperty(SupervisorNameProperty)
      End Get
    End Property

    Public Shared TimesheetDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.TimesheetDate, "Timesheet Date")
    ''' <summary>
    ''' Gets and sets the Timesheet Date value
    ''' </summary>
    <Display(Name:="Timesheet Date", Description:="")>
    Public Property TimesheetDate() As DateTime
      Get
        Return GetProperty(TimesheetDateProperty)
      End Get
      Set(ByVal Value As DateTime)
        SetProperty(TimesheetDateProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public Property StartDateTime() As DateTime
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the Endt Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
    Public ReadOnly Property EndDateTime() As DateTime
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared ShiftDurationProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShiftDuration, 0)
    ''' <summary>
    ''' Gets and Sets the Number of Weeks value
    ''' </summary>
    <Display(Name:="Shift Duration", Description:="")>
    Public ReadOnly Property ShiftDuration() As Decimal
      Get
        Return GetProperty(ShiftDurationProperty)
      End Get
    End Property

    Public Shared BioStartDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.BioStartDateTime, "Bio Start Date Time")
    ''' <summary>
    ''' Gets the Bio Start Date Time value
    ''' </summary>
    <Display(Name:="Bio Start Date Time", Description:="")>
    Public ReadOnly Property BioStartDateTime() As DateTime
      Get
        Return GetProperty(BioStartDateTimeProperty)
      End Get
    End Property

    Public Shared BioEndDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.BioEndDateTime, "Bio End Date Time")
    ''' <summary>
    ''' Gets the Bio End Date Time value
    ''' </summary>
    <Display(Name:="Bio End Date Time", Description:="")>
    Public ReadOnly Property BioEndDateTime() As DateTime
      Get
        Return GetProperty(BioEndDateTimeProperty)
      End Get
    End Property

    Public Shared AccessFlagIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessFlagID, "Access Flag")
    ''' <summary>
    ''' Gets and sets the Access Flag value
    ''' </summary>
    <Display(Name:="Access Flag", Description:="")>
    Public Property AccessFlagID() As Integer
      Get
        Return GetProperty(AccessFlagIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AccessFlagIDProperty, Value)
      End Set
    End Property

    Public Shared AccessFlagCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessFlagCount, "Access Flag")
    ''' <summary>
    ''' Gets the Access Flag value
    ''' </summary>
    <Display(Name:="Flags", Description:="")>
    Public ReadOnly Property AccessFlagCount() As Integer
      Get
        Return GetFlagCount()
      End Get
    End Property

    Public Shared AccessFlagIDDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccessFlagIDDescription, "Flag Description")
    ''' <summary>
    ''' Gets and sets the Access Flag ID Description value
    ''' </summary>
    <Display(Name:="Flag Description", Description:="")>
    Public Property AccessFlagIDDescription() As String
      Get
        Return GetProperty(AccessFlagIDDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AccessFlagIDDescriptionProperty, Value)
      End Set
    End Property

    Public Shared FlagReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.FlagReason, "") _
                                                                                                .AddSetExpression("FlagReasonChange(self)", False)
    ''' <summary>
    ''' Gets the Human Resource Name value
    ''' </summary>
    <Display(Name:="Flag Reason", Description:="")>
    Public Property FlagReason() As String
      Get
        Return GetProperty(FlagReasonProperty)
      End Get
      Set(value As String)
        SetProperty(FlagReasonProperty, value)
      End Set
    End Property

    Public Shared RoomCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomCount, "Room Count")
    ''' <summary>
    ''' Gets and sets the Staff Count value
    ''' </summary>
    <Display(Name:="Room Count", Description:="")>
    Public ReadOnly Property RoomCount() As Integer
      Get
        Return AccessRoomScheduleList.Count
      End Get
    End Property

    Public Shared AccessShiftIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessShiftID, "Access Shift", 0)
    ''' <summary>
    ''' Gets the Access Shift value
    ''' </summary>
    <Display(Name:="Access Shift", Description:="")>
    Public ReadOnly Property AccessShiftID() As Integer
      Get
        Return GetProperty(AccessShiftIDProperty)
      End Get
    End Property

#End Region

#Region "Expanded"
    <InitialDataOnly, AlwaysClean>
    Public Property Expanded As Boolean = False


    Public Property IsParentShift() As Boolean = True
#End Region

#Region " Child Lists "

    Public Shared AccessRoomScheduleListProperty As PropertyInfo(Of AccessRoomScheduleList) = RegisterProperty(Of AccessRoomScheduleList)(Function(c) c.AccessRoomScheduleList, "Access Studio Shift List")

    Public ReadOnly Property AccessRoomScheduleList() As AccessRoomScheduleList
      Get
        If GetProperty(AccessRoomScheduleListProperty) Is Nothing Then
          LoadProperty(AccessRoomScheduleListProperty, Biometrics.AccessRoomScheduleList.NewAccessRoomSchedulelist())
        End If
        Return GetProperty(AccessRoomScheduleListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Function GetFlagCount() As Object

      Return CommonData.Lists.ROAccessFlagList.GetFlags(AccessFlagID).Count

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SupervisorNameProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SupervisorName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Studio Supervisor")
        Else
          Return String.Format("Blank {0}", "Access Studio Supervisor")
        End If
      Else
        Return Me.SupervisorName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(FlagReasonProperty)
        .JavascriptRuleFunctionName = "CheckTimeChangedReason"
        .Severity = Singular.Rules.RuleSeverity.Warning
        .ServerRuleFunction = Function(ts)
                                If ts.FlagReason = "" AndAlso ts.AccessFlagID > 0 Then
                                  Return "Reason Required"
                                End If

                                Return ""
                              End Function

      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessStudioSupervisor() method.

    End Sub

    Public Shared Function NewAccessStudioSupervisor() As AccessStudioSupervisor

      Return DataPortal.CreateChild(Of AccessStudioSupervisor)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetAccessStudioSupervisor(dr As SafeDataReader) As AccessStudioSupervisor

      Dim a As New AccessStudioSupervisor()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SupervisorNameProperty, .GetString(0))
          LoadProperty(TimesheetDateProperty, .GetDateTime(1))
          LoadProperty(StartDateTimeProperty, .GetDateTime(2))
          LoadProperty(EndDateTimeProperty, .GetDateTime(3))
          LoadProperty(BioStartDateTimeProperty, .GetDateTime(4))
          LoadProperty(BioEndDateTimeProperty, .GetDateTime(5))
          LoadProperty(AccessFlagIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(AccessFlagIDDescriptionProperty, .GetString(7))
          LoadProperty(FlagReasonProperty, .GetString(8))
          LoadProperty(AccessShiftIDProperty, .GetInt32(9))
          LoadProperty(ShiftDurationProperty, .GetDecimal(10))
          LoadProperty(HumanResourceIDProperty, .GetInt32(11))
          LoadProperty(HumanResourceShiftIDProperty, .GetInt32(12))

        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, AccessShiftIDProperty)

      cm.Parameters.AddWithValue("@TimesheetDate", GetProperty(TimesheetDateProperty))
      cm.Parameters.AddWithValue("@StartDateTime", GetProperty(StartDateTimeProperty))
      cm.Parameters.AddWithValue("@EndDateTime", GetProperty(EndDateTimeProperty))
      cm.Parameters.AddWithValue("@BioStartDateTime", GetProperty(BioStartDateTimeProperty))
      cm.Parameters.AddWithValue("@BioEndDateTime", GetProperty(BioEndDateTimeProperty))
      cm.Parameters.AddWithValue("@AccessFlagID", GetProperty(AccessFlagIDProperty))
      cm.Parameters.AddWithValue("@AccessFlagIDDescription", GetProperty(AccessFlagIDDescriptionProperty))
      cm.Parameters.AddWithValue("@PlannedTimeChangedReason", GetProperty(FlagReasonProperty))


      Return Sub()

             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)

    End Sub

#End Region

  End Class

End Namespace