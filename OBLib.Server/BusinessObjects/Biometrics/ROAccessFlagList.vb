﻿' Generated 09 Jun 2015 11:54 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class ROAccessFlagList
    Inherits OBReadOnlyListBase(Of ROAccessFlagList, ROAccessFlag)

#Region " Business Methods "

    Public Function GetItem(AccessFlagID As Integer) As ROAccessFlag

      For Each child As ROAccessFlag In Me
        If child.AccessFlagID = AccessFlagID Then
          Return child
        End If
      Next
      Return Nothing

    End Function 

    Public Function GetFlags(Total As Integer) As List(Of ROAccessFlag)
      Dim List As New List(Of ROAccessFlag)

      If Total <= 0 Then
        Return List
      End If

      For Each child As ROAccessFlag In Me
        If child.AccessFlagID = Total Then
          List.Add(child)
          Return List
        End If
      Next

      Dim NextMaxID = GetNextMaxID(Total)
      List.Add(GetItem(NextMaxID))
      Dim NewTotal = Total - GetNextMaxID(Total)
      If NewTotal <> 0 Then
        List = List.Union(GetFlags(NewTotal).ToList).ToList
      End If
      Return List

    End Function

    Private Function GetNextMaxID(value As Integer) As Integer
      Dim Id As Integer = 0

      If value <= 0 Then
        Return Id
      End If

      Id = Me.Where(Function(c) c.AccessFlagID < value).Max(Function(f) f.AccessFlagID)
      Return Id

    End Function

    Public Overrides Function ToString() As String

      Return "Access Flags"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROAccessFlagList() As ROAccessFlagList

      Return New ROAccessFlagList()

    End Function

    Public Shared Sub BeginGetROAccessFlagList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROAccessFlagList)))

      Dim dp As New DataPortal(Of ROAccessFlagList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROAccessFlagList(CallBack As EventHandler(Of DataPortalResult(Of ROAccessFlagList)))

      Dim dp As New DataPortal(Of ROAccessFlagList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "


    Public Shared Function GetROAccessFlagList(Total As Integer) As ROAccessFlagList

      Dim newList As ROAccessFlagList = New ROAccessFlagList
      If Total > 0 Then
        For Each item As ROAccessFlag In CommonData.Lists.ROAccessFlagList.GetFlags(Total).OrderBy(Function(c) c.AccessFlagID)
          newList.Add(item)
        Next
      End If
       
      Return newList

    End Function

    Public Shared Function GetROAccessFlagList() As ROAccessFlagList

      Return DataPortal.Fetch(Of ROAccessFlagList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROAccessFlag.GetROAccessFlag(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROAccessFlagList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace