﻿' Generated 05 Dec 2014 13:40 - Singular Systems Object Generator Version 2.1.664
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace AccessLogs.[ReadOnly]

  <Serializable()> _
  Public Class ROAccessTerminal
    Inherits Singular.SingularReadOnlyBase(Of ROAccessTerminal)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessTerminalIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessTerminalID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccessTerminalID() As Integer?
      Get
        Return GetProperty(AccessTerminalIDProperty)
      End Get
    End Property

    Public Shared TerminalNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TerminalName, "Terminal Name")
    ''' <summary>
    ''' Gets the Terminal Name value
    ''' </summary>
    <Display(Name:="Terminal Name", Description:="")>
  Public ReadOnly Property TerminalName() As String
      Get
        Return GetProperty(TerminalNameProperty)
      End Get
    End Property

    Public Shared EnabledIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.EnabledInd, "Enabled", False)
    ''' <summary>
    ''' Gets the Enabled value
    ''' </summary>
    <Display(Name:="Enabled", Description:="")>
  Public ReadOnly Property EnabledInd() As Boolean
      Get
        Return GetProperty(EnabledIndProperty)
      End Get
    End Property

    Public Shared LocationNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LocationName, "Location Name")
    ''' <summary>
    ''' Gets the Location Name value
    ''' </summary>
    <Display(Name:="Location Name", Description:="")>
  Public ReadOnly Property LocationName() As String
      Get
        Return GetProperty(LocationNameProperty)
      End Get
    End Property

    Public Shared ZoneNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ZoneName, "Zone Name")
    ''' <summary>
    ''' Gets the Zone Name value
    ''' </summary>
    <Display(Name:="Zone Name", Description:="")>
  Public ReadOnly Property ZoneName() As String
      Get
        Return GetProperty(ZoneNameProperty)
      End Get
    End Property

    Public ReadOnly Property DropDownDisplay As String
      Get
        Return TerminalName & " (" & ZoneName & " - " & LocationName & ")"
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessTerminalIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.TerminalName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAccessTerminal(dr As SafeDataReader) As ROAccessTerminal

      Dim r As New ROAccessTerminal()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AccessTerminalIDProperty, .GetInt32(0))
        LoadProperty(TerminalNameProperty, .GetString(1))
        LoadProperty(EnabledIndProperty, .GetBoolean(2))
        LoadProperty(LocationNameProperty, .GetString(3))
        LoadProperty(ZoneNameProperty, .GetString(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace