﻿' Generated 05 Dec 2014 13:40 - Singular Systems Object Generator Version 2.1.664
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace AccessLogs.[ReadOnly]

  <Serializable()> _
  Public Class ROAccessTerminalList
    Inherits Singular.SingularReadOnlyListBase(Of ROAccessTerminalList, ROAccessTerminal)

#Region " Business Methods "

    Public Function GetItem(AccessTerminalID As Integer) As ROAccessTerminal

      For Each child As ROAccessTerminal In Me
        If child.AccessTerminalID = AccessTerminalID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROAccessTerminalList() As ROAccessTerminalList

      Return New ROAccessTerminalList()

    End Function

    Public Shared Sub BeginGetROAccessTerminalList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROAccessTerminalList)))

      Dim dp As New DataPortal(Of ROAccessTerminalList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROAccessTerminalList(CallBack As EventHandler(Of DataPortalResult(Of ROAccessTerminalList)))

      Dim dp As New DataPortal(Of ROAccessTerminalList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROAccessTerminalList() As ROAccessTerminalList

      Return DataPortal.Fetch(Of ROAccessTerminalList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROAccessTerminal.GetROAccessTerminal(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROAccessTerminalList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace