﻿' Generated 29 May 2015 14:50 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics.Maintenance

  <Serializable()> _
  Public Class AccessLocation
    Inherits OBBusinessBase(Of AccessLocation)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessLocationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessLocationID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccessLocationID() As Integer
      Get
        Return GetProperty(AccessLocationIDProperty)
      End Get
    End Property

    Public Shared AccessZoneIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessZoneID, "Access Zone", Nothing)
    ''' <summary>
    ''' Gets the Access Zone value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property AccessZoneID() As Integer?
      Get
        Return GetProperty(AccessZoneIDProperty)
      End Get
    End Property

    Public Shared ImpronetCTRLSLAProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImpronetCTRLSLA, "Impronet CTRLSLA", "")
    ''' <summary>
    ''' Gets and sets the Impronet CTRLSLA value
    ''' </summary>
    <Display(Name:="Impronet CTRLSLA", Description:=""),
    StringLength(20, ErrorMessage:="Impronet CTRLSLA cannot be more than 20 characters")>
    Public Property ImpronetCTRLSLA() As String
      Get
        Return GetProperty(ImpronetCTRLSLAProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ImpronetCTRLSLAProperty, Value)
      End Set
    End Property

    Public Shared ImpronetKeyProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImpronetKey, "Impronet Key", Nothing)
    ''' <summary>
    ''' Gets and sets the Impronet Key value
    ''' </summary>
    <Display(Name:="Impronet Key", Description:=""),
    Required(ErrorMessage:="Impronet Key required")>
    Public Property ImpronetKey() As Integer
      Get
        Return GetProperty(ImpronetKeyProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ImpronetKeyProperty, Value)
      End Set
    End Property

    Public Shared LocationNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LocationName, "Location Name", "")
    ''' <summary>
    ''' Gets and sets the Location Name value
    ''' </summary>
    <Display(Name:="Location Name", Description:=""),
    StringLength(255, ErrorMessage:="Location Name cannot be more than 255 characters")>
    Public Property LocationName() As String
      Get
        Return GetProperty(LocationNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared AccessTerminalListProperty As PropertyInfo(Of AccessTerminalList) = RegisterProperty(Of AccessTerminalList)(Function(c) c.AccessTerminalList, "Access Terminal List")

    Public ReadOnly Property AccessTerminalList() As AccessTerminalList
      Get
        If GetProperty(AccessTerminalListProperty) Is Nothing Then
          LoadProperty(AccessTerminalListProperty, Biometrics.Maintenance.AccessTerminalList.NewAccessTerminalList())
        End If
        Return GetProperty(AccessTerminalListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As AccessZone

      Return CType(CType(Me.Parent, AccessLocationList).Parent, AccessZone)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessLocationIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ImpronetCTRLSLA.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Location")
        Else
          Return String.Format("Blank {0}", "Access Location")
        End If
      Else
        Return Me.ImpronetCTRLSLA
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"AccessTerminals"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessLocation() method.

    End Sub

    Public Shared Function NewAccessLocation() As AccessLocation

      Return DataPortal.CreateChild(Of AccessLocation)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessLocation(dr As SafeDataReader) As AccessLocation

      Dim a As New AccessLocation()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessLocationIDProperty, .GetInt32(0))
          LoadProperty(AccessZoneIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ImpronetCTRLSLAProperty, .GetString(2))
          LoadProperty(ImpronetKeyProperty, .GetInt32(3))
          LoadProperty(LocationNameProperty, .GetString(4))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessLocation"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessLocation"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessLocationID As SqlParameter = .Parameters.Add("@AccessLocationID", SqlDbType.Int)
          paramAccessLocationID.Value = GetProperty(AccessLocationIDProperty)
          If Me.IsNew Then
            paramAccessLocationID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccessZoneID", Me.GetParent().AccessZoneID)
          .Parameters.AddWithValue("@ImpronetCTRLSLA", GetProperty(ImpronetCTRLSLAProperty))
          .Parameters.AddWithValue("@ImpronetKey", GetProperty(ImpronetKeyProperty))
          .Parameters.AddWithValue("@LocationName", GetProperty(LocationNameProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessLocationIDProperty, paramAccessLocationID.Value)
          End If
          ' update child objects
          If GetProperty(AccessTerminalListProperty) IsNot Nothing Then
            Me.AccessTerminalList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(AccessTerminalListProperty) IsNot Nothing Then
          Me.AccessTerminalList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessLocation"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccessLocationID", GetProperty(AccessLocationIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace