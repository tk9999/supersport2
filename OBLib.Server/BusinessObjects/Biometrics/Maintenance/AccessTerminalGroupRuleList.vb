﻿' Generated 24 Jul 2015 08:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics.Maintenance

  <Serializable()> _
  Public Class AccessTerminalGroupRuleList
    Inherits OBBusinessListBase(Of AccessTerminalGroupRuleList, AccessTerminalGroupRule)

#Region " Business Methods "

    Public Function GetItem(AccessTerminalGroupRuleID As Integer) As AccessTerminalGroupRule

      For Each child As AccessTerminalGroupRule In Me
        If child.AccessTerminalGroupRuleID = AccessTerminalGroupRuleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Access Terminal Group Rules"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewAccessTerminalGroupRuleList() As AccessTerminalGroupRuleList

      Return New AccessTerminalGroupRuleList()

    End Function

    Public Shared Sub BeginGetAccessTerminalGroupRuleList(CallBack As EventHandler(Of DataPortalResult(Of AccessTerminalGroupRuleList)))

      Dim dp As New DataPortal(Of AccessTerminalGroupRuleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAccessTerminalGroupRuleList() As AccessTerminalGroupRuleList

      Return DataPortal.Fetch(Of AccessTerminalGroupRuleList)(New Criteria())

    End Function

    'Private Sub Fetch(sdr As SafeDataReader)

    '  Me.RaiseListChangedEvents = False
    '  While sdr.Read
    '    Me.Add(AccessTerminalGroupRule.GetAccessTerminalGroupRule(sdr))
    '  End While
    '  Me.RaiseListChangedEvents = True

    'End Sub

    'Protected Overrides Sub DataPortal_Fetch(criteria As Object)

    '  Dim crit As Criteria = criteria
    '  Using cn As New SqlConnection(Singular.Settings.ConnectionString)
    '    cn.Open()
    '    Try
    '      Using cm As SqlCommand = cn.CreateCommand
    '        cm.CommandType = CommandType.StoredProcedure
    '        cm.CommandText = "GetProcsWeb.getAccessTerminalGroupRuleList"
    '        Using sdr As New SafeDataReader(cm.ExecuteReader)
    '          Fetch(sdr)
    '        End Using
    '      End Using
    '    Finally
    '      cn.Close()
    '    End Try
    '  End Using

    'End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AccessTerminalGroupRule In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AccessTerminalGroupRule In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace