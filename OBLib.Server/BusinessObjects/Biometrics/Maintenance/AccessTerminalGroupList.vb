﻿' Generated 29 May 2015 16:20 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics.Maintenance

  <Serializable()> _
  Public Class AccessTerminalGroupList
    Inherits OBBusinessListBase(Of AccessTerminalGroupList, AccessTerminalGroup)

#Region " Business Methods "

    Public Function GetItem(AccessTerminalGroupID As Integer) As AccessTerminalGroup

      For Each child As AccessTerminalGroup In Me
        If child.AccessTerminalGroupID = AccessTerminalGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Access Terminal Groups"

    End Function

    Public Function GetAccessTerminalGroupTerminal(AccessTerminalGroupTerminalID As Integer) As AccessTerminalGroupTerminal

      Dim obj As AccessTerminalGroupTerminal = Nothing
      For Each parent As AccessTerminalGroup In Me
        obj = parent.AccessTerminalGroupTerminalList.GetItem(AccessTerminalGroupTerminalID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewAccessTerminalGroupList() As AccessTerminalGroupList

      Return New AccessTerminalGroupList()

    End Function

    Public Shared Sub BeginGetAccessTerminalGroupList(CallBack As EventHandler(Of DataPortalResult(Of AccessTerminalGroupList)))

      Dim dp As New DataPortal(Of AccessTerminalGroupList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAccessTerminalGroupList() As AccessTerminalGroupList

      Return DataPortal.Fetch(Of AccessTerminalGroupList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AccessTerminalGroup.GetAccessTerminalGroup(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As AccessTerminalGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AccessTerminalGroupID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.AccessTerminalGroupTerminalList.RaiseListChangedEvents = False
          parent.AccessTerminalGroupTerminalList.Add(AccessTerminalGroupTerminal.GetAccessTerminalGroupTerminal(sdr))
          parent.AccessTerminalGroupTerminalList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As AccessTerminalGroup In Me
        child.CheckRules()
        For Each AccessTerminalGroupTerminal As AccessTerminalGroupTerminal In child.AccessTerminalGroupTerminalList
          AccessTerminalGroupTerminal.CheckRules()
        Next
      Next

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AccessTerminalGroupID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.AccessTerminalGroupRuleList.RaiseListChangedEvents = False
          parent.AccessTerminalGroupRuleList.Add(AccessTerminalGroupRule.GetAccessTerminalGroupRule(sdr))
          parent.AccessTerminalGroupRuleList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As AccessTerminalGroup In Me
        child.CheckRules()
        For Each AccessTerminalGroupRule As AccessTerminalGroupRule In child.AccessTerminalGroupRuleList
          AccessTerminalGroupRule.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAccessTerminalGroupList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AccessTerminalGroup In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AccessTerminalGroup In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace