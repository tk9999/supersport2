﻿' Generated 04 Jun 2015 06:45 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics.Maintenance

  <Serializable()> _
  Public Class AccessLogSource
    Inherits OBBusinessBase(Of AccessLogSource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessLogSourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessLogSourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccessLogSourceID() As Integer
      Get
        Return GetProperty(AccessLogSourceIDProperty)
      End Get
    End Property

    Public Shared AccessLogSourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccessLogSource, "Access Log Source", "")
    ''' <summary>
    ''' Gets and sets the Access Log Source value
    ''' </summary>
    <Display(Name:="Access Log Source", Description:=""),
    StringLength(100, ErrorMessage:="Access Log Source cannot be more than 100 characters")>
    Public Property AccessLogSource() As String
      Get
        Return GetProperty(AccessLogSourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AccessLogSourceProperty, Value)
      End Set
    End Property

    Public Shared MaxImportedHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MaxImportedHumanResourceID, "Max Imported Human Resource", 0)
    ''' <summary>
    ''' Gets and sets the Max Imported Human Resource value
    ''' </summary>
    <Display(Name:="Max Imported Human Resource", Description:=""),
    Required(ErrorMessage:="Max Imported Human Resource required")>
    Public Property MaxImportedHumanResourceID() As Integer
      Get
        Return GetProperty(MaxImportedHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MaxImportedHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared MaxAccessLogDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.MaxAccessLogDate, "Max Access Log Date")
    ''' <summary>
    ''' Gets and sets the Max Access Log Date value
    ''' </summary>
    <Display(Name:="Max Access Log Date", Description:=""),
    Required(ErrorMessage:="Max Access Log Date required")>
    Public Property MaxAccessLogDate As DateTime?
      Get
        Return GetProperty(MaxAccessLogDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(MaxAccessLogDateProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessLogSourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AccessLogSource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Log Source")
        Else
          Return String.Format("Blank {0}", "Access Log Source")
        End If
      Else
        Return Me.AccessLogSource
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessLogSource() method.

    End Sub

    Public Shared Function NewAccessLogSource() As AccessLogSource

      Return DataPortal.CreateChild(Of AccessLogSource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessLogSource(dr As SafeDataReader) As AccessLogSource

      Dim a As New AccessLogSource()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessLogSourceIDProperty, .GetInt32(0))
          LoadProperty(AccessLogSourceProperty, .GetString(1))
          LoadProperty(MaxImportedHumanResourceIDProperty, .GetInt32(2))
          LoadProperty(MaxAccessLogDateProperty, .GetValue(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessLogSource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessLogSource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessLogSourceID As SqlParameter = .Parameters.Add("@AccessLogSourceID", SqlDbType.Int)
          paramAccessLogSourceID.Value = GetProperty(AccessLogSourceIDProperty)
          If Me.IsNew Then
            paramAccessLogSourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccessLogSource", GetProperty(AccessLogSourceProperty))
          .Parameters.AddWithValue("@MaxImportedHumanResourceID", GetProperty(MaxImportedHumanResourceIDProperty))
          .Parameters.AddWithValue("@MaxAccessLogDate", (New SmartDate(GetProperty(MaxAccessLogDateProperty))).DBValue)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessLogSourceIDProperty, paramAccessLogSourceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessLogSource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccessLogSourceID", GetProperty(AccessLogSourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace