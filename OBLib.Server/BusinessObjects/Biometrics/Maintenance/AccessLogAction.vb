﻿' Generated 29 May 2015 14:44 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics.Maintenance

  <Serializable()> _
  Public Class AccessLogAction
    Inherits OBBusinessBase(Of AccessLogAction)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessLogActionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessLogActionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccessLogActionID() As Integer
      Get
        Return GetProperty(AccessLogActionIDProperty)
      End Get
    End Property

    Public Shared AccessLogActionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccessLogAction, "Access Log Action", "")
    ''' <summary>
    ''' Gets and sets the Access Log Action value
    ''' </summary>
    <Display(Name:="Access Log Action", Description:=""),
    StringLength(50, ErrorMessage:="Access Log Action cannot be more than 50 characters"), Required(ErrorMessage:="Access Log Action required")>
    Public Property AccessLogAction() As String
      Get
        Return GetProperty(AccessLogActionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AccessLogActionProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessLogActionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AccessLogAction.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Log Action")
        Else
          Return String.Format("Blank {0}", "Access Log Action")
        End If
      Else
        Return Me.AccessLogAction
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessLogAction() method.

    End Sub

    Public Shared Function NewAccessLogAction() As AccessLogAction

      Return DataPortal.CreateChild(Of AccessLogAction)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessLogAction(dr As SafeDataReader) As AccessLogAction

      Dim a As New AccessLogAction()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessLogActionIDProperty, .GetInt32(0))
          LoadProperty(AccessLogActionProperty, .GetString(1))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessLogAction"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessLogAction"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessLogActionID As SqlParameter = .Parameters.Add("@AccessLogActionID", SqlDbType.Int)
          paramAccessLogActionID.Value = GetProperty(AccessLogActionIDProperty)
          If Me.IsNew Then
            paramAccessLogActionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccessLogAction", GetProperty(AccessLogActionProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessLogActionIDProperty, paramAccessLogActionID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessLogAction"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccessLogActionID", GetProperty(AccessLogActionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace