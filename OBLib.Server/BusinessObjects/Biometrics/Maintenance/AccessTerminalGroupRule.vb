﻿' Generated 24 Jul 2015 08:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics.Maintenance

  <Serializable()> _
  Public Class AccessTerminalGroupRule
    Inherits OBBusinessBase(Of AccessTerminalGroupRule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessTerminalGroupRuleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessTerminalGroupRuleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AccessTerminalGroupRuleID() As Integer
      Get
        Return GetProperty(AccessTerminalGroupRuleIDProperty)
      End Get
    End Property

    Public Shared AccessTerminalGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessTerminalGroupID, "Access Terminal Group", Nothing)
    ''' <summary>
    ''' Gets the Access Terminal Group value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property AccessTerminalGroupID() As Integer?
      Get
        Return GetProperty(AccessTerminalGroupIDProperty)
      End Get
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets and sets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:=""),
    Required(ErrorMessage:="Effective Date required"),
    Singular.Web.DataAnnotations.AutoBindings(Singular.Web.KnockoutBindingString.enable, "$data.CanChangeEffectiveDate")>
    Public Property EffectiveDate As DateTime?
      Get
        Return GetProperty(EffectiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EffectiveDateProperty, Value)
      End Set
    End Property

    Public Shared StartedSlightlyLateBufferMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StartedSlightlyLateBufferMinutes, "Started Slightly Late Buffer Minutes", 0)
    ''' <summary>
    ''' Gets and sets the Started Slightly Late Buffer Minutes value
    ''' </summary>
    <Display(Name:="Started Slightly Late Buffer Minutes", Description:=""),
    Required(ErrorMessage:="Started Slightly Late Buffer Minutes required"),
    Singular.Web.DataAnnotations.AutoBindings(Singular.Web.KnockoutBindingString.enable, "$data.CanChangeEffectiveDate")>
    Public Property StartedSlightlyLateBufferMinutes() As Integer
      Get
        Return GetProperty(StartedSlightlyLateBufferMinutesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(StartedSlightlyLateBufferMinutesProperty, Value)
      End Set
    End Property

    Public Shared StartedLateActionBufferMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StartedLateActionBufferMinutes, "Started Late Action Buffer Minutes", 0)
    ''' <summary>
    ''' Gets and sets the Started Late Action Buffer Minutes value
    ''' </summary>
    <Display(Name:="Started Late Action Buffer Minutes", Description:=""),
    Required(ErrorMessage:="Started Late Action Buffer Minutes required"),
    Singular.Web.DataAnnotations.AutoBindings(Singular.Web.KnockoutBindingString.enable, "$data.CanChangeEffectiveDate")>
    Public Property StartedLateActionBufferMinutes() As Integer
      Get
        Return GetProperty(StartedLateActionBufferMinutesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(StartedLateActionBufferMinutesProperty, Value)
      End Set
    End Property

    Public Shared EndedEarlyActionBufferMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EndedEarlyActionBufferMinutes, "Ended Early Action Buffer Minutes", 0)
    ''' <summary>
    ''' Gets and sets the Ended Early Action Buffer Minutes value
    ''' </summary>
    <Display(Name:="Ended Early Action Buffer Minutes", Description:=""),
    Required(ErrorMessage:="Ended Early Action Buffer Minutes required"),
    Singular.Web.DataAnnotations.AutoBindings(Singular.Web.KnockoutBindingString.enable, "$data.CanChangeEffectiveDate")>
    Public Property EndedEarlyActionBufferMinutes() As Integer
      Get
        Return GetProperty(EndedEarlyActionBufferMinutesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EndedEarlyActionBufferMinutesProperty, Value)
      End Set
    End Property

    Public Shared EndedSlightlyEarlyActionBufferMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EndedSlightlyEarlyActionBufferMinutes, "Ended Slightly Early Action Buffer Minutes", 0)
    ''' <summary>
    ''' Gets and sets the Ended Slightly Early Action Buffer Minutes value
    ''' </summary>
    <Display(Name:="Ended Slightly Early Action Buffer Minutes", Description:=""),
    Required(ErrorMessage:="Ended Slightly Early Action Buffer Minutes required"),
    Singular.Web.DataAnnotations.AutoBindings(Singular.Web.KnockoutBindingString.enable, "$data.CanChangeEffectiveDate")>
    Public Property EndedSlightlyEarlyActionBufferMinutes() As Integer
      Get
        Return GetProperty(EndedSlightlyEarlyActionBufferMinutesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EndedSlightlyEarlyActionBufferMinutesProperty, Value)
      End Set
    End Property

    Public Shared CheckForAccessTransactionsHoursBeforeShiftStartProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CheckForAccessTransactionsHoursBeforeShiftStart, "Check For Access Transactions Hours Before Shift Start", 2)
    ''' <summary>
    ''' Gets and sets the Check For Access Transactions Hours Before Shift Start value
    ''' </summary>
    <Display(Name:="Check For Access Transactions Hours Before Shift Start", Description:=""),
    Required(ErrorMessage:="Check For Access Transactions Hours Before Shift Start required"),
    Singular.Web.DataAnnotations.AutoBindings(Singular.Web.KnockoutBindingString.enable, "$data.CanChangeEffectiveDate")>
    Public Property CheckForAccessTransactionsHoursBeforeShiftStart() As Integer
      Get
        Return GetProperty(CheckForAccessTransactionsHoursBeforeShiftStartProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CheckForAccessTransactionsHoursBeforeShiftStartProperty, Value)
      End Set
    End Property

    Public Shared CheckForAccessTransactionsHoursAfterShiftEndProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CheckForAccessTransactionsHoursAfterShiftEnd, "Check For Access Transactions Hours After Shift End", 2)
    ''' <summary>
    ''' Gets and sets the Check For Access Transactions Hours After Shift End value
    ''' </summary>
    <Display(Name:="Check For Access Transactions Hours After Shift End", Description:=""),
    Required(ErrorMessage:="Check For Access Transactions Hours After Shift End required"),
    Singular.Web.DataAnnotations.AutoBindings(Singular.Web.KnockoutBindingString.enable, "$data.CanChangeEffectiveDate")>
    Public Property CheckForAccessTransactionsHoursAfterShiftEnd() As Integer
      Get
        Return GetProperty(CheckForAccessTransactionsHoursAfterShiftEndProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CheckForAccessTransactionsHoursAfterShiftEndProperty, Value)
      End Set
    End Property

    Public Shared ShiftsMissingActionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftsMissingActionID, "Shifts Missing Action", 0)
    ''' <summary>
    ''' Gets and sets the Shifts Missing Action value
    ''' </summary>
    <Display(Name:="Shifts Missing Action", Description:=""),
    Singular.Web.DataAnnotations.AutoBindings(Singular.Web.KnockoutBindingString.enable, "$data.CanChangeEffectiveDate"),
    Singular.DataAnnotations.DropDownWeb(GetType(AccessLogs.ReadOnly.ROAccessLogActionList))>
    Public Property ShiftsMissingActionID() As Integer
      Get
        Return GetProperty(ShiftsMissingActionIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ShiftsMissingActionIDProperty, Value)
      End Set
    End Property

    Public Shared CanChangeEffectiveDateProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanChangeEffectiveDate, "Can Change Effective Date", True)
    ''' <summary>
    ''' Gets the Terminal Name value
    ''' </summary>
    <Display(Name:="Can Change Effective Date", Description:="")>
    Public ReadOnly Property CanChangeEffectiveDate() As Boolean
      Get
        If Me.IsNew Then
          Return True
        End If
        Return GetProperty(CanChangeEffectiveDateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessTerminalGroupRuleIDProperty)

    End Function

    Public Function GetParent() As AccessTerminalGroup

      Return CType(CType(Me.Parent, AccessTerminalGroupRuleList).Parent, AccessTerminalGroup)

    End Function

    Public Overrides Function ToString() As String

      If Me.AccessTerminalGroupRuleID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Terminal Group Rule")
        Else
          Return String.Format("Blank {0}", "Access Terminal Group Rule")
        End If
      Else
        Return Me.AccessTerminalGroupRuleID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub


#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessTerminalGroupRule() method.

    End Sub

    Public Shared Function NewAccessTerminalGroupRule() As AccessTerminalGroupRule

      Return DataPortal.CreateChild(Of AccessTerminalGroupRule)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessTerminalGroupRule(dr As SafeDataReader) As AccessTerminalGroupRule

      Dim a As New AccessTerminalGroupRule()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessTerminalGroupRuleIDProperty, .GetInt32(0))
          LoadProperty(AccessTerminalGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EffectiveDateProperty, .GetValue(2))
          LoadProperty(StartedSlightlyLateBufferMinutesProperty, .GetInt32(3))
          LoadProperty(StartedLateActionBufferMinutesProperty, .GetInt32(4))
          LoadProperty(EndedEarlyActionBufferMinutesProperty, .GetInt32(5))
          LoadProperty(EndedSlightlyEarlyActionBufferMinutesProperty, .GetInt32(6))
          LoadProperty(CheckForAccessTransactionsHoursBeforeShiftStartProperty, .GetInt32(7))
          LoadProperty(CheckForAccessTransactionsHoursAfterShiftEndProperty, .GetInt32(8))
          LoadProperty(ShiftsMissingActionIDProperty, .GetInt32(9))
          LoadProperty(CanChangeEffectiveDateProperty, .GetBoolean(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessTerminalGroupRule"

        DoInsertUpdateChild(cm)
      End Using
    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessTerminalGroupRule"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessTerminalGroupRuleID As SqlParameter = .Parameters.Add("@AccessTerminalGroupRuleID", SqlDbType.Int)
          paramAccessTerminalGroupRuleID.Value = GetProperty(AccessTerminalGroupRuleIDProperty)
          If Me.IsNew Then
            paramAccessTerminalGroupRuleID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccessTerminalGroupID", Me.GetParent().AccessTerminalGroupID)
          .Parameters.AddWithValue("@EffectiveDate", (New SmartDate(GetProperty(EffectiveDateProperty))).DBValue)
          .Parameters.AddWithValue("@StartedSlightlyLateBufferMinutes", GetProperty(StartedSlightlyLateBufferMinutesProperty))
          .Parameters.AddWithValue("@StartedLateActionBufferMinutes", GetProperty(StartedLateActionBufferMinutesProperty))
          .Parameters.AddWithValue("@EndedEarlyActionBufferMinutes", GetProperty(EndedEarlyActionBufferMinutesProperty))
          .Parameters.AddWithValue("@EndedSlightlyEarlyActionBufferMinutes", GetProperty(EndedSlightlyEarlyActionBufferMinutesProperty))
          .Parameters.AddWithValue("@CheckForAccessTransactionsHoursBeforeShiftStart", GetProperty(CheckForAccessTransactionsHoursBeforeShiftStartProperty))
          .Parameters.AddWithValue("@CheckForAccessTransactionsHoursAfterShiftEnd", GetProperty(CheckForAccessTransactionsHoursAfterShiftEndProperty))
          .Parameters.AddWithValue("@ShiftsMissingActionID", GetProperty(ShiftsMissingActionIDProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessTerminalGroupRuleIDProperty, paramAccessTerminalGroupRuleID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessTerminalGroupRule"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccessTerminalGroupRuleID", GetProperty(AccessTerminalGroupRuleIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace