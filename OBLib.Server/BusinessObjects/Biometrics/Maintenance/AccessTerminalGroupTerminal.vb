﻿' Generated 29 May 2015 16:20 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics.Maintenance

  <Serializable()> _
  Public Class AccessTerminalGroupTerminal
    Inherits OBBusinessBase(Of AccessTerminalGroupTerminal)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessTerminalGroupTerminalIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessTerminalGroupTerminalID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccessTerminalGroupTerminalID() As Integer
      Get
        Return GetProperty(AccessTerminalGroupTerminalIDProperty)
      End Get
    End Property

    Public Shared AccessTerminalGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessTerminalGroupID, "Access Terminal Group", Nothing)
    ''' <summary>
    ''' Gets the Access Terminal Group value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property AccessTerminalGroupID() As Integer?
      Get
        Return GetProperty(AccessTerminalGroupIDProperty)
      End Get
    End Property

    Public Shared AccessTerminalIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessTerminalID, "Access Terminal", Nothing)
    ''' <summary>
    ''' Gets and sets the Access Terminal value
    ''' </summary>
    <Display(Name:="Access Terminal", Description:=""),
    Required(ErrorMessage:="Access Terminal required"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.AccessLogs.ReadOnly.ROAccessTerminalList))>
    Public Property AccessTerminalID() As Integer?
      Get
        Return GetProperty(AccessTerminalIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccessTerminalIDProperty, Value)
      End Set
    End Property

    Public Shared AccessTerminalTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessTerminalTypeID, "Access Terminal Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Access Terminal Type value
    ''' </summary>
    <Display(Name:="Access Terminal Type", Description:=""),
    Required(ErrorMessage:="Access Terminal Type required"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Biometrics.Maintenance.AccessTerminalTypeList))>
    Public Property AccessTerminalTypeID() As Integer?
      Get
        Return GetProperty(AccessTerminalTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccessTerminalTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As AccessTerminalGroup

      Return CType(CType(Me.Parent, AccessTerminalGroupTerminalList).Parent, AccessTerminalGroup)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessTerminalGroupTerminalIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Terminal Group Terminal")
        Else
          Return String.Format("Blank {0}", "Access Terminal Group Terminal")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessTerminalGroupTerminal() method.

    End Sub

    Public Shared Function NewAccessTerminalGroupTerminal() As AccessTerminalGroupTerminal

      Return DataPortal.CreateChild(Of AccessTerminalGroupTerminal)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessTerminalGroupTerminal(dr As SafeDataReader) As AccessTerminalGroupTerminal

      Dim a As New AccessTerminalGroupTerminal()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessTerminalGroupTerminalIDProperty, .GetInt32(0))
          LoadProperty(AccessTerminalGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AccessTerminalIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(AccessTerminalTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessTerminalGroupTerminal"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessTerminalGroupTerminal"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessTerminalGroupTerminalID As SqlParameter = .Parameters.Add("@AccessTerminalGroupTerminalID", SqlDbType.Int)
          paramAccessTerminalGroupTerminalID.Value = GetProperty(AccessTerminalGroupTerminalIDProperty)
          If Me.IsNew Then
            paramAccessTerminalGroupTerminalID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccessTerminalGroupID", Me.GetParent().AccessTerminalGroupID)
          .Parameters.AddWithValue("@AccessTerminalID", GetProperty(AccessTerminalIDProperty))
          .Parameters.AddWithValue("@AccessTerminalTypeID", GetProperty(AccessTerminalTypeIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessTerminalGroupTerminalIDProperty, paramAccessTerminalGroupTerminalID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessTerminalGroupTerminal"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccessTerminalGroupTerminalID", GetProperty(AccessTerminalGroupTerminalIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace