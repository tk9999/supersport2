﻿' Generated 29 May 2015 16:20 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics.Maintenance

  <Serializable()> _
  Public Class AccessTerminalGroup
    Inherits OBBusinessBase(Of AccessTerminalGroup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessTerminalGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessTerminalGroupID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccessTerminalGroupID() As Integer
      Get
        Return GetProperty(AccessTerminalGroupIDProperty)
      End Get
    End Property

    Public Shared AccessTerminalGroupProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccessTerminalGroup, "Access Terminal Group", "")
    ''' <summary>
    ''' Gets and sets the Access Terminal Group value
    ''' </summary>
    <Display(Name:="Access Terminal Group", Description:=""),
    StringLength(20, ErrorMessage:="Access Terminal Group cannot be more than 20 characters")>
    Public Property AccessTerminalGroup() As String
      Get
        Return GetProperty(AccessTerminalGroupProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AccessTerminalGroupProperty, Value)
      End Set
    End Property

    Public Shared ExpectedShiftHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ExpectedShiftHours, "Expected Shift Hours", 0)
    ''' <summary>
    ''' Gets and sets the Expected Shift Hours value
    ''' </summary>
    <Display(Name:="Expected Shift Hours", Description:="The number of hours this Access Terminal Group is expected to have (Particularly needed if shift durations are frequently longer than 12 hours)")>
    Public Property ExpectedShiftHours() As Integer
      Get
        Return GetProperty(ExpectedShiftHoursProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ExpectedShiftHoursProperty, Value)
      End Set
    End Property



    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
        Public Property Expanded As Boolean = False

#End Region

#Region " Child Lists "

    Public Shared AccessTerminalGroupRuleListProperty As PropertyInfo(Of AccessTerminalGroupRuleList) = RegisterProperty(Of AccessTerminalGroupRuleList)(Function(c) c.AccessTerminalGroupRuleList, "Access Terminal Group Rule List")

    Public ReadOnly Property AccessTerminalGroupRuleList() As AccessTerminalGroupRuleList
      Get
        If GetProperty(AccessTerminalGroupRuleListProperty) Is Nothing Then
          LoadProperty(AccessTerminalGroupRuleListProperty, Biometrics.Maintenance.AccessTerminalGroupRuleList.NewAccessTerminalGroupRuleList())
        End If
        Return GetProperty(AccessTerminalGroupRuleListProperty)
      End Get
    End Property

    Public Shared AccessTerminalGroupTerminalListProperty As PropertyInfo(Of AccessTerminalGroupTerminalList) = RegisterProperty(Of AccessTerminalGroupTerminalList)(Function(c) c.AccessTerminalGroupTerminalList, "Access Terminal Group Terminal List")

    Public ReadOnly Property AccessTerminalGroupTerminalList() As AccessTerminalGroupTerminalList
      Get
        If GetProperty(AccessTerminalGroupTerminalListProperty) Is Nothing Then
          LoadProperty(AccessTerminalGroupTerminalListProperty, Biometrics.Maintenance.AccessTerminalGroupTerminalList.NewAccessTerminalGroupTerminalList())
        End If
        Return GetProperty(AccessTerminalGroupTerminalListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessTerminalGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AccessTerminalGroup.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Terminal Group")
        Else
          Return String.Format("Blank {0}", "Access Terminal Group")
        End If
      Else
        Return Me.AccessTerminalGroup
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"AccessTerminalGroupTerminals"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessTerminalGroup() method.

    End Sub

    Public Shared Function NewAccessTerminalGroup() As AccessTerminalGroup

      Return DataPortal.CreateChild(Of AccessTerminalGroup)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessTerminalGroup(dr As SafeDataReader) As AccessTerminalGroup

      Dim a As New AccessTerminalGroup()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessTerminalGroupIDProperty, .GetInt32(0))
          LoadProperty(AccessTerminalGroupProperty, .GetString(1))
          LoadProperty(ExpectedShiftHoursProperty, .GetInt32(2))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessTerminalGroup"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessTerminalGroup"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessTerminalGroupID As SqlParameter = .Parameters.Add("@AccessTerminalGroupID", SqlDbType.Int)
          paramAccessTerminalGroupID.Value = GetProperty(AccessTerminalGroupIDProperty)
          If Me.IsNew Then
            paramAccessTerminalGroupID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccessTerminalGroup", GetProperty(AccessTerminalGroupProperty))
          .Parameters.AddWithValue("@ExpectedShiftHours", GetProperty(ExpectedShiftHoursProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessTerminalGroupIDProperty, paramAccessTerminalGroupID.Value)
          End If
          ' update child objects
          UpdateChildLists()
          MarkOld()
        End With
      Else
        ' update child objects
        UpdateChildLists()
      End If

    End Sub

    Private Sub UpdateChildLists()

      If GetProperty(AccessTerminalGroupTerminalListProperty) IsNot Nothing Then
        Me.AccessTerminalGroupTerminalList.Update()
      End If

      If GetProperty(AccessTerminalGroupRuleListProperty) IsNot Nothing Then
        Me.AccessTerminalGroupRuleList.Update()
      End If
    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessTerminalGroup"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccessTerminalGroupID", GetProperty(AccessTerminalGroupIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace