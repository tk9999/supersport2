﻿' Generated 05 Dec 2014 13:03 - Singular Systems Object Generator Version 2.1.664
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace AccessLogs

  <Serializable()> _
  Public Class AccessTerminalGroupHumanResourceList
    Inherits Singular.SingularBusinessListBase(Of AccessTerminalGroupHumanResourceList, AccessTerminalGroupHumanResource)

#Region " Business Methods "

    Public Function GetItem(AccessTerminalGroupHumanResourceID As Integer) As AccessTerminalGroupHumanResource

      For Each child As AccessTerminalGroupHumanResource In Me
        If child.AccessTerminalGroupHumanResourceID = AccessTerminalGroupHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Access Terminal Group Human Resources"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewAccessTerminalGroupHumanResourceList() As AccessTerminalGroupHumanResourceList

      Return New AccessTerminalGroupHumanResourceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AccessTerminalGroupHumanResource In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AccessTerminalGroupHumanResource In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace