﻿' Generated 29 May 2015 14:50 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics.Maintenance

  <Serializable()> _
  Public Class AccessTerminal
    Inherits OBBusinessBase(Of AccessTerminal)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessTerminalIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessTerminalID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccessTerminalID() As Integer
      Get
        Return GetProperty(AccessTerminalIDProperty)
      End Get
    End Property

    Public Shared AccessLocationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessLocationID, "Access Location", Nothing)
    ''' <summary>
    ''' Gets the Access Location value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property AccessLocationID() As Integer?
      Get
        Return GetProperty(AccessLocationIDProperty)
      End Get
    End Property

    Public Shared AccessLogSourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessLogSourceID, "Access Log Source", Nothing)
    ''' <summary>
    ''' Gets and sets the Access Log Source value
    ''' </summary>
    <Display(Name:="Access Log Source", Description:=""),
    Required(ErrorMessage:="Access Log Source required"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Biometrics.Maintenance.AccessLogSourceList))>
    Public Property AccessLogSourceID() As Integer?
      Get
        Return GetProperty(AccessLogSourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccessLogSourceIDProperty, Value)
      End Set
    End Property

    Public Shared SourceKeyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SourceKey, "Source Key", "")
    ''' <summary>
    ''' Gets and sets the Source Key value
    ''' </summary>
    <Display(Name:="Source Key", Description:=""),
    StringLength(20, ErrorMessage:="Source Key cannot be more than 20 characters")>
    Public Property SourceKey() As String
      Get
        Return GetProperty(SourceKeyProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SourceKeyProperty, Value)
      End Set
    End Property

    Public Shared TerminalNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TerminalName, "Terminal Name", "")
    ''' <summary>
    ''' Gets and sets the Terminal Name value
    ''' </summary>
    <Display(Name:="Terminal Name", Description:=""),
    StringLength(255, ErrorMessage:="Terminal Name cannot be more than 255 characters")>
    Public Property TerminalName() As String
      Get
        Return GetProperty(TerminalNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TerminalNameProperty, Value)
      End Set
    End Property

    Public Shared FriendlyTerminalNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FriendlyTerminalName, "Friendly Terminal Name", "")
    ''' <summary>
    ''' Gets and sets the Friendly Terminal Name value
    ''' </summary>
    <Display(Name:="Friendly Terminal Name", Description:=""),
    StringLength(255, ErrorMessage:="Friendly Terminal Name cannot be more than 255 characters")>
    Public Property FriendlyTerminalName() As String
      Get
        Return GetProperty(FriendlyTerminalNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FriendlyTerminalNameProperty, Value)
      End Set
    End Property

    Public Shared EnabledIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.EnabledInd, "Enabled", True)
    ''' <summary>
    ''' Gets and sets the Enabled value
    ''' </summary>
    <Display(Name:="Enabled", Description:=""),
    Required(ErrorMessage:="Enabled required")>
    Public Property EnabledInd() As Boolean
      Get
        Return GetProperty(EnabledIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(EnabledIndProperty, Value)
      End Set
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleID, "Vehicle", 0)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleFullList))>
    Public Property VehicleID() As Integer
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared BiometricsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.BiometricsInd, "Biometrics", False)
    ''' <summary>
    ''' Gets and sets the Biometrics value
    ''' </summary>
    <Display(Name:="Biometrics", Description:=""),
    Required(ErrorMessage:="Biometrics required")>
    Public Property BiometricsInd() As Boolean
      Get
        Return GetProperty(BiometricsIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(BiometricsIndProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As AccessLocation

      Return CType(CType(Me.Parent, AccessTerminalList).Parent, AccessLocation)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessTerminalIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SourceKey.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Terminal")
        Else
          Return String.Format("Blank {0}", "Access Terminal")
        End If
      Else
        Return Me.SourceKey
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessTerminal() method.

    End Sub

    Public Shared Function NewAccessTerminal() As AccessTerminal

      Return DataPortal.CreateChild(Of AccessTerminal)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessTerminal(dr As SafeDataReader) As AccessTerminal

      Dim a As New AccessTerminal()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessTerminalIDProperty, .GetInt32(0))
          LoadProperty(AccessLocationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AccessLogSourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(SourceKeyProperty, .GetString(3))
          LoadProperty(TerminalNameProperty, .GetString(4))
          LoadProperty(FriendlyTerminalNameProperty, .GetString(5))
          LoadProperty(EnabledIndProperty, .GetBoolean(6))
          LoadProperty(VehicleIDProperty, .GetInt32(7))
          LoadProperty(BiometricsIndProperty, .GetBoolean(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessTerminal"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessTerminal"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessTerminalID As SqlParameter = .Parameters.Add("@AccessTerminalID", SqlDbType.Int)
          paramAccessTerminalID.Value = GetProperty(AccessTerminalIDProperty)
          If Me.IsNew Then
            paramAccessTerminalID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccessLocationID", Singular.Misc.NothingDBNull(GetProperty(AccessLocationIDProperty)))
          .Parameters.AddWithValue("@AccessLogSourceID", GetProperty(AccessLogSourceIDProperty))
          .Parameters.AddWithValue("@SourceKey", GetProperty(SourceKeyProperty))
          .Parameters.AddWithValue("@TerminalName", GetProperty(TerminalNameProperty))
          .Parameters.AddWithValue("@FriendlyTerminalName", GetProperty(FriendlyTerminalNameProperty))
          .Parameters.AddWithValue("@EnabledInd", GetProperty(EnabledIndProperty))
          .Parameters.AddWithValue("@VehicleID", GetProperty(VehicleIDProperty))
          .Parameters.AddWithValue("@BiometricsInd", GetProperty(BiometricsIndProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessTerminalIDProperty, paramAccessTerminalID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessTerminal"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccessTerminalID", GetProperty(AccessTerminalIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace