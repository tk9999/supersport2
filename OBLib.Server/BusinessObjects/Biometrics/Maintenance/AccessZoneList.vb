﻿' Generated 29 May 2015 14:50 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics.Maintenance

  <Serializable()> _
  Public Class AccessZoneList
    Inherits OBBusinessListBase(Of AccessZoneList, AccessZone)

#Region " Business Methods "

    Public Function GetItem(AccessZoneID As Integer) As AccessZone

      For Each child As AccessZone In Me
        If child.AccessZoneID = AccessZoneID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Access Zones"

    End Function

    Public Function GetAccessLocation(AccessLocationID As Integer) As AccessLocation

      Dim obj As AccessLocation = Nothing
      For Each parent As AccessZone In Me
        obj = parent.AccessLocationList.GetItem(AccessLocationID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewAccessZoneList() As AccessZoneList

      Return New AccessZoneList()

    End Function

    Public Shared Sub BeginGetAccessZoneList(CallBack As EventHandler(Of DataPortalResult(Of AccessZoneList)))

      Dim dp As New DataPortal(Of AccessZoneList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAccessZoneList() As AccessZoneList

      Return DataPortal.Fetch(Of AccessZoneList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AccessZone.GetAccessZone(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As AccessZone = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AccessZoneID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.AccessLocationList.RaiseListChangedEvents = False
          parent.AccessLocationList.Add(AccessLocation.GetAccessLocation(sdr))
          parent.AccessLocationList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As AccessLocation = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.AccessLocationID <> sdr.GetInt32(1) Then
            parentChild = Me.GetAccessLocation(sdr.GetInt32(1))
          End If
          parentChild.AccessTerminalList.RaiseListChangedEvents = False
          parentChild.AccessTerminalList.Add(AccessTerminal.GetAccessTerminal(sdr))
          parentChild.AccessTerminalList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As AccessZone In Me
        child.CheckRules()
        For Each AccessLocation As AccessLocation In child.AccessLocationList
          AccessLocation.CheckRules()

          For Each AccessTerminal As AccessTerminal In AccessLocation.AccessTerminalList
            AccessTerminal.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAccessZoneList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AccessZone In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AccessZone In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace