﻿' Generated 29 May 2015 14:50 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics.Maintenance

  <Serializable()> _
  Public Class AccessZone
    Inherits OBBusinessBase(Of AccessZone)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessZoneIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessZoneID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccessZoneID() As Integer
      Get
        Return GetProperty(AccessZoneIDProperty)
      End Get
    End Property

    Public Shared ImpronetCTRLSLAProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImpronetCTRLSLA, "Impronet CTRLSLA", "")
    ''' <summary>
    ''' Gets and sets the Impronet CTRLSLA value
    ''' </summary>
    <Display(Name:="Impronet CTRLSLA", Description:=""),
    StringLength(20, ErrorMessage:="Impronet CTRLSLA cannot be more than 20 characters")>
    Public Property ImpronetCTRLSLA() As String
      Get
        Return GetProperty(ImpronetCTRLSLAProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ImpronetCTRLSLAProperty, Value)
      End Set
    End Property

    Public Shared ImpronetKeyProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImpronetKey, "Impronet Key", 0)
    ''' <summary>
    ''' Gets and sets the Impronet Key value
    ''' </summary>
    <Display(Name:="Impronet Key", Description:=""),
    Required(ErrorMessage:="Impronet Key required")>
    Public Property ImpronetKey() As Integer
      Get
        Return GetProperty(ImpronetKeyProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ImpronetKeyProperty, Value)
      End Set
    End Property

    Public Shared ZoneNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ZoneName, "Zone Name", "")
    ''' <summary>
    ''' Gets and sets the Zone Name value
    ''' </summary>
    <Display(Name:="Zone Name", Description:=""),
    StringLength(255, ErrorMessage:="Zone Name cannot be more than 255 characters")>
    Public Property ZoneName() As String
      Get
        Return GetProperty(ZoneNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ZoneNameProperty, Value)
      End Set
    End Property

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False


#End Region

#Region " Child Lists "

    Public Shared AccessLocationListProperty As PropertyInfo(Of AccessLocationList) = RegisterProperty(Of AccessLocationList)(Function(c) c.AccessLocationList, "Access Location List")

    Public ReadOnly Property AccessLocationList() As AccessLocationList
      Get
        If GetProperty(AccessLocationListProperty) Is Nothing Then
          LoadProperty(AccessLocationListProperty, Biometrics.Maintenance.AccessLocationList.NewAccessLocationList())
        End If
        Return GetProperty(AccessLocationListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessZoneIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ImpronetCTRLSLA.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Zone")
        Else
          Return String.Format("Blank {0}", "Access Zone")
        End If
      Else
        Return Me.ImpronetCTRLSLA
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"AccessLocations"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessZone() method.

    End Sub

    Public Shared Function NewAccessZone() As AccessZone

      Return DataPortal.CreateChild(Of AccessZone)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessZone(dr As SafeDataReader) As AccessZone

      Dim a As New AccessZone()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessZoneIDProperty, .GetInt32(0))
          LoadProperty(ImpronetCTRLSLAProperty, .GetString(1))
          LoadProperty(ImpronetKeyProperty, .GetInt32(2))
          LoadProperty(ZoneNameProperty, .GetString(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessZone"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessZone"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessZoneID As SqlParameter = .Parameters.Add("@AccessZoneID", SqlDbType.Int)
          paramAccessZoneID.Value = GetProperty(AccessZoneIDProperty)
          If Me.IsNew Then
            paramAccessZoneID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ImpronetCTRLSLA", GetProperty(ImpronetCTRLSLAProperty))
          .Parameters.AddWithValue("@ImpronetKey", GetProperty(ImpronetKeyProperty))
          .Parameters.AddWithValue("@ZoneName", GetProperty(ZoneNameProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessZoneIDProperty, paramAccessZoneID.Value)
          End If
          ' update child objects
          If GetProperty(AccessLocationListProperty) IsNot Nothing Then
            Me.AccessLocationList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(AccessLocationListProperty) IsNot Nothing Then
          Me.AccessLocationList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessZone"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccessZoneID", GetProperty(AccessZoneIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace