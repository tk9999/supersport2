﻿' Generated 29 May 2015 14:43 - Singular Systems Object Generator Version 2.1.675
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics.Maintenance

  <Serializable()> _
  Public Class AccessTerminalType
    Inherits OBBusinessBase(Of AccessTerminalType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessTerminalTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessTerminalTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccessTerminalTypeID() As Integer
      Get
        Return GetProperty(AccessTerminalTypeIDProperty)
      End Get
    End Property

    Public Shared AccessTerminalTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccessTerminalType, "Access Terminal Type", "")
    ''' <summary>
    ''' Gets and sets the Access Terminal Type value
    ''' </summary>
    <Display(Name:="Access Terminal Type", Description:=""),
    StringLength(50, ErrorMessage:="Access Terminal Type cannot be more than 50 characters"), Required(ErrorMessage:="Access Terminal Type required")>
    Public Property AccessTerminalType() As String
      Get
        Return GetProperty(AccessTerminalTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AccessTerminalTypeProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessTerminalTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AccessTerminalType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Terminal Type")
        Else
          Return String.Format("Blank {0}", "Access Terminal Type")
        End If
      Else
        Return Me.AccessTerminalType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessTerminalType() method.

    End Sub

    Public Shared Function NewAccessTerminalType() As AccessTerminalType

      Return DataPortal.CreateChild(Of AccessTerminalType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessTerminalType(dr As SafeDataReader) As AccessTerminalType

      Dim a As New AccessTerminalType()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessTerminalTypeIDProperty, .GetInt32(0))
          LoadProperty(AccessTerminalTypeProperty, .GetString(1))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessTerminalType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessTerminalType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessTerminalTypeID As SqlParameter = .Parameters.Add("@AccessTerminalTypeID", SqlDbType.Int)
          paramAccessTerminalTypeID.Value = GetProperty(AccessTerminalTypeIDProperty)
          If Me.IsNew Then
            paramAccessTerminalTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccessTerminalType", GetProperty(AccessTerminalTypeProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessTerminalTypeIDProperty, paramAccessTerminalTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessTerminalType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccessTerminalTypeID", GetProperty(AccessTerminalTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace