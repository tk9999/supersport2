﻿Imports System.Linq
Imports System.Data.SqlClient

Namespace Biometrics

  <Serializable()> _
  Public Class ImpronetInterface

    Public Shared Sub GetData(CSConnectionString As String)

      Try

        MinAccessDateTime = Now

        Using c As New SqlConnection(CSConnectionString)
          c.Open()
          Dim str As String = GetSQLString()
          Dim cmd As New SqlCommand(str, c)
          cmd.CommandTimeout = 3600 ' 
          GetAndSaveData(cmd.ExecuteReader)

        End Using
      Catch ex As Exception
        Throw New Exception(ex.Message)
      End Try



      ' Dim length = DateDiff(DateInterval.Second, StartDate, Now)

    End Sub



    Public Shared MinAccessDateTime As Csla.SmartDate
    Private Shared count As Integer = 0
    Private Shared LockObj As New Object

    Public Shared Sub GetAndSaveData(sdr As SqlClient.SqlDataReader)

      Dim dtToSave As DataTable = Nothing
      While sdr.Read()
        If dtToSave Is Nothing Then
          'GetColumns
          dtToSave = New DataTable
          For i = 0 To sdr.FieldCount - 1
            dtToSave.Columns.Add(sdr.GetName(i), sdr.GetFieldType(i))
          Next
        End If
        Dim row As DataRow = dtToSave.NewRow
        For i = 0 To sdr.FieldCount - 1
          row(i) = sdr.GetValue(i)
        Next

        dtToSave.Rows.Add(row)

        If dtToSave.Rows.Count = 200 Then
          Dim tempDT = dtToSave.Copy
          dtToSave.Clear()
          'Commented 
          'Dim tempTask = New Threading.Thread(Sub()
          '                                      SyncLock (LockObj)
          '                                        SaveData(tempDT) ', cn)
          '                                      End SyncLock
          '                                    End Sub)
          count += 1
          SaveData(tempDT)

        End If
      End While

      If dtToSave IsNot Nothing AndAlso dtToSave.Rows.Count > 0 Then
        count += 1
        SaveData(dtToSave) ', cn)
      End If

      While count > 0
        SaveData(dtToSave)
      End While
      sdr.Close()

    End Sub

    Private Shared Sub SaveData(ByVal dt As DataTable)

      If dt.Columns.Contains("TR_TermSLA") AndAlso dt.Columns.Contains("TR_SQ") AndAlso dt.Columns.Contains("TR_DateTime") AndAlso dt.Columns.Contains("MST_ID") AndAlso dt.Columns.Contains("ET_Name") AndAlso
        dt.Columns.Contains("TERM_Name") AndAlso dt.Columns.Contains("TERM_Enabled") AndAlso dt.Columns.Contains("CTRL_SLA") AndAlso dt.Columns.Contains("LOC_No") AndAlso
        dt.Columns.Contains("LOC_Name") AndAlso dt.Columns.Contains("ZONE_No") AndAlso dt.Columns.Contains("ZONE_Name") Then
        'Has Required Columns
        Dim cmdstr As String = "INSERT INTO AccessStagingCentralSecurity(TR_TermSLA, TR_SQ, TR_DateTime, UserIDNo, ET_Name, TERM_Name, TERM_Enabled, CTRL_SLA, LOC_No, LOC_Name, ZONE_No, ZONE_Name)" & vbCrLf


        For Each row As DataRow In dt.Rows
          Dim trDateTime As Csla.SmartDate = New Csla.SmartDate(CStr(row.Item("TR_DateTime"))).ToString("yyyy-MM-dd HH:mm:ss")

          If trDateTime < MinAccessDateTime Then
            MinAccessDateTime = trDateTime
          End If

          cmdstr &= "SELECT '" & row.Item("TR_TermSLA").REPLACE("'", "''") & "', " & row.Item("TR_SQ") &
                    ", CONVERT(DATETIME, '" & trDateTime.ToString("yyyy-MM-dd HH:mm:ss") & "'), '" &
                    row.Item("MST_ID").REPLACE("'", "''") & "', '" &
                    row.Item("ET_Name").REPLACE("'", "''") & "', '" &
                    row.Item("TERM_Name").REPLACE("'", "''") &
                    "', CONVERT(BIT, " & row.Item("TERM_Enabled") & "), '" &
                    row.Item("CTRL_SLA").REPLACE("'", "''") & "', " &
                    row.Item("LOC_No") & ", '" &
                    row.Item("LOC_Name").REPLACE("'", "''") & "', " &
                    row.Item("ZONE_No") & ", '" &
                    row.Item("ZONE_Name").REPLACE("'", "''") & "' " & vbCrLf
          If dt.Rows.IndexOf(row) < dt.Rows.Count - 1 Then
            cmdstr &= "UNION ALL" & vbCrLf
          End If
        Next
        Using cn As New SqlClient.SqlConnection(Singular.Settings.ConnectionString)
          cn.Open()
          Dim cmd As New SqlClient.SqlCommand(cmdstr, cn)
          cmd.ExecuteNonQuery()
          count -= 1
        End Using
      End If

    End Sub

    <Serializable()>
    Public Class IDNumberDate
      Inherits OBReadOnlyBase(Of IDNumberDate)

      Public Property IDNumber As String
      Public Property TR_TermSLA As String
      Public Property LastDate As Integer
      Public Property LastSQ As Integer

      Public Function GetSQLSelectValues() As String

        Return String.Format(" SELECT '{0}' as IDNo,'{1}' as TR_TermSLA,'{2}' as lastDate,  '{3}' as LastSQ", Me.IDNumber, Me.TR_TermSLA, Me.LastDate, Me.LastSQ)

      End Function

    End Class

    <Serializable()>
    Public Class IDNumberDateList
      Inherits OBReadOnlyListBase(Of IDNumberDateList, IDNumberDate)

      Public Shared Function GetIDNumberDateList() As IDNumberDateList

        Return Csla.DataPortal.Fetch(Of IDNumberDateList)(New Criteria)

      End Function

      <Serializable()> _
      Public Class Criteria
        Inherits Csla.CriteriaBase(Of Criteria)

      End Class

      Protected Overrides Sub DataPortal_Fetch(criteria As Object)

        Dim crit As Criteria = criteria
        Using cn As New SqlConnection(Singular.Settings.ConnectionString)
          cn.Open()
          Try
            Using cm As SqlCommand = cn.CreateCommand
              cm.CommandType = CommandType.StoredProcedure
              cm.CommandText = "[CmdProcs].[cmdGetHRIDs]"
              cm.CommandTimeout = 180
              Using sdr As New Csla.Data.SafeDataReader(cm.ExecuteReader)
                While sdr.Read
                  Me.Add(New IDNumberDate() With {.IDNumber = sdr.GetString(0),
                                                  .TR_TermSLA = sdr.GetString(1),
                                                  .LastDate = sdr.GetInt32(2),
                                                  .LastSQ = sdr.GetInt32(3)})
                End While
              End Using
            End Using
          Finally
            cn.Close()
          End Try
        End Using

      End Sub

    End Class

    'Private Shared Function GetIDList() As List(Of IDNumberDate)

    '  Dim IDtable As DataTable
    '  Dim cmdProc As New Singular.CommandProc("[CmdProcs].[cmdGetHRIDs]")
    '  cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataSet
    '  cmdProc = cmdProc.Execute
    '  IDtable = cmdProc.Dataset.Tables(0)
    '  Return IDtable

    'End Function

    'Private Shared Function GetHRIDsString() As String

    '  Dim HRsString As String = ""
    '  For Each idnd In IDNumberDateList.GetIDNumberDateList
    '    If HRsString.Length <> 0 Then
    '      HRsString &= " UNION ALL" & vbCrLf
    '    End If
    '    HRsString &= idnd.GetSQLSelectValues & vbCrLf
    '  Next
    '  Return HRsString

    'End Function

    Private Shared Function GetHRIDsString() As String

      Dim HRsString As String = ""
      Dim IDNumberList = IDNumberDateList.GetIDNumberDateList
      'For Each idnd In IDNumberList
      '  If HRsString.Length <> 0 Then
      '    HRsString &= " UNION ALL" & vbCrLf
      '  End If
      '  HRsString &= idnd.GetSQLSelectValues & vbCrLf
      'Next
      Dim xmlString = GetHRIDsStringXMl(IDNumberList)

      Return xmlString

    End Function

    Private Shared Function GetHRIDsStringXMl(IDNumberList As IDNumberDateList) As String

      Dim HRsString As String = ""

      HRsString = (New XElement("DataSet", (From id In IDNumberList
                                                    Select New XElement("Table",
                                                                        New XAttribute("IDNo", id.IDNumber),
                                                                        New XAttribute("TR_TermSLA", id.TR_TermSLA),
                                                                        New XAttribute("lastDate", id.LastDate),
                                                                        New XAttribute("LastSQ", id.LastSQ)
                                                   )).ToArray())).ToString


      Return HRsString

    End Function


    Private Shared Function GetSQLString() As String
      Dim SQLString As String = ""

      SQLString = My.Resources.GetCentralSecurityData()
      SQLString = SQLString.Replace("@unionIDs", GetHRIDsString())
      Return SQLString
    End Function


  End Class

End Namespace
