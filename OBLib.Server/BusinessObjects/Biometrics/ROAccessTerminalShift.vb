﻿' Generated 05 Jun 2015 17:16 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class ROAccessTerminalShift
    Inherits OBReadOnlyBase(Of ROAccessTerminalShift)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessTransactionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessTransactionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AccessTransactionID() As Integer
      Get
        Return GetProperty(AccessTransactionIDProperty)
      End Get
    End Property

    Public Shared AccessDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AccessDateTime, "Access Date Time")
    ''' <summary>
    ''' Gets the Access Date Time value
    ''' </summary>
    <Display(Name:="Access Date Time", Description:="")>
  Public ReadOnly Property AccessDateTime As DateTime?
      Get
        Return GetProperty(AccessDateTimeProperty)
      End Get
    End Property

    Public Shared AccessTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccessTime, "Access Time")
    ''' <summary>
    ''' Gets the Access Date Time value
    ''' </summary>
    <Display(Name:="Time", Description:="")>
    Public ReadOnly Property AccessTime As String
      Get
        Return New Csla.SmartDate(AccessDateTime).ToString("HH:mm.ss")
      End Get
    End Property


    Public Shared TerminalProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Terminal, "Terminal")
    ''' <summary>
    ''' Gets the Terminal value
    ''' </summary>
    <Display(Name:="Terminal", Description:="")>
  Public ReadOnly Property Terminal() As String
      Get
        Return GetProperty(TerminalProperty)
      End Get
    End Property

    Public Shared StartEndIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.StartEndInd, "Start End", False)
    ''' <summary>
    ''' Gets the Start End value
    ''' </summary>
    <Display(Name:="Start/End", Description:="")>
    Public ReadOnly Property StartEndInd() As Boolean
      Get
        Return GetProperty(StartEndIndProperty)
      End Get
    End Property

    Public Shared AccessTerminalTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccessTerminalType, "Access Terminal Type")
    ''' <summary>
    ''' Gets the Access Terminal Type value
    ''' </summary>
    <Display(Name:="Access Terminal Type", Description:="")>
  Public ReadOnly Property AccessTerminalType() As String
      Get
        Return GetProperty(AccessTerminalTypeProperty)
      End Get
    End Property

    Public Shared TimedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TimedInd, "Bio Terminal?", False)
    ''' <summary>
    ''' Gets the Start End value
    ''' </summary>
    <Display(Name:="Bio Terminal?", Description:="Bio Terminal?")>
    Public ReadOnly Property TimedInd() As Boolean
      Get
        If AccessTerminalType.Length > 0 Then
          Return True
        End If
        Return False
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessTransactionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Terminal

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAccessTerminalShift(dr As SafeDataReader) As ROAccessTerminalShift

      Dim r As New ROAccessTerminalShift()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AccessTransactionIDProperty, .GetInt32(0))
        LoadProperty(AccessDateTimeProperty, .GetValue(1))
        LoadProperty(TerminalProperty, .GetString(2))
        LoadProperty(StartEndIndProperty, .GetBoolean(3))
        LoadProperty(AccessTerminalTypeProperty, .GetString(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace