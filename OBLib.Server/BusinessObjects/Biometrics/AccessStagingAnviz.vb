﻿' Generated 30 Mar 2015 14:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class AccessStagingAnviz
    Inherits OBBusinessBase(Of AccessStagingAnviz)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared LogIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.LogID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property LogID() As Integer
      Get
        Return GetProperty(LogIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.UserID, "User", "")
    ''' <summary>
    ''' Gets and sets the User value
    ''' </summary>
    <Display(Name:="User", Description:=""),
    StringLength(20, ErrorMessage:="User cannot be more than 20 characters")>
    Public Property UserID() As String
      Get
        Return GetProperty(UserIDProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(UserIDProperty, Value)
      End Set
    End Property

    Public Shared CheckTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CheckTime, "Check Time")
    ''' <summary>
    ''' Gets and sets the Check Time value
    ''' </summary>
    <Display(Name:="Check Time", Description:=""),
    Required(ErrorMessage:="Check Time required")>
    Public Property CheckTime As DateTime?
      Get
        Return GetProperty(CheckTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CheckTimeProperty, Value)
      End Set
    End Property

    Public Shared CheckTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CheckType, "Check Type", "")
    ''' <summary>
    ''' Gets and sets the Check Type value
    ''' </summary>
    <Display(Name:="Check Type", Description:=""),
    StringLength(2, ErrorMessage:="Check Type cannot be more than 2 characters")>
    Public Property CheckType() As String
      Get
        Return GetProperty(CheckTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CheckTypeProperty, Value)
      End Set
    End Property

    Public Shared SensorIDProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SensorID, "Sensor", "")
    ''' <summary>
    ''' Gets and sets the Sensor value
    ''' </summary>
    <Display(Name:="Sensor", Description:=""),
    StringLength(10, ErrorMessage:="Sensor cannot be more than 10 characters")>
    Public Property SensorID() As String
      Get
        Return GetProperty(SensorIDProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SensorIDProperty, Value)
      End Set
    End Property

    Public Shared CheckedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Checked, "Checked", False)
    ''' <summary>
    ''' Gets and sets the Checked value
    ''' </summary>
    <Display(Name:="Checked", Description:=""),
    Required(ErrorMessage:="Checked required")>
    Public Property Checked() As Boolean
      Get
        Return GetProperty(CheckedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CheckedProperty, Value)
      End Set
    End Property

    Public Shared WorkTypeProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WorkType, "Work Type", 0)
    ''' <summary>
    ''' Gets and sets the Work Type value
    ''' </summary>
    <Display(Name:="Work Type", Description:=""),
    Required(ErrorMessage:="Work Type required")>
    Public Property WorkType() As Integer
      Get
        Return GetProperty(WorkTypeProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WorkTypeProperty, Value)
      End Set
    End Property

    Public Shared AttFlagProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AttFlag, "Att Flag", 0)
    ''' <summary>
    ''' Gets and sets the Att Flag value
    ''' </summary>
    <Display(Name:="Att Flag", Description:=""),
    Required(ErrorMessage:="Att Flag required")>
    Public Property AttFlag() As Integer
      Get
        Return GetProperty(AttFlagProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AttFlagProperty, Value)
      End Set
    End Property

    Public Shared OpenDoorFlagProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OpenDoorFlag, "Open Door Flag", False)
    ''' <summary>
    ''' Gets and sets the Open Door Flag value
    ''' </summary>
    <Display(Name:="Open Door Flag", Description:=""),
    Required(ErrorMessage:="Open Door Flag required")>
    Public Property OpenDoorFlag() As Boolean
      Get
        Return GetProperty(OpenDoorFlagProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OpenDoorFlagProperty, Value)
      End Set
    End Property

    Public Shared FingerClientNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FingerClientName, "Finger Client Name", "")
    ''' <summary>
    ''' Gets and sets the Finger Client Name value
    ''' </summary>
    <Display(Name:="Finger Client Name", Description:=""),
    StringLength(100, ErrorMessage:="Finger Client Name cannot be more than 100 characters")>
    Public Property FingerClientName() As String
      Get
        Return GetProperty(FingerClientNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FingerClientNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(LogIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.UserID.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Staging Anviz")
        Else
          Return String.Format("Blank {0}", "Access Staging Anviz")
        End If
      Else
        Return Me.UserID
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessStagingAnviz() method.

    End Sub

    Public Shared Function NewAccessStagingAnviz() As AccessStagingAnviz

      Return DataPortal.CreateChild(Of AccessStagingAnviz)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessStagingAnviz(dr As SafeDataReader) As AccessStagingAnviz

      Dim a As New AccessStagingAnviz()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(LogIDProperty, .GetInt32(0))
          LoadProperty(UserIDProperty, .GetString(1))
          LoadProperty(CheckTimeProperty, .GetValue(2))
          LoadProperty(CheckTypeProperty, .GetString(3))
          LoadProperty(SensorIDProperty, .GetString(4))
          LoadProperty(CheckedProperty, .GetBoolean(5))
          LoadProperty(WorkTypeProperty, .GetInt32(6))
          LoadProperty(AttFlagProperty, .GetInt32(7))
          LoadProperty(OpenDoorFlagProperty, .GetBoolean(8))
          LoadProperty(FingerClientNameProperty, .GetString(9))
        End With
      End Using

      MarkAsChild()

      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessStagingAnviz"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database


    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

        
          .Parameters.AddWithValue("@LogID", GetProperty(LogIDProperty))
          .Parameters.AddWithValue("@UserID", GetProperty(UserIDProperty))
          .Parameters.AddWithValue("@CheckTime", (New SmartDate(GetProperty(CheckTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CheckType", GetProperty(CheckTypeProperty))
          .Parameters.AddWithValue("@SensorID", GetProperty(SensorIDProperty))
          .Parameters.AddWithValue("@Checked", GetProperty(CheckedProperty))
          .Parameters.AddWithValue("@WorkType", GetProperty(WorkTypeProperty))
          .Parameters.AddWithValue("@AttFlag", GetProperty(AttFlagProperty))
          .Parameters.AddWithValue("@OpenDoorFlag", GetProperty(OpenDoorFlagProperty))
          .Parameters.AddWithValue("@FingerClientName", GetProperty(FingerClientNameProperty))

          .ExecuteNonQuery()

 
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessStagingAnviz"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@LogID", GetProperty(LogIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace