﻿' Generated 25 Nov 2015 15:03 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class AccessStudioShiftDetails
    Inherits OBBusinessBase(Of AccessStudioShiftDetails)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared AccessStudioShiftIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessStudioShiftID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property AccessStudioShiftID() As Integer
      Get
        Return GetProperty(AccessStudioShiftIDProperty)
      End Get
    End Property

    Public Shared RoomScheduleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomSchedule, "Room Schedule")
    ''' <summary>
    ''' Gets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public ReadOnly Property RoomSchedule() As String
      Get
        Return GetProperty(RoomScheduleProperty)
      End Get
    End Property

    Public Shared RoomScheduleStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RoomScheduleStartDateTime, "Room Schedule Start Date Time")
    ''' <summary>
    ''' Gets the Room Schedule Start Date Time value
    ''' </summary>
    <Display(Name:="Room Schedule Start Date Time", Description:="")>
    Public ReadOnly Property RoomScheduleStartDateTime As DateTime?
      Get
        Return GetProperty(RoomScheduleStartDateTimeProperty)
      End Get
    End Property

    Public Shared RoomScheduleEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RoomScheduleEndDateTime, "Room Schedule End Date Time")
    ''' <summary>
    ''' Gets the Room Schedule End Date Time value
    ''' </summary>
    <Display(Name:="Room Schedule End Date Time", Description:="")>
    Public ReadOnly Property RoomScheduleEndDateTime As DateTime?
      Get
        Return GetProperty(RoomScheduleEndDateTimeProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared TimesheetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TimesheetDate, "Timesheet Date")
    ''' <summary>
    ''' Gets the Timesheet Date value
    ''' </summary>
    <Display(Name:="Timesheet Date", Description:="")>
    Public ReadOnly Property TimesheetDate As DateTime?
      Get
        Return GetProperty(TimesheetDateProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "ProductionSystemArea")
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="ProductionSystemArea", Description:="")>
    Public ReadOnly Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

#End Region

#Region "Expanded"
    '<DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    'Public Property Expanded As Boolean = False

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(d) d.IsExpanded, False)
    ''' <summary>
    ''' Gets the IsProcessing value
    ''' </summary>
    ''' 
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsExpandedProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.RoomSchedule

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessStudioShiftDetails(dr As SafeDataReader) As AccessStudioShiftDetails

      Dim r As New AccessStudioShiftDetails()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
        LoadProperty(AccessStudioShiftIDProperty, .GetInt32(1))
        LoadProperty(RoomScheduleProperty, .GetString(2))
        LoadProperty(RoomScheduleStartDateTimeProperty, .GetValue(3))
        LoadProperty(RoomScheduleEndDateTimeProperty, .GetValue(4))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(RoomProperty, .GetString(6))
        LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))

      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace