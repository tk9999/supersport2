﻿' Generated 30 Mar 2015 14:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class AccessStagingAnvizList
    Inherits OBBusinessListBase(Of AccessStagingAnvizList, AccessStagingAnviz)

#Region " Business Methods "

    Public Function GetItem(LogID As Integer) As AccessStagingAnviz

      For Each child As AccessStagingAnviz In Me
        If child.LogID = LogID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Access Staging Anvizs"

    End Function

    Private mAnvizLastLogID As Integer = 0
    Public ReadOnly Property AnvizLastLogID As Integer
      Get
        Return mAnvizLastLogID
      End Get
    End Property




#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property Accesspath As String

      Public Property LastLogID As Integer


      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewAccessStagingAnvizList() As AccessStagingAnvizList

      Return New AccessStagingAnvizList()

    End Function

    Public Shared Sub BeginGetAccessStagingAnvizList(CallBack As EventHandler(Of DataPortalResult(Of AccessStagingAnvizList)))

      Dim dp As New DataPortal(Of AccessStagingAnvizList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAccessStagingAnvizList(Accesspath As String, LastLogID As Integer) As AccessStagingAnvizList

      Return DataPortal.Fetch(Of AccessStagingAnvizList)(New Criteria() With {.Accesspath = Accesspath, .LastLogID = LastLogID})

    End Function

    Public Shared Function GetAHRIsToString(Accesspath As String) As String
      Dim MissingIDs = ""


      Using cnn As New OleDb.OleDbConnection(AccessConnectionString(Accesspath))
        cnn.Open()
        Try
          Using cmd As New OleDb.OleDbCommand(My.Resources.GetMissingIDs, cnn)
            Using sdr As New SafeDataReader(cmd.ExecuteReader())
              While sdr.Read
                MissingIDs += CStr(sdr.GetString(0)) + ", "
              End While
              MissingIDs = MissingIDs.Substring(0, (MissingIDs.Length - 2))
            End Using
          End Using
        Finally
          cnn.Close()
        End Try
      End Using

      Return MissingIDs
    End Function

    Public Shared Sub GetIdAndNameToInsert(MissingIDs As String, Accesspath As String)
      Dim insertScript As String = ""
      Dim insertStatement As String = ""
      MissingIDs = "'" + MissingIDs + "'"
      insertScript = My.Resources.GetInsertStatements()
      insertScript = insertScript.Replace("@IDs", MissingIDs)
      Try

        Dim cmd = New Singular.CommandProc(insertScript)
        cmd.CommandType = CommandType.Text
        cmd.FetchType = Singular.CommandProc.FetchTypes.DataRow
        cmd = cmd.Execute()
        insertStatement = cmd.DataRow(0)


      Catch ex As Exception
        Throw New Exception("Error Running InsertScript", ex)
      End Try
      '                     string that will be returned if no missing IDs found
      If insertStatement <> "INSERT INTO USerInfo (UserID, NAME) SELECT ID, n)" Then
        Try
          Using cnn As New OleDb.OleDbConnection(AccessConnectionString(Accesspath))
            cnn.Open()
            Try
              Using cmd As New OleDb.OleDbCommand(insertStatement, cnn)
                cmd.ExecuteNonQuery()
              End Using
            Finally
              cnn.Close()
            End Try
          End Using

        Catch ex As Exception
          Throw New Exception("Error inserting missing records to Access database: " + ex.Message + ": " + insertStatement, ex)
        End Try
      End If



    End Sub




    Public Sub Fetch(sdr As SafeDataReader)
      Dim MissingIDs As String = ""
      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AccessStagingAnviz.GetAccessStagingAnviz(sdr))
      End While


      'If sdr.NextResult Then
      '  While sdr.Read
      '    MissingIDs += CStr(sdr.GetInt32(1)) + ", "
      '  End While
      '  MissingIDs = MissingIDs.Trim(", ")
      'End If
      Me.RaiseListChangedEvents = True
    End Sub


    Public Sub UpdateMiscBiometrics()

      Try
        Dim cmdProc As New Singular.CommandProc("CmdProcs.[cmdPostUpdateBiometricsImport]")
        cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
        cmdProc.Parameters.AddWithValue("@ImportTypeID", CommonData.Enums.BiometricsImportType.AnvizImport)
        cmdProc = cmdProc.Execute
        Me.mAnvizLastLogID = CInt(cmdProc.DataRow(0))
      Catch ex As Exception
        Throw New Exception("Error updating Biometrics Stats", ex)
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria

      Using cnn As New OleDb.OleDbConnection(AccessConnectionString(crit.Accesspath))
        cnn.Open()
        Try
          Using cmd As New OleDb.OleDbCommand(My.Resources.GetCheckInOut, cnn)
            cmd.Parameters.AddWithValue("@LastLogID", crit.LastLogID)

            Using sdr As New SafeDataReader(cmd.ExecuteReader())
              Me.Fetch(sdr)
            End Using
          End Using
        Finally
          cnn.Close()
        End Try
      End Using

    End Sub

    'Protected Sub FetchAccessData(Accesspath As String, LastLogID As Integer)


    '  Using cnn As New OleDb.OleDbConnection(AccessConnectionString(Accesspath))
    '    cnn.Open()
    '    Try
    '      Using cmd As New OleDb.OleDbCommand(My.Resources.GetCheckInOut, cnn)
    '        cmd.Parameters.AddWithValue("@LastLogID", LastLogID)

    '        Using sdr As New SafeDataReader(cmd.ExecuteReader())
    '          Me.Fetch(sdr)
    '        End Using
    '      End Using
    '    Finally
    '      cnn.Close()
    '    End Try
    '  End Using

    'End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AccessStagingAnviz In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AccessStagingAnviz In Me
          Child.Insert()
        Next
        ' do a post update for misc
        UpdateMiscBiometrics()
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

    Sub FetchAccessData(Accesspath As String, AnvizLastLogID As Integer)
      Throw New NotImplementedException
    End Sub

  End Class

End Namespace