﻿
' Generated 25 Nov 2015 15:02 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class AccessStudioShift
    Inherits OBBusinessBase(Of AccessStudioShift)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessStudioShiftIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessStudioShiftID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AccessStudioShiftID() As Integer
      Get
        Return GetProperty(AccessStudioShiftIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Human Resource")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceName, "Human Resource Name")
    ''' <summary>
    ''' Gets the Human Resource Name value
    ''' </summary>
    <Display(Name:="Human Resource Name", Description:="")>
  Public ReadOnly Property HumanResourceName() As String
      Get
        Return GetProperty(HumanResourceNameProperty)
      End Get
    End Property

    Public Shared TimesheetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TimesheetDate, "Timesheet Date")
    ''' <summary>
    ''' Gets the Timesheet Date value
    ''' </summary>
    <Display(Name:="Timesheet Date", Description:="")>
  Public ReadOnly Property TimesheetDate As DateTime?
      Get
        Return GetProperty(TimesheetDateProperty)
      End Get
    End Property

    Public Shared ScheduleStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleStartDateTime, "Schedule Start Date Time")
    ''' <summary>
    ''' Gets the Schedule Start Date Time value
    ''' </summary>
    <Display(Name:="Schedule Start Time", Description:="")>
    Public ReadOnly Property ScheduleStartDateTime As DateTime?
      Get
        Return GetProperty(ScheduleStartDateTimeProperty)
      End Get
    End Property

    Public Shared ScheduleEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleEndDateTime, "Schedule End Date Time")
    ''' <summary>
    ''' Gets the Schedule End Date Time value
    ''' </summary>
    <Display(Name:="Schedule End Time", Description:="")>
    Public ReadOnly Property ScheduleEndDateTime As DateTime?
      Get
        Return GetProperty(ScheduleEndDateTimeProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Bio Start Time", Description:="")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="Bio End Time", Description:="")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared AccessShiftIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessShiftID, "Access Shift")
    ''' <summary>
    ''' Gets the Access Shift value
    ''' </summary>
    <Display(Name:="Access Shift", Description:="")>
  Public ReadOnly Property AccessShiftID() As Integer
      Get
        Return GetProperty(AccessShiftIDProperty)
      End Get
    End Property

    Public Shared AccessFlagIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessFlagID, "Access Flag")
    ''' <summary>
    ''' Gets the Access Flag value
    ''' </summary>
    <Display(Name:="Access Flag", Description:="")>
  Public ReadOnly Property AccessFlagID() As Integer
      Get
        Return GetProperty(AccessFlagIDProperty)
      End Get
    End Property

    Public Shared AccessFlagCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessFlagCount, "Access Flag")
    ''' <summary>
    ''' Gets the Access Flag value
    ''' </summary>
    <Display(Name:="Flags", Description:="")>
    Public ReadOnly Property AccessFlagCount() As Integer
      Get
        Return GetFlagCount()
      End Get
    End Property

    Public Shared FlagDecriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlagDecription, "Flag Decription", "")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Flag Decription", Description:="")>
    Public ReadOnly Property FlagDecription() As String
      Get
        Return GetProperty(FlagDecriptionProperty)
      End Get
    End Property

    Public Shared ShiftDurationProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShiftDuration, 0)
    ''' <summary>
    ''' Gets and Sets the Number of Weeks value
    ''' </summary>
    <Display(Name:="Shift Duration", Description:="")>
    Public ReadOnly Property ShiftDuration() As Decimal
      Get
        Return GetProperty(ShiftDurationProperty)
      End Get
    End Property

    Public Shared FlagReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.FlagReason, "") _
                                                                                                .AddSetExpression("FlagReasonChange(self)", False)
    ''' <summary>
    ''' Gets the Human Resource Name value
    ''' </summary>
    <Display(Name:="Flag Reason", Description:="")>
    Public Property FlagReason() As String
      Get
        Return GetProperty(FlagReasonProperty)
      End Get
      Set(value As String)
        SetProperty(FlagReasonProperty, value)
      End Set
    End Property

#End Region

#Region "Expanded"

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(d) d.IsExpanded, False)
    ''' <summary>
    ''' Gets the IsProcessing value
    ''' </summary>
    ''' 
    Public Overridable Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsExpandedProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared AccessStudioShiftDetailsListProperty As PropertyInfo(Of AccessStudioShiftDetailsList) = RegisterProperty(Of AccessStudioShiftDetailsList)(Function(c) c.AccessStudioShiftDetailsList, "RO Access Studio Shift Details List")

    Public ReadOnly Property AccessStudioShiftDetailsList() As AccessStudioShiftDetailsList
      Get
        If GetProperty(AccessStudioShiftDetailsListProperty) Is Nothing Then
          LoadProperty(AccessStudioShiftDetailsListProperty, Biometrics.AccessStudioShiftDetailsList.NewAccessStudioShiftDetailsList())
        End If
        Return GetProperty(AccessStudioShiftDetailsListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResourceName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Studio Shift")
        Else
          Return String.Format("Blank {0}", "Access Studio Shift")
        End If
      Else
        Return Me.HumanResourceName
      End If

    End Function

    Protected Function GetFlagCount() As Object

      Return CommonData.Lists.ROAccessFlagList.GetFlags(AccessFlagID).Count

    End Function


    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(FlagReasonProperty)
        .JavascriptRuleFunctionName = "CheckTimeChangedReason"
        .Severity = Singular.Rules.RuleSeverity.Warning
        .ServerRuleFunction = Function(ts)
                                If ts.FlagReason = "" AndAlso ts.AccessFlagID > 0 Then
                                  Return "Reason Required"
                                End If

                                Return ""
                              End Function

      End With
    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessStudioShift() method.

    End Sub

    Public Shared Function NewAccessStudioShift() As AccessStudioShift

      Return DataPortal.CreateChild(Of AccessStudioShift)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessStudioShift(dr As SafeDataReader) As AccessStudioShift

      Dim a As New AccessStudioShift()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessStudioShiftIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, .GetInt32(1))
          LoadProperty(HumanResourceNameProperty, .GetString(2))
          LoadProperty(TimesheetDateProperty, .GetValue(3))
          LoadProperty(ScheduleStartDateTimeProperty, .GetValue(4))
          LoadProperty(ScheduleEndDateTimeProperty, .GetValue(5))
          LoadProperty(StartDateTimeProperty, .GetValue(6))
          LoadProperty(EndDateTimeProperty, .GetValue(7))
          LoadProperty(AccessShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(AccessFlagIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(FlagDecriptionProperty, .GetString(10))
          LoadProperty(ShiftDurationProperty, .GetDecimal(11))
          LoadProperty(FlagReasonProperty, .GetString(12))
           
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessStudioShift"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessStudioShift"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceID As SqlParameter = .Parameters.Add("@HumanResourceID", SqlDbType.Int)
          paramHumanResourceID.Value = GetProperty(HumanResourceIDProperty)
          If Me.IsNew Then
            paramHumanResourceID.Direction = ParameterDirection.Output
          End If
  
          .Parameters.AddWithValue("@AccessShiftID", GetProperty(AccessShiftIDProperty)) 
          .Parameters.AddWithValue("@PlannedTimeChangedReason", GetProperty(FlagReasonProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceIDProperty, paramHumanResourceID.Value)
          End If
          ' update child objects
          MarkOld()
        End With
      Else
        ' update child objects
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAccessStudioShift"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace