﻿' Generated 25 Nov 2015 15:01 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class AccessRoomScheduleHRList
    Inherits OBBusinessListBase(Of AccessRoomScheduleHRList, AccessRoomScheduleHR)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As AccessRoomScheduleHR

      For Each child As AccessRoomScheduleHR In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByAccessShift(AccessShiftID As Integer) As AccessRoomScheduleHR

      For Each child As AccessRoomScheduleHR In Me
        If child.AccessShiftID = AccessShiftID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "HR Shifts"

    End Function


#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Object
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing

      Public Sub New()


      End Sub



      Public Sub New(StartDate As DateTime?, EndDate As DateTime?, Optional ProductionID As Integer? = Nothing)

        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.ProductionID = ProductionID

      End Sub




    End Class

#Region " Common "

    Public Shared Function NewAccessRoomScheduleHRList() As AccessRoomScheduleHRList

      Return New AccessRoomScheduleHRList()

    End Function

    Public Shared Sub BeginGetAccessRoomScheduleHRList(CallBack As EventHandler(Of DataPortalResult(Of AccessRoomScheduleHRList)))

      Dim dp As New DataPortal(Of AccessRoomScheduleHRList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAccessRoomScheduleHRList() As AccessRoomScheduleHRList

      Return DataPortal.Fetch(Of AccessRoomScheduleHRList)(New Criteria())

    End Function

    Public Shared Function GetAccessRoomScheduleHRList(StartDate As Date, EndDate As Date, ProductionID As Integer?) As AccessRoomScheduleHRList

      Return DataPortal.Fetch(Of AccessRoomScheduleHRList)(New Criteria(StartDate, EndDate, ProductionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AccessRoomScheduleHR.GetAccessRoomScheduleHR(sdr))
      End While
      Me.RaiseListChangedEvents = True



      For Each child As AccessRoomScheduleHR In Me
        child.CheckRules()
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAccessStudioShiftList"
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", CurrentUser.SystemID)

            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AccessRoomScheduleHR In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AccessRoomScheduleHR In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace