﻿' Generated 09 Jun 2015 11:54 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class ROAccessFlag
    Inherits OBReadOnlyBase(Of ROAccessFlag)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessFlagIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessFlagID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public ReadOnly Property AccessFlagID() As Integer
      Get
        Return GetProperty(AccessFlagIDProperty)
      End Get
    End Property

    Public Shared FlagReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlagReason, "Flag Reason", "")
    ''' <summary>
    ''' Gets the Flag Reason value
    ''' </summary>
    <Display(Name:="Flag Reason", Description:="")>
  Public ReadOnly Property FlagReason() As String
      Get
        Return GetProperty(FlagReasonProperty)
      End Get
    End Property
 
#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessFlagIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FlagReason

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAccessFlag(dr As SafeDataReader) As ROAccessFlag

      Dim r As New ROAccessFlag()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AccessFlagIDProperty, .GetInt32(0))
        LoadProperty(FlagReasonProperty, .GetString(1)) 
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace