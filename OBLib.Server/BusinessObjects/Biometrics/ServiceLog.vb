﻿' Generated 07 Apr 2015 11:13 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class ServiceLog
    Inherits OBBusinessBase(Of ServiceLog)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ServiceLogIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ServiceLogID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ServiceLogID() As Integer
      Get
        Return GetProperty(ServiceLogIDProperty)
      End Get
    End Property

    Public Shared LogDateTmeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LogDateTme, "Log Date Tme")
    ''' <summary>
    ''' Gets and sets the Log Date Tme value
    ''' </summary>
    <Display(Name:="Log Date Tme", Description:=""),
    Required(ErrorMessage:="Log Date Tme required")>
  Public Property LogDateTme As DateTime?
      Get
        Return GetProperty(LogDateTmeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LogDateTmeProperty, Value)
      End Set
    End Property

    Public Shared ServiceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ServiceName, "Service Name", "")
    ''' <summary>
    ''' Gets and sets the Service Name value
    ''' </summary>
    <Display(Name:="Service Name", Description:=""),
    StringLength(100, ErrorMessage:="Service Name cannot be more than 100 characters")>
  Public Property ServiceName() As String
      Get
        Return GetProperty(ServiceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ServiceNameProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
  Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ServiceLogIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ServiceName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Service Log")
        Else
          Return String.Format("Blank {0}", "Service Log")
        End If
      Else
        Return Me.ServiceName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewServiceLog() method.

    End Sub

    Public Shared Function NewServiceLog() As ServiceLog

      Return DataPortal.CreateChild(Of ServiceLog)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetServiceLog(dr As SafeDataReader) As ServiceLog

      Dim s As New ServiceLog()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ServiceLogIDProperty, .GetInt32(0))
          LoadProperty(LogDateTmeProperty, .GetValue(1))
          LoadProperty(ServiceNameProperty, .GetString(2))
          LoadProperty(DescriptionProperty, .GetString(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insServiceLog"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updServiceLog"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramServiceLogID As SqlParameter = .Parameters.Add("@ServiceLogID", SqlDbType.Int)
          paramServiceLogID.Value = GetProperty(ServiceLogIDProperty)
          If Me.IsNew Then
            paramServiceLogID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@LogDateTme", (New SmartDate(GetProperty(LogDateTmeProperty))).DBValue)
          .Parameters.AddWithValue("@ServiceName", GetProperty(ServiceNameProperty))
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ServiceLogIDProperty, paramServiceLogID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delServiceLog"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ServiceLogID", GetProperty(ServiceLogIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace