﻿' Generated 25 Nov 2015 15:01 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class AccessStudioShiftList
    Inherits OBBusinessListBase(Of AccessStudioShiftList, AccessStudioShift)

#Region " Business Methods "

    Public Function GetItem(AccessStudioShiftID As Integer) As AccessStudioShift

      For Each child As AccessStudioShift In Me
        If child.AccessStudioShiftID = AccessStudioShiftID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByAccessShift(AccessShiftID As Integer) As AccessStudioShift

      For Each child As AccessStudioShift In Me
        If child.AccessShiftID = AccessShiftID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Studio Shifts"

    End Function

    Public Function GetAccessStudioShiftDetails(RoomScheduleID As Integer) As AccessStudioShiftDetails

      Dim obj As AccessStudioShiftDetails = Nothing
      For Each parent As AccessStudioShift In Me
        obj = parent.AccessStudioShiftDetailsList.GetItem(RoomScheduleID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Object
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing

      Public Sub New()


      End Sub



      Public Sub New(StartDate As DateTime?, EndDate As DateTime?, Optional ProductionID As Integer? = Nothing)

        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.ProductionID = ProductionID

      End Sub




    End Class

#Region " Common "

    Public Shared Function NewAccessStudioShiftList() As AccessStudioShiftList

      Return New AccessStudioShiftList()

    End Function

    Public Shared Sub BeginGetAccessStudioShiftList(CallBack As EventHandler(Of DataPortalResult(Of AccessStudioShiftList)))

      Dim dp As New DataPortal(Of AccessStudioShiftList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAccessStudioShiftList() As AccessStudioShiftList

      Return DataPortal.Fetch(Of AccessStudioShiftList)(New Criteria())

    End Function

    Public Shared Function GetAccessStudioShiftList(StartDate As Date, EndDate As Date, ProductionID As Integer?) As AccessStudioShiftList

      Return DataPortal.Fetch(Of AccessStudioShiftList)(New Criteria(StartDate, EndDate, ProductionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AccessStudioShift.GetAccessStudioShift(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As AccessStudioShift = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AccessStudioShiftID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.AccessStudioShiftDetailsList.RaiseListChangedEvents = False
          parent.AccessStudioShiftDetailsList.Add(AccessStudioShiftDetails.GetAccessStudioShiftDetails(sdr))
          parent.AccessStudioShiftDetailsList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As AccessStudioShift In Me
        child.CheckRules()
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAccessStudioShiftList"
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", CurrentUser.SystemID)

            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AccessStudioShift In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AccessStudioShift In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace