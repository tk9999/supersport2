﻿' Generated 30 May 2016 14:30 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class AccessStudioSupervisorList
    Inherits OBBusinessListBase(Of AccessStudioSupervisorList, AccessStudioSupervisor)

#Region " Business Methods "

    Public Function GetItem(AccessShiftID As Integer) As AccessStudioSupervisor

      For Each child As AccessStudioSupervisor In Me
        If child.AccessShiftID = AccessShiftID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItem(TimeSheetDate As Date) As AccessStudioSupervisor

      For Each child As AccessStudioSupervisor In Me
        If child.TimesheetDate = TimeSheetDate Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetAccessStudioShift(ProductionSystemAreaID As Integer) As AccessRoomSchedule

      For Each childSup As AccessStudioSupervisor In Me
        For Each child As AccessRoomSchedule In childSup.AccessRoomScheduleList
          If child.ProductionSystemAreaID = ProductionSystemAreaID Then
            Return child
          End If
        Next
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Access Studio Supervisors"

    End Function

    Public Function GetItemByAccessShift(AccessShiftID As Integer) As Object
      ''todo:AKHONA
      'For Each childSup As AccessStudioSupervisor In Me
      '  If childSup.AccessShiftID = AccessShiftID Then
      '    Return childSup
      '  End If
      '  For Each child As AccessStudioShift In childSup.AccessStudioShiftList
      '    If child.AccessShiftID = AccessShiftID Then
      '      Return child
      '    End If
      '  Next
      'Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(StartDate As DateTime?, EndDate As DateTime?)

        Me.StartDate = StartDate
        Me.EndDate = EndDate

      End Sub

    End Class

    Public Shared Function NewAccessStudioSupervisorList() As AccessStudioSupervisorList

      Return New AccessStudioSupervisorList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetAccessStudioSupervisorList() As AccessStudioSupervisorList

      Return DataPortal.Fetch(Of AccessStudioSupervisorList)(New Criteria())

    End Function

    Public Shared Function GetAccessStudioSupervisorList(StartDate As Date, EndDate As Date) As AccessStudioSupervisorList

      Return DataPortal.Fetch(Of AccessStudioSupervisorList)(New Criteria(StartDate, EndDate))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AccessStudioSupervisor.GetAccessStudioSupervisor(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parentSup As AccessStudioSupervisor = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentSup Is Nothing OrElse parentSup.TimesheetDate <> sdr.GetDateTime(8) Then
            parentSup = Me.GetItem(sdr.GetDateTime(8))
          End If
          parentSup.AccessRoomScheduleList.RaiseListChangedEvents = False
          parentSup.AccessRoomScheduleList.Add(AccessRoomSchedule.GetAccessRoomSchedule(sdr))
          parentSup.AccessRoomScheduleList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentStudio As AccessRoomSchedule = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentStudio Is Nothing OrElse parentStudio.ProductionSystemAreaID <> sdr.GetInt32(1) Then
            parentStudio = Me.GetAccessStudioShift(sdr.GetInt32(1))
          End If
          parentStudio.AccessRoomScheduleHRList.RaiseListChangedEvents = False
          parentStudio.AccessRoomScheduleHRList.Add(AccessRoomScheduleHR.GetAccessRoomScheduleHR(sdr))
          parentStudio.AccessRoomScheduleHRList.RaiseListChangedEvents = True
        End While
      End If


      For Each childSup As AccessStudioSupervisor In Me
        childSup.CheckRules()
        For Each child As AccessRoomSchedule In childSup.AccessRoomScheduleList
          For Each childhr As AccessRoomScheduleHR In child.AccessRoomScheduleHRList
            childhr.CheckRules()
          Next
        Next
      Next


    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAccessStudioShiftList"
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@SystemID", OBLib.CommonData.Enums.System.ProductionServices)
            cm.Parameters.AddWithValue("@ProductionAreaID", OBLib.CommonData.Enums.ProductionArea.Studio)

            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Public Sub SaveFlagReason(AccessShiftID As Object, FlagReason As String,
                                HumanResourceID As Integer,
                                HumanResourceShiftID As Integer,
                                ScheduleDate As DateTime
                                )


      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            With cm
              .CommandText = "UpdProcsWeb.updAccessStudioShift"
              .CommandType = CommandType.StoredProcedure
              .Parameters.AddWithValue("@AccessShiftID", AccessShiftID)
              .Parameters.AddWithValue("@PlannedTimeChangedReason", FlagReason)

              .Parameters.AddWithValue("@HumanResourceID", HumanResourceID)
              .Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.ZeroNothingDBNull(HumanResourceShiftID))
              .Parameters.AddWithValue("@ScheduleDate", ScheduleDate)
              .Parameters.AddWithValue("@ModifiedBy", CurrentUser.UserID)

              .ExecuteNonQuery()


            End With
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace