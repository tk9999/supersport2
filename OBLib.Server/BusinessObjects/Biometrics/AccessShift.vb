﻿' Generated 22 Jun 2015 16:04 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ShiftPatterns.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class AccessShift
    Inherits OBBusinessBase(Of AccessShift)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessShiftIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessShiftID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AccessShiftID() As Integer
      Get
        Return GetProperty(AccessShiftIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(HumanResourceIDProperty, value)
      End Set
    End Property
 

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="Biometrics date")>
    Public ReadOnly Property StartDateTime As DateTime
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property



    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="Biometrics date")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Human Resource Shift", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:="")>
    Public Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared AccessFlagIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessFlagID, "Access Flag", 0)
    ''' <summary>
    ''' Gets and sets the Access Flag value
    ''' </summary>
    <Display(Name:="Access Flag", Description:="")>
    Public Property AccessFlagID() As Integer
      Get
        Return GetProperty(AccessFlagIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AccessFlagIDProperty, Value)
        Dim af = CommonData.Lists.ROAccessFlagList.GetItem(AccessFlagID)
        If af IsNot Nothing Then
          FlagDecription = af.FlagReason
        End If
      End Set
    End Property

    Public Shared FlaggedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.FlaggedDateTime, "Flagged Date Time")
    ''' <summary>
    ''' Gets and sets the Flagged Date Time value
    ''' </summary>
    <Display(Name:="Flagged Date Time", Description:="")>
    Public Property FlaggedDateTime As DateTime?
      Get
        Return GetProperty(FlaggedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(FlaggedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production", 0)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared AccessShiftOptionIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.AccessShiftOptionInd, "Access Shift Option", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Access Shift Option value
    ''' </summary>
    <Display(Name:="Access Shift Option", Description:="Tick Indicates that Accepting an access shift without a planned shift, Untick is reject ")>
    Public Property AccessShiftOptionInd() As Boolean?
      Get
        Return GetProperty(AccessShiftOptionIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(AccessShiftOptionIndProperty, Value)
        SetProperty(PlannedShiftOptionByProperty, CurrentUser.UserID)
        SetProperty(PlannedShiftOptionDateTimeProperty, Now)
      End Set
    End Property

    Public Shared PlannedShiftOptionByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PlannedShiftOptionBy, "Access Shift Option By", 0)
    ''' <summary>
    ''' Gets and sets the Access Shift Option By value
    ''' </summary>
    <Display(Name:="Access Shift Option By", Description:="links to the person who actioned the option")>
    Public Property PlannedShiftOptionBy() As Integer
      Get
        Return GetProperty(PlannedShiftOptionByProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PlannedShiftOptionByProperty, Value)
      End Set
    End Property

    Public Shared PlannedShiftOptionDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlannedShiftOptionDateTime, "Access Shift Option Date")
    ''' <summary>
    ''' Gets and sets the Access Shift Option Date value
    ''' </summary>
    <Display(Name:="Access Shift Option Date", Description:="")>
    Public Property PlannedShiftOptionDateTime As DateTime?
      Get
        Return GetProperty(PlannedShiftOptionDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(PlannedShiftOptionDateTimeProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared TotalShiftHoursProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalShiftHours, "Total Shift Hours", 0)
    ''' <summary>
    ''' Gets the Total Hours Worked value
    ''' </summary>
    <Display(Name:="Total Shift Hours", Description:="")>
    Public ReadOnly Property TotalShiftHours() As Integer
      Get
        Return GetProperty(TotalShiftHoursProperty)
      End Get
    End Property

    Public Shared SkillsCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SkillsCount, "Skills Count", 0)
    ''' <summary>
    ''' Gets the Total Hours Worked value
    ''' </summary>
    <Display(Name:="Skills Count", Description:="")>
    Public ReadOnly Property SkillsCount() As Integer
      Get
        Return GetProperty(SkillsCountProperty)
      End Get
    End Property

    Public Shared ShiftStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ShiftStartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date", Description:="HR Shift date")>
    Public Property ShiftStartDateTime As DateTime?
      Get
        Return GetProperty(ShiftStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ShiftStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ShiftEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ShiftEndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:="HR Shift date")>
    Public Property ShiftEndDateTime As DateTime?
      Get
        Return GetProperty(ShiftEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ShiftEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftTypeID, "Shift Type", 0)
    ''' <summary>
    ''' Gets and sets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSystemAreaShiftTypeList))>
    Public Property ShiftTypeID() As Integer
      Get
        Return GetProperty(AccessFlagIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AccessFlagIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline", 0)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(ROProductionAreaAllowedDisciplineList), FilterMethodName:="GetAllowedDisciplines", ValueMember:="DisciplineID", DisplayMember:="Discipline")>
    Public Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared ShiftDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ShiftDate, "Shift Date")
    ''' <summary>
    ''' Gets and sets the Timesheet Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public Property ShiftDate As DateTime?
      Get
        Return GetProperty(ShiftDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ShiftDateProperty, Value)
      End Set
    End Property

    Public Shared PlannedTimeChangedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PlannedTimeChangedReason, "Planned Start Time Changed Reason", "")
    ''' <summary>
    ''' Gets the Timesheet Month value
    ''' </summary>
    <Display(Name:="Time Changed Reason", Description:="Planned shift Time Changed Reason")>
    Public Property PlannedTimeChangedReason() As String
      Get
        Return GetProperty(PlannedTimeChangedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PlannedTimeChangedReasonProperty, Value)
      End Set
    End Property

    Public Shared ChangedShiftStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ChangedShiftStartDateTime, Nothing) 
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date", Description:="HR Shift date")>
    Public Property ChangedShiftStartDateTime As DateTime?
      Get
        Return GetProperty(ChangedShiftStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ChangedShiftStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ChangedShiftEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ChangedShiftEndDateTime, Nothing)

    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:="HR Shift date")>
    Public Property ChangedShiftEndDateTime As DateTime?
      Get
        Return GetProperty(ChangedShiftEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ChangedShiftEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared FlagDecriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FlagDecription, "Flag Decription", "")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Flag Decription", Description:="")>
    Public Property FlagDecription() As String
      Get
        Return GetProperty(FlagDecriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FlagDecriptionProperty, Value)
      End Set
    End Property

    Public Shared MustValidateAccessShiftProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MustValidateAccessShift, "", True)
    ''' <summary>
    ''' Gets and sets the Access Shift Option value
    ''' </summary>
    <Display(Name:="Must Validate AccessShift", Description:="")>
    Public Property MustValidateAccessShift() As Boolean
      Get
        Return GetProperty(MustValidateAccessShiftProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(MustValidateAccessShiftProperty, Value)
      End Set
    End Property

    Public Shared BiometricsMissingShiftDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.BiometricsMissingShiftDate, "Biometrics Missing Shift Date")
    ''' <summary>
    ''' Gets and sets the Access Shift Option Date value
    ''' </summary>
    <Display(Name:="Biometrics Missing Shift Date", Description:="")>
    Public Property BiometricsMissingShiftDate As DateTime?
      Get
        Return GetProperty(BiometricsMissingShiftDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(BiometricsMissingShiftDateProperty, Value)
      End Set
    End Property



    <Singular.DataAnnotations.ClientOnly()>
    Public Property IsLoading As Boolean = False

    <Singular.DataAnnotations.ClientOnly()>
    Public Property SyncID As Integer = 0

    <Singular.DataAnnotations.ClientOnly()>
    Public Property AccessGroupID As Integer = OBLib.CommonData.Enums.AccessTerminalGroup.ICR
#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessShiftIDProperty)

    End Function

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, Function(c) "Biometrics Shifts")

    Public Overrides Function ToString() As String
       
      Return "Biometrics Shifts"

    End Function

    Private mFlagTotal As Integer = 0
    Public Shared AccessFlagListProperty As PropertyInfo(Of ROAccessFlagList) = RegisterProperty(Of ROAccessFlagList)(Function(c) c.AccessFlagList, "Flags")

    Public ReadOnly Property AccessFlagList() As ROAccessFlagList
      Get
        If GetProperty(AccessFlagListProperty) Is Nothing OrElse AccessFlagID <> mFlagTotal Then

          LoadProperty(AccessFlagListProperty, Biometrics.ROAccessFlagList.GetROAccessFlagList(AccessFlagID))

        End If
        Return GetProperty(AccessFlagListProperty)
      End Get
    End Property

 

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(PlannedTimeChangedReasonProperty)
        .JavascriptRuleFunctionName = "AccessShiftBO.CheckTimeChangedReason"
        .Severity = Singular.Rules.RuleSeverity.Warning
        .ServerRuleFunction = Function(ts)
                                If ValidateGroupRules(OBLib.CommonData.Enums.AccessTerminalGroup.Outside_Broadcast) Then
                                  If ts.ChangedShiftStartDateTime IsNot Nothing OrElse ts.ChangedShiftEndDateTime IsNot Nothing Then
                                    If ts.PlannedTimeChangedReason = "" Then
                                      Return "Reason Required"
                                    End If
                                  End If
                                End If
                                Return ""
                              End Function

      End With

      With AddWebRule(ShiftTypeIDProperty)
        .JavascriptRuleFunctionName = "AccessShiftBO.CheckShiftType"
        .ServerRuleFunction = Function(ts)
                                If ValidateGroupRules(OBLib.CommonData.Enums.AccessTerminalGroup.ICR) Then
                                  If ts.AccessShiftOptionInd AndAlso Singular.Misc.IsNullNothing(ts.ShiftTypeID, True) Then
                                    Return "Shift type is required"
                                  End If
                                End If
                                Return ""
                              End Function
        .AffectedProperties.Add(AccessShiftOptionIndProperty)
        .AddTriggerProperty(AccessShiftOptionIndProperty)
      End With

      With AddWebRule(DisciplineIDProperty)
        .JavascriptRuleFunctionName = "AccessShiftBO.CheckDiscipline"
        .ServerRuleFunction = Function(ts)
                                If ValidateGroupRules(OBLib.CommonData.Enums.AccessTerminalGroup.ICR) Then
                                  If ts.AccessShiftOptionInd AndAlso Singular.Misc.IsNullNothing(ts.DisciplineID, True) Then
                                    Return "Discipline is required"
                                  End If
                                End If
                                Return ""

                              End Function
        .AffectedProperties.Add(AccessShiftOptionIndProperty)
        .AddTriggerProperty(AccessShiftOptionIndProperty)
      End With

      'With AddWebRule(ShiftEndDateTimeProperty,
      '    Function(d) CType(Me.Parent, AccessShiftList).AccessGroupID = OBLib.CommonData.Enums.AccessTerminalGroup.ICR AndAlso Singular.Dates.SafeCompare(Function() d.ShiftEndDateTime < d.ShiftStartDateTime) AndAlso d.AccessShiftOptionInd,
      '    Function(d) "Start date must be before end date")
      '  .AffectedProperties.Add(ShiftStartDateTimeProperty)
      '  .AddTriggerProperty(ShiftStartDateTimeProperty)
      '  '  .JavascriptRuleFunctionName = "CheckDates" ' ,
      'End With

    End Sub

    Private Function ValidateGroupRules(RuleAccessGroupID As Integer) As Boolean

      If Me.AccessGroupID = RuleAccessGroupID Then
        Return MustValidateAccessShift
      End If

      Return False
    End Function
#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessShift() method.

    End Sub

    Public Shared Function NewAccessShift() As AccessShift

      Return DataPortal.CreateChild(Of AccessShift)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessShift(dr As SafeDataReader, AccessGroupID As Integer, Optional MustValidateAccessShift As Boolean = True) As AccessShift

      Dim a As New AccessShift()
      a.Fetch(dr)
      a.AccessGroupID = AccessGroupID
      a.MustValidateAccessShift = MustValidateAccessShift
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessShiftIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(StartDateTimeProperty, .GetValue(2))
          LoadProperty(EndDateTimeProperty, .GetValue(3))
          LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(AccessFlagIDProperty, .GetInt32(5))
          LoadProperty(FlaggedDateTimeProperty, .GetValue(6))
          LoadProperty(ProductionIDProperty, .GetInt32(7))
          LoadProperty(PlannedShiftOptionByProperty, .GetInt32(8))
          LoadProperty(PlannedShiftOptionDateTimeProperty, .GetValue(9))
          LoadProperty(HumanResourceProperty, .GetString(10))
          LoadProperty(TotalShiftHoursProperty, .GetInt32(11))
          LoadProperty(SkillsCountProperty, .GetInt32(12))
          LoadProperty(DisciplineIDProperty, .GetInt32(13))
          LoadProperty(ShiftDateProperty, .GetValue(14))
          LoadProperty(PlannedTimeChangedReasonProperty, .GetString(15))
          LoadProperty(FlagDecriptionProperty, .GetString(16))
          LoadProperty(MustValidateAccessShiftProperty, .GetBoolean(17))
          LoadProperty(BiometricsMissingShiftDateProperty, .GetValue(18))

          LoadProperty(ShiftStartDateTimeProperty, StartDateTime)
          LoadProperty(ShiftEndDateTimeProperty, EndDateTime)

          If AccessFlagID = CommonData.Enums.AccessFlag.Biometrics_Missing Then
            ShiftDate = BiometricsMissingShiftDate
          End If

          If Singular.Misc.IsNullNothing(HumanResourceShiftID, True) AndAlso Singular.Misc.IsNullNothing(ProductionID, True) AndAlso Not Singular.Misc.IsNullNothing(PlannedShiftOptionBy, True) Then
            LoadProperty(AccessShiftOptionIndProperty, False)
          End If
        End With
      End Using

      MarkAsChild()
      MarkOld()
      '    BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAccessShift"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAccessShift"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessShiftID As SqlParameter = .Parameters.Add("@AccessShiftID", SqlDbType.Int)
          paramAccessShiftID.Value = GetProperty(AccessShiftIDProperty)
          If Me.IsNew Then
            paramAccessShiftID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.ZeroNothingDBNull(GetProperty(HumanResourceShiftIDProperty)))
          .Parameters.AddWithValue("@AccessFlagID", GetProperty(AccessFlagIDProperty))
          .Parameters.AddWithValue("@FlaggedDateTime", (New SmartDate(GetProperty(FlaggedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ProductionID", Singular.Misc.ZeroNothingDBNull(GetProperty(ProductionIDProperty)))
          .Parameters.AddWithValue("@PlannedShiftOptionBy", Singular.Misc.ZeroNothingDBNull(GetProperty(PlannedShiftOptionByProperty)))
          .Parameters.AddWithValue("@PlannedShiftOptionDateTime", (New SmartDate(GetProperty(PlannedShiftOptionDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUser.SystemID))
          .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUser.ProductionAreaID))
          .Parameters.AddWithValue("@PlannedTimeChangedReason", GetProperty(PlannedTimeChangedReasonProperty))
          .Parameters.AddWithValue("@BiometricsMissingShiftDate", (New SmartDate(GetProperty(BiometricsMissingShiftDateProperty))).DBValue)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessShiftIDProperty, paramAccessShiftID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      '' accessshifts must never be deleted from the front end
      ''If Me.IsNew Then Exit Sub

      ''Using cm As SqlCommand = New SqlCommand
      ''  cm.CommandText = "DelProcsWeb.delAccessShift"
      ''  cm.CommandType = CommandType.StoredProcedure
      ''  cm.Parameters.AddWithValue("@AccessShiftID", GetProperty(AccessShiftIDProperty))
      ''  DoDeleteChild(cm)
      ''End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

    <Serializable()> _
    Public Class FlagValues
      Property AccessFlagID As Integer = 0
      Property FlagDescription As String = 0
      Property SyncID As Integer
    End Class

    Public Function GetShiftFlagValues(StartDate As DateTime?, EndDate As DateTime?, AccessGroupID As Integer) As FlagValues
      Dim mResults As New FlagValues

      If AccessShiftID <> 0 Then
        Using cn As New SqlConnection(Singular.Settings.ConnectionString)
          cn.Open()
          Try
            Using cm As SqlCommand = cn.CreateCommand
              cm.CommandType = CommandType.StoredProcedure
              cm.CommandText = "GetProcsWeb.GetAccessShiftFlagValues"
              cm.Parameters.AddWithValue("@AccessShiftID", AccessShiftID)
              cm.Parameters.AddWithValue("@StartDateTime", Singular.Misc.NothingDBNull(StartDate))
              cm.Parameters.AddWithValue("@EndDateTime", Singular.Misc.NothingDBNull(EndDate))
              cm.Parameters.AddWithValue("@AccessGroupID", AccessGroupID)
              Using sdr As New SafeDataReader(cm.ExecuteReader)
                If sdr.Read Then
                  mResults.AccessFlagID = sdr.GetInt32(0)
                  mResults.FlagDescription = sdr.GetString(1)
                  LoadProperty(AccessFlagIDProperty, mResults.AccessFlagID)
                  LoadProperty(FlagDecriptionProperty, mResults.FlagDescription)
                End If
              End Using
            End Using
          Finally
            cn.Close()

          End Try

        End Using
      Else
        mResults.AccessFlagID = AccessFlagID
        mResults.FlagDescription = FlagDecription
      End If
      Return mResults
    End Function

#End If

#End Region

#End Region

  End Class

End Namespace