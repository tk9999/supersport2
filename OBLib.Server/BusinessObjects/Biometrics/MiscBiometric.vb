﻿' Generated 31 Mar 2015 09:11 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class MiscBiometric
    Inherits OBBusinessBase(Of MiscBiometric)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared MiscBiometricsIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MiscBiometricsID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property MiscBiometricsID() As Integer
      Get
        Return GetProperty(MiscBiometricsIDProperty)
      End Get
    End Property 

    Public Shared CentralSecurityLastLogTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CentralSecuritylastLogTime, "last Central Security Log")
    ''' <summary>
    ''' Gets and sets the Anviz Read Date value
    ''' </summary>
    <Display(Name:="Central Security last Log time", Description:="the last log the system read from the Central Security database")>
    Public Property CentralSecuritylastLogTime() As DateTime
      Get
        Return GetProperty(CentralSecurityLastLogTimeProperty)
      End Get
      Set(ByVal Value As DateTime)
        SetProperty(CentralSecurityLastLogTimeProperty, Value)
      End Set
    End Property

    Public Shared AnvizLastLogIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AnvizLastLogID, "Anviz last log")
    ''' <summary>
    ''' Gets and sets the Anviz Read Date value
    ''' </summary>
    <Display(Name:="Anviz Read Log", Description:="the log the system read from the anviz database")>
    Public Property AnvizLastLogID As Integer
      Get
        Return GetProperty(AnvizLastLogIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AnvizLastLogIDProperty, Value)
      End Set
    End Property

    Public Shared AnvizAccessPathProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AnvizAccessPath, "Anviz Access Path", "")
    ''' <summary>
    ''' Gets and sets the Anviz Access Path value
    ''' </summary>
    <Display(Name:="Anviz Access Path", Description:="location of the Anviz access Database"),
    StringLength(300, ErrorMessage:="Anviz Access Path cannot be more than 500 characters")>
    Public Property AnvizAccessPath() As String
      Get
        Return GetProperty(AnvizAccessPathProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AnvizAccessPathProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(MiscBiometricsIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AnvizAccessPath.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Misc Biometric")
        Else
          Return String.Format("Blank {0}", "Misc Biometric")
        End If
      Else
        Return Me.AnvizAccessPath
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewMiscBiometric() method.

    End Sub

    Public Shared Function NewMiscBiometric() As MiscBiometric

      Return DataPortal.CreateChild(Of MiscBiometric)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetMiscBiometric(dr As SafeDataReader) As MiscBiometric

      Dim m As New MiscBiometric()
      m.Fetch(dr)
      Return m

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(MiscBiometricsIDProperty, .GetInt32(0))
          LoadProperty(AnvizAccessPathProperty, .GetString(1))
          LoadProperty(AnvizLastLogIDProperty, .GetInt32(2)) 
          LoadProperty(CentralSecurityLastLogTimeProperty, .GetValue(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insMiscBiometric"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updMiscBiometric"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramMiscBiometricsID As SqlParameter = .Parameters.Add("@MiscBiometricsID", SqlDbType.Int)
          paramMiscBiometricsID.Value = GetProperty(MiscBiometricsIDProperty)
          If Me.IsNew Then
            paramMiscBiometricsID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AnvizAccessPath", GetProperty(AnvizAccessPathProperty))
          .Parameters.AddWithValue("@AnvizLastLogID", GetProperty(AnvizLastLogIDProperty)) 
          .Parameters.AddWithValue("@CentralSecurityLogTime", GetProperty(CentralSecurityLastLogTimeProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(MiscBiometricsIDProperty, paramMiscBiometricsID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delMiscBiometric"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@MiscBiometricsID", GetProperty(MiscBiometricsIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace