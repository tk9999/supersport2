﻿' Generated 31 Mar 2015 09:11 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Biometrics

  <Serializable()> _
  Public Class MiscBiometricList
    Inherits OBBusinessListBase(Of MiscBiometricList, MiscBiometric)

#Region " Business Methods "

    Public Function GetItem(MiscBiometricsID As Integer) As MiscBiometric

      For Each child As MiscBiometric In Me
        If child.MiscBiometricsID = MiscBiometricsID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Misc Biometrics"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewMiscBiometricList() As MiscBiometricList

      Return New MiscBiometricList()

    End Function

    Public Shared Sub BeginGetMiscBiometricList(CallBack As EventHandler(Of DataPortalResult(Of MiscBiometricList)))

      Dim dp As New DataPortal(Of MiscBiometricList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetMiscBiometricList() As MiscBiometricList

      Return DataPortal.Fetch(Of MiscBiometricList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(MiscBiometric.GetMiscBiometric(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Public Shared Sub PostUpdate(ImportTypeID As CommonData.Enums.BiometricsImportType)
      Try
        Dim cmdProc As New Singular.CommandProc("CmdProcs.[cmdPostUpdateBiometricsImport]")
        cmdProc.FetchType = Singular.CommandProc.FetchTypes.DataObject
        cmdProc.Parameters.AddWithValue("@ImportTypeID", ImportTypeID)
        cmdProc = cmdProc.Execute
      Catch ex As Exception
        Throw New Exception(ex.Message)
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getMiscBiometricList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As MiscBiometric In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As MiscBiometric In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace