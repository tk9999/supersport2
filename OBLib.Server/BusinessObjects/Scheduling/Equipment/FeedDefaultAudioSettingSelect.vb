﻿' Generated 21 Feb 2016 13:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace SatOps

  <Serializable()> _
  Public Class FeedDefaultAudioSettingSelect
    Inherits OBBusinessBase(Of FeedDefaultAudioSettingSelect)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AudioSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AudioSettingID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True), Key,
    Required(ErrorMessage:="ID required")>
    Public Property AudioSettingID() As Integer
      Get
        Return GetProperty(AudioSettingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AudioSettingIDProperty, Value)
      End Set
    End Property

    Public Shared AudioSettingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AudioSetting, "Audio Setting")
    ''' <summary>
    ''' Gets and sets the Audio Setting value
    ''' </summary>
    <Display(Name:="Audio Setting")>
    Public Property AudioSetting() As String
      Get
        Return GetProperty(AudioSettingProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AudioSettingProperty, Value)
      End Set
    End Property

    Public Shared AudioSettingOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AudioSettingOrder, "Audio Setting Order")
    ''' <summary>
    ''' Gets and sets the Audio Setting Order value
    ''' </summary>
    <Display(Name:="Audio Setting Order")>
    Public Property AudioSettingOrder() As Integer
      Get
        Return GetProperty(AudioSettingOrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AudioSettingOrderProperty, Value)
      End Set
    End Property

    Public Shared FeedDefaultAudioSettingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedDefaultAudioSettingID, "Feed Default Audio Setting")
    ''' <summary>
    ''' Gets and sets the Feed Default Audio Setting value
    ''' </summary>
    <Display(Name:="Feed Default Audio Setting", Description:="")>
    Public Property FeedDefaultAudioSettingID() As Integer?
      Get
        Return GetProperty(FeedDefaultAudioSettingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedDefaultAudioSettingIDProperty, Value)
      End Set
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedID, "Feed")
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Feed", Description:="")>
    Public Property FeedID() As Integer?
      Get
        Return GetProperty(FeedIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedIDProperty, Value)
      End Set
    End Property

    Public Shared GroupNumberProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.GroupNumber, "Group Number")
    ''' <summary>
    ''' Gets and sets the Group Number value
    ''' </summary>
    <Display(Name:="Group Number", Description:=""),
    Required(ErrorMessage:="Group Number required")>
    Public Property GroupNumber() As Integer?
      Get
        Return GetProperty(GroupNumberProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(GroupNumberProperty, Value)
      End Set
    End Property

    Public Shared PairNumberProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PairNumber, "Pair Number")
    ''' <summary>
    ''' Gets and sets the Pair Number value
    ''' </summary>
    <Display(Name:="Pair Number", Description:="")>
    Public Property PairNumber() As Integer?
      Get
        Return GetProperty(PairNumberProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PairNumberProperty, Value)
      End Set
    End Property

    Public Shared ChannelNumberProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ChannelNumber, "Channel Number")
    ''' <summary>
    ''' Gets and sets the Channel Number value
    ''' </summary>
    <Display(Name:="Channel Number", Description:="")>
    Public Property ChannelNumber() As Integer?
      Get
        Return GetProperty(ChannelNumberProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ChannelNumberProperty, Value)
      End Set
    End Property

    Public Shared IsSelectedClientProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(d) d.IsSelectedClient, False)
    ''' <summary>
    ''' Gets the IsProcessing value
    ''' </summary>
    ''' 
    Public Property IsSelectedClient() As Boolean
      Get
        Return GetProperty(IsSelectedClientProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsSelectedClientProperty, value)
      End Set
    End Property

    Public Shared IsSelectedOriginalProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(d) d.IsSelectedOriginal, False)
    ''' <summary>
    ''' Gets the IsProcessing value
    ''' </summary>
    ''' 
    Public Property IsSelectedOriginal() As Boolean
      Get
        Return GetProperty(IsSelectedOriginalProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsSelectedOriginalProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AudioSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AudioSetting.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feed Default Audio Setting Select")
        Else
          Return String.Format("Blank {0}", "Feed Default Audio Setting Select")
        End If
      Else
        Return Me.AudioSetting
      End If

    End Function

    Public Function GetParent() As EquipmentFeed

      Return CType(CType(Me.Parent, FeedDefaultAudioSettingSelectList).Parent, EquipmentFeed)

    End Function

    Shared Sub New()

      CType(IsSelectedProperty, Singular.SPropertyInfo(Of Boolean, FeedDefaultAudioSettingSelect)).AddSetExpression("FeedDefaultAudioSettingSelectBO.IsSelectedSet(self)")

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedDefaultAudioSettingSelect() method.

    End Sub

    Public Shared Function NewFeedDefaultAudioSettingSelect() As FeedDefaultAudioSettingSelect

      Return DataPortal.CreateChild(Of FeedDefaultAudioSettingSelect)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFeedDefaultAudioSettingSelect(dr As SafeDataReader) As FeedDefaultAudioSettingSelect

      Dim f As New FeedDefaultAudioSettingSelect()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AudioSettingIDProperty, .GetInt32(0))
          LoadProperty(AudioSettingProperty, .GetString(1))
          LoadProperty(AudioSettingOrderProperty, .GetInt32(2))
          LoadProperty(FeedDefaultAudioSettingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(GroupNumberProperty, .GetInt32(5))
          LoadProperty(PairNumberProperty, .GetInt32(6))
          LoadProperty(ChannelNumberProperty, .GetInt32(7))
          LoadProperty(IsSelectedProperty, .GetBoolean(8))
          LoadProperty(IsSelectedClientProperty, .GetBoolean(8))
          LoadProperty(IsSelectedOriginalProperty, .GetBoolean(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFeedDefaultAudioSettingSelect"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFeedDefaultAudioSettingSelect"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramFeedDefaultAudioSettingID As SqlParameter = .Parameters.Add("@FeedDefaultAudioSettingID", SqlDbType.Int)
          paramFeedDefaultAudioSettingID.Value = NothingDBNull(GetProperty(FeedDefaultAudioSettingIDProperty))
          paramFeedDefaultAudioSettingID.Direction = ParameterDirection.InputOutput
          .Parameters.AddWithValue("@AudioSettingID", GetProperty(AudioSettingIDProperty))
          '.Parameters.AddWithValue("@AudioSetting", GetProperty(AudioSettingProperty))
          '.Parameters.AddWithValue("@AudioSettingOrder", GetProperty(AudioSettingOrderProperty))
          .Parameters.AddWithValue("@FeedID", GetProperty(FeedIDProperty))
          '.Parameters.AddWithValue("@GroupNumber", GetProperty(GroupNumberProperty))
          '.Parameters.AddWithValue("@PairNumber", GetProperty(PairNumberProperty))
          '.Parameters.AddWithValue("@ChannelNumber", GetProperty(ChannelNumberProperty))
          .Parameters.AddWithValue("@IsSelected", GetProperty(IsSelectedProperty))
          .Parameters.AddWithValue("@IsSelectedOriginal", GetProperty(IsSelectedOriginalProperty))

          .ExecuteNonQuery()

          If Singular.Misc.IsNullNothing(paramFeedDefaultAudioSettingID.Value, False) Then
            SetProperty(FeedDefaultAudioSettingIDProperty, Nothing)
            SetProperty(IsSelectedOriginalProperty, False)
          Else
            SetProperty(FeedDefaultAudioSettingIDProperty, paramFeedDefaultAudioSettingID.Value)
            SetProperty(IsSelectedOriginalProperty, True)
          End If

          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delFeedDefaultAudioSettingSelect"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@AudioSettingID", GetProperty(AudioSettingIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      'cm.ExecuteNonQuery()
      'MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace