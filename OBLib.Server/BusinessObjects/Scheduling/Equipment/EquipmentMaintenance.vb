﻿' Generated 26 Mar 2015 18:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.Productions.Crew.ReadOnly
Imports OBLib.Vehicles.ReadOnly
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Productions.Base
Imports OBLib.SatOps.ReadOnly
Imports OBLib.Equipment.ReadOnly

Namespace SatOps

  <Serializable()> _
  Public Class EquipmentMaintenance
    Inherits OBBusinessBase(Of EquipmentMaintenance)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return EquipmentMaintenanceBO.toString(self)")

    Public Shared EquipmentScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentScheduleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property EquipmentScheduleID() As Integer
      Get
        Return GetProperty(EquipmentScheduleIDProperty)
      End Get
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EquipmentID, Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:=""),
    Required(ErrorMessage:="Equipment is required"),
    DropDownWeb(GetType(ROCircuitAvailabilityList),
                BeforeFetchJS:="EquipmentMaintenanceBO.setCircuitCriteriaBeforeRefresh",
                PreFindJSFunction:="EquipmentMaintenanceBO.triggerCircuitAutoPopulate",
                AfterFetchJS:="EquipmentMaintenanceBO.afterCircuitRefreshAjax",
                OnItemSelectJSFunction:="EquipmentMaintenanceBO.onCircuitSelected",
                LookupMember:="EquipmentName", DisplayMember:="EquipmentName", ValueMember:="EquipmentID",
                DropDownCssClass:="circuit-dropdown", DropDownColumns:={"EquipmentName", "IsAvailableString"}),
    SetExpression("EquipmentMaintenanceBO.EquipmentIDSet(self)", False)>
    Public Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingID, "ID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property ResourceBookingID() As Integer?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ResourceBookingIDProperty, value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer)
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Buffer"),
    SetExpression("EquipmentMaintenanceBO.StartDateTimeBufferSet(self)", False)>
    Public Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time is required"),
    SetExpression("EquipmentMaintenanceBO.StartDateTimeSet(self)", False)>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time is required"),
    SetExpression("EquipmentMaintenanceBO.EndDateTimeSet(self)", False)>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer)
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Buffer"),
    SetExpression("EquipmentMaintenanceBO.EndDateTimeBufferSet(self)", False)>
    Public Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared EquipmentNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentName, "Equipment", "")
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Equipment")>
    Public Property EquipmentName() As String
      Get
        Return GetProperty(EquipmentNameProperty)
      End Get
      Set(value As String)
        SetProperty(EquipmentNameProperty, value)
      End Set
    End Property
    'Required(ErrorMessage:="Equipment is required", AllowEmptyStrings:=False)

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Client", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' 
    ''' </summary>
    <Display(Name:="Client", Description:=""),
    DropDownWeb(GetType(RODebtorFindList),
                BeforeFetchJS:="EquipmentMaintenanceBO.setDebtorCriteriaBeforeRefresh",
                PreFindJSFunction:="EquipmentMaintenanceBO.triggerDebtorAutoPopulate",
                AfterFetchJS:="EquipmentMaintenanceBO.afterDebtorRefreshAjax",
                OnItemSelectJSFunction:="EquipmentMaintenanceBO.onDebtorSelected",
                LookupMember:="DebtorName", DisplayMember:="Debtor", ValueMember:="DebtorID",
                DropDownCssClass:="debtor-dropdown", DropDownColumns:={"Debtor", "ContactName", "ContactNumber"}),
    SetExpression("EquipmentMaintenanceBO.DebtorIDSet(self)")>
    Public Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DebtorIDProperty, Value)
      End Set
    End Property

    Public Shared DebtorNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DebtorName, "Client", "")
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Client", Description:="")>
    Public Property DebtorName() As String
      Get
        Return GetProperty(DebtorNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DebtorNameProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Title, "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Title", Description:=""),
    Required(ErrorMessage:="Title is required", AllowEmptyStrings:=False),
    SetExpression("EquipmentMaintenanceBO.TitleSet(self)", False)>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared EquipmentScheduleIDCopiedFromProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EquipmentScheduleIDCopiedFrom, Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment")>
    Public Property EquipmentScheduleIDCopiedFrom() As Integer?
      Get
        Return GetProperty(EquipmentScheduleIDCopiedFromProperty)
      End Get
      Set(value As Integer?)
        SetProperty(EquipmentScheduleIDCopiedFromProperty, value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, CInt(OBLib.CommonData.Enums.ProductionAreaStatus.New))
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Status", Description:=""),
    Required(ErrorMessage:="Status is required"),
     DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaStatusSelectList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="EquipmentMaintenanceBO.setStatusCriteriaBeforeRefresh",
                 PreFindJSFunction:="EquipmentMaintenanceBO.triggerStatusAutoPopulate",
                 AfterFetchJS:="EquipmentMaintenanceBO.afterStatusRefreshAjax",
                 LookupMember:="ProductionAreaStatusName", ValueMember:="ProductionAreaStatusID", DropDownColumns:={"ProductionAreaStatus"},
                 OnItemSelectJSFunction:="EquipmentMaintenanceBO.onStatusSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("EquipmentMaintenanceBO.ProductionAreaStatusIDSet(self)", False)>
    Public Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatusName, "Status", "New")
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Status", Description:=""),
    Required(ErrorMessage:="Status is required", AllowEmptyStrings:=False)>
    Public Property ProductionAreaStatusName() As String
      Get
        Return GetProperty(ProductionAreaStatusNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaStatusNameProperty, Value)
      End Set
    End Property

    Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "StatusCssClass", "new")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="StatusCssClass", Description:=""),
   Required(ErrorMessage:="Status Css Class required", AllowEmptyStrings:=False)>
    Public Property StatusCssClass() As String
      Get
        Return GetProperty(StatusCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCssClassProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDOwnerProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaIDOwner, "ProductionSystemAreaIDOwner", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="ProductionSystemAreaIDOwner", Description:="")>
    Public Property ProductionSystemAreaIDOwner() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDOwnerProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDOwnerProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "ProductionSystemAreaID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Booking Type", Description:="")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "SystemID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept is required"),
    DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area is required"),
    DropDownWeb(GetType(ROUserSystemAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDOriginalProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaStatusIDOriginal, Nothing)
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatusIDOriginal() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDOriginalProperty)
      End Get
      'Set(ByVal Value As Integer?)
      '  SetProperty(ProductionAreaStatusIDOriginalProperty, Value)
      'End Set
    End Property

    Public Shared ChangeDescriptionRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ChangeDescriptionRequired, "ChangeDescription", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="ChangeDescription", Description:=""), AlwaysClean>
    Public Property ChangeDescriptionRequired() As Boolean
      Get
        Return GetProperty(ChangeDescriptionRequiredProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ChangeDescriptionRequiredProperty, Value)
      End Set
    End Property

    Public Shared ChangeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangeDescription, "Change Reason", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Change Reason", Description:=""), AlwaysClean>
    Public Property ChangeDescription() As String
      Get
        Return GetProperty(ChangeDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChangeDescriptionProperty, Value)
      End Set
    End Property

    Public Shared IsOwnerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsOwner, "IsOwner", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="IsOwner"), AlwaysClean>
    Public Property IsOwner() As Boolean
      Get
        Return GetProperty(IsOwnerProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsOwnerProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Overrides Function ToString() As String

      Return "Production Feed"

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentScheduleIDProperty)

    End Function

    Sub SendChangeNotification()

      'Dim SponsorshipService As SponsorshipWebService.SSWSDSVCClient
      'Try
      '  SponsorshipService = New SponsorshipWebService.SSWSDSVCClient
      '  SponsorshipService.SatOpsUpdateAlert("94208fee-5615-11e5-a800-005056943beb", "SSWSD_Sober", Me.FeedID.ToString, Me.StartDateTimeBuffer.Value.ToString("dd MMM yyyy"), ChangeDescription)
      'Catch ex As Exception
      '  OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "EquipmentSchedule.vb", "SendChangeNotification", ex.Message)
      'End Try

    End Sub

    Public Sub SetOriginalStatus()
      LoadProperty(ProductionAreaStatusIDOriginalProperty, ProductionAreaStatusID)
    End Sub

    Public Sub SetIsOwner()
      If IsNew Then
        LoadProperty(IsOwnerProperty, True)
      Else
        LoadProperty(IsOwnerProperty, CompareSafe(ProductionSystemAreaIDOwner, ProductionSystemAreaID))
      End If
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(StartDateTimeBufferProperty)
        .JavascriptRuleFunctionName = "EquipmentScheduleBaseBO.BufferStartTimeValid"
        .ServerRuleFunction = AddressOf CallTimeValid
        .AddTriggerProperty(EndDateTimeProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(StartDateTimeProperty)
        .JavascriptRuleFunctionName = "EquipmentScheduleBaseBO.StartDateTimeValid"
        .ServerRuleFunction = AddressOf StartDateTimeValid
        .AddTriggerProperty(EndDateTimeProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(EndDateTimeProperty)
        .JavascriptRuleFunctionName = "EquipmentScheduleBaseBO.EndDateTimeValid"
        .ServerRuleFunction = AddressOf EndDateTimeValid
        .AddTriggerProperty(StartDateTimeProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(EndDateTimeBufferProperty)
        .JavascriptRuleFunctionName = "EquipmentScheduleBaseBO.BufferEndTimeValid"
        .ServerRuleFunction = AddressOf WrapTimeValid
        .AddTriggerProperty(StartDateTimeProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(ChangeDescriptionProperty)
        .JavascriptRuleFunctionName = "EquipmentMaintenanceBO.ChangeDescriptionValid"
        .ServerRuleFunction = AddressOf ChangeDescriptionValid
        .AddTriggerProperty(ChangeDescriptionRequiredProperty)
      End With

    End Sub

    Public Shared Function CallTimeValid(EquipmentMaintenance As EquipmentMaintenance) As String
      Dim Err As String = ""
      If EquipmentMaintenance.StartDateTimeBuffer Is Nothing Then
        Err &= "Buffer Start Time is required"
      ElseIf EquipmentMaintenance.StartDateTimeBuffer IsNot Nothing And EquipmentMaintenance.StartDateTime IsNot Nothing Then
        If EquipmentMaintenance.StartDateTimeBuffer.Value > EquipmentMaintenance.StartDateTime.Value Then
          Err &= "Buffer Start Time must be before Start Time"
        End If
      End If
      Return Err
    End Function

    Public Shared Function StartDateTimeValid(EquipmentMaintenance As EquipmentMaintenance) As String
      Dim Err As String = ""
      If EquipmentMaintenance.StartDateTime Is Nothing Then
        Err &= "Start Time is required"
      ElseIf EquipmentMaintenance.StartDateTime IsNot Nothing And EquipmentMaintenance.EndDateTime IsNot Nothing Then
        If EquipmentMaintenance.StartDateTime.Value > EquipmentMaintenance.EndDateTime.Value Then
          Err &= "Start Time must be before End Time"
        End If
      End If
      Return Err
    End Function

    Public Shared Function EndDateTimeValid(EquipmentMaintenance As EquipmentMaintenance) As String
      Dim Err As String = ""
      If EquipmentMaintenance.EndDateTime Is Nothing Then
        Err &= "End Time is required"
      ElseIf EquipmentMaintenance.StartDateTime IsNot Nothing And EquipmentMaintenance.EndDateTime IsNot Nothing Then
        If EquipmentMaintenance.StartDateTime.Value > EquipmentMaintenance.EndDateTime.Value Then
          Err &= "End Time must be after Start Time"
        End If
      End If
      Return Err
    End Function

    Public Shared Function WrapTimeValid(EquipmentMaintenance As EquipmentMaintenance) As String
      Dim Err As String = ""
      If EquipmentMaintenance.EndDateTime Is Nothing Then
        Err &= "Buffer End Time is required"
      ElseIf EquipmentMaintenance.EndDateTimeBuffer IsNot Nothing And EquipmentMaintenance.EndDateTime IsNot Nothing Then
        If EquipmentMaintenance.EndDateTimeBuffer.Value < EquipmentMaintenance.EndDateTime.Value Then
          Err &= "Buffer End Time must be after End Time"
        End If
      End If
      Return Err
    End Function

    Public Shared Function ChangeDescriptionValid(EquipmentMaintenance As EquipmentMaintenance) As String
      Dim ErrorMsg As String = ""
      If EquipmentMaintenance.ChangeDescriptionRequired Then
        ErrorMsg = "Change Reason is required"
      End If
      Return ErrorMsg
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipmentMaintenance() method.

    End Sub

    Public Shared Function NewEquipmentMaintenance() As EquipmentMaintenance

      Return DataPortal.CreateChild(Of EquipmentMaintenance)()

    End Function

    Public Shared Function GetEquipmentMaintenance(dr As SafeDataReader) As EquipmentMaintenance

      Dim f As New EquipmentMaintenance()
      f.Fetch(dr)
      Return f

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EquipmentScheduleIDProperty, .GetInt32(0))
          LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceBookingIDProperty, ZeroNothing(.GetInt32(2)))
          LoadProperty(ResourceIDProperty, ZeroNothing(.GetInt32(3)))
          LoadProperty(StartDateTimeBufferProperty, .GetValue(4))
          LoadProperty(StartDateTimeProperty, .GetValue(5))
          LoadProperty(EndDateTimeProperty, .GetValue(6))
          LoadProperty(EndDateTimeBufferProperty, .GetValue(7))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
          LoadProperty(EquipmentNameProperty, .GetString(12))
          LoadProperty(CommentsProperty, .GetString(13))
          LoadProperty(DebtorIDProperty, ZeroNothing(.GetInt32(14)))
          LoadProperty(DebtorNameProperty, .GetString(15))
          LoadProperty(TitleProperty, .GetString(16))
          LoadProperty(EquipmentScheduleIDCopiedFromProperty, ZeroNothing(.GetInt32(17)))
          LoadProperty(ProductionAreaStatusIDProperty, ZeroNothing(.GetInt32(18)))
          LoadProperty(ProductionAreaStatusNameProperty, .GetString(19))
          LoadProperty(StatusCssClassProperty, .GetString(20))
          LoadProperty(ProductionSystemAreaIDOwnerProperty, ZeroNothing(.GetInt32(21)))
          LoadProperty(ProductionSystemAreaIDProperty, ZeroNothing(.GetInt32(22)))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(23)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(24)))
          LoadProperty(ProductionAreaStatusIDOriginalProperty, ProductionAreaStatusID)
        End With
      End Using
      SetIsOwner()

      'FetchExtraProperties(sdr)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEquipmentMaintenance"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEquipmentMaintenance"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure
          SetIsOwner()
          Dim paramEquipmentMaintenanceID As SqlParameter = .Parameters.Add("@EquipmentScheduleID", SqlDbType.Int)
          paramEquipmentMaintenanceID.Value = GetProperty(EquipmentScheduleIDProperty)
          Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
          Dim paramProductionSystemAreaID As SqlParameter = .Parameters.Add("@ProductionSystemAreaID", SqlDbType.Int)
          paramProductionSystemAreaID.Value = GetProperty(ProductionSystemAreaIDProperty)
          Dim paramProductionSystemAreaIDOwner As SqlParameter = .Parameters.Add("@ProductionSystemAreaIDOwner", SqlDbType.Int)
          paramProductionSystemAreaIDOwner.Value = GetProperty(ProductionSystemAreaIDOwnerProperty)
          If Me.IsNew Then
            paramEquipmentMaintenanceID.Direction = ParameterDirection.Output
            paramResourceBookingID.Direction = ParameterDirection.Output
            paramProductionSystemAreaID.Direction = ParameterDirection.Output
            paramProductionSystemAreaIDOwner.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EquipmentID", GetProperty(EquipmentIDProperty))
          .Parameters.AddWithValue("@ResourceID", NothingDBNull(GetProperty(ResourceIDProperty)))
          .Parameters.AddWithValue("@StartDateTimeBuffer", (New SmartDate(GetProperty(StartDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTimeBuffer", (New SmartDate(GetProperty(EndDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@EquipmentName", GetProperty(EquipmentNameProperty))
          .Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
          .Parameters.AddWithValue("@DebtorID", NothingDBNull(GetProperty(DebtorIDProperty)))
          .Parameters.AddWithValue("@DebtorName", NothingDBNull(GetProperty(DebtorNameProperty)))
          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@EquipmentScheduleIDCopiedFrom", NothingDBNull(GetProperty(EquipmentScheduleIDCopiedFromProperty)))
          .Parameters.AddWithValue("@ProductionAreaStatusID", NothingDBNull(GetProperty(ProductionAreaStatusIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaStatusName", NothingDBNull(GetProperty(ProductionAreaStatusNameProperty)))
          .Parameters.AddWithValue("@StatusCssClass", GetProperty(StatusCssClassProperty))
          .Parameters.AddWithValue("@SystemID", NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EquipmentScheduleIDProperty, paramEquipmentMaintenanceID.Value)
            LoadProperty(ResourceBookingIDProperty, paramResourceBookingID.Value)
            LoadProperty(ProductionSystemAreaIDProperty, paramProductionSystemAreaID.Value)
            LoadProperty(ProductionSystemAreaIDOwnerProperty, paramProductionSystemAreaIDOwner.Value)
          End If
          MarkOld()
        End With
      Else
        ' update child objects
      End If
      SaveChildren()

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEquipmentMaintenance"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EquipmentScheduleID", GetProperty(EquipmentScheduleIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace