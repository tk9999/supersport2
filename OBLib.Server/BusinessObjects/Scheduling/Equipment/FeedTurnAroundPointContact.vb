﻿' Generated 08 Jun 2016 22:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.SatOps.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace SatOps

  <Serializable()> _
  Public Class FeedTurnAroundPointContact
    Inherits OBBusinessBase(Of FeedTurnAroundPointContact)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return FeedTurnAroundPointContactBO.FeedTurnAroundPointContactBOToString(self)")

    Public Shared FeedTurnAroundPointContactIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedTurnAroundPointContactID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedTurnAroundPointContactID() As Integer
      Get
        Return GetProperty(FeedTurnAroundPointContactIDProperty)
      End Get
    End Property

    Public Shared FeedTurnAroundPointIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedTurnAroundPointID, "Feed Turn Around Point", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed Turn Around Point value
    ''' </summary>
    <Display(Name:="Feed Turn Around Point", Description:="")>
    Public Property FeedTurnAroundPointID() As Integer?
      Get
        Return GetProperty(FeedTurnAroundPointIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedTurnAroundPointIDProperty, Value)
      End Set
    End Property

    Public Shared TurnAroundPointContactIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TurnAroundPointContactID, "TurnAround Point Contact", Nothing)
    ''' <summary>
    ''' Gets and sets the Turn Around Point Contact value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="TurnAround Point Contact", Description:=""),
    DropDownWeb(GetType(ROTurnAroundPointContactList),
                BeforeFetchJS:="FeedTurnAroundPointContactBO.setTurnAroundPointContactCriteriaBeforeRefresh",
                PreFindJSFunction:="FeedTurnAroundPointContactBO.triggerTurnAroundPointContactAutoPopulate",
                AfterFetchJS:="FeedTurnAroundPointContactBO.afterTurnAroundPointContactRefreshAjax",
                OnItemSelectJSFunction:="FeedTurnAroundPointContactBO.onTurnAroundPointContactSelected",
                LookupMember:="ContactName", DisplayMember:="ContactName", ValueMember:="TurnAroundPointContactID",
                DropDownCssClass:="tap-contact-dropdown", DropDownColumns:={"ContactName", "ContactNumber"}),
    SetExpression("FeedTurnAroundPointContactBO.TurnAroundPointContactIDSet(self)")>
    Public Property TurnAroundPointContactID() As Integer?
      Get
        Return GetProperty(TurnAroundPointContactIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TurnAroundPointContactIDProperty, Value)
      End Set
    End Property

    Public Shared ContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactName, "Contact Name", "")
    ''' <summary>
    ''' Gets and sets the Contact Name value
    ''' </summary>
    <Display(Name:="Contact Name", Description:=""),
    StringLength(50, ErrorMessage:="Contact Name cannot be more than 50 characters"),
    Required(ErrorMessage:="A contact name is required")>
    Public Property ContactName() As String
      Get
        Return GetProperty(ContactNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContactNameProperty, Value)
      End Set
    End Property

    Public Shared ContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactNumber, "Contact Number", "")
    ''' <summary>
    ''' Gets and sets the Contact Number value
    ''' </summary>
    <Display(Name:="Contact Number", Description:=""),
    StringLength(20, ErrorMessage:="Contact Number cannot be more than 20 characters"),
    Required(ErrorMessage:="A contact number is required")>
    Public Property ContactNumber() As String
      Get
        Return GetProperty(ContactNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContactNumberProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedTurnAroundPointContactIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feed Turn Around Point Contact")
        Else
          Return String.Format("Blank {0}", "Feed Turn Around Point Contact")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Public Function GetParent() As FeedTurnAroundPoint

      Return CType(CType(Me.Parent, FeedTurnAroundPointContactList).Parent, FeedTurnAroundPoint)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedTurnAroundPointContact() method.

    End Sub

    Public Shared Function NewFeedTurnAroundPointContact() As FeedTurnAroundPointContact

      Return DataPortal.CreateChild(Of FeedTurnAroundPointContact)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetFeedTurnAroundPointContact(dr As SafeDataReader) As FeedTurnAroundPointContact

      Dim f As New FeedTurnAroundPointContact()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedTurnAroundPointContactIDProperty, .GetInt32(0))
          LoadProperty(FeedTurnAroundPointIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(TurnAroundPointContactIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ContactNameProperty, .GetString(3))
          LoadProperty(ContactNumberProperty, .GetString(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, FeedTurnAroundPointContactIDProperty)

      cm.Parameters.AddWithValue("@FeedTurnAroundPointID", Me.GetParent.FeedTurnAroundPointID)
      cm.Parameters.AddWithValue("@TurnAroundPointContactID", GetProperty(TurnAroundPointContactIDProperty))
      cm.Parameters.AddWithValue("@ContactName", GetProperty(ContactNameProperty))
      cm.Parameters.AddWithValue("@ContactNumber", GetProperty(ContactNumberProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(FeedTurnAroundPointContactIDProperty, cm.Parameters("@FeedTurnAroundPointContactID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@FeedTurnAroundPointContactID", GetProperty(FeedTurnAroundPointContactIDProperty))
    End Sub

#End Region

  End Class

End Namespace