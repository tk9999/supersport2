﻿' Generated 02 Feb 2016 15:51 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps

  <Serializable()> _
  Public Class FeedIngestInstruction
    Inherits OBBusinessBase(Of FeedIngestInstruction)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedIngestInstructionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedIngestInstructionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeedIngestInstructionID() As Integer
      Get
        Return GetProperty(FeedIngestInstructionIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedID, "Feed", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Feed")>
    Public Property FeedID() As Integer?
      Get
        Return GetProperty(FeedIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedIDProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title"),
    StringLength(150, ErrorMessage:="Title cannot be more than 150 characters")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared IngestInstructionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IngestInstruction, "Ingest Instruction", "")
    ''' <summary>
    ''' Gets and sets the Ingest Instruction value
    ''' </summary>
    <Display(Name:="Ingest Instruction", Description:=""),
    Required(AllowEmptyStrings:=False),
    StringLength(1024, ErrorMessage:="Ingest Instruction cannot be more than 1024 characters")>
    Public Property IngestInstruction() As String
      Get
        Return GetProperty(IngestInstructionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IngestInstructionProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedIngestInstructionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Title.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feed Ingest Instruction")
        Else
          Return String.Format("Blank {0}", "Feed Ingest Instruction")
        End If
      Else
        Return Me.Title
      End If

    End Function

    Public Function GetParent() As EquipmentFeed

      Return CType(CType(Me.Parent, FeedIngestInstructionList).Parent, EquipmentFeed)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedIngestInstruction() method.

    End Sub

    Public Shared Function NewFeedIngestInstruction() As FeedIngestInstruction

      Return DataPortal.CreateChild(Of FeedIngestInstruction)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFeedIngestInstruction(dr As SafeDataReader) As FeedIngestInstruction

      Dim f As New FeedIngestInstruction()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedIngestInstructionIDProperty, .GetInt32(0))
          LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(TitleProperty, .GetString(2))
          LoadProperty(IngestInstructionProperty, .GetString(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insFeedIngestInstruction"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFeedIngestInstruction"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramFeedIngestInstructionID As SqlParameter = .Parameters.Add("@FeedIngestInstructionID", SqlDbType.Int)
          paramFeedIngestInstructionID.Value = GetProperty(FeedIngestInstructionIDProperty)
          If Me.IsNew Then
            paramFeedIngestInstructionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@FeedID", Singular.Misc.NothingDBNull(Me.GetParent.FeedID))
          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@IngestInstruction", GetProperty(IngestInstructionProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(FeedIngestInstructionIDProperty, paramFeedIngestInstructionID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delFeedIngestInstruction"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@FeedIngestInstructionID", GetProperty(FeedIngestInstructionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace