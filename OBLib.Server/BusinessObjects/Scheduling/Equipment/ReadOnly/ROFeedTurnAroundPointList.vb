﻿' Generated 28 Mar 2015 12:29 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace SatOps.ReadOnly

  <Serializable()> _
  Public Class ROFeedTurnAroundPointList
    Inherits SingularReadOnlyListBase(Of ROFeedTurnAroundPointList, ROFeedTurnAroundPoint)

#Region " Business Methods "

    Public Function GetItem(FeedTurnAroundPointID As Integer) As ROFeedTurnAroundPoint

      For Each child As ROFeedTurnAroundPoint In Me
        If child.FeedTurnAroundPointID = FeedTurnAroundPointID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Feed Turn Around Points"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROFeedTurnAroundPointList() As ROFeedTurnAroundPointList

      Return New ROFeedTurnAroundPointList()

    End Function

    Public Shared Sub BeginGetROFeedTurnAroundPointList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROFeedTurnAroundPointList)))

      Dim dp As New DataPortal(Of ROFeedTurnAroundPointList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROFeedTurnAroundPointList(CallBack As EventHandler(Of DataPortalResult(Of ROFeedTurnAroundPointList)))

      Dim dp As New DataPortal(Of ROFeedTurnAroundPointList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROFeedTurnAroundPointList() As ROFeedTurnAroundPointList

      Return DataPortal.Fetch(Of ROFeedTurnAroundPointList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROFeedTurnAroundPoint.GetROFeedTurnAroundPoint(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROFeedTurnAroundPointList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace