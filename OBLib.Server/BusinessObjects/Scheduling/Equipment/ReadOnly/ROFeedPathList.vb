﻿' Generated 05 May 2015 17:43 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace SatOps.ReadOnly

  <Serializable()> _
  Public Class ROFeedPathList
    Inherits SingularReadOnlyListBase(Of ROFeedPathList, ROFeedPath)

#Region " Business Methods "

    Public Function GetItem(FeedPathID As Integer) As ROFeedPath

      For Each child As ROFeedPath In Me
        If child.FeedPathID = FeedPathID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Feed Paths"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROFeedPathList() As ROFeedPathList

      Return New ROFeedPathList()

    End Function

    Public Shared Sub BeginGetROFeedPathList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROFeedPathList)))

      Dim dp As New DataPortal(Of ROFeedPathList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROFeedPathList(CallBack As EventHandler(Of DataPortalResult(Of ROFeedPathList)))

      Dim dp As New DataPortal(Of ROFeedPathList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROFeedPathList() As ROFeedPathList

      Return DataPortal.Fetch(Of ROFeedPathList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROFeedPath.GetROFeedPath(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROFeedPathList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace