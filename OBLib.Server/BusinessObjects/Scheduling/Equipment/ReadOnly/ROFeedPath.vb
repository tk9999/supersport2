﻿' Generated 05 May 2015 17:43 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace SatOps.ReadOnly

  <Serializable()> _
  Public Class ROFeedPath
    Inherits SingularReadOnlyBase(Of ROFeedPath)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedPathIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedPathID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeedPathID() As Integer
      Get
        Return GetProperty(FeedPathIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedID, "Feed", Nothing)
    ''' <summary>
    ''' Gets the Feed value
    ''' </summary>
    <Display(Name:="Feed", Description:="")>
  Public ReadOnly Property FeedID() As Integer?
      Get
        Return GetProperty(FeedIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared PathCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PathComments, "Path Comments", "")
    ''' <summary>
    ''' Gets the Path Comments value
    ''' </summary>
    <Display(Name:="Path Comments", Description:="''")>
  Public ReadOnly Property PathComments() As String
      Get
        Return GetProperty(PathCommentsProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedPathIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.PathComments

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROFeedPath(dr As SafeDataReader) As ROFeedPath

      Dim r As New ROFeedPath()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedPathIDProperty, .GetInt32(0))
        LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(PathCommentsProperty, .GetString(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace