﻿' Generated 02 Jul 2015 13:15 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.ReadOnly

  <Serializable()> _
  Public Class ROEquipmentScheduleList
    Inherits OBReadOnlyListBase(Of ROEquipmentScheduleList, ROEquipmentSchedule)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#Region " Business Methods "

    Public Function GetItem(EquipmentScheduleID As Integer) As ROEquipmentSchedule

      For Each child As ROEquipmentSchedule In Me
        If child.EquipmentScheduleID = EquipmentScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Equipment Schedules"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property EquipmentIDs As String

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROEquipmentScheduleList() As ROEquipmentScheduleList

      Return New ROEquipmentScheduleList()

    End Function

    Public Shared Sub BeginGetROEquipmentScheduleList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentScheduleList)))

      Dim dp As New DataPortal(Of ROEquipmentScheduleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROEquipmentScheduleList(CallBack As EventHandler(Of DataPortalResult(Of ROEquipmentScheduleList)))

      Dim dp As New DataPortal(Of ROEquipmentScheduleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROEquipmentScheduleList() As ROEquipmentScheduleList

      Return DataPortal.Fetch(Of ROEquipmentScheduleList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEquipmentSchedule.GetROEquipmentSchedule(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      'Dim EquipmentIDs As String = OBLib.Helpers.MiscHelper.IntegerArrayToXML(crit.EquipmentIDs.ToArray)
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEquipmentScheduleList"
            cm.Parameters.AddWithValue("@EquipmentIDs", Singular.Strings.MakeEmptyDBNull(crit.EquipmentIDs))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace