﻿' Generated 28 Mar 2015 12:30 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace SatOps.ReadOnly

  <Serializable()> _
  Public Class ROFeedTurnAroundPoint
    Inherits SingularReadOnlyBase(Of ROFeedTurnAroundPoint)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedTurnAroundPointIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedTurnAroundPointID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property FeedTurnAroundPointID() As Integer
      Get
        Return GetProperty(FeedTurnAroundPointIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedID, "Feed", Nothing)
    ''' <summary>
    ''' Gets the Feed value
    ''' </summary>
    <Display(Name:="Feed", Description:="")>
  Public ReadOnly Property FeedID() As Integer?
      Get
        Return GetProperty(FeedIDProperty)
      End Get
    End Property

    Public Shared TurnAroundPointIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TurnAroundPointID, "Turn Around Point", Nothing)
    ''' <summary>
    ''' Gets the Turn Around Point value
    ''' </summary>
    <Display(Name:="Turn Around Point", Description:="")>
  Public ReadOnly Property TurnAroundPointID() As Integer?
      Get
        Return GetProperty(TurnAroundPointIDProperty)
      End Get
    End Property

    Public Shared TurnAroundPointProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TurnAroundPointOrder, "Order", 0)
    ''' <summary>
    ''' Gets and sets the Turn Around Point value
    ''' </summary>
    <Display(Name:="Order", Description:="")>
    Public ReadOnly Property TurnAroundPointOrder() As Integer
      Get
        Return GetProperty(TurnAroundPointProperty)
      End Get
    End Property

    Public Shared AudioConfigurationTemplateIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AudioConfigurationTemplateID, "Audio Configuration Template", Nothing)
    ''' <summary>
    ''' Gets the Audio Configuration Template value
    ''' </summary>
    <Display(Name:="Audio Configuration Template", Description:="")>
    Public ReadOnly Property AudioConfigurationTemplateID() As Integer?
      Get
        Return GetProperty(AudioConfigurationTemplateIDProperty)
      End Get
    End Property

    Public Shared VideoConfigurationTemplateIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VideoConfigurationTemplateID, "Video Configuration Template", Nothing)
    ''' <summary>
    ''' Gets the Video Configuration Template value
    ''' </summary>
    <Display(Name:="Video Configuration Template", Description:="")>
    Public ReadOnly Property VideoConfigurationTemplateID() As Integer?
      Get
        Return GetProperty(VideoConfigurationTemplateIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedTurnAroundPointIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROFeedTurnAroundPoint(dr As SafeDataReader) As ROFeedTurnAroundPoint

      Dim r As New ROFeedTurnAroundPoint()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FeedTurnAroundPointIDProperty, .GetInt32(0))
        LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(TurnAroundPointIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(AudioConfigurationTemplateIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(VideoConfigurationTemplateIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(TurnAroundPointProperty, .GetInt32(9))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace