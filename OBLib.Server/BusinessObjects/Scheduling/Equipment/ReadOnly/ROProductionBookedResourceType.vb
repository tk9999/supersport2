﻿' Generated 09 May 2015 11:50 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace SatOps.ReadOnly

  <Serializable()> _
  Public Class ROProductionBookedResourceType
    Inherits SingularReadOnlyBase(Of ROProductionBookedResourceType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceTypeID, "Resource Type")
    ''' <summary>
    ''' Gets the Resource Type value
    ''' </summary>
    <Display(Name:="Resource Type", Description:="")>
  Public ReadOnly Property ResourceTypeID() As Integer
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
    End Property

    Public Shared ResourceTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceType, "Type")
    ''' <summary>
    ''' Gets the Resource Type value
    ''' </summary>
    <Display(Name:="Type", Description:="")>
    Public ReadOnly Property ResourceType() As String
      Get
        Return GetProperty(ResourceTypeProperty)
      End Get
    End Property

    Public Shared ResourceTypeCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceTypeCount, "Resource Type Count")
    ''' <summary>
    ''' Gets the Resource Type Count value
    ''' </summary>
    <Display(Name:="Resource Type Count", Description:="")>
  Public ReadOnly Property ResourceTypeCount() As Integer
      Get
        Return GetProperty(ResourceTypeCountProperty)
      End Get
    End Property

    Public Shared OBVanCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OBVanCount, "OB Van Count")
    ''' <summary>
    ''' Gets the OB Van Count value
    ''' </summary>
    <Display(Name:="OB Van Count", Description:="")>
  Public ReadOnly Property OBVanCount() As Integer
      Get
        Return GetProperty(OBVanCountProperty)
      End Get
    End Property

    Public Shared ResourceNamesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceNames, "Resource Names")
    ''' <summary>
    ''' Gets the Resource Names value
    ''' </summary>
    <Display(Name:="Resource Names", Description:="")>
  Public ReadOnly Property ResourceNames() As String
      Get
        Return GetProperty(ResourceNamesProperty)
      End Get
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:="")>
  Public ReadOnly Property RowNo() As Integer
      Get
        Return GetProperty(RowNoProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionBookedResourceType(dr As SafeDataReader) As ROProductionBookedResourceType

      Dim r As New ROProductionBookedResourceType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionIDProperty, .GetInt32(0))
        LoadProperty(ResourceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ResourceTypeProperty, .GetString(2))
        LoadProperty(ResourceTypeCountProperty, .GetInt32(3))
        LoadProperty(OBVanCountProperty, .GetInt32(4))
        LoadProperty(ResourceNamesProperty, .GetString(5))
        LoadProperty(RowNoProperty, .GetInt32(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace