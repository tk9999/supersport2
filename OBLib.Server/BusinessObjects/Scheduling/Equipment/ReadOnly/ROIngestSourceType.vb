﻿' Generated 27 Aug 2015 03:55 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace SatOps.ReadOnly

  <Serializable()> _
  Public Class ROIngestSourceType
    Inherits SingularReadOnlyBase(Of ROIngestSourceType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IngestSourceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.IngestSourceTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property IngestSourceTypeID() As Integer
      Get
        Return GetProperty(IngestSourceTypeIDProperty)
      End Get
    End Property

    Public Shared IngestSourceTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IngestSourceType, "Ingest Source Type", "")
    ''' <summary>
    ''' Gets the Ingest Source Type value
    ''' </summary>
    <Display(Name:="Ingest Source Type", Description:="")>
  Public ReadOnly Property IngestSourceType() As String
      Get
        Return GetProperty(IngestSourceTypeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(IngestSourceTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.IngestSourceType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROIngestSourceType(dr As SafeDataReader) As ROIngestSourceType

      Dim r As New ROIngestSourceType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(IngestSourceTypeIDProperty, .GetInt32(0))
        LoadProperty(IngestSourceTypeProperty, .GetString(1))
        LoadProperty(CreatedByProperty, .GetInt32(2))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
        LoadProperty(ModifiedByProperty, .GetInt32(4))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace