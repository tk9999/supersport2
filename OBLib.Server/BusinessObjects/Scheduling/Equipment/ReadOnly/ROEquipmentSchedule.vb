﻿' Generated 02 Jul 2015 13:15 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps.ReadOnly

  <Serializable()> _
  Public Class ROEquipmentSchedule
    Inherits OBReadOnlyBase(Of ROEquipmentSchedule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared EquipmentScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentScheduleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property EquipmentScheduleID() As Integer
      Get
        Return GetProperty(EquipmentScheduleIDProperty)
      End Get
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentID, "Equipment", Nothing)
    ''' <summary>
    ''' Gets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:="")>
  Public ReadOnly Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
    End Property

    Public Shared EquipmentScheduleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentScheduleTypeID, "Equipment Schedule Type", Nothing)
    ''' <summary>
    ''' Gets the Equipment Schedule Type value
    ''' </summary>
    <Display(Name:="Equipment Schedule Type", Description:="")>
  Public ReadOnly Property EquipmentScheduleTypeID() As Integer?
      Get
        Return GetProperty(EquipmentScheduleTypeIDProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
  Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
  Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared EquipmentServiceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentServiceID, "Equipment Service", Nothing)
    ''' <summary>
    ''' Gets the Equipment Service value
    ''' </summary>
    <Display(Name:="Equipment Service", Description:="")>
  Public ReadOnly Property EquipmentServiceID() As Integer?
      Get
        Return GetProperty(EquipmentServiceIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedID, "Feed", Nothing)
    ''' <summary>
    ''' Gets the Feed value
    ''' </summary>
    <Display(Name:="Feed", Description:="")>
  Public ReadOnly Property FeedID() As Integer?
      Get
        Return GetProperty(FeedIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ParentEquipmentScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentEquipmentScheduleID, "Parent Equipment Schedule", Nothing)
    ''' <summary>
    ''' Gets the Parent Equipment Schedule value
    ''' </summary>
    <Display(Name:="Parent Equipment Schedule", Description:="")>
  Public ReadOnly Property ParentEquipmentScheduleID() As Integer?
      Get
        Return GetProperty(ParentEquipmentScheduleIDProperty)
      End Get
    End Property

    Public Shared EquipmentScheduleCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentScheduleComments, "Equipment Schedule Comments", "")
    ''' <summary>
    ''' Gets the Equipment Schedule Comments value
    ''' </summary>
    <Display(Name:="Equipment Schedule Comments", Description:="")>
  Public ReadOnly Property EquipmentScheduleComments() As String
      Get
        Return GetProperty(EquipmentScheduleCommentsProperty)
      End Get
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Debtor", Nothing)
    ''' <summary>
    ''' Gets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="")>
  Public ReadOnly Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "Production Area Status", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:="")>
  Public ReadOnly Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
    End Property

    '  Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingID, "Resource Booking", Nothing)
    '  ''' <summary>
    '  ''' Gets the Resource Booking value
    '  ''' </summary>
    '  <Display(Name:="Resource Booking", Description:="")>
    'Public ReadOnly Property ResourceBookingID() As Integer?
    '    Get
    '      Return GetProperty(ResourceBookingIDProperty)
    '    End Get
    '  End Property

    Public Shared EquipmentScheduleTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentScheduleTitle, "Equipment Schedule Title", "")
    ''' <summary>
    ''' Gets the Equipment Schedule Title value
    ''' </summary>
    <Display(Name:="Equipment Schedule Title", Description:="")>
  Public ReadOnly Property EquipmentScheduleTitle() As String
      Get
        Return GetProperty(EquipmentScheduleTitleProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EquipmentScheduleComments

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEquipmentSchedule(dr As SafeDataReader) As ROEquipmentSchedule

      Dim r As New ROEquipmentSchedule()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(EquipmentScheduleIDProperty, .GetInt32(0))
        LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(EquipmentScheduleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(StartDateTimeProperty, .GetValue(3))
        LoadProperty(EndDateTimeProperty, .GetValue(4))
        LoadProperty(EquipmentServiceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(CreatedByProperty, .GetInt32(7))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(ModifiedByProperty, .GetInt32(9))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
        LoadProperty(ParentEquipmentScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(EquipmentScheduleCommentsProperty, .GetString(12))
        LoadProperty(DebtorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        'LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
        LoadProperty(EquipmentScheduleTitleProperty, .GetString(15))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace