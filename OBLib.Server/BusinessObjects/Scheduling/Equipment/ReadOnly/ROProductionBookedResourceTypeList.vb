﻿' Generated 09 May 2015 11:50 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace SatOps.ReadOnly

  <Serializable()> _
  Public Class ROProductionBookedResourceTypeList
    Inherits SingularReadOnlyListBase(Of ROProductionBookedResourceTypeList, ROProductionBookedResourceType)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROProductionBookedResourceType

      For Each child As ROProductionBookedResourceType In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production", Description:="")>
      Public Overridable Property ProductionID() As Integer?
        Get
          Return ReadProperty(ProductionIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionIDProperty, Value)
        End Set
      End Property

      'Public Shared ProductionIDsProperty As PropertyInfo(Of List(Of Integer)) = RegisterProperty(Of List(Of Integer))(Function(c) c.ProductionIDs, "Production", New List(Of Integer))
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="ProductionIDs", Description:="")>
      'Public Overridable Property ProductionIDs() As List(Of Integer)
      '  Get
      '    Return ReadProperty(ProductionIDsProperty)
      '  End Get
      '  Set(ByVal Value As List(Of Integer))
      '    LoadProperty(ProductionIDsProperty, Value)
      '  End Set
      'End Property

      Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceTypeID, "Resource Type", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Resource Type", Description:="")>
      Public Property ResourceTypeID() As Integer?
        Get
          Return ReadProperty(ResourceTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ResourceTypeIDProperty, Value)
        End Set
      End Property

      'Public Shared ResourceTypeIDsProperty As PropertyInfo(Of List(Of Integer)) = RegisterProperty(Of List(Of Integer))(Function(c) c.ResourceTypeIDs, "Production", New List(Of Integer))
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="ResourceTypeIDs", Description:="")>
      'Public Overridable Property ResourceTypeIDs() As List(Of Integer)
      '  Get
      '    Return ReadProperty(ResourceTypeIDsProperty)
      '  End Get
      '  Set(ByVal Value As List(Of Integer))
      '    LoadProperty(ResourceTypeIDsProperty, Value)
      '  End Set
      'End Property

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionBookedResourceTypeList() As ROProductionBookedResourceTypeList

      Return New ROProductionBookedResourceTypeList()

    End Function

    Public Shared Sub BeginGetROProductionBookedResourceTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionBookedResourceTypeList)))

      Dim dp As New DataPortal(Of ROProductionBookedResourceTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionBookedResourceTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionBookedResourceTypeList)))

      Dim dp As New DataPortal(Of ROProductionBookedResourceTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionBookedResourceTypeList() As ROProductionBookedResourceTypeList

      Return DataPortal.Fetch(Of ROProductionBookedResourceTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionBookedResourceType.GetROProductionBookedResourceType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionBookedResourceTypeList"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            'cm.Parameters.AddWithValue("@ProductionIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ProductionIDs)))
            cm.Parameters.AddWithValue("@ProductionIDs", Strings.MakeEmptyDBNull(""))
            cm.Parameters.AddWithValue("@ResourceTypeID", NothingDBNull(crit.ResourceTypeID))
            'cm.Parameters.AddWithValue("@ResourceTypeIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ResourceTypeIDs)))
            cm.Parameters.AddWithValue("@ResourceTypeIDs", Strings.MakeEmptyDBNull(""))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace