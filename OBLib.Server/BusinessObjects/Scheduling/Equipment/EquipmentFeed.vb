﻿' Generated 26 Mar 2015 18:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.Productions.Crew.ReadOnly
Imports OBLib.Vehicles.ReadOnly
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Productions.Base
Imports OBLib.SatOps.ReadOnly
Imports OBLib.Equipment.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly

Namespace SatOps

  <Serializable()> _
  Public Class EquipmentFeed
    Inherits OBBusinessBase(Of EquipmentFeed)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return EquipmentFeedBO.toString(self)")

    Public Shared EquipmentScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentScheduleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property EquipmentScheduleID() As Integer
      Get
        Return GetProperty(EquipmentScheduleIDProperty)
      End Get
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EquipmentID, Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:=""),
    Required(ErrorMessage:="Equipment is required"),
    DropDownWeb(GetType(ROCircuitAvailabilityList),
                BeforeFetchJS:="EquipmentFeedBO.setCircuitCriteriaBeforeRefresh",
                PreFindJSFunction:="EquipmentFeedBO.triggerCircuitAutoPopulate",
                AfterFetchJS:="EquipmentFeedBO.afterCircuitRefreshAjax",
                OnItemSelectJSFunction:="EquipmentFeedBO.onCircuitSelected",
                LookupMember:="EquipmentName", DisplayMember:="EquipmentName", ValueMember:="ResourceID",
                DropDownCssClass:="circuit-dropdown", DropDownColumns:={"EquipmentName", "IsAvailableString"}),
    SetExpression("EquipmentFeedBO.EquipmentIDSet(self)", False)>
    Public Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingID, "ID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property ResourceBookingID() As Integer?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ResourceBookingIDProperty, value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer)
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Buffer"),
    SetExpression("EquipmentFeedBO.StartDateTimeBufferSet(self)", False)>
    Public Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time is required"),
    SetExpression("EquipmentFeedBO.StartDateTimeSet(self)", False)>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time is required"),
    SetExpression("EquipmentFeedBO.EndDateTimeSet(self)", False)>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer)
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Buffer"),
    SetExpression("EquipmentFeedBO.EndDateTimeBufferSet(self)", False)>
    Public Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedID, "Feed", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Feed", Description:="")>
    Public Property FeedID() As Integer?
      Get
        Return GetProperty(FeedIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedIDProperty, Value)
      End Set
    End Property

    Public Shared FeedTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedTypeID, "Feed Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed Type value
    ''' </summary>
    <Display(Name:="Feed Type", Description:="The type of feed being booked"),
    Required(ErrorMessage:="Feed Type required"),
    DropDownWeb(GetType(ROFeedTypeList))>
    Public Property FeedTypeID() As Integer?
      Get
        Return GetProperty(FeedTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared EquipmentNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentName, "Equipment", "")
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Equipment")>
    Public Property EquipmentName() As String
      Get
        Return GetProperty(EquipmentNameProperty)
      End Get
      Set(value As String)
        SetProperty(EquipmentNameProperty, value)
      End Set
    End Property
    'Required(ErrorMessage:="Equipment is required", AllowEmptyStrings:=False)

    Public Shared EquipmentFeedCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentFeedComments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public Property EquipmentFeedComments() As String
      Get
        Return GetProperty(EquipmentFeedCommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EquipmentFeedCommentsProperty, Value)
      End Set
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Client", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' 
    ''' </summary>
    <Display(Name:="Client", Description:=""),
    DropDownWeb(GetType(RODebtorFindList),
                BeforeFetchJS:="EquipmentFeedBO.setDebtorCriteriaBeforeRefresh",
                PreFindJSFunction:="EquipmentFeedBO.triggerDebtorAutoPopulate",
                AfterFetchJS:="EquipmentFeedBO.afterDebtorRefreshAjax",
                OnItemSelectJSFunction:="EquipmentFeedBO.onDebtorSelected",
                LookupMember:="DebtorName", DisplayMember:="Debtor", ValueMember:="DebtorID",
                DropDownCssClass:="debtor-dropdown", DropDownColumns:={"Debtor", "ContactName", "ContactNumber"}),
    SetExpression("EquipmentFeedBO.DebtorIDSet(self)")>
    Public Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DebtorIDProperty, Value)
      End Set
    End Property

    Public Shared DebtorNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DebtorName, "Client", "")
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Client", Description:="")>
    Public Property DebtorName() As String
      Get
        Return GetProperty(DebtorNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DebtorNameProperty, Value)
      End Set
    End Property

    Public Shared EquipmentFeedTitleProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.EquipmentFeedTitle, "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Title", Description:=""),
    Required(ErrorMessage:="Title is required", AllowEmptyStrings:=False),
    SetExpression("EquipmentFeedBO.EquipmentFeedTitleSet(self)", False)>
    Public Property EquipmentFeedTitle() As String
      Get
        Return GetProperty(EquipmentFeedTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EquipmentFeedTitleProperty, Value)
      End Set
    End Property

    Public Shared EquipmentScheduleIDCopiedFromProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EquipmentScheduleIDCopiedFrom, Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment")>
    Public Property EquipmentScheduleIDCopiedFrom() As Integer?
      Get
        Return GetProperty(EquipmentScheduleIDCopiedFromProperty)
      End Get
      Set(value As Integer?)
        SetProperty(EquipmentScheduleIDCopiedFromProperty, value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, CInt(OBLib.CommonData.Enums.ProductionAreaStatus.New))
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Status", Description:=""),
    Required(ErrorMessage:="Status is required"),
     DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaStatusSelectList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="EquipmentFeedBO.setStatusCriteriaBeforeRefresh",
                 PreFindJSFunction:="EquipmentFeedBO.triggerStatusAutoPopulate",
                 AfterFetchJS:="EquipmentFeedBO.afterStatusRefreshAjax",
                 LookupMember:="ProductionAreaStatusName", ValueMember:="ProductionAreaStatusID", DropDownColumns:={"ProductionAreaStatus"},
                 OnItemSelectJSFunction:="EquipmentFeedBO.onStatusSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("EquipmentFeedBO.ProductionAreaStatusIDSet(self)", False)>
    Public Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatusName, "Status", "New")
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Status", Description:=""),
    Required(ErrorMessage:="Status is required", AllowEmptyStrings:=False)>
    Public Property ProductionAreaStatusName() As String
      Get
        Return GetProperty(ProductionAreaStatusNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaStatusNameProperty, Value)
      End Set
    End Property

    Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "StatusCssClass", "new")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="StatusCssClass", Description:=""),
   Required(ErrorMessage:="Status Css Class required", AllowEmptyStrings:=False)>
    Public Property StatusCssClass() As String
      Get
        Return GetProperty(StatusCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCssClassProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDOwnerProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaIDOwner, "ProductionSystemAreaIDOwner", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="ProductionSystemAreaIDOwner", Description:="")>
    Public Property ProductionSystemAreaIDOwner() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDOwnerProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDOwnerProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "ProductionSystemAreaID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Booking Type", Description:="")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "SystemID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept is required"),
    DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area is required"),
    DropDownWeb(GetType(ROUserSystemAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    'Public Shared EquipmentFeedTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EquipmentFeedTypeID, Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Equipment Schedule Type value
    ' ''' </summary>
    '<Display(Name:="Booking Type", Description:=""),
    'Required(ErrorMessage:="Equipment Schedule Type required"),
    'DropDownWeb(GetType(ROEquipmentFeedTypeList)),
    'SetExpression("EquipmentScheduleBaseBO.EquipmentFeedTypeIDSet(self)", False)>
    'Public Property EquipmentFeedTypeID() As Integer?
    '  Get
    '    Return GetProperty(EquipmentFeedTypeIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(EquipmentFeedTypeIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared EquipmentServiceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentServiceID, "Equipment Service", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Equipment Service value
    ' ''' </summary>
    '<Display(Name:="Equipment Service", Description:="")>
    'Public Property EquipmentServiceID() As Integer?
    '  Get
    '    Return GetProperty(EquipmentServiceIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(EquipmentServiceIDProperty, Value)
    '  End Set
    'End Property

    'Public Shared EquipmentFeedTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentFeedType, "Type", "")
    ' ''' <summary>
    ' ''' Gets the Modified Date Time value
    ' ''' </summary>
    '<Display(Name:="Type"),
    'Required(ErrorMessage:="Status is required", AllowEmptyStrings:=False)>
    'Public Property EquipmentFeedType() As String
    '  Get
    '    Return GetProperty(EquipmentFeedTypeProperty)
    '  End Get
    '  Set(value As String)
    '    SetProperty(EquipmentFeedTypeProperty, value)
    '  End Set
    'End Property

    'Public Shared BookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingDescription, "Description", "")
    ' ''' <summary>
    ' ''' Gets the Modified Date Time value
    ' ''' </summary>
    '<Display(Name:="Description"),
    'Required(ErrorMessage:="Status is required", AllowEmptyStrings:=False)>
    'Public Property BookingDescription() As String
    '  Get
    '    Return GetProperty(BookingDescriptionProperty)
    '  End Get
    '  Set(value As String)
    '    SetProperty(BookingDescriptionProperty, value)
    '  End Set
    'End Property

    'Public Shared ParentEquipmentFeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentEquipmentFeedID, "Feed", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Feed value
    ' ''' </summary>
    '<Display(Name:="Feed", Description:="")>
    'Public Property ParentEquipmentFeedID() As Integer?
    '  Get
    '    Return GetProperty(ParentEquipmentFeedIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(ParentEquipmentFeedIDProperty, Value)
    '  End Set
    'End Property

    ' Public Shared IgnoreClashesProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IgnoreClashes, False)
    ' ''' <summary>
    ' ''' Gets and sets the Can Move value
    ' ''' </summary>
    ' <Display(Name:="IgnoreClashes", Description:="")>
    ' Public Property IgnoreClashes() As Boolean
    '   Get
    '     Return GetProperty(IgnoreClashesProperty)
    '   End Get
    '   Set(ByVal Value As Boolean)
    '     SetProperty(IgnoreClashesProperty, Value)
    '   End Set
    ' End Property

    ' Public Shared IgnoreClashesReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IgnoreClashesReason, "IgnoreClashesReason", "")
    ' ''' <summary>
    ' ''' Gets and sets the Can Move value
    ' ''' </summary>
    ' <Display(Name:="StatusCssClass", Description:="")>
    ' Public Property IgnoreClashesReason() As String
    '   Get
    '     Return GetProperty(IgnoreClashesReasonProperty)
    '   End Get
    '   Set(ByVal Value As String)
    '     SetProperty(IgnoreClashesReasonProperty, Value)
    '   End Set
    ' End Property

    ' Public Shared PopoverContentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PopoverContent, "Popover Content", "")
    ' ''' <summary>
    ' ''' Gets and sets the Resource Booking Description value
    ' ''' </summary>
    ' <Display(Name:="Popover Content")>
    ' Public Property PopoverContent() As String
    '   Get
    '     Return GetProperty(PopoverContentProperty)
    '   End Get
    '   Set(ByVal Value As String)
    '     SetProperty(PopoverContentProperty, Value)
    '   End Set
    ' End Property

    ' Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingTypeID, "ResourceBookingTypeID", Nothing)
    ' ''' <summary>
    ' ''' Gets the ID value
    ' ''' </summary>
    ' ''' 
    ' <Display(Name:="Booking Type", Description:=""),
    'Required(ErrorMessage:="Booking Type required")>
    ' Public Property ResourceBookingTypeID() As Integer?
    '   Get
    '     Return GetProperty(ResourceBookingTypeIDProperty)
    '   End Get
    '   Set(ByVal Value As Integer?)
    '     SetProperty(ResourceBookingTypeIDProperty, Value)
    '   End Set
    ' End Property

    Public Shared ProductionAreaStatusIDOriginalProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaStatusIDOriginal, Nothing)
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property ProductionAreaStatusIDOriginal() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDOriginalProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDOriginalProperty, Value)
      End Set
    End Property

    Public Shared ChangeDescriptionRequiredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ChangeDescriptionRequired, "ChangeDescription", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="ChangeDescription", Description:=""), AlwaysClean>
    Public Property ChangeDescriptionRequired() As Boolean
      Get
        Return GetProperty(ChangeDescriptionRequiredProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ChangeDescriptionRequiredProperty, Value)
      End Set
    End Property

    Public Shared ChangeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangeDescription, "Change Reason", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Change Reason", Description:=""), AlwaysClean>
    Public Property ChangeDescription() As String
      Get
        Return GetProperty(ChangeDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChangeDescriptionProperty, Value)
      End Set
    End Property

    Public Shared AddSynergyGenRefNoProperty As PropertyInfo(Of Int64?) = RegisterSProperty(Of Int64?)(Function(c) c.AddSynergyGenRefNo, Nothing)
    ''' <summary>
    ''' Gets and sets the Synergy Gen Ref No value
    ''' </summary>
    <Display(Name:="Add Gen Ref", Description:="")>
    Public Property AddSynergyGenRefNo() As Int64?
      Get
        Return GetProperty(AddSynergyGenRefNoProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(AddSynergyGenRefNoProperty, Value)
      End Set
    End Property

    Public Shared ImportAllEventsSameDayAndVenueProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ImportAllEventsSameDayAndVenue, "Same Day and Venue", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Same Day and Venue"), AlwaysClean>
    Public Property ImportAllEventsSameDayAndVenue() As Boolean
      Get
        Return GetProperty(ImportAllEventsSameDayAndVenueProperty)
      End Get
      Set(value As Boolean)
        SetProperty(ImportAllEventsSameDayAndVenueProperty, value)
      End Set
    End Property

    Public Shared IsOwnerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsOwner, "IsOwner", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="IsOwner"), AlwaysClean>
    Public Property IsOwner() As Boolean
      Get
        Return GetProperty(IsOwnerProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsOwnerProperty, value)
      End Set
    End Property

    Public Shared StartDateTimeBufferOriginalProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTimeBufferOriginal)
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Buffer")>
    Public Property StartDateTimeBufferOriginal As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferOriginalProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeBufferOriginalProperty, Value)
      End Set
    End Property

    Public Shared IngestRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.IngestRate, "Ingest Rate", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Ingest Rate", Description:=""),
    Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property IngestRate() As Decimal
      Get
        Return GetProperty(IngestRateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(IngestRateProperty, Value)
      End Set
    End Property

    Public Shared CoordinationRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.CoordinationRate, "Coordination Rate", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Coordination Rate", Description:=""),
    Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property CoordinationRate() As Decimal
      Get
        Return GetProperty(CoordinationRateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(CoordinationRateProperty, Value)
      End Set
    End Property

    Public Shared CostCentreIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CostCentreID, "Cost Center", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed Type value
    ''' </summary>
    <Display(Name:="Cost Center", Description:="The type of feed being booked"),
    DropDownWeb(GetType(ROCostCentreList), DisplayMember:="CostCentreCode", ValueMember:="CostCentreID")>
    Public Property CostCentreID() As Integer?
      Get
        Return GetProperty(CostCentreIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CostCentreIDProperty, Value)
      End Set
    End Property

    Public Shared SlugNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SlugName, "Slug Name", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Slug Name", Description:="")>
    Public Property SlugName() As String
      Get
        Return GetProperty(SlugNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SlugNameProperty, Value)
      End Set
    End Property

    Public Shared PriorityFeedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PriorityFeed, "Priority Feed?", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Priority Feed?")>
    Public Property PriorityFeed() As Boolean
      Get
        Return GetProperty(PriorityFeedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(PriorityFeedProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Overrides Function ToString() As String

      Return "Production Feed"

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentScheduleIDProperty)

    End Function

    Public Function AddGenRef() As Singular.Web.Result

      Dim sh As Singular.Web.Result = Nothing
      Try
        Dim prods As List(Of FeedProduction) = Me.FeedProductionList.Where(Function(d) CompareSafe(d.SynergyGenRefNo, Me.AddSynergyGenRefNo)).ToList
        'Not already added
        If prods.Count = 0 Then
          Dim pdl As ProductionDetailBaseList = OBLib.Helpers.SynergyHelper.AddGenRef(Me.AddSynergyGenRefNo, "Feed")
          If pdl.Count = 1 Then
            Dim fp As FeedProduction = Me.FeedProductionList.AddNew
            fp.SynergyGenRefNo = pdl(0).SynergyGenRefNo
            fp.ProductionID = pdl(0).ProductionID
            fp.ProductionDescription = pdl(0).Title
            Me.EquipmentFeedTitle = fp.ProductionDescription
            'Me.GetParent.BookingDescription = Me.FeedDescription
            'Me.GetParent.EquipmentScheduleTitle = Me.FeedDescription
            'Me.GetParent.EquipmentScheduleBookingList(0).ResourceBookingDescription = Me.FeedDescription
          End If
        End If
        sh = New Singular.Web.Result(True)
      Catch ex As Exception
        sh = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try
      Return sh

    End Function

    Public Function AddGenRefs() As Singular.Web.Result

      Dim sh As Singular.Web.Result = Nothing
      Try
        For Each genref As Int64 In FeedGenRefs
          Dim prods As List(Of FeedProduction) = Me.FeedProductionList.Where(Function(d) CompareSafe(d.SynergyGenRefNo, genref)).ToList
          'Not already added
          If prods.Count = 0 Then
            Dim pdl As ProductionDetailBaseList = OBLib.Helpers.SynergyHelper.AddGenRef(genref, "Feed")
            If pdl.Count = 1 Then
              Dim fp As FeedProduction = Me.FeedProductionList.AddNew
              fp.SynergyGenRefNo = pdl(0).SynergyGenRefNo
              fp.ProductionID = pdl(0).ProductionID
              fp.ProductionDescription = pdl(0).Title
              Me.EquipmentFeedTitle = fp.ProductionDescription
              'Me.GetParent.BookingDescription = Me.FeedDescription
              'Me.GetParent.EquipmentScheduleTitle = Me.FeedDescription
              'Me.GetParent.EquipmentScheduleBookingList(0).ResourceBookingDescription = Me.FeedDescription
            End If
          End If
        Next
        sh = New Singular.Web.Result(True)
      Catch ex As Exception
        sh = New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try
      Return sh

    End Function

    Sub SendChangeNotification()

      Dim FeedDate As DateTime?
      If Me.StartDateTimeBufferOriginal Is Nothing Then
        FeedDate = Me.StartDateTimeBuffer
      Else
        FeedDate = Me.StartDateTimeBufferOriginal
      End If
      OBLib.ServiceHelpers.SSWSDHelpers.SendFeedChangeNotification(Me.FeedID, FeedDate.Value, ChangeDescription)

      'OBLib.OBMisc.LogICRServiceFeedback(OBLib.Security.Settings.CurrentUser.LoginName, Me.FeedID, "SendChangeNotification", "Sending Feedback Started")
      'Dim SSWSDService As SSWSD.SSWSDSVCClient
      'Try
      '  SSWSDService = New SSWSD.SSWSDSVCClient
      '  Dim FeedDate As DateTime?
      '  If Me.StartDateTimeBufferOriginal Is Nothing Then
      '    FeedDate = Me.StartDateTimeBuffer
      '  Else
      '    FeedDate = Me.StartDateTimeBufferOriginal
      '  End If
      '  Dim request As SSWSD.RetObj_SatOpsUpdateAlert = SSWSDService.SatOpsUpdateAlert("94208fee-5615-11e5-a800-005056943beb", "SSWSD_Sober", Me.FeedID.ToString, FeedDate.Value.ToString("dd MMM yyyy"), ChangeDescription)
      '  OBLib.OBMisc.LogICRServiceFeedback(OBLib.Security.Settings.CurrentUser.LoginName, Me.FeedID, "SatOpsUpdateAlert", request.SvcFeedback)
      '  Dim a As Object = Nothing
      'Catch ex As Exception
      '  OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "EquipmentSchedule.vb", "SendChangeNotification", ex.Message)
      'End Try

    End Sub

    Public Sub SetOriginalStatus()
      LoadProperty(ProductionAreaStatusIDOriginalProperty, ProductionAreaStatusID)
    End Sub

    Public Sub SetIsOwner()
      If IsNew Then
        LoadProperty(IsOwnerProperty, True)
      Else
        LoadProperty(IsOwnerProperty, CompareSafe(ProductionSystemAreaIDOwner, ProductionSystemAreaID))
      End If
    End Sub

#End Region

#End Region

#Region " Child Lists "

    Public Shared FeedProductionListProperty As PropertyInfo(Of FeedProductionList) = RegisterProperty(Of FeedProductionList)(Function(c) c.FeedProductionList, "Feed Production List")
    Public ReadOnly Property FeedProductionList() As FeedProductionList
      Get
        If GetProperty(FeedProductionListProperty) Is Nothing Then
          LoadProperty(FeedProductionListProperty, SatOps.FeedProductionList.NewFeedProductionList())
        End If
        Return GetProperty(FeedProductionListProperty)
      End Get
    End Property

    Public Shared FeedTurnAroundPointListProperty As PropertyInfo(Of FeedTurnAroundPointList) = RegisterProperty(Of FeedTurnAroundPointList)(Function(c) c.FeedTurnAroundPointList, "Feed Turn Around Point List")
    Public ReadOnly Property FeedTurnAroundPointList() As FeedTurnAroundPointList
      Get
        If GetProperty(FeedTurnAroundPointListProperty) Is Nothing Then
          LoadProperty(FeedTurnAroundPointListProperty, SatOps.FeedTurnAroundPointList.NewFeedTurnAroundPointList())
        End If
        Return GetProperty(FeedTurnAroundPointListProperty)
      End Get
    End Property

    Public Shared FeedPathListProperty As PropertyInfo(Of FeedPathList) = RegisterProperty(Of FeedPathList)(Function(c) c.FeedPathList, "Feed Path List")
    Public ReadOnly Property FeedPathList() As FeedPathList
      Get
        If GetProperty(FeedPathListProperty) Is Nothing Then
          LoadProperty(FeedPathListProperty, SatOps.FeedPathList.NewFeedPathList())
        End If
        Return GetProperty(FeedPathListProperty)
      End Get
    End Property

    Public Shared FeedDefaultAudioSettingListProperty As PropertyInfo(Of FeedDefaultAudioSettingList) = RegisterProperty(Of FeedDefaultAudioSettingList)(Function(c) c.FeedDefaultAudioSettingList, "Feed Default Audio Setting List")
    Public ReadOnly Property FeedDefaultAudioSettingList() As FeedDefaultAudioSettingList
      Get
        If GetProperty(FeedDefaultAudioSettingListProperty) Is Nothing Then
          LoadProperty(FeedDefaultAudioSettingListProperty, SatOps.FeedDefaultAudioSettingList.NewFeedDefaultAudioSettingList())
        End If
        Return GetProperty(FeedDefaultAudioSettingListProperty)
      End Get
    End Property

    Public Shared FeedDefaultAudioSettingSelectListProperty As PropertyInfo(Of FeedDefaultAudioSettingSelectList) = RegisterProperty(Of FeedDefaultAudioSettingSelectList)(Function(c) c.FeedDefaultAudioSettingSelectList, "Feed Default Audio Setting List")
    Public ReadOnly Property FeedDefaultAudioSettingSelectList() As FeedDefaultAudioSettingSelectList
      Get
        If GetProperty(FeedDefaultAudioSettingSelectListProperty) Is Nothing Then
          LoadProperty(FeedDefaultAudioSettingSelectListProperty, SatOps.FeedDefaultAudioSettingSelectList.NewFeedDefaultAudioSettingSelectList())
        End If
        Return GetProperty(FeedDefaultAudioSettingSelectListProperty)
      End Get
    End Property

    Public Shared FeedIngestInstructionListProperty As PropertyInfo(Of FeedIngestInstructionList) = RegisterProperty(Of FeedIngestInstructionList)(Function(c) c.FeedIngestInstructionList, "Feed Path List")
    Public ReadOnly Property FeedIngestInstructionList() As FeedIngestInstructionList
      Get
        If GetProperty(FeedIngestInstructionListProperty) Is Nothing Then
          LoadProperty(FeedIngestInstructionListProperty, SatOps.FeedIngestInstructionList.NewFeedIngestInstructionList())
        End If
        Return GetProperty(FeedIngestInstructionListProperty)
      End Get
    End Property

    Private mROCrewDetailByProductionList = New ROCrewDetailByProductionList
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ROCrewDetailByProductionList As ROCrewDetailByProductionList
      Get
        Return mROCrewDetailByProductionList
      End Get
    End Property

    Private mROVehicleDetailByProductionList = New ROVehicleDetailByProductionList
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ROVehicleDetailByProductionList As ROVehicleDetailByProductionList
      Get
        Return mROVehicleDetailByProductionList
      End Get
    End Property

    Public Shared EquipmentFeedCostListProperty As PropertyInfo(Of Costs.EquipmentFeedCostList) = RegisterProperty(Of Costs.EquipmentFeedCostList)(Function(c) c.EquipmentFeedCostList, "Equipment Feed Cost List")
    Public ReadOnly Property EquipmentFeedCostList() As Costs.EquipmentFeedCostList
      Get
        If GetProperty(EquipmentFeedCostListProperty) Is Nothing Then
          LoadProperty(EquipmentFeedCostListProperty, Costs.EquipmentFeedCostList.NewEquipmentFeedCostList())
        End If
        Return GetProperty(EquipmentFeedCostListProperty)
      End Get
    End Property

    Public Shared FeedGenRefsProperty As PropertyInfo(Of List(Of Int64)) = RegisterProperty(Of List(Of Int64))(Function(c) c.FeedGenRefs, "Feed List")
    Public ReadOnly Property FeedGenRefs() As List(Of Int64)
      Get
        If GetProperty(FeedGenRefsProperty) Is Nothing Then
          LoadProperty(FeedGenRefsProperty, New List(Of Int64))
        End If
        Return GetProperty(FeedGenRefsProperty)
      End Get
    End Property

    Public Shared IncidentItemListProperty As PropertyInfo(Of OBLib.Scheduling.Equipment.IncidentItemList) = RegisterProperty(Of OBLib.Scheduling.Equipment.IncidentItemList)(Function(c) c.IncidentItemList, "IncidentItemList")
    Public ReadOnly Property IncidentItemList() As OBLib.Scheduling.Equipment.IncidentItemList
      Get
        If GetProperty(IncidentItemListProperty) Is Nothing Then
          LoadProperty(IncidentItemListProperty, OBLib.Scheduling.Equipment.IncidentItemList.NewIncidentItemList())
        End If
        Return GetProperty(IncidentItemListProperty)
      End Get
    End Property

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(StartDateTimeBufferProperty)
        .JavascriptRuleFunctionName = "EquipmentScheduleBaseBO.BufferStartTimeValid"
        .ServerRuleFunction = AddressOf CallTimeValid
        .AddTriggerProperty(EndDateTimeProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(StartDateTimeProperty)
        .JavascriptRuleFunctionName = "EquipmentScheduleBaseBO.StartDateTimeValid"
        .ServerRuleFunction = AddressOf StartDateTimeValid
        .AddTriggerProperty(EndDateTimeProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(EndDateTimeProperty)
        .JavascriptRuleFunctionName = "EquipmentScheduleBaseBO.EndDateTimeValid"
        .ServerRuleFunction = AddressOf EndDateTimeValid
        .AddTriggerProperty(StartDateTimeProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(EndDateTimeBufferProperty)
        .JavascriptRuleFunctionName = "EquipmentScheduleBaseBO.BufferEndTimeValid"
        .ServerRuleFunction = AddressOf WrapTimeValid
        .AddTriggerProperty(StartDateTimeProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(ChangeDescriptionProperty)
        .JavascriptRuleFunctionName = "EquipmentFeedBO.ChangeDescriptionValid"
        .ServerRuleFunction = AddressOf ChangeDescriptionValid
        .AddTriggerProperty(ChangeDescriptionRequiredProperty)
      End With

    End Sub

    Public Shared Function CallTimeValid(EquipmentFeed As EquipmentFeed) As String
      Dim Err As String = ""
      If EquipmentFeed.StartDateTimeBuffer Is Nothing Then
        Err &= "Buffer Start Time is required"
      ElseIf EquipmentFeed.StartDateTimeBuffer IsNot Nothing And EquipmentFeed.StartDateTime IsNot Nothing Then
        If EquipmentFeed.StartDateTimeBuffer.Value > EquipmentFeed.StartDateTime.Value Then
          Err &= "Buffer Start Time must be before Start Time"
        End If
      End If
      Return Err
    End Function

    Public Shared Function StartDateTimeValid(EquipmentFeed As EquipmentFeed) As String
      Dim Err As String = ""
      If EquipmentFeed.StartDateTime Is Nothing Then
        Err &= "Start Time is required"
      ElseIf EquipmentFeed.StartDateTime IsNot Nothing And EquipmentFeed.EndDateTime IsNot Nothing Then
        If EquipmentFeed.StartDateTime.Value > EquipmentFeed.EndDateTime.Value Then
          Err &= "Start Time must be before End Time"
        End If
      End If
      Return Err
    End Function

    Public Shared Function EndDateTimeValid(EquipmentFeed As EquipmentFeed) As String
      Dim Err As String = ""
      If EquipmentFeed.EndDateTime Is Nothing Then
        Err &= "End Time is required"
      ElseIf EquipmentFeed.StartDateTime IsNot Nothing And EquipmentFeed.EndDateTime IsNot Nothing Then
        If EquipmentFeed.StartDateTime.Value > EquipmentFeed.EndDateTime.Value Then
          Err &= "End Time must be after Start Time"
        End If
      End If
      Return Err
    End Function

    Public Shared Function WrapTimeValid(EquipmentFeed As EquipmentFeed) As String
      Dim Err As String = ""
      If EquipmentFeed.EndDateTime Is Nothing Then
        Err &= "Buffer End Time is required"
      ElseIf EquipmentFeed.EndDateTimeBuffer IsNot Nothing And EquipmentFeed.EndDateTime IsNot Nothing Then
        If EquipmentFeed.EndDateTimeBuffer.Value < EquipmentFeed.EndDateTime.Value Then
          Err &= "Buffer End Time must be after End Time"
        End If
      End If
      Return Err
    End Function

    Public Shared Function ChangeDescriptionValid(EquipmentFeed As EquipmentFeed) As String
      Dim ErrorMsg As String = ""
      If EquipmentFeed.ChangeDescriptionRequired AndAlso EquipmentFeed.ChangeDescription.Trim.Length = 0 Then
        ErrorMsg = "Change Reason is required"
      End If
      Return ErrorMsg
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipmentFeed() method.

    End Sub

    Public Shared Function NewEquipmentFeed() As EquipmentFeed

      Return DataPortal.CreateChild(Of EquipmentFeed)()

    End Function

    Public Shared Function GetEquipmentFeed(dr As SafeDataReader) As EquipmentFeed

      Dim f As New EquipmentFeed()
      f.Fetch(dr)
      Return f

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EquipmentScheduleIDProperty, .GetInt32(0))
          LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceBookingIDProperty, ZeroNothing(.GetInt32(2)))
          LoadProperty(ResourceIDProperty, ZeroNothing(.GetInt32(3)))
          LoadProperty(StartDateTimeBufferProperty, .GetValue(4))
          LoadProperty(StartDateTimeProperty, .GetValue(5))
          LoadProperty(EndDateTimeProperty, .GetValue(6))
          LoadProperty(EndDateTimeBufferProperty, .GetValue(7))
          LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(FeedTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(CreatedByProperty, .GetInt32(10))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(11))
          LoadProperty(ModifiedByProperty, .GetInt32(12))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(13))
          LoadProperty(EquipmentNameProperty, .GetString(14))
          LoadProperty(EquipmentFeedCommentsProperty, .GetString(15))
          LoadProperty(DebtorIDProperty, ZeroNothing(.GetInt32(16)))
          LoadProperty(DebtorNameProperty, .GetString(17))
          LoadProperty(EquipmentFeedTitleProperty, .GetString(18))
          LoadProperty(EquipmentScheduleIDCopiedFromProperty, ZeroNothing(.GetInt32(19)))
          LoadProperty(ProductionAreaStatusIDProperty, ZeroNothing(.GetInt32(20)))
          LoadProperty(ProductionAreaStatusNameProperty, .GetString(21))
          LoadProperty(StatusCssClassProperty, .GetString(22))
          LoadProperty(ProductionSystemAreaIDOwnerProperty, ZeroNothing(.GetInt32(23)))
          LoadProperty(ProductionSystemAreaIDProperty, ZeroNothing(.GetInt32(24)))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(25)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(26)))
          LoadProperty(ProductionAreaStatusIDOriginalProperty, ZeroNothing(.GetInt32(20)))
          LoadProperty(StartDateTimeBufferOriginalProperty, StartDateTimeBuffer)
          LoadProperty(IngestRateProperty, .GetDecimal(27))
          LoadProperty(CoordinationRateProperty, .GetDecimal(28))
          LoadProperty(CostCentreIDProperty, ZeroNothing(.GetInt32(32)))
          LoadProperty(SlugNameProperty, .GetString(33))
          LoadProperty(PriorityFeedProperty, .GetBoolean(34))
          'LoadProperty(EquipmentFeedTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          'LoadProperty(EquipmentServiceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          'ProductionID = 11
          'LoadProperty(EquipmentFeedTypeProperty, .GetString(12))
          'LoadProperty(ParentEquipmentFeedIDProperty, ZeroNothing(.GetInt32(13)))
          'LoadProperty(IgnoreClashesProperty, .GetBoolean(26))
          'LoadProperty(IgnoreClashesReasonProperty, .GetString(27))
          'LoadProperty(PopoverContentProperty, .GetString(28))
          'LoadProperty(ResourceBookingTypeIDProperty, .GetInt32(29))
          'SetBookingDescription()
        End With
      End Using

      'FetchExtraProperties(sdr)

      MarkAsChild()
      MarkOld()
      SetIsOwner()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEquipmentFeed"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEquipmentFeed"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      SetIsOwner() 'reset just incase it has been tampered with client side
      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure
          'LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(25)))
          'LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(26)))
          'LoadProperty(ProductionAreaStatusIDOriginalProperty, ZeroNothing(.GetInt32(27)))
          Dim paramEquipmentFeedID As SqlParameter = .Parameters.Add("@EquipmentScheduleID", SqlDbType.Int)
          paramEquipmentFeedID.Value = GetProperty(EquipmentScheduleIDProperty)
          Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
          Dim paramProductionSystemAreaID As SqlParameter = .Parameters.Add("@ProductionSystemAreaID", SqlDbType.Int)
          paramProductionSystemAreaID.Value = GetProperty(ProductionSystemAreaIDProperty)
          Dim paramProductionSystemAreaIDOwner As SqlParameter = .Parameters.Add("@ProductionSystemAreaIDOwner", SqlDbType.Int)
          paramProductionSystemAreaIDOwner.Value = GetProperty(ProductionSystemAreaIDOwnerProperty)
          If Me.IsNew Then
            paramEquipmentFeedID.Direction = ParameterDirection.Output
            paramResourceBookingID.Direction = ParameterDirection.Output
            paramProductionSystemAreaID.Direction = ParameterDirection.Output
            paramProductionSystemAreaIDOwner.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EquipmentID", GetProperty(EquipmentIDProperty))
          .Parameters.AddWithValue("@ResourceID", NothingDBNull(GetProperty(ResourceIDProperty)))
          .Parameters.AddWithValue("@StartDateTimeBuffer", (New SmartDate(GetProperty(StartDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTimeBuffer", (New SmartDate(GetProperty(EndDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@FeedID", Singular.Misc.NothingDBNull(GetProperty(FeedIDProperty)))
          .Parameters.AddWithValue("@FeedTypeID", Singular.Misc.NothingDBNull(GetProperty(FeedTypeIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@EquipmentName", GetProperty(EquipmentNameProperty))
          .Parameters.AddWithValue("@EquipmentFeedComments", GetProperty(EquipmentFeedCommentsProperty))
          .Parameters.AddWithValue("@DebtorID", NothingDBNull(GetProperty(DebtorIDProperty)))
          .Parameters.AddWithValue("@DebtorName", NothingDBNull(GetProperty(DebtorNameProperty)))
          .Parameters.AddWithValue("@EquipmentFeedTitle", GetProperty(EquipmentFeedTitleProperty))
          .Parameters.AddWithValue("@EquipmentScheduleIDCopiedFrom", NothingDBNull(GetProperty(EquipmentScheduleIDCopiedFromProperty)))
          .Parameters.AddWithValue("@ProductionAreaStatusID", NothingDBNull(GetProperty(ProductionAreaStatusIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaStatusName", NothingDBNull(GetProperty(ProductionAreaStatusNameProperty)))
          .Parameters.AddWithValue("@StatusCssClass", GetProperty(StatusCssClassProperty))
          '.Parameters.AddWithValue("@ProductionSystemAreaIDOwner", NothingDBNull(GetProperty(ProductionSystemAreaIDOwnerProperty)))
          '.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(GetProperty(ProductionSystemAreaIDProperty)))
          .Parameters.AddWithValue("@SystemID", NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@IngestRate", GetProperty(IngestRateProperty))
          .Parameters.AddWithValue("@CoordinationRate", GetProperty(CoordinationRateProperty))
          .Parameters.AddWithValue("@CostCentreID", NothingDBNull(GetProperty(CostCentreIDProperty)))
          .Parameters.AddWithValue("@SlugName", GetProperty(SlugNameProperty))
          .Parameters.AddWithValue("@PriorityFeed", GetProperty(PriorityFeedProperty))
          '.Parameters.AddWithValue("@ResourceBookingID", NothingDBNull(GetProperty(ResourceBookingIDProperty)))
          '.Parameters.AddWithValue("@IgnoreClashes", GetProperty(IgnoreClashesProperty))
          '.Parameters.AddWithValue("@IgnoreClashesReason", GetProperty(IgnoreClashesReasonProperty))
          '.Parameters.AddWithValue("@PopoverContent", GetProperty(PopoverContentProperty))
          '.Parameters.AddWithValue("@ResourceBookingTypeID", NothingDBNull(GetProperty(ResourceBookingTypeIDProperty)))
          '.Parameters.AddWithValue("@EquipmentFeedTypeID", GetProperty(EquipmentFeedTypeIDProperty))
          '.Parameters.AddWithValue("@EquipmentServiceID", Singular.Misc.NothingDBNull(GetProperty(EquipmentServiceIDProperty)))
          '.Parameters.AddWithValue("@ParentEquipmentFeedID", NothingDBNull(GetProperty(ParentEquipmentFeedIDProperty)))
          'AddExtraParameters(.Parameters)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(EquipmentScheduleIDProperty, paramEquipmentFeedID.Value)
            LoadProperty(ResourceBookingIDProperty, paramResourceBookingID.Value)
            LoadProperty(ProductionSystemAreaIDProperty, paramProductionSystemAreaID.Value)
            LoadProperty(ProductionSystemAreaIDOwnerProperty, paramProductionSystemAreaIDOwner.Value)
          End If

          ' update child objects
          If GetProperty(FeedDefaultAudioSettingListProperty) IsNot Nothing Then
            FeedDefaultAudioSettingList.Update()
          End If
          If GetProperty(FeedProductionListProperty) IsNot Nothing Then
            FeedProductionList.Update()
          End If
          If GetProperty(FeedTurnAroundPointListProperty) IsNot Nothing Then
            FeedTurnAroundPointList.Update()
          End If
          If GetProperty(FeedPathListProperty) IsNot Nothing Then
            FeedPathList.Update()
          End If
          If GetProperty(FeedDefaultAudioSettingSelectListProperty) IsNot Nothing Then
            FeedDefaultAudioSettingSelectList.Update()
          End If
          If GetProperty(FeedIngestInstructionListProperty) IsNot Nothing Then
            FeedIngestInstructionList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(FeedDefaultAudioSettingListProperty) IsNot Nothing Then
          FeedDefaultAudioSettingList.Update()
        End If
        If GetProperty(FeedProductionListProperty) IsNot Nothing Then
          FeedProductionList.Update()
        End If
        If GetProperty(FeedTurnAroundPointListProperty) IsNot Nothing Then
          FeedTurnAroundPointList.Update()
        End If
        If GetProperty(FeedPathListProperty) IsNot Nothing Then
          FeedPathList.Update()
        End If
        If GetProperty(FeedDefaultAudioSettingSelectListProperty) IsNot Nothing Then
          FeedDefaultAudioSettingSelectList.Update()
        End If
        If GetProperty(FeedIngestInstructionListProperty) IsNot Nothing Then
          FeedIngestInstructionList.Update()
        End If
      End If

      'Save these items regardless of ownership or not
      SaveChildren()

      If Me.IsOwner Then

        'SendNotification
        If Me.ChangeDescriptionRequired Then
          Me.SendChangeNotification()
        ElseIf (Me.ProductionAreaStatusIDOriginal <> CType(OBLib.CommonData.Enums.ProductionAreaStatus.Confirmed, Integer) AndAlso (Me.ProductionAreaStatusID = CType(OBLib.CommonData.Enums.ProductionAreaStatus.Confirmed, Integer))) Then
          Me.ChangeDescription = "Feed Confirmed"
          Me.SendChangeNotification()
        End If
        LoadProperty(ProductionAreaStatusIDOriginalProperty, Me.ProductionAreaStatusID)

        'Set Original Values
        LoadProperty(StartDateTimeBufferOriginalProperty, StartDateTimeBuffer)

      End If


    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEquipmentFeed"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EquipmentFeedID", GetProperty(EquipmentScheduleIDProperty))
        cm.Parameters.AddWithValue("@FeedID", GetProperty(FeedIDProperty))
        cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub SaveChildren()
      UpdateChild(GetProperty(EquipmentFeedCostListProperty))
    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace