﻿' Generated 28 Mar 2015 12:29 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.SatOps.ReadOnly

Namespace SatOps

  <Serializable()> _
  Public Class FeedTurnAroundPoint
    Inherits SingularBusinessBase(Of FeedTurnAroundPoint)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsExpanded, "Expanded", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsExpandedProperty, value)
      End Set
    End Property

    Public Shared FeedTurnAroundPointIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedTurnAroundPointID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property FeedTurnAroundPointID() As Integer
      Get
        Return GetProperty(FeedTurnAroundPointIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedID, "Feed", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Feed", Description:="")>
    Public Property FeedID() As Integer?
      Get
        Return GetProperty(FeedIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedIDProperty, Value)
      End Set
    End Property
    'Required(ErrorMessage:="Feed required")

    Public Shared TurnAroundPointIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TurnAroundPointID, "Turn Around Point", Nothing)
    ''' <summary>
    ''' Gets and sets the Turn Around Point value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="TurnAround Point", Description:=""),
    Required(ErrorMessage:="TurnAround Point is required"),
    DropDownWeb(GetType(ROTurnAroundPointList),
                BeforeFetchJS:="FeedTurnAroundPointBO.setTurnAroundPointCriteriaBeforeRefresh",
                PreFindJSFunction:="FeedTurnAroundPointBO.triggerTurnAroundPointAutoPopulate",
                AfterFetchJS:="FeedTurnAroundPointBO.afterTurnAroundPointRefreshAjax",
                OnItemSelectJSFunction:="FeedTurnAroundPointBO.onTurnAroundPointSelected",
                LookupMember:="TurnAroundPoint", DisplayMember:="TurnAroundPoint", ValueMember:="TurnAroundPointID",
                DropDownCssClass:="tap-dropdown", DropDownColumns:={"TurnAroundPoint", "Country"}),
    SetExpression("FeedTurnAroundPointBO.TurnAroundPointIDSet(self)")>
    Public Property TurnAroundPointID() As Integer?
      Get
        Return GetProperty(TurnAroundPointIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TurnAroundPointIDProperty, Value)
      End Set
    End Property

    Public Shared TurnAroundPointOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TurnAroundPointOrder, "Order", 1)
    ''' <summary>
    ''' Gets and sets the Turn Around Point value
    ''' </summary>
    <Display(Name:="Order", Description:="")>
    Public Property TurnAroundPointOrder() As Integer
      Get
        Return GetProperty(TurnAroundPointOrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TurnAroundPointOrderProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared TurnAroundPointProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TurnAroundPoint, "Turn Around Point", "")
    ''' <summary>
    ''' Gets and sets the Turn Around Point value
    ''' </summary>
    <Display(Name:="Turn Around Point", Description:="")>
    Public Property TurnAroundPoint() As String
      Get
        Return GetProperty(TurnAroundPointProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TurnAroundPointProperty, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Feed Description value
    ''' </summary>
    <Display(Name:="Comments", Description:="Brief description of the turnaround point"),
    StringLength(250, ErrorMessage:="Comments cannot be more than 250 characters")>
    Public Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedTurnAroundPointIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feed Turn Around Point")
        Else
          Return String.Format("Blank {0}", "Feed Turn Around Point")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Public Function GetParent() As EquipmentFeed

      Return CType(CType(Me.Parent, FeedTurnAroundPointList).Parent, EquipmentFeed)

    End Function

#End Region

#Region " Child Lists "

    Public Shared FeedTurnAroundPointContactListProperty As PropertyInfo(Of FeedTurnAroundPointContactList) = RegisterProperty(Of FeedTurnAroundPointContactList)(Function(c) c.FeedTurnAroundPointContactList, "Feed Turn Around Point Contact List")
    Public ReadOnly Property FeedTurnAroundPointContactList() As FeedTurnAroundPointContactList
      Get
        If GetProperty(FeedTurnAroundPointContactListProperty) Is Nothing Then
          LoadProperty(FeedTurnAroundPointContactListProperty, SatOps.FeedTurnAroundPointContactList.NewFeedTurnAroundPointContactList())
        End If
        Return GetProperty(FeedTurnAroundPointContactListProperty)
      End Get
    End Property

    '    Public Shared FeedTurnAroundPointAudioSettingListProperty As PropertyInfo(Of FeedTurnAroundPointAudioSettingList) = RegisterProperty(Of FeedTurnAroundPointAudioSettingList)(Function(c) c.FeedTurnAroundPointAudioSettingList, "Feed Turn Around Point Audio Setting List")
    '    Public ReadOnly Property FeedTurnAroundPointAudioSettingList() As FeedTurnAroundPointAudioSettingList
    '      Get
    '        If GetProperty(FeedTurnAroundPointAudioSettingListProperty) Is Nothing Then
    '          LoadProperty(FeedTurnAroundPointAudioSettingListProperty, SatOps.FeedTurnAroundPointAudioSettingList.NewFeedTurnAroundPointAudioSettingList())
    '        End If
    '        Return GetProperty(FeedTurnAroundPointAudioSettingListProperty)
    '      End Get
    '    End Property

    '    Public Shared FeedTurnAroundPointVideoSettingListProperty As PropertyInfo(Of FeedTurnAroundPointVideoSettingList) = RegisterProperty(Of FeedTurnAroundPointVideoSettingList)(Function(c) c.FeedTurnAroundPointVideoSettingList, "Feed Turn Around Point Video Setting List")
    '    Public ReadOnly Property FeedTurnAroundPointVideoSettingList() As FeedTurnAroundPointVideoSettingList
    '      Get
    '        If GetProperty(FeedTurnAroundPointVideoSettingListProperty) Is Nothing Then
    '          LoadProperty(FeedTurnAroundPointVideoSettingListProperty, SatOps.FeedTurnAroundPointVideoSettingList.NewFeedTurnAroundPointVideoSettingList())
    '        End If
    '        Return GetProperty(FeedTurnAroundPointVideoSettingListProperty)
    '      End Get
    '    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(TurnAroundPointOrderProperty)
        .JavascriptRuleFunctionName = "FeedTurnAroundPointBO.TurnAroundPointOrderValid"
        .ServerRuleFunction = AddressOf TurnAroundPointOrderValid
      End With

    End Sub

    Public Function TurnAroundPointOrderValid(FeedTurnAroundPoint As FeedTurnAroundPoint) As String
      Dim ErrorMsg As String = ""
      If FeedTurnAroundPoint.TurnAroundPointOrder = 0 Then
        ErrorMsg = "Order cannot be 0"
      ElseIf FeedTurnAroundPoint.Parent IsNot Nothing Then
        Dim ftp As List(Of FeedTurnAroundPoint) = CType(FeedTurnAroundPoint.Parent, FeedTurnAroundPointList).Where(Function(d) d.TurnAroundPointOrder = FeedTurnAroundPoint.TurnAroundPointOrder And d IsNot FeedTurnAroundPoint).ToList
        If ftp.Count > 0 Then
          ErrorMsg = "Order must be unique"
        End If
      End If
      Return ErrorMsg
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedTurnAroundPoint() method.

    End Sub

    Public Shared Function NewFeedTurnAroundPoint() As FeedTurnAroundPoint

      Return DataPortal.CreateChild(Of FeedTurnAroundPoint)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFeedTurnAroundPoint(dr As SafeDataReader) As FeedTurnAroundPoint

      Dim f As New FeedTurnAroundPoint()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedTurnAroundPointIDProperty, .GetInt32(0))
          LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(TurnAroundPointIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          'LoadProperty(AudioConfigurationTemplateIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          'LoadProperty(VideoConfigurationTemplateIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
          'LoadProperty(AudioConfigurationTemplateNameProperty, .GetString(9))
          'LoadProperty(VideoConfigurationTemplateNameProperty, .GetString(10))
          LoadProperty(TurnAroundPointProperty, .GetString(7))
          LoadProperty(TurnAroundPointOrderProperty, .GetInt32(8))
          LoadProperty(CommentsProperty, .GetString(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insFeedTurnAroundPoint"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFeedTurnAroundPoint"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramFeedTurnAroundPointID As SqlParameter = .Parameters.Add("@FeedTurnAroundPointID", SqlDbType.Int)
          paramFeedTurnAroundPointID.Value = GetProperty(FeedTurnAroundPointIDProperty)
          If Me.IsNew Then
            paramFeedTurnAroundPointID.Direction = ParameterDirection.Output
          End If
          'GetProperty(FeedIDProperty)
          SetProperty(FeedIDProperty, Me.GetParent.FeedID)
          .Parameters.AddWithValue("@FeedID", NothingDBNull(Me.GetParent.FeedID))
          .Parameters.AddWithValue("@TurnAroundPointID", NothingDBNull(GetProperty(TurnAroundPointIDProperty)))
          '.Parameters.AddWithValue("@AudioConfigurationTemplateID", Singular.Misc.NothingDBNull(Nothing)) 'GetProperty(AudioConfigurationTemplateIDProperty)
          '.Parameters.AddWithValue("@VideoConfigurationTemplateID", Singular.Misc.NothingDBNull(Nothing)) 'GetProperty(VideoConfigurationTemplateIDProperty)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@TurnAroundPointOrder", GetProperty(TurnAroundPointOrderProperty))
          .Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(FeedTurnAroundPointIDProperty, paramFeedTurnAroundPointID.Value)
          End If
          ' update child objects
          UpdateChild(GetProperty(FeedTurnAroundPointContactListProperty))
          'If GetProperty(FeedTurnAroundPointContactListProperty) IsNot Nothing Then
          '  FeedTurnAroundPointContactList.Save()
          'End If
          'If GetProperty(FeedTurnAroundPointAudioSettingListProperty) IsNot Nothing Then
          '  FeedTurnAroundPointAudioSettingList.Update()
          'End If
          'If GetProperty(FeedTurnAroundPointVideoSettingListProperty) IsNot Nothing Then
          '  FeedTurnAroundPointVideoSettingList.Update()
          'End If
          MarkOld()
        End With
      Else
        UpdateChild(GetProperty(FeedTurnAroundPointContactListProperty))
        'If GetProperty(FeedTurnAroundPointContactListProperty) IsNot Nothing Then
        '  FeedTurnAroundPointContactList.Save()
        'End If
        'If GetProperty(FeedTurnAroundPointAudioSettingListProperty) IsNot Nothing Then
        '  FeedTurnAroundPointAudioSettingList.Update()
        'End If
        'If GetProperty(FeedTurnAroundPointVideoSettingListProperty) IsNot Nothing Then
        '  FeedTurnAroundPointVideoSettingList.Update()
        'End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delFeedTurnAroundPoint"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@FeedTurnAroundPointID", GetProperty(FeedTurnAroundPointIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub
      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace