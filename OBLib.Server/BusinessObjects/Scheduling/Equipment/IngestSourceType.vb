﻿' Generated 27 Aug 2015 03:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace SatOps

  <Serializable()> _
  Public Class IngestSourceType
    Inherits SingularBusinessBase(Of IngestSourceType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IngestSourceTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.IngestSourceTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property IngestSourceTypeID() As Integer
      Get
        Return GetProperty(IngestSourceTypeIDProperty)
      End Get
    End Property

    Public Shared IngestSourceTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IngestSourceType, "Ingest Source Type", "")
    ''' <summary>
    ''' Gets and sets the Ingest Source Type value
    ''' </summary>
    <Display(Name:="Ingest Source Type", Description:=""),
    StringLength(150, ErrorMessage:="Ingest Source Type cannot be more than 150 characters")>
  Public Property IngestSourceType() As String
      Get
        Return GetProperty(IngestSourceTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IngestSourceTypeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(IngestSourceTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.IngestSourceType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Ingest Source Type")
        Else
          Return String.Format("Blank {0}", "Ingest Source Type")
        End If
      Else
        Return Me.IngestSourceType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewIngestSourceType() method.

    End Sub

    Public Shared Function NewIngestSourceType() As IngestSourceType

      Return DataPortal.CreateChild(Of IngestSourceType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetIngestSourceType(dr As SafeDataReader) As IngestSourceType

      Dim i As New IngestSourceType()
      i.Fetch(dr)
      Return i

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(IngestSourceTypeIDProperty, .GetInt32(0))
          LoadProperty(IngestSourceTypeProperty, .GetString(1))
          LoadProperty(CreatedByProperty, .GetInt32(2))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
          LoadProperty(ModifiedByProperty, .GetInt32(4))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insIngestSourceType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updIngestSourceType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramIngestSourceTypeID As SqlParameter = .Parameters.Add("@IngestSourceTypeID", SqlDbType.Int)
          paramIngestSourceTypeID.Value = GetProperty(IngestSourceTypeIDProperty)
          If Me.IsNew Then
            paramIngestSourceTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@IngestSourceType", GetProperty(IngestSourceTypeProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(IngestSourceTypeIDProperty, paramIngestSourceTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delIngestSourceType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@IngestSourceTypeID", GetProperty(IngestSourceTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace