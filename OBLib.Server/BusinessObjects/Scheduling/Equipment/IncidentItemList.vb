﻿' Generated 25 Nov 2016 06:52 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Scheduling.Equipment

  <Serializable()> _
  Public Class IncidentItemList
    Inherits OBBusinessListBase(Of IncidentItemList, IncidentItem)

#Region " Business Methods "

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property FeedID As Integer?

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewIncidentItemList() As IncidentItemList

      Return New IncidentItemList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetIncidentItemList() As IncidentItemList

      Return DataPortal.Fetch(Of IncidentItemList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(IncidentItem.GetIncidentItem(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getIncidentItemList"
            cm.Parameters.AddWithValue("@FeedID", NothingDBNull(crit.FeedID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Public Sub UpdateIncidents(BookingItemList As List(Of OBLib.SSWSD.FeedItem))

      'do stuff
      'Dim incidentsListFeed As List(Of OBLib.SSWSD.IncidentItem) = BookingItemList.SelectMany(Function(d As OBLib.SSWSD.FeedItem) d._IncidentItemList).ToList
      'Dim incidentsListFeedProduction As List(Of OBLib.SSWSD.IncidentItem) = BookingItemList.SelectMany(Function(d As OBLib.SSWSD.FeedItem) d._FeedProdItems.SelectMany(Function(c As OBLib.SSWSD.FeedProdItem) c._IncidentItemList).ToList).ToList
      'Dim allItems As New List(Of OBLib.SSWSD.IncidentItem)
      'allItems.AddRange(incidentsListFeed.AsEnumerable)
      'allItems.AddRange(incidentsListFeedProduction.AsEnumerable)


      Dim dt As New DataTable
      dt.Columns.Add("IncidentItemID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("Active", GetType(System.Boolean)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("Created", GetType(DateTime)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("CreatorDetails", GetType(System.String)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("CreatorID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("DateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("Deleted", GetType(System.Boolean)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("Guid", GetType(System.String)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("ImportedID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("Incident", GetType(System.String)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("ParentID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("ParentType", GetType(System.String)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("Samrand", GetType(System.Boolean)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("FeedID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

      For Each booking As OBLib.SSWSD.FeedItem In BookingItemList

        For Each inc As OBLib.SSWSD.IncidentItem In booking._IncidentItemList
          Dim ni As IncidentItem = Me.AddNew
          ni.Active = inc._Active
          ni.Created = inc._Created
          ni.CreatorDetails = inc._CreatorDetails.mf_name & " " & inc._CreatorDetails.mf_surname
          ni.CreatorID = inc._CreatorID
          ni.DateTime = inc._DateTime
          ni.Deleted = inc._Deleted
          ni.ImportedGuid = inc._GUID
          ni.ImportedID = inc._ID
          ni.Incident = inc._Incident
          ni.ParentID = inc._ParentID
          ni.ParentType = inc._ParentType
          ni.Samrand = inc._Samrand
          ni.FeedID = booking._BookingSourceID
          dt.Rows.Add(New Object() {ni.IncidentItemID, ni.Active, ni.Created, ni.CreatorDetails, ni.CreatorID, ni.DateTime, ni.Deleted, ni.ImportedGuid, ni.ImportedID, ni.Incident, ni.ParentID, ni.ParentType, ni.Samrand, ni.FeedID})
        Next

        For Each bookingProduction As OBLib.SSWSD.FeedProdItem In booking._FeedProdItems
          For Each inc As OBLib.SSWSD.IncidentItem In bookingProduction._IncidentItemList
            Dim ni As IncidentItem = Me.AddNew
            ni.Active = inc._Active
            ni.Created = inc._Created
            ni.CreatorDetails = inc._CreatorDetails.mf_name & " " & inc._CreatorDetails.mf_surname
            ni.CreatorID = inc._CreatorID
            ni.DateTime = inc._DateTime
            ni.Deleted = inc._Deleted
            ni.ImportedGuid = inc._GUID
            ni.ImportedID = inc._ID
            ni.Incident = inc._Incident
            ni.ParentID = inc._ParentID
            ni.ParentType = inc._ParentType
            ni.Samrand = inc._Samrand
            ni.FeedID = bookingProduction._FeedID
            dt.Rows.Add(New Object() {ni.IncidentItemID, ni.Active, ni.Created, ni.CreatorDetails, ni.CreatorID, ni.DateTime, ni.Deleted, ni.ImportedGuid, ni.ImportedID, ni.Incident, ni.ParentID, ni.ParentType, ni.Samrand, ni.FeedID})
          Next
        Next

      Next

      Dim cmdProc As New Singular.CommandProc("InsProcsWeb.insIncidentItems")
      cmdProc.AddTableParameter("@IncidentItems", dt)
      cmdProc.UseTransaction = True
      cmdProc = cmdProc.Execute
      Dim a As Object = Nothing

    End Sub

#End Region

  End Class

End Namespace