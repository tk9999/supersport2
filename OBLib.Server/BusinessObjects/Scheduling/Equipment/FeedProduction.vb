﻿' Generated 15 Jun 2015 14:30 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.SatOps.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.Strings
Imports Singular.DataAnnotations

Namespace SatOps

  <Serializable()> _
  Public Class FeedProduction
    Inherits OBBusinessBase(Of FeedProduction)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsExpanded, "Expanded", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Expanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsExpandedProperty, value)
      End Set
    End Property

    Public Shared FeedProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedProductionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public ReadOnly Property FeedProductionID() As Integer
      Get
        Return GetProperty(FeedProductionIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedID, "Feed", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Feed", Description:="")>
    Public Property FeedID() As Integer?
      Get
        Return GetProperty(FeedIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:=""),
    Required(ErrorMessage:="Production required")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SynergyGenRefNoProperty As PropertyInfo(Of Int64?) = RegisterSProperty(Of Int64?)(Function(c) c.SynergyGenRefNo, Nothing) _
                                                                       .AddSetExpression("FeedProductionBO.SynergyGenRefNoSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Synergy Gen Ref No value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:=""),
    Required(ErrorMessage:="Gen Ref required")>
    Public Property SynergyGenRefNo() As Int64?
      Get
        Return GetProperty(SynergyGenRefNoProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(SynergyGenRefNoProperty, Value)
      End Set
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Description", "")
    ''' <summary>
    ''' Gets and sets the Feed Description value
    ''' </summary>
    <Display(Name:="Description", Description:="Brief description of the production")>
    Public Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionDescriptionProperty, Value)
      End Set
    End Property

    Public Shared UpdateFeedTAPsAndDestinationsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UpdateFeedTAPsAndDestinations, "Description", False)
    ''' <summary>
    ''' Gets and sets the Feed Description value
    ''' </summary>
    <Display(Name:="Description")>
    Public Property UpdateFeedTAPsAndDestinations() As Boolean
      Get
        Return GetProperty(UpdateFeedTAPsAndDestinationsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(UpdateFeedTAPsAndDestinationsProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Venue", "")
    ''' <summary>
    ''' Gets and sets the Feed Description value
    ''' </summary>
    <Display(Name:="Venue")>
    Public Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionVenueProperty, Value)
      End Set
    End Property

    Public Shared KickOffTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.KickOffTime, Nothing)
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Kick-Off Time")>
    Public ReadOnly Property KickOffTime() As DateTime?
      Get
        Return GetProperty(KickOffTimeProperty)
      End Get
    End Property

    <Display(Name:="Time")>
    Public ReadOnly Property KickOffTimeString() As String
      Get
        If KickOffTime IsNot Nothing Then
          Return KickOffTime.Value.ToString("dd MMM yy HH:mm")
        Else
          Return ""
        End If
      End Get
    End Property

    'Public Shared RoomProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Room, "")
    '<Display(Name:="Room")>
    'Public ReadOnly Property Room() As String
    '  Get
    '    Return GetProperty(RoomProperty)
    '  End Get
    'End Property

    Public Shared IngestIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IngestInd, "Ingest", False)
    ''' <summary>
    ''' Gets and sets the Ingest value
    ''' </summary>
    <Display(Name:="Ingest", Description:="Must this feed be ingested?")>
    Public Property IngestInd() As Boolean
      Get
        Return GetProperty(IngestIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IngestIndProperty, Value)
      End Set
    End Property

    Public Shared IngestSourceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IngestSourceTypeID, "Ingest Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed Type value
    ''' </summary>
    <Display(Name:="Ingest Type"),
    DropDownWeb(GetType(ROIngestSourceTypeList), DropDownType:=DropDownWeb.SelectType.Combo)>
    Public Property IngestSourceTypeID() As Integer?
      Get
        Return GetProperty(IngestSourceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(IngestSourceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared IngestSourceRoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IngestSourceRoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed Type value
    ''' </summary>
    <Display(Name:="Room"),
    DropDownWeb(GetType(RORoomList), DropDownType:=DropDownWeb.SelectType.Combo)>
    Public Property IngestSourceRoomID() As Integer?
      Get
        Return GetProperty(IngestSourceRoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(IngestSourceRoomIDProperty, Value)
      End Set
    End Property

    Public Shared IngestSourceDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IngestSourceDescription, "Ingest Description", "")
    ''' <summary>
    ''' Gets and sets the Ingest value
    ''' </summary>
    <Display(Name:="Ingest Description")>
    Public Property IngestSourceDescription() As String
      Get
        Return GetProperty(IngestSourceDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IngestSourceDescriptionProperty, Value)
      End Set
    End Property

    <Display(Name:="Audio Summary")>
    Public ReadOnly Property AudioSettingsSummary As String
      Get
        Return "Test 123"
      End Get
    End Property

    Public Shared IsSamrandFeedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsSamrandFeed, "Samrand Feed?", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Samrand Feed?")>
    Public Property IsSamrandFeed() As Boolean
      Get
        Return GetProperty(IsSamrandFeedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsSamrandFeedProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    'Public Shared AudioGroupListProperty As PropertyInfo(Of AudioGroupList) = RegisterProperty(Of AudioGroupList)(Function(c) c.AudioGroupList, "Audio Group List")
    'Public ReadOnly Property AudioGroupList() As AudioGroupList
    '  Get
    '    If GetProperty(AudioGroupListProperty) Is Nothing Then
    '      LoadProperty(AudioGroupListProperty, SatOps.AudioGroupList.NewAudioGroupList())
    '    End If
    '    Return GetProperty(AudioGroupListProperty)
    '  End Get
    'End Property

    Public Shared FeedProductionAudioSettingListProperty As PropertyInfo(Of FeedProductionAudioSettingList) = RegisterProperty(Of FeedProductionAudioSettingList)(Function(c) c.FeedProductionAudioSettingList, "Feed Production Audio Setting List")
    Public ReadOnly Property FeedProductionAudioSettingList() As FeedProductionAudioSettingList
      Get
        If GetProperty(FeedProductionAudioSettingListProperty) Is Nothing Then
          LoadProperty(FeedProductionAudioSettingListProperty, SatOps.FeedProductionAudioSettingList.NewFeedProductionAudioSettingList())
        End If
        Return GetProperty(FeedProductionAudioSettingListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feed Production")
        Else
          Return String.Format("Blank {0}", "Feed Production")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Public Function GetParent() As EquipmentFeed

      Return CType(CType(Me.Parent, FeedProductionList).Parent, EquipmentFeed)

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"FeedProductionAudioSettings"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(IngestIndProperty)
        .ServerRuleFunction = AddressOf IngestIndValid
        .JavascriptRuleFunctionName = "FeedBO.IngestIndValid"
        .AddTriggerProperty(IngestSourceDescriptionProperty)
        .AddTriggerProperty(IngestSourceRoomIDProperty)
        .AddTriggerProperty(IngestSourceTypeIDProperty)
        .AffectedProperties.Add(IngestSourceDescriptionProperty)
        .AffectedProperties.Add(IngestSourceRoomIDProperty)
        .AffectedProperties.Add(IngestSourceTypeIDProperty)
      End With

    End Sub

    Public Shared Function IngestIndValid(ByVal FeedProduction As FeedProduction)

      Dim Err As String = ""
      'If FeedProduction.IngestInd Then
      '  If IsNullNothing(FeedProduction.IngestSourceTypeID) Then
      '    Err &= "Ingest Source Type is required"
      '  Else
      '    If FeedProduction.IngestSourceTypeID = CType(OBLib.CommonData.Enums.IngestSourceType.IncomingFeed, Integer) Then
      '      If FeedProduction.IngestSourceDescription.Trim.Length = 0 Then
      '        'Err &= "Ingest Source Description is required"
      '      End If
      '    ElseIf FeedProduction.IngestSourceTypeID = CType(OBLib.CommonData.Enums.IngestSourceType.Gallery, Integer) Then
      '      If FeedProduction.IngestSourceRoomID Is Nothing Then
      '        Err &= If(Err.Trim.Length > 0, ",", "") & "Ingest Source Room is required"
      '      Else
      '        Dim ROROom As RORoom = OBLib.CommonData.Lists.RORoomList.GetItem(FeedProduction.IngestSourceRoomID)
      '        If ROROom IsNot Nothing Then
      '          If ROROom.RoomTypeID <> CType(OBLib.CommonData.Enums.RoomType.Gallery, Integer) Then
      '            Err &= If(Err.Trim.Length > 0, ",", "") & "Ingest Source Room must be a gallery"
      '          End If
      '        End If
      '      End If
      '    ElseIf FeedProduction.IngestSourceTypeID = CType(OBLib.CommonData.Enums.IngestSourceType.Studio, Integer) Then
      '      If FeedProduction.IngestSourceRoomID Is Nothing Then
      '        Err &= If(Err.Trim.Length > 0, ",", "") & "Ingest Source Room is required"
      '      Else
      '        Dim ROROom As RORoom = OBLib.CommonData.Lists.RORoomList.GetItem(FeedProduction.IngestSourceRoomID)
      '        If ROROom IsNot Nothing Then
      '          If ROROom.RoomTypeID <> CType(OBLib.CommonData.Enums.RoomType.Studio, Integer) Then
      '            Err &= If(Err.Trim.Length > 0, ",", "") & "Ingest Source Room must be a studio"
      '          End If
      '        End If
      '      End If
      '    ElseIf FeedProduction.IngestSourceTypeID = CType(OBLib.CommonData.Enums.IngestSourceType.Other, Integer) Then
      '      If FeedProduction.IngestSourceDescription.Trim.Length = 0 Then
      '        Err &= If(Err.Trim.Length > 0, ",", "") & "Ingest Source Description is required"
      '      End If
      '    End If
      '  End If
      'End If

      Return Err

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedProduction() method.

    End Sub

    Public Shared Function NewFeedProduction() As FeedProduction

      Return DataPortal.CreateChild(Of FeedProduction)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFeedProduction(dr As SafeDataReader) As FeedProduction

      Dim f As New FeedProduction()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedProductionIDProperty, .GetInt32(0))
          LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(SynergyGenRefNoProperty, Singular.Misc.ZeroNothing(.GetInt64(7)))
          LoadProperty(ProductionDescriptionProperty, .GetString(8))
          LoadProperty(ProductionVenueProperty, .GetString(9))
          LoadProperty(KickOffTimeProperty, .GetValue(10))
          LoadProperty(IngestIndProperty, .GetBoolean(11))
          LoadProperty(IngestSourceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(IngestSourceRoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(IngestSourceDescriptionProperty, .GetString(14))
          'LoadProperty(RoomProperty, .GetString(15))
          LoadProperty(IsSamrandFeedProperty, .GetBoolean(16))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insFeedProduction"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFeedProduction"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramFeedProductionID As SqlParameter = .Parameters.Add("@FeedProductionID", SqlDbType.Int)
          paramFeedProductionID.Value = GetProperty(FeedProductionIDProperty)
          If Me.IsNew Then
            paramFeedProductionID.Direction = ParameterDirection.Output
          End If
          ' GetProperty(FeedIDProperty)
          .Parameters.AddWithValue("@FeedID", Me.GetParent.FeedID)
          .Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", CurrentUser.UserID)
          .Parameters.AddWithValue("@SynergyGenRefNo", NothingDBNull(GetProperty(SynergyGenRefNoProperty)))
          .Parameters.AddWithValue("@IngestInd", GetProperty(IngestIndProperty))
          .Parameters.AddWithValue("@IngestSourceTypeID", NothingDBNull(GetProperty(IngestSourceTypeIDProperty)))
          .Parameters.AddWithValue("@IngestSourceRoomID", NothingDBNull(GetProperty(IngestSourceRoomIDProperty)))
          .Parameters.AddWithValue("@IngestSourceDescription", GetProperty(IngestSourceDescriptionProperty))
          .Parameters.AddWithValue("@IsSamrandFeed", GetProperty(IsSamrandFeedProperty))
          '.Parameters.AddWithValue("@UpdateFeedTAPsAndDestinations", GetProperty(UpdateFeedTAPsAndDestinationsProperty))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(FeedProductionIDProperty, paramFeedProductionID.Value)
          End If
          ' update child objects
          If GetProperty(FeedProductionAudioSettingListProperty) IsNot Nothing Then
            FeedProductionAudioSettingList.Update()
          End If
          MarkOld()
        End With
      Else
        If GetProperty(FeedProductionAudioSettingListProperty) IsNot Nothing Then
          FeedProductionAudioSettingList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delFeedProduction"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@FeedProductionID", GetProperty(FeedProductionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Sub ClearGenRef()

    End Sub

    Sub PopulateFromGenRef()

      'Dim iel As OBLib.Synergy.ReadOnly.ROSynergyEventList = OBLib.Synergy.ReadOnly.ROSynergyEventList.GetROSynergyEventList(Me.SynergyGenRefNo)
      'If iel.Count = 0 Then
      '  'Can't find gen ref in SOBER, do import
      '  OBLib.Synergy.Importer.[New].SynergyImporter.ImportData(Me.SynergyGenRefNo)
      '  iel = OBLib.Synergy.ReadOnly.ROSynergyEventList.GetROSynergyEventList(Me.SynergyGenRefNo)
      'End If

      'If iel.Count = 0 Then
      '  Throw New Exception("Gen Ref: " & Me.SynergyGenRefNo.ToString & " could not be found")
      'ElseIf iel.Count >= 1 Then
      '  Dim ProductionList As OBLib.Productions.Base.ProductionDetailBaseList = OBLib.Productions.Base.ProductionDetailBaseList.GetProductionDetailBaseList(Me.SynergyGenRefNo)
      '  If ProductionList.Count = 0 Then
      '    OBLib.Synergy.Importer.[New].SynergyImporter.CreateProduction(CurrentUser.UserID, Me.SynergyGenRefNo, Nothing)
      '    ProductionList = OBLib.Productions.Base.ProductionDetailBaseList.GetProductionDetailBaseList(Me.SynergyGenRefNo)
      '  End If
      '  If ProductionList.Count = 1 Then
      '    Me.ProductionID = ProductionList(0).ProductionID
      '    Me.ProductionDescription = ProductionList(0).Title
      '  ElseIf ProductionList.Count = 0 Then
      '    Throw New Exception("Failed to import production: " & Me.SynergyGenRefNo.ToString & " could not be found")
      '  Else
      '    Dim prefs As String = ""
      '    ProductionList.Select(Function(d) d.ProductionRefNo).ToList.ForEach(Sub(d)
      '                                                                          prefs &= d & ", "
      '                                                                        End Sub)
      '    Throw New Exception("Duplicate Gen Ref: " & vbCrLf _
      '                        & Me.SynergyGenRefNo.ToString & " exists on multiple productions, this is not allowed" & vbCrLf _
      '                        & "The production ref nums are: " & prefs)
      '  End If
      'End If

    End Sub

  End Class

End Namespace