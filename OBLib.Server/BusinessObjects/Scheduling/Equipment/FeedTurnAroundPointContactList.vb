﻿' Generated 08 Jun 2016 22:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace SatOps

  <Serializable()> _
  Public Class FeedTurnAroundPointContactList
    Inherits OBBusinessListBase(Of FeedTurnAroundPointContactList, FeedTurnAroundPointContact)

#Region " Business Methods "

    Public Function GetItem(FeedTurnAroundPointContactID As Integer) As FeedTurnAroundPointContact

      For Each child As FeedTurnAroundPointContact In Me
        If child.FeedTurnAroundPointContactID = FeedTurnAroundPointContactID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Feed Turn Around Point Contacts"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewFeedTurnAroundPointContactList() As FeedTurnAroundPointContactList

      Return New FeedTurnAroundPointContactList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetFeedTurnAroundPointContactList() As FeedTurnAroundPointContactList

      Return DataPortal.Fetch(Of FeedTurnAroundPointContactList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(FeedTurnAroundPointContact.GetFeedTurnAroundPointContact(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getFeedTurnAroundPointContactList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace