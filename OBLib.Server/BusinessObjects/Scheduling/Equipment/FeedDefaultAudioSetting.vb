﻿' Generated 05 Oct 2015 10:03 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SatOps.ReadOnly

Namespace SatOps

  <Serializable()> _
  Public Class FeedDefaultAudioSetting
    Inherits SingularBusinessBase(Of FeedDefaultAudioSetting)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedDefaultAudioSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedDefaultAudioSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property FeedDefaultAudioSettingID() As Integer
      Get
        Return GetProperty(FeedDefaultAudioSettingIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedID, "Feed", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Feed", Description:="")>
    Public Property FeedID() As Integer?
      Get
        Return GetProperty(FeedIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedIDProperty, Value)
      End Set
    End Property

    Public Shared GroupNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GroupNumber, "Group Number", 0)
    ''' <summary>
    ''' Gets and sets the Group Number value
    ''' </summary>
    <Display(Name:="Group Number", Description:="")>
    Public Property GroupNumber() As Integer
      Get
        Return GetProperty(GroupNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(GroupNumberProperty, Value)
      End Set
    End Property

    Public Shared PairNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PairNumber, "Pair Number", 0)
    ''' <summary>
    ''' Gets and sets the Pair Number value
    ''' </summary>
    <Display(Name:="Pair Number", Description:="")>
    Public Property PairNumber() As Integer
      Get
        Return GetProperty(PairNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PairNumberProperty, Value)
      End Set
    End Property

    Public Shared ChannelNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelNumber, "Channel", 0)
    ''' <summary>
    ''' Gets and sets the Channel Number value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public Property ChannelNumber() As Integer
      Get
        Return GetProperty(ChannelNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ChannelNumberProperty, Value)
      End Set
    End Property

    Public Shared AudioSettingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AudioSettingID, "Audio Setting", Nothing)
    ''' <summary>
    ''' Gets and sets the Audio Setting value
    ''' </summary>
    <Display(Name:="Audio Setting"),
    DropDownWeb(GetType(ROAudioSettingList), Source:=DropDownWeb.SourceType.CommonData)>
    Public Property AudioSettingID() As Integer?
      Get
        Return GetProperty(AudioSettingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AudioSettingIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedDefaultAudioSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.FeedDefaultAudioSettingID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feed Default Audio Setting")
        Else
          Return String.Format("Blank {0}", "Feed Default Audio Setting")
        End If
      Else
        Return Me.FeedDefaultAudioSettingID.ToString()
      End If

    End Function

    Public Function GetParent() As EquipmentFeed

      Return CType(CType(Me.Parent, FeedDefaultAudioSettingList).Parent, EquipmentFeed)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      'With AddWebRule(GroupNumberProperty)
      '  .JavascriptRuleFunctionName = "FeedDefaultAudioSettingBO.GroupNumberValid"
      '  .ServerRuleFunction = AddressOf GroupNumberValid
      'End With

      'With AddWebRule(PairNumberProperty)
      '  .JavascriptRuleFunctionName = "FeedDefaultAudioSettingBO.PairNumberValid"
      '  .ServerRuleFunction = AddressOf PairNumberValid
      'End With

      'With AddWebRule(ChannelNumberProperty)
      '  .JavascriptRuleFunctionName = "FeedDefaultAudioSettingBO.ChannelNumberValid"
      '  .ServerRuleFunction = AddressOf ChannelNumberValid
      'End With

      'With AddWebRule(FeedIDProperty)
      '  .JavascriptRuleFunctionName = "FeedDefaultAudioSettingBO.FeedAudioRowValid"
      '  .ServerRuleFunction = AddressOf FeedAudioRowValid
      '  .AddTriggerProperty(GroupNumberProperty)
      '  .AddTriggerProperty(PairNumberProperty)
      '  .AddTriggerProperty(ChannelNumberProperty)
      '  .AffectedProperties.Add(GroupNumberProperty)
      '  .AffectedProperties.Add(PairNumberProperty)
      '  .AffectedProperties.Add(ChannelNumberProperty)
      'End With

    End Sub

    Public Shared Function GroupNumberValid(FeedDefaultAudioSetting As FeedDefaultAudioSetting) As String

      Dim ErrorString As String = ""
      If FeedDefaultAudioSetting.GroupNumber <= 0 Then
        ErrorString = "Group Number must be above 0"
      End If
      Return ErrorString

    End Function

    Public Shared Function PairNumberValid(FeedDefaultAudioSetting As FeedDefaultAudioSetting) As String

      Dim ErrorString As String = ""
      If FeedDefaultAudioSetting.PairNumber <= 0 Then
        ErrorString = "Pair Number must be above 0"
      End If
      Return ErrorString

    End Function

    Public Shared Function ChannelNumberValid(FeedDefaultAudioSetting As FeedDefaultAudioSetting) As String

      Dim ErrorString As String = ""
      If FeedDefaultAudioSetting.ChannelNumber <= 0 Then
        ErrorString = "Channel Number must be above 0"
      End If
      Return ErrorString

    End Function

    'Public Shared Function FeedAudioRowValid(FeedDefaultAudioSetting As FeedDefaultAudioSetting) As String

    '  Dim ErrorString As String = ""
    '  Dim DuplicateGroupNumbers As Integer = FeedDefaultAudioSetting.GetParent.FeedDefaultAudioSettingList.Where(Function(d) d.GroupNumber = FeedDefaultAudioSetting.GroupNumber _
    '                                                                                                              AndAlso d.PairNumber = FeedDefaultAudioSetting.PairNumber _
    '                                                                                                              AndAlso d.ChannelNumber = FeedDefaultAudioSetting.ChannelNumber _
    '                                                                                                              AndAlso d.Guid <> FeedDefaultAudioSetting.Guid).Count
    '  If DuplicateGroupNumbers > 0 Then
    '    Return "This comibination of group, pair and channel numbers is already being used"
    '  End If
    '  Return ErrorString

    'End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedDefaultAudioSetting() method.

    End Sub

    Public Shared Function NewFeedDefaultAudioSetting() As FeedDefaultAudioSetting

      Return DataPortal.CreateChild(Of FeedDefaultAudioSetting)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFeedDefaultAudioSetting(dr As SafeDataReader) As FeedDefaultAudioSetting

      Dim f As New FeedDefaultAudioSetting()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedDefaultAudioSettingIDProperty, .GetInt32(0))
          LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(GroupNumberProperty, .GetInt32(2))
          LoadProperty(PairNumberProperty, .GetInt32(3))
          LoadProperty(ChannelNumberProperty, .GetInt32(4))
          LoadProperty(AudioSettingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insFeedDefaultAudioSetting"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFeedDefaultAudioSetting"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramFeedDefaultAudioSettingID As SqlParameter = .Parameters.Add("@FeedDefaultAudioSettingID", SqlDbType.Int)
          paramFeedDefaultAudioSettingID.Value = GetProperty(FeedDefaultAudioSettingIDProperty)
          If Me.IsNew Then
            paramFeedDefaultAudioSettingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@FeedID", NothingDBNull(Me.GetParent.FeedID))
          .Parameters.AddWithValue("@GroupNumber", GetProperty(GroupNumberProperty))
          .Parameters.AddWithValue("@PairNumber", GetProperty(PairNumberProperty))
          .Parameters.AddWithValue("@ChannelNumber", GetProperty(ChannelNumberProperty))
          .Parameters.AddWithValue("@AudioSettingID", NothingDBNull(GetProperty(AudioSettingIDProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(FeedDefaultAudioSettingIDProperty, paramFeedDefaultAudioSettingID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delFeedDefaultAudioSetting"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@FeedDefaultAudioSettingID", GetProperty(FeedDefaultAudioSettingIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace