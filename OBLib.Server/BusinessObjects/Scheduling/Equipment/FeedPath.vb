﻿' Generated 05 May 2015 17:43 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace SatOps

  <Serializable()> _
  Public Class FeedPath
    Inherits SingularBusinessBase(Of FeedPath)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedPathIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedPathID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property FeedPathID() As Integer
      Get
        Return GetProperty(FeedPathIDProperty)
      End Get
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedID, "Feed", Nothing)
    ''' <summary>
    ''' Gets the Feed value
    ''' </summary>
    <Display(Name:="Feed", Description:="")>
    Public ReadOnly Property FeedID() As Integer?
      Get
        Return GetProperty(FeedIDProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room", "")
    ''' <summary>
    ''' Gets the Path Comments value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(value As String)
        SetProperty(RoomProperty, value)
      End Set
    End Property

    Public Shared PathCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PathComments, "Path Comments", "")
    ''' <summary>
    ''' Gets the Path Comments value
    ''' </summary>
    <Display(Name:="Path Comments", Description:="''"),
    StringLength(500, ErrorMessage:="Path Comments cannot be more than 500 characters")>
    Public Property PathComments() As String
      Get
        Return GetProperty(PathCommentsProperty)
      End Get
      Set(value As String)
        SetProperty(PathCommentsProperty, value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByString, "Created By", "")
    ''' <summary>
    ''' Gets the Path Comments value
    ''' </summary>
    <Display(Name:="Created By")>
    Public ReadOnly Property CreatedByString() As String
      Get
        Return GetProperty(CreatedByStringProperty)
      End Get
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "RoomScheduleID", Nothing)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="RoomScheduleID")>
    Public Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(RoomScheduleIDProperty, value)
      End Set
    End Property

    Public Shared ProductionTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTitle, "Production Title", "")
    ''' <summary>
    ''' Gets the Path Comments value
    ''' </summary>
    <Display(Name:="Production Title")>
    Public ReadOnly Property ProductionTitle() As String
      Get
        Return GetProperty(ProductionTitleProperty)
      End Get
    End Property

    Public Shared RoomScheduleTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomScheduleTitle, "Room Booking", "")
    ''' <summary>
    ''' Gets the Path Comments value
    ''' </summary>
    <Display(Name:="Room Booking")>
    Public ReadOnly Property RoomScheduleTitle() As String
      Get
        Return GetProperty(RoomScheduleTitleProperty)
      End Get
    End Property

    Public ReadOnly Property CreationDetails As String
      Get
        Return Me.CreatedByString & Me.CreatedDateTime.Value.ToString("ddd dd MMM yy HH:mm")
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedPathIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.PathComments.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feed Path")
        Else
          Return String.Format("Blank {0}", "Feed Path")
        End If
      Else
        Return Me.PathComments
      End If

    End Function

    Public Function GetParent() As EquipmentFeed

      Return CType(CType(Me.Parent, FeedPathList).Parent, EquipmentFeed)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedPath() method.

    End Sub

    Public Shared Function NewFeedPath() As FeedPath

      Return DataPortal.CreateChild(Of FeedPath)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFeedPath(dr As SafeDataReader) As FeedPath

      Dim f As New FeedPath()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedPathIDProperty, .GetInt32(0))
          LoadProperty(FeedIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          'LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(PathCommentsProperty, .GetString(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetValue(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetValue(6))
          LoadProperty(RoomProperty, .GetString(7))
          LoadProperty(CreatedByStringProperty, .GetString(8))
          'LoadProperty(ProductionIDProperty, ZeroNothing(.GetInt32(10)))
          LoadProperty(RoomScheduleIDProperty, ZeroNothing(.GetInt32(9)))
          LoadProperty(ProductionTitleProperty, .GetString(10))
          LoadProperty(RoomScheduleTitleProperty, .GetString(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insFeedPath"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFeedPath"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramFeedPathID As SqlParameter = .Parameters.Add("@FeedPathID", SqlDbType.Int)
          paramFeedPathID.Value = GetProperty(FeedPathIDProperty)
          If Me.IsNew Then
            paramFeedPathID.Direction = ParameterDirection.Output
          End If
          ' GetProperty(FeedIDProperty)
          .Parameters.AddWithValue("@FeedID", Me.GetParent.FeedID)
          '.Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
          .Parameters.AddWithValue("@PathComments", GetProperty(PathCommentsProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          '.Parameters.AddWithValue("@ProductionID", NothingDBNull(Me.ProductionID))
          .Parameters.AddWithValue("@RoomScheduleID", NothingDBNull(Me.RoomScheduleID))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(FeedPathIDProperty, paramFeedPathID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delFeedPath"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@FeedPathID", GetProperty(FeedPathIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub
      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace