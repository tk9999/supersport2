﻿' Generated 05 Oct 2015 13:38 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SatOps.ReadOnly

Namespace SatOps

  <Serializable()> _
  Public Class FeedProductionAudioSetting
    Inherits SingularBusinessBase(Of FeedProductionAudioSetting)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FeedProductionAudioSettingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedProductionAudioSettingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FeedProductionAudioSettingID() As Integer
      Get
        Return GetProperty(FeedProductionAudioSettingIDProperty)
      End Get
    End Property

    Public Shared FeedProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedProductionID, "Feed Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Feed Production value
    ''' </summary>
    <Display(Name:="Feed Production", Description:="")>
    Public Property FeedProductionID() As Integer?
      Get
        Return GetProperty(FeedProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedProductionIDProperty, Value)
      End Set
    End Property

    Public Shared GroupNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GroupNumber, "Group Number", 0)
    ''' <summary>
    ''' Gets and sets the Group Number value
    ''' </summary>
    <Display(Name:="Group Number", Description:="")>
    Public Property GroupNumber() As Integer
      Get
        Return GetProperty(GroupNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(GroupNumberProperty, Value)
      End Set
    End Property

    Public Shared PairNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PairNumber, "Pair Number", 0)
    ''' <summary>
    ''' Gets and sets the Pair Number value
    ''' </summary>
    <Display(Name:="Pair Number", Description:="")>
    Public Property PairNumber() As Integer
      Get
        Return GetProperty(PairNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PairNumberProperty, Value)
      End Set
    End Property

    Public Shared ChannelNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelNumber, "Channel", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel Number value
    ''' </summary>
    <Display(Name:="Channel"), Required(ErrorMessage:="Channel is required")>
    Public Property ChannelNumber() As Integer?
      Get
        Return GetProperty(ChannelNumberProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ChannelNumberProperty, Value)
      End Set
    End Property

    Public Shared AudioSettingIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.AudioSettingID, Nothing)
    ''' <summary>
    ''' Gets and sets the Audio Setting value
    ''' </summary>
    <Display(Name:="Audio Setting"), Required(ErrorMessage:="Audio Setting is required"),
    DropDownWeb(GetType(ROAudioSettingList), Source:=DropDownWeb.SourceType.CommonData)>
    Public Property AudioSettingID() As Integer?
      Get
        Return GetProperty(AudioSettingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AudioSettingIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FeedProductionAudioSettingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.FeedProductionAudioSettingID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Feed Production Audio Setting")
        Else
          Return String.Format("Blank {0}", "Feed Production Audio Setting")
        End If
      Else
        Return Me.FeedProductionAudioSettingID.ToString()
      End If

    End Function

    Public Function GetParent() As FeedProduction

      Return CType(CType(Me.Parent, FeedProductionAudioSettingList).Parent, FeedProduction)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      'With AddWebRule(GroupNumberProperty)
      '  .JavascriptRuleFunctionName = "FeedProductionAudioSettingBO.GroupNumberValid"
      '  .ServerRuleFunction = AddressOf GroupNumberValid
      'End With

      'With AddWebRule(PairNumberProperty)
      '  .JavascriptRuleFunctionName = "FeedProductionAudioSettingBO.PairNumberValid"
      '  .ServerRuleFunction = AddressOf PairNumberValid
      'End With

      'With AddWebRule(ChannelNumberProperty)
      '  .JavascriptRuleFunctionName = "FeedProductionAudioSettingBO.ChannelNumberValid"
      '  .ServerRuleFunction = AddressOf ChannelNumberValid
      'End With

      'With AddWebRule(FeedProductionIDProperty)
      '  .JavascriptRuleFunctionName = "FeedProductionAudioSettingBO.FeedAudioRowValid"
      '  .ServerRuleFunction = AddressOf FeedAudioRowValid
      '  .AddTriggerProperty(GroupNumberProperty)
      '  .AddTriggerProperty(PairNumberProperty)
      '  .AddTriggerProperty(ChannelNumberProperty)
      '  .AffectedProperties.Add(GroupNumberProperty)
      '  .AffectedProperties.Add(PairNumberProperty)
      '  .AffectedProperties.Add(ChannelNumberProperty)
      'End With

    End Sub

    Public Shared Function GroupNumberValid(FeedProductionAudioSetting As FeedProductionAudioSetting) As String

      Dim ErrorString As String = ""
      If FeedProductionAudioSetting.GroupNumber <= 0 Then
        ErrorString = "Group Number must be above 0"
      End If
      Return ErrorString

    End Function

    Public Shared Function PairNumberValid(FeedProductionAudioSetting As FeedProductionAudioSetting) As String

      Dim ErrorString As String = ""
      If FeedProductionAudioSetting.PairNumber <= 0 Then
        ErrorString = "Pair Number must be above 0"
      End If
      Return ErrorString

    End Function

    Public Shared Function ChannelNumberValid(FeedProductionAudioSetting As FeedProductionAudioSetting) As String

      Dim ErrorString As String = ""
      If FeedProductionAudioSetting.ChannelNumber <= 0 Then
        ErrorString = "Channel Number must be above 0"
      End If
      Return ErrorString

    End Function

    Public Shared Function FeedAudioRowValid(FeedProductionAudioSetting As FeedProductionAudioSetting) As String

      Dim ErrorString As String = ""
      Dim DuplicateGroupNumbers As Integer = FeedProductionAudioSetting.GetParent.FeedProductionAudioSettingList.Where(Function(d) d.GroupNumber = FeedProductionAudioSetting.GroupNumber _
                                                                                                                  AndAlso d.PairNumber = FeedProductionAudioSetting.PairNumber _
                                                                                                                  AndAlso d.ChannelNumber = FeedProductionAudioSetting.ChannelNumber _
                                                                                                                  AndAlso d.Guid <> FeedProductionAudioSetting.Guid).Count
      If DuplicateGroupNumbers > 0 Then
        Return "This comibination of group, pair and channel numbers is already being used"
      End If
      Return ErrorString

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFeedProductionAudioSetting() method.

    End Sub

    Public Shared Function NewFeedProductionAudioSetting() As FeedProductionAudioSetting

      Return DataPortal.CreateChild(Of FeedProductionAudioSetting)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFeedProductionAudioSetting(dr As SafeDataReader) As FeedProductionAudioSetting

      Dim f As New FeedProductionAudioSetting()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(FeedProductionAudioSettingIDProperty, .GetInt32(0))
          LoadProperty(FeedProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(GroupNumberProperty, .GetInt32(2))
          LoadProperty(PairNumberProperty, .GetInt32(3))
          LoadProperty(ChannelNumberProperty, .GetInt32(4))
          LoadProperty(AudioSettingIDProperty, .GetInt32(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insFeedProductionAudioSetting"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updFeedProductionAudioSetting"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramFeedProductionAudioSettingID As SqlParameter = .Parameters.Add("@FeedProductionAudioSettingID", SqlDbType.Int)
          paramFeedProductionAudioSettingID.Value = GetProperty(FeedProductionAudioSettingIDProperty)
          If Me.IsNew Then
            paramFeedProductionAudioSettingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@FeedProductionID", NothingDBNull(Me.GetParent.FeedProductionID))
          .Parameters.AddWithValue("@GroupNumber", GetProperty(GroupNumberProperty))
          .Parameters.AddWithValue("@PairNumber", GetProperty(PairNumberProperty))
          .Parameters.AddWithValue("@ChannelNumber", GetProperty(ChannelNumberProperty))
          .Parameters.AddWithValue("@AudioSettingID", GetProperty(AudioSettingIDProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(FeedProductionAudioSettingIDProperty, paramFeedProductionAudioSettingID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delFeedProductionAudioSetting"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@FeedProductionAudioSettingID", GetProperty(FeedProductionAudioSettingIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace