﻿' Generated 26 Mar 2015 18:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Resources.Equipment

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Productions.Base

Namespace SatOps

  <Serializable()> _
  Public Class EquipmentFeedList
    Inherits OBBusinessListBase(Of EquipmentFeedList, EquipmentFeed)

#Region " Business Methods "

    Public Function GetItemByEquipmentScheduleID(EquipmentScheduleID As Integer) As EquipmentFeed

      For Each child As EquipmentFeed In Me
        If child.EquipmentScheduleID = EquipmentScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByFeedID(FeedID As Integer) As EquipmentFeed

      For Each child As EquipmentFeed In Me
        If child.FeedID = FeedID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetFeedProduction(FeedProductionID As Integer) As FeedProduction
      For Each child As EquipmentFeed In Me
        For Each ag As FeedProduction In child.FeedProductionList
          If ag.FeedProductionID = FeedProductionID Then
            Return ag
          End If
        Next
      Next
      Return Nothing
    End Function

    Public Function GetFeedTurnAroundPoint(FeedTurnAroundPointID As Integer) As FeedTurnAroundPoint
      For Each child As EquipmentFeed In Me
        For Each ag As FeedTurnAroundPoint In child.FeedTurnAroundPointList
          If ag.FeedTurnAroundPointID = FeedTurnAroundPointID Then
            Return ag
          End If
        Next
      Next
      Return Nothing
    End Function

    Public Overrides Function ToString() As String

      Return "Equipment Schedules"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Singular.SingularCriteriaBase(Of Criteria)

      Public Property EquipmentScheduleID As Integer? = Nothing
      Public Property ProductionSystemAreaID As Integer? = Nothing

      Public Sub New(EquipmentScheduleID As Integer?, ProductionSystemAreaID As Integer?)
        Me.EquipmentScheduleID = EquipmentScheduleID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    'Public Shared Function NewEquipmentFeedList() As EquipmentFeedList

    '  Return New EquipmentFeedList()

    'End Function

    'Public Shared Sub BeginGetEquipmentFeedList(CallBack As EventHandler(Of DataPortalResult(Of EquipmentFeedList)))

    '  Dim dp As New DataPortal(Of EquipmentFeedList)()
    '  AddHandler dp.FetchCompleted, CallBack
    '  dp.BeginFetch(New Criteria())

    'End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    'Public Shared Function GetEquipmentFeedList() As EquipmentFeedList

    '  Return DataPortal.Fetch(Of EquipmentFeedList)(New Criteria())

    'End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(EquipmentFeed.GetEquipmentFeed(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'Feed Productions
      Dim parent As EquipmentFeed = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.FeedID <> sdr.GetInt32(1) Then
            parent = Me.GetItemByFeedID(sdr.GetInt32(1))
          End If
          parent.FeedProductionList.RaiseListChangedEvents = False
          parent.FeedProductionList.Add(FeedProduction.GetFeedProduction(sdr))
          parent.FeedProductionList.RaiseListChangedEvents = True
        End While
      End If

      'Feed Production Audio Settings
      Dim parentFeedProduction As FeedProduction = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentFeedProduction Is Nothing OrElse parent.FeedID <> sdr.GetInt32(1) Then
            parentFeedProduction = Me.GetFeedProduction(sdr.GetInt32(1))
          End If
          parentFeedProduction.FeedProductionAudioSettingList.RaiseListChangedEvents = False
          parentFeedProduction.FeedProductionAudioSettingList.Add(FeedProductionAudioSetting.GetFeedProductionAudioSetting(sdr))
          parentFeedProduction.FeedProductionAudioSettingList.RaiseListChangedEvents = True
        End While
      End If

      'Turnaround Points
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.FeedID <> sdr.GetInt32(1) Then
            parent = Me.GetItemByFeedID(sdr.GetInt32(1))
          End If
          parent.FeedTurnAroundPointList.RaiseListChangedEvents = False
          parent.FeedTurnAroundPointList.Add(FeedTurnAroundPoint.GetFeedTurnAroundPoint(sdr))
          parent.FeedTurnAroundPointList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentTAP As FeedTurnAroundPoint = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentTAP Is Nothing OrElse parentTAP.FeedTurnAroundPointID <> sdr.GetInt32(1) Then
            parentTAP = Me.GetFeedTurnAroundPoint(sdr.GetInt32(1))
          End If
          parentTAP.FeedTurnAroundPointContactList.RaiseListChangedEvents = False
          parentTAP.FeedTurnAroundPointContactList.Add(FeedTurnAroundPointContact.GetFeedTurnAroundPointContact(sdr))
          parentTAP.FeedTurnAroundPointContactList.RaiseListChangedEvents = True
        End While
      End If

      'Paths
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.FeedID <> sdr.GetInt32(1) Then
            parent = Me.GetItemByFeedID(sdr.GetInt32(1))
          End If
          parent.FeedPathList.RaiseListChangedEvents = False
          parent.FeedPathList.Add(FeedPath.GetFeedPath(sdr))
          parent.FeedPathList.RaiseListChangedEvents = True
        End While
      End If

      'Feed Default Audio Settings
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.FeedID <> sdr.GetInt32(1) Then
            parent = Me.GetItemByFeedID(sdr.GetInt32(1))
          End If
          parent.FeedDefaultAudioSettingList.RaiseListChangedEvents = False
          parent.FeedDefaultAudioSettingList.Add(FeedDefaultAudioSetting.GetFeedDefaultAudioSetting(sdr))
          parent.FeedDefaultAudioSettingList.RaiseListChangedEvents = True
        End While
      End If

      'Feed Default Audio Settings Select List
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.FeedID <> sdr.GetInt32(4) Then
            parent = Me.GetItemByFeedID(sdr.GetInt32(4))
          End If
          parent.FeedDefaultAudioSettingSelectList.RaiseListChangedEvents = False
          parent.FeedDefaultAudioSettingSelectList.Add(FeedDefaultAudioSettingSelect.GetFeedDefaultAudioSettingSelect(sdr))
          parent.FeedDefaultAudioSettingSelectList.RaiseListChangedEvents = True
        End While
      End If

      'Feed Ingest Instructions
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.FeedID <> sdr.GetInt32(1) Then
            parent = Me.GetItemByFeedID(sdr.GetInt32(1))
          End If
          parent.FeedIngestInstructionList.RaiseListChangedEvents = False
          parent.FeedIngestInstructionList.Add(FeedIngestInstruction.GetFeedIngestInstruction(sdr))
          parent.FeedIngestInstructionList.RaiseListChangedEvents = True
        End While
      End If

      For Each Feed As EquipmentFeed In Me
        Feed.CheckRules()
        For Each ftp As FeedTurnAroundPoint In Feed.FeedTurnAroundPointList
          ftp.CheckRules()
          For Each ftpc As FeedTurnAroundPointContact In ftp.FeedTurnAroundPointContactList
            ftpc.CheckRules()
          Next
        Next
        For Each fa As FeedDefaultAudioSettingSelect In Feed.FeedDefaultAudioSettingSelectList
          fa.CheckRules()
        Next
        For Each fp As FeedPath In Feed.FeedPathList
          fp.CheckRules()
        Next
        For Each fp As FeedProduction In Feed.FeedProductionList
          fp.CheckRules()
          For Each fa As FeedProductionAudioSetting In fp.FeedProductionAudioSettingList
            fa.CheckRules()
          Next
        Next
        For Each fp As FeedIngestInstruction In Feed.FeedIngestInstructionList
          fp.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getEquipmentFeedList"
            cm.Parameters.AddWithValue("@EquipmentScheduleID", Singular.Misc.NothingDBNull(crit.EquipmentScheduleID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(crit.ProductionSystemAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As EquipmentFeed In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As EquipmentFeed In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace