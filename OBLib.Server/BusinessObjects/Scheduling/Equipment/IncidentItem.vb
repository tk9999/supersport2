﻿' Generated 25 Nov 2016 06:52 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Scheduling.Equipment

  <Serializable()> _
  Public Class IncidentItem
    Inherits OBBusinessBase(Of IncidentItem)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return IncidentItemBO.IncidentItemBOToString(self)")

    Public Shared IncidentItemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.IncidentItemID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property IncidentItemID() As Integer
      Get
        Return GetProperty(IncidentItemIDProperty)
      End Get
    End Property

    Public Shared ActiveProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Active, "Active", False)
    ''' <summary>
    ''' Gets and sets the Active value
    ''' </summary>
    <Display(Name:="Active", Description:=""),
    Required(ErrorMessage:="Active required")>
    Public Property Active() As Boolean
      Get
        Return GetProperty(ActiveProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ActiveProperty, Value)
      End Set
    End Property

    Public Shared CreatedProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.Created, "Created")
    ''' <summary>
    ''' Gets and sets the Created value
    ''' </summary>
    <Display(Name:="Created", Description:=""),
    Required(ErrorMessage:="Created required")>
    Public Property Created As Date
      Get
        Return GetProperty(CreatedProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(CreatedProperty, Value)
      End Set
    End Property

    Public Shared CreatorDetailsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatorDetails, "Creator Details", "")
    ''' <summary>
    ''' Gets and sets the Creator Details value
    ''' </summary>
    <Display(Name:="Creator Details", Description:=""),
    StringLength(200, ErrorMessage:="Creator Details cannot be more than 200 characters")>
    Public Property CreatorDetails() As String
      Get
        Return GetProperty(CreatorDetailsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CreatorDetailsProperty, Value)
      End Set
    End Property

    Public Shared CreatorIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatorID, "Creator", 0)
    ''' <summary>
    ''' Gets and sets the Creator value
    ''' </summary>
    <Display(Name:="Creator", Description:=""),
    Required(ErrorMessage:="Creator required")>
    Public Property CreatorID() As Integer
      Get
        Return GetProperty(CreatorIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CreatorIDProperty, Value)
      End Set
    End Property

    Public Shared DateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.DateTime, "Date Time")
    ''' <summary>
    ''' Gets and sets the Date Time value
    ''' </summary>
    <Display(Name:="Date Time", Description:=""),
    Required(ErrorMessage:="Date Time required")>
    Public Property DateTime As Date
      Get
        Return GetProperty(DateTimeProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(DateTimeProperty, Value)
      End Set
    End Property

    Public Shared DeletedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Deleted, "Deleted", False)
    ''' <summary>
    ''' Gets and sets the Deleted value
    ''' </summary>
    <Display(Name:="Deleted", Description:=""),
    Required(ErrorMessage:="Deleted required")>
    Public Property Deleted() As Boolean
      Get
        Return GetProperty(DeletedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DeletedProperty, Value)
      End Set
    End Property

    Public Shared ImportedGuidProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImportedGuid, "ImportedGuid", "")
    ''' <summary>
    ''' Gets and sets the Guid value
    ''' </summary>
    <Display(Name:="ImportedGuid", Description:=""),
    StringLength(100, ErrorMessage:="ImportedGuid cannot be more than 100 characters")>
    Public Property ImportedGuid() As String
      Get
        Return GetProperty(ImportedGuidProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ImportedGuidProperty, Value)
      End Set
    End Property

    Public Shared ImportedIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the Creator value
    ''' </summary>
    <Display(Name:="ImportedID", Description:=""),
    Required(ErrorMessage:="ImportedID required")>
    Public Property ImportedID() As Integer
      Get
        Return GetProperty(ImportedIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ImportedIDProperty, Value)
      End Set
    End Property

    Public Shared IncidentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Incident, "Incident", "")
    ''' <summary>
    ''' Gets and sets the Guid value
    ''' </summary>
    <Display(Name:="Incident", Description:=""),
    TextField(True, True, False, 10)>
    Public Property Incident() As String
      Get
        Return GetProperty(IncidentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IncidentProperty, Value)
      End Set
    End Property

    Public Shared ParentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ParentID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the Creator value
    ''' </summary>
    <Display(Name:="ParentID", Description:=""),
    Required(ErrorMessage:="ParentID required")>
    Public Property ParentID() As Integer
      Get
        Return GetProperty(ParentIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ParentIDProperty, Value)
      End Set
    End Property

    Public Shared ParentTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ParentType, "ParentType", "")
    ''' <summary>
    ''' Gets and sets the Guid value
    ''' </summary>
    <Display(Name:="ParentType", Description:="")>
    Public Property ParentType() As String
      Get
        Return GetProperty(ParentTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ParentTypeProperty, Value)
      End Set
    End Property

    Public Shared SamrandProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Samrand, "Samrand", False)
    ''' <summary>
    ''' Gets and sets the Samrand value
    ''' </summary>
    <Display(Name:="Samrand", Description:="")>
    Public Property Samrand() As Boolean
      Get
        Return GetProperty(SamrandProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SamrandProperty, Value)
      End Set
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.FeedID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the Creator value
    ''' </summary>
    <Display(Name:="ParentID", Description:="")>
    Public Property FeedID() As Integer
      Get
        Return GetProperty(FeedIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(FeedIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "


#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewIncidentItem() method.

    End Sub

    Public Shared Function NewIncidentItem() As IncidentItem

      Return DataPortal.CreateChild(Of IncidentItem)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetIncidentItem(dr As SafeDataReader) As IncidentItem

      Dim i As New IncidentItem()
      i.Fetch(dr)
      Return i

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(IncidentItemIDProperty, .GetInt32(0))
          LoadProperty(ActiveProperty, .GetBoolean(1))
          LoadProperty(CreatedProperty, .GetValue(2))
          LoadProperty(CreatorDetailsProperty, .GetString(3))
          LoadProperty(CreatorIDProperty, .GetInt32(4))
          LoadProperty(DateTimeProperty, .GetValue(5))
          LoadProperty(DeletedProperty, .GetBoolean(6))
          LoadProperty(ImportedGuidProperty, .GetString(7))
          LoadProperty(ImportedIDProperty, .GetInt32(8))
          LoadProperty(IncidentProperty, .GetString(9))
          LoadProperty(ParentIDProperty, .GetInt32(10))
          LoadProperty(ParentTypeProperty, .GetString(11))
          LoadProperty(SamrandProperty, .GetBoolean(12))
          LoadProperty(FeedIDProperty, .GetInt32(13))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, IncidentItemIDProperty)

      cm.Parameters.AddWithValue("@Active", GetProperty(ActiveProperty))
      cm.Parameters.AddWithValue("@Created", Created)
      cm.Parameters.AddWithValue("@CreatorDetails", GetProperty(CreatorDetailsProperty))
      cm.Parameters.AddWithValue("@CreatorID", GetProperty(CreatorIDProperty))
      cm.Parameters.AddWithValue("@DateTime", DateTime)
      cm.Parameters.AddWithValue("@Deleted", GetProperty(DeletedProperty))
      cm.Parameters.AddWithValue("@Guid", GetProperty(ImportedGuidProperty))
      cm.Parameters.AddWithValue("@ID", GetProperty(ImportedIDProperty))
      cm.Parameters.AddWithValue("@Incident", GetProperty(IncidentProperty))
      cm.Parameters.AddWithValue("ParentID", GetProperty(ParentIDProperty))
      cm.Parameters.AddWithValue("@ParentType", GetProperty(ParentTypeProperty))
      cm.Parameters.AddWithValue("@Samrand", GetProperty(SamrandProperty))
      cm.Parameters.AddWithValue("@FeedID", GetProperty(FeedIDProperty))


      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(IncidentItemIDProperty, cm.Parameters("@IncidentItemID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@IncidentItemID", GetProperty(IncidentItemIDProperty))
    End Sub

#End Region

  End Class

End Namespace