﻿' Generated 11 May 2017 09:05 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Scheduling.Generic.ReadOnly

  <Serializable()> _
  Public Class ROPSACrewGroupHRList
    Inherits OBReadOnlyListBase(Of ROPSACrewGroupHRList, ROPSACrewGroupHR)

#Region " Parent "

    <NotUndoable()> Private mParent As ROPSACrewGroup
#End Region

#Region " Business Methods "

    Public Function GetItem(PSACrewGroupID As Int64) As ROPSACrewGroupHR

      For Each child As ROPSACrewGroupHR In Me
        If child.PSACrewGroupID = PSACrewGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewROPSACrewGroupHRList() As ROPSACrewGroupHRList

      Return New ROPSACrewGroupHRList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace