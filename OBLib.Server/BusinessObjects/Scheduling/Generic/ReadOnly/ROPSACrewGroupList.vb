﻿' Generated 11 May 2017 09:05 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Scheduling.Generic.ReadOnly

  <Serializable()> _
  Public Class ROPSACrewGroupList
    Inherits OBReadOnlyListBase(Of ROPSACrewGroupList, ROPSACrewGroup)

#Region " Business Methods "

    Public Function GetItem(PSACrewGroupID As Int64) As ROPSACrewGroup

      For Each child As ROPSACrewGroup In Me
        If child.PSACrewGroupID = PSACrewGroupID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROPSACrewGroupHR(HumanResourceID As Integer) As ROPSACrewGroupHR

      Dim obj As ROPSACrewGroupHR = Nothing
      For Each parent As ROPSACrewGroup In Me
        obj = parent.ROPSACrewGroupHRList.GetItem(HumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionSystemAreaID As Integer? = Nothing
      Public Property CrewTypeID As Integer? = Nothing

      Public Sub New(ProductionSystemAreaID As Object, CrewTypeID As Object)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
        Me.CrewTypeID = CrewTypeID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROPSACrewGroupList() As ROPSACrewGroupList

      Return New ROPSACrewGroupList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROPSACrewGroupList() As ROPSACrewGroupList

      Return DataPortal.Fetch(Of ROPSACrewGroupList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPSACrewGroup.GetROPSACrewGroup(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROPSACrewGroup = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.PSACrewGroupID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.ROPSACrewGroupHRList.RaiseListChangedEvents = False
          parent.ROPSACrewGroupHRList.Add(ROPSACrewGroupHR.GetROPSACrewGroupHR(sdr))
          parent.ROPSACrewGroupHRList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionSystemAreaCrewTypeList"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@CrewTypeID", Singular.Misc.NothingDBNull(crit.CrewTypeID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace