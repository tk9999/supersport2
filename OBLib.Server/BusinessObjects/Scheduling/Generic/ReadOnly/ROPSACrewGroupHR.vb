﻿' Generated 11 May 2017 09:05 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Scheduling.Generic.ReadOnly

  <Serializable()> _
  Public Class ROPSACrewGroupHR
    Inherits OBReadOnlyBase(Of ROPSACrewGroupHR)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROPSACrewGroupHRBO.ROPSACrewGroupHRBOToString(self)")

    Public Shared PSACrewGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PSACrewGroupID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property PSACrewGroupID() As Integer
      Get
        Return GetProperty(PSACrewGroupIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRName, "HR Name")
    ''' <summary>
    ''' Gets the HR Name value
    ''' </summary>
    <Display(Name:="HR Name", Description:="")>
    Public ReadOnly Property HRName() As String
      Get
        Return GetProperty(HRNameProperty)
      End Get
    End Property

    Public Shared ImageFileNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImageFileName, "Image File Name")
    ''' <summary>
    ''' Gets the Image File Name value
    ''' </summary>
    <Display(Name:="Image File Name", Description:="")>
    Public ReadOnly Property ImageFileName() As String
      Get
        Return GetProperty(ImageFileNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HRName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROPSACrewGroupHR(dr As SafeDataReader) As ROPSACrewGroupHR

      Dim r As New ROPSACrewGroupHR()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(PSACrewGroupIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(HRNameProperty, .GetString(2))
        LoadProperty(ImageFileNameProperty, .GetString(3))
      End With

    End Sub

#End Region

  End Class

End Namespace