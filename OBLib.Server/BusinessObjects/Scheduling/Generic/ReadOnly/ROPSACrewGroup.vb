﻿' Generated 11 May 2017 09:05 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Scheduling.Generic.ReadOnly

  <Serializable()> _
  Public Class ROPSACrewGroup
    Inherits OBReadOnlyBase(Of ROPSACrewGroup)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROPSACrewGroupBO.ROPSACrewGroupBOToString(self)")

    Public Shared PSACrewGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PSACrewGroupID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property PSACrewGroupID() As Integer
      Get
        Return GetProperty(PSACrewGroupIDProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Production System Area")
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
    Public ReadOnly Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewTypeID, "Crew Type")
    ''' <summary>
    ''' Gets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="")>
    Public ReadOnly Property CrewTypeID() As Integer
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
    End Property

    Public Shared CrewTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CrewType, "Crew Type")
    ''' <summary>
    ''' Gets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="")>
    Public ReadOnly Property CrewType() As String
      Get
        Return GetProperty(CrewTypeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROPSACrewGroupHRListProperty As PropertyInfo(Of ROPSACrewGroupHRList) = RegisterProperty(Of ROPSACrewGroupHRList)(Function(c) c.ROPSACrewGroupHRList, "ROPSA Crew Group HR List")

    Public ReadOnly Property ROPSACrewGroupHRList() As ROPSACrewGroupHRList
      Get
        If GetProperty(ROPSACrewGroupHRListProperty) Is Nothing Then
          LoadProperty(ROPSACrewGroupHRListProperty, Scheduling.Generic.ReadOnly.ROPSACrewGroupHRList.NewROPSACrewGroupHRList())
        End If
        Return GetProperty(ROPSACrewGroupHRListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PSACrewGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CrewType

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROPSACrewGroup(dr As SafeDataReader) As ROPSACrewGroup

      Dim r As New ROPSACrewGroup()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(PSACrewGroupIDProperty, .GetInt32(0))
        LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CrewTypeProperty, .GetString(3))
      End With

    End Sub

#End Region

  End Class

End Namespace