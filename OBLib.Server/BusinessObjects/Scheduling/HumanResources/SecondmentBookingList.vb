﻿' Generated 13 Jun 2015 16:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.HR

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Resources

  <Serializable()> _
  Public Class SecondmentBookingList
    Inherits HRBookingList(Of SecondmentBookingList, SecondmentBooking)

#Region " Business Methods "

    Public Function CreateNew() As SecondmentBooking

      Dim tmp As New SecondmentBooking
      Me.Add(tmp)
      Return tmp

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Bookings"

    End Function

    Protected Overrides Function AddNewCore() As Object
      Return New SecondmentBooking
    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewSecondmentBookingList() As SecondmentBookingList

      Return New SecondmentBookingList()

    End Function

    Public Shared Sub BeginGetResourceBookingList(CallBack As EventHandler(Of DataPortalResult(Of SecondmentBookingList)))

      Dim dp As New DataPortal(Of SecondmentBookingList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Overrides Sub FetchChildLists(sdr As SafeDataReader)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace