﻿' Generated 13 Jun 2015 16:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Resources.ReadOnly
Imports OBLib.Productions.Base
Imports OBLib.HR

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Resources

  <Serializable>
  Public Class HRBooking(Of HRBookingType As HRBooking(Of HRBookingType))
    Inherits Resources.ResourceBookingBase(Of HRBookingType)

    Public Shared CrewScheduleDetailListProperty As PropertyInfo(Of CrewScheduleDetailList) = RegisterProperty(Of CrewScheduleDetailList)(Function(c) c.CrewScheduleDetailList, "Crew Schedule Detail List")
    Public ReadOnly Property CrewScheduleDetailList() As CrewScheduleDetailList
      Get
        If GetProperty(CrewScheduleDetailListProperty) Is Nothing Then
          LoadProperty(CrewScheduleDetailListProperty, New CrewScheduleDetailList)
        End If
        Return GetProperty(CrewScheduleDetailListProperty)
      End Get
    End Property

    Public Overrides Sub AddExtraParameters(Parameters As SqlClient.SqlParameterCollection)

    End Sub

    Public Overrides Sub DeleteChildren()
      'If removed server side
      For Each csd As CrewScheduleDetail In Me.CrewScheduleDetailList
        csd.DeleteSelf()
      Next
      'If removed clientside
      For Each csd As CrewScheduleDetail In Me.CrewScheduleDetailList.GetDeletedList
        csd.DeleteSelf()
      Next
    End Sub

    Public Overrides Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)

    End Sub

    Public Overrides Sub UpdateChildList()
      Me.CrewScheduleDetailList.Update()
    End Sub

  End Class

End Namespace
