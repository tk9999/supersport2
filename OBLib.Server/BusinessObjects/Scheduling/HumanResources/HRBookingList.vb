﻿' Generated 13 Jun 2015 16:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Resources.ReadOnly
Imports OBLib.Productions.Base
Imports OBLib.HR

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Resources

  <Serializable>
  Public Class HRBookingList(Of HRBookingListType As HRBookingList(Of HRBookingListType, HRBookingType), HRBookingType As HRBooking(Of HRBookingType))
    Inherits Resources.ResourceBookingBaseList(Of HRBookingListType, HRBookingType)

    Public Overrides Sub FetchChildLists(sdr As SafeDataReader)

    End Sub

  End Class

End Namespace
