﻿' Generated 13 Jun 2015 16:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Resources.ReadOnly
Imports OBLib.Productions.Base
Imports OBLib.HR

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Resources

  <Serializable()> _
  Public Class OffPeriodBooking
    Inherits HRBooking(Of OffPeriodBooking)

#Region " Properties and Methods "

#Region " Properties "

#End Region

    'Public Shared CrewScheduleDetailListProperty As PropertyInfo(Of CrewScheduleDetailList) = RegisterProperty(Of CrewScheduleDetailList)(Function(c) c.CrewScheduleDetailList, "Crew Schedule Detail List")
    'Public ReadOnly Property CrewScheduleDetailList() As CrewScheduleDetailList
    '  Get
    '    If GetProperty(CrewScheduleDetailListProperty) Is Nothing Then
    '      LoadProperty(CrewScheduleDetailListProperty, New CrewScheduleDetailList)
    '    End If
    '    Return GetProperty(CrewScheduleDetailListProperty)
    '  End Get
    'End Property


#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceBookingDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Resource Booking")
        Else
          Return String.Format("Blank {0}", "Resource Booking")
        End If
      Else
        Return Me.ResourceBookingDescription
      End If

    End Function

    Public Function GetParent() As HumanResourceOffPeriod
      Return CType(Me.GetParentList.Parent, HumanResourceOffPeriod)
    End Function

    Public Function GetParentList() As OffPeriodBookingList
      Return CType(Me.Parent, OffPeriodBookingList)
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()
    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceBooking() method.

    End Sub

    Public Shared Function NewResourceBooking() As OffPeriodBooking

      Return DataPortal.CreateChild(Of OffPeriodBooking)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetOffPeriodBooking(dr As SafeDataReader) As OffPeriodBooking

      Dim r As New OffPeriodBooking()
      r.Fetch(dr)
      Return r

    End Function

    'Friend Overrides Sub Insert()

    '  ' if we're not dirty then don't update the database
    '  Using cm As SqlCommand = New SqlCommand
    '    cm.CommandText = "InsProcsWeb.insHRLeaveBooking"
    '    DoInsertUpdateChild(cm)
    '  End Using

    'End Sub

    'Friend Overrides Sub Update()

    '  ' if we're not dirty then don't update the database
    '  Using cm As SqlCommand = New SqlCommand
    '    cm.CommandText = "UpdProcsWeb.updHRLeaveBooking"
    '    DoInsertUpdateChild(cm)
    '  End Using

    'End Sub

    'Friend Overrides Sub Insert()

    '  ' if we're not dirty then don't update the database
    '  Using cm As SqlCommand = New SqlCommand
    '    cm.CommandText = "InsProcsWeb.insHRResourceBooking"
    '    DoInsertUpdateChild(cm)
    '  End Using

    'End Sub

    'Friend Overrides Sub Update()

    '  ' if we're not dirty then don't update the database
    '  Using cm As SqlCommand = New SqlCommand
    '    cm.CommandText = "UpdProcsWeb.updHRResourceBooking"
    '    DoInsertUpdateChild(cm)
    '  End Using

    'End Sub

    'Protected Overrides Sub InsertUpdate(cm As SqlCommand)

    '  If Me.IsSelfDirty Then

    '    With cm
    '      .CommandType = CommandType.StoredProcedure

    '      Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
    '      paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
    '      If Me.IsNew Then
    '        paramResourceBookingID.Direction = ParameterDirection.Output
    '      End If
    '      .Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
    '      .Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
    '      .Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
    '      .Parameters.AddWithValue("@StartDateTimeBuffer", (New SmartDate(GetProperty(StartDateTimeBufferProperty))).DBValue)
    '      .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
    '      .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
    '      .Parameters.AddWithValue("@EndDateTimeBuffer", (New SmartDate(GetProperty(EndDateTimeBufferProperty))).DBValue)
    '      .Parameters.AddWithValue("@HumanResourceID", NothingDBNull(GetProperty(HumanResourceIDProperty)))
    '      .Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
    '      .Parameters.AddWithValue("@VehicleID", NothingDBNull(GetProperty(VehicleIDProperty)))
    '      .Parameters.AddWithValue("@EquipmentID", NothingDBNull(GetProperty(EquipmentIDProperty)))
    '      .Parameters.AddWithValue("@ChannelID", NothingDBNull(GetProperty(ChannelIDProperty)))
    '      .Parameters.AddWithValue("ModifiedBy", CurrentUser.UserID)
    '      .Parameters.AddWithValue("@IsCancelled", GetProperty(IsCancelledProperty))
    '      '.Parameters.AddWithValue("@SetupFeedInd", GetProperty(SetupFeedIndProperty))
    '      '.Parameters.AddWithValue("@SetupProductionSystemAreaInd", GetProperty(SetupFeedIndProperty))
    '      '.Parameters.AddWithValue("@SystemID", NothingDBNull(GetProperty(SystemIDProperty)))
    '      '.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))
    '      '.Parameters.AddWithValue("@RoomScheduleTypeID", NothingDBNull(GetProperty(RoomScheduleTypeIDProperty)))
    '      '.Parameters.AddWithValue("@ProductionAreaStatusID", NothingDBNull(GetProperty(ProductionAreaStatusIDProperty)))
    '      .ExecuteNonQuery()

    '      If Me.IsNew Then
    '        LoadProperty(ResourceBookingIDProperty, paramResourceBookingID.Value)
    '      End If
    '      ' update child objects
    '      If GetProperty(HROffPeriodListProperty) IsNot Nothing Then
    '        Me.HROffPeriodList.Update()
    '      End If
    '      MarkOld()
    '    End With
    '  Else
    '    If GetProperty(HROffPeriodListProperty) IsNot Nothing Then
    '      Me.HROffPeriodList.Update()
    '    End If
    '  End If

    'End Sub

    'Friend Sub DeleteSelf()

    '  ' if we're not dirty then don't update the database
    '  If Me.IsNew Then Exit Sub

    '  Using cm As SqlCommand = New SqlCommand
    '    cm.CommandText = "DelProcsWeb.delHRResourceBooking"
    '    cm.CommandType = CommandType.StoredProcedure
    '    cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
    '    cm.Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
    '    DoDeleteChild(cm)
    '  End Using

    'End Sub

    'Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

    '  If Me.IsNew Then Exit Sub

    '  cm.ExecuteNonQuery()
    '  MarkNew()

    'End Sub

#End Region

#Region " Overrides "

    Public Overrides Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)

    End Sub

    Public Overrides Sub BeforeInsertUpdate()
      Me.HumanResourceOffPeriodID = Me.GetParent.HumanResourceOffPeriodID
      Me.HumanResourceID = Me.GetParent.HumanResourceID
    End Sub

    Public Overrides Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)

    End Sub

    Public Overrides Sub UpdateChildList()
      For Each csd As CrewScheduleDetail In Me.CrewScheduleDetailList
        csd.HumanResourceOffPeriodID = Me.HumanResourceOffPeriodID
        csd.ResourceBookingID = Me.ResourceBookingID
      Next
      CrewScheduleDetailList.Update()
    End Sub

    Public Overrides Sub DeleteChildren()
      'If removed server side
      For Each csd As CrewScheduleDetail In Me.CrewScheduleDetailList
        csd.DeleteSelf()
      Next
      'If removed clientside
      For Each csd As CrewScheduleDetail In Me.CrewScheduleDetailList.GetDeletedList
        csd.DeleteSelf()
      Next
    End Sub

#End Region

#End Region

  End Class

End Namespace