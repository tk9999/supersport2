﻿' Generated 05 Jan 2015 09:38 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Vehicles.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.OutsideBroadcast.ReadOnly

Namespace OutsideBroadcast

  <Serializable()>
  Public Class ProductionHROBSimple
    Inherits OBBusinessBase(Of ProductionHROBSimple)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    <Display(Name:="Expanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHRID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property ProductionHRID() As Integer
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ProductionHRIDProperty, value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ResourceBookingIDProperty, value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:=""),
    Required(ErrorMessage:="Production System Area is required")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human ResourceID is required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.HumanResource, "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Human Resource is required")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="ResourceID", Description:=""),
    Required(ErrorMessage:="Resource is required")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Booking Description", "")
    ''' <summary>
    ''' Gets and sets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Booking Description", Description:=""),
    StringLength(500, ErrorMessage:="Booking Description cannot be more than 500 characters"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Booking Description is required")>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    DateField(FormatString:="ddd dd MMM yy HH:mm"),
    Required(ErrorMessage:="Start Time required"),
    SetExpression("ProductionHROBSimpleBO.StartDateTimeSet(self)")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    DateField(FormatString:="ddd dd MMM yy HH:mm"),
    Required(ErrorMessage:="End Date Time required"),
    SetExpression("ProductionHROBSimpleBO.EndDateTimeSet(self)")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared NewProductionHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.NewProductionHumanResourceID, Nothing)
    Public Property NewProductionHumanResourceID() As Integer?
      Get
        Return GetProperty(NewProductionHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NewProductionHumanResourceIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionHROBSimpleDetailListProperty As PropertyInfo(Of ProductionHROBSimpleDetailList) = RegisterProperty(Of ProductionHROBSimpleDetailList)(Function(c) c.ProductionHROBSimpleDetailList, "Production HROB Day Detail List")

    Public ReadOnly Property ProductionHROBSimpleDetailList() As ProductionHROBSimpleDetailList
      Get
        If GetProperty(ProductionHROBSimpleDetailListProperty) Is Nothing Then
          LoadProperty(ProductionHROBSimpleDetailListProperty, OutsideBroadcast.ProductionHROBSimpleDetailList.NewProductionHROBSimpleDetailList())
        End If
        Return GetProperty(ProductionHROBSimpleDetailListProperty)
      End Get
    End Property

    Public Shared ClashListProperty As PropertyInfo(Of List(Of Helpers.ResourceHelpers.ClashDetail)) = RegisterProperty(Of List(Of Helpers.ResourceHelpers.ClashDetail))(Function(c) c.ClashList, "ClashList")
    Public ReadOnly Property ClashList As List(Of Helpers.ResourceHelpers.ClashDetail)
      Get
        If GetProperty(ClashListProperty) Is Nothing Then
          LoadProperty(ClashListProperty, New List(Of Helpers.ResourceHelpers.ClashDetail))
        End If
        Return GetProperty(ClashListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceBookingID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production HROB Day")
        Else
          Return String.Format("Blank {0}", "Production HROB Day")
        End If
      Else
        Return Me.ResourceBookingID.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(ClashListProperty)
        .AddTriggerProperties({StartDateTimeProperty, EndDateTimeProperty})
        .AffectedProperties.AddRange({StartDateTimeProperty, EndDateTimeProperty})
        .JavascriptRuleFunctionName = "ProductionHROBSimpleBO.ClashListValid"
        .ServerRuleFunction = AddressOf ClashListValid
      End With

    End Sub

    Public Shared Function ClashListValid(ProductionHROBSimple As ProductionHROBSimple) As String

      Dim ErrorDescription As String = ""
      If ProductionHROBSimple.ClashList.Count > 0 Then
        ErrorDescription = "There are clashes with this booking"
      End If
      Return ErrorDescription

    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionHROBSimple() method.

    End Sub

    Public Shared Function NewProductionHROBSimple() As ProductionHROBSimple

      Return DataPortal.CreateChild(Of ProductionHROBSimple)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetProductionHROBSimple(dr As SafeDataReader) As ProductionHROBSimple

      Dim p As New ProductionHROBSimple()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionHRIDProperty, .GetInt32(0))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(HumanResourceProperty, .GetString(3))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ResourceBookingIDProperty, sdr.GetInt32(5))
          LoadProperty(ResourceBookingDescriptionProperty, sdr.GetString(6))
          LoadProperty(StartDateTimeProperty, sdr.GetValue(7))
          LoadProperty(EndDateTimeProperty, sdr.GetValue(8))
        End With
      End Using

      MarkAsChild()
      If ResourceBookingID = 0 Or ProductionHRID = 0 Then
        MarkNew()
      Else
        MarkOld()
      End If
      MarkDirty()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionHRIDProperty)
      AddOutputParam(cm, ResourceBookingIDProperty)

      cm.Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))
      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@HumanResource", GetProperty(HumanResourceProperty))
      cm.Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
      cm.Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
      cm.Parameters.AddWithValue("@StartDateTime", GetProperty(StartDateTimeProperty))
      cm.Parameters.AddWithValue("@EndDateTime", GetProperty(EndDateTimeProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@NewProductionHumanResourceID", NothingDBNull(GetProperty(NewProductionHumanResourceIDProperty)))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionHRIDProperty, cm.Parameters("@ProductionHRID").Value)
                 LoadProperty(ResourceBookingIDProperty, cm.Parameters("@ResourceBookingID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(ProductionHROBSimpleDetailListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      'cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
    End Sub

#End Region

  End Class

End Namespace
