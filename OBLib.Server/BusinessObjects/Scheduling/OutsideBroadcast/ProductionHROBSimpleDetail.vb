﻿' Generated 28 Jul 2016 14:27 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace OutsideBroadcast

  <Serializable()>
  Public Class ProductionHROBSimpleDetail
    Inherits OBBusinessBase(Of ProductionHROBSimpleDetail)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ProductionHROBSimpleDetailBO.ProductionHROBSimpleDetailBOToString(self)")

    Public Shared CrewScheduleDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewScheduleDetailID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property CrewScheduleDetailID() As Integer
      Get
        Return GetProperty(CrewScheduleDetailIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CrewScheduleDetailIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionHumanResourceID, "Production Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Human Resource value
    ''' </summary>
    <Display(Name:="Production Human Resource", Description:=""),
    Required(ErrorMessage:="Production Human Resource required")>
    Public Property ProductionHumanResourceID() As Integer?
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTimelineID, "Production Timeline", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Timeline value
    ''' </summary>
    <Display(Name:="Production Timeline", Description:=""),
    Required(ErrorMessage:="Production Timeline required")>
    Public Property ProductionTimelineID() As Integer?
      Get
        Return GetProperty(ProductionTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTimelineTypeID, "Timeline Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Timeline Type", Description:=""),
    Required(ErrorMessage:="Timeline Type required")>
    Public Property ProductionTimelineTypeID() As Integer?
      Get
        Return GetProperty(ProductionTimelineTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTimelineTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTimelineType, "Production Timeline Type")
    ''' <summary>
    ''' Gets and sets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Timeline Type", Description:="")>
    Public Property ProductionTimelineType() As String
      Get
        Return GetProperty(ProductionTimelineTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTimelineTypeProperty, Value)
      End Set
    End Property

    Public Shared TimesheetDateProperty As PropertyInfo(Of Date?) = RegisterSProperty(Of Date?)(Function(c) c.TimesheetDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Timesheet Date value
    ''' </summary>
    <Display(Name:="Date", Description:=""),
    Required(ErrorMessage:="Date required")>
    Public Property TimesheetDate As Date?
      Get
        Return GetProperty(TimesheetDateProperty)
      End Get
      Set(ByVal Value As Date?)
        SetProperty(TimesheetDateProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
    TimeField(TimeFormat:=TimeFormats.ShortTime),
    SetExpression("ProductionHROBSimpleDetailBO.StartDateTimeSet(self)")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
    TimeField(TimeFormat:=TimeFormats.ShortTime),
    SetExpression("ProductionHROBSimpleDetailBO.EndDateTimeSet(self)")>
    Public Property EndDateTime As Date
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human ResourceID is required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionHROBSimple

      Return CType(CType(Me.Parent, ProductionHROBSimpleDetailList).Parent, ProductionHROBSimple)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewScheduleDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionTimelineType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production HROB Day Detail")
        Else
          Return String.Format("Blank {0}", "Production HROB Day Detail")
        End If
      Else
        Return Me.ProductionTimelineType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionHROBSimpleDetail() method.

    End Sub

    Public Shared Function NewProductionHROBSimpleDetail() As ProductionHROBSimpleDetail

      Return DataPortal.CreateChild(Of ProductionHROBSimpleDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetProductionHROBSimpleDetail(dr As SafeDataReader) As ProductionHROBSimpleDetail

      Dim p As New ProductionHROBSimpleDetail()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CrewScheduleDetailIDProperty, .GetInt32(0))
          LoadProperty(ProductionHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionTimelineTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ProductionTimelineTypeProperty, .GetString(4))
          LoadProperty(TimesheetDateProperty, .GetValue(5))
          LoadProperty(StartDateTimeProperty, .GetValue(6))
          LoadProperty(EndDateTimeProperty, .GetValue(7))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(11))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        End With
      End Using

      MarkAsChild()
      If CrewScheduleDetailID = 0 Then
        MarkNew()
      Else
        MarkOld()
      End If
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, CrewScheduleDetailIDProperty)

      cm.Parameters.AddWithValue("@ResourceBookingID", Me.GetParent().ResourceBookingID)
      cm.Parameters.AddWithValue("@ProductionHumanResourceID", NothingDBNull(GetProperty(ProductionHumanResourceIDProperty)))
      cm.Parameters.AddWithValue("@ProductionTimelineID", NothingDBNull(GetProperty(ProductionTimelineIDProperty)))
      cm.Parameters.AddWithValue("@ProductionTimelineTypeID", NothingDBNull(GetProperty(ProductionTimelineTypeIDProperty)))
      cm.Parameters.AddWithValue("@ProductionTimelineType", GetProperty(ProductionTimelineTypeProperty))
      cm.Parameters.AddWithValue("@TimesheetDate", NothingDBNull(TimesheetDate))
      cm.Parameters.AddWithValue("@StartDateTime", NothingDBNull(StartDateTime))
      cm.Parameters.AddWithValue("@EndDateTime", NothingDBNull(EndDateTime))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@ProductionHRID", Me.GetParent().ProductionHRID)
      cm.Parameters.AddWithValue("@HumanResourceID", Me.GetParent().HumanResourceID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(CrewScheduleDetailIDProperty, cm.Parameters("@CrewScheduleDetailID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@CrewScheduleDetailID", GetProperty(CrewScheduleDetailIDProperty))
      cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
    End Sub

#End Region

  End Class

End Namespace