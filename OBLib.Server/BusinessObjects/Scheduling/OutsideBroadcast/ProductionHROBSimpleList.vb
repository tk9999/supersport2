﻿' Generated 28 Jul 2016 14:26 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace OutsideBroadcast

  <Serializable()>
  Public Class ProductionHROBSimpleList
    Inherits OBBusinessListBase(Of ProductionHROBSimpleList, ProductionHROBSimple)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ProductionHROBSimple

      For Each child As ProductionHROBSimple In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Shadows Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionSystemAreaID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing
      Public Property ProductionHumanResourceID As Integer? = Nothing

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewProductionHROBSimpleList() As ProductionHROBSimpleList

      Return New ProductionHROBSimpleList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetProductionHROBSimpleList() As ProductionHROBSimpleList

      Return DataPortal.Fetch(Of ProductionHROBSimpleList)(New Criteria())

    End Function
    Public Shared Function GetProductionHROBSimpleList(crit As Criteria) As ProductionHROBSimpleList

      Return DataPortal.Fetch(Of ProductionHROBSimpleList)(crit)

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionHROBSimple.GetProductionHROBSimple(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'Crew Schedule Details
      Dim parentResourceBooking As ProductionHROBSimple = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentResourceBooking Is Nothing OrElse (parentResourceBooking.HumanResourceID <> sdr.GetInt32(12)) Then
            parentResourceBooking = Me.GetItem(sdr.GetInt32(12))
          End If
          parentResourceBooking.ProductionHROBSimpleDetailList.RaiseListChangedEvents = False
          parentResourceBooking.ProductionHROBSimpleDetailList.Add(ProductionHROBSimpleDetail.GetProductionHROBSimpleDetail(sdr))
          parentResourceBooking.ProductionHROBSimpleDetailList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionProductionHROBListSimple"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@ProductionHumanResourceID", NothingDBNull(crit.ProductionHumanResourceID))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            'cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            'cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            'cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            'cm.Parameters.AddWithValue("@RoomIDs", Strings.MakeEmptyDBNull(crit.RoomIDs))
            'cm.Parameters.AddWithValue("@RoomScheduleIDs", Strings.MakeEmptyDBNull(crit.RoomScheduleIDs))
            'cm.Parameters.AddWithValue("@ProductionHumanResourceID", NothingDBNull(crit.ProductionHumanResourceID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace

