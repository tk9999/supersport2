﻿' Generated 28 Jul 2016 14:27 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace OutsideBroadcast

  <Serializable()>
  Public Class ProductionHROBSimpleDetailList
    Inherits OBBusinessListBase(Of ProductionHROBSimpleDetailList, ProductionHROBSimpleDetail)

#Region " Business Methods "

    Public Function GetItem(CrewScheduleDetailID As Integer) As ProductionHROBSimpleDetail

      For Each child As ProductionHROBSimpleDetail In Me
        If child.CrewScheduleDetailID = CrewScheduleDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewProductionHROBSimpleDetailList() As ProductionHROBSimpleDetailList

      Return New ProductionHROBSimpleDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace