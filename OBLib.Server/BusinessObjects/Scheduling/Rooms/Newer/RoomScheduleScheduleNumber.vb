﻿' Generated 22 Dec 2016 07:40 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Scheduling.Rooms

  <Serializable()>
  Public Class RoomScheduleScheduleNumber
    Inherits OBBusinessBase(Of RoomScheduleScheduleNumber)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RoomScheduleScheduleNumberIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleScheduleNumberID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property RoomScheduleScheduleNumberID() As Integer
      Get
        Return GetProperty(RoomScheduleScheduleNumberIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleScheduleNumberIDProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared ScheduleNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ScheduleNumber, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ScheduleNumber() As Integer
      Get
        Return GetProperty(ScheduleNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ScheduleNumberProperty, Value)
      End Set
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel", "")
    ''' <summary>
    ''' Gets and sets the  Production Area value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelShortNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Object

      Return CType(CType(Me.Parent, RoomScheduleScheduleNumberList).Parent, Object)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      'If Me.ChannelName.Length = 0 Then
      '  If Me.IsNew Then
      '    Return String.Format("New {0}", "S All Channel Sched")
      '  Else
      '    Return String.Format("Blank {0}", "S All Channel Sched")
      '  End If
      'Else
      '  Return Me.ChannelName
      'End If
      Return ""

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomScheduleScheduleNumber() method.

    End Sub

    Public Shared Function NewRoomScheduleScheduleNumber() As RoomScheduleScheduleNumber

      Return DataPortal.CreateChild(Of RoomScheduleScheduleNumber)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetRoomScheduleScheduleNumber(dr As SafeDataReader) As RoomScheduleScheduleNumber

      Dim s As New RoomScheduleScheduleNumber()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RoomScheduleScheduleNumberIDProperty, .GetInt32(0))
          LoadProperty(RoomScheduleIDProperty, .GetInt32(1))
          LoadProperty(ScheduleNumberProperty, .GetInt32(2))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, RoomScheduleScheduleNumberIDProperty)

      cm.Parameters.AddWithValue("@SynergyImportID", Me.GetParent.RoomScheduleID)
      cm.Parameters.AddWithValue("@ScheduleNumber", GetProperty(ScheduleNumberProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(RoomScheduleScheduleNumberIDProperty, cm.Parameters("@RoomScheduleScheduleNumberID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@RoomScheduleScheduleNumberID", GetProperty(RoomScheduleScheduleNumberIDProperty))
    End Sub

#End Region

  End Class

End Namespace