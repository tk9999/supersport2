﻿' Generated 16 Jun 2016 14:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Scheduling.Rooms

  <Serializable()>
  Public Class RoomSchedulePHR
    Inherits OBBusinessBase(Of RoomSchedulePHR)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RoomSchedulePHRBO.RoomSchedulePHRBOToString(self)")

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property ProductionHumanResourceID() As Integer
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHRID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ProductionHRID() As Integer
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionHRIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "Resource Booking")
    ''' <summary>
    ''' Gets and sets the Resource Booking value
    ''' </summary>
    <Display(Name:="Resource Booking", Description:="")>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Production System Area")
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:=""),
    Required(ErrorMessage:="Production System Area required")>
    Public Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required"),
    DropDownWeb(GetType(ROHumanResourceAvailabilityList),
                BeforeFetchJS:="RoomSchedulePHRBO.setHumanResourceCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomSchedulePHRBO.triggerHumanResourceAutoPopulate",
                AfterFetchJS:="RoomSchedulePHRBO.afterHumanResourceRefreshAjax",
                OnItemSelectJSFunction:="RoomSchedulePHRBO.onHumanResourceSelected",
                LookupMember:="HumanResource", DisplayMember:="HRName", ValueMember:="HumanResourceID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"HRName", "ContractType", "IsAvailableString", "CellPhoneNumber"})>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
    Public Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CallTime, "Call Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    Required(ErrorMessage:="Call Time required"),
    SetExpression("RoomSchedulePHRBO.CallTimeSet(self)")>
    Public Property CallTime As DateTime?
      Get
        Return GetProperty(CallTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeProperty, Value)
      End Set
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartTime, "Start Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
    SetExpression("RoomSchedulePHRBO.StartTimeSet(self)")>
    Public Property StartTime As DateTime?
      Get
        Return GetProperty(StartTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartTimeProperty, Value)
      End Set
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndTime, "End Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
    SetExpression("RoomSchedulePHRBO.EndTimeSet(self)")>
    Public Property EndTime As DateTime?
      Get
        Return GetProperty(EndTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndTimeProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.WrapTime, "Wrap Time")
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    Required(ErrorMessage:="Wrap Time required"),
    SetExpression("RoomSchedulePHRBO.WrapTimeSet(self)")>
    Public Property WrapTime As DateTime?
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeProperty, Value)
      End Set
    End Property

    'Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Resource Booking Description")
    ' ''' <summary>
    ' ''' Gets and sets the Resource Booking Description value
    ' ''' </summary>
    '<Display(Name:="Resource Booking Description", Description:="")>
    'Public Property ResourceBookingDescription() As String
    '  Get
    '    Return GetProperty(ResourceBookingDescriptionProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(ResourceBookingDescriptionProperty, Value)
    '  End Set
    'End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    Required(ErrorMessage:="Discipline required"),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplineListSelect),
                BeforeFetchJS:="RoomScheduleAreaProductionHumanResourceBO.setDisciplineIDCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleAreaProductionHumanResourceBO.triggerDisciplineIDAutoPopulate",
                AfterFetchJS:="RoomScheduleAreaProductionHumanResourceBO.afterDisciplineIDRefreshAjax",
                OnItemSelectJSFunction:="RoomScheduleAreaProductionHumanResourceBO.onDisciplineIDSelected",
                LookupMember:="Discipline", DisplayMember:="Discipline", ValueMember:="DisciplineID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Discipline"})>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionTypeID, "Position Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:=""),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplinePositionTypeListSelect),
                BeforeFetchJS:="RoomScheduleAreaProductionHumanResourceBO.setPositionTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleAreaProductionHumanResourceBO.triggerPositionTypeIDAutoPopulate",
                AfterFetchJS:="RoomScheduleAreaProductionHumanResourceBO.afterPositionTypeIDRefreshAjax",
                OnItemSelectJSFunction:="RoomScheduleAreaProductionHumanResourceBO.onPositionTypeIDSelected",
                LookupMember:="PositionType", DisplayMember:="PositionType", ValueMember:="PositionTypeID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Position"})>
    Public Property PositionTypeID() As Integer?
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared PositionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionType, "Position Type", "")
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
    Public Property PositionType() As String
      Get
        Return GetProperty(PositionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionTypeProperty, Value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:=""),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplinePositionTypeListSelect),
                BeforeFetchJS:="RoomScheduleAreaProductionHumanResourceBO.setPositionIDCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleAreaProductionHumanResourceBO.triggerPositionIDAutoPopulate",
                AfterFetchJS:="RoomScheduleAreaProductionHumanResourceBO.afterPositionIDRefreshAjax",
                OnItemSelectJSFunction:="RoomScheduleAreaProductionHumanResourceBO.onPositionIDSelected",
                LookupMember:="Position", DisplayMember:="Position", ValueMember:="PositionID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Position"})>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position", "")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionProperty, Value)
      End Set
    End Property

    Public Shared CallTimeCSDIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CallTimeCSDID, "Call Time CSD")
    ''' <summary>
    ''' Gets and sets the Call Time CSD value
    ''' </summary>
    <Display(Name:="Call Time CSD", Description:="")>
    Public Property CallTimeCSDID() As Integer
      Get
        Return GetProperty(CallTimeCSDIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CallTimeCSDIDProperty, Value)
      End Set
    End Property

    Public Shared OnAirTimeCSDIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OnAirTimeCSDID, "On Air Time CSD")
    ''' <summary>
    ''' Gets and sets the On Air Time CSD value
    ''' </summary>
    <Display(Name:="On Air Time CSD", Description:="")>
    Public Property OnAirTimeCSDID() As Integer
      Get
        Return GetProperty(OnAirTimeCSDIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OnAirTimeCSDIDProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeCSDIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WrapTimeCSDID, "Wrap Time CSD")
    ''' <summary>
    ''' Gets and sets the Wrap Time CSD value
    ''' </summary>
    <Display(Name:="Wrap Time CSD", Description:="")>
    Public Property WrapTimeCSDID() As Integer
      Get
        Return GetProperty(WrapTimeCSDIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WrapTimeCSDIDProperty, Value)
      End Set
    End Property

    Public Shared LinkedGuidProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LinkedGuid, "LinkedGuid", "")
    ''' <summary>
    ''' Gets and sets the LinkedGuid value
    ''' </summary>
    <Display(Name:="LinkedGuid", Description:="")>
    Public Property LinkedGuid() As String
      Get
        Return GetProperty(LinkedGuidProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LinkedGuidProperty, Value)
      End Set
    End Property

    Public Shared ClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ClashCount, "ClashCount", 0)
    ''' <summary>
    ''' Gets and sets the  Production Area value
    ''' </summary>
    <Display(Name:="ClashCount", Description:="")>
    Public Property ClashCount() As Integer
      Get
        Return GetProperty(ClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ClashCountProperty, Value)
      End Set
    End Property

    Public Shared ClashesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Clashes, "Clashes", "")
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Clashes")>
    Public Property Clashes() As String
      Get
        Return GetProperty(ClashesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ClashesProperty, Value)
      End Set
    End Property

    'Public Shared CallTimeMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CallTimeMatch, "CallTimeMatch", False)
    ' ''' <summary>
    ' ''' Gets and sets the Slug Layout In Scheduler value
    ' ''' </summary>
    '<Display(Name:="CallTimeMatch", Description:="")>
    'Public ReadOnly Property CallTimeMatch() As Boolean
    '  Get
    '    Return GetProperty(CallTimeMatchProperty)
    '  End Get
    'End Property

    'Public Shared OnAirStartMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnAirStartMatch, "OnAirStartMatch", False)
    ' ''' <summary>
    ' ''' Gets and sets the Slug Layout In Scheduler value
    ' ''' </summary>
    '<Display(Name:="OnAirStartMatch", Description:="")>
    'Public ReadOnly Property OnAirStartMatch() As Boolean
    '  Get
    '    Return GetProperty(OnAirStartMatchProperty)
    '  End Get
    'End Property

    'Public Shared OnAirEndMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnAirEndMatch, "OnAirEndMatch", False)
    ' ''' <summary>
    ' ''' Gets and sets the Slug Layout In Scheduler value
    ' ''' </summary>
    '<Display(Name:="OnAirEndMatch", Description:="")>
    'Public ReadOnly Property OnAirEndMatch() As Boolean
    '  Get
    '    Return GetProperty(OnAirEndMatchProperty)
    '  End Get
    'End Property

    'Public Shared WrapTimeMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.WrapTimeMatch, "WrapTimeMatch", False)
    ' ''' <summary>
    ' ''' Gets and sets the Slug Layout In Scheduler value
    ' ''' </summary>
    '<Display(Name:="WrapTimeMatch", Description:="")>
    'Public ReadOnly Property WrapTimeMatch() As Boolean
    '  Get
    '    Return GetProperty(WrapTimeMatchProperty)
    '  End Get
    'End Property

#End Region

#Region " Other Properties "

    Public Shared FreezeSetExpressionsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FreezeSetExpressions, "FreezeSetExpressions", False)
    ''' <summary>
    ''' Gets and sets the Is TBC value
    ''' </summary>
    <Display(Name:="FreezeSetExpressions", Description:=""), AlwaysClean>
    Public Property FreezeSetExpressions() As Boolean
      Get
        Return GetProperty(FreezeSetExpressionsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(FreezeSetExpressionsProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ClashListProperty As PropertyInfo(Of List(Of String)) = RegisterProperty(Of List(Of String))(Function(c) c.ClashList, "ClashList")
    Public ReadOnly Property ClashList() As List(Of String)
      Get
        If GetProperty(ClashListProperty) Is Nothing Then
          LoadProperty(ClashListProperty, New List(Of String))
        End If
        Return GetProperty(ClashListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As RoomSchedule

      Return CType(CType(Me.Parent, RoomSchedulePHRList).Parent, RoomSchedule)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHRIDProperty)

    End Function

    Public Overrides Function ToString() As String

      'If Me.IgnoreClashesReason.Length = 0 Then
      '  If Me.IsNew Then
      '    Return String.Format("New {0}", "Room Schedule Area Production HR Booking")
      '  Else
      '    Return String.Format("Blank {0}", "Room Schedule Area Production HR Booking")
      '  End If
      'Else
      '  Return Me.IgnoreClashesReason
      'End If
      Return ""

    End Function

    Public Function GetProductionHRID() As Integer?

      Dim phrList As List(Of RoomSchedulePHR) = Me.GetParent.RoomSchedulePHRList.Where(Function(d) CompareSafe(d.HumanResourceID, Me.HumanResourceID)).ToList
      If phrList.Count = 0 Then
        Return Nothing
      ElseIf phrList.Count = 1 Then
        Return phrList(0).ProductionHRID
      ElseIf phrList.Count > 1 Then
        Return phrList(0).ProductionHRID
      End If
    End Function

    Public Sub ResetClashList()

      LoadProperty(ClashListProperty, New List(Of String))
      LoadProperty(ClashesProperty, "")

    End Sub

    Private Function CalculateResourceBookingDescription() As String

      Return Me.GetParent.Room & ": " & Me.CallTime.Value.ToString("HH:mm") & " - " & Me.WrapTime.Value.ToString("HH:mm")

    End Function

    Private Function CalculatePopoverDescription() As String

      Return Me.GetParent.Room & ": " & CallTime.Value.ToString("HH:mm") & " - " & WrapTime.Value.ToString("HH:mm")

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(CallTimeProperty)
        .ServerRuleFunction = AddressOf CallTimeValid
        .JavascriptRuleFunctionName = "RoomSchedulePHRBO.CallTimeValid"
        .AffectedProperties.AddRange({CallTimeProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
        .AddTriggerProperties({CallTimeProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
      End With

      With AddWebRule(StartTimeProperty)
        .ServerRuleFunction = AddressOf StartTimeValid
        .JavascriptRuleFunctionName = "RoomSchedulePHRBO.StartTimeValid"
        .AffectedProperties.AddRange({CallTimeProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
        .AddTriggerProperties({CallTimeProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
      End With

      With AddWebRule(EndTimeProperty)
        .ServerRuleFunction = AddressOf EndTimeValid
        .JavascriptRuleFunctionName = "RoomSchedulePHRBO.EndTimeValid"
        .AffectedProperties.AddRange({CallTimeProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
        .AddTriggerProperties({CallTimeProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
      End With

      With AddWebRule(WrapTimeProperty)
        .ServerRuleFunction = AddressOf WrapTimeValid
        .JavascriptRuleFunctionName = "RoomSchedulePHRBO.WrapTimeValid"
        .AffectedProperties.AddRange({CallTimeProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
        .AddTriggerProperties({CallTimeProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
      End With

      'With AddWebRule(WrapTimeProperty)
      '  .ServerRuleFunction = AddressOf WrapTimeValid
      '  .JavascriptRuleFunctionName = "RoomSchedulePHRBO.WrapTimeValid"
      '  .AffectedProperties.AddRange({CallTimeProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
      '  .AddTriggerProperties({CallTimeProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
      'End With

    End Sub

    Public Shared Function CallTimeValid(RoomSchedulePHR As RoomSchedulePHR) As String
      If RoomSchedulePHR.CallTime Is Nothing Then
        Return "Call time is required"
      Else
        If RoomSchedulePHR.StartTime IsNot Nothing Then
          If RoomSchedulePHR.CallTime.Value.Subtract(RoomSchedulePHR.StartTime.Value).TotalMinutes > 0 Then
            Return "Call time must be before Start time"
          End If
        End If
      End If
      Return ""
    End Function

    Public Shared Function StartTimeValid(RoomSchedulePHR As RoomSchedulePHR) As String
      If RoomSchedulePHR.StartTime Is Nothing Then
        Return "Start time is required"
      Else
        If RoomSchedulePHR.EndTime IsNot Nothing Then
          If RoomSchedulePHR.StartTime.Value.Subtract(RoomSchedulePHR.EndTime.Value).TotalMinutes > 0 Then
            Return "Start time must be before End time"
          End If
        End If
      End If
      Return ""
    End Function

    Public Shared Function EndTimeValid(RoomSchedulePHR As RoomSchedulePHR) As String
      If RoomSchedulePHR.EndTime Is Nothing Then
        Return "End time is required"
      Else
        If RoomSchedulePHR.WrapTime IsNot Nothing Then
          If RoomSchedulePHR.EndTime.Value.Subtract(RoomSchedulePHR.WrapTime.Value).TotalMinutes > 0 Then
            Return "End time must be before Wrap time"
          End If
        End If
      End If
      Return ""
    End Function

    Public Shared Function WrapTimeValid(RoomSchedulePHR As RoomSchedulePHR) As String
      If RoomSchedulePHR.WrapTime Is Nothing Then
        Return "Wrap time is required"
      Else
        If RoomSchedulePHR.EndTime IsNot Nothing Then
          If RoomSchedulePHR.EndTime.Value.Subtract(RoomSchedulePHR.WrapTime.Value).TotalMinutes > 0 Then
            Return "End time must be before Wrap time"
          End If
        End If
      End If
      Return ""
    End Function

    Public Shared Function HasClashesValid(RoomSchedulePHR As RoomSchedulePHR) As String
      If RoomSchedulePHR.ClashList.Count > 0 Then
        Return RoomSchedulePHR.ClashList.Count.ToString & " clashes detected"
      Else
        Return ""
      End If
    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomSchedulePHR() method.

    End Sub

    Public Shared Function NewRoomSchedulePHR() As RoomSchedulePHR

      Return DataPortal.CreateChild(Of RoomSchedulePHR)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetRoomSchedulePHR(dr As SafeDataReader) As RoomSchedulePHR

      Dim r As New RoomSchedulePHR()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionHRIDProperty, .GetInt32(0))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          'LoadProperty(ResourceBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          'LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(CallTimeProperty, .GetValue(7))
          LoadProperty(StartTimeProperty, .GetValue(8))
          LoadProperty(EndTimeProperty, .GetValue(9))
          LoadProperty(WrapTimeProperty, .GetValue(10))
          'LoadProperty(IgnoreClashesReasonProperty, .GetString(11))
          'LoadProperty(IsCancelledProperty, .GetBoolean(12))
          'LoadProperty(IsTBCProperty, .GetBoolean(13))
          'LoadProperty(ResourceBookingDescriptionProperty, .GetString(14))
          'LoadProperty(StatusCssClassProperty, .GetString(15))
          LoadProperty(CallTimeCSDIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          LoadProperty(OnAirTimeCSDIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(WrapTimeCSDIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
          'LoadProperty(CallTimeTimelineIDProperty, ZeroNothing(.GetInt32(19)))
          'LoadProperty(OnAirTimeTimelineIDProperty, ZeroNothing(.GetInt32(20)))
          'LoadProperty(WrapTimeTimelineIDProperty, ZeroNothing(.GetInt32(21)))
          'LoadProperty(CallTimeMatchProperty, .GetBoolean(22))
          'LoadProperty(OnAirStartMatchProperty, .GetBoolean(23))
          'LoadProperty(OnAirEndMatchProperty, .GetBoolean(24))
          'LoadProperty(WrapTimeMatchProperty, .GetBoolean(25))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionHumanResourceIDProperty)
      AddInputOutputParam(cm, ProductionHRIDProperty)
      AddInputOutputParam(cm, ResourceBookingIDProperty)
      AddInputOutputParam(cm, CallTimeCSDIDProperty)
      AddInputOutputParam(cm, OnAirTimeCSDIDProperty)
      AddInputOutputParam(cm, WrapTimeCSDIDProperty)

      cm.Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent().ProductionSystemAreaID)
      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@ResourceID", NothingDBNull(GetProperty(ResourceIDProperty)))
      cm.Parameters.AddWithValue("@RoomScheduleID", Me.GetParent().RoomScheduleID)
      cm.Parameters.AddWithValue("@CallTime", NothingDBNull(GetProperty(CallTimeProperty)))
      cm.Parameters.AddWithValue("@StartTime", NothingDBNull(GetProperty(StartTimeProperty)))
      cm.Parameters.AddWithValue("@EndTime", NothingDBNull(GetProperty(EndTimeProperty)))
      cm.Parameters.AddWithValue("@WrapTime", NothingDBNull(GetProperty(WrapTimeProperty)))
      cm.Parameters.AddWithValue("@CallTimeTimelineID", Me.GetParent.CallTimeTimelineID)
      cm.Parameters.AddWithValue("@OnAirTimeTimelineID", Me.GetParent.OnAirTimeTimelineID)
      cm.Parameters.AddWithValue("@WrapTimeTimelineID", Me.GetParent.WrapTimeTimelineID)
      cm.Parameters.AddWithValue("@SystemID", NothingDBNull(Me.GetParent.SystemID))
      cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(Me.GetParent.ProductionAreaID))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(GetProperty(DisciplineIDProperty)))
      cm.Parameters.AddWithValue("@Discipline", GetProperty(DisciplineProperty))
      cm.Parameters.AddWithValue("@PositionTypeID", NothingDBNull(GetProperty(PositionTypeIDProperty)))
      cm.Parameters.AddWithValue("@PositionType", GetProperty(PositionTypeProperty))
      cm.Parameters.AddWithValue("@PositionID", NothingDBNull(GetProperty(PositionIDProperty)))
      cm.Parameters.AddWithValue("@Position", GetProperty(PositionProperty))
      cm.Parameters.AddWithValue("@HumanResource", GetProperty(HumanResourceProperty))
      cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(Me.GetParent.ProductionID))
      cm.Parameters.AddWithValue("@RoomID", NothingDBNull(Me.GetParent.RoomID))
      cm.Parameters.AddWithValue("@Room", Me.GetParent.Room)
      cm.Parameters.AddWithValue("@Title", Me.GetParent.Title)
      cm.Parameters.AddWithValue("@ResourceBookingDescription", Me.CalculateResourceBookingDescription)
      cm.Parameters.AddWithValue("@PopoverContent", Me.CalculatePopoverDescription)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionHumanResourceIDProperty, cm.Parameters("@ProductionHumanResourceID").Value)
                 LoadProperty(ProductionHRIDProperty, cm.Parameters("@ProductionHRID").Value)
                 LoadProperty(ResourceBookingIDProperty, cm.Parameters("@ResourceBookingID").Value)
                 LoadProperty(CallTimeCSDIDProperty, cm.Parameters("@CallTimeCSDID").Value)
                 LoadProperty(OnAirTimeCSDIDProperty, cm.Parameters("@OnAirTimeCSDID").Value)
                 LoadProperty(WrapTimeCSDIDProperty, cm.Parameters("@WrapTimeCSDID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ProductionHRID", GetProperty(ProductionHRIDProperty))
    End Sub

#End Region

  End Class

End Namespace