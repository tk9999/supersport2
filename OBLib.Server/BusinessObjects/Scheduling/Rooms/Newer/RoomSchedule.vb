﻿' Generated 22 Dec 2016 07:40 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Quoting.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Scheduling.Rooms

  <Serializable()>
  Public Class RoomSchedule
    Inherits OBBusinessBase(Of RoomSchedule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleTypeID, "RoomScheduleTypeID", Nothing)
    ''' <summary>
    ''' Gets and sets the  Production Area value
    ''' </summary>
    <Display(Name:="RoomScheduleTypeID", Description:="")>
    Public Property RoomScheduleTypeID() As Integer?
      Get
        Return GetProperty(RoomScheduleTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "ProductionID")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="ProductionID", Description:="")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingID, "Ad Hoc Booking")
    ''' <summary>
    ''' Gets and sets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking", Description:="")>
    Public Property AdHocBookingID() As Integer?
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AdHocBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDOwnerProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaIDOwner, " Area", Nothing)
    ''' <summary>
    ''' Gets and sets the  Production Area value
    ''' </summary>
    <Display(Name:="ProductionSystemArea", Description:="")>
    Public Property ProductionSystemAreaIDOwner() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDOwnerProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDOwnerProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets and sets the  Production Area value
    ''' </summary>
    <Display(Name:="Title", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Title is required")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
    Required(ErrorMessage:="Room required"),
    DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="RoomScheduleBO.setRoomCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleBO.triggerRoomAutoPopulate",
                AfterFetchJS:="RoomScheduleBO.afterRoomRefreshAjax",
                LookupMember:="Room", ValueMember:="RoomID", DropDownColumns:={"Room", "IsAvailableString"},
                OnItemSelectJSFunction:="RoomScheduleBO.onRoomSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("RoomScheduleBO.RoomIDSet(self)")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Room is required")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Debtor")
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:=""),
    DropDownWeb(GetType(RODebtorFindList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="RoomScheduleBO.setDebtorCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleBO.triggerDebtorAutoPopulate",
                AfterFetchJS:="RoomScheduleBO.afterDebtorRefreshAjax",
                OnItemSelectJSFunction:="RoomScheduleBO.onDebtorSelected",
                LookupMember:="Debtor", DisplayMember:="Debtor", ValueMember:="DebtorID",
                DropDownCssClass:="debtor-dropdown", DropDownColumns:={"Debtor", "ContactName", "ContactNumber"}),
    SetExpression("RoomScheduleBO.DebtorIDSet(self)")>
    Public Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DebtorIDProperty, Value)
      End Set
    End Property

    Public Shared DebtorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Debtor, "Debtor")
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="")>
    Public Property Debtor() As String
      Get
        Return GetProperty(DebtorProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DebtorProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, " Area", Nothing)
    ''' <summary>
    ''' Gets and sets the  Production Area value
    ''' </summary>
    <Display(Name:="ProductionSystemArea", Description:="")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required"),
    DropDownWeb(GetType(ROUserSystemAreaList), ThisFilterMember:="SystemID", DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "Status")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:=""),
    Required(ErrorMessage:="Status required"),
    DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaStatusSelectList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="RoomScheduleBO.setStatusCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleBO.triggerStatusAutoPopulate",
                AfterFetchJS:="RoomScheduleBO.afterStatusRefreshAjax",
                LookupMember:="ProductionAreaStatus", ValueMember:="ProductionAreaStatusID", DropDownColumns:={"ProductionAreaStatus"},
                OnItemSelectJSFunction:="RoomScheduleBO.onStatusSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("RoomScheduleBO.ProductionAreaStatusIDSet(self)")>
    Public Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Status")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaStatusProperty, Value)
      End Set
    End Property

    Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "Status Css Class")
    ''' <summary>
    ''' Gets and sets the Status Css Class value
    ''' </summary>
    <Display(Name:="Status Css Class", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Status Css Class cannot be blank")>
    Public Property StatusCssClass() As String
      Get
        Return GetProperty(StatusCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCssClassProperty, Value)
      End Set
    End Property

    Public Shared AreaCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AreaComments, "Comments")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public Property AreaComments() As String
      Get
        Return GetProperty(AreaCommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AreaCommentsProperty, Value)
      End Set
    End Property

    Public Shared CallTimeTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CallTimeTimelineID, "Call Time", Nothing)
    ''' <summary>
    ''' Gets and sets the Call Time Timeline value
    ''' </summary>
    <Display(Name:="Call Time", Description:="")>
    Public Property CallTimeTimelineID() As Integer?
      Get
        Return GetProperty(CallTimeTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CallTimeTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.CallTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    SetExpression("RoomScheduleBO.CallTimeSet(self)"),
    Required(ErrorMessage:="Call Time is required"),
    Singular.DataAnnotations.DateAndTimeField(FormatString:="HH:mm", ViewMode:="days")>
    Public Property CallTime As DateTime?
      Get
        Return GetProperty(CallTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeProperty, Value)
      End Set
    End Property

    Public Shared OnAirTimeTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OnAirTimeTimelineID, "On Air Time", Nothing)
    ''' <summary>
    ''' Gets and sets the On Air Time Timeline value
    ''' </summary>
    <Display(Name:="On Air Time", Description:="")>
    Public Property OnAirTimeTimelineID() As Integer?
      Get
        Return GetProperty(OnAirTimeTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OnAirTimeTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    SetExpression("RoomScheduleBO.StartTimeSet(self)"),
    Required(ErrorMessage:="Start Time is required"),
    Singular.DataAnnotations.DateAndTimeField(FormatString:="HH:mm", ViewMode:="days")>
    Public Property StartTime As DateTime?
      Get
        Return GetProperty(StartTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartTimeProperty, Value)
      End Set
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    SetExpression("RoomScheduleBO.EndTimeSet(self)"),
    Required(ErrorMessage:="End Time is required"),
    Singular.DataAnnotations.DateAndTimeField(FormatString:="HH:mm", ViewMode:="days")>
    Public Property EndTime As DateTime?
      Get
        Return GetProperty(EndTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndTimeProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.WrapTimeTimelineID, "Wrap Time", Nothing)
    ''' <summary>
    ''' Gets and sets the Wrap Time Timeline value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:="")>
    Public Property WrapTimeTimelineID() As Integer?
      Get
        Return GetProperty(WrapTimeTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(WrapTimeTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.WrapTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    SetExpression("RoomScheduleBO.WrapTimeSet(self)"),
    Required(ErrorMessage:="Wrap Time is required"),
    Singular.DataAnnotations.DateAndTimeField(FormatString:="HH:mm", ViewMode:="days")>
    Public Property WrapTime As DateTime?
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeProperty, Value)
      End Set
    End Property

    Public Shared IsOwnerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsOwner, "IsOwner", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="IsOwner")>
    Public Property IsOwner() As Boolean
      Get
        Return GetProperty(IsOwnerProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsOwnerProperty, value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    ''' 
    <Required(ErrorMessage:="Resource is required")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Is Cancelled value
    ''' </summary>
    <Display(Name:="Is Cancelled", Description:=""),
    Required(ErrorMessage:="Is Cancelled required")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared PopoverContentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PopoverContent, "Popover Content")
    ''' <summary>
    ''' Gets and sets the Popover Content value
    ''' </summary>
    <Display(Name:="Popover Content", Description:="")>
    Public Property PopoverContent() As String
      Get
        Return GetProperty(PopoverContentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PopoverContentProperty, Value)
      End Set
    End Property

    Public Shared SlugLayoutInSchedulerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SlugLayoutInScheduler, "Slug Layout In Scheduler", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="Slug Layout In Scheduler", Description:=""),
    Required(ErrorMessage:="Slug Layout In Scheduler required")>
    Public Property SlugLayoutInScheduler() As Boolean
      Get
        Return GetProperty(SlugLayoutInSchedulerProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SlugLayoutInSchedulerProperty, Value)
      End Set
    End Property

    Public Shared PendingDeleteProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PendingDelete, "PendingDelete", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="PendingDelete", Description:="")>
    Public Property PendingDelete() As Boolean
      Get
        Return GetProperty(PendingDeleteProperty)
      End Get
      Set(value As Boolean)
        SetProperty(PendingDeleteProperty, value)
      End Set
    End Property

    Public Shared CallTimeMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CallTimeMatch, "CallTimeMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="CallTimeMatch", Description:="")>
    Public ReadOnly Property CallTimeMatch() As Boolean
      Get
        Return GetProperty(CallTimeMatchProperty)
      End Get
    End Property

    Public Shared OnAirStartMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnAirStartMatch, "OnAirStartMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="OnAirStartMatch", Description:="")>
    Public ReadOnly Property OnAirStartMatch() As Boolean
      Get
        Return GetProperty(OnAirStartMatchProperty)
      End Get
    End Property

    Public Shared OnAirEndMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnAirEndMatch, "OnAirEndMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="OnAirEndMatch", Description:="")>
    Public ReadOnly Property OnAirEndMatch() As Boolean
      Get
        Return GetProperty(OnAirEndMatchProperty)
      End Get
    End Property

    Public Shared WrapTimeMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.WrapTimeMatch, "WrapTimeMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="WrapTimeMatch", Description:="")>
    Public ReadOnly Property WrapTimeMatch() As Boolean
      Get
        Return GetProperty(WrapTimeMatchProperty)
      End Get
    End Property

    Public Shared RoomMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomMatch, "RoomMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="RoomMatch", Description:="")>
    Public ReadOnly Property RoomMatch() As Boolean
      Get
        Return GetProperty(RoomMatchProperty)
      End Get
    End Property

    Public Shared ResourceIDPSAProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceIDPSA, "ResourceIDPSA")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="ResourceIDPSA", Description:="")>
    Public Property ResourceIDPSA() As Integer?
      Get
        Return GetProperty(ResourceIDPSAProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDPSAProperty, Value)
      End Set
    End Property

    Public Shared ResourceNamePSAProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceNamePSA, "ResourceNamePSA", "")
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="ResourceNamePSA"), AlwaysClean>
    Public Property ResourceNamePSA() As String
      Get
        Return GetProperty(ResourceNamePSAProperty)
      End Get
      Set(value As String)
        SetProperty(ResourceNamePSAProperty, value)
      End Set
    End Property

    Public Shared ResourceIDOwnerProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceIDOwner, "ResourceIDOwner")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="ResourceIDOwner", Description:="")>
    Public Property ResourceIDOwner() As Integer?
      Get
        Return GetProperty(ResourceIDOwnerProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDOwnerProperty, Value)
      End Set
    End Property

    Public Shared RSCallTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RSCallTime, "Call Time")
    ''' <summary>
    ''' Gets and sets the Call Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:="")>
    Public ReadOnly Property RSCallTime As DateTime?
      Get
        Return GetProperty(RSCallTimeProperty)
      End Get
    End Property

    Public Shared RSOnAirTimeStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RSOnAirTimeStart, "Start Time")
    ''' <summary>
    ''' Gets and sets the On Air Time Start value
    ''' </summary>
    <Display(Name:="Start Time", Description:="")>
    Public ReadOnly Property RSOnAirTimeStart As DateTime?
      Get
        Return GetProperty(RSOnAirTimeStartProperty)
      End Get
    End Property

    Public Shared RSOnAirTimeEndProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RSOnAirTimeEnd, "End Time")
    ''' <summary>
    ''' Gets and sets the On Air Time End value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public ReadOnly Property RSOnAirTimeEnd As DateTime?
      Get
        Return GetProperty(RSOnAirTimeEndProperty)
      End Get
    End Property

    Public Shared RSWrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RSWrapTime, "Wrap Time")
    ''' <summary>
    ''' Gets and sets the Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:="")>
    Public ReadOnly Property RSWrapTime As DateTime?
      Get
        Return GetProperty(RSWrapTimeProperty)
      End Get
    End Property

    Public Shared OwnerCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OwnerComments, "Comments")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public Property OwnerComments() As String
      Get
        Return GetProperty(OwnerCommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OwnerCommentsProperty, Value)
      End Set
    End Property

    Public Shared IsReconciledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsReconciled, "IsReconciled", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="IsReconciled", Description:="")>
    Public Property IsReconciled() As Boolean
      Get
        Return GetProperty(IsReconciledProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsReconciledProperty, value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNumber, " GenRefNumber", 0)
    ''' <summary>
    ''' Gets and sets the  Production Area value
    ''' </summary>
    <Display(Name:="GenRefNumber", Description:=""),
    Required(ErrorMessage:="GenRefNumber is required")>
    Public Property GenRefNumber() As Int64?
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared GenreDescProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreDesc, "Genre")
    ''' <summary>
    ''' Gets and sets the Series value
    ''' </summary>
    <Display(Name:="Genre", Description:="")>
    Public Property GenreDesc() As String
      Get
        Return GetProperty(GenreDescProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreDescProperty, Value)
      End Set
    End Property

    Public Shared SeriesTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SeriesTitle, "Series")
    ''' <summary>
    ''' Gets and sets the Series value
    ''' </summary>
    <Display(Name:="Series", Description:="")>
    Public Property SeriesTitle() As String
      Get
        Return GetProperty(SeriesTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SeriesTitleProperty, Value)
      End Set
    End Property

    Public Shared ProductionTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTitle, "Title", "")
    ''' <summary>
    ''' Gets and sets the  Production Area value
    ''' </summary>
    <Display(Name:="Title", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Title is required")>
    Public Property ProductionTitle() As String
      Get
        Return GetProperty(ProductionTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTitleProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared LoadedProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LoadedProductionAreaStatusID, "Status")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property LoadedProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(LoadedProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LoadedProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared ClashesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Clashes, "Clashes", "")
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Clashes")>
    Public Property Clashes() As String
      Get
        Return GetProperty(ClashesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ClashesProperty, Value)
      End Set
    End Property

    Public Shared ImportedSuccessfullyProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ImportedSuccessfully, "Imported Successfully", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="Imported Successfully", Description:="")>
    Public Property ImportedSuccessfully() As Boolean
      Get
        Return GetProperty(ImportedSuccessfullyProperty)
      End Get
      Set(value As Boolean)
        SetProperty(ImportedSuccessfullyProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared RoomScheduleScheduleNumberListProperty As PropertyInfo(Of OBLib.Scheduling.Rooms.RoomScheduleScheduleNumberList) = RegisterProperty(Of OBLib.Scheduling.Rooms.RoomScheduleScheduleNumberList)(Function(c) c.RoomScheduleScheduleNumberList, "Synergy Schedule List")
    Public ReadOnly Property RoomScheduleScheduleNumberList() As OBLib.Scheduling.Rooms.RoomScheduleScheduleNumberList
      Get
        If GetProperty(RoomScheduleScheduleNumberListProperty) Is Nothing Then
          LoadProperty(RoomScheduleScheduleNumberListProperty, OBLib.Scheduling.Rooms.RoomScheduleScheduleNumberList.NewRoomScheduleScheduleNumberList())
        End If
        Return GetProperty(RoomScheduleScheduleNumberListProperty)
      End Get
    End Property

    Public Shared RoomSchedulePHRListProperty As PropertyInfo(Of RoomSchedulePHRList) = RegisterProperty(Of RoomSchedulePHRList)(Function(c) c.RoomSchedulePHRList, "Synergy Schedule List")
    Public ReadOnly Property RoomSchedulePHRList() As RoomSchedulePHRList
      Get
        If GetProperty(RoomSchedulePHRListProperty) Is Nothing Then
          LoadProperty(RoomSchedulePHRListProperty, OBLib.Scheduling.Rooms.RoomSchedulePHRList.NewRoomSchedulePHRList())
        End If
        Return GetProperty(RoomSchedulePHRListProperty)
      End Get
    End Property

    Public Shared ClashListProperty As PropertyInfo(Of List(Of String)) = RegisterProperty(Of List(Of String))(Function(c) c.ClashList, "ClashList")
    Public ReadOnly Property ClashList() As List(Of String)
      Get
        If GetProperty(ClashListProperty) Is Nothing Then
          LoadProperty(ClashListProperty, New List(Of String))
        End If
        Return GetProperty(ClashListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Object

      Return CType(CType(Me.Parent, RoomScheduleList).Parent, Object)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      'If Me.ChannelName.Length = 0 Then
      '  If Me.IsNew Then
      '    Return String.Format("New {0}", "S All Channel Sched")
      '  Else
      '    Return String.Format("Blank {0}", "S All Channel Sched")
      '  End If
      'Else
      '  Return Me.ChannelName
      'End If
      Return ""

    End Function

    Public Sub ResetClashList()

      LoadProperty(ClashListProperty, New List(Of String))
      LoadProperty(ClashesProperty, "")

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(ClashesProperty)
        .JavascriptRuleFunctionName = "RoomScheduleBO.ClashesValid"
        .ServerRuleFunction = AddressOf ClashesValid
      End With

      With AddWebRule(RoomIDProperty)
        .AddTriggerProperties({CallTimeProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
        .JavascriptRuleFunctionName = "RoomScheduleBO.RoomIDValid"
        .AffectedProperties.AddRange({CallTimeProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
      End With

    End Sub

    Public Shared Function ClashesValid(RoomSched As RoomSchedule) As String

      If RoomSched.Clashes.Length > 0 Then
        Return "Clashes were detected: " & vbCrLf & RoomSched.Clashes
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomSchedule() method.

    End Sub

    Public Shared Function NewRoomSchedule() As RoomSchedule

      Return DataPortal.CreateChild(Of RoomSchedule)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetRoomSchedule(dr As SafeDataReader) As RoomSchedule

      Dim s As New RoomSchedule()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
          LoadProperty(RoomScheduleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(AdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(TitleProperty, .GetString(4))
          LoadProperty(ProductionSystemAreaIDOwnerProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(RoomProperty, .GetString(7))
          LoadProperty(DebtorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(DebtorProperty, .GetString(9))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(11)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(12)))
          LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(ProductionAreaStatusProperty, .GetString(14))
          LoadProperty(StatusCssClassProperty, .GetString(15))
          LoadProperty(AreaCommentsProperty, .GetString(16))
          LoadProperty(CallTimeTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(CallTimeProperty, .GetValue(18))
          LoadProperty(OnAirTimeTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
          LoadProperty(StartTimeProperty, .GetValue(20))
          LoadProperty(EndTimeProperty, .GetValue(21))
          LoadProperty(WrapTimeTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
          LoadProperty(WrapTimeProperty, .GetValue(23))
          LoadProperty(IsOwnerProperty, .GetBoolean(24))
          LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(25)))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(26)))
          LoadProperty(IsCancelledProperty, .GetBoolean(27))
          LoadProperty(PopoverContentProperty, .GetString(28))
          LoadProperty(SlugLayoutInSchedulerProperty, .GetBoolean(29))
          LoadProperty(PendingDeleteProperty, .GetBoolean(30))
          LoadProperty(CallTimeMatchProperty, .GetBoolean(31))
          LoadProperty(OnAirStartMatchProperty, .GetBoolean(32))
          LoadProperty(OnAirEndMatchProperty, .GetBoolean(33))
          LoadProperty(WrapTimeMatchProperty, .GetBoolean(34))
          LoadProperty(RoomMatchProperty, .GetBoolean(35))
          LoadProperty(ResourceIDPSAProperty, Singular.Misc.ZeroNothing(.GetInt32(36)))
          LoadProperty(ResourceNamePSAProperty, .GetString(37))
          LoadProperty(ResourceIDOwnerProperty, Singular.Misc.ZeroNothing(.GetInt32(38)))
          LoadProperty(RSCallTimeProperty, .GetValue(39))
          LoadProperty(RSOnAirTimeStartProperty, .GetValue(40))
          LoadProperty(RSOnAirTimeEndProperty, .GetValue(41))
          LoadProperty(RSWrapTimeProperty, .GetValue(42))
          LoadProperty(OwnerCommentsProperty, .GetString(43))
          LoadProperty(IsReconciledProperty, .GetBoolean(44))
          LoadProperty(GenRefNumberProperty, .GetInt64(45))
          LoadProperty(GenreDescProperty, .GetString(46))
          LoadProperty(SeriesTitleProperty, .GetString(47))
          LoadProperty(ProductionTitleProperty, .GetString(48))
          LoadProperty(CreatedByProperty, .GetInt32(49))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(50))
          LoadProperty(ModifiedByProperty, .GetInt32(51))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(52))
        End With
      End Using
      LoadProperty(LoadedProductionAreaStatusIDProperty, ProductionAreaStatusID)


      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      If Me.IsNew Then
        cm.CommandText = "InsProcsWeb.insRoomScheduleSimple"
      Else
        cm.CommandText = "UpdProcsWeb.updRoomScheduleSimple"
      End If

      AddPrimaryKeyParam(cm, RoomScheduleIDProperty)
      AddInputOutputParam(cm, ProductionSystemAreaIDOwnerProperty)
      AddInputOutputParam(cm, ProductionSystemAreaIDProperty)
      AddInputOutputParam(cm, ResourceBookingIDProperty)
      AddInputOutputParam(cm, CallTimeTimelineIDProperty)
      AddInputOutputParam(cm, OnAirTimeTimelineIDProperty)
      AddInputOutputParam(cm, WrapTimeTimelineIDProperty)
      AddInputOutputParam(cm, ProductionIDProperty)

      cm.Parameters.AddWithValue("@CallTime", GetProperty(CallTimeProperty))
      cm.Parameters.AddWithValue("@StartTime", GetProperty(StartTimeProperty))
      cm.Parameters.AddWithValue("@EndTime", GetProperty(EndTimeProperty))
      cm.Parameters.AddWithValue("@WrapTime", GetProperty(WrapTimeProperty))
      cm.Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
      cm.Parameters.AddWithValue("@Room", GetProperty(RoomProperty))
      cm.Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@ModifiedByName", OBLib.Security.Settings.CurrentUser.LoginName)
      cm.Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
      cm.Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
      cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      cm.Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@ResourceBookingDescription", Me.CalculateResourceBookingDescription)
      cm.Parameters.AddWithValue("@PopoverContent", Me.CalculatePopoverDescription)
      'new properties
      cm.Parameters.AddWithValue("@RoomScheduleTypeID", NothingDBNull(GetProperty(RoomScheduleTypeIDProperty)))
      cm.Parameters.AddWithValue("@AdHocBookingID", NothingDBNull(GetProperty(AdHocBookingIDProperty)))
      cm.Parameters.AddWithValue("@DebtorID", NothingDBNull(GetProperty(DebtorIDProperty)))
      cm.Parameters.AddWithValue("@Debtor", (GetProperty(DebtorProperty)))
      cm.Parameters.AddWithValue("@ProductionAreaStatusID", NothingDBNull(GetProperty(ProductionAreaStatusIDProperty)))
      cm.Parameters.AddWithValue("@ProductionAreaStatus", (GetProperty(ProductionAreaStatusProperty)))
      cm.Parameters.AddWithValue("@StatusCssClass", (GetProperty(StatusCssClassProperty)))
      cm.Parameters.AddWithValue("@AreaComments", (GetProperty(AreaCommentsProperty)))
      cm.Parameters.AddWithValue("@IsOwner", NothingDBNull(GetProperty(IsOwnerProperty)))
      cm.Parameters.AddWithValue("@IsCancelled", NothingDBNull(GetProperty(IsCancelledProperty)))
      cm.Parameters.AddWithValue("@SlugLayoutInScheduler", NothingDBNull(GetProperty(SlugLayoutInSchedulerProperty)))
      cm.Parameters.AddWithValue("@PendingDelete", NothingDBNull(GetProperty(PendingDeleteProperty)))
      cm.Parameters.AddWithValue("@RSCallTime", NothingDBNull(GetProperty(RSCallTimeProperty)))
      cm.Parameters.AddWithValue("@RSOnAirTimeStart", NothingDBNull(GetProperty(RSOnAirTimeStartProperty)))
      cm.Parameters.AddWithValue("@RSOnAirTimeEnd", NothingDBNull(GetProperty(RSOnAirTimeEndProperty)))
      cm.Parameters.AddWithValue("@RSWrapTime", NothingDBNull(GetProperty(RSWrapTimeProperty)))
      cm.Parameters.AddWithValue("@OwnerComments", (GetProperty(OwnerCommentsProperty)))
      cm.Parameters.AddWithValue("@IsReconciled", NothingDBNull(GetProperty(IsReconciledProperty)))
      cm.Parameters.AddWithValue("@LoadedProductionAreaStatusID", NothingDBNull(GetProperty(LoadedProductionAreaStatusIDProperty)))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(RoomScheduleIDProperty, cm.Parameters("@RoomScheduleID").Value)
                 LoadProperty(ProductionSystemAreaIDProperty, cm.Parameters("@ProductionSystemAreaID").Value)
                 LoadProperty(ResourceBookingIDProperty, cm.Parameters("@ResourceBookingID").Value)
                 LoadProperty(ProductionSystemAreaIDOwnerProperty, cm.Parameters("@ProductionSystemAreaIDOwner").Value)
                 LoadProperty(CallTimeTimelineIDProperty, cm.Parameters("@CallTimeTimelineID").Value)
                 LoadProperty(OnAirTimeTimelineIDProperty, cm.Parameters("@OnAirTimeTimelineID").Value)
                 LoadProperty(WrapTimeTimelineIDProperty, cm.Parameters("@WrapTimeTimelineID").Value)
                 LoadProperty(ProductionIDProperty, cm.Parameters("@ProductionID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
      UpdateChild(GetProperty(RoomSchedulePHRListProperty))
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      'cm.Parameters.AddWithValue("@RoomScheduleID", GetProperty(RoomScheduleIDProperty))
    End Sub

#End Region

    Private Function CalculateResourceBookingDescription() As String

      If CompareSafe(SystemID, CType(OBLib.CommonData.Enums.System.PlayoutOperations, Integer)) Then
        Dim channels As String = ""
        For Each rssn As RoomScheduleScheduleNumber In Me.RoomScheduleScheduleNumberList
          channels &= rssn.ChannelShortName & IIf(channels.Length = 0, "", ", ")
        Next
        Dim crew As String = ""
        For Each rssn As RoomSchedulePHR In Me.RoomSchedulePHRList
          crew &= rssn.HumanResource & " (" & rssn.CallTime.Value.ToString("HH:mm") & " - " & rssn.WrapTime.Value.ToString("HH:mm") & ")" & IIf(crew.Length = 0, "", ", ")
        Next
        Return channels & " - " & SeriesTitle & ": " & crew
      Else
        Return SeriesTitle
      End If

    End Function

    Private Function CalculatePopoverDescription() As String

      Dim crew As String = ""
      For Each rssn As RoomSchedulePHR In Me.RoomSchedulePHRList
        crew &= IIf(crew.Length = 0, "", vbCrLf) & rssn.HumanResource & ": " & rssn.CallTime.Value.ToString("HH:mm") & " - " & rssn.WrapTime.Value.ToString("HH:mm") & ")"
      Next
      Return Room & ": " & CallTime.Value.ToString("HH:mm") & " - " & WrapTime.Value.ToString("HH:mm") & vbCrLf & crew

    End Function

  End Class

End Namespace