﻿' Generated 22 Dec 2016 07:40 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Scheduling.Rooms

  <Serializable()>
  Public Class RoomScheduleList
    Inherits OBBusinessListBase(Of RoomScheduleList, RoomSchedule)

#Region " Business Methods "

    Public Function GetItem(RoomScheduleID As Integer) As RoomSchedule

      For Each child As RoomSchedule In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property RoomScheduleID As Integer?
      Public Property ProductionSystemAreaID As Integer?
      Public Sub New(RoomScheduleID As Integer?, ProductionSystemAreaID As Integer?)
        Me.RoomScheduleID = RoomScheduleID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewRoomScheduleList() As RoomScheduleList

      Return New RoomScheduleList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetRoomScheduleList(RoomScheduleID As Integer?, ProductionSystemAreaID As Integer?) As RoomScheduleList

      Return DataPortal.Fetch(Of RoomScheduleList)(New Criteria(RoomScheduleID, ProductionSystemAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(RoomSchedule.GetRoomSchedule(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'not concerned with checking rules as this editable object is used as a read-only object because we need the "read-only" fields to be sent to/from the server without the values being lost

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRoomScheduleList"
            cm.Parameters.AddWithValue("@RoomScheduleID", NothingDBNull(crit.RoomScheduleID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@CurrentUserID", NothingDBNull(OBLib.Security.Settings.CurrentUser.UserID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace