﻿' Generated 16 Jun 2016 14:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Slugs
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Scheduling.Rooms

  <Serializable()>
  Public Class RoomScheduleArea
    Inherits OBBusinessBase(Of RoomScheduleArea)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RoomScheduleAreaBO.RoomScheduleAreaBOToString(self)")

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleTypeID, "Room Schedule Type")
    ''' <summary>
    ''' Gets and sets the Room Schedule Type value
    ''' </summary>
    <Display(Name:="Room Schedule Type", Description:=""),
    Required(ErrorMessage:="Room Schedule Type required"),
    DropDownWeb(GetType(RORoomScheduleTypeList))>
    Public Property RoomScheduleTypeID() As Integer?
      Get
        Return GetProperty(RoomScheduleTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingID, "Ad Hoc Booking")
    ''' <summary>
    ''' Gets and sets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking", Description:="")>
    Public Property AdHocBookingID() As Integer?
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AdHocBookingIDProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:=""),
    Required(AllowEmptyStrings:=False),
    SetExpression("RoomScheduleAreaBO.TitleSet(self)")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDOwnerProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaIDOwner, "Production System Area", 0)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:=""),
    Required(ErrorMessage:="Owner required")>
    Public Property ProductionSystemAreaIDOwner() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDOwnerProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionSystemAreaIDOwnerProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
    Required(ErrorMessage:="Room required"),
    DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="RoomScheduleAreaBO.setRoomCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleAreaBO.triggerRoomAutoPopulate",
                AfterFetchJS:="RoomScheduleAreaBO.afterRoomRefreshAjax",
                LookupMember:="Room", ValueMember:="RoomID", DropDownColumns:={"Room", "IsAvailableString"},
                OnItemSelectJSFunction:="RoomScheduleAreaBO.onRoomSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("RoomScheduleAreaBO.RoomIDSet(self)")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Debtor")
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:=""),
    DropDownWeb(GetType(RODebtorFindList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="RoomScheduleAreaBO.setDebtorCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleAreaBO.triggerDebtorAutoPopulate",
                AfterFetchJS:="RoomScheduleAreaBO.afterDebtorRefreshAjax",
                OnItemSelectJSFunction:="RoomScheduleAreaBO.onDebtorSelected",
                LookupMember:="Debtor", DisplayMember:="Debtor", ValueMember:="DebtorID",
                DropDownCssClass:="debtor-dropdown", DropDownColumns:={"Debtor", "ContactName", "ContactNumber"}),
    SetExpression("RoomScheduleAreaBO.DebtorIDSet(self)")>
    Public Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DebtorIDProperty, Value)
      End Set
    End Property

    Public Shared DebtorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Debtor, "Debtor")
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="")>
    Public Property Debtor() As String
      Get
        Return GetProperty(DebtorProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DebtorProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Production System Area", 0)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:=""),
    Required(ErrorMessage:="Production System Area required")>
    Public Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, " Sub-Dept")
    ''' <summary>
    ''' Gets and sets the  System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID"),
    SetExpression("RoomScheduleAreaBO.SystemIDSet(self)")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, " Area")
    ''' <summary>
    ''' Gets and sets the  Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required"),
    DropDownWeb(GetType(ROUserSystemAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", ThisFilterMember:="SystemID"),
    SetExpression("RoomScheduleAreaBO.ProductionAreaIDSet(self)")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "Status")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:=""),
    Required(ErrorMessage:="Status required"),
    DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaStatusSelectList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="RoomScheduleAreaBO.setStatusCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleAreaBO.triggerStatusAutoPopulate",
                AfterFetchJS:="RoomScheduleAreaBO.afterStatusRefreshAjax",
                LookupMember:="ProductionAreaStatus", ValueMember:="ProductionAreaStatusID", DropDownColumns:={"ProductionAreaStatus"},
                OnItemSelectJSFunction:="RoomScheduleAreaBO.onStatusSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("RoomScheduleAreaBO.ProductionAreaStatusIDSet(self)")>
    Public Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Status")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaStatusProperty, Value)
      End Set
    End Property

    Public Shared StatusSetByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.StatusSetByUserID, "Status Set By User")
    ''' <summary>
    ''' Gets and sets the Status Set By User value
    ''' </summary>
    <Display(Name:="Status Set By User", Description:="")>
    Public Property StatusSetByUserID() As Integer?
      Get
        Return GetProperty(StatusSetByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(StatusSetByUserIDProperty, Value)
      End Set
    End Property

    Public Shared StatusSetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StatusSetDate, "Status Set Date")
    ''' <summary>
    ''' Gets and sets the Status Set Date value
    ''' </summary>
    <Display(Name:="Status Set Date", Description:="")>
    Public Property StatusSetDate As DateTime?
      Get
        Return GetProperty(StatusSetDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StatusSetDateProperty, Value)
      End Set
    End Property

    Public Shared AreaCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AreaComments, "Comments")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public Property AreaComments() As String
      Get
        Return GetProperty(AreaCommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AreaCommentsProperty, Value)
      End Set
    End Property

    Public Shared CallTimeTimelineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CallTimeTimelineID, "Call Time", 0)
    ''' <summary>
    ''' Gets and sets the Call Time Timeline value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    Required(ErrorMessage:="Call Time required")>
    Public Property CallTimeTimelineID() As Integer
      Get
        Return GetProperty(CallTimeTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CallTimeTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CallTime, "Call Time")
    ''' <summary>
    ''' Gets and sets the Call Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    Required(ErrorMessage:="Call Time required"),
    SetExpression("RoomScheduleAreaBO.CallTimeStartSet(self)")>
    Public Property CallTime As DateTime?
      Get
        Return GetProperty(CallTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeProperty, Value)
      End Set
    End Property

    Public Shared OnAirTimeTimelineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OnAirTimeTimelineID, "On Air Time", 0)
    ''' <summary>
    ''' Gets and sets the On Air Time Timeline value
    ''' </summary>
    <Display(Name:="On Air Time", Description:=""),
    Required(ErrorMessage:="On Air Time required")>
    Public Property OnAirTimeTimelineID() As Integer
      Get
        Return GetProperty(OnAirTimeTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OnAirTimeTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared OnAirTimeStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OnAirTimeStart, "Start Time")
    ''' <summary>
    ''' Gets and sets the On Air Time Start value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
    SetExpression("RoomScheduleAreaBO.StartDateTimeSet(self)")>
    Public Property OnAirTimeStart As DateTime?
      Get
        Return GetProperty(OnAirTimeStartProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OnAirTimeStartProperty, Value)
      End Set
    End Property

    Public Shared OnAirTimeEndProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OnAirTimeEnd, "End Time")
    ''' <summary>
    ''' Gets and sets the On Air Time End value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
    SetExpression("RoomScheduleAreaBO.EndDateTimeSet(self)")>
    Public Property OnAirTimeEnd As DateTime?
      Get
        Return GetProperty(OnAirTimeEndProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OnAirTimeEndProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeTimelineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WrapTimeTimelineID, "Wrap Time", 0)
    ''' <summary>
    ''' Gets and sets the Wrap Time Timeline value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    Required(ErrorMessage:="Wrap Time required")>
    Public Property WrapTimeTimelineID() As Integer
      Get
        Return GetProperty(WrapTimeTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WrapTimeTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.WrapTime, "Wrap Time")
    ''' <summary>
    ''' Gets and sets the Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    Required(ErrorMessage:="Wrap Time required"),
    SetExpression("RoomScheduleAreaBO.WrapTimeEndSet(self)")>
    Public Property WrapTime As DateTime?
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeProperty, Value)
      End Set
    End Property

    Public Shared IsOwnerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsOwner, "Is Owner", False)
    ''' <summary>
    ''' Gets and sets the Is Owner value
    ''' </summary>
    <Display(Name:="Is Owner", Description:="")>
    Public Property IsOwner() As Boolean
      Get
        Return GetProperty(IsOwnerProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsOwnerProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "Resource Booking", 0)
    ''' <summary>
    ''' Gets and sets the Resource Booking value
    ''' </summary>
    <Display(Name:="Resource Booking", Description:=""),
    Required(ErrorMessage:="Resource Booking required")>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingTypeID, "Resource Booking Type")
    ''' <summary>
    ''' Gets and sets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type", Description:=""),
    Required(ErrorMessage:="Resource Booking Type required")>
    Public Property ResourceBookingTypeID() As Integer?
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Resource Booking Description")
    ''' <summary>
    ''' Gets and sets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Resource Booking Description", Description:=""),
    Required(AllowEmptyStrings:=False)>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Is Cancelled value
    ''' </summary>
    <Display(Name:="Is Cancelled", Description:=""),
    Required(ErrorMessage:="Is Cancelled required")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared IsTBCProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTBC, "Is TBC", False)
    ''' <summary>
    ''' Gets and sets the Is TBC value
    ''' </summary>
    <Display(Name:="Is TBC", Description:=""),
    Required(ErrorMessage:="Is TBC required")>
    Public Property IsTBC() As Boolean
      Get
        Return GetProperty(IsTBCProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsTBCProperty, Value)
      End Set
    End Property

    Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "Status Css Class")
    ''' <summary>
    ''' Gets and sets the Status Css Class value
    ''' </summary>
    <Display(Name:="Status Css Class", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Status Css Class cannot be blank")>
    Public Property StatusCssClass() As String
      Get
        Return GetProperty(StatusCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCssClassProperty, Value)
      End Set
    End Property

    Public Shared PopoverContentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PopoverContent, "Popover Content")
    ''' <summary>
    ''' Gets and sets the Popover Content value
    ''' </summary>
    <Display(Name:="Popover Content", Description:="")>
    Public Property PopoverContent() As String
      Get
        Return GetProperty(PopoverContentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PopoverContentProperty, Value)
      End Set
    End Property

    Public Shared SlugLayoutInSchedulerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SlugLayoutInScheduler, "Slug Layout In Scheduler", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="Slug Layout In Scheduler", Description:=""),
    Required(ErrorMessage:="Slug Layout In Scheduler required")>
    Public Property SlugLayoutInScheduler() As Boolean
      Get
        Return GetProperty(SlugLayoutInSchedulerProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SlugLayoutInSchedulerProperty, Value)
      End Set
    End Property

    Public Shared PendingDeleteProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PendingDelete, "PendingDelete", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="PendingDelete", Description:="")>
    Public ReadOnly Property PendingDelete() As Boolean
      Get
        Return GetProperty(PendingDeleteProperty)
      End Get
    End Property

    Public Shared CallTimeMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CallTimeMatch, "CallTimeMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="CallTimeMatch", Description:="")>
    Public ReadOnly Property CallTimeMatch() As Boolean
      Get
        Return GetProperty(CallTimeMatchProperty)
      End Get
    End Property

    Public Shared OnAirStartMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnAirStartMatch, "OnAirStartMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="OnAirStartMatch", Description:="")>
    Public ReadOnly Property OnAirStartMatch() As Boolean
      Get
        Return GetProperty(OnAirStartMatchProperty)
      End Get
    End Property

    Public Shared OnAirEndMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnAirEndMatch, "OnAirEndMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="OnAirEndMatch", Description:="")>
    Public ReadOnly Property OnAirEndMatch() As Boolean
      Get
        Return GetProperty(OnAirEndMatchProperty)
      End Get
    End Property

    Public Shared WrapTimeMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.WrapTimeMatch, "WrapTimeMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="WrapTimeMatch", Description:="")>
    Public ReadOnly Property WrapTimeMatch() As Boolean
      Get
        Return GetProperty(WrapTimeMatchProperty)
      End Get
    End Property

    Public Shared RSCallTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RSCallTime, "Call Time")
    ''' <summary>
    ''' Gets and sets the Call Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:="")>
    Public ReadOnly Property RSCallTime As DateTime?
      Get
        Return GetProperty(RSCallTimeProperty)
      End Get
    End Property

    Public Shared RSOnAirTimeStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RSOnAirTimeStart, "Start Time")
    ''' <summary>
    ''' Gets and sets the On Air Time Start value
    ''' </summary>
    <Display(Name:="Start Time", Description:="")>
    Public ReadOnly Property RSOnAirTimeStart As DateTime?
      Get
        Return GetProperty(RSOnAirTimeStartProperty)
      End Get
    End Property

    Public Shared RSOnAirTimeEndProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RSOnAirTimeEnd, "End Time")
    ''' <summary>
    ''' Gets and sets the On Air Time End value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public ReadOnly Property RSOnAirTimeEnd As DateTime?
      Get
        Return GetProperty(RSOnAirTimeEndProperty)
      End Get
    End Property

    Public Shared RSWrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.RSWrapTime, "Wrap Time")
    ''' <summary>
    ''' Gets and sets the Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:="")>
    Public ReadOnly Property RSWrapTime As DateTime?
      Get
        Return GetProperty(RSWrapTimeProperty)
      End Get
    End Property

    Public Shared OwnerCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OwnerComments, "Comments")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public Property OwnerComments() As String
      Get
        Return GetProperty(OwnerCommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OwnerCommentsProperty, Value)
      End Set
    End Property

    Public Shared IsReconciledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsReconciled, "IsReconciled", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="IsReconciled", Description:="")>
    Public Property IsReconciled() As Boolean
      Get
        Return GetProperty(IsReconciledProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsReconciledProperty, value)
      End Set
    End Property

#End Region

#Region " Other Properties "

    Public Shared BookingTimesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingTimes, "Booking Times", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Booking Times"), AlwaysClean>
    Public Property BookingTimes() As String
      Get
        Return GetProperty(BookingTimesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BookingTimesProperty, Value)
      End Set
    End Property

    Public Shared EventFetchedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.EventFetched, "Event Fetched", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Event Fetched"), AlwaysClean>
    Public Property EventFetched() As Boolean
      Get
        Return GetProperty(EventFetchedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(EventFetchedProperty, Value)
      End Set
    End Property

    Public Shared LoadedProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LoadedProductionAreaStatusID, "Status")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property LoadedProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(LoadedProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LoadedProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared SelectedProductionHumanResourceGuidProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedProductionHumanResourceGuid, "Selected Production HR", "")
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Selected Production HR"), AlwaysClean>
    Public Property SelectedProductionHumanResourceGuid() As String
      Get
        Return GetProperty(SelectedProductionHumanResourceGuidProperty)
      End Get
      Set(value As String)
        SetProperty(SelectedProductionHumanResourceGuidProperty, value)
      End Set
    End Property

    Public Shared PermanentDeleteProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PermanentDelete, "PermanentDelete", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="PermanentDelete", Description:="")>
    Public ReadOnly Property PermanentDelete() As Boolean
      Get
        Return GetProperty(PermanentDeleteProperty)
      End Get
    End Property

    Public Shared RoomMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomMatch, "RoomMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="RoomMatch", Description:="")>
    Public ReadOnly Property RoomMatch() As Boolean
      Get
        Return GetProperty(RoomMatchProperty)
      End Get
    End Property

    Public Shared ResourceIDPSAProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceIDPSA, "ResourceIDPSA")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="ResourceIDPSA", Description:="")>
    Public Property ResourceIDPSA() As Integer?
      Get
        Return GetProperty(ResourceIDPSAProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDPSAProperty, Value)
      End Set
    End Property

    Public Shared ResourceNamePSAProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceNamePSA, "ResourceNamePSA", "")
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="ResourceNamePSA"), AlwaysClean>
    Public Property ResourceNamePSA() As String
      Get
        Return GetProperty(ResourceNamePSAProperty)
      End Get
      Set(value As String)
        SetProperty(ResourceNamePSAProperty, value)
      End Set
    End Property

    Public Shared RoomResourceIDOwnerProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomResourceIDOwner, "RoomResourceIDOwner")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="RoomResourceIDOwner", Description:="")>
    Public Property RoomResourceIDOwner() As Integer?
      Get
        Return GetProperty(RoomResourceIDOwnerProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomResourceIDOwnerProperty, Value)
      End Set
    End Property

    Public Shared CancellationSelectedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CancellationSelected, "CancellationSelected", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="CancellationSelected", Description:="")>
    Public Property CancellationSelected() As Boolean
      Get
        Return GetProperty(CancellationSelectedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CancellationSelectedProperty, Value)
      End Set
    End Property

    Public Shared CancellationReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancellationReason, "Cancellation Reason", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Cancellation Reason"), AlwaysClean>
    Public Property CancellationReason() As String
      Get
        Return GetProperty(CancellationReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancellationReasonProperty, Value)
      End Set
    End Property

    Public Shared CancellationDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.CancellationDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Cancellation Date"), AlwaysClean>
    Public Property CancellationDate() As DateTime?
      Get
        Return GetProperty(CancellationDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancellationDateProperty, Value)
      End Set
    End Property

    Public Shared ResourceNameOwnerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceNameOwner, "ResourceNameOwner", "")
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="ResourceNameOwner"), AlwaysClean>
    Public Property ResourceNameOwner() As String
      Get
        Return GetProperty(ResourceNameOwnerProperty)
      End Get
      Set(value As String)
        SetProperty(ResourceNameOwnerProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared RoomScheduleAreaProductionHumanResourceListProperty As PropertyInfo(Of RoomScheduleAreaProductionHumanResourceList) = RegisterProperty(Of RoomScheduleAreaProductionHumanResourceList)(Function(c) c.RoomScheduleAreaProductionHumanResourceList, "Room Schedule Area Production Human Resource List")
    Public ReadOnly Property RoomScheduleAreaProductionHumanResourceList() As RoomScheduleAreaProductionHumanResourceList
      Get
        If GetProperty(RoomScheduleAreaProductionHumanResourceListProperty) Is Nothing Then
          LoadProperty(RoomScheduleAreaProductionHumanResourceListProperty, Scheduling.Rooms.RoomScheduleAreaProductionHumanResourceList.NewRoomScheduleAreaProductionHumanResourceList())
        End If
        Return GetProperty(RoomScheduleAreaProductionHumanResourceListProperty)
      End Get
    End Property

    Public Shared RoomScheduleAreaProductionHRBookingListProperty As PropertyInfo(Of RoomScheduleAreaProductionHRBookingList) = RegisterProperty(Of RoomScheduleAreaProductionHRBookingList)(Function(c) c.RoomScheduleAreaProductionHRBookingList, "Room Schedule Area Production HR Booking List")
    Public ReadOnly Property RoomScheduleAreaProductionHRBookingList() As RoomScheduleAreaProductionHRBookingList
      Get
        If GetProperty(RoomScheduleAreaProductionHRBookingListProperty) Is Nothing Then
          LoadProperty(RoomScheduleAreaProductionHRBookingListProperty, Scheduling.Rooms.RoomScheduleAreaProductionHRBookingList.NewRoomScheduleAreaProductionHRBookingList())
        End If
        Return GetProperty(RoomScheduleAreaProductionHRBookingListProperty)
      End Get
    End Property

    Public Shared ClashDetailListProperty As PropertyInfo(Of List(Of Helpers.ResourceHelpers.ClashDetail)) = RegisterProperty(Of List(Of Helpers.ResourceHelpers.ClashDetail))(Function(c) c.ClashDetailList, "Clash List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ClashDetailList() As List(Of Helpers.ResourceHelpers.ClashDetail)
      Get
        If GetProperty(ClashDetailListProperty) Is Nothing Then
          LoadProperty(ClashDetailListProperty, New List(Of Helpers.ResourceHelpers.ClashDetail))
        End If
        Return GetProperty(ClashDetailListProperty)
      End Get
    End Property

    Public Shared SlugItemListProperty As PropertyInfo(Of SlugItemList) = RegisterProperty(Of SlugItemList)(Function(c) c.SlugItemList, "SlugItemList")
    Public ReadOnly Property SlugItemList() As SlugItemList
      Get
        If GetProperty(SlugItemListProperty) Is Nothing Then
          LoadProperty(SlugItemListProperty, New SlugItemList)
        End If
        Return GetProperty(SlugItemListProperty)
      End Get
    End Property

    Public Shared RoomScheduleChannelListProperty As PropertyInfo(Of Scheduling.Rooms.RoomScheduleChannelList) = RegisterProperty(Of Scheduling.Rooms.RoomScheduleChannelList)(Function(c) c.RoomScheduleChannelList, "RoomScheduleChannelList")
    Public ReadOnly Property RoomScheduleChannelList() As Scheduling.Rooms.RoomScheduleChannelList
      Get
        If GetProperty(RoomScheduleChannelListProperty) Is Nothing Then
          LoadProperty(RoomScheduleChannelListProperty, Scheduling.Rooms.RoomScheduleChannelList.NewRoomScheduleChannelList())
        End If
        Return GetProperty(RoomScheduleChannelListProperty)
      End Get
    End Property

    Public Shared ROObjectAuditTrailListProperty As PropertyInfo(Of OBLib.Audit.ROObjectAuditTrailList) = RegisterProperty(Of OBLib.Audit.ROObjectAuditTrailList)(Function(c) c.ROObjectAuditTrailList, "ROObjectAuditTrailList")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ROObjectAuditTrailList() As OBLib.Audit.ROObjectAuditTrailList
      Get
        If GetProperty(ROObjectAuditTrailListProperty) Is Nothing Then
          LoadProperty(ROObjectAuditTrailListProperty, New OBLib.Audit.ROObjectAuditTrailList)
        End If
        Return GetProperty(ROObjectAuditTrailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Sub UpdateCrewBookings()

      'Positions
      Dim psaSettings = CommonData.Lists.ROSystemProductionAreaDisciplinePositionTypeListFlat.Where(Function(d) CompareSafe(d.SystemID, Me.SystemID) AndAlso CompareSafe(d.ProductionAreaID, Me.ProductionAreaID)).ToList
      If psaSettings.Count > 0 Then
        For Each bkng As RoomScheduleAreaProductionHRBooking In Me.RoomScheduleAreaProductionHRBookingList
          Dim positionsString As String = ""
          Dim phrList As List(Of RoomScheduleAreaProductionHumanResource) = Me.RoomScheduleAreaProductionHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, bkng.HumanResourceID)).ToList
          For Each phr As RoomScheduleAreaProductionHumanResource In phrList
            Dim disciplineSettings = psaSettings.Where(Function(d) CompareSafe(d.DisciplineID, phr.DisciplineID())).ToList
            If (disciplineSettings.Count > 0 AndAlso phr.PositionID IsNot Nothing) Then
              Dim positionTypeSettings = disciplineSettings.Where(Function(d) CompareSafe(phr.PositionID, phr.PositionID)).ToList
              If positionTypeSettings.Count = 1 Then
                Dim setting = positionTypeSettings(0)
                If setting.IncludeInBookingDescription Then
                  If (positionsString.Length > 0) Then
                    positionsString &= ", " + setting.PositionShortName
                  Else
                    positionsString = setting.PositionShortName
                  End If
                End If
              End If
            End If
          Next
          'Update Positions
          If (positionsString = "") Then
            bkng.ResourceBookingDescription = Me.ResourceNamePSA + " - " + Me.ResourceBookingDescription
          Else
            bkng.ResourceBookingDescription = positionsString + ": " + Me.ResourceNamePSA + " - " + Me.ResourceBookingDescription
          End If
        Next
      End If

      'Statuses
      For Each bkng As RoomScheduleAreaProductionHRBooking In Me.RoomScheduleAreaProductionHRBookingList
        Dim phrList As List(Of RoomScheduleAreaProductionHumanResource) = Me.RoomScheduleAreaProductionHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, bkng.HumanResourceID)).ToList
        Dim cancellationCount As Integer = phrList.Where(Function(d) d.IsCancelled).Count
        bkng.StatusCssClass = Me.StatusCssClass
        'If Reconciled
        If (Me.ProductionAreaStatusID = 4 AndAlso cancellationCount = 0) Then
          bkng.IsCancelled = False
          'If Cancelled
        ElseIf Me.ProductionAreaStatusID = 5 Then
          bkng.IsCancelled = True
        Else
          If (cancellationCount = 0) Then
            bkng.IsCancelled = False
          Else
            bkng.IsCancelled = True
          End If
        End If
      Next

    End Sub

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Title.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Room Schedule Area")
        Else
          Return String.Format("Blank {0}", "Room Schedule Area")
        End If
      Else
        Return Me.Title
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

    Public Sub LoadBookingTimes()
      LoadProperty(BookingTimesProperty, GetSummarisedBookingTimes)
    End Sub

    Public Function GetSummarisedBookingTimes() As String

      Dim ct As String = ""
      If Me.CallTime IsNot Nothing Then
        ct = Me.CallTime.Value.ToString("dd MMM yy HH:mm")
      End If

      Dim wt As String = ""
      If Me.WrapTime IsNot Nothing Then
        wt = Me.WrapTime.Value.ToString("dd MMM yy HH:mm")
      End If

      Return ct & IIf(ct.Trim <> "", " - ", "") & wt

    End Function

    Public Sub SetPermanentDelete(Value As Boolean)
      SetProperty(PermanentDeleteProperty, Value)
    End Sub

    Public Sub AfterRoomScheduleAreaSaved()

      Dim afterSaveCmd As New Singular.CommandProc("CmdProcs.cmdProductionSystemAreaAfterSaved",
                                                   New String() {"@ProductionSystemAreaID", "@CurrentUserID"},
                                                   New Object() {Me.ProductionSystemAreaID, OBLib.Security.Settings.CurrentUserID})
      afterSaveCmd.UseTransaction = True
      afterSaveCmd.Execute()

    End Sub

    Public Sub UpdateReconciled(Silent As Boolean)
      If Silent Then
        LoadProperty(IsReconciledProperty, IIf(Singular.Misc.CompareSafe(ProductionAreaStatusID, CType(OBLib.CommonData.Enums.ProductionAreaStatus.Reconciled, Integer) AndAlso Not IsDirty), True, False))
      Else
        SetProperty(IsReconciledProperty, IIf(Singular.Misc.CompareSafe(ProductionAreaStatusID, CType(OBLib.CommonData.Enums.ProductionAreaStatus.Reconciled, Integer) AndAlso Not IsDirty), True, False))
      End If
    End Sub

    Public Sub UnReconcile()
      ProductionAreaStatusID = OBLib.CommonData.Enums.ProductionAreaStatus.[New]
      UpdateReconciled(False)
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(RoomScheduleIDProperty)
        .JavascriptRuleFunctionName = "RoomScheduleAreaBO.RoomScheduleIDValid"
        .ServerRuleFunction = AddressOf RoomScheduleIDValid
        .AddTriggerProperty(RoomIDProperty)
        .AddTriggerProperty(OnAirTimeStartProperty)
        .AddTriggerProperty(OnAirTimeEndProperty)
        .AffectedProperties.Add(RoomIDProperty)
        .AffectedProperties.Add(OnAirTimeStartProperty)
        .AffectedProperties.Add(OnAirTimeEndProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(CallTimeProperty)
        .JavascriptRuleFunctionName = "RoomScheduleAreaBO.CallTimeValid"
        .ServerRuleFunction = AddressOf CallTimeValid
        .AddTriggerProperty(OnAirTimeEndProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(OnAirTimeStartProperty)
        .JavascriptRuleFunctionName = "RoomScheduleAreaBO.OnAirTimeStartValid"
        .ServerRuleFunction = AddressOf StartDateTimeValid
        .AddTriggerProperty(OnAirTimeEndProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(OnAirTimeEndProperty)
        .JavascriptRuleFunctionName = "RoomScheduleAreaBO.OnAirEndTimeValid"
        .ServerRuleFunction = AddressOf EndDateTimeValid
        .AddTriggerProperty(OnAirTimeStartProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(WrapTimeProperty)
        .JavascriptRuleFunctionName = "RoomScheduleAreaBO.WrapTimeValid"
        .ServerRuleFunction = AddressOf WrapTimeValid
        .AddTriggerProperty(OnAirTimeStartProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(WrapTimeProperty)
        .JavascriptRuleFunctionName = "RoomScheduleAreaBO.WrapTimeValid"
        .ServerRuleFunction = AddressOf WrapTimeValid
        .AddTriggerProperty(OnAirTimeStartProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(CancellationSelectedProperty)
        .JavascriptRuleFunctionName = "RoomScheduleAreaBO.CancellationSelectedValid"
        .ServerRuleFunction = AddressOf CancellationSelectedValid
        .AddTriggerProperties({CancellationReasonProperty, CancellationDateProperty})
        .AffectedProperties.AddRange({CancellationReasonProperty, CancellationDateProperty})

      End With

    End Sub

    Public Overridable Function RoomScheduleIDValid(RoomSchedule As RoomScheduleArea) As String
      Dim ErrorString As String = ""
      If RoomSchedule.ClashDetailList().Count > 0 Then
        ErrorString = "This booking is clashing with other bookings"
      End If
      Return ErrorString
    End Function

    Public Overridable Function CallTimeValid(RoomSchedule As RoomScheduleArea) As String
      Dim Err As String = ""
      If RoomSchedule.CallTime Is Nothing Then
        Err &= "Call Time is required"
      ElseIf RoomSchedule.CallTime IsNot Nothing And RoomSchedule.OnAirTimeStart IsNot Nothing Then
        If RoomSchedule.CallTime.Value > RoomSchedule.OnAirTimeStart.Value Then
          Err &= "Call Time must be before On Air Start Time"
        End If
      End If
      Return Err
    End Function

    Public Overridable Function StartDateTimeValid(RoomSchedule As RoomScheduleArea) As String
      Dim Err As String = ""
      If RoomSchedule.OnAirTimeStart Is Nothing Then
        Err &= "Booking End Date Time is required"
      ElseIf RoomSchedule.OnAirTimeStart IsNot Nothing And RoomSchedule.OnAirTimeEnd IsNot Nothing Then
        If RoomSchedule.OnAirTimeStart.Value > RoomSchedule.OnAirTimeEnd.Value Then
          Err &= "On Air Start Time must be before On Air End Time"
        End If
      End If
      Return Err
    End Function

    Public Overridable Function EndDateTimeValid(RoomSchedule As RoomScheduleArea) As String
      Dim Err As String = ""
      If RoomSchedule.OnAirTimeEnd Is Nothing Then
        Err &= "Booking End Date Time is required"
      ElseIf RoomSchedule.OnAirTimeStart IsNot Nothing And RoomSchedule.OnAirTimeEnd IsNot Nothing Then
        If RoomSchedule.OnAirTimeStart.Value > RoomSchedule.OnAirTimeEnd.Value Then
          Err &= "On Air End Time must be after On Air Start Time"
        End If
      End If
      Return Err
    End Function

    Public Overridable Function WrapTimeValid(RoomSchedule As RoomScheduleArea) As String
      Dim Err As String = ""
      If RoomSchedule.WrapTime Is Nothing Then
        Err &= "Wrap Time is required"
      ElseIf RoomSchedule.WrapTime IsNot Nothing And RoomSchedule.OnAirTimeEnd IsNot Nothing Then
        If RoomSchedule.WrapTime.Value < RoomSchedule.OnAirTimeEnd.Value Then
          Err &= "Wrap Time must be after On Air End Time"
        End If
      End If
      Return Err
    End Function

    Public Overridable Function CancellationSelectedValid(RoomSchedule As RoomScheduleArea) As String
      Dim Err As String = ""
      If RoomSchedule.CancellationSelected AndAlso (RoomSchedule.CancellationDate Is Nothing Or RoomSchedule.CancellationReason.Trim.Length = 0) Then
        Err &= "Cancellation details are required"
      End If
      Return Err
    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomScheduleArea() method.

    End Sub

    Public Shared Function NewRoomScheduleArea() As RoomScheduleArea

      Return DataPortal.CreateChild(Of RoomScheduleArea)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetRoomScheduleArea(dr As SafeDataReader) As RoomScheduleArea

      Dim r As New RoomScheduleArea()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
          LoadProperty(RoomScheduleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(AdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(TitleProperty, .GetString(4))
          'LoadProperty(OwningSystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          'LoadProperty(OwningProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(ProductionSystemAreaIDOwnerProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(DebtorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(DebtorProperty, .GetString(8))
          LoadProperty(ProductionSystemAreaIDProperty, .GetInt32(9))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(10)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(11)))
          LoadProperty(RoomProperty, .GetString(12))
          LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(ProductionAreaStatusProperty, .GetString(14))
          LoadProperty(StatusSetByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(StatusSetDateProperty, .GetValue(16))
          LoadProperty(AreaCommentsProperty, .GetString(17))
          LoadProperty(CallTimeTimelineIDProperty, .GetInt32(18))
          LoadProperty(CallTimeProperty, .GetValue(19))
          LoadProperty(OnAirTimeTimelineIDProperty, .GetInt32(20))
          LoadProperty(OnAirTimeStartProperty, .GetValue(21))
          LoadProperty(OnAirTimeEndProperty, .GetValue(22))
          LoadProperty(WrapTimeTimelineIDProperty, .GetInt32(23))
          LoadProperty(WrapTimeProperty, .GetValue(24))
          LoadProperty(IsOwnerProperty, .GetBoolean(25))
          LoadProperty(ResourceBookingIDProperty, .GetInt32(26))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(27)))
          LoadProperty(ResourceBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(28)))
          LoadProperty(ResourceBookingDescriptionProperty, .GetString(29))
          LoadProperty(IsCancelledProperty, .GetBoolean(30))
          LoadProperty(IsTBCProperty, .GetBoolean(31))
          LoadProperty(StatusCssClassProperty, .GetString(32))
          LoadProperty(PopoverContentProperty, .GetString(33))
          LoadProperty(SlugLayoutInSchedulerProperty, .GetBoolean(34))
          LoadProperty(LoadedProductionAreaStatusIDProperty, ZeroDBNull(.GetInt32(13)))
          LoadProperty(PendingDeleteProperty, .GetBoolean(35))
          LoadProperty(CallTimeMatchProperty, .GetBoolean(36))
          LoadProperty(OnAirStartMatchProperty, .GetBoolean(37))
          LoadProperty(OnAirEndMatchProperty, .GetBoolean(38))
          LoadProperty(WrapTimeMatchProperty, .GetBoolean(39))
          LoadProperty(RSCallTimeProperty, .GetValue(40))
          LoadProperty(RSOnAirTimeStartProperty, .GetValue(41))
          LoadProperty(RSOnAirTimeEndProperty, .GetValue(42))
          LoadProperty(RSWrapTimeProperty, .GetValue(43))
          LoadProperty(OwnerCommentsProperty, .GetString(44))
          LoadProperty(RoomMatchProperty, .GetBoolean(45))
          LoadProperty(ResourceIDPSAProperty, Singular.Misc.ZeroNothing(.GetInt32(46)))
          LoadProperty(ResourceNamePSAProperty, .GetString(47))
          LoadProperty(RoomResourceIDOwnerProperty, Singular.Misc.ZeroNothing(.GetInt32(48)))
          LoadProperty(IsReconciledProperty, .GetBoolean(49))
          LoadProperty(ResourceNameOwnerProperty, .GetString(50))
        End With
      End Using
      LoadBookingTimes()

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      Me.UpdateCrewBookings()

      AddPrimaryKeyParam(cm, RoomScheduleIDProperty)
      AddOutputParam(cm, ProductionSystemAreaIDProperty)
      AddOutputParam(cm, ProductionSystemAreaIDOwnerProperty)
      AddOutputParam(cm, CallTimeTimelineIDProperty)
      AddOutputParam(cm, OnAirTimeTimelineIDProperty)
      AddOutputParam(cm, WrapTimeTimelineIDProperty)
      AddOutputParam(cm, ResourceBookingIDProperty)

      cm.Parameters.AddWithValue("@RoomScheduleTypeID", NothingDBNull(GetProperty(RoomScheduleTypeIDProperty)))
      cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(GetProperty(ProductionIDProperty)))
      cm.Parameters.AddWithValue("@AdHocBookingID", NothingDBNull(GetProperty(AdHocBookingIDProperty)))
      cm.Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      'cm.Parameters.AddWithValue("@OwningSystemID", NothingDBNull(GetProperty(OwningSystemIDProperty)))
      'cm.Parameters.AddWithValue("@OwningProductionAreaID", NothingDBNull(GetProperty(OwningProductionAreaIDProperty)))
      cm.Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
      cm.Parameters.AddWithValue("@DebtorID", NothingDBNull(GetProperty(DebtorIDProperty)))
      cm.Parameters.AddWithValue("@Debtor", GetProperty(DebtorProperty))
      'cm.Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))
      cm.Parameters.AddWithValue("@SystemID", NothingDBNull(GetProperty(SystemIDProperty)))
      cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))
      cm.Parameters.AddWithValue("@ProductionAreaStatusID", NothingDBNull(GetProperty(ProductionAreaStatusIDProperty)))
      cm.Parameters.AddWithValue("@ProductionAreaStatus", GetProperty(ProductionAreaStatusProperty))
      'cm.Parameters.AddWithValue("@StatusSetByUserID", OBLib.Security.Settings.CurrentUserID)
      'cm.Parameters.AddWithValue("@StatusSetDate", GetProperty(StatusSetDateProperty))
      cm.Parameters.AddWithValue("@AreaComments", GetProperty(AreaCommentsProperty))
      'cm.Parameters.AddWithValue("@CallTimeTimelineID", NothingDBNull(GetProperty(CallTimeTimelineIDProperty)))
      cm.Parameters.AddWithValue("@CallTime", GetProperty(CallTimeProperty))
      'cm.Parameters.AddWithValue("@OnAirTimeTimelineID", NothingDBNull(GetProperty(OnAirTimeTimelineIDProperty)))
      cm.Parameters.AddWithValue("@OnAirTimeStart", GetProperty(OnAirTimeStartProperty))
      cm.Parameters.AddWithValue("@OnAirTimeEnd", GetProperty(OnAirTimeEndProperty))
      'cm.Parameters.AddWithValue("@WrapTimeTimelineID", NothingDBNull(GetProperty(WrapTimeTimelineIDProperty)))
      cm.Parameters.AddWithValue("@WrapTime", GetProperty(WrapTimeProperty))
      cm.Parameters.AddWithValue("@IsOwner", GetProperty(IsOwnerProperty))
      'cm.Parameters.AddWithValue("@ResourceBookingID", NothingDBNull(GetProperty(ResourceBookingIDProperty))
      cm.Parameters.AddWithValue("@ResourceID", NothingDBNull(GetProperty(ResourceIDProperty)))
      cm.Parameters.AddWithValue("@ResourceBookingTypeID", NothingDBNull(GetProperty(ResourceBookingTypeIDProperty)))
      cm.Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
      cm.Parameters.AddWithValue("@IsCancelled", GetProperty(IsCancelledProperty))
      cm.Parameters.AddWithValue("@IsTBC", GetProperty(IsTBCProperty))
      cm.Parameters.AddWithValue("@StatusCssClass", GetProperty(StatusCssClassProperty))
      cm.Parameters.AddWithValue("@PopoverContent", GetProperty(PopoverContentProperty))
      cm.Parameters.AddWithValue("@SlugLayoutInScheduler", GetProperty(SlugLayoutInSchedulerProperty))
      cm.Parameters.AddWithValue("@LoadedProductionAreaStatusID", NothingDBNull(GetProperty(LoadedProductionAreaStatusIDProperty)))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@OwnerComments", GetProperty(OwnerCommentsProperty))
      cm.Parameters.AddWithValue("@Room", GetProperty(RoomProperty))
      cm.Parameters.AddWithValue("@ModifiedByName", OBLib.Security.Settings.CurrentUser.FirstNameSurname)
      cm.Parameters.AddWithValue("@IsReconciled", GetProperty(IsReconciledProperty))
      'cm.Parameters.AddWithValue("@IsDifferentFromParent", GetProperty(IsDifferentFromParentProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(RoomScheduleIDProperty, cm.Parameters("@RoomScheduleID").Value)
                 LoadProperty(ProductionSystemAreaIDProperty, cm.Parameters("@ProductionSystemAreaID").Value)
                 LoadProperty(ProductionSystemAreaIDOwnerProperty, cm.Parameters("@ProductionSystemAreaIDOwner").Value)
                 LoadProperty(CallTimeTimelineIDProperty, cm.Parameters("@CallTimeTimelineID").Value)
                 LoadProperty(OnAirTimeTimelineIDProperty, cm.Parameters("@OnAirTimeTimelineID").Value)
                 LoadProperty(WrapTimeTimelineIDProperty, cm.Parameters("@WrapTimeTimelineID").Value)
                 LoadProperty(ResourceBookingIDProperty, cm.Parameters("@ResourceBookingID").Value)
               End If
               MarkOld()
               UpdateReconciled(True)
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(RoomScheduleAreaProductionHumanResourceListProperty))
      UpdateChild(GetProperty(RoomScheduleAreaProductionHRBookingListProperty))
      If IsOwner Then
        UpdateChild(GetProperty(RoomScheduleChannelListProperty))
      End If

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@RoomScheduleID", GetProperty(RoomScheduleIDProperty))
      cm.Parameters.AddWithValue("@ProductionSystemAreaIDOwner", GetProperty(ProductionSystemAreaIDOwnerProperty))
      cm.Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))
      cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@PermanentDelete", GetProperty(PermanentDeleteProperty))
    End Sub

#End Region

  End Class

End Namespace