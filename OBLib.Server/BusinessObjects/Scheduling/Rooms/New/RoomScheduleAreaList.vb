﻿' Generated 16 Jun 2016 14:00 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Scheduling.Rooms

  <Serializable()> _
  Public Class RoomScheduleAreaList
    Inherits OBBusinessListBase(Of RoomScheduleAreaList, RoomScheduleArea)

#Region " Business Methods "

    Public Function GetItem(ProductionSystemAreaID As Integer) As RoomScheduleArea

      For Each child As RoomScheduleArea In Me
        If child.ProductionSystemAreaID = ProductionSystemAreaID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByRoomScheduleID(RoomScheduleID As Integer) As RoomScheduleArea

      For Each child As RoomScheduleArea In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetRoomScheduleAreaProductionHRBooking(ProductionHRID As Integer) As RoomScheduleAreaProductionHRBooking

      Dim obj As RoomScheduleAreaProductionHRBooking = Nothing
      For Each parent As RoomScheduleArea In Me
        obj = parent.RoomScheduleAreaProductionHRBookingList.GetItem(ProductionHRID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property RoomScheduleID As Integer?
      Public Property ProductionSystemAreaID As Integer?
      Public Property ProductionSystemAreaIDs As List(Of Integer)

      Public Sub New(RoomScheduleID As Integer?, ProductionSystemAreaID As Integer?)
        Me.RoomScheduleID = RoomScheduleID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New(ProductionSystemAreaIDs As List(Of Integer))
        Me.ProductionSystemAreaIDs = ProductionSystemAreaIDs
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewRoomScheduleAreaList() As RoomScheduleAreaList

      Return New RoomScheduleAreaList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetRoomScheduleAreaList() As RoomScheduleAreaList

      Return DataPortal.Fetch(Of RoomScheduleAreaList)(New Criteria())

    End Function

    Public Shared Function GetRoomScheduleAreaList(RoomScheduleID As Integer?, ProductionSystemAreaID As Integer?) As RoomScheduleAreaList

      Return DataPortal.Fetch(Of RoomScheduleAreaList)(New Criteria(RoomScheduleID, ProductionSystemAreaID))

    End Function

    Public Shared Function GetRoomScheduleAreaList(ProductionSystemAreaIDs As List(Of Integer)) As RoomScheduleAreaList

      Return DataPortal.Fetch(Of RoomScheduleAreaList)(New Criteria(ProductionSystemAreaIDs))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(RoomScheduleArea.GetRoomScheduleArea(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As RoomScheduleArea = Nothing

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionSystemAreaID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.RoomScheduleAreaProductionHumanResourceList.RaiseListChangedEvents = False
          parent.RoomScheduleAreaProductionHumanResourceList.Add(RoomScheduleAreaProductionHumanResource.GetRoomScheduleAreaProductionHumanResource(sdr))
          parent.RoomScheduleAreaProductionHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionSystemAreaID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.RoomScheduleAreaProductionHRBookingList.RaiseListChangedEvents = False
          parent.RoomScheduleAreaProductionHRBookingList.Add(RoomScheduleAreaProductionHRBooking.GetRoomScheduleAreaProductionHRBooking(sdr))
          parent.RoomScheduleAreaProductionHRBookingList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.RoomScheduleID <> sdr.GetInt32(1) Then
            parent = Me.GetItemByRoomScheduleID(sdr.GetInt32(1))
          End If
          parent.RoomScheduleChannelList.RaiseListChangedEvents = False
          parent.RoomScheduleChannelList.Add(RoomScheduleChannel.GetRoomScheduleChannel(sdr))
          parent.RoomScheduleChannelList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As RoomScheduleArea In Me
        child.CheckRules()
        For Each RoomScheduleAreaProductionHumanResource As RoomScheduleAreaProductionHumanResource In child.RoomScheduleAreaProductionHumanResourceList
          RoomScheduleAreaProductionHumanResource.CheckRules()
        Next
        For Each RoomScheduleAreaProductionHRBooking As RoomScheduleAreaProductionHRBooking In child.RoomScheduleAreaProductionHRBookingList
          RoomScheduleAreaProductionHRBooking.CheckRules()
        Next
        For Each RoomScheduleChannel As RoomScheduleChannel In child.RoomScheduleChannelList
          RoomScheduleChannel.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRoomScheduleAreaList"
            cm.Parameters.AddWithValue("@RoomScheduleID", NothingDBNull(crit.RoomScheduleID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ProductionSystemAreaIDs)))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace