﻿' Generated 14 Sep 2016 16:37 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Scheduling.Rooms

  <Serializable()> _
  Public Class RoomScheduleChannel
    Inherits OBBusinessBase(Of RoomScheduleChannel)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RoomScheduleChannelBO.RoomScheduleChannelBOToString(self)")

    Public Overrides Property IsSelected() As Boolean
      Get
        Return GetProperty(IsSelectedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsSelectedProperty, value)
      End Set
    End Property

    Public Shared RoomScheduleChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleChannelID, "Room Schedule Channel")
    ''' <summary>
    ''' Gets and sets the Room Schedule Channel value
    ''' </summary>
    <Display(Name:="Room Schedule Channel", Description:=""), Key,
    Required(ErrorMessage:="ID required")>
    Public Property RoomScheduleChannelID() As Integer
      Get
        Return GetProperty(RoomScheduleChannelIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleChannelIDProperty, Value)
      End Set
    End Property

    Public Shared ScheduleNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ScheduleNumber, "Schedule Number")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Schedule Number", Description:="")>
    Public Property ScheduleNumber() As Integer
      Get
        Return GetProperty(ScheduleNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ScheduleNumberProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "Room Schedule")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionChannelID, "Production Channel")
    ''' <summary>
    ''' Gets and sets the Production Channel value
    ''' </summary>
    <Display(Name:="Production Channel", Description:="")>
    Public Property ProductionChannelID() As Integer?
      Get
        Return GetProperty(ProductionChannelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionChannelIDProperty, Value)
      End Set
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel")
    ''' <summary>
    ''' Gets and sets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelShortNameProperty, Value)
      End Set
    End Property

    Public Shared ProductionChannelStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionChannelStatusID, "Status")
    ''' <summary>
    ''' Gets and sets the Production Channel Status value
    ''' </summary>
    <Display(Name:="Production Channel Status", Description:=""),
    Required(ErrorMessage:="Production Channel Status required")>
    Public Property ProductionChannelStatusID() As Integer?
      Get
        Return GetProperty(ProductionChannelStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionChannelStatusIDProperty, Value)
      End Set
    End Property

    Public Shared StatusCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCode, "Status")
    ''' <summary>
    ''' Gets and sets the Status Code value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property StatusCode() As String
      Get
        Return GetProperty(StatusCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCodeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ScheduleDateTime, "Schedule Date Time")
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Schedule Date Time", Description:=""),
    Required(ErrorMessage:="Schedule Date Time required")>
    Public Property ScheduleDateTime As Date
      Get
        Return GetProperty(ScheduleDateTimeProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(ScheduleDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ScheduleEndDate, "Schedule End Date")
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="Schedule End Date", Description:=""),
    Required(ErrorMessage:="Schedule End Date required")>
    Public Property ScheduleEndDate As Date
      Get
        Return GetProperty(ScheduleEndDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(ScheduleEndDateProperty, Value)
      End Set
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelID, "Channel")
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Channel", Description:=""),
    Required(ErrorMessage:="Channel required")>
    Public Property ChannelID() As Integer
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ChannelIDProperty, Value)
      End Set
    End Property

    Public Shared RemovedFromSynergyProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RemovedFromSynergy, "Removed From Synergy", False)
    ''' <summary>
    ''' Gets and sets the Removed From Synergy value
    ''' </summary>
    <Display(Name:="Removed From Synergy", Description:=""),
    Required(ErrorMessage:="Removed From Synergy required")>
    Public Property RemovedFromSynergy() As Boolean
      Get
        Return GetProperty(RemovedFromSynergyProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RemovedFromSynergyProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ScheduledTimesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ScheduledTimes, "Scheduled Times")
    ''' <summary>
    ''' Gets and sets the Status Code value
    ''' </summary>
    <Display(Name:="Scheduled Times", Description:="")>
    Public Property ScheduledTimes() As String
      Get
        Return GetProperty(ScheduledTimesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ScheduledTimesProperty, Value)
      End Set
    End Property

    Public Shared OtherRoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OtherRoomScheduleID, "Room Schedule")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public Property OtherRoomScheduleID() As Integer?
      Get
        Return GetProperty(OtherRoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OtherRoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared OtherRoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OtherRoomID, "Room")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property OtherRoomID() As Integer?
      Get
        Return GetProperty(OtherRoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OtherRoomIDProperty, Value)
      End Set
    End Property

    Public Shared OtherRoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OtherRoom, "Other Room")
    ''' <summary>
    ''' Gets and sets the Status Code value
    ''' </summary>
    <Display(Name:="Other Room", Description:="")>
    Public Property OtherRoom() As String
      Get
        Return GetProperty(OtherRoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OtherRoomProperty, Value)
      End Set
    End Property

    Public Shared OtherRoomScheduleTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OtherRoomScheduleTitle, "Other Booking")
    ''' <summary>
    ''' Gets and sets the Status Code value
    ''' </summary>
    <Display(Name:="Other Booking", Description:="")>
    Public Property OtherRoomScheduleTitle() As String
      Get
        Return GetProperty(OtherRoomScheduleTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OtherRoomScheduleTitleProperty, Value)
      End Set
    End Property

    Public Shared IsOtherRoomScheduleProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsOtherRoomSchedule, "Other Booking")
    ''' <summary>
    ''' Gets and sets the Status Code value
    ''' </summary>
    <Display(Name:="Other Booking", Description:="")>
    Public Property IsOtherRoomSchedule() As Boolean
      Get
        Return GetProperty(IsOtherRoomScheduleProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsOtherRoomScheduleProperty, Value)
      End Set
    End Property

    Public Shared CanEditProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanEdit, "CanEdit")
    ''' <summary>
    ''' Gets and sets the Status Code value
    ''' </summary>
    <Display(Name:="CanEdit", Description:="")>
    Public Property CanEdit() As Boolean
      Get
        Return GetProperty(CanEditProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CanEditProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As RoomScheduleArea

      If Me.Parent IsNot Nothing Then
        If CType(Me.Parent, RoomScheduleChannelList).Parent IsNot Nothing Then
          Return CType(CType(Me.Parent, RoomScheduleChannelList).Parent, RoomScheduleArea)
        End If
      End If
      Return Nothing

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleChannelIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ChannelShortName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Room Schedule Channel")
        Else
          Return String.Format("Blank {0}", "Room Schedule Channel")
        End If
      Else
        Return Me.ChannelShortName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomScheduleChannel() method.

    End Sub

    Public Shared Function NewRoomScheduleChannel() As RoomScheduleChannel

      Return DataPortal.CreateChild(Of RoomScheduleChannel)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetRoomScheduleChannel(dr As SafeDataReader) As RoomScheduleChannel

      Dim r As New RoomScheduleChannel()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ScheduleNumberProperty, .GetInt32(0))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(RoomScheduleChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ChannelShortNameProperty, .GetString(4))
          LoadProperty(ProductionChannelStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(StatusCodeProperty, .GetString(6))
          LoadProperty(ScheduleDateTimeProperty, .GetValue(7))
          LoadProperty(ScheduleEndDateProperty, .GetValue(8))
          LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(RemovedFromSynergyProperty, .GetBoolean(10))
          LoadProperty(CreatedByProperty, .GetInt32(11))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(12))
          LoadProperty(ModifiedByProperty, .GetInt32(13))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(14))
          LoadProperty(ScheduledTimesProperty, .GetString(15))
          LoadProperty(OtherRoomScheduleIDProperty, ZeroNothing(.GetInt32(16)))
          LoadProperty(OtherRoomIDProperty, ZeroNothing(.GetInt32(17)))
          LoadProperty(OtherRoomProperty, .GetString(18))
          LoadProperty(OtherRoomScheduleTitleProperty, .GetString(19))
          LoadProperty(IsOtherRoomScheduleProperty, .GetBoolean(20))
          LoadProperty(CanEditProperty, .GetBoolean(21))
          LoadProperty(IsSelectedProperty, .GetBoolean(22))
        End With
      End Using

      MarkAsChild()
      MarkDirty()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddInputOutputParam(cm, RoomScheduleChannelIDProperty)

      cm.Parameters.AddWithValue("@RoomScheduleID", ZeroNothing(GetProperty(RoomScheduleIDProperty)))
      cm.Parameters.AddWithValue("@ProductionChannelID", NothingDBNull(GetProperty(ProductionChannelIDProperty)))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@ScheduleNumber", GetProperty(ScheduleNumberProperty))
      cm.Parameters.AddWithValue("@IsSelected", GetProperty(IsSelectedProperty))
      'cm.Parameters.AddWithValue("@ChannelShortName", GetProperty(ChannelShortNameProperty))
      'cm.Parameters.AddWithValue("@ProductionChannelStatusID", GetProperty(ProductionChannelStatusIDProperty))
      'cm.Parameters.AddWithValue("@StatusCode", GetProperty(StatusCodeProperty))
      'cm.Parameters.AddWithValue("@ScheduleDateTime", ScheduleDateTime)
      'cm.Parameters.AddWithValue("@ScheduleEndDate", ScheduleEndDate)
      'cm.Parameters.AddWithValue("@ChannelID", GetProperty(ChannelIDProperty))
      'cm.Parameters.AddWithValue("@RemovedFromSynergy", GetProperty(RemovedFromSynergyProperty))

      Return Sub()
               'Post Save
               'If Me.IsNew Then
               Dim sd As Object = cm.Parameters("@RoomScheduleChannelID").Value
               LoadProperty(RoomScheduleChannelIDProperty, IIf(IsDBNull(sd), 0, sd))
               'End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@RoomScheduleChannelID", GetProperty(RoomScheduleChannelIDProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
    End Sub

#End Region

  End Class

End Namespace