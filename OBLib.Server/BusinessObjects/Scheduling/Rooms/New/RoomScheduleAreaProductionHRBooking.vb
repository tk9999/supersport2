﻿' Generated 16 Jun 2016 14:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Scheduling.Rooms

  <Serializable()> _
  Public Class RoomScheduleAreaProductionHRBooking
    Inherits OBBusinessBase(Of RoomScheduleAreaProductionHRBooking)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RoomScheduleAreaProductionHRBookingBO.RoomScheduleAreaProductionHRBookingBOToString(self)")

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHRID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property ProductionHRID() As Integer
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionHRIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Production System Area")
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:=""),
    Required(ErrorMessage:="Production System Area required")>
    Public Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
    Public Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "Resource Booking")
    ''' <summary>
    ''' Gets and sets the Resource Booking value
    ''' </summary>
    <Display(Name:="Resource Booking", Description:="")>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingTypeID, "Resource Booking Type")
    ''' <summary>
    ''' Gets and sets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type", Description:=""),
    Required(ErrorMessage:="Resource Booking Type required")>
    Public Property ResourceBookingTypeID() As Integer
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingTypeIDProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "Room Schedule")
    ''' <summary>
    ''' Gets the Room Schedule value
    ''' </summary>
    Public Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Call Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    Required(ErrorMessage:="Call Time required"),
    SetExpression("RoomScheduleAreaProductionHRBookingBO.StartDateTimeBufferSet(self)")>
    Public Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
    SetExpression("RoomScheduleAreaProductionHRBookingBO.StartDateTimeSet(self)")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
    SetExpression("RoomScheduleAreaProductionHRBookingBO.EndDateTimeSet(self)")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "Wrap Time")
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    Required(ErrorMessage:="Wrap Time required"),
    SetExpression("RoomScheduleAreaProductionHRBookingBO.EndDateTimeBufferSet(self)")>
    Public Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared IgnoreClashesReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IgnoreClashesReason, "Ignore Clashes Reason")
    ''' <summary>
    ''' Gets and sets the Ignore Clashes Reason value
    ''' </summary>
    <Display(Name:="Ignore Clashes Reason", Description:="")>
    Public Property IgnoreClashesReason() As String
      Get
        Return GetProperty(IgnoreClashesReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IgnoreClashesReasonProperty, Value)
      End Set
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Is Cancelled value
    ''' </summary>
    <Display(Name:="Is Cancelled", Description:=""),
    Required(ErrorMessage:="Is Cancelled required")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared IsTBCProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTBC, "Is TBC", False)
    ''' <summary>
    ''' Gets and sets the Is TBC value
    ''' </summary>
    <Display(Name:="Is TBC", Description:=""),
    Required(ErrorMessage:="Is TBC required")>
    Public Property IsTBC() As Boolean
      Get
        Return GetProperty(IsTBCProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsTBCProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Resource Booking Description")
    ''' <summary>
    ''' Gets and sets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Resource Booking Description", Description:="")>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "Status Css Class")
    ''' <summary>
    ''' Gets and sets the Status Css Class value
    ''' </summary>
    <Display(Name:="Status Css Class", Description:="")>
    Public Property StatusCssClass() As String
      Get
        Return GetProperty(StatusCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCssClassProperty, Value)
      End Set
    End Property

    Public Shared CallTimeCSDIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CallTimeCSDID, "Call Time CSD")
    ''' <summary>
    ''' Gets and sets the Call Time CSD value
    ''' </summary>
    <Display(Name:="Call Time CSD", Description:="")>
    Public Property CallTimeCSDID() As Integer
      Get
        Return GetProperty(CallTimeCSDIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CallTimeCSDIDProperty, Value)
      End Set
    End Property

    Public Shared OnAirTimeCSDIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OnAirTimeCSDID, "On Air Time CSD")
    ''' <summary>
    ''' Gets and sets the On Air Time CSD value
    ''' </summary>
    <Display(Name:="On Air Time CSD", Description:="")>
    Public Property OnAirTimeCSDID() As Integer
      Get
        Return GetProperty(OnAirTimeCSDIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OnAirTimeCSDIDProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeCSDIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WrapTimeCSDID, "Wrap Time CSD")
    ''' <summary>
    ''' Gets and sets the Wrap Time CSD value
    ''' </summary>
    <Display(Name:="Wrap Time CSD", Description:="")>
    Public Property WrapTimeCSDID() As Integer
      Get
        Return GetProperty(WrapTimeCSDIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WrapTimeCSDIDProperty, Value)
      End Set
    End Property

    Public Shared CallTimeTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CallTimeTimelineID, "Call Time", Nothing)
    ''' <summary>
    ''' Gets and sets the Call Time Timeline value
    ''' </summary>
    <Display(Name:="Call Time", Description:="")>
    Public Property CallTimeTimelineID() As Integer?
      Get
        Return GetProperty(CallTimeTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CallTimeTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared OnAirTimeTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OnAirTimeTimelineID, "On Air Time", Nothing)
    ''' <summary>
    ''' Gets and sets the On Air Time Timeline value
    ''' </summary>
    <Display(Name:="On Air Time", Description:="")>
    Public Property OnAirTimeTimelineID() As Integer?
      Get
        Return GetProperty(OnAirTimeTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OnAirTimeTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.WrapTimeTimelineID, "Wrap Time", Nothing)
    ''' <summary>
    ''' Gets and sets the Wrap Time Timeline value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:="")>
    Public Property WrapTimeTimelineID() As Integer?
      Get
        Return GetProperty(WrapTimeTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(WrapTimeTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared CallTimeMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CallTimeMatch, "CallTimeMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="CallTimeMatch", Description:="")>
    Public ReadOnly Property CallTimeMatch() As Boolean
      Get
        Return GetProperty(CallTimeMatchProperty)
      End Get
    End Property

    Public Shared OnAirStartMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnAirStartMatch, "OnAirStartMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="OnAirStartMatch", Description:="")>
    Public ReadOnly Property OnAirStartMatch() As Boolean
      Get
        Return GetProperty(OnAirStartMatchProperty)
      End Get
    End Property

    Public Shared OnAirEndMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnAirEndMatch, "OnAirEndMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="OnAirEndMatch", Description:="")>
    Public ReadOnly Property OnAirEndMatch() As Boolean
      Get
        Return GetProperty(OnAirEndMatchProperty)
      End Get
    End Property

    Public Shared WrapTimeMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.WrapTimeMatch, "WrapTimeMatch", False)
    ''' <summary>
    ''' Gets and sets the Slug Layout In Scheduler value
    ''' </summary>
    <Display(Name:="WrapTimeMatch", Description:="")>
    Public ReadOnly Property WrapTimeMatch() As Boolean
      Get
        Return GetProperty(WrapTimeMatchProperty)
      End Get
    End Property

    Public Shared FoundProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Found, "Found", False)
    ''' <summary>
    ''' Gets and sets the Is TBC value
    ''' </summary>
    <Display(Name:="Found", Description:="")>
    Public Property Found() As Boolean
      Get
        Return GetProperty(FoundProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(FoundProperty, Value)
      End Set
    End Property

#End Region

#Region " Other Properties "

    Public Shared FreezeSetExpressionsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FreezeSetExpressions, "FreezeSetExpressions", False)
    ''' <summary>
    ''' Gets and sets the Is TBC value
    ''' </summary>
    <Display(Name:="FreezeSetExpressions", Description:=""), AlwaysClean>
    Public Property FreezeSetExpressions() As Boolean
      Get
        Return GetProperty(FreezeSetExpressionsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(FreezeSetExpressionsProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ClashDetailListProperty As PropertyInfo(Of List(Of Helpers.ResourceHelpers.ClashDetail)) = RegisterProperty(Of List(Of Helpers.ResourceHelpers.ClashDetail))(Function(c) c.ClashDetailList, "Clash List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ClashDetailList() As List(Of Helpers.ResourceHelpers.ClashDetail)
      Get
        If GetProperty(ClashDetailListProperty) Is Nothing Then
          LoadProperty(ClashDetailListProperty, New List(Of Helpers.ResourceHelpers.ClashDetail))
        End If
        Return GetProperty(ClashDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As RoomScheduleArea

      Return CType(CType(Me.Parent, RoomScheduleAreaProductionHRBookingList).Parent, RoomScheduleArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHRIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.IgnoreClashesReason.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Room Schedule Area Production HR Booking")
        Else
          Return String.Format("Blank {0}", "Room Schedule Area Production HR Booking")
        End If
      Else
        Return Me.IgnoreClashesReason
      End If

    End Function

    Public Function GetProductionHumanResourceID() As Integer?

      Dim phrList As List(Of RoomScheduleAreaProductionHumanResource) = Me.GetParent.RoomScheduleAreaProductionHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, Me.HumanResourceID)).ToList
      If phrList.Count = 0 Then
        Return Nothing
      ElseIf phrList.Count = 1 Then
        Return phrList(0).ProductionHumanResourceID
      ElseIf phrList.Count > 1 Then
        Return phrList(0).ProductionHumanResourceID
      End If
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(StartDateTimeBufferProperty)
        .ServerRuleFunction = AddressOf CallTimeValid
        .JavascriptRuleFunctionName = "RoomScheduleAreaProductionHRBookingBO.CallTimeValid"
        .AffectedProperties.AddRange({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
        .AddTriggerProperties({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
      End With

      With AddWebRule(StartDateTimeProperty)
        .ServerRuleFunction = AddressOf StartTimeValid
        .JavascriptRuleFunctionName = "RoomScheduleAreaProductionHRBookingBO.StartTimeValid"
        .AffectedProperties.AddRange({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
        .AddTriggerProperties({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
      End With

      With AddWebRule(EndDateTimeProperty)
        .ServerRuleFunction = AddressOf EndTimeValid
        .JavascriptRuleFunctionName = "RoomScheduleAreaProductionHRBookingBO.EndTimeValid"
        .AffectedProperties.AddRange({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
        .AddTriggerProperties({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
      End With

      With AddWebRule(EndDateTimeBufferProperty)
        .ServerRuleFunction = AddressOf WrapTimeValid
        .JavascriptRuleFunctionName = "RoomScheduleAreaProductionHRBookingBO.WrapTimeValid"
        .AffectedProperties.AddRange({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
        .AddTriggerProperties({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
      End With

      'With AddWebRule(EndDateTimeBufferProperty)
      '  .ServerRuleFunction = AddressOf WrapTimeValid
      '  .JavascriptRuleFunctionName = "RoomScheduleAreaProductionHRBookingBO.WrapTimeValid"
      '  .AffectedProperties.AddRange({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
      '  .AddTriggerProperties({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
      'End With

    End Sub

    Public Shared Function CallTimeValid(RoomScheduleAreaProductionHRBooking As RoomScheduleAreaProductionHRBooking) As String
      If RoomScheduleAreaProductionHRBooking.StartDateTimeBuffer Is Nothing Then
        Return "Call time is required"
      Else
        If RoomScheduleAreaProductionHRBooking.StartDateTime IsNot Nothing Then
          If RoomScheduleAreaProductionHRBooking.StartDateTimeBuffer.Value.Subtract(RoomScheduleAreaProductionHRBooking.StartDateTime.Value).TotalMinutes > 0 Then
            Return "Call time must be before Start time"
          End If
        End If
      End If
      Return ""
    End Function

    Public Shared Function StartTimeValid(RoomScheduleAreaProductionHRBooking As RoomScheduleAreaProductionHRBooking) As String
      If RoomScheduleAreaProductionHRBooking.StartDateTime Is Nothing Then
        Return "Start time is required"
      Else
        If RoomScheduleAreaProductionHRBooking.EndDateTime IsNot Nothing Then
          If RoomScheduleAreaProductionHRBooking.StartDateTime.Value.Subtract(RoomScheduleAreaProductionHRBooking.EndDateTime.Value).TotalMinutes > 0 Then
            Return "Start time must be before End time"
          End If
        End If
      End If
      Return ""
    End Function

    Public Shared Function EndTimeValid(RoomScheduleAreaProductionHRBooking As RoomScheduleAreaProductionHRBooking) As String
      If RoomScheduleAreaProductionHRBooking.EndDateTime Is Nothing Then
        Return "End time is required"
      Else
        If RoomScheduleAreaProductionHRBooking.EndDateTimeBuffer IsNot Nothing Then
          If RoomScheduleAreaProductionHRBooking.EndDateTime.Value.Subtract(RoomScheduleAreaProductionHRBooking.EndDateTimeBuffer.Value).TotalMinutes > 0 Then
            Return "End time must be before Wrap time"
          End If
        End If
      End If
      Return ""
    End Function

    Public Shared Function WrapTimeValid(RoomScheduleAreaProductionHRBooking As RoomScheduleAreaProductionHRBooking) As String
      If RoomScheduleAreaProductionHRBooking.EndDateTimeBuffer Is Nothing Then
        Return "Wrap time is required"
      Else
        If RoomScheduleAreaProductionHRBooking.EndDateTime IsNot Nothing Then
          If RoomScheduleAreaProductionHRBooking.EndDateTime.Value.Subtract(RoomScheduleAreaProductionHRBooking.EndDateTimeBuffer.Value).TotalMinutes > 0 Then
            Return "End time must be before Wrap time"
          End If
        End If
      End If
      Return ""
    End Function

    Public Shared Function HasClashesValid(RoomScheduleAreaProductionHRBooking As RoomScheduleAreaProductionHRBooking) As String
      If RoomScheduleAreaProductionHRBooking.ClashDetailList.Count > 0 Then
        Return RoomScheduleAreaProductionHRBooking.ClashDetailList.Count.ToString & " clashes detected"
      Else
        Return ""
      End If
    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomScheduleAreaProductionHRBooking() method.

    End Sub

    Public Shared Function NewRoomScheduleAreaProductionHRBooking() As RoomScheduleAreaProductionHRBooking

      Return DataPortal.CreateChild(Of RoomScheduleAreaProductionHRBooking)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetRoomScheduleAreaProductionHRBooking(dr As SafeDataReader) As RoomScheduleAreaProductionHRBooking

      Dim r As New RoomScheduleAreaProductionHRBooking()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionHRIDProperty, .GetInt32(0))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ResourceBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(StartDateTimeBufferProperty, .GetValue(7))
          LoadProperty(StartDateTimeProperty, .GetValue(8))
          LoadProperty(EndDateTimeProperty, .GetValue(9))
          LoadProperty(EndDateTimeBufferProperty, .GetValue(10))
          LoadProperty(IgnoreClashesReasonProperty, .GetString(11))
          LoadProperty(IsCancelledProperty, .GetBoolean(12))
          LoadProperty(IsTBCProperty, .GetBoolean(13))
          LoadProperty(ResourceBookingDescriptionProperty, .GetString(14))
          LoadProperty(StatusCssClassProperty, .GetString(15))
          LoadProperty(CallTimeCSDIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          LoadProperty(OnAirTimeCSDIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(WrapTimeCSDIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
          LoadProperty(CallTimeTimelineIDProperty, ZeroNothing(.GetInt32(19)))
          LoadProperty(OnAirTimeTimelineIDProperty, ZeroNothing(.GetInt32(20)))
          LoadProperty(WrapTimeTimelineIDProperty, ZeroNothing(.GetInt32(21)))
          LoadProperty(CallTimeMatchProperty, .GetBoolean(22))
          LoadProperty(OnAirStartMatchProperty, .GetBoolean(23))
          LoadProperty(OnAirEndMatchProperty, .GetBoolean(24))
          LoadProperty(WrapTimeMatchProperty, .GetBoolean(25))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionHRIDProperty)
      'AddOutputParam(cm, ProductionSystemAreaIDProperty)
      AddOutputParam(cm, CallTimeCSDIDProperty)
      AddOutputParam(cm, OnAirTimeCSDIDProperty)
      AddOutputParam(cm, WrapTimeCSDIDProperty)
      AddOutputParam(cm, ResourceBookingIDProperty)

      cm.Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent().ProductionSystemAreaID)
      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@ResourceID", NothingDBNull(GetProperty(ResourceIDProperty)))
      'cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
      cm.Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
      cm.Parameters.AddWithValue("@RoomScheduleID", Me.GetParent().RoomScheduleID)
      cm.Parameters.AddWithValue("@StartDateTimeBuffer", StartDateTimeBuffer)
      cm.Parameters.AddWithValue("@StartDateTime", StartDateTime)
      cm.Parameters.AddWithValue("@EndDateTime", EndDateTime)
      cm.Parameters.AddWithValue("@EndDateTimeBuffer", EndDateTimeBuffer)
      cm.Parameters.AddWithValue("@IgnoreClashesReason", GetProperty(IgnoreClashesReasonProperty))
      cm.Parameters.AddWithValue("@IsCancelled", GetProperty(IsCancelledProperty))
      cm.Parameters.AddWithValue("@IsTBC", GetProperty(IsTBCProperty))
      cm.Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
      cm.Parameters.AddWithValue("@StatusCssClass", GetProperty(StatusCssClassProperty))
      'cm.Parameters.AddWithValue("@CallTimeCSDID", GetProperty(CallTimeCSDIDProperty))
      'cm.Parameters.AddWithValue("@OnAirTimeCSDID", GetProperty(OnAirTimeCSDIDProperty))
      'cm.Parameters.AddWithValue("@WrapTimeCSDID", GetProperty(WrapTimeCSDIDProperty))
      cm.Parameters.AddWithValue("@CallTimeTimelineID", NothingDBNull(GetProperty(CallTimeTimelineIDProperty)))
      cm.Parameters.AddWithValue("@OnAirTimeTimelineID", NothingDBNull(GetProperty(OnAirTimeTimelineIDProperty)))
      cm.Parameters.AddWithValue("@WrapTimeTimelineID", NothingDBNull(GetProperty(WrapTimeTimelineIDProperty)))
      'cm.Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent().ProductionSystemAreaID)
      cm.Parameters.AddWithValue("@ProductionHumanResourceID", NothingDBNull(Me.GetProductionHumanResourceID))
      cm.Parameters.AddWithValue("@SystemID", NothingDBNull(Me.GetParent().SystemID))
      cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(Me.GetParent().ProductionAreaID))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionHRIDProperty, cm.Parameters("@ProductionHRID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ProductionHRID", GetProperty(ProductionHRIDProperty))
    End Sub

#End Region

  End Class

End Namespace