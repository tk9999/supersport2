﻿' Generated 16 Jun 2016 14:00 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Scheduling.Rooms

  <Serializable()> _
  Public Class RoomScheduleTemplateList
    Inherits OBBusinessListBase(Of RoomScheduleTemplateList, RoomScheduleTemplate)

#Region " Business Methods "

    Public Function GetItem(ProductionSystemAreaID As Integer) As RoomScheduleTemplate

      For Each child As RoomScheduleTemplate In Me
        If child.ProductionSystemAreaIDOwner = ProductionSystemAreaID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByRoomScheduleID(RoomScheduleID As Integer) As RoomScheduleTemplate

      For Each child As RoomScheduleTemplate In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property RoomScheduleID As Integer?
      Public Property ProductionSystemAreaID As Integer?
      Public Property ProductionSystemAreaIDs As List(Of Integer)

      Public Sub New(RoomScheduleID As Integer?, ProductionSystemAreaID As Integer?)
        Me.RoomScheduleID = RoomScheduleID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New(ProductionSystemAreaIDs As List(Of Integer))
        Me.ProductionSystemAreaIDs = ProductionSystemAreaIDs
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewRoomScheduleTemplateList() As RoomScheduleTemplateList

      Return New RoomScheduleTemplateList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetRoomScheduleTemplateList() As RoomScheduleTemplateList

      Return DataPortal.Fetch(Of RoomScheduleTemplateList)(New Criteria())

    End Function

    Public Shared Function GetRoomScheduleTemplateList(RoomScheduleID As Integer?, ProductionSystemAreaID As Integer?) As RoomScheduleTemplateList

      Return DataPortal.Fetch(Of RoomScheduleTemplateList)(New Criteria(RoomScheduleID, ProductionSystemAreaID))

    End Function

    Public Shared Function GetRoomScheduleTemplateList(ProductionSystemAreaIDs As List(Of Integer)) As RoomScheduleTemplateList

      Return DataPortal.Fetch(Of RoomScheduleTemplateList)(New Criteria(ProductionSystemAreaIDs))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(RoomScheduleTemplate.GetRoomScheduleTemplate(sdr))
      End While
      Me.RaiseListChangedEvents = True

      For Each child As RoomScheduleTemplate In Me
        child.CheckRules()
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRoomScheduleTemplateList"
            cm.Parameters.AddWithValue("@RoomScheduleID", NothingDBNull(crit.RoomScheduleID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ProductionSystemAreaIDs)))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace