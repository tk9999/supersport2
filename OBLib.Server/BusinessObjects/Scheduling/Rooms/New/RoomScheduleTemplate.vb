﻿' Generated 16 Jun 2016 14:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Slugs
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Scheduling.Rooms

  <Serializable()> _
  Public Class RoomScheduleTemplate
    Inherits OBBusinessBase(Of RoomScheduleTemplate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RoomScheduleTemplateBO.RoomScheduleTemplateBOToString(self)")

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdHocBookingID, "AdHocBookingID")
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="AdHocBookingID", Description:="")>
    Public Property AdHocBookingID() As Integer
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AdHocBookingIDProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:=""),
    Required(AllowEmptyStrings:=False),
    SetExpression("RoomScheduleTemplateBO.TitleSet(self)")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
    Required(ErrorMessage:="Room required"),
    DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="RoomScheduleTemplateBO.setRoomCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleTemplateBO.triggerRoomAutoPopulate",
                AfterFetchJS:="RoomScheduleTemplateBO.afterRoomRefreshAjax",
                LookupMember:="Room", ValueMember:="RoomID", DropDownColumns:={"Room", "IsAvailableString"},
                OnItemSelectJSFunction:="RoomScheduleTemplateBO.onRoomSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("RoomScheduleTemplateBO.RoomIDSet(self)")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CallTime, "Call Time")
    ''' <summary>
    ''' Gets and sets the Call Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    Required(ErrorMessage:="Call Time required"),
    SetExpression("RoomScheduleTemplateBO.CallTimeStartSet(self)")>
    Public Property CallTime As DateTime?
      Get
        Return GetProperty(CallTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeProperty, Value)
      End Set
    End Property

    Public Shared OnAirTimeStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OnAirTimeStart, "Start Time")
    ''' <summary>
    ''' Gets and sets the On Air Time Start value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
    SetExpression("RoomScheduleTemplateBO.StartDateTimeSet(self)")>
    Public Property OnAirTimeStart As DateTime?
      Get
        Return GetProperty(OnAirTimeStartProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OnAirTimeStartProperty, Value)
      End Set
    End Property

    Public Shared OnAirTimeEndProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OnAirTimeEnd, "End Time")
    ''' <summary>
    ''' Gets and sets the On Air Time End value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
    SetExpression("RoomScheduleTemplateBO.EndDateTimeSet(self)")>
    Public Property OnAirTimeEnd As DateTime?
      Get
        Return GetProperty(OnAirTimeEndProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OnAirTimeEndProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.WrapTime, "Wrap Time")
    ''' <summary>
    ''' Gets and sets the Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    Required(ErrorMessage:="Wrap Time required"),
    SetExpression("RoomScheduleTemplateBO.WrapTimeEndSet(self)")>
    Public Property WrapTime As DateTime?
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDOwnerProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaIDOwner, "Production System Area", 0)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:=""),
    Required(ErrorMessage:="Owner required")>
    Public Property ProductionSystemAreaIDOwner() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDOwnerProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionSystemAreaIDOwnerProperty, Value)
      End Set
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Debtor")
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:=""),
    DropDownWeb(GetType(RODebtorFindList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="RoomScheduleTemplateBO.setDebtorCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleTemplateBO.triggerDebtorAutoPopulate",
                AfterFetchJS:="RoomScheduleTemplateBO.afterDebtorRefreshAjax",
                OnItemSelectJSFunction:="RoomScheduleTemplateBO.onDebtorSelected",
                LookupMember:="Debtor", DisplayMember:="Debtor", ValueMember:="DebtorID",
                DropDownCssClass:="debtor-dropdown", DropDownColumns:={"Debtor", "ContactName", "ContactNumber"}),
    SetExpression("RoomScheduleTemplateBO.DebtorIDSet(self)")>
    Public Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DebtorIDProperty, Value)
      End Set
    End Property

    Public Shared DebtorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Debtor, "Debtor")
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="")>
    Public Property Debtor() As String
      Get
        Return GetProperty(DebtorProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DebtorProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, " Sub-Dept")
    ''' <summary>
    ''' Gets and sets the  System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID"),
    SetExpression("RoomScheduleTemplateBO.SystemIDSet(self)")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, " Area")
    ''' <summary>
    ''' Gets and sets the  Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required"),
    DropDownWeb(GetType(ROUserSystemAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", ThisFilterMember:="SystemID"),
    SetExpression("RoomScheduleTemplateBO.ProductionAreaIDSet(self)")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "Resource Booking", 0)
    ''' <summary>
    ''' Gets and sets the Resource Booking value
    ''' </summary>
    <Display(Name:="Resource Booking", Description:=""),
    Required(ErrorMessage:="Resource Booking required")>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared OwnerCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OwnerComments, "Comments")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public Property OwnerComments() As String
      Get
        Return GetProperty(OwnerCommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OwnerCommentsProperty, Value)
      End Set
    End Property

    Public Shared TemplateIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TemplateID, " Template")
    ''' <summary>
    ''' Gets and sets the  System value
    ''' </summary>
    <Display(Name:="Template", Description:=""),
    Required(ErrorMessage:="Template is required")>
    Public Property TemplateID() As Integer?
      Get
        Return GetProperty(TemplateIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TemplateIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Title.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Room Schedule Area")
        Else
          Return String.Format("Blank {0}", "Room Schedule Area")
        End If
      Else
        Return Me.Title
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

    Public Function DoSetup() As Singular.Web.Result

      Dim cmd As New Singular.CommandProc("cmdProcs.cmdSetupRoomScheduleTemplate",
                            New String() {"@TemplateID", "@RoomID", "@StartDateTime", "@EndDateTime", "@CurrentUserID"},
                            New Object() {Me.TemplateID, Me.RoomID, Me.OnAirTimeStart, Me.OnAirTimeEnd, OBLib.Security.Settings.CurrentUserID})
      cmd.FetchType = CommandProc.FetchTypes.DataRow
      cmd.UseTransaction = True

      Try
        cmd = cmd.Execute
        Dim resultsRow = cmd.DataRow
        Dim CallTime As DateTime? = IIf(IsDBNull(resultsRow(0)), Nothing, resultsRow(0))
        Dim StartTime As DateTime? = resultsRow(1)
        Dim EndTime As DateTime? = resultsRow(2)
        Dim WrapTime As DateTime? = IIf(IsDBNull(resultsRow(3)), Nothing, resultsRow(3))
        Dim Title As String = resultsRow(4)

        Me.CallTime = CallTime
        Me.OnAirTimeStart = StartTime
        Me.OnAirTimeEnd = EndTime
        Me.WrapTime = WrapTime
        Me.Title = Title

        Return New Singular.Web.Result(True)
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(RoomScheduleIDProperty)
        .JavascriptRuleFunctionName = "RoomScheduleTemplateBO.RoomScheduleIDValid"
        .ServerRuleFunction = AddressOf RoomScheduleIDValid
        .AddTriggerProperty(RoomIDProperty)
        .AddTriggerProperty(OnAirTimeStartProperty)
        .AddTriggerProperty(OnAirTimeEndProperty)
        .AffectedProperties.Add(RoomIDProperty)
        .AffectedProperties.Add(OnAirTimeStartProperty)
        .AffectedProperties.Add(OnAirTimeEndProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(CallTimeProperty)
        .JavascriptRuleFunctionName = "RoomScheduleTemplateBO.CallTimeValid"
        .ServerRuleFunction = AddressOf CallTimeValid
        .AddTriggerProperty(OnAirTimeEndProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(OnAirTimeStartProperty)
        .JavascriptRuleFunctionName = "RoomScheduleTemplateBO.OnAirTimeStartValid"
        .ServerRuleFunction = AddressOf StartDateTimeValid
        .AddTriggerProperty(OnAirTimeEndProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(OnAirTimeEndProperty)
        .JavascriptRuleFunctionName = "RoomScheduleTemplateBO.OnAirEndTimeValid"
        .ServerRuleFunction = AddressOf EndDateTimeValid
        .AddTriggerProperty(OnAirTimeStartProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

      With AddWebRule(WrapTimeProperty)
        .JavascriptRuleFunctionName = "RoomScheduleTemplateBO.WrapTimeValid"
        .ServerRuleFunction = AddressOf WrapTimeValid
        .AddTriggerProperty(OnAirTimeStartProperty)
        '.AffectedProperties.Add(BookingTimesProperty)
      End With

    End Sub

    Public Overridable Function RoomScheduleIDValid(RoomSchedule As RoomScheduleTemplate) As String
      Dim ErrorString As String = ""
      'If RoomSchedule.ClashDetailList().Count > 0 Then
      '  ErrorString = "This booking is clashing with other bookings"
      'End If
      Return ErrorString
    End Function

    Public Overridable Function CallTimeValid(RoomSchedule As RoomScheduleTemplate) As String
      Dim Err As String = ""
      If RoomSchedule.CallTime Is Nothing Then
        Err &= "Call Time is required"
      ElseIf RoomSchedule.CallTime IsNot Nothing And RoomSchedule.OnAirTimeStart IsNot Nothing Then
        If RoomSchedule.CallTime.Value > RoomSchedule.OnAirTimeStart.Value Then
          Err &= "Call Time must be before On Air Start Time"
        End If
      End If
      Return Err
    End Function

    Public Overridable Function StartDateTimeValid(RoomSchedule As RoomScheduleTemplate) As String
      Dim Err As String = ""
      If RoomSchedule.OnAirTimeStart Is Nothing Then
        Err &= "Booking End Date Time is required"
      ElseIf RoomSchedule.OnAirTimeStart IsNot Nothing And RoomSchedule.OnAirTimeEnd IsNot Nothing Then
        If RoomSchedule.OnAirTimeStart.Value > RoomSchedule.OnAirTimeEnd.Value Then
          Err &= "On Air Start Time must be before On Air End Time"
        End If
      End If
      Return Err
    End Function

    Public Overridable Function EndDateTimeValid(RoomSchedule As RoomScheduleTemplate) As String
      Dim Err As String = ""
      If RoomSchedule.OnAirTimeEnd Is Nothing Then
        Err &= "Booking End Date Time is required"
      ElseIf RoomSchedule.OnAirTimeStart IsNot Nothing And RoomSchedule.OnAirTimeEnd IsNot Nothing Then
        If RoomSchedule.OnAirTimeStart.Value > RoomSchedule.OnAirTimeEnd.Value Then
          Err &= "On Air End Time must be after On Air Start Time"
        End If
      End If
      Return Err
    End Function

    Public Overridable Function WrapTimeValid(RoomSchedule As RoomScheduleTemplate) As String
      Dim Err As String = ""
      If RoomSchedule.WrapTime Is Nothing Then
        Err &= "Wrap Time is required"
      ElseIf RoomSchedule.WrapTime IsNot Nothing And RoomSchedule.OnAirTimeEnd IsNot Nothing Then
        If RoomSchedule.WrapTime.Value < RoomSchedule.OnAirTimeEnd.Value Then
          Err &= "Wrap Time must be after On Air End Time"
        End If
      End If
      Return Err
    End Function


#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomScheduleTemplate() method.

    End Sub

    Public Shared Function NewRoomScheduleTemplate() As RoomScheduleTemplate

      Return DataPortal.CreateChild(Of RoomScheduleTemplate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetRoomScheduleTemplate(dr As SafeDataReader) As RoomScheduleTemplate

      Dim r As New RoomScheduleTemplate()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(TitleProperty, .GetString(2))
          LoadProperty(ProductionSystemAreaIDOwnerProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(DebtorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(DebtorProperty, .GetString(6))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(7)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(8)))
          LoadProperty(RoomProperty, .GetString(9))
          LoadProperty(CallTimeProperty, .GetValue(10))
          LoadProperty(OnAirTimeStartProperty, .GetValue(11))
          LoadProperty(OnAirTimeEndProperty, .GetValue(12))
          LoadProperty(WrapTimeProperty, .GetValue(13))
          LoadProperty(ResourceBookingIDProperty, .GetInt32(14))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(OwnerCommentsProperty, .GetString(16))
        End With
      End Using
      'LoadBookingTimes()

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      cm.CommandText = "InsProcsWeb.insRoomScheduleTemplate" 'There is no update proc, this business object is used for insert only

      AddPrimaryKeyParam(cm, RoomScheduleIDProperty)
      AddOutputParam(cm, ProductionSystemAreaIDOwnerProperty)
      AddOutputParam(cm, ResourceBookingIDProperty)
      AddOutputParam(cm, ProductionIDProperty)
      AddOutputParam(cm, AdHocBookingIDProperty)

      cm.Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      cm.Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
      cm.Parameters.AddWithValue("@Room", GetProperty(RoomProperty))
      cm.Parameters.AddWithValue("@DebtorID", NothingDBNull(GetProperty(DebtorIDProperty)))
      cm.Parameters.AddWithValue("@Debtor", GetProperty(DebtorProperty))
      cm.Parameters.AddWithValue("@SystemID", NothingDBNull(GetProperty(SystemIDProperty)))
      cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))
      cm.Parameters.AddWithValue("@CallTime", GetProperty(CallTimeProperty))
      cm.Parameters.AddWithValue("@OnAirTimeStart", GetProperty(OnAirTimeStartProperty))
      cm.Parameters.AddWithValue("@OnAirTimeEnd", GetProperty(OnAirTimeEndProperty))
      cm.Parameters.AddWithValue("@WrapTime", GetProperty(WrapTimeProperty))
      cm.Parameters.AddWithValue("@ResourceID", NothingDBNull(GetProperty(ResourceIDProperty)))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@OwnerComments", GetProperty(OwnerCommentsProperty))
      cm.Parameters.AddWithValue("@TemplateID", NothingDBNull(GetProperty(TemplateIDProperty)))
      cm.Parameters.AddWithValue("@ModifiedByName", OBLib.Security.Settings.CurrentUser.FirstNameSurname)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(RoomScheduleIDProperty, cm.Parameters("@RoomScheduleID").Value)
                 LoadProperty(ProductionSystemAreaIDOwnerProperty, cm.Parameters("@ProductionSystemAreaIDOwner").Value)
                 LoadProperty(ResourceBookingIDProperty, cm.Parameters("@ResourceBookingID").Value)
                 LoadProperty(ProductionIDProperty, IIf(IsDBNull(cm.Parameters("@ProductionID").Value), 0, cm.Parameters("@ProductionID").Value))
                 LoadProperty(AdHocBookingIDProperty, IIf(IsDBNull(cm.Parameters("@AdHocBookingID").Value), 0, cm.Parameters("@AdHocBookingID").Value))
               End If
               MarkOld()
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@RoomScheduleID", GetProperty(RoomScheduleIDProperty))
      cm.Parameters.AddWithValue("@ProductionSystemAreaIDOwner", GetProperty(ProductionSystemAreaIDOwnerProperty))
      cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
    End Sub

#End Region

  End Class

End Namespace