﻿' Generated 16 Jun 2016 14:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Scheduling.Rooms

  <Serializable()> _
  Public Class RoomScheduleAreaProductionHRBookingList
    Inherits OBBusinessListBase(Of RoomScheduleAreaProductionHRBookingList, RoomScheduleAreaProductionHRBooking)

#Region " Business Methods "

    Public Function GetItem(ProductionHRID As Integer) As RoomScheduleAreaProductionHRBooking

      For Each child As RoomScheduleAreaProductionHRBooking In Me
        If child.ProductionHRID = ProductionHRID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByHumanResourceID(HumanResourceID As Integer) As RoomScheduleAreaProductionHRBooking

      For Each child As RoomScheduleAreaProductionHRBooking In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewRoomScheduleAreaProductionHRBookingList() As RoomScheduleAreaProductionHRBookingList

      Return New RoomScheduleAreaProductionHRBookingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace