﻿' Generated 16 Jun 2016 14:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Scheduling.Rooms

  <Serializable()> _
  Public Class RoomScheduleAreaProductionHumanResourceList
    Inherits OBBusinessListBase(Of RoomScheduleAreaProductionHumanResourceList, RoomScheduleAreaProductionHumanResource)

#Region " Business Methods "

    Public Function GetItem(ProductionHumanResourceID As Integer) As RoomScheduleAreaProductionHumanResource

      For Each child As RoomScheduleAreaProductionHumanResource In Me
        If child.ProductionHumanResourceID = ProductionHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewRoomScheduleAreaProductionHumanResourceList() As RoomScheduleAreaProductionHumanResourceList

      Return New RoomScheduleAreaProductionHumanResourceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetRoomScheduleAreaProductionHumanResourceList() As RoomScheduleAreaProductionHumanResourceList

      Return DataPortal.Fetch(Of RoomScheduleAreaProductionHumanResourceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(RoomScheduleAreaProductionHumanResource.GetRoomScheduleAreaProductionHumanResource(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRoomScheduleAreaProductionHumanResourceList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace