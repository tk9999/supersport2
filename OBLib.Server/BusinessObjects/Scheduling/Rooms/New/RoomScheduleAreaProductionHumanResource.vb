﻿' Generated 16 Jun 2016 14:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.SystemManagement.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Scheduling.Rooms

  <Serializable()> _
  Public Class RoomScheduleAreaProductionHumanResource
    Inherits OBBusinessBase(Of RoomScheduleAreaProductionHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RoomScheduleAreaProductionHumanResourceBO.RoomScheduleAreaProductionHumanResourceBOToString(self)")

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property ProductionHumanResourceID() As Integer
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area")
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:=""),
    Required(ErrorMessage:="Production System Area required")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    Required(ErrorMessage:="Discipline required"),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplineListSelect),
                BeforeFetchJS:="RoomScheduleAreaProductionHumanResourceBO.setDisciplineIDCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleAreaProductionHumanResourceBO.triggerDisciplineIDAutoPopulate",
                AfterFetchJS:="RoomScheduleAreaProductionHumanResourceBO.afterDisciplineIDRefreshAjax",
                OnItemSelectJSFunction:="RoomScheduleAreaProductionHumanResourceBO.onDisciplineIDSelected",
                LookupMember:="Discipline", DisplayMember:="Discipline", ValueMember:="DisciplineID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Discipline"}),
    SetExpressionBeforeChange("RoomScheduleAreaProductionHumanResourceBO.DisciplineIDBeforeSet(self)"),
    SetExpression("RoomScheduleAreaProductionHumanResourceBO.DisciplineIDSet(self)")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    SetExpression("RoomScheduleAreaProductionHumanResourceBO.DisciplineIDSet(self)")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionTypeID, "Position Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:=""),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplinePositionTypeListSelect),
                BeforeFetchJS:="RoomScheduleAreaProductionHumanResourceBO.setPositionTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleAreaProductionHumanResourceBO.triggerPositionTypeIDAutoPopulate",
                AfterFetchJS:="RoomScheduleAreaProductionHumanResourceBO.afterPositionTypeIDRefreshAjax",
                OnItemSelectJSFunction:="RoomScheduleAreaProductionHumanResourceBO.onPositionTypeIDSelected",
                LookupMember:="PositionType", DisplayMember:="PositionType", ValueMember:="PositionTypeID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Position"}),
    SetExpressionBeforeChange("RoomScheduleAreaProductionHumanResourceBO.PositionTypeIDBeforeSet(self)"),
    SetExpression("RoomScheduleAreaProductionHumanResourceBO.PositionTypeIDSet(self)")>
    Public Property PositionTypeID() As Integer?
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared PositionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionType, "Position Type", "")
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
    Public Property PositionType() As String
      Get
        Return GetProperty(PositionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionTypeProperty, Value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:=""),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplinePositionTypeListSelect),
                BeforeFetchJS:="RoomScheduleAreaProductionHumanResourceBO.setPositionIDCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleAreaProductionHumanResourceBO.triggerPositionIDAutoPopulate",
                AfterFetchJS:="RoomScheduleAreaProductionHumanResourceBO.afterPositionIDRefreshAjax",
                OnItemSelectJSFunction:="RoomScheduleAreaProductionHumanResourceBO.onPositionIDSelected",
                LookupMember:="Position", DisplayMember:="Position", ValueMember:="PositionID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Position"}),
    SetExpressionBeforeChange("RoomScheduleAreaProductionHumanResourceBO.PositionIDBeforeSet(self)"),
    SetExpression("RoomScheduleAreaProductionHumanResourceBO.PositionIDSet(self)")>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position", "")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    DropDownWeb(GetType(ROHumanResourceAvailabilityList),
                BeforeFetchJS:="RoomScheduleAreaProductionHumanResourceBO.setHumanResourceCriteriaBeforeRefresh",
                PreFindJSFunction:="RoomScheduleAreaProductionHumanResourceBO.triggerHumanResourceAutoPopulate",
                AfterFetchJS:="RoomScheduleAreaProductionHumanResourceBO.afterHumanResourceRefreshAjax",
                OnItemSelectJSFunction:="RoomScheduleAreaProductionHumanResourceBO.onHumanResourceSelected",
                LookupMember:="HumanResource", DisplayMember:="HRName", ValueMember:="HumanResourceID",
                DropDownCssClass:="hr-availability-dropdown", DropDownColumns:={"HRName"},
                OnCellCreateFunction:="RoomScheduleAreaProductionHumanResourceBO.onCellCreate"),
    SetExpressionBeforeChange("RoomScheduleAreaProductionHumanResourceBO.HumanResourceIDBeforeSet(self)"),
    SetExpression("RoomScheduleAreaProductionHumanResourceBO.HumanResourceIDSet(self)")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property
    ', "ContractType", "IsAvailableString", "RequiredHours", "TotalHours", "PublicHolidayCount", "OffWeekendCount", "ConsecutiveDays", "ShortfallPrevString", "ShortfallNextString"

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared CancelledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDate, "Cancelled Date")
    ''' <summary>
    ''' Gets and sets the Cancelled Date value
    ''' </summary>
    <Display(Name:="Cancelled Date", Description:="")>
    Public Property CancelledDate As DateTime?
      Get
        Return GetProperty(CancelledDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancelledDateProperty, Value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:=""),
    StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
      End Set
    End Property

    Public Shared TrainingIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TrainingInd, "Training", False)
    ''' <summary>
    ''' Gets and sets the Training value
    ''' </summary>
    <Display(Name:="Training", Description:=""),
    Required(ErrorMessage:="Training required")>
    Public Property TrainingInd() As Boolean
      Get
        Return GetProperty(TrainingIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(TrainingIndProperty, Value)
      End Set
    End Property

    Public Shared TBCIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TBCInd, "TBC", False)
    ''' <summary>
    ''' Gets and sets the TBC value
    ''' </summary>
    <Display(Name:="TBC", Description:=""),
    Required(ErrorMessage:="TBC required")>
    Public Property TBCInd() As Boolean
      Get
        Return GetProperty(TBCIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(TBCIndProperty, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:=""),
    StringLength(1000, ErrorMessage:="Comments cannot be more than 1000 characters")>
    Public Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

    Public Shared OrderNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNo, "Order No")
    ''' <summary>
    ''' Gets and sets the Order No value
    ''' </summary>
    <Display(Name:="Order No", Description:=""),
    Required(ErrorMessage:="Order No required")>
    Public Property OrderNo() As Integer
      Get
        Return GetProperty(OrderNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OrderNoProperty, Value)
      End Set
    End Property

    Public Shared PriorityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Priority, "Priority")
    ''' <summary>
    ''' Gets and sets the Priority value
    ''' </summary>
    <Display(Name:="Priority", Description:=""),
    Required(ErrorMessage:="Priority required")>
    Public Property Priority() As Integer
      Get
        Return GetProperty(PriorityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PriorityProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsCancelled, False)
    ''' <summary>
    ''' Gets and sets the Genre value
    ''' </summary>
    <Display(Name:="Cancelled?"),
    SetExpression("RoomScheduleAreaProductionHumanResourceBO.IsCancelledSet(self)")>
    Public Property IsCancelled() As Boolean
      Get
        Return ReadProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        LoadProperty(IsCancelledProperty, Value)
      End Set
    End Property

#End Region

#Region " Other Properties "

    Public Shared PreviousHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PreviousHumanResourceID, "Human Resource", Nothing)
    <Display(Name:="Previous Human Resource"), AlwaysClean>
    Public Property PreviousHumanResourceID() As Integer?
      Get
        Return GetProperty(PreviousHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PreviousHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared PreviousHumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreviousHumanResource, "Human Resource", "")
    <Display(Name:="Human Resource")>
    Public Property PreviousHumanResource() As String
      Get
        Return GetProperty(PreviousHumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PreviousHumanResourceProperty, Value)
      End Set
    End Property

    Public Shared PreviousResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PreviousResourceID, "ID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="PreviousResourceID")>
    Public Property PreviousResourceID() As Integer?
      Get
        Return GetProperty(PreviousResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(PreviousResourceIDProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As RoomScheduleArea

      If Me.Parent IsNot Nothing Then
        If CType(Me.Parent, RoomScheduleAreaProductionHumanResourceList).Parent IsNot Nothing Then
          Return CType(CType(Me.Parent, RoomScheduleAreaProductionHumanResourceList).Parent, RoomScheduleArea)
        End If
      End If
      Return Nothing

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Discipline.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Room Schedule Area Production Human Resource")
        Else
          Return String.Format("Blank {0}", "Room Schedule Area Production Human Resource")
        End If
      Else
        Return Me.Discipline
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(ProductionHumanResourceIDProperty)
        .JavascriptRuleFunctionName = "RoomScheduleAreaProductionHumanResourceBO.ProductionHumanResourceIDValid"
        .ServerRuleFunction = AddressOf ProductionHumanResourceIDValid
      End With

      With AddWebRule(CancelledDateProperty)
        .ServerRuleFunction = AddressOf CancelledDateValid
        .JavascriptRuleFunctionName = "RoomScheduleAreaProductionHumanResourceBO.CancelledDateValid"
        .AddTriggerProperty(CancelledReasonProperty)
        .AffectedProperties.Add(CancelledReasonProperty)
      End With

    End Sub

    Public Shared Function ProductionHumanResourceIDValid(RSProductionHumanResource As RoomScheduleAreaProductionHumanResource) As String
      Dim ErrorMsg As String = ""
      If RSProductionHumanResource.HumanResourceID IsNot Nothing Then
        If RSProductionHumanResource.GetParent IsNot Nothing Then
          Dim ProductionHR As RoomScheduleAreaProductionHRBooking = CType(RSProductionHumanResource.GetParent, RoomScheduleArea).RoomScheduleAreaProductionHRBookingList.GetItemByHumanResourceID(RSProductionHumanResource.HumanResourceID)
          If ProductionHR IsNot Nothing Then
            If ProductionHR.ClashDetailList.Count > 0 Then
              ErrorMsg = RSProductionHumanResource.HumanResource & " has clashes on his schedule"
            End If
          End If
        End If
      End If
      Return ErrorMsg
    End Function

    Public Shared Function CancelledDateValid(ProductionHumanResource As RoomScheduleAreaProductionHumanResource) As String

      Dim ErrorString As String = ""
      If ProductionHumanResource.CancelledDate IsNot Nothing AndAlso ProductionHumanResource.CancelledReason.Trim.Length = 0 Then
        ErrorString = "Cancelled Date and Cancelled Reason are required"
      End If
      Return ErrorString

    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomScheduleAreaProductionHumanResource() method.

    End Sub

    Public Shared Function NewRoomScheduleAreaProductionHumanResource() As RoomScheduleAreaProductionHumanResource

      Return DataPortal.CreateChild(Of RoomScheduleAreaProductionHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetRoomScheduleAreaProductionHumanResource(dr As SafeDataReader) As RoomScheduleAreaProductionHumanResource

      Dim r As New RoomScheduleAreaProductionHumanResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DisciplineProperty, .GetString(3))
          LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(PositionTypeProperty, .GetString(5))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(PositionProperty, .GetString(7))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(HumanResourceProperty, .GetString(9))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(CancelledDateProperty, .GetValue(11))
          LoadProperty(CancelledReasonProperty, .GetString(12))
          LoadProperty(TrainingIndProperty, .GetBoolean(13))
          LoadProperty(TBCIndProperty, .GetBoolean(14))
          LoadProperty(CommentsProperty, .GetString(15))
          LoadProperty(OrderNoProperty, .GetInt32(16))
          LoadProperty(PriorityProperty, .GetInt32(17))
          LoadProperty(CreatedByProperty, .GetInt32(18))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(19))
          LoadProperty(ModifiedByProperty, .GetInt32(20))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(21))
          LoadProperty(IsCancelledProperty, .GetBoolean(22))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionHumanResourceIDProperty)

      'GetProperty(ProductionSystemAreaIDProperty)
      cm.Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent.ProductionSystemAreaID)
      cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(GetProperty(DisciplineIDProperty)))
      cm.Parameters.AddWithValue("@Discipline", GetProperty(DisciplineProperty))
      cm.Parameters.AddWithValue("@PositionTypeID", NothingDBNull(GetProperty(PositionTypeIDProperty)))
      cm.Parameters.AddWithValue("@PositionType", GetProperty(PositionTypeProperty))
      cm.Parameters.AddWithValue("@PositionID", NothingDBNull(GetProperty(PositionIDProperty)))
      cm.Parameters.AddWithValue("@Position", GetProperty(PositionProperty))
      cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(GetProperty(HumanResourceIDProperty)))
      cm.Parameters.AddWithValue("@HumanResource", GetProperty(HumanResourceProperty))
      cm.Parameters.AddWithValue("@ResourceID", NothingDBNull(GetProperty(ResourceIDProperty)))
      cm.Parameters.AddWithValue("@CancelledDate", NothingDBNull(GetProperty(CancelledDateProperty)))
      cm.Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
      cm.Parameters.AddWithValue("@TrainingInd", GetProperty(TrainingIndProperty))
      cm.Parameters.AddWithValue("@TBCInd", GetProperty(TBCIndProperty))
      cm.Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
      cm.Parameters.AddWithValue("@OrderNo", GetProperty(OrderNoProperty))
      cm.Parameters.AddWithValue("@Priority", GetProperty(PriorityProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(Me.GetParent().ProductionID))
      cm.Parameters.AddWithValue("@SystemID", NothingDBNull(Me.GetParent().SystemID))
      cm.Parameters.AddWithValue("@RoomID", NothingDBNull(Me.GetParent().RoomID))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionHumanResourceIDProperty, cm.Parameters("@ProductionHumanResourceID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ProductionHumanResourceID", GetProperty(ProductionHumanResourceIDProperty))
    End Sub

#End Region

  End Class

End Namespace