﻿' Generated 23 Sep 2016 22:50 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Scheduling.Rooms

  <Serializable()> _
  Public Class ContentServicesDifference
    Inherits OBBusinessBase(Of ContentServicesDifference)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ContentServicesDifferenceBO.ContentServicesDifferenceBOToString(self)")

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDOwnrProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomIDOwnr, "Room ID Ownr")
    ''' <summary>
    ''' Gets and sets the Room ID Ownr value
    ''' </summary>
    <Display(Name:="Room ID Ownr", Description:="")>
    Public Property RoomIDOwnr() As Integer
      Get
        Return GetProperty(RoomIDOwnrProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomIDOwnrProperty, Value)
      End Set
    End Property

    Public Shared RoomIDServicesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomIDServices, "Room ID Services")
    ''' <summary>
    ''' Gets and sets the Room ID Services value
    ''' </summary>
    <Display(Name:="Room ID Services", Description:="")>
    Public Property RoomIDServices() As Integer
      Get
        Return GetProperty(RoomIDServicesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomIDServicesProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDOwnrProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceIDOwnr, "Resource ID Ownr")
    ''' <summary>
    ''' Gets and sets the Resource ID Ownr value
    ''' </summary>
    <Display(Name:="Resource ID Ownr", Description:="")>
    Public Property ResourceIDOwnr() As Integer
      Get
        Return GetProperty(ResourceIDOwnrProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceIDOwnrProperty, Value)
      End Set
    End Property

    Public Shared ResourceNameOwnrProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceNameOwnr, "Room")
    ''' <summary>
    ''' Gets and sets the Resource Name Ownr value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property ResourceNameOwnr() As String
      Get
        Return GetProperty(ResourceNameOwnrProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceNameOwnrProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDServicesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceIDServices, "Resource ID Services")
    ''' <summary>
    ''' Gets and sets the Resource ID Services value
    ''' </summary>
    <Display(Name:="Resource ID Services", Description:="")>
    Public Property ResourceIDServices() As Integer
      Get
        Return GetProperty(ResourceIDServicesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceIDServicesProperty, Value)
      End Set
    End Property

    Public Shared ResourceNameServicesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceNameServices, "Resource Name Services")
    ''' <summary>
    ''' Gets and sets the Resource Name Services value
    ''' </summary>
    <Display(Name:="Resource Name Services", Description:="")>
    Public Property ResourceNameServices() As String
      Get
        Return GetProperty(ResourceNameServicesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceNameServicesProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared NotAddedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.NotAdded, "Not Added", False)
    ''' <summary>
    ''' Gets and sets the Not Added value
    ''' </summary>
    <Display(Name:="Not Added", Description:="")>
    Public Property NotAdded() As Boolean
      Get
        Return GetProperty(NotAddedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(NotAddedProperty, Value)
      End Set
    End Property

    Public Shared RoomDifferentProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RoomDifferent, "Room Different", False)
    ''' <summary>
    ''' Gets and sets the Room Different value
    ''' </summary>
    <Display(Name:="Room Different", Description:="")>
    Public Property RoomDifferent() As Boolean
      Get
        Return GetProperty(RoomDifferentProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RoomDifferentProperty, Value)
      End Set
    End Property

    Public Shared StartTimeDifferentProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.StartTimeDifferent, "Start Time Different", False)
    ''' <summary>
    ''' Gets and sets the Start Time Different value
    ''' </summary>
    <Display(Name:="Start Time Different", Description:="")>
    Public Property StartTimeDifferent() As Boolean
      Get
        Return GetProperty(StartTimeDifferentProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(StartTimeDifferentProperty, Value)
      End Set
    End Property

    Public Shared EndTimeDifferentProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.EndTimeDifferent, "End Time Different", False)
    ''' <summary>
    ''' Gets and sets the End Time Different value
    ''' </summary>
    <Display(Name:="End Time Different", Description:="")>
    Public Property EndTimeDifferent() As Boolean
      Get
        Return GetProperty(EndTimeDifferentProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(EndTimeDifferentProperty, Value)
      End Set
    End Property

    Public Shared PSAIDOwnrProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PSAIDOwnr, "PSAID Ownr")
    ''' <summary>
    ''' Gets and sets the PSAID Ownr value
    ''' </summary>
    <Display(Name:="PSAID Ownr", Description:="")>
    Public Property PSAIDOwnr() As Integer
      Get
        Return GetProperty(PSAIDOwnrProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PSAIDOwnrProperty, Value)
      End Set
    End Property

    Public Shared PSAIDServicesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PSAIDServices, "PSAID Services")
    ''' <summary>
    ''' Gets and sets the PSAID Services value
    ''' </summary>
    <Display(Name:="PSAID Services", Description:="")>
    Public Property PSAIDServices() As Integer
      Get
        Return GetProperty(PSAIDServicesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PSAIDServicesProperty, Value)
      End Set
    End Property

    Public Shared CallTimeStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CallTimeStart, "Call Time Start")
    ''' <summary>
    ''' Gets and sets the Call Time Start value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    DateField(FormatString:="HH:mm")>
    Public Property CallTimeStart As DateTime?
      Get
        Return GetProperty(CallTimeStartProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeStartProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    DateField(FormatString:="HH:mm")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    DateField(FormatString:="HH:mm")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeEndProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.WrapTimeEnd, "Wrap Time End")
    ''' <summary>
    ''' Gets and sets the Wrap Time End value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    DateField(FormatString:="HH:mm")>
    Public Property WrapTimeEnd As DateTime?
      Get
        Return GetProperty(WrapTimeEndProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeEndProperty, Value)
      End Set
    End Property

    Public Shared CallTimeServicesProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CallTimeServices, "Call Time Services")
    ''' <summary>
    ''' Gets and sets the Call Time Services value
    ''' </summary>
    <Display(Name:="Call Time Services", Description:="")>
    Public Property CallTimeServices As DateTime?
      Get
        Return GetProperty(CallTimeServicesProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeServicesProperty, Value)
      End Set
    End Property

    Public Shared OnAirStartTimeServicesProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OnAirStartTimeServices, "On Air Start Time Services")
    ''' <summary>
    ''' Gets and sets the On Air Start Time Services value
    ''' </summary>
    <Display(Name:="On Air Start Time Services", Description:="")>
    Public Property OnAirStartTimeServices As DateTime?
      Get
        Return GetProperty(OnAirStartTimeServicesProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OnAirStartTimeServicesProperty, Value)
      End Set
    End Property

    Public Shared OnAirEndTimeServicesProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OnAirEndTimeServices, "On Air End Time Services")
    ''' <summary>
    ''' Gets and sets the On Air End Time Services value
    ''' </summary>
    <Display(Name:="On Air End Time Services", Description:="")>
    Public Property OnAirEndTimeServices As DateTime?
      Get
        Return GetProperty(OnAirEndTimeServicesProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OnAirEndTimeServicesProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeServicesProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.WrapTimeServices, "Wrap Time Services")
    ''' <summary>
    ''' Gets and sets the Wrap Time Services value
    ''' </summary>
    <Display(Name:="Wrap Time Services", Description:="")>
    Public Property WrapTimeServices As DateTime?
      Get
        Return GetProperty(WrapTimeServicesProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeServicesProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As String
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ModifiedBy, "Modified By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As String
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared PendingDeleteProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PendingDelete, "PendingDelete", False)
    ''' <summary>
    ''' Gets and sets the Room Different value
    ''' </summary>
    <Display(Name:="Pending Delete", Description:="")>
    Public Property PendingDelete() As Boolean
      Get
        Return GetProperty(PendingDeleteProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PendingDeleteProperty, Value)
      End Set
    End Property

    Public Shared CallTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CallTimeString, "Created By", "")
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    ''' 
    <Display(Name:="Call Date", Description:="")>
    Public ReadOnly Property CallTimeString() As String
      Get
        Return GetProperty(CallTimeStringProperty)
      End Get
    End Property

    Public Shared RoomChangeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomChangeDescription, "Room Change", "")
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    Public ReadOnly Property RoomChangeDescription() As String
      Get
        Return GetProperty(RoomChangeDescriptionProperty)
      End Get
    End Property

    Public Shared StartTimeChangeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StartTimeChangeDescription, "StartTimeChangeDescription", "")
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    Public ReadOnly Property StartTimeChangeDescription() As String
      Get
        Return GetProperty(StartTimeChangeDescriptionProperty)
      End Get
    End Property

    Public Shared EndTimeChangeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EndTimeChangeDescription, "EndTimeChangeDescription", "")
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    Public ReadOnly Property EndTimeChangeDescription() As String
      Get
        Return GetProperty(EndTimeChangeDescriptionProperty)
      End Get
    End Property

    Public Shared CrewCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewCount, "Crew")
    ''' <summary>
    ''' Gets and sets the PSAID Services value
    ''' </summary>
    <Display(Name:="Crew", Description:="")>
    Public Property CrewCount() As Integer
      Get
        Return GetProperty(CrewCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CrewCountProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceNameOwnr.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Content Services Difference")
        Else
          Return String.Format("Blank {0}", "Content Services Difference")
        End If
      Else
        Return Me.ResourceNameOwnr
      End If

    End Function

    'Public Function ActionContentServicesDifference(ContentServicesDifference As ContentServicesDifference) As Singular.Web.Result

    '  Dim cmd As Singular.CommandProc = New Singular.CommandProc("CmdProcs.cmdContentServicesDifferenceUpdate",
    '                                                             New String() {"@RoomScheduleID", "@PendingDelete", "@NotAdded", "@CurrentUserID"},
    '                                                             New Object() {ContentServicesDifference.RoomScheduleID, ContentServicesDifference.PendingDelete, ContentServicesDifference.NotAdded,
    '                                                                           OBLib.Security.Settings.CurrentUserID})
    '  cmd.UseTransaction = True
    '  Try
    '    cmd = cmd.Execute
    '    Return New Singular.Web.Result(True)
    '  Catch ex As Exception
    '    OBLib.OBMisc.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, "", "ActionContentServicesDifference", ex.Message)
    '    Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
    '  End Try
    'End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewContentServicesDifference() method.

    End Sub

    Public Shared Function NewContentServicesDifference() As ContentServicesDifference

      Return DataPortal.CreateChild(Of ContentServicesDifference)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetContentServicesDifference(dr As SafeDataReader) As ContentServicesDifference

      Dim c As New ContentServicesDifference()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
          LoadProperty(RoomIDOwnrProperty, .GetInt32(1))
          LoadProperty(RoomIDServicesProperty, .GetInt32(2))
          LoadProperty(ResourceIDOwnrProperty, .GetInt32(3))
          LoadProperty(ResourceNameOwnrProperty, .GetString(4))
          LoadProperty(ResourceIDServicesProperty, .GetInt32(5))
          LoadProperty(ResourceNameServicesProperty, .GetString(6))
          LoadProperty(TitleProperty, .GetString(7))
          LoadProperty(NotAddedProperty, .GetBoolean(8))
          LoadProperty(RoomDifferentProperty, .GetBoolean(9))
          LoadProperty(StartTimeDifferentProperty, .GetBoolean(10))
          LoadProperty(EndTimeDifferentProperty, .GetBoolean(11))
          LoadProperty(PSAIDOwnrProperty, .GetInt32(12))
          LoadProperty(PSAIDServicesProperty, .GetInt32(13))
          LoadProperty(CallTimeStartProperty, .GetValue(14))
          LoadProperty(StartDateTimeProperty, .GetValue(15))
          LoadProperty(EndDateTimeProperty, .GetValue(16))
          LoadProperty(WrapTimeEndProperty, .GetValue(17))
          LoadProperty(CallTimeServicesProperty, .GetValue(18))
          LoadProperty(OnAirStartTimeServicesProperty, .GetValue(19))
          LoadProperty(OnAirEndTimeServicesProperty, .GetValue(20))
          LoadProperty(WrapTimeServicesProperty, .GetValue(21))
          LoadProperty(CreatedByProperty, .GetString(22))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(23))
          LoadProperty(ModifiedByProperty, .GetString(24))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(25))
          LoadProperty(PendingDeleteProperty, .GetBoolean(26))
          LoadProperty(CallTimeStringProperty, .GetString(27))
          LoadProperty(RoomChangeDescriptionProperty, .GetString(28))
          LoadProperty(StartTimeChangeDescriptionProperty, .GetString(29))
          LoadProperty(EndTimeChangeDescriptionProperty, .GetString(30))
          LoadProperty(CrewCountProperty, .GetInt32(31))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      cm.CommandText = "UpdProcsWeb.updContentServicesStudioDifference"
      'AddPrimaryKeyParam(cm, RoomScheduleIDProperty)

      cm.Parameters.AddWithValue("@RoomScheduleID", GetProperty(RoomScheduleIDProperty))
      cm.Parameters.AddWithValue("@RoomIDOwnr", GetProperty(RoomIDOwnrProperty))
      cm.Parameters.AddWithValue("@RoomIDServices", NothingDBNull(GetProperty(RoomIDServicesProperty)))
      cm.Parameters.AddWithValue("@ResourceIDOwnr", GetProperty(ResourceIDOwnrProperty))
      cm.Parameters.AddWithValue("@ResourceNameOwnr", GetProperty(ResourceNameOwnrProperty))
      cm.Parameters.AddWithValue("@ResourceIDServices", NothingDBNull(GetProperty(ResourceIDServicesProperty)))
      cm.Parameters.AddWithValue("@ResourceNameServices", GetProperty(ResourceNameServicesProperty))
      cm.Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      cm.Parameters.AddWithValue("@NotAdded", GetProperty(NotAddedProperty))
      cm.Parameters.AddWithValue("@RoomDifferent", GetProperty(RoomDifferentProperty))
      cm.Parameters.AddWithValue("@StartTimeDifferent", GetProperty(StartTimeDifferentProperty))
      cm.Parameters.AddWithValue("@EndTimeDifferent", GetProperty(EndTimeDifferentProperty))
      cm.Parameters.AddWithValue("@PSAIDOwnr", NothingDBNull(GetProperty(PSAIDOwnrProperty)))
      cm.Parameters.AddWithValue("@PSAIDServices", NothingDBNull(GetProperty(PSAIDServicesProperty)))
      cm.Parameters.AddWithValue("@CallTimeStart", NothingDBNull(CallTimeStart))
      cm.Parameters.AddWithValue("@StartDateTime", NothingDBNull(StartDateTime))
      cm.Parameters.AddWithValue("@EndDateTime", NothingDBNull(EndDateTime))
      cm.Parameters.AddWithValue("@WrapTimeEnd", NothingDBNull(WrapTimeEnd))
      cm.Parameters.AddWithValue("@CallTimeServices", NothingDBNull(CallTimeServices))
      cm.Parameters.AddWithValue("@OnAirStartTimeServices", NothingDBNull(OnAirStartTimeServices))
      cm.Parameters.AddWithValue("@OnAirEndTimeServices", NothingDBNull(OnAirEndTimeServices))
      cm.Parameters.AddWithValue("@WrapTimeServices", NothingDBNull(WrapTimeServices))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@PendingDelete", GetProperty(PendingDeleteProperty))
      cm.Parameters.AddWithValue("@CrewCount", GetProperty(CrewCountProperty))

      Return Sub()
               ''Post Save
               'If Me.IsNew Then
               '  LoadProperty(RoomScheduleIDProperty, cm.Parameters("@RoomScheduleID").Value)
               'End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@RoomScheduleID", GetProperty(RoomScheduleIDProperty))
    End Sub

#End Region

  End Class

End Namespace