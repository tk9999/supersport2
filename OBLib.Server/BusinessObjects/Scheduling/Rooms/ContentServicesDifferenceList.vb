﻿' Generated 23 Sep 2016 22:50 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Scheduling.Rooms

  <Serializable()> _
  Public Class ContentServicesDifferenceList
    Inherits OBBusinessListBase(Of ContentServicesDifferenceList, ContentServicesDifference)

#Region " Business Methods "

    Public Function GetItem(RoomScheduleID As Integer) As ContentServicesDifference

      For Each child As ContentServicesDifference In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Shared StartDateProperty As PropertyInfo(Of Date?) = RegisterProperty(Of Date?)(Function(c) c.StartDate, "Start Date", Now)
      ''' <summary>
      ''' Gets and sets the Start Date Time value
      ''' </summary>
      <Display(Name:="Start Date"), Required(ErrorMessage:="Start Date is required"),
      SetExpression("ContentServicesDifferenceListCriteriaBO.StartDateSet(self)")>
      Public Property StartDate As Date?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As Date?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of Date?) = RegisterProperty(Of Date?)(Function(c) c.EndDate, "End Date", Now)
      ''' <summary>
      ''' Gets and sets the Start Date Time value
      ''' </summary>
      <Display(Name:="End Date"), Required(ErrorMessage:="End Date is required"),
      SetExpression("ContentServicesDifferenceListCriteriaBO.EndDateSet(self)")>
      Public Property EndDate As Date?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As Date?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Property IsProcessing As Boolean

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewContentServicesDifferenceList() As ContentServicesDifferenceList

      Return New ContentServicesDifferenceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetContentServicesDifferenceList() As ContentServicesDifferenceList

      Return DataPortal.Fetch(Of ContentServicesDifferenceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ContentServicesDifference.GetContentServicesDifference(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getContentAndServicesDifferenceList"
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace