﻿' Generated 17 Feb 2016 11:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomBookingList
    Inherits OBReadOnlyListBase(Of RORoomBookingList, RORoomBooking)

#Region " Business Methods "

    Public Function GetItem(RoomScheduleID As Integer) As RORoomBooking

      For Each child As RORoomBooking In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetRORoomBookingArea(ProductionSystemAreaID As Integer) As RORoomBookingArea

      Dim obj As RORoomBookingArea = Nothing
      For Each parent As RORoomBooking In Me
        obj = parent.RORoomBookingAreaList.GetItem(ProductionSystemAreaID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetRORoomBookingProduction(ProductionID As Integer) As RORoomBookingProduction

      Dim obj As RORoomBookingProduction = Nothing
      For Each parent As RORoomBooking In Me
        obj = parent.RORoomBookingProductionList.GetItem(ProductionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetRORoomBookingAdHocBooking(AdHocBookingID As Integer) As RORoomBookingAdHocBooking

      Dim obj As RORoomBookingAdHocBooking = Nothing
      For Each parent As RORoomBooking In Me
        obj = parent.RORoomBookingAdHocBookingList.GetItem(AdHocBookingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=False)> _
    Public Class Criteria
      Inherits SingularCriteriaBase(Of Criteria)

      Public Property RoomScheduleID As Integer? = Nothing
      Public Property ProductionSystemAreaID As Integer? = Nothing

      Public Sub New(RoomScheduleID As Integer?, ProductionSystemAreaID As Integer?)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
        Me.RoomScheduleID = RoomScheduleID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORoomBookingList() As RORoomBookingList

      Return New RORoomBookingList()

    End Function

    Public Shared Sub BeginGetRORoomBookingList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORoomBookingList)))

      Dim dp As New DataPortal(Of RORoomBookingList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRORoomBookingList(CallBack As EventHandler(Of DataPortalResult(Of RORoomBookingList)))

      Dim dp As New DataPortal(Of RORoomBookingList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORoomBookingList() As RORoomBookingList

      Return DataPortal.Fetch(Of RORoomBookingList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORoomBooking.GetRORoomBooking(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As RORoomBooking = Nothing
      If Me.Count > 0 Then
        If Me(0).RoomScheduleTypeID = CType(OBLib.CommonData.Enums.RoomScheduleType.Production, Integer) Then
          If sdr.NextResult() Then
            While sdr.Read
              parent = Me(0)
              'If parent Is Nothing OrElse parent.ProductionID <> sdr.GetInt32(0) Then
              '  parent = Me.GetRORoomBookingProduction(sdr.GetInt32(0))
              'End If
              parent.RORoomBookingProductionList.RaiseListChangedEvents = False
              parent.RORoomBookingProductionList.Add(RORoomBookingProduction.GetRORoomBookingProduction(sdr))
              parent.RORoomBookingProductionList.RaiseListChangedEvents = True
            End While
          End If
        End If

        If Me(0).RoomScheduleTypeID = CType(OBLib.CommonData.Enums.RoomScheduleType.AdHoc, Integer) Then
          If sdr.NextResult() Then
            While sdr.Read
              parent = Me(0)
              'If parent Is Nothing OrElse parent.AdHocBookingID <> sdr.GetInt32(0) Then
              '  parent = Me.GetItem(sdr.GetInt32(0))
              'End If
              parent.RORoomBookingAdHocBookingList.RaiseListChangedEvents = False
              parent.RORoomBookingAdHocBookingList.Add(RORoomBookingAdHocBooking.GetRORoomBookingAdHocBooking(sdr))
              parent.RORoomBookingAdHocBookingList.RaiseListChangedEvents = True
            End While
          End If
        End If
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.RoomScheduleID <> sdr.GetInt32(14) Then
            parent = Me.GetItem(sdr.GetInt32(14))
          End If
          If parent IsNot Nothing Then
            parent.RORoomBookingAreaList.RaiseListChangedEvents = False
            parent.RORoomBookingAreaList.Add(RORoomBookingArea.GetRORoomBookingArea(sdr))
            parent.RORoomBookingAreaList.RaiseListChangedEvents = True
          End If
        End While
      End If

      Dim parentChild As RORoomBookingArea = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.ProductionSystemAreaID <> sdr.GetInt32(21) Then
            parentChild = Me.GetRORoomBookingArea(sdr.GetInt32(21))
          End If
          If parentChild IsNot Nothing Then
            parentChild.RORoomBookingAreaPHRList.RaiseListChangedEvents = False
            parentChild.RORoomBookingAreaPHRList.Add(RORoomBookingAreaPHR.GetRORoomBookingAreaPHR(sdr))
            parentChild.RORoomBookingAreaPHRList.RaiseListChangedEvents = True
          End If
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORoomBookingList"
            cm.Parameters.AddWithValue("@RoomScheduleID", NothingDBNull(crit.RoomScheduleID))
            cm.Parameters.AddWithValue("@CurrentUserID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace