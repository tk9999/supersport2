﻿' Generated 17 Feb 2016 11:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomBookingAreaPHR
    Inherits OBReadOnlyBase(Of RORoomBookingAreaPHR)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ProductionHumanResourceID() As Integer
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
  Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
  Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionID, "Position")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
  Public ReadOnly Property PositionID() As Integer
      Get
        Return GetProperty(PositionIDProperty)
      End Get
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionTypeID, "Position Type")
    ''' <summary>
    ''' Gets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
  Public ReadOnly Property PositionTypeID() As Integer
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
    End Property

    Public Shared PreferredHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PreferredHumanResourceID, "Preferred Human Resource")
    ''' <summary>
    ''' Gets the Preferred Human Resource value
    ''' </summary>
    <Display(Name:="Preferred Human Resource", Description:="")>
  Public ReadOnly Property PreferredHumanResourceID() As Integer
      Get
        Return GetProperty(PreferredHumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SelectionReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectionReason, "Selection Reason")
    ''' <summary>
    ''' Gets the Selection Reason value
    ''' </summary>
    <Display(Name:="Selection Reason", Description:="")>
  Public ReadOnly Property SelectionReason() As String
      Get
        Return GetProperty(SelectionReasonProperty)
      End Get
    End Property

    Public Shared CancelledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDate, "Cancelled Date")
    ''' <summary>
    ''' Gets the Cancelled Date value
    ''' </summary>
    <Display(Name:="Cancelled Date", Description:="")>
  Public ReadOnly Property CancelledDate As DateTime?
      Get
        Return GetProperty(CancelledDateProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="")>
  Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CrewLeaderIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CrewLeaderInd, "Crew Leader", False)
    ''' <summary>
    ''' Gets the Crew Leader value
    ''' </summary>
    <Display(Name:="Crew Leader", Description:="")>
  Public ReadOnly Property CrewLeaderInd() As Boolean
      Get
        Return GetProperty(CrewLeaderIndProperty)
      End Get
    End Property

    Public Shared TrainingIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TrainingInd, "Training", False)
    ''' <summary>
    ''' Gets the Training value
    ''' </summary>
    <Display(Name:="Training", Description:="")>
  Public ReadOnly Property TrainingInd() As Boolean
      Get
        Return GetProperty(TrainingIndProperty)
      End Get
    End Property

    Public Shared TBCIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TBCInd, "TBC", False)
    ''' <summary>
    ''' Gets the TBC value
    ''' </summary>
    <Display(Name:="TBC", Description:="")>
  Public ReadOnly Property TBCInd() As Boolean
      Get
        Return GetProperty(TBCIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleID, "Vehicle")
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
  Public ReadOnly Property VehicleID() As Integer
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
  Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared ReliefCrewIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ReliefCrewInd, "Relief Crew", False)
    ''' <summary>
    ''' Gets the Relief Crew value
    ''' </summary>
    <Display(Name:="Relief Crew", Description:="")>
  Public ReadOnly Property ReliefCrewInd() As Boolean
      Get
        Return GetProperty(ReliefCrewIndProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Production System Area")
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared HRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRName, "HR Name")
    ''' <summary>
    ''' Gets the HR Name value
    ''' </summary>
    <Display(Name:="HR Name", Description:="")>
  Public ReadOnly Property HRName() As String
      Get
        Return GetProperty(HRNameProperty)
      End Get
    End Property

    Public Shared PrefHRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PrefHRName, "Pref HR Name")
    ''' <summary>
    ''' Gets the Pref HR Name value
    ''' </summary>
    <Display(Name:="Pref HR Name", Description:="")>
  Public ReadOnly Property PrefHRName() As String
      Get
        Return GetProperty(PrefHRNameProperty)
      End Get
    End Property

    Public Shared OrderNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNo, "Order No")
    ''' <summary>
    ''' Gets the Order No value
    ''' </summary>
    <Display(Name:="Order No", Description:="")>
  Public ReadOnly Property OrderNo() As Integer
      Get
        Return GetProperty(OrderNoProperty)
      End Get
    End Property

    Public Shared PriorityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Priority, "Priority")
    ''' <summary>
    ''' Gets the Priority value
    ''' </summary>
    <Display(Name:="Priority", Description:="")>
  Public ReadOnly Property Priority() As Integer
      Get
        Return GetProperty(PriorityProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
  Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
  Public ReadOnly Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
    End Property

    Public Shared DisciplinePositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DisciplinePosition, "Discipline Position")
    ''' <summary>
    ''' Gets the Discipline Position value
    ''' </summary>
    <Display(Name:="Discipline Position", Description:="")>
  Public ReadOnly Property DisciplinePosition() As String
      Get
        Return GetProperty(DisciplinePositionProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
  Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Start Date Time Buffer")
    ''' <summary>
    ''' Gets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Date Time Buffer", Description:="")>
  Public ReadOnly Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
  Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
  Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "End Date Time Buffer")
    ''' <summary>
    ''' Gets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Date Time Buffer", Description:="")>
  Public ReadOnly Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
    End Property

    <Display(Name:="Booking Times", Description:="")>
    Public ReadOnly Property BookingTimes() As String
      Get
        Dim ct As String = ""
        If Me.StartDateTimeBuffer IsNot Nothing Then
          ct = Me.StartDateTimeBuffer.Value.ToString("dd MMM yy HH:mm")
        End If
        Dim wt As String = ""
        If Me.EndDateTimeBuffer IsNot Nothing Then
          wt = Me.EndDateTimeBuffer.Value.ToString("dd MMM yy HH:mm")
        End If
        Return ct & IIf(ct.Trim <> "", " - ", "") & wt
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SelectionReason

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORoomBookingAreaPHR(dr As SafeDataReader) As RORoomBookingAreaPHR

      Dim r As New RORoomBookingAreaPHR()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(PreferredHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(SelectionReasonProperty, .GetString(7))
        LoadProperty(CancelledDateProperty, .GetValue(8))
        LoadProperty(CancelledReasonProperty, .GetString(9))
        LoadProperty(CrewLeaderIndProperty, .GetBoolean(10))
        LoadProperty(TrainingIndProperty, .GetBoolean(11))
        LoadProperty(TBCIndProperty, .GetBoolean(12))
        LoadProperty(CreatedByProperty, .GetInt32(13))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(14))
        LoadProperty(ModifiedByProperty, .GetInt32(15))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(16))
        LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
        LoadProperty(CommentsProperty, .GetString(19))
        LoadProperty(ReliefCrewIndProperty, .GetBoolean(20))
        LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
        LoadProperty(HRNameProperty, .GetString(23))
        LoadProperty(PrefHRNameProperty, .GetString(24))
        LoadProperty(OrderNoProperty, .GetInt32(25))
        LoadProperty(PriorityProperty, .GetInt32(26))
        LoadProperty(DisciplineProperty, .GetString(27))
        LoadProperty(PositionProperty, .GetString(28))
        LoadProperty(DisciplinePositionProperty, .GetString(29))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(30)))
        LoadProperty(StartDateTimeBufferProperty, .GetValue(31))
        LoadProperty(StartDateTimeProperty, .GetValue(32))
        LoadProperty(EndDateTimeProperty, .GetValue(33))
        LoadProperty(EndDateTimeBufferProperty, .GetValue(34))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace