﻿' Generated 17 Feb 2016 11:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomBookingArea
    Inherits OBReadOnlyBase(Of RORoomBookingArea)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
  Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
  Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaStatusID, "Production Area Status")
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:="")>
  Public ReadOnly Property ProductionAreaStatusID() As Integer
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared StatusDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StatusDate, "Status Date")
    ''' <summary>
    ''' Gets the Status Date value
    ''' </summary>
    <Display(Name:="Status Date", Description:="")>
  Public ReadOnly Property StatusDate As DateTime?
      Get
        Return GetProperty(StatusDateProperty)
      End Get
    End Property

    Public Shared StatusSetByUserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StatusSetByUserID, "Status Set By User")
    ''' <summary>
    ''' Gets the Status Set By User value
    ''' </summary>
    <Display(Name:="Status Set By User", Description:="")>
  Public ReadOnly Property StatusSetByUserID() As Integer
      Get
        Return GetProperty(StatusSetByUserIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSpecRequirementID, "Production Spec Requirement")
    ''' <summary>
    ''' Gets the Production Spec Requirement value
    ''' </summary>
    <Display(Name:="Production Spec Requirement", Description:="")>
  Public ReadOnly Property ProductionSpecRequirementID() As Integer
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
    End Property

    Public Shared NotComplySpecReasonsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotComplySpecReasons, "Not Comply Spec Reasons")
    ''' <summary>
    ''' Gets the Not Comply Spec Reasons value
    ''' </summary>
    <Display(Name:="Not Comply Spec Reasons", Description:="")>
  Public ReadOnly Property NotComplySpecReasons() As String
      Get
        Return GetProperty(NotComplySpecReasonsProperty)
      End Get
    End Property

    Public Shared StatusSetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StatusSetDate, "Status Set Date")
    ''' <summary>
    ''' Gets the Status Set Date value
    ''' </summary>
    <Display(Name:="Status Set Date", Description:="")>
  Public ReadOnly Property StatusSetDate As DateTime?
      Get
        Return GetProperty(StatusSetDateProperty)
      End Get
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "Room Schedule")
    ''' <summary>
    ''' Gets the Room Schedule value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionSpecRequirementName, "Production Spec Requirement Name")
    ''' <summary>
    ''' Gets the Production Spec Requirement Name value
    ''' </summary>
    <Display(Name:="Production Spec Requirement Name", Description:="")>
  Public ReadOnly Property ProductionSpecRequirementName() As String
      Get
        Return GetProperty(ProductionSpecRequirementNameProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Production Area Status")
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:="")>
  Public ReadOnly Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
    End Property

    Public Shared HasDoneAreaSetupIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasDoneAreaSetupInd, "Has Done Area Setup", False)
    ''' <summary>
    ''' Gets the Has Done Area Setup value
    ''' </summary>
    <Display(Name:="Has Done Area Setup", Description:="")>
  Public ReadOnly Property HasDoneAreaSetupInd() As Boolean
      Get
        Return GetProperty(HasDoneAreaSetupIndProperty)
      End Get
    End Property

    Public Shared AreaCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AreaComments, "Area Comments")
    ''' <summary>
    ''' Gets the Area Comments value
    ''' </summary>
    <Display(Name:="Area Comments", Description:="")>
  Public ReadOnly Property AreaComments() As String
      Get
        Return GetProperty(AreaCommentsProperty)
      End Get
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdHocBookingID, "Ad Hoc Booking")
    ''' <summary>
    ''' Gets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking", Description:="")>
  Public ReadOnly Property AdHocBookingID() As Integer
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Start Date Time Buffer")
    ''' <summary>
    ''' Gets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Date Time Buffer", Description:="")>
  Public ReadOnly Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
  Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
  Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "End Date Time Buffer")
    ''' <summary>
    ''' Gets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Date Time Buffer", Description:="")>
  Public ReadOnly Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
    End Property

    <Display(Name:="Booking Times", Description:="")>
    Public ReadOnly Property BookingTimes() As String
      Get
        Dim ct As String = ""
        If Me.StartDateTimeBuffer IsNot Nothing Then
          ct = Me.StartDateTimeBuffer.Value.ToString("dd MMM yy HH:mm")
        End If
        Dim wt As String = ""
        If Me.EndDateTimeBuffer IsNot Nothing Then
          wt = Me.EndDateTimeBuffer.Value.ToString("dd MMM yy HH:mm")
        End If
        Return ct & IIf(ct.Trim <> "", " - ", "") & wt
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared RORoomBookingAreaPHRListProperty As PropertyInfo(Of RORoomBookingAreaPHRList) = RegisterProperty(Of RORoomBookingAreaPHRList)(Function(c) c.RORoomBookingAreaPHRList, "RO Room Booking Area PHR List")

    Public ReadOnly Property RORoomBookingAreaPHRList() As RORoomBookingAreaPHRList
      Get
        If GetProperty(RORoomBookingAreaPHRListProperty) Is Nothing Then
          LoadProperty(RORoomBookingAreaPHRListProperty, Rooms.ReadOnly.RORoomBookingAreaPHRList.NewRORoomBookingAreaPHRList())
        End If
        Return GetProperty(RORoomBookingAreaPHRListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSystemAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.NotComplySpecReasons

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORoomBookingArea(dr As SafeDataReader) As RORoomBookingArea

      Dim r As New RORoomBookingArea()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionSystemAreaIDProperty, .GetInt32(0))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(StatusDateProperty, .GetValue(5))
        LoadProperty(StatusSetByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(CreatedByProperty, .GetInt32(7))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(8))
        LoadProperty(ModifiedByProperty, .GetInt32(9))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(10))
        LoadProperty(ProductionSpecRequirementIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(NotComplySpecReasonsProperty, .GetString(12))
        LoadProperty(StatusSetDateProperty, .GetValue(13))
        LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(ProductionSpecRequirementNameProperty, .GetString(15))
        LoadProperty(ProductionAreaStatusProperty, .GetString(16))
        LoadProperty(HasDoneAreaSetupIndProperty, .GetBoolean(17))
        LoadProperty(AreaCommentsProperty, .GetString(18))
        LoadProperty(AdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
        LoadProperty(StartDateTimeBufferProperty, .GetValue(20))
        LoadProperty(StartDateTimeProperty, .GetValue(21))
        LoadProperty(EndDateTimeProperty, .GetValue(22))
        LoadProperty(EndDateTimeBufferProperty, .GetValue(23))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace