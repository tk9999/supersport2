﻿' Generated 17 Feb 2016 11:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomBookingAdHocBooking
    Inherits OBReadOnlyBase(Of RORoomBookingAdHocBooking)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdHocBookingID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AdHocBookingID() As Integer
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
    End Property

    Public Shared AdHocBookingTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdHocBookingTypeID, "Ad Hoc Booking Type")
    ''' <summary>
    ''' Gets the Ad Hoc Booking Type value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking Type", Description:="")>
  Public ReadOnly Property AdHocBookingTypeID() As Integer
      Get
        Return GetProperty(AdHocBookingTypeIDProperty)
      End Get
    End Property

    <Display(Name:="Type", Description:="")>
    Public ReadOnly Property AdHocBookingType() As String
      Get
        Dim abt As OBLib.Maintenance.AdHoc.ReadOnly.ROAdHocBookingType = OBLib.CommonData.Lists.ROAdHocBookingTypeList.GetItem(AdHocBookingTypeID)
        If abt IsNot Nothing Then
          Return abt.AdHocBookingType
        End If
        Return "Unknown"
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
  Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
  Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
  Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
  Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
  Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared CountryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CountryID, "Country")
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="")>
  Public ReadOnly Property CountryID() As Integer
      Get
        Return GetProperty(CountryIDProperty)
      End Get
    End Property

    Public Shared RequiresTravelProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiresTravel, "Requires Travel", False)
    ''' <summary>
    ''' Gets the Requires Travel value
    ''' </summary>
    <Display(Name:="Requires Travel", Description:="")>
  Public ReadOnly Property RequiresTravel() As Boolean
      Get
        Return GetProperty(RequiresTravelProperty)
      End Get
    End Property

    Public Shared GenRefNoProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNo, "Gen Ref No")
    ''' <summary>
    ''' Gets the Gen Ref No value
    ''' </summary>
    <Display(Name:="Gen Ref No", Description:="")>
  Public ReadOnly Property GenRefNo() As Int64
      Get
        Return GetProperty(GenRefNoProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared LocationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.LocationID, "Location")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
  Public ReadOnly Property LocationID() As Integer
      Get
        Return GetProperty(LocationIDProperty)
      End Get
    End Property

    Public Shared CostCentreIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CostCentreID, "Cost Centre")
    ''' <summary>
    ''' Gets the Cost Centre value
    ''' </summary>
    <Display(Name:="Cost Centre", Description:="")>
  Public ReadOnly Property CostCentreID() As Integer
      Get
        Return GetProperty(CostCentreIDProperty)
      End Get
    End Property

    Public Shared UsingDefaultGenRefReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.UsingDefaultGenRefReason, "Using Default Gen Ref Reason")
    ''' <summary>
    ''' Gets the Using Default Gen Ref Reason value
    ''' </summary>
    <Display(Name:="Using Default Gen Ref Reason", Description:="")>
  Public ReadOnly Property UsingDefaultGenRefReason() As String
      Get
        Return GetProperty(UsingDefaultGenRefReasonProperty)
      End Get
    End Property

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country")
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="")>
  Public ReadOnly Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
  Public ReadOnly Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
    End Property

    <Display(Name:="Country (Location")>
    Public ReadOnly Property CountryLocation As String
      Get
        Return Country & IIf(Location.Trim.Length > 0, " (" & Location & ")", "")
      End Get
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Start Date Time Buffer")
    ''' <summary>
    ''' Gets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Date Time Buffer", Description:="")>
  Public ReadOnly Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "End Date Time Buffer")
    ''' <summary>
    ''' Gets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Date Time Buffer", Description:="")>
  Public ReadOnly Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
    End Property

    Public Shared SubDeptAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubDeptArea, "Sub Dept Area")
    ''' <summary>
    ''' Gets the Sub Dept Area value
    ''' </summary>
    <Display(Name:="Sub Dept Area", Description:="")>
  Public ReadOnly Property SubDeptArea() As String
      Get
        Return GetProperty(SubDeptAreaProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AdHocBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Title

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORoomBookingAdHocBooking(dr As SafeDataReader) As RORoomBookingAdHocBooking

      Dim r As New RORoomBookingAdHocBooking()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AdHocBookingIDProperty, .GetInt32(0))
        LoadProperty(AdHocBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(TitleProperty, .GetString(2))
        LoadProperty(DescriptionProperty, .GetString(3))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(StartDateTimeProperty, .GetValue(6))
        LoadProperty(EndDateTimeProperty, .GetValue(7))
        LoadProperty(CountryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(RequiresTravelProperty, .GetBoolean(9))
        LoadProperty(GenRefNoProperty, .GetInt64(10))
        LoadProperty(CreatedByProperty, .GetInt32(11))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(12))
        LoadProperty(ModifiedByProperty, .GetInt32(13))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(14))
        LoadProperty(LocationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
        LoadProperty(CostCentreIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
        LoadProperty(UsingDefaultGenRefReasonProperty, .GetString(17))
        LoadProperty(CountryProperty, .GetString(18))
        LoadProperty(LocationProperty, .GetString(19))
        LoadProperty(StartDateTimeBufferProperty, .GetValue(20))
        LoadProperty(EndDateTimeBufferProperty, .GetValue(21))
        LoadProperty(SubDeptAreaProperty, .GetString(22))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace