﻿' Generated 17 Feb 2016 11:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomBooking
    Inherits OBReadOnlyBase(Of RORoomBooking)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared RoomScheduleTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleTypeID, "Room Schedule Type")
    ''' <summary>
    ''' Gets the Room Schedule Type value
    ''' </summary>
    <Display(Name:="Room Schedule Type", Description:="")>
  Public ReadOnly Property RoomScheduleTypeID() As Integer
      Get
        Return GetProperty(RoomScheduleTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
  Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
  Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
  Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdHocBookingID, "Ad Hoc Booking")
    ''' <summary>
    ''' Gets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking", Description:="")>
  Public ReadOnly Property AdHocBookingID() As Integer
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
  Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared CallTimeStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CallTimeStart, "Call Time Start")
    ''' <summary>
    ''' Gets the Call Time Start value
    ''' </summary>
    <Display(Name:="Call Time Start", Description:="")>
  Public ReadOnly Property CallTimeStart As DateTime?
      Get
        Return GetProperty(CallTimeStartProperty)
      End Get
    End Property

    Public Shared WrapTimeEndProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.WrapTimeEnd, "Wrap Time End")
    ''' <summary>
    ''' Gets the Wrap Time End value
    ''' </summary>
    <Display(Name:="Wrap Time End", Description:="")>
  Public ReadOnly Property WrapTimeEnd As DateTime?
      Get
        Return GetProperty(WrapTimeEndProperty)
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
  Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared LiveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.LiveInd, "Live", False)
    ''' <summary>
    ''' Gets the Live value
    ''' </summary>
    <Display(Name:="Live", Description:="")>
  Public ReadOnly Property LiveInd() As Boolean
      Get
        Return GetProperty(LiveIndProperty)
      End Get
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DebtorID, "Debtor")
    ''' <summary>
    ''' Gets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="")>
  Public ReadOnly Property DebtorID() As Integer
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
    End Property

    Public Shared DebtorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Debtor, "Debtor")
    ''' <summary>
    ''' Gets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="")>
  Public ReadOnly Property Debtor() As String
      Get
        Return GetProperty(DebtorProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaStatusID, "Production Area Status")
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:="")>
  Public ReadOnly Property ProductionAreaStatusID() As Integer
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Production Area Status")
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:="")>
  Public ReadOnly Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared IsOwnerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsOwner, "Is Owner", False)
    ''' <summary>
    ''' Gets the Is Owner value
    ''' </summary>
    <Display(Name:="Is Owner", Description:="")>
  Public ReadOnly Property IsOwner() As Boolean
      Get
        Return GetProperty(IsOwnerProperty)
      End Get
    End Property

    Public Shared OwnedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OwnedBy, "Owned By")
    ''' <summary>
    ''' Gets the Owned By value
    ''' </summary>
    <Display(Name:="Owned By", Description:="")>
  Public ReadOnly Property OwnedBy() As String
      Get
        Return GetProperty(OwnedByProperty)
      End Get
    End Property

    Public Shared OwningSystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OwningSystemID, "Owning System")
    ''' <summary>
    ''' Gets the Owning System value
    ''' </summary>
    <Display(Name:="Owning System", Description:="")>
  Public ReadOnly Property OwningSystemID() As Integer
      Get
        Return GetProperty(OwningSystemIDProperty)
      End Get
    End Property

    Public Shared OwningProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OwningProductionAreaID, "Owning Production Area")
    ''' <summary>
    ''' Gets the Owning Production Area value
    ''' </summary>
    <Display(Name:="Owning Production Area", Description:="")>
  Public ReadOnly Property OwningProductionAreaID() As Integer
      Get
        Return GetProperty(OwningProductionAreaIDProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared RORoomBookingAreaListProperty As PropertyInfo(Of RORoomBookingAreaList) = RegisterProperty(Of RORoomBookingAreaList)(Function(c) c.RORoomBookingAreaList, "RO Room Booking Area List")

    Public ReadOnly Property RORoomBookingAreaList() As RORoomBookingAreaList
      Get
        If GetProperty(RORoomBookingAreaListProperty) Is Nothing Then
          LoadProperty(RORoomBookingAreaListProperty, Rooms.ReadOnly.RORoomBookingAreaList.NewRORoomBookingAreaList())
        End If
        Return GetProperty(RORoomBookingAreaListProperty)
      End Get
    End Property

    Public Shared RORoomBookingProductionListProperty As PropertyInfo(Of RORoomBookingProductionList) = RegisterProperty(Of RORoomBookingProductionList)(Function(c) c.RORoomBookingProductionList, "RO Room Booking Production List")

    Public ReadOnly Property RORoomBookingProductionList() As RORoomBookingProductionList
      Get
        If GetProperty(RORoomBookingProductionListProperty) Is Nothing Then
          LoadProperty(RORoomBookingProductionListProperty, Rooms.ReadOnly.RORoomBookingProductionList.NewRORoomBookingProductionList())
        End If
        Return GetProperty(RORoomBookingProductionListProperty)
      End Get
    End Property

    Public Shared RORoomBookingAdHocBookingListProperty As PropertyInfo(Of RORoomBookingAdHocBookingList) = RegisterProperty(Of RORoomBookingAdHocBookingList)(Function(c) c.RORoomBookingAdHocBookingList, "RO Room Booking Ad Hoc Booking List")

    Public ReadOnly Property RORoomBookingAdHocBookingList() As RORoomBookingAdHocBookingList
      Get
        If GetProperty(RORoomBookingAdHocBookingListProperty) Is Nothing Then
          LoadProperty(RORoomBookingAdHocBookingListProperty, Rooms.ReadOnly.RORoomBookingAdHocBookingList.NewRORoomBookingAdHocBookingList())
        End If
        Return GetProperty(RORoomBookingAdHocBookingListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Comments

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORoomBooking(dr As SafeDataReader) As RORoomBooking

      Dim r As New RORoomBooking()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(RoomScheduleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(StartDateTimeProperty, .GetValue(4))
        LoadProperty(EndDateTimeProperty, .GetValue(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(9))
        LoadProperty(AdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(CommentsProperty, .GetString(11))
        LoadProperty(CallTimeStartProperty, .GetValue(12))
        LoadProperty(WrapTimeEndProperty, .GetValue(13))
        LoadProperty(TitleProperty, .GetString(14))
        LoadProperty(LiveIndProperty, .GetBoolean(15))
        LoadProperty(DebtorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
        LoadProperty(DebtorProperty, .GetString(17))
        LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
        LoadProperty(ProductionAreaStatusProperty, .GetString(19))
        LoadProperty(RoomProperty, .GetString(20))
        LoadProperty(IsOwnerProperty, .GetBoolean(21))
        LoadProperty(OwnedByProperty, .GetString(22))
        LoadProperty(OwningSystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
        LoadProperty(OwningProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace