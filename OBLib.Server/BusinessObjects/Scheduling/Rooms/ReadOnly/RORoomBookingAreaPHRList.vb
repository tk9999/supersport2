﻿' Generated 17 Feb 2016 11:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomBookingAreaPHRList
    Inherits OBReadOnlyListBase(Of RORoomBookingAreaPHRList, RORoomBookingAreaPHR)

#Region " Parent "

    <NotUndoable()> Private mParent As RORoomBookingArea
#End Region

#Region " Business Methods "

    Public Function GetItem(ProductionHumanResourceID As Integer) As RORoomBookingAreaPHR

      For Each child As RORoomBookingAreaPHR In Me
        If child.ProductionHumanResourceID = ProductionHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewRORoomBookingAreaPHRList() As RORoomBookingAreaPHRList

      Return New RORoomBookingAreaPHRList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace