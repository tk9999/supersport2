﻿' Generated 17 Feb 2016 11:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomBookingProduction
    Inherits OBReadOnlyBase(Of RORoomBookingProduction)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Production Description")
    ''' <summary>
    ''' Gets the Production Description value
    ''' </summary>
    <Display(Name:="Production Description", Description:="")>
    Public ReadOnly Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
    End Property

    Public Shared TeamsPlayingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamsPlaying, "Teams Playing")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Teams Playing", Description:="")>
    Public ReadOnly Property TeamsPlaying() As String
      Get
        Return GetProperty(TeamsPlayingProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeID, "Production Type")
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionTypeID() As Integer
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventTypeID, "Event Type")
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public ReadOnly Property EventTypeID() As Integer
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueID, "Production Venue")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="")>
    Public ReadOnly Property ProductionVenueID() As Integer
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
    End Property

    Public Shared VenueConfirmedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.VenueConfirmedDate, "Venue Confirmed Date")
    ''' <summary>
    ''' Gets the Venue Confirmed Date value
    ''' </summary>
    <Display(Name:="Venue Confirmed Date", Description:="")>
    Public ReadOnly Property VenueConfirmedDate As DateTime?
      Get
        Return GetProperty(VenueConfirmedDateProperty)
      End Get
    End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionRefNo, "Production Ref No")
    ''' <summary>
    ''' Gets the Production Ref No value
    ''' </summary>
    <Display(Name:="Production Ref No", Description:="")>
    Public ReadOnly Property ProductionRefNo() As String
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
    End Property

    Public Shared ParentProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ParentProductionID, "Parent Production")
    ''' <summary>
    ''' Gets the Parent Production value
    ''' </summary>
    <Display(Name:="Parent Production", Description:="")>
    Public ReadOnly Property ParentProductionID() As Integer
      Get
        Return GetProperty(ParentProductionIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SynergyGenRefNoProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.SynergyGenRefNo, "Synergy Gen Ref No")
    ''' <summary>
    ''' Gets the Synergy Gen Ref No value
    ''' </summary>
    <Display(Name:="Synergy Gen Ref No", Description:="")>
    Public ReadOnly Property SynergyGenRefNo() As Int64
      Get
        Return GetProperty(SynergyGenRefNoProperty)
      End Get
    End Property

    Public Shared PlayStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlayStartDateTime, "Play Start Date Time")
    ''' <summary>
    ''' Gets the Play Start Date Time value
    ''' </summary>
    <Display(Name:="Play Start Date Time", Description:="")>
    Public ReadOnly Property PlayStartDateTime As DateTime?
      Get
        Return GetProperty(PlayStartDateTimeProperty)
      End Get
    End Property

    Public Shared PlayEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlayEndDateTime, "Play End Date Time")
    ''' <summary>
    ''' Gets the Play End Date Time value
    ''' </summary>
    <Display(Name:="Play End Date Time", Description:="")>
    Public ReadOnly Property PlayEndDateTime As DateTime?
      Get
        Return GetProperty(PlayEndDateTimeProperty)
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Production Type")
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "Event Type")
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Production Venue")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="")>
    Public ReadOnly Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
    End Property

    Public Shared PlaceHolderIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PlaceHolderInd, "Place Holder", False)
    ''' <summary>
    ''' Gets the Place Holder value
    ''' </summary>
    <Display(Name:="Place Holder", Description:="")>
    Public ReadOnly Property PlaceHolderInd() As Boolean
      Get
        Return GetProperty(PlaceHolderIndProperty)
      End Get
    End Property

    Public Shared CreationTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreationTypeID, "Creation Type")
    ''' <summary>
    ''' Gets the Creation Type value
    ''' </summary>
    <Display(Name:="Creation Type", Description:="")>
    Public ReadOnly Property CreationTypeID() As Integer
      Get
        Return GetProperty(CreationTypeIDProperty)
      End Get
    End Property

    Public Shared VissionViewIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.VissionViewInd, "Vission View", False)
    ''' <summary>
    ''' Gets the Vission View value
    ''' </summary>
    <Display(Name:="Vission View", Description:="")>
    Public ReadOnly Property VissionViewInd() As Boolean
      Get
        Return GetProperty(VissionViewIndProperty)
      End Get
    End Property

    Public Shared HighlightsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HighlightsInd, "Highlights", False)
    ''' <summary>
    ''' Gets the Highlights value
    ''' </summary>
    <Display(Name:="Highlights", Description:="")>
    Public ReadOnly Property HighlightsInd() As Boolean
      Get
        Return GetProperty(HighlightsIndProperty)
      End Get
    End Property

    Public Shared MyAreaAddedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MyAreaAddedInd, "My Area Added", False)
    ''' <summary>
    ''' Gets the My Area Added value
    ''' </summary>
    <Display(Name:="My Area Added", Description:="")>
    Public ReadOnly Property MyAreaAddedInd() As Boolean
      Get
        Return GetProperty(MyAreaAddedIndProperty)
      End Get
    End Property

    Public Shared AreaCountProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AreaCount, "Area Count", False)
    ''' <summary>
    ''' Gets the Area Count value
    ''' </summary>
    <Display(Name:="Area Count", Description:="")>
    Public ReadOnly Property AreaCount() As Boolean
      Get
        Return GetProperty(AreaCountProperty)
      End Get
    End Property

    Public Shared OBCityIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBCityInd, "OB City", False)
    ''' <summary>
    ''' Gets the OB City value
    ''' </summary>
    <Display(Name:="OB City", Description:="")>
    Public ReadOnly Property OBCityInd() As Boolean
      Get
        Return GetProperty(OBCityIndProperty)
      End Get
    End Property

    Public Shared OBContentIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBContentInd, "OB Content", False)
    ''' <summary>
    ''' Gets the OB Content value
    ''' </summary>
    <Display(Name:="OB Content", Description:="")>
    Public ReadOnly Property OBContentInd() As Boolean
      Get
        Return GetProperty(OBContentIndProperty)
      End Get
    End Property

    Public Shared ProductionTypeEventTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionTypeEventType, "")
    ''' <summary>
    ''' Gets and sets the ProductionType value
    ''' </summary>
    <Display(Name:="Genre (Series)")>
    Public Overridable ReadOnly Property ProductionTypeEventType() As String
      Get
        Return ProductionType & " (" & EventType & ")"
      End Get
    End Property

    <Display(Name:="Event Start")>
    Public Overridable ReadOnly Property EventStart() As String
      Get
        If Me.PlayStartDateTime IsNot Nothing Then
          Return Me.PlayStartDateTime.Value.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Event End")>
    Public Overridable ReadOnly Property EventEnd() As String
      Get
        If Me.PlayEndDateTime IsNot Nothing Then
          Return Me.PlayEndDateTime.Value.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORoomBookingProduction(dr As SafeDataReader) As RORoomBookingProduction

      Dim r As New RORoomBookingProduction()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionIDProperty, .GetInt32(0))
        LoadProperty(ProductionDescriptionProperty, .GetString(1))
        LoadProperty(TeamsPlayingProperty, .GetString(2))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(VenueConfirmedDateProperty, .GetValue(6))
        LoadProperty(ProductionRefNoProperty, .GetString(7))
        LoadProperty(ParentProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(12))
        LoadProperty(SynergyGenRefNoProperty, .GetInt64(13))
        LoadProperty(PlayStartDateTimeProperty, .GetValue(14))
        LoadProperty(PlayEndDateTimeProperty, .GetValue(15))
        LoadProperty(TitleProperty, .GetString(16))
        LoadProperty(ProductionTypeProperty, .GetString(17))
        LoadProperty(EventTypeProperty, .GetString(18))
        LoadProperty(ProductionVenueProperty, .GetString(19))
        LoadProperty(PlaceHolderIndProperty, .GetBoolean(20))
        LoadProperty(CreationTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
        LoadProperty(VissionViewIndProperty, .GetBoolean(22))
        LoadProperty(HighlightsIndProperty, .GetBoolean(23))
        LoadProperty(MyAreaAddedIndProperty, .GetBoolean(25))
        LoadProperty(AreaCountProperty, .GetBoolean(26))
        LoadProperty(OBCityIndProperty, .GetBoolean(27))
        LoadProperty(OBContentIndProperty, .GetBoolean(28))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace