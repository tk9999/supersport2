﻿' Generated 17 Feb 2016 11:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomBookingAdHocBookingList
    Inherits OBReadOnlyListBase(Of RORoomBookingAdHocBookingList, RORoomBookingAdHocBooking)

#Region " Parent "

    <NotUndoable()> Private mParent As RORoomBooking
#End Region

#Region " Business Methods "

    Public Function GetItem(AdHocBookingID As Integer) As RORoomBookingAdHocBooking

      For Each child As RORoomBookingAdHocBooking In Me
        If child.AdHocBookingID = AdHocBookingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewRORoomBookingAdHocBookingList() As RORoomBookingAdHocBookingList

      Return New RORoomBookingAdHocBookingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace