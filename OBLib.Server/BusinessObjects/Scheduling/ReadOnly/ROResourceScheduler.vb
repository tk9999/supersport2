﻿' Generated 23 Jun 2016 08:08 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Scheduling.ReadOnly

  <Serializable()> _
  Public Class ROResourceScheduler
    Inherits OBReadOnlyBase(Of ROResourceScheduler)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROResourceSchedulerBO.ROResourceSchedulerBOToString(self)")

    Public Shared ResourceSchedulerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceSchedulerID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ResourceSchedulerID() As Integer
      Get
        Return GetProperty(ResourceSchedulerIDProperty)
      End Get
    End Property

    Public Shared ResourceSchedulerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceScheduler, "Resource Scheduler", "")
    ''' <summary>
    ''' Gets the Resource Scheduler value
    ''' </summary>
    <Display(Name:="Resource Scheduler", Description:="")>
    Public ReadOnly Property ResourceScheduler() As String
      Get
        Return GetProperty(ResourceSchedulerProperty)
      End Get
    End Property

    Public Shared DefaultDateViewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DefaultDateViewTypeID, "Default Date View Type", Nothing)
    ''' <summary>
    ''' Gets the Default Date View Type value
    ''' </summary>
    <Display(Name:="Default Date View Type", Description:="")>
    Public ReadOnly Property DefaultDateViewTypeID() As Integer?
      Get
        Return GetProperty(DefaultDateViewTypeIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared PageNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PageName, "Page Name", "")
    ''' <summary>
    ''' Gets the Page Name value
    ''' </summary>
    <Display(Name:="Page Name", Description:="")>
    Public ReadOnly Property PageName() As String
      Get
        Return GetProperty(PageNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceSchedulerIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceScheduler

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROResourceScheduler(dr As SafeDataReader) As ROResourceScheduler

      Dim r As New ROResourceScheduler()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ResourceSchedulerIDProperty, .GetInt32(0))
        LoadProperty(ResourceSchedulerProperty, .GetString(1))
        LoadProperty(DefaultDateViewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(PageNameProperty, .GetString(4))
      End With

    End Sub

#End Region

  End Class

End Namespace