﻿' Generated 10 Mar 2015 12:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity

  <Serializable()> _
  Public Class AdhocCrewTimesheetHumanResource
    Inherits SingularBusinessBase(Of AdhocCrewTimesheetHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AdhocCrewTimesheetHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdhocCrewTimesheetHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AdhocCrewTimesheetHumanResourceID() As Integer
      Get
        Return GetProperty(AdhocCrewTimesheetHumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
  Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared AdhocCrewTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdhocCrewTimesheetID, "Adhoc Crew Timesheet", Nothing)
    ''' <summary>
    ''' Gets the Adhoc Crew Timesheet value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property AdhocCrewTimesheetID() As Integer?
      Get
        Return GetProperty(AdhocCrewTimesheetIDProperty)
      End Get
    End Property

    Public Shared CrewStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewStartDateTime, "Crew Start Date Time")
    ''' <summary>
    ''' Gets and sets the Crew Start Date Time value
    ''' </summary>
    <Display(Name:="Crew Start Date Time", Description:=""),
    Required(ErrorMessage:="Crew Start Date Time required")>
  Public Property CrewStartDateTime As DateTime?
      Get
        Return GetProperty(CrewStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CrewStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CrewEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewEndDateTime, "Crew End Date Time")
    ''' <summary>
    ''' Gets and sets the Crew End Date Time value
    ''' </summary>
    <Display(Name:="Crew End Date Time", Description:=""),
    Required(ErrorMessage:="Crew End Date Time required")>
  Public Property CrewEndDateTime As DateTime?
      Get
        Return GetProperty(CrewEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CrewEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:=""),
    StringLength(100, ErrorMessage:="Description cannot be more than 100 characters")>
  Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CrewTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTimesheetID, "Crew Timesheet", Nothing)
    ''' <summary>
    ''' Gets and sets the Crew Timesheet value
    ''' </summary>
    <Display(Name:="Crew Timesheet", Description:="")>
  Public Property CrewTimesheetID() As Integer?
      Get
        Return GetProperty(CrewTimesheetIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTimesheetIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required", AllowEmptyStrings:=False)>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As AdhocCrewTimesheet

      Return CType(CType(Me.Parent, AdhocCrewTimesheetHumanResourceList).Parent, AdhocCrewTimesheet)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AdhocCrewTimesheetHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Description.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Adhoc Crew Timesheet Human Resource")
        Else
          Return String.Format("Blank {0}", "Adhoc Crew Timesheet Human Resource")
        End If
      Else
        Return Me.Description
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(CrewStartDateTimeProperty)
        .ServerRuleFunction = AddressOf TimesValid
        .JavascriptRuleFunctionName = "AdhocCrewTimesheetHumanResourceBO.TimesValid"
        .AddTriggerProperty(CrewEndDateTimeProperty)
      End With

    End Sub

    Public Function TimesValid(AdhocCrewTimesheetHumanResource As AdhocCrewTimesheetHumanResource) As String
      Dim Err As String = ""
      If AdhocCrewTimesheetHumanResource.CrewStartDateTime Is Nothing Then
        Err &= "Start Date Time is required"
      ElseIf AdhocCrewTimesheetHumanResource.CrewEndDateTime Is Nothing Then
        Err &= "End Date Time is required"
      ElseIf AdhocCrewTimesheetHumanResource.CrewStartDateTime.Value > AdhocCrewTimesheetHumanResource.CrewEndDateTime.Value Then
        Err &= "Start Time must be before End Time"
      End If
      Return Err
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAdhocCrewTimesheetHumanResource() method.

    End Sub

    Public Shared Function NewAdhocCrewTimesheetHumanResource() As AdhocCrewTimesheetHumanResource

      Return DataPortal.CreateChild(Of AdhocCrewTimesheetHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAdhocCrewTimesheetHumanResource(dr As SafeDataReader) As AdhocCrewTimesheetHumanResource

      Dim a As New AdhocCrewTimesheetHumanResource()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AdhocCrewTimesheetHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AdhocCrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CrewStartDateTimeProperty, .GetValue(3))
          LoadProperty(CrewEndDateTimeProperty, .GetValue(4))
          LoadProperty(DescriptionProperty, .GetString(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(CrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(HumanResourceProperty, .GetString(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAdhocCrewTimesheetHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAdhocCrewTimesheetHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAdhocCrewTimesheetHumanResourceID As SqlParameter = .Parameters.Add("@AdhocCrewTimesheetHumanResourceID", SqlDbType.Int)
          paramAdhocCrewTimesheetHumanResourceID.Value = GetProperty(AdhocCrewTimesheetHumanResourceIDProperty)
          If Me.IsNew Then
            paramAdhocCrewTimesheetHumanResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@AdhocCrewTimesheetID", Me.GetParent().AdhocCrewTimesheetID)
          .Parameters.AddWithValue("@CrewStartDateTime", (New SmartDate(GetProperty(CrewStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CrewEndDateTime", (New SmartDate(GetProperty(CrewEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@CrewTimesheetID", Singular.Misc.NothingDBNull(GetProperty(CrewTimesheetIDProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AdhocCrewTimesheetHumanResourceIDProperty, paramAdhocCrewTimesheetHumanResourceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAdhocCrewTimesheetHumanResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AdhocCrewTimesheetHumanResourceID", GetProperty(AdhocCrewTimesheetHumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace