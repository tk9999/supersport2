﻿' Generated 18 Feb 2015 10:25 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Timesheets.ReadOnly
Imports OBLib.Maintenance.Timesheets

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity

  <Serializable()> _
  Public Class CrewTimesheetList
    Inherits SingularBusinessListBase(Of CrewTimesheetList, CrewTimesheet)

#Region " Properties "

    Public Property LastCrewTimesheetLastMonth As CrewTimesheet = Nothing
    Public Property TimesheetMonth As TimesheetMonth = Nothing

    Public ReadOnly Property TimesheetHasChangesFromOtherMonthsInd As Boolean
      Get
        If CType(Me.Parent, OBCityTimesheet).TimesheetAdjustmentList IsNot Nothing Then
          Dim count As Integer = 0
          For Each td As TimesheetAdjustment In CType(Me.Parent, OBCityTimesheet).TimesheetAdjustmentList
            If td.AdjustedTimesheetMonthID <> CType(Me.Parent, OBCityTimesheet).TimesheetMonthID Then
              count += 1
            End If
          Next
          If count > 0 Then
            Return True
          End If
        End If
        'If AdjustmentsMadeToMeInOtherMonths IsNot Nothing AndAlso AdjustmentsMadeToMeInOtherMonths.Count > 0 Then
        '  Return True
        'End If
        Return False
      End Get
    End Property

#End Region

#Region " Business Methods "

    Public Function GetItem(CrewTimesheetID As Integer) As CrewTimesheet

      For Each child As CrewTimesheet In Me
        If child.CrewTimesheetID = CrewTimesheetID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Crew Timesheets"

    End Function

    Public Sub DoCalculations()

      Dim previous As CrewTimesheet
      LastCrewTimesheetLastMonth = Me.Where(Function(d) d.LastDayPreviousMonthInd).FirstOrDefault

      For Each current As CrewTimesheet In Me
        If Not current.LastDayPreviousMonthInd Then
          previous = Me.Where(Function(d) d.DateOrder = (current.DateOrder - 1)).FirstOrDefault
          Dim prevShortfall = 0 'If(previous Is Nothing, 0, previous.ShortfallForMonth)
          Dim prevHours = 0 'If(previous Is Nothing, 0, previous.HoursForMonth)
          If previous IsNot LastCrewTimesheetLastMonth Then
            prevShortfall = previous.ShortfallForMonth
            prevHours = previous.HoursForMonth
            current.CalculateHoursForDay()
            current.CalculateOvertimeForDay(previous, prevHours)
            current.CalculateHoursAboveOTLimit(previous)
            current.CalculateShortfall(previous, prevShortfall)
          Else
            current.CalculateHoursForDay()
            current.CalculateOvertimeForDay(previous, 0)
            current.CalculateHoursAboveOTLimit(previous)
            current.CalculateShortfall(previous, 0)
          End If
        Else
          'current.CalculateHoursPerDay()
          'current.CalculateDailyOvertime(LastCrewTimesheetLastMonth, 0)
          'current.HoursAboveOTLimit = 0
          'current.CalculateShortfall(LastCrewTimesheetLastMonth, 0)
        End If
      Next

    End Sub

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits SingularCriteriaBase(Of Criteria)

      Public Shared MonthDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.MonthDate, Nothing)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Start Date", Description:=""),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property MonthDate As DateTime?
        Get
          Return ReadProperty(MonthDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(MonthDateProperty, Value)
        End Set
      End Property

      Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production", Description:="")>
      Public Property ProductionID() As Integer?
        Get
          Return ReadProperty(ProductionIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionIDProperty, Value)
        End Set
      End Property

      Public Shared LastDayPreviousMonthProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.LastDayPreviousMonth, Nothing)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Last Day Previous Month", Description:=""),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property LastDayPreviousMonth As DateTime?
        Get
          Return ReadProperty(LastDayPreviousMonthProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(LastDayPreviousMonthProperty, Value)
        End Set
      End Property

      Public Shared TimesheetMonthIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.TimesheetMonthID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Timesheet Month", Description:="")>
      Public Property TimesheetMonthID() As Integer?
        Get
          Return ReadProperty(TimesheetMonthIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(TimesheetMonthIDProperty, Value)
        End Set
      End Property

      Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HumanResourceID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Human Resource", Description:="")>
      Public Property HumanResourceID() As Integer?
        Get
          Return ReadProperty(HumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(HumanResourceIDProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SystemID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:="")>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:="")>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.DisciplineID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Discipline", Description:="")>
      Public Property DisciplineID() As Integer?
        Get
          Return ReadProperty(DisciplineIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(DisciplineIDProperty, Value)
        End Set
      End Property

      Public Shared CrewTimesheetIDsProperty As PropertyInfo(Of List(Of Integer)) = RegisterSProperty(Of List(Of Integer))(Function(c) c.CrewTimesheetIDs, New List(Of Integer))
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="CrewTimesheetIDs", Description:="")>
      Public Property CrewTimesheetIDs() As List(Of Integer)
        Get
          Return ReadProperty(CrewTimesheetIDsProperty)
        End Get
        Set(ByVal Value As List(Of Integer))
          LoadProperty(CrewTimesheetIDsProperty, Value)
        End Set
      End Property

      Public Shared HumanResourceIDsProperty As PropertyInfo(Of List(Of Integer)) = RegisterSProperty(Of List(Of Integer))(Function(c) c.HumanResourceIDs, New List(Of Integer))
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="HumanResourceIDs", Description:="")>
      Public Property HumanResourceIDs() As List(Of Integer)
        Get
          Return ReadProperty(HumanResourceIDsProperty)
        End Get
        Set(ByVal Value As List(Of Integer))
          LoadProperty(HumanResourceIDsProperty, Value)
        End Set
      End Property

      Public Sub New(HumanResourceID As Object, Month As DateTime?)

        Me.HumanResourceID = HumanResourceID
        Me.MonthDate = Month

      End Sub

      Public Sub New(HumanResourceID As Object, TimesheetMonthID As Integer?, Month As DateTime?)

        Me.HumanResourceID = HumanResourceID
        Me.TimesheetMonthID = TimesheetMonthID
        Me.MonthDate = Month

      End Sub

      Public Sub New(HumanResourceID As Object, Month As DateTime?, DisciplineID As Integer)

        Me.HumanResourceID = HumanResourceID
        Me.MonthDate = Month
        Me.DisciplineID = DisciplineID

      End Sub

      Public Sub New(LastDayPreviousMonth As DateTime, HumanResourceID As Integer)

        Me.HumanResourceID = HumanResourceID
        Me.LastDayPreviousMonth = LastDayPreviousMonth

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewCrewTimesheetList() As CrewTimesheetList

      Return New CrewTimesheetList()

    End Function

    'Public Shared Sub BeginGetCrewTimesheetList(CallBack As EventHandler(Of DataPortalResult(Of CrewTimesheetList)))

    '  Dim dp As New DataPortal(Of CrewTimesheetList)()
    '  AddHandler dp.FetchCompleted, CallBack
    '  dp.BeginFetch(New Criteria())

    'End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetCrewTimesheetList() As CrewTimesheetList

      Return DataPortal.Fetch(Of CrewTimesheetList)(New Criteria(OBLib.Security.Settings.CurrentUser.HumanResourceID, Now))

    End Function

    Public Shared Function GetCrewTimesheetList(HumanResourceID As Integer?, SelectedROTimesheetMonth As ROTimesheetMonth) As CrewTimesheetList
      Dim ct As New Criteria(HumanResourceID, SelectedROTimesheetMonth.TimesheetMonthID, SelectedROTimesheetMonth.StartDate)
      Return DataPortal.Fetch(Of CrewTimesheetList)(ct)
    End Function

    Public Shared Function GetLastItemLastMonth(ByVal LastDayPreviousMonth As SmartDate, HumanResourceID As Integer) As CrewTimesheet

      Return DataPortal.Fetch(Of CrewTimesheetList)(New Criteria(LastDayPreviousMonth, HumanResourceID)).FirstOrDefault()

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(CrewTimesheet.GetCrewTimesheet(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getCrewTimesheetList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@Month", NothingDBNull(crit.MonthDate))
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            'cm.Parameters.AddWithValue("@CrewTimesheetIDs", NothingDBNull(Nothing))
            cm.Parameters.AddWithValue("@CrewTimesheetIDs", Strings.MakeEmptyDBNull(OBLib.Helpers.MiscHelper.IntegerArrayToXML(crit.CrewTimesheetIDs.ToArray)))
            cm.Parameters.AddWithValue("@LastDayPreviousMonth", NothingDBNull(crit.LastDayPreviousMonth))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@HumanResourceIDs", Strings.MakeEmptyDBNull(OBLib.Helpers.MiscHelper.IntegerArrayToXML(crit.HumanResourceIDs.ToArray)))
            'cm.Parameters.AddWithValue("@HumanResourceIDs", NothingDBNull(Nothing))
            cm.Parameters.AddWithValue("@TimesheetMonthID", NothingDBNull(crit.TimesheetMonthID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As CrewTimesheet In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As CrewTimesheet In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace