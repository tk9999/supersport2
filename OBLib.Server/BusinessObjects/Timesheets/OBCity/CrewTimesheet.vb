﻿' Generated 18 Feb 2015 10:25 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Timesheets.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity

  <Serializable()> _
  Public Class CrewTimesheet
    Inherits SingularBusinessBase(Of CrewTimesheet)

#Region " Properties and Methods "

#Region " Properties "

    Private mSystemParam As ROSystemParameter = Nothing
    Private mTimesheetCategory As ROTimesheetCategory = Nothing
    'Private mTimesheetMonth As OBLib.Maintenance.General.ReadOnly.ROTimesheetMonthOld = Nothing

    Public Shared CrewTimesheetIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewTimesheetID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property CrewTimesheetID() As Integer
      Get
        Return GetProperty(CrewTimesheetIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource that this timesheet is for"),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        If CompareSafe(OBLib.Security.Settings.CurrentUser.UserID, OBLib.CommonData.Enums.Users.SuperUser) Then
          SetProperty(HumanResourceIDProperty, Value)
        End If
      End Set
    End Property

    Public Shared AdhocCrewTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdhocCrewTimesheetID, "Adhoc Crew Timesheet", Nothing)
    ''' <summary>
    ''' Gets and sets the Adhoc Crew Timesheet value
    ''' </summary>
    <Display(Name:="Adhoc Crew Timesheet", Description:="Link to the AdHoc Crew Timesheet")>
    Public Property AdhocCrewTimesheetID() As Integer?
      Get
        Return GetProperty(AdhocCrewTimesheetIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        If CompareSafe(OBLib.Security.Settings.CurrentUser.UserID, OBLib.CommonData.Enums.Users.SuperUser) Then
          SetProperty(AdhocCrewTimesheetIDProperty, Value)
        End If
      End Set
    End Property

    Public Shared CalculatedStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CalculatedStartDateTime, "Calculated Start Date Time")
    ''' <summary>
    ''' Gets and sets the Calculated Start Date Time value
    ''' </summary>
    <Display(Name:="Calculated Start Time", Description:="This is the calculated start time for the day. This cannot be editted by the user."),
    Required(ErrorMessage:="Calculated Start Time required")>
    Public Property CalculatedStartDateTime As DateTime?
      Get
        Return GetProperty(CalculatedStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        If CompareSafe(OBLib.Security.Settings.CurrentUser.UserID, OBLib.CommonData.Enums.Users.SuperUser) Then
          SetProperty(CalculatedStartDateTimeProperty, Value)
        End If
      End Set
    End Property

    Public Shared CalculatedEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CalculatedEndDateTime, "Calculated End Date Time")
    ''' <summary>
    ''' Gets and sets the Calculated End Date Time value
    ''' </summary>
    <Display(Name:="Calculated End Time", Description:="This is the calculated end time for the day. This cannot be editted by the user."),
    Required(ErrorMessage:="Calculated End Time required")>
    Public Property CalculatedEndDateTime As DateTime?
      Get
        Return GetProperty(CalculatedEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        If CompareSafe(OBLib.Security.Settings.CurrentUser.UserID, OBLib.CommonData.Enums.Users.SuperUser) Then
          SetProperty(CalculatedEndDateTimeProperty, Value)
        End If
      End Set
    End Property

    Public Shared CrewStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewStartDateTime, "Crew Start Date Time")
    ''' <summary>
    ''' Gets and sets the Crew Start Date Time value
    ''' </summary>
    <Display(Name:="Crew Start Time", Description:="Defaults to CalculatedStartDateTime. If it differs then we know that the HR has made a change and will need to be authorised"),
    Required(ErrorMessage:="Crew Start Time required")>
    Public Property CrewStartDateTime As DateTime?
      Get
        Return GetProperty(CrewStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CrewStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CrewEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewEndDateTime, "Crew End Date Time")
    ''' <summary>
    ''' Gets and sets the Crew End Date Time value
    ''' </summary>
    <Display(Name:="Crew End Time", Description:="Defaults to CalculatedEndDateTime. If it differs then we know that the HR has made a change and will need to be authorised"),
    Required(ErrorMessage:="Crew End Time required")>
    Public Property CrewEndDateTime As DateTime?
      Get
        Return GetProperty(CrewEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CrewEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ManagerStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ManagerStartDateTime, "Manager Start Date Time")
    ''' <summary>
    ''' Gets and sets the Manager Start Date Time value
    ''' </summary>
    <Display(Name:="Manager Start Time", Description:="Defaults to CalculatedStartDateTime. If it differs then we know that the HR has made a change and will need to be authorised")>
    Public Property ManagerStartDateTime As DateTime?
      Get
        Return GetProperty(ManagerStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ManagerStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ManagerEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ManagerEndDateTime, "Manager End Date Time")
    ''' <summary>
    ''' Gets and sets the Manager End Date Time value
    ''' </summary>
    <Display(Name:="Manager End Time", Description:="Defaults to CalculatedEndDateTime. If it differs then we know that the HR has made a change and will need to be authorised")>
    Public Property ManagerEndDateTime As DateTime?
      Get
        Return GetProperty(ManagerEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ManagerEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared OvertimeHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.OvertimeHours, "Overtime Hours", CType(0.0, Decimal))
    ''' <summary>
    ''' Gets and sets the Overtime Hours value
    ''' </summary>
    <Display(Name:="Overtime", Description:="Hours of shift that are above the required monthly amount")>
    Public Property OvertimeHours() As Decimal
      Get
        Return GetProperty(OvertimeHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        If CompareSafe(OBLib.Security.Settings.CurrentUser.UserID, OBLib.CommonData.Enums.Users.SuperUser) Then
          SetProperty(OvertimeHoursProperty, Value)
        End If
      End Set
    End Property

    Public Shared ShortfallHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShortfallHours, "Shortfall Hours", CType(0.0, Decimal))
    ''' <summary>
    ''' Gets and sets the Shortfall Hours value
    ''' </summary>
    <Display(Name:="Shortfall", Description:="Hours credited due to less than the Short Fall Minimum Hours between this shift and the previous shift")>
    Public Property ShortfallHours() As Decimal
      Get
        Return GetProperty(ShortfallHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        If CompareSafe(OBLib.Security.Settings.CurrentUser.UserID, OBLib.CommonData.Enums.Users.SuperUser) Then
          SetProperty(ShortfallHoursProperty, Value)
        End If
      End Set
    End Property

    Public Shared CrewChangeDetailsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CrewChangeDetails, "Crew Change Details", "")
    ''' <summary>
    ''' Gets and sets the Crew Change Details value
    ''' </summary>
    <Display(Name:="Query Reason", Description:="Space for the human resource to capture comments regarding their timesheet item."),
    StringLength(200, ErrorMessage:="Crew Change Details cannot be more than 200 characters")>
    Public Property CrewChangeDetails() As String
      Get
        Return GetProperty(CrewChangeDetailsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CrewChangeDetailsProperty, Value)
      End Set
    End Property

    Public Shared ManagerChangeDetailsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerChangeDetails, "Manager Change Details", "")
    ''' <summary>
    ''' Gets and sets the Manager Change Details value
    ''' </summary>
    <Display(Name:="Manager Change Details", Description:="Reason why the manager has changed the record"),
    StringLength(200, ErrorMessage:="Manager Change Details cannot be more than 200 characters")>
    Public Property ManagerChangeDetails() As String
      Get
        Return GetProperty(ManagerChangeDetailsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ManagerChangeDetailsProperty, Value)
      End Set
    End Property

    Public Shared CrewCheckedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewCheckedDate, "Crew Checked Date")
    ''' <summary>
    ''' Gets and sets the Crew Checked Date value
    ''' </summary>
    <Display(Name:="Crew Checked Date", Description:="The date that the HR went onto the web a marked their timesheet record as correct and has not made any changes")>
    Public Property CrewCheckedDate As DateTime?
      Get
        Return GetProperty(CrewCheckedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CrewCheckedDateProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.AuthorisedByUserID, Nothing)
    ''' <summary>
    ''' Gets and sets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised By User", Description:="The user that authorised this timesheet record")>
    Public Property AuthorisedByUserID() As Integer?
      Get
        Return GetProperty(AuthorisedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AuthorisedByUserIDProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Authorised Date Time", Description:="The date and time the timesheet item was authorised")>
    Public Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(AuthorisedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Description, "")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="Description of what the HR worked on that day"),
    StringLength(100, ErrorMessage:="Description cannot be more than 100 characters")>
    Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared ShortDescriptionProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ShortDescription, "")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description")>
    Public Property ShortDescription() As String
      Get
        Return GetProperty(ShortDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ShortDescriptionProperty, Value)
      End Set
    End Property

    Public Shared AccountingDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.AccountingDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Accounting Date value
    ''' </summary>
    <Display(Name:="Date", Description:="The date that will be used to determine which payment run/timesheet month the record needs to be paid under"),
    Required(ErrorMessage:="Date required"),
    DateField(FormatString:="dd MMM")>
    Public Property AccountingDate As DateTime?
      Get
        Return GetProperty(AccountingDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(AccountingDateProperty, Value)
      End Set
    End Property

    Public Shared AccumulateHoursIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.AccumulateHoursInd, True)
    ''' <summary>
    ''' Gets and sets the Accumulate Hours value
    ''' </summary>
    <Display(Name:="Accumulate Hours", Description:="Whether or not the record must included accumulated hours (running total)")>
    Public Property AccumulateHoursInd() As Boolean
      Get
        Return GetProperty(AccumulateHoursIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(AccumulateHoursIndProperty, Value)
      End Set
    End Property

    Public Shared ActualHoursIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.ActualHoursInd, True)
    ''' <summary>
    ''' Gets and sets the Actual Hours value
    ''' </summary>
    <Display(Name:="Actual Hours", Description:="Whether or not this record must be accounted for in actual hours")>
    Public Property ActualHoursInd() As Boolean
      Get
        Return GetProperty(ActualHoursIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ActualHoursIndProperty, Value)
      End Set
    End Property

    Public Shared OvertimeCalcIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.OvertimeCalcInd, True)
    ''' <summary>
    ''' Gets and sets the Overtime Calc value
    ''' </summary>
    <Display(Name:="Overtime Calc", Description:="Does this record qualify for overtime?")>
    Public Property OvertimeCalcInd() As Boolean
      Get
        Return GetProperty(OvertimeCalcIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OvertimeCalcIndProperty, Value)
      End Set
    End Property

    Public Shared ShortfallCalcIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.ShortfallCalcInd, True)
    ''' <summary>
    ''' Gets and sets the Shortfall Calc value
    ''' </summary>
    <Display(Name:="Shortfall Calc", Description:="Does this record qualify for shortfall?")>
    Public Property ShortfallCalcInd() As Boolean
      Get
        Return GetProperty(ShortfallCalcIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ShortfallCalcIndProperty, Value)
      End Set
    End Property

    Public Shared TimesheetCategoryIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.TimesheetCategoryID, Nothing)
    ''' <summary>
    ''' Gets and sets the Timesheet Category value
    ''' </summary>
    <Display(Name:="Category", Description:="The Category under which this timesheet entry has been categorised"),
    DropDownWeb(GetType(ROTimesheetCategoryList), DisplayMember:="TimesheetCategory", ValueMember:="TimesheetCategoryID")>
    Public Property TimesheetCategoryID() As Integer?
      Get
        Return GetProperty(TimesheetCategoryIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TimesheetCategoryIDProperty, Value)
      End Set
    End Property

    'Custom Logic Required Here
    Public Shared ManualOverrideIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.ManualOverrideInd, False)
    ''' <summary>
    ''' Gets and sets the Manual Override value
    ''' </summary>
    <Display(Name:="Manual Override", Description:="Whether or not this record has been manually overriden. If true, some of the standard system rules will be ignored and will take the manually overriden fields over the system calculation")>
    Public Property ManualOverrideInd() As Boolean
      Get
        Return GetProperty(ManualOverrideIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ManualOverrideIndProperty, Value)
      End Set
    End Property

    Public Shared OverridenByUserIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.OverridenByUserID, Nothing)
    ''' <summary>
    ''' Gets and sets the Overriden By User value
    ''' </summary>
    <Display(Name:="Overriden By User", Description:="The user who overrode the record")>
    Public Property OverridenByUserID() As Integer?
      Get
        Return GetProperty(OverridenByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OverridenByUserIDProperty, Value)
      End Set
    End Property

    Public Shared OverridenDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.OverridenDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Overriden Date Time value
    ''' </summary>
    <Display(Name:="Overriden Date Time", Description:="When the record was overriden")>
    Public Property OverridenDateTime As DateTime?
      Get
        Return GetProperty(OverridenDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OverridenDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", OBLib.Security.Settings.CurrentUser.UserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared TimesheetMonthIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.TimesheetMonthID, Nothing)
    ''' <summary>
    ''' Gets and sets the Timesheet Month value
    ''' </summary>
    <Display(Name:="Timesheet Month", Description:="The timesheet month that the record was assigned to for payment.")>
    Public Property TimesheetMonthID() As Integer?
      Get
        Return GetProperty(TimesheetMonthIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TimesheetMonthIDProperty, Value)
      End Set
    End Property

    Public Shared HoursForDayProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.HoursForDay, CType(0.0, Decimal))
    ''' <summary>
    ''' Gets and sets the Hours For Day value
    ''' </summary>
    <Display(Name:="Day Hrs", Description:="The amount of hours that the worked for thet day (system calculated)")>
    Public Property HoursForDay() As Decimal
      Get
        Return GetProperty(HoursForDayProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(HoursForDayProperty, Value)
      End Set
    End Property

    Public Shared CreditorInvoiceDetailIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.CreditorInvoiceDetailID, Nothing)
    ''' <summary>
    ''' Gets and sets the Creditor Invoice Detail value
    ''' </summary>
    <Display(Name:="Creditor Invoice Detail", Description:="Link to the invoice (Freelancers only)")>
    Public Property CreditorInvoiceDetailID() As Integer?
      Get
        Return GetProperty(CreditorInvoiceDetailIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CreditorInvoiceDetailIDProperty, Value)
      End Set
    End Property

    Public Shared AdHocItemIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.AdHocItemInd, False)
    ''' <summary>
    ''' Gets and sets the Manual Override value
    ''' </summary>
    <Display(Name:="Manual Override", Description:="Whether or not this record is exclusively for an adhoc timesheet")>
    Public Property AdHocItemInd() As Boolean
      Get
        Return GetProperty(AdHocItemIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(AdHocItemIndProperty, Value)
      End Set
    End Property

    Public Shared SnTForDayProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.SnTForDay, CType(0.0, Decimal))
    ''' <summary>
    ''' Gets and sets the Hours For Day value
    ''' </summary>
    <Display(Name:="SnT", Description:="")>
    Public Property SnTForDay() As Decimal
      Get
        Return GetProperty(SnTForDayProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(SnTForDayProperty, Value)
      End Set
    End Property

    Public Shared LastDayPreviousMonthIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.LastDayPreviousMonthInd, False)
    ''' <summary>
    ''' Gets and sets the Manual Override value
    ''' </summary>
    <Display(Name:="Last Day Previous Month", Description:="Whether or not this record is exclusively for an adhoc timesheet")>
    Public Property LastDayPreviousMonthInd() As Boolean
      Get
        Return GetProperty(LastDayPreviousMonthIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(LastDayPreviousMonthIndProperty, Value)
      End Set
    End Property

    Public Shared DateOrderProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.DateOrder, 0)
    ''' <summary>
    ''' Gets and sets the Manual Override value
    ''' </summary>
    <Display(Name:="Date Order", Description:="")>
    Public Property DateOrder() As Integer
      Get
        Return GetProperty(DateOrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(DateOrderProperty, Value)
      End Set
    End Property

    Public Shared DayOfWeekProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DayOfWeek, "Day Of Week")
    ''' <summary>
    ''' Gets the Manager Change Details value
    ''' </summary>
    <Display(Name:="Day", Description:="")>
    Public Property DayOfWeek() As String
      Get
        Return GetProperty(DayOfWeekProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DayOfWeekProperty, Value)
      End Set
    End Property

    <Display(Name:="PH?", Description:="")>
    Public Property PublicHolidayInd() As Boolean
      Get
        Return IsPublicHoliday()
      End Get
      Set(ByVal Value As Boolean)

      End Set
    End Property

    <Display(Name:="PH Hrs")>
    Public Property PublicHolidayHours As Decimal
      Get
        Return GetPublicHolidayHours()
      End Get
      Set(ByVal Value As Decimal)

      End Set
    End Property

    <Display(Name:="Act Hrs")>
    Public Property ActualHoursWorked As Decimal
      Get
        Return GetActualHoursWorked()
      End Get
      Set(ByVal Value As Decimal)

      End Set
    End Property

    <Display(Name:="Mnth Hrs")>
    Public Property HoursForMonth As Decimal
      Get
        Return GetRunningTotal()
      End Get
      Set(ByVal Value As Decimal)

      End Set
    End Property

    Public Shared HoursAboveOTLimitProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.HoursAboveOTLimit, CType(0.0, Decimal))
    ''' <summary>
    ''' Gets and sets the Hours For Day value
    ''' </summary>
    <Display(Name:="Hrs > Req", Description:="")>
    Public Property HoursAboveOTLimit() As Decimal
      Get
        Return GetProperty(HoursAboveOTLimitProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(HoursAboveOTLimitProperty, Value)
      End Set
    End Property

    <Display(Name:="SwitchOver Point")>
    Public ReadOnly Property SwitchOverPointInd As Boolean
      Get
        'if this is into overtime and ((current total for month - todays total) < 180)
        'mTimesheetMonth.HoursBeforeOvertime
        If IntoOvertimeInd AndAlso ((Me.HoursForMonth - Me.HoursForDay) < Me.GetParent.TMHoursBeforeOvertime) Then
          Return True
        Else
          Return False
        End If
      End Get
    End Property

    <Display(Name:="Into Overtime?")>
    Public ReadOnly Property IntoOvertimeInd As Boolean
      Get
        'mTimesheetMonth.HoursBeforeOvertime
        If Me.HoursForMonth >= Me.GetParent.TMHoursBeforeOvertime Then
          Return True
        Else
          Return False
        End If
      End Get
    End Property

    Public ReadOnly Property SystemParameter As ROSystemParameter
      Get
        Return CommonData.Lists.ROSystemParameterList.FirstOrDefault()
        'Return mSystemParam
      End Get
    End Property

    Public ReadOnly Property TimesheetCategory As ROTimesheetCategory
      Get
        If mTimesheetCategory Is Nothing Then
          mTimesheetCategory = OBLib.CommonData.Lists.ROTimesheetCategoryList.GetItem(TimesheetCategoryID)
        End If
        Return mTimesheetCategory
      End Get
    End Property

    <Display(Name:="Mnth SFall")>
    Public ReadOnly Property ShortfallForMonth As Decimal
      Get
        Return GetTotalShortfall()
      End Get
    End Property

    Public Shared OriginalCrewStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalCrewStartDateTime, "Crew Start Date Time")
    ''' <summary>
    ''' Gets and sets the Crew Start Date Time value
    ''' </summary>
    <Display(Name:="Crew Start Time", Description:="Defaults to CalculatedStartDateTime. If it differs then we know that the HR has made a change and will need to be authorised")>
    Public Property OriginalCrewStartDateTime As DateTime?
      Get
        Return GetProperty(OriginalCrewStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OriginalCrewStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared OriginalCrewEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalCrewEndDateTime, "Crew End Date Time")
    ''' <summary>
    ''' Gets and sets the Crew End Date Time value
    ''' </summary>
    <Display(Name:="Crew End Time", Description:="Defaults to CalculatedEndDateTime. If it differs then we know that the HR has made a change and will need to be authorised")>
    Public Property OriginalCrewEndDateTime As DateTime?
      Get
        Return GetProperty(OriginalCrewEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OriginalCrewEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared OriginalManagerStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalManagerStartDateTime, "Crew Start Date Time")
    ''' <summary>
    ''' Gets and sets the Crew Start Date Time value
    ''' </summary>
    <Display(Name:="Manager Start Time", Description:="Defaults to CalculatedStartDateTime. If it differs then we know that the HR has made a change and will need to be authorised")>
    Public Property OriginalManagerStartDateTime As DateTime?
      Get
        Return GetProperty(OriginalManagerStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OriginalManagerStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared OriginalManagerEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalManagerEndDateTime, "Crew End Date Time")
    ''' <summary>
    ''' Gets and sets the Crew End Date Time value
    ''' </summary>
    <Display(Name:="Manager End Time", Description:="Defaults to CalculatedEndDateTime. If it differs then we know that the HR has made a change and will need to be authorised")>
    Public Property OriginalManagerEndDateTime As DateTime?
      Get
        Return GetProperty(OriginalManagerEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OriginalManagerEndDateTimeProperty, Value)
      End Set
    End Property

    <Display(Name:="Start Date")>
    Public ReadOnly Property StartDate As String
      Get
        If CompareSafe(TimesheetCategoryID, CType(OBLib.CommonData.Enums.TimesheetCategories.UnBooked, Integer)) Then
          Return ""
        End If
        Return CrewStartDateTime.Value.ToString("dd MMM yy")
      End Get
    End Property

    <Display(Name:="Start Time")>
    Public ReadOnly Property StartTime As String
      Get
        If CompareSafe(TimesheetCategoryID, CType(OBLib.CommonData.Enums.TimesheetCategories.UnBooked, Integer)) Then
          Return ""
        End If
        Return CrewStartDateTime.Value.ToString("HH:mm")
      End Get
    End Property

    <Display(Name:="End Date")>
    Public ReadOnly Property EndDate As String
      Get
        If CompareSafe(TimesheetCategoryID, CType(OBLib.CommonData.Enums.TimesheetCategories.UnBooked, Integer)) Then
          Return ""
        End If
        Return CrewEndDateTime.Value.ToString("dd MMM yy")
      End Get
    End Property

    <Display(Name:="End Time")>
    Public ReadOnly Property EndTime As String
      Get
        If CompareSafe(TimesheetCategoryID, CType(OBLib.CommonData.Enums.TimesheetCategories.UnBooked, Integer)) Then
          Return ""
        End If
        Return CrewEndDateTime.Value.ToString("HH:mm")
      End Get
    End Property

    <Display(Name:="PH Hours")>
    Public ReadOnly Property PublicHolidayHoursString As String
      Get
        If PublicHolidayHours = 0 Then
          Return ""
        Else
          Return PublicHolidayHours.ToString("0.00")
        End If
      End Get
    End Property

    <Display(Name:="Shortfall")>
    Public ReadOnly Property ShortfallHoursString As String
      Get
        If ShortfallHours = 0 Then
          Return ""
        Else
          Return ShortfallHours.ToString("0.00")
        End If
      End Get
    End Property

    <Display(Name:="SnT")>
    Public ReadOnly Property SnTForDayString As String
      Get
        If SnTForDay = 0 Then
          Return ""
        Else
          Return SnTForDay.ToString("0.00")
        End If
      End Get
    End Property

    <Display(Name:="Start Date")>
    Public ReadOnly Property CalcStartDate As String
      Get
        Return CalculatedStartDateTime.Value.ToString("dd MMM yy")
      End Get
    End Property

    <Display(Name:="Start Time")>
    Public ReadOnly Property CalcStartTime As String
      Get
        Return CalculatedStartDateTime.Value.ToString("HH:mm")
      End Get
    End Property

    <Display(Name:="End Date")>
    Public ReadOnly Property CalcEndDate As String
      Get
        Return CalculatedEndDateTime.Value.ToString("dd MMM yy")
      End Get
    End Property

    <Display(Name:="End Time")>
    Public ReadOnly Property CalcEndTime As String
      Get
        Return CalculatedEndDateTime.Value.ToString("HH:mm")
      End Get
    End Property

    Public Shared MealsProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Meals, "Meals", CType(0.0, Decimal))
    ''' <summary>
    ''' Gets and sets the Overtime Hours value
    ''' </summary>
    <Display(Name:="Meals", Description:="Total Reimbursement amount")>
    Public Property Meals() As Decimal
      Get
        Return GetProperty(MealsProperty)
      End Get
      Set(ByVal Value As Decimal)
        If CompareSafe(OBLib.Security.Settings.CurrentUser.UserID, OBLib.CommonData.Enums.Users.SuperUser) Then
          SetProperty(MealsProperty, Value)
        End If
      End Set
    End Property

    <Display(Name:="Meals")>
    Public ReadOnly Property MealsString As String
      Get
        If Meals = 0 Then
          Return ""
        Else
          Return Meals.ToString("0.00")
        End If
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewTimesheetIDProperty)

    End Function

    Public Overrides Function ToString() As String

      'If Me.CrewChangeDetails.Length = 0 Then
      '  If Me.IsNew Then
      '    Return String.Format("New {0}", "Crew Timesheet")
      '  Else
      '    Return String.Format("Blank {0}", "Crew Timesheet")
      '  End If
      'Else
      '  Return Me.CrewChangeDetails
      'End If
      Return Me.AccountingDate.Value.ToString("dd MMM yyyy") & " - " & Me.TimesheetCategory.TimesheetCategory

    End Function

    Public Function GetParent() As OBCityTimesheet

      Return CType(CType(Me.Parent, CrewTimesheetList).Parent, OBCityTimesheet)

    End Function

#Region " General "

    Public Function IsPublicHoliday() As Boolean

      Dim holiday As ROPublicHoliday = Nothing

      If IsNullNothing(AccountingDate) Then
        Return False
      Else
        holiday = CommonData.Lists.ROPublicHolidayList.Where(Function(day) day.HolidayDate = AccountingDate).FirstOrDefault()
        If holiday Is Nothing Then
          Return False
        Else
          Return True
        End If
      End If

      Return False

    End Function

    Public Function IsUnbooked() As Boolean

      If CompareSafe(TimesheetCategoryID, CType(CommonData.Enums.TimesheetCategories.UnBooked, Integer)) Then
        Return True
      End If

      Return False

    End Function

    Public Function IsLeave() As Boolean

      If CompareSafe(TimesheetCategoryID, CType(CommonData.Enums.TimesheetCategories.Leave, Integer)) Then
        Return True
      End If

      Return False

    End Function

    Public Function IsOff() As Boolean

      If CompareSafe(TimesheetCategoryID, CType(CommonData.Enums.TimesheetCategories.Off, Integer)) Then
        Return True
      End If

      Return False

    End Function

    Public Function IsUnpaidLeave() As Boolean

      If CompareSafe(TimesheetCategoryID, CType(CommonData.Enums.TimesheetCategories.UnpaidLeave, Integer)) Then
        Return True
      End If

      Return False

    End Function

    Public Function IsDayAway() As Boolean

      If CompareSafe(TimesheetCategoryID, CType(CommonData.Enums.TimesheetCategories.DayAway, Integer)) Then
        Return True
      End If

      Return False

    End Function

    Public Function GetHoursUntilMidnight()

      Dim hours As Decimal = 0
      Dim minutes As Decimal = 0
      minutes = CrewEndDateTime.Value.Date.Subtract(CrewStartDateTime.Value).TotalMinutes
      hours = RoundToHalfHour(minutes / 60)

      Return hours

    End Function

    Public Function IsWorkingInToNextDay() As Boolean

      If CrewStartDateTime.Value.Day < CrewEndDateTime.Value.Day Then
        Return True
      End If

      Return False

    End Function

    Public Function HoursWorkedUntilMidnight() As Decimal

      If IsWorkingInToNextDay() Then
        Return GetHoursUntilMidnight()
      Else
        'Dim ParentList As CrewTimesheetList = CType(Parent, CrewTimesheetList)
        Return ActualHoursWorked
      End If

    End Function

    Public Function RoundToHalfHour(ByVal Value As Decimal) As Decimal

      Dim result As Decimal = 0

      result = (Math.Round(Value * 2, 0) / 2)

      Return result

    End Function

    Public Function GetActualHoursWorked()

      Dim hours As Decimal = 0
      Dim minutes As Decimal = 0
      minutes = CrewEndDateTime.Value.Subtract(CrewStartDateTime.Value).TotalMinutes
      hours = RoundToHalfHour(minutes / 60)

      Return hours

    End Function

    Public Function GetRunningTotal() As Decimal
      Dim mItemsBeforeMe As List(Of CrewTimesheet) = CType(Me.Parent, CrewTimesheetList).Where(Function(d) CType(d.AccountingDate, Date) < AccountingDate _
                                                                                               AndAlso d.TimesheetCategoryID <> CommonData.Enums.TimesheetCategories.UnBooked _
                                                                                               AndAlso Not d.LastDayPreviousMonthInd).ToList
      Dim before As Decimal = mItemsBeforeMe.Sum(Function(d) d.HoursForDay)
      Return before + Me.HoursForDay
    End Function

    Public Function GetTotalSnT() As Decimal
      Dim mItemsBeforeMe As List(Of CrewTimesheet) = CType(Me.Parent, CrewTimesheetList).Where(Function(d) CType(d.AccountingDate, Date) < AccountingDate _
                                                                                               AndAlso d.TimesheetCategoryID <> CommonData.Enums.TimesheetCategories.UnBooked _
                                                                                               AndAlso Not d.LastDayPreviousMonthInd).ToList
      Dim before As Decimal = mItemsBeforeMe.Sum(Function(d) d.SnTForDay)
      Return before + Me.HoursForDay
    End Function

#End Region

#Region " Public Holidays "

    Public Function GetPublicHolidayHours() As Decimal

      If QualifiesForPublicHolidayHours() Then
        Return CalculatePublicHolidayHours(GetPublicHolidayHourCalculationType(AccountingDate.Value))
      End If

      Return 0

    End Function

    Public Function QualifiesForPublicHolidayHours() As Boolean

      If IsPublicHoliday() AndAlso Not IsUnbooked() AndAlso Not IsLeave() AndAlso Not IsOff() AndAlso Not IsUnpaidLeave() Then
        Return True
      End If

      Return False

    End Function

    Public Shared Function GetPublicHolidayHourCalculationType(EffectiveDate As Date) As CommonData.Enums.PublicHolidayCalculationType

      Dim AbsorbAndUntilMidnight As Date = CType(CommonData.Lists.ROCalculationChangedDateList.GetItem(CommonData.Enums.PublicHolidayCalculationType.UpToMidnight).EffectiveDate, Date)
      Dim OvertimeDependent As Date = CType(CommonData.Lists.ROCalculationChangedDateList.GetItem(CommonData.Enums.PublicHolidayCalculationType.OvertimeDependent).EffectiveDate, Date)

      If EffectiveDate >= AbsorbAndUntilMidnight And EffectiveDate < OvertimeDependent Then
        Return CommonData.Enums.PublicHolidayCalculationType.UpToMidnight

      ElseIf EffectiveDate >= OvertimeDependent Then
        Return CommonData.Enums.PublicHolidayCalculationType.OvertimeDependent

      End If

      Return CommonData.Enums.PublicHolidayCalculationType.Original

    End Function

    Public Function CalculatePublicHolidayHours(ByVal CalculationType As CommonData.Enums.PublicHolidayCalculationType) As Decimal

      Select Case CalculationType

        Case CommonData.Enums.PublicHolidayCalculationType.Original
          Return HoursForDay

        Case CommonData.Enums.PublicHolidayCalculationType.UpToMidnight
          Return HoursWorkedUntilMidnight()

        Case CommonData.Enums.PublicHolidayCalculationType.OvertimeDependent
          Return HoursWorkedUntilMidnight()

      End Select

      Return 0

    End Function

#End Region

    Private Function CanRecalculate() As Boolean

      'Dim Result As Boolean = False
      ''if timesheet month is open and the current timesheet is not overriden
      'If Not Me.GetParent.TMClosedInd AndAlso OverridenByUserID Is Nothing Then
      '  Result = True
      'End If
      'Return Result
      Return True

    End Function

    Public Sub CalculateHoursForDay()

      If CanRecalculate() Then
        'If TimesheetCategory IsNot Nothing Then
        'if we are set to use the actual hours worked
        If ActualHoursInd Then
          'calculate the hours worked
          HoursForDay = GetActualHoursWorked()
          'if this category does require you to check if the hours worked falls between the min and max (in order to determine if the max must be awarded)
          If TimesheetCategory.MinMaxHoursCalcInd Then
            'check that the min and max hours are set, just to be safe
            If TimesheetCategoryHasMinMaxHours() Then
              If HoursWorkedIsBetweenMinMaxHours() Then
                HoursForDay = TimesheetCategory.MaxHours
                'no need to check if it is greater than the max, as the hours per day as the variable is already defaulted to actual hours
              End If
              'if either min hours or max hours is null, then it will default to actual hours
            End If
            'no need to check anything here either as the hours per day is already defaulted to actual hours
          End If
        Else
          'if not subject to minmax calculation, then get the hours it should be based off of
          SetNonMinMaxTimesheetHours()
        End If
      End If

    End Sub

    Public Sub CalculateHoursAboveOTLimit(ByVal PreviousEntry As CrewTimesheet)

      'TimesheetMonth.HoursBeforeOvertime
      If Me.HoursForMonth > Me.GetParent.TMHoursBeforeOvertime Then
        Me.HoursAboveOTLimit = RoundToHalfHour(Math.Abs(Me.GetParent.TMHoursBeforeOvertime - Me.HoursForMonth))
      End If

    End Sub

    Public Sub CalculateOvertimeHours(ByVal PreviousEntry As CrewTimesheet)

      If CanRecalculate() Then 'Not TimesheetMonth.ClosedInd
        If (Not Me.IsNew) AndAlso (Not Singular.Misc.IsNullNothing(Me.OverridenByUserID, True) Or (Not Singular.Misc.IsNullNothing(Me.AuthorisedByUserID, True))) Then
          'timesheet has been overriden, do nothing - leave as is
        Else
          If PreviousEntry IsNot Nothing Then

            Dim previousTotalMonthlyHours As Decimal = PreviousEntry.HoursForMonth
            Me.CalculateHoursAboveOTLimit(PreviousEntry)
            Dim currentTotalMonthlyHours As Decimal = Me.HoursForMonth
            Dim otime As Decimal = 0
            Dim MonthHoursNoOvertime As Integer = Me.GetParent.TMHoursBeforeOvertime

            'if the current total is still below 180 hours
            If currentTotalMonthlyHours < MonthHoursNoOvertime Then
              'then there is no overtime
              otime = 0
              'if the total of is above 180 hours
            ElseIf currentTotalMonthlyHours > MonthHoursNoOvertime Then
              'first check if the previous entry total hours was below 180, if it was then only get the difference between 180 and current monthly hours
              If previousTotalMonthlyHours < MonthHoursNoOvertime And currentTotalMonthlyHours > MonthHoursNoOvertime Then
                otime = currentTotalMonthlyHours - MonthHoursNoOvertime
                OvertimeHours = otime
              Else
                If currentTotalMonthlyHours > MonthHoursNoOvertime Then
                  otime = HoursForDay 'currentTotalMonthlyHours - previousTotalMonthlyHours
                End If
                OvertimeHours = HoursForDay
              End If
            End If

            'just in case
            If otime < 0 Then
              otime = 0
            End If

            HoursAboveOTLimit = RoundToHalfHour(Math.Abs(PreviousEntry.HoursAboveOTLimit + otime))

          End If
        End If
      End If

    End Sub

    Public Sub CalculateShortfall(ByVal PreviousEntry As CrewTimesheet, PreviousShortFall As Decimal)

      If CanRecalculate() Then 'Not TimesheetMonth.ClosedInd
        Dim shortfall As Decimal = 0
        Dim ShortfallMinHours As Integer = SystemParameter.ShortfallMinHours

        If Me.ShortfallCalcInd = False Then
          Me.ShortfallHours = 0
        Else
          If ((Not Me.IsNew) AndAlso (Not Singular.Misc.IsNullNothing(Me.OverridenByUserID, True) Or (Not Singular.Misc.IsNullNothing(Me.AuthorisedByUserID, True)))) _
             Or ((PreviousEntry IsNot Nothing AndAlso _
                  PreviousEntry.TimesheetCategoryID = CType(CommonData.Enums.TimesheetCategories.UnBooked, Integer)) Or _
                  (Me.TimesheetCategoryID = CType(CommonData.Enums.TimesheetCategories.UnBooked, Integer))) Then
            'Do Nothing
          Else
            If PreviousEntry IsNot Nothing AndAlso PreviousEntry.TimesheetCategory IsNot Nothing Then
              If PreviousEntry IsNot Nothing AndAlso (CType(PreviousEntry.AccountingDate, Date).Day = (CType(Me.AccountingDate, Date).Day - 1)) Then 'AndAlso PreviousEntry.TimesheetCategory.ShortfallCalcInd
                Dim prevDayEndTime As DateTime = Nothing
                'if the crew has not checked their time
                If IsNullNothing(Me.CrewCheckedDate) Then
                  'use the calculated time to calculate the shortfall
                  prevDayEndTime = CType(PreviousEntry.CalculatedEndDateTime, DateTime)
                  shortfall = CType(Me.CalculatedStartDateTime, DateTime).Subtract(prevDayEndTime).TotalHours
                Else
                  'otherwise use the time they entered to calculate the shortfall
                  prevDayEndTime = CType(PreviousEntry.CrewEndDateTime, DateTime)
                  shortfall = CType(Me.CrewStartDateTime, DateTime).Subtract(prevDayEndTime).TotalHours
                End If
                'if the difference between the current item start time and previous item end time is less than the minimum time specified in maintenance
                If shortfall <= ShortfallMinHours Then
                  'give them the difference as their shortfall
                  shortfall = (shortfall - ShortfallMinHours)
                Else
                  'otherwise do not calculate shortfall
                  shortfall = 0
                End If
                'set the shortfall to what as calculated
                Me.ShortfallHours = RoundToHalfHour(Math.Abs(shortfall))
              Else
                Me.ShortfallHours = 0
              End If
            End If
          End If
        End If
      End If

    End Sub

    Public Sub SetupVariables()
      mTimesheetCategory = CommonData.Lists.ROTimesheetCategoryList.GetItem(TimesheetCategoryID)
      mSystemParam = CommonData.Lists.ROSystemParameterList.FirstOrDefault() 'CalculatedStartDateTime.Value
      'mTimesheetMonth = CommonData.Lists.ROTimesheetMonthListOld.GetItem(AccountingDate.Value)
    End Sub

    Private Function TimesheetCategoryHasMinMaxHours()

      Dim Result As Boolean = False
      If Not IsNullNothing(TimesheetCategory.MinHours) AndAlso Not IsNullNothing(TimesheetCategory.MaxHours) Then
        Result = True
      End If
      Return Result

    End Function

    Private Function HoursWorkedIsBetweenMinMaxHours() As Boolean

      Dim Result As Boolean = False
      If HoursForDay >= TimesheetCategory.MinHours And HoursForDay <= TimesheetCategory.MaxHours Then
        Result = True
      End If
      Return Result

    End Function

    Private Function SetNonMinMaxTimesheetHours()
      'SetupVariables()
      If TimesheetCategory IsNot Nothing Then
        If ((CompareSafe(TimesheetCategory.TimesheetCategoryID, 2) Or CompareSafe(TimesheetCategory.TimesheetCategoryID, 8)) AndAlso (CrewCheckedDate Is Nothing)) _
            Or CompareSafe(TimesheetCategory.TimesheetCategoryID, 7) _
            Or CompareSafe(TimesheetCategory.TimesheetCategoryID, 4) _
            Or (CompareSafe(TimesheetCategory.TimesheetCategoryID, 3)) _
            Or (CompareSafe(TimesheetCategory.TimesheetCategoryID, 13)) _
            Or (CompareSafe(TimesheetCategory.TimesheetCategoryID, 15)) Then
          HoursForDay = TimesheetCategory.MaxHours
        End If
      Else
        HoursForDay = 0
      End If
      'HoursForDay = 0
      Return HoursForDay
    End Function

    Public Sub HandleManualOverride()

      If Not ManualOverrideInd Then
        'Remove override
        Dim tcat As ROTimesheetCategory = Nothing
        If TimesheetCategoryID IsNot Nothing Then
          tcat = CommonData.Lists.ROTimesheetCategoryList.GetItem(TimesheetCategoryID)
          If tcat IsNot Nothing Then
            Me.OvertimeCalcInd = tcat.OvertimeCalcInd
            Me.ShortfallCalcInd = tcat.ShortfallCalcInd
          End If
        End If
        OverridenByUserID = Nothing
        OverridenDateTime = Nothing
      Else
        'Set override
        OverridenByUserID = OBLib.Security.Settings.CurrentUserID
        OverridenDateTime = Now
      End If

    End Sub

    Public Function GetTotalShortfall() As Decimal
      Dim mItemsBeforeMe As List(Of CrewTimesheet) = CType(Me.Parent, CrewTimesheetList).Where(Function(d) CType(d.AccountingDate, Date) < AccountingDate.Value).ToList
      Dim before As Decimal = mItemsBeforeMe.Sum(Function(d) d.ShortfallHours)
      Return before + Me.ShortfallHours
    End Function

    Public Sub CalculateOvertimeForDay(ByVal PreviousEntry As CrewTimesheet, ByVal PreviousHours As Decimal)

      If Not Me.GetParent.TMClosedInd Then 'Not TimesheetMonth.ClosedInd
        SetupVariables()
        Dim MonthHoursNoOvertime = Me.GetParent.TMHoursBeforeOvertime
        If PreviousEntry IsNot Nothing AndAlso PreviousEntry.TimesheetCategoryID <> CType(CommonData.Enums.TimesheetCategories.UnBooked, Integer) Then
          Dim mHoursBeforeAdd As Decimal = PreviousEntry.HoursForMonth
          Dim mHoursAfterAdd As Decimal = Me.HoursForMonth
          If mHoursAfterAdd >= MonthHoursNoOvertime And mHoursBeforeAdd <= MonthHoursNoOvertime Then
            'hours are greater than 180 after adding
            OvertimeHours = RoundToHalfHour(mHoursAfterAdd - MonthHoursNoOvertime)
          ElseIf mHoursBeforeAdd <= MonthHoursNoOvertime Then
            'if hours before adding in is less than 180, then no overtime is to be calculated
            OvertimeHours = 0
          ElseIf mHoursBeforeAdd > MonthHoursNoOvertime Then
            'if the total hours were over 180 already, then just take the days hours as overtime
            OvertimeHours = Me.HoursForDay
          End If
        Else
          'if the total hours were over 180 already
          If PreviousHours >= MonthHoursNoOvertime Then
            'then just take the days hours as overtime
            OvertimeHours = Me.HoursForDay
          Else
            OvertimeHours = 0
          End If
        End If
      End If

    End Sub

    Public Function GetShortDescription() As String
      If Description.Length > 50 Then
        Return Description.Substring(0, 50) & "..."
      Else
        Return Description
      End If
    End Function

    Public Sub SetShortDescription()
      SetProperty(ShortDescriptionProperty, GetShortDescription)
    End Sub

    Public Sub LoadShortDescription()
      LoadProperty(ShortDescriptionProperty, GetShortDescription)
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(CrewChangeDetailsProperty)
        .ServerRuleFunction = AddressOf CrewChangeDetailsValid
        '.ASyncBusyText = "Checking Rules..."
        .JavascriptRuleFunctionName = "CrewTimesheetBO.CrewChangeDetailsValid"
        .AddTriggerProperty(CrewStartDateTimeProperty)
        .AddTriggerProperty(CrewEndDateTimeProperty)
      End With

      With AddWebRule(ManagerChangeDetailsProperty)
        .ServerRuleFunction = AddressOf ManagerChangeDetailsValid
        '.ASyncBusyText = "Checking Rules..."
        .JavascriptRuleFunctionName = "CrewTimesheetBO.ManagerChangeDetailsValid"
        .AddTriggerProperty(ManagerStartDateTimeProperty)
        .AddTriggerProperty(ManagerEndDateTimeProperty)
      End With

    End Sub

    Public Shared Function CrewChangeDetailsValid(CrewTimesheet As CrewTimesheet)

      If CrewTimesheet.CrewChangeDetails.Trim.Length = 0 AndAlso _
         CrewTimesheet.CrewStartDateTime IsNot Nothing AndAlso _
         CrewTimesheet.CrewEndDateTime IsNot Nothing AndAlso _
         (CrewTimesheet.CalculatedStartDateTime <> CrewTimesheet.CrewStartDateTime Or _
          CrewTimesheet.CalculatedEndDateTime <> CrewTimesheet.CrewEndDateTime) Then
        Return "Crew Change Details is required"
      End If
      Return ""

    End Function

    Public Shared Function ManagerChangeDetailsValid(CrewTimesheet As CrewTimesheet)

      If CrewTimesheet.ManagerChangeDetails.Trim.Length = 0 AndAlso _
         CrewTimesheet.ManagerEndDateTime IsNot Nothing AndAlso _
         CrewTimesheet.ManagerEndDateTime IsNot Nothing AndAlso _
         (CrewTimesheet.CrewStartDateTime <> CrewTimesheet.ManagerStartDateTime Or _
          CrewTimesheet.CrewEndDateTime <> CrewTimesheet.ManagerEndDateTime) Then
        Return "Manager Change Details is required"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCrewTimesheet() method.

    End Sub

    Public Shared Function NewCrewTimesheet() As CrewTimesheet

      Return DataPortal.CreateChild(Of CrewTimesheet)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCrewTimesheet(dr As SafeDataReader) As CrewTimesheet

      Dim c As New CrewTimesheet()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CrewTimesheetIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AdhocCrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CalculatedStartDateTimeProperty, .GetValue(3))
          LoadProperty(CalculatedEndDateTimeProperty, .GetValue(4))
          LoadProperty(CrewStartDateTimeProperty, .GetValue(5))
          LoadProperty(CrewEndDateTimeProperty, .GetValue(6))
          LoadProperty(ManagerStartDateTimeProperty, .GetValue(7))
          LoadProperty(ManagerEndDateTimeProperty, .GetValue(8))
          LoadProperty(OvertimeHoursProperty, .GetDecimal(9))
          LoadProperty(ShortfallHoursProperty, .GetDecimal(10))
          LoadProperty(CrewChangeDetailsProperty, .GetString(11))
          LoadProperty(ManagerChangeDetailsProperty, .GetString(12))
          LoadProperty(CrewCheckedDateProperty, .GetValue(13))
          LoadProperty(AuthorisedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(AuthorisedDateTimeProperty, .GetValue(15))
          LoadProperty(DescriptionProperty, .GetString(16))
          LoadProperty(AccountingDateProperty, .GetValue(17))
          LoadProperty(AccumulateHoursIndProperty, .GetBoolean(18))
          LoadProperty(ActualHoursIndProperty, .GetBoolean(19))
          LoadProperty(OvertimeCalcIndProperty, .GetBoolean(20))
          LoadProperty(ShortfallCalcIndProperty, .GetBoolean(21))
          LoadProperty(TimesheetCategoryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
          LoadProperty(ManualOverrideIndProperty, .GetBoolean(23))
          LoadProperty(OverridenByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
          LoadProperty(OverridenDateTimeProperty, .GetValue(25))
          LoadProperty(CreatedByProperty, .GetInt32(26))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(27))
          LoadProperty(ModifiedByProperty, .GetInt32(28))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(29))
          LoadProperty(TimesheetMonthIDProperty, Singular.Misc.ZeroNothing(.GetInt32(30)))
          LoadProperty(HoursForDayProperty, .GetDecimal(31))
          LoadProperty(CreditorInvoiceDetailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(32)))
          LoadProperty(AdHocItemIndProperty, .GetBoolean(33))
          'LoadProperty(HumanResourceProperty, .GetString(34))
          'LoadProperty(IDNoProperty, .GetString(35))
          'LoadProperty(StaffNoProperty, .GetString(36))
          LoadProperty(SnTForDayProperty, .GetDecimal(34))
          'LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(38)))
          LoadProperty(LastDayPreviousMonthIndProperty, .GetBoolean(35))
          LoadProperty(DateOrderProperty, .GetInt32(36))
          LoadProperty(DayOfWeekProperty, AccountingDate.Value.ToString("ddd"))
          LoadProperty(OriginalCrewStartDateTimeProperty, CrewStartDateTime)
          LoadProperty(OriginalCrewEndDateTimeProperty, CrewEndDateTime)
          LoadProperty(OriginalManagerStartDateTimeProperty, ManagerStartDateTime)
          LoadProperty(OriginalManagerEndDateTimeProperty, ManagerEndDateTime)
          LoadProperty(DateOrderProperty, .GetInt32(36))
          LoadProperty(MealsProperty, .GetDecimal(37))
        End With
      End Using

      SetupVariables()
      LoadShortDescription()
      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCrewTimesheet"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCrewTimesheet"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCrewTimesheetID As SqlParameter = .Parameters.Add("@CrewTimesheetID", SqlDbType.Int)
          paramCrewTimesheetID.Value = GetProperty(CrewTimesheetIDProperty)
          If Me.IsNew Then
            paramCrewTimesheetID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@AdhocCrewTimesheetID", Singular.Misc.NothingDBNull(GetProperty(AdhocCrewTimesheetIDProperty)))
          .Parameters.AddWithValue("@CalculatedStartDateTime", (New SmartDate(GetProperty(CalculatedStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CalculatedEndDateTime", (New SmartDate(GetProperty(CalculatedEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CrewStartDateTime", (New SmartDate(GetProperty(CrewStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CrewEndDateTime", (New SmartDate(GetProperty(CrewEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ManagerStartDateTime", (New SmartDate(GetProperty(ManagerStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ManagerEndDateTime", (New SmartDate(GetProperty(ManagerEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@OvertimeHours", GetProperty(OvertimeHoursProperty))
          .Parameters.AddWithValue("@ShortfallHours", GetProperty(ShortfallHoursProperty))
          .Parameters.AddWithValue("@CrewChangeDetails", GetProperty(CrewChangeDetailsProperty))
          .Parameters.AddWithValue("@ManagerChangeDetails", GetProperty(ManagerChangeDetailsProperty))
          .Parameters.AddWithValue("@CrewCheckedDate", (New SmartDate(GetProperty(CrewCheckedDateProperty))).DBValue)
          .Parameters.AddWithValue("@AuthorisedByUserID", NothingDBNull(GetProperty(AuthorisedByUserIDProperty)))
          .Parameters.AddWithValue("@AuthorisedDateTime", (New SmartDate(GetProperty(AuthorisedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          .Parameters.AddWithValue("@AccountingDate", (New SmartDate(GetProperty(AccountingDateProperty))).DBValue)
          .Parameters.AddWithValue("@AccumulateHoursInd", GetProperty(AccumulateHoursIndProperty))
          .Parameters.AddWithValue("@ActualHoursInd", GetProperty(ActualHoursIndProperty))
          .Parameters.AddWithValue("@OvertimeCalcInd", GetProperty(OvertimeCalcIndProperty))
          .Parameters.AddWithValue("@ShortfallCalcInd", GetProperty(ShortfallCalcIndProperty))
          .Parameters.AddWithValue("@TimesheetCategoryID", GetProperty(TimesheetCategoryIDProperty))
          .Parameters.AddWithValue("@ManualOverrideInd", GetProperty(ManualOverrideIndProperty))
          .Parameters.AddWithValue("@OverridenByUserID", NothingDBNull(Singular.Misc.NothingDBNull(GetProperty(OverridenByUserIDProperty))))
          .Parameters.AddWithValue("@OverridenDateTime", (New SmartDate(GetProperty(OverridenDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@TimesheetMonthID", GetProperty(TimesheetMonthIDProperty))
          .Parameters.AddWithValue("@HoursForDay", GetProperty(HoursForDayProperty))
          .Parameters.AddWithValue("@CreditorInvoiceDetailID", Singular.Misc.NothingDBNull(GetProperty(CreditorInvoiceDetailIDProperty)))
          .Parameters.AddWithValue("@Meals", GetProperty(MealsProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CrewTimesheetIDProperty, paramCrewTimesheetID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

      'Me.GetParent.DoCalculations()

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCrewTimesheet"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CrewTimesheetID", GetProperty(CrewTimesheetIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace