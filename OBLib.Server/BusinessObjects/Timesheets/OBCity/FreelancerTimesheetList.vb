﻿' Generated 19 May 2014 16:30 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets

  <Serializable()> _
  Public Class FreelancerTimesheetList
    Inherits SingularBusinessListBase(Of FreelancerTimesheetList, FreelancerTimesheet)

    '#Region " Properties "

    '    Public ReadOnly Property TotalHours As Decimal
    '      Get
    '        Return Me.Sum(Function(d) d.HoursForDay)
    '      End Get
    '    End Property

    '#End Region

#Region " Business Methods "

    Public Function GetItem(CrewTimesheetID As Integer) As FreelancerTimesheet

      For Each child As FreelancerTimesheet In Me
        If child.CrewTimesheetID = CrewTimesheetID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceIDs As String = ""
      Public Property StartDate As Date? = Nothing
      Public Property EndDate As Date? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New(HumanResourceIDs As String, StartDate As Date?, EndDate As Date?, SystemID As Integer?, ProductionAreaID As Integer?)
        Me.HumanResourceIDs = HumanResourceIDs
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewFreelancerTimesheetList() As FreelancerTimesheetList

      Return New FreelancerTimesheetList()

    End Function

    Public Shared Sub BeginGetFreelancerTimesheetList(CallBack As EventHandler(Of DataPortalResult(Of FreelancerTimesheetList)))

      Dim dp As New DataPortal(Of FreelancerTimesheetList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetFreelancerTimesheetList() As FreelancerTimesheetList

      Return DataPortal.Fetch(Of FreelancerTimesheetList)(New Criteria())

    End Function

    Public Shared Function GetFreelancerTimesheetList(HumanResourceIDs As String, StartDate As Date?, EndDate As Date?, SystemID As Integer?, ProductionAreaID As Integer?) As FreelancerTimesheetList

      Return DataPortal.Fetch(Of FreelancerTimesheetList)(New Criteria(HumanResourceIDs, StartDate, EndDate, SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(FreelancerTimesheet.GetFreelancerTimesheet(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getFreelancerTimesheetList"
            cm.Parameters.AddWithValue("@HumanResourceIDs", Singular.Strings.MakeEmptyDBNull(crit.HumanResourceIDs))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As FreelancerTimesheet In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As FreelancerTimesheet In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace