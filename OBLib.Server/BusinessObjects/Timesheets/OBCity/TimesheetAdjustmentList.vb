﻿' Generated 07 Mar 2015 09:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity

  <Serializable()> _
  Public Class TimesheetAdjustmentList
    Inherits SingularBusinessListBase(Of TimesheetAdjustmentList, TimesheetAdjustment)

#Region " Business Methods "

    Public Function GetItem(TimesheetAdjustmentsID As Integer) As TimesheetAdjustment

      For Each child As TimesheetAdjustment In Me
        If child.TimesheetAdjustmentsID = TimesheetAdjustmentsID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Timesheet Adjustments"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits SingularCriteriaBase(Of Criteria)

      Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HumanResourceID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Human Resource", Description:="")>
      Public Property HumanResourceID() As Integer?
        Get
          Return ReadProperty(HumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(HumanResourceIDProperty, Value)
        End Set
      End Property

      Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Human Resource", Description:="")>
      Public Property ManagerHumanResourceID() As Integer?
        Get
          Return ReadProperty(ManagerHumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ManagerHumanResourceIDProperty, Value)
        End Set
      End Property

      Public Shared ReflectedInMonthIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ReflectedInMonthID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Reflected Month", Description:="")>
      Public Property ReflectedInMonthID() As Integer?
        Get
          Return ReadProperty(ReflectedInMonthIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ReflectedInMonthIDProperty, Value)
        End Set
      End Property

      Public Shared AdjustedMonthIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.AdjustedMonthID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Adjusted Month", Description:="")>
      Public Property AdjustedMonthID() As Integer?
        Get
          Return ReadProperty(AdjustedMonthIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(AdjustedMonthIDProperty, Value)
        End Set
      End Property

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewTimesheetAdjustmentList() As TimesheetAdjustmentList

      Return New TimesheetAdjustmentList()

    End Function

    Public Shared Sub BeginGetTimesheetAdjustmentList(CallBack As EventHandler(Of DataPortalResult(Of TimesheetAdjustmentList)))

      Dim dp As New DataPortal(Of TimesheetAdjustmentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetTimesheetAdjustmentList() As TimesheetAdjustmentList

      Return DataPortal.Fetch(Of TimesheetAdjustmentList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(TimesheetAdjustment.GetTimesheetAdjustment(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getTimesheetAdjustmentList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@ManagerHumanResourceID", NothingDBNull(crit.ManagerHumanResourceID))
            cm.Parameters.AddWithValue("@ReflectedInMonthID", NothingDBNull(crit.ReflectedInMonthID))
            cm.Parameters.AddWithValue("@AdjustedMonthID", NothingDBNull(crit.AdjustedMonthID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As TimesheetAdjustment In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As TimesheetAdjustment In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace