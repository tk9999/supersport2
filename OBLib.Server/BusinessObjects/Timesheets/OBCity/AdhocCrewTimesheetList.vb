﻿' Generated 10 Mar 2015 12:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity

  <Serializable()> _
  Public Class AdhocCrewTimesheetList
    Inherits SingularBusinessListBase(Of AdhocCrewTimesheetList, AdhocCrewTimesheet)

#Region " Business Methods "

    Public Function GetItem(AdhocCrewTimesheetID As Integer) As AdhocCrewTimesheet

      For Each child As AdhocCrewTimesheet In Me
        If child.AdhocCrewTimesheetID = AdhocCrewTimesheetID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Adhoc Crew Timesheets"

    End Function

    Public Function GetAdhocCrewTimesheetHumanResource(AdhocCrewTimesheetHumanResourceID As Integer) As AdhocCrewTimesheetHumanResource

      Dim obj As AdhocCrewTimesheetHumanResource = Nothing
      For Each parent As AdhocCrewTimesheet In Me
        obj = parent.AdhocCrewTimesheetHumanResourceList.GetItem(AdhocCrewTimesheetHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property AdHocCrewTimesheetID As Integer? = Nothing

      Public Sub New(AdHocCrewTimesheetID As Integer?)
        Me.AdHocCrewTimesheetID = AdHocCrewTimesheetID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewAdhocCrewTimesheetList() As AdhocCrewTimesheetList

      Return New AdhocCrewTimesheetList()

    End Function

    Public Shared Sub BeginGetAdhocCrewTimesheetList(CallBack As EventHandler(Of DataPortalResult(Of AdhocCrewTimesheetList)))

      Dim dp As New DataPortal(Of AdhocCrewTimesheetList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAdhocCrewTimesheetList() As AdhocCrewTimesheetList

      Return DataPortal.Fetch(Of AdhocCrewTimesheetList)(New Criteria())

    End Function

    Public Shared Function GetAdhocCrewTimesheetList(AdHocCrewTimesheetID As Integer?) As AdhocCrewTimesheetList

      Return DataPortal.Fetch(Of AdhocCrewTimesheetList)(New Criteria(AdHocCrewTimesheetID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AdhocCrewTimesheet.GetAdhocCrewTimesheet(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As AdhocCrewTimesheet = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AdhocCrewTimesheetID <> sdr.GetInt32(2) Then
            parent = Me.GetItem(sdr.GetInt32(2))
          End If
          parent.AdhocCrewTimesheetHumanResourceList.RaiseListChangedEvents = False
          parent.AdhocCrewTimesheetHumanResourceList.Add(AdhocCrewTimesheetHumanResource.GetAdhocCrewTimesheetHumanResource(sdr))
          parent.AdhocCrewTimesheetHumanResourceList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As AdhocCrewTimesheet In Me
        child.CheckRules()
        For Each AdhocCrewTimesheetHumanResource As AdhocCrewTimesheetHumanResource In child.AdhocCrewTimesheetHumanResourceList
          AdhocCrewTimesheetHumanResource.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAdhocCrewTimesheetList"
            cm.Parameters.AddWithValue("@AdHocCrewTimesheetID", NothingDBNull(crit.AdHocCrewTimesheetID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AdhocCrewTimesheet In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AdhocCrewTimesheet In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace