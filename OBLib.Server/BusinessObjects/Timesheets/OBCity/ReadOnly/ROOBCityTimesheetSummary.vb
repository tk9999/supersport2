﻿' Generated 10 Mar 2015 17:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity.ReadOnly

  <Serializable()> _
  Public Class ROOBCityTimesheetSummary
    Inherits SingularReadOnlyBase(Of ROOBCityTimesheetSummary)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractTypeID, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractTypeID() As Integer
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared BookedDaysProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BookedDays, "Booked Days")
    ''' <summary>
    ''' Gets the Booked Days value
    ''' </summary>
    <Display(Name:="Days", Description:="")>
    Public ReadOnly Property BookedDays() As Integer
      Get
        Return GetProperty(BookedDaysProperty)
      End Get
    End Property

    Public Shared UnbookedDaysProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UnbookedDays, "Unbooked Days")
    ''' <summary>
    ''' Gets the Unbooked Days value
    ''' </summary>
    <Display(Name:="Unbooked", Description:="")>
    Public ReadOnly Property UnbookedDays() As Integer
      Get
        Return GetProperty(UnbookedDaysProperty)
      End Get
    End Property

    Public Shared HoursForMonthProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.HoursForMonth, "Hours For Month")
    ''' <summary>
    ''' Gets the Hours For Month value
    ''' </summary>
    <Display(Name:="Hours", Description:="")>
    Public ReadOnly Property HoursForMonth() As Decimal
      Get
        Return GetProperty(HoursForMonthProperty)
      End Get
    End Property

    Public Shared SnTForMonthProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.SnTForMonth, "Sn T For Month")
    ''' <summary>
    ''' Gets the Sn T For Month value
    ''' </summary>
    <Display(Name:="S&T", Description:="")>
    Public ReadOnly Property SnTForMonth() As Decimal
      Get
        Return GetProperty(SnTForMonthProperty)
      End Get
    End Property

    Public Shared ShortfallForMonthProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShortfallForMonth, "Shortfall For Month")
    ''' <summary>
    ''' Gets the Shortfall For Month value
    ''' </summary>
    <Display(Name:="Shortfall", Description:="")>
    Public ReadOnly Property ShortfallForMonth() As Decimal
      Get
        Return GetProperty(ShortfallForMonthProperty)
      End Get
    End Property

    Public Shared PercUsedProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.PercUsed, "Perc Used")
    ''' <summary>
    ''' Gets the Perc Used value
    ''' </summary>
    <Display(Name:="Perc", Description:="")>
    Public ReadOnly Property PercUsed() As Decimal
      Get
        Return GetProperty(PercUsedProperty)
      End Get
    End Property

    Public Shared OvertimeForMonthProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.OvertimeForMonth, "Overtime For Month")
    ''' <summary>
    ''' Gets the Hours For Month value
    ''' </summary>
    <Display(Name:="Overtime", Description:="")>
    Public ReadOnly Property OvertimeForMonth() As Decimal
      Get
        Return GetProperty(OvertimeForMonthProperty)
      End Get
    End Property

    Public Shared TimesheetMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetMonthID, "Timesheet Month ID")
    ''' <summary>
    ''' Gets the Unbooked Days value
    ''' </summary>
    <Display(Name:="Timesheet Month ID", Description:="")>
    Public ReadOnly Property TimesheetMonthID() As Integer
      Get
        Return GetProperty(TimesheetMonthIDProperty)
      End Get
    End Property


#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROOBCityTimesheetSummary(dr As SafeDataReader) As ROOBCityTimesheetSummary

      Dim r As New ROOBCityTimesheetSummary()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceProperty, .GetString(1))
        LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ContractTypeProperty, .GetString(3))
        LoadProperty(BookedDaysProperty, .GetInt32(4))
        LoadProperty(UnbookedDaysProperty, .GetInt32(5))
        LoadProperty(HoursForMonthProperty, .GetDecimal(6))
        LoadProperty(SnTForMonthProperty, .GetDecimal(7))
        LoadProperty(ShortfallForMonthProperty, .GetDecimal(8))
        LoadProperty(PercUsedProperty, .GetDecimal(9))
        LoadProperty(OvertimeForMonthProperty, .GetDecimal(10))
        LoadProperty(TimesheetMonthIDProperty, .GetInt32(11))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace