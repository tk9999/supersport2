﻿' Generated 18 Feb 2015 10:59 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity.ReadOnly

  <Serializable()> _
  Public Class ROCrewTimesheet
    Inherits SingularReadOnlyBase(Of ROCrewTimesheet)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CrewTimesheetIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewTimesheetID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property CrewTimesheetID() As Integer
      Get
        Return GetProperty(CrewTimesheetIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource that this timesheet is for")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared AdhocCrewTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdhocCrewTimesheetID, "Adhoc Crew Timesheet", Nothing)
    ''' <summary>
    ''' Gets the Adhoc Crew Timesheet value
    ''' </summary>
    <Display(Name:="Adhoc Crew Timesheet", Description:="Link to the AdHoc Crew Timesheet")>
    Public ReadOnly Property AdhocCrewTimesheetID() As Integer?
      Get
        Return GetProperty(AdhocCrewTimesheetIDProperty)
      End Get
    End Property

    Public Shared CalculatedStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CalculatedStartDateTime, "Calculated Start Date Time")
    ''' <summary>
    ''' Gets the Calculated Start Date Time value
    ''' </summary>
    <Display(Name:="Calculated Start Date Time", Description:="This is the calculated start time for the day. This cannot be editted by the user.")>
    Public ReadOnly Property CalculatedStartDateTime As DateTime?
      Get
        Return GetProperty(CalculatedStartDateTimeProperty)
      End Get
    End Property

    Public Shared CalculatedEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CalculatedEndDateTime, "Calculated End Date Time")
    ''' <summary>
    ''' Gets the Calculated End Date Time value
    ''' </summary>
    <Display(Name:="Calculated End Date Time", Description:="This is the calculated end time for the day. This cannot be editted by the user.")>
    Public ReadOnly Property CalculatedEndDateTime As DateTime?
      Get
        Return GetProperty(CalculatedEndDateTimeProperty)
      End Get
    End Property

    Public Shared CrewStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewStartDateTime, "Crew Start Date Time")
    ''' <summary>
    ''' Gets the Crew Start Date Time value
    ''' </summary>
    <Display(Name:="Crew Start Date Time", Description:="Defaults to CalculatedStartDateTime. If it differs then we know that the HR has made a change and will need to be authorised")>
    Public ReadOnly Property CrewStartDateTime As DateTime?
      Get
        Return GetProperty(CrewStartDateTimeProperty)
      End Get
    End Property

    Public Shared CrewEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewEndDateTime, "Crew End Date Time")
    ''' <summary>
    ''' Gets the Crew End Date Time value
    ''' </summary>
    <Display(Name:="Crew End Date Time", Description:="Defaults to CalculatedEndDateTime. If it differs then we know that the HR has made a change and will need to be authorised")>
    Public ReadOnly Property CrewEndDateTime As DateTime?
      Get
        Return GetProperty(CrewEndDateTimeProperty)
      End Get
    End Property

    Public Shared ManagerStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ManagerStartDateTime, "Manager Start Date Time")
    ''' <summary>
    ''' Gets the Manager Start Date Time value
    ''' </summary>
    <Display(Name:="Manager Start Date Time", Description:="Defaults to CalculatedStartDateTime. If it differs then we know that the HR has made a change and will need to be authorised")>
    Public ReadOnly Property ManagerStartDateTime As DateTime?
      Get
        Return GetProperty(ManagerStartDateTimeProperty)
      End Get
    End Property

    Public Shared ManagerEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ManagerEndDateTime, "Manager End Date Time")
    ''' <summary>
    ''' Gets the Manager End Date Time value
    ''' </summary>
    <Display(Name:="Manager End Date Time", Description:="Defaults to CalculatedEndDateTime. If it differs then we know that the HR has made a change and will need to be authorised")>
    Public ReadOnly Property ManagerEndDateTime As DateTime?
      Get
        Return GetProperty(ManagerEndDateTimeProperty)
      End Get
    End Property

    Public Shared OvertimeHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.OvertimeHours, "Overtime Hours", CDec(0))
    ''' <summary>
    ''' Gets the Overtime Hours value
    ''' </summary>
    <Display(Name:="Overtime Hours", Description:="Hours of shift that are overtime")>
    Public ReadOnly Property OvertimeHours() As Decimal
      Get
        Return GetProperty(OvertimeHoursProperty)
      End Get
    End Property

    Public Shared ShortfallHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShortfallHours, "Shortfall Hours", CDec(0))
    ''' <summary>
    ''' Gets the Shortfall Hours value
    ''' </summary>
    <Display(Name:="Shortfall Hours", Description:="Hours credited due to less than the Short Fall Minimum Hours between this shift and the previous shift")>
    Public ReadOnly Property ShortfallHours() As Decimal
      Get
        Return GetProperty(ShortfallHoursProperty)
      End Get
    End Property

    Public Shared CrewChangeDetailsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CrewChangeDetails, "Crew Change Details", "")
    ''' <summary>
    ''' Gets the Crew Change Details value
    ''' </summary>
    <Display(Name:="Crew Change Details", Description:="Space for the human resource to capture comments regarding their timesheet item.")>
    Public ReadOnly Property CrewChangeDetails() As String
      Get
        Return GetProperty(CrewChangeDetailsProperty)
      End Get
    End Property

    Public Shared ManagerChangeDetailsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerChangeDetails, "Manager Change Details", "")
    ''' <summary>
    ''' Gets the Manager Change Details value
    ''' </summary>
    <Display(Name:="Manager Change Details", Description:="Reason why the manager has changed the record")>
    Public ReadOnly Property ManagerChangeDetails() As String
      Get
        Return GetProperty(ManagerChangeDetailsProperty)
      End Get
    End Property

    Public Shared CrewCheckedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewCheckedDate, "Crew Checked Date")
    ''' <summary>
    ''' Gets the Crew Checked Date value
    ''' </summary>
    <Display(Name:="Crew Checked Date", Description:="The date that the HR went onto the web a marked their timesheet record as correct and has not made any changes")>
    Public ReadOnly Property CrewCheckedDate As DateTime?
      Get
        Return GetProperty(CrewCheckedDateProperty)
      End Get
    End Property

    Public Shared AuthorisedByUserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuthorisedByUserID, "Authorised By User", 0)
    ''' <summary>
    ''' Gets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised By User", Description:="The user that authorised this timesheet record")>
    Public ReadOnly Property AuthorisedByUserID() As Integer
      Get
        Return GetProperty(AuthorisedByUserIDProperty)
      End Get
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, "Authorised Date Time")
    ''' <summary>
    ''' Gets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Authorised Date Time", Description:="The date and time the timesheet item was authorised")>
    Public ReadOnly Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="Description of what the HR worked on that day")>
    Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared AccountingDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AccountingDate, "Accounting Date")
    ''' <summary>
    ''' Gets the Accounting Date value
    ''' </summary>
    <Display(Name:="Accounting Date", Description:="The date that will be used to determine which payment run/timesheet month the record needs to be paid under")>
    Public ReadOnly Property AccountingDate As DateTime?
      Get
        Return GetProperty(AccountingDateProperty)
      End Get
    End Property

    Public Shared AccumulateHoursIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AccumulateHoursInd, "Accumulate Hours", True)
    ''' <summary>
    ''' Gets the Accumulate Hours value
    ''' </summary>
    <Display(Name:="Accumulate Hours", Description:="Whether or not the record must included accumulated hours (running total)")>
    Public ReadOnly Property AccumulateHoursInd() As Boolean
      Get
        Return GetProperty(AccumulateHoursIndProperty)
      End Get
    End Property

    Public Shared ActualHoursIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ActualHoursInd, "Actual Hours", True)
    ''' <summary>
    ''' Gets the Actual Hours value
    ''' </summary>
    <Display(Name:="Actual Hours", Description:="Whether or not this record must be accounted for in actual hours")>
    Public ReadOnly Property ActualHoursInd() As Boolean
      Get
        Return GetProperty(ActualHoursIndProperty)
      End Get
    End Property

    Public Shared OvertimeCalcIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OvertimeCalcInd, "Overtime Calc", True)
    ''' <summary>
    ''' Gets the Overtime Calc value
    ''' </summary>
    <Display(Name:="Overtime Calc", Description:="Does this record qualify for overtime?")>
    Public ReadOnly Property OvertimeCalcInd() As Boolean
      Get
        Return GetProperty(OvertimeCalcIndProperty)
      End Get
    End Property

    Public Shared ShortfallCalcIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShortfallCalcInd, "Shortfall Calc", True)
    ''' <summary>
    ''' Gets the Shortfall Calc value
    ''' </summary>
    <Display(Name:="Shortfall Calc", Description:="Does this record qualify for shortfall?")>
    Public ReadOnly Property ShortfallCalcInd() As Boolean
      Get
        Return GetProperty(ShortfallCalcIndProperty)
      End Get
    End Property

    Public Shared TimesheetCategoryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetCategoryID, "Timesheet Category", 0)
    ''' <summary>
    ''' Gets the Timesheet Category value
    ''' </summary>
    <Display(Name:="Timesheet Category", Description:="The Category under which this timesheet entry has been categorised")>
    Public ReadOnly Property TimesheetCategoryID() As Integer
      Get
        Return GetProperty(TimesheetCategoryIDProperty)
      End Get
    End Property

    Public Shared ManualOverrideIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ManualOverrideInd, "Manual Override", False)
    ''' <summary>
    ''' Gets the Manual Override value
    ''' </summary>
    <Display(Name:="Manual Override", Description:="Whether or not this record has been manually overriden. If true, some of the standard system rules will be ignored and will take the manually overriden fields over the system calculation")>
    Public ReadOnly Property ManualOverrideInd() As Boolean
      Get
        Return GetProperty(ManualOverrideIndProperty)
      End Get
    End Property

    Public Shared OverridenByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OverridenByUserID, "Overriden By User", Nothing)
    ''' <summary>
    ''' Gets the Overriden By User value
    ''' </summary>
    <Display(Name:="Overriden By User", Description:="The user who overrode the record")>
    Public ReadOnly Property OverridenByUserID() As Integer?
      Get
        Return GetProperty(OverridenByUserIDProperty)
      End Get
    End Property

    Public Shared OverridenDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OverridenDateTime, "Overriden Date Time")
    ''' <summary>
    ''' Gets the Overriden Date Time value
    ''' </summary>
    <Display(Name:="Overriden Date Time", Description:="When the record was overriden")>
    Public ReadOnly Property OverridenDateTime As DateTime?
      Get
        Return GetProperty(OverridenDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared TimesheetMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetMonthID, "Timesheet Month", 0)
    ''' <summary>
    ''' Gets the Timesheet Month value
    ''' </summary>
    <Display(Name:="Timesheet Month", Description:="The timesheet month that the record was assigned to for payment.")>
    Public ReadOnly Property TimesheetMonthID() As Integer
      Get
        Return GetProperty(TimesheetMonthIDProperty)
      End Get
    End Property

    Public Shared HoursForDayProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.HoursForDay, "Hours For Day", CDec(0))
    ''' <summary>
    ''' Gets the Hours For Day value
    ''' </summary>
    <Display(Name:="Hours For Day", Description:="The amount of hours that the worked for thet day (system calculated)")>
    Public ReadOnly Property HoursForDay() As Decimal
      Get
        Return GetProperty(HoursForDayProperty)
      End Get
    End Property

    Public Shared CreditorInvoiceDetailIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CreditorInvoiceDetailID, "Creditor Invoice Detail", Nothing)
    ''' <summary>
    ''' Gets the Creditor Invoice Detail value
    ''' </summary>
    <Display(Name:="Creditor Invoice Detail", Description:="Link to the invoice (Freelancers only)")>
    Public ReadOnly Property CreditorInvoiceDetailID() As Integer?
      Get
        Return GetProperty(CreditorInvoiceDetailIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewTimesheetIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CrewChangeDetails

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCrewTimesheet(dr As SafeDataReader) As ROCrewTimesheet

      Dim r As New ROCrewTimesheet()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CrewTimesheetIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(AdhocCrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CalculatedStartDateTimeProperty, .GetValue(3))
        LoadProperty(CalculatedEndDateTimeProperty, .GetValue(4))
        LoadProperty(CrewStartDateTimeProperty, .GetValue(5))
        LoadProperty(CrewEndDateTimeProperty, .GetValue(6))
        LoadProperty(ManagerStartDateTimeProperty, .GetValue(7))
        LoadProperty(ManagerEndDateTimeProperty, .GetValue(8))
        LoadProperty(OvertimeHoursProperty, .GetDecimal(9))
        LoadProperty(ShortfallHoursProperty, .GetDecimal(10))
        LoadProperty(CrewChangeDetailsProperty, .GetString(11))
        LoadProperty(ManagerChangeDetailsProperty, .GetString(12))
        LoadProperty(CrewCheckedDateProperty, .GetValue(13))
        LoadProperty(AuthorisedByUserIDProperty, .GetInt32(14))
        LoadProperty(AuthorisedDateTimeProperty, .GetValue(15))
        LoadProperty(DescriptionProperty, .GetString(16))
        LoadProperty(AccountingDateProperty, .GetValue(17))
        LoadProperty(AccumulateHoursIndProperty, .GetBoolean(18))
        LoadProperty(ActualHoursIndProperty, .GetBoolean(19))
        LoadProperty(OvertimeCalcIndProperty, .GetBoolean(20))
        LoadProperty(ShortfallCalcIndProperty, .GetBoolean(21))
        LoadProperty(TimesheetCategoryIDProperty, .GetInt32(22))
        LoadProperty(ManualOverrideIndProperty, .GetBoolean(23))
        LoadProperty(OverridenByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
        LoadProperty(OverridenDateTimeProperty, .GetValue(25))
        LoadProperty(CreatedByProperty, .GetInt32(26))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(27))
        LoadProperty(ModifiedByProperty, .GetInt32(28))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(29))
        LoadProperty(TimesheetMonthIDProperty, .GetInt32(30))
        LoadProperty(HoursForDayProperty, .GetDecimal(31))
        LoadProperty(CreditorInvoiceDetailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(32)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace