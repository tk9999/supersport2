﻿' Generated 10 Mar 2015 12:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity.ReadOnly

  <Serializable()> _
  Public Class ROAdhocCrewTimesheet
    Inherits SingularReadOnlyBase(Of ROAdhocCrewTimesheet)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AdhocCrewTimesheetIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdhocCrewTimesheetID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property AdhocCrewTimesheetID() As Integer
      Get
        Return GetProperty(AdhocCrewTimesheetIDProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
    Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:=""),
    DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    <Display(Name:="Start Date", Description:="")>
    Public ReadOnly Property StartDate As String
      Get
        If StartDateTime IsNot Nothing Then
          Return StartDateTime.Value.ToString("dd-MMM-yy")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Time", Description:="")>
    Public ReadOnly Property StartTime As String
      Get
        If StartDateTime IsNot Nothing Then
          Return StartDateTime.Value.ToString("HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:=""),
    DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    <Display(Name:="End Date", Description:="")>
    Public ReadOnly Property EndDate As String
      Get
        If EndDateTime IsNot Nothing Then
          Return EndDateTime.Value.ToString("dd-MMM-yy")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Time", Description:="")>
    Public ReadOnly Property EndTime As String
      Get
        If EndDateTime IsNot Nothing Then
          Return EndDateTime.Value.ToString("HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared TimesheetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TimesheetDate, "Timesheet Date")
    ''' <summary>
    ''' Gets the Timesheet Date value
    ''' </summary>
    <Display(Name:="Timesheet Date", Description:=""),
    DateField(FormatString:="dd-MMM-yy")>
    Public ReadOnly Property TimesheetDate As DateTime?
      Get
        Return GetProperty(TimesheetDateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(Name:="Created Date", Description:=""),
    DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Modified Date", Description:=""),
    DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared HRCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HRCount, "HR Count", 0)
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="HR Count", Description:="")>
    Public ReadOnly Property HRCount() As Integer
      Get
        Return GetProperty(HRCountProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Created By", Description:="")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared ModifiedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ModifiedByName, "Modified By", "")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Last Modified By", Description:="")>
    Public ReadOnly Property ModifiedByName() As String
      Get
        Return GetProperty(ModifiedByNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AdhocCrewTimesheetIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Description

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAdhocCrewTimesheet(dr As SafeDataReader) As ROAdhocCrewTimesheet

      Dim r As New ROAdhocCrewTimesheet()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AdhocCrewTimesheetIDProperty, .GetInt32(0))
        LoadProperty(DescriptionProperty, .GetString(1))
        LoadProperty(StartDateTimeProperty, .GetValue(2))
        LoadProperty(EndDateTimeProperty, .GetValue(3))
        LoadProperty(TimesheetDateProperty, .GetValue(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(HRCountProperty, .GetInt32(9))
        LoadProperty(CreatedByNameProperty, .GetString(10))
        LoadProperty(ModifiedByNameProperty, .GetString(11))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace