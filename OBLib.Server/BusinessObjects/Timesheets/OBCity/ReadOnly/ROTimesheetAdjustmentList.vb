﻿' Generated 07 Mar 2015 09:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity.ReadOnly

  <Serializable()> _
  Public Class ROTimesheetAdjustmentList
    Inherits SingularReadOnlyListBase(Of ROTimesheetAdjustmentList, ROTimesheetAdjustment)

#Region " Business Methods "

    Public Function GetItem(TimesheetAdjustmentsID As Integer) As ROTimesheetAdjustment

      For Each child As ROTimesheetAdjustment In Me
        If child.TimesheetAdjustmentsID = TimesheetAdjustmentsID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Timesheet Adjustments"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROTimesheetAdjustmentList() As ROTimesheetAdjustmentList

      Return New ROTimesheetAdjustmentList()

    End Function

    Public Shared Sub BeginGetROTimesheetAdjustmentList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROTimesheetAdjustmentList)))

      Dim dp As New DataPortal(Of ROTimesheetAdjustmentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROTimesheetAdjustmentList(CallBack As EventHandler(Of DataPortalResult(Of ROTimesheetAdjustmentList)))

      Dim dp As New DataPortal(Of ROTimesheetAdjustmentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROTimesheetAdjustmentList() As ROTimesheetAdjustmentList

      Return DataPortal.Fetch(Of ROTimesheetAdjustmentList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTimesheetAdjustment.GetROTimesheetAdjustment(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTimesheetAdjustmentList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace