﻿' Generated 10 Mar 2015 17:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity.ReadOnly

  <Serializable()> _
  Public Class ROOBCityTimesheetSummaryList
    Inherits SingularReadOnlyListBase(Of ROOBCityTimesheetSummaryList, ROOBCityTimesheetSummary)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROOBCityTimesheetSummary

      For Each child As ROOBCityTimesheetSummary In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public HumanResourceID As Object = Nothing
      Public HumanResource As Object = Nothing
      Public ContractTypeID As Object = Nothing
      Public ContractType As Object = Nothing

      Public Shared TimesheetMonthIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TimesheetMonthID, "Timesheet Month", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Timesheet Month", Description:=""),
      Required(ErrorMessage:="Timesheet Month is required")>
      Public Property TimesheetMonthID() As Integer?
        Get
          Return ReadProperty(TimesheetMonthIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(TimesheetMonthIDProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", OBLib.Security.Settings.CurrentUser.SystemID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
      Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area is required")>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared CurrentUserHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CurrentUserHumanResourceID, "Current User Human Resource ID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Current User Human Resource ID is required")>
      Public Property CurrentUserHumanResourceID() As Integer?
        Get
          Return ReadProperty(CurrentUserHumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(CurrentUserHumanResourceIDProperty, Value)
        End Set
      End Property

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Keyword, "Keyword", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Keyword", Description:="")>
      Public Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Sub New(HumanResourceID As Object, HumanResource As Object,
                     ContractTypeID As Object, ContractType As Object)

        Me.HumanResourceID = HumanResourceID
        Me.HumanResource = HumanResource
        Me.ContractTypeID = ContractTypeID
        Me.ContractType = ContractType
        Me.Keyword = ""

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROOBCityTimesheetSummaryList() As ROOBCityTimesheetSummaryList

      Return New ROOBCityTimesheetSummaryList()

    End Function

    Public Shared Sub BeginGetROOBCityTimesheetSummaryList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROOBCityTimesheetSummaryList)))

      Dim dp As New DataPortal(Of ROOBCityTimesheetSummaryList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROOBCityTimesheetSummaryList(CallBack As EventHandler(Of DataPortalResult(Of ROOBCityTimesheetSummaryList)))

      Dim dp As New DataPortal(Of ROOBCityTimesheetSummaryList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROOBCityTimesheetSummaryList() As ROOBCityTimesheetSummaryList

      Return DataPortal.Fetch(Of ROOBCityTimesheetSummaryList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROOBCityTimesheetSummary.GetROOBCityTimesheetSummary(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROOBCityTimesheetSummaryList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@CurrentUserHumanResourceID", NothingDBNull(crit.CurrentUserHumanResourceID))
            cm.Parameters.AddWithValue("@Keyword", Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@TimesheetMonthID", crit.TimesheetMonthID)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace