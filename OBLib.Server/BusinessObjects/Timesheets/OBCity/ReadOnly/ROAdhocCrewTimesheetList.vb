﻿' Generated 10 Mar 2015 12:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity.ReadOnly

  <Serializable()> _
  Public Class ROAdhocCrewTimesheetList
    Inherits SingularReadOnlyListBase(Of ROAdhocCrewTimesheetList, ROAdhocCrewTimesheet)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(AdhocCrewTimesheetID As Integer) As ROAdhocCrewTimesheet

      For Each child As ROAdhocCrewTimesheet In Me
        If child.AdhocCrewTimesheetID = AdhocCrewTimesheetID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Adhoc Crew Timesheets"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
      <Display(Name:="Start Date", Description:="")>
      Public Property StartDate As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
      <Display(Name:="End Date", Description:="")>
      Public Property EndDate As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", OBLib.Security.Settings.CurrentUser.SystemID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
      Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area is required")>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared CurrentUserHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CurrentUserHumanResourceID, "Current User Human Resource ID", OBLib.Security.Settings.CurrentUser.HumanResourceID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Current User Human Resource ID is required")>
      Public Property CurrentUserHumanResourceID() As Integer?
        Get
          Return ReadProperty(CurrentUserHumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(CurrentUserHumanResourceIDProperty, Value)
        End Set
      End Property

      '@HumanResourceIDs XML = NULL,
      '@TimesheetMonthID Int = 37,
      '@CurrentUserHumanResourceID Int = 6,

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROAdhocCrewTimesheetList() As ROAdhocCrewTimesheetList

      Return New ROAdhocCrewTimesheetList()

    End Function

    Public Shared Sub BeginGetROAdhocCrewTimesheetList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROAdhocCrewTimesheetList)))

      Dim dp As New DataPortal(Of ROAdhocCrewTimesheetList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROAdhocCrewTimesheetList(CallBack As EventHandler(Of DataPortalResult(Of ROAdhocCrewTimesheetList)))

      Dim dp As New DataPortal(Of ROAdhocCrewTimesheetList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROAdhocCrewTimesheetList() As ROAdhocCrewTimesheetList

      Return DataPortal.Fetch(Of ROAdhocCrewTimesheetList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROAdhocCrewTimesheet.GetROAdhocCrewTimesheet(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROAdhocCrewTimesheetList"
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@CurrentUserHumanResourceID", NothingDBNull(crit.CurrentUserHumanResourceID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace