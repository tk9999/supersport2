﻿' Generated 07 Mar 2015 09:46 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity.ReadOnly

  <Serializable()> _
  Public Class ROTimesheetAdjustment
    Inherits SingularReadOnlyBase(Of ROTimesheetAdjustment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TimesheetAdjustmentsIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetAdjustmentsID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property TimesheetAdjustmentsID() As Integer
      Get
        Return GetProperty(TimesheetAdjustmentsIDProperty)
      End Get
    End Property

    Public Shared AdjustmentProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Adjustment, "Adjustment", 0)
    ''' <summary>
    ''' Gets the Adjustment value
    ''' </summary>
    <Display(Name:="Adjustment", Description:="The value by which the timesheet must be adjusted")>
    Public ReadOnly Property Adjustment() As Decimal
      Get
        Return GetProperty(AdjustmentProperty)
      End Get
    End Property

    Public Shared AdjustmentReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdjustmentReason, "Adjustment Reason", "")
    ''' <summary>
    ''' Gets the Adjustment Reason value
    ''' </summary>
    <Display(Name:="Adjustment Reason", Description:="The reason for the adjustment")>
    Public ReadOnly Property AdjustmentReason() As String
      Get
        Return GetProperty(AdjustmentReasonProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As SmartDate
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDate, "Modified Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDate() As SmartDate
      Get
        Return GetProperty(ModifiedDateProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="human resource")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared AdjustedTimesheetMonthIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdjustedTimesheetMonthID, "Adjusted Timesheet Month", Nothing)
    ''' <summary>
    ''' Gets the Adjusted Timesheet Month value
    ''' </summary>
    <Display(Name:="Adjusted Timesheet Month", Description:="month which is to be corrected")>
    Public ReadOnly Property AdjustedTimesheetMonthID() As Integer?
      Get
        Return GetProperty(AdjustedTimesheetMonthIDProperty)
      End Get
    End Property

    Public Shared TimesheetHourTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TimesheetHourTypeID, "Timesheet Hour Type", Nothing)
    ''' <summary>
    ''' Gets the Timesheet Hour Type value
    ''' </summary>
    <Display(Name:="Timesheet Hour Type", Description:="which section of timesheets should the adjustment be added to")>
    Public ReadOnly Property TimesheetHourTypeID() As Integer?
      Get
        Return GetProperty(TimesheetHourTypeIDProperty)
      End Get
    End Property

    Public Shared ReflectInTimesheetMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ReflectInTimesheetMonthID, "Reflect In Timesheet Month", 0)
    ''' <summary>
    ''' Gets the Reflect In Timesheet Month value
    ''' </summary>
    <Display(Name:="Reflect In Timesheet Month", Description:="timesheet month in which the adjustment must reflect")>
    Public ReadOnly Property ReflectInTimesheetMonthID() As Integer
      Get
        Return GetProperty(ReflectInTimesheetMonthIDProperty)
      End Get
    End Property

    Public Shared AuthorisedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisedByUserID, "Authorised By User", Nothing)
    ''' <summary>
    ''' Gets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised By User", Description:="")>
    Public ReadOnly Property AuthorisedByUserID() As Integer?
      Get
        Return GetProperty(AuthorisedByUserIDProperty)
      End Get
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, "Authorised Date Time")
    ''' <summary>
    ''' Gets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Authorised Date Time", Description:="")>
    Public ReadOnly Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TimesheetAdjustmentsIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AdjustmentReason

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROTimesheetAdjustment(dr As SafeDataReader) As ROTimesheetAdjustment

      Dim r As New ROTimesheetAdjustment()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TimesheetAdjustmentsIDProperty, .GetInt32(0))
        LoadProperty(AdjustmentProperty, .GetDecimal(1))
        LoadProperty(AdjustmentReasonProperty, .GetString(2))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateProperty, .GetSmartDate(6))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(AdjustedTimesheetMonthIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(TimesheetHourTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
        LoadProperty(ReflectInTimesheetMonthIDProperty, .GetInt32(10))
        LoadProperty(AuthorisedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(AuthorisedDateTimeProperty, .GetValue(12))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace