﻿' Generated 03 Mar 2015 09:10 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Timesheets.OBCity

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity

  <Serializable()> _
  Public Class OBCityTimesheet
    Inherits SingularBusinessBase(Of OBCityTimesheet)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No")
    ''' <summary>
    ''' Gets and sets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
    Public Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IDNoProperty, Value)
      End Set
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code")
    ''' <summary>
    ''' Gets and sets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="")>
    Public Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EmployeeCodeProperty, Value)
      End Set
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractTypeID, "Contract Type")
    ''' <summary>
    ''' Gets and sets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:=""),
    Required(ErrorMessage:="Contract Type required")>
    Public Property ContractTypeID() As Integer
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ContractTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type")
    ''' <summary>
    ''' Gets and sets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContractTypeProperty, Value)
      End Set
    End Property

    Public Shared TMStartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TMStartDate, "TM Start Date")
    ''' <summary>
    ''' Gets and sets the TM Start Date value
    ''' </summary>
    <Display(Name:="TM Start Date", Description:=""),
    Required(ErrorMessage:="TM Start Date required")>
    Public Property TMStartDate As DateTime?
      Get
        Return GetProperty(TMStartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TMStartDateProperty, Value)
      End Set
    End Property

    Public Shared TMEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TMEndDate, "TM End Date")
    ''' <summary>
    ''' Gets and sets the TM End Date value
    ''' </summary>
    <Display(Name:="TM End Date", Description:=""),
    Required(ErrorMessage:="TM End Date required")>
    Public Property TMEndDate As DateTime?
      Get
        Return GetProperty(TMEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TMEndDateProperty, Value)
      End Set
    End Property

    Public Shared TMHoursBeforeOvertimeProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TMHoursBeforeOvertime, "TM Hours Before Overtime")
    ''' <summary>
    ''' Gets and sets the TM Hours Before Overtime value
    ''' </summary>
    <Display(Name:="TM Hours Before Overtime", Description:=""),
    Required(ErrorMessage:="TM Hours Before Overtime required")>
    Public Property TMHoursBeforeOvertime() As Decimal
      Get
        Return GetProperty(TMHoursBeforeOvertimeProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TMHoursBeforeOvertimeProperty, Value)
      End Set
    End Property

    Public Shared TMClosedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TMClosedInd, "TM Closed", False)
    ''' <summary>
    ''' Gets and sets the TM Closed value
    ''' </summary>
    <Display(Name:="TM Closed", Description:=""),
    Required(ErrorMessage:="TM Closed required")>
    Public Property TMClosedInd() As Boolean
      Get
        Return GetProperty(TMClosedIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(TMClosedIndProperty, Value)
      End Set
    End Property

    Public Shared TMMonthProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TMMonth, "TM Month")
    ''' <summary>
    ''' Gets and sets the TM Month value
    ''' </summary>
    <Display(Name:="TM Month", Description:=""),
    Required(ErrorMessage:="TM Month required")>
    Public Property TMMonth() As Integer
      Get
        Return GetProperty(TMMonthProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TMMonthProperty, Value)
      End Set
    End Property

    Public Shared TMYearProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TMYear, "TM Year")
    ''' <summary>
    ''' Gets and sets the TM Year value
    ''' </summary>
    <Display(Name:="TM Year", Description:=""),
    Required(ErrorMessage:="TM Year required")>
    Public Property TMYear() As Integer
      Get
        Return GetProperty(TMYearProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TMYearProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required(ErrorMessage:="System required")>
    Public Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area")
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:=""),
    Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ShortfallMinHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShortfallMinHours, "TM Hours Before Overtime")
    ''' <summary>
    ''' Gets and sets the TM Hours Before Overtime value
    ''' </summary>
    <Display(Name:="Shortfall Min Hours", Description:="")>
    Public Property ShortfallMinHours() As Decimal
      Get
        Return GetProperty(ShortfallMinHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(ShortfallMinHoursProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Contract Type value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared CurrentUserIsHRProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CurrentUserIsHR, "Current User Is HR")
    ''' <summary>
    ''' Gets and sets the Contract Type value
    ''' </summary>
    <Display(Name:="Current User Is HR", Description:="")>
    Public Property CurrentUserIsHR() As Boolean
      Get
        Return GetProperty(CurrentUserIsHRProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CurrentUserIsHRProperty, Value)
      End Set
    End Property

    Public Shared CurrentUserIsManagerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CurrentUserIsManager, "Current User Is Manager")
    ''' <summary>
    ''' Gets and sets the Contract Type value
    ''' </summary>
    <Display(Name:="Current User Is Manager", Description:="")>
    Public Property CurrentUserIsManager() As Boolean
      Get
        Return GetProperty(CurrentUserIsManagerProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CurrentUserIsManagerProperty, Value)
      End Set
    End Property

    Public Shared TimesheetMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetMonthID, "Timesheet Month")
    ''' <summary>
    ''' Gets and sets the Contract Type value
    ''' </summary>
    <Display(Name:="Timesheet Month", Description:="")>
    Public Property TimesheetMonthID() As Integer
      Get
        Return GetProperty(TimesheetMonthIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TimesheetMonthIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared CrewTimesheetListProperty As PropertyInfo(Of CrewTimesheetList) = RegisterProperty(Of CrewTimesheetList)(Function(c) c.CrewTimesheetList, "Crew Timesheet List")
    Public ReadOnly Property CrewTimesheetList() As CrewTimesheetList
      Get
        If GetProperty(CrewTimesheetListProperty) Is Nothing Then
          LoadProperty(CrewTimesheetListProperty, OBLib.Timesheets.OBCity.CrewTimesheetList.NewCrewTimesheetList())
        End If
        Return GetProperty(CrewTimesheetListProperty)
      End Get
    End Property

    Public Shared TimesheetAdjustmentListProperty As PropertyInfo(Of TimesheetAdjustmentList) = RegisterProperty(Of TimesheetAdjustmentList)(Function(c) c.TimesheetAdjustmentList, "Timesheet Adjustment List")
    Public ReadOnly Property TimesheetAdjustmentList() As TimesheetAdjustmentList
      Get
        If GetProperty(TimesheetAdjustmentListProperty) Is Nothing Then
          LoadProperty(TimesheetAdjustmentListProperty, OBLib.Timesheets.OBCity.TimesheetAdjustmentList.NewTimesheetAdjustmentList())
        End If
        Return GetProperty(TimesheetAdjustmentListProperty)
      End Get
    End Property

    Public Shared TimesheetMealDetailListProperty As PropertyInfo(Of TimesheetMealDetailList) = RegisterProperty(Of TimesheetMealDetailList)(Function(c) c.TimesheetMealDetailList, "Timesheet Meal Detail List")
    Public ReadOnly Property TimesheetMealDetailList() As TimesheetMealDetailList
      Get
        If GetProperty(TimesheetMealDetailListProperty) Is Nothing Then
          LoadProperty(TimesheetMealDetailListProperty, Timesheets.OBCity.TimesheetMealDetailList.NewTimesheetMealDetailList())
        End If
        Return GetProperty(TimesheetMealDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "OB City Timesheet")
        Else
          Return String.Format("Blank {0}", "OB City Timesheet")
        End If
      Else
        Return Me.HumanResource
      End If

    End Function

    Public Sub DoCalculations()
      CrewTimesheetList.DoCalculations()
    End Sub

#End Region

#Region " Summary Properties "

#Region " Unadjusted Values "

    Public ReadOnly Property UnadjustedHours As Decimal
      Get
        Return GetHoursWorked()
      End Get
    End Property

    Public ReadOnly Property UnadjustedPlanningHours As Decimal
      Get
        Return GetPlanningHours()
      End Get
    End Property

    Public ReadOnly Property UnadjustedTotal As Decimal
      Get
        Return GetHoursAndPlanning()
      End Get
    End Property

    Public ReadOnly Property UnadjustedRequiredHours As Decimal
      Get
        Return TMHoursBeforeOvertime
      End Get
    End Property

    Public ReadOnly Property UnadjustedPublicHolidayHours As Decimal
      Get
        Return GetTotalPublicHolidayHours()
      End Get
    End Property

    Public ReadOnly Property UnadjustedPublicHolidayHoursAdjusted As Decimal
      Get
        Return GetAdjustedPHHours()
      End Get
    End Property

    Public ReadOnly Property UnadjustedOvertime As Decimal
      Get
        Dim CalculationType As CommonData.Enums.PublicHolidayCalculationType = Me.GetPublicHolidayHourCalculationType(TMStartDate)
        Select Case CalculationType
          Case CommonData.Enums.PublicHolidayCalculationType.Original Or CommonData.Enums.PublicHolidayCalculationType.UpToMidnight
            Return GetTotalOvertime()

          Case CommonData.Enums.PublicHolidayCalculationType.OvertimeDependent
            Return OvertimeLessPublicHolidaysOverLimit

        End Select
        'Return GetTotalOvertime()
      End Get
    End Property

    Public ReadOnly Property UnadjustedShortfall As Decimal
      Get
        Return GetTotalShortfall()
      End Get
    End Property

    Public ReadOnly Property UnadjustedTotalOvertimeAndShortfall As Decimal
      Get
        Return GetFinalOverTime()
      End Get
    End Property

#End Region

#Region " Adjustment Values "

    Public ReadOnly Property NormalHoursAdjustment As Decimal
      Get
        Return GetTotalNormalHourAdjustments()
      End Get
    End Property

    Public ReadOnly Property PlanningHoursAdjustment As Decimal
      Get
        Return GetTotalPlanningHourAdjustments()
      End Get
    End Property

    Public ReadOnly Property TotalAdjustment As Decimal
      Get
        Return 0 'GetHoursAndPlanning()
      End Get
    End Property

    Public ReadOnly Property RequiredHoursAdjustment As Decimal
      Get
        Return 0
      End Get
    End Property

    Public ReadOnly Property PublicHolidayHoursAdjustment As Decimal
      Get
        Return GetTotalPublicHolidayHourAdjustments()
      End Get
    End Property

    Public ReadOnly Property PublicHolidayHoursAdjustedAdjustment As Decimal
      Get
        Return GetTotalPublicHolidayHourAdjustments()
      End Get
    End Property

    Public ReadOnly Property OvertimeAdjustment As Decimal
      Get
        Return GetTotalOvertimeHourAdjustments()
      End Get
    End Property

    Public ReadOnly Property ShortfallAdjustment As Decimal
      Get
        Return GetTotalShortfallHourAdjustments()
      End Get
    End Property

    Public ReadOnly Property TotalOvertimeAndShortfallAdjustment As Decimal
      Get
        Return GetFinalOverTime()
      End Get
    End Property

#End Region

#Region " Final Values "

    Public ReadOnly Property FinalHours As Decimal
      Get
        Return LessThanZeroReturnZero(UnadjustedHours + (NormalHoursAdjustment))
      End Get
    End Property

    Public ReadOnly Property FinalPlanningHours As Decimal
      Get
        Return LessThanZeroReturnZero(UnadjustedPlanningHours + (PlanningHoursAdjustment))
      End Get
    End Property

    Public ReadOnly Property FinalTotal As Decimal
      Get
        Return LessThanZeroReturnZero(FinalHours)
        'FinalPlanningHours
      End Get
    End Property

    Public ReadOnly Property FinalRequiredHours As Decimal
      Get
        Return TMHoursBeforeOvertime
      End Get
    End Property

    Public ReadOnly Property FinalPublicHolidayHours As Decimal
      Get
        Return 0
      End Get
    End Property

    Public ReadOnly Property FinalPublicHolidayHoursAdjusted As Decimal
      Get
        Return LessThanZeroReturnZero(UnadjustedPublicHolidayHoursAdjusted + PublicHolidayHoursAdjustment)
      End Get
    End Property

    Public ReadOnly Property FinalOvertime As Decimal
      Get
        'x 1.5
        Dim CalculationType As CommonData.Enums.PublicHolidayCalculationType = Me.GetPublicHolidayHourCalculationType(TMStartDate)
        Select Case CalculationType
          Case CommonData.Enums.PublicHolidayCalculationType.Original Or CommonData.Enums.PublicHolidayCalculationType.UpToMidnight
            Return LessThanZeroReturnZero(UnadjustedOvertime + (OvertimeAdjustment))

          Case CommonData.Enums.PublicHolidayCalculationType.OvertimeDependent
            Return LessThanZeroReturnZero(UnadjustedOvertime + (OvertimeAdjustment)) '* (CommonData.Lists.TimesheetHourTypeList.GetItem(CommonData.Enums.TimesheetHourType.Overtime).Rate)

        End Select
      End Get
    End Property

    Public ReadOnly Property FinalShortfall As Decimal
      Get
        Return LessThanZeroReturnZero(UnadjustedShortfall + (ShortfallAdjustment))
      End Get
    End Property

    Public ReadOnly Property FinalTotalOvertimeAndShortfall As Decimal
      Get
        Return LessThanZeroReturnZero(FinalOvertime + FinalShortfall)
      End Get
    End Property

    Public ReadOnly Property FinalTotalSnTForMonth As Decimal
      Get
        Return LessThanZeroReturnZero(GetSnTForMonth())
      End Get
    End Property

    Public ReadOnly Property PublicHolidaysTotal As Decimal
      Get
        Return CalculateTotalPublicHolidayHours()
      End Get
    End Property

    'amount of public holiday hours worked before the overtime limit
    Public ReadOnly Property PublicHolidaysUnderLimit As Decimal
      Get
        Return CalculatePublicHolidaysUnderLimit()
      End Get
    End Property

    'amount of public holiday hours worked once you are into the overtime limit
    'this must be removed from other overtime as it gets paid at a different rate to normal overtime
    'this is paid at 2x rate
    Public ReadOnly Property PublicHolidaysOverLimit As Decimal
      Get
        Return CalculatePublicHolidaysOverLimit()
      End Get
    End Property

    'overtime remaining after the public holiday hours have been removed from total overtime
    'this remaining overtime is to be paid at 1.5x normal rate
    Public ReadOnly Property OvertimeLessPublicHolidaysOverLimit As Decimal
      Get
        Return GetTotalOvertime() - PublicHolidaysOverLimit
      End Get
    End Property

    Public ReadOnly Property FinalPublicHolidaysUnderLimit As Decimal
      Get
        'x 1
        Return CalculatePublicHolidaysUnderLimit() * (CommonData.Lists.ROTimesheetHourTypeList.GetItem(CInt(CommonData.Enums.TimesheetHourType.Normal)).Rate)
      End Get
    End Property

    Public ReadOnly Property FinalPublicHolidaysOverLimit As Decimal
      Get
        'x 2
        Return CalculatePublicHolidaysOverLimit() * (CommonData.Lists.ROTimesheetHourTypeList.GetItem(CInt(CommonData.Enums.TimesheetHourType.PublicHoliday)).Rate)
      End Get
    End Property

#End Region

#End Region

#Region " Summary Methods "

    Public Function GetHoursWorked() As Decimal

      Dim result As Decimal = 0
      result = Me.CrewTimesheetList.Where(Function(d) Not d.LastDayPreviousMonthInd).Sum(Function(item) item.HoursForDay)
      Return RoundToHalfHour(result)

    End Function

    Public Function GetTotalNormalHourAdjustments()

      Dim result As Decimal = 0
      result = Me.TimesheetAdjustmentList.Where(Function(tmt) tmt.TimesheetHourTypeID = CommonData.Enums.TimesheetHourType.Normal AndAlso (tmt.AuthorisedByUserID IsNot Nothing Or tmt.ReflectedInSameMonthInd)).Sum(Function(d) d.Adjustment)
      Return result

    End Function

    Public Function GetTotalOvertimeHourAdjustments()

      Dim result As Decimal = 0
      result = Me.TimesheetAdjustmentList.Where(Function(tmt) tmt.TimesheetHourTypeID = CommonData.Enums.TimesheetHourType.Overtime AndAlso (tmt.AuthorisedByUserID IsNot Nothing Or tmt.ReflectedInSameMonthInd)).Sum(Function(d) d.Adjustment)
      Return result

    End Function

    Public Function GetTotalPublicHolidayHourAdjustments()

      Dim result As Decimal = 0
      result = Me.TimesheetAdjustmentList.Where(Function(tmt) tmt.TimesheetHourTypeID = CommonData.Enums.TimesheetHourType.PublicHoliday AndAlso (tmt.AuthorisedByUserID IsNot Nothing Or tmt.ReflectedInSameMonthInd)).Sum(Function(d) d.Adjustment)
      Return result

    End Function

    Public Function GetTotalShortfallHourAdjustments()

      Dim result As Decimal = 0
      result = Me.TimesheetAdjustmentList.Where(Function(tmt) tmt.TimesheetHourTypeID = CommonData.Enums.TimesheetHourType.Shortfall AndAlso (tmt.AuthorisedByUserID IsNot Nothing Or tmt.ReflectedInSameMonthInd)).Sum(Function(d) d.Adjustment)
      Return result

    End Function

    Public Function GetTotalPlanningHourAdjustments()

      Dim result As Decimal = 0
      result = Me.TimesheetAdjustmentList.Where(Function(tmt) tmt.TimesheetHourTypeID = CommonData.Enums.TimesheetHourType.Planning AndAlso (tmt.AuthorisedByUserID IsNot Nothing Or tmt.ReflectedInSameMonthInd)).Sum(Function(d) d.Adjustment)
      Return result

    End Function

    Public Function GetPlanningHours() As Decimal

      Dim planHours As Decimal = 0
      planHours = GetPlanningLevelHours(TMStartDate)
      Return planHours

    End Function

    Public Function GetHoursAndPlanning() As Decimal

      Return RoundToHalfHour(GetTotalHours())
      ' + GetPlanningHours() + PlanningHoursAdjustment

    End Function

    Public Function GetTotalOvertime() As Decimal

      Dim hrsbot As Decimal = TMHoursBeforeOvertime
      Dim result As Decimal = 0
      result = GetHoursAndPlanning() - hrsbot
      If result < 0 Then
        result = 0
      End If
      Return RoundToHalfHour(result)

    End Function

    Public Function GetTotalShortfall() As Decimal

      Dim result As Decimal = 0
      result = Me.CrewTimesheetList.Where(Function(d) Not d.LastDayPreviousMonthInd).Sum(Function(item) item.ShortfallHours)
      Return RoundToHalfHour(result)

    End Function

    Public Function GetFinalOverTime() As Decimal

      Dim result As Decimal = 0
      result = GetTotalOvertime() + GetTotalShortfall()
      Return RoundToHalfHour(result)

    End Function

    Public Function GetTotalHours() As Decimal

      Dim result As Decimal = 0
      result = GetHoursWorked()
      Return RoundToHalfHour(result)

    End Function

    Public Function GetFinalTimeForMonth() As Decimal

      Dim result As Decimal = 0
      result = GetHoursAndPlanning() + GetTotalShortfall()
      Return RoundToHalfHour(result)

    End Function

    Public Function GetTotalPublicHolidayHours() As Decimal

      Dim sum As Decimal = 0
      sum = Me.CrewTimesheetList.Sum(Function(d) d.PublicHolidayHours)

      Return RoundToHalfHour(sum)

    End Function

    Public Function GetAdjustedPHHours() As Decimal

      Dim adjustedHrs As Decimal = 0
      Dim phHours As Decimal = GetTotalPublicHolidayHours()
      Dim totalOtime As Decimal = GetTotalOvertime()
      Dim CalculationType As CommonData.Enums.PublicHolidayCalculationType = Me.GetPublicHolidayHourCalculationType(TMStartDate)
      Select Case CalculationType
        Case CommonData.Enums.PublicHolidayCalculationType.Original
          Return phHours
        Case CommonData.Enums.PublicHolidayCalculationType.UpToMidnight
          Return AbsorbedPublicHolidayHours()
        Case CommonData.Enums.PublicHolidayCalculationType.OvertimeDependent
          Return 0
      End Select
      Return 0

    End Function

    Private Function AbsorbedPublicHolidayHours() As Decimal

      Dim adjustedHrs As Decimal = 0
      Dim phHours As Decimal = GetTotalPublicHolidayHours()
      Dim totalOtime As Decimal = GetTotalOvertime()

      'If there are no public holdiay hours
      If phHours = 0 Then
        adjustedHrs = 0

        'If there is no overtime, then public holiday hours stays unchanged
      ElseIf totalOtime = 0 Then
        adjustedHrs = phHours

        'If there are public holiday hours and there is overtime, we need to start decreasing public holiday time
      ElseIf phHours > 0 AndAlso totalOtime > 0 Then

        'If there are more public holiday hours than there is overtime, then decrease public holidays by itself
        If phHours >= totalOtime Then
          adjustedHrs = (phHours - totalOtime)

          'If there are less public holiday hours than there is overtime, decrease the public hours to 0
        ElseIf phHours < totalOtime Then
          adjustedHrs = 0
        End If

      End If

      Return adjustedHrs

    End Function

    Public Function GetSnTForMonth() As Decimal

      Dim result As Decimal = 0
      result = Me.CrewTimesheetList.Sum(Function(item) item.SnTForDay)
      Return RoundToHalfHour(result)

    End Function

    Private Function CalculateTotalPublicHolidayHours() As Decimal

      Return FinalPublicHolidaysUnderLimit + FinalPublicHolidaysOverLimit  'Me.Where(Function(CrewTimesheet) CrewTimesheet.IsPublicHoliday).Sum(Function(CrewTimesheet) CrewTimesheet.PublicHolidayHours)

    End Function

    Private Function CalculatePublicHolidaysUnderLimit() As Decimal

      Dim HoursUnderLimitExcludingSwitchOver As Decimal = Me.CrewTimesheetList.Where(Function(CrewTimesheet) CrewTimesheet.IsPublicHoliday And Not CrewTimesheet.IntoOvertimeInd And Not CrewTimesheet.SwitchOverPointInd).Sum(Function(CrewTimesheet) CrewTimesheet.PublicHolidayHours)
      Dim SwitchOverDay As CrewTimesheet = Me.CrewTimesheetList.Where(Function(CrewTimesheet) CrewTimesheet.IsPublicHoliday And CrewTimesheet.SwitchOverPointInd And Not (CrewTimesheet.IsLeave And CrewTimesheet.PublicHolidayHours = 0)).FirstOrDefault
      Dim SwitchOverDayTotal As Decimal = 0

      If SwitchOverDay IsNot Nothing Then
        SwitchOverDayTotal = (TMHoursBeforeOvertime - (SwitchOverDay.HoursForMonth - SwitchOverDay.HoursForDay))
      End If
      Dim TotalUnderLimit = HoursUnderLimitExcludingSwitchOver + SwitchOverDayTotal
      Return TotalUnderLimit

    End Function

    Private Function CalculatePublicHolidaysOverLimit() As Decimal

      Dim HoursOverLimitExcludingSwitchOver As Decimal = Me.CrewTimesheetList.Where(Function(CrewTimesheet) CrewTimesheet.IsPublicHoliday And CrewTimesheet.IntoOvertimeInd And Not CrewTimesheet.SwitchOverPointInd).Sum(Function(CrewTimesheet) CrewTimesheet.PublicHolidayHours)
      Dim SwitchOverDay As CrewTimesheet = Me.CrewTimesheetList.Where(Function(CrewTimesheet) CrewTimesheet.IsPublicHoliday And CrewTimesheet.SwitchOverPointInd And Not (CrewTimesheet.IsLeave And CrewTimesheet.PublicHolidayHours = 0)).FirstOrDefault
      Dim SwitchOverDayTotal As Decimal = 0

      If SwitchOverDay IsNot Nothing Then
        SwitchOverDayTotal = (SwitchOverDay.HoursForMonth - TMHoursBeforeOvertime)
      End If
      Dim TotalOverLimit = HoursOverLimitExcludingSwitchOver + SwitchOverDayTotal
      Return TotalOverLimit

    End Function

    Public Function LessThanZeroReturnZero(Value As Decimal) As Decimal

      If Value < 0 Then
        Return 0
      Else
        Return Value
      End If

    End Function

    Public Function RoundToHalfHour(ByVal Value As Decimal) As Decimal

      Dim result As Decimal = 0

      Dim hours As Decimal = CType(Value.ToString.Split(".").ElementAt(0), Decimal)
      Dim minutes As Decimal = Value - hours

      If (minutes > 0.5) Then
        result = hours + 1
      ElseIf (minutes > 0 And minutes < 0.5) Then
        result = hours + 0.5
      Else
        result = Value
      End If

      Return result

    End Function

    Public Function GetPublicHolidayHourCalculationType(EffectiveDate As Date) As CommonData.Enums.PublicHolidayCalculationType

      Dim AbsorbAndUntilMidnight As Date = CType(CommonData.Lists.ROCalculationChangedDateList.GetItem(CommonData.Enums.PublicHolidayCalculationType.UpToMidnight).EffectiveDate, Date)
      Dim OvertimeDependent As Date = CType(CommonData.Lists.ROCalculationChangedDateList.GetItem(CommonData.Enums.PublicHolidayCalculationType.OvertimeDependent).EffectiveDate, Date)

      If EffectiveDate >= AbsorbAndUntilMidnight And EffectiveDate < OvertimeDependent Then
        Return CommonData.Enums.PublicHolidayCalculationType.UpToMidnight

      ElseIf EffectiveDate >= OvertimeDependent Then
        Return CommonData.Enums.PublicHolidayCalculationType.OvertimeDependent

      End If

      Return CommonData.Enums.PublicHolidayCalculationType.Original

    End Function

    Public Function GetPlanningLevelHours(StartDate As DateTime?) As Decimal

      Dim result = 0
      Dim cProc As New Singular.CommandProc("[CmdProcs].[cmdGetPlanningHours]", _
                                      New String() {"@HumanResourceID", "@Month"}, _
                                      New Object() {NothingDBNull(HumanResourceID), NothingDBNull(StartDate)})
      cProc.CommandType = CommandType.StoredProcedure
      cProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
      cProc = cProc.Execute()

      If cProc.DataRow IsNot Nothing Then
        If Not IsNullNothing(cProc.DataRow(0)) Then
          result = cProc.DataRow(0)
        End If
      End If

      Return result

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewOBCityTimesheet() method.

    End Sub

    Public Shared Function NewOBCityTimesheet() As OBCityTimesheet

      Return DataPortal.CreateChild(Of OBCityTimesheet)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetOBCityTimesheet(dr As SafeDataReader) As OBCityTimesheet

      Dim o As New OBCityTimesheet()
      o.Fetch(dr)
      Return o

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceProperty, .GetString(0))
          LoadProperty(IDNoProperty, .GetString(1))
          LoadProperty(EmployeeCodeProperty, .GetString(2))
          LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ContractTypeProperty, .GetString(4))
          LoadProperty(TMStartDateProperty, .GetValue(5))
          LoadProperty(TMEndDateProperty, .GetValue(6))
          LoadProperty(TMHoursBeforeOvertimeProperty, .GetDecimal(7))
          LoadProperty(TMClosedIndProperty, .GetBoolean(8))
          LoadProperty(TMMonthProperty, .GetInt32(9))
          LoadProperty(TMYearProperty, .GetInt32(10))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(ShortfallMinHoursProperty, .GetDecimal(13))
          LoadProperty(HumanResourceIDProperty, ZeroNothing(.GetInt32(14)))
          LoadProperty(CurrentUserIsHRProperty, .GetBoolean(15))
          LoadProperty(CurrentUserIsManagerProperty, .GetBoolean(16))
          LoadProperty(TimesheetMonthIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insOBCityTimesheet"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updOBCityTimesheet"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResource As SqlParameter = .Parameters.Add("@HumanResource", SqlDbType.Int)
          paramHumanResource.Value = GetProperty(HumanResourceProperty)

          If Me.IsNew Then
            paramHumanResource.Direction = ParameterDirection.Output
          End If

          .Parameters.AddWithValue("@IDNo", GetProperty(IDNoProperty))
          .Parameters.AddWithValue("@EmployeeCode", GetProperty(EmployeeCodeProperty))
          .Parameters.AddWithValue("@ContractTypeID", GetProperty(ContractTypeIDProperty))
          .Parameters.AddWithValue("@ContractType", GetProperty(ContractTypeProperty))
          .Parameters.AddWithValue("@TMStartDate", (New SmartDate(GetProperty(TMStartDateProperty))).DBValue)
          .Parameters.AddWithValue("@TMEndDate", (New SmartDate(GetProperty(TMEndDateProperty))).DBValue)
          .Parameters.AddWithValue("@TMHoursBeforeOvertime", GetProperty(TMHoursBeforeOvertimeProperty))
          .Parameters.AddWithValue("@TMClosedInd", GetProperty(TMClosedIndProperty))
          .Parameters.AddWithValue("@TMMonth", GetProperty(TMMonthProperty))
          .Parameters.AddWithValue("@TMYear", GetProperty(TMYearProperty))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))

          '.ExecuteNonQuery()

          'If Me.IsNew Then
          '  LoadProperty(HumanResourceProperty, paramHumanResource.Value)
          'End If
          ' update child objects
          If GetProperty(CrewTimesheetListProperty) IsNot Nothing Then
            Me.CrewTimesheetList.Update()
          End If
          If GetProperty(TimesheetAdjustmentListProperty) IsNot Nothing Then
            Me.TimesheetAdjustmentList.Update()
          End If
          If GetProperty(TimesheetMealDetailListProperty) IsNot Nothing Then
            Me.TimesheetMealDetailList.Update()
          End If
          MarkOld()
        End With
      Else
        If GetProperty(CrewTimesheetListProperty) IsNot Nothing Then
          Me.CrewTimesheetList.Update()
        End If
        If GetProperty(TimesheetAdjustmentListProperty) IsNot Nothing Then
          Me.TimesheetAdjustmentList.Update()
        End If
        If GetProperty(TimesheetMealDetailListProperty) IsNot Nothing Then
          Me.TimesheetMealDetailList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delOBCityTimesheet"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResource", GetProperty(HumanResourceProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace