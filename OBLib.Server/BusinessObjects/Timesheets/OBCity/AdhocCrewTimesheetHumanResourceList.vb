﻿' Generated 10 Mar 2015 12:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity

  <Serializable()> _
  Public Class AdhocCrewTimesheetHumanResourceList
    Inherits SingularBusinessListBase(Of AdhocCrewTimesheetHumanResourceList, AdhocCrewTimesheetHumanResource)

#Region " Business Methods "

    Public Function GetItem(AdhocCrewTimesheetHumanResourceID As Integer) As AdhocCrewTimesheetHumanResource

      For Each child As AdhocCrewTimesheetHumanResource In Me
        If child.AdhocCrewTimesheetHumanResourceID = AdhocCrewTimesheetHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByHRID(HumanResourceID As Integer?) As AdhocCrewTimesheetHumanResource

      For Each child As AdhocCrewTimesheetHumanResource In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Adhoc Crew Timesheet Human Resources"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewAdhocCrewTimesheetHumanResourceList() As AdhocCrewTimesheetHumanResourceList

      Return New AdhocCrewTimesheetHumanResourceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AdhocCrewTimesheetHumanResource In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AdhocCrewTimesheetHumanResource In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region


    Public Sub AddNewAttendee(HumanResourceID As Integer?, HumanResource As String)
      Dim n As AdhocCrewTimesheetHumanResource = Me.AddNew
      n.HumanResourceID = HumanResourceID
      n.HumanResource = HumanResource
      n.CrewStartDateTime = n.GetParent.StartDateTime
      n.CrewEndDateTime = n.GetParent.EndDateTime
      Me.Add(n)
    End Sub

  End Class

End Namespace