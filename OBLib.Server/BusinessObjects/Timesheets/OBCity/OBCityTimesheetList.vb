﻿' Generated 03 Mar 2015 09:10 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Users.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Timesheets.OBCity

  <Serializable()>
  Public Class OBCityTimesheetList
    Inherits SingularBusinessListBase(Of OBCityTimesheetList, OBCityTimesheet)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As OBCityTimesheet

      For Each child As OBCityTimesheet In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits SingularCriteriaBase(Of Criteria)

      Public Shared MonthDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.MonthDate, Nothing)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Start Date", Description:=""),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property MonthDate As DateTime?
        Get
          Return ReadProperty(MonthDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(MonthDateProperty, Value)
        End Set
      End Property

      Public Shared TimesheetMonthIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.TimesheetMonthID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      '''       
      <Display(Name:="Timesheet Month", Description:=""),
       DropDownWeb(GetType(OBLib.Maintenance.Timesheets.ReadOnly.ROTimesheetMonthList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                   BeforeFetchJS:="OBCityTimesheetBO.setTimesheetMonthCriteriaBeforeRefresh",
                   PreFindJSFunction:="OBCityTimesheetBO.triggerTimesheetMonthAutoPopulate",
                   AfterFetchJS:="OBCityTimesheetBO.afterTimesheetMonthRefreshAjax",
                   OnItemSelectJSFunction:="OBCityTimesheetBO.onCriteriaTimesheetMonthSelected",
                   LookupMember:="TimesheetMonth", DisplayMember:="MonthYear", ValueMember:="TimesheetMonthID")>
      Public Property TimesheetMonthID() As Integer?
        Get
          Return ReadProperty(TimesheetMonthIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(TimesheetMonthIDProperty, Value)
        End Set
      End Property

      Public Shared TimesheetMonthProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetMonth, "Month", "")
      ''' <summary>
      ''' Gets and sets the Human Resource value
      ''' </summary>
      <Display(Name:="Month", Description:="")>
      Public Property TimesheetMonth() As String
        Get
          Return ReadProperty(TimesheetMonthProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(TimesheetMonthProperty, Value)
        End Set
      End Property

      Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.UserID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="User", Description:="")>
      Public Property UserID() As Integer?
        Get
          Return ReadProperty(UserIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(UserIDProperty, Value)
        End Set
      End Property

      Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HumanResourceID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Staff Member", Description:=""),
      DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROUserTimesheetHumanResourceList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  BeforeFetchJS:="OBCityTimesheetBO.setUserCriteriaBeforeRefresh",
                  PreFindJSFunction:="OBCityTimesheetBO.triggerAutoPopulate",
                  AfterFetchJS:="OBCityTimesheetBO.afterUserRefreshAjax",
                  OnItemSelectJSFunction:="OBCityTimesheetBO.onUserSelected",
                  LookupMember:="HumanResource", DisplayMember:="FullName", ValueMember:="HumanResourceID"),
      SetExpression("OBCityTimesheetBO.CriteriaHumanResourceIDSet(self)")>
      Public Property HumanResourceID() As Integer?
        Get
          Return ReadProperty(HumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(HumanResourceIDProperty, Value)
        End Set
      End Property

      Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
      ''' <summary>
      ''' Gets and sets the Human Resource value
      ''' </summary>
      <Display(Name:="Human Resource", Description:="")>
      Public Property HumanResource() As String
        Get
          Return ReadProperty(HumanResourceProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(HumanResourceProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SystemID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:="")>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:="")>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Manager", Description:="")>
      Public Property ManagerHumanResourceID() As Integer?
        Get
          Return ReadProperty(ManagerHumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ManagerHumanResourceIDProperty, Value)
        End Set
      End Property

      Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ContractTypeID, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Contract Type", Description:="")>
      Public Property ContractTypeID() As Integer?
        Get
          Return ReadProperty(ContractTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ContractTypeIDProperty, Value)
        End Set
      End Property

      'Public Shared OtherTimesheetHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OtherTimesheetHumanResourceID, "Staff Member", Nothing)
      ' ''' <summary>
      ' ''' Gets and sets the Human Resource value
      ' ''' </summary>
      '<Display(Name:="Staff Member"),
      'DropDownWeb(GetType(ROUserManagedHumanResourceList), DisplayMember:="HumanResourceName", ValueMember:="HumanResourceID"),
      'SetExpression("UserProfilePage.OtherTimesheetHumanResourceIDSet(self)")>
      'Public Property OtherTimesheetHumanResourceID() As Integer?
      '  Get
      '    Return GetProperty(OtherTimesheetHumanResourceIDProperty)
      '  End Get
      '  Set(ByVal Value As Integer?)
      '    SetProperty(OtherTimesheetHumanResourceIDProperty, Value)
      '  End Set
      'End Property

      Public Sub New(HumanResourceID As Object, TimesheetMonthID As Integer?,
                     SystemID As Integer?, ProductionAreaID As Integer?,
                     ManagerHumanResourceID As Integer?,
                     ContractTypeID As Integer?)

        Me.HumanResourceID = HumanResourceID
        Me.TimesheetMonthID = TimesheetMonthID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.ManagerHumanResourceID = ManagerHumanResourceID
        Me.ContractTypeID = ContractTypeID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewOBCityTimesheetList() As OBCityTimesheetList

      Return New OBCityTimesheetList()

    End Function

    Public Shared Sub BeginGetOBCityTimesheetList(CallBack As EventHandler(Of DataPortalResult(Of OBCityTimesheetList)))

      Dim dp As New DataPortal(Of OBCityTimesheetList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetOBCityTimesheetList() As OBCityTimesheetList

      Return DataPortal.Fetch(Of OBCityTimesheetList)(New Criteria())

    End Function

    Public Shared Function GetOBCityTimesheetList(HumanResourceID As Integer?, TimesheetMonthID As Integer?,
                                                  SystemID As Integer, ProductionAreaID As Integer?,
                                                  ManagerHumanResourceID As Integer?,
                                                  ContractTypeID As Integer?) As OBCityTimesheetList

      Return DataPortal.Fetch(Of OBCityTimesheetList)(New Criteria(HumanResourceID, TimesheetMonthID, SystemID, ProductionAreaID, ManagerHumanResourceID, ContractTypeID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(OBCityTimesheet.GetOBCityTimesheet(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As OBCityTimesheet = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.CrewTimesheetList.RaiseListChangedEvents = False
          parent.CrewTimesheetList.Add(CrewTimesheet.GetCrewTimesheet(sdr))
          parent.CrewTimesheetList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(7) Then
            parent = Me.GetItem(sdr.GetInt32(7))
          End If
          If parent IsNot Nothing Then
            parent.TimesheetAdjustmentList.RaiseListChangedEvents = False
            parent.TimesheetAdjustmentList.Add(TimesheetAdjustment.GetTimesheetAdjustment(sdr))
            parent.TimesheetAdjustmentList.RaiseListChangedEvents = True
          End If
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          If parent IsNot Nothing Then
            parent.TimesheetMealDetailList.RaiseListChangedEvents = False
            parent.TimesheetMealDetailList.Add(TimesheetMealDetail.GetTimesheetMealDetail(sdr))
            parent.TimesheetMealDetailList.RaiseListChangedEvents = True
          End If
        End While
      End If

      For Each child As OBCityTimesheet In Me
        child.CheckRules()
        For Each CrewTimesheet As CrewTimesheet In child.CrewTimesheetList
          CrewTimesheet.CheckRules()
        Next
        For Each TimesheetAdjustment As TimesheetAdjustment In child.TimesheetAdjustmentList
          TimesheetAdjustment.CheckRules()
        Next
        For Each TimesheetMealDetail As TimesheetMealDetail In child.TimesheetMealDetailList
          TimesheetMealDetail.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getOBCityTimesheetList"
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@TimesheetMonthID", NothingDBNull(crit.TimesheetMonthID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@CurrentUserHumanResourceID", NothingDBNull(OBLib.Security.Settings.CurrentUser.HumanResourceID))
            cm.Parameters.AddWithValue("@ManagerHumanResourceID", NothingDBNull(crit.ManagerHumanResourceID))
            cm.Parameters.AddWithValue("@ContractTypeID", NothingDBNull(crit.ContractTypeID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As OBCityTimesheet In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As OBCityTimesheet In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace