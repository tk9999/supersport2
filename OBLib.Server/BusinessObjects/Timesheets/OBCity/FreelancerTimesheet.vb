﻿' Generated 19 May 2014 16:30 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets

  <Serializable()> _
  Public Class FreelancerTimesheet
    Inherits SingularBusinessBase(Of FreelancerTimesheet)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CrewTimesheetIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewTimesheetID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CrewTimesheetID() As Integer?
      Get
        Return GetProperty(CrewTimesheetIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared AdhocCrewTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdhocCrewTimesheetID, "Adhoc Crew Timesheet", Nothing)
    ''' <summary>
    ''' Gets the Adhoc Crew Timesheet value
    ''' </summary>
    <Display(Name:="Adhoc Crew Timesheet", Description:="")>
    Public ReadOnly Property AdhocCrewTimesheetID() As Integer?
      Get
        Return GetProperty(AdhocCrewTimesheetIDProperty)
      End Get
    End Property

    Public Shared CalculatedStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CalculatedStartDateTime, "Calculated Start Date Time")
    ''' <summary>
    ''' Gets the Calculated Start Date Time value
    ''' </summary>
    <Display(Name:="Calculated Start Date Time", Description:="")>
    Public ReadOnly Property CalculatedStartDateTime As DateTime?
      Get
        Return GetProperty(CalculatedStartDateTimeProperty)
      End Get
    End Property

    Public Shared CalculatedEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CalculatedEndDateTime, "Calculated End Date Time")
    ''' <summary>
    ''' Gets the Calculated End Date Time value
    ''' </summary>
    <Display(Name:="Calculated End Date Time", Description:="")>
    Public ReadOnly Property CalculatedEndDateTime As DateTime?
      Get
        Return GetProperty(CalculatedEndDateTimeProperty)
      End Get
    End Property

    Public Shared CrewStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewStartDateTime, "Crew Start Date Time")
    ''' <summary>
    ''' Gets the Crew Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:="")>
    Public ReadOnly Property CrewStartDateTime As DateTime?
      Get
        Return GetProperty(CrewStartDateTimeProperty)
      End Get
    End Property

    '<Singular.DataAnnotations.TimeField(TimeFormat:=TimeFormats.Custom, CustomTimeFormat:="HH:mm")>
    Public ReadOnly Property StartTime As String
      Get
        If CrewStartDateTime IsNot Nothing Then
          Return CrewStartDateTime.Value.ToString("HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared CrewEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewEndDateTime, "Crew End Date Time")
    ''' <summary>
    ''' Gets the Crew End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public ReadOnly Property CrewEndDateTime As DateTime?
      Get
        Return GetProperty(CrewEndDateTimeProperty)
      End Get
    End Property

    '<Singular.DataAnnotations.TimeField(TimeFormat:=TimeFormats.Custom, CustomTimeFormat:="HH:mm")>
    Public ReadOnly Property EndTime As String
      Get
        If CrewEndDateTime IsNot Nothing Then
          Return CrewEndDateTime.Value.ToString("HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared ManagerStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ManagerStartDateTime, "Manager Start Date Time")
    ''' <summary>
    ''' Gets the Manager Start Date Time value
    ''' </summary>
    <Display(Name:="Manager Start Date Time", Description:="")>
    Public ReadOnly Property ManagerStartDateTime As DateTime?
      Get
        Return GetProperty(ManagerStartDateTimeProperty)
      End Get
    End Property

    Public Shared ManagerEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ManagerEndDateTime, "Manager End Date Time")
    ''' <summary>
    ''' Gets the Manager End Date Time value
    ''' </summary>
    <Display(Name:="Manager End Date Time", Description:="")>
    Public ReadOnly Property ManagerEndDateTime As DateTime?
      Get
        Return GetProperty(ManagerEndDateTimeProperty)
      End Get
    End Property

    Public Shared OvertimeHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.OvertimeHours, "Overtime Hours")
    ''' <summary>
    ''' Gets the Overtime Hours value
    ''' </summary>
    <Display(Name:="Additional Hrs", Description:="")>
    Public ReadOnly Property OvertimeHours() As Decimal
      Get
        Return GetProperty(OvertimeHoursProperty)
      End Get
    End Property

    Public Shared ShortfallHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShortfallHours, "Shortfall Hours")
    ''' <summary>
    ''' Gets the Shortfall Hours value
    ''' </summary>
    <Display(Name:="Shortfall Hours", Description:="")>
    Public ReadOnly Property ShortfallHours() As Decimal
      Get
        Return GetProperty(ShortfallHoursProperty)
      End Get
    End Property

    Public Shared CrewChangeDetailsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CrewChangeDetails, "Crew Change Details")
    ''' <summary>
    ''' Gets the Crew Change Details value
    ''' </summary>
    <Display(Name:="Crew Change Details", Description:=""),
    StringLength(50, ErrorMessage:="Crew Change Details cannot be more than 50 characters")>
    Public ReadOnly Property CrewChangeDetails() As String
      Get
        Return GetProperty(CrewChangeDetailsProperty)
      End Get
    End Property

    Public Shared ManagerChangeDetailsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerChangeDetails, "Manager Change Details")
    ''' <summary>
    ''' Gets the Manager Change Details value
    ''' </summary>
    <Display(Name:="Manager Change Details", Description:=""),
    StringLength(50, ErrorMessage:="Manager Change Details cannot be more than 50 characters")>
    Public ReadOnly Property ManagerChangeDetails() As String
      Get
        Return GetProperty(ManagerChangeDetailsProperty)
      End Get
    End Property

    Public Shared CrewCheckedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewCheckedDate, "Crew Checked Date")
    ''' <summary>
    ''' Gets the Crew Checked Date value
    ''' </summary>
    <Display(Name:="Crew Checked Date", Description:="")>
    Public ReadOnly Property CrewCheckedDate As DateTime?
      Get
        Return GetProperty(CrewCheckedDateProperty)
      End Get
    End Property

    Public Shared AuthorisedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisedByUserID, "Authorised By User", Nothing)
    ''' <summary>
    ''' Gets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised By User", Description:="")>
    Public ReadOnly Property AuthorisedByUserID() As Integer?
      Get
        Return GetProperty(AuthorisedByUserIDProperty)
      End Get
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, "Authorised Date Time")
    ''' <summary>
    ''' Gets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Authorised Date Time", Description:="")>
    Public ReadOnly Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
    End Property

    Public Shared AccountingDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AccountingDate, "Accounting Date")
    ''' <summary>
    ''' Gets the Accounting Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public ReadOnly Property AccountingDate As DateTime?
      Get
        Return GetProperty(AccountingDateProperty)
      End Get
    End Property

    Public Shared AccumulateHoursIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AccumulateHoursInd, "Accumulate Hours", False)
    ''' <summary>
    ''' Gets the Accumulate Hours value
    ''' </summary>
    <Display(Name:="Accumulate Hours", Description:="")>
    Public ReadOnly Property AccumulateHoursInd() As Boolean
      Get
        Return GetProperty(AccumulateHoursIndProperty)
      End Get
    End Property

    Public Shared ActualHoursIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ActualHoursInd, "Actual Hours", False)
    ''' <summary>
    ''' Gets the Actual Hours value
    ''' </summary>
    <Display(Name:="Actual Hours", Description:="")>
    Public ReadOnly Property ActualHoursInd() As Boolean
      Get
        Return GetProperty(ActualHoursIndProperty)
      End Get
    End Property

    Public Shared OvertimeCalcIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OvertimeCalcInd, "Overtime Calc", False)
    ''' <summary>
    ''' Gets the Overtime Calc value
    ''' </summary>
    <Display(Name:="Overtime Calc", Description:="")>
    Public ReadOnly Property OvertimeCalcInd() As Boolean
      Get
        Return GetProperty(OvertimeCalcIndProperty)
      End Get
    End Property

    Public Shared ShortfallCalcIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShortfallCalcInd, "Shortfall Calc", False)
    ''' <summary>
    ''' Gets the Shortfall Calc value
    ''' </summary>
    <Display(Name:="Shortfall Calc", Description:="")>
    Public ReadOnly Property ShortfallCalcInd() As Boolean
      Get
        Return GetProperty(ShortfallCalcIndProperty)
      End Get
    End Property

    Public Shared TimesheetCategoryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TimesheetCategoryID, "Timesheet Category", Nothing)
    ''' <summary>
    ''' Gets the Timesheet Category value
    ''' </summary>
    <Display(Name:="Timesheet Category", Description:="")>
    Public ReadOnly Property TimesheetCategoryID() As Integer?
      Get
        Return GetProperty(TimesheetCategoryIDProperty)
      End Get
    End Property

    Public Shared ManualOverrideIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ManualOverrideInd, "Manual Override", False)
    ''' <summary>
    ''' Gets the Manual Override value
    ''' </summary>
    <Display(Name:="Manual Override", Description:="")>
    Public ReadOnly Property ManualOverrideInd() As Boolean
      Get
        Return GetProperty(ManualOverrideIndProperty)
      End Get
    End Property

    Public Shared OverridenByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OverridenByUserID, "Overriden By User", Nothing)
    ''' <summary>
    ''' Gets the Overriden By User value
    ''' </summary>
    <Display(Name:="Overriden By User", Description:="")>
    Public ReadOnly Property OverridenByUserID() As Integer?
      Get
        Return GetProperty(OverridenByUserIDProperty)
      End Get
    End Property

    Public Shared OverridenDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OverridenDateTime, "Overriden Date Time")
    ''' <summary>
    ''' Gets the Overriden Date Time value
    ''' </summary>
    <Display(Name:="Overriden Date Time", Description:="")>
    Public ReadOnly Property OverridenDateTime As DateTime?
      Get
        Return GetProperty(OverridenDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared TimesheetMonthIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TimesheetMonthID, "Timesheet Month", Nothing)
    ''' <summary>
    ''' Gets the Timesheet Month value
    ''' </summary>
    <Display(Name:="Timesheet Month", Description:="")>
    Public ReadOnly Property TimesheetMonthID() As Integer?
      Get
        Return GetProperty(TimesheetMonthIDProperty)
      End Get
    End Property

    Public Shared PublicHolidayIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PublicHolidayInd, "Public Holiday?", False)
    ''' <summary>
    ''' Gets the Public Holiday value
    ''' </summary>
    <Display(Name:="Public Holiday", Description:="")>
    Public ReadOnly Property PublicHolidayInd() As Boolean
      Get
        Return GetProperty(PublicHolidayIndProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:=""),
    StringLength(50, ErrorMessage:="Description cannot be more than 50 characters")>
    Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared HoursForDayProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.HoursForDay, "Awarded Hours")
    ''' <summary>
    ''' Gets the Hours For Day value
    ''' </summary>
    <Display(Name:="Awarded Hours", Description:="")>
    Public ReadOnly Property HoursForDay() As Decimal
      Get
        Return GetProperty(HoursForDayProperty)
      End Get
    End Property

    Public Shared RequiredHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RequiredHours, "Hours Required", 10)
    ''' <summary>
    ''' Gets the Overtime value
    ''' </summary>
    <Display(Name:="Required Hours", Description:="")>
    Public ReadOnly Property RequiredHours() As Decimal
      Get
        Return GetProperty(RequiredHoursProperty)
      End Get
    End Property

    Public Shared OvertimeProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Overtime, "Additional Hours")
    ''' <summary>
    ''' Gets the Overtime value
    ''' </summary>
    <Display(Name:="Additional Hours", Description:="")>
    Public ReadOnly Property Overtime() As Decimal
      Get
        Return GetProperty(OvertimeProperty)
      End Get
    End Property

    Public Shared SnTProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.SnT, "S&T")
    ''' <summary>
    ''' Gets the Sn T value
    ''' </summary>
    <Display(Name:="S&T", Description:="")>
    Public ReadOnly Property SnT() As Decimal
      Get
        Return GetProperty(SnTProperty)
      End Get
    End Property

    Public Shared TimesheetCategoryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetCategory, "TimesheetCategory")
    ''' <summary>
    ''' Gets the Manager Change Details value
    ''' </summary>
    <Display(Name:="Category", Description:="")>
    Public ReadOnly Property TimesheetCategory() As String
      Get
        Return GetProperty(TimesheetCategoryProperty)
      End Get
    End Property

    Public Shared DayOfWeekProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DayOfWeek, "Day Of Week")
    ''' <summary>
    ''' Gets the Manager Change Details value
    ''' </summary>
    <Display(Name:="Day", Description:="")>
    Public ReadOnly Property DayOfWeek() As String
      Get
        Return GetProperty(DayOfWeekProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewTimesheetIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CrewChangeDetails.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Freelancer Timesheet")
        Else
          Return String.Format("Blank {0}", "Freelancer Timesheet")
        End If
      Else
        Return Me.CrewChangeDetails
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewFreelancerTimesheet() method.

    End Sub

    Public Shared Function NewFreelancerTimesheet() As FreelancerTimesheet

      Return DataPortal.CreateChild(Of FreelancerTimesheet)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetFreelancerTimesheet(dr As SafeDataReader) As FreelancerTimesheet

      Dim f As New FreelancerTimesheet()
      f.Fetch(dr)
      Return f

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CrewTimesheetIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AdhocCrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CalculatedStartDateTimeProperty, .GetValue(3))
          LoadProperty(CalculatedEndDateTimeProperty, .GetValue(4))
          LoadProperty(CrewStartDateTimeProperty, .GetValue(5))
          LoadProperty(CrewEndDateTimeProperty, .GetValue(6))
          LoadProperty(ManagerStartDateTimeProperty, .GetValue(7))
          LoadProperty(ManagerEndDateTimeProperty, .GetValue(8))
          LoadProperty(OvertimeHoursProperty, .GetDecimal(9))
          LoadProperty(ShortfallHoursProperty, .GetDecimal(10))
          LoadProperty(CrewChangeDetailsProperty, .GetString(11))
          LoadProperty(ManagerChangeDetailsProperty, .GetString(12))
          LoadProperty(CrewCheckedDateProperty, .GetValue(13))
          LoadProperty(AuthorisedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(AuthorisedDateTimeProperty, .GetValue(15))
          LoadProperty(AccountingDateProperty, .GetValue(16))
          LoadProperty(AccumulateHoursIndProperty, .GetBoolean(17))
          LoadProperty(ActualHoursIndProperty, .GetBoolean(18))
          LoadProperty(OvertimeCalcIndProperty, .GetBoolean(19))
          LoadProperty(ShortfallCalcIndProperty, .GetBoolean(20))
          LoadProperty(TimesheetCategoryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
          LoadProperty(ManualOverrideIndProperty, .GetBoolean(22))
          LoadProperty(OverridenByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
          LoadProperty(OverridenDateTimeProperty, .GetValue(24))
          LoadProperty(CreatedByProperty, .GetInt32(25))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(26))
          LoadProperty(ModifiedByProperty, .GetInt32(27))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(28))
          LoadProperty(TimesheetMonthIDProperty, Singular.Misc.ZeroNothing(.GetInt32(29)))
          LoadProperty(PublicHolidayIndProperty, .GetBoolean(30))
          LoadProperty(DescriptionProperty, .GetString(31))
          LoadProperty(HoursForDayProperty, .GetDecimal(32))
          LoadProperty(OvertimeProperty, .GetDecimal(33))
          LoadProperty(SnTProperty, .GetDecimal(34))
          'timesheet category 35
          LoadProperty(TimesheetCategoryProperty, .GetString(35))
          'invoice detail id 36
          '
          LoadProperty(DayOfWeekProperty, .GetString(37))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insFreelancerTimesheet"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updFreelancerTimesheet"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCrewTimesheetID As SqlParameter = .Parameters.Add("@CrewTimesheetID", SqlDbType.Int)
          paramCrewTimesheetID.Value = GetProperty(CrewTimesheetIDProperty)
          If Me.IsNew Then
            paramCrewTimesheetID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@AdhocCrewTimesheetID", GetProperty(AdhocCrewTimesheetIDProperty))
          cm.Parameters.AddWithValue("@CalculatedStartDateTime", (New SmartDate(GetProperty(CalculatedStartDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@CalculatedEndDateTime", (New SmartDate(GetProperty(CalculatedEndDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@CrewStartDateTime", (New SmartDate(GetProperty(CrewStartDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@CrewEndDateTime", (New SmartDate(GetProperty(CrewEndDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@ManagerStartDateTime", (New SmartDate(GetProperty(ManagerStartDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@ManagerEndDateTime", (New SmartDate(GetProperty(ManagerEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@OvertimeHours", GetProperty(OvertimeHoursProperty))
          .Parameters.AddWithValue("@ShortfallHours", GetProperty(ShortfallHoursProperty))
          .Parameters.AddWithValue("@CrewChangeDetails", GetProperty(CrewChangeDetailsProperty))
          .Parameters.AddWithValue("@ManagerChangeDetails", GetProperty(ManagerChangeDetailsProperty))
          cm.Parameters.AddWithValue("@CrewCheckedDate", (New SmartDate(GetProperty(CrewCheckedDateProperty))).DBValue)
          .Parameters.AddWithValue("@AuthorisedByUserID", GetProperty(AuthorisedByUserIDProperty))
          cm.Parameters.AddWithValue("@AuthorisedDateTime", (New SmartDate(GetProperty(AuthorisedDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@AccountingDate", (New SmartDate(GetProperty(AccountingDateProperty))).DBValue)
          .Parameters.AddWithValue("@AccumulateHoursInd", GetProperty(AccumulateHoursIndProperty))
          .Parameters.AddWithValue("@ActualHoursInd", GetProperty(ActualHoursIndProperty))
          .Parameters.AddWithValue("@OvertimeCalcInd", GetProperty(OvertimeCalcIndProperty))
          .Parameters.AddWithValue("@ShortfallCalcInd", GetProperty(ShortfallCalcIndProperty))
          .Parameters.AddWithValue("@TimesheetCategoryID", GetProperty(TimesheetCategoryIDProperty))
          .Parameters.AddWithValue("@ManualOverrideInd", GetProperty(ManualOverrideIndProperty))
          .Parameters.AddWithValue("@OverridenByUserID", GetProperty(OverridenByUserIDProperty))
          cm.Parameters.AddWithValue("@OverridenDateTime", (New SmartDate(GetProperty(OverridenDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@TimesheetMonthID", GetProperty(TimesheetMonthIDProperty))
          .Parameters.AddWithValue("@PublicHolidayInd", GetProperty(PublicHolidayIndProperty))
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          .Parameters.AddWithValue("@HoursForDay", GetProperty(HoursForDayProperty))
          .Parameters.AddWithValue("@Overtime", GetProperty(OvertimeProperty))
          .Parameters.AddWithValue("@SnT", GetProperty(SnTProperty))
          .Parameters.AddWithValue("@DayOfWeek", GetProperty(DayOfWeekProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CrewTimesheetIDProperty, paramCrewTimesheetID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delFreelancerTimesheet"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CrewTimesheetID", GetProperty(CrewTimesheetIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace