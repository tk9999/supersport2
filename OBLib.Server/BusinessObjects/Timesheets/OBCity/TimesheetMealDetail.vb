﻿' Generated 20 Nov 2015 07:07 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Timesheets.OBCity

  <Serializable()> _
  Public Class TimesheetMealDetail
    Inherits OBBusinessBase(Of TimesheetMealDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared ShiftStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ShiftStart, "Shift Start")
    ''' <summary>
    ''' Gets and sets the Shift Start value
    ''' </summary>
    <Display(Name:="Shift Start", Description:=""),
    Required(ErrorMessage:="Shift Start required")>
  Public Property ShiftStart As DateTime?
      Get
        Return GetProperty(ShiftStartProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ShiftStartProperty, Value)
      End Set
    End Property

    Public Shared ShiftEndProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ShiftEnd, "Shift End")
    ''' <summary>
    ''' Gets and sets the Shift End value
    ''' </summary>
    <Display(Name:="Shift End", Description:=""),
    Required(ErrorMessage:="Shift End required")>
  Public Property ShiftEnd As DateTime?
      Get
        Return GetProperty(ShiftEndProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ShiftEndProperty, Value)
      End Set
    End Property

    Public Shared MealDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.MealDate, "Meal Date")
    ''' <summary>
    ''' Gets and sets the Meal Date value
    ''' </summary>
    <Display(Name:="Meal Date", Description:=""),
    Required(ErrorMessage:="Meal Date required")>
  Public Property MealDate As DateTime?
      Get
        Return GetProperty(MealDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(MealDateProperty, Value)
      End Set
    End Property

    Public Shared MealProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Meal, "Meal")
    ''' <summary>
    ''' Gets and sets the Meal value
    ''' </summary>
    <Display(Name:="Meal", Description:="")>
  Public Property Meal() As String
      Get
        Return GetProperty(MealProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MealProperty, Value)
      End Set
    End Property

    Public Shared ReimbursementProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Reimbursement, "Reimbursement")
    ''' <summary>
    ''' Gets and sets the Reimbursement value
    ''' </summary>
    <Display(Name:="Reimbursement", Description:=""),
    Required(ErrorMessage:="Reimbursement required")>
  Public Property Reimbursement() As Decimal
      Get
        Return GetProperty(ReimbursementProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(ReimbursementProperty, Value)
      End Set
    End Property

    Public Shared NotQualifiesReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotQualifiesReason, "Not Qualifies Reason")
    ''' <summary>
    ''' Gets and sets the Not Qualifies Reason value
    ''' </summary>
    <Display(Name:="Not Qualifies Reason", Description:="")>
  Public Property NotQualifiesReason() As String
      Get
        Return GetProperty(NotQualifiesReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotQualifiesReasonProperty, Value)
      End Set
    End Property

    Public Shared ShiftDurationProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShiftDuration, "Shift Duration")
    ''' <summary>
    ''' Gets and sets the Shift Duration value
    ''' </summary>
    <Display(Name:="Shift Duration", Description:=""),
    Required(ErrorMessage:="Shift Duration required")>
  Public Property ShiftDuration() As Decimal
      Get
        Return GetProperty(ShiftDurationProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(ShiftDurationProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As OBCityTimesheet

      Return CType(CType(Me.Parent, TimesheetMealDetailList).Parent, OBCityTimesheet)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Meal.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Timesheet Meal Detail")
        Else
          Return String.Format("Blank {0}", "Timesheet Meal Detail")
        End If
      Else
        Return Me.Meal
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTimesheetMealDetail() method.

    End Sub

    Public Shared Function NewTimesheetMealDetail() As TimesheetMealDetail

      Return DataPortal.CreateChild(Of TimesheetMealDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTimesheetMealDetail(dr As SafeDataReader) As TimesheetMealDetail

      Dim t As New TimesheetMealDetail()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceIDProperty, .GetInt32(0))
          LoadProperty(ShiftStartProperty, .GetValue(1))
          LoadProperty(ShiftEndProperty, .GetValue(2))
          LoadProperty(MealDateProperty, .GetValue(3))
          LoadProperty(MealProperty, .GetString(4))
          LoadProperty(ReimbursementProperty, .GetDecimal(5))
          LoadProperty(NotQualifiesReasonProperty, .GetString(6))
          LoadProperty(ShiftDurationProperty, .GetDecimal(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTimesheetMealDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTimesheetMealDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceID As SqlParameter = .Parameters.Add("@HumanResourceID", SqlDbType.Int)
          paramHumanResourceID.Value = GetProperty(HumanResourceIDProperty)
          If Me.IsNew Then
            paramHumanResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ShiftStart", (New SmartDate(GetProperty(ShiftStartProperty))).DBValue)
          .Parameters.AddWithValue("@ShiftEnd", (New SmartDate(GetProperty(ShiftEndProperty))).DBValue)
          .Parameters.AddWithValue("@MealDate", (New SmartDate(GetProperty(MealDateProperty))).DBValue)
          .Parameters.AddWithValue("@Meal", GetProperty(MealProperty))
          .Parameters.AddWithValue("@Reimbursement", GetProperty(ReimbursementProperty))
          .Parameters.AddWithValue("@NotQualifiesReason", GetProperty(NotQualifiesReasonProperty))
          .Parameters.AddWithValue("@ShiftDuration", GetProperty(ShiftDurationProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceIDProperty, paramHumanResourceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTimesheetMealDetail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace