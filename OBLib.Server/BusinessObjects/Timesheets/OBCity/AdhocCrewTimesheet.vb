﻿' Generated 10 Mar 2015 12:08 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity

  <Serializable()> _
  Public Class AdhocCrewTimesheet
    Inherits SingularBusinessBase(Of AdhocCrewTimesheet)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AdhocCrewTimesheetIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdhocCrewTimesheetID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property AdhocCrewTimesheetID() As Integer
      Get
        Return GetProperty(AdhocCrewTimesheetIDProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:=""),
    Required(ErrorMessage:="Description required"),
    StringLength(200, ErrorMessage:="Description cannot be more than 200 characters")>
  Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime) _
                                                                        .AddSetExpression("AdhocCrewTimesheetBO.StartDateTimeSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime) _
                                                                        .AddSetExpression("AdhocCrewTimesheetBO.EndDateTimeSet(self)", False)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    Required(ErrorMessage:="End Date required")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared TimesheetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TimesheetDate, "Timesheet Date")
    ''' <summary>
    ''' Gets and sets the Timesheet Date value
    ''' </summary>
    <Display(Name:="Timesheet Date", Description:=""),
    Required(ErrorMessage:="Timesheet Date required")>
  Public Property TimesheetDate As DateTime?
      Get
        Return GetProperty(TimesheetDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TimesheetDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared AdhocCrewTimesheetHumanResourceListProperty As PropertyInfo(Of AdhocCrewTimesheetHumanResourceList) = RegisterProperty(Of AdhocCrewTimesheetHumanResourceList)(Function(c) c.AdhocCrewTimesheetHumanResourceList, "Adhoc Crew Timesheet Human Resource List")

    Public ReadOnly Property AdhocCrewTimesheetHumanResourceList() As AdhocCrewTimesheetHumanResourceList
      Get
        If GetProperty(AdhocCrewTimesheetHumanResourceListProperty) Is Nothing Then
          LoadProperty(AdhocCrewTimesheetHumanResourceListProperty, Timesheets.OBCity.AdhocCrewTimesheetHumanResourceList.NewAdhocCrewTimesheetHumanResourceList())
        End If
        Return GetProperty(AdhocCrewTimesheetHumanResourceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AdhocCrewTimesheetIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Description.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Adhoc Crew Timesheet")
        Else
          Return String.Format("Blank {0}", "Adhoc Crew Timesheet")
        End If
      Else
        Return Me.Description
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"AdhocCrewTimesheetHumanResources"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(StartDateTimeProperty)
        .ServerRuleFunction = AddressOf TimesValid
        .JavascriptRuleFunctionName = "AdhocCrewTimesheetBO.TimesValid"
        .AddTriggerProperty(EndDateTimeProperty)
        .AddTriggerProperty(TimesheetDateProperty)
        .AffectedProperties.Add(EndDateTimeProperty)
        .AffectedProperties.Add(TimesheetDateProperty)
      End With

    End Sub

    Public Function TimesValid(AdhocCrewTimesheet As AdhocCrewTimesheet) As String
      Dim Err As String = ""
      If AdhocCrewTimesheet.StartDateTime Is Nothing Then
        Err &= "Start Date Time is required"
      ElseIf AdhocCrewTimesheet.EndDateTime Is Nothing Then
        Err &= "End Date Time is required"
      ElseIf AdhocCrewTimesheet.StartDateTime.Value > AdhocCrewTimesheet.EndDateTime.Value Then
          Err &= "Start Time must be before End Time"
      End If
      Return Err
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAdhocCrewTimesheet() method.

    End Sub

    Public Shared Function NewAdhocCrewTimesheet() As AdhocCrewTimesheet

      Return DataPortal.CreateChild(Of AdhocCrewTimesheet)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAdhocCrewTimesheet(dr As SafeDataReader) As AdhocCrewTimesheet

      Dim a As New AdhocCrewTimesheet()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AdhocCrewTimesheetIDProperty, .GetInt32(0))
          LoadProperty(DescriptionProperty, .GetString(1))
          LoadProperty(StartDateTimeProperty, .GetValue(2))
          LoadProperty(EndDateTimeProperty, .GetValue(3))
          LoadProperty(TimesheetDateProperty, .GetValue(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAdhocCrewTimesheet"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAdhocCrewTimesheet"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAdhocCrewTimesheetID As SqlParameter = .Parameters.Add("@AdhocCrewTimesheetID", SqlDbType.Int)
          paramAdhocCrewTimesheetID.Value = GetProperty(AdhocCrewTimesheetIDProperty)
          If Me.IsNew Then
            paramAdhocCrewTimesheetID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@TimesheetDate", (New SmartDate(GetProperty(TimesheetDateProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AdhocCrewTimesheetIDProperty, paramAdhocCrewTimesheetID.Value)
          End If
          ' update child objects
          If GetProperty(AdhocCrewTimesheetHumanResourceListProperty) IsNot Nothing Then
            Me.AdhocCrewTimesheetHumanResourceList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(AdhocCrewTimesheetHumanResourceListProperty) IsNot Nothing Then
          Me.AdhocCrewTimesheetHumanResourceList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAdhocCrewTimesheet"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AdhocCrewTimesheetID", GetProperty(AdhocCrewTimesheetIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace