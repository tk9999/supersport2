﻿' Generated 07 Mar 2015 09:44 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Timesheets.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Timesheets.OBCity

  <Serializable()> _
  Public Class TimesheetAdjustment
    Inherits SingularBusinessBase(Of TimesheetAdjustment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TimesheetAdjustmentsIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TimesheetAdjustmentsID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property TimesheetAdjustmentsID() As Integer
      Get
        Return GetProperty(TimesheetAdjustmentsIDProperty)
      End Get
    End Property

    Public Shared AdjustmentProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Adjustment, "Adjustment", 0)
    ''' <summary>
    ''' Gets and sets the Adjustment value
    ''' </summary>
    <Display(Name:="Adjustment", Description:="The value by which the timesheet must be adjusted"),
    Required(ErrorMessage:="Adjustment required")>
    Public Property Adjustment() As Decimal
      Get
        Return GetProperty(AdjustmentProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AdjustmentProperty, Value)
      End Set
    End Property

    Public Shared AdjustmentReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdjustmentReason, "Adjustment Reason", "")
    ''' <summary>
    ''' Gets and sets the Adjustment Reason value
    ''' </summary>
    <Display(Name:="Adjustment Reason", Description:="The reason for the adjustment"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Adjustment Reason is required")>
    Public Property AdjustmentReason() As String
      Get
        Return GetProperty(AdjustmentReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AdjustmentReasonProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As SmartDate
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDate, "Modified Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDate() As SmartDate
      Get
        Return GetProperty(ModifiedDateProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="human resource"),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared AdjustedTimesheetMonthIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdjustedTimesheetMonthID, "Adjusted Month", Nothing)
    ''' <summary>
    ''' Gets and sets the Adjusted Timesheet Month value
    ''' </summary>
    <Display(Name:="Adjusted Month", Description:="month which is to be corrected"),
    Required(ErrorMessage:="Adjusted Month required"),
    DropDownWeb(GetType(ROTimesheetMonthList))>
    Public Property AdjustedTimesheetMonthID() As Integer?
      Get
        Return GetProperty(AdjustedTimesheetMonthIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AdjustedTimesheetMonthIDProperty, Value)
      End Set
    End Property

    Public Shared TimesheetHourTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TimesheetHourTypeID, "Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Timesheet Hour Type value
    ''' </summary>
    <Display(Name:="Type"),
    Required(ErrorMessage:="Type required"),
    DropDownWeb(GetType(ROTimesheetHourTypeList))>
    Public Property TimesheetHourTypeID() As Integer?
      Get
        Return GetProperty(TimesheetHourTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TimesheetHourTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ReflectInTimesheetMonthIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ReflectInTimesheetMonthID, "Reflected Month", Nothing)
    ''' <summary>
    ''' Gets and sets the Reflect In Timesheet Month value
    ''' </summary>
    <Display(Name:="Reflected Month", Description:="timesheet month in which the adjustment must reflect"),
     Required(ErrorMessage:="Reflected Month required"),
    DropDownWeb(GetType(ROTimesheetMonthList))>
    Public Property ReflectInTimesheetMonthID() As Integer?
      Get
        Return GetProperty(ReflectInTimesheetMonthIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ReflectInTimesheetMonthIDProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisedByUserID, "Auth By", Nothing)
    ''' <summary>
    ''' Gets and sets the Authorised By User value
    ''' </summary>
    <Display(Name:="Auth By", Description:="")>
    Public Property AuthorisedByUserID() As Integer?
      Get
        Return GetProperty(AuthorisedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AuthorisedByUserIDProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, "Auth Time")
    ''' <summary>
    ''' Gets and sets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Auth Time", Description:="")>
    Public Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(AuthorisedDateTimeProperty, Value)
      End Set
    End Property

    Public ReadOnly Property ReflectedInSameMonthInd As Boolean
      Get
        Return CompareSafe(ReflectInTimesheetMonthID, AdjustedTimesheetMonthID)
      End Get
    End Property

    Public Shared AuthorisedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.Authorised, False) _
                                                                   .AddSetExpression("TimesheetAdjustmentBO.AuthorisedSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised?", Description:="")>
    Public Property Authorised() As Boolean
      Get
        Return GetProperty(AuthorisedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(AuthorisedProperty, Value)
      End Set
    End Property

    Public ReadOnly Property TimesheetHourType() As String
      Get
        If TimesheetHourTypeID <> 0 Then
          Dim tm As ROTimesheetHourType = OBLib.CommonData.Lists.ROTimesheetHourTypeList.GetItem(TimesheetHourTypeID)
          Return If(tm Is Nothing, "", tm.TimesheetHourType)
        End If
        Return ""
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TimesheetAdjustmentsIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AdjustmentReason.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Timesheet Adjustment")
        Else
          Return String.Format("Blank {0}", "Timesheet Adjustment")
        End If
      Else
        Return Me.AdjustmentReason
      End If

    End Function

    Public Function GetParent() As OBCityTimesheet

      Return CType(CType(Me.Parent, TimesheetAdjustmentList).Parent, OBCityTimesheet)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTimesheetAdjustment() method.

    End Sub

    Public Shared Function NewTimesheetAdjustment() As TimesheetAdjustment

      Return DataPortal.CreateChild(Of TimesheetAdjustment)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTimesheetAdjustment(dr As SafeDataReader) As TimesheetAdjustment

      Dim t As New TimesheetAdjustment()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TimesheetAdjustmentsIDProperty, .GetInt32(0))
          LoadProperty(AdjustmentProperty, .GetDecimal(1))
          LoadProperty(AdjustmentReasonProperty, .GetString(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateProperty, .GetSmartDate(6))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(AdjustedTimesheetMonthIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(TimesheetHourTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(ReflectInTimesheetMonthIDProperty, .GetInt32(10))
          LoadProperty(AuthorisedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(AuthorisedDateTimeProperty, .GetValue(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTimesheetAdjustment"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTimesheetAdjustment"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTimesheetAdjustmentsID As SqlParameter = .Parameters.Add("@TimesheetAdjustmentsID", SqlDbType.Int)
          paramTimesheetAdjustmentsID.Value = GetProperty(TimesheetAdjustmentsIDProperty)
          If Me.IsNew Then
            paramTimesheetAdjustmentsID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Adjustment", GetProperty(AdjustmentProperty))
          .Parameters.AddWithValue("@AdjustmentReason", GetProperty(AdjustmentReasonProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@AdjustedTimesheetMonthID", GetProperty(AdjustedTimesheetMonthIDProperty))
          .Parameters.AddWithValue("@TimesheetHourTypeID", GetProperty(TimesheetHourTypeIDProperty))
          .Parameters.AddWithValue("@ReflectInTimesheetMonthID", GetProperty(ReflectInTimesheetMonthIDProperty))
          .Parameters.AddWithValue("@AuthorisedByUserID", Singular.Misc.NothingDBNull(GetProperty(AuthorisedByUserIDProperty)))
          .Parameters.AddWithValue("@AuthorisedDateTime", (New SmartDate(GetProperty(AuthorisedDateTimeProperty))).DBValue)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(TimesheetAdjustmentsIDProperty, paramTimesheetAdjustmentsID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTimesheetAdjustment"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TimesheetAdjustmentsID", GetProperty(TimesheetAdjustmentsIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace