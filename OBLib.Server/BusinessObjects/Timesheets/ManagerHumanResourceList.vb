﻿' Generated 17 Jul 2014 16:25 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class ManagerHumanResourceList
    Inherits OBBusinessListBase(Of ManagerHumanResourceList, ManagerHumanResource)

#Region " Business Methods "

    Public Property ManagerHumanResourceID As Integer = 0

    Public Function GetItem(ManagerHumanResourceID As Integer) As ManagerHumanResource

      For Each child As ManagerHumanResource In Me
        If child.ManagerHumanResourceID = ManagerHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Protected Overrides Function AddNewCore() As Object

      Dim obj As ManagerHumanResource = ManagerHumanResource.NewManagerHumanResource
      obj.ManagerHumanResourceID = ManagerHumanResourceID
      Me.Add(obj)
      Return obj

    End Function

    Public Overrides Function ToString() As String

      Return "RO Project Human Resources"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public HumanResourceID As Integer = OBLib.Security.Settings.CurrentUser.HumanResourceID

      Public AllManagersInd As Boolean = False
      Public Sub New(HumanResourceID As Integer)

        Me.HumanResourceID = HumanResourceID

      End Sub

      Public Sub New(AllManagersInd As Boolean)

        Me.AllManagersInd = AllManagersInd

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewManagerHumanResourceList() As ManagerHumanResourceList

      Return New ManagerHumanResourceList()

    End Function

    Public Shared Sub BeginGetManagerHumanResourceList(CallBack As EventHandler(Of DataPortalResult(Of ManagerHumanResourceList)))

      Dim dp As New DataPortal(Of ManagerHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "
     
    Public Shared Function GetManagerHumanResourceList(ManagerHumanResourceID As Integer) As ManagerHumanResourceList

      Dim mList = DataPortal.Fetch(Of ManagerHumanResourceList)(New Criteria(ManagerHumanResourceID))
      mList.ManagerHumanResourceID = ManagerHumanResourceID
      Return mList

    End Function

    Public Shared Function GetAllManagerList() As ManagerHumanResourceList

      Return DataPortal.Fetch(Of ManagerHumanResourceList)(New Criteria(True))

    End Function

    Public Shared Function GetManagerHumanResourceList() As ManagerHumanResourceList

      Return DataPortal.Fetch(Of ManagerHumanResourceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ManagerHumanResource.GetManagerHumanResource(sdr))
      End While
      Me.RaiseListChangedEvents = True


    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getManagerHumanResourceList"
            cm.Parameters.AddWithValue("@ManagerHumanResourceID", crit.HumanResourceID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ManagerHumanResource In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        ' Link the new objects to the correct Manager
        For Each Child As ManagerHumanResource In Me
          If Child.IsNew Then
            Child.ManagerHumanResourceID = ManagerHumanResourceID
            Child.Update()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace