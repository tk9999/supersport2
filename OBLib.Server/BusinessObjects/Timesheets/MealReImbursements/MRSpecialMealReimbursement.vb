﻿' Generated 22 Sep 2014 11:37 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.NSWTimesheets.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class MRSpecialMealReimbursement
    Inherits OBBusinessBase(Of MRSpecialMealReimbursement)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared MRSpecialMealReimbursementIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MRSpecialMealReimbursementID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property MRSpecialMealReimbursementID() As Integer
      Get
        Return GetProperty(MRSpecialMealReimbursementIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HumanResourceID, Nothing) _
                                                                                                        .AddSetExpression("SetAmount(self)") _
                                                                                                         .AddSetExpression("SetEmployeCode(self)")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROManagerHumanResourceList), ValueMember:="HumanResourceID", Source:=DropDownWeb.SourceType.ViewModel)>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared EmployeeCodeIDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code", "")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee No", Description:="EmployeeCode")>
    Public Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeIDNoProperty)
      End Get
      Set(value As String)
        SetProperty(EmployeeCodeIDNoProperty, value)
      End Set
    End Property

    Public Shared ReimbursementReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ReimbursementReason, "Reimbursement Reason", "")
    ''' <summary>
    ''' Gets and sets the Reimbursement Reason value
    ''' </summary>
    <Display(Name:="Reimbursement Reason", Description:=""), Required(ErrorMessage:="Reason required"),
    StringLength(100, ErrorMessage:="Reimbursement Reason cannot be more than 100 characters")>
    Public Property ReimbursementReason() As String
      Get
        Return GetProperty(ReimbursementReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ReimbursementReasonProperty, Value)
      End Set
    End Property


    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing) _
                                                                                                        .AddSetExpression("SetEndDates(self)") _
                                                                                                         .AddSetExpression("SetAmount(self)")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date required"), Singular.DataAnnotations.DateField(MaxDateProperty:="ViewModel.MaxDate()", MinDateProperty:="ViewModel.StartDate()")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing) _
                                                                           .AddSetExpression("SetAmount(self)")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    Required(ErrorMessage:="End Date Time required"), Singular.DataAnnotations.DateField(MinDateProperty:="ViewModel.StartDate()", MaxDateProperty:="ViewModel.MaxDate()")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ReimbursementAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ReimbursementAmount, "Amount", 0)
    ''' <summary>
    ''' Gets and sets the Reimbursement Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:=""), Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.CurrencyDecimals)>
    Public ReadOnly Property ReimbursementAmount() As Decimal
      Get
        Return GetProperty(ReimbursementAmountProperty)
      End Get
    End Property

    Public Shared CreatedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CreatedByUserID, "Created By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Created By User value
    ''' </summary>
    <Display(Name:="Created By User", Description:="")>
    Public Property CreatedByUserID() As Integer?
      Get
        Return GetProperty(CreatedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CreatedByUserIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As SmartDate
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Amount, "Amount", 0)
    ''' <summary>
    ''' Gets and sets the Authorised Reimbursement Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:="")>
    Public ReadOnly Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
    End Property

    Public Shared NoOfMealsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfMeals, "No of Meals", 0)
    ''' <summary>
    ''' Gets  the Authorised Meal Vouchers value
    ''' </summary>
    <Display(Name:="No of Meals", Description:="")>
    Public ReadOnly Property NoOfMeals() As Integer
      Get
        Return GetProperty(NoOfMealsProperty)
      End Get
    End Property

    Public Shared NSWTimesheetCategoryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NSWTimesheetCategoryID, "NSW Timesheet Category", Nothing)
    ''' <summary>
    ''' Gets and sets the NSW Timesheet Category value
    ''' </summary>
    <Display(Name:="Category", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(NSWTimesheetCategoryList), Source:=DropDownWeb.SourceType.CommonData),
    Required(ErrorMessage:="Category required")>
    Public Property NSWTimesheetCategoryID() As Integer?
      Get
        Return GetProperty(NSWTimesheetCategoryIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NSWTimesheetCategoryIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:=""), _
    Singular.DataAnnotations.DropDownWeb(GetType(RONSWOpenMonthProductionList), FilterMethodName:="FilterProductionWeekly")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    <Singular.DataAnnotations.ClientOnly()>
    Public Property IsLoading As Boolean = False

    Public Shared SyncIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SyncID, "SyncID", 0)
    ''' <summary>
    ''' Gets  the Authorised Meal Vouchers value
    ''' </summary>
    <Display(Name:="SyncID", Description:="")>
    Public Property SyncID() As Integer
      Get
        Return GetProperty(SyncIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SyncIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(MRSpecialMealReimbursementIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ReimbursementReason.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "NSW Special Meal Reimbursement")
        Else
          Return String.Format("Blank {0}", "NSW Special Meal Reimbursement")
        End If
      Else
        Return Me.ReimbursementReason
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(StartDateTimeProperty)
        .JavascriptRuleFunctionName = "CheckHRDates"
        .ServerRuleFunction = Function(smr)
                                Dim list = smr.FindParent(Of MRSpecialMealReimbursementList)()
                                If list IsNot Nothing Then
                                  list = list.GetHRSMR(smr.HumanResourceID)
                                  For Each itm As MRSpecialMealReimbursement In list
                                    If itm.Guid <> smr.Guid Then
                                      If (smr.StartDateTime > itm.StartDateTime AndAlso smr.StartDateTime < itm.EndDateTime) _
                                          OrElse (smr.EndDateTime > itm.StartDateTime AndAlso smr.EndDateTime < itm.EndDateTime) _
                                          OrElse Singular.Misc.CompareSafe(smr.StartDateTime, itm.StartDateTime) AndAlso Singular.Misc.CompareSafe(smr.EndDateTime, itm.EndDateTime) Then
                                        Return "You have already captured a Special Meal for this person between " + CType(itm.StartDateTime, Date).ToString("dd MMM yyyy - HH:mm") + " & " + CType(itm.EndDateTime, Date).ToString("dd MMM yyyy - HH:mm")
                                      End If
                                    End If
                                  Next
                                End If


                                If smr.StartDateTime IsNot Nothing Then
                                  If CType(smr.StartDateTime, Date).DayOfWeek = DayOfWeek.Saturday OrElse CType(smr.StartDateTime, Date).DayOfWeek = DayOfWeek.Sunday Then

                                    Return "Cannot have special meals on a weakend"
                                  End If
                                End If

                                If smr.EndDateTime IsNot Nothing Then
                                  If CType(smr.EndDateTime, Date).DayOfWeek = DayOfWeek.Saturday OrElse CType(smr.EndDateTime, Date).DayOfWeek = DayOfWeek.Sunday Then

                                    Return "Cannot have special meals on a weakend"

                                  End If
                                End If

                                If smr.StartDateTime IsNot Nothing AndAlso smr.EndDateTime IsNot Nothing Then
                                  If CommonData.Lists.PublicHolidayList.IsHoliday(smr.EndDateTime) OrElse CommonData.Lists.PublicHolidayList.IsHoliday(smr.StartDateTime) Then
                                    Return "Cannot have special meals on a Public Holiday List"
                                  End If
                                End If



                                Return ""

                              End Function
        .AffectedProperties.Add(HumanResourceIDProperty)
        .AddTriggerProperty(HumanResourceIDProperty)
        .AffectedProperties.Add(EndDateTimeProperty)
        .AddTriggerProperty(EndDateTimeProperty)
      End With


      With AddWebRule(ProductionIDProperty)
        .JavascriptRuleFunctionName = "CheckProduction"
        .ServerRuleFunction = Function(ts)
                                Dim Cat = CommonData.Lists.NSWTimesheetCategoryList.GetItem(ts.NSWTimesheetCategoryID)
                                If Cat IsNot Nothing Then
                                  If Cat.ProductionRelatedInd AndAlso Singular.Misc.IsNullNothing(ts.ProductionID, True) Then
                                    Return "Production is required"
                                  End If
                                End If
                                Return ""

                              End Function
        .AffectedProperties.Add(NSWTimesheetCategoryIDProperty)
        .AddTriggerProperty(NSWTimesheetCategoryIDProperty)
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewMRSpecialMealReimbursement() method.
      CreatedByUserID = OBLib.Security.Settings.CurrentUserID
    End Sub

    Public Shared Function NewMRSpecialMealReimbursement() As MRSpecialMealReimbursement

      Return DataPortal.CreateChild(Of MRSpecialMealReimbursement)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetMRSpecialMealReimbursement(dr As SafeDataReader) As MRSpecialMealReimbursement

      Dim n As New MRSpecialMealReimbursement()
      n.Fetch(dr)
      Return n

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(MRSpecialMealReimbursementIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ReimbursementReasonProperty, .GetString(2))
          LoadProperty(StartDateTimeProperty, .GetValue(3))
          LoadProperty(CreatedByUserIDProperty, .GetInt32(4))
          LoadProperty(CreatedDateProperty, .GetSmartDate(5))
          LoadProperty(EndDateTimeProperty, .GetValue(6))

          LoadProperty(AmountProperty, .GetDecimal(7))
          LoadProperty(NoOfMealsProperty, .GetInt32(8))
          LoadProperty(NSWTimesheetCategoryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(EmployeeCodeIDNoProperty, .GetString(11))


        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insMRSpecialMealReimbursement"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updMRSpecialMealReimbursement"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramMRSpecialMealReimbursementID As SqlParameter = .Parameters.Add("@MRSpecialMealReimbursementID", SqlDbType.Int)
          paramMRSpecialMealReimbursementID.Value = GetProperty(MRSpecialMealReimbursementIDProperty)
          If Me.IsNew Then
            paramMRSpecialMealReimbursementID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@ReimbursementReason", GetProperty(ReimbursementReasonProperty))
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CreatedByUserID", GetProperty(CreatedByUserIDProperty))
          .Parameters.AddWithValue("@NSWTimesheetCategoryID", GetProperty(NSWTimesheetCategoryIDProperty))
          .Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(GetProperty(ProductionIDProperty)))
          .Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
          .Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(MRSpecialMealReimbursementIDProperty, paramMRSpecialMealReimbursementID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delMRSpecialMealReimbursement"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@MRSpecialMealReimbursementID", GetProperty(MRSpecialMealReimbursementIDProperty))
        cm.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
        cm.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace