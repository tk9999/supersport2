﻿' Generated 10 Sep 2014 15:51 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class NSWTimesheetsAuthorisationEntry
    Inherits OBBusinessBase(Of NSWTimesheetsAuthorisationEntry)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared NSWTimesheetEntryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NSWTimesheetEntryID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property NSWTimesheetEntryID() As Integer
      Get
        Return GetProperty(NSWTimesheetEntryIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID, "Manager Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="Manager Human Resource", Description:="")>
  Public ReadOnly Property ManagerHumanResourceID() As Integer?
      Get
        Return GetProperty(ManagerHumanResourceIDProperty)
      End Get
    End Property

    Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "First Name", "")
    ''' <summary>
    ''' Gets the First Name value
    ''' </summary>
    <Display(Name:="First Name", Description:="")>
  Public ReadOnly Property FirstName() As String
      Get
        Return GetProperty(FirstNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
  Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared EmployeeCodeIDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCodeIDNo, "Employee Code", "")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee No./ID No.", Description:="EmployeeCode Or IDNo")>
    Public ReadOnly Property EmployeeCodeIDNo() As String
      Get
        Return GetProperty(EmployeeCodeIDNoProperty)
      End Get
    End Property
  
    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared HoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Hours, "Hours", 0)
    ''' <summary>
    ''' Gets the Hours value
    ''' </summary>
    <Display(Name:="Hours", Description:=""), Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property Hours() As Decimal
      Get
        Return GetProperty(HoursProperty)
      End Get
    End Property

    Public Shared NSWTimesheetCategoryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NSWTimesheetCategory, "Category ", "")
    ''' <summary>
    ''' Gets the NSW Timesheet Category ID 1 value
    ''' </summary>
    <Display(Name:="Category", Description:="")>
    Public ReadOnly Property NSWTimesheetCategory() As String
      Get
        Return GetProperty(NSWTimesheetCategoryProperty)
      End Get
    End Property

    Public Shared ProductionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Production, "Production", "")
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public ReadOnly Property Production() As String
      Get
        Return GetProperty(ProductionProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
  Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, "Authorised Date Time")
    ''' <summary>
    ''' Gets and sets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Authorised Date Time", Description:="")>
  Public Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(AuthorisedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisedByUserID, "Authorised By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised By User", Description:="")>
    Public Property AuthorisedByUserID() As Integer?
      Get
        Return GetProperty(AuthorisedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AuthorisedByUserIDProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedIDProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.AuthorisedID, 0) _
                                                                     .AddSetExpression("AuthChanged(self);") _
                                                                     .AddSetExpression("PreAuthChanged(self, args);", True)

    ''' <summary>
    ''' Gets and sets the Authorised value
    ''' </summary>
    <Display(Name:="Authorised", Description:=""), Singular.DataAnnotations.RadioButtonList(GetType(CommonData.Enums.NSWAuthoriseStatus)),
    Required(ErrorMessage:="Authorised required")>
    Public Property AuthorisedID() As Integer
      Get
        Return GetProperty(AuthorisedIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AuthorisedIDProperty, Value)
        SetProperty(AuthorisedByUserIDProperty, OBLib.Security.Settings.CurrentUserID)
      End Set
    End Property

    Public Shared RejectedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RejectedReason, "Rejected Reason", "")
    ''' <summary>
    ''' Gets and sets the Authorised value
    ''' </summary>
    <Display(Name:="Rejected Reason", Description:="")>
    Public Property RejectedReason() As String
      Get
        Return GetProperty(RejectedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RejectedReasonProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As NSWTimesheetsAuthorisation

      Return CType(CType(Me.Parent, NSWTimesheetsAuthorisationEntryList).Parent, NSWTimesheetsAuthorisation)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(NSWTimesheetEntryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.FirstName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "NSW Timesheets Authorisation Entry")
        Else
          Return String.Format("Blank {0}", "NSW Timesheets Authorisation Entry")
        End If
      Else
        Return Me.StartDateTime.ToString & " - " & Me.EndDateTime.ToString
      End If

    End Function

 

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(RejectedReasonProperty)
        .JavascriptRuleFunctionName = "CheckRejectedReason"
        .ServerRuleFunction = Function(ts)
                                If ts.AuthorisedID = CommonData.Enums.NSWAuthoriseStatus.Rejected AndAlso Singular.Misc.IsNullNothingOrEmpty(ts.RejectedReason) Then
                                  Return "Rejected Reason is required"
                                End If
                                Return ""

                              End Function
        .AffectedProperties.Add(AuthorisedIDProperty)
        .AddTriggerProperty(AuthorisedIDProperty)
      End With 

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewNSWTimesheetsAuthorisationEntry() method.

    End Sub

    Public Shared Function NewNSWTimesheetsAuthorisationEntry() As NSWTimesheetsAuthorisationEntry

      Return DataPortal.CreateChild(Of NSWTimesheetsAuthorisationEntry)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetNSWTimesheetsAuthorisationEntry(dr As SafeDataReader) As NSWTimesheetsAuthorisationEntry

      Dim n As New NSWTimesheetsAuthorisationEntry()
      n.Fetch(dr)
      Return n

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(NSWTimesheetEntryIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ManagerHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(FirstNameProperty, .GetString(3))
          LoadProperty(SurnameProperty, .GetString(4)) 
          LoadProperty(EmployeeCodeIDNoProperty, .GetString(5))
          LoadProperty(StartDateTimeProperty, .GetValue(6))
          LoadProperty(EndDateTimeProperty, .GetValue(7))
          LoadProperty(HoursProperty, .GetDecimal(8))
          LoadProperty(NSWTimesheetCategoryProperty, .GetString(9))
          LoadProperty(ProductionProperty, .GetString(10))
          LoadProperty(DescriptionProperty, .GetString(11))
          LoadProperty(AuthorisedDateTimeProperty, .GetValue(12))
          LoadProperty(AuthorisedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(AuthorisedIDProperty, .GetInt32(14))
          LoadProperty(RejectedReasonProperty, .GetString(15))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insNSWTimesheetsAuthorisationEntry"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updNSWTimesheetsAuthorisationEntry"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramNSWTimesheetCategoryID As SqlParameter = .Parameters.Add("@NSWTimesheetEntryID", SqlDbType.Int)
          paramNSWTimesheetCategoryID.Value = GetProperty(NSWTimesheetEntryIDProperty)
          If Me.IsNew Then
            paramNSWTimesheetCategoryID.Direction = ParameterDirection.Output
          End If

          .Parameters.AddWithValue("@AuthorisedID", GetProperty(AuthorisedIDProperty))
          .Parameters.AddWithValue("@AuthorisedByUserID", Singular.Misc.NothingDBNull(GetProperty(AuthorisedByUserIDProperty)))
          .Parameters.AddWithValue("@RejectedReason", GetProperty(RejectedReasonProperty))
          .Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
          .Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)

          .ExecuteNonQuery()

 
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
 

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace