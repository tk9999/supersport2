﻿' Generated 22 Sep 2014 11:37 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.NSWTimesheets.NSWTimesheetsAuthorisationList

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class MRSpecialMealReimbursementList
    Inherits OBBusinessListBase(Of MRSpecialMealReimbursementList, MRSpecialMealReimbursement)

#Region " Business Methods "

    Public Function GetItem(MRSpecialMealReimbursementID As Integer) As MRSpecialMealReimbursement

      For Each child As MRSpecialMealReimbursement In Me
        If child.MRSpecialMealReimbursementID = MRSpecialMealReimbursementID Then
          Return child
        End If
      Next
      Return Nothing

    End Function


    Public Function GetHRSMR(HumanresourceID As Integer) As MRSpecialMealReimbursementList

      Dim list As New MRSpecialMealReimbursementList
      For Each child As MRSpecialMealReimbursement In Me
        If child.HumanResourceID = HumanresourceID Then
          list.Add(child)
        End If
      Next
      Return list

    End Function

    Public Overrides Function ToString() As String

      Return "NSW Special Meal Reimbursements"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewMRSpecialMealReimbursementList() As MRSpecialMealReimbursementList

      Return New MRSpecialMealReimbursementList()

    End Function

    Public Shared Sub BeginGetMRSpecialMealReimbursementList(CallBack As EventHandler(Of DataPortalResult(Of MRSpecialMealReimbursementList)))

      Dim dp As New DataPortal(Of MRSpecialMealReimbursementList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetMRSpecialMealReimbursementList() As MRSpecialMealReimbursementList

      Return DataPortal.Fetch(Of MRSpecialMealReimbursementList)(New Criteria())

    End Function

    Public Shared Function GetReimbursementAmount(HRID As Integer, StartDate As Object, EndDate As Object, SyncID As Integer) As MRValues
      Dim mResults As New MRValues

      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.[GetCalcMealReimbursementForShift]"
            cm.Parameters.AddWithValue("@HumanResourceID", HRID)
            cm.Parameters.AddWithValue("@StartDateTime", StartDate)
            cm.Parameters.AddWithValue("@EndDateTime", EndDate)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              If sdr.Read Then
                mResults.NoOfMeals = sdr.GetInt32(3)
                mResults.MRAmount = sdr.GetDecimal(4)

              End If
            End Using
          End Using
        Finally
          cn.Close()
        End Try

      End Using

      mResults.SyncID = SyncID
      Return mResults
    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(MRSpecialMealReimbursement.GetMRSpecialMealReimbursement(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getMRSpecialMealReimbursementList"
            cm.Parameters.AddWithValue("@ManagerHumanResourceID", OBLib.Security.Settings.CurrentUser.HumanResourceID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As MRSpecialMealReimbursement In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As MRSpecialMealReimbursement In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region


  End Class

End Namespace