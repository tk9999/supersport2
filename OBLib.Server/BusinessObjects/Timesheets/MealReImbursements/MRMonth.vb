﻿' Generated 09 Sep 2014 15:07 - Singular Systems Object Generator Version 2.1.674
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class MRMonth
    Inherits OBBusinessBase(Of MRMonth)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared MRMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MRMonthID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property MRMonthID() As Integer
      Get
        Return GetProperty(MRMonthIDProperty)
      End Get
    End Property

    Public Shared MonthStartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.MonthStartDate, "Month Start Date")
    ''' <summary>
    ''' Gets and sets the Month Start Date value
    ''' </summary>
    <Display(Name:="Month Start Date", Description:=""),
    Required(ErrorMessage:="Month Start Date required")>
    Public Property MonthStartDate As DateTime?
      Get
        Return GetProperty(MonthStartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(MonthStartDateProperty, Value)
      End Set
    End Property

    Public Shared MonthEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.MonthEndDate, "Month End Date")
    ''' <summary>
    ''' Gets and sets the Month End Date value
    ''' </summary>
    <Display(Name:="Month End Date", Description:=""),
    Required(ErrorMessage:="Month End Date required")>
    Public Property MonthEndDate As DateTime?
      Get
        Return GetProperty(MonthEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(MonthEndDateProperty, Value)
      End Set
    End Property

    Public Shared ManuallyClosedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ManuallyClosedDateTime, "Manually Closed Date Time")
    ''' <summary>
    ''' Gets and sets the Manually Closed Date Time value
    ''' </summary>
    <Display(Name:="Manually Closed Date Time", Description:="")>
    Public Property ManuallyClosedDateTime As DateTime?
      Get
        Return GetProperty(ManuallyClosedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ManuallyClosedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ManuallyClosedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManuallyClosedBy, "Manually Closed By", Nothing)
    ''' <summary>
    ''' Gets and sets the Manually Closed By value
    ''' </summary>
    <Display(Name:="Manually Closed By", Description:="")>
    Public Property ManuallyClosedBy() As Integer?
      Get
        Return GetProperty(ManuallyClosedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ManuallyClosedByProperty, Value)
      End Set
    End Property

    Public Shared AutoClosedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AutoClosedDateTime, "Auto Closed Date Time")
    ''' <summary>
    ''' Gets and sets the Auto Closed Date Time value
    ''' </summary>
    <Display(Name:="Auto Closed Date Time", Description:="")>
    Public Property AutoClosedDateTime As DateTime?
      Get
        Return GetProperty(AutoClosedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(AutoClosedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared AutoCloseReopenedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AutoCloseReopenedBy, "Auto Close Reopened By", Nothing)
    ''' <summary>
    ''' Gets and sets the Auto Close Reopened By value
    ''' </summary>
    <Display(Name:="Auto Close Reopened By", Description:="")>
    Public Property AutoCloseReopenedBy() As Integer?
      Get
        Return GetProperty(AutoCloseReopenedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AutoCloseReopenedByProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(MRMonthIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.MRMonthID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "MR Month")
        Else
          Return String.Format("Blank {0}", "MR Month")
        End If
      Else
        Return Me.MonthStartDate.ToString("dd/MMM/yyyy")
      End If

    End Function

    Public Function CanCloseMonthInd() As Boolean

      If MonthEndDate IsNot Nothing Then
        Return MonthEndDate <= Now
      End If
      Return False

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewMRMonth() method.

    End Sub

    Public Shared Function NewMRMonth() As MRMonth

      Return DataPortal.CreateChild(Of MRMonth)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetMRMonth(dr As SafeDataReader) As MRMonth

      Dim n As New MRMonth()
      n.Fetch(dr)
      Return n

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(MRMonthIDProperty, .GetInt32(0))
          LoadProperty(MonthStartDateProperty, .GetValue(1))
          LoadProperty(MonthEndDateProperty, .GetValue(2))
          LoadProperty(ManuallyClosedDateTimeProperty, .GetValue(3))
          LoadProperty(ManuallyClosedByProperty, .GetValue(4))
          LoadProperty(AutoClosedDateTimeProperty, .GetValue(5))
          LoadProperty(AutoCloseReopenedByProperty, .GetValue(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insMRMonth"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updMRMonth"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramMRMonthID As SqlParameter = .Parameters.Add("@MRMonthID", SqlDbType.Int)
          paramMRMonthID.Value = GetProperty(MRMonthIDProperty)
          If Me.IsNew Then
            paramMRMonthID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@MonthStartDate", (New SmartDate(GetProperty(MonthStartDateProperty))).DBValue)
          .Parameters.AddWithValue("@MonthEndDate", (New SmartDate(GetProperty(MonthEndDateProperty))).DBValue)
          .Parameters.AddWithValue("@ManuallyClosedDateTime", (New SmartDate(GetProperty(ManuallyClosedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ManuallyClosedBy", Singular.Misc.ZeroNothingDBNull(GetProperty(ManuallyClosedByProperty)))
          .Parameters.AddWithValue("@AutoClosedDateTime", (New SmartDate(GetProperty(AutoClosedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@AutoCloseReopenedBy", Singular.Misc.ZeroNothingDBNull(GetProperty(AutoCloseReopenedByProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(MRMonthIDProperty, paramMRMonthID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delMRMonth"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@MRMonthID", GetProperty(MRMonthIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace