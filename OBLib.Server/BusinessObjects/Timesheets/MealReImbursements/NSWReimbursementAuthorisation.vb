﻿' Generated 11 Sep 2014 12:51 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class NSWReimbursementAuthorisation
    Inherits OBBusinessBase(Of NSWReimbursementAuthorisation)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared NSWReimbursementAuthorisationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NSWReimbursementAuthorisationID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property NSWReimbursementAuthorisationID() As Integer
      Get
        Return GetProperty(NSWReimbursementAuthorisationIDProperty)
      End Get
    End Property

    Public Shared MRMonthIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.MRMonthID, "MR Month", Nothing)
    ''' <summary>
    ''' Gets and sets the MR Month value
    ''' </summary>
    <Display(Name:="MR Month", Description:=""),
    Required(ErrorMessage:="MR Month required")>
    Public ReadOnly Property MRMonthID() As Integer?
      Get
        Return GetProperty(MRMonthIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared AuthorisedHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AuthorisedHours, "Authorised Hours", 0)
    ''' <summary>
    ''' Gets and sets the Authorised Hours value
    ''' </summary>
    <Display(Name:="Hours", Description:=""),
    Required(ErrorMessage:="Authorised Hours required")>
    Public ReadOnly Property AuthorisedHours() As Decimal
      Get
        Return GetProperty(AuthorisedHoursProperty)
      End Get
    End Property

    Public Shared AuthorisedNoOfMealsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuthorisedNoOfMeals, "Authorised No of Meals", 0)
    ''' <summary>
    ''' Gets and sets the Authorised Meal Vouchers value
    ''' </summary>
    <Display(Name:="Normal Meals", Description:="")>
    Public ReadOnly Property AuthorisedNoOfMeals() As Integer
      Get
        Return GetProperty(AuthorisedNoOfMealsProperty)
      End Get
    End Property

    Public Shared SpecialMealsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SpecialMeals, "Special Meals", 0)
    ''' <summary>
    ''' Gets and sets the Authorised Meal Vouchers value
    ''' </summary>
    <Display(Name:="Special Meals", Description:="")>
    Public ReadOnly Property SpecialMeals() As Integer
      Get
        Return GetProperty(SpecialMealsProperty)
      End Get
    End Property

    Public Shared ICRMealsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ICRMeals, "ICR Meals", 0)
    ''' <summary>
    ''' Gets and sets the Authorised Meal Vouchers value
    ''' </summary>
    <Display(Name:="ICR Meals", Description:="")>
    Public ReadOnly Property ICRMeals() As Integer
      Get
        Return GetProperty(ICRMealsProperty)
      End Get
    End Property

    Public Shared TotalMealsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalMeals, "Total Meals", 0)
    ''' <summary>
    ''' Gets and sets the Authorised Meal Vouchers value
    ''' </summary>
    <Display(Name:="Total Meals", Description:="")>
    Public ReadOnly Property TotalMeals() As Integer
      Get
        Return GetProperty(TotalMealsProperty)
      End Get
    End Property

    Public Shared AuthorisedReimbursementAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AuthorisedReimbursementAmount, "Authorised Reimbursement Amount", 0)
    ''' <summary>
    ''' Gets and sets the Authorised Reimbursement Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:=""),
    Required(ErrorMessage:="Authorised Reimbursement Amount required")>
    Public ReadOnly Property AuthorisedReimbursementAmount() As Decimal
      Get
        Return GetProperty(AuthorisedReimbursementAmountProperty)
      End Get
    End Property
     
 

    Public Shared AuthorisedIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuthorisedID, "Authorised", 0)
    ''' <summary>
    ''' Gets and sets the Authorised value
    ''' </summary>
    <Display(Name:="Authorised", Description:="0 = Pending, 1 = Authorised, 2 = Rejected"),
     Singular.DataAnnotations.RadioButtonList(GetType(CommonData.Enums.NSWAuthoriseStatus))>
    Public Property AuthorisedID() As Integer
      Get
        Return GetProperty(AuthorisedIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AuthorisedIDProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, "Authorised Date Time")
    ''' <summary>
    ''' Gets and sets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Authorised Date Time", Description:="")>
    Public Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(AuthorisedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisedByUserID, "Authorised By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised By User", Description:="")>
    Public Property AuthorisedByUserID() As Integer?
      Get
        Return GetProperty(AuthorisedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AuthorisedByUserIDProperty, Value)
      End Set
    End Property


    Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "First Name", "")
    ''' <summary>
    ''' Gets the First Name value
    ''' </summary>
    <Display(Name:="First Name", Description:="")>
    Public ReadOnly Property FirstName() As String
      Get
        Return GetProperty(FirstNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared EmployeeCodeIDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCodeIDNo, "Employee Code", "")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code / ID no", Description:="")>
    Public ReadOnly Property EmployeeCodeIDNo() As String
      Get
        Return GetProperty(EmployeeCodeIDNoProperty)
      End Get
    End Property

    Public Shared NoEntriesProperty As PropertyInfo(Of Int32) = RegisterProperty(Of Int32)(Function(c) c.NoEntries, "No Entries", 0)
    ''' <summary>
    ''' Gets the No Entries value
    ''' </summary>
    <Display(Name:="No Entries", Description:="")>
    Public ReadOnly Property NoEntries() As Int32
      Get
        Return GetProperty(NoEntriesProperty)
      End Get
    End Property

    Public Shared TimeAuthorisedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimeAuthorisedBy, "Time Authorised By", "")
    ''' <summary>
    ''' Gets the TimeAuthorisedBy value
    ''' </summary>
    <Display(Name:="Time Authorised By", Description:="")>
    Public ReadOnly Property TimeAuthorisedBy() As String
      Get
        Return GetProperty(TimeAuthorisedByProperty)
      End Get
    End Property

    Public Shared RejectedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RejectedReason, "Rejected Reason", "")
    ''' <summary>
    ''' Gets and sets the Authorised value
    ''' </summary>
    <Display(Name:="Rejected Reason", Description:="")>
    Public Property RejectedReason() As String
      Get
        Return GetProperty(RejectedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RejectedReasonProperty, Value)
      End Set
    End Property

    <DefaultValue(False)>
    Public Property Expanded As Boolean = False

    <Singular.DataAnnotations.ClientOnly()>
    Public Property IsLoading As Boolean = False

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System ID value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area", 0)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared NSWReimbursementAuthorisationDetailListProperty As PropertyInfo(Of NSWReimbursementAuthorisationDetailList) = RegisterProperty(Of NSWReimbursementAuthorisationDetailList)(Function(c) c.NSWReimbursementAuthorisationDetailList, "NSW Reimbursement Authorisation Detail List")

    Public ReadOnly Property NSWReimbursementAuthorisationDetailList() As NSWReimbursementAuthorisationDetailList
      Get
        If GetProperty(NSWReimbursementAuthorisationDetailListProperty) Is Nothing Then
          LoadProperty(NSWReimbursementAuthorisationDetailListProperty, NSWTimesheets.NSWReimbursementAuthorisationDetailList.NewNSWReimbursementAuthorisationDetailList())
        End If
        Return GetProperty(NSWReimbursementAuthorisationDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(NSWReimbursementAuthorisationIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.MRMonthID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "NSW Reimbursement Authorisation")
        Else
          Return String.Format("Blank {0}", "NSW Reimbursement Authorisation")
        End If
      Else
        Return Me.MRMonthID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(RejectedReasonProperty)
        .JavascriptRuleFunctionName = "CheckRejectedReason"
        .ServerRuleFunction = Function(ts)
                                If ts.AuthorisedID = CommonData.Enums.NSWAuthoriseStatus.Rejected AndAlso Singular.Misc.IsNullNothingOrEmpty(ts.RejectedReason) Then
                                  Return "Rejected Reason is required"
                                End If
                                Return ""

                              End Function
        .AffectedProperties.Add(AuthorisedIDProperty)
        .AddTriggerProperty(AuthorisedIDProperty)
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewNSWReimbursementAuthorisation() method.

    End Sub

    Public Shared Function NewNSWReimbursementAuthorisation() As NSWReimbursementAuthorisation

      Return DataPortal.CreateChild(Of NSWReimbursementAuthorisation)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetNSWReimbursementAuthorisation(dr As SafeDataReader) As NSWReimbursementAuthorisation

      Dim n As New NSWReimbursementAuthorisation()
      n.Fetch(dr)
      Return n

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(NSWReimbursementAuthorisationIDProperty, .GetInt32(0))
          LoadProperty(MRMonthIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(AuthorisedHoursProperty, .GetDecimal(3))
          LoadProperty(AuthorisedNoOfMealsProperty, .GetInt32(4))
          LoadProperty(AuthorisedReimbursementAmountProperty, .GetDecimal(5))
          LoadProperty(AuthorisedIDProperty, .GetInt32(6))
          LoadProperty(AuthorisedDateTimeProperty, .GetValue(7))
          LoadProperty(AuthorisedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(FirstNameProperty, .GetString(9))
          LoadProperty(SurnameProperty, .GetString(10))
          LoadProperty(EmployeeCodeIDnoProperty, .GetString(11))
          LoadProperty(NoEntriesProperty, .GetInt32(12))
          LoadProperty(TimeAuthorisedByProperty, .GetString(13))
          LoadProperty(SpecialMealsProperty, .GetInt32(14))
          LoadProperty(ICRMealsProperty, .GetInt32(15))
          LoadProperty(TotalMealsProperty, .GetInt32(16))
          LoadProperty(RejectedReasonProperty, .GetString(17))
          LoadProperty(SystemIDProperty, .GetValue(18))
          LoadProperty(ProductionAreaIDProperty, .GetValue(19))

        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insNSWReimbursementAuthorisation"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updNSWReimbursementAuthorisation"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramNSWReimbursementAuthorisationID As SqlParameter = .Parameters.Add("@NSWReimbursementAuthorisationID", SqlDbType.Int)
          paramNSWReimbursementAuthorisationID.Value = GetProperty(NSWReimbursementAuthorisationIDProperty)
          If Me.IsNew Then
            paramNSWReimbursementAuthorisationID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@MRMonthID", GetProperty(MRMonthIDProperty))
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@AuthorisedHours", GetProperty(AuthorisedHoursProperty))
          .Parameters.AddWithValue("@AuthorisedMealVouchers", GetProperty(AuthorisedNoOfMealsProperty))
          .Parameters.AddWithValue("@AuthorisedReimbursementAmount", GetProperty(AuthorisedReimbursementAmountProperty))
          .Parameters.AddWithValue("@AuthorisedID", GetProperty(AuthorisedIDProperty))
          .Parameters.AddWithValue("@AuthorisedDateTime", (New SmartDate(GetProperty(AuthorisedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@AuthorisedByUserID", Singular.Misc.NothingDBNull(GetProperty(AuthorisedByUserIDProperty)))
          .Parameters.AddWithValue("@RejectedReason", GetProperty(RejectedReasonProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(NSWReimbursementAuthorisationIDProperty, paramNSWReimbursementAuthorisationID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delNSWReimbursementAuthorisation"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@NSWReimbursementAuthorisationID", GetProperty(NSWReimbursementAuthorisationIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace