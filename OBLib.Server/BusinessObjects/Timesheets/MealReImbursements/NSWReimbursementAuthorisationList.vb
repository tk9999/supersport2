﻿' Generated 11 Sep 2014 12:51 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class NSWReimbursementAuthorisationList
    Inherits OBBusinessListBase(Of NSWReimbursementAuthorisationList, NSWReimbursementAuthorisation)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As NSWReimbursementAuthorisation

      For Each child As NSWReimbursementAuthorisation In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "NSW Reimbursement Authorisations"

    End Function

    'Public Function StatusSummary() As String

    '  Return String.Format("Pending: {0},<br> Authorised: {1},<br> Rejected: {2}", PendingSummary, AuthorisedSummary, RejectedSummary)

    'End Function

    Public Function AuthorisedCount() As String

      Return GetAuthCount(CommonData.Enums.NSWAuthoriseStatus.Authorised).ToString  

    End Function


    Public Function AuthorisedAmount() As String

      Return  GetAuthAmount(CommonData.Enums.NSWAuthoriseStatus.Authorised)

    End Function

    Public Function PendingCount() As String


      Return GetAuthCount(CommonData.Enums.NSWAuthoriseStatus.Pending).ToString  
    End Function

    Public Function PendingAmount() As String


      Return GetAuthAmount(CommonData.Enums.NSWAuthoriseStatus.Pending)

    End Function

    Public Function RejectedCount() As String


      Return GetAuthCount(CommonData.Enums.NSWAuthoriseStatus.Rejected)

    End Function

    Public Function RejectedAmount() As String


      Return GetAuthAmount(CommonData.Enums.NSWAuthoriseStatus.Rejected)


    End Function

    Public Function GetAuthCount(AuthStatusID As Integer) As Integer

      Return Me.Where(Function(c) Singular.Misc.CompareSafe(c.AuthorisedID, AuthStatusID)).Count

    End Function

    Public Function GetAuthAmount(AuthStatusID As Integer) As String

      Dim amnt As Decimal = 0.0
      amnt = Me.Where(Function(c) Singular.Misc.CompareSafe(c.AuthorisedID, AuthStatusID)).Sum(Function(d) d.AuthorisedReimbursementAmount)

      Return "R " & amnt.ToString("# ##0.00")

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewNSWReimbursementAuthorisationList() As NSWReimbursementAuthorisationList

      Return New NSWReimbursementAuthorisationList()

    End Function

    Public Shared Sub BeginGetNSWReimbursementAuthorisationList(CallBack As EventHandler(Of DataPortalResult(Of NSWReimbursementAuthorisationList)))

      Dim dp As New DataPortal(Of NSWReimbursementAuthorisationList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetNSWReimbursementAuthorisationList() As NSWReimbursementAuthorisationList

      Return DataPortal.Fetch(Of NSWReimbursementAuthorisationList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(NSWReimbursementAuthorisation.GetNSWReimbursementAuthorisation(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As NSWReimbursementAuthorisation = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.NSWReimbursementAuthorisationDetailList.RaiseListChangedEvents = False
          parent.NSWReimbursementAuthorisationDetailList.Add(NSWReimbursementAuthorisationDetail.GetNSWReimbursementAuthorisationDetail(sdr))
          parent.NSWReimbursementAuthorisationDetailList.RaiseListChangedEvents = True
        End While
      End If

      Me.AllowNew = False
      Me.AllowRemove = False
      For Each child As NSWReimbursementAuthorisation In Me

        child.NSWReimbursementAuthorisationDetailList.AllowNew = False
        child.NSWReimbursementAuthorisationDetailList.AllowRemove = False
      Next
    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getNSWReimbursementAuthorisationList" 
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As NSWReimbursementAuthorisation In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As NSWReimbursementAuthorisation In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace