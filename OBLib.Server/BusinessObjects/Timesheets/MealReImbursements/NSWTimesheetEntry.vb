﻿' Generated 09 Sep 2014 16:27 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.NSWTimesheets.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class NSWTimesheetEntry
    Inherits OBBusinessBase(Of NSWTimesheetEntry)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared NSWTimesheetEntryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NSWTimesheetEntryID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property NSWTimesheetEntryID() As Integer
      Get
        Return GetProperty(NSWTimesheetEntryIDProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Now.Date) _
                                                                                                        .AddSetExpression("SetEndDates(self)")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date required"), Singular.DataAnnotations.DateField(MaxDateProperty:="ViewModel.MaxDate()", MinDateProperty:="ViewModel.StartDate()")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    Required(ErrorMessage:="End Date Time required"), Singular.DataAnnotations.DateField(MinDateProperty:="ViewModel.StartDate()", MaxDateProperty:="ViewModel.MaxDate()")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared NSWTimesheetCategoryIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NSWTimesheetCategoryID, "NSW Timesheet Category", Nothing)
    ''' <summary>
    ''' Gets and sets the NSW Timesheet Category value
    ''' </summary>
    <Display(Name:="Category", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(NSWTimesheetCategoryList)),
    Required(ErrorMessage:="Category required")>
    Public Property NSWTimesheetCategoryID() As Integer?
      Get
        Return GetProperty(NSWTimesheetCategoryIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NSWTimesheetCategoryIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:=""), _
    Singular.DataAnnotations.DropDownWeb(GetType(RONSWOpenMonthProductionList), FilterMethodName:="FilterProductionWeekly", Source:=DropDownWeb.SourceType.ViewModel)>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:=""),
    StringLength(512, ErrorMessage:="Description cannot be more than 512 characters"), _
    Required(ErrorMessage:="Description required")>
    Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared RejectedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RejectedReason, "Rejected Reason", "")
    ''' <summary>
    ''' Gets and sets the RejectedReason value
    ''' </summary>
    <Display(Name:="Rejected Reason", Description:="")>
    Public ReadOnly Property RejectedReason() As String
      Get
        Return GetProperty(RejectedReasonProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared AuthorisedIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuthorisedID, "Authorised", 0)
    ''' <summary>
    ''' Gets and sets the Authorised value
    ''' </summary>
    <Display(Name:="Authorised", Description:="0 = Pending, 1 = Authorised, 2 = Rejected")>
    Public Property AuthorisedID() As Integer
      Get
        Return GetProperty(AuthorisedIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AuthorisedIDProperty, Value)
      End Set
    End Property

    Public Shared StatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Status, "Status", "Pending")
    ''' <summary>
    ''' Gets and sets the Authorised value
    ''' </summary>
    <Display(Name:="Status")>
    Public ReadOnly Property Status() As String
      Get
        Select Case AuthorisedID
          Case 1
            Return "Authorised"
          Case 2
            Return "Rejected"
          Case 0
            Return "Pending"
        End Select
        Return "Pending"
      End Get
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, "Authorised Date Time")
    ''' <summary>
    ''' Gets and sets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Authorised Date Time", Description:="")>
    Public Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(AuthorisedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AuthorisedByUserID, "Authorised By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised By User", Description:="")>
    Public Property AuthorisedByUserID() As Integer?
      Get
        Return GetProperty(AuthorisedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AuthorisedByUserIDProperty, Value)
      End Set
    End Property

 

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(NSWTimesheetEntryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Description.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "NSW Timesheet Entry")
        Else
          Return String.Format("Blank {0}", "NSW Timesheet Entry")
        End If
      Else
        Return Me.Description
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      AddWebRule(StartDateTimeProperty, Function(c) c.StartDateTime >= c.EndDateTime, Function(c) "Start Date must be less than end date")

      With AddWebRule(StartDateTimeProperty)
        .JavascriptRuleFunctionName = "CheckDates"
        .ServerRuleFunction = Function(tse)
                                Dim list = tse.FindParent(Of NSWTimesheetEntryList)()
                                If list IsNot Nothing Then
                                  For Each itm As NSWTimesheetEntry In list
                                    If itm.Guid <> tse.Guid Then
                                      If (tse.StartDateTime > itm.StartDateTime AndAlso tse.StartDateTime < itm.EndDateTime) _
                                          OrElse (tse.EndDateTime > itm.StartDateTime AndAlso tse.EndDateTime < itm.EndDateTime) _
                                          OrElse Singular.Misc.CompareSafe(tse.StartDateTime, itm.StartDateTime) AndAlso Singular.Misc.CompareSafe(tse.EndDateTime, itm.EndDateTime) Then
                                        Return "You have already captured a timesheet entry between " + CType(itm.StartDateTime, Date).ToString("dd MMM yyyy - HH:mm") + " & " + CType(itm.EndDateTime, Date).ToString("dd MMM yyyy - HH:mm")

                                      End If
                                    End If
                                  Next
                                End If
                                Return ""
                              End Function

        .AffectedProperties.Add(EndDateTimeProperty)
        .AddTriggerProperty(EndDateTimeProperty)
      End With

      With AddWebRule(ProductionIDProperty)
        .JavascriptRuleFunctionName = "CheckProduction"
        .ServerRuleFunction = Function(ts)
                                Dim Cat = CommonData.Lists.NSWTimesheetCategoryList.GetItem(ts.NSWTimesheetCategoryID)
                                If Cat IsNot Nothing Then
                                  If Cat.ProductionRelatedInd AndAlso Singular.Misc.IsNullNothing(ts.ProductionID, True) Then
                                    Return "Production is required"
                                  End If
                                End If
                                Return ""

                              End Function
        .AffectedProperties.Add(NSWTimesheetCategoryIDProperty)
        .AddTriggerProperty(NSWTimesheetCategoryIDProperty)
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewNSWTimesheetEntry() method.
			HumanResourceID = OBLib.Security.OBWebSecurity.CurrentIdentity.HumanResourceID
    End Sub

    Public Shared Function NewNSWTimesheetEntry() As NSWTimesheetEntry

      Return DataPortal.CreateChild(Of NSWTimesheetEntry)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetNSWTimesheetEntry(dr As SafeDataReader) As NSWTimesheetEntry

      Dim n As New NSWTimesheetEntry()
      n.Fetch(dr)
      Return n

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(NSWTimesheetEntryIDProperty, .GetInt32(0))
          LoadProperty(StartDateTimeProperty, .GetValue(1))
          LoadProperty(EndDateTimeProperty, .GetValue(2))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(NSWTimesheetCategoryIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(DescriptionProperty, .GetString(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(AuthorisedIDProperty, .GetInt32(9))
          LoadProperty(AuthorisedDateTimeProperty, .GetValue(10))
          LoadProperty(AuthorisedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11))) 
          LoadProperty(RejectedReasonProperty, .GetString(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insNSWTimesheetEntry"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updNSWTimesheetEntry"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty AndAlso AuthorisedID <> 1 Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramNSWTimesheetEntryID As SqlParameter = .Parameters.Add("@NSWTimesheetEntryID", SqlDbType.Int)
          paramNSWTimesheetEntryID.Value = GetProperty(NSWTimesheetEntryIDProperty)
          If Me.IsNew Then
            paramNSWTimesheetEntryID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@NSWTimesheetCategoryID", GetProperty(NSWTimesheetCategoryIDProperty))
          .Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(GetProperty(ProductionIDProperty)))
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          .Parameters.AddWithValue("@CreatedBy", GetProperty(CreatedByProperty))
          .Parameters.AddWithValue("@AuthorisedID", GetProperty(AuthorisedIDProperty))
          .Parameters.AddWithValue("@AuthorisedDateTime", (New SmartDate(GetProperty(AuthorisedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@AuthorisedByUserID", Singular.Misc.NothingDBNull(GetProperty(AuthorisedByUserIDProperty))) 
           
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(NSWTimesheetEntryIDProperty, paramNSWTimesheetEntryID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delNSWTimesheetEntry"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@NSWTimesheetEntryID", GetProperty(NSWTimesheetEntryIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace