﻿' Generated 11 Sep 2014 08:03 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class RONSWInfoList
    Inherits OBReadOnlyListBase(Of RONSWInfoList, RONSWInfo)

#Region " Business Methods "

    Public Function GetItem(InfoID As Integer) As RONSWInfo

      For Each child As RONSWInfo In Me
        If child.InfoID = InfoID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRONSWInfoList() As RONSWInfoList

      Return New RONSWInfoList()

    End Function

    Public Shared Sub BeginGetRONSWInfoList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RONSWInfoList)))

      Dim dp As New DataPortal(Of RONSWInfoList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRONSWInfoList(CallBack As EventHandler(Of DataPortalResult(Of RONSWInfoList)))

      Dim dp As New DataPortal(Of RONSWInfoList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRONSWInfoList() As RONSWInfoList

      Return DataPortal.Fetch(Of RONSWInfoList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RONSWInfo.GetRONSWInfo(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRONSWInfoList"
            cm.Parameters.AddWithValue("@HumanResourceID", OBLib.Security.Settings.CurrentUser.HumanResourceID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace