﻿' Generated 22 Dec 2014 08:05 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets.ReadOnly

  <Serializable()> _
  Public Class ROMRMonth
    Inherits OBReadOnlyBase(Of ROMRMonth)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared MRMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MRMonthID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property MRMonthID() As Integer
      Get
        Return GetProperty(MRMonthIDProperty)
      End Get
    End Property

    Public Shared MonthStartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.MonthStartDate, "Month Start Date")
    ''' <summary>
    ''' Gets the Month Start Date value
    ''' </summary>
    <Display(Name:="Month Start Date", Description:="")>
  Public ReadOnly Property MonthStartDate As DateTime?
      Get
        Return GetProperty(MonthStartDateProperty)
      End Get
    End Property

    Public Shared MonthEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.MonthEndDate, "Month End Date")
    ''' <summary>
    ''' Gets the Month End Date value
    ''' </summary>
    <Display(Name:="Month End Date", Description:="")>
  Public ReadOnly Property MonthEndDate As DateTime?
      Get
        Return GetProperty(MonthEndDateProperty)
      End Get
    End Property

    Public Shared ManuallyClosedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ManuallyClosedDateTime, "Manually Closed Date Time")
    ''' <summary>
    ''' Gets the Manually Closed Date Time value
    ''' </summary>
    <Display(Name:="Manually Closed Date Time", Description:="")>
  Public ReadOnly Property ManuallyClosedDateTime As DateTime?
      Get
        Return GetProperty(ManuallyClosedDateTimeProperty)
      End Get
    End Property

    Public Shared ManuallyClosedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ManuallyClosedBy, "Manually Closed By", 0)
    ''' <summary>
    ''' Gets the Manually Closed By value
    ''' </summary>
    <Display(Name:="Manually Closed By", Description:="")>
  Public ReadOnly Property ManuallyClosedBy() As Integer
      Get
        Return GetProperty(ManuallyClosedByProperty)
      End Get
    End Property

    Public Shared AutoClosedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AutoClosedDateTime, "Auto Closed Date Time")
    ''' <summary>
    ''' Gets the Auto Closed Date Time value
    ''' </summary>
    <Display(Name:="Auto Closed Date Time", Description:="")>
  Public ReadOnly Property AutoClosedDateTime As DateTime?
      Get
        Return GetProperty(AutoClosedDateTimeProperty)
      End Get
    End Property

    Public Shared AutoCloseReopenedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AutoCloseReopenedBy, "Auto Close Reopened By", 0)
    ''' <summary>
    ''' Gets the Auto Close Reopened By value
    ''' </summary>
    <Display(Name:="Auto Close Reopened By", Description:="")>
  Public ReadOnly Property AutoCloseReopenedBy() As Integer
      Get
        Return GetProperty(AutoCloseReopenedByProperty)
      End Get
    End Property

    Public Shared MonthNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MonthName, "Month", "")
    ''' <summary>
    ''' Gets the Month value
    ''' </summary>
    <Display(Name:="Month", Description:="")>
    Public ReadOnly Property MonthName() As String
      Get
        Return GetProperty(MonthNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(MRMonthIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.MRMonthID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROMRMonth(dr As SafeDataReader) As ROMRMonth

      Dim r As New ROMRMonth()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(MRMonthIDProperty, .GetInt32(0))
        LoadProperty(MonthStartDateProperty, .GetValue(1))
        LoadProperty(MonthEndDateProperty, .GetValue(2))
        LoadProperty(ManuallyClosedDateTimeProperty, .GetValue(3))
        LoadProperty(ManuallyClosedByProperty, .GetInt32(4))
        LoadProperty(AutoClosedDateTimeProperty, .GetValue(5))
        LoadProperty(AutoCloseReopenedByProperty, .GetInt32(6))
        LoadProperty(MonthNameProperty, .GetString(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace