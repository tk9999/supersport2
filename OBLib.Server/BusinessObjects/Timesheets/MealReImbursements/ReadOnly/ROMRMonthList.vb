﻿' Generated 22 Dec 2014 08:05 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets.ReadOnly

  <Serializable()> _
  Public Class ROMRMonthList
    Inherits OBReadOnlyListBase(Of ROMRMonthList, ROMRMonth)

#Region " Business Methods "

    Public Function GetItem(MRMonthID As Integer) As ROMRMonth

      For Each child As ROMRMonth In Me
        If child.MRMonthID = MRMonthID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "MR Months"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property PastNoMonths As Integer? = Nothing
      Public Property FetchCurrentAndPrevOpenMonths As Boolean? = CType(Nothing, Boolean?)

      Public Sub New()


      End Sub

      Public Sub New(PastNoMonths As Integer?)

        Me.PastNoMonths = PastNoMonths

      End Sub

      Public Sub New(FetchCurrentAndPrevOpenMonths As Boolean)

        Me.FetchCurrentAndPrevOpenMonths = FetchCurrentAndPrevOpenMonths

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROMRMonthList() As ROMRMonthList

      Return New ROMRMonthList()

    End Function

    Public Shared Sub BeginGetROMRMonthList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROMRMonthList)))

      Dim dp As New DataPortal(Of ROMRMonthList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROMRMonthList(CallBack As EventHandler(Of DataPortalResult(Of ROMRMonthList)))

      Dim dp As New DataPortal(Of ROMRMonthList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROMRMonthList() As ROMRMonthList

      Return DataPortal.Fetch(Of ROMRMonthList)(New Criteria())

    End Function

    Public Shared Function GetROMRMonthList(PastNoMonths As Integer) As ROMRMonthList

      Return DataPortal.Fetch(Of ROMRMonthList)(New Criteria(PastNoMonths))

    End Function

    Public Shared Function GetROMRMonthList(FetchCurrentAndPrevOpenMonths As Boolean) As ROMRMonthList

      Return DataPortal.Fetch(Of ROMRMonthList)(New Criteria(FetchCurrentAndPrevOpenMonths))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROMRMonth.GetROMRMonth(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROMRMonthList"
            cm.Parameters.AddWithValue("@PastNoMonths", Singular.Misc.NothingDBNull(crit.PastNoMonths))
            cm.Parameters.AddWithValue("@FetchCurrentAndPrevOpenMonths", Singular.Misc.NothingDBNull(crit.FetchCurrentAndPrevOpenMonths))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace