﻿' Generated 11 Sep 2014 08:03 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class RONSWInfo
    Inherits OBReadOnlyBase(Of RONSWInfo)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared InfoIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.InfoID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property InfoID() As Integer
      Get
        Return GetProperty(InfoIDProperty)
      End Get
    End Property

    Public Shared IsNSWManagerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsNSWManager, "Is NSW Manager", 0)
    ''' <summary>
    ''' Gets the Is NSW Manager value
    ''' </summary>
    <Display(Name:="Is NSW Manager", Description:="")>
  Public ReadOnly Property IsNSWManager() As Boolean
      Get
        Return GetProperty(IsNSWManagerProperty)
      End Get
    End Property

    Public Shared IsNSWIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsNSWInd, "Is NSW", 0)
    ''' <summary>
    ''' Gets the Is NSW value
    ''' </summary>
    <Display(Name:="Is NSW", Description:="")>
    Public ReadOnly Property IsNSWInd() As Boolean
      Get
        Return GetProperty(IsNSWIndProperty)
      End Get
    End Property

    Public Shared OpenMonthStartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OpenMonthStartDate, "Open Month Start Date")
    ''' <summary>
    ''' Gets the Open Month Start Date value
    ''' </summary>
    <Display(Name:="Open Month Start Date", Description:="")>
  Public ReadOnly Property OpenMonthStartDate As DateTime?
      Get
        Return GetProperty(OpenMonthStartDateProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(InfoIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.OpenMonthStartDate.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRONSWInfo(dr As SafeDataReader) As RONSWInfo

      Dim r As New RONSWInfo()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(InfoIDProperty, .GetInt32(0))
        LoadProperty(IsNSWManagerProperty, .GetBoolean(1))
        LoadProperty(IsNSWIndProperty, .GetBoolean(2))
        LoadProperty(OpenMonthStartDateProperty, .GetValue(3))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace