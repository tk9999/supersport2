﻿' Generated 10 Sep 2014 15:51 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class NSWTimesheetsAuthorisationEntryList
    Inherits OBBusinessListBase(Of NSWTimesheetsAuthorisationEntryList, NSWTimesheetsAuthorisationEntry)

#Region " Business Methods "

    Public Function GetItem(NSWTimesheetEntryID As Int32) As NSWTimesheetsAuthorisationEntry

      For Each child As NSWTimesheetsAuthorisationEntry In Me
        If child.NSWTimesheetEntryID = NSWTimesheetEntryID Then
          Return child
        End If
      Next
      Return Nothing

    End Function
     

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewNSWTimesheetsAuthorisationEntryList() As NSWTimesheetsAuthorisationEntryList

      Return New NSWTimesheetsAuthorisationEntryList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As NSWTimesheetsAuthorisationEntry In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As NSWTimesheetsAuthorisationEntry In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace