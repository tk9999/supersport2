﻿' Generated 09 Sep 2014 15:07 - Singular Systems Object Generator Version 2.1.674
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class NSWTimesheetCategory
    Inherits OBBusinessBase(Of NSWTimesheetCategory)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared NSWTimesheetCategoryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NSWTimesheetCategoryID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property NSWTimesheetCategoryID() As Integer
      Get
        Return GetProperty(NSWTimesheetCategoryIDProperty)
      End Get
    End Property

    Public Shared TimesheetCategoryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetCategory, "Timesheet Category", "")
    ''' <summary>
    ''' Gets and sets the Timesheet Category value
    ''' </summary>
    <Display(Name:="Timesheet Category", Description:="Description of the Timesheet Category"),
    StringLength(50, ErrorMessage:="Timesheet Category cannot be more than 50 characters"), _
      Required(ErrorMessage:="Category is required")>
    Public Property TimesheetCategory() As String
      Get
        Return GetProperty(TimesheetCategoryProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TimesheetCategoryProperty, Value)
      End Set
    End Property

    Public Shared ProductionRelatedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ProductionRelatedInd, "Production Related", False)
    ''' <summary>
    ''' Gets and sets the Production Related value
    ''' </summary>
    <Display(Name:="Production Related", Description:="Tick indicates that a production is required for Timesheet Entries of this category"),
    Required(ErrorMessage:="Production Related required")>
    Public Property ProductionRelatedInd() As Boolean
      Get
        Return GetProperty(ProductionRelatedIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ProductionRelatedIndProperty, Value)
      End Set
    End Property

    Public Shared CategoryOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CategoryOrder, "Order", 0)
    ''' <summary>
    ''' Gets and sets the Production Related value
    ''' </summary>
    <Display(Name:="Order", Description:="Order to sort the Categories")>
    Public Property CategoryOrder() As Integer
      Get
        Return GetProperty(CategoryOrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CategoryOrderProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(NSWTimesheetCategoryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.TimesheetCategory.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "NSW Timesheet Category")
        Else
          Return String.Format("Blank {0}", "NSW Timesheet Category")
        End If
      Else
        Return Me.TimesheetCategory
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewNSWTimesheetCategory() method.

    End Sub

    Public Shared Function NewNSWTimesheetCategory() As NSWTimesheetCategory

      Return DataPortal.CreateChild(Of NSWTimesheetCategory)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetNSWTimesheetCategory(dr As SafeDataReader) As NSWTimesheetCategory

      Dim n As New NSWTimesheetCategory()
      n.Fetch(dr)
      Return n

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(NSWTimesheetCategoryIDProperty, .GetInt32(0))
          LoadProperty(TimesheetCategoryProperty, .GetString(1))
          LoadProperty(ProductionRelatedIndProperty, .GetBoolean(2))
          LoadProperty(CategoryOrderProperty, .GetInt32(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insNSWTimesheetCategory"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updNSWTimesheetCategory"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramNSWTimesheetCategoryID As SqlParameter = .Parameters.Add("@NSWTimesheetCategoryID", SqlDbType.Int)
          paramNSWTimesheetCategoryID.Value = GetProperty(NSWTimesheetCategoryIDProperty)
          If Me.IsNew Then
            paramNSWTimesheetCategoryID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@TimesheetCategory", GetProperty(TimesheetCategoryProperty))
          .Parameters.AddWithValue("@ProductionRelatedInd", GetProperty(ProductionRelatedIndProperty))
          .Parameters.AddWithValue("@CategoryOrder", GetProperty(CategoryOrderProperty))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(NSWTimesheetCategoryIDProperty, paramNSWTimesheetCategoryID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delNSWTimesheetCategory"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@NSWTimesheetCategoryID", GetProperty(NSWTimesheetCategoryIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace