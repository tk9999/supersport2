﻿' Generated 09 Sep 2014 15:07 - Singular Systems Object Generator Version 2.1.674
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class MRMonthList
    Inherits OBBusinessListBase(Of MRMonthList, MRMonth)

#Region " Business Methods "

    Public Function GetItem(MRMonthID As Integer) As MRMonth

      For Each child As MRMonth In Me
        If child.MRMonthID = MRMonthID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetMonthBy(mDate As Date) As MRMonth

      For Each child As MRMonth In Me

        If CType(child.MonthStartDate, Date) <= mDate AndAlso CType(child.MonthEndDate, Date) >= mDate Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "NSW Reimbursement Months"

    End Function

    Public Function GetOpenStartDate() As DateTime

      Dim olist As List(Of MRMonth) = Me.Where(Function(c) c.ManuallyClosedDateTime Is Nothing AndAlso c.AutoClosedDateTime Is Nothing).ToList
      Dim rm = olist.Min(Function(o) o.MonthStartDate)
      Return rm

    End Function

    Public Function GetOpenMonth() As MRMonth

      Dim obj As MRMonth = GetMonthBy(GetOpenStartDate)

      Return obj

    End Function

    Public Sub CloseOpenMonth()
      Dim CurrMnth = GetOpenMonth()
      CurrMnth.ManuallyClosedBy = OBLib.Security.Settings.CurrentUserID
      CurrMnth.ManuallyClosedDateTime = Now

      Dim NextMonth = Me.GetMonthBy(CType(CurrMnth.MonthEndDate, Date).AddDays(1))
      If NextMonth Is Nothing Then
        NextMonth = AddNextMonth(CurrMnth)
        Me.Add(NextMonth)
      Else
        ' add another month to keep 2 months  ahead in DB
        Dim NextMonth2 = Me.GetMonthBy(CType(NextMonth.MonthEndDate, Date).AddDays(1))
        If NextMonth2 Is Nothing Then
          NextMonth2 = AddNextMonth(NextMonth)
          Me.Add(NextMonth2)
        End If
        NextMonth.ManuallyClosedBy = Nothing
        NextMonth.ManuallyClosedDateTime = Nothing
        End If
    End Sub

    Private Function AddNextMonth(currMonth As MRMonth) As MRMonth
      Dim NextMonth = New MRMonth
      Dim StartDate = CType(currMonth.MonthEndDate, Date).AddDays(1)
      StartDate = New Date(StartDate.Year, StartDate.Month, CommonData.Enums.MRMonthDay.StartDay)

      Dim EndDate = CType(currMonth.MonthEndDate, Date).AddMonths(1)
      EndDate = New Date(EndDate.Year, EndDate.Month, CommonData.Enums.MRMonthDay.EndDay)

      NextMonth.MonthStartDate = StartDate
      NextMonth.MonthEndDate = EndDate
      Return NextMonth
    End Function

    Public Function OpenManuallyPrevMonth() As Boolean
      Dim CurrMnth = GetOpenMonth()
      'CurrMnth.ManuallyClosedBy = OBLib.Security.Settings.CurrentUserID
      'CurrMnth.ManuallyClosedDateTime = Now

      Dim PrevMonth = Me.GetMonthBy(CType(CurrMnth.MonthStartDate, Date).AddMonths(-1))
      If PrevMonth IsNot Nothing Then
        If PrevMonth.AutoClosedDateTime Is Nothing Then
          PrevMonth.ManuallyClosedDateTime = Nothing
          PrevMonth.ManuallyClosedBy = Nothing
        Else
          Return True ' 
        End If
      End If
      Return False
    End Function

    Public Sub OpenAutoClosedPrevMonth()
      Dim CurrMnth = GetOpenMonth()

      Dim PrevMonth = Me.GetMonthBy(CType(CurrMnth.MonthStartDate, Date).AddMonths(-1))
      If PrevMonth IsNot Nothing Then

        PrevMonth.ManuallyClosedDateTime = Nothing
        PrevMonth.ManuallyClosedBy = Nothing

        PrevMonth.AutoClosedDateTime = Nothing
        PrevMonth.AutoCloseReopenedBy = Nothing

      End If 
    End Sub

    Public Function CanReOpenMonth() As Boolean

      Dim PrevMonth = Me.GetMonthBy(CType(GetOpenMonth().MonthStartDate, Date).AddMonths(-1))
      If PrevMonth Is Nothing Then Return False
      Dim dMonth = Me.GetMonthBy(Now.AddMonths(-1)) ' can Only go back One month
      Dim ddate = New Date(CType(GetOpenMonth().MonthEndDate, Date).Year, CType(GetOpenMonth().MonthEndDate, Date).Month, 3)
      If PrevMonth.MonthEndDate < Now.Date AndAlso Now.Date <= ddate Then
        Return True
      End If
      Return False

 

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewMRMonthList() As MRMonthList

      Return New MRMonthList()

    End Function

    Public Shared Sub BeginGetMRMonthList(CallBack As EventHandler(Of DataPortalResult(Of MRMonthList)))

      Dim dp As New DataPortal(Of MRMonthList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetMRMonthList() As MRMonthList

      Return DataPortal.Fetch(Of MRMonthList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(MRMonth.GetMRMonth(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getMRMonthList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As MRMonth In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As MRMonth In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region



  End Class

End Namespace