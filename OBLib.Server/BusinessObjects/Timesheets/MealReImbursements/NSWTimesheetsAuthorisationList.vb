﻿' Generated 10 Sep 2014 15:51 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class NSWTimesheetsAuthorisationList
    Inherits OBBusinessListBase(Of NSWTimesheetsAuthorisationList, NSWTimesheetsAuthorisation)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Int32) As NSWTimesheetsAuthorisation

      For Each child As NSWTimesheetsAuthorisation In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

 

    Public Overrides Function ToString() As String

      Return "s"

    End Function

    Public Function GetNSWTimesheetsAuthorisationEntry(NSWTimesheetEntryID As Int32) As NSWTimesheetsAuthorisationEntry

      Dim obj As NSWTimesheetsAuthorisationEntry = Nothing
      For Each parent As NSWTimesheetsAuthorisation In Me
        obj = parent.NSWTimesheetsAuthorisationEntryList.GetItem(NSWTimesheetEntryID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewNSWTimesheetsAuthorisationList() As NSWTimesheetsAuthorisationList

      Return New NSWTimesheetsAuthorisationList()

    End Function

    Public Shared Sub BeginGetNSWTimesheetsAuthorisationList(CallBack As EventHandler(Of DataPortalResult(Of NSWTimesheetsAuthorisationList)))

      Dim dp As New DataPortal(Of NSWTimesheetsAuthorisationList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Class MRValues
      Property NoOfMeals As Integer = 0
      Property MRAmount As Decimal = 0
      Property SyncID As Integer
    End Class

    Public Shared Function GetReimbursementValues(HRID As Integer, JSon As String, SyncID As Integer) As MRValues
      Dim mResults As New MRValues

      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.[GetCalcNSWMealReimbursementValues]"
            cm.Parameters.AddWithValue("@HumanResourceID", HRID)
            cm.Parameters.AddWithValue("@JsonOverridingAuthorisations", JSon)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              If sdr.Read Then
                mResults.NoOfMeals = sdr.GetInt32(1)
                mResults.MRAmount = sdr.GetDecimal(2)

              End If
            End Using
          End Using
        Finally
          cn.Close()
        End Try

      End Using

      mResults.SyncID = SyncID
      Return mResults
    End Function

    Public Shared Function GetNSWTimesheetsAuthorisationList() As NSWTimesheetsAuthorisationList

      Return DataPortal.Fetch(Of NSWTimesheetsAuthorisationList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(NSWTimesheetsAuthorisation.GetNSWTimesheetsAuthorisation(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As NSWTimesheetsAuthorisation = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.NSWTimesheetsAuthorisationEntryList.RaiseListChangedEvents = False
          parent.NSWTimesheetsAuthorisationEntryList.Add(NSWTimesheetsAuthorisationEntry.GetNSWTimesheetsAuthorisationEntry(sdr))
          parent.NSWTimesheetsAuthorisationEntryList.RaiseListChangedEvents = True
        End While
      End If

      Me.AllowNew = False
      Me.AllowRemove = False
      For Each child As NSWTimesheetsAuthorisation In Me
        child.CheckRules()
        child.NSWTimesheetsAuthorisationEntryList.AllowNew = False
        child.NSWTimesheetsAuthorisationEntryList.AllowRemove = False
        For Each NSWTimesheetsAuthorisationEntry As NSWTimesheetsAuthorisationEntry In child.NSWTimesheetsAuthorisationEntryList
          NSWTimesheetsAuthorisationEntry.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getNSWTimesheetsAuthorisationList"
            cm.Parameters.AddWithValue("@ManagerHumanResourceID", OBLib.Security.Settings.CurrentUser.HumanResourceID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As NSWTimesheetsAuthorisation In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As NSWTimesheetsAuthorisation In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace