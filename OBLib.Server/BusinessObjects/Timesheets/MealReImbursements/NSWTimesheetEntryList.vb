﻿' Generated 09 Sep 2014 16:27 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class NSWTimesheetEntryList
    Inherits OBBusinessListBase(Of NSWTimesheetEntryList, NSWTimesheetEntry)

#Region " Business Methods "

    Public Function GetItem(NSWTimesheetEntryID As Integer) As NSWTimesheetEntry

      For Each child As NSWTimesheetEntry In Me
        If child.NSWTimesheetEntryID = NSWTimesheetEntryID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "NSW Timesheet Entrys"

    End Function
    Private mOpenStartDate As DateTime
    Public ReadOnly Property OpenStartDate As DateTime
      Get
        Return mOpenStartDate
      End Get
    End Property

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

       
      Public EndDate As Object 

      Public Sub New(EndDate As Object)
         
        Me.EndDate = EndDate
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewNSWTimesheetEntryList() As NSWTimesheetEntryList

      Return New NSWTimesheetEntryList()

    End Function

    Public Shared Sub BeginGetNSWTimesheetEntryList(CallBack As EventHandler(Of DataPortalResult(Of NSWTimesheetEntryList)))

      Dim dp As New DataPortal(Of NSWTimesheetEntryList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetNSWTimesheetEntryList() As NSWTimesheetEntryList

      Return DataPortal.Fetch(Of NSWTimesheetEntryList)(New Criteria())

    End Function
     

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(NSWTimesheetEntry.GetNSWTimesheetEntry(sdr))
      End While
      Me.RaiseListChangedEvents = True
       
      If sdr.NextResult() Then
        sdr.Read()

        mOpenStartDate = sdr.GetValue(0)
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getNSWTimesheetEntryList"  
            cm.Parameters.AddWithValue("@HumanResourceID", Security.OBWebSecurity.CurrentIdentity.HumanResourceID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As NSWTimesheetEntry In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As NSWTimesheetEntry In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace