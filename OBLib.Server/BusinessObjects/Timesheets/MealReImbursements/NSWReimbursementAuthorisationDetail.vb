﻿' Generated 10 Sep 2014 15:51 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class NSWReimbursementAuthorisationDetail
    Inherits OBBusinessBase(Of NSWReimbursementAuthorisationDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared NSWTimesheetEntryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NSWTimesheetEntryID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property NSWTimesheetEntryID() As Integer
      Get
        Return GetProperty(NSWTimesheetEntryIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared NSWReimbursementAuthorisationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NSWReimbursementAuthorisationID, "Manager Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="Manager Human Resource", Description:="")>
    Public ReadOnly Property NSWReimbursementAuthorisationID() As Integer?
      Get
        Return GetProperty(NSWReimbursementAuthorisationIDProperty)
      End Get
    End Property

    Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "First Name", "")
    ''' <summary>
    ''' Gets the First Name value
    ''' </summary>
    <Display(Name:="First Name", Description:="")>
    Public ReadOnly Property FirstName() As String
      Get
        Return GetProperty(FirstNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property


    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared HoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Hours, "Hours")
    ''' <summary>
    ''' Gets the Hours value
    ''' </summary>
    <Display(Name:="Hours", Description:=""), Singular.DataAnnotations.NumberField(Singular.Formatting.NumberFormatType.Decimals)>
    Public ReadOnly Property Hours() As Decimal
      Get
        Return GetProperty(HoursProperty)
      End Get
    End Property

    Public Shared NSWTimesheetCategoryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NSWTimesheetCategory, "Category ", "")
    ''' <summary>
    ''' Gets the NSW Timesheet Category ID 1 value
    ''' </summary>
    <Display(Name:="Category", Description:="")>
    Public ReadOnly Property NSWTimesheetCategory() As String
      Get
        Return GetProperty(NSWTimesheetCategoryProperty)
      End Get
    End Property

    Public Shared ProductionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Production, "Production", "")
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public ReadOnly Property Production() As String
      Get
        Return GetProperty(ProductionProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
    Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared AuthorisedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.AuthorisedDateTime, "Authorised Date Time")
    ''' <summary>
    ''' Gets and sets the Authorised Date Time value
    ''' </summary>
    <Display(Name:="Authorised Date", Description:="")>
    Public Property AuthorisedDateTime As DateTime?
      Get
        Return GetProperty(AuthorisedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(AuthorisedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared AuthorisedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AuthorisedBy, "Authorised By", "")
    ''' <summary>
    ''' Gets and sets the Authorised By User value
    ''' </summary>
    <Display(Name:="Authorised By", Description:="")>
    Public ReadOnly Property AuthorisedBy() As String
      Get
        Return GetProperty(AuthorisedByProperty)
      End Get
    End Property

    Public Shared MealTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MealType, "Meal Type", "")
    ''' <summary>
    ''' Gets and sets the Authorised value
    ''' </summary>
    <Display(Name:="Meal Type", Description:="")>
    Public Property MealType() As String
      Get
        Return GetProperty(MealTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MealTypeProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As NSWReimbursementAuthorisation

      Return CType(CType(Me.Parent, NSWReimbursementAuthorisationDetailList).Parent, NSWReimbursementAuthorisation)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(NSWTimesheetEntryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.FirstName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "NSW Reimbursement Authorisation Detail")
        Else
          Return String.Format("Blank {0}", "NSW Reimbursement Authorisation Detail")
        End If
      Else
        Return Me.StartDateTime.ToString & " - " & Me.EndDateTime.ToString
      End If

    End Function



#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewNSWTimesheetsAuthorisationEntry() method.

    End Sub

    Public Shared Function NewNSWReimbursementAuthorisationDetail() As NSWReimbursementAuthorisationDetail

      Return DataPortal.CreateChild(Of NSWReimbursementAuthorisationDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetNSWReimbursementAuthorisationDetail(dr As SafeDataReader) As NSWReimbursementAuthorisationDetail

      Dim n As New NSWReimbursementAuthorisationDetail()
      n.Fetch(dr)
      Return n

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(NSWTimesheetEntryIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1))) 
          LoadProperty(StartDateTimeProperty, .GetValue(2))
          LoadProperty(EndDateTimeProperty, .GetValue(3))
          LoadProperty(HoursProperty, .GetDecimal(4))
          LoadProperty(NSWTimesheetCategoryProperty, .GetString(5))
          LoadProperty(ProductionProperty, .GetString(6))
          LoadProperty(DescriptionProperty, .GetString(7))
          LoadProperty(AuthorisedDateTimeProperty, .GetValue(8))
          LoadProperty(AuthorisedByProperty, .GetString(9))
          LoadProperty(MealTypeProperty, .GetString(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insNSWReimbursementAuthorisationDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updNSWReimbursementAuthorisationDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)



    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database


    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace