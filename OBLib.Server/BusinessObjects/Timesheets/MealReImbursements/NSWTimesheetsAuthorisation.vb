﻿' Generated 10 Sep 2014 15:51 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class NSWTimesheetsAuthorisation
    Inherits OBBusinessBase(Of NSWTimesheetsAuthorisation)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceID() As Int32
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID, "Manager Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Manager Human Resource value
    ''' </summary>
    <Display(Name:="Manager Human Resource", Description:="")>
  Public ReadOnly Property ManagerHumanResourceID() As Integer?
      Get
        Return GetProperty(ManagerHumanResourceIDProperty)
      End Get
    End Property

    Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "First Name", "")
    ''' <summary>
    ''' Gets the First Name value
    ''' </summary>
    <Display(Name:="First Name", Description:="")>
  Public ReadOnly Property FirstName() As String
      Get
        Return GetProperty(FirstNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
  Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property 

    Public Shared EmployeeCodeIDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCodeIDNo, "Employee Code", "")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee No.  /  ID No.", Description:="EmployeeCode Or IDNo")>
    Public ReadOnly Property EmployeeCodeIDNo() As String
      Get
        Return GetProperty(EmployeeCodeIDNoProperty)
      End Get
    End Property

    Public Shared NoEntriesProperty As PropertyInfo(Of Int32) = RegisterProperty(Of Int32)(Function(c) c.NoEntries, "No of Entries", 0)
    ''' <summary>
    ''' Gets the No Entries value
    ''' </summary>
    <Display(Name:="No of Entries", Description:="")>
    Public ReadOnly Property NoEntries() As Int32
      Get
        Return GetProperty(NoEntriesProperty)
      End Get
    End Property

    Public Shared TotalTimeProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalTime, "Total Time", 0)
    ''' <summary>
    ''' Gets the Total Time value
    ''' </summary>
    <Display(Name:="Total Time", Description:="")>
    Public ReadOnly Property TotalTime() As Decimal
      Get
        Return GetProperty(TotalTimeProperty)
      End Get
    End Property

    Public Shared TotalPendingProperty As PropertyInfo(Of Integer) = RegisterReadOnlyProperty(Of Integer)(Function(c) c.TotalPending, "var c = 0; for(var i=0;i<self.NSWTimesheetsAuthorisationEntryList().length;i++){ if(self.NSWTimesheetsAuthorisationEntryList()[i].AuthorisedID() == 0) c++; }; return c;")
    ''' <summary>
    ''' Gets the Total Time value
    ''' </summary>
    <Display(Name:="Pending", Description:="")>
    Public ReadOnly Property TotalPending() As Integer
      Get
        Return GetProperty(TotalPendingProperty)
      End Get
    End Property

    Public Shared TotalAuthorisedProperty As PropertyInfo(Of Integer) = RegisterReadOnlyProperty(Of Integer)(Function(c) c.TotalAuthorised, "var c = 0; for(var i=0;i<self.NSWTimesheetsAuthorisationEntryList().length;i++){ if(self.NSWTimesheetsAuthorisationEntryList()[i].AuthorisedID() == 1) c++; }; return c;")
    ''' <summary>
    ''' Gets the Total Time value
    ''' </summary>
    <Display(Name:="Authorised", Description:="")>
    Public ReadOnly Property TotalAuthorised() As Integer
      Get
        Return GetProperty(TotalAuthorisedProperty)
      End Get
    End Property

    Public Shared TotalRejectedProperty As PropertyInfo(Of Integer) = RegisterReadOnlyProperty(Of Integer)(Function(c) c.TotalRejected, "var c = 0; for(var i=0;i<self.NSWTimesheetsAuthorisationEntryList().length;i++){ if(self.NSWTimesheetsAuthorisationEntryList()[i].AuthorisedID() == 2) c++; }; return c;")
    ''' <summary>
    ''' Gets the Total Time value
    ''' </summary>
    <Display(Name:="Rejected", Description:="")>
    Public ReadOnly Property TotalRejected() As Integer
      Get
        Return GetProperty(TotalRejectedProperty)
      End Get
    End Property

    Public Shared AuthorisedNoOfMealsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AuthorisedNoOfMeals, "Authorised No of Meals", 0)
    ''' <summary>
    ''' Gets and sets the Authorised Meal Vouchers value
    ''' </summary>
    <Display(Name:="No of Meals", Description:="")>
    Public Property AuthorisedNoOfMeals() As Integer
      Get
        Return GetProperty(AuthorisedNoOfMealsProperty)
      End Get
      Set(value As Integer)
        SetProperty(AuthorisedNoOfMealsProperty, value)
      End Set
    End Property

    Public Shared AuthorisedReimbursementAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AuthorisedReimbursementAmount, "Authorised Reimbursement Amount", 0)
    ''' <summary>
    ''' Gets and sets the Authorised Reimbursement Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:="")>
    Public Property AuthorisedReimbursementAmount() As Decimal
      Get
        Return GetProperty(AuthorisedReimbursementAmountProperty)
      End Get
      Set(value As Decimal)
        SetProperty(AuthorisedReimbursementAmountProperty, value)
      End Set

    End Property


    <DefaultValue(False)>
    Public Property Expanded As Boolean = False

    <Singular.DataAnnotations.ClientOnly()>
    Public Property IsLoading As Boolean = False

    <Singular.DataAnnotations.ClientOnly()>
    Public Property SyncID As Integer = 0

#End Region

#Region " Child Lists "

    Public Shared NSWTimesheetsAuthorisationEntryListProperty As PropertyInfo(Of NSWTimesheetsAuthorisationEntryList) = RegisterProperty(Of NSWTimesheetsAuthorisationEntryList)(Function(c) c.NSWTimesheetsAuthorisationEntryList, "NSW Timesheets Authorisation Entry List")

    Public ReadOnly Property NSWTimesheetsAuthorisationEntryList() As NSWTimesheetsAuthorisationEntryList
      Get
        If GetProperty(NSWTimesheetsAuthorisationEntryListProperty) Is Nothing Then
          LoadProperty(NSWTimesheetsAuthorisationEntryListProperty, NSWTimesheets.NSWTimesheetsAuthorisationEntryList.NewNSWTimesheetsAuthorisationEntryList())
        End If
        Return GetProperty(NSWTimesheetsAuthorisationEntryListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetAuthCount(AuthStatusID As Integer) As Integer

      Return NSWTimesheetsAuthorisationEntryList.Where(Function(c) Singular.Misc.CompareSafe(c.AuthorisedID, AuthStatusID)).Count

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.FirstName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "NSW Timesheets Authorisation")
        Else
          Return String.Format("Blank {0}", "NSW Timesheets Authorisation")
        End If
      Else
        Return Me.FirstName & " - " & Me.Surname
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property


#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewNSWTimesheetsAuthorisation() method.

    End Sub

    Public Shared Function NewNSWTimesheetsAuthorisation() As NSWTimesheetsAuthorisation

      Return DataPortal.CreateChild(Of NSWTimesheetsAuthorisation)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetNSWTimesheetsAuthorisation(dr As SafeDataReader) As NSWTimesheetsAuthorisation

      Dim n As New NSWTimesheetsAuthorisation()
      n.Fetch(dr)
      Return n

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceIDProperty, .GetInt32(0))
          LoadProperty(ManagerHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(FirstNameProperty, .GetString(2))
          LoadProperty(SurnameProperty, .GetString(3)) 
          LoadProperty(EmployeeCodeIDNoProperty, .GetString(4))
          LoadProperty(NoEntriesProperty, .GetInt32(5))
          LoadProperty(TotalTimeProperty, .GetDecimal(6))
          LoadProperty(AuthorisedNoOfMealsProperty, .GetInt32(7))
          LoadProperty(AuthorisedReimbursementAmountProperty, .GetDecimal(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insNSWTimesheetsAuthorisation"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updNSWTimesheetsAuthorisation"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)
       
      ' update child objects

        If GetProperty(NSWTimesheetsAuthorisationEntryListProperty) IsNot Nothing Then
          Me.NSWTimesheetsAuthorisationEntryList.Update()
      End If 

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delNSWTimesheetsAuthorisation"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace