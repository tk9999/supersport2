﻿' Generated 17 Jul 2014 16:25 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace NSWTimesheets

  <Serializable()> _
  Public Class ManagerHumanResource
    Inherits OBBusinessBase(Of ManagerHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ManagerHumanResourceID, "ID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False, Name:="Manager", Description:="")>
    Public Property ManagerHumanResourceID() As Integer
      Get
        Return GetProperty(ManagerHumanResourceIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ManagerHumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HumanResourceID, Nothing) _
                                                                         .AddSetExpression("SetHumanResourceCode(self)")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>     
    <Display(Name:="Human Resource", Description:=""),
     Required(ErrorMessage:="Human Resource required"), Singular.DataAnnotations.DropDownWeb(GetType(HR.ReadOnly.ROHumanResourceList), ValueMember:="HumanResourceID", DisplayMember:="PreferredFirstSurname")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(HumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceName, "Human Resource", "")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceName() As String
      Get
        Return GetProperty(HumanResourceNameProperty)
      End Get
    End Property

    Public Shared HumanResourceCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceCode, "Human Resource Code", "")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="")>
    Public ReadOnly Property HumanResourceCode() As String
      Get
        Return GetProperty(HumanResourceCodeProperty)
      End Get
    End Property


 

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ManagerHumanResourceID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Project Human Resource")
        Else
          Return String.Format("Blank {0}", "Project Human Resource")
        End If
      Else
        Return Me.HumanResourceName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(HumanResourceIDProperty)
        .JavascriptRuleFunctionName = "CheckUniqueHumanResourceID"
        .ServerRuleFunction = Function(mhr)
                                Dim list = mhr.FindParent(Of ManagerHumanResourceList)()
                                If list IsNot Nothing Then
                                  For Each itm As ManagerHumanResource In list
                                    If itm.Guid <> mhr.Guid Then
                                      If Singular.Misc.CompareSafe(mhr.HumanResourceID, itm.HumanResourceID) Then
                                        Return "This person has already been added"

                                      End If
                                    End If
                                  Next
                                End If
                                Return ""
                              End Function


      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewManagerHumanResource() method.

    End Sub

    Public Shared Function NewManagerHumanResource() As ManagerHumanResource

      Return DataPortal.CreateChild(Of ManagerHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetManagerHumanResource(dr As SafeDataReader) As ManagerHumanResource

      Dim p As New ManagerHumanResource()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceNameProperty, .GetString(1))
          LoadProperty(HumanResourceCodeProperty, .GetString(2))

          LoadProperty(ManagerHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))

        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insManagerHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updManagerHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

    
          .Parameters.AddWithValue("@ManagerHumanResourceID", Singular.Misc.NothingDBNull(GetProperty(ManagerHumanResourceIDProperty)))
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty)) 

          .ExecuteNonQuery()

          'If Me.IsNew Then
          '  LoadProperty(ManagerHumanResourceIDProperty, paramManagerHumanResourceID.Value)
          'End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delManagerHumanResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace