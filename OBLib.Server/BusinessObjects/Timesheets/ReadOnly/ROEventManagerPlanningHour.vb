﻿' Generated 14 Apr 2015 11:15 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc


Namespace Timesheets.OBCity.ReadOnly

  <Serializable()> _
  Public Class ROEventManagerPlanningHour
    Inherits SingularReadOnlyBase(Of ROEventManagerPlanningHour)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared ProdDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProdDescription, "Prod Description")
    ''' <summary>
    ''' Gets the Prod Description value
    ''' </summary>
    <Display(Name:="Prod Description", Description:="")>
  Public ReadOnly Property ProdDescription() As String
      Get
        Return GetProperty(ProdDescriptionProperty)
      End Get
    End Property

    Public Shared HoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Hours, "Hours")
    ''' <summary>
    ''' Gets the Hours value
    ''' </summary>
    <Display(Name:="Hours", Description:="")>
  Public ReadOnly Property Hours() As Decimal
      Get
        Return GetProperty(HoursProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProdDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEventManagerPlanningHour(dr As SafeDataReader) As ROEventManagerPlanningHour

      Dim r As New ROEventManagerPlanningHour()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionIDProperty, .GetInt32(0))
        LoadProperty(ProdDescriptionProperty, .GetString(1))
        LoadProperty(HoursProperty, .GetDecimal(2))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace