﻿' Generated 09 Oct 2014 15:43 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = false Then
Imports System.Data.SqlClient
#End If

namespace NSWTimesheets.ReadOnly

<Serializable()> _ 
public class ROManagerHumanResourceList
Inherits OBReadOnlyListBase(Of ROManagerHumanResourceList, ROManagerHumanResource)

#region " Business Methods "

public Function GetItem(HumanResourceID As integer) As ROManagerHumanResource

      For Each child As ROManagerHumanResource In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
return Nothing

End Function

Public Overrides Function ToString() As String

      Return "Human Resources "

End Function

#End Region

#region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public HumanResourceID As Integer = OBLib.Security.Settings.CurrentUser.HumanResourceID
       
      Public Sub New(HumanResourceID As Integer)

        Me.HumanResourceID = HumanResourceID

      End Sub

      Public Sub New()


      End Sub

    End Class

#region " Common "

    Public Shared Function NewROManagerHumanResourceList() As ROManagerHumanResourceList

      Return New ROManagerHumanResourceList()

    End Function

    Public Shared Sub BeginGetROManagerHumanResourceList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROManagerHumanResourceList)))

      Dim dp As New DataPortal(Of ROManagerHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROManagerHumanResourceList(CallBack As EventHandler(Of DataPortalResult(Of ROManagerHumanResourceList)))

      Dim dp As New DataPortal(Of ROManagerHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROManagerHumanResourceList(ManagerHumanResourceID As Integer) As ROManagerHumanResourceList

      Return DataPortal.Fetch(Of ROManagerHumanResourceList)(New Criteria(ManagerHumanResourceID))
       

    End Function

    Public Shared Function GetROManagerHumanResourceList() As ROManagerHumanResourceList

      Return DataPortal.Fetch(Of ROManagerHumanResourceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROManagerHumanResource.GetROManagerHumanResource(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROManagerHumanResourceList"
            cm.Parameters.AddWithValue("@ManagerHumanResourceID", crit.HumanResourceID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

End Class

End Namespace