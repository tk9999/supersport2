﻿' Generated 14 Apr 2015 11:15 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc


Namespace Timesheets.OBCity.ReadOnly

  <Serializable()> _
  Public Class ROEventManagerPlanningHourList
    Inherits SingularReadOnlyListBase(Of ROEventManagerPlanningHourList, ROEventManagerPlanningHour)

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROEventManagerPlanningHour

      For Each child As ROEventManagerPlanningHour In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Month As SmartDate
      Public HumanResourceID As Integer

      Public Sub New()

      End Sub

      Public Sub New(Month As SmartDate, HumanResourceID As Integer)

        Me.Month = Month
        Me.HumanResourceID = HumanResourceID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROEventManagerPlanningHourList() As ROEventManagerPlanningHourList

      Return New ROEventManagerPlanningHourList()

    End Function

    Public Shared Sub BeginGetROEventManagerPlanningHourList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROEventManagerPlanningHourList)))

      Dim dp As New DataPortal(Of ROEventManagerPlanningHourList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROEventManagerPlanningHourList(CallBack As EventHandler(Of DataPortalResult(Of ROEventManagerPlanningHourList)))

      Dim dp As New DataPortal(Of ROEventManagerPlanningHourList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROEventManagerPlanningHourList(Month As SmartDate, HumanResourceID As Integer) As ROEventManagerPlanningHourList

      Return DataPortal.Fetch(Of ROEventManagerPlanningHourList)(New Criteria(Month, HumanResourceID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROEventManagerPlanningHour.GetROEventManagerPlanningHour(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROEventManagerPlanningHourList"
            cm.Parameters.AddWithValue("@Month", crit.Month.DBValue)
            cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.ZeroDBNull(crit.HumanResourceID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace