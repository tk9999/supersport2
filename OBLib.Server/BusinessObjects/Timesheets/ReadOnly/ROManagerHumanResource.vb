﻿' Generated 09 Oct 2014 15:43 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = false Then
Imports System.Data.SqlClient
#End If

namespace NSWTimesheets.ReadOnly

<Serializable()> _ 
public class ROManagerHumanResource
Inherits OBReadOnlyBase(Of ROManagerHumanResource)

#region " Properties and Methods "

#region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID", 0)
''' <summary>
''' Gets the ID value
''' </summary>
		<Display(AutoGenerateField:=false), Key> 
Public ReadOnly Property HumanResourceID() As Integer
Get
return GetProperty(HumanResourceIDProperty)
End Get
End Property

public  shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceName, "Human Resource Name", "")
''' <summary>
''' Gets the Human Resource Name value
''' </summary>
		<Display(Name:="Human Resource Name", Description:="")> 
Public ReadOnly Property HumanResourceName() As String
Get
return GetProperty(HumanResourceNameProperty)
End Get
End Property

public  shared EmployeeCodeIDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCodeIDNo, "Employee Code ID No", "")
''' <summary>
''' Gets the Employee Code ID No value
''' </summary>
		<Display(Name:="Employee Code ID No", Description:="")> 
Public ReadOnly Property EmployeeCodeIDNo() As String
Get
return GetProperty(EmployeeCodeIDNoProperty)
End Get
End Property

public  shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerHumanResourceID, "Manager Human Resource", Nothing)
''' <summary>
''' Gets the Manager Human Resource value
''' </summary>
		<Display(Name:="Manager Human Resource", Description:="")> 
Public ReadOnly Property ManagerHumanResourceID() As Integer?
Get
return GetProperty(ManagerHumanResourceIDProperty)
End Get
End Property

#End Region

#region " Methods "

protected Overrides Function GetIdValue() As Object

return GetProperty(HumanResourceIDProperty)

End Function

Public Overrides Function ToString As String

return Me.HumanResourceName

End Function

#End Region

#End Region

#region " Data Access & Factory Methods "

#region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#else

#End Region

#region " .Net Data Access "

 friend  shared  Function GetROManagerHumanResource(dr As SafeDataReader) As ROManagerHumanResource

Dim r As New ROManagerHumanResource()
r.Fetch(dr)
return r

End Function

protected Sub Fetch(sdr As SafeDataReader)

With sdr
LoadProperty(HumanResourceIDProperty, .GetInt32(0))
LoadProperty(HumanResourceNameProperty, .GetString(1))
LoadProperty(EmployeeCodeIDNoProperty, .GetString(2))
LoadProperty(ManagerHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
End With

End Sub

#End If

#End Region

#End Region

End Class

End Namespace