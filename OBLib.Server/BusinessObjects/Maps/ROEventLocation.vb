﻿' Generated 28 Apr 2014 18:33 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Geocoding.Google

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maps.ReadOnly

  <Serializable()> _
  Public Class ROEventLocation
    Inherits SingularReadOnlyBase(Of ROEventLocation)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public ReadOnly Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue")
    ''' <summary>
    ''' Gets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public ReadOnly Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
    End Property

    Public Shared LatitudeProperty As PropertyInfo(Of Decimal?) = RegisterProperty(Of Decimal?)(Function(c) c.Latitude, "Latitude", Nothing)
    ''' <summary>
    ''' Gets the Latitude value
    ''' </summary>
    <Display(Name:="Latitude", Description:="")>
    Public ReadOnly Property Latitude() As Decimal?
      Get
        Return GetProperty(LatitudeProperty)
      End Get
    End Property

    Public Shared LongitudeProperty As PropertyInfo(Of Decimal?) = RegisterProperty(Of Decimal?)(Function(c) c.Longitude, "Longitude", Nothing)
    ''' <summary>
    ''' Gets the Longitude value
    ''' </summary>
    <Display(Name:="Longitude", Description:="")>
    Public ReadOnly Property Longitude() As Decimal?
      Get
        Return GetProperty(LongitudeProperty)
      End Get
    End Property

    Public Shared GoogleLocationStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GoogleLocationString, "Google Location String")
    ''' <summary>
    ''' Gets the Google Location String value
    ''' </summary>
    <Display(Name:="Google Location String", Description:="")>
    Public ReadOnly Property GoogleLocationString() As String
      Get
        Return GetProperty(GoogleLocationStringProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CountryProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Country

    End Function

    Public Function UpdateCoOrdinates(GoogleLocationResult As GoogleAddress) As Boolean

      Dim cProc As New Singular.CommandProc("[CmdProcs].[cmdUpdateEventLocation]",
                                             New String() {
                                                           "@Country",
                                                           "@Location",
                                                           "@Venue",
                                                           "@Latitude",
                                                           "@Longitude"
                                                     },
                                            New Object() {
                                                           Country,
                                                           Location,
                                                           Venue,
                                                           GoogleLocationResult.Coordinates.Latitude,
                                                           GoogleLocationResult.Coordinates.Longitude
                                                    })

      cProc.CommandType = CommandType.StoredProcedure
      cProc.FetchType = Singular.CommandProc.FetchTypes.None

      Try
        cProc.Execute()
        Return True
      Catch ex As Exception
        Return False
      End Try

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROEventLocation(dr As SafeDataReader) As ROEventLocation

      Dim r As New ROEventLocation()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CountryProperty, .GetString(0))
        LoadProperty(LocationProperty, .GetString(1))
        LoadProperty(VenueProperty, .GetString(2))
        LoadProperty(LatitudeProperty, ZeroNothing(.GetDecimal(3)))
        LoadProperty(LongitudeProperty, ZeroNothing(.GetDecimal(4)))
        LoadProperty(GoogleLocationStringProperty, .GetString(5))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace