﻿' Generated 24 Apr 2014 15:49 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Geocoding.Google

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maps

  <Serializable()> _
  Public Class ROImportedEventLocation
    Inherits SingularReadOnlyBase(Of ROImportedEventLocation)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public ReadOnly Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue")
    ''' <summary>
    ''' Gets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public ReadOnly Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
    End Property

    Public Shared GoogleLocationStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GoogleLocationString, "Google Location String")
    ''' <summary>
    ''' Gets the Google Location String value
    ''' </summary>
    <Display(Name:="Google Location String", Description:="")>
    Public ReadOnly Property GoogleLocationString() As String
      Get
        Return GetProperty(GoogleLocationStringProperty)
      End Get
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNumber, "Gen Ref Number", Nothing)
    ''' <summary>
    ''' Gets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref Number", Description:="")>
    Public ReadOnly Property GenRefNumber() As Int64?
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared GenreDescProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreDesc, "Genre Desc")
    ''' <summary>
    ''' Gets the Genre Desc value
    ''' </summary>
    <Display(Name:="Genre Desc", Description:="")>
    Public ReadOnly Property GenreDesc() As String
      Get
        Return GetProperty(GenreDescProperty)
      End Get
    End Property

    Public Shared SeriesTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SeriesTitle, "Series Title")
    ''' <summary>
    ''' Gets the Series Title value
    ''' </summary>
    <Display(Name:="Series Title", Description:="")>
    Public ReadOnly Property SeriesTitle() As String
      Get
        Return GetProperty(SeriesTitleProperty)
      End Get
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LiveDate, "Live Date")
    ''' <summary>
    ''' Gets the Live Date value
    ''' </summary>
    <Display(Name:="Live Date", Description:="")>
    Public ReadOnly Property LiveDate As DateTime?
      Get
        Return GetProperty(LiveDateProperty)
      End Get
    End Property

    Public Shared ImportedEventIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventID, "Imported Event ID")
    ''' <summary>
    ''' Gets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref Number", Description:="")>
    Public ReadOnly Property ImportedEventID() As Integer
      Get
        Return GetProperty(ImportedEventIDProperty)
      End Get
    End Property

    Public Shared LatitudeProperty As PropertyInfo(Of Decimal?) = RegisterProperty(Of Decimal?)(Function(c) c.Latitude, "Latitude", Nothing)
    ''' <summary>
    ''' Gets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Latitude", Description:="")>
    Public ReadOnly Property Latitude() As Decimal?
      Get
        Return GetProperty(LatitudeProperty)
      End Get
    End Property

    Public Shared LongitudeProperty As PropertyInfo(Of Decimal?) = RegisterProperty(Of Decimal?)(Function(c) c.Longitude, "Longitude", Nothing)
    ''' <summary>
    ''' Gets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Longitude", Description:="")>
    Public ReadOnly Property Longitude() As Decimal?
      Get
        Return GetProperty(LongitudeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CountryProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Country

    End Function

    Public Function UpdateCoOrdinates(GoogleLocationResult As GoogleAddress) As Boolean

      Dim cProc As New Singular.CommandProc("CmdProcs.cmdUpdateImportedEventLocation",
                                             New String() {
                                                           "@GenRefNumber",
                                                           "@Latitude",
                                                           "@Longitude"
                                                     },
                                            New Object() {
                                                           GenRefNumber,
                                                           GoogleLocationResult.Coordinates.Latitude,
                                                           GoogleLocationResult.Coordinates.Longitude
                                                    })

      cProc.CommandType = CommandType.StoredProcedure
      cProc.FetchType = Singular.CommandProc.FetchTypes.None

      Try
        cProc.Execute()
        Return True
      Catch ex As Exception
        Return False
      End Try

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROImportedEventLocation(dr As SafeDataReader) As ROImportedEventLocation

      Dim r As New ROImportedEventLocation()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CountryProperty, .GetString(0))
        LoadProperty(LocationProperty, .GetString(1))
        LoadProperty(VenueProperty, .GetString(2))
        LoadProperty(GoogleLocationStringProperty, .GetString(3))
        LoadProperty(GenRefNumberProperty, ZeroNothing(.GetInt64(4)))
        LoadProperty(TitleProperty, .GetString(5))
        LoadProperty(GenreDescProperty, .GetString(6))
        LoadProperty(SeriesTitleProperty, .GetString(7))
        LoadProperty(LiveDateProperty, .GetValue(8))
        LoadProperty(ImportedEventIDProperty, .GetInt32(9))
        LoadProperty(LatitudeProperty, ZeroNothing(.GetDecimal(10)))
        LoadProperty(LongitudeProperty, ZeroNothing(.GetDecimal(11)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace