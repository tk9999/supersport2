﻿
<Serializable>
Public Class CrewScheduleDetailList
  Inherits Productions.Base.CrewScheduleDetailBaseList(Of CrewScheduleDetailList, CrewScheduleDetail)

  Public Overrides Function FetchNewItem(sdr As Csla.Data.SafeDataReader) As CrewScheduleDetail
    Dim csd As New CrewScheduleDetail
    csd.PublicFetch(sdr)
    Return csd
  End Function

  Public Overrides Function NewCrewScheduleDetailList() As CrewScheduleDetailList
    Return New CrewScheduleDetailList
  End Function

End Class