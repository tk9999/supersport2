﻿Imports Singular.DataAnnotations
Imports System.ComponentModel.DataAnnotations

<Serializable>
Public Class CrewScheduleDetail
  Inherits Productions.Base.CrewScheduleDetailBase(Of CrewScheduleDetail)

  ''Add New
  '  CType(StartDateTimeProperty, Singular.SPropertyInfo(Of DateTime?, CrewScheduleDetail)).AddSetExpression("CrewScheduleDetailBO.BeforeStartDateTimeSet(self, args)") '.SetExpressionJS = "RSHRBookingDetailBO.BeforeStartDateTimeSet(self, args)"
  '  CType(StartDateTimeProperty, Singular.SPropertyInfo(Of DateTime?, CrewScheduleDetail)).AddSetExpression("CrewScheduleDetailBO.StartDateTimeSet(self, args)")
  '  CType(EndDateTimeProperty, Singular.SPropertyInfo(Of DateTime?, CrewScheduleDetail)).AddSetExpression("CrewScheduleDetailBO.BeforeEndDateTimeSet(self, args)") '.SetExpressionJS = "RSHRBookingDetailBO.BeforeStartDateTimeSet(self, args)"
  '  CType(EndDateTimeProperty, Singular.SPropertyInfo(Of DateTime?, CrewScheduleDetail)).AddSetExpression("CrewScheduleDetailBO.EndDateTimeSet(self, args)")

#Region " Properties "

  <Display(Name:="Start Date Time"),
  Required(ErrorMessage:="Start Date Time required"),
  SetExpression("CrewScheduleDetailBO.StartDateTimeSet(self)")>
  Public Overrides Property StartDateTime As DateTime?
    Get
      Return GetProperty(StartDateTimeProperty)
    End Get
    Set(ByVal Value As DateTime?)
      SetProperty(StartDateTimeProperty, Value)
    End Set
  End Property

  <Display(Name:="End Date Time"),
  Required(ErrorMessage:="End Date Time required"),
  SetExpression("CrewScheduleDetailBO.EndDateTimeSet(self)")>
  Public Overrides Property EndDateTime As DateTime?
    Get
      Return GetProperty(EndDateTimeProperty)
    End Get
    Set(ByVal Value As DateTime?)
      SetProperty(EndDateTimeProperty, Value)
    End Set
  End Property

#End Region

  Public Shared Function GetCrewScheduleDetail(sdr As Csla.Data.SafeDataReader) As CrewScheduleDetail
    Dim csd As New CrewScheduleDetail
    csd.Fetch(sdr)
    Return csd
  End Function

#Region " Overrides "

  Public Overrides Sub AddExtraParameters(Parameters As SqlClient.SqlParameterCollection)

  End Sub

  Public Overrides Function CreateNewInstance() As CrewScheduleDetail

  End Function

  Public Overrides Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)

  End Sub

  Public Overrides Function GetParent() As Object

  End Function

  Public Overrides Function GetParentList() As Object

  End Function

  Public Overrides Sub UpdateChildLists()

  End Sub

  Public Overrides Sub BeforeInsertUpdate()

  End Sub

#End Region

End Class
