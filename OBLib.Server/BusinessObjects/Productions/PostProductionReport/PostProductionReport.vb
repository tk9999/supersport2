﻿' Generated 16 Feb 2015 10:10 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.PostProductionReports.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace PostProductionReports

  <Serializable()> _
  Public Class PostProductionReport
    Inherits OBBusinessBase(Of PostProductionReport)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PostProductionReportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PostProductionReportID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property PostProductionReportID() As Integer
      Get
        Return GetProperty(PostProductionReportIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:=""),
    Required(ErrorMessage:="Production required")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared PostProductionReportSectionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PostProductionReportSectionID, "Section", Nothing)
    ''' <summary>
    ''' Gets and sets the Post Production Report Section value
    ''' </summary>
    <Display(Name:="Section", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(ROPostProductionReportSectionList), DisplayMember:="PostProductionReportSection"),
    Required(ErrorMessage:="Section required")>
    Public Property PostProductionReportSectionID() As Integer?
      Get
        Return GetProperty(PostProductionReportSectionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PostProductionReportSectionIDProperty, Value)
      End Set
    End Property

    Public Shared Column1Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column1, "Column 1", "")
    ''' <summary>
    ''' Gets and sets the Column 1 value
    ''' </summary>
    <Display(Name:="Column 1", Description:="")>
    Public Property Column1() As String
      Get
        Return GetProperty(Column1Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(Column1Property, Value)
      End Set
    End Property

    Public Shared Column2Property As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Column2, "Column 2", "")
    ''' <summary>
    ''' Gets and sets the Column 2 value
    ''' </summary>
    <Display(Name:="Column 2", Description:="")>
    Public Property Column2() As String
      Get
        Return GetProperty(Column2Property)
      End Get
      Set(ByVal Value As String)
        SetProperty(Column2Property, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PostProductionReportIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Column1.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Post Production Report")
        Else
          Return String.Format("Blank {0}", "Post Production Report")
        End If
      Else
        Return Me.Column1
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPostProductionReport() method.

    End Sub

    Public Shared Function NewPostProductionReport() As PostProductionReport

      Return DataPortal.CreateChild(Of PostProductionReport)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetPostProductionReport(dr As SafeDataReader) As PostProductionReport

      Dim p As New PostProductionReport()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(PostProductionReportIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(PostProductionReportSectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(Column1Property, .GetString(3))
          LoadProperty(Column2Property, .GetString(4))
          LoadProperty(CommentsProperty, .GetString(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insPostProductionReport"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updPostProductionReport"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramPostProductionReportID As SqlParameter = .Parameters.Add("@PostProductionReportID", SqlDbType.Int)
          paramPostProductionReportID.Value = GetProperty(PostProductionReportIDProperty)
          If Me.IsNew Then
            paramPostProductionReportID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
          .Parameters.AddWithValue("@PostProductionReportSectionID", GetProperty(PostProductionReportSectionIDProperty))
          .Parameters.AddWithValue("@Column1", GetProperty(Column1Property))
          .Parameters.AddWithValue("@Column2", GetProperty(Column2Property))
          .Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(PostProductionReportIDProperty, paramPostProductionReportID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delPostProductionReport"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@PostProductionReportID", GetProperty(PostProductionReportIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace