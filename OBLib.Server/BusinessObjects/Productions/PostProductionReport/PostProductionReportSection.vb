﻿' Generated 16 Feb 2015 10:10 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace PostProductionReports

  <Serializable()> _
  Public Class PostProductionReportSection
    Inherits OBBusinessBase(Of PostProductionReportSection)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PostProductionReportSectionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PostProductionReportSectionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property PostProductionReportSectionID() As Integer
      Get
        Return GetProperty(PostProductionReportSectionIDProperty)
      End Get
    End Property

    Public Shared PostProductionReportSectionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PostProductionReportSection, "Post Production Report Section", "")
    ''' <summary>
    ''' Gets and sets the Post Production Report Section value
    ''' </summary>
    <Display(Name:="Post Production Report Section", Description:=""),
    Required(ErrorMessage:="Post Production Report Section required"),
    StringLength(50, ErrorMessage:="Post Production Report Section cannot be more than 50 characters")>
    Public Property PostProductionReportSection() As String
      Get
        Return GetProperty(PostProductionReportSectionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PostProductionReportSectionProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PostProductionReportSectionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.PostProductionReportSection.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Post Production Report Section")
        Else
          Return String.Format("Blank {0}", "Post Production Report Section")
        End If
      Else
        Return Me.PostProductionReportSection
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewPostProductionReportSection() method.

    End Sub

    Public Shared Function NewPostProductionReportSection() As PostProductionReportSection

      Return DataPortal.CreateChild(Of PostProductionReportSection)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetPostProductionReportSection(dr As SafeDataReader) As PostProductionReportSection

      Dim p As New PostProductionReportSection()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(PostProductionReportSectionIDProperty, .GetInt32(0))
          LoadProperty(PostProductionReportSectionProperty, .GetString(1))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insPostProductionReportSection"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updPostProductionReportSection"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramPostProductionReportSectionID As SqlParameter = .Parameters.Add("@PostProductionReportSectionID", SqlDbType.Int)
          paramPostProductionReportSectionID.Value = GetProperty(PostProductionReportSectionIDProperty)
          If Me.IsNew Then
            paramPostProductionReportSectionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@PostProductionReportSection", GetProperty(PostProductionReportSectionProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(PostProductionReportSectionIDProperty, paramPostProductionReportSectionID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delPostProductionReportSection"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@PostProductionReportSectionID", GetProperty(PostProductionReportSectionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace