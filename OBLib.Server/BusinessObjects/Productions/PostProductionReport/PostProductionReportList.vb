﻿' Generated 16 Feb 2015 10:10 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace PostProductionReports

  <Serializable()> _
  Public Class PostProductionReportList
    Inherits OBBusinessListBase(Of PostProductionReportList, PostProductionReport)

#Region " Business Methods "

    Public Function GetItem(PostProductionReportID As Integer) As PostProductionReport

      For Each child As PostProductionReport In Me
        If child.PostProductionReportID = PostProductionReportID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Post Production Reports"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public ProductionID As Integer

      Public Sub New(ByVal ProductionID As Object)

        Me.ProductionID = ProductionID

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewPostProductionReportList() As PostProductionReportList

      Return New PostProductionReportList()

    End Function

    Public Shared Sub BeginGetPostProductionReportList(CallBack As EventHandler(Of DataPortalResult(Of PostProductionReportList)))

      Dim dp As New DataPortal(Of PostProductionReportList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetPostProductionReportList(ByVal ProductionID As Integer) As PostProductionReportList

      Return DataPortal.Fetch(Of PostProductionReportList)(New Criteria(ProductionID))
    End Function

    Public Shared Function GetPostProductionReportList() As PostProductionReportList

      Return DataPortal.Fetch(Of PostProductionReportList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(PostProductionReport.GetPostProductionReport(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getPostProductionReportList"
            cm.Parameters.AddWithValue("@ProductionID", crit.ProductionID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As PostProductionReport In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As PostProductionReport In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace