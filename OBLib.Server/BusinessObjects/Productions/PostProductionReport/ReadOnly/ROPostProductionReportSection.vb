﻿' Generated 20 Apr 2015 08:30 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace PostProductionReports.ReadOnly

  <Serializable()> _
  Public Class ROPostProductionReportSection
    Inherits OBReadOnlyBase(Of ROPostProductionReportSection)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared PostProductionReportSectionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PostProductionReportSectionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property PostProductionReportSectionID() As Integer
      Get
        Return GetProperty(PostProductionReportSectionIDProperty)
      End Get
    End Property

    Public Shared PostProductionReportSectionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PostProductionReportSection, "Post Production Report Section", "")
    ''' <summary>
    ''' Gets the Post Production Report Section value
    ''' </summary>
    <Display(Name:="Post Production Report Section", Description:="")>
  Public ReadOnly Property PostProductionReportSection() As String
      Get
        Return GetProperty(PostProductionReportSectionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(PostProductionReportSectionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.PostProductionReportSection

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROPostProductionReportSection(dr As SafeDataReader) As ROPostProductionReportSection

      Dim r As New ROPostProductionReportSection()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(PostProductionReportSectionIDProperty, .GetInt32(0))
        LoadProperty(PostProductionReportSectionProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace