﻿' Generated 12 Sep 2014 06:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Temp

  <Serializable()> _
  Public Class ROHRClashOB
    Inherits SingularReadOnlyBase(Of ROHRClashOB)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared ParentDescProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ParentDesc, "Parent Desc")
    ''' <summary>
    ''' Gets the Parent Desc value
    ''' </summary>
    <Display(Name:="Parent Desc", Description:="")>
    Public ReadOnly Property ParentDesc() As String
      Get
        Return GetProperty(ParentDescProperty)
      End Get
    End Property

    Public Shared ChildDescProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChildDesc, "Child Desc")
    ''' <summary>
    ''' Gets the Child Desc value
    ''' </summary>
    <Display(Name:="Child Desc", Description:="")>
    Public ReadOnly Property ChildDesc() As String
      Get
        Return GetProperty(ChildDescProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Production Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ParentDesc

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROHRClashOB(dr As SafeDataReader) As ROHRClashOB

      Dim r As New ROHRClashOB()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(ParentDescProperty, .GetString(1))
        LoadProperty(ChildDescProperty, .GetString(2))
        LoadProperty(SystemProperty, .GetString(3))
        LoadProperty(ProductionAreaProperty, .GetString(4))
        LoadProperty(StartDateTimeProperty, .GetValue(5))
        LoadProperty(EndDateTimeProperty, .GetValue(6))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace