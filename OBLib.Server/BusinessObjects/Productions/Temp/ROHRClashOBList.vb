﻿' Generated 12 Sep 2014 06:53 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Areas
Imports OBLib.Productions.Schedules

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Temp

  <Serializable()> _
  Public Class ROHRClashOBList
    Inherits SingularReadOnlyListBase(Of ROHRClashOBList, ROHRClashOB)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROHRClashOB

      For Each child As ROHRClashOB In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "R Os"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionSystemArea As ProductionSystemArea
      Public Property HumanResourceID As Integer? = Nothing

      Public Sub New(ProductionSystemArea As ProductionSystemArea, HumanResourceID As Integer?)
        Me.ProductionSystemArea = ProductionSystemArea
        Me.HumanResourceID = HumanResourceID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROHRClashOBList() As ROHRClashOBList

      Return New ROHRClashOBList()

    End Function

    Public Shared Sub BeginGetROHRClashOBList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROHRClashOBList)))

      Dim dp As New DataPortal(Of ROHRClashOBList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROHRClashOBList(CallBack As EventHandler(Of DataPortalResult(Of ROHRClashOBList)))

      Dim dp As New DataPortal(Of ROHRClashOBList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROHRClashOBList(ByRef ProductionSystemArea As ProductionSystemArea, HumanResourceID As Integer?) As ROHRClashOBList

      Return DataPortal.Fetch(Of ROHRClashOBList)(New Criteria(ProductionSystemArea, HumanResourceID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROHRClashOB.GetROHRClashOB(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROHRClashOBList"

            'Timeline
            'Dim TimelineDat As New DataSet("DataSet")
            Dim TimelineTable As New DataTable '= ds.Tables.Add("Table")
            TimelineTable.Columns.Add("ProductionTimelineID", GetType(Int32)).ColumnMapping = MappingType.Attribute
            TimelineTable.Columns.Add("ProductionID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            TimelineTable.Columns.Add("ProductionTimelineTypeID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            TimelineTable.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
            TimelineTable.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
            TimelineTable.Columns.Add("VehicleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            TimelineTable.Columns.Add("CrewTypeID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            TimelineTable.Columns.Add("ProductionSystemAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            TimelineTable.Columns.Add("RoomID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            TimelineTable.Columns.Add("TimelineDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
            TimelineTable.Columns.Add("SystemID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            TimelineTable.Columns.Add("ProductionAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

            For Each pt As ProductionTimeline In crit.ProductionSystemArea.ProductionTimelineList
              If pt.IsValid Then
                Dim row As DataRow = TimelineTable.NewRow
                row("ProductionTimelineID") = NothingDBNull(pt.ProductionTimelineID)
                row("ProductionID") = NothingDBNull(pt.ProductionID)
                row("ProductionTimelineTypeID") = NothingDBNull(pt.ProductionTimelineTypeID)
                row("StartDateTime") = pt.StartDateTime
                row("EndDateTime") = pt.EndDateTime
                row("VehicleID") = NothingDBNull(pt.VehicleID)
                row("CrewTypeID") = NothingDBNull(pt.CrewTypeID)
                row("ProductionSystemAreaID") = NothingDBNull(pt.ProductionSystemAreaID)
                row("RoomID") = NothingDBNull(pt.RoomID)
                row("TimelineDate") = pt.TimelineDate
                row("SystemID") = pt.SystemID
                row("ProductionAreaID") = pt.ProductionAreaID
                TimelineTable.Rows.Add(row)
              End If
            Next

            'Production Human Resource
            'Dim TimelineDat As New DataSet("DataSet")
            Dim PHRTable As New DataTable '= ds.Tables.Add("Table")
            PHRTable.Columns.Add("ProductionHumanResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
            PHRTable.Columns.Add("ProductionID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            PHRTable.Columns.Add("DisciplineID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            PHRTable.Columns.Add("PositionID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            PHRTable.Columns.Add("PositionTypeID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            PHRTable.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            PHRTable.Columns.Add("VehicleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            PHRTable.Columns.Add("SystemID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            PHRTable.Columns.Add("ProductionSystemAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            PHRTable.Columns.Add("RoomID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

            For Each phr As ProductionHumanResource In crit.ProductionSystemArea.ProductionHumanResourceList
              'If phr.IsValid Then
              Dim row As DataRow = PHRTable.NewRow
              row("ProductionHumanResourceID") = NothingDBNull(phr.ProductionHumanResourceID)
              row("ProductionID") = NothingDBNull(phr.ProductionID)
              row("DisciplineID") = NothingDBNull(phr.DisciplineID)
              row("PositionID") = NothingDBNull(phr.PositionID)
              row("PositionTypeID") = NothingDBNull(phr.PositionTypeID)
              row("HumanResourceID") = NothingDBNull(phr.HumanResourceID)
              row("VehicleID") = NothingDBNull(phr.VehicleID)
              row("SystemID") = NothingDBNull(phr.SystemID)
              row("ProductionSystemAreaID") = NothingDBNull(phr.ProductionSystemAreaID)
              row("RoomID") = NothingDBNull(phr.RoomID)
              PHRTable.Rows.Add(row)
              'End If
            Next

            'Web CrewSchedule Detail
            'Dim ds As New DataSet("DataSet")
            Dim CrewSched As DataTable = New DataTable 'ds.Tables.Add("Table")
            'CrewSched.Columns.Add("GUID", GetType(Guid)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("CrewScheduleDetailID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("ProductionHumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("ProductionTimelineID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("ProductionTimelineTypeID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("TimesheetDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("HumanResourceOffPeriodID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("HumanResourceScheduleTypeID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("HumanResourceShiftID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("ProductionAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
            CrewSched.Columns.Add("SystemID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

            Dim query = From HRSched As HRProductionSchedule In crit.ProductionSystemArea.HRProductionScheduleList
                        From HRSchedDetail As HRProductionScheduleDetail In HRSched.HRProductionScheduleDetailList
                        Select New With { _
                                          Key .CrewScheduleDetailID = HRSchedDetail.CrewScheduleDetailID, _
                                          Key .ProductionHumanResourceID = HRSchedDetail.ProductionHumanResourceID, _
                                          Key .ProductionTimelineID = HRSchedDetail.ProductionTimelineID, _
                                          Key .ProductionTimelineTypeID = HRSchedDetail.ProductionTimelineTypeID, _
                                          Key .StartDateTime = HRSchedDetail.StartDateTime, _
                                          Key .EndDateTime = HRSchedDetail.EndDateTime, _
                                          Key .TimesheetDate = HRSchedDetail.TimesheetDate, _
                                          Key .HumanResourceID = HRSchedDetail.HumanResourceID, _
                                          Key .HumanResourceOffPeriodID = HRSchedDetail.HumanResourceOffPeriodID, _
                                          Key .HumanResourceScheduleTypeID = HRSchedDetail.HumanResourceScheduleTypeID,
                                          Key .HumanResourceShiftID = HRSchedDetail.HumanResourceShiftID, _
                                          Key .ProductionAreaID = HRSchedDetail.ProductionAreaID, _
                                          Key .SystemID = HRSchedDetail.SystemID
                                        }


            For Each obj In query
              Dim row As DataRow = CrewSched.NewRow
              row("CrewScheduleDetailID") = NothingDBNull(obj.CrewScheduleDetailID)
              row("ProductionHumanResourceID") = NothingDBNull(obj.ProductionHumanResourceID)
              row("ProductionTimelineID") = NothingDBNull(obj.ProductionTimelineID)
              row("ProductionTimelineTypeID") = NothingDBNull(obj.ProductionTimelineTypeID)
              row("StartDateTime") = obj.StartDateTime
              row("EndDateTime") = obj.EndDateTime
              row("TimesheetDate") = NothingDBNull(obj.TimesheetDate)
              row("HumanResourceID") = NothingDBNull(obj.HumanResourceID)
              row("HumanResourceOffPeriodID") = NothingDBNull(obj.HumanResourceOffPeriodID)
              row("HumanResourceScheduleTypeID") = NothingDBNull(obj.HumanResourceScheduleTypeID)
              row("HumanResourceShiftID") = NothingDBNull(obj.HumanResourceShiftID)
              row("ProductionAreaID") = NothingDBNull(obj.ProductionAreaID)
              row("SystemID") = NothingDBNull(obj.SystemID)
              CrewSched.Rows.Add(row)
            Next

            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionSystemArea.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.ProductionSystemArea.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionSystemArea.ProductionAreaID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))

            Dim WebTimelines As SqlParameter = cm.Parameters.Add("@WebProductionTimelines", SqlDbType.Structured)
            WebTimelines.Value = TimelineTable

            Dim WebProductionHumanResources As SqlParameter = cm.Parameters.Add("@WebProductionHumanResources", SqlDbType.Structured)
            WebProductionHumanResources.Value = PHRTable

            Dim WebCrewScheduleDetails As SqlParameter = cm.Parameters.Add("@WebCrewScheduleDetails", SqlDbType.Structured)
            WebCrewScheduleDetails.Value = CrewSched

            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace