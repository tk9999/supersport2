﻿' Generated 05 Jan 2015 09:28 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Base

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Budgeting

  <Serializable()> _
  Public Class ProductionBudgetedList
    Inherits ProductionDetailList(Of ProductionBudgetedList, ProductionBudgeted)

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ProductionBudgeted

      For Each child As ProductionBudgeted In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Sub ClearDeletedList()

      DeletedList.Clear()

    End Sub

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      ' Inherits CriteriaBase(Of Criteria)
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property ProductionID As Integer?

      Public Sub New(ProductionID As Integer?)
        Me.ProductionID = ProductionID
      End Sub

      Public Sub New()
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionBudgeted() As ProductionBudgetedList

      Return New ProductionBudgetedList()

    End Function

    Public Shared Sub BeginGetProductionBudgetedList(CallBack As EventHandler(Of DataPortalResult(Of ProductionBudgetedList)))

      Dim dp As New DataPortal(Of ProductionBudgetedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionBudgetedList() As ProductionBudgetedList

      Return DataPortal.Fetch(Of ProductionBudgetedList)(New Criteria())

    End Function

    Public Shared Function GetProductionBudgetedList(ProductionID As Integer?) As ProductionBudgetedList

      Return DataPortal.Fetch(Of ProductionBudgetedList)(New Criteria(ProductionID))

    End Function

    Protected Overrides Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      While sdr.Read
        Me.Add(ProductionBudgeted.GetProductionBudgeted(sdr))
      End While
      Me.RaiseListChangedEvents = True

      FetchChildLists(sdr)

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionBudgetedList"
            cm.Parameters.AddWithValue("@ProductionID", crit.ProductionID)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionBudgeted In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionBudgeted In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

    Public Overrides Sub FetchChildLists(sdr As Csla.Data.SafeDataReader)

    End Sub

    Public Overrides Function FetchNewItem(sdr As SafeDataReader) As ProductionBudgeted
      Dim pt As New ProductionBudgeted
      pt.PublicFetch(sdr)
      Return pt
    End Function

  End Class

End Namespace