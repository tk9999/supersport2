﻿' Generated 16 Jul 2015 11:41 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Productions.Budgeting.ReadOnly

  <Serializable()> _
  Public Class ROProductionBudgetedList
    Inherits SingularReadOnlyListBase(Of ROProductionBudgetedList, ROProductionBudgeted)

    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROProductionBudgeted

      For Each child As ROProductionBudgeted In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date", New SmartDate(DateTime.Now))

      <Display(Name:="Start Date", Description:="")>
      Public Property StartDate() As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date", New SmartDate(DateTime.Now))

      <Display(Name:="End Date", Description:="")>
      Public Property EndDate() As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Overridable Property Keyword As String = ""
      Public Property DomesticInd As Boolean?
      Public Property BudgetedInd As Boolean?
      Public Property RingFencedInd As Boolean?

      Public Sub New(StartDate, EndDate, Keyword, BudgetedInd, RingFencedInd, DomesticInd)
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.Keyword = Keyword
        Me.BudgetedInd = BudgetedInd
        Me.RingFencedInd = RingFencedInd
        Me.DomesticInd = DomesticInd
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionBudgetedList() As ROProductionBudgetedList

      Return New ROProductionBudgetedList()

    End Function

    Public Shared Sub BeginGetROProductionBudgetedList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionBudgetedList)))

      Dim dp As New DataPortal(Of ROProductionBudgetedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionBudgetedList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionBudgetedList)))

      Dim dp As New DataPortal(Of ROProductionBudgetedList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionBudgetedList(StartDate As DateTime?, EndDate As DateTime?, Keyword As String,
                                                       BudgetedInd As Boolean?, RingFencedInd As Boolean?,
                                                       DomesticInd As Boolean?) As ROProductionBudgetedList

      Return DataPortal.Fetch(Of ROProductionBudgetedList)(New Criteria(StartDate, EndDate, Keyword, BudgetedInd, RingFencedInd, DomesticInd))

    End Function

    Public Shared Function GetROProductionBudgetedList() As ROProductionBudgetedList

      Return DataPortal.Fetch(Of ROProductionBudgetedList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionBudgeted.GetROProductionBudgeted(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionBudgetedList"
            cm.Parameters.AddWithValue("@Keyword", Singular.Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@PlayStartDateTime", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@PlayEndDateTime", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@Budgeted", Singular.Misc.NothingDBNull(crit.BudgetedInd))
            cm.Parameters.AddWithValue("@RingFenced", Singular.Misc.NothingDBNull(crit.RingFencedInd))
            cm.Parameters.AddWithValue("@DomesticInd", Singular.Misc.NothingDBNull(crit.DomesticInd))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class
End Namespace
