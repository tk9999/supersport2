﻿' Generated 16 Jul 2015 11:41 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Productions.Budgeting.ReadOnly

  <Serializable()> _
  Public Class ROProductionBudgeted
    Inherits SingularReadOnlyBase(Of ROProductionBudgeted)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property SelectedInd As Boolean = False

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared PlayStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlayStartDateTime, "Play Start Date Time")
    ''' <summary>
    ''' Gets the Play Start Date Time value
    ''' </summary>
    <Display(Name:="Play Start Date", Description:="")>
    Public ReadOnly Property PlayStartDateTime As DateTime?
      Get
        Return GetProperty(PlayStartDateTimeProperty)
      End Get
    End Property

    Public Shared PlayEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlayEndDateTime, "Play End Date Time")
    ''' <summary>
    ''' Gets the Play End Date Time value
    ''' </summary>
    <Display(Name:="Play End Date", Description:="")>
    Public ReadOnly Property PlayEndDateTime As DateTime?
      Get
        Return GetProperty(PlayEndDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeID, "Production Type")
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionTypeID() As Integer
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventTypeID, "Event Type")
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public ReadOnly Property EventTypeID() As Integer
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "Event Type")
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="")>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Production Description")
    ''' <summary>
    ''' Gets the Production Description value
    ''' </summary>
    <Display(Name:="Production Description", Description:="")>
    Public ReadOnly Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueID, "Production Venue ID")
    ''' <summary>
    ''' Gets the Production Venue ID value
    ''' </summary>
    <Display(Name:="Production Venue ID", Description:="")>
    Public ReadOnly Property ProductionVenueID() As Integer
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Production Venue")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="")>
    Public ReadOnly Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared BudgetedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.BudgetedInd, "Budgeted", False)
    ''' <summary>
    ''' Gets the Budgeted value
    ''' </summary>
    <Display(Name:="Budgeted", Description:="Indicates whether the production is budgeted or unbudgeted")>
    Public ReadOnly Property BudgetedInd() As Boolean
      Get
        Return GetProperty(BudgetedIndProperty)
      End Get
    End Property

    Public Shared RingFencedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RingFencedInd, "Ring Fenced", False)
    ''' <summary>
    ''' Gets the Ring Fenced value
    ''' </summary>
    <Display(Name:="Ring Fenced", Description:="Indicates if production is Ring Fenced")>
    Public ReadOnly Property RingFencedInd() As Boolean
      Get
        Return GetProperty(RingFencedIndProperty)
      End Get
    End Property

    Public Shared RecoupedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RecoupedInd, "Recouped", False)
    ''' <summary>
    ''' Gets the Recouped value
    ''' </summary>
    <Display(Name:="Recouped", Description:="Indicates if unbudgeted production has been recouped")>
    Public ReadOnly Property RecoupedInd() As Boolean
      Get
        Return GetProperty(RecoupedIndProperty)
      End Get
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Debtor")
    ''' <summary>
    ''' Gets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="Name of the company making the payment for the production")>
    Public ReadOnly Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
    End Property

    Public Shared TotalRecoupedProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalRecouped, "Total Recouped")
    ''' <summary>
    ''' Gets the Total Recouped value
    ''' </summary>
    <Display(Name:="Total Recouped", Description:="Total amount recouped if production was unbudgeted")>
    Public ReadOnly Property TotalRecouped() As Decimal
      Get
        Return GetProperty(TotalRecoupedProperty)
      End Get
    End Property

    Public Shared NoRecoupReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NoRecoupReason, "No Recoup Reason")
    ''' <summary>
    ''' Gets the No Recoup Reason value
    ''' </summary>
    <Display(Name:="No Recoup Reason", Description:="Reason for no recoupment if production was unbudgeted")>
    Public ReadOnly Property NoRecoupReason() As String
      Get
        Return GetProperty(NoRecoupReasonProperty)
      End Get
    End Property

    Public Shared RealCostProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RealCost, "Real Cost")
    ''' <summary>
    ''' Gets the Real Cost value
    ''' </summary>
    <Display(Name:="Real Cost", Description:="Total cost of the production")>
    Public ReadOnly Property RealCost() As Decimal
      Get
        Return GetProperty(RealCostProperty)
      End Get
    End Property

    Public Shared ProductionQuotesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionQuotes, "Production Quotes")
    ''' <summary>
    ''' Gets the number of Production Quotes linked to Production
    ''' </summary>
    <Display(Name:="Production Quotes", Description:="Number of quotations")>
    Public ReadOnly Property ProductionQuotes() As Integer
      Get
        Return GetProperty(ProductionQuotesProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Title

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionBudgeted(dr As SafeDataReader) As ROProductionBudgeted

      Dim r As New ROProductionBudgeted()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionIDProperty, .GetInt32(0))
        LoadProperty(TitleProperty, .GetString(1))
        LoadProperty(PlayStartDateTimeProperty, .GetValue(2))
        LoadProperty(PlayEndDateTimeProperty, .GetValue(3))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(EventTypeProperty, .GetString(6))
        LoadProperty(ProductionDescriptionProperty, .GetString(7))
        LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(ProductionVenueProperty, .GetString(9))
        LoadProperty(CreatedByProperty, .GetInt32(10))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(11))
        LoadProperty(ModifiedByProperty, .GetInt32(12))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(13))
        LoadProperty(BudgetedIndProperty, .GetBoolean(14))
        LoadProperty(RingFencedIndProperty, .GetBoolean(15))
        LoadProperty(RecoupedIndProperty, .GetBoolean(16))
        LoadProperty(DebtorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(TotalRecoupedProperty, .GetDecimal(18))
        LoadProperty(NoRecoupReasonProperty, .GetString(19))
        LoadProperty(RealCostProperty, .GetDecimal(20))
        LoadProperty(ProductionQuotesProperty, .GetInt32(21))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class
End Namespace