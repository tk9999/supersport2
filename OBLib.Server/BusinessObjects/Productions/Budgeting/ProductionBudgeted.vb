﻿' Generated 05 Jan 2015 09:28 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Productions.Base

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Budgeting

  <Serializable()> _
  Public Class ProductionBudgeted
    Inherits ProductionDetail(Of ProductionBudgeted)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "ProductionBudgetedBO.ProductionBudgetedBOToString(self)")

    Public Shared BudgetedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.BudgetedInd, "Budgeted", False)
    ''' <summary>
    ''' Gets and Sets the Budgeted value
    ''' </summary>
    <Display(Name:="Budgeted", Description:="Indicates whether the production is budgeted or unbudgeted")>
    Public Property BudgetedInd() As Boolean
      Get
        Return GetProperty(BudgetedIndProperty)
      End Get
      Set(value As Boolean)
        SetProperty(BudgetedIndProperty, value)
      End Set
    End Property

    Public Shared RingFencedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RingFencedInd, "Ring Fenced", False)
    ''' <summary>
    ''' Gets and Sets the Ring Fenced value
    ''' </summary>
    <Display(Name:="Ring Fenced", Description:="Indicates if production is Ring Fenced")>
    Public Property RingFencedInd() As Boolean
      Get
        Return GetProperty(RingFencedIndProperty)
      End Get
      Set(value As Boolean)
        SetProperty(RingFencedIndProperty, value)
      End Set
    End Property

    Public Shared RecoupedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RecoupedInd, "Recouped", False)
    ''' <summary>
    ''' Gets and Sets the Recouped value
    ''' </summary>
    <Display(Name:="Recouped", Description:="Indicates if unbudgeted production has been recouped")>
    Public Property RecoupedInd() As Boolean
      Get
        Return GetProperty(RecoupedIndProperty)
      End Get
      Set(value As Boolean)
        SetProperty(RecoupedIndProperty, value)
      End Set
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Debtor")
    ''' <summary>
    ''' Gets and Sets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="Name of the company making the payment for the production")>
    Public Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(DebtorIDProperty, value)
      End Set
    End Property

    Public Shared TotalRecoupedAmountProperty As PropertyInfo(Of Decimal?) = RegisterProperty(Of Decimal?)(Function(c) c.TotalRecoupedAmount, "Total Recouped Amount")
    ''' <summary>
    ''' Gets and Sets the Total Recouped value
    ''' </summary>
    <Display(Name:="Total Recouped", Description:="Total amount recouped if production was unbudgeted")>
    Public Property TotalRecoupedAmount() As Decimal?
      Get
        Return GetProperty(TotalRecoupedAmountProperty)
      End Get
      Set(value As Decimal?)
        SetProperty(TotalRecoupedAmountProperty, value)
      End Set
    End Property

    Public Shared NoRecoupReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NoRecoupReason, "No Recoup Reason")
    ''' <summary>
    ''' Gets and Sets the No Recoup Reason value
    ''' </summary>
    <Display(Name:="No Recoup Reason", Description:="Reason for no recoupment if production was unbudgeted")>
    Public Property NoRecoupReason() As String
      Get
        Return GetProperty(NoRecoupReasonProperty)
      End Get
      Set(value As String)
        SetProperty(NoRecoupReasonProperty, value)
      End Set
    End Property

    Public Shared RealCostAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.RealCostAmount, "Real Cost Amount")
    ''' <summary>
    ''' Gets and Sets the Real Cost value
    ''' </summary>
    <Display(Name:="Real Cost", Description:="Total cost of the production")>
    Public Property RealCostAmount() As Decimal
      Get
        Return GetProperty(RealCostAmountProperty)
      End Get
      Set(value As Decimal)
        SetProperty(RealCostAmountProperty, value)
      End Set
    End Property

    Public Shared ProductionQuotesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionQuotes, "Production Quotes")
    ''' <summary>
    ''' Gets the number of Production Quotes linked to Production
    ''' </summary>
    <Display(Name:="Number of Linked Quotations", Description:="Number of quotations")>
    Public ReadOnly Property ProductionQuotes() As Integer
      Get
        Return GetProperty(ProductionQuotesProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Detail")
        Else
          Return String.Format("Blank {0}", "Production Detail")
        End If
      Else
        Return Me.ProductionDescription
      End If

    End Function

#End Region

#Region " Child Lists "

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(PlayStartDateTimeProperty)
        .JavascriptRuleFunctionName = "ProductionBudgetedBO.PlayStartDateTimeValid"
        .ServerRuleFunction = AddressOf PlayStartDateTimeValid
        .AddTriggerProperty(PlayEndDateTimeProperty)
        .AddTriggerProperty(PlaceHolderIndProperty)
        .AffectedProperties.Add(PlayEndDateTimeProperty)
      End With

      With AddWebRule(DebtorIDProperty)
        .JavascriptRuleFunctionName = "ProductionBudgetedBO.DebtorNameRequired"
        .ServerRuleFunction = AddressOf DebtorNameRequired
      End With

      With AddWebRule(TotalRecoupedAmountProperty)
        .JavascriptRuleFunctionName = "ProductionBudgetedBO.AmountRecoupedRequired"
        .ServerRuleFunction = AddressOf AmountRecoupedRequired
      End With

      With AddWebRule(NoRecoupReasonProperty)
        .JavascriptRuleFunctionName = "ProductionBudgetedBO.NoRecoupReasonRequired"
        .ServerRuleFunction = AddressOf NoRecoupReasonRequired
      End With

      With AddWebRule(RealCostAmountProperty)
        .JavascriptRuleFunctionName = "ProductionBudgetedBO.TotalCostRequired"
        .ServerRuleFunction = AddressOf TotalCostRequired
      End With

    End Sub

    Public Function PlayStartDateTimeValid(ProductionBudgeted As ProductionBudgeted) As String
      Dim Err As String = ""
      If ProductionBudgeted.PlayStartDateTime Is Nothing Then
        Err &= "Live Start Time is required"
      ElseIf ProductionBudgeted.PlayStartDateTime IsNot Nothing AndAlso ProductionBudgeted.PlayEndDateTime IsNot Nothing Then
        If ProductionBudgeted.PlayStartDateTime.Value > ProductionBudgeted.PlayEndDateTime.Value Then
          Err &= "Live Start Time must be before Live End Time"
        End If
      End If
      Return Err
    End Function

    Public Function DebtorNameRequired(ProductionBudgeted As ProductionBudgeted) As String
      Dim Err As String = ""
      If ProductionBudgeted.BudgetedInd AndAlso ProductionBudgeted.RecoupedInd AndAlso ProductionBudgeted.DebtorID = 0 Then
        Err = "Debtor name is required"
      End If
      Return Err
    End Function

    Public Function AmountRecoupedRequired(ProductionBudgeted As ProductionBudgeted) As String
      Dim Err As String = ""
      If Not ProductionBudgeted.BudgetedInd AndAlso ProductionBudgeted.RecoupedInd AndAlso ProductionBudgeted.TotalRecoupedAmount = 0 Then
        Err = "Amount Recouped is required"
      End If
      Return Err
    End Function

    Public Function NoRecoupReasonRequired(ProductionBudgeted As ProductionBudgeted) As String
      Dim Err As String = ""
      If Not ProductionBudgeted.BudgetedInd AndAlso Not ProductionBudgeted.RecoupedInd AndAlso ProductionBudgeted.NoRecoupReason.Trim.Length = 0 Then
        Err = "Reason Required for no recoupment"
      End If
      Return Err
    End Function

    Public Function TotalCostRequired(ProductionBudgeted As ProductionBudgeted) As String
      Dim Err As String = ""
      If (Not ProductionBudgeted.BudgetedInd OrElse ProductionBudgeted.RingFencedInd) AndAlso ProductionBudgeted.RealCostAmount = 0 Then
        Err = "Total Cost Required"
      End If
      Return Err
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewManualProduction() method.

    End Sub

    Public Shared Function NewManualProduction() As ProductionBudgeted

      Return DataPortal.CreateChild(Of ProductionBudgeted)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionBudgeted(dr As SafeDataReader) As ProductionBudgeted

      Dim p As New ProductionBudgeted()
      p.Fetch(dr)
      Return p

    End Function


    Protected Overloads Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionIDProperty, .GetInt32(0))
          LoadProperty(TitleProperty, .GetString(1))
          LoadProperty(PlayStartDateTimeProperty, .GetValue(2))
          LoadProperty(PlayEndDateTimeProperty, .GetValue(3))
          LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ProductionTypeProperty, .GetString(5))
          LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(EventTypeProperty, .GetString(7))
          LoadProperty(ProductionDescriptionProperty, .GetString(8))
          LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(ProductionVenueProperty, .GetString(10))
          LoadProperty(CreatedByProperty, .GetInt32(11))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(12))
          LoadProperty(ModifiedByProperty, .GetInt32(13))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(14))
          LoadProperty(BudgetedIndProperty, .GetBoolean(15))
          LoadProperty(RingFencedIndProperty, .GetBoolean(16))
          LoadProperty(RecoupedIndProperty, .GetBoolean(17))
          LoadProperty(DebtorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
          LoadProperty(TotalRecoupedAmountProperty, .GetDecimal(19))
          LoadProperty(NoRecoupReasonProperty, .GetString(20))
          LoadProperty(RealCostAmountProperty, .GetDecimal(21))
          LoadProperty(TeamsPlayingProperty, .GetString(22))
          LoadProperty(ProductionQuotesProperty, .GetInt32(23))
          'LoadProperty(VenueConfirmedDateProperty, .GetValue(6))
          'LoadProperty(ProductionRefNoProperty, .GetString(7))
          'LoadProperty(ParentProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          'LoadProperty(SynergyGenRefNoProperty, Singular.Misc.ZeroNothing(.GetInt64(13)))
          'LoadProperty(ProductionTypeProperty, .GetString(17))
          'LoadProperty(PlaceHolderIndProperty, .GetBoolean(20))
          'LoadProperty(CreationTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      If Singular.Security.HasAccess("Productions.Can Insert Production Detail") Then
        ' if we're not dirty then don't update the database
        Using cm As SqlCommand = New SqlCommand
          cm.CommandText = "[InsProcsWeb].[insProductionBudgeted]"
          DoInsertUpdateChild(cm)
        End Using
      End If

    End Sub

    Friend Sub Update()

      If Singular.Security.HasAccess("Productions.Can Update Production Detail") Then
        ' if we're not dirty then don't update the database
        Using cm As SqlCommand = New SqlCommand
          cm.CommandText = "[UpdProcsWeb].[updProductionBudgeted]"
          DoInsertUpdateChild(cm)
        End Using
      End If

    End Sub

    'OVERRIDE BASE InsertUpdate - WE ARE GOING TO DO SOMETHING CUSTOM
    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then
        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionID As SqlParameter = .Parameters.Add("@ProductionID", SqlDbType.Int)
          paramProductionID.Value = GetProperty(ProductionIDProperty)
          If Me.IsNew Then
            paramProductionID.Direction = ParameterDirection.Output
          End If

          Dim paramProductionRefNo As SqlParameter = .Parameters.Add("@ProductionRefNo", SqlDbType.VarChar, 50)
          paramProductionRefNo.Value = GetProperty(ProductionRefNoProperty)
          paramProductionRefNo.Direction = ParameterDirection.InputOutput

          If Me.IsNew Then
            ProductionDescription = ProductionTypeEventType & " at " & ProductionVenue & " - " & TeamsPlaying
            If PlaceHolderInd Then
              ProductionDescription = "PLACEHOLDER: " & ProductionDescription
            End If
          End If

          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@ProductionDescription", GetProperty(ProductionDescriptionProperty))
          .Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(GetProperty(ProductionTypeIDProperty)))
          .Parameters.AddWithValue("@EventTypeID", NothingDBNull(GetProperty(EventTypeIDProperty)))
          .Parameters.AddWithValue("@TeamsPlaying", GetProperty(TeamsPlayingProperty))
          .Parameters.AddWithValue("@ProductionVenueID", NothingDBNull(GetProperty(ProductionVenueIDProperty)))
          .Parameters.AddWithValue("@VenueConfirmedDate", (New SmartDate(GetProperty(VenueConfirmedDateProperty))).DBValue)
          .Parameters.AddWithValue("@PlayStartDateTime", (New SmartDate(GetProperty(PlayStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@PlayEndDateTime", (New SmartDate(GetProperty(PlayEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@SynergyGenRefNo", NothingDBNull(GetProperty(SynergyGenRefNoProperty)))
          .Parameters.AddWithValue("@ParentProductionID", NothingDBNull(GetProperty(ParentProductionIDProperty)))
          .Parameters.AddWithValue("@PlaceHolderInd", GetProperty(PlaceHolderIndProperty))
          .Parameters.AddWithValue("@CreationTypeID", NothingDBNull(GetProperty(CreationTypeIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@BudgetedInd", BudgetedInd)
          .Parameters.AddWithValue("@RingFencedInd", RingFencedInd)
          .Parameters.AddWithValue("@RecoupedInd", RecoupedInd)
          .Parameters.AddWithValue("@DebtorID", ZeroNothingDBNull(DebtorID))
          .Parameters.AddWithValue("@TotalRecoupedAmount", TotalRecoupedAmount)
          .Parameters.AddWithValue("@NoRecoupReason", NoRecoupReason)
          .Parameters.AddWithValue("@RealCostAmount", RealCostAmount)
          '.Parameters.AddWithValue("@OBCityInd", OBCityInd)
          '.Parameters.AddWithValue("@OBContentInd", OBContentInd)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionIDProperty, paramProductionID.Value)
            LoadProperty(ProductionRefNoProperty, paramProductionRefNo.Value)
          End If
          ' update child objects
          MarkOld()
        End With
      Else
        ' update child objects
      End If

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub
      If Singular.Security.HasAccess("Productions.Can Delete Production Detail") Then
        cm.ExecuteNonQuery()
        MarkNew()
      End If

    End Sub

    Public Overrides Sub FetchExtraProperties(sdr As SafeDataReader)

    End Sub

    Public Overrides Sub AddExtraParameters(Parameters As SqlParameterCollection)
      Parameters.AddWithValue("@BudgetedInd", BudgetedInd)
      Parameters.AddWithValue("@RingFencedInd", RingFencedInd)
      Parameters.AddWithValue("@RecoupedInd", RecoupedInd)
      Parameters.AddWithValue("@DebtorID", ZeroNothingDBNull(DebtorID))
      Parameters.AddWithValue("@TotalRecoupedAmount", TotalRecoupedAmount)
      Parameters.AddWithValue("@NoRecoupReason", NoRecoupReason)
      Parameters.AddWithValue("@RealCostAmount", RealCostAmount)
    End Sub

    Public Overrides Sub UpdateChildLists()

    End Sub

#End If

#End Region

#End Region

    Public Overrides Function GetParent() As Object
      Return Nothing
    End Function

    Public Overrides Function GetParentList() As Object
      Return CType(Me.Parent, ProductionBudgetedList)
    End Function

    Public Overrides Sub DeleteChildren()

    End Sub
  End Class

End Namespace