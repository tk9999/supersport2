﻿' Generated 05 Jan 2015 09:28 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base

  <Serializable()> _
  Public Class ManualProduction
    Inherits ProductionDetail(Of ManualProduction)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared VisionViewIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.VisionViewInd, "VisionViewInd", False)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="VisionView", Description:="")>
    Public ReadOnly Property VisionViewInd() As Boolean?
      Get
        Return GetProperty(VisionViewIndProperty)
      End Get
    End Property

    Public Shared OBCityIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBCityInd, "OB City", False)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="OB City", Description:="")>
    Public Property OBCityInd() As Boolean
      Get
        Return GetProperty(OBCityIndProperty)
      End Get
      Set(value As Boolean)
        SetProperty(OBCityIndProperty, value)
      End Set
    End Property

    Public Shared OBContentIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBContentInd, "OB Content", False)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="OB Content", Description:="")>
    Public Property OBContentInd() As Boolean
      Get
        Return GetProperty(OBContentIndProperty)
      End Get
      Set(value As Boolean)
        SetProperty(OBContentIndProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Detail")
        Else
          Return String.Format("Blank {0}", "Production Detail")
        End If
      Else
        Return Me.ProductionDescription
      End If

    End Function

#End Region

#Region " Child Lists "

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(ProductionVenueIDProperty)
        .ServerRuleFunction = AddressOf ProductionVenueIDValid
        .JavascriptRuleFunctionName = "ManualProductionBO.ProductionVenueIDValid"
        .AddTriggerProperty(ProductionVenueProperty)
        .AffectedProperties.Add(ProductionVenueProperty)
      End With

      With AddWebRule(PlayStartDateTimeProperty)
        .JavascriptRuleFunctionName = "ManualProductionBO.PlayStartDateTimeValid"
        .ServerRuleFunction = AddressOf PlayStartDateTimeValid
        .AddTriggerProperty(PlayEndDateTimeProperty)
        .AddTriggerProperty(PlaceHolderIndProperty)
        .AffectedProperties.Add(PlayEndDateTimeProperty)
      End With

    End Sub

    Public Function ProductionVenueIDValid(ManualProduction As ManualProduction) As String
      Dim ErrorMsg As String = ""
      Dim RA As String = ""
      If ManualProduction.ProductionVenueID Is Nothing AndAlso (ManualProduction.PlaceHolderInd OrElse ManualProduction.OBCityInd OrElse ManualProduction.OBContentInd) Then
        ErrorMsg = "Production Venue is required"
      End If
      Return ErrorMsg
    End Function

    Public Function PlayStartDateTimeValid(ManualProduction As ManualProduction) As String
      Dim Err As String = ""
      If ManualProduction.PlayStartDateTime Is Nothing AndAlso (ManualProduction.PlaceHolderInd OrElse ManualProduction.OBCityInd OrElse ManualProduction.OBContentInd) Then
        Err &= "Live Start Time is required"
      ElseIf ManualProduction.PlayStartDateTime IsNot Nothing And ManualProduction.PlayEndDateTime IsNot Nothing Then
        If ManualProduction.PlayStartDateTime.Value > ManualProduction.PlayEndDateTime.Value Then
          Err &= "Live Start Time must be before Live End Time"
        End If
      End If
      Return Err
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewManualProduction() method.

    End Sub

    Public Shared Function NewManualProduction() As ManualProduction

      Return DataPortal.CreateChild(Of ManualProduction)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetManualProduction(dr As SafeDataReader) As ManualProduction

      Dim p As New ManualProduction()
      p.Fetch(dr)
      Return p

    End Function

    'Protected Sub Fetch(sdr As SafeDataReader)

    '  Using BypassPropertyChecks
    '    With sdr
    '      LoadProperty(ProductionIDProperty, .GetInt32(0))
    '      LoadProperty(ProductionDescriptionProperty, .GetString(1))
    '      LoadProperty(TeamsPlayingProperty, .GetString(2))
    '      LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
    '      LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
    '      LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
    '      LoadProperty(VenueConfirmedDateProperty, .GetValue(6))
    '      LoadProperty(ProductionRefNoProperty, .GetString(7))
    '      LoadProperty(ParentProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
    '      LoadProperty(CreatedByProperty, .GetInt32(9))
    '      LoadProperty(CreatedDateTimeProperty, .GetDateTime(10))
    '      LoadProperty(ModifiedByProperty, .GetInt32(11))
    '      LoadProperty(ModifiedDateTimeProperty, .GetDateTime(12))
    '      LoadProperty(SynergyGenRefNoProperty, Singular.Misc.ZeroNothing(.GetInt64(13)))
    '      LoadProperty(PlayStartDateTimeProperty, .GetValue(14))
    '      LoadProperty(PlayEndDateTimeProperty, .GetValue(15))
    '      LoadProperty(TitleProperty, .GetString(16))
    '      LoadProperty(ProductionTypeProperty, .GetString(17))
    '      LoadProperty(EventTypeProperty, .GetString(18))
    '      LoadProperty(ProductionVenueProperty, .GetString(19))
    '      LoadProperty(PlaceHolderIndProperty, .GetBoolean(20))
    '      LoadProperty(CreationTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
    '      LoadProperty(VisionViewIndProperty, .GetBoolean(22))
    '      LoadProperty(HighlightsIndProperty, .GetBoolean(23))
    '      LoadProperty(MyAreaAddedIndProperty, .GetBoolean(24))
    '      LoadProperty(AreaCountProperty, .GetInt32(25))
    '      LoadProperty(OBCityIndProperty, .GetBoolean(26))
    '      LoadProperty(OBContentIndProperty, .GetBoolean(27))
    '    End With
    '  End Using

    '  MarkAsChild()
    '  MarkOld()
    '  BusinessRules.CheckRules()

    'End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "[InsProcsWeb].[insManualProduction]"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "[UpdProcsWeb].[updManualProduction]"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    'OVERRIDE BASE InsertUpdate - WE ARE GOING TO DO SOMETHING CUSTOM
    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then
        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionID As SqlParameter = .Parameters.Add("@ProductionID", SqlDbType.Int)
          paramProductionID.Value = GetProperty(ProductionIDProperty)
          If Me.IsNew Then
            paramProductionID.Direction = ParameterDirection.Output
          End If

          Dim paramProductionRefNo As SqlParameter = .Parameters.Add("@ProductionRefNo", SqlDbType.VarChar, 50)
          paramProductionRefNo.Value = GetProperty(ProductionRefNoProperty)
          paramProductionRefNo.Direction = ParameterDirection.InputOutput

          If Me.IsNew Then
            ProductionDescription = ProductionTypeEventType & " at " & ProductionVenue & " - " & TeamsPlaying
            If PlaceHolderInd Then
              ProductionDescription = "PLACEHOLDER: " & ProductionDescription
            End If
          End If

          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@ProductionDescription", GetProperty(ProductionDescriptionProperty))
          .Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(GetProperty(ProductionTypeIDProperty)))
          .Parameters.AddWithValue("@EventTypeID", NothingDBNull(GetProperty(EventTypeIDProperty)))
          .Parameters.AddWithValue("@TeamsPlaying", GetProperty(TeamsPlayingProperty))
          .Parameters.AddWithValue("@ProductionVenueID", NothingDBNull(GetProperty(ProductionVenueIDProperty)))
          .Parameters.AddWithValue("@VenueConfirmedDate", (New SmartDate(GetProperty(VenueConfirmedDateProperty))).DBValue)
          .Parameters.AddWithValue("@PlayStartDateTime", (New SmartDate(GetProperty(PlayStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@PlayEndDateTime", (New SmartDate(GetProperty(PlayEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@SynergyGenRefNo", NothingDBNull(GetProperty(SynergyGenRefNoProperty)))
          .Parameters.AddWithValue("@ParentProductionID", NothingDBNull(GetProperty(ParentProductionIDProperty)))
          .Parameters.AddWithValue("@PlaceHolderInd", GetProperty(PlaceHolderIndProperty))
          .Parameters.AddWithValue("@CreationTypeID", NothingDBNull(GetProperty(CreationTypeIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@OBCityInd", OBCityInd)
          .Parameters.AddWithValue("@OBContentInd", OBContentInd)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionIDProperty, paramProductionID.Value)
            LoadProperty(ProductionRefNoProperty, paramProductionRefNo.Value)
          End If
          ' update child objects
          MarkOld()
        End With
      Else
        ' update child objects
      End If

    End Sub

    'Friend Sub DeleteSelf()

    '  ' if we're not dirty then don't update the database
    '  If Me.IsNew Then Exit Sub

    '  If Singular.Security.HasAccess("Productions.Can Delete Production Detail") Then
    '    If Not DeleteDisabled Then
    '      Using cm As SqlCommand = New SqlCommand
    '        cm.CommandText = "DelProcsWeb.delManualProduction"
    '        cm.CommandType = CommandType.StoredProcedure
    '        cm.Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
    '        DoDeleteChild(cm)
    '      End Using
    '    End If
    '  End If

    'End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub
      If Singular.Security.HasAccess("Productions.Can Delete Production Detail") Then
        cm.ExecuteNonQuery()
        MarkNew()
      End If

    End Sub

    Public Overrides Sub FetchExtraProperties(sdr As SafeDataReader)

    End Sub

    Public Overrides Sub AddExtraParameters(Parameters As SqlParameterCollection)
      Parameters.AddWithValue("@OBCityInd", OBCityInd)
      Parameters.AddWithValue("@OBContentInd", OBContentInd)
    End Sub

    Public Overrides Sub UpdateChildLists()

    End Sub

#End If

#End Region

#End Region

    Public Overrides Function GetParent() As Object
      Return Nothing
    End Function

    Public Overrides Function GetParentList() As Object
      Return CType(Me.Parent, ManualProductionList)
    End Function

    Public Overrides Sub DeleteChildren()

    End Sub
  End Class

End Namespace