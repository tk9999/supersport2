﻿' Generated 05 Jan 2015 09:28 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base

  <Serializable()> _
  Public MustInherit Class ProductionDetail(Of T As ProductionDetail(Of T))
    Inherits OBBusinessBase(Of T)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key, Browsable(True), Display(AutoGenerateField:=True)>
    Public Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ProductionIDProperty, value)
      End Set
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionDescription, "")
    ''' <summary>
    ''' Gets and sets the Production Description value
    ''' </summary>
    <Display(Name:="Production Description", Description:=""),
    StringLength(200, ErrorMessage:="Description cannot be more than 200 characters")>
    Public Overridable Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionDescriptionProperty, Value)
      End Set
    End Property

    Public Shared TeamsPlayingProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.TeamsPlaying, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Teams", Description:=""),
    StringLength(200, ErrorMessage:="Teams Playing cannot be more than 200 characters")>
    Public Overridable Property TeamsPlaying() As String
      Get
        Return GetProperty(TeamsPlayingProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TeamsPlayingProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Production Type", Description:=""),
    DropDownWeb(GetType(ROProductionTypeList),
                BeforeFetchJS:="ProductionDetailBO.setProductionTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="ProductionDetailBO.triggerProductionTypeIDAutoPopulate",
                AfterFetchJS:="ProductionDetailBO.afterProductionTypeIDRefreshAjax",
                OnItemSelectJSFunction:="ProductionDetailBO.onProductionTypeIDSelected",
                LookupMember:="ProductionType", DisplayMember:="ProductionType", ValueMember:="ProductionTypeID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"ProductionType"}),
    SetExpression("ProductionDetailBO.ProductionTypeIDSet(self)")>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionType, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTypeProperty, Value)
      End Set
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EventTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Event Type", Description:=""),
    DropDownWeb(GetType(ROEventTypeList),
                BeforeFetchJS:="ProductionDetailBO.setEventTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="ProductionDetailBO.triggerEventTypeIDAutoPopulate",
                AfterFetchJS:="ProductionDetailBO.afterEventTypeIDRefreshAjax",
                OnItemSelectJSFunction:="ProductionDetailBO.onEventTypeIDSelected",
                LookupMember:="EventType", DisplayMember:="EventType", ValueMember:="EventTypeID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"EventType"}),
    SetExpression("ProductionDetailBO.EventTypeIDSet(self)")>
    Public Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EventTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.EventType, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventTypeProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionVenueID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Venue", Description:=""),
    DropDownWeb(GetType(ROProductionVenueList),
                 BeforeFetchJS:="ProductionDetailBO.setProductionVenueCriteriaBeforeRefresh",
                 PreFindJSFunction:="ProductionDetailBO.triggerProductionVenueAutoPopulate",
                 AfterFetchJS:="ProductionDetailBO.afterProductionVenueRefreshAjax",
                 OnItemSelectJSFunction:="ProductionDetailBO.onProductionVenueSelected",
                 LookupMember:="ProductionVenue", DisplayMember:="ProductionVenue", ValueMember:="ProductionVenueID",
                 DropDownCssClass:="hr-dropdown", DropDownColumns:={"ProductionVenue"}),
    SetExpression("ProductionDetailBO.ProductionVenueIDSet(self)")>
    Public Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionVenue, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionVenueProperty, Value)
      End Set
    End Property

    Public Shared VenueConfirmedDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.VenueConfirmedDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Venue Confirmed Date value
    ''' </summary>
    <Display(Name:="Venue Confirmed Date", Description:="")>
    Public Overridable Property VenueConfirmedDate As DateTime?
      Get
        Return GetProperty(VenueConfirmedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(VenueConfirmedDateProperty, Value)
      End Set
    End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionRefNo, "")
    ''' <summary>
    ''' Gets and sets the Production Ref No value
    ''' </summary>
    <Display(Name:="Ref Num", Description:="")>
    Public Overridable ReadOnly Property ProductionRefNo() As String
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
    End Property

    Public Shared ParentProductionIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ParentProductionID, Nothing)
    ''' <summary>
    ''' Gets and sets the Parent Production value
    ''' </summary>
    <Display(Name:="Parent Production", Description:="")>
    Public Overridable Property ParentProductionID() As Integer?
      Get
        Return GetProperty(ParentProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ParentProductionIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SynergyGenRefNoProperty As PropertyInfo(Of Int64?) = RegisterSProperty(Of Int64?)(Function(c) c.SynergyGenRefNo, Nothing)
    ''' <summary>
    ''' Gets and sets the Synergy Gen Ref No value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:="")>
    Public Overridable Property SynergyGenRefNo() As Int64?
      Get
        Return GetProperty(SynergyGenRefNoProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(SynergyGenRefNoProperty, Value)
      End Set
    End Property

    Public Shared PlayStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.PlayStartDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Play Start Date Time value
    ''' </summary>
    <Display(Name:="Live Start Time", Description:="")>
    Public Overridable Property PlayStartDateTime As DateTime?
      Get
        Return GetProperty(PlayStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(PlayStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared PlayEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.PlayEndDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Play End Date Time value
    ''' </summary>
    <Display(Name:="Live End Time", Description:="")>
    Public Overridable Property PlayEndDateTime As DateTime?
      Get
        Return GetProperty(PlayEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(PlayEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Title, "")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title")>
    Public Overridable Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeEventTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionTypeEventType, "")
    ''' <summary>
    ''' Gets and sets the ProductionType value
    ''' </summary>
    <Display(Name:="Genre (Series)"), AlwaysClean>
    Public Property ProductionTypeEventType() As String
      Get
        Return GetProperty(ProductionTypeEventTypeProperty)
      End Get
      Set(value As String)
        SetProperty(ProductionTypeEventTypeProperty, value)
      End Set
    End Property

    Public Shared DeleteDisabledProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.DeleteDisabled, True)
    <AlwaysClean>
    Private ReadOnly Property DeleteDisabled As Boolean
      Get
        Return GetProperty(DeleteDisabledProperty)
      End Get
    End Property

    Public Shared PlaceHolderIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.PlaceHolderInd)
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised By value
    ''' </summary>
    <Display(Name:="PlaceHolder?")>
    Public Property PlaceHolderInd() As Boolean
      Get
        Return GetProperty(PlaceHolderIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PlaceHolderIndProperty, Value)
      End Set
    End Property

    Public Shared CreationTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CreationTypeID, "CreationTypeID", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="CreationTypeID", Description:="")>
    Public ReadOnly Property CreationTypeID() As Integer?
      Get
        Return GetProperty(CreationTypeIDProperty)
      End Get
      'Set(value As Integer)
      '  SetProperty(CreationTypeIDProperty, value)
      'End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Detail")
        Else
          Return String.Format("Blank {0}", "Production Detail")
        End If
      Else
        Return Me.ProductionDescription
      End If

    End Function

    'Public Function GetParentList() As ProductionDetailList(Of T)

    '  Return CType(Me.Parent, ProductionDetailList(Of T))()

    'End Function

    'Public Function GetParent() As OBLib.Rooms.RoomSchedule

    '  If Me.Parent IsNot Nothing Then
    '    Return CType(CType(Me.Parent.Parent, ProductionDetailList(Of T, C).Parent, OBLib.Rooms.RoomSchedule)
    '  End If

    '  Return CType(Me.GetParentList().Parent, OBLib.Rooms.RoomSchedule)

    'End Function

    Public Sub CopyFrom(From As ProductionDetail(Of T))

      LoadProperty(ProductionIDProperty, From.ProductionID)
      LoadProperty(ProductionDescriptionProperty, From.ProductionDescription)
      LoadProperty(TeamsPlayingProperty, From.TeamsPlaying)
      LoadProperty(ProductionTypeIDProperty, From.ProductionTypeID)
      LoadProperty(EventTypeIDProperty, From.EventTypeID)
      LoadProperty(ProductionVenueIDProperty, From.ProductionVenueID)
      LoadProperty(VenueConfirmedDateProperty, From.VenueConfirmedDate)
      LoadProperty(ProductionRefNoProperty, From.ProductionRefNo)
      LoadProperty(ParentProductionIDProperty, From.ParentProductionID)
      LoadProperty(CreatedByProperty, From.CreatedBy)
      LoadProperty(CreatedDateTimeProperty, From.CreatedDateTime)
      LoadProperty(ModifiedByProperty, From.ModifiedBy)
      LoadProperty(ModifiedDateTimeProperty, From.ModifiedDateTime)
      LoadProperty(SynergyGenRefNoProperty, From.SynergyGenRefNo)
      LoadProperty(PlayStartDateTimeProperty, From.PlayStartDateTime)
      LoadProperty(PlayEndDateTimeProperty, From.PlayEndDateTime)
      LoadProperty(TitleProperty, From.Title)
      LoadProperty(ProductionTypeProperty, From.ProductionType)
      LoadProperty(EventTypeProperty, From.EventType)
      LoadProperty(ProductionVenueProperty, From.ProductionVenue)
      LoadProperty(ProductionTypeEventTypeProperty, From.ProductionTypeEventType)
      LoadProperty(DeleteDisabledProperty, From.DeleteDisabled)
      LoadProperty(PlaceHolderIndProperty, From.PlaceHolderInd)
      LoadProperty(CreationTypeIDProperty, From.CreationTypeID)
      'LoadProperty(VisionViewIndProperty, From.VisionViewInd)
      'LoadProperty(HighlightsIndProperty, From.HighlightsInd)
      'LoadProperty(MyAreaAddedIndProperty, From.MyAreaAddedInd)
      'LoadProperty(AreaCountProperty, From.AreaCount)
      MarkOld()

    End Sub

    Public Sub SetCreationTypeID(NewCreationTypeID As Integer?)
      SetProperty(CreationTypeIDProperty, NewCreationTypeID)
    End Sub

    Public Sub SetProductionTypeEventType(NewProductionTypeEventType As String)
      SetProperty(ProductionTypeEventTypeProperty, NewProductionTypeEventType)
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(ProductionTypeIDProperty)
        .ServerRuleFunction = AddressOf ProductionTypeIDValid
        .JavascriptRuleFunctionName = JSObjectName & ".ProductionTypeIDValid"
        .AddTriggerProperty(ProductionTypeProperty)
        .AddTriggerProperty(ProductionTypeEventTypeProperty)
        .AffectedProperties.Add(ProductionTypeProperty)
        .AffectedProperties.Add(ProductionTypeEventTypeProperty)
      End With

      With AddWebRule(EventTypeIDProperty)
        .ServerRuleFunction = AddressOf EventTypeIDValid
        .JavascriptRuleFunctionName = JSObjectName & ".EventTypeIDValid"
        .AddTriggerProperty(EventTypeProperty)
        .AddTriggerProperty(ProductionTypeEventTypeProperty)
        .AffectedProperties.Add(EventTypeProperty)
        .AffectedProperties.Add(ProductionTypeEventTypeProperty)
      End With

      'With AddWebRule(ProductionVenueIDProperty)
      '  .ServerRuleFunction = AddressOf ProductionVenueIDValid
      '  .JavascriptRuleFunctionName = JSObjectName & ".ProductionVenueIDValid"
      '  .AddTriggerProperty(ProductionVenueProperty)
      '  .AffectedProperties.Add(ProductionVenueProperty)
      'End With

      With AddWebRule(PlayStartDateTimeProperty)
        .JavascriptRuleFunctionName = JSObjectName & ".PlayStartDateTimeValid"
        .ServerRuleFunction = AddressOf PlayStartDateTimeValid
        .AddTriggerProperty(PlayEndDateTimeProperty)
        .AddTriggerProperty(PlaceHolderIndProperty)
        .AffectedProperties.Add(PlayEndDateTimeProperty)
      End With

      'With AddWebRule(TeamsPlayingProperty)
      '  .JavascriptRuleFunctionName = JSObjectName & ".TeamsValid"
      '  .ServerRuleFunction = AddressOf TeamsValid
      'End With

      With AddWebRule(TitleProperty)
        .JavascriptRuleFunctionName = JSObjectName & ".TitleValid"
        .ServerRuleFunction = AddressOf TitleValid
      End With

      With AddWebRule(SynergyGenRefNoProperty)
        .JavascriptRuleFunctionName = JSObjectName & ".GenRefNoValid"
        .ServerRuleFunction = AddressOf GenRefNoValid
        .AddTriggerProperty(CreationTypeIDProperty)
      End With

    End Sub

    Public Overridable Function ProductionTypeIDValid(ProductionDetail As T) As String
      Dim ErrorMsg As String = ""
      Dim RA As String = ""
      If ProductionDetail.ProductionTypeID Is Nothing Then
        ErrorMsg = "Production Type is required"
      End If
      Return ErrorMsg
    End Function

    Public Overridable Function EventTypeIDValid(ProductionDetail As T) As String
      Dim ErrorMsg As String = ""
      Dim RA As String = ""
      If ProductionDetail.EventTypeID Is Nothing Then
        ErrorMsg = "Event Type is required"
      End If
      Return ErrorMsg
    End Function

    Public Overridable Function TitleValid(ProductionDetail As T) As String
      Dim ErrorMsg As String = ""
      Dim RA As String = ""
      If ProductionDetail.Title.Trim.Length = 0 Then
        ErrorMsg = "Title is required"
      End If
      Return ErrorMsg
    End Function

    'Public Overridable Function TeamsValid(ProductionDetail As T) As String
    '  Dim ErrorMsg As String = ""
    '  Dim RA As String = ""
    '  'AndAlso (ProductionDetail.PlaceHolderInd OrElse ProductionDetail.OBCityInd OrElse ProductionDetail.OBContentInd)
    '  If ProductionDetail.TeamsPlaying.Trim.Length = 0 AndAlso ProductionDetail.PlaceHolderInd Then
    '    ErrorMsg = "Teams are required"
    '  End If
    '  Return ErrorMsg
    'End Function

    Public Overridable Function GenRefNoValid(ProductionDetail As T) As String
      Dim ErrorMsg As String = ""
      If ProductionDetail.CreationTypeID = CType(OBLib.CommonData.Enums.CreationType.Imported, Integer) AndAlso ProductionDetail.SynergyGenRefNo Is Nothing Then
        ErrorMsg = "Gen Ref is required"
      End If
      Return ErrorMsg
    End Function

    'Public Overridable Function ProductionVenueIDValid(ProductionDetailBase As T) As String
    '  Dim ErrorMsg As String = ""
    '  Dim RA As String = ""
    '  If ProductionDetailBase.ProductionVenueID Is Nothing Then
    '    ErrorMsg = "Venue is required"
    '  End If
    '  Return ErrorMsg
    '  'AndAlso (ProductionDetailBase.PlaceHolderInd OrElse ProductionDetailBase.OBCityInd OrElse ProductionDetailBase.OBContentInd)
    'End Function

    Public Overridable Function PlayStartDateTimeValid(ProductionDetailBase As T) As String
      Dim Err As String = ""
      'If ProductionDetailBase.PlayStartDateTime Is Nothing Then
      '  Err &= "Live Start Time is required"
      'ElseIf ProductionDetailBase.PlayStartDateTime IsNot Nothing And ProductionDetailBase.PlayEndDateTime IsNot Nothing Then
      '  If ProductionDetailBase.PlayStartDateTime.Value > ProductionDetailBase.PlayEndDateTime.Value Then
      '    Err &= "Live Start Time must be before Live End Time"
      '  End If
      'End If
      Return Err
      'AndAlso (ProductionDetailBase.PlaceHolderInd OrElse ProductionDetailBase.OBCityInd OrElse ProductionDetailBase.OBContentInd)
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionDetail() method.

    End Sub

    Public Shared Function NewProductionDetail() As ProductionDetail(Of T)

      Return DataPortal.CreateChild(Of ProductionDetail(Of T))()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Sub PublicFetch(sdr As SafeDataReader)
      Fetch(sdr)
    End Sub

    Protected Overridable Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionIDProperty, .GetInt32(0))
          LoadProperty(ProductionDescriptionProperty, .GetString(1))
          LoadProperty(TeamsPlayingProperty, .GetString(2))
          LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(VenueConfirmedDateProperty, .GetValue(6))
          LoadProperty(ProductionRefNoProperty, .GetString(7))
          LoadProperty(ParentProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(10))
          LoadProperty(ModifiedByProperty, .GetInt32(11))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(12))
          LoadProperty(SynergyGenRefNoProperty, Singular.Misc.ZeroNothing(.GetInt64(13)))
          LoadProperty(PlayStartDateTimeProperty, .GetValue(14))
          LoadProperty(PlayEndDateTimeProperty, .GetValue(15))
          LoadProperty(TitleProperty, .GetString(16))
          LoadProperty(ProductionTypeProperty, .GetString(17))
          LoadProperty(EventTypeProperty, .GetString(18))
          LoadProperty(ProductionVenueProperty, .GetString(19))
          LoadProperty(PlaceHolderIndProperty, .GetBoolean(20))
          LoadProperty(CreationTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
          'LoadProperty(VisionViewIndProperty, .GetBoolean(22))
          'LoadProperty(HighlightsIndProperty, .GetBoolean(23))
          'LoadProperty(MyAreaAddedIndProperty, .GetBoolean(24))
          'LoadProperty(AreaCountProperty, .GetInt32(25))
          'LoadProperty(OBCityIndProperty, .GetBoolean(26))
          'LoadProperty(OBContentIndProperty, .GetBoolean(27))
        End With
      End Using
      FetchExtraProperties(sdr)
      LoadProperty(ProductionTypeEventTypeProperty, ProductionType & " (" & EventType & ")")

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      'If Singular.Security.HasAccess("Productions.Can Insert Production Detail") Then
      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "[InsProcsWeb].[insProductionDetailBase]"
        DoInsertUpdateChild(cm)
      End Using
      'End If

    End Sub

    Friend Sub Update()

      'If Singular.Security.HasAccess("Productions.Can Update Production Detail") Then
      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "[UpdProcsWeb].[updProductionDetailBase]"
        DoInsertUpdateChild(cm)
      End Using
      'End If

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then
        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionID As SqlParameter = .Parameters.Add("@ProductionID", SqlDbType.Int)
          paramProductionID.Value = GetProperty(ProductionIDProperty)
          If Me.IsNew Then
            paramProductionID.Direction = ParameterDirection.Output
          End If

          Dim paramProductionRefNo As SqlParameter = .Parameters.Add("@ProductionRefNo", SqlDbType.VarChar, 50)
          paramProductionRefNo.Value = GetProperty(ProductionRefNoProperty)
          paramProductionRefNo.Direction = ParameterDirection.InputOutput

          If Me.IsNew Then
            ProductionDescription = ProductionTypeEventType & " at " & ProductionVenue
          End If

          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@ProductionDescription", GetProperty(ProductionDescriptionProperty))
          .Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(GetProperty(ProductionTypeIDProperty)))
          .Parameters.AddWithValue("@EventTypeID", NothingDBNull(GetProperty(EventTypeIDProperty)))
          .Parameters.AddWithValue("@TeamsPlaying", GetProperty(TeamsPlayingProperty))
          .Parameters.AddWithValue("@ProductionVenueID", NothingDBNull(GetProperty(ProductionVenueIDProperty)))
          .Parameters.AddWithValue("@VenueConfirmedDate", (New SmartDate(GetProperty(VenueConfirmedDateProperty))).DBValue)
          .Parameters.AddWithValue("@PlayStartDateTime", (New SmartDate(GetProperty(PlayStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@PlayEndDateTime", (New SmartDate(GetProperty(PlayEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@SynergyGenRefNo", NothingDBNull(GetProperty(SynergyGenRefNoProperty)))
          .Parameters.AddWithValue("@ParentProductionID", NothingDBNull(GetProperty(ParentProductionIDProperty)))
          .Parameters.AddWithValue("@PlaceHolderInd", GetProperty(PlaceHolderIndProperty))
          .Parameters.AddWithValue("@CreationTypeID", NothingDBNull(GetProperty(CreationTypeIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          AddExtraParameters(.Parameters)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionIDProperty, paramProductionID.Value)
            LoadProperty(ProductionRefNoProperty, paramProductionRefNo.Value)
          End If
          ' update child objects
          UpdateChildLists()
          MarkOld()
        End With
      Else
        ' update child objects
        UpdateChildLists()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub
      DeleteChildren()

      'If Singular.Security.HasAccess("Productions.Can Delete Production Detail") Then
      '  If Not DeleteDisabled Then
      '    Using cm As SqlCommand = New SqlCommand
      '      cm.CommandText = "DelProcsWeb.delProductionDetailBase"
      '      cm.CommandType = CommandType.StoredProcedure
      '      cm.Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
      '      DoDeleteChild(cm)
      '    End Using
      '  End If
      'End If

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      'If Me.IsNew Then Exit Sub
      'If Singular.Security.HasAccess("Productions.Can Delete Production Detail") Then
      '  cm.ExecuteNonQuery()
      '  MarkNew()
      'End If

    End Sub

#End If

#End Region

#Region " Overridables "

    Public MustOverride Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)
    Public MustOverride Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)
    Public MustOverride Sub UpdateChildLists()

    <Browsable(False)>
    Public Overridable ReadOnly Property JSObjectName As String
      Get
        Return "ProductionDetailBO"
      End Get
    End Property

    Public MustOverride Function GetParent() As Object
    Public MustOverride Function GetParentList() As Object
    Public MustOverride Sub DeleteChildren()

#End Region

#End Region

  End Class

End Namespace