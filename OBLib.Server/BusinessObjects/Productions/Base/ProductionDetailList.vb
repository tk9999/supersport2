﻿' Generated 05 Jan 2015 09:28 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base

  <Serializable()> _
  Public MustInherit Class ProductionDetailList(Of T As ProductionDetailList(Of T, C), C As ProductionDetail(Of C))
    Inherits OBBusinessListBase(Of T, C)

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ProductionDetail(Of C)

      For Each child As ProductionDetail(Of C) In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Sub ClearDeletedList()

      DeletedList.Clear()

    End Sub

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Integer? = Nothing
      Public Property ProductionIDs As String = ""
      Public Property CurrentSystemID As Integer? = Nothing
      Public Property CurrentProductionAreaID As Integer? = Nothing
      Public Property GenRefNumber As Int64? = Nothing

      Public Sub New(ProductionID As Integer?, ProductionIDs As String,
                     CurrentSystemID As Integer?, CurrentProductionAreaID As Integer?)
        Me.ProductionID = ProductionID
        Me.ProductionIDs = ProductionIDs
        Me.CurrentSystemID = CurrentSystemID
        Me.CurrentProductionAreaID = CurrentProductionAreaID
      End Sub

      Public Sub New(GenRefNumber As Int64)
        Me.GenRefNumber = GenRefNumber
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    'Public Shared Function NewProductionDetailList() As ProductionDetailList(Of T, C)

    '  Return New ProductionDetailList(Of T, C)()

    'End Function

    'Public Shared Sub BeginGetProductionDetailList(CallBack As EventHandler(Of DataPortalResult(Of ProductionDetailList(Of T, C))))

    '  Dim dp As New DataPortal(Of ProductionDetailList(Of T, C))()
    '  AddHandler dp.FetchCompleted, CallBack
    '  dp.BeginFetch(New Criteria())

    'End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    'Public Shared Function GetProductionDetailList() As ProductionDetailList(Of T, C)

    '  Return DataPortal.Fetch(Of ProductionDetailList(Of T, C))(New Criteria())

    'End Function

    'Public Shared Function GetProductionDetailList(GenRefNumber As Int64) As ProductionDetailList(Of T, C)

    '  Return DataPortal.Fetch(Of ProductionDetailList(Of T, C))(New Criteria(GenRefNumber))

    'End Function

    'Public Shared Function GetProductionDetailList(ProductionID As Integer?,
    '                                                   CurrentSystemID As Integer?, CurrentProductionAreaID As Integer?) As ProductionDetailList(Of T, C)

    '  Return DataPortal.Fetch(Of ProductionDetailList(Of T, C))(New Criteria(ProductionID, "", CurrentSystemID, CurrentProductionAreaID))

    'End Function

    'Public Shared Function GetProductionDetailList(ProductionIDs As String,
    '                                                   CurrentSystemID As Integer?, CurrentProductionAreaID As Integer?) As ProductionDetailList(Of T, C)

    '  Return DataPortal.Fetch(Of ProductionDetailList(Of T, C))(New Criteria(Nothing, ProductionIDs, CurrentSystemID, CurrentProductionAreaID))

    'End Function

    Protected Overridable Sub Fetch(sdr As SafeDataReader)
      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(FetchNewItem(sdr))
      End While
      Me.RaiseListChangedEvents = True
    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionDetailBaseList"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@ProductionIDs", Strings.MakeEmptyDBNull(crit.ProductionIDs))
            cm.Parameters.AddWithValue("@CurrentSystemID", NothingDBNull(crit.CurrentSystemID))
            cm.Parameters.AddWithValue("@CurrentProductionAreaID", NothingDBNull(crit.CurrentProductionAreaID))
            cm.Parameters.AddWithValue("@GenRefNumber", NothingDBNull(crit.GenRefNumber))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionDetail(Of C) In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionDetail(Of C) In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

    Public MustOverride Sub FetchChildLists(sdr As Csla.Data.SafeDataReader)

#End If

#End Region

#End Region

    Public MustOverride Function FetchNewItem(sdr As Csla.Data.SafeDataReader) As C

  End Class

End Namespace