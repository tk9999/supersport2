﻿' Generated 29 May 2014 18:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Productions.Specs.ReadOnly
Imports OBLib.Productions.Areas
Imports OBLib.Productions.Specs
Imports OBLib.Productions.Vehicles
Imports OBLib.Productions.Correspondence
Imports OBLib.Maintenance.Productions.Correspondence
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance

Namespace Productions

  <Serializable()> _
  Public Class Production
    Inherits OBBusinessBase(Of Production)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "ProductionBO.ProductionToString(self)")

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Production Description")
    ''' <summary>
    ''' Gets and sets the Production Description value
    ''' </summary>
    <Display(Name:="Description", Description:=""),
    Required(ErrorMessage:="Description is required"),
    StringLength(200, ErrorMessage:="Description cannot be more than 200 characters")>
    Public Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionDescriptionProperty, Value)
      End Set
    End Property

    Public Shared TeamsPlayingProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.TeamsPlaying, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Teams Playing", Description:=""),
    StringLength(200, ErrorMessage:="Teams Playing cannot be more than 200 characters"),
    SetExpression("ProductionBO.TeamsPlayingSet(self)")>
    Public Property TeamsPlaying() As String
      Get
        Return GetProperty(TeamsPlayingProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TeamsPlayingProperty, Value)
      End Set
    End Property
    '.AddSetExpression("ProductionBO.Production_SetDefaultProductionDescription(self)", False)

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Production Type", Description:=""),
    Required(ErrorMessage:="Production Type required"),
    DropDownWeb(GetType(ROProductionTypeList),
                BeforeFetchJS:="ProductionBO.setProductionTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="ProductionBO.triggerProductionTypeIDAutoPopulate",
                AfterFetchJS:="ProductionBO.afterProductionTypeIDRefreshAjax",
                OnItemSelectJSFunction:="ProductionBO.onProductionTypeIDSelected",
                LookupMember:="ProductionType", DisplayMember:="ProductionType", ValueMember:="ProductionTypeID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"ProductionType"}),
    SetExpression("ProductionBO.ProductionTypeIDSet(self)")>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionType, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTypeProperty, Value)
      End Set
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EventTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Event Type", Description:=""),
    Required(ErrorMessage:="Event Type required"),
    DropDownWeb(GetType(ROEventTypeList),
                BeforeFetchJS:="ProductionBO.setEventTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="ProductionBO.triggerEventTypeIDAutoPopulate",
                AfterFetchJS:="ProductionBO.afterEventTypeIDRefreshAjax",
                OnItemSelectJSFunction:="ProductionBO.onEventTypeIDSelected",
                LookupMember:="EventType", DisplayMember:="EventType", ValueMember:="EventTypeID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"EventType"}),
    SetExpression("ProductionBO.EventTypeIDSet(self)")>
    Public Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EventTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.EventType, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventTypeProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionVenueID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Venue", Description:=""),
    DropDownWeb(GetType(ROProductionVenueList),
                 BeforeFetchJS:="ProductionBO.setProductionVenueCriteriaBeforeRefresh",
                 PreFindJSFunction:="ProductionBO.triggerProductionVenueAutoPopulate",
                 AfterFetchJS:="ProductionBO.afterProductionVenueRefreshAjax",
                 OnItemSelectJSFunction:="ProductionBO.onProductionVenueSelected",
                 LookupMember:="ProductionVenue", DisplayMember:="ProductionVenue", ValueMember:="ProductionVenueID",
                 DropDownCssClass:="hr-dropdown", DropDownColumns:={"ProductionVenue"}),
    SetExpression("ProductionBO.ProductionVenueIDSet(self)")>
    Public Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionVenue, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="")>
    Public Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionVenueProperty, Value)
      End Set
    End Property

    Public Shared VenueConfirmedDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.VenueConfirmedDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Venue Confirmed Date value
    ''' </summary>
    <Display(Name:="Venue Confirmed Date", Description:="")>
    Public Property VenueConfirmedDate As DateTime?
      Get
        Return GetProperty(VenueConfirmedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(VenueConfirmedDateProperty, Value)
      End Set
    End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionRefNo, "Production Ref No")
    ''' <summary>
    ''' Gets and sets the Production Ref No value
    ''' </summary>
    <Display(Name:="Ref Num", Description:="")>
    Public Property ProductionRefNo() As String
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
      Set(value As String)
        SetProperty(ProductionRefNoProperty, value)
      End Set
    End Property

    Public Shared ParentProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentProductionID, "Parent Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Parent Production value
    ''' </summary>
    <Display(Name:="Parent Production", Description:="")>
    Public Property ParentProductionID() As Integer?
      Get
        Return GetProperty(ParentProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ParentProductionIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SynergyGenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.SynergyGenRefNo, "Synergy Gen Ref No")
    ''' <summary>
    ''' Gets and sets the Synergy Gen Ref No value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:="")>
    Public Property SynergyGenRefNo() As Int64?
      Get
        Return GetProperty(SynergyGenRefNoProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(SynergyGenRefNoProperty, Value)
      End Set
    End Property

    Public Shared PlayStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.PlayStartDateTime)
    ''' <summary>
    ''' Gets and sets the Play Start Date Time value
    ''' </summary>
    <Display(Name:="Live Start Time", Description:=""),
    Required(ErrorMessage:="Live start time of the event"),
    SetExpression("ProductionBO.PlayStartDateTimeSet(self)")>
    Public Property PlayStartDateTime As DateTime?
      Get
        Return GetProperty(PlayStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(PlayStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared PlayEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.PlayEndDateTime)
    ''' <summary>
    ''' Gets and sets the Play End Date Time value
    ''' </summary>
    <Display(Name:="Live End Time", Description:="End date and time of the event"),
    SetExpression("ProductionBO.PlayEndDateTimeSet(self)")>
    Public Property PlayEndDateTime As DateTime?
      Get
        Return GetProperty(PlayEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(PlayEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:=""),
    StringLength(200, ErrorMessage:="Title cannot be more than 200 characters"),
    SetExpression("ProductionBO.TitleSet(self)"),
    Required(AllowEmptyStrings:=False)>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared PlaceholderIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.PlaceholderInd)
    <Display(Name:="Is Placeholder?")>
    Public Property PlaceholderInd() As Boolean
      Get
        Return GetProperty(PlaceholderIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PlaceholderIndProperty, Value)
      End Set
    End Property

    Public Shared CreationTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreationTypeID, "CreationTypeID", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="CreationTypeID", Description:="")>
    Public Property CreationTypeID() As Integer
      Get
        Return GetProperty(CreationTypeIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(CreationTypeIDProperty, value)
      End Set
    End Property

    Public Shared VisionViewIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.VisionViewInd, "VisionViewInd", False)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="VisionView", Description:="")>
    Public Property VisionViewInd() As Boolean?
      Get
        Return GetProperty(VisionViewIndProperty)
      End Get
      Set(value As Boolean?)
        SetProperty(VisionViewIndProperty, value)
      End Set
    End Property

    Public Shared GraphicsSuppliersProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GraphicsSuppliers, "Graphics")
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Graphics", Description:="")>
    Public ReadOnly Property GraphicsSuppliers() As String
      Get
        Return GetProperty(GraphicsSuppliersProperty)
      End Get
    End Property

    Public Shared OBFacilitySuppliersProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OBFacilitySuppliers, "OB Facility")
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="OB Facility", Description:="")>
    Public ReadOnly Property OBFacilitySuppliers() As String
      Get
        Return GetProperty(OBFacilitySuppliersProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    <Display(Name:="Live Times", Description:="")>
    Public ReadOnly Property LiveTimes() As String
      Get
        Dim LiveTimesS As String = ""
        If PlayStartDateTime IsNot Nothing Then
          LiveTimesS &= PlayStartDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          If PlayEndDateTime IsNot Nothing Then
            LiveTimesS &= " - " & PlayEndDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          End If
        End If
        Return LiveTimesS
      End Get
    End Property

    Public Shared IsOBViewProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsOBView)
    <Display(Name:="IsOBView")>
    Public Property IsOBView() As Boolean
      Get
        Return GetProperty(IsOBViewProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsOBViewProperty, Value)
      End Set
    End Property

    Public Shared IsRoomViewProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsRoomView)
    <Display(Name:="IsRoomView")>
    Public Property IsRoomView() As Boolean
      Get
        Return GetProperty(IsRoomViewProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsRoomViewProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods"

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Title.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production R")
        Else
          Return String.Format("Blank {0}", "Production R")
        End If
      Else
        Return Me.Title
      End If

    End Function

    'Public Function GetGraphicsSuppliers() As String
    '  Dim GraphicsSupplier As String = ""
    '  'If Me.ProductionSystemAreaList.Count > 0 Then
    '  '  For Each ProductionSystemArea As ProductionSystemArea In ProductionSystemAreaList
    '  '    If ProductionSystemArea.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
    '  '      If ProductionSystemArea.ProductionOutsourceServiceList.Count > 0 Then
    '  '        For Each ProductionOutsourceService As ProductionOutsourceService In ProductionSystemArea.ProductionOutsourceServiceList
    '  '          If ProductionOutsourceService.OutsourceServiceTypeID = CType(OBLib.CommonData.Enums.OutsourceServiceType.Graphics, Integer) Then
    '  '            Dim Supplier As ROSupplier = CommonData.Lists.ROSupplierList.GetItem(ProductionOutsourceService.SupplierID)
    '  '            GraphicsSupplier = GraphicsSupplier & Supplier.Supplier & ", "
    '  '          End If
    '  '        Next
    '  '      End If
    '  '    End If
    '  '  Next
    '  'End If
    '  Return GraphicsSupplier
    'End Function

    'Public Function GetOBFacilitySuppliers() As String
    '  Dim OBFacilitySupplier As String = ""
    '  'If Me.ProductionSystemAreaList.Count > 0 Then
    '  '  For Each ProductionSystemArea As ProductionSystemArea In ProductionSystemAreaList
    '  '    If ProductionSystemArea.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
    '  '      If ProductionSystemArea.ProductionOutsourceServiceList.Count > 0 Then
    '  '        For Each ProductionOutsourceService As ProductionOutsourceService In ProductionSystemArea.ProductionOutsourceServiceList
    '  '          If ProductionOutsourceService.OutsourceServiceTypeID = CType(OBLib.CommonData.Enums.OutsourceServiceType.OBFacility, Integer) Then
    '  '            'If ProductionOutsourceService.SupplierID = CType(OBLib.CommonData.Enums.OutsourceServiceSuppliers.VisionView, Integer) Then
    '  '            Dim Supplier As ROSupplier = CommonData.Lists.ROSupplierList.GetItem(ProductionOutsourceService.SupplierID)
    '  '            OBFacilitySupplier = OBFacilitySupplier & Supplier.Supplier & ", "
    '  '            'Else
    '  '            'End If
    '  '          End If
    '  '          If OBFacilitySupplier = "" Then
    '  '            OBFacilitySupplier = "SS OB"
    '  '          End If
    '  '        Next
    '  '      End If
    '  '    End If
    '  '  Next
    '  'End If
    '  Return OBFacilitySupplier
    'End Function

    'Public Sub AutoAddTemplateCommentItems()

    '  Throw New Exception("Removed for Rebuild")

    '  'For Each cd As String In ProductionCommentDefaults.ClientDetails
    '  '  ProductionCommentSectionList.AddComment(CInt(CommonData.Enums.ProductionCommentReportSection.ClientDetails), cd)
    '  'Next

    '  'ProductionCommentSectionList.AddComment(CInt(CommonData.Enums.ProductionCommentReportSection.ProductionDetails), ProductionCommentDefaults.ProductionDetails.OBTelephoneNumbersHeader, ProductionCommentDefaults.ProductionDetails.OBTelephoneNumbersContent)
    '  'ProductionCommentSectionList.AddComment(CInt(CommonData.Enums.ProductionCommentReportSection.ProductionDetails), ProductionCommentDefaults.ProductionDetails.TapeStockHeader, ProductionCommentDefaults.ProductionDetails.TapeStockContent)
    '  'ProductionCommentSectionList.AddComment(CInt(CommonData.Enums.ProductionCommentReportSection.ProductionDetails), ProductionCommentDefaults.ProductionDetails.DressCodeHeader, ProductionCommentDefaults.ProductionDetails.DressCodeContent)
    '  'ProductionCommentSectionList.AddComment(CInt(CommonData.Enums.ProductionCommentReportSection.Equipment), ProductionCommentDefaults.Equipment.SpecialArrangementsHeader, ProductionCommentDefaults.Equipment.SpecialArrangementsContent)


    'End Sub

    '#Region " Main "

    '    'Client Side
    '    'Public Function HasSnTDocuments() As Boolean

    '    '  'DO A COMMAND PROC CALL

    '    '  Dim SAndTDocCount As Integer = 0
    '    '  If HasSnT Then
    '    '    For Each document In Me.ProductionDocumentList
    '    '      If document.DocumentTypeID = 16 Then
    '    '        SAndTDocCount += 1
    '    '      End If
    '    '    Next
    '    '  End If

    '    '  If HasSnT Then
    '    '    If SAndTDocCount = 0 Then
    '    '      Return False
    '    '    End If
    '    '  Else
    '    '    Return True
    '    '  End If

    '    '  Return False

    '    'End Function

    '    'Public Function HasPAForms() As Boolean

    '    '  Dim SPAFormsCount As Integer = 0
    '    '  For Each document In Me.ProductionDocumentList
    '    '    If document.DocumentTypeID = 6 Then
    '    '      SPAFormsCount += 1
    '    '    End If
    '    '  Next

    '    '  If SPAFormsCount > 1 Then
    '    '    Return True
    '    '  Else
    '    '    Return False
    '    '  End If

    '    'End Function

    '    'Private Function CanUnReconcileToday() As Boolean

    '    '  If Singular.Security.HasAccess("Productions", "Un-reconciled productions today") Then
    '    '    If Not ReconciledSetDate Is Nothing AndAlso ReconciledSetDate = Now.Date Then
    '    '      Return True
    '    '    End If
    '    '  End If
    '    '  Return False

    '    'End Function

    '    'Public Function CheckCanReconcile() As String

    '    '  Dim CannotReconcileReason As String = ""

    '    '  If Not HasSnTDocuments() Then
    '    '    CannotReconcileReason &= "S&T Document required <br/>"
    '    '  End If

    '    '  If Not HasPAForms() Then
    '    '    CannotReconcileReason &= "PA Forms required <br/>"
    '    '  End If

    '    '  If Not Singular.Security.HasAccess("Productions", "Reconcile Production") Then
    '    '    CannotReconcileReason &= "You are not authorised to Reconcile this production <br/>"
    '    '  End If

    '    '  Me.CannotReconcile = CannotReconcileReason

    '    '  Return CannotReconcileReason

    '    'End Function

    '    'Public Function CheckCanUnReconcile() As String

    '    '  Dim CannotUnReconcileReason As String = ""

    '    '  Dim CurrentUserIsEventManager As Boolean = False
    '    '  Dim EM As List(Of ProductionHumanResource) = Me.ProductionSystemAreaList(0).ProductionHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, OBLib.Security.Settings.CurrentUser.HumanResourceID) And CompareSafe(d.DisciplineID, CType(CommonData.Enums.Discipline.EventManager, Integer))).ToList
    '    '  If EM.Count > 0 Then
    '    '    CurrentUserIsEventManager = True
    '    '  End If

    '    '  If CurrentUserIsEventManager Then
    '    '    If CanUnReconcileToday() Then
    '    '      CannotUnReconcileReason &= "You are not allowed to un-reconcile this production <br/>"
    '    '    End If
    '    '  Else
    '    '    If Not Singular.Security.HasAccess("Productions", "Un-reconciled productions") Then
    '    '      CannotUnReconcileReason &= "You are not authorised to un-reconcile this production <br/>"
    '    '    End If
    '    '  End If

    '    '  CannotUnReconcile = CannotUnReconcileReason

    '    '  Return CannotUnReconcileReason

    '    'End Function

    '    'Private Sub PopulateROEquipment()

    '    '  Dim VehicleIDsXML As String = ""
    '    '  Dim EquipmentIDsXML As String = ""
    '    '  VehicleIDsXML = mProductionVehicleList.GetXMLCriteriaString("VehicleID")
    '    '  EquipmentIDsXML = mProductionEquipmentList.GetXMLCriteriaString("EquipmentID")
    '    '  If mProductionVehicleList.Count = 0 AndAlso mProductionEquipmentList.Count = 0 Then
    '    '    mROProductionEquipmentList = [ReadOnly].ROProductionEquipmentList.NewROProductionEquipmentList
    '    '  Else
    '    '    mROProductionEquipmentList = [ReadOnly].ROProductionEquipmentList.GetROProductionEquipmentList(VehicleIDsXML, EquipmentIDsXML)
    '    '  End If

    '    'End Sub

    '#End Region

    '#Region " Vehicles "

    '    Public Sub RemoveVehicle(VehicleID As Integer?)

    '      'Dim Vehicle As ProductionVehicle = Me.ProductionVehicleList.GetItemByVehicleID(VehicleID)

    '      'If CommonData.Lists.ROVehicleList.GetItem(VehicleID).VehicleTypeID = CType(CommonData.Enums.VehicleType.OBVan, Integer) Then
    '      '  Dim v As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleFull = CommonData.Lists.ROVehicleFullList.GetItem(VehicleID)
    '      '  RemoveHumanResourcesForVehicle(v)
    '      'End If

    '      'RemoveHumanTimelinesForVehicle(VehicleID)
    '      'Me.ProductionVehicleList.Remove(Vehicle)
    '      ''Me.ProductionSystemAreaList(0).CheckAllClashes()
    '      ''Me.ProductionSystemAreaList(0).CheckScheduleValidOld(Nothing)

    '    End Sub

    '    Public Sub RemoveHumanResourcesForVehicle(ByVal Vehicle As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleFull)

    '      'Dim ProductionHumanResourcesToRemove As New List(Of ProductionHumanResource)
    '      'Dim ProductionHumanResourcesToReset As New List(Of ProductionHumanResource)

    '      'For Each vhr As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleHumanResource In Vehicle.ROVehicleHumanResourceList
    '      '  Dim cvhr As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleHumanResource = vhr
    '      '  For Each phr As ProductionHumanResource In Me.ProductionSystemAreaList(0).ProductionHumanResourceList
    '      '    If Singular.Misc.CompareSafe(cvhr.DisciplineID, phr.DisciplineID) AndAlso Singular.Misc.CompareSafe(cvhr.HumanResourceID, phr.HumanResourceID) Then
    '      '      ProductionHumanResourcesToRemove.Add(phr)
    '      '    ElseIf Singular.Misc.CompareSafe(cvhr.HumanResourceID, phr.HumanResourceID) Then
    '      '      ProductionHumanResourcesToReset.Add(phr)
    '      '    End If
    '      '  Next
    '      'Next

    '      'ProductionHumanResourcesToRemove.ForEach(Sub(phr)
    '      '                                           Me.ProductionSystemAreaList(0).DeleteProductionHumanResource(phr)
    '      '                                         End Sub)

    '      'ProductionHumanResourcesToReset.ForEach(Sub(phr)
    '      '                                          Me.ProductionSystemAreaList(0).ClearHumanResourceID(phr)
    '      '                                          phr.VehicleID = Nothing
    '      '                                          phr.VehicleName = ""
    '      '                                        End Sub)

    '    End Sub

    '    Public Sub RemoveHumanTimelinesForVehicle(ByVal VehicleID As Integer?)
    '      'Me.ProductionSystemAreaList(0).DeleteVehiclePreTravelItems(VehicleID)
    '      'Me.ProductionSystemAreaList(0).DeleteVehiclePostTravelItems(VehicleID)
    '    End Sub

    '    Private Class TempStuff

    '      Public Property StartDateTime As DateTime?
    '      Public Property EndDateTime As DateTime?

    '    End Class

    '    Public Sub GetMinMaxTimesForVehicle(ByVal iVehicleID As Integer, ByVal iProductionTimelineTypeID As CommonData.Enums.ProductionTimelineType,
    '                                        ByRef StartDateTime As DateTime?, ByRef EndDateTime As DateTime?)

    '      'StartDateTime = Me.ProductionSystemAreaList(0).ProductionTimelineList.Min(Function(d) d.StartDateTime)
    '      'EndDateTime = Me.ProductionSystemAreaList(0).ProductionTimelineList.Max(Function(d) d.EndDateTime)

    '    End Sub

    '    Public Sub CheckVehicleClashes()
    '      'For Each PV As ProductionVehicle In ProductionVehicleList
    '      '  PV.CheckClash()
    '      'Next
    '    End Sub

    '#End Region

    '#Region " Travel "

    '    'Public Sub AddNewTravelRequisition(SystemID As Integer?)

    '    '  Dim NewTR As OBLib.Travel.TravelRequisition = Me.TravelRequisitionList.AddNew()
    '    '  NewTR.SystemID = SystemID
    '    '  NewTR.VersionNo = 1

    '    'End Sub

    '    Public Function CanOpenTravelPage() As String

    '      Dim Message As String = ""

    '      'Dim TransmissionCount As Integer = Me.ProductionSystemAreaList(0).ProductionTimelineList.Where(Function(tm) tm.ProductionTimelineTypeID IsNot Nothing AndAlso tm.ProductionTimelineTypeID = CInt(CommonData.Enums.ProductionTimelineType.Transmission)).Count
    '      ''if the total timeline count is = transmission count, then only transmission times have been specified, and this is not allow because it causes clashing conflicts
    '      'If TransmissionCount = Me.ProductionSystemAreaList(0).ProductionTimelineList.Count Then
    '      '  Message = Message & "There are only transmission times are currently specified in the timeline." & "</br>"
    '      'End If

    '      ''determine how many people have 0 production days
    '      'Dim HrNames As List(Of String) = New List(Of String)
    '      'For Each HRProductionSchedule As Productions.Schedules.HRProductionSchedule In Me.ProductionSystemAreaList(0).HRProductionScheduleList
    '      '  If HRProductionSchedule.HRProductionScheduleDetailList.Count = 0 Then
    '      '    HrNames.Add(HRProductionSchedule.HumanResource)
    '      '  End If
    '      'Next

    '      'If HrNames.Count > 0 Then
    '      '  Message += "The following people do not have a crew schedule:" & "</br>"
    '      '  For Each HR As String In HrNames
    '      '    Message += HR & "</br>"
    '      '  Next
    '      'End If

    '      Return Message

    '    End Function

    '#End Region

    '#Region " Emails "

    '    Private Function GetEventManagerName() As String
    '      Dim EventManagers As String = ""
    '      'For Each phr As ProductionHumanResource In Me.ProductionSystemAreaList(0).ProductionHumanResourceList.Where(Function(d) CompareSafe(d.DisciplineID, CType(CommonData.Enums.Discipline.EventManager, Integer)) _
    '      '                                                                                                            OrElse CompareSafe(d.DisciplineID, CType(CommonData.Enums.Discipline.EventManagerIntern, Integer)))
    '      '  If phr.HumanResourceID IsNot Nothing Then
    '      '    Dim ROHR As OBLib.HR.ReadOnly.ROHumanResource = CommonData.Lists.ROHumanResourceList.GetItem(phr.HumanResourceID)
    '      '    If ROHR IsNot Nothing Then
    '      '      If EventManagers = "" Then
    '      '        EventManagers += ROHR.PreferredFirstSurname
    '      '      Else
    '      '        EventManagers += " " & ROHR.PreferredFirstSurname
    '      '      End If
    '      '    End If
    '      '  End If
    '      'Next
    '      Return EventManagers
    '    End Function

    '    Private Sub SetupEmails()

    '      Throw New Exception("Removed for Rebuild - SetupEmails")

    '      'Dim EventManagers As String = ""

    '      ''Production Un-Reconciled Email
    '      'If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CType(CommonData.Enums.System.ProductionServices, Integer)) AndAlso OriginalReconciled AndAlso ReconciledDate Is Nothing Then
    '      '  If EventManagers = "" Then
    '      '    EventManagers = GetEventManagerName()
    '      '  End If

    '      '  'For Each phr As ProductionHumanResource In Me.ProductionSystemAreaList(0).ProductionHumanResourceList.Where(Function(d) CompareSafe(d.DisciplineID, CType(CommonData.Enums.Discipline.EventManager, Integer)))
    '      '  '  If phr.HumanResourceID IsNot Nothing Then
    '      '  '    Dim ROHR As OBLib.HR.ReadOnly.ROHumanResource = CommonData.Lists.ROHumanResourceList.GetItem(phr.HumanResourceID)
    '      '  '    If ROHR IsNot Nothing Then
    '      '  '      If EventManagers = "" Then
    '      '  '        EventManagers += ROHR.PreferredFirstSurname
    '      '  '      Else
    '      '  '        EventManagers += " " & ROHR.PreferredFirstSurname
    '      '  '      End If
    '      '  '    End If
    '      '  '  End If
    '      '  'Next
    '      '  'Dim UnReconciledProduction As OBLib.Emails.UnReconciledProductions = New OBLib.Emails.UnReconciledProductions(Me, OBLib.Security.Settings.CurrentUser, OBLib.Security.Settings.CurrentUser.SystemID, EventManagers)
    '      '  'Me.EmailList.Add(UnReconciledProduction.Email)
    '      'End If

    '      'If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CType(CommonData.Enums.System.ProductionServices, Integer)) AndAlso LoadedCrewFinalised AndAlso CrewPlanningFinalisedDate Is Nothing Then
    '      '  If EventManagers = "" Then
    '      '    EventManagers = GetEventManagerName()
    '      '  End If

    '      '  'Dim UnReconciledProduction As OBLib.Emails.UnCrewFinalisedProductions = New OBLib.Emails.UnCrewFinalisedProductions(Me, OBLib.Security.Settings.CurrentUser, OBLib.Security.Settings.CurrentUser.SystemID, EventManagers)
    '      '  'Me.EmailList.Add(UnReconciledProduction.Email)

    '      'End If

    '      'If Not Me.IsNew Then
    '      '  If OBLib.Helpers.Production.MainDetailsChanged(LoadedProductionTypeID, ProductionTypeID,
    '      '                                                 LoadedEventTypeID, EventTypeID,
    '      '                                                 LoadedProductionVenueID, ProductionVenueID,
    '      '                                                 LoadedTeamsPlaying, TeamsPlaying,
    '      '                                                 LoadedPlayStartDateTime, PlayStartDateTime,
    '      '                                                 LoadedPlayEndDateTime, PlayEndDateTime,
    '      '                                                 LoadedTitle, Title) Then
    '      '    Dim PDChangedEmail As OBLib.Emails.ProductionDetailsChangedEmail = New OBLib.Emails.ProductionDetailsChangedEmail(LoadedProductionTypeID, ProductionTypeID,
    '      '                                                                                                                      LoadedEventTypeID, EventTypeID,
    '      '                                                                                                                      LoadedProductionVenueID, ProductionVenueID,
    '      '                                                                                                                      LoadedTeamsPlaying, TeamsPlaying,
    '      '                                                                                                                      LoadedPlayStartDateTime, PlayStartDateTime,
    '      '                                                                                                                      LoadedPlayEndDateTime, PlayEndDateTime, OBLib.Security.Settings.CurrentUserID,
    '      '                                                                                                                      LoadedTitle, Title)
    '      '    Me.EmailList.Add(PDChangedEmail.RCEmail)
    '      '  End If
    '      'End If

    '    End Sub

    '#End Region

    '#Region " Post Save Functionality "

    '    Public Sub DoPostSave()

    '      Throw New Exception("Removed for Rebuild - DoPostSave")

    '      ''If anyone has been removed
    '      'If Me.ProductionSystemAreaList(0).RemovedHumanResourceList.Count > 0 Then
    '      '  Dim RemovedTravelEmail As Singular.Emails.Email = CheckTravelForRemovedHumanResources()
    '      '  If RemovedTravelEmail IsNot Nothing Then
    '      '    Me.EmailList.Add(RemovedTravelEmail)
    '      '    Me.EmailList.Save()
    '      '    Me.EmailList.Clear()
    '      '  End If
    '      '  RemoveHumanResourceTravel()
    '      'End If
    '      'Me.ProductionSystemAreaList(0).RemovedHumanResourceList.Clear()

    '      ''If anyone has been changed after crew finalised
    '      'If Me.ProductionSystemAreaList(0).RecordChangeList.Count > 0 Then
    '      '  Dim RecordChangeEmail As Singular.Emails.Email = New OBLib.Emails.ChangedProductionHumanResources(Me, OBLib.Security.Settings.CurrentUser.SystemID).Email
    '      '  If RecordChangeEmail IsNot Nothing Then
    '      '    Me.EmailList.Add(RecordChangeEmail)
    '      '    Me.EmailList.Save()
    '      '    Me.EmailList.Clear()
    '      '  End If
    '      'End If
    '      'Me.ProductionSystemAreaList(0).RecordChangeList.Clear()

    '    End Sub

    '    Public Function CheckTravelForRemovedHumanResources() As Singular.Emails.Email

    '      Dim cmd As New Singular.CommandProc("CmdProcs.cmdGetTravelAndProductionHumanResources", _
    '                                          New String() {"ProductionID",
    '                                                        "SystemID",
    '                                                        "ProductionAreaID"}, _
    '                                          New Object() {Me.ProductionID,
    '                                                        OBLib.Security.Settings.CurrentUser.SystemID,
    '                                                        CType(OBLib.CommonData.Enums.ProductionArea.OB, Integer)})

    '      cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
    '      cmd = cmd.Execute(0)

    '      'No Travel book
    '      If cmd.Dataset.Tables(0).Rows.Count > 0 Then
    '        Dim TravelHRIDs As List(Of Integer) = New List(Of Integer)
    '        Dim ProductionHRIDs As List(Of Integer) = New List(Of Integer)
    '        Dim UnmatchedHRIDs As List(Of String) = New List(Of String)
    '        'Travel
    '        For Each Row As DataRow In cmd.Dataset.Tables(0).Rows
    '          TravelHRIDs.Add(Row("HumanResourceID"))
    '        Next
    '        'Production
    '        For Each Row As DataRow In cmd.Dataset.Tables(1).Rows
    '          ProductionHRIDs.Add(Row("HumanResourceID"))
    '        Next
    '        'Match Travel HR to PHR
    '        TravelHRIDs.ForEach(Sub(z)
    '                              If Not ProductionHRIDs.Contains(z) Then
    '                                UnmatchedHRIDs.Add(z.ToString)
    '                              End If
    '                            End Sub)
    '        'If there is Travel booked for deleted Production Human Resources
    '        If UnmatchedHRIDs.Count > 0 Then
    '          Dim EmailList As Singular.Emails.EmailList = Singular.Emails.EmailList.NewEmailList()
    '          Dim emr As OBLib.Emails.RemovedProductionHumanResources = New OBLib.Emails.RemovedProductionHumanResources(Me.ProductionID, OBLib.Security.Settings.CurrentUser.SystemID, OBLib.Helpers.MiscHelper.StringArrayToXML(UnmatchedHRIDs.ToArray), OBLib.Security.Settings.CurrentUser.ProductionAreaID)
    '          Dim em As Singular.Emails.Email = emr.Email
    '          Return em
    '        End If
    '      End If

    '      Return Nothing

    '    End Function

    '    Public Sub RemoveHumanResourceTravel()

    '      Throw New Exception("Removed for Rebuild - RemoveHumanResourceTravel")
    '      'If Me.ProductionSystemAreaList(0).RemovedHumanResourceList.Count > 0 Then
    '      '  Dim HRIDList As List(Of String) = New List(Of String)
    '      '  For Each phrh As ProductionHumanResource In Me.ProductionSystemAreaList(0).RemovedHumanResourceList
    '      '    Dim PreferredHRID As Integer? = Nothing
    '      '    'In the case then the Production Human Resource record is deleted, won't find it in ProductionHumanResourceList
    '      '    Dim cphr As ProductionHumanResource = Me.ProductionSystemAreaList(0).ProductionHumanResourceList.GetItem(phrh.ProductionHumanResourceID)
    '      '    If cphr IsNot Nothing Then
    '      '      Dim tmp As ProductionHumanResource = Me.ProductionSystemAreaList(0).ProductionHumanResourceList.GetItem(phrh.ProductionHumanResourceID)
    '      '      If tmp.PreferredHumanResourceID IsNot Nothing Then
    '      '        PreferredHRID = tmp.PreferredHumanResourceID
    '      '      End If
    '      '    End If
    '      '    Dim CompareHRID As Integer? = phrh.LoadedHumanResourceID
    '      '    If Me.ProductionSystemAreaList(0).ProductionHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, CompareHRID)).Count = 0 Then
    '      '      If IsNullNothing(PreferredHRID) Then
    '      '        HRIDList.Add(phrh.LoadedHumanResourceID.ToString)
    '      '      ElseIf phrh.LoadedHumanResourceID <> PreferredHRID Then
    '      '        HRIDList.Add(phrh.HumanResourceID.ToString)
    '      '      End If
    '      '    End If
    '      '  Next

    '      '  If HRIDList.Count > 0 Then
    '      '    Dim HRIDs As String = OBLib.Helpers.MiscHelper.StringArrayToXML(HRIDList.ToArray)
    '      '    Dim cmd As New Singular.CommandProc("CmdProcs.cmdCancelHumanResourceTravel", _
    '      '                                New String() {"HumanResourceIDs",
    '      '                                              "ProductionID",
    '      '                                              "UserID",
    '      '                                              "SystemID",
    '      '                                              "ProductionAreaID"}, _
    '      '                                New Object() {HRIDs,
    '      '                                              Me.ProductionID,
    '      '                                              Settings.CurrentUser.UserID,
    '      '                                              OBLib.Security.Settings.CurrentUser.SystemID,
    '      '                                              CType(OBLib.CommonData.Enums.ProductionArea.OB, Integer)})
    '      '    cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
    '      '    cmd = cmd.Execute(0)
    '      '    If cmd.Dataset.Tables(0).Rows.Count > 0 Then
    '      '      Dim HRName As String = ""
    '      '      For Each Row As DataRow In cmd.Dataset.Tables(0).Rows
    '      '        If HRName = "" Then
    '      '          HRName += Row("HumanResource")
    '      '        Else
    '      '          HRName += ", " + Row("HumanResource")
    '      '        End If
    '      '      Next
    '      '    End If
    '      '  End If
    '      'End If

    '    End Sub

    '#End Region

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(TeamsPlayingProperty)
        .ServerRuleFunction = AddressOf TeamsPlayingValid
        .JavascriptRuleFunctionName = "ProductionBO.TeamsPlayingValid"
      End With

      With AddWebRule(PlayEndDateTimeProperty)
        .ServerRuleFunction = AddressOf PlayEndDateTimeValid
        .JavascriptRuleFunctionName = "ProductionBO.PlayEndDateTimeValid"
      End With

    End Sub

    Public Shared Function TeamsPlayingValid(Production As Production) As String
      If Production.IsOBView AndAlso Production.TeamsPlaying.Trim.Length = 0 Then
        Return "Teams are required"
      End If
      Return ""
    End Function

    Public Shared Function PlayEndDateTimeValid(Production As Production) As String
      If Production.IsOBView AndAlso Production.PlayEndDateTime Is Nothing Then
        Return "Live End Time is required"
      End If
      Return ""
    End Function

#End Region

#Region " Data Access & Factory Methods"

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProduction() method.

    End Sub

    Public Shared Function NewProduction() As Production

      Return DataPortal.CreateChild(Of Production)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProduction(dr As SafeDataReader) As Production

      Dim p As New Production()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionIDProperty, .GetInt32(0))
          LoadProperty(ProductionDescriptionProperty, .GetString(1))
          LoadProperty(TeamsPlayingProperty, .GetString(2))
          LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(VenueConfirmedDateProperty, .GetValue(6))
          LoadProperty(ProductionRefNoProperty, .GetString(7))
          LoadProperty(ParentProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ModifiedByProperty, .GetInt32(11))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
          LoadProperty(SynergyGenRefNoProperty, ZeroNothing(.GetInt64(13)))
          LoadProperty(PlayStartDateTimeProperty, .GetValue(14))
          LoadProperty(PlayEndDateTimeProperty, .GetValue(15))
          LoadProperty(TitleProperty, .GetString(16))
          LoadProperty(CreationTypeIDProperty, .GetInt32(17))
          LoadProperty(PlaceholderIndProperty, .GetBoolean(18))
          LoadProperty(VisionViewIndProperty, .GetBoolean(19))
          LoadProperty(GraphicsSuppliersProperty, .GetString(20))
          LoadProperty(OBFacilitySuppliersProperty, .GetString(21))
          LoadProperty(CityProperty, .GetString(22))
          LoadProperty(ProductionTypeProperty, .GetString(23))
          LoadProperty(EventTypeProperty, .GetString(24))
          LoadProperty(ProductionVenueProperty, .GetString(25))
          LoadProperty(IsOBViewProperty, .GetBoolean(26))
          LoadProperty(IsRoomViewProperty, .GetBoolean(27))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProduction"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProduction"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionID As SqlParameter = .Parameters.Add("@ProductionID", SqlDbType.Int)
          paramProductionID.Value = GetProperty(ProductionIDProperty)
          Dim paramProductionRefNo As SqlParameter = .Parameters.Add("@ProductionRefNo", SqlDbType.VarChar)
          paramProductionRefNo.Size = 50
          paramProductionRefNo.Value = GetProperty(ProductionRefNoProperty)
          If Me.IsNew Then
            paramProductionID.Direction = ParameterDirection.Output
            paramProductionRefNo.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionDescription", GetProperty(ProductionDescriptionProperty))
          .Parameters.AddWithValue("@TeamsPlaying", GetProperty(TeamsPlayingProperty))
          .Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(GetProperty(ProductionTypeIDProperty)))
          .Parameters.AddWithValue("@EventTypeID", NothingDBNull(GetProperty(EventTypeIDProperty)))
          .Parameters.AddWithValue("@ProductionVenueID", NothingDBNull(GetProperty(ProductionVenueIDProperty)))
          .Parameters.AddWithValue("@VenueConfirmedDate", (New SmartDate(GetProperty(VenueConfirmedDateProperty))).DBValue)
          '.Parameters.AddWithValue("@ProductionRefNo", GetProperty(ProductionRefNoProperty))
          .Parameters.AddWithValue("@ParentProductionID", NothingDBNull(GetProperty(ParentProductionIDProperty)))
          '.Parameters.AddWithValue("@SystemID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@SynergyGenRefNo", NothingDBNull(GetProperty(SynergyGenRefNoProperty)))
          .Parameters.AddWithValue("@PlayStartDateTime", (New SmartDate(GetProperty(PlayStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@PlayEndDateTime", (New SmartDate(GetProperty(PlayEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          '.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(OBLib.Security.Settings.CurrentUser.ProductionAreaID))
          .Parameters.AddWithValue("@PlaceHolderInd", GetProperty(PlaceholderIndProperty))
          '.Parameters.AddWithValue("@CreationTypeID", NothingDBNull(GetProperty(CreationTypeIDProperty)))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionIDProperty, paramProductionID.Value)
            LoadProperty(ProductionRefNoProperty, paramProductionRefNo.Value)
          End If
          'SetupEmails()
          ' update child objects
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProduction"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub
      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace