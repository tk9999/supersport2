﻿' Generated 04 Nov 2014 09:47 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Changes

  <Serializable()> _
  Public Class ProductionChange
    Inherits SingularBusinessBase(Of ProductionChange)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionChangeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionChangeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionChangeID() As Integer
      Get
        Return GetProperty(ProductionChangeIDProperty)
      End Get
    End Property

    Public Shared ProductionChangeTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionChangeTypeID, "Production Change Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Change Type value
    ''' </summary>
    <Display(Name:="Production Change Type", Description:=""),
    Required(ErrorMessage:="Production Change Type required")>
    Public Property ProductionChangeTypeID() As Integer?
      Get
        Return GetProperty(ProductionChangeTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionChangeTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ChangedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ChangedBy, "Changed By", Nothing)
    ''' <summary>
    ''' Gets and sets the Changed By value
    ''' </summary>
    <Display(Name:="Changed By", Description:=""),
    Required(ErrorMessage:="Changed By required")>
    Public Property ChangedBy() As Integer?
      Get
        Return GetProperty(ChangedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ChangedByProperty, Value)
      End Set
    End Property

    Public Shared ChangedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ChangedDateTime, "Changed Date Time")
    ''' <summary>
    ''' Gets and sets the Changed Date Time value
    ''' </summary>
    <Display(Name:="Changed Date Time", Description:=""),
    Required(ErrorMessage:="Changed Date Time required")>
    Public Property ChangedDateTime As DateTime?
      Get
        Return GetProperty(ChangedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ChangedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:=""),
    Required(ErrorMessage:="Production required")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared OldProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OldProductionTypeID, "Old Production Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Old Production Type value
    ''' </summary>
    <Display(Name:="Old Production Type", Description:="")>
    Public Property OldProductionTypeID() As Integer?
      Get
        Return GetProperty(OldProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OldProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared NewProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NewProductionTypeID, "New Production Type", Nothing)
    ''' <summary>
    ''' Gets and sets the New Production Type value
    ''' </summary>
    <Display(Name:="New Production Type", Description:="")>
    Public Property NewProductionTypeID() As Integer?
      Get
        Return GetProperty(NewProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NewProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared OldEventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OldEventTypeID, "Old Event Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Old Event Type value
    ''' </summary>
    <Display(Name:="Old Event Type", Description:="")>
    Public Property OldEventTypeID() As Integer?
      Get
        Return GetProperty(OldEventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OldEventTypeIDProperty, Value)
      End Set
    End Property

    Public Shared NewEventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NewEventTypeID, "New Event Type", Nothing)
    ''' <summary>
    ''' Gets and sets the New Event Type value
    ''' </summary>
    <Display(Name:="New Event Type", Description:="")>
    Public Property NewEventTypeID() As Integer?
      Get
        Return GetProperty(NewEventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NewEventTypeIDProperty, Value)
      End Set
    End Property

    Public Shared OldProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OldProductionVenueID, "Old Production Venue", Nothing)
    ''' <summary>
    ''' Gets and sets the Old Production Venue value
    ''' </summary>
    <Display(Name:="Old Production Venue", Description:="")>
    Public Property OldProductionVenueID() As Integer?
      Get
        Return GetProperty(OldProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OldProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared NewProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NewProductionVenueID, "New Production Venue", Nothing)
    ''' <summary>
    ''' Gets and sets the New Production Venue value
    ''' </summary>
    <Display(Name:="New Production Venue", Description:="")>
    Public Property NewProductionVenueID() As Integer?
      Get
        Return GetProperty(NewProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NewProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared OldTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OldTitle, "Old Title", "")
    ''' <summary>
    ''' Gets and sets the Old Title value
    ''' </summary>
    <Display(Name:="Old Title", Description:=""),
    StringLength(500, ErrorMessage:="Old Title cannot be more than 500 characters")>
    Public Property OldTitle() As String
      Get
        Return GetProperty(OldTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OldTitleProperty, Value)
      End Set
    End Property

    Public Shared NewTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NewTitle, "New Title", "")
    ''' <summary>
    ''' Gets and sets the New Title value
    ''' </summary>
    <Display(Name:="New Title", Description:=""),
    StringLength(500, ErrorMessage:="New Title cannot be more than 500 characters")>
    Public Property NewTitle() As String
      Get
        Return GetProperty(NewTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NewTitleProperty, Value)
      End Set
    End Property

    Public Shared OldTeamsPlayingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OldTeamsPlaying, "Old Teams Playing", "")
    ''' <summary>
    ''' Gets and sets the Old Teams Playing value
    ''' </summary>
    <Display(Name:="Old Teams Playing", Description:=""),
    StringLength(200, ErrorMessage:="Old Teams Playing cannot be more than 200 characters")>
    Public Property OldTeamsPlaying() As String
      Get
        Return GetProperty(OldTeamsPlayingProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OldTeamsPlayingProperty, Value)
      End Set
    End Property

    Public Shared NewTeamsPlayingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NewTeamsPlaying, "New Teams Playing", "")
    ''' <summary>
    ''' Gets and sets the New Teams Playing value
    ''' </summary>
    <Display(Name:="New Teams Playing", Description:=""),
    StringLength(200, ErrorMessage:="New Teams Playing cannot be more than 200 characters")>
    Public Property NewTeamsPlaying() As String
      Get
        Return GetProperty(NewTeamsPlayingProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NewTeamsPlayingProperty, Value)
      End Set
    End Property

    Public Shared OldEventStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OldEventStartDateTime, "Old Event Start Date Time")
    ''' <summary>
    ''' Gets and sets the Old Event Start Date Time value
    ''' </summary>
    <Display(Name:="Old Event Start Date Time", Description:="")>
    Public Property OldEventStartDateTime As DateTime?
      Get
        Return GetProperty(OldEventStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OldEventStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared NewEventStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.NewEventStartDateTime, "New Event Start Date Time")
    ''' <summary>
    ''' Gets and sets the New Event Start Date Time value
    ''' </summary>
    <Display(Name:="New Event Start Date Time", Description:="")>
    Public Property NewEventStartDateTime As DateTime?
      Get
        Return GetProperty(NewEventStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(NewEventStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared OldEventEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OldEventEndDateTime, "Old Event End Date Time")
    ''' <summary>
    ''' Gets and sets the Old Event End Date Time value
    ''' </summary>
    <Display(Name:="Old Event End Date Time", Description:="")>
    Public Property OldEventEndDateTime As DateTime?
      Get
        Return GetProperty(OldEventEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OldEventEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared NewEventEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.NewEventEndDateTime, "New Event End Date Time")
    ''' <summary>
    ''' Gets and sets the New Event End Date Time value
    ''' </summary>
    <Display(Name:="New Event End Date Time", Description:="")>
    Public Property NewEventEndDateTime As DateTime?
      Get
        Return GetProperty(NewEventEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(NewEventEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ProductionChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionChannelID, "Production Channel", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Channel value
    ''' </summary>
    <Display(Name:="Production Channel", Description:="")>
    Public Property ProductionChannelID() As Integer?
      Get
        Return GetProperty(ProductionChannelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionChannelIDProperty, Value)
      End Set
    End Property

    Public Shared AddedChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AddedChannelID, "Added Channel", Nothing)
    ''' <summary>
    ''' Gets and sets the Added Channel value
    ''' </summary>
    <Display(Name:="Added Channel", Description:="")>
    Public Property AddedChannelID() As Integer?
      Get
        Return GetProperty(AddedChannelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AddedChannelIDProperty, Value)
      End Set
    End Property

    Public Shared ChannelStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ChannelStartDateTime, "Channel Start Date Time")
    ''' <summary>
    ''' Gets and sets the Channel Start Date Time value
    ''' </summary>
    <Display(Name:="Channel Start Date Time", Description:="")>
    Public Property ChannelStartDateTime As DateTime?
      Get
        Return GetProperty(ChannelStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ChannelStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ChannelEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ChannelEndDateTime, "Channel End Date Time")
    ''' <summary>
    ''' Gets and sets the Channel End Date Time value
    ''' </summary>
    <Display(Name:="Channel End Date Time", Description:="")>
    Public Property ChannelEndDateTime As DateTime?
      Get
        Return GetProperty(ChannelEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ChannelEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared PreviousChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PreviousChannelID, "Previous Channel", Nothing)
    ''' <summary>
    ''' Gets and sets the Previous Channel value
    ''' </summary>
    <Display(Name:="Previous Channel", Description:="")>
    Public Property PreviousChannelID() As Integer?
      Get
        Return GetProperty(PreviousChannelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PreviousChannelIDProperty, Value)
      End Set
    End Property

    Public Shared SwitchedToChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SwitchedToChannelID, "Switched To Channel", Nothing)
    ''' <summary>
    ''' Gets and sets the Switched To Channel value
    ''' </summary>
    <Display(Name:="Switched To Channel", Description:="")>
    Public Property SwitchedToChannelID() As Integer?
      Get
        Return GetProperty(SwitchedToChannelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SwitchedToChannelIDProperty, Value)
      End Set
    End Property

    Public Shared OriginalChannelStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalChannelStartDateTime, "Original Channel Start Date Time")
    ''' <summary>
    ''' Gets and sets the Original Channel Start Date Time value
    ''' </summary>
    <Display(Name:="Original Channel Start Date Time", Description:="")>
    Public Property OriginalChannelStartDateTime As DateTime?
      Get
        Return GetProperty(OriginalChannelStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OriginalChannelStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared OriginalChannelEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalChannelEndDateTime, "Original Channel End Date Time")
    ''' <summary>
    ''' Gets and sets the Original Channel End Date Time value
    ''' </summary>
    <Display(Name:="Original Channel End Date Time", Description:="")>
    Public Property OriginalChannelEndDateTime As DateTime?
      Get
        Return GetProperty(OriginalChannelEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OriginalChannelEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared OldProductionChannelStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OldProductionChannelStatusID, "Old Production Channel Status", Nothing)
    ''' <summary>
    ''' Gets and sets the Old Production Channel Status value
    ''' </summary>
    <Display(Name:="Old Production Channel Status", Description:="")>
    Public Property OldProductionChannelStatusID() As Integer?
      Get
        Return GetProperty(OldProductionChannelStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OldProductionChannelStatusIDProperty, Value)
      End Set
    End Property

    Public Shared NewProductionChannelStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NewProductionChannelStatusID, "New Production Channel Status", Nothing)
    ''' <summary>
    ''' Gets and sets the New Production Channel Status value
    ''' </summary>
    <Display(Name:="New Production Channel Status", Description:="")>
    Public Property NewProductionChannelStatusID() As Integer?
      Get
        Return GetProperty(NewProductionChannelStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NewProductionChannelStatusIDProperty, Value)
      End Set
    End Property

    Public Shared ReleasedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ReleasedInd, "Released", False)
    ''' <summary>
    ''' Gets and sets the Released value
    ''' </summary>
    <Display(Name:="Released", Description:="")>
    Public Property ReleasedInd() As Boolean
      Get
        Return GetProperty(ReleasedIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ReleasedIndProperty, Value)
      End Set
    End Property

    Public Shared ReleasedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ReleasedBy, "Released By", Nothing)
    ''' <summary>
    ''' Gets and sets the Released By value
    ''' </summary>
    <Display(Name:="Released By", Description:="")>
    Public Property ReleasedBy() As Integer?
      Get
        Return GetProperty(ReleasedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ReleasedByProperty, Value)
      End Set
    End Property

    Public Shared ReleasedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ReleasedDateTime, "Released Date Time")
    ''' <summary>
    ''' Gets and sets the Released Date Time value
    ''' </summary>
    <Display(Name:="Released Date Time", Description:="")>
    Public Property ReleasedDateTime As DateTime?
      Get
        Return GetProperty(ReleasedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ReleasedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared OldGenRefProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.OldGenRef, "Old Gen Ref", Nothing)
    ''' <summary>
    ''' Gets and sets the Released By value
    ''' </summary>
    <Display(Name:="Released By", Description:="")>
    Public Property OldGenRef() As Int64?
      Get
        Return GetProperty(OldGenRefProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(OldGenRefProperty, Value)
      End Set
    End Property

    Public Shared NewGenRefProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.NewGenRef, "New Gen Ref", Nothing)
    ''' <summary>
    ''' Gets and sets the Released By value
    ''' </summary>
    <Display(Name:="Released By", Description:="")>
    Public Property NewGenRef() As Int64?
      Get
        Return GetProperty(NewGenRefProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(NewGenRefProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionChangeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.OldTitle.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Change")
        Else
          Return String.Format("Blank {0}", "Production Change")
        End If
      Else
        Return Me.OldTitle
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionChange() method.

    End Sub

    Public Shared Function NewProductionChange() As ProductionChange

      Return DataPortal.CreateChild(Of ProductionChange)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionChange(dr As SafeDataReader) As ProductionChange

      Dim p As New ProductionChange()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionChangeIDProperty, .GetInt32(0))
          LoadProperty(ProductionChangeTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ChangedByProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ChangedDateTimeProperty, .GetValue(3))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(OldProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(NewProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(OldEventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(NewEventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(OldProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(NewProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(OldTitleProperty, .GetString(11))
          LoadProperty(NewTitleProperty, .GetString(12))
          LoadProperty(OldTeamsPlayingProperty, .GetString(13))
          LoadProperty(NewTeamsPlayingProperty, .GetString(14))
          LoadProperty(OldEventStartDateTimeProperty, .GetValue(15))
          LoadProperty(NewEventStartDateTimeProperty, .GetValue(16))
          LoadProperty(OldEventEndDateTimeProperty, .GetValue(17))
          LoadProperty(NewEventEndDateTimeProperty, .GetValue(18))
          LoadProperty(ProductionChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
          LoadProperty(AddedChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(20)))
          LoadProperty(ChannelStartDateTimeProperty, .GetValue(21))
          LoadProperty(ChannelEndDateTimeProperty, .GetValue(22))
          LoadProperty(PreviousChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
          LoadProperty(SwitchedToChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
          LoadProperty(OriginalChannelStartDateTimeProperty, .GetValue(25))
          LoadProperty(OriginalChannelEndDateTimeProperty, .GetValue(26))
          LoadProperty(OldProductionChannelStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(27)))
          LoadProperty(NewProductionChannelStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(28)))
          LoadProperty(ReleasedIndProperty, .GetBoolean(29))
          LoadProperty(ReleasedByProperty, Singular.Misc.ZeroNothing(.GetInt32(30)))
          LoadProperty(ReleasedDateTimeProperty, .GetValue(31))
          LoadProperty(OldGenRefProperty, ZeroNothing(.GetInt64(32)))
          LoadProperty(NewGenRefProperty, ZeroNothing(.GetInt64(33)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionChange"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionChange"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionChangeID As SqlParameter = .Parameters.Add("@ProductionChangeID", SqlDbType.Int)
          paramProductionChangeID.Value = GetProperty(ProductionChangeIDProperty)
          If Me.IsNew Then
            paramProductionChangeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionChangeTypeID", GetProperty(ProductionChangeTypeIDProperty))
          .Parameters.AddWithValue("@ChangedBy", GetProperty(ChangedByProperty))
          .Parameters.AddWithValue("@ChangedDateTime", (New SmartDate(GetProperty(ChangedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
          .Parameters.AddWithValue("@OldProductionTypeID", Singular.Misc.NothingDBNull(GetProperty(OldProductionTypeIDProperty)))
          .Parameters.AddWithValue("@NewProductionTypeID", Singular.Misc.NothingDBNull(GetProperty(NewProductionTypeIDProperty)))
          .Parameters.AddWithValue("@OldEventTypeID", Singular.Misc.NothingDBNull(GetProperty(OldEventTypeIDProperty)))
          .Parameters.AddWithValue("@NewEventTypeID", Singular.Misc.NothingDBNull(GetProperty(NewEventTypeIDProperty)))
          .Parameters.AddWithValue("@OldProductionVenueID", Singular.Misc.NothingDBNull(GetProperty(OldProductionVenueIDProperty)))
          .Parameters.AddWithValue("@NewProductionVenueID", Singular.Misc.NothingDBNull(GetProperty(NewProductionVenueIDProperty)))
          .Parameters.AddWithValue("@OldTitle", GetProperty(OldTitleProperty))
          .Parameters.AddWithValue("@NewTitle", GetProperty(NewTitleProperty))
          .Parameters.AddWithValue("@OldTeamsPlaying", GetProperty(OldTeamsPlayingProperty))
          .Parameters.AddWithValue("@NewTeamsPlaying", GetProperty(NewTeamsPlayingProperty))
          .Parameters.AddWithValue("@OldEventStartDateTime", (New SmartDate(GetProperty(OldEventStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@NewEventStartDateTime", (New SmartDate(GetProperty(NewEventStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@OldEventEndDateTime", (New SmartDate(GetProperty(OldEventEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@NewEventEndDateTime", (New SmartDate(GetProperty(NewEventEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ProductionChannelID", Singular.Misc.NothingDBNull(GetProperty(ProductionChannelIDProperty)))
          .Parameters.AddWithValue("@AddedChannelID", Singular.Misc.NothingDBNull(GetProperty(AddedChannelIDProperty)))
          .Parameters.AddWithValue("@ChannelStartDateTime", (New SmartDate(GetProperty(ChannelStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ChannelEndDateTime", (New SmartDate(GetProperty(ChannelEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@PreviousChannelID", Singular.Misc.NothingDBNull(GetProperty(PreviousChannelIDProperty)))
          .Parameters.AddWithValue("@SwitchedToChannelID", Singular.Misc.NothingDBNull(GetProperty(SwitchedToChannelIDProperty)))
          .Parameters.AddWithValue("@OriginalChannelStartDateTime", (New SmartDate(GetProperty(OriginalChannelStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@OriginalChannelEndDateTime", (New SmartDate(GetProperty(OriginalChannelEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@OldProductionChannelStatusID", Singular.Misc.NothingDBNull(GetProperty(OldProductionChannelStatusIDProperty)))
          .Parameters.AddWithValue("@NewProductionChannelStatusID", Singular.Misc.NothingDBNull(GetProperty(NewProductionChannelStatusIDProperty)))
          .Parameters.AddWithValue("@ReleasedInd", Singular.Misc.NothingDBNull(GetProperty(ReleasedIndProperty)))
          .Parameters.AddWithValue("@ReleasedBy", Singular.Misc.NothingDBNull(GetProperty(ReleasedByProperty)))
          .Parameters.AddWithValue("@ReleasedDateTime", (New SmartDate(GetProperty(ReleasedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@OldSynergyGenRefNo", NothingDBNull(GetProperty(OldGenRefProperty)))
          .Parameters.AddWithValue("@NewSynergyGenRefNo", NothingDBNull(GetProperty(NewGenRefProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionChangeIDProperty, paramProductionChangeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionChange"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionChangeID", GetProperty(ProductionChangeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace