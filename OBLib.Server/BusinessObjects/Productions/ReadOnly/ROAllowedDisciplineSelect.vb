﻿' Generated 23 May 2015 12:04 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Productions.ReadOnly

  <Serializable()> _
  Public Class ROAllowedDisciplineSelect
    Inherits OBReadOnlyBase(Of ROAllowedDisciplineSelect)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared QuantityProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.Quantity, 0)
    ''' <summary>
    ''' Gets the Outside Broadcast Ind value
    ''' </summary>
    Public Property Quantity() As Integer
      Get
        Return GetProperty(QuantityProperty)
      End Get
      Set(value As Integer)
        LoadProperty(QuantityProperty, value)
      End Set
    End Property

    Public Shared AllowedDisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AllowedDisciplineID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property AllowedDisciplineID() As Integer
      Get
        Return GetProperty(AllowedDisciplineIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
    End Property

    Public Shared PositionRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PositionRequiredInd, "Position Required", False)
    ''' <summary>
    ''' Gets the Position Required value
    ''' </summary>
    <Display(Name:="Position Required", Description:="")>
    Public ReadOnly Property PositionRequiredInd() As Boolean
      Get
        Return GetProperty(PositionRequiredIndProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
    End Property

    Public Shared OrderNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNo, "Order No")
    ''' <summary>
    ''' Gets the Order No value
    ''' </summary>
    <Display(Name:="Order No", Description:="")>
    Public ReadOnly Property OrderNo() As Integer
      Get
        Return GetProperty(OrderNoProperty)
      End Get
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:=""), Key>
    Public ReadOnly Property RowNo() As Int64
      Get
        Return GetProperty(RowNoProperty)
      End Get
    End Property

    Public Shared DisciplinePositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DisciplinePosition, "Discipline (Position)")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property DisciplinePosition() As String
      Get
        Return GetProperty(DisciplinePositionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AllowedDisciplineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Discipline

    End Function


    Public Sub LoadDisciplinePosition()

      LoadProperty(DisciplinePositionProperty, Me.Discipline & IIf(Me.PositionID IsNot Nothing, " (" & Me.Position & ")", ""))

    End Sub

    Shared Sub New()

      CType(IsSelectedProperty, Singular.SPropertyInfo(Of Boolean, ROAllowedDisciplineSelect)).AddSetExpression("ROAllowedDisciplineSelectBO.IsSelectedSet(self)", False)

    End Sub

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAllowedDisciplineSelect(dr As SafeDataReader) As ROAllowedDisciplineSelect

      Dim r As New ROAllowedDisciplineSelect()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(AllowedDisciplineIDProperty, .GetInt32(0))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(DisciplineProperty, .GetString(2))
        LoadProperty(PositionProperty, .GetString(3))
        LoadProperty(PositionRequiredIndProperty, .GetBoolean(4))
        LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(OrderNoProperty, .GetInt32(6))
        LoadProperty(RowNoProperty, .GetInt32(7))
      End With
      LoadDisciplinePosition()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace