﻿' Generated 13 Apr 2015 13:58 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base.ReadOnly

  <Serializable()> _
  Public Class ROProductionDetailList
    Inherits SingularReadOnlyListBase(Of ROProductionDetailList, ROProductionDetail)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROProductionDetail

      For Each child As ROProductionDetail In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared TxDateFromProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.TxDateFrom, Nothing)
      <Display(Name:="Start Date", Description:="")>
      Public Overridable Property TxDateFrom As DateTime?
        Get
          Return ReadProperty(TxDateFromProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(TxDateFromProperty, Value)
        End Set
      End Property

      Public Shared TxDateToProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.TxDateTo, Nothing)
      <Display(Name:="End Date", Description:="")>
      Public Overridable Property TxDateTo As DateTime?
        Get
          Return ReadProperty(TxDateToProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(TxDateToProperty, Value)
        End Set
      End Property

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Keyword, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Keyword", Description:="")>
      Public Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Sub New(Keyword As String)
        Me.Keyword = Keyword
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionDetailList() As ROProductionDetailList

      Return New ROProductionDetailList()

    End Function

    Public Shared Sub BeginGetROProductionDetailList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionDetailList)))

      Dim dp As New DataPortal(Of ROProductionDetailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    Public Shared Sub BeginGetROProductionDetailList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionDetailList)))

      Dim dp As New DataPortal(Of ROProductionDetailList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionDetailList() As ROProductionDetailList

      Return DataPortal.Fetch(Of ROProductionDetailList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionDetail.GetROProductionDetail(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionDetailList"
            cm.Parameters.AddWithValue("@Keyword", Singular.Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@SystemID", NothingDBnull(OBLib.Security.Settings.CurrentUser.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(OBLib.Security.Settings.CurrentUser.ProductionAreaID))
            cm.Parameters.AddWithValue("@TxDateFrom", NothingDBNull(crit.TxDateFrom))
            cm.Parameters.AddWithValue("@TxDateTo", NothingDBNull(crit.TxDateTo))
            cm.Parameters.AddWithValue("CurrentSystemID", NothingDBNull(OBLib.Security.Settings.CurrentUser.SystemID))
            cm.Parameters.AddWithValue("CurrentProductionAreaID", NothingDBNull(OBLib.Security.Settings.CurrentUser.ProductionAreaID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace