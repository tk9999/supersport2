﻿' Generated 30 Apr 2015 10:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions

  <Serializable()> _
  Public Class SelectedAllowedDiscipline
    Inherits SingularBusinessBase(Of SelectedAllowedDiscipline)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNo, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property RowNo() As Integer
      Get
        Return GetProperty(RowNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RowNoProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "DisciplineID", Nothing)
    ''' <summary>
    ''' Gets and sets the Type Order value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets and sets the Resource Name value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position", "")
    ''' <summary>
    ''' Gets and sets the Resource Name value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionProperty, Value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "PositionID", Nothing)
    ''' <summary>
    ''' Gets and sets the Type Order value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared OrderNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNo, "Type Order", 0)
    ''' <summary>
    ''' Gets and sets the Type Order value
    ''' </summary>
    <Display(Name:="Type Order", Description:="")>
    Public Property OrderNo() As Integer
      Get
        Return GetProperty(OrderNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OrderNoProperty, Value)
      End Set
    End Property

    Public Shared QuantityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Quantity, "Quantity", 0)
    ''' <summary>
    ''' Gets and sets the Type Order value
    ''' </summary>
    <Display(Name:="Quantity", Description:="")>
    Public Property Quantity() As Integer
      Get
        Return GetProperty(QuantityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(QuantityProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "ProductionSystemArea", Nothing)
    ''' <summary>
    ''' Gets and sets the Type Order value
    ''' </summary>
    <Display(Name:="ProductionSystemArea", Description:="")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the Type Order value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Type Order value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Type Order value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RowNoProperty)

    End Function

    Public Overrides Function ToString() As String

      Return ""

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSelectedAllowedDiscipline() method.

    End Sub

    Public Shared Function NewSelectedAllowedDiscipline() As SelectedAllowedDiscipline

      Return DataPortal.CreateChild(Of SelectedAllowedDiscipline)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSelectedAllowedDiscipline(dr As SafeDataReader) As SelectedAllowedDiscipline

      Dim r As New SelectedAllowedDiscipline()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          'LoadProperty(ResourceIDProperty, .GetInt32(0))
          'LoadProperty(ResourceNameProperty, .GetString(1))
          'LoadProperty(ResourceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          'LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          'LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          'LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          'LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          'LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          'LoadProperty(CreatedByProperty, .GetInt32(8))
          'LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
          'LoadProperty(ModifiedByProperty, .GetInt32(10))
          'LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSelectedAllowedDiscipline"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      '' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "UpdProcsWeb.updSelectedAllowedDiscipline"
      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then
        With cm
          .CommandType = CommandType.StoredProcedure
          .Parameters.AddWithValue("@RowNo", GetProperty(RowNoProperty))
          .Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
          .Parameters.AddWithValue("@PositionID", Singular.Misc.NothingDBNull(GetProperty(PositionIDProperty)))
          .Parameters.AddWithValue("@Quantity", GetProperty(QuantityProperty))
          .Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))
          .Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(GetProperty(RoomIDProperty)))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
          .ExecuteNonQuery()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      '' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delSelectedAllowedDiscipline"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@SelectedAllowedDisciplineID", GetProperty(ResourceIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace