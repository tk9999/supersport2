﻿' Generated 22 Feb 2016 12:37 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Imports Singular
Imports Singular.Misc

Namespace Productions.ReadOnly

  <Serializable()> _
  Public Class ROProductionFindList
    Inherits OBReadOnlyListBase(Of ROProductionFindList, ROProductionFind)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROProductionFind

      For Each child As ROProductionFind In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      <Display(Name:="System ID", Description:="")>
      Public Property SystemID As Integer?

      <Display(Name:="Production Area ID", Description:="")>
      Public Property ProductionAreaID As Integer?

      <Display(Name:="Production Ref No", Description:=""), SetExpression("ROProductionFindListCriteriaBO.ProductionRefNoSet(self)", , 400), TextField()>
      Public Property ProductionRefNo As String = ""

      'Required(ErrorMessage:="A Production Reference No. is required to search for a production"),

      <Display(Name:="Production ID", Description:="")>
      Public Property ProductionID As Integer?

      <Display(Name:="Keyword", Description:=""), SetExpression("ROProductionFindListCriteriaBO.ProductionKeyWordSet(self)", , 400), TextField()>
      Public Property KeyWord As String = ""

      <Display(Name:="Production Type ID", Description:="")>
      Public Property ProductionTypeID As Integer?



      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?, ProductionRefNo As String, ProductionID As Integer?, ProductionTypeID As Integer?, KeyWord As String)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.ProductionRefNo = ProductionRefNo
        Me.ProductionID = ProductionID
        Me.ProductionTypeID = ProductionTypeID
        Me.KeyWord = KeyWord
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROProductionFindList() As ROProductionFindList

      Return New ROProductionFindList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROProductionFindList() As ROProductionFindList

      Return DataPortal.Fetch(Of ROProductionFindList)(New Criteria())

    End Function

    Public Shared Function GetROProductionFindList(SystemID As Integer?, ProductionAreaID As Integer?, ProductionRefNo As String, ProductionID As Integer?, ProductionTypeID As Integer?, KeyWord As String) As ROProductionFindList

      Return DataPortal.Fetch(Of ROProductionFindList)(New Criteria(SystemID, ProductionAreaID, ProductionRefNo, ProductionID, ProductionTypeID, KeyWord))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionFind.GetROProductionFind(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionFindList"
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@ProductionRefNo", Singular.Strings.MakeEmptyDBNull(crit.ProductionRefNo))
            cm.Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@ProductionTypeID", Singular.Misc.NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@KeyWord", Singular.Strings.MakeEmptyDBNull(crit.KeyWord))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace

