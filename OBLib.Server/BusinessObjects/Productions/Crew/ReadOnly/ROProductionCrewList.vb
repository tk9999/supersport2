﻿' Generated 17 Feb 2015 13:15 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.Misc
Imports Singular

Namespace Productions.Crew.ReadOnly

  <Serializable()> _
  Public Class ROProductionCrewList
    Inherits SingularReadOnlyListBase(Of ROProductionCrewList, ROProductionCrew)

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROProductionCrew

      For Each child As ROProductionCrew In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area ID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production System Area ID", Description:="")>
      Public Property ProductionSystemAreaID() As Integer?
        Get
          Return ReadProperty(ProductionSystemAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionSystemAreaIDProperty, Value)
        End Set
      End Property

      Public Sub New(ProductionSystemAreaID As Integer?)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionCrewList() As ROProductionCrewList

      Return New ROProductionCrewList()

    End Function

    Public Shared Sub BeginGetROProductionCrewList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionCrewList)))

      Dim dp As New DataPortal(Of ROProductionCrewList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionCrewList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionCrewList)))

      Dim dp As New DataPortal(Of ROProductionCrewList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionCrewList() As ROProductionCrewList

      Return DataPortal.Fetch(Of ROProductionCrewList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionCrew.GetROProductionCrew(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionCrewList"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace