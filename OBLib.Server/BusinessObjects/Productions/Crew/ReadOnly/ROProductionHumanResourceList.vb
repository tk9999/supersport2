﻿' Generated 17 Feb 2015 18:43 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Productions.Crew.ReadOnly

  <Serializable()>
  Public Class ROProductionHumanResourceList
    Inherits SingularReadOnlyListBase(Of ROProductionHumanResourceList, ROProductionHumanResource)

#Region " Business Methods "

    Public Function GetItem(ProductionHumanResourceID As Integer) As ROProductionHumanResource

      For Each child As ROProductionHumanResource In Me
        If child.ProductionHumanResourceID = ProductionHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Human Resources"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area ID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production System Area ID", Description:="")>
      Public Property ProductionSystemAreaID() As Integer?
        Get
          Return ReadProperty(ProductionSystemAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionSystemAreaIDProperty, Value)
        End Set
      End Property

      Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "HumanResourceID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="HumanResourceID", Description:="")>
      Public Property HumanResourceID() As Integer?
        Get
          Return ReadProperty(HumanResourceIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(HumanResourceIDProperty, Value)
        End Set
      End Property

      <PrimarySearchField>
      Public Property DisciplinePosition As String

      Public Sub New(ProductionSystemAreaID As Integer?)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionHumanResourceList() As ROProductionHumanResourceList

      Return New ROProductionHumanResourceList()

    End Function

    Public Shared Sub BeginGetROProductionHumanResourceList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionHumanResourceList)))

      Dim dp As New DataPortal(Of ROProductionHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionHumanResourceList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionHumanResourceList)))

      Dim dp As New DataPortal(Of ROProductionHumanResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionHumanResourceList() As ROProductionHumanResourceList

      Return DataPortal.Fetch(Of ROProductionHumanResourceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionHumanResource.GetROProductionHumanResource(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionHumanResourceList"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@DisciplinePosition", Strings.MakeEmptyDBNull(crit.DisciplinePosition))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace