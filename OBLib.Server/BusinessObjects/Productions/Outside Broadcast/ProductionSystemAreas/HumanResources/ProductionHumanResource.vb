﻿' Generated 30 Jun 2014 10:57 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Productions.Vehicles.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Productions.Crew
Imports OBLib.Productions.Vehicles
Imports OBLib.Productions.Schedules
Imports OBLib.Maintenance.Vehicles.ReadOnly
Imports OBLib.Productions.Crew.ReadOnly
Imports OBLib.Productions.OB

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Areas

  <Serializable()> _
  Public Class ProductionHumanResource
    Inherits SingularBusinessBase(Of ProductionHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property ProductionHumanResourceID() As Integer
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The parent Production for this Human resource")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.DisciplineID, Nothing) _
                                                                      .AddSetExpression(OBLib.JSCode.ProductionHumanResourceJS.DisciplineIDSet, False)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="The discipline required"),
    Required(ErrorMessage:="Discipline required"),
    DropDownWeb(GetType(RODisciplineList), Source:=DropDownWeb.SourceType.ViewModel)>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.PositionID, Nothing) _
                                                                    .AddSetExpression(OBLib.JSCode.ProductionHumanResourceJS.PositionIDSet, False)
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="The camera position that this human resource is allocated to"),
    DropDownWeb(GetType(ROPositionList), ThisFilterMember:="DisciplineID", Source:=DropDownWeb.SourceType.ViewModel)>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionTypeID, "Position Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:=""),
    DropDownWeb(GetType(ROPositionTypeList), Source:=DropDownWeb.SourceType.ViewModel, ValueMember:="PositionTypeID", DisplayMember:="PositionType", FilterMethodName:="FilterPositionTypeList")>
    Public Property PositionTypeID() As Integer?
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionTypeIDProperty, Value)
      End Set
    End Property
    'DropDownWeb(GetType(ROPositionTypeList), ThisFilterMember:="DisciplineID", Source:=DropDownWeb.SourceType.ViewModel)>
    'DropDownWeb(GetType(ROPositionTypeList), Source:=DropDownWeb.SourceType.ViewModel, ValueMember:="PositionTypeID", DisplayMember:="PositionType", FilterMethodName:="FilterPositionTypeList")>

    Public Shared FilteredProductionTypeIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FilteredProductionTypeInd, "Qualified for production type?", True)
    ''' <summary>
    ''' Gets and sets the Filtered Production Type value
    ''' </summary>
    <Display(Name:="Filter by production type?", Description:="Only show resources qualified for this production type")>
    Public Property FilteredProductionTypeInd() As Boolean
      Get
        Return GetProperty(FilteredProductionTypeIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(FilteredProductionTypeIndProperty, Value)
      End Set
    End Property

    Public Shared FilteredPositionIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FilteredPositionInd, "Qualified for position?", True)
    ''' <summary>
    ''' Gets and sets the Filtered Position value
    ''' </summary>
    <Display(Name:="Filter by position?", Description:="Only show resources qualified for this position")>
    Public Property FilteredPositionInd() As Boolean
      Get
        Return GetProperty(FilteredPositionIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(FilteredPositionIndProperty, Value)
      End Set
    End Property

    Public Shared IncludedOffIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IncludedOffInd, "Resources on leave?", False)
    ''' <summary>
    ''' Gets and sets the Included Off value
    ''' </summary>
    <Display(Name:="Include resources on leave?", Description:="Include resources whom are on leave")>
    Public Property IncludedOffInd() As Boolean
      Get
        Return GetProperty(IncludedOffIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IncludedOffIndProperty, Value)
      End Set
    End Property

    Public Shared PreferredHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PreferredHumanResourceID, "Preferred Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Preferred Human Resource value
    ''' </summary>
    <Display(Name:="Preferred Human Resource", Description:="This is the preferred resource that we want to allocate to this production/discipline/position. The HumanResourceID will default to this HR if they are available.")>
    Public Property PreferredHumanResourceID() As Integer?
      Get
        Return GetProperty(PreferredHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PreferredHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared SuppliedHumanResourceIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SuppliedHumanResourceInd, "Supplied/External Resource?", False)
    ''' <summary>
    ''' Gets and sets the Supplied Human Resource value
    ''' </summary>
    <Display(Name:="Supplied/External Resource?", Description:="Tick indicates that this human resource is supplied")>
    Public Property SuppliedHumanResourceInd() As Boolean
      Get
        Return GetProperty(SuppliedHumanResourceIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SuppliedHumanResourceIndProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource to be used")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared SelectionReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectionReason, "Selection Reason", "")
    ''' <summary>
    ''' Gets and sets the Selection Reason value
    ''' </summary>
    <Display(Name:="Selection Reason", Description:="This field needs to be filled in if the resource selected is not available through the default parameters"),
    StringLength(200, ErrorMessage:="Selection Reason cannot be more than 200 characters")>
    Public Property SelectionReason() As String
      Get
        Return GetProperty(SelectionReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SelectionReasonProperty, Value)
      End Set
    End Property

    Public Shared CancelledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDate, "Cancelled Date")
    ''' <summary>
    ''' Gets and sets the Cancelled Date value
    ''' </summary>
    <Display(Name:="Cancelled Date", Description:="The date (and time) that the human resource was cancelled for a production. This is used to calculate the cancellation fee based on the day difference between the cancellation date and the transmission start date")>
    Public Property CancelledDate As DateTime?
      Get
        Return GetProperty(CancelledDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancelledDateProperty, Value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.CancelledReason, "") _
                                                                       .AddSetExpression(OBLib.JSCode.ProductionHumanResourceJS.HandleCancelledReason, False)
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="The reason for the cancellation"),
    StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
      End Set
    End Property

    Public Shared CrewLeaderIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CrewLeaderInd, "Crew Leader", False)
    ''' <summary>
    ''' Gets and sets the Crew Leader value
    ''' </summary>
    <Display(Name:="Crew Leader?", Description:="Tick indicates that this human resource is the crew leader for this discipline on this production")>
    Public Property CrewLeaderInd() As Boolean
      Get
        Return GetProperty(CrewLeaderIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CrewLeaderIndProperty, Value)
      End Set
    End Property

    Public Shared TrainingIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TrainingInd, "Training", False)
    ''' <summary>
    ''' Gets and sets the Training value
    ''' </summary>
    <Display(Name:="Training", Description:="Tick indicates that this human resource is providing training for the discipline they have been loaded for")>
    Public Property TrainingInd() As Boolean
      Get
        Return GetProperty(TrainingIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(TrainingIndProperty, Value)
      End Set
    End Property

    Public Shared TBCIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TBCInd, "TBC", False)
    ''' <summary>
    ''' Gets and sets the TBC value
    ''' </summary>
    <Display(Name:="TBC", Description:="")>
    Public Property TBCInd() As Boolean
      Get
        Return GetProperty(TBCIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(TBCIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="The vehicle that the human resource is driving. Can only have a value if the discipline is Driver"),
    DropDownWeb(GetType(ROVehicleOldList), ValueMember:="VehicleID", DisplayMember:="VehicleName", FilterMethodName:="GetVehicleDropDown")>
    Public Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required(ErrorMessage:="System required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:=""),
    StringLength(1000, ErrorMessage:="Comments cannot be more than 1000 characters")>
    Public Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

    Public Shared ReliefCrewIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ReliefCrewInd, "Relief Crew", False)
    ''' <summary>
    ''' Gets and sets the Relief Crew value
    ''' </summary>
    <Display(Name:="Relief Crew", Description:="Indicator for whether or not the Human Resource is part of the Relief Crew")>
    Public Property ReliefCrewInd() As Boolean
      Get
        Return GetProperty(ReliefCrewIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ReliefCrewIndProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared Property ProductionHumanResourceClashesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionHumanResourceClashes, "Production System Area Clashes", "")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Clashes", Description:=""),
    ClientOnly()>
    Public Property ProductionHumanResourceClashes() As String
      Get
        Return GetProperty(ProductionHumanResourceClashesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionHumanResourceClashesProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionProperty, Value)
      End Set
    End Property

    Public Shared PositionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionType, "Position Type")
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
    Public Property PositionType() As String
      Get
        Return GetProperty(PositionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionTypeProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.HumanResource) _
                                                                     .AddSetExpression(OBLib.JSCode.ProductionHumanResourceJS.HumanResourceSet, False)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared PrefHumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PrefHumanResource, "Pref Human Resource")
    ''' <summary>
    ''' Gets and sets the Pref Human Resource value
    ''' </summary>
    <Display(Name:="Pref Human Resource", Description:=""),
    StringLength(50, ErrorMessage:="Pref Human Resource cannot be more than 50 characters")>
    Public Property PrefHumanResource() As String
      Get
        Return GetProperty(PrefHumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PrefHumanResourceProperty, Value)
      End Set
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name")
    ''' <summary>
    ''' Gets and sets the Vehicle Name value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="")>
    Public Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VehicleNameProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, OBLib.JSCode.ProductionHumanResourceJS.PHRToString)

    Public ReadOnly Property CanEditPHR As Boolean
      Get
        If GetParent() IsNot Nothing Then
          Select Case GetParent.SystemID
            Case CommonData.Enums.System.ProductionServices,
                 CommonData.Enums.System.ProductionContent
              Select Case GetParent.ProductionAreaID
                Case CommonData.Enums.ProductionArea.OB
                  Return CanEdit()
              End Select
          End Select
        End If
        Return True
      End Get
    End Property

    Private Function CanEdit() As Boolean
      If Not Singular.Security.HasAccess("Productions", "Edit Human Resources") Then
        CanEditPHRReason = "You do not have the security permissions to make changes to the human resources"
        Return False
      End If
      If GetParent.GetParent.PlanningFinalised Then
        If Not Singular.Security.HasAccess("Productions", "Can Edit Human Resources after finalised") Then
          CanEditPHRReason = "You are not authorized to make changes to the human resources after they have been finalized"
          Return False
        End If
      ElseIf GetParent.GetParent.CrewFinalised Then
        If Not Singular.Security.HasAccess("Productions", "Can Edit Human Resources before finalised") Then
          CanEditPHRReason = "You are not authorized to make changes to the human resources before they have been finalized"
          Return False
        End If
      End If
      Return True
    End Function

    <ClientOnly()>
    Public Property CanEditPHRReason As String

    Public Shared ClashProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Clash, "Clash")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Clash", Description:="")>
    Public Property Clash() As String
      Get
        Return GetProperty(ClashProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ClashProperty, Value)
      End Set
    End Property

    Public Shared VisibleProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Visible, "Visible", True)
    ''' <summary>
    ''' Gets and sets the Relief Crew value
    ''' </summary>
    <Display(Name:="Visible", Description:=""),
    ClientOnly()>
    Public Property Visible() As Boolean
      Get
        Return GetProperty(VisibleProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(VisibleProperty, Value)
      End Set
    End Property

    <Browsable(False)> _
    Public ReadOnly Property PriorityServer() As Integer
      Get
        If PositionID Is Nothing Then
          Return 0
        Else
          Return OBLib.CommonData.Lists.ROPositionList.GetItem(PositionID).Priority
        End If
      End Get
    End Property

    Public Shared OrderNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNo, "Order No", 0)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Order No", Description:="")>
    Public Property OrderNo() As Integer
      Get
        Return GetProperty(OrderNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OrderNoProperty, Value)
      End Set
    End Property

    Public Shared PriorityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Priority, "Priority", 0)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Priority", Description:="")>
    Public Property Priority() As Integer
      Get
        Return GetProperty(PriorityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PriorityProperty, Value)
      End Set
    End Property

    <ClientOnly()>
    Public Property FetchingQualifiedHR() As Boolean = False

    <ClientOnly()>
    Public Property DetailVisible() As Boolean = False

    Public Shared LoadedHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LoadedHumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource to be used")>
    Public Property LoadedHumanResourceID() As Integer?
      Get
        Return GetProperty(LoadedHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LoadedHumanResourceIDProperty, Value)
      End Set
    End Property

    Friend Function IsDriver() As Boolean

      Return CompareSafe(DisciplineID, CType(CommonData.Enums.Discipline.Driver, Integer))

    End Function

    Public Shared LoadedDisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.LoadedDisciplineID, Nothing)
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    ''' 
    <Browsable(False)>
    Public Property LoadedDisciplineID() As Integer?
      Get
        Return GetProperty(LoadedDisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LoadedDisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared LoadedPositionIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.LoadedPositionID, Nothing)
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    ''' 
    <Browsable(False)>
    Public Property LoadedPositionID() As Integer?
      Get
        Return GetProperty(LoadedPositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LoadedPositionIDProperty, Value)
      End Set
    End Property

    Public Shared LoadedPreferredHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.LoadedPreferredHumanResourceID, Nothing)
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    ''' 
    <Browsable(False)>
    Public Property LoadedPreferredHumanResourceID() As Integer?
      Get
        Return GetProperty(LoadedPreferredHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LoadedPreferredHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared LoadedCancelledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LoadedCancelledDate, "Cancelled Date")
    ''' <summary>
    ''' Gets and sets the Cancelled Date value
    ''' </summary>
    <Browsable(False)>
    Public Property LoadedCancelledDate As DateTime?
      Get
        Return GetProperty(LoadedCancelledDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LoadedCancelledDateProperty, Value)
      End Set
    End Property

    Public Shared LoadedTBCIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.LoadedTBCInd, "TBC", False)
    ''' <summary>
    ''' Gets and sets the TBC value
    ''' </summary>
    <Browsable(False)>
    Public Property LoadedTBCInd() As Boolean
      Get
        Return GetProperty(LoadedTBCIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(LoadedTBCIndProperty, Value)
      End Set
    End Property

    Public Shared LoadedCancelledReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.LoadedCancelledReason, "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Browsable(False)>
    Public Property LoadedCancelledReason() As String
      Get
        Return GetProperty(LoadedCancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LoadedCancelledReasonProperty, Value)
      End Set
    End Property

    Public Shared CoreCrewIndServerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CoreCrewIndServer, "Core Crew", False)
    Public ReadOnly Property CoreCrewIndServer() As Boolean
      Get
        Return GetProperty(CoreCrewIndServerProperty)
      End Get
    End Property

    Public Shared CoreDisciplineIndServerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CoreDisciplineIndServer, "Core Discipline", False)
    Public ReadOnly Property CoreDisciplineIndServer() As Boolean
      Get
        Return GetProperty(CoreDisciplineIndServerProperty)
      End Get
    End Property

    Public ReadOnly Property CoreCrewInd() As Boolean
      Get
        Dim ClientCoreCrew As Boolean = False
        If DisciplineID IsNot Nothing Then
          If {1, 2, 5, 6, 7, 11}.Contains(DisciplineID) Then
            ClientCoreCrew = True
          End If
        End If
        Return Me.CoreCrewIndServer Or ClientCoreCrew
      End Get
    End Property

    Public ReadOnly Property CoreDisciplineInd() As Boolean
      Get
        Dim ClientCoreDiscipline As Boolean = False
        If DisciplineID IsNot Nothing Then
          If {1, 2, 5, 6, 7, 11}.Contains(DisciplineID) Then
            ClientCoreDiscipline = True
          End If
        End If
        Return Me.CoreDisciplineIndServer Or ClientCoreDiscipline
      End Get
    End Property

    Public ReadOnly Property CoreCrewDisciplineInd() As Boolean
      Get
        Return CoreCrewInd Or CoreDisciplineInd
      End Get
    End Property

    <Browsable(False)>
    Public ReadOnly Property ExistingProductionHR As ProductionHROB
      Get
        Dim phrList As List(Of ProductionHROB) = Me.GetParent.ProductionHROBList.Where(Function(d) CompareSafe(d.HumanResourceID, Me.HumanResourceID)).ToList
        If phrList.Count = 1 Then
          Return phrList(0)
        ElseIf phrList.Count > 1 Then
          Throw New Exception("PhrList count > 1")
        Else
          Return Nothing
        End If
      End Get
    End Property

#End Region

#Region " Child Lists "

    <ClientOnlyNoData(DataOption:=ClientOnly.DataOptionType.DontSendData)>
    Public Property SearchResultsFilter As String = ""

    Private mROSkilledHRList As ROSkilledHRList
    <ClientOnlyNoData()>
    Public ReadOnly Property ROSkilledHRList As ROSkilledHRList
      Get
        Return mROSkilledHRList
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionSystemArea

      Return CType(CType(Me.Parent, ProductionHumanResourceList).Parent, ProductionSystemArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.IsNew AndAlso HumanResourceID Is Nothing AndAlso DisciplineID Is Nothing AndAlso PositionID Is Nothing Then
        Return "New Production Human Resource"
      ElseIf HumanResourceID IsNot Nothing Then
        Return CommonData.Lists.ROHumanResourceList.GetItem(HumanResourceID).PreferredFirstSurname
      ElseIf DisciplineID IsNot Nothing AndAlso PositionID IsNot Nothing Then
        Return CommonData.Lists.RODisciplineList.GetItem(DisciplineID).Discipline & ": " & CommonData.Lists.ROPositionList.GetItem(PositionID).Position
      ElseIf PositionID IsNot Nothing Then
        Return CommonData.Lists.ROPositionList.GetItem(PositionID).Position
      ElseIf DisciplineID IsNot Nothing Then
        Return CommonData.Lists.RODisciplineList.GetItem(DisciplineID).Discipline
      Else
        Return "Production Human Resource"
      End If

    End Function

    Public Function GetParentSystemAreaID() As Integer?

      If Me.GetParent Is Nothing Then
        Return Me.ProductionSystemAreaID
      Else
        Return Me.GetParent.ProductionSystemAreaID
      End If

    End Function

    Public Sub RemoveScheduleItemsForPHR()

      If Me.HumanResourceID IsNot Nothing Then
        Dim HRSchedule As HRProductionSchedule = Me.GetParent.HRProductionScheduleList.GetItem(Me.HumanResourceID)
        If HRSchedule IsNot Nothing Then
          Dim ItemsToRemove As New List(Of HRProductionScheduleDetail)
          HRSchedule.HRProductionScheduleDetailList.ToList.ForEach(Sub(ScheduledItem)
                                                                     If ScheduledItem.ProductionHumanResource Is Me Then
                                                                       ItemsToRemove.Add(ScheduledItem)
                                                                     End If
                                                                   End Sub)
          For Each item In ItemsToRemove
            HRSchedule.HRProductionScheduleDetailList.Remove(item)
          Next
        End If
      End If

    End Sub

    Public Function GetChange(Action As String) As OBLib.Helpers.RecordChange

      Dim changes As New List(Of OBLib.Helpers.RecordChangeDetail)

      If Not CompareSafe(LoadedDisciplineID, DisciplineID) Then
        Dim ChangeDetail As New OBLib.Helpers.RecordChangeDetail("Discipline", "", "")
        If LoadedDisciplineID IsNot Nothing Then
          ChangeDetail.PreviousValue = OBLib.CommonData.Lists.RODisciplineList.GetItem(LoadedDisciplineID).Discipline
        Else
          ChangeDetail.PreviousValue = ""
        End If
        If DisciplineID IsNot Nothing Then
          ChangeDetail.NewValue = OBLib.CommonData.Lists.RODisciplineList.GetItem(DisciplineID).Discipline
        Else
          ChangeDetail.NewValue = ""
        End If
        changes.Add(ChangeDetail)
      End If

      If Not CompareSafe(LoadedPositionID, PositionID) Then
        Dim ChangeDetail As New OBLib.Helpers.RecordChangeDetail("Position", "", "")
        If LoadedPositionID IsNot Nothing Then
          ChangeDetail.PreviousValue = OBLib.CommonData.Lists.ROPositionList.GetItem(LoadedPositionID).Position
        Else
          ChangeDetail.PreviousValue = ""
        End If
        If PositionID IsNot Nothing Then
          ChangeDetail.NewValue = OBLib.CommonData.Lists.ROPositionList.GetItem(PositionID).Position
        Else
          ChangeDetail.NewValue = ""
        End If
        changes.Add(ChangeDetail)
      End If

      'If mLoadedPositionTypeID ,PositionTypeID Then

      'End If

      If Not CompareSafe(LoadedPreferredHumanResourceID, PreferredHumanResourceID) Then
        Dim ChangeDetail As New OBLib.Helpers.RecordChangeDetail("Preferred Human Resource", "", "")
        If LoadedPreferredHumanResourceID IsNot Nothing Then
          ChangeDetail.NewValue = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(LoadedPreferredHumanResourceID).PreferredFirstSurname
        Else
          ChangeDetail.NewValue = ""
        End If
        If PreferredHumanResourceID IsNot Nothing Then
          ChangeDetail.NewValue = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(PreferredHumanResourceID).PreferredFirstSurname
        Else
          ChangeDetail.NewValue = ""
        End If
        changes.Add(ChangeDetail)
      End If

      'If mLoadedSuppliedHumanResourceInd ,SuppliedHumanResourceInd Then

      'End If

      If Not CompareSafe(LoadedHumanResourceID, HumanResourceID) Then
        Dim ChangeDetail As New OBLib.Helpers.RecordChangeDetail("Human Resource", "", "")
        If LoadedHumanResourceID IsNot Nothing Then
          ChangeDetail.PreviousValue = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(LoadedHumanResourceID).PreferredFirstSurname
        Else
          ChangeDetail.PreviousValue = ""
        End If
        If HumanResourceID IsNot Nothing Then
          ChangeDetail.NewValue = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(HumanResourceID).PreferredFirstSurname
        Else
          ChangeDetail.NewValue = ""
        End If
        changes.Add(ChangeDetail)
      End If

      'If mLoadedSelectionReason ,SelectionReason Then

      'End If

      If Not CompareSafe(LoadedCancelledDate, CancelledDate) Then
        Dim ChangeDetail As New OBLib.Helpers.RecordChangeDetail("Cancelled Date", "", "")
        If LoadedCancelledDate Is Nothing Then
          ChangeDetail.PreviousValue = ""
        Else
          ChangeDetail.PreviousValue = LoadedCancelledDate.Value.ToString("dd-MMM-yy HH:mm")
        End If
        If CancelledDate Is Nothing Then
          ChangeDetail.NewValue = ""
        Else
          ChangeDetail.NewValue = CancelledDate.ToString("dd-MMM-yy HH:mm")
        End If
        changes.Add(ChangeDetail)
      End If

      If Not CompareSafe(LoadedCancelledReason, CancelledReason) Then
        Dim ChangeDetail As New OBLib.Helpers.RecordChangeDetail("Cancelled Reason", "", "")
        ChangeDetail.PreviousValue = LoadedCancelledReason
        ChangeDetail.NewValue = CancelledReason
        changes.Add(ChangeDetail)
      End If

      'If mLoadedCrewLeaderInd ,CrewLeaderInd Then

      'End If

      'If mLoadedTrainingInd ,TrainingInd Then

      'End If

      If Not CompareSafe(LoadedTBCInd, TBCInd) Then
        Dim ChangeDetail As New OBLib.Helpers.RecordChangeDetail("TBC", "", "")
        If LoadedTBCInd Then
          ChangeDetail.PreviousValue = "Yes"
        Else
          ChangeDetail.PreviousValue = "No"
        End If
        If TBCInd Then
          ChangeDetail.NewValue = "Yes"
        Else
          ChangeDetail.NewValue = "No"
        End If
        changes.Add(ChangeDetail)
      End If

      'If mLoadedIncludedOffInd ,IncludedOffInd Then

      'End If

      'If mLoadedFilteredProductionTypeInd ,FilteredProductionTypeInd Then

      'End If

      'If mLoadedFilteredPositionInd ,FilteredPositionInd Then

      'End If

      'If mLoadedVehicleID ,VehicleID Then

      'End If

      'If mLoadedComments ,Comments Then

      'End If

      'If mOriginalReliefCrewInd ,ReliefCrewInd Then

      'End If

      Dim RecordChange As OBLib.Helpers.RecordChange
      If changes.Count > 0 Then
        Dim MainDescription As String = Me.ToString
        Dim SubDescription As String = ""
        Dim ChangedBy As String = OBLib.Security.Settings.CurrentUser.LoginName
        RecordChange = New OBLib.Helpers.RecordChange(MainDescription, SubDescription, ChangedBy, Action, changes)
      End If

      Return RecordChange

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(DisciplineIDProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionHumanResourceJS.DisciplineIDRule
      End With

      With AddWebRule(PositionIDProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionHumanResourceJS.PositionRequiredRule
      End With

      With AddWebRule(HumanResourceIDProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionHumanResourceJS.HumanResourceRequiredRule
      End With

      With AddWebRule(PositionTypeIDProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionHumanResourceJS.PositionTypeIDRule
      End With

      With AddWebRule(VehicleIDProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionHumanResourceJS.VehicleIDRule
      End With

      'With AddWebRule(ClashProperty,
      '           Function(d) d.Clash <> "",
      '           Function(d) d.Clash)
      '  .AddTriggerProperty(DisciplineIDProperty)
      'End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionHumanResource() method.

    End Sub

    Public Shared Function NewProductionHumanResource() As ProductionHumanResource

      Return DataPortal.CreateChild(Of ProductionHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionHumanResource(dr As SafeDataReader) As ProductionHumanResource

      Dim p As New ProductionHumanResource()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(FilteredProductionTypeIndProperty, .GetBoolean(5))
          LoadProperty(FilteredPositionIndProperty, .GetBoolean(6))
          LoadProperty(IncludedOffIndProperty, .GetBoolean(7))
          LoadProperty(PreferredHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(SuppliedHumanResourceIndProperty, .GetBoolean(9))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(SelectionReasonProperty, .GetString(11))
          LoadProperty(CancelledDateProperty, .GetValue(12))
          LoadProperty(CancelledReasonProperty, .GetString(13))
          LoadProperty(CrewLeaderIndProperty, .GetBoolean(14))
          LoadProperty(TrainingIndProperty, .GetBoolean(15))
          LoadProperty(TBCIndProperty, .GetBoolean(16))
          LoadProperty(CreatedByProperty, .GetInt32(17))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(18))
          LoadProperty(ModifiedByProperty, .GetInt32(19))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(20))
          LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
          LoadProperty(CommentsProperty, .GetString(23))
          LoadProperty(ReliefCrewIndProperty, .GetBoolean(24))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(25)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(26)))
          LoadProperty(ProductionHumanResourceClashesProperty, .GetString(27))
          LoadProperty(DisciplineProperty, .GetString(28))
          LoadProperty(PositionProperty, .GetString(29))
          LoadProperty(PositionTypeProperty, .GetString(30))
          LoadProperty(HumanResourceProperty, .GetString(31))
          LoadProperty(PrefHumanResourceProperty, .GetString(32))
          LoadProperty(VehicleNameProperty, .GetString(33))
          LoadProperty(RoomProperty, .GetString(34))
          LoadProperty(OrderNoProperty, .GetInt32(35))
          LoadProperty(PriorityProperty, .GetInt32(36))
          LoadProperty(CoreCrewIndServerProperty, .GetBoolean(37))
          LoadProperty(CoreDisciplineIndServerProperty, .GetBoolean(38))
          LoadProperty(LoadedDisciplineIDProperty, DisciplineID)
          LoadProperty(LoadedPositionIDProperty, PositionID)
          LoadProperty(LoadedHumanResourceIDProperty, HumanResourceID)
          LoadProperty(LoadedPreferredHumanResourceIDProperty, PreferredHumanResourceID)
          LoadProperty(LoadedTBCIndProperty, TBCInd)
          LoadProperty(LoadedCancelledDateProperty, CancelledDate)
          LoadProperty(LoadedCancelledReasonProperty, CancelledReason)

        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()
      Dim CanCommitInsert As Boolean = False
      ' if we're not dirty then don't update the database
      If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices AndAlso Not GetParent().GetParent().TimelineReadyInd Then
        If Singular.Security.HasAccess("Productions", "Can Finalise Timeline") Then
          CanCommitInsert = True
        End If
      Else
        CanCommitInsert = True
      End If
      If CanCommitInsert Then
        Using cm As SqlCommand = New SqlCommand
          cm.CommandText = "InsProcsWeb.insProductionHumanResource"
          DoInsertUpdateChild(cm)
        End Using
      End If
    End Sub

    Friend Sub Update()
      ' if we're not dirty then don't update the database
      Dim CanCommitUpdate As Boolean = False
      If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices AndAlso Not GetParent().GetParent().TimelineReadyInd Then
        If Singular.Security.HasAccess("Productions", "Can Finalise Timeline") Then
          CanCommitUpdate = True
        End If
      Else
        CanCommitUpdate = True
      End If
      If CanCommitUpdate Then
        Using cm As SqlCommand = New SqlCommand
          cm.CommandText = "UpdProcsWeb.updProductionHumanResource"
          DoInsertUpdateChild(cm)
        End Using
      End If
    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        If CompareSafe(Me.GetParent.ProductionAreaID, CType(CommonData.Enums.ProductionArea.OB, Integer)) _
            AndAlso CompareSafe(Me.SystemID, CType(CommonData.Enums.System.ProductionServices, Integer)) _
            AndAlso Me.GetParent.IsOnOrAfterCrewFinalised Then
          Dim Change As OBLib.Helpers.RecordChange = Nothing
          If Me.IsNew Then
            Change = Me.GetChange("Added")
          Else
            Change = Me.GetChange("Updated")
          End If
          If Change IsNot Nothing Then
            Me.GetParent.RecordChangeList.Add(Change)
          End If
        End If

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionHumanResourceID As SqlParameter = .Parameters.Add("@ProductionHumanResourceID", SqlDbType.Int)
          paramProductionHumanResourceID.Value = GetProperty(ProductionHumanResourceIDProperty)
          If Me.IsNew Then
            paramProductionHumanResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionID", Me.GetParent.ProductionID) 'GetProperty(ProductionIDProperty)
          .Parameters.AddWithValue("@DisciplineID", NothingDBNull(GetProperty(DisciplineIDProperty)))
          .Parameters.AddWithValue("@PositionID", NothingDBNull(GetProperty(PositionIDProperty)))
          .Parameters.AddWithValue("@PositionTypeID", NothingDBNull(GetProperty(PositionTypeIDProperty)))
          .Parameters.AddWithValue("@FilteredProductionTypeInd", GetProperty(FilteredProductionTypeIndProperty))
          .Parameters.AddWithValue("@FilteredPositionInd", GetProperty(FilteredPositionIndProperty))
          .Parameters.AddWithValue("@IncludedOffInd", GetProperty(IncludedOffIndProperty))
          .Parameters.AddWithValue("@PreferredHumanResourceID", NothingDBNull(GetProperty(PreferredHumanResourceIDProperty)))
          .Parameters.AddWithValue("@SuppliedHumanResourceInd", GetProperty(SuppliedHumanResourceIndProperty))
          .Parameters.AddWithValue("@HumanResourceID", NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@SelectionReason", GetProperty(SelectionReasonProperty))
          .Parameters.AddWithValue("@CancelledDate", (New SmartDate(GetProperty(CancelledDateProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@CrewLeaderInd", GetProperty(CrewLeaderIndProperty))
          .Parameters.AddWithValue("@TrainingInd", GetProperty(TrainingIndProperty))
          .Parameters.AddWithValue("@TBCInd", GetProperty(TBCIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@VehicleID", NothingDBNull(ZeroNothing(GetProperty(VehicleIDProperty))))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
          .Parameters.AddWithValue("@ReliefCrewInd", GetProperty(ReliefCrewIndProperty))
          .Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(GetParentSystemAreaID))
          .Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
          .Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionHumanResourceIDProperty, paramProductionHumanResourceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If
    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub
      Dim CanCommitDelete As Boolean = False

      If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices AndAlso Not GetParent().GetParent().TimelineReadyInd Then
        If Singular.Security.HasAccess("Productions", "Can Finalise Timeline") Then
          CanCommitDelete = True
        End If
      Else
        CanCommitDelete = True
      End If

      If CanCommitDelete Then
        'For the Change HR after Crew Finalised Email
        If CompareSafe(Me.GetParent.SystemID, CommonData.Enums.System.ProductionServices) AndAlso CompareSafe(Me.GetParent.ProductionAreaID, CType(OBLib.CommonData.Enums.ProductionArea.OB, Integer)) AndAlso Me.GetParent.IsOnOrAfterCrewFinalised Then
          Dim Change As OBLib.Helpers.RecordChange = New OBLib.Helpers.RecordChange(Me.ToString, "", OBLib.Security.Settings.CurrentUser.LoginName, "Deleted", New List(Of OBLib.Helpers.RecordChangeDetail))
          If Change IsNot Nothing Then
            Me.GetParent.RecordChangeList.Add(Change)
          End If
        End If
        Using cm As SqlCommand = New SqlCommand
          cm.CommandText = "DelProcsWeb.delProductionHumanResource"
          cm.CommandType = CommandType.StoredProcedure
          cm.Parameters.AddWithValue("@ProductionHumanResourceID", GetProperty(ProductionHumanResourceIDProperty))
          cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(GetParentSystemAreaID))
          cm.Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUserID)
          DoDeleteChild(cm)
        End Using
      End If

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub
      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace