﻿function PositionRequiredRule(Value, Rule, Args) {
  if (ProductionHelper.ProductionHumanResources.Rules.PositionRequired(CtlError.Object)) {
    CtlError.AddError("Position required");
  }
};

function HumanResourceRequiredRule(Value, Rule, Args) {
  ProductionHelper.ProductionHumanResources.Rules.HumanResourceRequired(CtlError)
};

function PHRToString(Value, Rule, Args) {
  var CurrentProductionHumanResource = self;
  var RODiscipline = ClientData.RODisciplineList.Find("DisciplineID", CurrentProductionHumanResource.DisciplineID());
  var ROPosition = ClientData.ROPositionList.Find("PositionID", CurrentProductionHumanResource.PositionID());
  var String = "";
  if (CurrentProductionHumanResource.HumanResource()) {
    if (CurrentProductionHumanResource.HumanResource().length > 0) {
      String += CurrentProductionHumanResource.HumanResource();
    }
  }
  if (RODiscipline) {
    if (String.length > 0) { String += ' - ' }
    String += RODiscipline.Discipline;
  }
  if (ROPosition) {
    if (String.length > 0) { String += ' ' }
    String += ' (' + ROPosition.Position + ')';
  }
  return String;
};

function VehicleIDRule(Value, Rule, Args) {
  var CurrentProductionHumanResource = CtlError.Object;
  if (CurrentProductionHumanResource.DisciplineID()) {
    if (CurrentProductionHumanResource.DisciplineID() != 5 && CurrentProductionHumanResource.VehicleID()) {
      CtlError.AddError("Vehicle can only be selected for 'Driver' discipline");
    } else if (CurrentProductionHumanResource.DisciplineID() == 5 && !CurrentProductionHumanResource.VehicleID()) {
      CtlError.AddError("Vehicle must be selected");
    }
  }
};

function DisciplineIDRule(Value, Rule, Args) {
  var CurrentProductionHumanResource = CtlError.Object;
  var JibCount = 0;
  if (CurrentProductionHumanResource.DisciplineID()) {
    if (CurrentProductionHumanResource.DisciplineID() == 9 || CurrentProductionHumanResource.DisciplineID() == 10) {
      ViewModel.ROProductionVehicleList().Iterate(function (Vehicle, Index) {
        if (Vehicle.Jib()) {
          JibCount += 1;
        }
      })
      if (JibCount == 0) {
        CtlError.AddError("This discipline can only be added if a Jib vehicle has been added");
      }
    }
  }
};

function PositionTypeIDRule(Value, Rule, Args) {
  var CurrentProductionHumanResource = CtlError.Object;
  if (CurrentProductionHumanResource.DisciplineID() && CurrentProductionHumanResource.PositionTypeID()) {
    var Found = false;
    ClientData.ROPositionTypeList.Iterate(function (ROPosition, Index) {
      if (ROPosition.DisciplineID == CurrentProductionHumanResource.DisciplineID()) {
        Found = true;
      }
    });
    var Discipline = ClientData.RODisciplineList.Find("DisciplineID", CurrentProductionHumanResource.DisciplineID());
    if (!Found && Discipline.PositionRequiredInd) {
      CtlError.AddError("Position Type is not valid for this production type and discipline");
    }
  }
};

function HandleCancelledReason(self) {
  if (self.CancelledReason() == '') {
    self.CancelledDate(null);
  } else {
    self.CancelledDate(new Date());
  };
}

function DisciplineIDSet(self) {
  if (self.DisciplineID()) {
    var RODiscipline = ClientData.RODisciplineList.Find('DisciplineID', self.DisciplineID());
    self.OrderNo(RODiscipline.OrderNo)
    self.Discipline(RODiscipline.Discipline)
  } else {
    self.OrderNo(0)
    self.Discipline("")
  };
  if (self.IsValid()) {
    ProductionHelper.ProductionHumanResources.Methods.SortList(self.GetParent())
  }
}

function PositionIDSet(self) {
  if (self.PositionID()) {
    var ROPosition = ClientData.ROPositionList.Find('PositionID', self.PositionID());
    self.Priority(ROPosition.Priority)
    self.Position(ROPosition.Position)
  } else {
    self.Priority(0)
    self.Position("")
  };
  if (self.IsValid()) {
    ProductionHelper.ProductionHumanResources.Methods.SortList(self.GetParent())
  }
}

function HumanResourceSet(self) {
  if (self.IsValid()) {
    ProductionHelper.ProductionHumanResources.Methods.SortList(self.GetParent())
  }
}