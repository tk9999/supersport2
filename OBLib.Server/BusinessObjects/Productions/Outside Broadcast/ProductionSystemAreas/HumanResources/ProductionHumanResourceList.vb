﻿' Generated 30 Jun 2014 10:57 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Vehicles.ReadOnly
Imports OBLib.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports System.Linq

Namespace Productions.Areas

  <Serializable()> _
  Public Class ProductionHumanResourceList
    Inherits SingularBusinessListBase(Of ProductionHumanResourceList, ProductionHumanResource)

#Region " Business Methods "

    Public Function GetItem(ProductionHumanResourceID As Integer) As ProductionHumanResource

      For Each child As ProductionHumanResource In Me
        If child.ProductionHumanResourceID = ProductionHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByHumanResourceID(HumanResourceID As Integer) As ProductionHumanResource

      For Each child As ProductionHumanResource In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByGuid(Guid As Guid) As ProductionHumanResource

      For Each child As ProductionHumanResource In Me
        If child.Guid = Guid Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Human Resources"

    End Function

    Public Function AddVehicleDriver(ByVal Vehicle As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleFull, VehicleHumanResource As ROVehicleHumanResource, ProductionSystemArea As ProductionSystemArea) As ProductionHumanResource

      Dim phr As ProductionHumanResource = Me.AddNew
      phr.DisciplineID = VehicleHumanResource.DisciplineID
      phr.Discipline = CommonData.Lists.RODisciplineList.GetItem(phr.DisciplineID).Discipline
      phr.HumanResourceID = VehicleHumanResource.HumanResourceID
      phr.HumanResource = CommonData.Lists.ROHumanResourceList.GetItem(phr.HumanResourceID).PreferredFirstSurname
      phr.SystemID = ProductionSystemArea.SystemID
      phr.ProductionID = ProductionSystemArea.ProductionID
      phr.VehicleID = VehicleHumanResource.VehicleID
      phr.VehicleName = Vehicle.VehicleName
      Return phr

    End Function

    Public Function AddVehicleHR(ByVal Vehicle As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleFull, VehicleHumanResource As ROVehicleHumanResource, ProductionSystemArea As ProductionSystemArea) As ProductionHumanResource

      Dim phr As ProductionHumanResource = Me.AddNew
      phr.DisciplineID = VehicleHumanResource.DisciplineID
      phr.Discipline = CommonData.Lists.RODisciplineList.GetItem(phr.DisciplineID).Discipline
      phr.HumanResourceID = VehicleHumanResource.HumanResourceID
      phr.HumanResource = CommonData.Lists.ROHumanResourceList.GetItem(phr.HumanResourceID).PreferredFirstSurname
      'phr.VehicleID = VehicleHumanResource.VehicleID
      'phr.VehicleName = CommonData.Lists.ROVehicleList.GetItem(phr.VehicleID).VehicleName
      phr.SystemID = ProductionSystemArea.SystemID
      phr.ProductionID = ProductionSystemArea.ProductionID
      If phr.DisciplineID IsNot Nothing AndAlso CompareSafe(phr.DisciplineID, CType(CommonData.Enums.Discipline.Engineer, Integer)) Then
        Dim HR As ROHumanResourceFull = CommonData.Lists.ROHumanResourceFullList.Where(Function(d) d.HumanResourceID = VehicleHumanResource.HumanResourceID).FirstOrDefault
        If HR IsNot Nothing Then
          If HR.IsSkilled(Nothing, CommonData.Enums.Discipline.Engineer, CommonData.Enums.Position.UnitSupervisor, ProductionSystemArea.GetParent.PlayStartDateTime) Then
            phr.PositionID = CType(CommonData.Enums.Position.UnitSupervisor, Integer)
          Else
            phr.PositionID = CType(CommonData.Enums.Position.Engineer, Integer)
          End If
        End If
      End If
      Return phr

    End Function

    Public Function AddEventManager() As ProductionHumanResource

      Dim phr As ProductionHumanResource = Me.AddNew
      phr.DisciplineID = CType(CommonData.Enums.Discipline.EventManager, Integer)
      phr.Discipline = CommonData.Lists.RODisciplineList.GetItem(phr.DisciplineID).Discipline
      phr.SystemID = CType(CommonData.Enums.System.ProductionServices, Integer)
      phr.ProductionID = phr.GetParent.ProductionID
      'phr.ProductionAreaID = phr.
      Return phr

    End Function

    Public Sub RemoveFromList(PHR As ProductionHumanResource)
      Dim Index As Integer = Me.IndexOf(PHR)
      Me.RemoveAt(Index)
    End Sub

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewProductionHumanResourceList() As ProductionHumanResourceList

      Return New ProductionHumanResourceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionHumanResource In DeletedList
          'For the Removed HR email and Travel
          If Child.LoadedHumanResourceID IsNot Nothing Then
            Child.GetParent().RemovedHumanResourceList.Add(Child)
          End If
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionHumanResource In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            'For the Removed HR email and Travel
            If Not CompareSafe(Child.LoadedHumanResourceID, Child.HumanResourceID) Then
              Child.GetParent().RemovedHumanResourceList.Add(Child)
            End If
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

 

  End Class

End Namespace