﻿' Generated 15 Apr 2015 19:40 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Schedules
Imports OBLib.Productions.Areas

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.OB

  <Serializable()> _
  Public Class CrewScheduleDetailOB
    Inherits OBBusinessBase(Of CrewScheduleDetailOB)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SelectedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Selected, "Selected", False)
    ''' <summary>
    ''' Gets and sets the Exclude value
    ''' </summary>
    <Display(Name:="Selected")>
    Public Property Selected() As Boolean
      Get
        Return GetProperty(SelectedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectedProperty, Value)
      End Set
    End Property

    Public Shared CrewScheduleDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewScheduleDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property CrewScheduleDetailID() As Integer
      Get
        Return GetProperty(CrewScheduleDetailIDProperty)
      End Get
    End Property

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionHumanResourceID, "Production Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Human Resource value
    ''' </summary>
    <Display(Name:="Production Human Resource", Description:="The human resource for the production that this detail relates to")>
    Public Property ProductionHumanResourceID() As Integer?
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTimelineID, "Production Timeline", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Timeline value
    ''' </summary>
    <Display(Name:="Production Timeline", Description:="The timeline item that this detail was created from. Can be NULL if the user needs to capture an unplanned resource.")>
    Public Property ProductionTimelineID() As Integer?
      Get
        Return GetProperty(ProductionTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared DetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Detail, "Detail", "")
    ''' <summary>
    ''' Gets and sets the Detail value
    ''' </summary>
    <Display(Name:="Detail", Description:="Space to capture extra details"),
    StringLength(200, ErrorMessage:="Detail cannot be more than 200 characters")>
    Public Property Detail() As String
      Get
        Return GetProperty(DetailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DetailProperty, Value)
      End Set
    End Property

    Public Shared StartTimeOffsetProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StartTimeOffset, "Start Time Offset", 0)
    ''' <summary>
    ''' Gets and sets the Start Time Offset value
    ''' </summary>
    <Display(Name:="Start Time Offset", Description:="The number of hours and minutes to offset the calculated start time by"),
    Required(ErrorMessage:="Start Time Offset required")>
    Public Property StartTimeOffset() As Integer
      Get
        Return GetProperty(StartTimeOffsetProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(StartTimeOffsetProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing) _
                                                                        .AddSetExpression("CrewScheduleDetailBaseBO.StartDateTimeSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="The start date and time for the scheduled task"),
    Required(ErrorMessage:="Start Date Time required")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing) _
                                                                        .AddSetExpression("CrewScheduleDetailBaseBO.EndDateTimeSet(self)", False)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="The end date and time for the scheduled task"),
    Required(ErrorMessage:="End Date Time required")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndTimeOffsetProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EndTimeOffset, "End Time Offset", 0)
    ''' <summary>
    ''' Gets and sets the End Time Offset value
    ''' </summary>
    <Display(Name:="End Time Offset", Description:="The number of hours and minutes to offset the calculated end time by"),
    Required(ErrorMessage:="End Time Offset required")>
    Public Property EndTimeOffset() As Integer
      Get
        Return GetProperty(EndTimeOffsetProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EndTimeOffsetProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineMatchIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ProductionTimelineMatchInd, "Production Timeline Match", True)
    ''' <summary>
    ''' Gets and sets the Production Timeline Match value
    ''' </summary>
    <Display(Name:="Production Timeline Match", Description:="Tick indicates that this human resource's schedule must always match the production's schedule. Should this be unticked the system will not automatically update the HR schedule to match the producion's schedule"),
    Required(ErrorMessage:="Production Timeline Match required")>
    Public Property ProductionTimelineMatchInd() As Boolean
      Get
        Return GetProperty(ProductionTimelineMatchIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ProductionTimelineMatchIndProperty, Value)
      End Set
    End Property

    Public Shared ExcludeIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ExcludeInd, "Exclude", False)
    ''' <summary>
    ''' Gets and sets the Exclude value
    ''' </summary>
    <Display(Name:="Exclude", Description:="Tick indicates that this HR's time should not be allocated to them"),
    Required(ErrorMessage:="Exclude required")>
    Public Property ExcludeInd() As Boolean
      Get
        Return GetProperty(ExcludeIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ExcludeIndProperty, Value)
      End Set
    End Property

    Public Shared ExcludeReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ExcludeReason, "Exclude Reason", "")
    ''' <summary>
    ''' Gets and sets the Exclude Reason value
    ''' </summary>
    <Display(Name:="Exclude Reason", Description:="The reason this HR's time was marked as excluded"),
    StringLength(200, ErrorMessage:="Exclude Reason cannot be more than 200 characters")>
    Public Property ExcludeReason() As String
      Get
        Return GetProperty(ExcludeReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ExcludeReasonProperty, Value)
      End Set
    End Property

    Public Shared CrewTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTimesheetID, "Crew Timesheet", Nothing)
    ''' <summary>
    ''' Gets and sets the Crew Timesheet value
    ''' </summary>
    <Display(Name:="Crew Timesheet", Description:="The timesheet record that will be created for this schedule detail one the production has finished")>
    Public Property CrewTimesheetID() As Integer?
      Get
        Return GetProperty(CrewTimesheetIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTimesheetIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared TimesheetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TimesheetDate, "Date")
    ''' <summary>
    ''' Gets and sets the Timesheet Date value
    ''' </summary>
    <Display(Name:="Date", Description:=""),
    Required(ErrorMessage:="Timesheet Date required")>
    Public Property TimesheetDate As DateTime?
      Get
        Return GetProperty(TimesheetDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TimesheetDateProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceOffPeriodID, "Human Resource Off Period", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Off Period value
    ''' </summary>
    <Display(Name:="Human Resource Off Period", Description:="the off period (leave) that the scheduled item is linked to")>
    Public Property HumanResourceOffPeriodID() As Integer?
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceOffPeriodIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceScheduleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceScheduleTypeID, "Human Resource Schedule Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Schedule Type value
    ''' </summary>
    <Display(Name:="Human Resource Schedule Type", Description:="the type of booking"),
    Required(ErrorMessage:="Human Resource Schedule Type required")>
    Public Property HumanResourceScheduleTypeID() As Integer?
      Get
        Return GetProperty(HumanResourceScheduleTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceScheduleTypeIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Human Resource Shift", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:="")>
    Public Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingID, "Ad Hoc Booking", Nothing)
    ''' <summary>
    ''' Gets and sets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking", Description:="")>
    Public Property AdHocBookingID() As Integer?
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AdHocBookingIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceSecondmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceSecondmentID, "Human Resource Secondment", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Secondment value
    ''' </summary>
    <Display(Name:="Human Resource Secondment", Description:="")>
    Public Property HumanResourceSecondmentID() As Integer?
      Get
        Return GetProperty(HumanResourceSecondmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceSecondmentIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTimelineTypeID, "Timeline", Nothing)
    ''' <summary>
    ''' Gets the Production Timeline value
    ''' </summary>
    <Display(Name:="Timeline", Description:="")>
    Public Property ProductionTimelineTypeID() As Integer?
      Get
        Return GetProperty(ProductionTimelineTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTimelineTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTimelineType, "Timeline")
    ''' <summary>
    ''' Gets and sets the Exclude Reason value
    ''' </summary>
    <Display(Name:="Timeline", Description:="")>
    Public Property ProductionTimelineType() As String
      Get
        Return GetProperty(ProductionTimelineTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTimelineTypeProperty, Value)
      End Set
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionHRID, Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ProductionHRID() As Integer?
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionHRIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ResourceBookingID, Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ResourceBookingID() As Integer?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared HasClashesIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasClashesInd, "HasClashesInd", False)
    ''' <summary>
    ''' Gets and sets the Exclude value
    ''' </summary>
    <Display(Name:="Has Clashes")>
    Public Property HasClashesInd() As Boolean
      Get
        Return GetProperty(HasClashesIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasClashesIndProperty, Value)
      End Set
    End Property

    <Display(Name:="Starts", Description:="")>
    Public ReadOnly Property Starts() As String
      Get
        If Me.StartDateTime IsNot Nothing Then
          Return Me.StartDateTime.Value.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Ends", Description:="")>
    Public ReadOnly Property Ends() As String
      Get
        If Me.EndDateTime IsNot Nothing Then
          Return Me.EndDateTime.Value.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Browsable(False)>
    Public Property ProductionHumanResource As Productions.Areas.ProductionHumanResource

    <Browsable(False)>
    Public Property ProductionTimeline As Productions.Areas.ProductionTimeline

    Public ReadOnly Property ProductionTimelineGuid As String
      Get
        If Me.ProductionTimeline IsNot Nothing Then
          Return Me.ProductionTimeline.Guid.ToString
        End If
        Return ""
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewScheduleDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Detail.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Crew Schedule Detail Base")
        Else
          Return String.Format("Blank {0}", "Crew Schedule Detail Base")
        End If
      Else
        Return Me.Detail
      End If

    End Function

    Public Function GetParentList() As CrewScheduleDetailOBList
      Return CType(Me.Parent, CrewScheduleDetailOBList)
    End Function

    Public Function GetParent() As Resources.ResourceBookingOB
      If GetParentList() IsNot Nothing Then
        Return CType(GetParentList.Parent, Resources.ResourceBookingOB)
      End If
      Return Nothing
    End Function

    Public Function GetProductionHumanResource() As Productions.Areas.ProductionHumanResource

      Dim productionHROBBooking As Resources.ResourceBookingOB = Me.GetParent
      If productionHROBBooking IsNot Nothing Then
        Dim productionHR As Productions.OB.ProductionHROB = productionHROBBooking.GetParent
        If productionHR IsNot Nothing Then
          If productionHR.ExistingProductionHumanResources.Count = 1 Then
            Return productionHR.ExistingProductionHumanResources(0)
          ElseIf productionHR.ExistingProductionHumanResources.Count > 1 Then
            Return productionHR.ExistingProductionHumanResources(0)
          Else
            Return Nothing
          End If
        End If
      End If

      Return Nothing

    End Function

    Public Sub New(csd As HRProductionScheduleDetail)

      SetProperty(HumanResourceIDProperty, csd.HumanResourceID)
      SetProperty(CrewScheduleDetailIDProperty, 0) 'csd.CrewScheduleDetailID
      SetProperty(ProductionHumanResourceIDProperty, csd.ProductionHumanResourceID)
      SetProperty(ProductionTimelineIDProperty, csd.ProductionTimelineID)
      SetProperty(TimesheetDateProperty, csd.TimesheetDate)
      SetProperty(StartDateTimeProperty, csd.StartDateTime)
      SetProperty(EndDateTimeProperty, csd.EndDateTime)
      SetProperty(DetailProperty, csd.HumanResourceID)
      SetProperty(ExcludeIndProperty, csd.ExcludeInd)
      SetProperty(ExcludeReasonProperty, csd.ExcludeReason)
      SetProperty(CrewTimesheetIDProperty, csd.CrewTimesheetID)
      SetProperty(CreatedByProperty, csd.CreatedBy)
      SetProperty(CreatedDateTimeProperty, csd.CreatedDateTime)
      SetProperty(ModifiedByProperty, csd.ModifiedBy)
      SetProperty(ModifiedDateTimeProperty, csd.ModifiedDateTime)
      SetProperty(ProductionTimelineTypeIDProperty, csd.ProductionTimelineTypeID)
      SetProperty(ProductionTimelineTypeProperty, csd.ProductionTimelineType)
      'SetProperty(FreeIndProperty, csd.FreeInd)
      'SetProperty(ProductionTimelineGuidProperty, csd.ProductionTimelineGuid)
      'SetProperty(ProductionHumanResourceGuidProperty, csd.ProductionHumanResourceGuid)
      SetProperty(HumanResourceShiftIDProperty, csd.HumanResourceShiftID)
      SetProperty(HumanResourceOffPeriodIDProperty, csd.HumanResourceOffPeriodID)
      SetProperty(SystemIDProperty, csd.SystemID)
      SetProperty(ProductionAreaIDProperty, csd.ProductionAreaID)
      SetProperty(HumanResourceScheduleTypeIDProperty, csd.HumanResourceScheduleTypeID)
      SetProperty(ProductionTimelineMatchIndProperty, csd.ProductionTimelineMatchInd)
      SetProperty(EndTimeOffsetProperty, csd.EndTimeOffset)
      SetProperty(StartTimeOffsetProperty, csd.StartTimeOffset)

      If CrewScheduleDetailID = 0 Then
        MarkNew()
        MarkDirty()
      Else
        MarkDirty()
      End If

      Me.ProductionHumanResource = csd.ProductionHumanResource
      Me.ProductionTimeline = csd.ProductionTimeline

      MarkAsChild()

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(TimesheetDateProperty)
        .JavascriptRuleFunctionName = "CrewScheduleDetailBaseBO.TimesheetDateValid"
        .ServerRuleFunction = AddressOf TimesheetDateValid
        .AddTriggerProperty(StartDateTimeProperty)
        .AffectedProperties.Add(StartDateTimeProperty)
      End With

      With AddWebRule(StartDateTimeProperty)
        .JavascriptRuleFunctionName = "CrewScheduleDetailBaseBO.StartDateTimeValid"
        .ServerRuleFunction = AddressOf StartDateTimeValid
        .AddTriggerProperty(EndDateTimeProperty)
        .AffectedProperties.Add(EndDateTimeProperty)
      End With

    End Sub

    Public Function TimesheetDateValid(csd As CrewScheduleDetailOB) As String
      Dim Errors As String = ""
      If Me.TimesheetDate Is Nothing Then
        Errors &= "Schedule Date is required"
      End If
      If Me.StartDateTime Is Nothing Then
        Errors &= "Start Time is required"
      End If
      If Errors = "" Then
        If Me.TimesheetDate.Value.Date <> Me.StartDateTime.Value.Date Then
          Errors &= "Timesheet Date must be the same as Start Date"
        End If
      End If
      Return Errors
    End Function

    Public Function StartDateTimeValid(csd As CrewScheduleDetailOB) As String
      Dim Errors As String = ""
      '1440 minutes = 24 hours
      '720 minutes = 12 hours
      If Me.TimesheetDate Is Nothing Then
        Errors &= "Schedule Date is required"
      End If
      If Me.StartDateTime Is Nothing Then
        Errors &= "Start Time is required"
      End If
      If Me.EndDateTime Is Nothing Then
        Errors &= "End Time is required"
      End If
      If Errors = "" Then
        If Me.EndDateTime.Value.Subtract(Me.StartDateTime.Value).TotalMinutes < 0 Then
          Errors &= "Start Time must be before End time"
        End If
      End If
      Return Errors
    End Function

    Public Function GetLocalClashes(csd As CrewScheduleDetailOB) As List(Of String)

      Dim cl As New List(Of String)
      'For Each crewSched As CrewScheduleDetailOB In Me.GetParent.CrewScheduleDetailOBList
      '  If crewSched.Guid <> Me.Guid Then
      '    If crewSched.StartDateTime > Me.EndDateTime Then
      '      cl.Add("End Time Clash with CSD")
      '    End If
      '    If Me.EndDateTime < crewSched.StartDateTime Then
      '      cl.Add("Start Time Clash with CSD")
      '    End If
      '  End If
      'Next
      Return cl

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCrewScheduleDetailOB() method.

    End Sub

    Public Shared Function NewCrewScheduleDetailOB(ByVal ProductionHumanResource As Productions.Areas.ProductionHumanResource, ByVal ProductionTimeline As Productions.Areas.ProductionTimeline) As CrewScheduleDetailOB

      Return DataPortal.CreateChild(Of CrewScheduleDetailOB)(ProductionHumanResource, ProductionTimeline)

    End Function

    Public Sub New(Template As OBLib.Helpers.Templates.HRScheduleTemplate)

      With Template
        'Me.CrewScheduleDetailID = .CrewScheduleDetailID
        Me.ProductionHumanResourceID = .ProductionHumanResourceID
        Me.ProductionTimelineID = .ProductionTimelineID
        'Me.Detail = .Detail 
        'Me.StartTimeOffset = . 
        Me.StartDateTime = .StartDateTime
        Me.EndDateTime = .EndDateTime
        'Me.EndTimeOffset = . 
        'Me.ProductionTimelineMatchInd = . 
        'Me.ExcludeInd = . 
        'Me.ExcludeReason = . 
        'Me.CrewTimesheetID = . 
        'Me.CreatedBy = . 
        'Me.CreatedDateTime = . 
        'Me.ModifiedBy = . 
        'Me.ModifiedDateTime = . 
        Me.ProductionHumanResource = Template.ProductionHumanResource
        Me.ProductionTimeline = Template.ProductionTimeline
        Me.TimesheetDate = .TimesheetDate
        Me.HumanResourceID = .HumanResourceID
        Me.HumanResourceOffPeriodID = Nothing
        Me.HumanResourceScheduleTypeID = OBLib.CommonData.Enums.HumanResourceScheduleType.Production
        Me.HumanResourceShiftID = Nothing
        Me.ProductionAreaID = .ProductionAreaID
        Me.SystemID = .SystemID
        Me.AdHocBookingID = Nothing
        Me.HumanResourceSecondmentID = Nothing
        Me.ProductionHRID = Nothing 'Me.GetParent.GetParent.ProductionHRID
        Me.ResourceBookingID = Nothing 'Me.GetParent.ResourceBookingID
        Me.ProductionTimelineTypeID = .ProductionTimelineTypeID
        Me.ProductionTimelineType = .ProductionTimelineType
      End With

      MarkAsChild()

    End Sub

    'Public Sub New(HRProductionScheduleDetail As OBLib.Productions.Schedules.HRProductionScheduleDetail)

    '  SetProperty(CrewScheduleDetailIDProperty, HRProductionScheduleDetail.CrewScheduleDetailID)
    '  SetProperty(CreatedByProperty, HRProductionScheduleDetail.CreatedBy)
    '  SetProperty(ModifiedByProperty, HRProductionScheduleDetail.ModifiedBy)
    '  With HRProductionScheduleDetail
    '    'Me.CrewScheduleDetailID = .CrewScheduleDetailID
    '    Me.ProductionHumanResourceID = .ProductionHumanResourceID
    '    Me.ProductionTimelineID = .ProductionTimelineID
    '    Me.Detail = .Detail
    '    Me.StartTimeOffset = .StartTimeOffset
    '    Me.StartDateTime = .StartDateTime
    '    Me.EndDateTime = .EndDateTime
    '    Me.EndTimeOffset = .EndTimeOffset
    '    Me.ProductionTimelineMatchInd = .ProductionTimelineMatchInd
    '    Me.ExcludeInd = .ExcludeInd
    '    Me.ExcludeReason = .ExcludeReason
    '    Me.CrewTimesheetID = .CrewTimesheetID
    '    Me.ProductionHumanResource = HRProductionScheduleDetail.ProductionHumanResource
    '    Me.ProductionTimeline = HRProductionScheduleDetail.ProductionTimeline
    '    Me.TimesheetDate = .TimesheetDate
    '    Me.HumanResourceID = .HumanResourceID
    '    Me.HumanResourceOffPeriodID = Nothing
    '    Me.HumanResourceScheduleTypeID = OBLib.CommonData.Enums.HumanResourceScheduleType.Production
    '    Me.HumanResourceShiftID = Nothing
    '    Me.ProductionAreaID = .ProductionAreaID
    '    Me.SystemID = .SystemID
    '    Me.AdHocBookingID = Nothing
    '    Me.HumanResourceSecondmentID = Nothing
    '    'Me.ProductionHRID = Me.GetParent.GetParent.ProductionHRID
    '    'Me.ResourceBookingID = Me.GetParent.ResourceBookingID
    '    Me.ProductionTimelineTypeID = .ProductionTimelineTypeID
    '    Me.ProductionTimelineType = .ProductionTimelineType
    '  End With

    '  MarkAsChild()

    'End Sub

    'Public Sub New()
    '  MarkAsChild()
    'End Sub

    Public Sub New(ByVal ProductionHumanResource As Productions.Areas.ProductionHumanResource, ByVal ProductionTimeline As Productions.Areas.ProductionTimeline)

      Me.ProductionHumanResource = ProductionHumanResource
      Me.ProductionTimeline = ProductionTimeline
      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCrewScheduleDetailOB(dr As SafeDataReader, ByVal ProductionHumanResource As Productions.Areas.ProductionHumanResource, ByVal ProductionTimeline As Productions.Areas.ProductionTimeline) As CrewScheduleDetailOB

      Dim c As New CrewScheduleDetailOB(ProductionHumanResource, ProductionTimeline)
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CrewScheduleDetailIDProperty, .GetInt32(0))
          LoadProperty(ProductionHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DetailProperty, .GetString(3))
          LoadProperty(StartTimeOffsetProperty, .GetInt32(4))
          LoadProperty(StartDateTimeProperty, .GetValue(5))
          LoadProperty(EndDateTimeProperty, .GetValue(6))
          LoadProperty(EndTimeOffsetProperty, .GetInt32(7))
          LoadProperty(ProductionTimelineMatchIndProperty, .GetBoolean(8))
          LoadProperty(ExcludeIndProperty, .GetBoolean(9))
          LoadProperty(ExcludeReasonProperty, .GetString(10))
          LoadProperty(CrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(CreatedByProperty, .GetInt32(12))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(13))
          LoadProperty(ModifiedByProperty, .GetInt32(14))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(15))
          LoadProperty(TimesheetDateProperty, .GetValue(16))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(HumanResourceOffPeriodIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
          LoadProperty(HumanResourceScheduleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
          LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(20)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
          LoadProperty(AdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
          LoadProperty(HumanResourceSecondmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
          LoadProperty(ProductionHRIDProperty, Singular.Misc.ZeroNothing(.GetInt32(25)))
          LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(26)))
          LoadProperty(ProductionTimelineTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(27)))
          LoadProperty(ProductionTimelineTypeProperty, .GetString(28))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCrewScheduleDetailOB"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCrewScheduleDetailOB"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCrewScheduleDetailID As SqlParameter = .Parameters.Add("@CrewScheduleDetailID", SqlDbType.Int)
          paramCrewScheduleDetailID.Value = GetProperty(CrewScheduleDetailIDProperty)
          If Me.IsNew And CrewScheduleDetailID = 0 Then
            paramCrewScheduleDetailID.Direction = ParameterDirection.Output
          End If

          If ProductionTimeline Is Nothing Then
            Dim psa As OBLib.Productions.Areas.ProductionSystemArea = Me.GetParent.GetParent.GetParent
            Dim timelines As List(Of ProductionTimeline) = psa.ProductionTimelineList.Where(Function(d) CompareSafe(d.ProductionTimelineTypeID, Me.ProductionTimelineTypeID)).ToList
            If timelines.Count > 0 Then
              Me.ProductionTimeline = timelines(0)
            End If
          End If

          If ProductionHumanResource Is Nothing Then
            Dim psa As OBLib.Productions.Areas.ProductionSystemArea = Me.GetParent.GetParent.GetParent
            Dim phrs As List(Of ProductionHumanResource) = psa.ProductionHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, Me.HumanResourceID)).ToList
            If phrs.Count > 0 Then
              Me.ProductionHumanResource = phrs(0)
            End If
          End If

          .Parameters.AddWithValue("@ProductionHumanResourceID", ProductionHumanResource.ProductionHumanResourceID) 'Singular.Misc.NothingDBNull(GetProperty(ProductionHumanResourceIDProperty))
          .Parameters.AddWithValue("@ProductionTimelineID", ProductionTimeline.ProductionTimelineID) 'Singular.Misc.NothingDBNull(GetProperty(ProductionTimelineIDProperty))
          .Parameters.AddWithValue("@Detail", GetProperty(DetailProperty))
          .Parameters.AddWithValue("@StartTimeOffset", GetProperty(StartTimeOffsetProperty))
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndTimeOffset", GetProperty(EndTimeOffsetProperty))
          .Parameters.AddWithValue("@ProductionTimelineMatchInd", GetProperty(ProductionTimelineMatchIndProperty))
          .Parameters.AddWithValue("@ExcludeInd", GetProperty(ExcludeIndProperty))
          .Parameters.AddWithValue("@ExcludeReason", GetProperty(ExcludeReasonProperty))
          .Parameters.AddWithValue("@CrewTimesheetID", Singular.Misc.NothingDBNull(GetProperty(CrewTimesheetIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@TimesheetDate", (New SmartDate(GetProperty(TimesheetDateProperty))).DBValue)
          .Parameters.AddWithValue("@HumanResourceID", NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@HumanResourceOffPeriodID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceOffPeriodIDProperty)))
          .Parameters.AddWithValue("@HumanResourceScheduleTypeID", NothingDBNull(GetProperty(HumanResourceScheduleTypeIDProperty)))
          .Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceShiftIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@AdHocBookingID", NothingDBNull(GetProperty(AdHocBookingIDProperty)))
          .Parameters.AddWithValue("@HumanResourceSecondmentID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceSecondmentIDProperty)))
          'If Me.GetParent IsNot Nothing Then
          .Parameters.AddWithValue("@ResourceBookingID", Me.GetParent.ResourceBookingID)
          'Else
          '  .Parameters.AddWithValue("@ResourceBookingID", Singular.Misc.NothingDBNull(GetProperty(ResourceBookingIDProperty)))
          'End If
          .Parameters.AddWithValue("@ProductionHRID", Me.GetParent.GetParent.ProductionHRID)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CrewScheduleDetailIDProperty, paramCrewScheduleDetailID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCrewScheduleDetailOB"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CrewScheduleDetailID", GetProperty(CrewScheduleDetailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace