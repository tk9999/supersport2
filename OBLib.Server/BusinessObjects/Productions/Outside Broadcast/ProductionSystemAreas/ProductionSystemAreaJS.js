﻿function CheckAreaTimelineValid(obj) {
  Singular.ShowLoadingBar();
  ViewModel.CallServerMethod('CheckAreaValid', { ClientProductionSystemArea: KOFormatter.Serialise(obj) , RemovedGuid: '' }, function (WebResult) {
    console.log(WebResult);
    if (WebResult.Data == '') {
      ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionSystemAreaClashes('');
    } else {
      ViewModel.CurrentProduction().ProductionSystemAreaList()[0].ProductionSystemAreaClashes(WebResult.Data);
      Singular.HideLoadingBar();
    };
  });
};

function ProductionSystemAreaToString() {
  if (self.ProductionAreaID()) {
    var ar = ClientData.ROProductionAreaList.Find('ProductionAreaID', self.ProductionAreaID())
    return ar.ProductionArea;
  } else {
    return 'New Area'
  }
};

function ProductionSpecRequirementIDSet(self) {
  ProductionHelper.ProductionSystemAreas.Events.ProductionSpecRequirementIDSet(self);
}

function ProductionStatusIDChanged(self) {
  ProductionHelper.ProductionSystemAreas.Events.ProductionStatusIDChanged(self)
}

function NotComplySpecReasonsSet(self) {
  ProductionHelper.ProductionSystemAreas.Events.NotComplySpecReasonsSet(self);
}

function CheckSpecRequirementValid(self) {
  ProductionHelper.ProductionSystemAreas.Methods.CheckSpecValid(self)
}

function SpecRequirementValid(Value, Rule, Args) {
  ProductionHelper.ProductionSystemAreas.Methods.SpecRequirementValid(Value, Rule, CtlError)
}