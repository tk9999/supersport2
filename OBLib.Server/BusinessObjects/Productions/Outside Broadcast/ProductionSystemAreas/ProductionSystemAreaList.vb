﻿' Generated 30 Jun 2014 10:57 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Areas

  <Serializable()> _
  Public Class ProductionSystemAreaList
    Inherits SingularBusinessListBase(Of ProductionSystemAreaList, ProductionSystemArea)

#Region " Business Methods "

    Public Function GetItem(ProductionSystemAreaID As Integer) As ProductionSystemArea

      For Each child As ProductionSystemArea In Me
        If child.ProductionSystemAreaID = ProductionSystemAreaID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetOBItem() As ProductionSystemArea

      For Each child As ProductionSystemArea In Me
        If child.ProductionAreaID = CommonData.Enums.ProductionArea.OB Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production System Areas"

    End Function

    Public Function GetProductionTimeline(ProductionTimelineID As Integer) As ProductionTimeline

      Dim obj As ProductionTimeline = Nothing
      For Each parent As ProductionSystemArea In Me
        obj = parent.ProductionTimelineList.GetItem(ProductionTimelineID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetProductionHumanResource(ProductionHumanResourceID As Integer) As ProductionHumanResource

      Dim obj As ProductionHumanResource = Nothing
      For Each parent As ProductionSystemArea In Me
        obj = parent.ProductionHumanResourceList.GetItem(ProductionHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionSystemAreaID As Integer? = Nothing

      Public Sub New(ProductionSystemAreaID As Integer?)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionSystemAreaList() As ProductionSystemAreaList

      Return New ProductionSystemAreaList()

    End Function

    Public Shared Sub BeginGetProductionSystemAreaList(CallBack As EventHandler(Of DataPortalResult(Of ProductionSystemAreaList)))

      Dim dp As New DataPortal(Of ProductionSystemAreaList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionSystemAreaList() As ProductionSystemAreaList

      Return DataPortal.Fetch(Of ProductionSystemAreaList)(New Criteria())

    End Function

    Public Shared Function GetProductionSystemAreaList(ProductionSystemAreaID As Integer?) As ProductionSystemAreaList

      Return DataPortal.Fetch(Of ProductionSystemAreaList)(New Criteria(ProductionSystemAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionSystemArea.GetProductionSystemArea(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'Dim parent As ProductionSystemArea = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.ProductionSystemAreaID <> sdr.GetInt32(12) Then
      '      parent = Me.GetItem(sdr.GetInt32(12))
      '    End If
      '    parent.ProductionTimelineList.RaiseListChangedEvents = False
      '    parent.ProductionTimelineList.Add(ProductionTimeline.GetProductionTimeline(sdr))
      '    parent.ProductionTimelineList.RaiseListChangedEvents = True
      '  End While
      'End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.ProductionSystemAreaID <> sdr.GetInt32(25) Then
      '      parent = Me.GetItem(sdr.GetInt32(25))
      '    End If
      '    parent.ProductionHumanResourceList.RaiseListChangedEvents = False
      '    parent.ProductionHumanResourceList.Add(ProductionHumanResource.GetProductionHumanResource(sdr))
      '    parent.ProductionHumanResourceList.RaiseListChangedEvents = True
      '  End While
      'End If

      'For Each child As ProductionSystemArea In Me
      '  child.CheckRules()
      '  For Each ProductionTimeline As ProductionTimeline In child.ProductionTimelineList
      '    ProductionTimeline.CheckRules()
      '  Next
      '  For Each ProductionHumanResource As ProductionHumanResource In child.ProductionHumanResourceList
      '    ProductionHumanResource.CheckRules()
      '  Next
      '  For Each AreaSchedule As AreaSchedule In child.AreaScheduleList
      '    AreaSchedule.CheckRules()
      '  Next
      'Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionSystemAreaList"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionSystemArea In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionSystemArea In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace