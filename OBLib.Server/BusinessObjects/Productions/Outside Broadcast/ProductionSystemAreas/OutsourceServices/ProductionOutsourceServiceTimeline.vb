﻿' Generated 20 Jul 2014 17:30 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Services

  <Serializable()> _
  Public Class ProductionOutsourceServiceTimeline
    Inherits SingularBusinessBase(Of ProductionOutsourceServiceTimeline)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionOutsourceServiceTimelineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionOutsourceServiceTimelineID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionOutsourceServiceTimelineID() As Integer
      Get
        Return GetProperty(ProductionOutsourceServiceTimelineIDProperty)
      End Get
    End Property

    Public Shared ProductionOutsourceServiceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionOutsourceServiceID, "Production Outsource Service", Nothing)
    ''' <summary>
    ''' Gets the Production Outsource Service value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionOutsourceServiceID() As Integer?
      Get
        Return GetProperty(ProductionOutsourceServiceIDProperty)
      End Get
    End Property

    Public Shared ServiceStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ServiceStartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Service Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="The date that this service is required from"),
    Required(ErrorMessage:="Start Date Time is required")>
    Public Property ServiceStartDateTime As DateTime?
      Get
        Return GetProperty(ServiceStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ServiceStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ServiceEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ServiceEndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the Service End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="The date that this service is required until"),
    Required(ErrorMessage:="End Date Time is required")>
    Public Property ServiceEndDateTime As DateTime?
      Get
        Return GetProperty(ServiceEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ServiceEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CateringBreakfastIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CateringBreakfastInd, "Breakfast?", False)
    ''' <summary>
    ''' Gets and sets the Catering Breakfast value
    ''' </summary>
    <Display(Name:="Breakfast?", Description:="Tick indicated that this outsource service is supplying breakfasts")>
    Public Property CateringBreakfastInd() As Boolean
      Get
        Return GetProperty(CateringBreakfastIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CateringBreakfastIndProperty, Value)
      End Set
    End Property

    Public Shared CateringLunchIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CateringLunchInd, "Lunch?", False)
    ''' <summary>
    ''' Gets and sets the Catering Lunch value
    ''' </summary>
    <Display(Name:="Lunch?", Description:="Tick indicated that this outsource service is supplying lunches")>
    Public Property CateringLunchInd() As Boolean
      Get
        Return GetProperty(CateringLunchIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CateringLunchIndProperty, Value)
      End Set
    End Property

    Public Shared CateringDinnerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CateringDinnerInd, "Dinner?", False)
    ''' <summary>
    ''' Gets and sets the Catering Dinner value
    ''' </summary>
    <Display(Name:="Dinner?", Description:="Tick indicated that this outsource service is supplying dinners")>
    Public Property CateringDinnerInd() As Boolean
      Get
        Return GetProperty(CateringDinnerIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CateringDinnerIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, OBLib.JSCode.ProductionOutsourceServiceTimelineJS.OutsourceServiceTimelineToString)

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionOutsourceService

      Return CType(CType(Me.Parent, ProductionOutsourceServiceTimelineList).Parent, ProductionOutsourceService)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionOutsourceServiceTimelineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Outsource Service Timeline")
        Else
          Return String.Format("Blank {0}", "Production Outsource Service Timeline")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(CateringBreakfastIndProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionOutsourceServiceTimelineJS.CateringValid
        .AddTriggerProperty(CateringLunchIndProperty)
        .AddTriggerProperty(CateringDinnerIndProperty)
      End With

      With AddWebRule(ServiceStartDateTimeProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionOutsourceServiceTimelineJS.StartEndDatesValid
      End With

      With AddWebRule(ServiceEndDateTimeProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionOutsourceServiceTimelineJS.StartEndDatesValid
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionOutsourceServiceTimeline() method.

    End Sub

    Public Shared Function NewProductionOutsourceServiceTimeline() As ProductionOutsourceServiceTimeline

      Return DataPortal.CreateChild(Of ProductionOutsourceServiceTimeline)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionOutsourceServiceTimeline(dr As SafeDataReader) As ProductionOutsourceServiceTimeline

      Dim p As New ProductionOutsourceServiceTimeline()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionOutsourceServiceTimelineIDProperty, .GetInt32(0))
          LoadProperty(ProductionOutsourceServiceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ServiceStartDateTimeProperty, .GetValue(2))
          LoadProperty(ServiceEndDateTimeProperty, .GetValue(3))
          LoadProperty(CateringBreakfastIndProperty, .GetBoolean(4))
          LoadProperty(CateringLunchIndProperty, .GetBoolean(5))
          LoadProperty(CateringDinnerIndProperty, .GetBoolean(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionOutsourceServiceTimeline"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionOutsourceServiceTimeline"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionOutsourceServiceTimelineID As SqlParameter = .Parameters.Add("@ProductionOutsourceServiceTimelineID", SqlDbType.Int)
          paramProductionOutsourceServiceTimelineID.Value = GetProperty(ProductionOutsourceServiceTimelineIDProperty)
          If Me.IsNew Then
            paramProductionOutsourceServiceTimelineID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionOutsourceServiceID", Me.GetParent().ProductionOutsourceServiceID)
          cm.Parameters.AddWithValue("@ServiceStartDateTime", (New SmartDate(GetProperty(ServiceStartDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@ServiceEndDateTime", (New SmartDate(GetProperty(ServiceEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@CateringBreakfastInd", GetProperty(CateringBreakfastIndProperty))
          .Parameters.AddWithValue("@CateringLunchInd", GetProperty(CateringLunchIndProperty))
          .Parameters.AddWithValue("@CateringDinnerInd", GetProperty(CateringDinnerIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionOutsourceServiceTimelineIDProperty, paramProductionOutsourceServiceTimelineID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionOutsourceServiceTimeline"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionOutsourceServiceTimelineID", GetProperty(ProductionOutsourceServiceTimelineIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace