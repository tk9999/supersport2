﻿' Generated 20 Jul 2014 17:30 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Services

  <Serializable()> _
  Public Class ProductionOutsourceServiceDetail
    Inherits SingularBusinessBase(Of ProductionOutsourceServiceDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionOutsourceServiceDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionOutsourceServiceDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionOutsourceServiceDetailID() As Integer
      Get
        Return GetProperty(ProductionOutsourceServiceDetailIDProperty)
      End Get
    End Property

    Public Shared ProductionOutsourceServiceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionOutsourceServiceID, "Production Outsource Service", Nothing)
    ''' <summary>
    ''' Gets the Production Outsource Service value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionOutsourceServiceID() As Integer?
      Get
        Return GetProperty(ProductionOutsourceServiceIDProperty)
      End Get
    End Property

    Public Shared ProductionOutsourceServiceDetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionOutsourceServiceDetail, "Detail", "")
    ''' <summary>
    ''' Gets and sets the Production Outsource Service Detail value
    ''' </summary>
    <Display(Name:="Detail", Description:="The details of the outsource service"),
    Required(ErrorMessage:="Detail required"),
    StringLength(100, ErrorMessage:="Detail cannot be more than 100 characters")>
    Public Property ProductionOutsourceServiceDetail() As String
      Get
        Return GetProperty(ProductionOutsourceServiceDetailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionOutsourceServiceDetailProperty, Value)
      End Set
    End Property

    Public Shared RequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiredInd, "Required?", True)
    ''' <summary>
    ''' Gets and sets the Required value
    ''' </summary>
    <Display(Name:="Required?", Description:="Tick indicates that this detail is required")>
    Public Property RequiredInd() As Boolean
      Get
        Return GetProperty(RequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RequiredIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionOutsourceService

      Return CType(CType(Me.Parent, ProductionOutsourceServiceDetailList).Parent, ProductionOutsourceService)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionOutsourceServiceDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionOutsourceServiceDetail.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Outsource Service Detail")
        Else
          Return String.Format("Blank {0}", "Production Outsource Service Detail")
        End If
      Else
        Return Me.ProductionOutsourceServiceDetail
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionOutsourceServiceDetail() method.

    End Sub

    Public Shared Function NewProductionOutsourceServiceDetail() As ProductionOutsourceServiceDetail

      Return DataPortal.CreateChild(Of ProductionOutsourceServiceDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionOutsourceServiceDetail(dr As SafeDataReader) As ProductionOutsourceServiceDetail

      Dim p As New ProductionOutsourceServiceDetail()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionOutsourceServiceDetailIDProperty, .GetInt32(0))
          LoadProperty(ProductionOutsourceServiceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionOutsourceServiceDetailProperty, .GetString(2))
          LoadProperty(RequiredIndProperty, .GetBoolean(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionOutsourceServiceDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionOutsourceServiceDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionOutsourceServiceDetailID As SqlParameter = .Parameters.Add("@ProductionOutsourceServiceDetailID", SqlDbType.Int)
          paramProductionOutsourceServiceDetailID.Value = GetProperty(ProductionOutsourceServiceDetailIDProperty)
          If Me.IsNew Then
            paramProductionOutsourceServiceDetailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionOutsourceServiceID", Me.GetParent().ProductionOutsourceServiceID)
          .Parameters.AddWithValue("@ProductionOutsourceServiceDetail", GetProperty(ProductionOutsourceServiceDetailProperty))
          .Parameters.AddWithValue("@RequiredInd", GetProperty(RequiredIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionOutsourceServiceDetailIDProperty, paramProductionOutsourceServiceDetailID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionOutsourceServiceDetail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionOutsourceServiceDetailID", GetProperty(ProductionOutsourceServiceDetailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace