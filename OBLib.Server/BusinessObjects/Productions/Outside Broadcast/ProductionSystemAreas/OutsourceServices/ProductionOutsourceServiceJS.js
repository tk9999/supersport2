﻿function OutsourceServiceTypeIDValid(Value, Rule, Args) {
  ProductionHelper.OutsourceServices.Rules.OutsourceServiceTypeIDValid(Value, Rule, CtlError)
}

function NotRequiredIndValid(Value, Rule, Args) {
  ProductionHelper.OutsourceServices.Rules.NotRequiredIndValid(Value, Rule, CtlError)
}

function OutsourceServiceToString() {
  var ROOutsourceServiceType = ClientData.ROOutsourceServiceTypeList.Find("OutsourceServiceTypeID", self.OutsourceServiceTypeID());
  if (ROOutsourceServiceType) {
    return ROOutsourceServiceType.OutsourceServiceType
  } else {
    return 'New Outsource Service'
  }
}

function QuotedAmountValid(Value, Rule, Args) {
  ProductionHelper.OutsourceServices.Rules.QuotedAmountValid(Value, Rule, CtlError)
}