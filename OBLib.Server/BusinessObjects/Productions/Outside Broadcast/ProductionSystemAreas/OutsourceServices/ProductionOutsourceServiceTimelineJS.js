﻿function CateringValid(Value, Rule, Args) {
  ProductionHelper.OutsourceServices.Timelines.Rules.CateringValid(Value, Rule, CtlError)
}

function StartEndDatesValid(Value, Rule, Args) {
  ProductionHelper.OutsourceServices.Timelines.Rules.StartEndDatesValid(Value, Rule, CtlError)
}

function OutsourceServiceTimelineToString() {  
  var ROOutsourceServiceType = ClientData.ROOutsourceServiceTypeList.Find("OutsourceServiceTypeID", self.GetParent().OutsourceServiceTypeID());
  if (ROOutsourceServiceType) {
    return ROOutsourceServiceType.OutsourceServiceType + ' - Timeline'
  } else {
    return 'New Outsource Service Timeline'
  }
}