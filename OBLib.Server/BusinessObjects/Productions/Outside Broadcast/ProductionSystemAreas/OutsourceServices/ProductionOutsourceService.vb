﻿' Generated 20 Jul 2014 17:30 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Areas
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Services

  <Serializable()> _
  Public Class ProductionOutsourceService
    Inherits SingularBusinessBase(Of ProductionOutsourceService)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared ProductionOutsourceServiceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionOutsourceServiceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionOutsourceServiceID() As Integer
      Get
        Return GetProperty(ProductionOutsourceServiceIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The parent Production for this Outsource Service")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared OutsourceServiceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OutsourceServiceTypeID, "Service Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Outsource Service Type value
    ''' </summary>
    <Display(Name:="Service Type", Description:="The type of outsource service that will be required at the production"),
    Required(ErrorMessage:="Service Type required"),
    DropDownWeb(GetType(ROOutsourceServiceTypeList), Source:=DropDownWeb.SourceType.ViewModel)>
    Public Property OutsourceServiceTypeID() As Integer?
      Get
        Return GetProperty(OutsourceServiceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OutsourceServiceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="The supplier that this outsource service is from"),
    DropDownWeb(GetType(ROSupplierList), Source:=DropDownWeb.SourceType.ViewModel)>
    Public Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierIDProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="A description for the outsource service"),
    StringLength(200, ErrorMessage:="Description cannot be more than 200 characters")>
    Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared QuoteRequestedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteRequestedDate, "Quote Requested")
    ''' <summary>
    ''' Gets and sets the Quote Requested Date value
    ''' </summary>
    <Display(Name:="Quote Requested", Description:="The date that the quote was requested from the supplier")>
    Public Property QuoteRequestedDate As DateTime?
      Get
        Return GetProperty(QuoteRequestedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(QuoteRequestedDateProperty, Value)
      End Set
    End Property

    Public Shared QuoteReceivedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteReceivedDate, "Quote Received")
    ''' <summary>
    ''' Gets and sets the Quote Received Date value
    ''' </summary>
    <Display(Name:="Quote Received", Description:="The date that the quote was received from the supplier")>
    Public Property QuoteReceivedDate As DateTime?
      Get
        Return GetProperty(QuoteReceivedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(QuoteReceivedDateProperty, Value)
      End Set
    End Property

    Public Shared QuotedAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.QuotedAmount, "Amount", 0)
    ''' <summary>
    ''' Gets and sets the Quoted Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:="The quoted amount for the outsource service from the supplier")>
    Public Property QuotedAmount() As Decimal
      Get
        Return GetProperty(QuotedAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(QuotedAmountProperty, Value)
      End Set
    End Property

    Public Shared VATInclusiveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.VATInclusiveInd, "VAT Inclusive?", False)
    ''' <summary>
    ''' Gets and sets the VAT Inclusive value
    ''' </summary>
    <Display(Name:="VAT Inclusive?", Description:="Tick indicates that VAT is incuded in the quoted amount")>
    Public Property VATInclusiveInd() As Boolean
      Get
        Return GetProperty(VATInclusiveIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(VATInclusiveIndProperty, Value)
      End Set
    End Property

    Public Shared NotRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.NotRequiredInd, "Not Required", False)
    ''' <summary>
    ''' Gets and sets the Not Required value
    ''' </summary>
    <Display(Name:="Not Required", Description:="True indicates that this outsource service is not required. A reason must be supplied if this is true")>
    Public Property NotRequiredInd() As Boolean
      Get
        Return GetProperty(NotRequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(NotRequiredIndProperty, Value)
      End Set
    End Property

    Public Shared NotRequiredReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotRequiredReason, "Not Required Reason", "")
    ''' <summary>
    ''' Gets and sets the Not Required Reason value
    ''' </summary>
    <Display(Name:="Not Required Reason", Description:="Tick indicates that the outsource service was cancelled"),
    StringLength(100, ErrorMessage:="Not Required Reason cannot be more than 100 characters")>
    Public Property NotRequiredReason() As String
      Get
        Return GetProperty(NotRequiredReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotRequiredReasonProperty, Value)
      End Set
    End Property

    Public Shared NotRequiredByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NotRequiredBy, "Not Required By", OBLib.Security.Settings.CurrentUserID)
    ''' <summary>
    ''' Gets and sets the Not Required By value
    ''' </summary>
    <Display(Name:="Not Required By", Description:="")>
    Public Property NotRequiredBy() As Integer?
      Get
        Return GetProperty(NotRequiredByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NotRequiredByProperty, Value)
      End Set
    End Property

    Public Shared SAPOrderNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SAPOrderNo, "SAP Order No", "")
    ''' <summary>
    ''' Gets and sets the SAP Order No value
    ''' </summary>
    <Display(Name:="SAP Order No", Description:="The SAP Order Number"),
    StringLength(50, ErrorMessage:="SAP Order No cannot be more than 50 characters")>
    Public Property SAPOrderNo() As String
      Get
        Return GetProperty(SAPOrderNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SAPOrderNoProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    DropDownWeb(GetType(ROSystemList))>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Production System Area", 0)
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    'Public Shared WarningsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Warnings, "Warnings", "")
    ' ''' <summary>
    ' ''' Gets and sets the VAT Inclusive value
    ' ''' </summary>
    '<Display(Name:="Warnings")>
    'Public Property Warnings() As String
    '  Get
    '    Return GetProperty(WarningsProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(WarningsProperty, Value)
    '  End Set
    'End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, OBLib.JSCode.ProductionOutsourceServiceJS.OutsourceServiceToString)

#End Region

#Region " Child Lists "

    Public Shared ProductionOutsourceServiceDetailListProperty As PropertyInfo(Of ProductionOutsourceServiceDetailList) = RegisterProperty(Of ProductionOutsourceServiceDetailList)(Function(c) c.ProductionOutsourceServiceDetailList, "Production Outsource Service Detail List")

    Public ReadOnly Property ProductionOutsourceServiceDetailList() As ProductionOutsourceServiceDetailList
      Get
        If GetProperty(ProductionOutsourceServiceDetailListProperty) Is Nothing Then
          LoadProperty(ProductionOutsourceServiceDetailListProperty, Productions.Services.ProductionOutsourceServiceDetailList.NewProductionOutsourceServiceDetailList())
        End If
        Return GetProperty(ProductionOutsourceServiceDetailListProperty)
      End Get
    End Property

    Public Shared ProductionOutsourceServiceTimelineListProperty As PropertyInfo(Of ProductionOutsourceServiceTimelineList) = RegisterProperty(Of ProductionOutsourceServiceTimelineList)(Function(c) c.ProductionOutsourceServiceTimelineList, "Production Outsource Service Timeline List")

    Public ReadOnly Property ProductionOutsourceServiceTimelineList() As ProductionOutsourceServiceTimelineList
      Get
        If GetProperty(ProductionOutsourceServiceTimelineListProperty) Is Nothing Then
          LoadProperty(ProductionOutsourceServiceTimelineListProperty, Productions.Services.ProductionOutsourceServiceTimelineList.NewProductionOutsourceServiceTimelineList())
        End If
        Return GetProperty(ProductionOutsourceServiceTimelineListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionSystemArea

      Return CType(CType(Me.Parent, ProductionOutsourceServiceList).Parent, ProductionSystemArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionOutsourceServiceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Description.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Outsource Service")
        Else
          Return String.Format("Blank {0}", "Production Outsource Service")
        End If
      Else
        Return Me.Description
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ProductionOutsourceServiceDetails", "ProductionOutsourceServiceTimelines"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(OutsourceServiceTypeIDProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionOutsourceServiceJS.OutsourceServiceTypeIDValid
      End With

      With AddWebRule(NotRequiredIndProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionOutsourceServiceJS.NotRequiredIndValid
        .AddTriggerProperty(NotRequiredReasonProperty)
      End With

      With AddWebRule(QuotedAmountProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionOutsourceServiceJS.QuotedAmountValid
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionOutsourceService() method.

    End Sub

    Public Shared Function NewProductionOutsourceService() As ProductionOutsourceService

      Return DataPortal.CreateChild(Of ProductionOutsourceService)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionOutsourceService(dr As SafeDataReader) As ProductionOutsourceService

      Dim p As New ProductionOutsourceService()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionOutsourceServiceIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(OutsourceServiceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(SupplierIDProperty, .GetInt32(3))
          LoadProperty(DescriptionProperty, .GetString(4))
          LoadProperty(QuoteRequestedDateProperty, .GetValue(5))
          LoadProperty(QuoteReceivedDateProperty, .GetValue(6))
          LoadProperty(QuotedAmountProperty, .GetDecimal(7))
          LoadProperty(VATInclusiveIndProperty, .GetBoolean(8))
          LoadProperty(NotRequiredIndProperty, .GetBoolean(9))
          LoadProperty(NotRequiredReasonProperty, .GetString(10))
          LoadProperty(NotRequiredByProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(SAPOrderNoProperty, .GetString(12))
          LoadProperty(CreatedByProperty, .GetInt32(13))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(14))
          LoadProperty(ModifiedByProperty, .GetInt32(15))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(16))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(ProductionSystemAreaIDProperty, .GetInt32(18))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionOutsourceService"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionOutsourceService"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionOutsourceServiceID As SqlParameter = .Parameters.Add("@ProductionOutsourceServiceID", SqlDbType.Int)
          paramProductionOutsourceServiceID.Value = GetProperty(ProductionOutsourceServiceIDProperty)
          If Me.IsNew Then
            paramProductionOutsourceServiceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionID", NothingDBNull(Me.GetParent.ProductionID))
          .Parameters.AddWithValue("@OutsourceServiceTypeID", GetProperty(OutsourceServiceTypeIDProperty))
          .Parameters.AddWithValue("@SupplierID", NothingDBNull(GetProperty(SupplierIDProperty)))
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          .Parameters.AddWithValue("@QuoteRequestedDate", (New SmartDate(GetProperty(QuoteRequestedDateProperty))).DBValue)
          .Parameters.AddWithValue("@QuoteReceivedDate", (New SmartDate(GetProperty(QuoteReceivedDateProperty))).DBValue)
          .Parameters.AddWithValue("@QuotedAmount", GetProperty(QuotedAmountProperty))
          .Parameters.AddWithValue("@VATInclusiveInd", GetProperty(VATInclusiveIndProperty))
          .Parameters.AddWithValue("@NotRequiredInd", GetProperty(NotRequiredIndProperty))
          .Parameters.AddWithValue("@NotRequiredReason", GetProperty(NotRequiredReasonProperty))
          .Parameters.AddWithValue("@NotRequiredBy", Singular.Misc.NothingDBNull(GetProperty(NotRequiredByProperty)))
          .Parameters.AddWithValue("@SAPOrderNo", GetProperty(SAPOrderNoProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@SystemID", NothingDBNull(Me.GetParent.SystemID))
          .Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent().ProductionSystemAreaID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionOutsourceServiceIDProperty, paramProductionOutsourceServiceID.Value)
          End If
          ' update child objects
          If GetProperty(ProductionOutsourceServiceDetailListProperty) IsNot Nothing Then
            Me.ProductionOutsourceServiceDetailList.Update()
          End If
          If GetProperty(ProductionOutsourceServiceTimelineListProperty) IsNot Nothing Then
            Me.ProductionOutsourceServiceTimelineList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ProductionOutsourceServiceDetailListProperty) IsNot Nothing Then
          Me.ProductionOutsourceServiceDetailList.Update()
        End If
        If GetProperty(ProductionOutsourceServiceTimelineListProperty) IsNot Nothing Then
          Me.ProductionOutsourceServiceTimelineList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionOutsourceService"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionOutsourceServiceID", GetProperty(ProductionOutsourceServiceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace