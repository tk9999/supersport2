﻿' Generated 20 Jul 2014 17:30 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Services

  <Serializable()> _
  Public Class ProductionOutsourceServiceList
    Inherits SingularBusinessListBase(Of ProductionOutsourceServiceList, ProductionOutsourceService)

#Region " Business Methods "

    Public Function OutsourceServiceTypeExists(ByVal OutsourceServiceTypeID As Integer) As Boolean

      For Each child As ProductionOutsourceService In Me
        If Singular.Misc.CompareSafe(child.OutsourceServiceTypeID, OutsourceServiceTypeID) Then
          Return True
        End If
      Next
      Return False

    End Function

    Public Function GetItem(ProductionOutsourceServiceID As Integer) As ProductionOutsourceService

      For Each child As ProductionOutsourceService In Me
        If child.ProductionOutsourceServiceID = ProductionOutsourceServiceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Outsource Services"

    End Function

    Public Function GetProductionOutsourceServiceDetail(ProductionOutsourceServiceDetailID As Integer) As ProductionOutsourceServiceDetail

      Dim obj As ProductionOutsourceServiceDetail = Nothing
      For Each parent As ProductionOutsourceService In Me
        obj = parent.ProductionOutsourceServiceDetailList.GetItem(ProductionOutsourceServiceDetailID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetProductionOutsourceServiceTimeline(ProductionOutsourceServiceTimelineID As Integer) As ProductionOutsourceServiceTimeline

      Dim obj As ProductionOutsourceServiceTimeline = Nothing
      For Each parent As ProductionOutsourceService In Me
        obj = parent.ProductionOutsourceServiceTimelineList.GetItem(ProductionOutsourceServiceTimelineID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewProductionOutsourceServiceList() As ProductionOutsourceServiceList

      Return New ProductionOutsourceServiceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionOutsourceService In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionOutsourceService In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace