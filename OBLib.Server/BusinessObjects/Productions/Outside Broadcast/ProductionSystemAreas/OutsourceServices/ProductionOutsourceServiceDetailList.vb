﻿' Generated 20 Jul 2014 17:30 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Services

  <Serializable()> _
  Public Class ProductionOutsourceServiceDetailList
    Inherits SingularBusinessListBase(Of ProductionOutsourceServiceDetailList, ProductionOutsourceServiceDetail)

#Region " Business Methods "

    Public Function GetItem(ProductionOutsourceServiceDetailID As Integer) As ProductionOutsourceServiceDetail

      For Each child As ProductionOutsourceServiceDetail In Me
        If child.ProductionOutsourceServiceDetailID = ProductionOutsourceServiceDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Outsource Service Details"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewProductionOutsourceServiceDetailList() As ProductionOutsourceServiceDetailList

      Return New ProductionOutsourceServiceDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionOutsourceServiceDetail In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionOutsourceServiceDetail In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace