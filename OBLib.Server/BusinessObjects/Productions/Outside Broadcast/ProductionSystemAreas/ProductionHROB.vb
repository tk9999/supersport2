﻿' Generated 18 Jun 2015 18:34 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Resources
Imports OBLib.HR.ReadOnly
Imports OBLib.Productions.Areas
Imports OBLib.Helpers.Templates

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Productions.OB

  <Serializable()> _
  Public Class ProductionHROB
    Inherits OBBusinessBase(Of ProductionHROB)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHRID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public ReadOnly Property ProductionHRID() As Integer
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.HumanResource, "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="ResourceID", Description:="")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    <Browsable(False)>
    Public ReadOnly Property ExistingProductionHumanResources As List(Of ProductionHumanResource)
      Get
        If Me.GetParent IsNot Nothing Then
          Return Me.GetParent.ProductionHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, Me.HumanResourceID)).ToList
        End If
        Return New List(Of ProductionHumanResource)
      End Get
    End Property

    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Disciplines", Description:="")>
    Public ReadOnly Property Disciplines() As String
      Get
        Dim Str As String = ""
        If ExistingProductionHumanResources.Count > 0 Then
          Dim currIndex As Integer = 0
          For Each phr As ProductionHumanResource In ExistingProductionHumanResources
            If currIndex = 0 Then
              Str = phr.Discipline
            Else
              Str &= ", " & phr.Discipline
            End If
          Next
        End If
        Return Str
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CityIDProperty, Value)
      End Set
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City")
    ''' <summary>
    ''' Gets and sets the All Clashes value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CityCodeProperty, Value)
      End Set
    End Property

    Public Shared LocalProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Local, "Local?", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="Local?", Description:="")>
    Public Property Local() As Boolean
      Get
        Return GetProperty(LocalProperty)
      End Get
      Set(value As Boolean)
        SetProperty(LocalProperty, value)
      End Set
    End Property

    <Display(Name:="Starts")>
    Public ReadOnly Property MinStart As String
      Get
        If Me.ResourceBookingOBList.Count > 0 Then
          Return Me.ResourceBookingOBList.Min(Function(d) d.StartDateTime.Value).ToString(" ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Ends")>
    Public ReadOnly Property MaxEnd As String
      Get
        If Me.ResourceBookingOBList.Count > 0 Then
          Return Me.ResourceBookingOBList.Max(Function(d) d.EndDateTime.Value).ToString(" ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    'Public ReadOnly Property AllClashes As List(Of String)
    '  Get
    '    Return Me.ResourceBookingOBList.SelectMany(Function(d) d.ClashList).ToList
    '  End Get
    'End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHRIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.IsNew Then
        Return Me.HumanResource & " - New"
      Else
        Return Me.HumanResource
      End If

    End Function

    Public Function GetParentList() As ProductionHROBList
      Return CType(Me.Parent, ProductionHROBList)
    End Function

    Public Function GetParent() As OBLib.Productions.Areas.ProductionSystemArea
      If GetParentList() IsNot Nothing Then
        Return CType(GetParentList().Parent, OBLib.Productions.Areas.ProductionSystemArea)
      End If
      Return Nothing
    End Function

    'Public Sub UpdateResourceBookingTimesFromTimeline()

    '  Dim ProductionSystemArea As ProductionSystemArea = Me.GetParent()
    '  For Each ProductionHRResourceBooking As ProductionHRResourceBooking In Me.ProductionHRResourceBookingList
    '    'Call Time
    '    Dim callTime As CrewScheduleDetailBase = ProductionHRResourceBooking.CrewScheduleDetailBaseList.Where(Function(d) d.IsStudioCallTime).FirstOrDefault
    '    callTime.TimesheetDate = ProductionSystemArea.StudioCallTime.TimelineDate
    '    callTime.StartDateTime = ProductionSystemArea.StudioCallTime.StartDateTime
    '    callTime.EndDateTime = ProductionSystemArea.StudioOnAirTime.StartDateTime

    '    'On Air Time
    '    Dim onAirTime As CrewScheduleDetailBase = ProductionHRResourceBooking.CrewScheduleDetailBaseList.Where(Function(d) d.IsStudioOnAirTime).FirstOrDefault
    '    onAirTime.TimesheetDate = ProductionSystemArea.StudioOnAirTime.TimelineDate
    '    onAirTime.StartDateTime = ProductionSystemArea.StudioOnAirTime.StartDateTime
    '    onAirTime.EndDateTime = ProductionSystemArea.StudioOnAirTime.EndDateTime

    '    'Wrap Time
    '    Dim wrapTime As CrewScheduleDetailBase = ProductionHRResourceBooking.CrewScheduleDetailBaseList.Where(Function(d) d.IsStudioWrapTime).FirstOrDefault
    '    wrapTime.TimesheetDate = ProductionSystemArea.StudioOnAirTime.TimelineDate
    '    wrapTime.StartDateTime = ProductionSystemArea.StudioOnAirTime.EndDateTime
    '    wrapTime.EndDateTime = ProductionSystemArea.StudioWrapTime.EndDateTime
    '  Next
    '  UpdateResourceBookingTimes()

    'End Sub

    'Sub UpdateResourceBookingTimes()

    '  Me.ProductionHRResourceBookingList.ToList.ForEach(Sub(bkng)

    '                                                      bkng.StartDateTimeBuffer = Me.StudioCallTime.StartDateTime
    '                                                      bkng.StartDateTime = Me.StudioOnAirTime.StartDateTime
    '                                                      bkng.EndDateTime = Me.StudioOnAirTime.EndDateTime
    '                                                      bkng.EndDateTimeBuffer = Me.StudioWrapTime.EndDateTime

    '                                                    End Sub)
    '  'CheckClashes()

    'End Sub

#End Region

#Region " Child Lists "

    Public Shared ResourceBookingOBListProperty As PropertyInfo(Of ResourceBookingOBList) = RegisterProperty(Of ResourceBookingOBList)(Function(c) c.ResourceBookingOBList, "ResourceBookingOBList")
    Public ReadOnly Property ResourceBookingOBList() As ResourceBookingOBList
      Get
        If GetProperty(ResourceBookingOBListProperty) Is Nothing Then
          LoadProperty(ResourceBookingOBListProperty, Resources.ResourceBookingOBList.NewResourceBookingOBList())
        End If
        Return GetProperty(ResourceBookingOBListProperty)
      End Get
    End Property

    'Public Shared UnSelectedProductionTimelineListProperty As PropertyInfo(Of List(Of UnSelectedTimeline)) = RegisterProperty(Of List(Of UnSelectedTimeline))(Function(c) c.UnSelectedProductionTimelineList, "UnSelected Production Timeline List")
    'Public ReadOnly Property UnSelectedProductionTimelineList() As List(Of UnSelectedTimeline)
    '  Get
    '    If GetProperty(UnSelectedProductionTimelineListProperty) Is Nothing Then
    '      LoadProperty(UnSelectedProductionTimelineListProperty, New List(Of UnSelectedTimeline))
    '    End If
    '    Return GetProperty(UnSelectedProductionTimelineListProperty)
    '  End Get
    'End Property

    'Public ReadOnly Property MissingTimelines() As List(Of ProductionTimeline)
    '  Get
    '    If IsActive Then
    '      Dim CurrentMissingTimelines As New List(Of ProductionTimeline)
    '      Dim SelectedTimelies As New List(Of ProductionTimeline)
    '      SelectedTimelies = Me.ResourceBookingOBList(0).CrewScheduleDetailOBList.Select(Function(d) d.ProductionTimeline).ToList
    '      Me.GetParent.ProductionTimelineList.ToList.ForEach(Sub(pt)
    '                                                           If Not SelectedTimelies.Contains(pt) Then
    '                                                             CurrentMissingTimelines.Add(pt)
    '                                                           End If
    '                                                         End Sub)
    '      Return CurrentMissingTimelines
    '    Else
    '      Return New List(Of ProductionTimeline)
    '    End If
    '  End Get
    'End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      'With AddWebRule(ResourceBookingOBListProperty)
      '  '.JavascriptRuleFunctionName = "ProductionHumanResourceBaseBO.ClashCountValid"
      '  .ServerRuleFunction = AddressOf ResourceBookingOBListValid
      '  .AffectedProperties.Add(HumanResourceIDProperty)
      'End With

    End Sub

    'Public Shared Function ResourceBookingOBListValid(ProductionHROB As ProductionHROB) As String
    '  Dim ErrorMsg As String = ""
    '  Dim l As List(Of String) = ProductionHROB.ResourceBookingOBList.SelectMany(Function(d) d.ClashList).ToList
    '  If l.Count > 0 Then
    '    ErrorMsg = "Clashes have been detected"
    '  End If
    '  Return ErrorMsg
    'End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionHRBase() method.

    End Sub

    Public Shared Function NewProductionHRBase() As ProductionHROB

      Return DataPortal.CreateChild(Of ProductionHROB)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionHROB(dr As SafeDataReader) As ProductionHROB

      Dim p As New ProductionHROB()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionHRIDProperty, .GetInt32(0))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(HumanResourceProperty, .GetString(3))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(CityIDProperty, ZeroNothing(.GetInt32(5)))
          LoadProperty(CityCodeProperty, (.GetString(6)))
          LoadProperty(LocalProperty, .GetBoolean(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionHROB"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionHROB"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'Me.ProductionHumanResourceBaseList.Update()

      If Me.IsSelfDirty Then
        With cm
          .CommandType = CommandType.StoredProcedure
          Dim paramProductionHRID As SqlParameter = .Parameters.Add("@ProductionHRID", SqlDbType.Int)
          paramProductionHRID.Value = GetProperty(ProductionHRIDProperty)
          If Me.IsNew Then
            paramProductionHRID.Direction = ParameterDirection.Output
          End If
          If Me.GetParent IsNot Nothing Then
            .Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent.ProductionSystemAreaID)
          Else
            .Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(GetProperty(ProductionSystemAreaIDProperty)))
          End If
          .Parameters.AddWithValue("@HumanResourceID", NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@ResourceID", NothingDBNull(GetProperty(ResourceIDProperty)))
          .ExecuteNonQuery()
          If Me.IsNew Then
            LoadProperty(ProductionHRIDProperty, paramProductionHRID.Value)
          End If
          ' update child objects
          ResourceBookingOBList.Update()
          MarkOld()
        End With
      Else
        ResourceBookingOBList.Update()
        MarkOld()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionHROB"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionHRID", GetProperty(ProductionHRIDProperty))
        cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Function GetResourceBookingTable() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceBookingID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As Resources.ResourceBookingOB In Me.ResourceBookingOBList
        Dim row As DataRow = RBTable.NewRow
        row("ResourceID") = NothingDBNull(rb.ResourceID)
        row("ResourceBookingID") = NothingDBNull(rb.ResourceBookingID)
        row("StartDateTimeBuffer") = rb.StartDateTimeBuffer.ToString
        row("StartDateTime") = rb.StartDateTime.ToString
        row("EndDateTime") = rb.EndDateTime.ToString
        row("EndDateTimeBuffer") = rb.EndDateTimeBuffer.ToString
        RBTable.Rows.Add(row)
      Next
      Return RBTable

    End Function

    'Public Sub CheckClashes()

    '  Dim rbl As New List(Of Helpers.ResourceHelpers.ResourceBooking)
    '  For Each rb As Resources.ResourceBookingOB In Me.ResourceBookingOBList
    '    If rb.ResourceID IsNot Nothing And rb.StartDateTime IsNot Nothing And rb.EndDateTime IsNot Nothing Then
    '      Dim rbs As New Helpers.ResourceHelpers.ResourceBooking(rb.Guid, rb.ResourceID, rb.ResourceBookingID, rb.StartDateTimeBuffer, rb.StartDateTime, rb.EndDateTime, rb.EndDateTimeBuffer)
    '      rbl.Add(rbs)
    '    End If
    '  Next

    '  OBLib.Helpers.ResourceHelpers.GetResourceClashes(rbl)

    '  For Each rb As Helpers.ResourceHelpers.ResourceBooking In rbl
    '    For Each rbs As Resources.ResourceBookingOB In Me.ResourceBookingOBList
    '      Dim att As Resources.ResourceBookingOB = Me.ResourceBookingOBList.Where(Function(d) d.Guid = rb.ObjectGuid).FirstOrDefault
    '      If att IsNot Nothing Then
    '        rb.ClashesWith.ForEach(Sub(cls)
    '                                 att.ClashList.Add(cls)
    '                               End Sub)
    '      End If
    '    Next
    '  Next

    '  'Dim RBTable As DataTable = GetResourceBookingTable()
    '  'Dim cmd As New Singular.CommandProc("RuleProcs.ruleResourcesAvailable")
    '  'Dim ResourceBookings As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
    '  'ResourceBookings.Name = "@ResourceBookings"
    '  'ResourceBookings.SqlType = SqlDbType.Structured
    '  'ResourceBookings.Value = RBTable
    '  'cmd.Parameters.Add(ResourceBookings)
    '  'cmd.FetchType = CommandProc.FetchTypes.DataSet
    '  'cmd = cmd.Execute(0)

    '  'For Each rb As Resources.ProductionHRResourceBooking In Me.ProductionHRResourceBookingList
    '  '  rb.ClashList.Clear()
    '  'Next

    '  'For Each dr As DataRow In cmd.Dataset.Tables(0).Rows
    '  '  Me.ProductionHRResourceBookingList(0).ClashList.Add(dr(12) & ": " & dr(11) & " from: " & CType(dr(7), DateTime).ToString("dd MMM HH:mm"))
    '  'Next

    'End Sub

    'Public Sub SetupUnSelectedTimelines()
    '  Me.UnSelectedProductionTimelineList.Clear()
    '  For Each pt As ProductionTimeline In Me.GetParent.ProductionTimelineList
    '    If pt.IsValid Then
    '      If Me.ResourceBookingOBList.GetItemByProductionTimeline(pt) Is Nothing Then
    '        Dim NewItem As New Helpers.Templates.UnSelectedTimeline(pt, Nothing)
    '        Me.UnSelectedProductionTimelineList.Add(NewItem)
    '      End If
    '    End If
    '  Next
    'End Sub

    Sub Fiddle()
      MarkNew()
      MarkDirty()
      SetProperty(ProductionHRIDProperty, 0)

      Me.ResourceBookingOBList.ToList.ForEach(Sub(x)
                                                x.Fiddle()
                                              End Sub)

    End Sub


  End Class

End Namespace