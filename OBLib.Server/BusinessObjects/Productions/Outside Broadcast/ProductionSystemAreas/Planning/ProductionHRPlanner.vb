﻿' Generated 08 Dec 2014 09:39 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.General.ReadOnly

Namespace Productions.Areas

  <Serializable()> _
  Public Class ProductionHRPlanner
    Inherits OBBusinessBase(Of ProductionHRPlanner)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionSystemAreaPlannerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaPlannerID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public Property ProductionSystemAreaPlannerID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaPlannerIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionSystemAreaPlannerIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
    Public ReadOnly Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    Required(ErrorMessage:="Discipline Required"),
    Singular.DataAnnotations.DropDownWeb(GetType(RODisciplineList), Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel)>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared PlanningHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.PlanningHours, "Planning Hours", 0)
    ''' <summary>
    ''' Gets and sets the Planning Hours value
    ''' </summary>
    <Display(Name:="Planning Hours", Description:="")>
    Public Property PlanningHours() As Decimal
      Get
        Return GetProperty(PlanningHoursProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(PlanningHoursProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "Production Area Status", OBLib.CommonData.Enums.ProductionAreaStatus.New)
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:="")>
    Public Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared StatusSetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StatusSetDate, "Status Date", Now)
    ''' <summary>
    ''' Gets and sets the Status Date value
    ''' </summary>
    <Display(Name:="Status Set Date", Description:="")>
    Public Property StatusSetDate As DateTime?
      Get
        Return GetProperty(StatusSetDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StatusSetDateProperty, Value)
      End Set
    End Property

    Public Shared StatusSetByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StatusSetBy, "Status Set By", OBLib.Security.Settings.CurrentUserID)
    ''' <summary>
    ''' Gets and sets the Status Set By value
    ''' </summary>
    <Display(Name:="Status Set By", Description:="")>
    Public Property StatusSetBy() As Integer
      Get
        Return GetProperty(StatusSetByProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(StatusSetByProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.HumanResource)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource Required")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionSystemArea

      Return CType(CType(Me.Parent, ProductionHRPlannerList).Parent, ProductionSystemArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSystemAreaPlannerIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResourceID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production HR Planner")
        Else
          Return String.Format("Blank {0}", "Production HR Planner")
        End If
      Else
        Return Me.HumanResourceID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionHRPlanner() method.

    End Sub

    Public Shared Function NewProductionHRPlanner() As ProductionHRPlanner

      Return DataPortal.CreateChild(Of ProductionHRPlanner)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionHRPlanner(dr As SafeDataReader) As ProductionHRPlanner

      Dim p As New ProductionHRPlanner()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionSystemAreaPlannerIDProperty, .GetInt32(0))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(PlanningHoursProperty, .GetDecimal(4))
          LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(StatusSetDateProperty, .GetValue(6))
          LoadProperty(StatusSetByProperty, .GetInt32(7))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(11))
          LoadProperty(HumanResourceProperty, .GetString(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionHRPlanner"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionHRPlanner"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionSystemAreaPlannerID As SqlParameter = .Parameters.Add("@ProductionSystemAreaPlannerID", SqlDbType.Int)
          paramProductionSystemAreaPlannerID.Value = GetProperty(ProductionSystemAreaPlannerIDProperty)
          If Me.IsNew Then
            paramProductionSystemAreaPlannerID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent().ProductionSystemAreaID)
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
          .Parameters.AddWithValue("@PlanningHours", GetProperty(PlanningHoursProperty))
          .Parameters.AddWithValue("@ProductionAreaStatusID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaStatusIDProperty)))
          .Parameters.AddWithValue("@StatusSetDate", (New SmartDate(GetProperty(StatusSetDateProperty))).DBValue)
          .Parameters.AddWithValue("@StatusSetBy", Singular.Misc.NothingDBNull(GetProperty(StatusSetByProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionSystemAreaPlannerIDProperty, paramProductionSystemAreaPlannerID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionHRPlanner"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionSystemAreaPlannerID", GetProperty(ProductionSystemAreaPlannerIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace