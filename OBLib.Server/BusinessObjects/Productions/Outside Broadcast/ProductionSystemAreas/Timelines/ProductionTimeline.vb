﻿' Generated 30 Jun 2014 10:57 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Productions.Vehicles.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Productions.Vehicles
Imports OBLib.Maintenance.Vehicles.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Areas

  <Serializable()> _
  Public Class ProductionTimeline
    Inherits SingularBusinessBase(Of ProductionTimeline)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionTimelineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTimelineID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property ProductionTimelineID() As Integer
      Get
        Return GetProperty(ProductionTimelineIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The parent Production for this Timeline item")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionTimelineTypeID, Nothing) _
                                                                                  .AddSetExpression(OBLib.JSCode.ProductionTimelineJS.ProductionTimelineTypeIDSet)

    ''' <summary>
    ''' Gets and sets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Timeline Type", Description:="The type of timeline item"),
    Required(ErrorMessage:="Timeline Type required"),
    DropDownWeb(GetType(ROProductionAreaAllowedTimelineTypeList),
                ValueMember:="ProductionTimelineTypeID", DisplayMember:="ProductionTimelineType", Source:=DropDownWeb.SourceType.ViewModel,
                FilterMethodName:="FilterAllowedTimelineTypes")>
    Public Property ProductionTimelineTypeID() As Integer?
      Get
        Return GetProperty(ProductionTimelineTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTimelineTypeIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing) _
                                                                        .AddSetExpression(OBLib.JSCode.ProductionTimelineJS.StartTimeSet, False)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:="The start date and time of this timeline item"),
    Required(ErrorMessage:="Start Time required"),
    TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property
    ', MaxDateProperty:="EndDateTime"

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing) _
                                                                        .AddSetExpression(OBLib.JSCode.ProductionTimelineJS.EndTimeSet, False)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:="The end date and time of this timeline item"),
    Required(ErrorMessage:="End Time required"),
    TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property
    ', MinDateProperty:="StartDateTime"

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.VehicleID, Nothing) _
                                                                   .AddSetExpression(OBLib.JSCode.ProductionTimelineJS.VehicleIDSet, False)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="The vehicle that the pre/post travel applies to. If left blank then the pre/post travel applies to all the vehicles on the production. Must be left blank from non vehicle pre/post travel types"),
    DropDownWeb(GetType(ROVehicleOldList), ValueMember:="VehicleID", DisplayMember:="VehicleName", FilterMethodName:="GetVehicleDropDown")>
    Public Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="Any comment that the event planner wishes to add. Will be pulled through onto the production schedule"),
    StringLength(500, ErrorMessage:="Comments cannot be more than 500 characters")>
    Public Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ParentProductionTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentProductionTimelineID, "Parent Production Timeline", Nothing)
    ''' <summary>
    ''' Gets and sets the Parent Production Timeline value
    ''' </summary>
    <Display(Name:="Parent Production Timeline", Description:="")>
    Public Property ParentProductionTimelineID() As Integer?
      Get
        Return GetProperty(ParentProductionTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ParentProductionTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, value)
      End Set
    End Property

    Public Shared ProductionTimelineTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTimelineType, "Production Timeline Type")
    ''' <summary>
    ''' Gets and sets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Production Timeline Type", Description:="")>
    Public Property ProductionTimelineType() As String
      Get
        Return GetProperty(ProductionTimelineTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTimelineTypeProperty, Value)
      End Set
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name")
    ''' <summary>
    ''' Gets and sets the Vehicle Name value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="")>
    Public Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VehicleNameProperty, Value)
      End Set
    End Property

    Public Shared CreateByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreateBy, "Create By")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Create By", Description:=""),
    ClientOnly()>
    Public Property CreateBy() As String
      Get
        Return GetProperty(CreateByProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CreateByProperty, Value)
      End Set
    End Property

    Public Shared ModByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ModBy, "Mod By")
    ''' <summary>
    ''' Gets and sets the Mod By value
    ''' </summary>
    <Display(Name:="Mod By", Description:=""),
    ClientOnly()>
    Public Property ModBy() As String
      Get
        Return GetProperty(ModByProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ModByProperty, Value)
      End Set
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "Crew Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="The crew type to which this timeline type will apply"),
    DropDownWeb(GetType(ROCrewTypeList), ValueMember:="CrewTypeID", DisplayMember:="CrewType", Source:=DropDownWeb.SourceType.ViewModel)>
    Public Property CrewTypeID() As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTypeIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Room", Description:="The room to which the production timeline type will apply")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared Property TimelineClashesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimelineClashes, "Timeline Clashes", "")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Timeline Clashes", Description:="")>
    Public Property TimelineClashes() As String
      Get
        Return GetProperty(TimelineClashesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TimelineClashesProperty, Value)
      End Set
    End Property

    Public Shared CanRenderProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CanRender, "CanRender", True)
    <ClientOnly()>
    Public Property CanRender As Boolean
      Get
        Return GetProperty(CanRenderProperty)
      End Get
      Set(value As Boolean)
        SetProperty(CanRenderProperty, value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, OBLib.JSCode.ProductionTimelineJS.TimelineToString)

    '<Display(Name:="Start Date", Description:="The start date and time of this timeline item"),
    'Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy HH:mm")>
    'Public ReadOnly Property ROStartDateTime As DateTime?
    '  Get
    '    Return GetProperty(StartDateTimeProperty)
    '  End Get
    'End Property

    '<Display(Name:="End Date", Description:="The end date and time of this timeline item"),
    'Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy HH:mm")>
    'Public ReadOnly Property ROEndDateTime As DateTime?
    '  Get
    '    Return GetProperty(EndDateTimeProperty)
    '  End Get
    'End Property

    <Browsable(False)>
    Public ReadOnly Property ROProductionTimelineType As ROProductionTimelineType
      Get
        Return CommonData.Lists.ROProductionTimelineTypeList.GetItem(ProductionTimelineTypeID)
      End Get
    End Property

    <ClientOnly()>
    Public Property SelectInd As Boolean = False

    Public Shared SharedTimelineProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SharedTimeline, "Shared Timeline", False)
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised By value
    ''' </summary>
    <Display(Name:="Shared?", Description:="")>
    Public Property SharedTimeline() As Boolean
      Get
        Return GetProperty(SharedTimelineProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SharedTimelineProperty, Value)
      End Set
    End Property

    Public Shared TimelineDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.TimelineDate, Nothing) _
                                                                       .AddSetExpression(OBLib.JSCode.ProductionTimelineJS.TimelineDateSet, False)
    ''' <summary>
    ''' Gets and sets the Timeline Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
     Required(ErrorMessage:="Start Date is required"),
     DateField(FormatString:="dd MMM yyyy")>
    Public Property TimelineDate As DateTime?
      Get
        Return GetProperty(TimelineDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TimelineDateProperty, Value)
      End Set
    End Property

    Public Shared OvernightIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OvernightInd, "Overnight", False)
    ''' <summary>
    ''' Gets and sets the Overnight value
    ''' </summary>
    <Display(Name:="Overnight", Description:="")>
    Public Property OvernightInd() As Boolean
      Get
        Return GetProperty(OvernightIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OvernightIndProperty, Value)
      End Set
    End Property

    Public Shared OvernightSetByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OvernightSetBy, "Overnight Set By", Nothing)
    ''' <summary>
    ''' Gets and sets the Overnight Set By value
    ''' </summary>
    <Display(Name:="Overnight Set By", Description:="")>
    Public Property OvernightSetBy() As Integer?
      Get
        Return GetProperty(OvernightSetByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OvernightSetByProperty, Value)
      End Set
    End Property

    Public Shared OvernightSetDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.OvernightSetDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Overnight Set Date value
    ''' </summary>
    <Display(Name:="Overnight Set Date", Description:="")>
    Public Property OvernightSetDate As DateTime?
      Get
        Return GetProperty(OvernightSetDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OvernightSetDateProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionSystemArea

      Return CType(CType(Me.Parent, ProductionTimelineList).Parent, ProductionSystemArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionTimelineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      'If Me.Comments.Length = 0 Then
      '  If Me.IsNew Then
      '    Return String.Format("New {0}", "Production Timeline")
      '  Else
      '    Return String.Format("Blank {0}", "Production Timeline")
      '  End If
      'Else
      '  Return Me.Comments
      'End If
      Return Me.ROProductionTimelineType.ProductionTimelineType & " - " & Me.StartDateTime

    End Function

    Public Function CalculateTimesheetDate() As Date?

      Dim TimesheetDate As Date? = Nothing
      If (ROProductionTimelineType.PreTransmissionInd = False OrElse ROProductionTimelineType.PreTransmissionInd Is Nothing) And (StartDateTime.Value.Hour >= 0 AndAlso StartDateTime.Value.Hour <= 4) Then
        TimesheetDate = StartDateTime.Value.Date.AddDays(-1)
      Else
        TimesheetDate = StartDateTime.Value.Date
      End If
      Return TimesheetDate

    End Function

    Public Function IsVehicleTravel() As Boolean
      Return (ProductionTimelineTypeID = CommonData.Enums.ProductionTimelineType.VehiclePreTravel _
              OrElse _
              ProductionTimelineTypeID = CommonData.Enums.ProductionTimelineType.VehiclePostTravel)
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      'Me.AddMultiplePropertyRule(AddressOf Singular.CSLALib.Rules.CompareProperties, New Singular.CSLALib.Rules.Args.ComparePropertyArgs("StartDateTime", "EndDateTime", "<="), New String() {"StartDateTime", "EndDateTime"})
      With AddWebRule(StartDateTimeProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionTimelineJS.StartDateTimeValid
      End With

      With AddWebRule(EndDateTimeProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionTimelineJS.EndDateTimeValid
      End With

      With AddWebRule(StartDateTimeProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionTimelineJS.StartDateTimeBeforeEndDateTime
      End With

      With AddWebRule(EndDateTimeProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionTimelineJS.StartDateTimeBeforeEndDateTime
      End With

      With AddWebRule(VehicleIDProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionTimelineJS.VehicleIDValid
        .AddTriggerProperties(ProductionTimelineTypeIDProperty)
      End With

      With AddWebRule(ProductionTimelineTypeIDProperty)
        .JavascriptRuleCode = OBLib.JSCode.ProductionTimelineJS.TempTimelineValid
        '.AddTriggerProperties(New Csla.Core.IPropertyInfo() {Me.GetParent.ProductionAreaStatusIDProperty})
      End With

      With AddWebRule(StartDateTimeProperty,
                      Function(d) d.TimelineClashes <> "",
                      Function(d) d.TimelineClashes)
        .AddTriggerProperties(TimelineClashesProperty)
      End With

      With AddWebRule(EndDateTimeProperty,
                      Function(d) d.TimelineClashes <> "",
                      Function(d) d.TimelineClashes)
        .AddTriggerProperties(TimelineClashesProperty)
      End With

      With AddWebRule(TimelineDateProperty,
                      Function(d) d.TimelineClashes <> "",
                      Function(d) d.TimelineClashes)
        .AddTriggerProperties(TimelineClashesProperty)
      End With

      With AddWebRule(VehicleIDProperty,
                      Function(d) d.TimelineClashes <> "",
                      Function(d) d.TimelineClashes)
        .AddTriggerProperties(TimelineClashesProperty)
      End With

      'With AddWebRule(ProductionTimelineTypeIDProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.ProductionTimelineJS.TimelineOverlaps
      'End With

      'With AddWebRule(StartDateTimeProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.ProductionSystemAreaJS.CheckAreaTimelineValid
      '  .AddTriggerProperties(New Csla.Core.IPropertyInfo() {ProductionTimelineTypeIDProperty, VehicleIDProperty, CrewTypeIDProperty, RoomIDProperty})
      'End With

      'With AddWebRule(EndDateTimeProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.ProductionSystemAreaJS.CheckAreaTimelineValid
      '  .AddTriggerProperties(New Csla.Core.IPropertyInfo() {ProductionTimelineTypeIDProperty, VehicleIDProperty, CrewTypeIDProperty, RoomIDProperty})
      'End With

      'With AddWebRule(VehicleIDProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.ProductionTimelineJS.TimelineOverlaps
      'End With

      'With AddWebRule(CrewTypeIDProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.ProductionTimelineJS.TimelineOverlaps
      'End With

      'With AddWebRule(RoomIDProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.ProductionTimelineJS.TimelineOverlaps
      'End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionTimeline() method.

    End Sub

    Public Shared Function NewProductionTimeline() As ProductionTimeline

      Return DataPortal.CreateChild(Of ProductionTimeline)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionTimeline(dr As SafeDataReader) As ProductionTimeline

      Dim p As New ProductionTimeline()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionTimelineIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionTimelineTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(StartDateTimeProperty, .GetValue(3))
          LoadProperty(EndDateTimeProperty, .GetValue(4))
          LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(CommentsProperty, .GetString(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ParentProductionTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(ProductionTimelineTypeProperty, .GetString(15))
          LoadProperty(VehicleNameProperty, .GetString(16))
          LoadProperty(CreateByProperty, .GetString(17))
          LoadProperty(ModByProperty, .GetString(18))
          LoadProperty(TimelineClashesProperty, .GetString(19))
          LoadProperty(SharedTimelineProperty, .GetBoolean(20))
          LoadProperty(TimelineDateProperty, .GetValue(21))
          LoadProperty(OvernightIndProperty, .GetBoolean(22))
          LoadProperty(OvernightSetByProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
          LoadProperty(OvernightSetDateProperty, .GetValue(24))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(25)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(26)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionTimeline"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionTimeline"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        If Me.SharedTimeline Then
          Dim x = {23, 11}
          If x.Contains(ProductionTimelineTypeID) Then
            If OBLib.Security.Settings.CurrentUser.SystemID = 2 And Not Singular.Security.HasAccess("Productions", "Can Edit Build-Up For All Areas") Then
              Exit Sub
            End If
          Else
            If OBLib.Security.Settings.CurrentUser.SystemID = 2 Then
              Exit Sub
            End If
          End If
        End If

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionTimelineID As SqlParameter = .Parameters.Add("@ProductionTimelineID", SqlDbType.Int)
          paramProductionTimelineID.Value = GetProperty(ProductionTimelineIDProperty)
          If Me.IsNew Then
            paramProductionTimelineID.Direction = ParameterDirection.Output
          End If
          ProductionID = Me.GetParent.ProductionID
          'SystemID = Me.GetParent.SystemID
          .Parameters.AddWithValue("@ProductionID", NothingDBNull(Me.GetParent.ProductionID)) ' GetProperty(ProductionIDProperty)
          .Parameters.AddWithValue("@ProductionTimelineTypeID", GetProperty(ProductionTimelineTypeIDProperty))
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@VehicleID", Singular.Misc.NothingDBNull(GetProperty(VehicleIDProperty)))
          .Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@ParentProductionTimelineID", Singular.Misc.NothingDBNull(GetProperty(ParentProductionTimelineIDProperty)))
          .Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(Me.GetParent.ProductionSystemAreaID))
          .Parameters.AddWithValue("@CrewTypeID", Singular.Misc.NothingDBNull(GetProperty(CrewTypeIDProperty)))
          '.Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(GetProperty(RoomIDProperty)))
          .Parameters.AddWithValue("@TimelineDate", (New SmartDate(GetProperty(TimelineDateProperty))).DBValue)
          '.Parameters.AddWithValue("@OvernightInd", Singular.Misc.NothingDBNull(GetProperty(OvernightIndProperty)))
          '.Parameters.AddWithValue("@OvernightSetBy", Singular.Misc.NothingDBNull(GetProperty(OvernightSetByProperty)))
          '.Parameters.AddWithValue("@OvernightSetDate", (New SmartDate(GetProperty(OvernightSetDateProperty))).DBValue)
          .Parameters.AddWithValue("@SystemID", NothingDBNull(Me.GetParent.SystemID))
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(Me.GetParent.ProductionAreaID))
          .Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUserID)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionTimelineIDProperty, paramProductionTimelineID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionTimeline"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionTimelineID", GetProperty(ProductionTimelineIDProperty))
        cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(Me.GetParent.ProductionSystemAreaID))
        cm.Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUserID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace