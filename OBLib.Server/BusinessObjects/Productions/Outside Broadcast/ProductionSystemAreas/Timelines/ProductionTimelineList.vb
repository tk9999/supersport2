﻿' Generated 30 Jun 2014 10:57 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Productions.Old

Namespace Productions.Areas

  <Serializable()> _
  Public Class ProductionTimelineList
    Inherits SingularBusinessListBase(Of ProductionTimelineList, ProductionTimeline)

#Region " Business Methods "

    Public Function GetItem(ProductionTimelineID As Integer) As ProductionTimeline

      For Each child As ProductionTimeline In Me
        If child.ProductionTimelineID = ProductionTimelineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Timelines"

    End Function

    Public Function GetMinStartDate(Optional ProductionTimelineTypesToIgnore As List(Of Integer?) = Nothing) As Date

      Dim MinDate As Date = Date.MaxValue
      For Each pt As ProductionTimeline In Me
        If Singular.Misc.IsNullNothing(pt.ProductionTimelineTypeID) Then Continue For
        If ProductionTimelineTypesToIgnore IsNot Nothing AndAlso ProductionTimelineTypesToIgnore.Contains(pt.ProductionTimelineTypeID) Then Continue For

        If pt.StartDateTime.HasValue AndAlso CDate(pt.StartDateTime) < MinDate AndAlso
           Not CommonData.Lists.ROProductionTimelineTypeList.GetItem(pt.ProductionTimelineTypeID).FreeInd Then
          MinDate = pt.StartDateTime
        End If
      Next
      Return MinDate

    End Function

    Public Function GetMaxEndDate(Optional ProductionTimelineTypesToIgnore As List(Of Integer?) = Nothing) As Date

      Dim MaxDate As Date = Date.MinValue
      For Each pt As ProductionTimeline In Me.Where(Function(f) Not IsDBNull(f.ProductionTimelineTypeID))
        If ProductionTimelineTypesToIgnore IsNot Nothing AndAlso ProductionTimelineTypesToIgnore.Contains(pt.ProductionTimelineTypeID) Then Continue For

        If pt.EndDateTime.HasValue AndAlso CDate(pt.EndDateTime) > MaxDate AndAlso
           Not CommonData.Lists.ROProductionTimelineTypeList.GetItem(pt.ProductionTimelineTypeID).FreeInd Then
          MaxDate = pt.EndDateTime
        End If
      Next
      Return MaxDate

    End Function

    Public Function AddTx(SystemID As Integer?, ProductionAreaID As Integer?) As ProductionTimeline
      Dim pt As ProductionTimeline = Me.AddNew
      pt.ProductionTimelineTypeID = CType(CommonData.Enums.ProductionTimelineType.Transmission, Integer)
      pt.ProductionTimelineType = OBLib.CommonData.Lists.ROProductionTimelineTypeList.GetItem(pt.ProductionTimelineTypeID).ProductionTimelineType
      Dim Production As OBLib.Productions.Old.Production = pt.GetParent.GetParent
      If Production IsNot Nothing AndAlso Production.PlayStartDateTime IsNot Nothing AndAlso Production.PlayEndDateTime IsNot Nothing Then
        pt.StartDateTime = Production.PlayStartDateTime
        pt.EndDateTime = Production.PlayEndDateTime
      End If
      Return pt
    End Function

    Public Function GetItemByGuid(Guid As Guid) As ProductionTimeline

      For Each child As ProductionTimeline In Me
        If child.Guid = Guid Then
          Return child
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewProductionTimelineList() As ProductionTimelineList

      Return New ProductionTimelineList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionTimeline In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionTimeline In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace