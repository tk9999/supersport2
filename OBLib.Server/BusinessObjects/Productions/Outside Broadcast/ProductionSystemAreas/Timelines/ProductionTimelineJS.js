﻿function TimelineSameDate(Value, Rule, Args) {
  var Timeline = self;
  if (!OBMisc.Dates.SameDay(Timeline.StartDateTime(), Timeline.EndDateTime())) {
    CtlError.AddError("Dates invalid");
  };
};

function TimelineToString() {
  var tt = ClientData.ROProductionAreaAllowedTimelineTypeList.Find('ProductionTimelineTypeID', self.ProductionTimelineTypeID());
  if (tt) {
    return tt.ProductionTimelineType;
  } else {
    return 'Timeline'
  }
};

function StartDateTimeBeforeEndDateTime(Value, Rule, Args) {
  var Timeline = self;
  var SDT = new Date(Timeline.StartDateTime());
  var EDT = new Date(Timeline.EndDateTime());
  if (SDT != NaN && EDT != NaN) {
    var ST = new Date(Timeline.StartDateTime()).getTime();
    var ET = new Date(Timeline.EndDateTime()).getTime();
    if (!(ST <= ET)) { CtlError.AddError("Start Time must be before End Time"); }
  };
};

function CheckTimelineValid(self) {

  var TimelineTypeSet = (self.ProductionTimelineTypeID() ? true : false)
  var StartTimeSet = (self.StartDateTime() ? true : false)
  var EndTimeSet = (self.EndDateTime() ? true : false)

  if (TimelineTypeSet && StartTimeSet && EndTimeSet) {
    var SameDay = OBMisc.Dates.SameDay(self.StartDateTime(), self.EndDateTime())
    if (SameDay) {
      self.TimelineClashes("")
      //Singular.ShowLoadingBar();
      //var obj = new ProductionSystemAreaRules_ProductionTimelineClientObject();
      //obj.ProductionTimeline(self);
      //Singular.SendCommand('CheckSchedules', { TimelineGuid: self.Guid() }, function (d) { });
//      ViewModel.CallServerMethod('CheckTimelineValid', { ProductionTimelineClient: KOFormatter.Serialise(obj) }, function (WebResult) {
//        if (WebResult.Data == '') { self.TimelineClashes('') } else { self.TimelineClashes(WebResult.Data) };
//        Singular.SendCommand('CheckSchedules', {}, function (d) {
//          Singular.HideLoadingBar();
//          if (self.IsValid()) {
//            //self.GetParent().ProductionTimelineList.ProcessOptions.SortOptions({ SortProperty: 'StartDateTime', SortAsc: true });
//          }
//        });
//      })
    }
  }

};

function VehicleIDValid(Value, Rule, Args) {

  //only check this rule for OB's
  if (self.GetParent().ProductionAreaID() == 1) {
    var Timeline = self;
    var ProductionSystemArea = Timeline.GetParent();
    if (Timeline.ProductionTimelineTypeID()) {
      switch (ProductionSystemArea.ProductionAreaID()) {
        case 1:
          CheckVanRulesOB();
          break;
        default:
          break;
      }
    }
  };

  function CheckVanRulesOB() {
    var AllowedVehicleTimelineTypes = [1, 14, 22, 25, 26];
    var VehicleSelected = ((Timeline.VehicleID()) ? true : false);
    if (AllowedVehicleTimelineTypes.indexOf(Timeline.ProductionTimelineTypeID()) < 0 && VehicleSelected) {
      CtlError.AddError("Vehicle can only be selected for vehicle/driver pre/post types")
    }
  }

};

function TempTimelineValid(Value, Rule, Args) {
  //only check this rule for OB's
  if (self.GetParent().ProductionAreaID() == 1) {
    var Timeline = self;
    var ProductionSystemArea = Timeline.GetParent();
    var Production = ProductionSystemArea.GetParent();
    var StatusValid = (Production.PlanningFinalised() || Production.Reconciled());
    //var InvalidStatuses = [3, 4]; //Planning Finalised, Reconciled
    //var IsInvalidStatus = ((InvalidStatuses.indexOf(ProductionSystemArea.ProductionAreaStatusID()) >= 0) ? true : false);
    if (Timeline.ProductionTimelineTypeID() == 41 && StatusValid) {
      CtlError.AddError("Temp Timelines must be removed before Planning finalised");
    };
  };
};

function TimelineTypeIDValid(Value, Rule, Args) {
  //excluded
};

function SetProductionTimelineType(self) {
  ClientData.ROProductionAreaAllowedTimelineTypeList.Iterate(function (Item, Index) {
    if (Item.ProductionTimelineTypeID == self.ProductionTimelineTypeID()) {
      self.ProductionTimelineType(Item.ProductionTimelineType);
    };
  });
};

function SetEndTime(self) {
  var Timeline = self;
  if (!OBMisc.Dates.SameDay(Timeline.StartDateTime(), Timeline.EndDateTime())) {
    //End Time
    var ED = new Date(Timeline.EndDateTime());
    var Hours = ED.getHours();
    var Minutes = ED.getMinutes();
    //Start Time
    var SD = new Date(Timeline.StartDateTime());
    var Year = SD.getFullYear();
    var Month = SD.getMonth();
    var Day = SD.getDate();
    //New End Date
    var NewEndDate = new Date(Year, Month, Day, Hours, Minutes, 0);
    self.SuspendChanges = true
    Timeline.EndDateTime(NewEndDate);
    self.SuspendChanges = false
  };
};

function CheckVehicleValid(self) {
  if (self.IsValid) {
    var ExcludedTimelineTypes = [2, 3, 5, 7, 8, 12, 13, 20, 22, 25, 26, 28, 29, 32, 33, 34, 35, 36, 37, 41, 42, 43, 46, 47, 48, 49];
    var Tx = [11];
    if (ExcludedTimelineTypes.indexOf(self.ProductionTimelineTypeID()) < 0 && Tx.indexOf(self.ProductionTimelineTypeID()) >= 0) {
      Singular.SendCommand('CheckVehiclesValid', {}, function (d) { })
    }
  }
}

function StartDateTimeValid(self) {
  var td = new Date(self.TimelineDate());
  var sd = new Date(self.StartDateTime());
  var ed = new Date(self.EndDateTime());
  if (!moment(sd).isSame(td, 'day')) {
    CtlError.AddError("Start Date and Start Time must be on the same day");
  }
  if (moment(sd).isAfter(ed, 'minute') || moment(sd).isSame(ed, 'minute')) {
    CtlError.AddError("End Time cannot be before Start Time");
  }
};

function EndDateTimeValid(self) {
  var td = new Date(self.TimelineDate());
  var sd = new Date(self.StartDateTime());
  var ed = new Date(self.EndDateTime());
  if (!moment(ed).isSame(td, 'day')) {
    CtlError.AddError("Start Date and End Time must be on the same day");
  }
  if (moment(sd).isAfter(ed, 'minute') || moment(sd).isSame(ed, 'minute')) {
    CtlError.AddError("End Time cannot be before Start Time");
  }
};

function TimelineDateSet(self) {
  ProductionHelper.ProductionTimelines.Events.TimelineDateSet(self);
}

function StartTimeSet(self) {
  ProductionHelper.ProductionTimelines.Events.StartTimeSet(self);
}

function EndTimeSet(self) {
  ProductionHelper.ProductionTimelines.Events.EndTimeSet(self);
}

function ProductionTimelineTypeIDSet(self) {
  ProductionHelper.ProductionTimelines.Events.ProductionTimelineTypeIDSet(self);
}

function VehicleIDSet(self) {
  ProductionHelper.ProductionTimelines.Events.VehicleIDSet(self);
}