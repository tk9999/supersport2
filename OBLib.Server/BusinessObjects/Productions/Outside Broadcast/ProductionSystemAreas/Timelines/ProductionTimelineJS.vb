' Singular Systems .js CodeGen Utility version 1.0.2.
' This code is generated by an automated tool. Edit the .js file this was generated from, not this file.
' Generated from C:\Clients\OutsideBroadcast\Development\OutsideBroadcast-QA\OBLib.Server\BusinessObjects\Productions\Outside Broadcast\ProductionSystemAreas\Timelines\ProductionTimelineJS.js at 19 Aug 2015 - 12:36:55.

Namespace JSCode

Public Class ProductionTimelineJS

Public Const TimelineSameDate As String = "var Timeline = self;" & vbCrLf &
"  if (!OBMisc.Dates.SameDay(Timeline.StartDateTime(), Timeline.EndDateTime())) {" & vbCrLf &
"    CtlError.AddError(""Dates invalid"");" & vbCrLf &
"  };"

Public Const TimelineToString As String = "var tt = ClientData.ROProductionAreaAllowedTimelineTypeList.Find('ProductionTimelineTypeID', self.ProductionTimelineTypeID());" & vbCrLf &
"  if (tt) {" & vbCrLf &
"    return tt.ProductionTimelineType;" & vbCrLf &
"  } else {" & vbCrLf &
"    return 'Timeline'" & vbCrLf &
"  }"

Public Const StartDateTimeBeforeEndDateTime As String = "var Timeline = self;" & vbCrLf &
"  var SDT = new Date(Timeline.StartDateTime());" & vbCrLf &
"  var EDT = new Date(Timeline.EndDateTime());" & vbCrLf &
"  if (SDT != NaN && EDT != NaN) {" & vbCrLf &
"    var ST = new Date(Timeline.StartDateTime()).getTime();" & vbCrLf &
"    var ET = new Date(Timeline.EndDateTime()).getTime();" & vbCrLf &
"    if (!(ST <= ET)) { CtlError.AddError(""Start Time must be before End Time""); }" & vbCrLf &
"  };"

Public Const CheckTimelineValid As String = "var TimelineTypeSet = (self.ProductionTimelineTypeID() ? true : false)" & vbCrLf &
"  var StartTimeSet = (self.StartDateTime() ? true : false)" & vbCrLf &
"  var EndTimeSet = (self.EndDateTime() ? true : false)" & vbCrLf &
"" & vbCrLf &
"  if (TimelineTypeSet && StartTimeSet && EndTimeSet) {" & vbCrLf &
"    var SameDay = OBMisc.Dates.SameDay(self.StartDateTime(), self.EndDateTime())" & vbCrLf &
"    if (SameDay) {" & vbCrLf &
"      self.TimelineClashes("""")" & vbCrLf &
"      //Singular.ShowLoadingBar();" & vbCrLf &
"      //var obj = new ProductionSystemAreaRules_ProductionTimelineClientObject();" & vbCrLf &
"      //obj.ProductionTimeline(self);" & vbCrLf &
"      //Singular.SendCommand('CheckSchedules', { TimelineGuid: self.Guid() }, function (d) { });" & vbCrLf &
"//      ViewModel.CallServerMethod('CheckTimelineValid', { ProductionTimelineClient: KOFormatter.Serialise(obj) }, function (WebResult) {" & vbCrLf &
"//        if (WebResult.Data == '') { self.TimelineClashes('') } else { self.TimelineClashes(WebResult.Data) };" & vbCrLf &
"//        Singular.SendCommand('CheckSchedules', {}, function (d) {" & vbCrLf &
"//          Singular.HideLoadingBar();" & vbCrLf &
"//          if (self.IsValid()) {" & vbCrLf &
"//            //self.GetParent().ProductionTimelineList.ProcessOptions.SortOptions({ SortProperty: 'StartDateTime', SortAsc: true });" & vbCrLf &
"//          }" & vbCrLf &
"//        });" & vbCrLf &
"//      })" & vbCrLf &
"    }" & vbCrLf &
"  }"

Public Const VehicleIDValid As String = "//only check this rule for OB's" & vbCrLf &
"  if (self.GetParent().ProductionAreaID() == 1) {" & vbCrLf &
"    var Timeline = self;" & vbCrLf &
"    var ProductionSystemArea = Timeline.GetParent();" & vbCrLf &
"    if (Timeline.ProductionTimelineTypeID()) {" & vbCrLf &
"      switch (ProductionSystemArea.ProductionAreaID()) {" & vbCrLf &
"        case 1:" & vbCrLf &
"          CheckVanRulesOB();" & vbCrLf &
"          break;" & vbCrLf &
"        default:" & vbCrLf &
"          break;" & vbCrLf &
"      }" & vbCrLf &
"    }" & vbCrLf &
"  };" & vbCrLf &
"" & vbCrLf &
"  function CheckVanRulesOB() {" & vbCrLf &
"    var AllowedVehicleTimelineTypes = [1, 14, 22, 25, 26];" & vbCrLf &
"    var VehicleSelected = ((Timeline.VehicleID()) ? true : false);" & vbCrLf &
"    if (AllowedVehicleTimelineTypes.indexOf(Timeline.ProductionTimelineTypeID()) < 0 && VehicleSelected) {" & vbCrLf &
"      CtlError.AddError(""Vehicle can only be selected for vehicle/driver pre/post types"")" & vbCrLf &
"    }" & vbCrLf &
"  }"

Public Const TempTimelineValid As String = "//only check this rule for OB's" & vbCrLf &
"  if (self.GetParent().ProductionAreaID() == 1) {" & vbCrLf &
"    var Timeline = self;" & vbCrLf &
"    var ProductionSystemArea = Timeline.GetParent();" & vbCrLf &
"    var Production = ProductionSystemArea.GetParent();" & vbCrLf &
"    var StatusValid = (Production.PlanningFinalised() || Production.Reconciled());" & vbCrLf &
"    //var InvalidStatuses = [3, 4]; //Planning Finalised, Reconciled" & vbCrLf &
"    //var IsInvalidStatus = ((InvalidStatuses.indexOf(ProductionSystemArea.ProductionAreaStatusID()) >= 0) ? true : false);" & vbCrLf &
"    if (Timeline.ProductionTimelineTypeID() == 41 && StatusValid) {" & vbCrLf &
"      CtlError.AddError(""Temp Timelines must be removed before Planning finalised"");" & vbCrLf &
"    };" & vbCrLf &
"  };"

Public Const TimelineTypeIDValid As String = "//excluded"

Public Const SetProductionTimelineType As String = "ClientData.ROProductionAreaAllowedTimelineTypeList.Iterate(function (Item, Index) {" & vbCrLf &
"    if (Item.ProductionTimelineTypeID == self.ProductionTimelineTypeID()) {" & vbCrLf &
"      self.ProductionTimelineType(Item.ProductionTimelineType);" & vbCrLf &
"    };" & vbCrLf &
"  });"

Public Const SetEndTime As String = "var Timeline = self;" & vbCrLf &
"  if (!OBMisc.Dates.SameDay(Timeline.StartDateTime(), Timeline.EndDateTime())) {" & vbCrLf &
"    //End Time" & vbCrLf &
"    var ED = new Date(Timeline.EndDateTime());" & vbCrLf &
"    var Hours = ED.getHours();" & vbCrLf &
"    var Minutes = ED.getMinutes();" & vbCrLf &
"    //Start Time" & vbCrLf &
"    var SD = new Date(Timeline.StartDateTime());" & vbCrLf &
"    var Year = SD.getFullYear();" & vbCrLf &
"    var Month = SD.getMonth();" & vbCrLf &
"    var Day = SD.getDate();" & vbCrLf &
"    //New End Date" & vbCrLf &
"    var NewEndDate = new Date(Year, Month, Day, Hours, Minutes, 0);" & vbCrLf &
"    self.SuspendChanges = true" & vbCrLf &
"    Timeline.EndDateTime(NewEndDate);" & vbCrLf &
"    self.SuspendChanges = false" & vbCrLf &
"  };"

Public Const CheckVehicleValid As String = "if (self.IsValid) {" & vbCrLf &
"    var ExcludedTimelineTypes = [2, 3, 5, 7, 8, 12, 13, 20, 22, 25, 26, 28, 29, 32, 33, 34, 35, 36, 37, 41, 42, 43, 46, 47, 48, 49];" & vbCrLf &
"    var Tx = [11];" & vbCrLf &
"    if (ExcludedTimelineTypes.indexOf(self.ProductionTimelineTypeID()) < 0 && Tx.indexOf(self.ProductionTimelineTypeID()) >= 0) {" & vbCrLf &
"      Singular.SendCommand('CheckVehiclesValid', {}, function (d) { })" & vbCrLf &
"    }" & vbCrLf &
"  }"

Public Const StartDateTimeValid As String = "var td = new Date(self.TimelineDate());" & vbCrLf &
"  var sd = new Date(self.StartDateTime());" & vbCrLf &
"  var ed = new Date(self.EndDateTime());" & vbCrLf &
"  if (!moment(sd).isSame(td, 'day')) {" & vbCrLf &
"    CtlError.AddError(""Start Date and Start Time must be on the same day"");" & vbCrLf &
"  }" & vbCrLf &
"  if (moment(sd).isAfter(ed, 'minute') || moment(sd).isSame(ed, 'minute')) {" & vbCrLf &
"    CtlError.AddError(""End Time cannot be before Start Time"");" & vbCrLf &
"  }"

Public Const EndDateTimeValid As String = "var td = new Date(self.TimelineDate());" & vbCrLf &
"  var sd = new Date(self.StartDateTime());" & vbCrLf &
"  var ed = new Date(self.EndDateTime());" & vbCrLf &
"  if (!moment(ed).isSame(td, 'day')) {" & vbCrLf &
"    CtlError.AddError(""Start Date and End Time must be on the same day"");" & vbCrLf &
"  }" & vbCrLf &
"  if (moment(sd).isAfter(ed, 'minute') || moment(sd).isSame(ed, 'minute')) {" & vbCrLf &
"    CtlError.AddError(""End Time cannot be before Start Time"");" & vbCrLf &
"  }"

Public Const TimelineDateSet As String = "ProductionHelper.ProductionTimelines.Events.TimelineDateSet(self);"

Public Const StartTimeSet As String = "ProductionHelper.ProductionTimelines.Events.StartTimeSet(self);"

Public Const EndTimeSet As String = "ProductionHelper.ProductionTimelines.Events.EndTimeSet(self);"

Public Const ProductionTimelineTypeIDSet As String = "ProductionHelper.ProductionTimelines.Events.ProductionTimelineTypeIDSet(self);"

Public Const VehicleIDSet As String = "ProductionHelper.ProductionTimelines.Events.VehicleIDSet(self);"

end class
end namespace
