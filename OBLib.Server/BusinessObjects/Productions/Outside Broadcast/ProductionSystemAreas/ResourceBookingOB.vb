﻿' Generated 13 Jun 2015 16:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Resources.ReadOnly
Imports OBLib.Productions.Base
Imports OBLib.Helpers.Templates
Imports OBLib.Productions.Schedules

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Productions.OB

Namespace Resources

  <Serializable()> _
  Public Class ResourceBookingOB
    Inherits ResourceBookingBase(Of ResourceBookingOB)
    'Implements IResourceBooking

#Region " Properties and Methods "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

#Region " Properties "

    <Display(Name:="Starts", Description:="")>
    Public ReadOnly Property Starts() As String
      Get
        If Me.StartDateTime IsNot Nothing Then
          Return Me.StartDateTime.Value.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Ends", Description:="")>
    Public ReadOnly Property Ends() As String
      Get
        If Me.EndDateTime IsNot Nothing Then
          Return Me.EndDateTime.Value.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared ClashCountProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.ClashCount, 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Expanded")>
    Public Property ClashCount() As Integer
      Get
        Return GetProperty(ClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ClashCountProperty, Value)
      End Set
    End Property

#Region " Non Database Properties "

    'Public Overrides Property ObjType As String = "OBLib.Resources.ResourceBookingOB, OBLib"

#End Region

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceBookingDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Resource Booking")
        Else
          Return String.Format("Blank {0}", "Resource Booking")
        End If
      Else
        Return Me.ResourceBookingDescription
      End If

    End Function

    Public Function GetParentList() As ResourceBookingOBList
      Return CType(Me.Parent, ResourceBookingOBList)
    End Function

    Public Function GetParent() As ProductionHROB
      If GetParentList() IsNot Nothing Then
        Return CType(GetParentList.Parent, ProductionHROB)
      End If
      Return Nothing
    End Function

    'Public Sub AddUnSelectedTimeline(UnSelectedTimeline As UnSelectedTimeline)

    '  Dim phr As OBLib.Productions.Areas.ProductionHumanResource = Me.GetParent.ExistingProductionHumanResources(0)
    '  Dim csd As CrewScheduleDetailOB = New CrewScheduleDetailOB(phr, UnSelectedTimeline.ProductionTimeline)
    '  Me.CrewScheduleDetailOBList.Add(csd)
    '  csd.ProductionTimelineID = UnSelectedTimeline.ProductionTimeline.ProductionTimelineID
    '  csd.ProductionTimelineType = UnSelectedTimeline.ProductionTimeline.ProductionTimelineType
    '  csd.TimesheetDate = UnSelectedTimeline.ProductionTimeline.TimelineDate
    '  csd.StartDateTime = UnSelectedTimeline.ProductionTimeline.StartDateTime
    '  csd.EndDateTime = UnSelectedTimeline.ProductionTimeline.EndDateTime
    '  csd.HumanResourceID = Me.GetParent.HumanResourceID
    '  csd.HumanResourceScheduleTypeID = OBLib.CommonData.Enums.HumanResourceScheduleType.Production

    'End Sub

    'Public Function GetResourceBookingHelper() As Helpers.ResourceHelpers.ResourceBooking Implements IResourceBooking.GetResourceBookingHelper
    '  Return New Helpers.ResourceHelpers.ResourceBooking(Me.Guid, Me.ResourceID, Me.ResourceBookingID, Me.StartDateTimeBuffer, Me.StartDateTime, Me.EndDateTime, Me.EndDateTimeBuffer)
    'End Function

    Public Sub CopySchedule(HRProductionScheduleDetailList As HRProductionScheduleDetailList)
      HRProductionScheduleDetailList.ToList.ForEach(Sub(csd)
                                                      Dim newCSD As New CrewScheduleDetailOB(csd)
                                                      Me.CrewScheduleDetailOBList.Add(newCSD)
                                                    End Sub)
    End Sub

#End Region

#Region " Child Lists "

    Public Shared CrewScheduleDetailOBListProperty As PropertyInfo(Of CrewScheduleDetailOBList) = RegisterProperty(Of CrewScheduleDetailOBList)(Function(c) c.CrewScheduleDetailOBList, "Crew Schedule Detail OB List")
    Public ReadOnly Property CrewScheduleDetailOBList() As CrewScheduleDetailOBList
      Get
        If GetProperty(CrewScheduleDetailOBListProperty) Is Nothing Then
          LoadProperty(CrewScheduleDetailOBListProperty, Productions.OB.CrewScheduleDetailOBList.NewCrewScheduleDetailOBList())
        End If
        Return GetProperty(CrewScheduleDetailOBListProperty)
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()
      With AddWebRule(HasClashesProperty)
        '.JavascriptRuleFunctionName = "ResourceBookingOBBO.HasClashesValid"
        .AddTriggerProperty(ClashCountProperty)
        .ServerRuleFunction = AddressOf HasClashesValid
        .AffectedProperties.Add(StartDateTimeProperty)
        .AffectedProperties.Add(EndDateTimeProperty)
      End With
    End Sub

    Public Function HasClashesValid(ResourceBookingOB As ResourceBookingOB) As String
      If ResourceBookingOB.ClashCount > 0 Then
        Return ResourceBookingOB.ClashCount.ToString & " clashes detected"
      Else
        Return ""
      End If
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceBooking() method.

    End Sub

    Public Shared Function NewResourceBooking() As ResourceBooking

      Return DataPortal.CreateChild(Of ResourceBooking)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetResourceBookingOB(dr As SafeDataReader) As ResourceBookingOB

      Dim r As New ResourceBookingOB()
      r.Fetch(dr)
      Return r

    End Function

    Friend Overrides Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insResourceBookingOB"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Overrides Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updResourceBookingOB"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
          If Me.IsNew Then
            paramResourceBookingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
          .Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
          .Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
          .Parameters.AddWithValue("@StartDateTimeBuffer", (New SmartDate(GetProperty(StartDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTimeBuffer", (New SmartDate(GetProperty(EndDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@HumanResourceID", NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
          .Parameters.AddWithValue("@VehicleID", NothingDBNull(GetProperty(VehicleIDProperty)))
          .Parameters.AddWithValue("@EquipmentID", NothingDBNull(GetProperty(EquipmentIDProperty)))
          .Parameters.AddWithValue("@ChannelID", NothingDBNull(GetProperty(ChannelIDProperty)))
          .Parameters.AddWithValue("ModifiedBy", CurrentUser.UserID)
          .Parameters.AddWithValue("@HumanResourceShiftID", NothingDBNull(GetProperty(HumanResourceShiftIDProperty)))
          .Parameters.AddWithValue("@IsCancelled", GetProperty(IsCancelledProperty))
          .Parameters.AddWithValue("@HumanResourceOffPeriodID", NothingDBNull(GetProperty(HumanResourceOffPeriodIDProperty)))
          .Parameters.AddWithValue("@HumanResourceSecondmentID", NothingDBNull(GetProperty(HumanResourceSecondmentIDProperty)))
          .Parameters.AddWithValue("@ProductionHRID", NothingDBNull(Me.GetParent.ProductionHRID))
          .Parameters.AddWithValue("@StatusCssClass", GetProperty(StatusCssClassProperty))
          .Parameters.AddWithValue("@IgnoreClashes", GetProperty(IgnoreClashesProperty))
          .Parameters.AddWithValue("@IgnoreClashesReason", GetProperty(IgnoreClashesReasonProperty))
          .Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent.ProductionSystemAreaID)
          .Parameters.AddWithValue("@RoomScheduleID", NothingDBNull(GetProperty(RoomScheduleIDProperty)))
          .Parameters.AddWithValue("@IsLocked", GetProperty(IsLockedProperty))
          .Parameters.AddWithValue("@IsLockedReason", GetProperty(IsLockedReasonProperty))
          .Parameters.AddWithValue("@IsTBC", GetProperty(IsTBCProperty))
          .Parameters.AddWithValue("@IsBeingEditedBy", "")
          .Parameters.AddWithValue("@InEditDateTime", NothingDBNull(Nothing))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ResourceBookingIDProperty, paramResourceBookingID.Value)
          End If
          ' update child objects
          If GetProperty(CrewScheduleDetailOBListProperty) IsNot Nothing Then
            Me.CrewScheduleDetailOBList.Update()
          End If
          MarkOld()
        End With
      Else
        If GetProperty(CrewScheduleDetailOBListProperty) IsNot Nothing Then
          Me.CrewScheduleDetailOBList.Update()
        End If
      End If

    End Sub

    Friend Overrides Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delResourceBookingOB"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
        cm.Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
        cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUser.UserID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Public Overrides Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)
      LoadProperty(ProductionHRIDProperty, ZeroNothing(sdr.GetInt32(15)))
    End Sub

    Public Overrides Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)

    End Sub

    Public Overrides Sub UpdateChildList()
      'Me.CrewScheduleDetailOBList.Update()
    End Sub

    Public Overrides Sub DeleteChildren()

    End Sub

    Sub Fiddle()
      MarkNew()
      MarkDirty()
      SetProperty(ResourceBookingIDProperty, 0)
    End Sub

  End Class

End Namespace