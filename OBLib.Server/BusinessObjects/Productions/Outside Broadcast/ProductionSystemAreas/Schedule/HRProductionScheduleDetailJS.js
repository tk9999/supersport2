﻿function HRProductionScheduleDetailToString() { return self.GetParent().HumanResource() + ' - ' + self.ProductionTimelineType() }

function StartDateTimeValid(Value, Rule, CtlError) {
  ProductionHelper.Schedules.Rules.StartDateTimeValid(Value, Rule, CtlError);
}

function EndDateTimeValid(Value, Rule, CtlError) {
  ProductionHelper.Schedules.Rules.EndDateTimeValid(Value, Rule, CtlError);
}

function StartTimeSet(self) {
  ProductionHelper.Schedules.Events.StartDateTimeSet(self);
}

function EndTimeSet(self) {
  ProductionHelper.Schedules.Events.EndDateTimeSet(self);
}