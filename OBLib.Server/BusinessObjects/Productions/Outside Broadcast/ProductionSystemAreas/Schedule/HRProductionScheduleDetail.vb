﻿' Generated 21 Jul 2014 14:59 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Areas
Imports Singular.DataAnnotations
Imports OBLib.Productions.OB
Imports OBLib.Biometrics

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Schedules

  <Serializable()> _
  Public Class HRProductionScheduleDetail
    Inherits SingularBusinessBase(Of HRProductionScheduleDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="HR", Description:=""), Key()>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(HumanResourceIDProperty, value)
      End Set
    End Property

    Public Shared CrewScheduleDetailIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewScheduleDetailID, "Crew Schedule Detail", Nothing)
    ''' <summary>
    ''' Gets and sets the Crew Schedule Detail value
    ''' </summary>
    <Display(Name:="CSD", Description:="")>
    Public Property CrewScheduleDetailID() As Integer?
      Get
        Return GetProperty(CrewScheduleDetailIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewScheduleDetailIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionHumanResourceID, "Production Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Human Resource value
    ''' </summary>
    <Display(Name:="PHR", Description:="")>
    Public Property ProductionHumanResourceID() As Integer?
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTimelineID, "Production Timeline", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Timeline value
    ''' </summary>
    <Display(Name:="Timeline", Description:="")>
    Public Property ProductionTimelineID() As Integer?
      Get
        Return GetProperty(ProductionTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared TimesheetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TimesheetDate, "Timesheet Date")
    ''' <summary>
    ''' Gets and sets the Timesheet Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public Property TimesheetDate As DateTime?
      Get
        Return GetProperty(TimesheetDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TimesheetDateProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing) _
                                                                        .AddSetExpression(OBLib.JSCode.HRProductionScheduleDetailJS.StartTimeSet, False)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property
    '                                                                        

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing) _
                                                                      .AddSetExpression(OBLib.JSCode.HRProductionScheduleDetailJS.EndTimeSet, False)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property
    '                                                                       

    Public Shared DetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Detail, "Detail")
    ''' <summary>
    ''' Gets and sets the Detail value
    ''' </summary>
    <Display(Name:="Detail", Description:=""),
    StringLength(50, ErrorMessage:="Detail cannot be more than 50 characters")>
    Public Property Detail() As String
      Get
        Return GetProperty(DetailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DetailProperty, Value)
      End Set
    End Property

    Public Shared ExcludeIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ExcludeInd, "Exclude", False)
    ''' <summary>
    ''' Gets and sets the Exclude value
    ''' </summary>
    <Display(Name:="Exclude", Description:="")>
    Public Property ExcludeInd() As Boolean
      Get
        Return GetProperty(ExcludeIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ExcludeIndProperty, Value)
      End Set
    End Property

    Public Shared ExcludeReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ExcludeReason, "Exclude Reason")
    ''' <summary>
    ''' Gets and sets the Exclude Reason value
    ''' </summary>
    <Display(Name:="Exclude Reason", Description:=""),
    StringLength(50, ErrorMessage:="Exclude Reason cannot be more than 50 characters")>
    Public Property ExcludeReason() As String
      Get
        Return GetProperty(ExcludeReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ExcludeReasonProperty, Value)
      End Set
    End Property

    Public Shared CrewTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTimesheetID, "Crew Timesheet", Nothing)
    ''' <summary>
    ''' Gets and sets the Crew Timesheet value
    ''' </summary>
    <Display(Name:="Timesheet", Description:="")>
    Public Property CrewTimesheetID() As Integer?
      Get
        Return GetProperty(CrewTimesheetIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTimesheetIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionTimelineTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTimelineTypeID, "Production Timeline Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Timeline Type", Description:="")>
    Public Property ProductionTimelineTypeID() As Integer?
      Get
        Return GetProperty(ProductionTimelineTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTimelineTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTimelineType, "Production Timeline Type")
    ''' <summary>
    ''' Gets and sets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Timeline Type", Description:="")>
    Public Property ProductionTimelineType() As String
      Get
        Return GetProperty(ProductionTimelineTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTimelineTypeProperty, Value)
      End Set
    End Property

    Public Shared ClashProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Clash, "Clash")
    ''' <summary>
    ''' Gets and sets the Clash value
    ''' </summary>
    <Display(Name:="Clash", Description:="")>
    Public Property Clash() As String
      Get
        Return GetProperty(ClashProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ClashProperty, Value)
      End Set
    End Property

    Public Shared FreeIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FreeInd, "Free?", False)
    ''' <summary>
    ''' Gets and sets the Exclude value
    ''' </summary>
    <Display(Name:="Free?", Description:="")>
    Public Property FreeInd() As Boolean
      Get
        Return GetProperty(FreeIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(FreeIndProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, OBLib.JSCode.HRProductionScheduleDetailJS.HRProductionScheduleDetailToString)

    <Browsable(False)>
    Public Property ProductionHumanResource As ProductionHumanResource

    <Browsable(False)>
    Public Property ProductionTimeline As ProductionTimeline

    Public Shared VisibleProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Visible, "Visible", True)
    ''' <summary>
    ''' Gets and sets the Exclude value
    ''' </summary>
    <Display(Name:="Visible", Description:=""),
    ClientOnly()>
    Public Property Visible() As Boolean
      Get
        Return GetProperty(VisibleProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(VisibleProperty, Value)
      End Set
    End Property

    Public Shared SelectedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Selected, "Selected?", False)
    ''' <summary>
    ''' Gets and sets the Exclude value
    ''' </summary>
    <Display(Name:="Selected?", Description:="")>
    Public Property Selected() As Boolean
      Get
        Return GetProperty(SelectedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectedProperty, Value)
      End Set
    End Property

    Public Shared AddedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Added, "Free?", False)
    ''' <summary>
    ''' Gets and sets the Exclude value
    ''' </summary>
    <Display(Name:="Free?", Description:="")>
    Public Property Added() As Boolean
      Get
        Return GetProperty(AddedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(AddedProperty, Value)
      End Set
    End Property

    Public Shared UpdatedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Updated, "Free?", False)
    ''' <summary>
    ''' Gets and sets the Exclude value
    ''' </summary>
    <Display(Name:="Free?", Description:="")>
    Public Property Updated() As Boolean
      Get
        Return GetProperty(UpdatedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(UpdatedProperty, Value)
      End Set
    End Property

    Public Shared DeletedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Deleted, "Free?", False)
    ''' <summary>
    ''' Gets and sets the Exclude value
    ''' </summary>
    <Display(Name:="Free?", Description:="")>
    Public Property Deleted() As Boolean
      Get
        Return GetProperty(DeletedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DeletedProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineGuidProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTimelineGuid, "ProductionTimelineGuid")
    ''' <summary>
    ''' Gets and sets the All Clashes value
    ''' </summary>
    <Display(Name:="ProductionTimelineGuid", Description:="")>
    Public Property ProductionTimelineGuid() As String
      Get
        Return GetProperty(ProductionTimelineGuidProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTimelineGuidProperty, Value)
      End Set
    End Property

    Public Shared ProductionHumanResourceGuidProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionHumanResourceGuid, "ProductionHumanResourceGuid")
    ''' <summary>
    ''' Gets and sets the All Clashes value
    ''' </summary>
    <Display(Name:="ProductionHumanResourceGuid", Description:="")>
    Public Property ProductionHumanResourceGuid() As String
      Get
        Return GetProperty(ProductionHumanResourceGuidProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionHumanResourceGuidProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Shift", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:="")>
    Public Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceOffPeriodID, "Off Period", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Off Period value
    ''' </summary>
    <Display(Name:="Human Resource Off Period", Description:="the off period (leave) that the scheduled item is linked to")>
    Public Property HumanResourceOffPeriodID() As Integer?
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceOffPeriodIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", OBLib.Security.Settings.CurrentUser.SystemID)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required(ErrorMessage:="System required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:=""),
    Required(ErrorMessage:="Production Area required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceScheduleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceScheduleTypeID, "Human Resource Schedule Type", 1)
    ''' <summary>
    ''' Gets and sets the Human Resource Schedule Type value
    ''' </summary>
    <Display(Name:="Human Resource Schedule Type", Description:="the type of booking"),
    Required(ErrorMessage:="Human Resource Schedule Type required")>
    Public Property HumanResourceScheduleTypeID() As Integer?
      Get
        Return GetProperty(HumanResourceScheduleTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceScheduleTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineMatchIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ProductionTimelineMatchInd, "Production Timeline Match", True)
    ''' <summary>
    ''' Gets and sets the Production Timeline Match value
    ''' </summary>
    <Display(Name:="Production Timeline Match", Description:="Tick indicates that this human resource's schedule must always match the production's schedule. Should this be unticked the system will not automatically update the HR schedule to match the producion's schedule"),
    Browsable(False)>
    Public Property ProductionTimelineMatchInd() As Boolean
      Get
        Return GetProperty(ProductionTimelineMatchIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ProductionTimelineMatchIndProperty, Value)
      End Set
    End Property

    Public Shared EndTimeOffsetProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EndTimeOffset, "End Time Offset", 0)
    ''' <summary>
    ''' Gets and sets the End Time Offset value
    ''' </summary>
    <Display(Name:="End Time Offset", Description:="The number of hours and minutes to offset the calculated end time by"),
    Required(ErrorMessage:="End Time Offset required"),
    Browsable(False)>
    Public Property EndTimeOffset() As Integer
      Get
        Return GetProperty(EndTimeOffsetProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EndTimeOffsetProperty, Value)
      End Set
    End Property

    Public Shared StartTimeOffsetProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StartTimeOffset, "Start Time Offset", 0)
    ''' <summary>
    ''' Gets and sets the Start Time Offset value
    ''' </summary>
    <Display(Name:="Start Time Offset", Description:="The number of hours and minutes to offset the calculated start time by"),
    Browsable(False)>
    Public Property StartTimeOffset() As Integer
      Get
        Return GetProperty(StartTimeOffsetProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(StartTimeOffsetProperty, Value)
      End Set
    End Property

    Public Shared TimesheetIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.TimesheetInd, "Timeline Indicator", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets the Timeline Ind value
    ''' </summary>
    <Display(Name:="Timeline?", Description:="True if a timesheet has been created for the schedule")>
    Public ReadOnly Property TimesheetInd() As Boolean?
      Get
        Return GetProperty(TimesheetIndProperty)
      End Get
    End Property

    Public Shared InvoiceDetailIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InvoiceDetailInd, "Invoice Detail Indicator", False)
    ''' <summary>
    ''' Gets the Invoice Detail Ind value
    ''' </summary>
    <Display(Name:="Invoice Detail?", Description:="True if an invoice has been created for the schedule")>
    Public ReadOnly Property InvoiceDetailInd() As Boolean
      Get
        Return GetProperty(InvoiceDetailIndProperty)
      End Get
    End Property

    Public Shared TimesheetMonthClosedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TimesheetMonthClosedInd, "Timesheet Month Closed", False)
    ''' <summary>
    ''' Gets the Invoice Detail Ind value
    ''' </summary>
    <Display(Name:="Timesheet Month Closed?", Description:="True if the timesheet month is closed")>
    Public ReadOnly Property TimesheetMonthClosedInd() As Boolean
      Get
        Return GetProperty(TimesheetMonthClosedIndProperty)
      End Get
    End Property

    Public Shared InvoiceNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.InvoiceNo, "Invoice Number", "")
    ''' <summary>
    ''' Gets the Invoice Number value
    ''' </summary>
    <Display(Name:="Invoice Number", Description:="Invoice Number for the timesheet")>
    Public ReadOnly Property InvoiceNo() As String
      Get
        Return GetProperty(InvoiceNoProperty)
      End Get
    End Property

    Public Shared TimesheetMonthProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TimesheetMonth, "Timesheet Month", "")
    ''' <summary>
    ''' Gets the Timesheet Month value
    ''' </summary>
    <Display(Name:="Timesheet Month", Description:="Timesheet Month")>
    Public ReadOnly Property TimesheetMonth() As String
      Get
        Return GetProperty(TimesheetMonthProperty)
      End Get
    End Property

    <Browsable(False)>
    Public ReadOnly Property ProductionHR As ProductionHROB
      Get
        Return Me.GetParent.GetParent.ProductionHROBList.Where(Function(d) CompareSafe(d.HumanResourceID, Me.HumanResourceID)).FirstOrDefault
      End Get
    End Property

    Public Shared DirtifyProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Dirtify, "Timesheet Month", False)
    Public Property Dirtify As Boolean
      Get
        Return GetProperty(DirtifyProperty)
      End Get
      Set(value As Boolean)
        SetProperty(DirtifyProperty, value)
      End Set
    End Property


    Public Shared OrigStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OrigStartDateTime, "")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Orig Start Date", Description:="")>
    Public ReadOnly Property OrigStartDateTime As DateTime?
      Get
        Return GetProperty(OrigStartDateTimeProperty)
      End Get
    End Property
    '                                                                        

    Public Shared OrigEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OrigEndDateTime, "")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public ReadOnly Property OrigEndDateTime As DateTime?
      Get
        Return GetProperty(OrigEndDateTimeProperty)
      End Get
    End Property

    Public Shared AccessShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessShiftID, "Biometrics Access Shift", Nothing)
    ''' <summary>
    '''  
    ''' </summary>
    <Display(Name:="Access shift", Description:="Biometrics Access shift")>
    Public ReadOnly Property AccessShiftID() As Integer?
      Get
        Return GetProperty(AccessShiftIDProperty)
      End Get
    End Property


    Private mAccessShift As AccessShift
    Public Shared AccessFlagIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessFlagID, "Biometrics Access Flag", Nothing)
    ''' <summary>
    ''' Gets he total of Biomentics Access logs
    ''' </summary>
    <Display(Name:="Access Flag", Description:="Biometrics Access Flag (Total)")>
    Public ReadOnly Property AccessFlagID() As Integer
      Get
        Return GetProperty(AccessFlagIDProperty)
      End Get
    End Property

    Public Shared BioStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.BioStartDateTime, "")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Bio Start Date", Description:="")>
    Public ReadOnly Property BioStartDateTime As DateTime?
      Get
        Return GetProperty(BioStartDateTimeProperty)
      End Get
    End Property

    Public Shared BioEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.BioEndDateTime, "")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public ReadOnly Property BioEndDateTime As DateTime?
      Get
        Return GetProperty(BioEndDateTimeProperty)
      End Get
    End Property
#End Region

#Region " Methods "

    Public Function GetParent() As HRProductionSchedule

      Return CType(CType(Me.Parent, HRProductionScheduleDetailList).Parent, HRProductionSchedule)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewScheduleDetailIDProperty)
      'HumanResourceIDProperty

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionHumanResource.HumanResource & " - " & Me.ProductionTimeline.ProductionTimelineType & " - " & Me.ProductionTimeline.StartDateTime.Value.ToString("dd-MMM-yy HH:mm")

    End Function

    Public Function CanEditScheduleCheck() As Boolean

      If OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
        Return True
      End If

      If CompareSafe(Me.GetParent.ContractTypeID, CInt(CommonData.Enums.ContractType.Freeleancer)) Then

        If OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
          Return True
        End If

        If GetProperty(TimesheetIndProperty) AndAlso GetProperty(InvoiceDetailIndProperty) Then
          Return False
        End If

      Else

        If OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
          Return True
        End If

        If TimesheetInd IsNot Nothing Then
          If GetProperty(TimesheetIndProperty) Then
            'Dim tm As Timesheets.TimesheetMonth = CommonData.Lists.TimesheetMonthList.GetItem(mTimesheetDate.Date)
            If GetProperty(TimesheetMonthClosedIndProperty) Then
              Return False
            End If
          End If

        Else
          Dim tm As OBLib.Maintenance.General.ReadOnly.ROTimesheetMonthOld = OBLib.CommonData.Lists.ROTimesheetMonthListOld.GetItem(StartDateTime.Value.Date) 'mTimesheetDate.Date)
          If tm IsNot Nothing AndAlso tm.ClosedInd Then
            Return False
          End If

        End If

      End If

      Return True

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      'AddWebRule(ClashProperty,
      '           Function(d) d.Clash <> "",
      '           Function(d) d.Clash)

      With AddWebRule(StartDateTimeProperty)
        .JavascriptRuleCode = OBLib.JSCode.HRProductionScheduleDetailJS.StartDateTimeValid
        .AddTriggerProperties({TimesheetDateProperty})
      End With

      'With AddWebRule(TimesheetDateProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.HRProductionScheduleDetailJS.StartDateTimeValid
      'End With

      'With AddWebRule(JustAddedProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.HRProductionScheduleDetailJS.StartDateTimeValid
      'End With

      With AddWebRule(EndDateTimeProperty)
        .JavascriptRuleCode = OBLib.JSCode.HRProductionScheduleDetailJS.EndDateTimeValid
        .AddTriggerProperties({TimesheetDateProperty})
      End With

      'With AddWebRule(StartDateTimeProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.HRProductionScheduleDetailJS.ScheduleSameDate
      'End With

      'With AddWebRule(EndDateTimeProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.HRProductionScheduleDetailJS.ScheduleSameDate
      'End With

      'With AddWebRule(StartDateTimeProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.HRProductionScheduleDetailJS.StartDateTimeBeforeEndDateTime
      'End With

      'With AddWebRule(EndDateTimeProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.HRProductionScheduleDetailJS.StartDateTimeBeforeEndDateTime
      'End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHRProductionScheduleDetail() method.

    End Sub

    Public Shared Function NewHRProductionScheduleDetail() As HRProductionScheduleDetail

      Return DataPortal.CreateChild(Of HRProductionScheduleDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHRProductionScheduleDetail(dr As SafeDataReader, ByVal ProductionHumanResource As ProductionHumanResource, ByVal ProductionTimeline As ProductionTimeline) As HRProductionScheduleDetail

      Dim h As New HRProductionScheduleDetail()
      h.ProductionHumanResource = ProductionHumanResource
      h.ProductionTimeline = ProductionTimeline
      If ProductionTimeline IsNot Nothing Then
        h.ProductionTimelineGuid = ProductionTimeline.Guid.ToString
      End If
      h.ProductionHumanResourceGuid = ProductionHumanResource.Guid.ToString
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceIDProperty, .GetInt32(0))
          LoadProperty(CrewScheduleDetailIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(TimesheetDateProperty, .GetValue(4))
          LoadProperty(StartDateTimeProperty, .GetValue(5))
          LoadProperty(EndDateTimeProperty, .GetValue(6))
          LoadProperty(DetailProperty, .GetString(7))
          LoadProperty(ExcludeIndProperty, .GetBoolean(8))
          LoadProperty(ExcludeReasonProperty, .GetString(9))
          LoadProperty(CrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(CreatedByProperty, .GetInt32(11))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(12))
          LoadProperty(ModifiedByProperty, .GetInt32(13))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(14))
          LoadProperty(ProductionTimelineTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(ProductionTimelineTypeProperty, .GetString(16))
          LoadProperty(ClashProperty, .GetString(17))
          LoadProperty(FreeIndProperty, .GetBoolean(18))
          LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
          LoadProperty(HumanResourceOffPeriodIDProperty, Singular.Misc.ZeroNothing(.GetInt32(20)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
          LoadProperty(HumanResourceScheduleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
          LoadProperty(StartTimeOffsetProperty, .GetInt32(24))
          LoadProperty(EndTimeOffsetProperty, .GetInt32(25))
          LoadProperty(ProductionTimelineMatchIndProperty, .GetBoolean(26))
          LoadProperty(TimesheetIndProperty, .GetBoolean(27))
          LoadProperty(InvoiceDetailIndProperty, .GetBoolean(28))
          LoadProperty(TimesheetMonthClosedIndProperty, .GetBoolean(29))
          LoadProperty(InvoiceNoProperty, .GetString(30))
          LoadProperty(TimesheetMonthProperty, .GetString(31))
          LoadProperty(BioStartDateTimeProperty, .GetValue(32))
          LoadProperty(BioEndDateTimeProperty, .GetValue(33))
          LoadProperty(AccessShiftIDProperty, .GetInt32(34))
          LoadProperty(AccessFlagIDProperty, .GetInt32(35))

          LoadProperty(OrigStartDateTimeProperty, StartDateTime)
          LoadProperty(OrigEndDateTimeProperty, EndDateTime)
          'LoadProperty(C, .GetInt32(29))
          'LoadProperty(JustAddedProperty, False)
        End With
      End Using
      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()
      Dim CanCommitInsert As Boolean = False
      If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices Then
        If Not GetParent().GetParent().GetParent().IsTimelineReady Then
          If Singular.Security.HasAccess("Productions", "Can Finalise Timeline") Then
            CanCommitInsert = True
          End If
        Else
          CanCommitInsert = True
        End If
      ElseIf OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
        CanCommitInsert = True
      End If
      If CanCommitInsert Then
        Using cm As SqlCommand = New SqlCommand
          cm.CommandText = "InsProcsWeb.insHRProductionScheduleDetail"
          DoInsertUpdateChild(cm)
        End Using
      End If
    End Sub

    Friend Sub Update()
      Dim CanCommitUpdate As Boolean = False
      If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices Then
        If Not GetParent().GetParent().GetParent().IsTimelineReady Then
          If Singular.Security.HasAccess("Productions", "Can Finalise Timeline") Then
            CanCommitUpdate = True
          End If
        Else
          CanCommitUpdate = True
        End If
      ElseIf OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
        CanCommitUpdate = True
      End If
      If CanCommitUpdate Then
        Using cm As SqlCommand = New SqlCommand
          cm.CommandText = "UpdProcsWeb.updHRProductionScheduleDetail"
          DoInsertUpdateChild(cm)
        End Using
      End If
    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If ProductionTimeline IsNot Nothing Then
        Dim tsd = ProductionTimeline.CalculateTimesheetDate
        If TimesheetDate Is Nothing OrElse CType(TimesheetDate, Date?) <> tsd Then
          TimesheetDate = tsd
        End If
      End If

      If ProductionTimeline IsNot Nothing AndAlso ProductionHumanResource IsNot Nothing Then
        If Me.IsSelfDirty Then
          With cm
            .CommandType = CommandType.StoredProcedure
            Dim paramCrewScheduleDetailID As SqlParameter = .Parameters.Add("@CrewScheduleDetailID", SqlDbType.Int)
            paramCrewScheduleDetailID.Value = IIf(GetProperty(CrewScheduleDetailIDProperty) Is Nothing, 0, GetProperty(CrewScheduleDetailIDProperty))
            If Me.IsNew Then
              paramCrewScheduleDetailID.Direction = ParameterDirection.Output
            End If
            .Parameters.AddWithValue("@ProductionHumanResourceID", Singular.Misc.NothingDBNull(Me.ProductionHumanResource.ProductionHumanResourceID))
            .Parameters.AddWithValue("@ProductionTimelineID", Singular.Misc.NothingDBNull(Me.ProductionTimeline.ProductionTimelineID))
            .Parameters.AddWithValue("@Detail", GetProperty(DetailProperty))
            .Parameters.AddWithValue("@StartTimeOffset", GetProperty(StartTimeOffsetProperty))
            .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
            .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
            .Parameters.AddWithValue("@EndTimeOffset", GetProperty(EndTimeOffsetProperty))
            .Parameters.AddWithValue("@ProductionTimelineMatchInd", GetProperty(ProductionTimelineMatchIndProperty))
            .Parameters.AddWithValue("@ExcludeInd", GetProperty(ExcludeIndProperty))
            .Parameters.AddWithValue("@ExcludeReason", GetProperty(ExcludeReasonProperty))
            .Parameters.AddWithValue("@CrewTimesheetID", Singular.Misc.NothingDBNull(GetProperty(CrewTimesheetIDProperty)))
            .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
            .Parameters.AddWithValue("@TimesheetDate", (New SmartDate(GetProperty(TimesheetDateProperty))).DBValue)
            .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
            .Parameters.AddWithValue("@HumanResourceOffPeriodID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceOffPeriodIDProperty)))
            .Parameters.AddWithValue("@HumanResourceScheduleTypeID", GetProperty(HumanResourceScheduleTypeIDProperty))
            .Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceShiftIDProperty)))
            .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUser.ProductionAreaID))
            .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUser.SystemID))
            .Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent().GetParent().ProductionSystemAreaID)
            .Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
            .Parameters.AddWithValue("@Source", "Old Web Productions Screen") 
            .Parameters.AddWithValue("@ProductionID", Me.GetParent().ProductionID)
            .Parameters.AddWithValue("@AccessShiftID", Singular.Misc.NothingDBNull(GetProperty(AccessShiftIDProperty)))

            .ExecuteNonQuery()
            If Me.IsNew Then
              LoadProperty(CrewScheduleDetailIDProperty, paramCrewScheduleDetailID.Value)
            End If
            ' update child objects
            ' mChildList.Update()
            MarkOld()
          End With
        Else
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Dim CanCommitDelete As Boolean = False

      If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices Then
        If Not GetParent().GetParent().GetParent().IsTimelineReady Then
          If Singular.Security.HasAccess("Productions", "Can Finalise Timeline") Then
            CanCommitDelete = True
          End If
        Else
          CanCommitDelete = True
        End If
      ElseIf OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
        CanCommitDelete = True
      End If

      If CanCommitDelete Then
        If Me.CanEditScheduleCheck Then
          Using cm As SqlCommand = New SqlCommand
            cm.CommandText = "DelProcsWeb.delHRProductionScheduleDetail"
            cm.CommandType = CommandType.StoredProcedure
            cm.Parameters.AddWithValue("@CrewScheduleDetailID", GetProperty(CrewScheduleDetailIDProperty))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent().GetParent().ProductionSystemAreaID)
            cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
            'If Me.ProductionHR Is Nothing Then

            'End If
            'cm.Parameters.AddWithValue("@ProductionHRID", Me.ProductionHR.ProductionHRID)
            'cm.Parameters.AddWithValue("@ResourceBookingID", Me.ProductionHR.ResourceBookingOBList(0).ResourceBookingID)
            DoDeleteChild(cm)
          End Using
        End If
      End If
      'If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices AndAlso Not GetParent().GetParent().GetParent().TimelineReadyInd Then
      '  If Singular.Security.HasAccess("Productions", "Can Delete Crew Schedule Details before Timeline Finalised") Then
      '    If Me.CanEditScheduleCheck Then
      '      Using cm As SqlCommand = New SqlCommand
      '        cm.CommandText = "DelProcsWeb.delHRProductionScheduleDetail"
      '        cm.CommandType = CommandType.StoredProcedure
      '        cm.Parameters.AddWithValue("@CrewScheduleDetailID", GetProperty(CrewScheduleDetailIDProperty))
      '        cm.Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent().GetParent().ProductionSystemAreaID)
      '        cm.Parameters.AddWithValue("@LoggedInUserID", Singular.Misc.NothingDBNull(OBLib.Security.Settings.CurrentUserID))
      '        DoDeleteChild(cm)
      '      End Using
      '    End If

      '  End If
      'Else

      'End If
    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Sub Fiddle()
      MarkNew()
      MarkDirty()
      SetProperty(CrewScheduleDetailIDProperty, 0)
    End Sub

  End Class

End Namespace