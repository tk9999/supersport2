﻿' Generated 07 Jul 2014 06:56 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Crew.ReadOnly

  <Serializable()> _
  Public Class ROSkilledHR
    Inherits SingularReadOnlyBase(Of ROSkilledHR)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Name")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Name", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract", Description:="")>
    Public ReadOnly Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryInd, "Prim?", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="Prim?", Description:="")>
    Public ReadOnly Property PrimaryInd() As Boolean
      Get
        Return GetProperty(PrimaryIndProperty)
      End Get
    End Property

    Public Shared AlreadyBookedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AlreadyBookedInd, "Already Booked")
    ''' <summary>
    ''' Gets the Already Booked value
    ''' </summary>
    <Display(Name:="Already Booked", Description:="")>
    Public ReadOnly Property AlreadyBookedInd() As Boolean
      Get
        Return GetProperty(AlreadyBookedIndProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="CityID")>
    Public ReadOnly Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="City")>
    Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="City")>
    Public ReadOnly Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
    End Property

    'Public Shared ProductionCountProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionCount, "Prds")
    ' ''' <summary>
    ' ''' Gets the ID value
    ' ''' </summary>
    '<Display(Name:="Prds")>
    'Public ReadOnly Property ProductionCount() As Integer?
    '  Get
    '    Return GetProperty(ProductionCountProperty)
    '  End Get
    'End Property

    Public Shared CrewScheduleHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.CrewScheduleHours, "Sched")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Sched")>
    Public ReadOnly Property CrewScheduleHours() As Decimal
      Get
        Return GetProperty(CrewScheduleHoursProperty)
      End Get
    End Property

    Public Shared TimesheetHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TimesheetHours, "Tsht")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Tsht")>
    Public ReadOnly Property TimesheetHours() As Decimal
      Get
        Return GetProperty(TimesheetHoursProperty)
      End Get
    End Property

    'Public Shared DaysWorkedProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DaysWorked, "Days")
    ' ''' <summary>
    ' ''' Gets the ID value
    ' ''' </summary>
    '<Display(Name:="Days")>
    'Public ReadOnly Property DaysWorked() As Integer?
    '  Get
    '    Return GetProperty(DaysWorkedProperty)
    '  End Get
    'End Property

    Public Shared ContractDaysProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractDays, "Ctr Days")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Ctr Days")>
    Public ReadOnly Property ContractDays() As Integer?
      Get
        Return GetProperty(ContractDaysProperty)
      End Get
    End Property

    <Display(Name:="Visible?", Description:=""), ClientOnly()>
    Public Property VisibleInd() As Boolean = True

    <Display(Name:="Already Added?", Description:=""), ClientOnly()>
    Public Property AlreadyAddedInd() As Boolean = False

#End Region

#Region " Child Lists "

    Public Shared ROSkilledHRPositionListProperty As PropertyInfo(Of ROSkilledHRPositionList) = RegisterProperty(Of ROSkilledHRPositionList)(Function(c) c.ROSkilledHRPositionList, "RO Skilled HR Position List")

    Public ReadOnly Property ROSkilledHRPositionList() As ROSkilledHRPositionList
      Get
        If GetProperty(ROSkilledHRPositionListProperty) Is Nothing Then
          LoadProperty(ROSkilledHRPositionListProperty, Productions.Crew.ReadOnly.ROSkilledHRPositionList.NewROSkilledHRPositionList())
        End If
        Return GetProperty(ROSkilledHRPositionListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSkilledHR(dr As SafeDataReader) As ROSkilledHR

      Dim r As New ROSkilledHR()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(HumanResourceProperty, .GetString(1))
        LoadProperty(ContractTypeProperty, .GetString(2))
        LoadProperty(PrimaryIndProperty, .GetBoolean(3))
        LoadProperty(AlreadyBookedIndProperty, .GetBoolean(4))
        LoadProperty(CityIDProperty, .GetInt32(5))
        LoadProperty(CityProperty, .GetString(6))
        LoadProperty(CityCodeProperty, .GetString(7))
        ' LoadProperty(ProductionCountProperty, .GetInt32(8))
        LoadProperty(CrewScheduleHoursProperty, .GetDecimal(9))
        LoadProperty(TimesheetHoursProperty, .GetDecimal(10))
        'LoadProperty(DaysWorkedProperty, .GetInt32(11))
        LoadProperty(ContractDaysProperty, .GetInt32(12))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace