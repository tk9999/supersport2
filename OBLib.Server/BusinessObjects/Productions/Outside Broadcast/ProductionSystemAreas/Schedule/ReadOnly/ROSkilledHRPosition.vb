﻿' Generated 07 Jul 2014 06:56 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Crew.ReadOnly

  <Serializable()> _
  Public Class ROSkilledHRPosition
    Inherits SingularReadOnlyBase(Of ROSkilledHRPosition)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Production Type")
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
    End Property

    Public Shared PositionLevelProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionLevel, "Position Level")
    ''' <summary>
    ''' Gets the Position Level value
    ''' </summary>
    <Display(Name:="Position Level", Description:="")>
    Public ReadOnly Property PositionLevel() As Integer
      Get
        Return GetProperty(PositionLevelProperty)
      End Get
    End Property

    Public Shared LevelDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LevelDescription, "Level Description")
    ''' <summary>
    ''' Gets the Level Description value
    ''' </summary>
    <Display(Name:="Level Description", Description:="")>
    Public ReadOnly Property LevelDescription() As String
      Get
        Return GetProperty(LevelDescriptionProperty)
      End Get
    End Property

    <Display(Name:="Visible?", Description:=""), ClientOnly()>
    Public Property VisibleInd() As Boolean = False

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Position

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSkilledHRPosition(dr As SafeDataReader) As ROSkilledHRPosition

      Dim r As New ROSkilledHRPosition()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDProperty, .GetInt32(0))
        LoadProperty(PositionProperty, .GetString(1))
        LoadProperty(ProductionTypeProperty, .GetString(2))
        LoadProperty(PositionLevelProperty, .GetInt32(3))
        LoadProperty(LevelDescriptionProperty, .GetString(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace