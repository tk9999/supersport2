﻿' Generated 07 Jul 2014 06:56 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Crew.ReadOnly

  <Serializable()> _
  Public Class ROSkilledHRPositionList
    Inherits SingularReadOnlyListBase(Of ROSkilledHRPositionList, ROSkilledHRPosition)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROSkilledHRPosition

      For Each child As ROSkilledHRPosition In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROSkilledHRPositionList() As ROSkilledHRPositionList

      Return New ROSkilledHRPositionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace