﻿' Generated 07 Jul 2014 06:56 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Crew.ReadOnly

  <Serializable()> _
  Public Class ROSkilledHRList
    Inherits SingularReadOnlyListBase(Of ROSkilledHRList, ROSkilledHR)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROSkilledHR

      For Each child As ROSkilledHR In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

    Public Function GetROSkilledHRPosition(HumanResourceID As Integer) As ROSkilledHRPosition

      Dim obj As ROSkilledHRPosition = Nothing
      For Each parent As ROSkilledHR In Me
        obj = parent.ROSkilledHRPositionList.GetItem(HumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), WebFetchable()> _
    Public Class Criteria
      Inherits Singular.Paging.PageCriteria(Of Criteria)

      Public Property ExcludeHRIDs As String = ""
      Public Property ProductionID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property DisciplineID As Integer? = Nothing
      Public Property FilteredProductionTypeInd As Boolean = False
      Public Property ProductionTypeID As Integer? = Nothing
      Public Property FilteredPositionInd As Boolean = False
      Public Property PositionID As Integer? = Nothing
      Public Property IncludedOffInd As Boolean = False
      Public Property TxStartDate As DateTime? = Nothing
      Public Property TxEndDate As DateTime? = Nothing
      Public Property PositionLevelID As Integer? = Nothing
      Public Property VehicleID As Integer? = Nothing
      Public Property RoomID As Integer? = Nothing
      Public Property FilterName As String = ""

      Public Sub New(ByVal ExcludeHRIDs As String,
                     ByVal ProductionID As Integer?,
                     ByVal SystemID As Integer?,
                     ByVal ProductionAreaID As Integer?,
                     ByVal DisciplineID As Integer?,
                     ByVal FilteredProductionTypeInd As Boolean,
                     ByVal ProductionTypeID As Integer?,
                     ByVal FilteredPositionInd As Boolean,
                     ByVal PositionID As Integer?,
                     ByVal IncludedOffInd As Boolean,
                     ByVal TxStartDate As DateTime?,
                     ByVal TxEndDate As DateTime?,
                     ByVal PositionLevelID As Integer?,
                     ByVal VehicleID As Integer?,
                     ByVal RoomID As Integer?,
                     ByVal FilterName As String)
        'ByVal ExcludeHRIDs As List(Of Integer),

        Me.ExcludeHRIDs = ExcludeHRIDs
        Me.ProductionID = ProductionID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.DisciplineID = DisciplineID
        Me.FilteredProductionTypeInd = FilteredProductionTypeInd
        Me.ProductionTypeID = ProductionTypeID
        Me.FilteredPositionInd = FilteredPositionInd
        Me.PositionID = PositionID
        Me.IncludedOffInd = IncludedOffInd
        Me.TxStartDate = TxStartDate
        Me.TxEndDate = TxEndDate
        Me.PositionLevelID = PositionLevelID
        Me.VehicleID = VehicleID
        Me.RoomID = RoomID
        Me.FilterName = FilterName

      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSkilledHRList() As ROSkilledHRList

      Return New ROSkilledHRList()

    End Function

    Public Shared Sub BeginGetROSkilledHRList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSkilledHRList)))

      Dim dp As New DataPortal(Of ROSkilledHRList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    'Public Shared Sub BeginGetROSkilledHRList(CallBack As EventHandler(Of DataPortalResult(Of ROSkilledHRList)))

    '  Dim dp As New DataPortal(Of ROSkilledHRList)()
    '  AddHandler dp.FetchCompleted, CallBack
    '  dp.BeginFetch(New Criteria())

    'End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSkilledHRList() As ROSkilledHRList

      Return DataPortal.Fetch(Of ROSkilledHRList)(New Criteria())

    End Function

    Public Shared Function GetROSkilledHRList(ByVal ExcludeHRIDs As String,
                                              ByVal ProductionID As Integer?,
                                              ByVal SystemID As Integer?,
                                              ByVal ProductionAreaID As Integer?,
                                              ByVal DisciplineID As Integer?,
                                              ByVal FilteredProductionTypeInd As Boolean,
                                              ByVal ProductionTypeID As Integer?,
                                              ByVal FilteredPositionInd As Boolean,
                                              ByVal PositionID As Integer?,
                                              ByVal IncludedOffInd As Boolean,
                                              ByVal TxStartDate As DateTime?,
                                              ByVal TxEndDate As DateTime?,
                                              ByVal PositionLevelID As Integer?,
                                              ByVal VehicleID As Integer?,
                                              ByVal RoomID As Integer?,
                                              ByVal FilterName As String) As ROSkilledHRList
      'ByVal ExcludeHRIDs As List(Of Integer),
      'ExcludeHRIDs,


      Return DataPortal.Fetch(Of ROSkilledHRList)(New Criteria(ExcludeHRIDs,
                                                               ProductionID,
                                                               SystemID,
                                                               ProductionAreaID,
                                                               DisciplineID,
                                                               FilteredProductionTypeInd,
                                                               ProductionTypeID,
                                                               FilteredPositionInd,
                                                               PositionID,
                                                               IncludedOffInd,
                                                               TxStartDate,
                                                               TxEndDate,
                                                               PositionLevelID,
                                                               VehicleID,
                                                               RoomID,
                                                               FilterName))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSkilledHR.GetROSkilledHR(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROSkilledHR = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.ROSkilledHRPositionList.RaiseListChangedEvents = False
          parent.ROSkilledHRPositionList.Add(ROSkilledHRPosition.GetROSkilledHRPosition(sdr))
          parent.ROSkilledHRPositionList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSkilledHRList"
            cm.Parameters.AddWithValue("ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("FilteredProductionTypeInd", NothingDBNull(crit.FilteredProductionTypeInd))
            cm.Parameters.AddWithValue("ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("FilteredPositionInd", NothingDBNull(crit.FilteredPositionInd))
            cm.Parameters.AddWithValue("PositionID", NothingDBNull(crit.PositionID))
            cm.Parameters.AddWithValue("IncludedOffInd", NothingDBNull(crit.IncludedOffInd))
            cm.Parameters.AddWithValue("TxStartDate", NothingDBNull(crit.TxStartDate))
            cm.Parameters.AddWithValue("TxEndDate", NothingDBNull(crit.TxEndDate))
            cm.Parameters.AddWithValue("PositionLevelID", NothingDBNull(crit.PositionLevelID))
            cm.Parameters.AddWithValue("VehicleID", NothingDBNull(crit.VehicleID))
            cm.Parameters.AddWithValue("RoomID", NothingDBNull(crit.RoomID))
            cm.Parameters.AddWithValue("FilterName", NothingDBNull(crit.FilterName))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace