﻿' Generated 21 Jul 2014 14:58 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Productions.Areas
Imports OBLib.Helpers.Templates
Imports OBLib.Biometrics

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Schedules

  <Serializable()> _
  Public Class HRProductionSchedule
    Inherits SingularBusinessBase(Of HRProductionSchedule)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    ''' 
    <Key()>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "Crew Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Crew Type", Description:="")>
    Public Property CrewTypeID() As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTypeIDProperty, Value)
      End Set
    End Property

    Public Shared CrewTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CrewType, "CrewType")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="CrewType", Description:="")>
    Public Property CrewType() As String
      Get
        Return GetProperty(CrewTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CrewTypeProperty, Value)
      End Set
    End Property

    Public Shared DisciplinesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Disciplines, "Disciplines")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Disciplines", Description:="")>
    Public Property Disciplines() As String
      Get
        Return GetProperty(DisciplinesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplinesProperty, Value)
      End Set
    End Property

    Public Shared AllClashesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AllClashes, "Clashes")
    ''' <summary>
    ''' Gets and sets the All Clashes value
    ''' </summary>
    <Display(Name:="Clashes", Description:="")>
    Public Property AllClashes() As String
      Get
        Return GetProperty(AllClashesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AllClashesProperty, Value)
      End Set
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CityID, "City", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public Property CityID() As Integer?
      Get
        Return GetProperty(CityIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CityIDProperty, Value)
      End Set
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City")
    ''' <summary>
    ''' Gets and sets the All Clashes value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CityCodeProperty, Value)
      End Set
    End Property

    Public Shared LocalProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Local, "Local?", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="Local?", Description:="")>
    Public Property Local() As Boolean
      Get
        Return GetProperty(LocalProperty)
      End Get
      Set(value As Boolean)
        SetProperty(LocalProperty, value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, OBLib.JSCode.HRProductionScheduleJS.HRProductionScheduleToString)

    <ClientOnly()>
    Public Property Visible As Boolean = True

    <ClientOnly()>
    Public Property ScheduleVisible As Boolean = False

    <ClientOnly()>
    Public Property SaveSucceededVisible As Boolean = False

    <ClientOnly()>
    Public Property SaveFailedVisible As Boolean = False

    <ClientOnly()>
    Public Property IsBusyClient As Boolean = False

    Public Shared HasPreAndPostTravelProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasPreAndPostTravel, "HasPreAndPostTravel?", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary>
    <Display(Name:="HasPreAndPostTravel?", Description:="")>
    Public Property HasPreAndPostTravel() As Boolean
      Get
        Return GetProperty(HasPreAndPostTravelProperty)
      End Get
      Set(value As Boolean)
        SetProperty(HasPreAndPostTravelProperty, value)
      End Set
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "Contract Type", Nothing)
    ''' <summary>
    ''' Gets the Contrac Type ID value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="The number of hours and minutes to offset the calculated start time by")>
    Public ReadOnly Property ContractTypeID() As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared CoreCrewIndServerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CoreCrewIndServer, "Core Crew", False)
    Public ReadOnly Property CoreCrewIndServer() As Boolean
      Get
        Return GetProperty(CoreCrewIndServerProperty)
      End Get
    End Property

    Public Shared CoreDisciplineIndServerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CoreDisciplineIndServer, "Core Discipline", False)
    Public ReadOnly Property CoreDisciplineIndServer() As Boolean
      Get
        Return GetProperty(CoreDisciplineIndServerProperty)
      End Get
    End Property

    Public Property CoreCrewInd() As Boolean
      Get
        Dim ClientCoreCrew As Boolean = False
        If HumanResourceID IsNot Nothing Then
          Dim phrs As List(Of ProductionHumanResource) = Me.GetParent.ProductionHumanResourceList.Where(Function(c) CompareSafe(c.HumanResourceID, HumanResourceID)).ToList
          For Each phr As ProductionHumanResource In phrs
            If phr.DisciplineID IsNot Nothing Then
              If {1, 2, 5, 6, 7, 11}.Contains(phr.DisciplineID) Then
                ClientCoreCrew = True
              End If
            End If
          Next
        End If
        Return Me.CoreCrewIndServer Or ClientCoreCrew
      End Get
      Set(value As Boolean)

      End Set
    End Property

    Public Property CoreDisciplineInd() As Boolean
      Get
        Dim ClientCoreDiscipline As Boolean = False
        If HumanResourceID IsNot Nothing Then
          Dim phrs As List(Of ProductionHumanResource) = Me.GetParent.ProductionHumanResourceList.Where(Function(c) CompareSafe(c.HumanResourceID, HumanResourceID)).ToList
          For Each phr As ProductionHumanResource In phrs
            If phr.DisciplineID IsNot Nothing Then
              If {1, 2, 5, 6, 7, 11}.Contains(phr.DisciplineID) Then
                ClientCoreDiscipline = True
              End If
            End If
          Next
        End If
        Return Me.CoreDisciplineIndServer Or ClientCoreDiscipline
      End Get
      Set(value As Boolean)

      End Set
    End Property

    Public Property CoreCrewDisciplineInd() As Boolean
      Get
        Return CoreCrewInd Or CoreDisciplineInd
      End Get
      Set(value As Boolean)

      End Set
    End Property

    Public ReadOnly Property IsEventManager As Boolean
      Get
        Dim phrs As List(Of ProductionHumanResource) = Me.GetParent.ProductionHumanResourceList.Where(Function(c) CompareSafe(c.HumanResourceID, HumanResourceID)).ToList
        Dim emd As List(Of ProductionHumanResource) = phrs.Where(Function(c) CompareSafe(c.DisciplineID, CType(OBLib.CommonData.Enums.Discipline.EventManager, Integer)) Or CompareSafe(c.DisciplineID, CType(OBLib.CommonData.Enums.Discipline.EventManagerIntern, Integer))).ToList
        If emd.Count > 0 Then
          Return True
        Else
          Return False
        End If
      End Get
    End Property


    Public Shared AccessFlagCountDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccessFlagCountDescription, "Total Access flag count", "")
    ''' <summary>
    ''' Gets total Access flag count
    ''' </summary>
    <Display(Name:="Access flags", Description:="total Access flags")>
    Public ReadOnly Property AccessFlagCountDescription() As String
      Get
        Return "Bio flags: " & AccessShiftList.Where(Function(c) c.AccessFlagID > 0).Count().ToString
      End Get
    End Property

    Public ReadOnly Property AccessFlagCount() As Integer
      Get
        Return AccessShiftList.Where(Function(c) c.AccessFlagID > 0).Count()
      End Get
    End Property

    ' Public Shared AccessFlagTypeProperty As PropertyInfo(Of Integer) = RegisterReadOnlyProperty(Of Integer)(Function(c) c.AccessFlagType, "getAccessFlagType(selft)")
    Public Property MakeFlagInformationalWithReason() As Boolean

    Public ReadOnly Property IsAccessFlagInformational() As Boolean
      Get
        Return AccessShiftList.IsInformational
      End Get
    End Property

    Public Shared CancelledDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDateTime, "Cancelled Date")
    ''' <summary>
    ''' Gets and sets the Flagged Date Time value
    ''' </summary>
    <Display(Name:="Cancelled Date Time", Description:="")>
    Public ReadOnly Property CancelledDateTime() As DateTime?
      Get
        Return GetProperty(CancelledDateTimeProperty)
      End Get
    End Property

    Public Shared IsOBsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsOBs, "Local?", False)
    ''' <summary>
    ''' Gets the Primary value
    ''' </summary> 
    Public ReadOnly Property IsOBs() As Boolean
      Get

        If Me.SystemID = CommonData.Enums.System.ProductionServices AndAlso _
                                 Me.ProductionAreaID = CommonData.Enums.System.ProductionServices Then
          Return True
        Else
          Return False
        End If
      End Get
    End Property

    Public Property IsRemoved As Boolean = False

    'Public Shared CoreDisciplineIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CoreDisciplineInd, "Core Discipline", False)
    ' ''' <summary>
    ' ''' Gets and sets the TBC value
    ' ''' </summary>
    'Public ReadOnly Property CoreDisciplineInd() As Boolean
    '  Get
    '    Return GetProperty(CoreDisciplineIndProperty)
    '  End Get
    'End Property

    'Public Shared CoreCrewIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CoreCrewInd, "Core Crew", False)
    ' ''' <summary>
    ' ''' Gets and sets the TBC value
    ' ''' </summary>
    'Public ReadOnly Property CoreCrewInd() As Boolean
    '  Get
    '    Return GetProperty(CoreCrewIndProperty)
    '  End Get
    'End Property

    'Public Shared CoreCrewDisciplineIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CoreCrewDisciplineInd, "Core Crew and Discipline", False)
    ' ''' <summary>
    ' ''' Gets and sets the TBC value
    ' ''' </summary>
    'Public ReadOnly Property CoreCrewDisciplineInd() As Boolean
    '  Get
    '    Return Me.CoreCrewInd AndAlso CoreDisciplineInd
    '  End Get
    'End Property

#End Region

#Region " Child Lists "

    Public Shared HRProductionScheduleDetailListProperty As PropertyInfo(Of HRProductionScheduleDetailList) = RegisterProperty(Of HRProductionScheduleDetailList)(Function(c) c.HRProductionScheduleDetailList, "HR Production Schedule Detail List")
    <Browsable(False)>
    Public ReadOnly Property HRProductionScheduleDetailList() As HRProductionScheduleDetailList
      Get
        If GetProperty(HRProductionScheduleDetailListProperty) Is Nothing Then
          LoadProperty(HRProductionScheduleDetailListProperty, Productions.Schedules.HRProductionScheduleDetailList.NewHRProductionScheduleDetailList())
        End If
        Return GetProperty(HRProductionScheduleDetailListProperty)
      End Get
    End Property

    Public Shared HRProductionScheduleDetailListClientProperty As PropertyInfo(Of HRProductionScheduleDetailList) = RegisterProperty(Of HRProductionScheduleDetailList)(Function(c) c.HRProductionScheduleDetailListClient, "HR Production Schedule Detail List")
    Public Property HRProductionScheduleDetailListClient() As HRProductionScheduleDetailList
      Get
        If GetProperty(HRProductionScheduleDetailListClientProperty) Is Nothing Then
          LoadProperty(HRProductionScheduleDetailListClientProperty, Productions.Schedules.HRProductionScheduleDetailList.NewHRProductionScheduleDetailList())
        End If
        Return GetProperty(HRProductionScheduleDetailListClientProperty)
      End Get
      Set(value As HRProductionScheduleDetailList)
        SetProperty(HRProductionScheduleDetailListClientProperty, value)
      End Set
    End Property

    Public Shared UnSelectedProductionTimelineListProperty As PropertyInfo(Of List(Of UnSelectedTimeline)) = RegisterProperty(Of List(Of UnSelectedTimeline))(Function(c) c.UnSelectedProductionTimelineList, "UnSelected Production Timeline List")
    Public ReadOnly Property UnSelectedProductionTimelineList() As List(Of UnSelectedTimeline)
      Get
        If GetProperty(UnSelectedProductionTimelineListProperty) Is Nothing Then
          LoadProperty(UnSelectedProductionTimelineListProperty, New List(Of UnSelectedTimeline))
        End If
        Return GetProperty(UnSelectedProductionTimelineListProperty)
      End Get
      'Set(value As List(Of UnSelectedTimeline))
      '  SetProperty(UnSelectedProductionTimelineListProperty, value)
      'End Set
    End Property

    Public Shared AccessShiftListProperty As PropertyInfo(Of Biometrics.AccessShiftList) = RegisterProperty(Of Biometrics.AccessShiftList)(Function(c) c.AccessShiftList, "Shift list")

    <Display(Name:="Biometrics Shifts", Description:="Access Shits")>
    Public ReadOnly Property AccessShiftList() As Biometrics.AccessShiftList
      Get
        If GetProperty(AccessShiftListProperty) Is Nothing Then
          LoadProperty(AccessShiftListProperty, Biometrics.AccessShiftList.NewAccessShiftList)
        End If
        Return GetProperty(AccessShiftListProperty)
      End Get
    End Property

    Public Shared AccessHRBookingListProperty As PropertyInfo(Of Biometrics.AccessHRBookingList) = RegisterProperty(Of Biometrics.AccessHRBookingList)(Function(c) c.AccessHRBookingList, "HR Booking list")

    <Display(Name:="HR Bookings", Description:="Bookings")>
    Public ReadOnly Property AccessHRBookingList() As Biometrics.AccessHRBookingList
      Get
        If GetProperty(AccessHRBookingListProperty) Is Nothing Then
          LoadProperty(AccessHRBookingListProperty, Biometrics.AccessHRBookingList.NewAccessHRBookingList)
        End If
        Return GetProperty(AccessHRBookingListProperty)
      End Get
    End Property

    'Set(value As ProductionTimelineList)
    '    SetProperty(UnSelectedProductionTimelineListProperty, value)
    'End Set

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "HR Production Schedule")
        Else
          Return String.Format("Blank {0}", "HR Production Schedule")
        End If
      Else
        Return Me.HumanResource
      End If

    End Function

    Public Function GetParent() As Areas.ProductionSystemArea

      Return CType(CType(Me.Parent, HRProductionScheduleList).Parent, Areas.ProductionSystemArea)

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

    Public Sub RemoveFreeIndOnDay()

      Dim FreeItemCountPerDay = (From data In HRProductionScheduleDetailList
                                 Group By Day = data.TimesheetDate.Value.Date Into Group, Count()
                                 Select New With {
                                                  Key .Day = Day,
                                                  Key .TotalCount = Count,
                                                  Key .FreeCount = Group.Count(Function(d) d.FreeInd)
                                                })

      For Each item In FreeItemCountPerDay
        If item.TotalCount > item.FreeCount Then
          Dim DayItems As List(Of HRProductionScheduleDetail) = HRProductionScheduleDetailList.Where(Function(d) d.TimesheetDate.Value.Date = item.Day And d.FreeInd).ToList
          For Each DayItem As HRProductionScheduleDetail In DayItems
            Dim Index As Integer = Me.HRProductionScheduleDetailList.IndexOf(DayItem)
            If Me.HRProductionScheduleDetailList(Index).CanEditScheduleCheck Then
              Me.HRProductionScheduleDetailList.RemoveAt(Index)
            End If
          Next
        End If
      Next

    End Sub

    Public Sub AddNewItems(Template As List(Of Helpers.Templates.HRSchedule))
      Dim ScheduleTemplate As Helpers.Templates.HRSchedule = Template.Where(Function(d) d.HumanResourceID = HumanResourceID).FirstOrDefault()
      If ScheduleTemplate IsNot Nothing Then
        ScheduleTemplate.HRScheduleTemplateList.ForEach(Sub(DetailItem)
                                                          'Check if my schedule already has that item
                                                          If Not HRProductionScheduleDetailList.Contains(DetailItem.ProductionTimeline.Guid) Then
                                                            'If it does not have the item, then add it

                                                          End If
                                                        End Sub)
      End If
    End Sub

    Public Sub RemoveUnNeededDriverItems()

      'Travel
      Dim DriverTravelTimelineTypes() As CommonData.Enums.ProductionTimelineType = {CommonData.Enums.ProductionTimelineType.VehiclePreTravel,
                                                                                    CommonData.Enums.ProductionTimelineType.DriverPreTravel,
                                                                                    CommonData.Enums.ProductionTimelineType.VehiclePostTravel,
                                                                                    CommonData.Enums.ProductionTimelineType.DriverPostTravel}

      Dim OpsTravelTimelineTypes() As CommonData.Enums.ProductionTimelineType = {CommonData.Enums.ProductionTimelineType.OpsPreTravel,
                                                                                 CommonData.Enums.ProductionTimelineType.RigPreTravel,
                                                                                 CommonData.Enums.ProductionTimelineType.OpsPostTravel,
                                                                                 CommonData.Enums.ProductionTimelineType.RigPostTravel}

      Dim DriverTravelTimelines As List(Of HRProductionScheduleDetail) = HRProductionScheduleDetailList.Where(Function(d) DriverTravelTimelineTypes.Contains(d.ProductionTimelineTypeID)).ToList
      Dim OpsTravelTimelines As List(Of HRProductionScheduleDetail) = HRProductionScheduleDetailList.Where(Function(d) OpsTravelTimelineTypes.Contains(d.ProductionTimelineTypeID)).ToList
      If DriverTravelTimelines.Count > 0 And OpsTravelTimelines.Count > 0 Then
        For Each OpsTimeline As HRProductionScheduleDetail In OpsTravelTimelines
          Dim Index As Integer = Me.HRProductionScheduleDetailList.IndexOf(OpsTimeline)
          If Me.HRProductionScheduleDetailList(Index).CanEditScheduleCheck Then
            Me.HRProductionScheduleDetailList.RemoveAt(Index)
          End If
        Next
      End If

      'DeRig
      Dim DriverDriverDeRigTimelineTypes() As CommonData.Enums.ProductionTimelineType = {CommonData.Enums.ProductionTimelineType.DriverDeRig}

      Dim OpsDeRigTimelineTypes() As CommonData.Enums.ProductionTimelineType = {CommonData.Enums.ProductionTimelineType.OpsDeRig,
                                                                                CommonData.Enums.ProductionTimelineType.RigDeRig}

      Dim DriverDeRigTimelines As List(Of HRProductionScheduleDetail) = HRProductionScheduleDetailList.Where(Function(d) DriverDriverDeRigTimelineTypes.Contains(d.ProductionTimelineTypeID)).ToList
      Dim OpsDeRigTimelines As List(Of HRProductionScheduleDetail) = HRProductionScheduleDetailList.Where(Function(d) OpsDeRigTimelineTypes.Contains(d.ProductionTimelineTypeID)).ToList
      If DriverDeRigTimelines.Count > 0 And OpsDeRigTimelines.Count > 0 Then
        For Each OpsDeRigTimeline As HRProductionScheduleDetail In OpsDeRigTimelines
          Dim Index As Integer = Me.HRProductionScheduleDetailList.IndexOf(OpsDeRigTimeline)
          If Me.HRProductionScheduleDetailList(Index).CanEditScheduleCheck Then
            Me.HRProductionScheduleDetailList.RemoveAt(Index)
          End If
        Next
      End If

    End Sub

    Public Sub RemoveTravel()

      Dim TravelTimelineTypes() As CommonData.Enums.ProductionTimelineType = {CommonData.Enums.ProductionTimelineType.RigPreTravel,
                                                                              CommonData.Enums.ProductionTimelineType.RigPostTravel,
                                                                              CommonData.Enums.ProductionTimelineType.VehiclePostTravel,
                                                                              CommonData.Enums.ProductionTimelineType.DriverPostTravel,
                                                                              CommonData.Enums.ProductionTimelineType.OpsPreTravel,
                                                                              CommonData.Enums.ProductionTimelineType.OpsPostTravel}

      Dim TravelTimelines As List(Of HRProductionScheduleDetail) = HRProductionScheduleDetailList.Where(Function(d) TravelTimelineTypes.Contains(d.ProductionTimelineTypeID)).ToList
      For Each TravelTimeline As HRProductionScheduleDetail In TravelTimelines
        Dim Index As Integer = Me.HRProductionScheduleDetailList.IndexOf(TravelTimeline)
        If Me.HRProductionScheduleDetailList(Index).CanEditScheduleCheck Then
          Me.HRProductionScheduleDetailList.RemoveAt(Index)
        End If
      Next

    End Sub

    Public Sub RemoveDaysAway()
      Dim DayAwayTimelineTypes() As CommonData.Enums.ProductionTimelineType = {CommonData.Enums.ProductionTimelineType.DayAway}
      Dim DaysAway As List(Of HRProductionScheduleDetail) = HRProductionScheduleDetailList.Where(Function(d) DayAwayTimelineTypes.Contains(d.ProductionTimelineTypeID)).ToList
      For Each DayAway As HRProductionScheduleDetail In DaysAway
        Dim Index As Integer = Me.HRProductionScheduleDetailList.IndexOf(DayAway)
        If Me.HRProductionScheduleDetailList(Index).CanEditScheduleCheck Then
          Me.HRProductionScheduleDetailList.RemoveAt(Index)
        End If
      Next
    End Sub

    Public Function GetFirstProductionHumanResource() As Areas.ProductionHumanResource

      For Each phr As ProductionHumanResource In GetParent.ProductionHumanResourceList
        If phr.HumanResourceID = Me.HumanResourceID Then
          Return phr
        End If
      Next
      Return Nothing
      'Return GetParent.ProductionHumanResourceList.Where(Function(d) d.HumanResourceID = Me.HumanResourceID).FirstOrDefault()
    End Function

    Public Sub AddTimeline(ProductionTimeline As Areas.ProductionTimeline)
      Dim ExistingSchedule As HRProductionScheduleDetail = Me.HRProductionScheduleDetailList.GetItemByProductionTimeline(ProductionTimeline)
      If ExistingSchedule Is Nothing Then
        Dim NewSchedule As HRProductionScheduleDetail = Me.HRProductionScheduleDetailList.AddNew
        Dim phr As Areas.ProductionHumanResource = GetFirstProductionHumanResource()
        NewSchedule.ProductionTimeline = ProductionTimeline
        NewSchedule.ProductionHumanResource = phr
        NewSchedule.ProductionHumanResourceID = phr.ProductionHumanResourceID
        NewSchedule.ProductionTimelineTypeID = ProductionTimeline.ProductionTimelineTypeID
        NewSchedule.ProductionTimelineType = ProductionTimeline.ProductionTimelineType
        NewSchedule.ProductionTimelineID = ProductionTimeline.ProductionTimelineID
        NewSchedule.StartDateTime = ProductionTimeline.StartDateTime
        NewSchedule.EndDateTime = ProductionTimeline.EndDateTime
        NewSchedule.TimesheetDate = ProductionTimeline.CalculateTimesheetDate
        NewSchedule.Visible = True
        NewSchedule.HumanResourceID = phr.HumanResourceID
        NewSchedule.FreeInd = ProductionTimeline.ROProductionTimelineType.FreeInd
        NewSchedule.CrewTimesheetID = Nothing
        NewSchedule.ExcludeInd = False
        NewSchedule.ExcludeReason = ""
        NewSchedule.Detail = ""
        NewSchedule.Clash = ""
        NewSchedule.SystemID = ProductionTimeline.SystemID
        NewSchedule.ProductionAreaID = ProductionTimeline.ProductionAreaID
      End If
    End Sub

    Public Sub UpdateTimeline(ProductionTimeline As Areas.ProductionTimeline)
      Dim ExistingSchedule As HRProductionScheduleDetail = Me.HRProductionScheduleDetailList.GetItemByProductionTimeline(ProductionTimeline)
      If ExistingSchedule IsNot Nothing Then
        ExistingSchedule.StartDateTime = ProductionTimeline.StartDateTime
        ExistingSchedule.EndDateTime = ProductionTimeline.EndDateTime
        ExistingSchedule.TimesheetDate = ProductionTimeline.CalculateTimesheetDate
        ExistingSchedule.Clash = ""
      End If
    End Sub

    Public Sub RemoveTimeline(ProductionTimeline As Areas.ProductionTimeline)
      Dim ExistingSchedule As HRProductionScheduleDetail = Me.HRProductionScheduleDetailList.GetItemByProductionTimeline(ProductionTimeline)
      If ExistingSchedule IsNot Nothing Then
        Dim Index As Integer = Me.HRProductionScheduleDetailList.IndexOf(ExistingSchedule)
        Me.HRProductionScheduleDetailList.RemoveAt(Index)
      End If
    End Sub

    Public Sub RemoveSpecificPHR(ProductionHumanResource As Areas.ProductionHumanResource)
      Dim ExistingSchedules As List(Of HRProductionScheduleDetail) = Me.HRProductionScheduleDetailList.Where(Function(d) d.ProductionHumanResource Is ProductionHumanResource).ToList
      ExistingSchedules.ForEach(Sub(ItemToRemove)
                                  Dim Index As Integer = Me.HRProductionScheduleDetailList.IndexOf(ItemToRemove)
                                  Me.HRProductionScheduleDetailList.RemoveAt(Index)
                                End Sub)
    End Sub

    'Public Sub CheckClashesServer()

    '  Dim ds As New DataSet("DataSet")
    '  Dim dt As DataTable = ds.Tables.Add("Table")
    '  dt.Columns.Add("GUID", GetType(Guid)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("ProductionID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("SystemID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("ProductionAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("CrewScheduleDetailID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  dt.Columns("CrewScheduleDetailID").AllowDBNull = True
    '  dt.Columns.Add("ProductionTimelineID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  dt.Columns("ProductionTimelineID").AllowDBNull = True
    '  dt.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute

    '  Dim query = From HRSchedDetail As HRProductionScheduleDetail In Me.HRProductionScheduleDetailList
    '              Select New With { _
    '                                Key .Guid = HRSchedDetail.Guid, _
    '                                Key .HumanResourceID = Me.HumanResourceID, _
    '                                Key .ProductionID = Me.ProductionID, _
    '                                Key .SystemID = Me.SystemID, _
    '                                Key .ProductionAreaID = Me.ProductionAreaID, _
    '                                Key .CrewScheduleDetailID = HRSchedDetail.CrewScheduleDetailID, _
    '                                Key .ProductionTimelineID = HRSchedDetail.ProductionTimelineID, _
    '                                Key .StartDateTime = HRSchedDetail.StartDateTime, _
    '                                Key .EndDateTime = HRSchedDetail.EndDateTime
    '                              }


    '  For Each obj In query
    '    Dim row As DataRow = dt.NewRow
    '    row("Guid") = obj.Guid
    '    row("HumanResourceID") = obj.HumanResourceID
    '    row("ProductionID") = NothingDBNull(obj.ProductionID)
    '    row("SystemID") = obj.SystemID
    '    row("ProductionAreaID") = obj.ProductionAreaID
    '    row("CrewScheduleDetailID") = NothingDBNull(obj.CrewScheduleDetailID)
    '    row("ProductionTimelineID") = NothingDBNull(obj.ProductionTimelineID)
    '    row("StartDateTime") = NothingDBNull(obj.StartDateTime)
    '    row("EndDateTime") = NothingDBNull(obj.EndDateTime)
    '    dt.Rows.Add(row)
    '  Next

    '  Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdValidateAreaSchedule]",
    '                                      "@AreaScheduleXML",
    '                                      ds.GetXml)
    '  cmd.FetchType = CommandProc.FetchTypes.DataSet
    '  cmd = cmd.Execute

    '  'Reset the clashes
    '  Me.AllClashes = ""

    '  Dim HRProductionSchedule As HRProductionSchedule = Nothing
    '  Dim HumanResourceID As Integer? = Nothing
    '  Dim GuidString As String = ""
    '  Dim GUid As Guid = Nothing
    '  Dim AreaScheduleDetail As HRProductionScheduleDetail = Nothing

    '  For Each dr As DataRow In cmd.Dataset.Tables(0).Rows
    '    Dim Clash As String = dr(4)
    '    GUid = dr(1)
    '    AreaScheduleDetail = Me.HRProductionScheduleDetailList.GetItemByGuid(GUid)
    '    If AreaScheduleDetail IsNot Nothing Then
    '      AreaScheduleDetail.Clash = Clash
    '    End If
    '  Next

    'End Sub

    Public Sub CheckClashesClient()

      Dim ds As New DataSet("DataSet")
      Dim dt As DataTable = ds.Tables.Add("Table")
      dt.Columns.Add("GUID", GetType(Guid)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("ProductionID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("SystemID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("ProductionAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("CrewScheduleDetailID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("ProductionTimelineID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute

      Dim query = From HRSchedDetail As HRProductionScheduleDetail In Me.HRProductionScheduleDetailListClient
                  Select New With { _
                                    Key .Guid = HRSchedDetail.Guid, _
                                    Key .HumanResourceID = Me.HumanResourceID, _
                                    Key .ProductionID = Me.ProductionID, _
                                    Key .SystemID = Me.SystemID, _
                                    Key .ProductionAreaID = Me.ProductionAreaID, _
                                    Key .CrewScheduleDetailID = HRSchedDetail.CrewScheduleDetailID, _
                                    Key .ProductionTimelineID = HRSchedDetail.ProductionTimelineID, _
                                    Key .StartDateTime = HRSchedDetail.StartDateTime, _
                                    Key .EndDateTime = HRSchedDetail.EndDateTime
                                  }


      For Each obj In query
        Dim row As DataRow = dt.NewRow
        row("Guid") = obj.Guid
        row("HumanResourceID") = obj.HumanResourceID
        row("ProductionID") = NothingDBNull(obj.ProductionID)
        row("SystemID") = obj.SystemID
        row("ProductionAreaID") = obj.ProductionAreaID
        row("CrewScheduleDetailID") = NothingDBNull(obj.CrewScheduleDetailID)
        row("ProductionTimelineID") = NothingDBNull(obj.ProductionTimelineID)
        row("StartDateTime") = obj.StartDateTime
        row("EndDateTime") = obj.EndDateTime
        dt.Rows.Add(row)
      Next

      Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdValidateAreaSchedule]",
                                          "@AreaScheduleXML",
                                          ds.GetXml)
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute

      'Reset the clashes
      Me.AllClashes = ""

      Dim HRProductionSchedule As HRProductionSchedule = Nothing
      Dim HumanResourceID As Integer? = Nothing
      Dim GuidString As String = ""
      Dim GUid As Guid = Nothing
      Dim AreaScheduleDetail As HRProductionScheduleDetail = Nothing

      For Each dr As DataRow In cmd.Dataset.Tables(0).Rows
        Dim Clash As String = dr(4)
        GUid = dr(1)
        AreaScheduleDetail = Me.HRProductionScheduleDetailList.GetItemByGuid(GUid)
        If AreaScheduleDetail IsNot Nothing Then
          AreaScheduleDetail.Clash = Clash
        End If
      Next

    End Sub

    Public Sub UpdateFromClient(ClientSchedule As HRProductionSchedule)
      'Dim RemovedListClient As New List(Of HRProductionScheduleDetail)
      For Each ClientItem In ClientSchedule.HRProductionScheduleDetailListClient
        If ClientItem.Added Then
          'Add to server side list
          Me.AddItemForClient(ClientItem)
        Else
          'updated or deleted from client, so it must exist on the server side
          Dim ServerItem As HRProductionScheduleDetail = Me.HRProductionScheduleDetailList.GetItemByGuid(ClientItem.Guid)
          If ServerItem IsNot Nothing Then
            'Dim accessShift As AccessShift
            'Dim ClientAccessShift As AccessShift
            'If ServerItem.AccessShiftID IsNot Nothing Then

            '  accessShift = Me.AccessShiftList.GetItem(ServerItem.AccessShiftID, ServerItem.TimesheetDate)
            '  ClientAccessShift = ClientSchedule.AccessShiftList.GetItem(ServerItem.AccessShiftID, ServerItem.TimesheetDate)
            'End If
            If ClientItem.Deleted Then
              'RemovedListClient.Add(ClientDetailItem)
              Dim Index = Me.HRProductionScheduleDetailList.IndexOf(ServerItem)
              If Index >= 0 Then
                Me.HRProductionScheduleDetailList.RemoveAt(Index)
              End If
            ElseIf ClientItem.Updated Then
              Dim IsTimeChanged As Boolean = False
              If ServerItem.StartDateTime <> ClientItem.StartDateTime OrElse ServerItem.EndDateTime <> ClientItem.EndDateTime Then
                IsTimeChanged = True
              End If

              ServerItem.Detail = ClientItem.Detail
              ServerItem.EndDateTime = ClientItem.EndDateTime
              ServerItem.ExcludeInd = ClientItem.ExcludeInd
              ServerItem.ExcludeReason = ClientItem.ExcludeReason
              ServerItem.FreeInd = ClientItem.FreeInd
              'ServerItem.ProductionHumanResource = ClientDetailItem.ProductionHumanResource
              'ServerItem.ProductionHumanResourceID = ClientDetailItem.ProductionHumanResourceID
              'ServerItem.ProductionTimeline = ClientDetailItem.ProductionTimeline
              'ServerItem.ProductionTimelineID = ClientDetailItem.ProductionTimelineID
              ServerItem.ProductionTimelineTypeID = ClientItem.ProductionTimelineTypeID
              ServerItem.StartDateTime = ClientItem.StartDateTime
              ServerItem.TimesheetDate = ClientItem.TimesheetDate
              'If ServerItem.StartDateTime.Value.Subtract(ServerItem.EndDateTime.Value).TotalSeconds >= 0 Then
              '  Dim ED As Integer = ServerItem.EndDateTime.Value.Day
              '  Dim EY As Integer = ServerItem.EndDateTime.Value.Year
              '  Dim EM As Integer = ServerItem.EndDateTime.Value.Month
              '  Dim ETS As TimeSpan = ServerItem.StartDateTime.Value.TimeOfDay
              '  Dim nd As DateTime = New DateTime(EY, EM, ED, ETS.Hours, ETS.Minutes, ETS.Seconds)
              'End If
              If IsTimeChanged Then
                'If accessShift IsNot Nothing Then
                '  accessShift.ChangedShiftStartDateTime = Me.HRProductionScheduleDetailList.GetScheduleStartTimeForBiometrics(ServerItem.TimesheetDate)
                '  accessShift.ChangedShiftEndDateTime = Me.HRProductionScheduleDetailList.GetScheduleEndTimeForBiometrics(ServerItem.TimesheetDate)

                '  If ServerItem.StartDateTime = accessShift.ChangedShiftStartDateTime OrElse ServerItem.EndDateTime = accessShift.ChangedShiftEndDateTime Then

                '    accessShift.ShiftStartDateTime = ClientItem.OrigStartDateTime
                '    accessShift.ShiftEndDateTime = ClientItem.OrigEndDateTime

                '    accessShift.GetShiftFlagValues(accessShift.ChangedShiftStartDateTime, accessShift.ChangedShiftEndDateTime, OBLib.CommonData.Enums.AccessTerminalGroup.Outside_Broadcast)
                '  End If
                'End If
              End If
            End If
            'If ClientAccessShift IsNot Nothing AndAlso ClientAccessShift.IsDirty Then
            '  AccessShift.PlannedTimeChangedReason = ClientAccessShift.PlannedTimeChangedReason

            'End If
            If ClientSchedule.IsRemoved <> Me.IsRemoved Then
              Me.IsRemoved = ClientSchedule.IsRemoved
              Dim phr = Me.GetFirstProductionHumanResource
              If phr IsNot Nothing Then
                phr.CancelledDate = Now
                phr.CancelledReason = "Human Resource Removed"
              End If
              'For Each HRB As AccessHRBooking In ClientSchedule.AccessHRBookingList
              '  If HRB.IsDirty Then
              '    Dim ServerAccessBooking = Me.AccessHRBookingList.GetItem(HRB.KeyValue)
              '    ServerAccessBooking.IsCancelled = HRB.IsCancelled
              '    ServerAccessBooking.CancelledReason = HRB.CancelledReason
              '    ServerAccessBooking.RentalCarReplacementHRID = HRB.RentalCarReplacementHRID
              '  End If
              'Next
            End If
          End If
        End If
      Next

      OBLib.Helpers.ProductionSchedules.CheckProductionAvailabilityOB(Me.GetParent, HumanResourceID)
      Me.HRProductionScheduleDetailListClient = HRProductionScheduleDetailList
    End Sub

    Private Sub AddItemForClient(ClientDetailItem As HRProductionScheduleDetail)
      'Me.HRProductionScheduleDetailList.ReplaceOrAdd(ClientDetailItem)
      Dim NewItem As HRProductionScheduleDetail = Me.HRProductionScheduleDetailList.AddNew
      Dim pt As ProductionTimeline = Me.GetParent.ProductionTimelineList.Where(Function(d) d.Guid.ToString = ClientDetailItem.ProductionTimelineGuid).FirstOrDefault
      Dim phr As ProductionHumanResource = Me.GetParent.ProductionHumanResourceList.Where(Function(d) d.HumanResourceID IsNot Nothing AndAlso CompareSafe(d.HumanResourceID, Me.HumanResourceID)).FirstOrDefault
      If pt IsNot Nothing And phr IsNot Nothing Then
        With NewItem
          .Added = False
          .Clash = ClientDetailItem.Clash
          .Deleted = False
          .Detail = ""
          .EndDateTime = ClientDetailItem.EndDateTime
          .ExcludeInd = False
          .ExcludeReason = ""
          .FreeInd = False
          .HumanResourceID = Me.HumanResourceID
          .ProductionHumanResource = phr
          .ProductionHumanResourceGuid = phr.Guid.ToString
          .ProductionTimeline = pt
          .ProductionTimelineGuid = pt.Guid.ToString
          .ProductionTimelineID = pt.ProductionTimelineID
          .ProductionTimelineType = pt.ProductionTimelineType
          .ProductionTimelineTypeID = pt.ProductionTimelineTypeID
          .Selected = False
          .StartDateTime = pt.StartDateTime
          .TimesheetDate = pt.CalculateTimesheetDate
          .Updated = False
          .Visible = True
          .HumanResourceShiftID = Nothing
          .HumanResourceOffPeriodID = Nothing
          .SystemID = pt.SystemID
          .ProductionAreaID = pt.ProductionAreaID
          .HumanResourceScheduleTypeID = CType(CommonData.Enums.HumanResourceScheduleType.Production, Integer)
          .ProductionTimelineMatchInd = True
          .StartTimeOffset = 0
          .EndTimeOffset = 0
        End With
      End If
      'Me.HRProductionScheduleDetailList.GetItemByGuid(ClientDetailItem.Guid).Added = False
    End Sub

    Public Sub SetupUnSelectedTimelines()
      Me.UnSelectedProductionTimelineList.Clear()
      For Each pt As ProductionTimeline In Me.GetParent.ProductionTimelineList
        If pt.IsValid Then
          If Me.HRProductionScheduleDetailList.GetItemByProductionTimeline(pt) Is Nothing Then
            Dim NewItem As New Helpers.Templates.UnSelectedTimeline(pt, ContractTypeID)
            Me.UnSelectedProductionTimelineList.Add(NewItem)
            'Me.UnSelectedProductionTimelineList.AddNew
          End If
        End If
      Next
    End Sub

    Public Sub UpdateDetails()

      Dim VenueCityID As Integer?
      Dim ROProductionVenue As Maintenance.Productions.ReadOnly.ROProductionVenueOld = CommonData.Lists.ROProductionVenueList.GetItem(Me.GetParent.GetParent.ProductionVenueID)
      Dim ProductionVenueCity As Maintenance.Locations.ReadOnly.ROCity = Nothing
      If ROProductionVenue IsNot Nothing Then
        'ProductionVenueCity = CommonData.Lists.ROCityList.GetItem(ROProductionVenue.CityID)
        VenueCityID = ROProductionVenue.CityID
      End If

      Dim ROHR As OBLib.HR.ReadOnly.ROHumanResource = CommonData.Lists.ROHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, HumanResourceID)).FirstOrDefault
      If ROHR IsNot Nothing Then
        Dim City As OBLib.Maintenance.Locations.ReadOnly.ROCity = CommonData.Lists.ROCityList.Where(Function(d) CompareSafe(d.CityID, ROHR.CityID)).FirstOrDefault
        Me.CityID = City.CityID
        Me.CityCode = City.CityCode
        If CompareSafe(VenueCityID, ROHR.CityID) Then
          Me.Local = True
        Else
          Me.Local = False
        End If
      End If
      Me.HumanResourceID = HumanResourceID
      Me.HumanResource = HumanResource

      Dim phrl As List(Of ProductionHumanResource) = Me.GetParent.ProductionHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, Me.HumanResourceID)).ToList

      Me.Disciplines = ""
      Dim DisciplineIDs As List(Of Integer?) = phrl.Select(Function(d As ProductionHumanResource) d.DisciplineID).Distinct().ToList
      DisciplineIDs.ForEach(Sub(DisciplineID)
                              Me.Disciplines &= IIf(Me.Disciplines.Length = 0, "", ", ") & CommonData.Lists.RODisciplineList.Where(Function(d) d.DisciplineID = DisciplineID).FirstOrDefault.Discipline
                            End Sub)

      'HRProductionSchedule.Local = False
      'HRProductionSchedule.CityID = Nothing
      'HRProductionSchedule.CityCode = ""
      'HRProductionSchedule.CrewType = ""
      'HRProductionSchedule.CrewTypeID = ""

    End Sub

    Public Sub CheckPreAndPostTravel()

      If SystemID = CType(CommonData.Enums.System.ProductionServices, Integer) And ProductionAreaID = CType(CommonData.Enums.ProductionArea.OB, Integer) Then

        Dim HasPreTravel As Boolean = False
        Dim HasPostTravel As Boolean = False

        Dim PreTravelTypes As Integer() = {CommonData.Enums.ProductionTimelineType.VehiclePreTravel,
                                           CommonData.Enums.ProductionTimelineType.RigPreTravel,
                                           CommonData.Enums.ProductionTimelineType.OpsPreTravel,
                                           CommonData.Enums.ProductionTimelineType.DriverPreTravel}

        Dim PostTravelTypes As Integer() = {CommonData.Enums.ProductionTimelineType.VehiclePostTravel,
                                            CommonData.Enums.ProductionTimelineType.RigPostTravel,
                                            CommonData.Enums.ProductionTimelineType.OpsPostTravel,
                                            CommonData.Enums.ProductionTimelineType.DriverPostTravel}

        Dim list As List(Of HRProductionScheduleDetail) = Me.HRProductionScheduleDetailList.OrderBy(Function(f) f.StartDateTime).ToList

        Dim First As Boolean = True
        For Each item As HRProductionScheduleDetail In list
          If First Then
            First = False
            If PreTravelTypes.Contains(item.ProductionTimelineTypeID) Then
              HasPreTravel = True
            End If
          End If
          If Not First And item Is list.Last Then
            If PostTravelTypes.Contains(item.ProductionTimelineTypeID) Then
              HasPostTravel = True
            End If
          End If
        Next

        If Local Then
          HasPreAndPostTravel = True
        Else
          If HasPreTravel AndAlso HasPostTravel Then
            HasPreAndPostTravel = True
          Else
            HasPreAndPostTravel = False
          End If
        End If

      Else
        HasPreAndPostTravel = True
      End If

    End Sub

    Public Sub PopulateMissingBiometricsShifts()

      If Me.IsOBs Then
        If Not GetParent.GetParent.Reconciled Then
          Dim ShiftDay = (From data In HRProductionScheduleDetailList.FilterOutTravelTimelines
                                     Group By Day = data.TimesheetDate.Value.Date, HRID = HumanResourceID Into Group, Count()
                                     Select New With {
                                                      Key .Day = Day,
                                                          .HumanResourceID = HRID,
                                                      Key .TotalCount = Count
                                                    })

          For Each item In ShiftDay
            Dim Ashift = Me.AccessShiftList.GetItem(item.Day)
            If Ashift Is Nothing Then
              Dim BShift = Me.AccessShiftList.AddNew
              With BShift
                .ShiftDate = item.Day
                .BiometricsMissingShiftDate = item.Day
                .HumanResourceID = item.HumanResourceID
                .ProductionID = Me.ProductionID
                If GetParent.GetParent.Reconciled Then
                  .MustValidateAccessShift = False
                End If
                .AccessFlagID = CommonData.Enums.AccessFlag.Biometrics_Missing
              End With
              BShift.CheckRules()
            End If
          Next
        End If
      End If
    End Sub

    Public Sub UpdateHRProductionSchedule()
      Update()

    End Sub
#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      AddWebRule(AllClashesProperty,
                 Function(d) d.AllClashes <> "",
                 Function(d) d.AllClashes)

      With AddWebRule(HasPreAndPostTravelProperty,
                      Function(d) Not d.Local And Not d.HasPreAndPostTravel,
                      Function(d) "Missing Pre and Post Travel, days away could be generated")
        .Severity = Singular.Rules.RuleSeverity.Warning
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHRProductionSchedule() method.

    End Sub

    Public Shared Function NewHRProductionSchedule() As HRProductionSchedule

      Return DataPortal.CreateChild(Of HRProductionSchedule)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetHRProductionSchedule(dr As SafeDataReader) As HRProductionSchedule

      Dim h As New HRProductionSchedule()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceProperty, .GetString(1))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(AllClashesProperty, .GetString(6))
          LoadProperty(CityIDProperty, .GetInt32(7))
          LoadProperty(CityCodeProperty, .GetString(8))
          LoadProperty(LocalProperty, .GetBoolean(9))
          LoadProperty(CrewTypeProperty, .GetString(10))
          LoadProperty(DisciplinesProperty, .GetString(11))
          LoadProperty(HasPreAndPostTravelProperty, .GetBoolean(12))
          LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(CoreCrewIndServerProperty, .GetBoolean(15))
          LoadProperty(CancelledDateTimeProperty, .GetValue(16))

          IsRemoved = Not IsNullNothing(CancelledDateTime)
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insHRProductionSchedule"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updHRProductionSchedule"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then
      '  With cm
      '    .CommandType = CommandType.StoredProcedure
      '    'Dim paramHumanResourceID As SqlParameter = .Parameters.Add("@HumanResourceID", SqlDbType.Int)
      '    'paramHumanResourceID.Value = GetProperty(HumanResourceIDProperty)
      '    'If Me.IsNew Then
      '    '  paramHumanResourceID.Direction = ParameterDirection.Output
      '    'End If
      '    '.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      '    '.Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))
      '    '.Parameters.AddWithValue("@AllClashes", GetProperty(AllClashesProperty))
      '    .ExecuteNonQuery()
      '    'If Me.IsNew Then
      '    '  LoadProperty(HumanResourceIDProperty, paramHumanResourceID.Value)
      '    'End If
      '    ' update child objects
      '    If GetProperty(HRProductionScheduleDetailListProperty) IsNot Nothing Then
      '      Me.HRProductionScheduleDetailList.Update()
      '    End If
      '    MarkOld()
      '  End With
      'Else
      ' update child objects
      If GetProperty(HRProductionScheduleDetailListProperty) IsNot Nothing Then
        Me.HRProductionScheduleDetailList.Update()
      End If
      'If GetProperty(AccessShiftListProperty) IsNot Nothing Then
      '  Me.AccessShiftList.Update()
      'End If
      'If GetProperty(AccessHRBookingListProperty) IsNot Nothing Then
      '  Me.AccessHRBookingList.Update()
      'End If

      MarkOld()
      'End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Me.HRProductionScheduleDetailList.DODeletes()
      'For Each csd As HRProductionScheduleDetail In Me.HRProductionScheduleDetailList
      '  csd.DeleteSelf()
      'Next

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delHRProductionSchedule"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      '  cm.Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
      '  cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      '  cm.Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub
      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace