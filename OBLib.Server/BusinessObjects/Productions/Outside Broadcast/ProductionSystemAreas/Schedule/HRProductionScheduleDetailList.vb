﻿' Generated 21 Jul 2014 14:59 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Schedules

  <Serializable()> _
  Public Class HRProductionScheduleDetailList
    Inherits SingularBusinessListBase(Of HRProductionScheduleDetailList, HRProductionScheduleDetail)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As HRProductionScheduleDetail

      For Each child As HRProductionScheduleDetail In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByProductionTimelineID(ProductionTimelineID As Integer) As HRProductionScheduleDetail

      For Each child As HRProductionScheduleDetail In Me
        If child.ProductionTimelineID = ProductionTimelineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByProductionTimeline(ProductionTimeline As Areas.ProductionTimeline) As HRProductionScheduleDetail

      For Each child As HRProductionScheduleDetail In Me
        If child.ProductionTimeline Is ProductionTimeline Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByGuid(Guid As Guid) As HRProductionScheduleDetail

      For Each child As HRProductionScheduleDetail In Me
        If child.Guid = Guid Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "HR Production Schedule Details"

    End Function

    Public Overloads Function Contains(ProductionHumanResource As Areas.ProductionHumanResource, ProductionTimeline As Areas.ProductionTimeline, _
                                       StartDate As Date, EndDate As Date) As Boolean

      For Each item As HRProductionScheduleDetail In Me
        If ((ProductionHumanResource.IsNew AndAlso ProductionHumanResource Is item.ProductionHumanResource) OrElse (Not ProductionHumanResource.IsNew AndAlso Singular.Misc.CompareSafe(item.HumanResourceID, ProductionHumanResource.HumanResourceID))) AndAlso _
          ((ProductionTimeline.IsNew AndAlso ProductionTimeline Is item.ProductionTimeline) OrElse (Not ProductionTimeline.IsNew AndAlso Singular.Misc.CompareSafe(item.ProductionTimelineTypeID, ProductionTimeline.ProductionTimelineTypeID))) AndAlso _
          CDate(item.StartDateTime).Equals(StartDate) AndAlso CDate(item.EndDateTime).Equals(EndDate) Then
          Return True
        End If
      Next
      Return False

    End Function

    Public Overloads Function Contains(TimelineGuid As Guid) As Boolean

      For Each item As HRProductionScheduleDetail In Me
        If item.ProductionTimeline.Guid = TimelineGuid Then
          Return True
        End If
      Next
      Return False

    End Function

    Public Sub RemoveScheduledItem(Guid As String)
      Dim GID As Guid = New Guid(Guid)
      Dim ItemToRemove As HRProductionScheduleDetail = Me.GetItemByGuid(GID)
      If ItemToRemove IsNot Nothing Then
        Me.Remove(ItemToRemove)
      End If
    End Sub

    Public Function GetScheduleStartTimeForBiometrics(TimesheetDate As Date) As DateTime


      Dim StartDateTime = FilterOutTravelTimelines.Where(Function(csd) csd.TimesheetDate = TimesheetDate).Min(Function(s) s.StartDateTime)
      Return StartDateTime
    End Function


    Public Function GetScheduleEndTimeForBiometrics(TimeSheetDate As Date) As DateTime


      Dim EndDateTime = FilterOutTravelTimelines.Where(Function(csd) csd.TimesheetDate = TimeSheetDate).Max(Function(s) s.EndDateTime)


      Return EndDateTime
    End Function

    Public Function FilterOutTravelTimelines() As List(Of HRProductionScheduleDetail)
      Dim ptls = New Integer() {1, 2, 3, 12, 13, 14, 25, 26, 32, 33, 34, 35, 36, 37}
      Dim List = New List(Of HRProductionScheduleDetail)
      For Each Item As HRProductionScheduleDetail In Me
        If Not ptls.Contains(Item.ProductionTimelineTypeID) Then
          List.Add(Item)
        End If
      Next

      Return List
    End Function
#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewHRProductionScheduleDetailList() As HRProductionScheduleDetailList

      Return New HRProductionScheduleDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

    Public Sub DODeletes()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As HRProductionScheduleDetail In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As HRProductionScheduleDetail In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As HRProductionScheduleDetail In Me
          If Child.CanEditScheduleCheck() Then
            If Child.IsNew Then
              Child.Insert()
            Else
              Child.Update()
            End If
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace