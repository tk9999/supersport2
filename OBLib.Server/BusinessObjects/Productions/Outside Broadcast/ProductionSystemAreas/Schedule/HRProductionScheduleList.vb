﻿' Generated 21 Jul 2014 14:58 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Areas

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Schedules

  <Serializable()> _
  Public Class HRProductionScheduleList
    Inherits SingularBusinessListBase(Of HRProductionScheduleList, HRProductionSchedule)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As HRProductionSchedule

      For Each child As HRProductionSchedule In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

    Public Function GetHRProductionScheduleDetail(HumanResourceID As Integer) As HRProductionScheduleDetail

      Dim obj As HRProductionScheduleDetail = Nothing
      For Each parent As HRProductionSchedule In Me
        obj = parent.HRProductionScheduleDetailList.GetItem(HumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionSystemAreaID As Integer? = Nothing
      Public Property ProductionID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing
      Public Property ProductionHumanResourceList As ProductionHumanResourceList = Nothing
      Public Property ProductionTimelineList As ProductionTimelineList = Nothing

      Public Sub New(ProductionSystemAreaID As Integer?,
                     ProductionID As Integer?,
                     SystemID As Integer?,
                     ProductionAreaID As Integer?,
                     HumanResourceID As Integer?,
                     ProductionHumanResourceList As ProductionHumanResourceList,
                     ProductionTimelineList As ProductionTimelineList)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
        Me.ProductionID = ProductionID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.HumanResourceID = HumanResourceID
        Me.ProductionHumanResourceList = ProductionHumanResourceList
        Me.ProductionTimelineList = ProductionTimelineList
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewHRProductionScheduleList() As HRProductionScheduleList

      Return New HRProductionScheduleList()

    End Function

    Public Shared Sub BeginGetHRProductionScheduleList(CallBack As EventHandler(Of DataPortalResult(Of HRProductionScheduleList)))

      Dim dp As New DataPortal(Of HRProductionScheduleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetHRProductionScheduleList() As HRProductionScheduleList

      Return DataPortal.Fetch(Of HRProductionScheduleList)(New Criteria())

    End Function

    Public Shared Function GetHRProductionScheduleList(ProductionSystemAreaID As Integer?,
                                                       ProductionID As Integer?,
                                                       SystemID As Integer?,
                                                       ProductionAreaID As Integer?,
                                                       HumanResourceID As Integer?,
                                                       ProductionHumanResourceList As ProductionHumanResourceList,
                                                       ProductionTimelineList As ProductionTimelineList) As HRProductionScheduleList

      Return DataPortal.Fetch(Of HRProductionScheduleList)(New Criteria(ProductionSystemAreaID,
                                                                        ProductionID,
                                                                        SystemID,
                                                                        ProductionAreaID,
                                                                        HumanResourceID,
                                                                        ProductionHumanResourceList,
                                                                        ProductionTimelineList))

    End Function

    Private Sub Fetch(sdr As SafeDataReader,
                      ProductionHumanResourceList As ProductionHumanResourceList,
                      ProductionTimelineList As ProductionTimelineList)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(HRProductionSchedule.GetHRProductionSchedule(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parentProductionHumanResource As ProductionHumanResource = Nothing
      Dim parentProductionTimeline As ProductionTimeline = Nothing

      Dim parent As HRProductionSchedule = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          If IsNothing(parentProductionHumanResource) OrElse parentProductionHumanResource.ProductionHumanResourceID <> sdr.GetInt32(2) Then
            parentProductionHumanResource = ProductionHumanResourceList.GetItem(sdr.GetInt32(2))
          End If
          If IsNothing(parentProductionTimeline) OrElse parentProductionTimeline.ProductionTimelineID <> sdr.GetInt32(3) Then
            parentProductionTimeline = ProductionTimelineList.GetItem(sdr.GetInt32(3))
          End If
          parent.HRProductionScheduleDetailList.RaiseListChangedEvents = False
          parent.HRProductionScheduleDetailList.Add(HRProductionScheduleDetail.GetHRProductionScheduleDetail(sdr, parentProductionHumanResource, parentProductionTimeline))
          parent.HRProductionScheduleDetailList.RaiseListChangedEvents = True
          parentProductionHumanResource = Nothing
          parentProductionTimeline = Nothing
        End While
      End If

      parent = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If IsNothing(parent) OrElse parent.HumanResourceID <> sdr.GetInt32(2) Then
            parent = Me.GetItem(sdr.GetInt32(2))
          End If
          If parent IsNot Nothing Then
            parent.AccessShiftList.RaiseListChangedEvents = False
            parent.AccessShiftList.Add(Biometrics.AccessShift.GetAccessShift(sdr, OBLib.CommonData.Enums.AccessTerminalGroup.Outside_Broadcast))
            parent.AccessShiftList.RaiseListChangedEvents = True
          End If
        End While
      End If

      For Each child As HRProductionSchedule In Me
        child.CheckRules()
        For Each HRProductionScheduleDetail As HRProductionScheduleDetail In child.HRProductionScheduleDetailList
          HRProductionScheduleDetail.CheckRules()
        Next
        child.CheckPreAndPostTravel()
        child.PopulateMissingBiometricsShifts()
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHRProductionScheduleList"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr, crit.ProductionHumanResourceList, crit.ProductionTimelineList)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        '' Loop through each deleted child object and call its Update() method
        For Each Child As HRProductionSchedule In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        '' Loop through each non-deleted child object and call its Update() method
        For Each Child As HRProductionSchedule In Me
          'If Child.IsNew Then
          '  Child.Insert()
          'Else
          Child.Update()
          'End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace