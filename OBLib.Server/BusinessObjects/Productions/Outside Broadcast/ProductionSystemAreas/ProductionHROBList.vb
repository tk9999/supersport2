﻿' Generated 18 Jun 2015 18:34 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Resources
Imports OBLib.Productions.Base

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Productions.OB

  <Serializable()> _
  Public Class ProductionHROBList
    Inherits OBBusinessListBase(Of ProductionHROBList, ProductionHROB)

#Region " Business Methods "

    Public Function GetItem(ProductionHRID As Integer) As ProductionHROB

      For Each child As ProductionHROB In Me
        If child.ProductionHRID = ProductionHRID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByGuid(ProductionHRGuid As Guid) As ProductionHROB

      For Each child As ProductionHROB In Me
        If child.Guid = ProductionHRGuid Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByHumanResourceID(HumanResourceID As Integer) As ProductionHROB

      For Each child As ProductionHROB In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByResourceID(ResourceID As Integer) As ProductionHROB

      For Each child As ProductionHROB In Me
        If child.ResourceID = ResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetOBBooking(ResourceBookingID As Integer) As ResourceBookingOB

      For Each child As ProductionHROB In Me
        For Each child2 As ResourceBookingOB In child.ResourceBookingOBList
          If child2.ResourceBookingID = ResourceBookingID Then
            Return child2
          End If
        Next
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production H Rs"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      'Public Property ProductionHumanResourceID As Integer?
      Public Property ProductionSystemAreaID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing
      'Public Property HumanResource As String = ""
      'Public Property SystemID As Integer?
      'Public Property ProductionHRID As Integer?

      Public Sub New(ProductionSystemAreaID As Integer?, HumanResourceID As Integer?)
        'Me.ProductionHumanResourceID = ProductionHumanResourceID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
        Me.HumanResourceID = HumanResourceID
        'Me.HumanResource = HumanResource
        'Me.SystemID = SystemID
        'Me.ProductionHRID = ProductionHRID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionHROBList() As ProductionHROBList

      Return New ProductionHROBList()

    End Function

    Public Shared Sub BeginGetProductionHROBList(CallBack As EventHandler(Of DataPortalResult(Of ProductionHROBList)))

      Dim dp As New DataPortal(Of ProductionHROBList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionHROBList() As ProductionHROBList

      Return DataPortal.Fetch(Of ProductionHROBList)(New Criteria())

    End Function

    Public Shared Function GetProductionHROBList(ProductionSystemAreaID As Integer?, HumanResourceID As Integer?) As ProductionHROBList

      Return DataPortal.Fetch(Of ProductionHROBList)(New Criteria(ProductionSystemAreaID, HumanResourceID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      ''Production HR
      'Me.RaiseListChangedEvents = False
      'While sdr.Read
      '  Me.Add(ProductionHROB.GetProductionHROB(sdr))
      'End While
      'Me.RaiseListChangedEvents = True

      ''Resource Bookings
      'Dim parent As ProductionHROB = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.ProductionHRID <> sdr.GetInt32(0) Then
      '      parent = Me.GetItem(sdr.GetInt32(0))
      '    End If
      '    parent.ResourceBookingOBList.RaiseListChangedEvents = False
      '    parent.ResourceBookingOBList.Add(ResourceBookingOB.GetResourceBookingOB(sdr))
      '    parent.ResourceBookingOBList.RaiseListChangedEvents = True
      '  End While
      'End If

      ''Crew Schedule Details
      'Dim parentResourceBooking As ResourceBookingOB = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentResourceBooking Is Nothing OrElse (parentResourceBooking.ResourceBookingID <> sdr.GetInt32(26)) Then
      '      parentResourceBooking = Me.GetOBBooking(sdr.GetInt32(26))
      '    End If
      '    parentResourceBooking.CrewScheduleDetailOBList.RaiseListChangedEvents = False
      '    parentResourceBooking.CrewScheduleDetailOBList.Add(CrewScheduleDetailOB.GetCrewScheduleDetailOB(sdr, ))
      '    parentResourceBooking.CrewScheduleDetailOBList.RaiseListChangedEvents = True
      '  End While
      'End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionHROBList"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(crit.ProductionSystemAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        DeleteBulk()
        DeletedList.Clear()
        ' Loop through each deleted child object and call its Update() method
        'For Each Child As ProductionHROB In DeletedList
        '  For Each rb As ResourceBookingOB In Child.ResourceBookingOBList
        '    'For Each cs As CrewScheduleDetailOB In rb.CrewScheduleDetailOBList
        '    '  cs.DeleteSelf()
        '    'Next
        '    rb.DeleteSelf()
        '  Next
        '  Child.DeleteSelf()
        'Next
        'Me.DeleteBulk()
        For Each Child As ProductionHROB In DeletedList
          Child.ResourceBookingOBList.Clear()
        Next
        DeleteBulk()

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionHROB In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub



    Private Function CreateProductionHRTableParameter() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ProductionHRID", GetType(Int32)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each csd As ProductionHROB In Me.DeletedList
        If Not csd.IsNew Then
          Dim row As DataRow = RBTable.NewRow
          row("ProductionHRID") = NothingDBNull(csd.ProductionHRID)
          RBTable.Rows.Add(row)
        End If
      Next

      Return RBTable

    End Function

    Private Sub DeleteBulk()

      Dim dt As DataTable = CreateProductionHRTableParameter()
      Dim cm As New Singular.CommandProc("DelProcs.delCrewScheduleDetailBulk")
      cm.Parameters.AddWithValue("@ProductionHRBulk", dt, SqlDbType.Structured)
      cm.UseTransaction = True
      cm.Execute()
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delProductionHRBulk"
      '  cm.CommandType = CommandType.StoredProcedure
      '  'Dim sqlP As SqlClient.SqlParameter = New SqlClient.SqlParameter("@ProductionHRBulk", dt)
      '  'sqlP.SqlDbType = SqlDbType.Structured
      '  cm.Parameters.AddWithValue("@ProductionHRBulk", dt)
      '  cm.Connection = CType(Csla.ApplicationContext.LocalContext("cn"), SqlClient.SqlConnection)
      '  cm.Transaction = Csla.ApplicationContext.LocalContext("tr")
      '  Singular.CSLALib.ContextInfo.SetContextInfoOnConnection(cm.Connection, cm.Transaction)
      '  cm.ExecuteNonQuery()
      'End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace