﻿' Generated 13 Jun 2015 16:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions
Imports OBLib.Productions.OB

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Resources

  <Serializable()> _
  Public Class ResourceBookingOBList
    Inherits ResourceBookingBaseList(Of ResourceBookingOBList, ResourceBookingOB)
    'Implements IResourceBookingList

#Region " Business Methods "

    'Public Function GetItem(ResourceBookingID As Integer) As ResourceBooking

    '  For Each child As ResourceBooking In Me
    '    If child.ResourceBookingID = ResourceBookingID Then
    '      Return child
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    Public Function CreateNew() As ResourceBookingOB

      Dim tmp As New ResourceBookingOB
      Me.Add(tmp)
      Return tmp

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Bookings"

    End Function

    'Public Function GetItemByProductionTimeline(ProductionTimeline As Areas.ProductionTimeline) As CrewScheduleDetailOB

    '  For Each rb As ResourceBookingOB In Me
    '    For Each child As CrewScheduleDetailOB In rb.CrewScheduleDetailOBList
    '      If child.ProductionTimeline Is ProductionTimeline Then
    '        Return child
    '      End If
    '    Next
    '  Next
    '  Return Nothing

    'End Function

    'Public Sub UpdateClashes()
    '  'Dim tbl As Helpers.ResourceHelpers.ResourceBooking = 
    'End Sub

    'Public Function GetResourceBookingHelperList() As List(Of Helpers.ResourceHelpers.ResourceBooking) Implements IResourceBookingList.GetResourceBookingHelperList
    '  Dim l As New List(Of Helpers.ResourceHelpers.ResourceBooking)
    '  For Each rb As ResourceBookingOB In Me
    '    l.Add(rb.GetResourceBookingHelper())
    '  Next
    '  Return l
    'End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ResourceBookingID As Integer? = Nothing
      Public Property ResourceIDs As String = ""
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing

      Public Sub New(ResourceIDs As String, StartDate As DateTime?, EndDate As DateTime?)
        Me.ResourceIDs = ResourceIDs
        Me.StartDate = StartDate
        Me.EndDate = EndDate
      End Sub

      Public Sub New(ResourceBookingID As Integer?)
        Me.ResourceBookingID = ResourceBookingID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewResourceBookingOBList() As ResourceBookingOBList

      Return New ResourceBookingOBList()

    End Function

    Public Shared Sub BeginGetResourceBookingList(CallBack As EventHandler(Of DataPortalResult(Of ResourceBookingOBList)))

      Dim dp As New DataPortal(Of ResourceBookingOBList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetResourceBookingOBList(ResourceBookingID As Integer?) As ResourceBookingOBList

      Return DataPortal.Fetch(Of ResourceBookingOBList)(New Criteria(ResourceBookingID))

    End Function

    Public Shared Function GetResourceBookingOBList(ResourceIDs As String, StartDate As DateTime?, EndDate As DateTime?) As ResourceBookingOBList

      Return DataPortal.Fetch(Of ResourceBookingOBList)(New Criteria(ResourceIDs, StartDate, EndDate))

    End Function

    Protected Overrides Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ResourceBookingOB.GetResourceBookingOB(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getResourceBookingList"
            cm.Parameters.AddWithValue("@ResourceIDs", Strings.MakeEmptyDBNull(crit.ResourceIDs))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@ResourceBookingID", NothingDBNull(crit.ResourceBookingID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    'Friend Sub Update()

    '  Me.RaiseListChangedEvents = False
    '  Try
    '    ' Loop through each deleted child object and call its Update() method
    '    For Each Child As ResourceBooking In DeletedList
    '      Child.DeleteSelf()
    '    Next

    '    ' Then clear the list of deleted objects because they are truly gone now.
    '    DeletedList.Clear()

    '    ' Loop through each non-deleted child object and call its Update() method
    '    For Each Child As ResourceBooking In Me
    '      If Child.IsNew Then
    '        Child.Insert()
    '      Else
    '        Child.Update()
    '      End If
    '    Next
    '  Finally
    '    Me.RaiseListChangedEvents = True
    '  End Try

    'End Sub

    'Protected Overrides Sub DataPortal_Update()

    '  UpdateTransactional(AddressOf Update)

    'End Sub

    Public Overrides Sub FetchChildLists(sdr As SafeDataReader)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace