﻿' Generated 26 Jun 2014 17:54 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Areas.ReadOnly

  <Serializable()> _
  Public Class ROProductionTimelineList
    Inherits SingularReadOnlyListBase(Of ROProductionTimelineList, ROProductionTimeline)

#Region " Business Methods "

    Public Function GetItem(ProductionTimelineID As Integer) As ROProductionTimeline

      For Each child As ROProductionTimeline In Me
        If child.ProductionTimelineID = ProductionTimelineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Timelines"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing

      Public Sub New(ProductionID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?)
        Me.ProductionID = ProductionID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionTimelineList() As ROProductionTimelineList

      Return New ROProductionTimelineList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionTimelineList(ProductionID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?) As ROProductionTimelineList

      Return DataPortal.Fetch(Of ROProductionTimelineList)(New Criteria(ProductionID, SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionTimeline.GetROProductionTimeline(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionTimelineList"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace