﻿' Generated 02 Jul 2014 10:21 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Areas.ReadOnly

  <Serializable()> _
  Public Class ROAllowedAreaStatusList
    Inherits SingularReadOnlyListBase(Of ROAllowedAreaStatusList, ROAllowedAreaStatus)

#Region " Business Methods "

    Public Function GetItem(StatusMatrixID As Integer) As ROAllowedAreaStatus

      For Each child As ROAllowedAreaStatus In Me
        If child.StatusMatrixID = StatusMatrixID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property CurrentStatusID As Integer? = Nothing

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?, CurrentStatusID As Integer?)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.CurrentStatusID = CurrentStatusID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROAllowedAreaStatusList() As ROAllowedAreaStatusList

      Return New ROAllowedAreaStatusList()

    End Function

    Public Shared Sub BeginGetROAllowedAreaStatusList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROAllowedAreaStatusList)))

      Dim dp As New DataPortal(Of ROAllowedAreaStatusList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROAllowedAreaStatusList(CallBack As EventHandler(Of DataPortalResult(Of ROAllowedAreaStatusList)))

      Dim dp As New DataPortal(Of ROAllowedAreaStatusList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROAllowedAreaStatusList(SystemID As Integer, ProductionAreaID As Integer, CurrentStatusID As Integer) As ROAllowedAreaStatusList

      Return DataPortal.Fetch(Of ROAllowedAreaStatusList)(New Criteria(SystemID, ProductionAreaID, CurrentStatusID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROAllowedAreaStatus.GetROAllowedAreaStatus(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROAllowedAreaStatusList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@CurrentStatusID", NothingDBNull(crit.CurrentStatusID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace