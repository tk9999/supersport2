﻿' Generated 26 Jun 2014 17:54 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Areas.ReadOnly

  <Serializable()> _
  Public Class ROProductionSystemAreaList
    Inherits SingularReadOnlyListBase(Of ROProductionSystemAreaList, ROProductionSystemArea)

#Region " Business Methods "

    Public Function GetItem(ProductionSystemAreaID As Integer) As ROProductionSystemArea

      For Each child As ROProductionSystemArea In Me
        If child.ProductionSystemAreaID = ProductionSystemAreaID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production System Areas"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Integer? = Nothing

      Public Sub New(ProductionID As Integer?)
        Me.ProductionID = ProductionID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function GetROProductionSystemAreaList(ProductionID As Integer?) As ROProductionSystemAreaList

      Return DataPortal.Fetch(Of ROProductionSystemAreaList)(New Criteria(ProductionID))

    End Function

    Public Shared Function NewROProductionSystemAreaList() As ROProductionSystemAreaList

      Return New ROProductionSystemAreaList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ROProductionSystemArea.GetROProductionSystemArea(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionSystemAreaList"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace