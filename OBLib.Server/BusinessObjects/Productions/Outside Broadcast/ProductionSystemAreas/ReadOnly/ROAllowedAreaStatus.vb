﻿' Generated 02 Jul 2014 10:21 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Productions.Areas.ReadOnly

  <Serializable()> _
  Public Class ROAllowedAreaStatus
    Inherits SingularReadOnlyBase(Of ROAllowedAreaStatus)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared StatusMatrixIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StatusMatrixID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property StatusMatrixID() As Integer?
      Get
        Return GetProperty(StatusMatrixIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared CurrProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CurrProductionAreaStatusID, "Curr Production Area Status", Nothing)
    ''' <summary>
    ''' Gets the Curr Production Area Status value
    ''' </summary>
    <Display(Name:="Curr Production Area Status", Description:="")>
    Public ReadOnly Property CurrProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(CurrProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared CurrentStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CurrentStatus, "Current Status")
    ''' <summary>
    ''' Gets the Current Status value
    ''' </summary>
    <Display(Name:="Current Status", Description:="")>
    Public ReadOnly Property CurrentStatus() As String
      Get
        Return GetProperty(CurrentStatusProperty)
      End Get
    End Property

    Public Shared NextStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NextStatus, "Next Status")
    ''' <summary>
    ''' Gets the Next Status value
    ''' </summary>
    <Display(Name:="Next Status", Description:="")>
    Public ReadOnly Property NextStatus() As String
      Get
        Return GetProperty(NextStatusProperty)
      End Get
    End Property

    Public Shared NxtProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NxtProductionAreaStatusID, "Nxt Production Area Status", Nothing)
    ''' <summary>
    ''' Gets the Nxt Production Area Status value
    ''' </summary>
    <Display(Name:="Nxt Production Area Status", Description:="")>
    Public ReadOnly Property NxtProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(NxtProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared BackwardIndProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BackwardInd, "Backward")
    ''' <summary>
    ''' Gets the Backward value
    ''' </summary>
    <Display(Name:="Backward", Description:="")>
    Public ReadOnly Property BackwardInd() As Integer
      Get
        Return GetProperty(BackwardIndProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(StatusMatrixIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CurrentStatus

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROAllowedAreaStatus(dr As SafeDataReader) As ROAllowedAreaStatus

      Dim r As New ROAllowedAreaStatus()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(StatusMatrixIDProperty, .GetInt32(0))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CurrProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(CurrentStatusProperty, .GetString(4))
        LoadProperty(NextStatusProperty, .GetString(5))
        LoadProperty(NxtProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(BackwardIndProperty, .GetInt32(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace