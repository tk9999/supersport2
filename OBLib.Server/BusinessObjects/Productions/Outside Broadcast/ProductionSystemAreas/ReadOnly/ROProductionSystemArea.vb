﻿' Generated 26 Jun 2014 17:54 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Areas.ReadOnly

  <Serializable()> _
  Public Class ROProductionSystemArea
    Inherits SingularReadOnlyBase(Of ROProductionSystemArea)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public ReadOnly Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "Production Area Status", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Production Area Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared StatusDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StatusDate, "Status Date")
    ''' <summary>
    ''' Gets the Status Date value
    ''' </summary>
    <Display(Name:="Status Date", Description:="")>
    Public ReadOnly Property StatusDate As DateTime?
      Get
        Return GetProperty(StatusDateProperty)
      End Get
    End Property

    Public Shared StatusSetByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.StatusSetByUserID, "Status Set By User", Nothing)
    ''' <summary>
    ''' Gets the Status Set By User value
    ''' </summary>
    <Display(Name:="Status Set By User", Description:="")>
    Public ReadOnly Property StatusSetByUserID() As Integer?
      Get
        Return GetProperty(StatusSetByUserIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DepartmentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Department, "Department", "")
    ''' <summary>
    ''' Gets and sets the Department value
    ''' </summary>
    <Display(Name:="Department", Description:="")>
    Public ReadOnly Property Department() As String
      Get
        Return GetProperty(DepartmentProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "Sub-Department")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Department", Description:="")>
    Public ReadOnly Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area")
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:=""),
    Required(ErrorMessage:="Area is required")>
    Public ReadOnly Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Area Status")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Area Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
    End Property

    Public Shared SetByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SetBy, "Set By")
    ''' <summary>
    ''' Gets and sets the Mod By value
    ''' </summary>
    <Display(Name:="Status Set By", Description:="")>
    Public ReadOnly Property SetBy() As String
      Get
        Return GetProperty(SetByProperty)
      End Get
    End Property

    Public Shared CreateByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreateBy, "Create By")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Created By", Description:="")>
    Public ReadOnly Property CreateBy() As String
      Get
        Return GetProperty(CreateByProperty)
      End Get
    End Property

    Public Shared ModByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ModBy, "Mod By")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Last Mod By", Description:="")>
    Public ReadOnly Property ModBy() As String
      Get
        Return GetProperty(ModByProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSystemAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionSystemArea(dr As SafeDataReader) As ROProductionSystemArea

      Dim r As New ROProductionSystemArea()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionSystemAreaIDProperty, .GetInt32(0))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(StatusDateProperty, .GetValue(5))
        LoadProperty(StatusSetByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(CreatedByProperty, .GetInt32(7))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
        LoadProperty(ModifiedByProperty, .GetInt32(9))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
        LoadProperty(DepartmentProperty, .GetString(11))
        LoadProperty(SystemProperty, .GetString(12))
        LoadProperty(ProductionAreaProperty, .GetString(13))
        LoadProperty(ProductionAreaStatusProperty, .GetString(14))
        LoadProperty(SetByProperty, .GetString(15))
        LoadProperty(CreateByProperty, .GetString(16))
        LoadProperty(ModByProperty, .GetString(17))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace