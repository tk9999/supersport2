﻿' Generated 30 Jun 2014 08:00 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Areas.ReadOnly

  <Serializable()> _
  Public Class ROProductionDisciplineTimelineTypeList
    Inherits SingularReadOnlyListBase(Of ROProductionDisciplineTimelineTypeList, ROProductionDisciplineTimelineType)

#Region " Business Methods "

    Public Function GetItem(DisciplineID As Integer) As ROProductionDisciplineTimelineType

      For Each child As ROProductionDisciplineTimelineType In Me
        If child.DisciplineID = DisciplineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionSystemAreaID As Integer? = Nothing

      Public Sub New(ProductionID As Integer?, SystemID As Integer?, ProductionSystemAreaID As Integer?)
        Me.ProductionID = ProductionID
        Me.SystemID = SystemID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionDisciplineTimelineTypeList() As ROProductionDisciplineTimelineTypeList

      Return New ROProductionDisciplineTimelineTypeList()

    End Function

    Public Shared Sub BeginGetROProductionDisciplineTimelineTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionDisciplineTimelineTypeList)))

      Dim dp As New DataPortal(Of ROProductionDisciplineTimelineTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionDisciplineTimelineTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionDisciplineTimelineTypeList)))

      Dim dp As New DataPortal(Of ROProductionDisciplineTimelineTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionDisciplineTimelineTypeList() As ROProductionDisciplineTimelineTypeList

      Return DataPortal.Fetch(Of ROProductionDisciplineTimelineTypeList)(New Criteria())

    End Function

    Public Shared Function GetROProductionDisciplineTimelineTypeList(ProductionID As Integer?, SystemID As Integer?, ProductionSystemAreaID As Integer?) As ROProductionDisciplineTimelineTypeList

      Return DataPortal.Fetch(Of ROProductionDisciplineTimelineTypeList)(New Criteria(ProductionID, SystemID, ProductionSystemAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionDisciplineTimelineType.GetROProductionDisciplineTimelineType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionDisciplineTimelineTypeList"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace