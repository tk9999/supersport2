﻿' Generated 27 Jun 2014 09:44 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.ReadOnly

  <Serializable()> _
  Public Class ROSharedSystemAreaTimelineList
    Inherits SingularReadOnlyListBase(Of ROSharedSystemAreaTimelineList, ROSharedSystemAreaTimeline)

#Region " Business Methods "

    Public Function GetItem(SharedSystemAreaTimelineID As Integer) As ROSharedSystemAreaTimeline

      For Each child As ROSharedSystemAreaTimeline In Me
        If child.SharedSystemAreaTimelineID = SharedSystemAreaTimelineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Shared System Area Timelines"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSharedSystemAreaTimelineList() As ROSharedSystemAreaTimelineList

      Return New ROSharedSystemAreaTimelineList()

    End Function

    Public Shared Sub BeginGetROSharedSystemAreaTimelineList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSharedSystemAreaTimelineList)))

      Dim dp As New DataPortal(Of ROSharedSystemAreaTimelineList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    Public Shared Sub BeginGetROSharedSystemAreaTimelineList(CallBack As EventHandler(Of DataPortalResult(Of ROSharedSystemAreaTimelineList)))

      Dim dp As New DataPortal(Of ROSharedSystemAreaTimelineList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSharedSystemAreaTimelineList() As ROSharedSystemAreaTimelineList

      Return DataPortal.Fetch(Of ROSharedSystemAreaTimelineList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSharedSystemAreaTimeline.GetROSharedSystemAreaTimeline(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSharedSystemAreaTimelineList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace