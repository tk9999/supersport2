﻿' Generated 30 Jun 2014 10:57 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Productions.Services
Imports OBLib.Productions.Schedules
Imports System.Web
Imports OBLib.Productions.Specs.Old
Imports OBLib.Productions.Specs.ReadOnly.Old
Imports OBLib.Productions.Vehicles
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Productions.OB
Imports OBLib.Productions.Old

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Areas

  <Serializable()> _
  Public Class ProductionSystemArea
    Inherits SingularBusinessBase(Of ProductionSystemArea)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key()>
    Public Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(ProductionSystemAreaIDProperty, value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Department", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Department", Description:=""),
    DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), DropDownType:=DropDownWeb.SelectType.Combo,
                Source:=DropDownWeb.SourceType.ViewModel),
    Required(ErrorMessage:="Sub-Dept required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROProductionAreaList), DropDownType:=DropDownWeb.SelectType.Combo,
                Source:=DropDownWeb.SourceType.ViewModel),
    Required(ErrorMessage:="Area required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, Nothing) _
                                                                                .AddSetExpression(OBLib.JSCode.ProductionSystemAreaJS.ProductionStatusIDChanged, False)
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:=""),
    DropDownWeb(GetType(ROProductionAreaAllowedStatusList), DropDownType:=DropDownWeb.SelectType.Combo,
                FilterMethodName:="GetAllowedStatuses", ValueMember:="ProductionAreaStatusID", DisplayMember:="ProductionAreaStatus",
                Source:=DropDownWeb.SourceType.ViewModel),
    Required(ErrorMessage:="Status required")>
    Public Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property
    'DropDownWeb(GetType(OBLib.Maintenance.Productions.Areas.ReadOnly.ROAllowedAreaStatusList),
    '            DisplayMember:="ProductionAreaStatus", ValueMember:="ProductionAreaStatusID", Source:=DropDownWeb.SourceType.ViewModel, FilterMethodName:="FilterAllowedAreaStatuses")

    Public Shared StatusDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StatusDate, "Status Date")
    ''' <summary>
    ''' Gets and sets the Status Date value
    ''' </summary>
    <Display(Name:="Status Date", Description:=""),
    DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public Property StatusDate As DateTime?
      Get
        Return GetProperty(StatusDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StatusDateProperty, Value)
      End Set
    End Property

    Public Shared StatusSetByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.StatusSetByUserID, "Set By", OBLib.Security.Settings.CurrentUserID)
    ''' <summary>
    ''' Gets and sets the Status Set By User value
    ''' </summary>
    <Display(Name:="Status Set By", Description:=""),
    Required(ErrorMessage:="Status Set By required")>
    Public Property StatusSetByUserID() As Integer?
      Get
        Return GetProperty(StatusSetByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(StatusSetByUserIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(Name:="Date"),
    DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Date"),
    DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared Property ProductionSystemAreaClashesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionSystemAreaClashes, "Production System Area Clashes", "")
    ''' <summary>
    ''' Gets and sets the Create By value
    ''' </summary>
    <Display(Name:="Clashes", Description:="")>
    Public Property ProductionSystemAreaClashes() As String
      Get
        Return GetProperty(ProductionSystemAreaClashesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionSystemAreaClashesProperty, Value)
      End Set
    End Property

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionSpecRequirementID, Nothing) _
                                                                                     .AddSetExpression(OBLib.JSCode.ProductionSystemAreaJS.ProductionSpecRequirementIDSet, False)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Spec", Description:=""),
    DropDownWeb(GetType(ROProductionSpecRequirementListOld), DropDownType:=DropDownWeb.SelectType.Combo,
                FilterMethodName:="FilterProductionSpecRequirementList", ValueMember:="ProductionSpecRequirementID",
                DisplayMember:="ProductionSpecRequirementName", UnselectedText:="Select Spec")>
    Public Property ProductionSpecRequirementID() As Integer?
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSpecRequirementIDProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, OBLib.JSCode.ProductionSystemAreaJS.ProductionSystemAreaToString)

    Public Shared SpecNotCompliantProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SpecNotCompliant, "SpecNotCompliant", False)
    ''' <summary>
    ''' Gets and sets the Spec Name value
    ''' </summary>
    <Display(Name:="SpecNotCompliant", Description:="")>
    Public Property SpecNotCompliant() As Boolean
      Get
        Return GetProperty(SpecNotCompliantProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SpecNotCompliantProperty, Value)
      End Set
    End Property

    Public Shared NotComplySpecReasonsProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.NotComplySpecReasons, "") _
                                                                            .AddSetExpression(OBLib.JSCode.ProductionSystemAreaJS.NotComplySpecReasonsSet, False)
    ''' <summary>
    ''' Gets and sets the Not Comply Spec Reasons value
    ''' </summary>
    <Display(Name:="Not Comply Spec Reasons", Description:=""),
    StringLength(500, ErrorMessage:="Not Comply Spec Reasons cannot be more than 500 characters")>
    Public Property NotComplySpecReasons() As String
      Get
        Return GetProperty(NotComplySpecReasonsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotComplySpecReasonsProperty, Value)
      End Set
    End Property

    Public Shared StatusSetByNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.StatusSetByName, "")
    <Display(Name:="Set By")>
    Public Property StatusSetByName As String
      Get
        Return GetProperty(StatusSetByNameProperty)
      End Get
      Set(value As String)
        SetProperty(StatusSetByNameProperty, value)
      End Set
    End Property

    Public Shared CurrentUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CurrentUserID, "CurrentUserID", Nothing)
    ''' <summary>
    ''' Gets and sets the Status Set By User value
    ''' </summary>
    <Display(Name:="Status Set By User", Description:="")>
    Public ReadOnly Property CurrentUserID() As Integer?
      Get
        Return OBLib.Security.Settings.CurrentUserID
      End Get
    End Property

    Public Shared CurrentUserNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CurrentUserName, "CurrentUserName", "")
    ''' <summary>
    ''' Gets and sets the Status Set By User value
    ''' </summary>
    <Display(Name:="Current User Name", Description:="")>
    Public ReadOnly Property CurrentUserName() As String
      Get
        SetProperty(CurrentUserNameProperty, OBLib.Security.Settings.CurrentUser.FirstNameSurname)
        Return GetProperty(CurrentUserNameProperty)
      End Get
    End Property

    Public Shared LoadedProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.LoadedProductionAreaStatusID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property LoadedProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(LoadedProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared LoadedStatusDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LoadedStatusDate, "Status Date")
    ''' <summary>
    ''' Gets and sets the Status Date value
    ''' </summary>
    <Display(Name:="Status Date", Description:="")>
    Public ReadOnly Property LoadedStatusDate As DateTime?
      Get
        Return GetProperty(LoadedStatusDateProperty)
      End Get
    End Property

    Public Shared LoadedStatusSetByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LoadedStatusSetByUserID, "Set By", OBLib.Security.Settings.CurrentUserID)
    ''' <summary>
    ''' Gets and sets the Status Set By User value
    ''' </summary>
    <Display(Name:="Status Set By", Description:="")>
    Public ReadOnly Property LoadedStatusSetByUserID() As Integer?
      Get
        Return GetProperty(LoadedStatusSetByUserIDProperty)
      End Get
    End Property

    Public Shared StatusSetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StatusSetDate, "Status Set Date")
    ''' <summary>
    ''' Gets and sets the Room Schedule Status Set Date value
    ''' </summary>
    <Display(Name:="Status Set Date", Description:=""),
    DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public Property StatusSetDate As DateTime?
      Get
        Return GetProperty(StatusSetDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StatusSetDateProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "Set By", Nothing)
    ''' <summary>
    ''' Gets and sets the Status Set By User value
    ''' </summary>
    <Display(Name:="RoomScheduleID", Description:="")>
    Public ReadOnly Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    'Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Set By", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Status Set By User value
    ' ''' </summary>
    '<Display(Name:="RoomID", Description:="")>
    'Public ReadOnly Property RoomID() As Integer?
    '  Get
    '    Return GetProperty(RoomIDProperty)
    '  End Get
    'End Property

    Public Shared MealReimbursementProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MealReimbursement, "Meal Reimbursements?", False)
    ''' <summary>
    ''' Gets and sets the Spec Name value
    ''' </summary>
    <Display(Name:="Meal Reimbursements?", Description:="")>
    Public Property MealReimbursement() As Boolean
      Get
        Return GetProperty(MealReimbursementProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(MealReimbursementProperty, Value)
      End Set
    End Property

    Public Shared IsBeingEditedBySomeoneElseProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsBeingEditedBySomeoneElse, "Is Being Edited By", False)
    ''' <summary>
    ''' 
    ''' </summary>
    <Display(Name:="IsBeingEditedBy", Description:="")>
    Public ReadOnly Property IsBeingEditedBySomeoneElse() As Boolean
      Get
        Return GetProperty(IsBeingEditedBySomeoneElseProperty)
      End Get
    End Property

    Public Shared InEditByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.InEditByName, "InEditByName")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="InEditByName", Description:="")>
    Public ReadOnly Property InEditByName() As String
      Get
        Return GetProperty(InEditByNameProperty)
      End Get
    End Property

    Public Shared InEditDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.InEditDateTime)
    ''' <summary>
    ''' 
    ''' </summary>
    <Display(Name:="IsBeingEditedBy", Description:="")>
    Public ReadOnly Property InEditDateTime() As DateTime?
      Get
        Return GetProperty(InEditDateTimeProperty)
      End Get
    End Property

    Public Shared EditImagePathProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EditImagePath, "EditImagePath")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="EditImagePath", Description:="")>
    Public ReadOnly Property EditImagePath() As String
      Get
        Return GetProperty(EditImagePathProperty)
      End Get
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Int32?) = RegisterProperty(Of Int32?)(Function(c) c.ResourceBookingID, "ResourceBookingID", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="ResourceBookingID", Description:="")>
    Public ReadOnly Property ResourceBookingID() As Int32?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
    End Property

    Public Shared ChecklTravelTimelineRuleProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ChecklTravelTimelineRule, "ChecklTravelTimelineRule", False)
    <Display(Description:="")>
    Public Property ChecklTravelTimelineRule() As Boolean
      Get
        Return GetProperty(ChecklTravelTimelineRuleProperty)
      End Get
      Set(value As Boolean)
        SetProperty(ChecklTravelTimelineRuleProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionSpecRequirementEquipmentTypeListProperty As PropertyInfo(Of ProductionSpecRequirementEquipmentTypeList) = RegisterProperty(Of ProductionSpecRequirementEquipmentTypeList)(Function(c) c.ProductionSpecRequirementEquipmentTypeList, "Production Spec Requirement Equipment Type List")
    Public ReadOnly Property ProductionSpecRequirementEquipmentTypeList() As ProductionSpecRequirementEquipmentTypeList
      Get
        If GetProperty(ProductionSpecRequirementEquipmentTypeListProperty) Is Nothing Then
          LoadProperty(ProductionSpecRequirementEquipmentTypeListProperty, ProductionSpecRequirementEquipmentTypeList.NewProductionSpecRequirementEquipmentTypeList())
        End If
        Return GetProperty(ProductionSpecRequirementEquipmentTypeListProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementPositionListProperty As PropertyInfo(Of ProductionSpecRequirementPositionList) = RegisterProperty(Of ProductionSpecRequirementPositionList)(Function(c) c.ProductionSpecRequirementPositionList, "Production Spec Requirement Equipment Type List")
    Public ReadOnly Property ProductionSpecRequirementPositionList() As ProductionSpecRequirementPositionList
      Get
        If GetProperty(ProductionSpecRequirementPositionListProperty) Is Nothing Then
          LoadProperty(ProductionSpecRequirementPositionListProperty, ProductionSpecRequirementPositionList.NewProductionSpecRequirementPositionList())
        End If
        Return GetProperty(ProductionSpecRequirementPositionListProperty)
      End Get
    End Property

    Public Shared ProductionTimelineListProperty As PropertyInfo(Of ProductionTimelineList) = RegisterProperty(Of ProductionTimelineList)(Function(c) c.ProductionTimelineList, "Production Timeline List")
    Public ReadOnly Property ProductionTimelineList() As ProductionTimelineList
      Get
        If GetProperty(ProductionTimelineListProperty) Is Nothing Then
          LoadProperty(ProductionTimelineListProperty, Productions.Areas.ProductionTimelineList.NewProductionTimelineList())
        End If
        Return GetProperty(ProductionTimelineListProperty)
      End Get
    End Property

    Public Shared ProductionHumanResourceListProperty As PropertyInfo(Of ProductionHumanResourceList) = RegisterProperty(Of ProductionHumanResourceList)(Function(c) c.ProductionHumanResourceList, "Production Human Resource List")
    Public ReadOnly Property ProductionHumanResourceList() As ProductionHumanResourceList
      Get
        If GetProperty(ProductionHumanResourceListProperty) Is Nothing Then
          LoadProperty(ProductionHumanResourceListProperty, Productions.Areas.ProductionHumanResourceList.NewProductionHumanResourceList())
        End If
        Return GetProperty(ProductionHumanResourceListProperty)
      End Get
    End Property

    Public Shared ProductionOutsourceServiceListProperty As PropertyInfo(Of ProductionOutsourceServiceList) = RegisterProperty(Of ProductionOutsourceServiceList)(Function(c) c.ProductionOutsourceServiceList, "Production Outsource Service List")
    Public ReadOnly Property ProductionOutsourceServiceList() As ProductionOutsourceServiceList
      Get
        If GetProperty(ProductionOutsourceServiceListProperty) Is Nothing Then
          LoadProperty(ProductionOutsourceServiceListProperty, Productions.Services.ProductionOutsourceServiceList.NewProductionOutsourceServiceList())
        End If
        Return GetProperty(ProductionOutsourceServiceListProperty)
      End Get
    End Property

    Public Shared HRProductionScheduleListProperty As PropertyInfo(Of HRProductionScheduleList) = RegisterProperty(Of HRProductionScheduleList)(Function(c) c.HRProductionScheduleList, "HR Production Schedule List")
    Public ReadOnly Property HRProductionScheduleList() As HRProductionScheduleList
      Get
        If GetProperty(HRProductionScheduleListProperty) Is Nothing Then
          LoadProperty(HRProductionScheduleListProperty, Productions.Schedules.HRProductionScheduleList.NewHRProductionScheduleList())
        End If
        Return GetProperty(HRProductionScheduleListProperty)
      End Get
    End Property

    Public Shared RemovedHumanResourceListProperty As PropertyInfo(Of ProductionHumanResourceList) = RegisterProperty(Of ProductionHumanResourceList)(Function(c) c.RemovedHumanResourceList, "Removed Human Resource List")
    <Browsable(False)>
    Public ReadOnly Property RemovedHumanResourceList() As ProductionHumanResourceList
      Get
        If GetProperty(RemovedHumanResourceListProperty) Is Nothing Then
          LoadProperty(RemovedHumanResourceListProperty, Productions.Areas.ProductionHumanResourceList.NewProductionHumanResourceList())
        End If
        Return GetProperty(RemovedHumanResourceListProperty)
      End Get
    End Property

    Public Shared RecordChangeListProperty As PropertyInfo(Of List(Of OBLib.Helpers.RecordChange)) = RegisterProperty(Of List(Of OBLib.Helpers.RecordChange))(Function(c) c.RecordChangeList, "Record Change List")
    <Browsable(False)>
    Public ReadOnly Property RecordChangeList As List(Of OBLib.Helpers.RecordChange)
      Get
        If GetProperty(RecordChangeListProperty) Is Nothing Then
          LoadProperty(RecordChangeListProperty, New List(Of OBLib.Helpers.RecordChange))
        End If
        Return GetProperty(RecordChangeListProperty)
      End Get
    End Property

    'Public Shared ProductionHRPlannerListProperty As PropertyInfo(Of ProductionHRPlannerList) = RegisterProperty(Of ProductionHRPlannerList)(Function(c) c.ProductionHRPlannerList, "Production HR Planner List")
    'Public ReadOnly Property ProductionHRPlannerList() As ProductionHRPlannerList
    '  Get
    '    If GetProperty(ProductionHRPlannerListProperty) Is Nothing Then
    '      LoadProperty(ProductionHRPlannerListProperty, Productions.Areas.ProductionHRPlannerList.NewProductionHRPlannerList())
    '    End If
    '    Return GetProperty(ProductionHRPlannerListProperty)
    '  End Get
    'End Property

    Public Shared ProductionHROBListProperty As PropertyInfo(Of ProductionHROBList) = RegisterProperty(Of ProductionHROBList)(Function(c) c.ProductionHROBList, "Production HR OB List")
    <Browsable(False)>
    Public ReadOnly Property ProductionHROBList() As ProductionHROBList
      Get
        If GetProperty(ProductionHROBListProperty) Is Nothing Then
          LoadProperty(ProductionHROBListProperty, Productions.OB.ProductionHROBList.NewProductionHROBList)
        End If
        Return GetProperty(ProductionHROBListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As OBLib.Productions.Old.Production

      Return CType(CType(Me.Parent, ProductionSystemAreaList).Parent, OBLib.Productions.Old.Production)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSystemAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return CommonData.Lists.ROProductionAreaList.GetItem(ProductionAreaID).ProductionArea & " - " & CommonData.Lists.ROSystemList.GetItem(SystemID).System

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ProductionTimelines", "ProductionHumanResources", "ProductionHR"}
      End Get
    End Property

#Region " Spec "

    Public Sub ClearProductionSpec()

      ProductionSpecRequirementEquipmentTypeList.Clear()
      ProductionSpecRequirementPositionList.Clear()

    End Sub

    Public Sub SetupProductionSpecRequirement(ProductionSpecRequirement As Specs.Old.ProductionSpecRequirement)

      ProductionSpecRequirementID = ProductionSpecRequirement.ProductionSpecRequirementID
      If Me.GetParent.ProductionTypeID Is Nothing Then
        Me.GetParent.ProductionTypeID = ProductionSpecRequirement.ProductionTypeID
        'Me.GetParent.ProductionType = CommonData.Lists.ROProductionTypeList.GetItem(Me.GetParent.ProductionTypeID).ProductionType
      End If
      If Me.GetParent.EventTypeID Is Nothing Then
        Me.GetParent.EventTypeID = ProductionSpecRequirement.EventTypeID
        'Me.GetParent.EventType = CommonData.Lists.ROEventTypeList.GetItem(Me.GetParent.EventTypeID).EventType
      End If

    End Sub

    Public Sub PopulateProductionSpec(ProductionSpecRequirementID As Integer)

      Me.ProductionSpecRequirementID = ProductionSpecRequirementID
      Dim psrl As ProductionSpecRequirementList = Productions.Specs.Old.ProductionSpecRequirementList.GetProductionSpecRequirementList(ProductionSpecRequirementID, SystemID, ProductionAreaID, True)
      Dim ProductionSpecRequirement As ProductionSpecRequirement = psrl.GetItem(ProductionSpecRequirementID)

      'If I have found the spec
      If ProductionSpecRequirement IsNot Nothing Then
        SetupProductionSpecRequirement(ProductionSpecRequirement)
        OBLib.Helpers.ProductionSpecRequirements.PopulateOBSpec(Me.GetParent, ProductionSpecRequirement, Me.SystemID, Me.ProductionAreaID)
      End If

    End Sub

    Public Sub CheckNotComplySpecReasonsValid()  'Production As Production As String

      'this is a an Outside Broadcasting rule only
      If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CType(CommonData.Enums.System.ProductionServices, Integer)) Then
        Dim SpecRequirementEquipmentTypeMatch As Boolean = True
        Dim SpecRequirementPositionMatch As Boolean = True
        If ProductionSpecRequirementID IsNot Nothing Then
          Dim ROProductionSpec As ROProductionSpecRequirementOld = CommonData.Lists.ROProductionSpecRequirementList.GetItem(ProductionSpecRequirementID)
          If ROProductionSpec IsNot Nothing Then
            'check to see if the position requirements match the spec
            Dim SpecEquipment As List(Of ROProductionSpecRequirementPosition) = ROProductionSpec.ROProductionSpecRequirementPositionList.Where(Function(d) CompareSafe(d.SystemID, OBLib.Security.Settings.CurrentUser.SystemID)).ToList
            Dim ProductionEquipmentCount = ProductionSpecRequirementPositionList.Count
            Dim SpecEquipmentCount = SpecEquipment.Count
            If ProductionEquipmentCount <> SpecEquipmentCount Then
              SpecRequirementPositionMatch = False
            Else
              For Each item As ProductionSpecRequirementPosition In ProductionSpecRequirementPositionList
                If Not Singular.Misc.IsNullNothing(item.PositionID) Then
                  Dim ROMatch As OBLib.Productions.Specs.ReadOnly.Old.ROProductionSpecRequirementPosition = ROProductionSpec.ROProductionSpecRequirementPositionList.GetItemByPositionID(item.PositionID, item.EquipmentSubTypeID, OBLib.Security.Settings.CurrentUser.SystemID)
                  If ROMatch Is Nothing OrElse ROMatch.EquipmentQuantity <> item.EquipmentQuantity Then
                    SpecRequirementPositionMatch = False
                    Exit For
                  End If
                End If
              Next
            End If

            'check to see if the equipment requirements match the spec
            If ProductionSpecRequirementEquipmentTypeList.Count <> ROProductionSpec.ROProductionSpecRequirementEquipmentTypeList.Where(Function(d) CompareSafe(d.SystemID, OBLib.Security.Settings.CurrentUser.SystemID)).Count Then
              SpecRequirementEquipmentTypeMatch = False
            Else
              For Each item As ProductionSpecRequirementEquipmentType In ProductionSpecRequirementEquipmentTypeList
                If Not Singular.Misc.IsNullNothing(item.EquipmentTypeID) Then
                  Dim ROMatch As OBLib.Productions.Specs.ReadOnly.Old.ROProductionSpecRequirementEquipmentType = ROProductionSpec.ROProductionSpecRequirementEquipmentTypeList.GetItemByEquipmentTypeID(item.EquipmentTypeID, item.EquipmentSubTypeID, OBLib.Security.Settings.CurrentUser.SystemID)
                  If ROMatch Is Nothing OrElse ROMatch.EquipmentQuantity <> item.EquipmentQuantity Then
                    SpecRequirementEquipmentTypeMatch = False
                    Exit For
                  End If
                End If
              Next
            End If
          End If
        End If

        If String.IsNullOrEmpty(NotComplySpecReasons) And Not (SpecRequirementEquipmentTypeMatch And SpecRequirementPositionMatch) Then
          SpecNotCompliant = True
          Exit Sub
        End If

      End If

      SpecNotCompliant = False

    End Sub

#End Region

#Region " Outsource Srvices "

    'When production venue is set or changes
    Public Sub SetupAutoOutsourceServices()

      If Me.GetParent.ProductionVenueID IsNot Nothing And Me.GetParent.VenueConfirmedDate IsNot Nothing Then
        Dim pv As OBLib.Maintenance.General.ReadOnly.ROProductionVenueFull = CommonData.Lists.ROProductionVenueFullList.GetItem(Me.GetParent.ProductionVenueID)

        'create a generic list of items for this production type match
        Dim PVOSL As IEnumerable(Of OBLib.Maintenance.General.ReadOnly.ROProductionVenueOutsourceServiceType) = From Item In pv.ROProductionVenueOutsourceServiceTypeList
                                                                                                                Where Singular.Misc.IsNullNothing(Item.ProductionTypeID) OrElse Item.ProductionTypeID = Me.GetParent.ProductionTypeID
        For Each ProductionVenueOutsourceServiceType As Maintenance.General.ReadOnly.ROProductionVenueOutsourceServiceType In PVOSL
          If Not Me.ProductionOutsourceServiceList.OutsourceServiceTypeExists(ProductionVenueOutsourceServiceType.OutsourceServiceTypeID) Then
            Dim ProductionOutsourceService As OBLib.Productions.Services.ProductionOutsourceService = Me.ProductionOutsourceServiceList.AddNew
            ProductionOutsourceService.ProductionID = Me.ProductionID
            ProductionOutsourceService.SystemID = Me.SystemID
            ProductionOutsourceService.OutsourceServiceTypeID = ProductionVenueOutsourceServiceType.OutsourceServiceTypeID
          End If
        Next
        For Each OutsourceServiceType As Maintenance.General.ReadOnly.ROOutsourceServiceType In CommonData.Lists.ROOutsourceServiceTypeList
          If OutsourceServiceType.AlwaysRequiredInd AndAlso Not Me.ProductionOutsourceServiceList.OutsourceServiceTypeExists(OutsourceServiceType.OutsourceServiceTypeID) Then
            Dim ProductionOutsourceService As OBLib.Productions.Services.ProductionOutsourceService = Me.ProductionOutsourceServiceList.AddNew
            ProductionOutsourceService.ProductionID = Me.ProductionID
            ProductionOutsourceService.SystemID = Me.SystemID
            ProductionOutsourceService.OutsourceServiceTypeID = OutsourceServiceType.OutsourceServiceTypeID
          End If
        Next
      End If

    End Sub

#End Region

#Region " Timelines "

    Public Sub AddNewTimeline()

      Dim pt As ProductionTimeline = Me.ProductionTimelineList.AddNew()
      pt.ProductionID = Me.ProductionID
      pt.ProductionSystemAreaID = ProductionSystemAreaID
      pt.SystemID = Me.SystemID
      pt.ProductionAreaID = Me.ProductionAreaID
      'If OBLib.Security.Settings.CurrentUser.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
      '  pt.TimelineDate = Me.GetParent.PlayStartDateTime
      'End If

    End Sub

    Public Sub UpdateTimeline(ClientTimeline As ProductionTimeline)

      'Dim ServerTimeline As ProductionTimeline = Me.ProductionTimelineList.Where(Function(spt) spt.Guid = ClientTimeline.Guid)
      Me.ProductionTimelineList.ReplaceOrAdd(ClientTimeline)
      ''New
      'If ServerTimeline Is Nothing Then

      '  'Add
      'ElseIf ServerTimeline IsNot Nothing Then

      '  'Delete???
      'End If

    End Sub

    Public Sub UpdateTimeline(ProductionTimelineClient As Helpers.RuleHelper.ProductionSystemAreaRules.ProductionTimelineClient)

      Dim pt As ProductionTimeline = ProductionTimelineList.Where(Function(d) d.Guid = ProductionTimelineClient.ProductionTimeline.Guid).FirstOrDefault

      If pt IsNot Nothing Then
        pt.ProductionTimelineTypeID = ProductionTimelineClient.ProductionTimeline.ProductionTimelineTypeID
        pt.StartDateTime = ProductionTimelineClient.ProductionTimeline.StartDateTime
        pt.EndDateTime = ProductionTimelineClient.ProductionTimeline.EndDateTime
        pt.VehicleID = ProductionTimelineClient.ProductionTimeline.VehicleID
        pt.Comments = ProductionTimelineClient.ProductionTimeline.Comments

        Dim ChangedHumanResourceIDs As New List(Of Integer)
        For Each ar As HRProductionSchedule In HRProductionScheduleList
          For Each asd As HRProductionScheduleDetail In ar.HRProductionScheduleDetailList
            If (asd.ProductionTimelineID = pt.ProductionTimelineID) Or (asd.ProductionTimeline.Guid = pt.Guid) Or (asd.ProductionTimeline Is pt) Then
              asd.ProductionTimelineID = pt.ProductionTimelineID
              asd.ProductionTimelineTypeID = pt.ProductionTimelineTypeID
              'asd.ProductionTimelineType = pt.ProductionTimelineTypeID
              asd.StartDateTime = pt.StartDateTime
              asd.EndDateTime = pt.EndDateTime
              If Not ChangedHumanResourceIDs.Contains(asd.HumanResourceID) Then
                ChangedHumanResourceIDs.Add(asd.HumanResourceID)
              End If
            End If
          Next
        Next
      End If

    End Sub

    'Public Sub UpdateTimeline(TimelineGuid As String)

    '  Dim g As Guid = New Guid(TimelineGuid)
    '  Dim pt As ProductionTimeline = ProductionTimelineList.Where(Function(d) d.Guid = g).FirstOrDefault

    '  If pt IsNot Nothing Then
    '    'pt.ProductionTimelineTypeID = ProductionTimelineClient.ProductionTimeline.ProductionTimelineTypeID
    '    'pt.StartDateTime = ProductionTimelineClient.ProductionTimeline.StartDateTime
    '    'pt.EndDateTime = ProductionTimelineClient.ProductionTimeline.EndDateTime
    '    'pt.VehicleID = ProductionTimelineClient.ProductionTimeline.VehicleID
    '    'pt.Comments = ProductionTimelineClient.ProductionTimeline.Comments

    '    Dim ChangedHumanResourceIDs As New List(Of Integer)
    '    For Each ar As HRProductionSchedule In HRProductionScheduleList
    '      For Each asd As HRProductionScheduleDetail In ar.HRProductionScheduleDetailList
    '        If (asd.ProductionTimelineID = pt.ProductionTimelineID) Or (asd.ProductionTimeline.Guid = pt.Guid) Or (asd.ProductionTimeline Is pt) Then
    '          asd.ProductionTimelineID = pt.ProductionTimelineID
    '          asd.ProductionTimelineTypeID = pt.ProductionTimelineTypeID
    '          'asd.ProductionTimelineType = pt.ProductionTimelineTypeID
    '          asd.StartDateTime = pt.StartDateTime
    '          asd.EndDateTime = pt.EndDateTime
    '          If Not ChangedHumanResourceIDs.Contains(asd.HumanResourceID) Then
    '            ChangedHumanResourceIDs.Add(asd.HumanResourceID)
    '          End If
    '        End If
    '      Next
    '    Next
    '    CheckScheduleValid()
    '  End If

    '  'Check Clashes
    '  'CheckScheduleValid()

    'End Sub

    Public Sub RemoveTimeline(Guid As String)

      'Get the timeline item which is to be removed
      Dim gid As Guid = New Guid(Guid)
      Dim TimelineToRemove As ProductionTimeline = Nothing
      For Each pt As ProductionTimeline In ProductionTimelineList
        If pt.Guid = gid Then
          TimelineToRemove = pt
          Exit For
        End If
      Next

      'Remove from schedules
      Dim ItemsToRemove As List(Of HRProductionScheduleDetail)
      For Each HRSched As HRProductionSchedule In HRProductionScheduleList
        Dim LocalHRSched As HRProductionSchedule = HRSched
        ItemsToRemove = New List(Of HRProductionScheduleDetail)
        For Each HRSchedDetail As HRProductionScheduleDetail In HRSched.HRProductionScheduleDetailList
          If (HRSchedDetail.ProductionTimelineID = TimelineToRemove.ProductionTimelineID) Or (HRSchedDetail.ProductionTimeline.Guid = TimelineToRemove.Guid) Or (HRSchedDetail.ProductionTimeline Is TimelineToRemove) Then
            ItemsToRemove.Add(HRSchedDetail)
          End If
        Next
        ItemsToRemove.ForEach(Sub(ItemToRemove)
                                LocalHRSched.HRProductionScheduleDetailList.Remove(ItemToRemove)
                              End Sub)
      Next

      'Remove from timeline list
      If TimelineToRemove IsNot Nothing Then
        ProductionTimelineList.Remove(TimelineToRemove)
      End If

    End Sub

    Public Sub RemoveTimeline(TimelineToRemove As ProductionTimeline)

      'Remove from schedules
      RemoveSchedulesForTimeline(TimelineToRemove)

      'Remove from timeline list
      ProductionTimelineList.Remove(TimelineToRemove)

    End Sub

    Public Sub RemoveSchedulesForTimeline(TimelineToRemove As ProductionTimeline)

      'Remove from schedules
      For Each HRSched As HRProductionSchedule In HRProductionScheduleList
        Dim CurrentHRSched As HRProductionSchedule = HRSched
        Dim SchedulesToRemove As New List(Of HRProductionScheduleDetail)
        For Each HRSchedDetail As HRProductionScheduleDetail In CurrentHRSched.HRProductionScheduleDetailList
          If (HRSchedDetail.ProductionTimelineID = TimelineToRemove.ProductionTimelineID) Or (HRSchedDetail.ProductionTimeline.Guid = TimelineToRemove.Guid) Or (HRSchedDetail.ProductionTimeline Is TimelineToRemove) Then
            SchedulesToRemove.Add(HRSchedDetail)
          End If
        Next
        SchedulesToRemove.ForEach(Sub(ScheduleToRemove)
                                    CurrentHRSched.HRProductionScheduleDetailList.Remove(ScheduleToRemove)
                                  End Sub)
      Next

    End Sub

    Public Sub DeleteVehiclePreTravelItems(VehicleID As Integer?)

      RemoveAll(VehicleID, CommonData.Enums.ProductionTimelineType.VehiclePreTravel)

    End Sub

    Public Sub DeleteVehiclePostTravelItems(VehicleID As Integer?)

      RemoveAll(VehicleID, CommonData.Enums.ProductionTimelineType.VehiclePostTravel)

    End Sub

    Public Sub RemoveAll(VehicleID As Integer?, ByVal ProductionTimelineTypeID As Integer)

      Dim TimelinesToRemove As New List(Of ProductionTimeline)

      For Each pt As ProductionTimeline In Me.ProductionTimelineList
        If Singular.Misc.CompareSafe(pt.ProductionTimelineTypeID, CType(ProductionTimelineTypeID, Integer)) AndAlso (VehicleID IsNot Nothing AndAlso CompareSafe(pt.VehicleID, VehicleID)) Then
          TimelinesToRemove.Add(pt)
        End If
      Next

      TimelinesToRemove.ForEach(Sub(pt)
                                  Me.RemoveTimeline(pt)
                                End Sub)

    End Sub

#Region " Validation "

    Public Shared Function DateRangesOverlap(Range1Start As DateTime, Range1End As DateTime, Range2Start As DateTime, Range2End As DateTime, AllowStartAndEndEqual As Boolean) As Boolean
      If AllowStartAndEndEqual Then
        If (Range1Start < Range2End And Range1Start > Range2Start) Or (Range1End < Range2End And Range1End > Range2Start) Then
          Return True
        End If
      Else
        If (Range1Start <= Range2End And Range1Start >= Range2Start) Or (Range1End <= Range2End And Range1End >= Range2Start) Then
          Return True
        End If
      End If
      Return False
    End Function

    Public Class DisciplineClash

      Public Property DisciplineID As Integer
      Public Property Discipline As String
      Public Property TimelineList As List(Of DisciplineTimeline)

      Public Sub New(DisciplineID As Integer, Discipline As String, FlatTimelineList As IEnumerable(Of FlatDisciplineClash))
        Me.DisciplineID = DisciplineID
        Me.Discipline = Discipline
        TimelineList = (From data In FlatTimelineList
                        Select New DisciplineTimeline With {.ProductionTimelineTypeID = data.ProductionTimelineTypeID, .ProductionTimelineType = data.ProductionTimelineType,
                                                            .FreeInd = data.FreeInd, .VehicleID = data.VehicleID,
                                                            .StartDateTime = data.StartDateTime, .EndDateTime = data.EndDateTime}).ToList
        TimelineList.OrderBy(Function(d) d.StartDateTime)
      End Sub

    End Class

    Public Class DisciplineTimeline
      Public Property ProductionTimelineTypeID As Integer
      Public Property ProductionTimelineType As String
      Public Property FreeInd As Boolean
      Public Property VehicleID As Integer?
      Public Property StartDateTime As DateTime
      Public Property EndDateTime As DateTime
    End Class

    Public Class FlatDisciplineClash
      Public Property DisciplineID As Integer
      Public Property Discipline As String
      Public Property ProductionTimelineTypeID As Integer
      Public Property ProductionTimelineType As String
      Public Property FreeInd As Boolean
      Public Property VehicleID As Integer?
      Public Property StartDateTime As DateTime
      Public Property EndDateTime As DateTime
      Public Property TimelineGuid As String
    End Class

    Public Class ParentTimelineClash

      Public Property TimelineGuid As String
      Public Property ProductionTimelineTypeID As Integer
      Public Property ProductionTimelineType As String
      Public Property Disciplines As List(Of PTDiscipline)
      Public Property ParentTimelineClash As String

      Public Sub BuildDisciplinesList(AllData As IEnumerable(Of FlatDisciplineClash))

        Dim DisciplinesList As List(Of PTDiscipline) = (From fdc In AllData
                                                        Group By DisciplineID = fdc.DisciplineID, _
                                                                 Discipline = fdc.Discipline _
                                                        Into Group
                                                        Select New PTDiscipline(Me.TimelineGuid, DisciplineID, Discipline, Group)).ToList
        Me.Disciplines = DisciplinesList

      End Sub

      Public Sub New(TimelineGuid As String, ProductionTimelineTypeID As Integer, ProductionTimelineType As String, AllData As IEnumerable(Of FlatDisciplineClash))
        Me.TimelineGuid = TimelineGuid
        Me.ProductionTimelineTypeID = ProductionTimelineTypeID
        Me.ProductionTimelineType = ProductionTimelineType
        BuildDisciplinesList(AllData)
      End Sub

    End Class

    Public Class PTDiscipline

      Public Property DisciplineID As Integer
      Public Property Discipline As String
      Public Property PTTimelines As List(Of PTTimeline)
      Public Property ParentDisciplineClash As String
      Public Property ClashList As List(Of String)
      Public Property ParentTimelineGuid As String

      Public Sub CalculateClashes()

        Dim ErrorMessage As String = ""

        Dim Clashes = (From copy1 In PTTimelines
                       From copy2 In PTTimelines
                       Where DateRangesOverlap(copy1.StartDateTime, copy1.EndDateTime, copy2.StartDateTime, copy2.EndDateTime, True) _
                         And If(IsNullNothing(copy1.VehicleID, True), 0, copy1.VehicleID) = If(IsNullNothing(copy2.VehicleID, True), 0, copy2.VehicleID) _
                         And Not copy1.FreeInd _
                         And Not copy2.FreeInd _
                         And copy1 IsNot copy2 _
                         And (IsNullNothing(copy1.VehicleID) OrElse CommonData.Lists.ROVehicleList.GetItem(copy1.VehicleID).VehicleTypeID <> CType(CommonData.Enums.VehicleType.OBVan, Integer)) _
                         And (IsNullNothing(copy2.VehicleID) OrElse CommonData.Lists.ROVehicleList.GetItem(copy2.VehicleID).VehicleTypeID <> CType(CommonData.Enums.VehicleType.OBVan, Integer)) _
                         And (copy1.TimelineGuid = Me.ParentTimelineGuid Or copy2.TimelineGuid = Me.ParentTimelineGuid)
                       Select New With {
                                        .Err = copy1.TimelineType & " " & (copy1.StartDateTime.Value.ToShortDateString) & " overlaps with " & _
                                               copy2.TimelineType & " " & (copy2.StartDateTime.Value.ToShortDateString)
                                       }).Select(Function(d) d.Err).Distinct.ToList
        ClashList = Clashes
        'If Clashes.Count > 0 Then
        '  ErrorMessage &= Discipline & ": " & "<br>"
        '  Clashes.ToList.ForEach(Sub(Err)
        '                           ErrorMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;" & Err & ", <br>"
        '                         End Sub)
        'End If

      End Sub

      Public Sub New(ParentTimelineGuid As String, DisciplineID As Integer, Discipline As String, GroupData As IEnumerable(Of FlatDisciplineClash))

        Me.ParentTimelineGuid = ParentTimelineGuid
        Me.DisciplineID = DisciplineID
        Me.Discipline = Discipline

        Dim PTTimelines As List(Of PTTimeline) = (From fdc In GroupData
                                                  Group By TimelineGuid = fdc.TimelineGuid, _
                                                                 ProductionTimelineTypeID = fdc.ProductionTimelineTypeID, _
                                                                 ProductionTimelineType = fdc.ProductionTimelineType, _
                                                                 VehicleID = fdc.VehicleID, _
                                                                 StartDateTime = fdc.StartDateTime, _
                                                                 EndDateTime = fdc.EndDateTime, _
                                                                 FreeInd = fdc.FreeInd _
                                                  Into Group
                                                  Select New PTTimeline(TimelineGuid, ProductionTimelineTypeID, ProductionTimelineType,
                                                                        ZeroNothing(VehicleID), StartDateTime, EndDateTime, FreeInd)).ToList
        Me.PTTimelines = PTTimelines

        CalculateClashes()

      End Sub

    End Class

    Public Class PTTimeline

      Public Property TimelineGuid As String = ""
      Public Property TimelineTypeID As Integer? = Nothing
      Public Property TimelineType As String = ""
      Public Property VehicleID As Integer? = Nothing
      Public Property StartDateTime As DateTime? = Nothing
      Public Property EndDateTime As DateTime? = Nothing
      Public Property FreeInd As Boolean? = Nothing

      Public Sub New(TimelineGuid As String, TimelineTypeID As Integer, TimelineType As String, VehicleID As Integer?,
                     StartDateTime As DateTime, EndDateTime As DateTime, FreeInd As Boolean)

        Me.TimelineGuid = TimelineGuid
        Me.TimelineTypeID = TimelineTypeID
        Me.TimelineType = TimelineType
        Me.VehicleID = VehicleID
        Me.StartDateTime = StartDateTime
        Me.EndDateTime = EndDateTime
        Me.FreeInd = FreeInd

      End Sub

    End Class

    Public Class TTimelineClash
      Public Property TimelineGuid As String
      Public Property Discipline As String
      Public Property Clash As String
    End Class

    Public Function CheckTimelineValid() As String

      Dim ErrorMessage As String = ""
      Dim FlatDisciplineTimelines As List(Of FlatDisciplineClash) = (From pt In ProductionTimelineList
                                                                     Join ptt In OBLib.CommonData.Lists.ROProductionTimelineTypeList On ptt.ProductionTimelineTypeID Equals pt.ProductionTimelineTypeID
                                                                     Join pttd In OBLib.CommonData.Lists.ROProductionTimelineTypeDisciplineList On pttd.ProductionTimelineTypeID Equals ptt.ProductionTimelineTypeID
                                                                     Join phr In ProductionHumanResourceList On phr.DisciplineID Equals pttd.DisciplineID
                                                                     Where pt.IsValid _
                                                                           AndAlso phr.IsValid _
                                                                           AndAlso If(IsNullNothing(phr.VehicleID, True), 0, phr.VehicleID) = If(IsNullNothing(pt.VehicleID, True), 0, pt.VehicleID)
                                                                     Select New FlatDisciplineClash With {
                                                                                                           .DisciplineID = phr.DisciplineID, .Discipline = OBLib.CommonData.Lists.RODisciplineList.GetItem(phr.DisciplineID).Discipline,
                                                                                                           .ProductionTimelineTypeID = pttd.ProductionTimelineTypeID, .ProductionTimelineType = ptt.ProductionTimelineType,
                                                                                                           .FreeInd = ptt.FreeInd, .VehicleID = phr.VehicleID,
                                                                                                           .StartDateTime = pt.StartDateTime, .EndDateTime = pt.EndDateTime,
                                                                                                           .TimelineGuid = pt.Guid.ToString
                                                                    }).Distinct.ToList

      Dim ParentTimelineClashList As List(Of ParentTimelineClash) = (From fdc In FlatDisciplineTimelines
                                                                     Group By TimelineGuid = fdc.TimelineGuid, _
                                                                              ProductionTimelineTypeID = fdc.ProductionTimelineTypeID, _
                                                                              ProductionTimelineType = fdc.ProductionTimelineType _
                                                                     Into Group
                                                                     Select New ParentTimelineClash(TimelineGuid, ProductionTimelineTypeID, ProductionTimelineType, FlatDisciplineTimelines)).ToList

      For Each item As ProductionTimeline In Me.ProductionTimelineList
        item.TimelineClashes = ""
      Next

      ParentTimelineClashList.ForEach(Sub(ptc)
                                        Dim ProductionTimeline As ProductionTimeline = Me.ProductionTimelineList.GetItemByGuid(New Guid(ptc.TimelineGuid))
                                        ptc.Disciplines.ForEach(Sub(dis)
                                                                  If dis.ClashList.Count > 0 Then
                                                                    ProductionTimeline.TimelineClashes = dis.Discipline & ": " & "<br>"
                                                                    dis.ClashList.ForEach(Sub(cl)
                                                                                            ProductionTimeline.TimelineClashes &= "&nbsp;&nbsp;&nbsp;&nbsp;" & cl & ", <br>"
                                                                                          End Sub)
                                                                  End If
                                                                End Sub)
                                      End Sub)

      'Dim StructuredClashes As List(Of DisciplineClash) = (From FlatData In FlatDisciplineTimelines
      '                                                     Group By DisciplineID = FlatData.DisciplineID, Discipline = FlatData.Discipline Into Group
      '                                                     Select New DisciplineClash(DisciplineID, Discipline, Group)
      '                                                    ).ToList

      'StructuredClashes.ForEach(Sub(Discipline)

      '                            Dim Clashes = (From copy1 In Discipline.TimelineList
      '                                           From copy2 In Discipline.TimelineList
      '                                           Where DateRangesOverlap(copy1.StartDateTime, copy1.EndDateTime, copy2.StartDateTime, copy2.EndDateTime, True) _
      '                                             And IsNull(copy1.VehicleID, 0) = IsNull(copy2.VehicleID, 0) _
      '                                             And Not copy1.FreeInd _
      '                                             And Not copy2.FreeInd _
      '                                             And copy1 IsNot copy2 _
      '                                             And (IsNullNothing(copy1.VehicleID) OrElse CommonData.Lists.ROVehicleList.GetItem(copy1.VehicleID).VehicleTypeID <> CType(CommonData.Enums.VehicleType.OBVan, Integer)) _
      '                                             And (IsNullNothing(copy2.VehicleID) OrElse CommonData.Lists.ROVehicleList.GetItem(copy2.VehicleID).VehicleTypeID <> CType(CommonData.Enums.VehicleType.OBVan, Integer))
      '                                           Select New With {
      '                                                            .Err = copy1.ProductionTimelineType & " " & (copy1.StartDateTime.ToShortDateString) & " overlaps with " & _
      '                                                                   copy2.ProductionTimelineType & " " & (copy2.StartDateTime.ToShortDateString)
      '                                                           }).Select(Function(d) d.Err).Distinct.ToList


      '                            '.ProductionTimelineType1 = copy1.ProductionTimelineType,
      '                            '.StartDateTime1 = copy1.StartDateTime,
      '                            '.EndDateTime1 = copy1.EndDateTime,
      '                            '.VehicleID1 = copy1.VehicleID,
      '                            '.ProductionTimelineType2 = copy2.ProductionTimelineType,
      '                            '.StartDateTime2 = copy2.StartDateTime,
      '                            '.EndDateTime2 = copy2.EndDateTime,
      '                            '.VehicleID2 = copy2.VehicleID,
      '                            '.ProductionTimelineType1 & " " & (Err.StartDateTime1.ToShortDateString) & " overlaps with " & _
      '                            '                                           Err.ProductionTimelineType2 & " " & (Err.StartDateTime2.ToShortDateString)

      '                            If Clashes.Count > 0 Then
      '                              ErrorMessage &= Discipline.Discipline & ": " & "<br>"
      '                              Clashes.ToList.ForEach(Sub(Err)
      '                                                       ErrorMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;" & Err & ", <br>"
      '                                                     End Sub)
      '                            End If

      '                          End Sub)

      Return ErrorMessage

    End Function

#End Region

#End Region

#Region " Production Human Resources "

    Public Function AddNewProductionHumanResource() As ProductionHumanResource
      Dim NewPHR As ProductionHumanResource = Me.ProductionHumanResourceList.AddNew
      NewPHR.SystemID = Me.SystemID
      NewPHR.ProductionID = Me.ProductionID
      NewPHR.ProductionSystemAreaID = Me.ProductionSystemAreaID
      Return NewPHR
    End Function

    Public Sub SetHumanResourceID(Guid As Guid, OldHumanResourceID As Integer?, NewHumanResourceID As Integer?, NewHumanResource As String,
                                  DisciplineID As Integer?, PositionID As Integer?)
      Dim PHR As ProductionHumanResource = Me.ProductionHumanResourceList.GetItemByGuid(Guid)
      PHR.HumanResourceID = NewHumanResourceID
      PHR.HumanResource = NewHumanResource
      PHR.DisciplineID = DisciplineID
      PHR.PositionID = PositionID

      Dim HRProductionSchedule As HRProductionSchedule = Nothing
      If OldHumanResourceID IsNot Nothing Then
        'If the old human resource has a schedule, switch it to the new human resource id
        HRProductionSchedule = Me.HRProductionScheduleList.GetItem(OldHumanResourceID)
        If HRProductionSchedule IsNot Nothing Then
          HRProductionSchedule.HumanResourceID = NewHumanResourceID
          HRProductionSchedule.HumanResource = NewHumanResource
          For Each hrpd As HRProductionScheduleDetail In HRProductionSchedule.HRProductionScheduleDetailList
            hrpd.HumanResourceID = NewHumanResourceID
          Next
        Else
          'otherwise generate a new schedule
          If SystemID = CType(OBLib.CommonData.Enums.System.ProductionServices, Integer) Then
            CalculateProductionHR()
          Else
            GenerateProductionSchedule(NewHumanResourceID)
          End If
        End If
      Else
        If SystemID = CType(OBLib.CommonData.Enums.System.ProductionServices, Integer) Then
          CalculateProductionHR()
        Else
          GenerateProductionSchedule(NewHumanResourceID)
        End If
        HRProductionSchedule = Me.HRProductionScheduleList.GetItem(NewHumanResourceID)
      End If
      If HRProductionSchedule IsNot Nothing Then
        HRProductionSchedule.UpdateDetails()
        OBLib.Helpers.ProductionSchedules.CheckProductionAvailabilityOB(Me, HRProductionSchedule.HumanResourceID)
      End If

    End Sub

    Public Sub DeleteProductionHumanResource(Guid As Guid)
      Dim PHR As ProductionHumanResource = Me.ProductionHumanResourceList.GetItemByGuid(Guid)
      RemoveSchedule(PHR)
      Me.ProductionHumanResourceList.RemoveFromList(PHR)
    End Sub

    Public Sub DeleteProductionHumanResource(ProductionHumanResource As ProductionHumanResource)
      RemoveSchedule(ProductionHumanResource)
      Me.ProductionHumanResourceList.RemoveFromList(ProductionHumanResource)
    End Sub

    Public Sub ClearHumanResourceID(Guid As Guid)
      Dim PHR As ProductionHumanResource = Me.ProductionHumanResourceList.GetItemByGuid(Guid)
      RemoveSchedule(PHR)
      PHR.HumanResourceID = Nothing
      PHR.HumanResource = ""
      PHR.Clash = ""
    End Sub

    Public Sub ClearHumanResourceID(ProductionHumanResource As ProductionHumanResource)
      RemoveSchedule(ProductionHumanResource)
      ProductionHumanResource.HumanResourceID = Nothing
      ProductionHumanResource.HumanResource = ""
      ProductionHumanResource.Clash = ""
    End Sub

    Public Sub RemoveSchedule(ProductionHumanResource As ProductionHumanResource)
      If OBLib.Security.Settings.CurrentUser.SystemID = CType(CommonData.Enums.System.ProductionContent, Integer) And OBLib.Security.Settings.CurrentUser.ProductionAreaID = CType(CommonData.Enums.ProductionArea.OB, Integer) Then
        'Remove his/her entire schedule
        If ProductionHumanResource.HumanResourceID IsNot Nothing Then
          Dim ExistingSchedule As HRProductionSchedule = Me.HRProductionScheduleList.GetItem(ProductionHumanResource.HumanResourceID)
          Me.HRProductionScheduleList.Remove(ExistingSchedule)
        End If
      Else
        If ProductionHumanResource.HumanResourceID IsNot Nothing Then
          Dim ProductionDisciplineCount As Integer = Me.ProductionHumanResourceList.Where(Function(phr) CompareSafe(phr.HumanResourceID, ProductionHumanResource.HumanResourceID)).Count
          If ProductionDisciplineCount > 1 Then
            'Leave it alone
            ''Remove only the specific one
            'Dim ExistingSchedule As HRProductionSchedule = Me.HRProductionScheduleList.GetItem(ProductionHumanResource.HumanResourceID)
            'ExistingSchedule.RemoveSpecificPHR(ProductionHumanResource)
          Else
            'Remove his/her entire schedule
            Dim ExistingSchedule As HRProductionSchedule = Me.HRProductionScheduleList.GetItem(ProductionHumanResource.HumanResourceID)
            Me.HRProductionScheduleList.Remove(ExistingSchedule)
          End If
        End If
      End If
    End Sub

    Public Sub RemoveHumanResourceID(ProductionHumanResource As ProductionHumanResource)
      ClearHumanResourceID(ProductionHumanResource)
    End Sub

#End Region

#Region " Crew "

    Private Sub AutoPopulateHumanResourcesForCameras(ProductionSystemArea As ProductionSystemArea)

      Dim PHRL As ProductionHumanResourceList = Productions.Areas.ProductionHumanResourceList.NewProductionHumanResourceList()
      Dim PositionsTotalCameras As Integer = ProductionSpecRequirementPositionList.GetTotalCameraCount
      Dim EquipmentTotalCameras As Integer = ProductionSpecRequirementEquipmentTypeList.GetTotalCameraCount

      Dim NoOfEVS_Spec As Integer = (From pos As ProductionSpecRequirementPosition In ProductionSpecRequirementPositionList
                                     Where Singular.Misc.CompareSafe(pos.DisciplineID, CType(CommonData.Enums.Discipline.EVSOperator, Integer))
                                     Select pos).Count
      '                                     Join p As Maintenance.General.ReadOnly.ROPosition In CommonData.Lists.ROPositionList On pos.PositionID Equals p.PositionID
      Dim NoOfVisionController_Spec As Integer = (From pos As ProductionSpecRequirementPosition In ProductionSpecRequirementPositionList
                                                  Where Singular.Misc.CompareSafe(pos.DisciplineID, CType(CommonData.Enums.Discipline.VisionController, Integer))
                                                  Select pos).Count
      '                                                  Join p As Maintenance.General.ReadOnly.ROPosition In CommonData.Lists.ROPositionList On pos.PositionID Equals p.PositionID


      'add all the human resource template items for the camera disciplines
      Dim posList = From pos In ProductionSpecRequirementPositionList
                    Where Singular.Misc.CompareSafe(pos.DisciplineID, CType(CommonData.Enums.Discipline.CameraOperator, Integer))
                    Select pos
                    Order By pos.Priority
      'Join p In CommonData.Lists.ROPositionList On p.PositionID Equals pos.PositionID

      For Each pos In posList
        Dim phr As ProductionHumanResource = Nothing
        For j As Integer = 0 To IIf(pos.EquipmentQuantity > 0, pos.EquipmentQuantity, 1) - 1
          phr = PHRL.AddNew
          If phr IsNot Nothing Then
            phr.DisciplineID = pos.DisciplineID
            phr.Discipline = CommonData.Lists.RODisciplineList.GetItem(pos.DisciplineID).Discipline
            phr.PositionID = pos.PositionID
            phr.Position = CommonData.Lists.ROPositionList.GetItem(phr.PositionID).Position
            phr.SystemID = pos.SystemID
            phr.SystemID = pos.SystemID
            phr.ProductionID = ProductionSystemArea.ProductionID
          End If
        Next
        'End If
      Next

      'Mow add the human resource template items for the bashers, take the higher camera count from between equipment requirements or position requirements
      Dim ptbr As Maintenance.Productions.Specs.ReadOnly.ROProductionTypeBasherRequirement = CommonData.Lists.ROProductionTypeBasherRequirementList.GetItemForCameraCount(IIf(EquipmentTotalCameras > PositionsTotalCameras, EquipmentTotalCameras, PositionsTotalCameras))

      'If there is a matching requirement spec
      If ptbr IsNot Nothing Then
        For i As Integer = 0 To ptbr.NoOfBashers - 1
          Dim phr As ProductionHumanResource = Nothing
          phr = PHRL.AddNew
          If phr IsNot Nothing Then
            phr.DisciplineID = CType(CommonData.Enums.Discipline.Bashers, Integer)
            phr.Discipline = CommonData.Lists.RODisciplineList.GetItem(phr.DisciplineID).Discipline
            phr.SystemID = ProductionSystemArea.SystemID
            phr.ProductionID = ProductionSystemArea.ProductionID
          End If
        Next
      End If

      'Now add the human resource template items for the EVSs and Vision Controllers, take the higher camera count from between equipment requirements or position requirements
      Dim ptcr As Maintenance.Productions.Specs.ReadOnly.ROProductionTypeCrewRequirement = CommonData.Lists.ROProductionTypeCrewRequirementList.GetItemForCameraCount(IIf(EquipmentTotalCameras > PositionsTotalCameras, EquipmentTotalCameras, PositionsTotalCameras))
      Dim EVSPositionList As IEnumerable(Of Maintenance.General.ReadOnly.ROPosition) = CommonData.Lists.ROPositionList.Where(Function(d) d.DisciplineID = CType(CommonData.Enums.Discipline.EVSOperator, Integer)).OrderBy(Function(d) d.Priority)
      Dim VisionControllerPositionList As IEnumerable(Of Maintenance.General.ReadOnly.ROPosition) = CommonData.Lists.ROPositionList.Where(Function(d) d.DisciplineID = CType(CommonData.Enums.Discipline.VisionController, Integer)).OrderBy(Function(d) d.Priority)

      'If there is a matching requirment spec
      If ptcr IsNot Nothing Then
        'take the larger EVS spec between the production position requirements and the requirment spec setting
        For i As Integer = 0 To Math.Max(ptcr.NoOfEVS, NoOfEVS_Spec) - 1
          Dim phr As ProductionHumanResource = Nothing
          phr = PHRL.AddNew
          If phr IsNot Nothing Then
            phr.DisciplineID = CType(CommonData.Enums.Discipline.EVSOperator, Integer)
            phr.Discipline = CommonData.Lists.RODisciplineList.GetItem(phr.DisciplineID).Discipline
            'auto complete the position based on the priority of the positions for the discipline
            If i < EVSPositionList.Count Then
              phr.PositionID = EVSPositionList(i).PositionID
              phr.Position = CommonData.Lists.ROPositionList.GetItem(phr.PositionID).Position
            End If
            phr.SystemID = ProductionSystemArea.SystemID
            phr.ProductionID = ProductionSystemArea.ProductionID
          End If
        Next
        For i As Integer = 0 To Math.Max(ptcr.NoOfVisionControllers, NoOfVisionController_Spec) - 1
          Dim phr As ProductionHumanResource = Nothing
          phr = PHRL.AddNew
          If phr IsNot Nothing Then
            phr.DisciplineID = CType(CommonData.Enums.Discipline.VisionController, Integer)
            phr.Discipline = CommonData.Lists.RODisciplineList.GetItem(phr.DisciplineID).Discipline
            'auto complete the position based on the priority of the positions for the discipline
            If i < VisionControllerPositionList.Count Then
              phr.PositionID = VisionControllerPositionList(i).PositionID
              phr.Position = CommonData.Lists.ROPositionList.GetItem(phr.PositionID).Position
            End If
            phr.SystemID = ProductionSystemArea.SystemID
            phr.ProductionID = ProductionSystemArea.ProductionID
          End If
        Next
      End If

      MergeHumanResourceList(PHRL, ProductionSystemArea, CommonData.Enums.Discipline.CameraOperator, CommonData.Enums.Discipline.VisionController, CommonData.Enums.Discipline.EVSOperator, CommonData.Enums.Discipline.Bashers)

    End Sub

    Private Sub AutoPopulateVehicleHumanResources(ByVal PHRLNew As ProductionHumanResourceList, ByVal Vehicle As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleFull, ProductionSystemArea As ProductionSystemArea)

      For Each vhr As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleHumanResource In Vehicle.ROVehicleHumanResourceList
        If Singular.Misc.CompareSafe(vhr.DisciplineID, CType(CommonData.Enums.Discipline.Driver, Integer)) Then
          PHRLNew.AddVehicleDriver(Vehicle, vhr, ProductionSystemArea)
        Else
          PHRLNew.AddVehicleHR(Vehicle, vhr, ProductionSystemArea)
        End If
      Next

    End Sub

    Private Sub AutoPopulateHumanResourcesForProductionSystem(ProductionSystemArea As ProductionSystemArea)

      If ProductionSpecRequirementPositionList.Count <> 0 Then

        Dim PHRL As ProductionHumanResourceList = Productions.Areas.ProductionHumanResourceList.NewProductionHumanResourceList()

        'Loop through all Production Spec Requirements and Auto populating Prod. HR List
        ProductionSpecRequirementPositionList.ToList.ForEach(Sub(z)

                                                               'Add the discipline the number of times specified by the quanitity
                                                               For i As Integer = 0 To (z.EquipmentQuantity - 1)
                                                                 Dim phr As ProductionHumanResource = Nothing
                                                                 phr = PHRL.AddNew
                                                                 phr.DisciplineID = z.DisciplineID
                                                                 phr.Discipline = CommonData.Lists.RODisciplineList.GetItem(phr.DisciplineID).Discipline
                                                                 If (Not IsNullNothing(z.PositionID)) Then
                                                                   phr.PositionID = z.PositionID
                                                                   phr.Position = CommonData.Lists.ROPositionList.GetItem(phr.PositionID).Position
                                                                 End If
                                                                 phr.SystemID = OBLib.Security.Settings.CurrentUser.SystemID
                                                               Next
                                                             End Sub)

        'merge the list from above with the HR list already on this production and remove any excess disciplines
        MergeHumanResourceList(PHRL, ProductionSystemArea)

      End If

    End Sub

    Private Sub MergeHumanResourceList(ByVal NewList As ProductionHumanResourceList, ProductionSystemArea As ProductionSystemArea, ByVal ParamArray DeleteExcessDisciplines() As CommonData.Enums.Discipline)

      Dim TempList As New List(Of ProductionHumanResource)

      For Each child As ProductionHumanResource In ProductionSystemArea.ProductionHumanResourceList
        TempList.Add(child)
      Next

      'create a list where the same items are in the new list and the list to keep
      Dim InnerJoinList = (From tempItem In TempList
                           Join newPHR In NewList On tempItem.DisciplineID Equals newPHR.DisciplineID And
                                                     tempItem.PositionID Equals newPHR.PositionID And
                                                     tempItem.HumanResourceID Equals newPHR.HumanResourceID
                                                     Select tempItem, newPHR).ToList

      For Each item In InnerJoinList
        Dim index As Integer = NewList.IndexOf(item.newPHR)
        If index < 0 Then
          'Dim x As Integer = 0
          'debugging code
        ElseIf index >= NewList.Count Then
          'Dim x As Integer = 0
          'debugging code
        Else
          NewList.RemoveAt(index)
          index = TempList.IndexOf(item.tempItem)
          If index >= 0 Then
            TempList.RemoveAt(index)
          End If
        End If
      Next

      Dim query = From phr In NewList
                  Order By phr.DisciplineID, phr.PriorityServer
                  Select phr

      For Each NewPHR As ProductionHumanResource In query
        Dim PHRNew As ProductionHumanResource = NewPHR
        Dim match As ProductionHumanResource = Nothing

        If Not Singular.Misc.IsNullNothing(NewPHR.DisciplineID) AndAlso Not Singular.Misc.IsNullNothing(NewPHR.HumanResourceID) Then
          match = TempList.Where(Function(f) Singular.Misc.CompareSafe(f.HumanResourceID, PHRNew.HumanResourceID) AndAlso Singular.Misc.CompareSafe(f.PositionID, PHRNew.PositionID) AndAlso Singular.Misc.CompareSafe(f.DisciplineID, PHRNew.DisciplineID)).FirstOrDefault
          'lets just see if we can find an item with a matching discipline and make it more specific instead of adding duplicates
        End If
        If match Is Nothing Then
          If Not Singular.Misc.IsNullNothing(NewPHR.DisciplineID) AndAlso Not Singular.Misc.IsNullNothing(NewPHR.PositionID) Then
            'match = NewList.GetItemByPositionID(NewPHR.PositionID,NewPHR.DisciplineID)
            match = TempList.Where(Function(f) Singular.Misc.CompareSafe(f.PositionID, PHRNew.PositionID) AndAlso Singular.Misc.CompareSafe(f.DisciplineID, PHRNew.DisciplineID)).FirstOrDefault
            'lets just see if we can find an item with a matching discipline and make it more specific instead of adding duplicates
            If match Is Nothing Then
              match = TempList.Where(Function(f) Singular.Misc.CompareSafe(f.DisciplineID, PHRNew.DisciplineID)).FirstOrDefault
              If match IsNot Nothing Then
                match.PositionID = PHRNew.PositionID
                match.Position = PHRNew.Position
              End If
            End If
          ElseIf Not Singular.Misc.IsNullNothing(NewPHR.DisciplineID) Then
            match = TempList.Where(Function(f) Singular.Misc.CompareSafe(f.DisciplineID, PHRNew.DisciplineID) And Singular.Misc.IsNullNothing(f.PositionID)).FirstOrDefault
          End If
        End If

        If match IsNot Nothing Then
          'TempList and mProductionHumanResourceList both contain the exact same objects
          'Find the index of the matched object in the templist and remove it because we are done with it
          Dim index As Integer = TempList.IndexOf(match)
          If index >= 0 Then
            TempList.RemoveAt(index)
          End If
          'Find the index of the matched object in the mProductionHumanResourceList list and update the human resource is we have if
          index = ProductionSystemArea.ProductionHumanResourceList.IndexOf(match)
          Dim RealMatch As ProductionHumanResource = ProductionSystemArea.ProductionHumanResourceList.Item(index)
          If Not Singular.Misc.IsNullNothing(PHRNew.HumanResourceID) And Singular.Misc.IsNullNothing(RealMatch.HumanResourceID) Then
            RealMatch.HumanResourceID = PHRNew.HumanResourceID
            RealMatch.HumanResource = PHRNew.HumanResource
          End If
        Else
          'If Not mProductionHumanResourceList.Where(Function(itm) Singular.Misc.CompareSafe(itm.HumanResourceID, NewPHR.HumanResourceID)).Count > 0 Then
          Me.ProductionHumanResourceList.Add(NewPHR)
          'End If
          'If Not Singular.Misc.IsNullNothing(NewPHR.HumanResourceID) Then mProductionHumanResourceList.CheckRulesForHumanResourceID(NewPHR.HumanResourceID)
        End If

      Next

      For Each Discipline As CommonData.Enums.Discipline In DeleteExcessDisciplines
        Dim disc As CommonData.Enums.Discipline = Discipline
        For Each phr As ProductionHumanResource In TempList.Where(Function(f) Singular.Misc.CompareSafe(f.DisciplineID, CType(disc, Integer))) '.FilterList(False, "DisciplineID", Discipline)
          'Dim index As Integer = ProductionSystemArea.ProductionHumanResourceList.IndexOf(phr)
          Me.ProductionHumanResourceList.Remove(phr) '.RemoveAt(index)
        Next
      Next

    End Sub

    Public Sub PopulateHumanResources(ProductionSystemArea As ProductionSystemArea)

      'Clear the current schedules
      ProductionSystemArea.ClearProductionSchedule(Nothing)

      'Populate the Production Human Resources
      PopulateHR(ProductionSystemArea)

      'Generate the schedules
      ProductionSystemArea.GenerateAllProductionSchedules()

    End Sub

    Private Sub PopulateHR(ProductionSystemArea As ProductionSystemArea)

      If OBLib.Security.Settings.CurrentUser.SystemID = CommonData.Enums.System.ProductionServices Then

        AutoPopulateHumanResourcesForCameras(ProductionSystemArea)
        Dim PHRL As ProductionHumanResourceList = Productions.Areas.ProductionHumanResourceList.NewProductionHumanResourceList
        'add all the Human Resources that have been allocated to each vehicle
        For Each PV As ProductionVehicle In Me.GetParent.ProductionVehicleList
          Dim v As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleFull = CommonData.Lists.ROVehicleFullList.GetItem(PV.VehicleID)
          AutoPopulateVehicleHumanResources(PHRL, v, ProductionSystemArea)
        Next

        'merge the list from above with the HR list already on this production and remove any excess disciplines
        MergeHumanResourceList(PHRL, ProductionSystemArea, CommonData.Enums.Discipline.Driver, CommonData.Enums.Discipline.UnitSupervisor, CommonData.Enums.Discipline.Engineer, _
                                                           CommonData.Enums.Discipline.AudioMixer, CommonData.Enums.Discipline.AudioAssistant)


        'now go through each driver and try to allocate them to another discipline this production requires
        For Each phrDriver As ProductionHumanResource In ProductionSystemArea.ProductionHumanResourceList.Where(Function(f) Singular.Misc.CompareSafe(f.DisciplineID, CommonData.Enums.Discipline.Driver))
          Dim phrd As ProductionHumanResource = phrDriver
          Dim hr As OBLib.HR.ReadOnly.ROHumanResourceFull = CommonData.Lists.ROHumanResourceFullList.GetItem(phrDriver.HumanResourceID)
          'for every other production human resource where the discipline that is NOT driver and the human resource has not yet been set
          For Each phrOther As ProductionHumanResource In ProductionSystemArea.ProductionHumanResourceList.Where(Function(f) Singular.Misc.IsNullNothing(f.HumanResourceID) AndAlso Not Singular.Misc.CompareSafe(f.DisciplineID, CommonData.Enums.Discipline.Driver))
            Dim phrO As ProductionHumanResource = phrOther
            'make sure that this driver has not already been assigned to another discipline
            If Me.ProductionHumanResourceList.Where(Function(f) Singular.Misc.CompareSafe(f.DisciplineID, phrO.DisciplineID) AndAlso Singular.Misc.CompareSafe(f.HumanResourceID, phrd.HumanResourceID)).Count = 0 Then
              If hr.IsSkilled(Me.GetParent.ProductionTypeID, phrOther.DisciplineID, phrOther.PositionID, Me.GetParent.PlayStartDateTime) Then
                phrOther.HumanResourceID = hr.HumanResourceID
                phrOther.HumanResource = hr.PreferredFirstSurname
                Exit For
              End If
            End If
          Next
        Next

      ElseIf OBLib.Security.Settings.CurrentUser.SystemID = CommonData.Enums.System.ProductionContent Then
        AutoPopulateHumanResourcesForProductionSystem(ProductionSystemArea)
        Dim PHRL As ProductionHumanResourceList = Productions.Areas.ProductionHumanResourceList.NewProductionHumanResourceList
        'merge the list from above with the HR list already on this production and remove any excess disciplines
        MergeHumanResourceList(PHRL, ProductionSystemArea)

      End If

    End Sub

    Public Function HasEditHumanResourcesAccess() As Boolean

      Return Singular.Security.HasAccess("Productions", "Edit Human Resources")

    End Function

    Public Function HasEditHumanResourcesAfterFinalisedAccess() As Boolean

      Return Singular.Security.HasAccess("Productions", "Can Edit Human Resources after finalised")

    End Function

    Public Function HasEditHumanResourcesBeforeFinalisedAccess() As Boolean

      Return Singular.Security.HasAccess("Productions", "Can Edit Human Resources before finalised")

    End Function

    Public Function TimelineIsReady() As Boolean
      Return Me.GetParent.TimelineReadyInd
    End Function

    Public Function IsOnOrAfterCrewFinalised() As Boolean

      Dim CrewFinalisedStatusIDs = {2, 3, 4, 5}
      Dim AreaCrewFinalised As Boolean = If(CrewFinalisedStatusIDs.Contains(Me.ProductionAreaStatusID), True, False)
      Dim ParentCrewFinalised As Boolean = Me.GetParent.LoadedCrewFinalised

      If AreaCrewFinalised And ParentCrewFinalised Then
        Return True
      End If

      Return False

    End Function

    'Public Function CanEditHumanResources() As String

    '  If Not HasEditHumanResourcesAccess() Then
    '    Return "You do not have the security permissions to make changes to the human resources"
    '  End If

    '  If IsCrewFinalised() Then
    '    If Not HasEditHumanResourcesAfterFinalisedAccess() Then
    '      Return "You are not authorized to make changes to the human resources after they have been finalized"
    '    End If
    '  Else
    '    If Not HasEditHumanResourcesBeforeFinalisedAccess() Then
    '      Return "You are not authorized to make changes to the human resources before they have been finalized"
    '    End If
    '  End If

    '  Return ""

    'End Function

    Public ReadOnly Property CanEditHumanResources As String
      Get
        If Not HasEditHumanResourcesAccess() Then
          Return "You do not have the security permissions to make changes to the human resources"
        End If
        If Not TimelineIsReady() Then
          If SystemID = 2 Then
            Return ""
          End If
          'Event Manager
          If Singular.Security.HasAccess("Productions", "Can Finalise Timeline") Then
            Return ""
          Else
            Return "You are not authorised to change crew before the timeline has been finalised"
          End If
        ElseIf IsOnOrAfterCrewFinalised() Then
          'Vusi, Chevelle
          If Not HasEditHumanResourcesAfterFinalisedAccess() Then
            Return "You are not authorized to make changes to the human resources after they have been finalized"
          End If
        Else
          'Event Managers after Crew Finalised
          If Not HasEditHumanResourcesBeforeFinalisedAccess() Then
            Return "You are not authorized to make changes to the human resources before they have been finalized"
          End If
        End If
        Return ""
      End Get
    End Property


    Public Function CanDeleteHumanResources() As String

      If Not HasEditHumanResourcesAccess() Then
        Return "You do not have the security permissions to make changes to the human resources"
      End If

      Return ""

    End Function

#End Region

#Region " Schedules "

    Public Function GenerateScheduleTemplate(ByVal HRID As Integer?) As List(Of Helpers.Templates.HRSchedule)

      Dim VenueCityID As Integer?
      Dim ROProductionVenue As Maintenance.Productions.ReadOnly.ROProductionVenueOld = CommonData.Lists.ROProductionVenueList.GetItem(Me.GetParent.ProductionVenueID)
      Dim ProductionVenueCity As Maintenance.Locations.ReadOnly.ROCity = Nothing
      If ROProductionVenue IsNot Nothing Then
        'ProductionVenueCity = CommonData.Lists.ROCityList.GetItem(ROProductionVenue.CityID)
        VenueCityID = ROProductionVenue.CityID
      End If

      Dim FlatList As List(Of Helpers.Templates.HRScheduleTemplate) = (From pt In ProductionTimelineList
                                                                        Join pttd In CommonData.Lists.ROProductionTimelineTypeDisciplineList On pttd.ProductionTimelineTypeID Equals pt.ProductionTimelineTypeID
                                                                        Join phr In ProductionHumanResourceList On phr.DisciplineID Equals pttd.DisciplineID
                                                                        Where ( _
                                                                                ((HRID IsNot Nothing AndAlso phr.HumanResourceID = HRID) Or HRID Is Nothing) _
                                                                                And phr.HumanResourceID IsNot Nothing _
                                                                                And phr.DisciplineID IsNot Nothing
                                                                        )
                                                                        Select New Helpers.Templates.HRScheduleTemplate(phr.HumanResourceID, phr.HumanResource,
                                                                                                                        phr.DisciplineID, phr.VehicleID,
                                                                                                                        pt.ProductionTimelineTypeID,
                                                                                                                        pt.StartDateTime, pt.EndDateTime,
                                                                                                                        pt, phr)).ToList
      'Join ptt In CommonData.Lists.ROProductionTimelineTypeList On ptt.ProductionTimelineTypeID Equals pt.ProductionTimelineTypeID
      'Join hr In CommonData.Lists.ROHumanResourceList On hr.HumanResourceID Equals phr.HumanResourceID
      'Join cit In CommonData.Lists.ROCityList On cit.CityID Equals hr.CityID
      '_And ((phr.VehicleID Is Nothing Or pt.VehicleID Is Nothing) Or (CompareSafe(phr.VehicleID, pt.VehicleID)))
      'pt.ProductionTimelineID, 
      'phr.ProductionHumanResourceID,
      ', hr.CityID, cit.CityCode, VenueCityID
      'ptt.ProductionTimelineType,
      'ptt.PreTransmissionInd, ptt.FreeInd,

      Dim StructuredList As List(Of Helpers.Templates.HRSchedule) = (From data In FlatList
                                                                     Group By HumanResourceID = data.HumanResourceID, HumanResource = data.HumanResource Into Group
                                                                     Select New Helpers.Templates.HRSchedule(HumanResourceID, HumanResource, VenueCityID, Group)).OrderBy(Function(d) d.HumanResource).ToList
      ', _ CityID = data.CityID, CityCode = data.CityCode, Local = data.Local 
      ', CityID, CityCode, Local

      Dim Temp As List(Of ProductionHumanResource) = (From phr In ProductionHumanResourceList
                                                              Where phr.HumanResourceID IsNot Nothing _
                                                                And phr.DisciplineID IsNot Nothing).ToList


      Dim HRMissing As List(Of ProductionHumanResource) = (From data In Temp
                                                           Where Not StructuredList.Select(Function(d) d.HumanResourceID).ToList.Contains(data.HumanResourceID)).ToList

      Return StructuredList

    End Function

    Public Sub GenerateAllProductionSchedules()
      ClearProductionSchedule(Nothing)
      GenerateProductionSchedule(Nothing)
    End Sub

    Public Sub ClearProductionSchedule(HRID As Integer?)
      If HRID Is Nothing Then

        'Me.HRProductionScheduleList.Clear()

        Dim TempHRProductionScheduleDetailList As New List(Of HRProductionScheduleDetail)

        For Each hrs As HRProductionSchedule In Me.HRProductionScheduleList
          For Each hrsd As HRProductionScheduleDetail In hrs.HRProductionScheduleDetailList
            If hrsd.CanEditScheduleCheck() Then
              TempHRProductionScheduleDetailList.Add(hrsd)
            End If
          Next

          For Each hrsd As HRProductionScheduleDetail In TempHRProductionScheduleDetailList
            hrs.HRProductionScheduleDetailList.Remove(hrsd)
          Next
          TempHRProductionScheduleDetailList = New List(Of HRProductionScheduleDetail)
        Next


        'Me.HRProductionScheduleList.Clear()
        'Dim HRProductionSchedule As HRProductionSchedule = Me.HRProductionScheduleList.GetItem(HRID)
        'Dim Index As Integer = Me.HRProductionScheduleList.IndexOf(HRProductionSchedule)

        'If HRProductionSchedule.HRProductionScheduleDetailList.Where(Function(c) c.CanEditScheduleCheck).Count() = 0 Then
        '  Me.HRProductionScheduleList.RemoveAt(Index)
        'Else
        '  Dim TempHRProductionScheduleDetailList As New List(Of HRProductionScheduleDetail)

        '  For Each hrpsd As HRProductionScheduleDetail In HRProductionSchedule.HRProductionScheduleDetailList
        '    If hrpsd.CanEditScheduleCheck() Then
        '      TempHRProductionScheduleDetailList.Add(hrpsd)
        '    End If
        '  Next

        '  For Each hrsd As HRProductionScheduleDetail In TempHRProductionScheduleDetailList
        '    Me.HRProductionScheduleList.GetItem(HRID).HRProductionScheduleDetailList.Remove(hrsd)
        '  Next

        'End If
      Else
        Dim HRProductionSchedule As HRProductionSchedule = Me.HRProductionScheduleList.GetItem(HRID)
        Dim Index As Integer = Me.HRProductionScheduleList.IndexOf(HRProductionSchedule)
        Me.HRProductionScheduleList.RemoveAt(Index)
      End If
    End Sub

    Public Sub CheckAllPreAndPostTravel()
      For Each item As HRProductionSchedule In Me.HRProductionScheduleList
        item.CheckPreAndPostTravel()
      Next
    End Sub

    Public Sub SetupAllUnSelectedTimelines()
      For Each item As HRProductionSchedule In Me.HRProductionScheduleList
        item.SetupUnSelectedTimelines()
      Next
    End Sub

    Public Function GenerateProductionSchedule(HRID As Integer?)
      Dim Template As List(Of Helpers.Templates.HRSchedule) = GenerateScheduleTemplate(HRID)
      AddNewSchedules(HRID, Template)
      ProcessSchedules(HRID, Template)
      Return Template
    End Function

    Public Sub AddNewSchedule(HRSchedule As Helpers.Templates.HRSchedule)
      Dim Schedule As HRProductionSchedule = Nothing
      Schedule = Me.HRProductionScheduleList.GetItem(HRSchedule.HumanResourceID)

      If Schedule Is Nothing Then
        Schedule = Me.HRProductionScheduleList.AddNew
        With Schedule
          .HumanResourceID = HRSchedule.HumanResourceID
          .HumanResource = HRSchedule.HumanResource
          .Local = HRSchedule.Local
          .CityID = HRSchedule.CityID
          .CityCode = HRSchedule.CityCode
          .Disciplines = HRSchedule.Disciplines
          .CrewType = HRSchedule.CrewType
          .ProductionID = Me.ProductionID
          .SystemID = Me.SystemID
          .ProductionAreaID = Me.ProductionAreaID
          .Visible = True
        End With
      End If

      HRSchedule.HRScheduleTemplateList.ForEach(Sub(ScheduleItemTemplate)

                                                  Dim AlreadyContainsItem As Boolean = Schedule.HRProductionScheduleDetailList.Contains(ScheduleItemTemplate.ProductionTimeline.Guid)
                                                  Dim VehiclesEqual As Boolean = False

                                                  If (ScheduleItemTemplate.ProductionHumanResource.VehicleID Is Nothing _
                                                      OrElse ScheduleItemTemplate.ProductionTimeline.VehicleID Is Nothing _
                                                      OrElse CompareSafe(ScheduleItemTemplate.ProductionHumanResource.VehicleID, ScheduleItemTemplate.ProductionTimeline.VehicleID)) Then
                                                    VehiclesEqual = True
                                                  End If

                                                  If VehiclesEqual And Not AlreadyContainsItem Then
                                                    Dim NewScheduleItem As New HRProductionScheduleDetail '= Schedule.HRProductionScheduleDetailList.AddNew
                                                    With NewScheduleItem
                                                      .HumanResourceID = ScheduleItemTemplate.HumanResourceID
                                                      .CrewScheduleDetailID = Nothing
                                                      .ProductionHumanResourceID = ScheduleItemTemplate.ProductionHumanResourceID
                                                      .ProductionTimelineID = ScheduleItemTemplate.ProductionTimelineID
                                                      .TimesheetDate = ScheduleItemTemplate.TimesheetDate
                                                      .StartDateTime = ScheduleItemTemplate.StartDateTime
                                                      .EndDateTime = ScheduleItemTemplate.EndDateTime
                                                      .Detail = ""
                                                      .ExcludeInd = False
                                                      .ExcludeReason = ""
                                                      .CrewTimesheetID = Nothing
                                                      .ProductionTimelineTypeID = ScheduleItemTemplate.ProductionTimelineTypeID
                                                      .ProductionTimelineType = ScheduleItemTemplate.ProductionTimelineType
                                                      .HumanResourceID = ScheduleItemTemplate.HumanResourceID
                                                      .Clash = ""
                                                      .Visible = False
                                                      .ProductionHumanResource = ScheduleItemTemplate.ProductionHumanResource
                                                      .ProductionTimeline = ScheduleItemTemplate.ProductionTimeline
                                                      .SystemID = ScheduleItemTemplate.SystemID
                                                      .ProductionAreaID = ScheduleItemTemplate.ProductionAreaID
                                                      .HumanResourceScheduleTypeID = CType(OBLib.CommonData.Enums.HumanResourceScheduleType.Production, Integer)
                                                      .HumanResourceShiftID = Nothing
                                                      .HumanResourceOffPeriodID = Nothing
                                                      .HumanResourceID = ScheduleItemTemplate.HumanResourceID
                                                      .StartTimeOffset = 0
                                                      .EndTimeOffset = 0
                                                      .ProductionTimelineMatchInd = True
                                                    End With
                                                    Schedule.HRProductionScheduleDetailList.Add(NewScheduleItem)

                                                  End If

                                                End Sub)

    End Sub

    Public Sub AddNewSchedules(HRID As Integer?, ByVal HRProductionSchedule As List(Of Helpers.Templates.HRSchedule))
      Dim Schedule As HRProductionSchedule = Nothing
      If HRID IsNot Nothing Then
        HRProductionSchedule.Where(Function(d) d.HumanResourceID = HRID).ToList.ForEach(Sub(HRSchedule)
                                                                                          'Schedule = Me.HRProductionScheduleList.GetItem(HRSchedule.HumanResourceID)
                                                                                          'If Schedule Is Nothing Then
                                                                                          AddNewSchedule(HRSchedule)
                                                                                          'End If
                                                                                        End Sub)
      Else
        HRProductionSchedule.ForEach(Sub(HRSchedule)
                                       'Schedule = Me.HRProductionScheduleList.GetItem(HRSchedule.HumanResourceID)
                                       'If Schedule Is Nothing Then
                                       AddNewSchedule(HRSchedule)
                                       'End If
                                     End Sub)
      End If

    End Sub

    Public Sub ProcessSchedules(HRID As Integer?, Template As List(Of Helpers.Templates.HRSchedule))
      For Each HRProductionSchedule As HRProductionSchedule In Me.HRProductionScheduleList
        HRProductionSchedule.RemoveFreeIndOnDay()
        HRProductionSchedule.AddNewItems(Template)
        HRProductionSchedule.RemoveUnNeededDriverItems()
        'HRProductionSchedule.RemoveDuplicates()
        If HRProductionSchedule.Local Then
          HRProductionSchedule.RemoveTravel()
        End If
        If HRProductionSchedule.Local Or HRProductionSchedule.CityID = 1 Then
          HRProductionSchedule.RemoveDaysAway()
        End If
      Next
    End Sub

    Public Sub UpdateFromHRTimelineSelect(HRTimelineSelect As OBLib.Helpers.Templates.HRTimelineSelect)

      For Each HRSelect As OBLib.Helpers.Templates.HRSelect In HRTimelineSelect.HRSelectList
        If HRSelect.SelectInd Then
          Dim HRProductionSchedule As HRProductionSchedule = Me.HRProductionScheduleList.GetItem(HRSelect.HumanResourceID)
          For Each TimelineSelect As OBLib.Helpers.Templates.TimelineSelect In HRTimelineSelect.TimelineSelectList
            If TimelineSelect.SelectInd Then
              Select Case HRTimelineSelect.Mode
                Case "Add Items"
                  HRProductionSchedule.AddTimeline(TimelineSelect.ProductionTimeline)
                Case "Update Items"
                  HRProductionSchedule.UpdateTimeline(TimelineSelect.ProductionTimeline)
                Case "Delete Items"
                  HRProductionSchedule.RemoveTimeline(TimelineSelect.ProductionTimeline)
              End Select
            End If
          Next
        End If
      Next

    End Sub

    'Public Sub CheckScheduleValid()

    '  Dim ds As New DataSet("DataSet")
    '  Dim dt As DataTable = ds.Tables.Add("Table")
    '  dt.Columns.Add("GUID", GetType(Guid)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("ProductionID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("SystemID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("ProductionAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("CrewScheduleDetailID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("ProductionTimelineID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
    '  dt.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute

    '  Dim query = From HRSched As HRProductionSchedule In HRProductionScheduleList
    '              From HRSchedDetail As HRProductionScheduleDetail In HRSched.HRProductionScheduleDetailList
    '              Select New With { _
    '                                Key .Guid = HRSchedDetail.Guid, _
    '                                Key .HumanResourceID = HRSched.HumanResourceID, _
    '                                Key .ProductionID = HRSched.ProductionID, _
    '                                Key .SystemID = HRSched.SystemID, _
    '                                Key .ProductionAreaID = HRSched.ProductionAreaID, _
    '                                Key .CrewScheduleDetailID = HRSchedDetail.CrewScheduleDetailID, _
    '                                Key .ProductionTimelineID = HRSchedDetail.ProductionTimelineID, _
    '                                Key .StartDateTime = HRSchedDetail.StartDateTime, _
    '                                Key .EndDateTime = HRSchedDetail.EndDateTime
    '                              }


    '  For Each obj In query
    '    Dim row As DataRow = dt.NewRow
    '    row("Guid") = obj.Guid
    '    row("HumanResourceID") = obj.HumanResourceID
    '    row("ProductionID") = NothingDBNull(obj.ProductionID)
    '    row("SystemID") = obj.SystemID
    '    row("ProductionAreaID") = obj.ProductionAreaID
    '    row("CrewScheduleDetailID") = NothingDBNull(obj.CrewScheduleDetailID)
    '    row("ProductionTimelineID") = NothingDBNull(obj.ProductionTimelineID)
    '    row("StartDateTime") = obj.StartDateTime
    '    row("EndDateTime") = obj.EndDateTime
    '    dt.Rows.Add(row)
    '  Next

    '  Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdValidateAreaSchedule]",
    '                                      "@AreaScheduleXML",
    '                                      ds.GetXml)
    '  cmd.FetchType = CommandProc.FetchTypes.DataSet
    '  cmd = cmd.Execute

    '  'Reset the clashes
    '  For Each HRPS As HRProductionSchedule In Me.HRProductionScheduleList
    '    HRPS.AllClashes = ""
    '    For Each HRPSD As HRProductionScheduleDetail In HRPS.HRProductionScheduleDetailList
    '      HRPSD.Clash = ""
    '    Next
    '  Next

    '  Dim HRProductionSchedule As HRProductionSchedule = Nothing
    '  Dim HumanResourceID As Integer? = Nothing
    '  Dim GuidString As String = ""
    '  Dim GUid As Guid = Nothing
    '  Dim AreaScheduleDetail As HRProductionScheduleDetail = Nothing

    '  For Each dr As DataRow In cmd.Dataset.Tables(0).Rows
    '    HumanResourceID = dr(0)
    '    HRProductionSchedule = Me.HRProductionScheduleList.GetItem(HumanResourceID)
    '    If HRProductionSchedule IsNot Nothing Then
    '      GUid = dr(1)
    '      AreaScheduleDetail = HRProductionSchedule.HRProductionScheduleDetailList.GetItemByGuid(GUid)
    '      If AreaScheduleDetail IsNot Nothing Then
    '        Dim Clash As String = dr(4)
    '        HRProductionSchedule.AllClashes &= Clash & ", <br/>"
    '        AreaScheduleDetail.Clash &= Clash
    '      End If
    '    End If
    '  Next

    'End Sub

    'Public Sub CheckScheduleValidOld(HumanResourceID As Integer?)

    '  Dim sd As DateTime? = Nothing
    '  Dim ed As DateTime = Nothing
    '  For Each hrps As HRProductionSchedule In Me.HRProductionScheduleList
    '    If hrps.HRProductionScheduleDetailList.Count > 0 Then
    '      'Use Schedule
    '      sd = hrps.HRProductionScheduleDetailList.Min(Function(d) d.StartDateTime)
    '      ed = hrps.HRProductionScheduleDetailList.Max(Function(d) d.EndDateTime)
    '    Else
    '      'Use Timeline
    '      Me.GetMinMaxTimesForHumanResourceFromTimeline(hrps.HumanResourceID, sd, ed)
    '    End If
    '    Dim cmd As New Singular.CommandProc("CmdProcs.cmdHumanResourceAvailable", _
    '                             New String() {"ProductionID",
    '                                           "HumanResourceID",
    '                                            "StartDate",
    '                                            "EndDate",
    '                                            "CheckID",
    '                                            "SystemID",
    '                                            "ProductionHumanResourceID",
    '                                            "HumanResourceOffPeriodID"}, _
    '                             New Object() {Singular.Misc.NothingDBNull(hrps.ProductionID),
    '                                             hrps.HumanResourceID,
    '                                             New SmartDate(sd).DBValue,
    '                                             New SmartDate(ed).DBValue,
    '                                             Singular.Misc.NothingDBNull(Nothing),
    '                                             Singular.Misc.NothingDBNull(hrps.SystemID),
    '                                             Singular.Misc.NothingDBNull(Nothing),
    '                                             Singular.Misc.NothingDBNull(Nothing)})
    '    cmd.FetchType = Singular.CommandProc.FetchTypes.DataRow
    '    cmd = cmd.Execute(0)
    '    Dim ErrorMessage As String = ""
    '    ErrorMessage = Singular.Misc.IsNull(cmd.DataRow(0), "")
    '    If ErrorMessage.Trim.Length = 0 Then
    '      ErrorMessage = ""
    '    End If
    '    hrps.AllClashes = ""
    '    hrps.AllClashes = ErrorMessage
    '  Next

    '  'For Each phr As ProductionHumanResource In Me.ProductionHumanResourceList
    '  '  If phr.HumanResourceID IsNot Nothing AndAlso (HumanResourceID Is Nothing Or CompareSafe(HumanResourceID, phr.HumanResourceID)) Then
    '  '    Dim ProductionSchedule As HRProductionSchedule = Me.HRProductionScheduleList.GetItem(phr.HumanResourceID)
    '  '    Dim sd As DateTime? = Nothing
    '  '    Dim ed As DateTime = Nothing
    '  '    If ProductionSchedule IsNot Nothing AndAlso ProductionSchedule.HRProductionScheduleDetailList.Count > 0 Then
    '  '      sd = ProductionSchedule.HRProductionScheduleDetailList.Min(Function(d) d.StartDateTime)
    '  '      ed = ProductionSchedule.HRProductionScheduleDetailList.Max(Function(d) d.EndDateTime)
    '  '    Else
    '  '      Me.GetMinMaxTimesForHumanResourceFromTimeline(phr.HumanResourceID, sd, ed)
    '  '    End If
    '  '    Dim cmd As New Singular.CommandProc("CmdProcs.cmdHumanResourceAvailable", _
    '  '                            New String() {"ProductionID",
    '  '                                            "HumanResourceID",
    '  '                                            "StartDate",
    '  '                                            "EndDate",
    '  '                                            "CheckID",
    '  '                                            "SystemID",
    '  '                                            "ProductionHumanResourceID",
    '  '                                            "HumanResourceOffPeriodID"}, _
    '  '                            New Object() {Singular.Misc.NothingDBNull(phr.ProductionID),
    '  '                                            phr.HumanResourceID,
    '  '                                            New SmartDate(sd).DBValue,
    '  '                                            New SmartDate(ed).DBValue,
    '  '                                            Singular.Misc.NothingDBNull(Nothing),
    '  '                                            Singular.Misc.NothingDBNull(phr.SystemID),
    '  '                                            Singular.Misc.ZeroDBNull(phr.ProductionHumanResourceID),
    '  '                                            Singular.Misc.NothingDBNull(Nothing)})
    '  '    cmd.FetchType = Singular.CommandProc.FetchTypes.DataRow
    '  '    cmd = cmd.Execute(0)
    '  '    Dim ErrorMessage As String = ""
    '  '    ErrorMessage = Singular.Misc.IsNull(cmd.DataRow(0), "")
    '  '    If ErrorMessage.Trim.Length = 0 Then
    '  '      ErrorMessage = ""
    '  '    End If
    '  '    phr.Clash = ""
    '  '    phr.Clash = ErrorMessage
    '  '  End If
    '  'Next

    'End Sub

    Friend Sub GetMinMaxTimesForHumanResourceFromTimeline(ByVal iHumanResourceID As Integer?, ByRef StartDateTime As DateTime?, ByRef EndDateTime As DateTime?)

      Dim query3 = _
      From i In (From pt In ProductionTimelineList
                  Join ptt In CommonData.Lists.ROProductionTimelineTypeList On pt.ProductionTimelineTypeID Equals ptt.ProductionTimelineTypeID
                  Join pttd In CommonData.Lists.ROProductionTimelineTypeDisciplineList On pt.ProductionTimelineTypeID Equals pttd.ProductionTimelineTypeID
                  Join phr In ProductionHumanResourceList On phr.DisciplineID Equals pttd.DisciplineID And pt.ProductionID Equals phr.ProductionID
                  Where Singular.Misc.CompareSafe(phr.HumanResourceID, iHumanResourceID) AndAlso
                        Not ptt.FreeInd AndAlso
                        (Not phr.IsDriver OrElse
                         phr.IsDriver AndAlso pt.IsVehicleTravel AndAlso (Singular.Misc.CompareSafe(phr.VehicleID, pt.VehicleID) OrElse
                                                                          Singular.Misc.IsNullNothing(phr.VehicleID) OrElse
                                                                          Singular.Misc.IsNullNothing(pt.VehicleID))) _
                        Or pt.ProductionTimelineTypeID = CommonData.Enums.ProductionTimelineType.TempTimelineBookings
                      Group By phr.HumanResourceID
      Into MinStartDate = Min(pt.StartDateTime), MaxEndDate = Max(pt.EndDateTime)
                  Select MinStartDate, MaxEndDate)
                Select New With {Key .StartDateTime = i.MinStartDate, _
                                 Key .EndDateTime = i.MaxEndDate}

      For Each item In query3
        StartDateTime = item.StartDateTime
        EndDateTime = item.EndDateTime
      Next

      If StartDateTime Is Nothing Or EndDateTime Is Nothing Then
        StartDateTime = Me.ProductionTimelineList.Min(Function(d) d.StartDateTime)
        EndDateTime = Me.ProductionTimelineList.Max(Function(d) d.EndDateTime)
      End If

    End Sub

    Public Sub ClearSchedule()

      Dim TempHRProductionScheduleDetailList As New List(Of HRProductionScheduleDetail)

      For Each hrs As HRProductionSchedule In Me.HRProductionScheduleList
        For Each hrsd As HRProductionScheduleDetail In hrs.HRProductionScheduleDetailList
          If hrsd.CanEditScheduleCheck() Then
            TempHRProductionScheduleDetailList.Add(hrsd)
          End If
        Next
        'hrs.HRProductionScheduleDetailList.Clear()
        For Each hrsd As HRProductionScheduleDetail In TempHRProductionScheduleDetailList
          hrs.HRProductionScheduleDetailList.Remove(hrsd)
        Next
        TempHRProductionScheduleDetailList = New List(Of HRProductionScheduleDetail)
      Next

    End Sub

    Public Sub CheckAvailability(Optional HumanResourceID As Integer? = Nothing)

      OBLib.Helpers.ProductionSchedules.CheckProductionAvailabilityOB(Me, HumanResourceID)

    End Sub

#End Region

#End Region

    Public Sub MakeSchedulesDirty()

      For Each hrs As HRProductionSchedule In Me.HRProductionScheduleList
        For Each csd As HRProductionScheduleDetail In hrs.HRProductionScheduleDetailList
          csd.Dirtify = Not csd.Dirtify
        Next
      Next

      For Each hrs As HRProductionSchedule In Me.HRProductionScheduleList
        Dim unmatchedCSDs As New List(Of HRProductionScheduleDetail)
        For Each csd As HRProductionScheduleDetail In hrs.HRProductionScheduleDetailList
          Dim phrList As List(Of ProductionHumanResource) = Me.ProductionHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, csd.HumanResourceID)).ToList
          If phrList.Count = 0 Then
            unmatchedCSDs.Add(csd)
          End If
        Next
        unmatchedCSDs.ForEach(Sub(itm)
                                hrs.HRProductionScheduleDetailList.Remove(itm)
                              End Sub)
      Next

    End Sub

    Public Sub CalculateProductionHR()

      OBLib.CommonData.Refresh("ROHumanResourceList")
      Dim rohr As HR.ReadOnly.ROHumanResource = Nothing
      Me.ProductionHROBList.Clear()

      For Each phr As ProductionHumanResource In Me.ProductionHumanResourceList
        If Not IsNullNothing(phr.HumanResourceID) Then
          rohr = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(phr.HumanResourceID)
          If phr.ExistingProductionHR Is Nothing Then
            Dim ph As ProductionHROB = New ProductionHROB()
            ph.HumanResourceID = phr.HumanResourceID
            ph.HumanResource = phr.HumanResource
            ph.CityID = rohr.CityID
            ph.CityCode = rohr.City
            ph.ProductionSystemAreaID = Me.ProductionSystemAreaID
            Me.ProductionHROBList.Add(ph)
          End If
        End If
      Next

      'Remove those hr's which were removed from the production
      Dim phToRemove As New List(Of ProductionHROB)
      For Each ph As ProductionHROB In Me.ProductionHROBList
        If ph.ExistingProductionHumanResources.Count = 0 Then
          phToRemove.Add(ph)
        End If
      Next
      phToRemove.ForEach(Sub(ph)
                           Me.ProductionHROBList.Remove(ph)
                         End Sub)

      'Create the resource bookings
      Dim ROStatus As ROProductionAreaAllowedStatus = OBLib.CommonData.Lists.ROProductionAreaAllowedStatusList.GetItem(ProductionAreaStatusID, SystemID, ProductionAreaID)
      For Each ph As ProductionHROB In Me.ProductionHROBList
        'If no Production HR
        If ph.ResourceBookingOBList.Count = 0 Then
          'Add New One
          ph.ResourceBookingOBList.AddNew()
        End If
        rohr = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(ph.HumanResourceID)
        ph.ResourceBookingOBList(0).HumanResourceID = rohr.HumanResourceID
        ph.ResourceBookingOBList(0).ResourceID = rohr.ResourceID
        ph.ResourceBookingOBList(0).ResourceName = rohr.PreferredFirstSurname
        If SystemID = 2 Then
          ph.ResourceBookingOBList(0).ResourceBookingTypeID = 2
        Else
          ph.ResourceBookingOBList(0).ResourceBookingTypeID = 1
        End If
        ph.ResourceBookingOBList(0).ResourceBookingDescription = Me.GetParent.ProductionDescription
        ph.ResourceBookingOBList(0).StartDateTimeBuffer = Nothing
        ph.ResourceBookingOBList(0).StatusCssClass = ROStatus.CssClass
        If Me.ProductionAreaStatusID = CType(OBLib.CommonData.Enums.ProductionAreaStatus.Cancelled, Integer) Then
          ph.ResourceBookingOBList(0).IsCancelled = True
          ph.ResourceBookingOBList(0).IgnoreClashes = True
          ph.ResourceBookingOBList(0).IgnoreClashesReason = "Production Cancelled"
          ph.ResourceBookingOBList(0).IsLocked = True
          ph.ResourceBookingOBList(0).IsLockedReason = "Cancelled"
        ElseIf Me.ProductionAreaStatusID = CType(OBLib.CommonData.Enums.ProductionAreaStatus.Reconciled, Integer) Then
          ph.ResourceBookingOBList(0).IsLocked = True
          ph.ResourceBookingOBList(0).IsLockedReason = "Reconciled"
        Else
          ph.ResourceBookingOBList(0).IsLocked = False
          ph.ResourceBookingOBList(0).IsLockedReason = ""
          ph.ResourceBookingOBList(0).IsCancelled = False
          ph.ResourceBookingOBList(0).IgnoreClashes = False
          ph.ResourceBookingOBList(0).IgnoreClashesReason = ""
        End If
        ph.ResourceBookingOBList(0).EndDateTimeBuffer = Nothing
      Next

      'Ensure the CSD's are linked up correctly
      For Each ph As ProductionHROB In Me.ProductionHROBList
        ph.Fiddle()
        For Each hr As HRProductionSchedule In Me.HRProductionScheduleList
          If CompareSafe(hr.HumanResourceID, ph.HumanResourceID) Then
            ph.ResourceBookingOBList(0).StartDateTime = hr.HRProductionScheduleDetailList.Where(Function(d) Not d.ExcludeInd).Min(Function(c) c.StartDateTime)
            ph.ResourceBookingOBList(0).EndDateTime = hr.HRProductionScheduleDetailList.Where(Function(d) Not d.ExcludeInd).Max(Function(c) c.EndDateTime)
            ph.ResourceBookingOBList(0).CopySchedule(hr.HRProductionScheduleDetailList)
            If ph.ResourceBookingOBList(0).CrewScheduleDetailOBList.Count = 0 Then
              ph.ResourceBookingOBList(0).StartDateTime = Me.ProductionTimelineList.Min(Function(d) d.StartDateTime)
              ph.ResourceBookingOBList(0).EndDateTime = Me.ProductionTimelineList.Max(Function(d) d.EndDateTime)
            End If
            'For Each csd As HRProductionScheduleDetail In hr.HRProductionScheduleDetailList
            '  csd.Fiddle()
            'Next
          End If
        Next
      Next

      'ph.ResourceBookingOBList(0).StartDateTime = Me.HRProductionScheduleList.Where(Function(d) CompareSafe(d.HumanResourceID, ph.HumanResourceID)).FirstOrDefault.HRProductionScheduleDetailList.Min(Function(c) c.StartDateTime)
      'ph.ResourceBookingOBList(0).EndDateTime = Me.HRProductionScheduleList.Where(Function(d) CompareSafe(d.HumanResourceID, ph.HumanResourceID)).FirstOrDefault.HRProductionScheduleDetailList.Min(Function(d) d.EndDateTime)
      'If IsNullNothing(ph.ResourceBookingOBList(0).StartDateTime) Or IsNullNothing(ph.ResourceBookingOBList(0).EndDateTime) Then
      '  Dim SD As DateTime
      '  Dim ED As DateTime
      '  GetMinMaxTimesForHumanResourceFromTimeline(ph.HumanResourceID, SD, ED)
      '  ph.ResourceBookingOBList(0).StartDateTime = SD
      '  ph.ResourceBookingOBList(0).EndDateTime = ED
      'End If

      Me.ProductionHROBList.CheckRules()

    End Sub

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(ProductionSystemAreaClashesProperty,
                 Function(d) d.ProductionSystemAreaClashes <> "",
                 Function(d) d.ProductionSystemAreaClashes)
      End With

      With AddWebRule(NotComplySpecReasonsProperty)
        '.ServerRuleFunction = AddressOf CheckNotComplySpecReasonsValid
        .JavascriptRuleCode = OBLib.JSCode.ProductionSystemAreaJS.SpecRequirementValid
        .AddTriggerProperty(SpecNotCompliantProperty)
        '.ASyncBusyText = "Checking Spec Compliance"
      End With


      With AddWebRule(ChecklTravelTimelineRuleProperty)
        .AddTriggerProperties({ProductionTimelineListProperty})
        .AffectedProperties.AddRange({ProductionTimelineListProperty})
        '.ServerRuleFunction = AddressOf CheckNotComplySpecReasonsValid
        .JavascriptRuleFunctionName = "ProductionHelper.Production.Rules.CheckTravelTimelines"
      End With

      'With AddWebRule(ProductionSpecRequirementIDProperty)
      '  .JavascriptRuleCode = OBLib.JSCode.ProductionSystemAreaJS.SpecRequirementValid
      'End With


    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionSystemArea() method.

    End Sub

    Public Shared Function NewProductionSystemArea() As ProductionSystemArea

      Return DataPortal.CreateChild(Of ProductionSystemArea)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionSystemArea(dr As SafeDataReader) As ProductionSystemArea

      Dim p As New ProductionSystemArea()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionSystemAreaIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(StatusDateProperty, .GetValue(5))
          LoadProperty(StatusSetByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ProductionSystemAreaClashesProperty, .GetString(11))
          LoadProperty(ProductionSpecRequirementIDProperty, ZeroNothing(.GetInt32(12)))
          LoadProperty(NotComplySpecReasonsProperty, .GetString(13))
          LoadProperty(LoadedProductionAreaStatusIDProperty, ProductionAreaStatusID)
          LoadProperty(LoadedStatusDateProperty, StatusDate)
          LoadProperty(LoadedStatusSetByUserIDProperty, StatusSetByUserID)
          LoadProperty(StatusSetDateProperty, .GetDateTime(14))
          LoadProperty(RoomScheduleIDProperty, ZeroNothing(.GetInt32(15)))
          'LoadProperty(RoomIDProperty, ZeroNothing(.GetInt32(16)))
          LoadProperty(MealReimbursementProperty, .GetBoolean(17))
          LoadProperty(IsBeingEditedBySomeoneElseProperty, .GetBoolean(18))
          LoadProperty(InEditByNameProperty, .GetString(19))
          LoadProperty(InEditDateTimeProperty, .GetValue(20))
          LoadProperty(EditImagePathProperty, .GetString(21))
          LoadProperty(ResourceBookingIDProperty, .GetInt32(22))
          LoadProperty(ChecklTravelTimelineRuleProperty, .GetBoolean(25))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionSystemArea"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionSystemArea"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionSystemAreaID As SqlParameter = .Parameters.Add("@ProductionSystemAreaID", SqlDbType.Int)
          paramProductionSystemAreaID.Value = GetProperty(ProductionSystemAreaIDProperty)
          If Me.IsNew Then
            paramProductionSystemAreaID.Direction = ParameterDirection.Output
          End If
          'GetProperty(ProductionIDProperty)
          .Parameters.AddWithValue("@ProductionID", NothingDBNull(Me.GetParent.ProductionID))
          .Parameters.AddWithValue("@SystemID", NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaStatusID", GetProperty(ProductionAreaStatusIDProperty))
          .Parameters.AddWithValue("@StatusDate", (New SmartDate(GetProperty(StatusDateProperty))).DBValue)
          .Parameters.AddWithValue("@StatusSetByUserID", GetProperty(StatusSetByUserIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@ProductionSpecRequirementID", NothingDBNull(GetProperty(ProductionSpecRequirementIDProperty)))
          .Parameters.AddWithValue("@NotComplySpecReasons", GetProperty(NotComplySpecReasonsProperty))
          If Not CompareSafe(LoadedProductionAreaStatusID, ProductionAreaStatusID) Then
            StatusSetDate = Now
          End If
          .Parameters.AddWithValue("@StatusSetDate", GetProperty(StatusSetDateProperty))
          .Parameters.AddWithValue("@RoomScheduleID", NothingDBNull(GetProperty(RoomScheduleIDProperty)))
          .Parameters.AddWithValue("@AreaComments", "")
          .Parameters.AddWithValue("@AdHocBookingID", NothingDBNull(Nothing))
          '.Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
          .Parameters.AddWithValue("@MealReimbursement", GetProperty(MealReimbursementProperty))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionSystemAreaIDProperty, paramProductionSystemAreaID.Value)
            Me.ProductionID = Me.GetParent.ProductionID
          End If
          ' update child objects
          If GetProperty(ProductionSpecRequirementEquipmentTypeListProperty) IsNot Nothing Then
            Me.ProductionSpecRequirementEquipmentTypeList.Update()
          End If
          If GetProperty(ProductionSpecRequirementPositionListProperty) IsNot Nothing Then
            Me.ProductionSpecRequirementPositionList.Update()
          End If
          If GetProperty(ProductionOutsourceServiceListProperty) IsNot Nothing Then
            Me.ProductionOutsourceServiceList.Update()
          End If
          If GetProperty(ProductionTimelineListProperty) IsNot Nothing Then
            Me.ProductionTimelineList.Update()
          End If
          If GetProperty(ProductionHumanResourceListProperty) IsNot Nothing Then
            Me.ProductionHumanResourceList.Update()
          End If
          CalculateProductionHR()
          Me.HRProductionScheduleList.Clear()
          If GetProperty(HRProductionScheduleListProperty) IsNot Nothing Then
            Me.HRProductionScheduleList.Update()
          End If
          If GetProperty(ProductionHROBListProperty) IsNot Nothing Then
            Me.ProductionHROBList.Update()
          End If
          'If GetProperty(ProductionHRPlannerListProperty) IsNot Nothing Then
          '  Me.ProductionHRPlannerList.Update()
          'End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ProductionSpecRequirementEquipmentTypeListProperty) IsNot Nothing Then
          Me.ProductionSpecRequirementEquipmentTypeList.Update()
        End If
        If GetProperty(ProductionSpecRequirementPositionListProperty) IsNot Nothing Then
          Me.ProductionSpecRequirementPositionList.Update()
        End If
        If GetProperty(ProductionOutsourceServiceListProperty) IsNot Nothing Then
          Me.ProductionOutsourceServiceList.Update()
        End If
        If GetProperty(ProductionTimelineListProperty) IsNot Nothing Then
          Me.ProductionTimelineList.Update()
        End If
        If GetProperty(ProductionHumanResourceListProperty) IsNot Nothing Then
          Me.ProductionHumanResourceList.Update()
        End If
        CalculateProductionHR()
        Me.HRProductionScheduleList.Clear()
        If GetProperty(HRProductionScheduleListProperty) IsNot Nothing Then
          Me.HRProductionScheduleList.Update()
        End If
        If GetProperty(ProductionHROBListProperty) IsNot Nothing Then
          Me.ProductionHROBList.Update()
        End If
        'If GetProperty(ProductionHRPlannerListProperty) IsNot Nothing Then
        '  Me.ProductionHRPlannerList.Update()
        'End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionSystemArea"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace