﻿' Generated 04 Jun 2014 16:57 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Productions.Areas

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports System.Linq
Imports OBLib.Productions.Old

Namespace Productions.Vehicles

  <Serializable()> _
  Public Class ProductionVehicle
    Inherits OBBusinessBase(Of ProductionVehicle)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVehicleID, "ID", 0)
    Dim e As Object

    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionVehicleID() As Integer
      Get
        Return GetProperty(ProductionVehicleIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The parent Production for this Vehicle"),
    Required(ErrorMessage:="Production required")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="The vehicle that is assigned to the production"),
    Required(ErrorMessage:="Vehicle required")>
    Public Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared QuoteRequestedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteRequestedDate, "Quote Requested Date")
    ''' <summary>
    ''' Gets and sets the Quote Requested Date value
    ''' </summary>
    <Display(Name:="Quote Requested Date", Description:="The date that the quote was requested from the supplier")>
    Public Property QuoteRequestedDate As DateTime?
      Get
        Return GetProperty(QuoteRequestedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(QuoteRequestedDateProperty, Value)
      End Set
    End Property

    Public Shared QuoteReceivedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteReceivedDate, "Quote Received Date")
    ''' <summary>
    ''' Gets and sets the Quote Received Date value
    ''' </summary>
    <Display(Name:="Quote Received Date", Description:="The date that the quote was received from the supplier")>
    Public Property QuoteReceivedDate As DateTime?
      Get
        Return GetProperty(QuoteReceivedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(QuoteReceivedDateProperty, Value)
      End Set
    End Property

    Public Shared QuotedAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.QuotedAmount, "Quoted Amount", CDec(0))
    ''' <summary>
    ''' Gets and sets the Quoted Amount value
    ''' </summary>
    <Display(Name:="Quoted Amount", Description:="The quoted amount for the outsource service from the supplier"),
    Required(ErrorMessage:="Quoted Amount required"),
    Singular.DataAnnotations.NumberField("R #,##0.00;R (#,##0.00)")>
    Public Property QuotedAmount() As Decimal
      Get
        Return GetProperty(QuotedAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(QuotedAmountProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name", "")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="Name of the vehicle")>
    Public Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
      Set(value As String)
        SetProperty(VehicleNameProperty, value)
      End Set
    End Property

    Public Shared VehicleDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleDescription, "Vehicle Name", "")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Vehicle Description", Description:="Description for the vehicle")>
    Public Property VehicleDescription() As String
      Get
        Return GetProperty(VehicleDescriptionProperty)
      End Get
      Set(value As String)
        SetProperty(VehicleDescriptionProperty, value)
      End Set
    End Property

    Public Shared VehicleTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleTypeID, "Vehicle Type")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:="The type of vehicle")>
    Public Property VehicleTypeID() As Integer
      Get
        Return GetProperty(VehicleTypeIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(VehicleTypeIDProperty, value)
      End Set
    End Property

    Public Shared VehicleTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleType, "Vehicle Type")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:="The type of vehicle")>
    Public Property VehicleType() As String
      Get
        Return GetProperty(VehicleTypeProperty)
      End Get
      Set(value As String)
        SetProperty(VehicleTypeProperty, value)
      End Set
    End Property

    Public Shared OBVanProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBVan, "Vehicle Type")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="OB Van", Description:="The type of vehicle")>
    Public Property OBVan() As Boolean
      Get
        Return GetProperty(OBVanProperty)
      End Get
      Set(value As Boolean)
        SetProperty(OBVanProperty, value)
      End Set
    End Property

    Public Shared JibProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Jib, "Vehicle Type")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Jib", Description:="The type of vehicle")>
    Public Property Jib() As Boolean
      Get
        Return GetProperty(JibProperty)
      End Get
      Set(value As Boolean)
        SetProperty(JibProperty, value)
      End Set
    End Property

    Public Shared SuppliedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Supplied, "Supplied?", False)
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Supplied?")>
    Public Property Supplied() As Boolean
      Get
        Return GetProperty(SuppliedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(SuppliedProperty, value)
      End Set
    End Property

    Public Shared ClashProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Clash, "Clash", "")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Clash")>
    Public Property Clash() As String
      Get
        Return GetProperty(ClashProperty)
      End Get
      Set(value As String)
        SetProperty(ClashProperty, value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "ProductionVehicleBO.ProductionVehicleToString(self)")

#End Region

#Region " Methods "

    Public Function GetParent() As OBLib.Productions.Old.Production

      Return CType(CType(Me.Parent, ProductionVehicleList).Parent, OBLib.Productions.Old.Production)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVehicleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Vehicle")
        Else
          Return String.Format("Blank {0}", "Production Vehicle")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Public Class VehicleMinMax
      Public Property VehicleID As Integer?
      Public Property StartDateTime As DateTime?
      Public Property EndDateTime As DateTime?
      Public Sub New(VehicleID As Integer?, StartDateTime As DateTime?, EndDateTime As DateTime?)
        Me.VehicleID = VehicleID
        Me.StartDateTime = StartDateTime
        Me.EndDateTime = EndDateTime
      End Sub
    End Class

    Public Class HRSched
      Public Property HumanResourceID As Integer?
      Public Property StartDateTime As DateTime?
      Public Property EndDateTime As DateTime?
      Public Sub New(HumanResourceID As Integer?, StartDateTime As DateTime?, EndDateTime As DateTime?)
        Me.HumanResourceID = HumanResourceID
        Me.StartDateTime = StartDateTime
        Me.EndDateTime = EndDateTime
      End Sub
    End Class

    Public Sub CheckClash()

      Dim msg As String = ""
      'get the start and end dates for the proposed travel to and from this production
      Dim TravelToStartDate As DateTime? = Nothing
      Dim TravelToEndDate As DateTime? = Nothing
      Dim TravelFromStartDate As DateTime? = Nothing
      Dim TravelFromEndDate As DateTime? = Nothing
      Dim DriverTimes As List(Of VehicleMinMax) = Nothing
      Dim Sched As List(Of HRSched) = Nothing

      If Me.GetParent.ProductionSystemAreaList(0).ProductionTimelineList.Count = 0 Then
        TravelToStartDate = Me.GetParent.PlayStartDateTime
        TravelToEndDate = Me.GetParent.PlayEndDateTime
        TravelFromStartDate = Me.GetParent.PlayStartDateTime
        TravelFromEndDate = Me.GetParent.PlayEndDateTime

      ElseIf Me.GetParent.ProductionSystemAreaList(0).ProductionTimelineList.Count = 1 Then
        Dim TL As ProductionTimeline = Me.GetParent.ProductionSystemAreaList(0).ProductionTimelineList(0)
        TravelToStartDate = TL.StartDateTime
        TravelToEndDate = TL.EndDateTime
        TravelFromStartDate = TL.StartDateTime
        TravelFromEndDate = TL.EndDateTime
      Else

        Me.GetParent.GetMinMaxTimesForVehicle(Me.VehicleID, CommonData.Enums.ProductionTimelineType.VehiclePreTravel, TravelToStartDate, TravelToEndDate)
        Me.GetParent.GetMinMaxTimesForVehicle(Me.VehicleID, CommonData.Enums.ProductionTimelineType.VehiclePostTravel, TravelFromStartDate, TravelFromEndDate)

        'if travel has not been planned then we need to get the first and last item's times in the time line
        'need to exclude rig/ops/driver travel timeline items as these can happen concurrently with the vehicle pre/post travel
        If Not TravelToStartDate.HasValue OrElse Not TravelFromEndDate.HasValue Then

          Dim list As New List(Of Integer?) From {CommonData.Enums.ProductionTimelineType.RigPreTravel,
                                                                                 CommonData.Enums.ProductionTimelineType.RigPostTravel,
                                                                                 CommonData.Enums.ProductionTimelineType.OpsPreTravel,
                                                                                 CommonData.Enums.ProductionTimelineType.OpsPostTravel,
                                                                                 CommonData.Enums.ProductionTimelineType.DriverPreTravel,
                                                                                 CommonData.Enums.ProductionTimelineType.DriverPostTravel}
          'The pre/post travel for rig/ops/drivers does not affect the vehicle availability

          'if vehicle is not the OB van then the vehicle does not need to be at the venue for rig/meals
          If CommonData.Lists.ROVehicleList.GetItem(Me.VehicleID).VehicleTypeID <> CommonData.Enums.VehicleType.OBVan Then
            list.Add(CommonData.Enums.ProductionTimelineType.Rig)
            list.Add(CommonData.Enums.ProductionTimelineType.Meals)
            list.Add(CommonData.Enums.ProductionTimelineType.MealsOps)
            list.Add(CommonData.Enums.ProductionTimelineType.MealsRig)
            list.Add(CommonData.Enums.ProductionTimelineType.DayAway)
          End If

          Sched = (From arsch As Schedules.HRProductionSchedule In Me.GetParent.ProductionSystemAreaList(0).HRProductionScheduleList
                   From csd As Schedules.HRProductionScheduleDetail In arsch.HRProductionScheduleDetailList
                   Where csd.ExcludeInd = False
                   Select New HRSched(csd.HumanResourceID, csd.StartDateTime, csd.EndDateTime)).ToList

          DriverTimes = (From phr As Areas.ProductionHumanResource In Me.GetParent.ProductionSystemAreaList(0).ProductionHumanResourceList
                         Join sch As HRSched In Sched On sch.HumanResourceID Equals phr.HumanResourceID
                          Where (Singular.Misc.CompareSafe(phr.VehicleID, Me.VehicleID))
                          Group By phr.VehicleID Into Group
                          Select New VehicleMinMax(VehicleID, Group.Min(Function(d) d.sch.StartDateTime), Group.Max(Function(d) d.sch.EndDateTime))).ToList


          If DriverTimes.Count > 0 Then
            TravelToStartDate = DriverTimes(0).StartDateTime
            TravelFromEndDate = DriverTimes(0).EndDateTime
          End If

          If Not TravelToStartDate.HasValue Then
            For Each pt As ProductionTimeline In Me.GetParent.ProductionSystemAreaList(0).ProductionTimelineList
              If pt.IsValid Then
                If list.Contains(pt.ProductionTimelineTypeID) Then Continue For
                If pt.IsVehicleTravel AndAlso (Singular.Misc.IsNullNothing(pt.VehicleID) OrElse pt.VehicleID <> Me.VehicleID) Then Continue For
                If Not CommonData.Lists.ROProductionTimelineTypeList.GetItem(pt.ProductionTimelineTypeID).FreeInd Then
                  If pt.StartDateTime.HasValue AndAlso (Not TravelToStartDate.HasValue OrElse CDate(pt.StartDateTime) < TravelToStartDate) Then
                    TravelToStartDate = pt.StartDateTime
                  End If
                  If pt.EndDateTime.HasValue AndAlso (Not TravelFromEndDate.HasValue OrElse CDate(pt.EndDateTime) > TravelFromEndDate) Then
                    TravelFromEndDate = pt.EndDateTime
                  End If
                End If
              End If
            Next
          End If
          If Not TravelToStartDate.HasValue Then
            TravelToStartDate = Me.GetParent.ProductionSystemAreaList(0).ProductionTimelineList.GetMinStartDate(list)
          End If
          If Not TravelFromEndDate.HasValue Then
            TravelFromEndDate = Me.GetParent.ProductionSystemAreaList(0).ProductionTimelineList.GetMaxEndDate(list)
          End If
        End If

      End If

      If TravelToStartDate Is Nothing And TravelFromEndDate Is Nothing Then
        'If Me.GetParent.ProductionSystemAreaList(0).ProductionTimelineList.Count = 0 Then
        TravelToStartDate = Now
        TravelFromEndDate = Now
        'End If
      End If

      If TravelToStartDate Is Nothing And TravelFromEndDate Is Nothing Then
        'If Me.GetParent.ProductionSystemAreaList(0).ProductionTimelineList.Count = 0 Then
        TravelToStartDate = Now
        TravelFromEndDate = Now
        'End If
      End If

      OBLib.Helpers.RuleHelper.VehicleRules.UpdateClash(Me, TravelToStartDate, TravelFromEndDate)

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(ClashProperty,
                 Function(d) d.Clash <> "",
                 Function(d) d.Clash)
        .AffectedProperties.Add(VehicleIDProperty)
        .AffectedProperties.Add(VehicleNameProperty)
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionVehicle() method.

    End Sub

    Public Shared Function NewProductionVehicle() As ProductionVehicle

      Return DataPortal.CreateChild(Of ProductionVehicle)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionVehicle(dr As SafeDataReader) As ProductionVehicle

      Dim p As New ProductionVehicle()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionVehicleIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(QuoteRequestedDateProperty, .GetValue(3))
          LoadProperty(QuoteReceivedDateProperty, .GetValue(4))
          LoadProperty(QuotedAmountProperty, .GetDecimal(5))
          LoadProperty(CreatedByProperty, .GetInt32(6))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ModifiedByProperty, .GetInt32(8))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(VehicleNameProperty, .GetString(10))
          LoadProperty(VehicleDescriptionProperty, .GetString(11))
          LoadProperty(VehicleTypeIDProperty, .GetInt32(12))
          LoadProperty(VehicleTypeProperty, .GetString(13))
          LoadProperty(OBVanProperty, .GetBoolean(14))
          LoadProperty(JibProperty, .GetBoolean(15))
          LoadProperty(SuppliedProperty, .GetBoolean(16))
          LoadProperty(ClashProperty, .GetString(17))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionVehicle"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionVehicle"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionVehicleID As SqlParameter = .Parameters.Add("@ProductionVehicleID", SqlDbType.Int)
          paramProductionVehicleID.Value = GetProperty(ProductionVehicleIDProperty)
          If Me.IsNew Then
            paramProductionVehicleID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionID", Me.GetParent.ProductionID)
          .Parameters.AddWithValue("@VehicleID", GetProperty(VehicleIDProperty))
          .Parameters.AddWithValue("@QuoteRequestedDate", (New SmartDate(GetProperty(QuoteRequestedDateProperty))).DBValue)
          .Parameters.AddWithValue("@QuoteReceivedDate", (New SmartDate(GetProperty(QuoteReceivedDateProperty))).DBValue)
          .Parameters.AddWithValue("@QuotedAmount", GetProperty(QuotedAmountProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
          .Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionVehicleIDProperty, paramProductionVehicleID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionVehicle"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionVehicleID", GetProperty(ProductionVehicleIDProperty))
        cm.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
        cm.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
        cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
        cm.Parameters.AddWithValue("@ProductionID", GetParent.ProductionID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace