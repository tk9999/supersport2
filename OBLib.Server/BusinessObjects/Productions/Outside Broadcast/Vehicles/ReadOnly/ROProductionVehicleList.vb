﻿' Generated 27 Jun 2014 10:26 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROProductionVehicleList
    Inherits SingularReadOnlyListBase(Of ROProductionVehicleList, ROProductionVehicle)

#Region " Business Methods "

    Public Function GetItem(ProductionVehicleID As Integer) As ROProductionVehicle

      For Each child As ROProductionVehicle In Me
        If child.ProductionVehicleID = ProductionVehicleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Vehicles"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Integer? = Nothing

      Public Sub New(ProductionID As Integer?)
        Me.ProductionID = ProductionID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionVehicleList() As ROProductionVehicleList

      Return New ROProductionVehicleList()

    End Function

    Public Shared Sub BeginGetROProductionVehicleList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionVehicleList)))

      Dim dp As New DataPortal(Of ROProductionVehicleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionVehicleList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionVehicleList)))

      Dim dp As New DataPortal(Of ROProductionVehicleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionVehicleList() As ROProductionVehicleList

      Return DataPortal.Fetch(Of ROProductionVehicleList)(New Criteria())

    End Function

    Public Shared Function GetROProductionVehicleList(ProductionID As Integer?) As ROProductionVehicleList

      Return DataPortal.Fetch(Of ROProductionVehicleList)(New Criteria(ProductionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionVehicle.GetROProductionVehicle(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionVehicleList"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace