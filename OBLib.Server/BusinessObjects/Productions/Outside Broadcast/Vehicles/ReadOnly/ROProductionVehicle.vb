﻿' Generated 27 Jun 2014 10:26 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Vehicles.ReadOnly

  <Serializable()> _
  Public Class ROProductionVehicle
    Inherits SingularReadOnlyBase(Of ROProductionVehicle)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionVehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVehicleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionVehicleID() As Integer
      Get
        Return GetProperty(ProductionVehicleIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The parent Production for this Vehicle")>
    Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="The vehicle that is assigned to the production")>
    Public ReadOnly Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared QuoteRequestedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteRequestedDate, "Quote Requested Date")
    ''' <summary>
    ''' Gets the Quote Requested Date value
    ''' </summary>
    <Display(Name:="Quote Requested Date", Description:="The date that the quote was requested from the supplier")>
    Public ReadOnly Property QuoteRequestedDate As DateTime?
      Get
        Return GetProperty(QuoteRequestedDateProperty)
      End Get
    End Property

    Public Shared QuoteReceivedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteReceivedDate, "Quote Received Date")
    ''' <summary>
    ''' Gets the Quote Received Date value
    ''' </summary>
    <Display(Name:="Quote Received Date", Description:="The date that the quote was received from the supplier")>
    Public ReadOnly Property QuoteReceivedDate As DateTime?
      Get
        Return GetProperty(QuoteReceivedDateProperty)
      End Get
    End Property

    Public Shared QuotedAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.QuotedAmount, "Quoted Amount", 0)
    ''' <summary>
    ''' Gets the Quoted Amount value
    ''' </summary>
    <Display(Name:="Quoted Amount", Description:="The quoted amount for the outsource service from the supplier")>
    Public ReadOnly Property QuotedAmount() As Decimal
      Get
        Return GetProperty(QuotedAmountProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name", "")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="Name of the vehicle")>
    Public ReadOnly Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
    End Property

    Public Shared VehicleDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleDescription, "Vehicle Name", "")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Vehicle Description", Description:="Description for the vehicle")>
    Public ReadOnly Property VehicleDescription() As String
      Get
        Return GetProperty(VehicleDescriptionProperty)
      End Get
    End Property

    Public Shared VehicleTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleTypeID, "Vehicle Type")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:="The type of vehicle")>
    Public ReadOnly Property VehicleTypeID() As Integer
      Get
        Return GetProperty(VehicleTypeIDProperty)
      End Get
    End Property

    Public Shared VehicleTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleType, "Vehicle Type")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:="The type of vehicle")>
    Public ReadOnly Property VehicleType() As String
      Get
        Return GetProperty(VehicleTypeProperty)
      End Get
    End Property

    Public Shared OBVanProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBVan, "Vehicle Type")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="OB Van", Description:="The type of vehicle")>
    Public ReadOnly Property OBVan() As Boolean
      Get
        Return GetProperty(OBVanProperty)
      End Get
    End Property

    Public Shared JibProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Jib, "Vehicle Type")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Jib", Description:="The type of vehicle")>
    Public ReadOnly Property Jib() As Boolean
      Get
        Return GetProperty(JibProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionVehicleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionVehicle(dr As SafeDataReader) As ROProductionVehicle

      Dim r As New ROProductionVehicle()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionVehicleIDProperty, .GetInt32(0))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(QuoteRequestedDateProperty, .GetValue(3))
        LoadProperty(QuoteReceivedDateProperty, .GetValue(4))
        LoadProperty(QuotedAmountProperty, .GetDecimal(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(VehicleNameProperty, .GetString(10))
        LoadProperty(VehicleDescriptionProperty, .GetString(11))
        LoadProperty(VehicleTypeIDProperty, .GetInt32(12))
        LoadProperty(VehicleTypeProperty, .GetString(13))
        LoadProperty(OBVanProperty, .GetBoolean(14))
        LoadProperty(JibProperty, .GetBoolean(15))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace