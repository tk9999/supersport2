﻿' Generated 04 Jun 2014 16:57 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Old

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Vehicles

  <Serializable()> _
  Public Class ProductionVehicleList
    Inherits OBBusinessListBase(Of ProductionVehicleList, ProductionVehicle)

#Region " Business Methods "

    Public Function GetItem(ProductionVehicleID As Integer) As ProductionVehicle

      For Each child As ProductionVehicle In Me
        If child.ProductionVehicleID = ProductionVehicleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Vehicles"

    End Function

    Public Function GetItemByVehicleID(VehicleID As Integer) As ProductionVehicle

      For Each child As ProductionVehicle In Me
        If child.VehicleID = VehicleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Sub AddNewVehicle(Production As OBLib.Productions.Old.Production, VehicleID As Integer)
      Dim ROVehicle As Maintenance.Vehicles.ReadOnly.ROVehicleOld = OBLib.CommonData.Lists.ROVehicleList.GetItem(VehicleID)
      Dim NewVehicle As ProductionVehicle = Me.AddNew()
      NewVehicle.ProductionID = Production.ProductionID
      NewVehicle.VehicleID = ROVehicle.VehicleID
      NewVehicle.VehicleName = ROVehicle.VehicleName
      NewVehicle.VehicleDescription = ROVehicle.VehicleDescription
      NewVehicle.OBVan = ROVehicle.OBVan
      NewVehicle.Jib = ROVehicle.Jib
      NewVehicle.VehicleType = ROVehicle.VehicleType
      NewVehicle.VehicleTypeID = ROVehicle.VehicleTypeID
      NewVehicle.Supplied = ROVehicle.Supplied
      NewVehicle.CheckClash()
    End Sub

    'Public Sub RemoveVehicle(VehicleID As Integer)
    '  Dim ProductionVehicle As ProductionVehicle = Me.Where(Function(d) d.VehicleID = VehicleID).FirstOrDefault
    '  If ProductionVehicle IsNot Nothing Then
    '    Dim Production As Production = ProductionVehicle.GetParent()
    '    If Production IsNot Nothing Then
    '      Production.ProductionSystemAreaList(0).ProductionHumanResourceList.RemoveHumanResourcesForVehicle(VehicleID)
    '    End If
    '    Me.Remove(ProductionVehicle)
    '  End If
    'End Sub

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public ProductionID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(ProductionID As Integer?)

        Me.ProductionID = ProductionID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionVehicleList() As ProductionVehicleList

      Return New ProductionVehicleList()

    End Function

    Public Shared Sub BeginGetProductionVehicleList(CallBack As EventHandler(Of DataPortalResult(Of ProductionVehicleList)))

      Dim dp As New DataPortal(Of ProductionVehicleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionVehicleList() As ProductionVehicleList

      Return DataPortal.Fetch(Of ProductionVehicleList)(New Criteria())

    End Function

    Public Shared Function GetProductionVehicleList(ProductionID As Integer?) As ProductionVehicleList

      Return DataPortal.Fetch(Of ProductionVehicleList)(New Criteria(ProductionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionVehicle.GetProductionVehicle(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionVehicleList"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionVehicle In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionVehicle In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace