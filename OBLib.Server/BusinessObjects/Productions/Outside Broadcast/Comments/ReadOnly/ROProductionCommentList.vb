﻿' Generated 02 Jun 2014 12:32 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.ReadOnly

  <Serializable()> _
  Public Class ROProductionCommentList
    Inherits OBReadOnlyListBase(Of ROProductionCommentList, ROProductionComment)

#Region " Business Methods "

    Public Function GetItem(ProductionCommentID As Integer) As ROProductionComment

      For Each child As ROProductionComment In Me
        If child.ProductionCommentID = ProductionCommentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Comments"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=False)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionCommentHeader As String = ""

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionCommentList() As ROProductionCommentList

      Return New ROProductionCommentList()

    End Function

    Public Shared Sub BeginGetROProductionCommentList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionCommentList)))

      Dim dp As New DataPortal(Of ROProductionCommentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionCommentList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionCommentList)))

      Dim dp As New DataPortal(Of ROProductionCommentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionCommentList() As ROProductionCommentList

      Return DataPortal.Fetch(Of ROProductionCommentList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionComment.GetROProductionComment(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionCommentList"
            cm.Parameters.AddWithValue("@ProductionCommentHeader", Singular.Strings.MakeEmptyDBNull(crit.ProductionCommentHeader))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace