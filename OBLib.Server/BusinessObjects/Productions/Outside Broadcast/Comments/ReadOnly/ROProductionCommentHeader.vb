﻿' Generated 20 May 2014 10:01 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.ReadOnly

  <Serializable()> _
  Public Class ROProductionCommentHeader
    Inherits OBReadOnlyBase(Of ROProductionCommentHeader)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionCommentHeaderIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionCommentHeaderID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionCommentHeaderID() As Integer
      Get
        Return GetProperty(ProductionCommentHeaderIDProperty)
      End Get
    End Property

    Public Shared ProductionCommentHeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionCommentHeader, "Production Comment Header", "")
    ''' <summary>
    ''' Gets the Production Comment Header value
    ''' </summary>
    <Display(Name:="Production Comment Header", Description:="Production comment header name")>
    Public ReadOnly Property ProductionCommentHeader() As String
      Get
        Return GetProperty(ProductionCommentHeaderProperty)
      End Get
    End Property

    Public Shared ProductionCommentReportSectionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionCommentReportSectionID, "Production Comment Report Section")
    ''' <summary>
    ''' Gets the Production Comment Header value
    ''' </summary>
    <Display(Name:="Production Comment Report Section", Description:="Production Comment Report Section")>
    Public ReadOnly Property ProductionCommentReportSectionID() As Integer
      Get
        Return GetProperty(ProductionCommentReportSectionIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionCommentHeaderIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionCommentHeader

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionCommentHeader(dr As SafeDataReader) As ROProductionCommentHeader

      Dim r As New ROProductionCommentHeader()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionCommentHeaderProperty, .GetString(0))
        LoadProperty(ProductionCommentReportSectionIDProperty, .GetInt32(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace