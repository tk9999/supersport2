﻿' Generated 02 Jun 2014 12:32 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.ReadOnly

  <Serializable()> _
  Public Class ROProductionComment
    Inherits OBReadOnlyBase(Of ROProductionComment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionCommentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionCommentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionCommentID() As Integer
      Get
        Return GetProperty(ProductionCommentIDProperty)
      End Get
    End Property

    Public Shared ProductionCommentHeaderIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionCommentHeaderID, "Production Comment Header", Nothing)
    ''' <summary>
    ''' Gets the Production Comment Header value
    ''' </summary>
    <Display(Name:="Production Comment Header", Description:="Link to the Production comment header")>
    Public ReadOnly Property ProductionCommentHeaderID() As Integer?
      Get
        Return GetProperty(ProductionCommentHeaderIDProperty)
      End Get
    End Property

    Public Shared ProductionCommentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionComment, "Production Comment", "")
    ''' <summary>
    ''' Gets the Production Comment value
    ''' </summary>
    <Display(Name:="Production Comment", Description:="Comment made by the user")>
    Public ReadOnly Property ProductionComment() As String
      Get
        Return GetProperty(ProductionCommentProperty)
      End Get
    End Property

    'Public Shared ProductionCommentValueMemberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionCommentValueMember, "Production Comment Value Member", "")
    ' ''' <summary>
    ' ''' Gets the Production Comment value
    ' ''' </summary>
    <Display(Name:="Production Comment", Description:="Comment made by the user")>
    Public ReadOnly Property ProductionCommentValueMember() As String
      Get
        Return GetProperty(ProductionCommentProperty)
      End Get
    End Property

    Public Shared AdditionalCommentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdditionalComment, "Additional Comment", "")
    ''' <summary>
    ''' Gets the Additional Comment value
    ''' </summary>
    <Display(Name:="Additional Comment", Description:="Any additional comments that need to be made")>
    Public ReadOnly Property AdditionalComment() As String
      Get
        Return GetProperty(AdditionalCommentProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionCommentReportSectionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionCommentReportSectionID, "Production Comment Report Section")
    ''' <summary>
    ''' Gets the Production Comment Header value
    ''' </summary>
    <Display(Name:="Production Comment Report Section", Description:="Production Comment Report Section")>
    Public ReadOnly Property ProductionCommentReportSectionID() As Integer
      Get
        Return GetProperty(ProductionCommentReportSectionIDProperty)
      End Get
    End Property

    Public Shared ProductionCommentReportSectionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionCommentReportSection, "Production Comment Report Section")
    ''' <summary>
    ''' Gets the Production Comment Header value
    ''' </summary>
    <Display(Name:="Production Comment Report Section", Description:="Production Comment Report Section")>
    Public ReadOnly Property ProductionCommentReportSection() As String
      Get
        Return GetProperty(ProductionCommentReportSectionIDProperty)
      End Get
    End Property

    Public Shared ProductionCommentHeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionCommentHeader, "Production Comment Header")
    ''' <summary>
    ''' Gets the Production Comment Header value
    ''' </summary>
    <Display(Name:="Production Comment Header", Description:="Link to the Production comment header")>
    Public ReadOnly Property ProductionCommentHeader() As String
      Get
        Return GetProperty(ProductionCommentHeaderProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionCommentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionComment

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionComment(dr As SafeDataReader) As ROProductionComment

      Dim r As New ROProductionComment()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionCommentReportSectionIDProperty, .GetInt32(0))
        LoadProperty(ProductionCommentReportSectionProperty, .GetString(1))
        LoadProperty(ProductionCommentHeaderProperty, .GetString(2))
        LoadProperty(ProductionCommentProperty, .GetString(3))
        LoadProperty(AdditionalCommentProperty, .GetString(4))

      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace