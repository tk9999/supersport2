﻿' Generated 02 Jun 2014 08:21 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.ReadOnly

  <Serializable()> _
  Public Class ROProductionCommentReportSectionList
    Inherits OBReadOnlyListBase(Of ROProductionCommentReportSectionList, ROProductionCommentReportSection)

#Region " Business Methods "

    Public Function GetItem(ProductionCommentReportSectionID As Integer) As ROProductionCommentReportSection

      For Each child As ROProductionCommentReportSection In Me
        If child.ProductionCommentReportSectionID = ProductionCommentReportSectionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Comment Report Sections"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionCommentReportSectionList() As ROProductionCommentReportSectionList

      Return New ROProductionCommentReportSectionList()

    End Function

    Public Shared Sub BeginGetROProductionCommentReportSectionList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionCommentReportSectionList)))

      Dim dp As New DataPortal(Of ROProductionCommentReportSectionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionCommentReportSectionList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionCommentReportSectionList)))

      Dim dp As New DataPortal(Of ROProductionCommentReportSectionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionCommentReportSectionList() As ROProductionCommentReportSectionList

      Return DataPortal.Fetch(Of ROProductionCommentReportSectionList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionCommentReportSection.GetROProductionCommentReportSection(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionCommentReportSectionList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace