﻿' Generated 20 May 2014 08:06 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions

  <Serializable()> _
  Public Class ProductionCommentHeader
    Inherits OBBusinessBase(Of ProductionCommentHeader)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared ProductionCommentHeaderIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionCommentHeaderID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionCommentHeaderID() As Integer
      Get
        Return GetProperty(ProductionCommentHeaderIDProperty)
      End Get
    End Property

    Public Shared ProductionCommentSectionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionCommentSectionID, "Production Comment Section", Nothing)
    ''' <summary>
    ''' Gets the Production Comment Section value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionCommentSectionID() As Integer?
      Get
        Return GetProperty(ProductionCommentSectionIDProperty)
      End Get
    End Property

    Public Shared ProductionCommentHeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionCommentHeader, "Production Comment Header", "")
    ''' <summary>
    ''' Gets and sets the Production Comment Header value
    ''' </summary>
    <Display(Name:="Production Comment Header", Description:="Production comment header name"),
    Required(ErrorMessage:="Production Comment Header required"),
    StringLength(100, ErrorMessage:="Production Comment Header cannot be more than 100 characters"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROProductionCommentHeaderList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, DisplayMember:="ProductionCommentHeader", ValueMember:="ProductionCommentHeader", FilterMethodName:="CommentHeadFilter", Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel)>
    Public Property ProductionCommentHeader() As String
      Get
        Return GetProperty(ProductionCommentHeaderProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionCommentHeaderProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionCommentListProperty As PropertyInfo(Of ProductionCommentList) = RegisterProperty(Of ProductionCommentList)(Function(c) c.ProductionCommentList, "Production Comment List")

    Public ReadOnly Property ProductionCommentList() As ProductionCommentList
      Get
        If GetProperty(ProductionCommentListProperty) Is Nothing Then
          LoadProperty(ProductionCommentListProperty, Productions.ProductionCommentList.NewProductionCommentList())
        End If
        Return GetProperty(ProductionCommentListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionCommentSection

      Return CType(CType(Me.Parent, ProductionCommentHeaderList).Parent, ProductionCommentSection)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionCommentHeaderIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionCommentHeader.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Comment Header")
        Else
          Return String.Format("Blank {0}", "Production Comment Header")
        End If
      Else
        Return Me.ProductionCommentHeader
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ProductionComments"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionCommentHeader() method.

    End Sub

    Public Shared Function NewProductionCommentHeader() As ProductionCommentHeader

      Return DataPortal.CreateChild(Of ProductionCommentHeader)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionCommentHeader(dr As SafeDataReader) As ProductionCommentHeader

      Dim p As New ProductionCommentHeader()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionCommentHeaderIDProperty, .GetInt32(0))
          LoadProperty(ProductionCommentSectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionCommentHeaderProperty, .GetString(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionCommentHeader"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionCommentHeader"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionCommentHeaderID As SqlParameter = .Parameters.Add("@ProductionCommentHeaderID", SqlDbType.Int)
          paramProductionCommentHeaderID.Value = GetProperty(ProductionCommentHeaderIDProperty)
          If Me.IsNew Then
            paramProductionCommentHeaderID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionCommentSectionID", Me.GetParent().ProductionCommentSectionID)
          .Parameters.AddWithValue("@ProductionCommentHeader", GetProperty(ProductionCommentHeaderProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionCommentHeaderIDProperty, paramProductionCommentHeaderID.Value)
          End If
          ' update child objects
          If GetProperty(ProductionCommentListProperty) IsNot Nothing Then
            Me.ProductionCommentList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ProductionCommentListProperty) IsNot Nothing Then
          Me.ProductionCommentList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionCommentHeader"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionCommentHeaderID", GetProperty(ProductionCommentHeaderIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace