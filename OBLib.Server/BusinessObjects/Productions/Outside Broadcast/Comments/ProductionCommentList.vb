﻿' Generated 20 May 2014 08:06 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions

  <Serializable()> _
  Public Class ProductionCommentList
    Inherits OBBusinessListBase(Of ProductionCommentList, ProductionComment)

#Region " Business Methods "

    Public Function GetItem(Comment As String) As ProductionComment

      For Each child As ProductionComment In Me
        If child.ProductionComment = Comment Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItem(ProductionCommentID As Integer) As ProductionComment

      For Each child As ProductionComment In Me
        If child.ProductionCommentID = ProductionCommentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Comments"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewProductionCommentList() As ProductionCommentList

      Return New ProductionCommentList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionComment In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionComment In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace