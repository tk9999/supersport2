﻿' Generated 20 May 2014 08:06 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions

  <Serializable()> _
  Public Class ProductionComment
    Inherits OBBusinessBase(Of ProductionComment)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionCommentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionCommentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionCommentID() As Integer
      Get
        Return GetProperty(ProductionCommentIDProperty)
      End Get
    End Property

    Public Shared ProductionCommentHeaderIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionCommentHeaderID, "Production Comment Header", Nothing)
    ''' <summary>
    ''' Gets the Production Comment Header value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionCommentHeaderID() As Integer?
      Get
        Return GetProperty(ProductionCommentHeaderIDProperty)
      End Get
    End Property

    Public Shared ProductionCommentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionComment, "Production Comment", "")
    ''' <summary>
    ''' Gets and sets the Production Comment value
    ''' </summary>
    <Display(Name:="Production Comment", Description:="Comment made by the user"),
    Required(ErrorMessage:="Production Comment required")>
    Public Property ProductionComment() As String
      Get
        Return GetProperty(ProductionCommentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionCommentProperty, Value)
      End Set
    End Property

    Public Shared AdditionalCommentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdditionalComment, "Additional Comment", "")
    ''' <summary>
    ''' Gets and sets the Additional Comment value
    ''' </summary>
    <Display(Name:="Additional Comment", Description:="Any additional comments that need to be made"),
    StringLength(500, ErrorMessage:="Additional Comment cannot be more than 500 characters")>
    Public Property AdditionalComment() As String
      Get
        Return GetProperty(AdditionalCommentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AdditionalCommentProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionCommentHeaderProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionCommentHeader, "Production Comment Header")
    ''' <summary>
    ''' Gets the Production Comment Header value
    ''' </summary>
    <Display(Name:="Production Comment Header", Description:="Production Comment Header")>
    Public Property ProductionCommentHeader() As String
      Get
        Return GetProperty(ProductionCommentHeaderProperty)
      End Get
      Set(value As String)
        SetProperty(ProductionCommentHeaderProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionCommentHeader

      Return CType(CType(Me.Parent, ProductionCommentList).Parent, ProductionCommentHeader)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionCommentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionComment.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Comment")
        Else
          Return String.Format("Blank {0}", "Production Comment")
        End If
      Else
        Return Me.ProductionComment
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionComment() method.

    End Sub

    Public Shared Function NewProductionComment() As ProductionComment

      Return DataPortal.CreateChild(Of ProductionComment)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionComment(dr As SafeDataReader) As ProductionComment

      Dim p As New ProductionComment()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionCommentIDProperty, .GetInt32(0))
          LoadProperty(ProductionCommentHeaderIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionCommentProperty, .GetString(2))
          LoadProperty(AdditionalCommentProperty, .GetString(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(ProductionCommentHeaderProperty, .GetString(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionComment"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionComment"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionCommentID As SqlParameter = .Parameters.Add("@ProductionCommentID", SqlDbType.Int)
          paramProductionCommentID.Value = GetProperty(ProductionCommentIDProperty)
          If Me.IsNew Then
            paramProductionCommentID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionCommentHeaderID", Me.GetParent().ProductionCommentHeaderID)
          .Parameters.AddWithValue("@ProductionComment", GetProperty(ProductionCommentProperty))
          .Parameters.AddWithValue("@AdditionalComment", GetProperty(AdditionalCommentProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionCommentIDProperty, paramProductionCommentID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionComment"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionCommentID", GetProperty(ProductionCommentIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace