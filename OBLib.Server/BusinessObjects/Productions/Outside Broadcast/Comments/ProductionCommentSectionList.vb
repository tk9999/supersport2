﻿' Generated 20 May 2014 08:06 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions

  <Serializable()> _
  Public Class ProductionCommentSectionList
    Inherits OBBusinessListBase(Of ProductionCommentSectionList, ProductionCommentSection)

#Region " Business Methods "

    Public Function GetItem(ProductionCommentSectionID As Integer) As ProductionCommentSection

      For Each child As ProductionCommentSection In Me
        If child.ProductionCommentSectionID = ProductionCommentSectionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Comment Sections"

    End Function

    Public Function GetProductionCommentHeader(ProductionCommentHeaderID As Integer) As ProductionCommentHeader

      Dim obj As ProductionCommentHeader = Nothing
      For Each parent As ProductionCommentSection In Me
        obj = parent.ProductionCommentHeaderList.GetItem(ProductionCommentHeaderID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Sub AddComment(ProductionCommentReportSectionID As Integer, ProductionCommentHeader As String, Comment As String)

      Dim pch = AddComment(ProductionCommentReportSectionID, ProductionCommentHeader)

      Dim pc = pch.ProductionCommentList.GetItem(Comment)
      If pc Is Nothing Then
        pc = pch.ProductionCommentList.AddNew()
        pc.ProductionComment = Comment
        pc.ProductionCommentHeader = ProductionCommentHeader
      End If

    End Sub

    Public Function AddComment(ProductionCommentReportSectionID As Integer, ProductionCommentHeader As String) As ProductionCommentHeader

      Dim ro = CommonData.Lists.ROProductionCommentReportSectionList.GetItem(ProductionCommentReportSectionID)
      Dim pcs As ProductionCommentSection = Me.GetItemByReportSectionID(ProductionCommentReportSectionID)
      If pcs Is Nothing Then
        pcs = Me.AddNew
        pcs.ProductionCommentReportSectionID = ProductionCommentReportSectionID
      End If
      Dim PCH = pcs.ProductionCommentHeaderList.GetItem(ProductionCommentHeader)
      If PCH Is Nothing Then
        PCH = pcs.ProductionCommentHeaderList.AddNew
        PCH.ProductionCommentHeader = ProductionCommentHeader
      End If
      Return PCH

    End Function

    Public Function GetItemByReportSectionID(ProductionCommentReportSectionID As Integer) As ProductionCommentSection

      For Each child As ProductionCommentSection In Me
        If child.ProductionCommentReportSectionID = ProductionCommentReportSectionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public ProductionID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(ProductionID As Integer?)

        Me.ProductionID = ProductionID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionCommentSectionList() As ProductionCommentSectionList

      Return New ProductionCommentSectionList()

    End Function

    Public Shared Sub BeginGetProductionCommentSectionList(CallBack As EventHandler(Of DataPortalResult(Of ProductionCommentSectionList)))

      Dim dp As New DataPortal(Of ProductionCommentSectionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionCommentSectionList() As ProductionCommentSectionList

      Return DataPortal.Fetch(Of ProductionCommentSectionList)(New Criteria())

    End Function

    Public Shared Function GetProductionCommentSectionList(ProductionID As Integer?) As ProductionCommentSectionList

      Return DataPortal.Fetch(Of ProductionCommentSectionList)(New Criteria(ProductionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionCommentSection.GetProductionCommentSection(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ProductionCommentSection = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionCommentSectionID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionCommentHeaderList.RaiseListChangedEvents = False
          parent.ProductionCommentHeaderList.Add(ProductionCommentHeader.GetProductionCommentHeader(sdr))
          parent.ProductionCommentHeaderList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As ProductionCommentHeader = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.ProductionCommentHeaderID <> sdr.GetInt32(1) Then
            parentChild = Me.GetProductionCommentHeader(sdr.GetInt32(1))
          End If
          parentChild.ProductionCommentList.RaiseListChangedEvents = False
          parentChild.ProductionCommentList.Add(ProductionComment.GetProductionComment(sdr))
          parentChild.ProductionCommentList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As ProductionCommentSection In Me
        child.CheckRules()
        For Each ProductionCommentHeader As ProductionCommentHeader In child.ProductionCommentHeaderList
          ProductionCommentHeader.CheckRules()

          For Each ProductionComment As ProductionComment In ProductionCommentHeader.ProductionCommentList
            ProductionComment.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionCommentSectionList"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionCommentSection In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionCommentSection In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace