﻿' Generated 20 May 2014 08:06 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions

  <Serializable()> _
  Public Class ProductionCommentHeaderList
    Inherits OBBusinessListBase(Of ProductionCommentHeaderList, ProductionCommentHeader)

#Region " Business Methods "

    Public Function GetItem(ProductionCommentHeader As String) As ProductionCommentHeader

      For Each child As ProductionCommentHeader In Me
        If child.ProductionCommentHeader = ProductionCommentHeader Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItem(ProductionCommentHeaderID As Integer) As ProductionCommentHeader

      For Each child As ProductionCommentHeader In Me
        If child.ProductionCommentHeaderID = ProductionCommentHeaderID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Comment Headers"

    End Function

    Public Function GetProductionComment(ProductionCommentID As Integer) As ProductionComment

      Dim obj As ProductionComment = Nothing
      For Each parent As ProductionCommentHeader In Me
        obj = parent.ProductionCommentList.GetItem(ProductionCommentID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewProductionCommentHeaderList() As ProductionCommentHeaderList

      Return New ProductionCommentHeaderList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionCommentHeader In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionCommentHeader In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace