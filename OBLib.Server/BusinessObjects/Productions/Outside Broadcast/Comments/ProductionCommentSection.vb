﻿' Generated 20 May 2014 08:06 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Productions.ReadOnly
Imports OBLib.Productions.Old

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions

  <Serializable()> _
  Public Class ProductionCommentSection
    Inherits OBBusinessBase(Of ProductionCommentSection)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

    Public Shared ProductionCommentSectionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionCommentSectionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionCommentSectionID() As Integer
      Get
        Return GetProperty(ProductionCommentSectionIDProperty)
      End Get
    End Property

    Public Shared ProductionCommentReportSectionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionCommentReportSectionID, "Production Comment Report Section", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Comment Report Section value
    ''' </summary>
    <Display(Name:="Production Comment Report Section", Description:="Production Comment Report Section Name"),
    Required(ErrorMessage:="Production Comment Report Section required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROProductionCommentReportSectionList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property ProductionCommentReportSectionID() As Integer?
      Get
        Return GetProperty(ProductionCommentReportSectionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionCommentReportSectionIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="Link to the Production")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionCommentHeaderListProperty As PropertyInfo(Of ProductionCommentHeaderList) = RegisterProperty(Of ProductionCommentHeaderList)(Function(c) c.ProductionCommentHeaderList, "Production Comment Header List")

    Public ReadOnly Property ProductionCommentHeaderList() As ProductionCommentHeaderList
      Get
        If GetProperty(ProductionCommentHeaderListProperty) Is Nothing Then
          LoadProperty(ProductionCommentHeaderListProperty, Productions.ProductionCommentHeaderList.NewProductionCommentHeaderList())
        End If
        Return GetProperty(ProductionCommentHeaderListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Production

      Return CType(CType(Me.Parent, ProductionCommentSectionList).Parent, Production)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionCommentSectionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionCommentReportSectionID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Comment Section")
        Else
          Return String.Format("Blank {0}", "Production Comment Section")
        End If
      Else
        Return Me.ProductionCommentReportSectionID.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ProductionCommentHeaders"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionCommentSection() method.

    End Sub

    Public Shared Function NewProductionCommentSection() As ProductionCommentSection

      Return DataPortal.CreateChild(Of ProductionCommentSection)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionCommentSection(dr As SafeDataReader) As ProductionCommentSection

      Dim p As New ProductionCommentSection()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionCommentSectionIDProperty, .GetInt32(0))
          LoadProperty(ProductionCommentReportSectionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionCommentSection"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionCommentSection"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionCommentSectionID As SqlParameter = .Parameters.Add("@ProductionCommentSectionID", SqlDbType.Int)
          paramProductionCommentSectionID.Value = GetProperty(ProductionCommentSectionIDProperty)
          If Me.IsNew Then
            paramProductionCommentSectionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionCommentReportSectionID", GetProperty(ProductionCommentReportSectionIDProperty))
          .Parameters.AddWithValue("@ProductionID", Me.GetParent.ProductionID)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionCommentSectionIDProperty, paramProductionCommentSectionID.Value)
          End If
          ' update child objects
          If GetProperty(ProductionCommentHeaderListProperty) IsNot Nothing Then
            Me.ProductionCommentHeaderList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ProductionCommentHeaderListProperty) IsNot Nothing Then
          Me.ProductionCommentHeaderList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionCommentSection"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionCommentSectionID", GetProperty(ProductionCommentSectionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace