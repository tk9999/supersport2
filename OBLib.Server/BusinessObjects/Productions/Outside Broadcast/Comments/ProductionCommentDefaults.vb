﻿Public Class ProductionCommentDefaults

  Private Shared mClientDetails As List(Of String)
  Public Shared ReadOnly Property ClientDetails As List(Of String)
    Get
      If mClientDetails Is Nothing Then
        mClientDetails = New List(Of String)
        mClientDetails.AddRange({"Producer", "Director", "VT Director", "P.A.", "Floormanager", "Stats", "SNG/Microwave", "Event Manager"})
      End If
      Return mClientDetails
    End Get
  End Property

  Public Class ProductionDetails

    Public Const OBTelephoneNumbersHeader As String = "OB Telephone Numbers"
    Public Const OBTelephoneNumbersContent As String = "011 607 6895"

    Public Const TapeStockHeader As String = "Tape Stock"
    Public Const TapeStockContent As String = "Producer to supply."

    Public Const DressCodeHeader As String = "Dress Code"
    Public Const DressCodeContent As String = "Normal, Jeans and tennis shoes." & vbCrLf &
                                      "All crew on field to wear accreditation" & vbCrLf &
                                      "All crew in stadium grounds to wear" & vbCrLf &
                                      "accreditation"

  End Class

  Public Class Equipment

    Public Const SpecialArrangementsHeader As String = "Special Arrangements"
    Public Const SpecialArrangementsContent As String = "All crew to wear accredation's all time" & vbCrLf &
      "All Crew  on the field to wear SuperSport bibs" & vbCrLf &
      "All Cameramen on Scaffolding - ensure that camera's are secured from blowing/falling over" & vbCrLf &
      "Any Scaffolding higher than 3 meters, all crew have to wear safety belts"

  End Class

End Class