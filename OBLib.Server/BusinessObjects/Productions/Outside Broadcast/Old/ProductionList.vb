﻿' Generated 29 May 2014 18:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Areas
Imports OBLib.Productions.Specs.Old
Imports OBLib.Productions.Vehicles
Imports OBLib.Productions.Services
Imports OBLib.Productions.Schedules
Imports OBLib.Productions.Correspondence
Imports OBLib.Resources
Imports OBLib.Productions.OB
Imports OBLib.Travel

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Old

  <Serializable()> _
  Public Class ProductionList
    Inherits SingularBusinessListBase(Of ProductionList, Production)

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As Production

      For Each child As Production In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetProductionSystemArea(ProductionSystemAreaID As Integer) As ProductionSystemArea

      Dim obj As ProductionSystemArea = Nothing
      For Each prod As Production In Me
        obj = prod.ProductionSystemAreaList.GetItem(ProductionSystemAreaID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    'Public Function GetAreaSchedule(HumanResourceID As Integer) As AreaSchedule

    '  Dim obj As AreaSchedule = Nothing
    '  For Each prod As Production In Me
    '    For Each pa As ProductionSystemArea In prod.ProductionSystemAreaList
    '      obj = pa.AreaScheduleList.GetItem(HumanResourceID)
    '      If obj IsNot Nothing Then
    '        Return obj
    '      End If
    '    Next
    '  Next
    '  Return Nothing

    'End Function

    Public Function GetHRProductionSchedule(HumanResourceID As Integer) As HRProductionSchedule

      Dim obj As HRProductionSchedule = Nothing
      For Each prod As Production In Me
        For Each pa As ProductionSystemArea In prod.ProductionSystemAreaList
          obj = pa.HRProductionScheduleList.GetItem(HumanResourceID)
          If obj IsNot Nothing Then
            Return obj
          End If
        Next
      Next
      Return Nothing

    End Function

    Public Function GetProductionOutsourceService(ProductionOutsourceServiceID As Integer) As ProductionOutsourceService

      Dim obj As ProductionOutsourceService = Nothing
      For Each prod As Production In Me
        For Each pa As ProductionSystemArea In prod.ProductionSystemAreaList
          obj = pa.ProductionOutsourceServiceList.GetItem(ProductionOutsourceServiceID)
          If obj IsNot Nothing Then
            Return obj
          End If
        Next
      Next
      Return Nothing

    End Function

    Public Function GetProductionCommentSection(ProductionCommentSectionID As Integer) As ProductionCommentSection

      Dim obj As ProductionCommentSection = Nothing
      For Each prod As Production In Me
        'For Each pcs As ProductionCommentSection In prod.ProductionCommentSectionList
        obj = prod.ProductionCommentSectionList.GetItem(ProductionCommentSectionID)
        If obj IsNot Nothing Then
          Return obj
        End If
        'Next
      Next
      Return Nothing

    End Function

    Public Function GetProductionCommentHeader(ProductionCommentHeaderID As Integer) As ProductionCommentHeader

      Dim obj As ProductionCommentHeader = Nothing

      For Each prod As Production In Me
        For Each pcs As ProductionCommentSection In prod.ProductionCommentSectionList
          obj = pcs.ProductionCommentHeaderList.GetItem(ProductionCommentHeaderID)
          If obj IsNot Nothing Then
            Return obj
          End If
        Next
      Next
      'For Each parent As ProductionCommentSection In Me
      '  obj = parent.ProductionCommentHeaderList.GetItem(ProductionCommentHeaderID)
      '  If obj IsNot Nothing Then
      '    Return obj
      '  End If
      'Next
      Return Nothing

    End Function

    Public Function GetProductionAudioConfiguration(ProductionAudioConfigurationID As Integer) As ProductionAudioConfiguration

      Dim obj As ProductionAudioConfiguration = Nothing
      For Each prod As Production In Me
        obj = prod.ProductionAudioConfigurationList.GetItem(ProductionAudioConfigurationID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetProductionHROB(ProductionHRID As Integer) As ProductionHROB

      Dim obj As ProductionHROB = Nothing
      For Each prod As Production In Me
        For Each psa As ProductionSystemArea In prod.ProductionSystemAreaList
          For Each ph As ProductionHROB In psa.ProductionHROBList
            If ph.ProductionHRID = ProductionHRID Then
              Return ph
            End If
          Next
        Next
      Next
      Return Nothing

    End Function

    Public Function GetResourceBookingOB(ResourceBookingID As Integer) As ResourceBookingOB

      Dim obj As ProductionHROB = Nothing
      For Each prod As Production In Me
        For Each psa As ProductionSystemArea In prod.ProductionSystemAreaList
          For Each ph As ProductionHROB In psa.ProductionHROBList
            For Each rb As ResourceBookingOB In ph.ResourceBookingOBList
              If rb.ResourceBookingID = ResourceBookingID Then
                Return rb
              End If
            Next
          Next
        Next
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production List"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing

      Public Sub New(ProductionID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?)
        Me.ProductionID = ProductionID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionList() As ProductionList

      Return New ProductionList()

    End Function

    Public Shared Sub BeginGetProductionList(CallBack As EventHandler(Of DataPortalResult(Of ProductionList)))

      Dim dp As New DataPortal(Of ProductionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionList(ProductionID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?) As ProductionList

      Return DataPortal.Fetch(Of ProductionList)(New Criteria(ProductionID, SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(Production.GetProduction(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As Production = Nothing

      'Vehicle
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionVehicleList.RaiseListChangedEvents = False
          parent.ProductionVehicleList.Add(ProductionVehicle.GetProductionVehicle(sdr))
          parent.ProductionVehicleList.RaiseListChangedEvents = True
        End While
      End If

      'Audio
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionAudioConfigurationList.RaiseListChangedEvents = False
          parent.ProductionAudioConfigurationList.Add(ProductionAudioConfiguration.GetProductionAudioConfiguration(sdr))
          parent.ProductionAudioConfigurationList.RaiseListChangedEvents = True
        End While
      End If

      'Audio Detail
      Dim parentAudio As ProductionAudioConfiguration = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentAudio Is Nothing OrElse parentAudio.ProductionAudioConfigurationID <> sdr.GetInt32(1) Then
            parentAudio = Me.GetProductionAudioConfiguration(sdr.GetInt32(1))
          End If
          parentAudio.ProductionAudioConfigurationDetailList.RaiseListChangedEvents = False
          parentAudio.ProductionAudioConfigurationDetailList.Add(ProductionAudioConfigurationDetail.GetProductionAudioConfigurationDetail(sdr))
          parentAudio.ProductionAudioConfigurationDetailList.RaiseListChangedEvents = True
        End While
      End If

      'Area
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionSystemAreaList.RaiseListChangedEvents = False
          parent.ProductionSystemAreaList.Add(ProductionSystemArea.GetProductionSystemArea(sdr))
          parent.ProductionSystemAreaList.RaiseListChangedEvents = True
        End While
      End If

      'Spec Equipment Type
      Dim parentArea As ProductionSystemArea = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentArea Is Nothing OrElse parentArea.ProductionSystemAreaID <> sdr.GetInt32(12) Then
            parentArea = Me.GetProductionSystemArea(sdr.GetInt32(12))
          End If
          parentArea.ProductionSpecRequirementEquipmentTypeList.RaiseListChangedEvents = False
          parentArea.ProductionSpecRequirementEquipmentTypeList.Add(ProductionSpecRequirementEquipmentType.GetProductionSpecRequirementEquipmentType(sdr))
          parentArea.ProductionSpecRequirementEquipmentTypeList.RaiseListChangedEvents = True
        End While
      End If

      'Spec Position
      If sdr.NextResult() Then
        While sdr.Read
          If parentArea Is Nothing OrElse parentArea.ProductionSystemAreaID <> sdr.GetInt32(13) Then
            parentArea = Me.GetProductionSystemArea(sdr.GetInt32(13))
          End If
          parentArea.ProductionSpecRequirementPositionList.RaiseListChangedEvents = False
          parentArea.ProductionSpecRequirementPositionList.Add(ProductionSpecRequirementPosition.GetProductionSpecRequirementPosition(sdr))
          parentArea.ProductionSpecRequirementPositionList.RaiseListChangedEvents = True
        End While
      End If

      'Production Timeline
      If sdr.NextResult() Then
        While sdr.Read
          If parentArea Is Nothing OrElse parentArea.ProductionSystemAreaID <> sdr.GetInt32(12) Then
            parentArea = Me.GetProductionSystemArea(sdr.GetInt32(12))
          End If
          Dim ISSharedTimeline As Boolean = sdr.GetBoolean(20)
          If parentArea Is Nothing And ISSharedTimeline Then
            parentArea = Me(0).ProductionSystemAreaList(0)
          End If
          parentArea.ProductionTimelineList.RaiseListChangedEvents = False
          parentArea.ProductionTimelineList.Add(ProductionTimeline.GetProductionTimeline(sdr))
          parentArea.ProductionTimelineList.RaiseListChangedEvents = True
        End While
      End If

      'Production Human Resource
      If sdr.NextResult() Then
        While sdr.Read
          If parentArea Is Nothing OrElse parentArea.ProductionSystemAreaID <> sdr.GetInt32(25) Then
            parentArea = Me.GetProductionSystemArea(sdr.GetInt32(25))
          End If
          parentArea.ProductionHumanResourceList.RaiseListChangedEvents = False
          parentArea.ProductionHumanResourceList.Add(ProductionHumanResource.GetProductionHumanResource(sdr))
          parentArea.ProductionHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      'Production Outsourced Service
      If sdr.NextResult() Then
        While sdr.Read
          If parentArea Is Nothing OrElse parentArea.ProductionSystemAreaID <> sdr.GetInt32(18) Then
            parentArea = Me.GetProductionSystemArea(sdr.GetInt32(18))
          End If
          parentArea.ProductionOutsourceServiceList.RaiseListChangedEvents = False
          parentArea.ProductionOutsourceServiceList.Add(ProductionOutsourceService.GetProductionOutsourceService(sdr))
          parentArea.ProductionOutsourceServiceList.RaiseListChangedEvents = True
        End While
      End If

      'Production Outsourced Service Detail
      Dim parentService As ProductionOutsourceService = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentService Is Nothing OrElse parentService.ProductionOutsourceServiceID <> sdr.GetInt32(1) Then
            parentService = Me.GetProductionOutsourceService(sdr.GetInt32(1))
          End If
          parentService.ProductionOutsourceServiceDetailList.RaiseListChangedEvents = False
          parentService.ProductionOutsourceServiceDetailList.Add(ProductionOutsourceServiceDetail.GetProductionOutsourceServiceDetail(sdr))
          parentService.ProductionOutsourceServiceDetailList.RaiseListChangedEvents = True
        End While
      End If

      'Production Outsourced Service Timeline
      If sdr.NextResult() Then
        While sdr.Read
          If parentService Is Nothing OrElse parentService.ProductionOutsourceServiceID <> sdr.GetInt32(1) Then
            parentService = Me.GetProductionOutsourceService(sdr.GetInt32(1))
          End If
          parentService.ProductionOutsourceServiceTimelineList.RaiseListChangedEvents = False
          parentService.ProductionOutsourceServiceTimelineList.Add(ProductionOutsourceServiceTimeline.GetProductionOutsourceServiceTimeline(sdr))
          parentService.ProductionOutsourceServiceTimelineList.RaiseListChangedEvents = True
        End While
      End If

      'HR Schedule
      If sdr.NextResult() Then
        While sdr.Read
          If parentArea Is Nothing OrElse parentArea.ProductionSystemAreaID <> sdr.GetInt32(2) Then
            parentArea = Me.GetProductionSystemArea(sdr.GetInt32(2))
          End If
          parentArea.HRProductionScheduleList.RaiseListChangedEvents = False
          parentArea.HRProductionScheduleList.Add(HRProductionSchedule.GetHRProductionSchedule(sdr))
          parentArea.HRProductionScheduleList.RaiseListChangedEvents = True
        End While
      End If

      'HR Schedule Detail
      Dim parentHRSchedule As HRProductionSchedule = Nothing
      If sdr.NextResult() Then
        Dim parentProductionHumanResource As ProductionHumanResource = Nothing
        Dim parentProductionTimeline As ProductionTimeline = Nothing
        While sdr.Read
          If parentHRSchedule Is Nothing OrElse parentHRSchedule.HumanResourceID <> sdr.GetInt32(0) Then
            parentHRSchedule = Me.GetHRProductionSchedule(sdr.GetInt32(0))
          End If
          If IsNothing(parentProductionHumanResource) OrElse parentProductionHumanResource.ProductionHumanResourceID <> sdr.GetInt32(2) Then
            parentProductionHumanResource = Me(0).ProductionSystemAreaList(0).ProductionHumanResourceList.GetItem(sdr.GetInt32(2))
          End If
          If IsNothing(parentProductionTimeline) OrElse parentProductionTimeline.ProductionTimelineID <> sdr.GetInt32(3) Then
            parentProductionTimeline = Me(0).ProductionSystemAreaList(0).ProductionTimelineList.GetItem(sdr.GetInt32(3))
          End If
          parentHRSchedule.HRProductionScheduleDetailList.RaiseListChangedEvents = False
          parentHRSchedule.HRProductionScheduleDetailList.Add(HRProductionScheduleDetail.GetHRProductionScheduleDetail(sdr, parentProductionHumanResource, parentProductionTimeline))
          parentHRSchedule.HRProductionScheduleDetailList.RaiseListChangedEvents = True
          parentProductionHumanResource = Nothing
          parentProductionTimeline = Nothing
        End While
      End If

      parentHRSchedule = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentHRSchedule Is Nothing OrElse parentHRSchedule.HumanResourceID <> sdr.GetInt32(1) Then
            parentHRSchedule = Me.GetHRProductionSchedule(sdr.GetInt32(1))
          End If
          If parentHRSchedule IsNot Nothing Then
            parentHRSchedule.AccessShiftList.RaiseListChangedEvents = False
            parentHRSchedule.AccessShiftList.Add(Biometrics.AccessShift.GetAccessShift(sdr, OBLib.CommonData.Enums.AccessTerminalGroup.Outside_Broadcast, Not parentHRSchedule.GetParent.GetParent.Reconciled))
            parentHRSchedule.AccessShiftList.RaiseListChangedEvents = True
          End If
        End While
      End If

      parentHRSchedule = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentHRSchedule Is Nothing OrElse parentHRSchedule.HumanResourceID <> sdr.GetInt32(1) Then
            parentHRSchedule = Me.GetHRProductionSchedule(sdr.GetInt32(1))
          End If
          If parentHRSchedule IsNot Nothing Then
            parentHRSchedule.AccessHRBookingList.RaiseListChangedEvents = False
            parentHRSchedule.AccessHRBookingList.Add(Biometrics.AccessHRBooking.GetAccessHRBooking(sdr))
            parentHRSchedule.AccessHRBookingList.RaiseListChangedEvents = True
          End If
        End While
      End If

      'parentHRSchedule = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentHRSchedule Is Nothing OrElse parentHRSchedule.HumanResourceID <> sdr.GetInt32(1) Then
      '      parentHRSchedule = Me.GetHRProductionSchedule(sdr.GetInt32(1))
      '    End If
      '    If parentHRSchedule IsNot Nothing Then
      '      parentHRSchedule.AccessShiftList.RaiseListChangedEvents = False
      '      parentHRSchedule.AccessShiftList.Add(Biometrics.AccessShift.GetAccessShift(sdr, OBLib.CommonData.Enums.AccessTerminalGroup.Outside_Broadcast))
      '      parentHRSchedule.AccessShiftList.RaiseListChangedEvents = True

      '    End If
      '  End While
      'End If

      ''Production HR Planner
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentArea Is Nothing OrElse parentArea.ProductionSystemAreaID <> sdr.GetInt32(1) Then
      '      parentArea = Me.GetProductionSystemArea(sdr.GetInt32(1))
      '    End If
      '    parentArea.ProductionHRPlannerList.RaiseListChangedEvents = False
      '    parentArea.ProductionHRPlannerList.Add(ProductionHRPlanner.GetProductionHRPlanner(sdr))
      '    parentArea.ProductionHRPlannerList.RaiseListChangedEvents = True
      '  End While
      'End If

      'Production Documents
      If sdr.NextResult Then
        While sdr.Read
          If IsNothing(parent) OrElse parent.ProductionID <> sdr.GetInt32(4) Then
            parent = Me.GetItem(sdr.GetInt32(4))
          End If
          parent.ProductionDocumentList.RaiseListChangedEvents = False
          parent.ProductionDocumentList.Add(ProductionDocument.GetProductionDocument(sdr))
          parent.ProductionDocumentList.RaiseListChangedEvents = True
        End While
      End If

      'Production General Comments
      If sdr.NextResult Then
        While sdr.Read
          If IsNothing(parent) OrElse parent.ProductionID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionGeneralCommentList.RaiseListChangedEvents = False
          parent.ProductionGeneralCommentList.Add(ProductionGeneralComment.GetProductionGeneralComment(sdr))
          parent.ProductionGeneralCommentList.RaiseListChangedEvents = True
        End While
      End If

      'Production Petty Cash
      If sdr.NextResult Then
        While sdr.Read
          If IsNothing(parent) OrElse parent.ProductionID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
            'If parent Is Nothing Then parent = Me.GetItem(sdr.GetInt32(3))
          End If
          parent.ProductionPettyCashList.RaiseListChangedEvents = False
          parent.ProductionPettyCashList.Add(ProductionPettyCash.GetProductionPettyCash(sdr))
          parent.ProductionPettyCashList.RaiseListChangedEvents = True
        End While
      End If


      'Production Comments
      If sdr.NextResult Then
        While sdr.Read
          If IsNothing(parent) OrElse parent.ProductionID <> sdr.GetInt32(2) Then
            parent = Me.GetItem(sdr.GetInt32(2))
          End If
          parent.ProductionCommentSectionList.RaiseListChangedEvents = False
          parent.ProductionCommentSectionList.Add(ProductionCommentSection.GetProductionCommentSection(sdr))
          parent.ProductionCommentSectionList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentCommentSection As ProductionCommentSection = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentCommentSection Is Nothing OrElse parentCommentSection.ProductionCommentSectionID <> sdr.GetInt32(1) Then
            parentCommentSection = Me.GetProductionCommentSection(sdr.GetInt32(1))
          End If
          parentCommentSection.ProductionCommentHeaderList.RaiseListChangedEvents = False
          parentCommentSection.ProductionCommentHeaderList.Add(ProductionCommentHeader.GetProductionCommentHeader(sdr))
          parentCommentSection.ProductionCommentHeaderList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentCommentHeader As ProductionCommentHeader = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentCommentHeader Is Nothing OrElse parentCommentHeader.ProductionCommentHeaderID <> sdr.GetInt32(1) Then
            parentCommentHeader = Me.GetProductionCommentHeader(sdr.GetInt32(1))
          End If
          parentCommentHeader.ProductionCommentList.RaiseListChangedEvents = False
          parentCommentHeader.ProductionCommentList.Add(ProductionComment.GetProductionComment(sdr))
          parentCommentHeader.ProductionCommentList.RaiseListChangedEvents = True
        End While
      End If

      'New Bookings
      If sdr.NextResult() Then
        While sdr.Read
          If parentArea Is Nothing OrElse parentArea.ProductionSystemAreaID <> sdr.GetInt32(1) Then
            parentArea = Me.GetProductionSystemArea(sdr.GetInt32(1))
          End If
          If parentArea IsNot Nothing Then
            parentArea.ProductionHROBList.RaiseListChangedEvents = False
            parentArea.ProductionHROBList.Add(ProductionHROB.GetProductionHROB(sdr))
            parentArea.ProductionHROBList.RaiseListChangedEvents = True
          End If
        End While
      End If

      'Resource Bookings
      Dim parentProductionHROB As ProductionHROB = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentProductionHROB Is Nothing OrElse parentProductionHROB.ProductionHRID <> sdr.GetInt32(19) Then
            parentProductionHROB = Me.GetProductionHROB(sdr.GetInt32(19))
          End If
          If parentProductionHROB IsNot Nothing Then
            parentProductionHROB.ResourceBookingOBList.RaiseListChangedEvents = False
            parentProductionHROB.ResourceBookingOBList.Add(ResourceBookingOB.GetResourceBookingOB(sdr))
            parentProductionHROB.ResourceBookingOBList.RaiseListChangedEvents = True
          End If
        End While
      End If

      'CrewSchedules
      If sdr.NextResult Then
        While sdr.Read

        End While
      End If

      'Travel Requisition
      If sdr.NextResult Then
        While sdr.Read
          If IsNothing(parent) OrElse parent.ProductionID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
            'If parent Is Nothing Then parent = Me.GetItem(sdr.GetInt32(3))
          End If
          parent.TravelRequisitionList.RaiseListChangedEvents = False
          parent.TravelRequisitionList.Add(TravelRequisition.GetTravelRequisition(sdr))
          parent.TravelRequisitionList.RaiseListChangedEvents = True
        End While
      End If

      ''Crew Schedule Details
      'Dim parentResourceBooking As ResourceBookingOB = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentResourceBooking Is Nothing OrElse (parentResourceBooking.ResourceBookingID <> sdr.GetInt32(26)) Then
      '      parentResourceBooking = Me.GetResourceBookingOB(sdr.GetInt32(26))
      '    End If
      '    Dim parentProductionHumanResource As ProductionHumanResource = Nothing
      '    Dim parentProductionTimeline As ProductionTimeline = Nothing
      '    If IsNothing(parentProductionHumanResource) OrElse parentProductionHumanResource.ProductionHumanResourceID <> sdr.GetInt32(1) Then
      '      parentProductionHumanResource = Me(0).ProductionSystemAreaList(0).ProductionHumanResourceList.GetItem(sdr.GetInt32(1))
      '    End If
      '    If IsNothing(parentProductionTimeline) OrElse parentProductionTimeline.ProductionTimelineID <> sdr.GetInt32(2) Then
      '      parentProductionTimeline = Me(0).ProductionSystemAreaList(0).ProductionTimelineList.GetItem(sdr.GetInt32(2))
      '    End If
      '    parentResourceBooking.CrewScheduleDetailOBList.RaiseListChangedEvents = False
      '    parentResourceBooking.CrewScheduleDetailOBList.Add(OBLib.Productions.OB.CrewScheduleDetailOB.GetCrewScheduleDetailOB(sdr, parentProductionHumanResource, parentProductionTimeline))
      '    parentResourceBooking.CrewScheduleDetailOBList.RaiseListChangedEvents = True
      '  End While
      'End If

      For Each Production As Production In Me
        Production.CheckRules()

        For Each pv As ProductionVehicle In Production.ProductionVehicleList
          pv.CheckRules()
          pv.CheckClash()
        Next

        For Each psa As ProductionSystemArea In Production.ProductionSystemAreaList
          psa.CheckRules()
          psa.CheckNotComplySpecReasonsValid()

          For Each psret As ProductionSpecRequirementEquipmentType In psa.ProductionSpecRequirementEquipmentTypeList
            psret.CheckRules()
          Next

          For Each psrep As ProductionSpecRequirementPosition In psa.ProductionSpecRequirementPositionList
            psrep.CheckRules()
          Next

          For Each ProductionOSS As ProductionOutsourceService In psa.ProductionOutsourceServiceList
            ProductionOSS.CheckRules()
            For Each ProductionOSSD As ProductionOutsourceServiceDetail In ProductionOSS.ProductionOutsourceServiceDetailList
              ProductionOSSD.CheckRules()
            Next
            For Each ProductionOSST As ProductionOutsourceServiceTimeline In ProductionOSS.ProductionOutsourceServiceTimelineList
              ProductionOSST.CheckRules()
            Next
          Next

          For Each ProductionTimeline As ProductionTimeline In psa.ProductionTimelineList
            ProductionTimeline.CheckRules()
          Next

          For Each ProductionHumanResource As ProductionHumanResource In psa.ProductionHumanResourceList
            ProductionHumanResource.CheckRules()
          Next

          For Each HRProductionSchedule As HRProductionSchedule In psa.HRProductionScheduleList
            HRProductionSchedule.UpdateDetails()
            If Not psa.GetParent.Reconciled Then
              HRProductionSchedule.PopulateMissingBiometricsShifts()
            End If
            HRProductionSchedule.CheckRules()
            For Each asd As HRProductionScheduleDetail In HRProductionSchedule.HRProductionScheduleDetailList
              asd.CheckRules()
            Next
            For Each asd As OBLib.Biometrics.AccessShift In HRProductionSchedule.AccessShiftList
              asd.CheckRules()
            Next

            HRProductionSchedule.CheckPreAndPostTravel()
          Next

          'For Each ProductionHRPlanner As ProductionHRPlanner In psa.ProductionHRPlannerList
          '  ProductionHRPlanner.CheckRules()
          'Next

          psa.CheckTimelineValid()
          psa.CheckAvailability()
        Next

        For Each pd As ProductionDocument In Production.ProductionDocumentList
          pd.CheckRules()
        Next

        For Each pgc As ProductionGeneralComment In Production.ProductionGeneralCommentList
          pgc.CheckRules()
        Next

        For Each ppc As ProductionPettyCash In Production.ProductionPettyCashList
          ppc.CheckRules()
        Next

        For Each pcs As ProductionCommentSection In Production.ProductionCommentSectionList
          pcs.CheckRules()
          For Each pch As ProductionCommentHeader In pcs.ProductionCommentHeaderList
            pch.CheckRules()
            For Each pc As ProductionComment In pch.ProductionCommentList
              pc.CheckRules()
            Next
          Next
        Next

      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionListOld"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As Production In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As Production In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace