﻿' Generated 29 May 2014 18:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Productions.Specs.ReadOnly
Imports OBLib.Productions.Areas
Imports OBLib.Productions.Specs
Imports OBLib.Productions.Vehicles
Imports OBLib.Productions.Correspondence
Imports OBLib.Maintenance.Productions.Correspondence
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Productions.Services

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance

Namespace Productions.Old

  <Serializable()> _
  Public Class Production
    Inherits SingularBusinessBase(Of Production)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Production Description")
    ''' <summary>
    ''' Gets and sets the Production Description value
    ''' </summary>
    <Display(Name:="Description", Description:=""),
    Required(ErrorMessage:="Description is required"),
    StringLength(200, ErrorMessage:="Description cannot be more than 200 characters")>
    Public Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionDescriptionProperty, Value)
      End Set
    End Property

    Public Shared TeamsPlayingProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.TeamsPlaying, "") _
                                                                    .AddSetExpression("ProductionBO.Production_SetDefaultProductionDescription(self)", False)
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Teams Playing", Description:=""),
    StringLength(200, ErrorMessage:="Teams Playing cannot be more than 200 characters")>
    Public Property TeamsPlaying() As String
      Get
        Return GetProperty(TeamsPlayingProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TeamsPlayingProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionTypeID, Nothing) _
                                                                          .AddSetExpression("ProductionBO.ProductionTypeIDChecks(self)", False) _
                                                                          .AddSetExpression("ProductionBO.Production_SetDefaultProductionDescription(self)", False)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:=""),
    Required(ErrorMessage:="Production Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionTypeList), Source:=DropDownWeb.SourceType.CommonData)>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property
    'Required(ErrorMessage:="Production Type required")
    'Singular.DataAnnotations.DropDownWeb(GetType(ROProductionTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.FindScreen)

    ', Singular.Web.WebFetchable()

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EventTypeID, Nothing) _
                                                                     .AddSetExpression("ProductionBO.Production_SetDefaultProductionDescription(self)", False)
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:=""),
    Required(ErrorMessage:="Event Type required"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROEventTypeList), ThisFilterMember:="ProductionTypeID", Source:=DropDownWeb.SourceType.CommonData)>
    Public Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EventTypeIDProperty, Value)
      End Set
    End Property
    'Required(ErrorMessage:="Event Type required")
    'Singular.DataAnnotations.DropDownWeb(GetType(ROEventTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.FindScreen)

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSpecRequirementID, "Production Spec", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Spec Requirement value
    ''' </summary>
    <Display(Name:="Spec", Description:="")>
    Public Property ProductionSpecRequirementID() As Integer?
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSpecRequirementIDProperty, Value)
      End Set
    End Property
    ''    Singular.DataAnnotations.DropDownWeb(GetType(ROProductionSpecRequirementList), ThisFilterMember:="ProductionTypeID")
    '', Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel
    ''Required(ErrorMessage:="Spec required")
    ''Singular.DataAnnotations.DropDownWeb(GetType(ROProductionSpecRequirementList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.FindScreen)

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionVenueID, Nothing) _
                                                                           .AddSetExpression("ProductionBO.SetupAutoServices(self)", False) _
                                                                           .AddSetExpression("ProductionBO.SetVenueConfirmedDate(self)", False) _
                                                                           .AddSetExpression("ProductionBO.Production_SetDefaultProductionDescription(self)", False)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:=""),
    Required(ErrorMessage:="Venue required"),
    DropDownWeb(GetType(ROProductionVenueOldList), UnselectedText:="Stadium")>
    Public Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueIDProperty, Value)
      End Set
    End Property
    'Required(ErrorMessage:="Venue required")
    'Singular.DataAnnotations.DropDownWeb(GetType(ROProductionVenueList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.FindScreen)

    Public Shared VenueConfirmedDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.VenueConfirmedDate, Nothing) _
                                                                             .AddSetExpression("ProductionBO.SetupAutoServices(self)", False)
    ''' <summary>
    ''' Gets and sets the Venue Confirmed Date value
    ''' </summary>
    <Display(Name:="Venue Confirmed Date", Description:="")>
    Public Property VenueConfirmedDate As DateTime?
      Get
        Return GetProperty(VenueConfirmedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(VenueConfirmedDateProperty, Value)
      End Set
    End Property

    Public Shared HDRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HDRequiredInd, "HD Required", True)
    ''' <summary>
    ''' Gets and sets the HD Required value
    ''' </summary>
    <Display(Name:="HD Required", Description:="")>
    Public Property HDRequiredInd() As Boolean
      Get
        Return GetProperty(HDRequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HDRequiredIndProperty, Value)
      End Set
    End Property

    Public Shared NotComplySpecReasonsProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.NotComplySpecReasons, "")
    ''' <summary>
    ''' Gets and sets the Not Comply Spec Reasons value
    ''' </summary>
    <Display(Name:="Not Comply Spec Reasons", Description:="")>
    Public Property NotComplySpecReasons() As String
      Get
        Return GetProperty(NotComplySpecReasonsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotComplySpecReasonsProperty, Value)
      End Set
    End Property

    Public Shared CrewFinalisedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CrewFinalised, "Crew Finalised?", False)
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised By value
    ''' </summary>
    <Display(Name:="Crew Finalised?", Description:="")>
    Public Property CrewFinalised() As Boolean
      Get
        Return GetProperty(CrewFinalisedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CrewFinalisedProperty, Value)
      End Set
    End Property

    Public Shared LoadedCrewFinalisedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.LoadedCrewFinalised, "Crew Finalised?", False)
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised By value
    ''' </summary>
    <Browsable(False)>
    Public Property LoadedCrewFinalised() As Boolean
      Get
        Return GetProperty(LoadedCrewFinalisedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(LoadedCrewFinalisedProperty, Value)
      End Set
    End Property

    Public Shared CrewPlanningFinalisedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewPlanningFinalisedBy, "Crew Planning Finalised By", Nothing)
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised By value
    ''' </summary>
    <Display(Name:="Crew Planning Finalised By", Description:="")>
    Public Property CrewPlanningFinalisedBy() As Integer?
      Get
        Return GetProperty(CrewPlanningFinalisedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewPlanningFinalisedByProperty, Value)
      End Set
    End Property

    Public Shared CrewPlanningFinalisedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewPlanningFinalisedDate, "Date")
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public Property CrewPlanningFinalisedDate As DateTime?
      Get
        Return GetProperty(CrewPlanningFinalisedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CrewPlanningFinalisedDateProperty, Value)
      End Set
    End Property

    Public Shared PlanningFinalisedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PlanningFinalised, "Planning Finalised?", False)
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised By value
    ''' </summary>
    <Display(Name:="Planning Finalised?", Description:="")>
    Public Property PlanningFinalised() As Boolean
      Get
        Return GetProperty(PlanningFinalisedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PlanningFinalisedProperty, Value)
      End Set
    End Property

    Public Shared PlanningFinalisedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlanningFinalisedDate, "Date")
    ''' <summary>
    ''' Gets and sets the Planning Finalised Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public Property PlanningFinalisedDate As DateTime?
      Get
        Return GetProperty(PlanningFinalisedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(PlanningFinalisedDateProperty, Value)
      End Set
    End Property

    Public Shared PlanningFinalisedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PlanningFinalisedBy, "Planning Finalised By", Nothing)
    ''' <summary>
    ''' Gets and sets the Planning Finalised By value
    ''' </summary>
    <Display(Name:="Planning Finalised By", Description:="")>
    Public Property PlanningFinalisedBy() As Integer?
      Get
        Return GetProperty(PlanningFinalisedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PlanningFinalisedByProperty, Value)
      End Set
    End Property

    Public Shared CancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Cancelled, "Cancelled?", False)
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised By value
    ''' </summary>
    <Display(Name:="Cancelled?", Description:="")>
    Public Property Cancelled() As Boolean
      Get
        Return GetProperty(CancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CancelledProperty, Value)
      End Set
    End Property

    Public Shared CancelledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDate, "Date")
    ''' <summary>
    ''' Gets and sets the Cancelled Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public Property CancelledDate As DateTime?
      Get
        Return GetProperty(CancelledDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancelledDateProperty, Value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="")>
    Public Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
      End Set
    End Property

    Public Shared ReconciledProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.Reconciled) _
                                                                   .AddSetExpression("ProductionBO.CheckCanReconcile(self)", False)
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised By value
    ''' </summary>
    <Display(Name:="Reconciled?", Description:="")>
    Public Property Reconciled() As Boolean
      Get
        Return GetProperty(ReconciledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ReconciledProperty, Value)
      End Set
    End Property

    Public Shared ReconciledByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ReconciledBy, "Reconciled By", Nothing)
    ''' <summary>
    ''' Gets and sets the Reconciled By value
    ''' </summary>
    <Display(Name:="Reconciled By", Description:="")>
    Public Property ReconciledBy() As Integer?
      Get
        Return GetProperty(ReconciledByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ReconciledByProperty, Value)
      End Set
    End Property

    Public Shared ReconciledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ReconciledDate, "Date")
    ''' <summary>
    ''' Gets and sets the Reconciled Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public Property ReconciledDate As DateTime?
      Get
        Return GetProperty(ReconciledDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ReconciledDateProperty, Value)
      End Set
    End Property

    Public Shared ReconciledSetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ReconciledSetDate, "Reconciled Set Date")
    ''' <summary>
    ''' Gets and sets the Reconciled Date value
    ''' </summary>
    <Display(Name:="Reconciled Set Date", Description:="")>
    Public Property ReconciledSetDate As DateTime?
      Get
        Return GetProperty(ReconciledSetDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ReconciledSetDateProperty, Value)
      End Set
    End Property

    Public Shared ProductionDerigTemplateIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionDerigTemplateID, "Production Derig Template", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Derig Template value
    ''' </summary>
    <Display(Name:="Production Derig Template", Description:="")>
    Public Property ProductionDerigTemplateID() As Integer?
      Get
        Return GetProperty(ProductionDerigTemplateIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionDerigTemplateIDProperty, Value)
      End Set
    End Property

    Public Shared AllowDuplicateHumanResourcesIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AllowDuplicateHumanResourcesInd, "Allow Duplicate Human Resources", False)
    ''' <summary>
    ''' Gets and sets the Allow Duplicate Human Resources value
    ''' </summary>
    <Display(Name:="Allow Duplicate Human Resources", Description:="")>
    Public Property AllowDuplicateHumanResourcesInd() As Boolean
      Get
        Return GetProperty(AllowDuplicateHumanResourcesIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(AllowDuplicateHumanResourcesIndProperty, Value)
      End Set
    End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionRefNo, "Production Ref No")
    ''' <summary>
    ''' Gets and sets the Production Ref No value
    ''' </summary>
    <Display(Name:="Ref Num", Description:="")>
    Public ReadOnly Property ProductionRefNo() As String
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
    End Property
    'Set(ByVal Value As String)
    '  SetProperty(ProductionRefNoProperty, Value)
    'End Set
    ',
    'StringLength(4, ErrorMessage:="Production Ref No cannot be more than 4 characters")

    Public Shared ParentProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentProductionID, "Parent Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Parent Production value
    ''' </summary>
    <Display(Name:="Parent Production", Description:="")>
    Public Property ParentProductionID() As Integer?
      Get
        Return GetProperty(ParentProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ParentProductionIDProperty, Value)
      End Set
    End Property

    'Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Department", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the System value
    ' ''' </summary>
    '<Display(Name:="Sub-Department", Description:="")>
    'Public Property SystemID() As Integer?
    '  Get
    '    Return GetProperty(SystemIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(SystemIDProperty, Value)
    '  End Set
    'End Property
    ''    Singular.DataAnnotations.DropDownWeb(GetType(ROSystemList))
    ''This property is no longer needed at this level, it has been moved to the ProductionSystemArea level

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CommentatorCrewFinalisedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CommentatorCrewFinalised, "Crew Finalised?", False)
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised By value
    ''' </summary>
    <Display(Name:="Crew Finalised?", Description:="")>
    Public Property CommentatorCrewFinalised() As Boolean
      Get
        Return GetProperty(CommentatorCrewFinalisedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CommentatorCrewFinalisedProperty, Value)
      End Set
    End Property

    Public Shared CommentatorCrewPlanningFinalisedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CommentatorCrewPlanningFinalisedBy, "Commentator Crew Planning Finalised By", Nothing)
    ''' <summary>
    ''' Gets and sets the Commentator Crew Planning Finalised By value
    ''' </summary>
    <Display(Name:="Commentator Crew Planning Finalised By", Description:="")>
    Public Property CommentatorCrewPlanningFinalisedBy() As Integer?
      Get
        Return GetProperty(CommentatorCrewPlanningFinalisedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CommentatorCrewPlanningFinalisedByProperty, Value)
      End Set
    End Property

    Public Shared CommentatorCrewPlanningFinalisedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CommentatorCrewPlanningFinalisedDate, "Commentator Crew Planning Finalised Date")
    ''' <summary>
    ''' Gets and sets the Commentator Crew Planning Finalised Date value
    ''' </summary>
    <Display(Name:="Commentator Crew Planning Finalised Date", Description:="")>
    Public Property CommentatorCrewPlanningFinalisedDate As DateTime?
      Get
        Return GetProperty(CommentatorCrewPlanningFinalisedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CommentatorCrewPlanningFinalisedDateProperty, Value)
      End Set
    End Property

    Public Shared CommentatorPlanningFinalisedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CommentatorPlanningFinalised, "Planning Finalised?", False)
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised By value
    ''' </summary>
    <Display(Name:="Planning Finalised?", Description:="")>
    Public Property CommentatorPlanningFinalised() As Boolean
      Get
        Return GetProperty(CommentatorPlanningFinalisedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CommentatorPlanningFinalisedProperty, Value)
      End Set
    End Property

    Public Shared CommentatorPlanningFinalisedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CommentatorPlanningFinalisedBy, "Commentator Planning Finalised By", Nothing)
    ''' <summary>
    ''' Gets and sets the Commentator Planning Finalised By value
    ''' </summary>
    <Display(Name:="Commentator Planning Finalised By", Description:="")>
    Public Property CommentatorPlanningFinalisedBy() As Integer?
      Get
        Return GetProperty(CommentatorPlanningFinalisedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CommentatorPlanningFinalisedByProperty, Value)
      End Set
    End Property

    Public Shared CommentatorPlanningFinalisedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CommentatorPlanningFinalisedDate, "Commentator Planning Finalised Date")
    ''' <summary>
    ''' Gets and sets the Commentator Planning Finalised Date value
    ''' </summary>
    <Display(Name:="Commentator Planning Finalised Date", Description:="")>
    Public Property CommentatorPlanningFinalisedDate As DateTime?
      Get
        Return GetProperty(CommentatorPlanningFinalisedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CommentatorPlanningFinalisedDateProperty, Value)
      End Set
    End Property

    Public Shared CommentatorReconciledByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CommentatorReconciledBy, "Commentator Reconciled By", Nothing)
    ''' <summary>
    ''' Gets and sets the Commentator Reconciled By value
    ''' </summary>
    <Display(Name:="Commentator Reconciled By", Description:="")>
    Public Property CommentatorReconciledBy() As Integer?
      Get
        Return GetProperty(CommentatorReconciledByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CommentatorReconciledByProperty, Value)
      End Set
    End Property

    Public Shared CommentatorReconciledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CommentatorReconciledDate, "Commentator Reconciled Date")
    ''' <summary>
    ''' Gets and sets the Commentator Reconciled Date value
    ''' </summary>
    <Display(Name:="Commentator Reconciled Date", Description:="")>
    Public Property CommentatorReconciledDate As DateTime?
      Get
        Return GetProperty(CommentatorReconciledDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CommentatorReconciledDateProperty, Value)
      End Set
    End Property

    Public Shared CommentatorReconciledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CommentatorReconciled, "Reconciled?", False)
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised By value
    ''' </summary>
    <Display(Name:="Reconciled?", Description:="")>
    Public Property CommentatorReconciled() As Boolean
      Get
        Return GetProperty(CommentatorReconciledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CommentatorReconciledProperty, Value)
      End Set
    End Property

    Public Shared SynergyGenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.SynergyGenRefNo, "Synergy Gen Ref No")
    ''' <summary>
    ''' Gets and sets the Synergy Gen Ref No value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:="")>
    Public Property SynergyGenRefNo() As Int64?
      Get
        Return GetProperty(SynergyGenRefNoProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(SynergyGenRefNoProperty, Value)
      End Set
    End Property

    Public Shared ProductionGradeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionGradeID, "Grade", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Grade value
    ''' </summary>
    <Display(Name:="Grade", Description:="")>
    Public Property ProductionGradeID() As Integer?
      Get
        Return GetProperty(ProductionGradeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionGradeIDProperty, Value)
      End Set
    End Property
    'Singular.DataAnnotations.DropDownWeb(GetType(ROProductionGradeList))

    Public Shared PlayStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.PlayStartDateTime) _
                                                                            .AddSetExpression("ProductionBO.PlayStartSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Play Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:=""),
    Required(ErrorMessage:="Start date and time of the event")>
    Public Property PlayStartDateTime As DateTime?
      Get
        Return GetProperty(PlayStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(PlayStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared PlayEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.PlayEndDateTime) _
                                                                          .AddSetExpression("ProductionBO.PlayEndSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Play End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="End date and time of the event"),
    Required(ErrorMessage:="End Date Time required")>
    Public Property PlayEndDateTime As DateTime?
      Get
        Return GetProperty(PlayEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(PlayEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:=""),
    StringLength(200, ErrorMessage:="Title cannot be more than 200 characters")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    'Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionType, "Production Type") _
    '                                                                  .AddSetExpression(OBLib.JSCode.ProductionBO.Production_SetDefaultProductionDescription, False)
    ' ''' <summary>
    ' ''' Gets and sets the Production Type value
    ' ''' </summary>
    '<Display(Name:="Production Type", Description:="")>
    'Public Property ProductionType() As String
    '  Get
    '    Return GetProperty(ProductionTypeProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(ProductionTypeProperty, Value)
    '  End Set
    'End Property

    'Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.EventType, "Event Type") _
    '                                                             .AddSetExpression(OBLib.JSCode.ProductionBO.Production_SetDefaultProductionDescription, False)
    ' ''' <summary>
    ' ''' Gets and sets the Event Type value
    ' ''' </summary>
    '<Display(Name:="Event Type", Description:="")>
    'Public Property EventType() As String
    '  Get
    '    Return GetProperty(EventTypeProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(EventTypeProperty, Value)
    '  End Set
    'End Property

    'Public Shared SpecNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SpecName, "Spec")
    ' ''' <summary>
    ' ''' Gets and sets the Spec Name value
    ' ''' </summary>
    '<Display(Name:="Spec", Description:=""),
    'Required(ErrorMessage:="Spec is required")>
    'Public Property SpecName() As String
    '  Get
    '    Return GetProperty(SpecNameProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(SpecNameProperty, Value)
    '  End Set
    'End Property

    'Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionVenue, "") _
    '                                                                   .AddSetExpression(OBLib.JSCode.ProductionBO.Production_SetDefaultProductionDescription, False)
    ' ''' <summary>
    ' ''' Gets and sets the Production Venue value
    ' ''' </summary>
    '<Display(Name:="Venue", Description:=""),
    'Required(ErrorMessage:="Venue is required")>
    'Public Property ProductionVenue() As String
    '  Get
    '    Return GetProperty(ProductionVenueProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(ProductionVenueProperty, Value)
    '  End Set
    'End Property

    'Public Shared TxTimesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TxTimes, "TxTimes")
    ' ''' <summary>
    ' ''' Gets and sets the Production Venue value
    ' ''' </summary>
    '<Display(Name:="Tx Times", Description:="")>
    'Public ReadOnly Property TxTimes() As String
    '  Get
    '    Return GetProperty(TxTimesProperty)
    '  End Get
    '  'Set(ByVal Value As String)
    '  '  SetProperty(TxTimesProperty, Value)
    '  'End Set
    'End Property

    'Public Shared ProductionGradeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionGrade, "Production Grade")
    ' ''' <summary>
    ' ''' Gets and sets the Production Type value
    ' ''' </summary>
    '<Display(Name:="Production Grade", Description:="")>
    'Public Property ProductionGrade() As String
    '  Get
    '    Return GetProperty(ProductionGradeProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(ProductionGradeProperty, Value)
    '  End Set
    'End Property

    Public Shared HasSnTProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasSnT, "Has SnT?", False)
    ''' <summary>
    ''' Gets and sets the Crew Planning Finalised By value
    ''' </summary>
    <Display(Name:="Has SnT?", Description:="")>
    Public Property HasSnT() As Boolean
      Get
        Return GetProperty(HasSnTProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasSnTProperty, Value)
      End Set
    End Property

    Public Shared CannotReconcileReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CannotReconcileReason, "Cannot Reconcile", "")
    ''' <summary>
    ''' Gets and sets the Spec Name value
    ''' </summary>
    <Display(Name:="Cannot Reconcile", Description:="")>
    Public Property CannotReconcileReason() As String
      Get
        Return GetProperty(CannotReconcileReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CannotReconcileReasonProperty, Value)
      End Set
    End Property

    Public Shared CannotUnReconcileReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CannotUnReconcileReason, "Cannot UnReconcile", "")
    ''' <summary>
    ''' Gets and sets the Spec Name value
    ''' </summary>
    <Display(Name:="Cannot UnReconcile", Description:="")>
    Public Property CannotUnReconcileReason() As String
      Get
        Return GetProperty(CannotUnReconcileReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CannotUnReconcileReasonProperty, Value)
      End Set
    End Property

    Public Shared PlaceholderIndProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.PlaceholderInd)
    Public Property PlaceholderInd() As Boolean
      Get
        Return GetProperty(PlaceholderIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PlaceholderIndProperty, Value)
      End Set
    End Property

    Public Shared OriginalReconciledProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.OriginalReconciled)
    <Display(Name:="Reconciled?", Description:=""),
    Browsable(False)>
    Public Property OriginalReconciled() As Boolean
      Get
        Return GetProperty(OriginalReconciledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OriginalReconciledProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "ProductionBO.ProductionToString(self)")

    Public ReadOnly Property CanReconcileAccess As Boolean
      Get
        Return Singular.Security.HasAccess("Productions", "Reconcile Production")
      End Get
    End Property

    Public ReadOnly Property CanUnReconcileTodayAccess As Boolean
      Get
        Return Singular.Security.HasAccess("Productions", "Un-reconciled productions today")
      End Get
    End Property

    Public ReadOnly Property CanUnReconcileAccess As Boolean
      Get
        Return Singular.Security.HasAccess("Productions", "Un-reconciled productions")
      End Get
    End Property

    Public ReadOnly Property CanUnFinaliseCrewAccess As Boolean
      Get
        Return Singular.Security.HasAccess("Productions", "Can Un-Finalise Crew")
      End Get
    End Property

    Public ReadOnly Property CanFinaliseCrewAccess As Boolean
      Get
        Return Singular.Security.HasAccess("Productions", "Finalise Crew")
      End Get
    End Property

    Public ReadOnly Property CanEditCrewSchedulesAfterFinalised As Boolean
      Get
        If CrewFinalised Then
          Return Singular.Security.HasAccess("Productions", "Can Edit Crew Schedules after finalised")
        End If
        Return True
      End Get
    End Property

    Public ReadOnly Property CanEditHumanResourcesAfterFinalised As Boolean
      Get
        If CrewFinalised Then
          Return Singular.Security.HasAccess("Productions", "Can Edit Human Resources after finalised")
        End If
        Return True
      End Get
    End Property


    Public ReadOnly Property CurrentUserIsEventManager As Boolean
      Get
        Dim EM As List(Of ProductionHumanResource) = Me.ProductionSystemAreaList(0).ProductionHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, OBLib.Security.Settings.CurrentUser.HumanResourceID) _
                                                                                                                      And (CompareSafe(d.DisciplineID, CType(CommonData.Enums.Discipline.EventManager, Integer)) _
                                                                                                                           OrElse CompareSafe(d.DisciplineID, CType(CommonData.Enums.Discipline.EventManagerIntern, Integer)))).ToList
        If EM.Count > 0 Then
          Return True
        End If
        Return False
      End Get
    End Property

    Public ReadOnly Property SystemAreaReconciled As Boolean
      Get
        If OBLib.Security.Settings.CurrentUser.SystemID = 1 Then
          Return Reconciled
        ElseIf OBLib.Security.Settings.CurrentUser.SystemID = 2 Then
          Return CommentatorReconciled
        End If
        Return True
      End Get
    End Property

    Public ReadOnly Property SystemAreaCancelled As Boolean
      Get
        If OBLib.Security.Settings.CurrentUser.SystemID = 1 Then
          Return Cancelled
        ElseIf OBLib.Security.Settings.CurrentUser.SystemID = 2 Then
          Return False
        End If
        Return True
      End Get
    End Property

    Public ReadOnly Property CanOpenTravelMessage
      Get
        Return Me.CanOpenTravelPage()
      End Get
    End Property


    Public Shared LoadedTeamsPlayingProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.LoadedTeamsPlaying, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Teams Playing", Description:=""),
    Browsable(False)>
    Public Property LoadedTeamsPlaying() As String
      Get
        Return GetProperty(LoadedTeamsPlayingProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LoadedTeamsPlayingProperty, Value)
      End Set
    End Property

    Public Shared LoadedProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.LoadedProductionTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:=""),
    Browsable(False)>
    Public Property LoadedProductionTypeID() As Integer?
      Get
        Return GetProperty(LoadedProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LoadedProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared LoadedEventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.LoadedEventTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:=""),
    Browsable(False)>
    Public Property LoadedEventTypeID() As Integer?
      Get
        Return GetProperty(LoadedEventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LoadedEventTypeIDProperty, Value)
      End Set
    End Property

    Public Shared LoadedProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.LoadedProductionVenueID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:=""),
    Browsable(False)>
    Public Property LoadedProductionVenueID() As Integer?
      Get
        Return GetProperty(LoadedProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(LoadedProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared LoadedPlayStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.LoadedPlayStartDateTime)
    ''' <summary>
    ''' Gets and sets the Play Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public Property LoadedPlayStartDateTime As DateTime?
      Get
        Return GetProperty(LoadedPlayStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LoadedPlayStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared LoadedPlayEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.LoadedPlayEndDateTime)
    ''' <summary>
    ''' Gets and sets the Play End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="End date and time of the event")>
    Public Property LoadedPlayEndDateTime As DateTime?
      Get
        Return GetProperty(LoadedPlayEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LoadedPlayEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared LoadedTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoadedTitle, "Title", "")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:=""),
    Browsable(False)>
    Public Property LoadedTitle() As String
      Get
        Return GetProperty(LoadedTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LoadedTitleProperty, Value)
      End Set
    End Property

    Public Shared CrewPlanningFirstFinalisedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewPlanningFirstFinalisedDate, "Date")
    ''' <summary>
    ''' Gets and sets the Crew Planning First Finalised Date value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public Property CrewPlanningFirstFinalisedDate As DateTime?
      Get
        Return GetProperty(CrewPlanningFirstFinalisedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CrewPlanningFirstFinalisedDateProperty, Value)
      End Set
    End Property

    Public Shared CreationTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreationTypeID, "CreationTypeID", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="CreationTypeID", Description:="")>
    Public Property CreationTypeID() As Integer
      Get
        Return GetProperty(CreationTypeIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(CreationTypeIDProperty, value)
      End Set
    End Property

    Public Shared VisionViewIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.VisionViewInd, "VisionViewInd", False)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="VisionView", Description:="")>
    Public Property VisionViewInd() As Boolean?
      Get
        Return GetProperty(VisionViewIndProperty)
      End Get
      Set(value As Boolean?)
        SetProperty(VisionViewIndProperty, value)
      End Set
    End Property

    Public Shared TimelineReadyIndProperty As PropertyInfo(Of Boolean?) = RegisterSProperty(Of Boolean?)(Function(c) c.TimelineReadyInd, False) _
                                                                          .AddSetExpression("ProductionHelper.Production.Events.TimelineReadySet(self)", False)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Timeline Ready? ", Description:="")>
    Public Property TimelineReadyInd() As Boolean?
      Get
        Return GetProperty(TimelineReadyIndProperty)
      End Get
      Set(value As Boolean?)
        SetProperty(TimelineReadyIndProperty, value)
        SetProperty(TimelineReadyByUserIDProperty, OBLib.Security.Settings.CurrentUserID)
        SetProperty(TimelineReadyDateTimeProperty, DateTime.Now)
      End Set
    End Property

    Public Shared TimelineReadyDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TimelineReadyDateTime, "TimelineReadyDateTime")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="Timeline Ready DateTime", Description:="")>
    Public Property TimelineReadyDateTime As DateTime?
      Get
        Return GetProperty(TimelineReadyDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TimelineReadyDateTimeProperty, Value)
      End Set
    End Property

    Public Shared TimelineReadyByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TimelineReadyByUserID, "Timeline ReadyBy UserID", Nothing)
    ''' <summary>
    ''' Gets and sets the BA Voyager Type value
    ''' </summary>
    <Display(Name:="Timeline Ready ByUser ID", Description:="Timeline ReadyBy UserID")>
    Public Property TimelineReadyByUserID() As Integer?
      Get
        Return GetProperty(TimelineReadyByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TimelineReadyByUserIDProperty, Value)
      End Set
    End Property

    <Display(Description:=""), Browsable(False)>
    Public ReadOnly Property CanViewRequirementsInd() As Boolean
      Get
        Return True
      End Get
    End Property
    <Display(Description:=""), Browsable(False)>
    Public ReadOnly Property CanViewVehiclesInd() As Boolean
      Get
        Return True '(OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent) Or (OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices AndAlso TimelineReadyInd)
      End Get
    End Property
    <Display(Description:=""), Browsable(False)>
    Public ReadOnly Property CanViewServicesInd() As Boolean
      Get
        If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent Then
          Return True
        Else
          If PlaceholderInd Then
            Return False
          Else
            Return IsTimelineReady
          End If
        End If
        Return False
        'Return (OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent) Or (OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices AndAlso TimelineReadyInd)
      End Get
    End Property
    <Display(Description:=""), Browsable(False)>
    Public ReadOnly Property CanViewCrewInd() As Boolean
      Get
        Return True
      End Get
    End Property
    <Display(Description:=""), Browsable(False)>
    Public ReadOnly Property CanViewScheduleInd() As Boolean
      Get
        Return True
      End Get
    End Property
    <Display(Description:=""), Browsable(False)>
    Public ReadOnly Property CanViewCommentsInd() As Boolean
      Get
        If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent Then
          Return True
        Else
          If PlaceholderInd Then
            Return False
          Else
            Return IsTimelineReady
          End If
        End If
        Return False
        'Return (OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent) Or (OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices AndAlso TimelineReadyInd)
      End Get
    End Property
    <Display(Description:=""), Browsable(False)>
    Public ReadOnly Property CanViewAudioInd() As Boolean
      Get
        If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent Then
          Return True
        Else
          If PlaceholderInd Then
            Return False
          Else
            Return IsTimelineReady
          End If
        End If
        Return False
        'Return (OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent) Or (OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices AndAlso TimelineReadyInd)
      End Get
    End Property
    <Display(Description:=""), Browsable(False)>
    Public ReadOnly Property CanViewCorrespondenceInd() As Boolean
      Get
        If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent Then
          Return True
        Else
          If PlaceholderInd Then
            Return False
          Else
            Return IsTimelineReady
          End If
        End If
        Return False
        'Return (OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent) Or (OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices AndAlso TimelineReadyInd)
      End Get
    End Property

    <Display(Description:=""), Browsable(False)>
    Public ReadOnly Property CanViewTravelInd() As Boolean
      Get
        If OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent Then
          Return True
        Else
          If PlaceholderInd Then
            Return False
          Else
            Return IsTimelineReady
          End If
        End If
        Return False
        'Return (OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionContent) Or (OBLib.Security.Settings.CurrentUser.SystemID = OBLib.CommonData.Enums.System.ProductionServices AndAlso TimelineReadyInd)
      End Get
    End Property

    Public Shared GraphicsSuppliersProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GraphicsSuppliers, "Graphics")
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Graphics", Description:="")>
    Public ReadOnly Property GraphicsSuppliers() As String
      Get
        Return GetGraphicsSuppliers()
      End Get
    End Property

    Public Shared OBFacilitySuppliersProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OBFacilitySuppliers, "OB Facility")
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="OB Facility", Description:="")>
    Public ReadOnly Property OBFacilitySuppliers() As String
      Get
        Return GetOBFacilitySuppliers()
      End Get
    End Property

    Public ReadOnly Property CanViewOBFacilitySuppliersInd() As Boolean
      Get
        If ProductionSystemAreaList.Count > 0 Then
          Return (ProductionSystemAreaList(0).SystemID = OBLib.CommonData.Enums.System.ProductionContent)
        End If
        Return False
      End Get
    End Property

    Public ReadOnly Property CanFinaliseTimeline As Boolean
      Get
        Return Singular.Security.HasAccess("Productions", "Can Finalise Timeline")
      End Get
    End Property

    Public ReadOnly Property CanUnFinaliseTimeline As Boolean
      Get
        Return Singular.Security.HasAccess("Productions", "Can Unfinalise Timeline")
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public ReadOnly Property IsTimelineReadyFunctionalityEnabled
      Get
        Return OBLib.CommonData.TimelineReadyEnabled
      End Get
    End Property

    Public ReadOnly Property IsTimelineReady As Boolean
      Get
        If IsTimelineReadyFunctionalityEnabled Then
          Return IsTimelineReady
        Else
          Return True
        End If
      End Get
    End Property

    Public Shared ScheduleTimeValidityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ScheduleTimeValidity, "Validate Schedule times", "")
    ''' <summary>
    ''' Gets and sets the Spec Name value
    ''' </summary>
    <Display(Name:="Validate Schedule", Description:="")>
    Public Property ScheduleTimeValidity() As String
      Get
        Return GetProperty(ScheduleTimeValidityProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ScheduleTimeValidityProperty, Value)
      End Set
    End Property

    Public Property CanEditSpec As Boolean
      Get
        Return Singular.Security.HasAccess("Production System Areas.Can Edit Spec")
      End Get
      Set(value As Boolean)

      End Set
    End Property

    Public Shared CancelledTravelHRNamesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledTravelHRNames, "CancelledTravelHRNames", "")
    ''' <summary>
    ''' Gets and sets the Spec Name value
    ''' </summary>
    <Display(Name:="CancelledTravelHRNames", Description:="")>
    Public Property CancelledTravelHRNames() As String
      Get
        Return GetProperty(CancelledTravelHRNamesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledTravelHRNamesProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionSystemAreaListProperty As PropertyInfo(Of ProductionSystemAreaList) = RegisterProperty(Of ProductionSystemAreaList)(Function(c) c.ProductionSystemAreaList, "Production System Area List")
    Public ReadOnly Property ProductionSystemAreaList() As ProductionSystemAreaList
      Get
        If GetProperty(ProductionSystemAreaListProperty) Is Nothing Then
          LoadProperty(ProductionSystemAreaListProperty, Productions.Areas.ProductionSystemAreaList.NewProductionSystemAreaList())
        End If
        Return GetProperty(ProductionSystemAreaListProperty)
      End Get
    End Property

    Public Shared ProductionVehicleListProperty As PropertyInfo(Of ProductionVehicleList) = RegisterProperty(Of ProductionVehicleList)(Function(c) c.ProductionVehicleList, "Production Vehicle List")
    Public ReadOnly Property ProductionVehicleList() As ProductionVehicleList
      Get
        If GetProperty(ProductionVehicleListProperty) Is Nothing Then
          LoadProperty(ProductionVehicleListProperty, Productions.Vehicles.ProductionVehicleList.NewProductionVehicleList())
        End If
        Return GetProperty(ProductionVehicleListProperty)
      End Get
    End Property

    Public Shared ProductionDocumentListProperty As PropertyInfo(Of ProductionDocumentList) = RegisterProperty(Of ProductionDocumentList)(Function(c) c.ProductionDocumentList, "Production Document List")
    Public ReadOnly Property ProductionDocumentList() As ProductionDocumentList
      Get
        If GetProperty(ProductionDocumentListProperty) Is Nothing Then
          LoadProperty(ProductionDocumentListProperty, Productions.Correspondence.ProductionDocumentList.NewProductionDocumentList)
        End If
        Return GetProperty(ProductionDocumentListProperty)
      End Get
    End Property

    Public Shared ProductionGeneralCommentListProperty As PropertyInfo(Of ProductionGeneralCommentList) = RegisterProperty(Of ProductionGeneralCommentList)(Function(c) c.ProductionGeneralCommentList, "Production General Comment List")
    Public ReadOnly Property ProductionGeneralCommentList() As ProductionGeneralCommentList
      Get
        If GetProperty(ProductionGeneralCommentListProperty) Is Nothing Then
          LoadProperty(ProductionGeneralCommentListProperty, Productions.Correspondence.ProductionGeneralCommentList.NewProductionGeneralCommentList)
        End If
        Return GetProperty(ProductionGeneralCommentListProperty)
      End Get
    End Property

    Public Shared ProductionPettyCashListProperty As PropertyInfo(Of ProductionPettyCashList) = RegisterProperty(Of ProductionPettyCashList)(Function(c) c.ProductionPettyCashList, "Production Petty Cash List")
    Public ReadOnly Property ProductionPettyCashList() As ProductionPettyCashList
      Get
        If GetProperty(ProductionPettyCashListProperty) Is Nothing Then
          LoadProperty(ProductionPettyCashListProperty, Productions.Correspondence.ProductionPettyCashList.NewProductionPettyCashList)
        End If
        Return GetProperty(ProductionPettyCashListProperty)
      End Get
    End Property

    Public Shared ProductionCommentSectionListProperty As PropertyInfo(Of ProductionCommentSectionList) = RegisterProperty(Of ProductionCommentSectionList)(Function(c) c.ProductionCommentSectionList, "Production Petty Cash List")
    Public ReadOnly Property ProductionCommentSectionList() As ProductionCommentSectionList
      Get
        If GetProperty(ProductionCommentSectionListProperty) Is Nothing Then
          LoadProperty(ProductionCommentSectionListProperty, Productions.ProductionCommentSectionList.NewProductionCommentSectionList)
        End If
        Return GetProperty(ProductionCommentSectionListProperty)
      End Get
    End Property

    Public Shared TravelRequisitionListProperty As PropertyInfo(Of OBLib.Travel.TravelRequisitionList) = RegisterProperty(Of OBLib.Travel.TravelRequisitionList)(Function(c) c.TravelRequisitionList, "Travel Requisition List")
    Public ReadOnly Property TravelRequisitionList() As OBLib.Travel.TravelRequisitionList
      Get
        If GetProperty(TravelRequisitionListProperty) Is Nothing Then
          LoadProperty(TravelRequisitionListProperty, OBLib.Travel.TravelRequisitionList.NewTravelRequisitionList)
        End If
        Return GetProperty(TravelRequisitionListProperty)
      End Get
    End Property

    Public Shared ProductionAudioConfigurationListProperty As PropertyInfo(Of ProductionAudioConfigurationList) = RegisterProperty(Of ProductionAudioConfigurationList)(Function(c) c.ProductionAudioConfigurationList, "Production Petty Cash List")
    Public ReadOnly Property ProductionAudioConfigurationList() As ProductionAudioConfigurationList
      Get
        If GetProperty(ProductionAudioConfigurationListProperty) Is Nothing Then
          LoadProperty(ProductionAudioConfigurationListProperty, Productions.ProductionAudioConfigurationList.NewProductionAudioConfigurationList)
        End If
        Return GetProperty(ProductionAudioConfigurationListProperty)
      End Get
    End Property

    Public Shared EmailListProperty As PropertyInfo(Of Singular.Emails.EmailList) = RegisterProperty(Of Singular.Emails.EmailList)(Function(c) c.EmailList, "Email List")
    <Browsable(False)>
    Public ReadOnly Property EmailList() As Singular.Emails.EmailList
      Get
        If GetProperty(EmailListProperty) Is Nothing Then
          LoadProperty(EmailListProperty, Singular.Emails.EmailList.NewEmailList)
        End If
        Return GetProperty(EmailListProperty)
      End Get
    End Property

#End Region

#Region " Methods"

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Title.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production R")
        Else
          Return String.Format("Blank {0}", "Production R")
        End If
      Else
        Return Me.Title
      End If

    End Function

    Public Function GetGraphicsSuppliers() As String
      Dim GraphicsSupplier As String = ""
      If Me.ProductionSystemAreaList.Count > 0 Then
        For Each ProductionSystemArea As ProductionSystemArea In ProductionSystemAreaList
          If ProductionSystemArea.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
            If ProductionSystemArea.ProductionOutsourceServiceList.Count > 0 Then
              For Each ProductionOutsourceService As ProductionOutsourceService In ProductionSystemArea.ProductionOutsourceServiceList
                If ProductionOutsourceService.OutsourceServiceTypeID = CType(OBLib.CommonData.Enums.OutsourceServiceType.Graphics, Integer) Then
                  Dim Supplier As ROSupplier = CommonData.Lists.ROSupplierList.GetItem(ProductionOutsourceService.SupplierID)
                  GraphicsSupplier = GraphicsSupplier & Supplier.Supplier & ", "
                End If
              Next
            End If
          End If
        Next
      End If
      Return GraphicsSupplier
    End Function

    Public Function GetOBFacilitySuppliers() As String
      Dim OBFacilitySupplier As String = ""
      If Me.ProductionSystemAreaList.Count > 0 Then
        For Each ProductionSystemArea As ProductionSystemArea In ProductionSystemAreaList
          If ProductionSystemArea.SystemID = CType(OBLib.CommonData.Enums.System.ProductionContent, Integer) Then
            If ProductionSystemArea.ProductionOutsourceServiceList.Count > 0 Then
              For Each ProductionOutsourceService As ProductionOutsourceService In ProductionSystemArea.ProductionOutsourceServiceList
                If ProductionOutsourceService.OutsourceServiceTypeID = CType(OBLib.CommonData.Enums.OutsourceServiceType.OBFacility, Integer) Then
                  'If ProductionOutsourceService.SupplierID = CType(OBLib.CommonData.Enums.OutsourceServiceSuppliers.VisionView, Integer) Then
                  Dim Supplier As ROSupplier = CommonData.Lists.ROSupplierList.GetItem(ProductionOutsourceService.SupplierID)
                  OBFacilitySupplier = OBFacilitySupplier & Supplier.Supplier & ", "
                  'Else
                  'End If
                End If
              Next
            End If
          End If
        Next
      End If
      If OBFacilitySupplier = "" Then
        OBFacilitySupplier = "SS OB"
      End If
      Return OBFacilitySupplier
    End Function


    Public Sub AutoAddTemplateCommentItems()

      For Each cd As String In ProductionCommentDefaults.ClientDetails
        ProductionCommentSectionList.AddComment(CInt(CommonData.Enums.ProductionCommentReportSection.ClientDetails), cd)
      Next

      ProductionCommentSectionList.AddComment(CInt(CommonData.Enums.ProductionCommentReportSection.ProductionDetails), ProductionCommentDefaults.ProductionDetails.OBTelephoneNumbersHeader, ProductionCommentDefaults.ProductionDetails.OBTelephoneNumbersContent)
      ProductionCommentSectionList.AddComment(CInt(CommonData.Enums.ProductionCommentReportSection.ProductionDetails), ProductionCommentDefaults.ProductionDetails.TapeStockHeader, ProductionCommentDefaults.ProductionDetails.TapeStockContent)
      ProductionCommentSectionList.AddComment(CInt(CommonData.Enums.ProductionCommentReportSection.ProductionDetails), ProductionCommentDefaults.ProductionDetails.DressCodeHeader, ProductionCommentDefaults.ProductionDetails.DressCodeContent)
      ProductionCommentSectionList.AddComment(CInt(CommonData.Enums.ProductionCommentReportSection.Equipment), ProductionCommentDefaults.Equipment.SpecialArrangementsHeader, ProductionCommentDefaults.Equipment.SpecialArrangementsContent)


    End Sub

#Region " Areas "

    Public Function AddNewSystemArea(SystemID As Integer?, ProductionAreaID As Integer?) As ProductionSystemArea

      Dim NewArea As OBLib.Productions.Areas.ProductionSystemArea = Me.ProductionSystemAreaList.AddNew()
      NewArea.SystemID = SystemID
      NewArea.ProductionAreaID = ProductionAreaID
      NewArea.ProductionAreaStatusID = CType(OBLib.CommonData.Enums.ProductionAreaStatus.New, Integer)
      NewArea.StatusDate = Now

      'If an OB for Production Services, then add an event manager
      If CompareSafe(NewArea.SystemID, CType(CommonData.Enums.System.ProductionServices, Integer)) And CompareSafe(NewArea.ProductionAreaID, CType(CommonData.Enums.ProductionArea.OB, Integer)) Then
        NewArea.ProductionHumanResourceList.AddEventManager()
        'NewArea.ProductionTimelineList.AddTx(SystemID, ProductionAreaID)
      End If

      Return NewArea

    End Function

    Public Function GetTxTimes() As String

      Dim txStart = Me.ProductionSystemAreaList(0).ProductionTimelineList.Where(Function(x) Singular.Misc.CompareSafe(x.ProductionTimelineTypeID, CInt(CommonData.Enums.ProductionTimelineType.Transmission))).OrderBy(Function(y) y.StartDateTime).FirstOrDefault
      If txStart IsNot Nothing Then
        If Singular.Misc.IsNullNothing(txStart.StartDateTime) Then
          Return Now.ToString("dd-MMM-yy HH:mm")
        Else
          Return (CType(txStart.StartDateTime, Date).ToString("dd-MMM-yy")) & ": " & (CType(txStart.StartDateTime, Date).ToString("HH:mm")) & " - " & (CType(txStart.EndDateTime, Date).ToString("HH:mm"))
        End If
      Else
        Return Now.ToString("dd-MMM-yy HH:mm")
      End If

    End Function

    Public Function GetTxStartTime() As DateTime?

      Dim txStart = Me.ProductionSystemAreaList(0).ProductionTimelineList.Where(Function(x) Singular.Misc.CompareSafe(x.ProductionTimelineTypeID, CInt(CommonData.Enums.ProductionTimelineType.Transmission))).OrderBy(Function(y) y.StartDateTime).FirstOrDefault
      If txStart IsNot Nothing Then

        Return txStart.StartDateTime
      End If

    End Function

    Public Sub CalculateProductionHR(RegenerateCrewSchedules As Boolean)
      'RegenerateCrewSchedules
      Me.ProductionSystemAreaList(0).CalculateProductionHR()
    End Sub

#End Region

#Region " Main "

    'Client Side
    'Public Function HasSnTDocuments() As Boolean

    '  'DO A COMMAND PROC CALL

    '  Dim SAndTDocCount As Integer = 0
    '  If HasSnT Then
    '    For Each document In Me.ProductionDocumentList
    '      If document.DocumentTypeID = 16 Then
    '        SAndTDocCount += 1
    '      End If
    '    Next
    '  End If

    '  If HasSnT Then
    '    If SAndTDocCount = 0 Then
    '      Return False
    '    End If
    '  Else
    '    Return True
    '  End If

    '  Return False

    'End Function

    'Public Function HasPAForms() As Boolean

    '  Dim SPAFormsCount As Integer = 0
    '  For Each document In Me.ProductionDocumentList
    '    If document.DocumentTypeID = 6 Then
    '      SPAFormsCount += 1
    '    End If
    '  Next

    '  If SPAFormsCount > 1 Then
    '    Return True
    '  Else
    '    Return False
    '  End If

    'End Function

    'Private Function CanUnReconcileToday() As Boolean

    '  If Singular.Security.HasAccess("Productions", "Un-reconciled productions today") Then
    '    If Not ReconciledSetDate Is Nothing AndAlso ReconciledSetDate = Now.Date Then
    '      Return True
    '    End If
    '  End If
    '  Return False

    'End Function

    'Public Function CheckCanReconcile() As String

    '  Dim CannotReconcileReason As String = ""

    '  If Not HasSnTDocuments() Then
    '    CannotReconcileReason &= "S&T Document required <br/>"
    '  End If

    '  If Not HasPAForms() Then
    '    CannotReconcileReason &= "PA Forms required <br/>"
    '  End If

    '  If Not Singular.Security.HasAccess("Productions", "Reconcile Production") Then
    '    CannotReconcileReason &= "You are not authorised to Reconcile this production <br/>"
    '  End If

    '  Me.CannotReconcile = CannotReconcileReason

    '  Return CannotReconcileReason

    'End Function

    'Public Function CheckCanUnReconcile() As String

    '  Dim CannotUnReconcileReason As String = ""

    '  Dim CurrentUserIsEventManager As Boolean = False
    '  Dim EM As List(Of ProductionHumanResource) = Me.ProductionSystemAreaList(0).ProductionHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, OBLib.Security.Settings.CurrentUser.HumanResourceID) And CompareSafe(d.DisciplineID, CType(CommonData.Enums.Discipline.EventManager, Integer))).ToList
    '  If EM.Count > 0 Then
    '    CurrentUserIsEventManager = True
    '  End If

    '  If CurrentUserIsEventManager Then
    '    If CanUnReconcileToday() Then
    '      CannotUnReconcileReason &= "You are not allowed to un-reconcile this production <br/>"
    '    End If
    '  Else
    '    If Not Singular.Security.HasAccess("Productions", "Un-reconciled productions") Then
    '      CannotUnReconcileReason &= "You are not authorised to un-reconcile this production <br/>"
    '    End If
    '  End If

    '  CannotUnReconcile = CannotUnReconcileReason

    '  Return CannotUnReconcileReason

    'End Function

    'Private Sub PopulateROEquipment()

    '  Dim VehicleIDsXML As String = ""
    '  Dim EquipmentIDsXML As String = ""
    '  VehicleIDsXML = mProductionVehicleList.GetXMLCriteriaString("VehicleID")
    '  EquipmentIDsXML = mProductionEquipmentList.GetXMLCriteriaString("EquipmentID")
    '  If mProductionVehicleList.Count = 0 AndAlso mProductionEquipmentList.Count = 0 Then
    '    mROProductionEquipmentList = [ReadOnly].ROProductionEquipmentList.NewROProductionEquipmentList
    '  Else
    '    mROProductionEquipmentList = [ReadOnly].ROProductionEquipmentList.GetROProductionEquipmentList(VehicleIDsXML, EquipmentIDsXML)
    '  End If

    'End Sub

#End Region

#Region " Vehicles "

    Public Sub RemoveVehicle(VehicleID As Integer?)

      Dim Vehicle As ProductionVehicle = Me.ProductionVehicleList.GetItemByVehicleID(VehicleID)

      If CommonData.Lists.ROVehicleList.GetItem(VehicleID).VehicleTypeID = CType(CommonData.Enums.VehicleType.OBVan, Integer) Then
        Dim v As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleFull = CommonData.Lists.ROVehicleFullList.GetItem(VehicleID)
        RemoveHumanResourcesForVehicle(v)
      End If

      RemoveHumanTimelinesForVehicle(VehicleID)
      Me.ProductionVehicleList.Remove(Vehicle)
      'Me.ProductionSystemAreaList(0).CheckAllClashes()
      'Me.ProductionSystemAreaList(0).CheckScheduleValidOld(Nothing)

    End Sub

    Public Sub RemoveHumanResourcesForVehicle(ByVal Vehicle As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleFull)

      Dim ProductionHumanResourcesToRemove As New List(Of ProductionHumanResource)
      Dim ProductionHumanResourcesToReset As New List(Of ProductionHumanResource)

      For Each vhr As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleHumanResource In Vehicle.ROVehicleHumanResourceList
        Dim cvhr As OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleHumanResource = vhr
        For Each phr As ProductionHumanResource In Me.ProductionSystemAreaList(0).ProductionHumanResourceList
          If Singular.Misc.CompareSafe(cvhr.DisciplineID, phr.DisciplineID) AndAlso Singular.Misc.CompareSafe(cvhr.HumanResourceID, phr.HumanResourceID) Then
            ProductionHumanResourcesToRemove.Add(phr)
          ElseIf Singular.Misc.CompareSafe(cvhr.HumanResourceID, phr.HumanResourceID) Then
            ProductionHumanResourcesToReset.Add(phr)
          End If
        Next
      Next

      ProductionHumanResourcesToRemove.ForEach(Sub(phr)
                                                 Me.ProductionSystemAreaList(0).DeleteProductionHumanResource(phr)
                                               End Sub)

      ProductionHumanResourcesToReset.ForEach(Sub(phr)
                                                Me.ProductionSystemAreaList(0).ClearHumanResourceID(phr)
                                                phr.VehicleID = Nothing
                                                phr.VehicleName = ""
                                              End Sub)

    End Sub

    Public Sub RemoveHumanTimelinesForVehicle(ByVal VehicleID As Integer?)
      Me.ProductionSystemAreaList(0).DeleteVehiclePreTravelItems(VehicleID)
      Me.ProductionSystemAreaList(0).DeleteVehiclePostTravelItems(VehicleID)
    End Sub

    Public Sub GetMinMaxTimesForVehicle(ByVal iVehicleID As Integer, ByVal iProductionTimelineTypeID As CommonData.Enums.ProductionTimelineType,
                                        ByRef StartDateTime As DateTime?, ByRef EndDateTime As DateTime?)

      Dim query1 = _
      From i In (From pt As ProductionTimeline In ProductionSystemAreaList(0).ProductionTimelineList
                  Join ptt As OBLib.Maintenance.Productions.ReadOnly.ROProductionTimelineType In CommonData.Lists.ROProductionTimelineTypeList On pt.ProductionTimelineTypeID Equals ptt.ProductionTimelineTypeID
                  Join pv As ProductionVehicle In ProductionVehicleList On pv.ProductionID Equals pt.ProductionID And If(IsNullNothing(pv.VehicleID, True), 0, pv.VehicleID) Equals If(IsNullNothing(pt.VehicleID, True), 0, pt.VehicleID)
                  Where pt.IsValid And Singular.Misc.CompareSafe(pv.VehicleID, iVehicleID) AndAlso
                          Singular.Misc.CompareSafe(pt.ProductionTimelineTypeID, CType(iProductionTimelineTypeID, Integer)) AndAlso
                          Not ptt.FreeInd
                  Group By pv.VehicleID
                  Into MinStartDate = Min(CDate(pt.StartDateTime)), MaxEndDate = Max(CDate(pt.EndDateTime))
                  Select MinStartDate, MaxEndDate)
                  Select New With {Key .StartDateTime = i.MinStartDate, _
                                   Key .EndDateTime = i.MaxEndDate}

      'where.... andalso not ptt.FreeInd
      If query1.Count > 0 Then
        For Each item In query1
          If Not StartDateTime.HasValue OrElse StartDateTime > item.StartDateTime Then StartDateTime = item.StartDateTime
          If Not EndDateTime.HasValue OrElse EndDateTime < item.EndDateTime Then EndDateTime = item.EndDateTime
        Next
      Else

        Dim query2 = _
        From i In (From pt In ProductionSystemAreaList(0).ProductionTimelineList
                    Join ptt In CommonData.Lists.ROProductionTimelineTypeList On pt.ProductionTimelineTypeID Equals ptt.ProductionTimelineTypeID
                    Join pv As ProductionVehicle In ProductionVehicleList On pv.ProductionID Equals pt.ProductionID
                    Where pt.IsValid And Singular.Misc.CompareSafe(If(IsNullNothing(pv.VehicleID, True), 0, pv.VehicleID), If(IsNullNothing(iVehicleID, True), 0, iVehicleID)) AndAlso Singular.Misc.CompareSafe(ptt.ProductionTimelineTypeID, CType(iProductionTimelineTypeID, Integer)) AndAlso
                          Not ptt.FreeInd AndAlso
                          Singular.Misc.IsNullNothing(pt.VehicleID)
                    Group By pv.VehicleID
                    Into MinStartDate = Min(pt.StartDateTime), MaxEndDate = Max(pt.EndDateTime)
                    Select VehicleID, MinStartDate, MaxEndDate)
                  Select New With {Key .StartDateTime = i.MinStartDate, _
                                   Key .EndDateTime = i.MaxEndDate, _
                                   Key .VehicleID = i.VehicleID}

        For Each item In query2

          If Not StartDateTime.HasValue OrElse StartDateTime > item.StartDateTime Then
            StartDateTime = item.StartDateTime
          End If

          If Not EndDateTime.HasValue OrElse EndDateTime < item.EndDateTime Then
            EndDateTime = item.EndDateTime
          End If

        Next
      End If

    End Sub

    Public Sub CheckVehicleClashes()
      For Each PV As ProductionVehicle In ProductionVehicleList
        PV.CheckClash()
      Next
    End Sub

#End Region

#Region " Travel "

    Public Sub AddNewTravelRequisition(SystemID As Integer?)

      Dim NewTR As OBLib.Travel.TravelRequisition = Me.TravelRequisitionList.AddNew()
      NewTR.SystemID = SystemID
      NewTR.VersionNo = 1

    End Sub

    Public Function CanOpenTravelPage() As String

      Dim Message As String = ""

      Dim TransmissionCount As Integer = Me.ProductionSystemAreaList(0).ProductionTimelineList.Where(Function(tm) tm.ProductionTimelineTypeID IsNot Nothing AndAlso tm.ProductionTimelineTypeID = CInt(CommonData.Enums.ProductionTimelineType.Transmission)).Count
      'if the total timeline count is = transmission count, then only transmission times have been specified, and this is not allow because it causes clashing conflicts
      If TransmissionCount = Me.ProductionSystemAreaList(0).ProductionTimelineList.Count Then
        Message = Message & "There are only transmission times are currently specified in the timeline." & "</br>"
      End If

      'determine how many people have 0 production days
      Dim HrNames As List(Of String) = New List(Of String)
      For Each HRProductionSchedule As Productions.Schedules.HRProductionSchedule In Me.ProductionSystemAreaList(0).HRProductionScheduleList
        If HRProductionSchedule.HRProductionScheduleDetailList.Count = 0 Then
          HrNames.Add(HRProductionSchedule.HumanResource)
        End If
      Next

      If HrNames.Count > 0 Then
        Message += "The following people do not have a crew schedule:" & "</br>"
        For Each HR As String In HrNames
          Message += HR & "</br>"
        Next
      End If

      Return Message

    End Function

#End Region

#Region " Emails "

    Private Function GetEventManagerName() As String
      Dim EventManagers As String = ""

      For Each phr As ProductionHumanResource In Me.ProductionSystemAreaList(0).ProductionHumanResourceList.Where(Function(d) CompareSafe(d.DisciplineID, CType(CommonData.Enums.Discipline.EventManager, Integer)) _
                                                                                                                  OrElse CompareSafe(d.DisciplineID, CType(CommonData.Enums.Discipline.EventManagerIntern, Integer)))
        If phr.HumanResourceID IsNot Nothing Then
          Dim ROHR As OBLib.HR.ReadOnly.ROHumanResource = CommonData.Lists.ROHumanResourceList.GetItem(phr.HumanResourceID)
          If ROHR IsNot Nothing Then
            If EventManagers = "" Then
              EventManagers += ROHR.PreferredFirstSurname
            Else
              EventManagers += " " & ROHR.PreferredFirstSurname
            End If
          End If
        End If
      Next
      Return EventManagers
    End Function

    Private Sub SetupEmails()

      Dim EventManagers As String = ""

      'Production Un-Reconciled Email
      If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CType(CommonData.Enums.System.ProductionServices, Integer)) AndAlso OriginalReconciled AndAlso ReconciledDate Is Nothing Then
        If EventManagers = "" Then
          EventManagers = GetEventManagerName()
        End If

        'For Each phr As ProductionHumanResource In Me.ProductionSystemAreaList(0).ProductionHumanResourceList.Where(Function(d) CompareSafe(d.DisciplineID, CType(CommonData.Enums.Discipline.EventManager, Integer)))
        '  If phr.HumanResourceID IsNot Nothing Then
        '    Dim ROHR As OBLib.HR.ReadOnly.ROHumanResource = CommonData.Lists.ROHumanResourceList.GetItem(phr.HumanResourceID)
        '    If ROHR IsNot Nothing Then
        '      If EventManagers = "" Then
        '        EventManagers += ROHR.PreferredFirstSurname
        '      Else
        '        EventManagers += " " & ROHR.PreferredFirstSurname
        '      End If
        '    End If
        '  End If
        'Next
        Dim UnReconciledProduction As OBLib.Emails.UnReconciledProductions = New OBLib.Emails.UnReconciledProductions(Me, OBLib.Security.Settings.CurrentUser, OBLib.Security.Settings.CurrentUser.SystemID, EventManagers)
        Me.EmailList.Add(UnReconciledProduction.Email)
      End If

      If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CType(CommonData.Enums.System.ProductionServices, Integer)) AndAlso LoadedCrewFinalised AndAlso CrewPlanningFinalisedDate Is Nothing Then
        If EventManagers = "" Then
          EventManagers = GetEventManagerName()
        End If

        Dim UnReconciledProduction As OBLib.Emails.UnCrewFinalisedProductions = New OBLib.Emails.UnCrewFinalisedProductions(Me, OBLib.Security.Settings.CurrentUser, OBLib.Security.Settings.CurrentUser.SystemID, EventManagers)
        Me.EmailList.Add(UnReconciledProduction.Email)

      End If

      'If Not Me.IsNew Then
      '  If OBLib.Helpers.Production.MainDetailsChanged(LoadedProductionTypeID, ProductionTypeID,
      '                                                 LoadedEventTypeID, EventTypeID,
      '                                                 LoadedProductionVenueID, ProductionVenueID,
      '                                                 LoadedTeamsPlaying, TeamsPlaying,
      '                                                 LoadedPlayStartDateTime, PlayStartDateTime,
      '                                                 LoadedPlayEndDateTime, PlayEndDateTime,
      '                                                 LoadedTitle, Title) Then
      '    Dim PDChangedEmail As OBLib.Emails.ProductionDetailsChangedEmail = New OBLib.Emails.ProductionDetailsChangedEmail(LoadedProductionTypeID, ProductionTypeID,
      '                                                                                                                      LoadedEventTypeID, EventTypeID,
      '                                                                                                                      LoadedProductionVenueID, ProductionVenueID,
      '                                                                                                                      LoadedTeamsPlaying, TeamsPlaying,
      '                                                                                                                      LoadedPlayStartDateTime, PlayStartDateTime,
      '                                                                                                                      LoadedPlayEndDateTime, PlayEndDateTime, OBLib.Security.Settings.CurrentUserID,
      '                                                                                                                      LoadedTitle, Title)
      '    Me.EmailList.Add(PDChangedEmail.RCEmail)
      '  End If
      'End If

    End Sub

#End Region

#Region " Post Save Functionality "

    Public Sub DoPostSave()

      'If anyone has been removed

      'Cancel Travel (new method)
      CancelTravelForRemovedHumanResources()

      ''Cancel Travel (old method)
      'If Me.ProductionSystemAreaList(0).RemovedHumanResourceList.Count > 0 Then
      '  Dim RemovedTravelEmail As Singular.Emails.Email = CheckTravelForRemovedHumanResources()
      '  If RemovedTravelEmail IsNot Nothing Then
      '    Me.EmailList.Add(RemovedTravelEmail)
      '    Me.EmailList.Save()
      '    Me.EmailList.Clear()
      '  End If
      '  RemoveHumanResourceTravel()
      'End If
      Me.ProductionSystemAreaList(0).RemovedHumanResourceList.Clear()

      'If anyone has been changed after crew finalised
      If Me.ProductionSystemAreaList(0).RecordChangeList.Count > 0 Then
        Dim RecordChangeEmail As Singular.Emails.Email = New OBLib.Emails.ChangedProductionHumanResources(Me, OBLib.Security.Settings.CurrentUser.SystemID).Email
        If RecordChangeEmail IsNot Nothing Then
          Me.EmailList.Add(RecordChangeEmail)
          Me.EmailList.Save()
          Me.EmailList.Clear()
        End If
      End If
      Me.ProductionSystemAreaList(0).RecordChangeList.Clear()

    End Sub

    'Public Function CheckTravelForRemovedHumanResources() As Singular.Emails.Email

    '  Try
    '    Dim cmd As New Singular.CommandProc("CmdProcs.cmdGetTravelAndProductionHumanResources",
    '                                New String() {"ProductionID",
    '                                              "SystemID",
    '                                              "ProductionAreaID"},
    '                                New Object() {Me.ProductionID,
    '                                              OBLib.Security.Settings.CurrentUser.SystemID,
    '                                              CType(OBLib.CommonData.Enums.ProductionArea.OB, Integer)})

    '    cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
    '    cmd = cmd.Execute(0)

    '    'No Travel book
    '    If cmd.Dataset.Tables(0).Rows.Count > 0 Then
    '      Dim TravelHRIDs As List(Of Integer) = New List(Of Integer)
    '      Dim ProductionHRIDs As List(Of Integer) = New List(Of Integer)
    '      Dim UnmatchedHRIDs As List(Of String) = New List(Of String)
    '      'Travel
    '      For Each Row As DataRow In cmd.Dataset.Tables(0).Rows
    '        TravelHRIDs.Add(Row("HumanResourceID"))
    '      Next
    '      'Production
    '      For Each Row As DataRow In cmd.Dataset.Tables(1).Rows
    '        ProductionHRIDs.Add(Row("HumanResourceID"))
    '      Next
    '      'Match Travel HR to PHR
    '      TravelHRIDs.ForEach(Sub(z)
    '                            If Not ProductionHRIDs.Contains(z) Then
    '                              UnmatchedHRIDs.Add(z.ToString)
    '                            End If
    '                          End Sub)
    '      'If there is Travel booked for deleted Production Human Resources
    '      If UnmatchedHRIDs.Count > 0 Then
    '        Dim EmailList As Singular.Emails.EmailList = Singular.Emails.EmailList.NewEmailList()
    '        Dim emr As OBLib.Emails.RemovedProductionHumanResources = New OBLib.Emails.RemovedProductionHumanResources(Me.ProductionID, OBLib.Security.Settings.CurrentUser.SystemID, OBLib.Helpers.MiscHelper.StringArrayToXML(UnmatchedHRIDs.ToArray), OBLib.Security.Settings.CurrentUser.ProductionAreaID)
    '        Dim em As Singular.Emails.Email = emr.Email
    '        Return em
    '      End If
    '    End If
    '  Catch ex As Exception

    '  End Try

    '  Return Nothing

    'End Function

    Public Sub CancelTravelForRemovedHumanResources()
      Try
        Dim cmd As New Singular.CommandProc("CmdProcs.cmdCancelTravelBookings",
                                            New String() {"@ProductionSystemAreaID", "@LoggedInUserID"},
                                            New Object() {Me.ProductionSystemAreaList(0).ProductionSystemAreaID, OBLib.Security.Settings.CurrentUser.UserID})

        cmd.UseTransaction = True
        cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
        cmd = cmd.Execute(0)
        Dim ds As DataSet = cmd.Dataset
        Dim tbl As DataTable = ds.Tables(0)
        Me.CancelledTravelHRNames = ""
        For Each dr As DataRow In tbl.Rows
          Me.CancelledTravelHRNames = Me.CancelledTravelHRNames & dr(0) & ", "
        Next
        Dim x As Object = Nothing
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, "Production.vb (Old)", "CancelTravelForRemovedHumanResources - PSAID - " & Me.ProductionSystemAreaList(0).ProductionSystemAreaID.ToString, ex.Message)
      End Try
    End Sub

    'Public Sub RemoveHumanResourceTravel()

    '  If Me.ProductionSystemAreaList(0).RemovedHumanResourceList.Count > 0 Then
    '    Dim HRIDList As List(Of String) = New List(Of String)
    '    For Each phrh As ProductionHumanResource In Me.ProductionSystemAreaList(0).RemovedHumanResourceList
    '      Dim PreferredHRID As Integer? = Nothing
    '      'In the case then the Production Human Resource record is deleted, won't find it in ProductionHumanResourceList
    '      Dim cphr As ProductionHumanResource = Me.ProductionSystemAreaList(0).ProductionHumanResourceList.GetItem(phrh.ProductionHumanResourceID)
    '      If cphr IsNot Nothing Then
    '        Dim tmp As ProductionHumanResource = Me.ProductionSystemAreaList(0).ProductionHumanResourceList.GetItem(phrh.ProductionHumanResourceID)
    '        If tmp.PreferredHumanResourceID IsNot Nothing Then
    '          PreferredHRID = tmp.PreferredHumanResourceID
    '        End If
    '      End If
    '      Dim CompareHRID As Integer? = phrh.LoadedHumanResourceID
    '      If Me.ProductionSystemAreaList(0).ProductionHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, CompareHRID)).Count = 0 Then
    '        If IsNullNothing(PreferredHRID) Then
    '          HRIDList.Add(phrh.LoadedHumanResourceID.ToString)
    '        ElseIf phrh.LoadedHumanResourceID <> PreferredHRID Then
    '          HRIDList.Add(phrh.HumanResourceID.ToString)
    '        End If
    '      End If
    '    Next

    '    If HRIDList.Count > 0 Then
    '      Dim HRIDs As String = OBLib.Helpers.MiscHelper.StringArrayToXML(HRIDList.ToArray)
    '      Dim cmd As New Singular.CommandProc("CmdProcs.cmdCancelHumanResourceTravel", _
    '                                  New String() {"HumanResourceIDs",
    '                                                "ProductionID",
    '                                                "UserID",
    '                                                "SystemID",
    '                                                "ProductionAreaID"}, _
    '                                  New Object() {HRIDs,
    '                                                Me.ProductionID,
    '                                                Settings.CurrentUser.UserID,
    '                                                OBLib.Security.Settings.CurrentUser.SystemID,
    '                                                CType(OBLib.CommonData.Enums.ProductionArea.OB, Integer)})
    '      cmd.FetchType = Singular.CommandProc.FetchTypes.DataSet
    '      cmd = cmd.Execute(0)
    '      If cmd.Dataset.Tables(0).Rows.Count > 0 Then
    '        Dim HRName As String = ""
    '        For Each Row As DataRow In cmd.Dataset.Tables(0).Rows
    '          If HRName = "" Then
    '            HRName += Row("HumanResource")
    '          Else
    '            HRName += ", " + Row("HumanResource")
    '          End If
    '        Next
    '      End If
    '    End If
    '  End If

    'End Sub

#End Region

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      AddWebRule(CannotReconcileReasonProperty,
                 Function(d) d.CannotReconcileReason <> "",
                 Function(d) d.CannotReconcileReason)

      AddWebRule(CannotUnReconcileReasonProperty,
                Function(d) d.CannotUnReconcileReason <> "",
                Function(d) d.CannotUnReconcileReason)

      AddWebRule(CancelledReasonProperty,
                 Function(d) d.Cancelled And d.CancelledReason = "",
                 Function(d) "Cancelled reason is required")

      With AddWebRule(ScheduleTimeValidityProperty,
          Function(d) d.ScheduleTimeValidity <> "",
          Function(d) d.ScheduleTimeValidity)
        .Severity = Singular.Rules.RuleSeverity.Warning
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods"

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProduction() method.

    End Sub

    Public Shared Function NewProduction() As Production

      Return DataPortal.CreateChild(Of Production)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProduction(dr As SafeDataReader) As Production

      Dim p As New Production()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionIDProperty, .GetInt32(0))
          LoadProperty(ProductionDescriptionProperty, .GetString(1))
          LoadProperty(TeamsPlayingProperty, .GetString(2))
          LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ProductionSpecRequirementIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(VenueConfirmedDateProperty, .GetValue(7))
          LoadProperty(HDRequiredIndProperty, .GetBoolean(8))
          LoadProperty(NotComplySpecReasonsProperty, .GetString(9))
          LoadProperty(CrewPlanningFinalisedByProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(CrewPlanningFinalisedDateProperty, .GetValue(11))
          LoadProperty(PlanningFinalisedDateProperty, .GetValue(12))
          LoadProperty(CancelledDateProperty, .GetValue(13))
          LoadProperty(CancelledReasonProperty, .GetString(14))
          LoadProperty(PlanningFinalisedByProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(ReconciledByProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          LoadProperty(ReconciledDateProperty, .GetValue(17))
          LoadProperty(ProductionDerigTemplateIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
          LoadProperty(AllowDuplicateHumanResourcesIndProperty, .GetBoolean(19))
          LoadProperty(ProductionRefNoProperty, .GetString(20))
          LoadProperty(ParentProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
          'LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
          LoadProperty(CreatedByProperty, .GetInt32(22))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(23))
          LoadProperty(ModifiedByProperty, .GetInt32(24))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(25))
          LoadProperty(CommentatorCrewPlanningFinalisedByProperty, Singular.Misc.ZeroNothing(.GetInt32(26)))
          LoadProperty(CommentatorCrewPlanningFinalisedDateProperty, .GetValue(27))
          LoadProperty(CommentatorPlanningFinalisedByProperty, Singular.Misc.ZeroNothing(.GetInt32(28)))
          LoadProperty(CommentatorPlanningFinalisedDateProperty, .GetValue(29))
          LoadProperty(CommentatorReconciledByProperty, Singular.Misc.ZeroNothing(.GetInt32(30)))
          LoadProperty(CommentatorReconciledDateProperty, .GetValue(31))
          LoadProperty(SynergyGenRefNoProperty, ZeroNothing(.GetInt64(32)))
          LoadProperty(ProductionGradeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(33)))
          LoadProperty(PlayStartDateTimeProperty, .GetValue(34))
          LoadProperty(PlayEndDateTimeProperty, .GetValue(35))
          LoadProperty(TitleProperty, .GetString(36))
          'LoadProperty(ProductionTypeProperty, .GetString(38))
          'LoadProperty(EventTypeProperty, .GetString(39))
          'LoadProperty(SpecNameProperty, .GetString(40))
          'LoadProperty(ProductionVenueProperty, .GetString(41))
          'LoadProperty(TxTimesProperty, .GetString(42))
          'LoadProperty(ProductionGradeProperty, .GetString(43))
          LoadProperty(CrewFinalisedProperty, .GetBoolean(37))
          LoadProperty(PlanningFinalisedProperty, .GetBoolean(38))
          LoadProperty(CancelledProperty, .GetBoolean(39))
          LoadProperty(ReconciledProperty, .GetBoolean(40))
          LoadProperty(ReconciledSetDateProperty, .GetValue(41))
          LoadProperty(CommentatorCrewFinalisedProperty, .GetBoolean(42))
          LoadProperty(CommentatorPlanningFinalisedProperty, .GetBoolean(43))
          LoadProperty(CommentatorReconciledProperty, .GetBoolean(44))
          LoadProperty(CrewPlanningFirstFinalisedDateProperty, .GetValue(45))
          LoadProperty(CreationTypeIDProperty, .GetInt32(46))
          LoadProperty(PlaceholderIndProperty, .GetBoolean(47))
          LoadProperty(VisionViewIndProperty, .GetBoolean(48))
          LoadProperty(TimelineReadyIndProperty, .GetBoolean(49))
          LoadProperty(TimelineReadyDateTimeProperty, .GetValue(50))
          LoadProperty(TimelineReadyByUserIDProperty, .GetInt32(51))
          LoadProperty(GraphicsSuppliersProperty, .GetString(52))
          LoadProperty(OBFacilitySuppliersProperty, .GetString(53))
          LoadProperty(CityProperty, .GetString(54))
          LoadProperty(OriginalReconciledProperty, Reconciled)
          LoadProperty(LoadedProductionTypeIDProperty, ProductionTypeID)
          LoadProperty(LoadedEventTypeIDProperty, EventTypeID)
          LoadProperty(LoadedTeamsPlayingProperty, TeamsPlaying)
          LoadProperty(LoadedProductionVenueIDProperty, ProductionVenueID)
          LoadProperty(LoadedPlayStartDateTimeProperty, PlayStartDateTime)
          LoadProperty(LoadedPlayEndDateTimeProperty, PlayEndDateTime)
          LoadProperty(LoadedCrewFinalisedProperty, CrewFinalised)
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionOld"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionOld"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionID As SqlParameter = .Parameters.Add("@ProductionID", SqlDbType.Int)
          paramProductionID.Value = GetProperty(ProductionIDProperty)
          If Me.IsNew Then
            paramProductionID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionDescription", GetProperty(ProductionDescriptionProperty))
          .Parameters.AddWithValue("@TeamsPlaying", GetProperty(TeamsPlayingProperty))
          .Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(GetProperty(ProductionTypeIDProperty)))
          .Parameters.AddWithValue("@EventTypeID", NothingDBNull(GetProperty(EventTypeIDProperty)))
          .Parameters.AddWithValue("@ProductionSpecRequirementID", NothingDBNull(GetProperty(ProductionSpecRequirementIDProperty)))
          .Parameters.AddWithValue("@ProductionVenueID", NothingDBNull(GetProperty(ProductionVenueIDProperty)))
          .Parameters.AddWithValue("@VenueConfirmedDate", (New SmartDate(GetProperty(VenueConfirmedDateProperty))).DBValue)
          .Parameters.AddWithValue("@HDRequiredInd", GetProperty(HDRequiredIndProperty))
          .Parameters.AddWithValue("@NotComplySpecReasons", GetProperty(NotComplySpecReasonsProperty))
          .Parameters.AddWithValue("@CrewPlanningFinalisedBy", NothingDBNull(GetProperty(CrewPlanningFinalisedByProperty)))
          .Parameters.AddWithValue("@CrewPlanningFinalisedDate", (New SmartDate(GetProperty(CrewPlanningFinalisedDateProperty))).DBValue)
          .Parameters.AddWithValue("@PlanningFinalisedDate", (New SmartDate(GetProperty(PlanningFinalisedDateProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledDate", (New SmartDate(GetProperty(CancelledDateProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@PlanningFinalisedBy", NothingDBNull(GetProperty(PlanningFinalisedByProperty)))
          .Parameters.AddWithValue("@ReconciledBy", NothingDBNull(GetProperty(ReconciledByProperty)))
          .Parameters.AddWithValue("@ReconciledDate", (New SmartDate(GetProperty(ReconciledDateProperty))).DBValue)
          .Parameters.AddWithValue("@ProductionDerigTemplateID", NothingDBNull(GetProperty(ProductionDerigTemplateIDProperty)))
          .Parameters.AddWithValue("@AllowDuplicateHumanResourcesInd", GetProperty(AllowDuplicateHumanResourcesIndProperty))
          .Parameters.AddWithValue("@ProductionRefNo", GetProperty(ProductionRefNoProperty))
          .Parameters.AddWithValue("@ParentProductionID", NothingDBNull(GetProperty(ParentProductionIDProperty)))
          .Parameters.AddWithValue("@SystemID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
          .Parameters.AddWithValue("@CommentatorCrewPlanningFinalisedBy", NothingDBNull(GetProperty(CommentatorCrewPlanningFinalisedByProperty)))
          .Parameters.AddWithValue("@CommentatorCrewPlanningFinalisedDate", (New SmartDate(GetProperty(CommentatorCrewPlanningFinalisedDateProperty))).DBValue)
          .Parameters.AddWithValue("@CommentatorPlanningFinalisedBy", NothingDBNull(GetProperty(CommentatorPlanningFinalisedByProperty)))
          .Parameters.AddWithValue("@CommentatorPlanningFinalisedDate", (New SmartDate(GetProperty(CommentatorPlanningFinalisedDateProperty))).DBValue)
          .Parameters.AddWithValue("@CommentatorReconciledBy", NothingDBNull(GetProperty(CommentatorReconciledByProperty)))
          .Parameters.AddWithValue("@CommentatorReconciledDate", (New SmartDate(GetProperty(CommentatorReconciledDateProperty))).DBValue)
          .Parameters.AddWithValue("@SynergyGenRefNo", NothingDBNull(GetProperty(SynergyGenRefNoProperty)))
          .Parameters.AddWithValue("@ProductionGradeID", NothingDBNull(GetProperty(ProductionGradeIDProperty)))
          .Parameters.AddWithValue("@PlayStartDateTime", (New SmartDate(GetProperty(PlayStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@PlayEndDateTime", (New SmartDate(GetProperty(PlayEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@ReconciledSetDate", (New SmartDate(GetProperty(ReconciledSetDateProperty))).DBValue)
          .Parameters.AddWithValue("@CrewPlanningFirstFinalisedDate", (New SmartDate(GetProperty(CrewPlanningFirstFinalisedDateProperty))).DBValue)
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(OBLib.Security.Settings.CurrentUser.ProductionAreaID))
          .Parameters.AddWithValue("@PlaceHolderInd", GetProperty(PlaceholderIndProperty))
          .Parameters.AddWithValue("@TimelineReadyInd", GetProperty(TimelineReadyIndProperty))
          .Parameters.AddWithValue("@TimelineReadyDateTime", (New SmartDate(GetProperty(TimelineReadyDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@TimelineReadyByUserID", NothingDBNull(GetProperty(TimelineReadyByUserIDProperty)))
          '.Parameters.AddWithValue("@ProductionType", GetProperty(ProductionTypeProperty))
          '.Parameters.AddWithValue("@EventType", GetProperty(EventTypeProperty))
          '.Parameters.AddWithValue("@SpecName", GetProperty(SpecNameProperty))
          '.Parameters.AddWithValue("@ProductionVenue", GetProperty(ProductionVenueProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionIDProperty, paramProductionID.Value)
          End If
          SetupEmails()

          ' update child objects
          If GetProperty(ProductionSystemAreaListProperty) IsNot Nothing Then
            Me.ProductionSystemAreaList.Update()
          End If
          If GetProperty(ProductionVehicleListProperty) IsNot Nothing Then
            Me.ProductionVehicleList.Update()
          End If
          If GetProperty(ProductionDocumentListProperty) IsNot Nothing Then
            Me.ProductionDocumentList.Update()
          End If
          If GetProperty(ProductionGeneralCommentListProperty) IsNot Nothing Then
            Me.ProductionGeneralCommentList.Update()
          End If
          If GetProperty(ProductionPettyCashListProperty) IsNot Nothing Then
            Me.ProductionPettyCashList.Update()
          End If
          If GetProperty(ProductionCommentSectionListProperty) IsNot Nothing Then
            Me.ProductionCommentSectionList.Update()
          End If
          If GetProperty(TravelRequisitionListProperty) IsNot Nothing Then
            Me.TravelRequisitionList.Update()
          End If
          If GetProperty(ProductionAudioConfigurationListProperty) IsNot Nothing Then
            Me.ProductionAudioConfigurationList.Update()
          End If
          If GetProperty(EmailListProperty) IsNot Nothing Then
            Me.EmailList.Save()
            Me.EmailList.Clear()
          End If
          MarkOld()
        End With
      Else
        If GetProperty(ProductionSystemAreaListProperty) IsNot Nothing Then
          Me.ProductionSystemAreaList.Update()
        End If
        If GetProperty(ProductionVehicleListProperty) IsNot Nothing Then
          Me.ProductionVehicleList.Update()
        End If
        If GetProperty(ProductionDocumentListProperty) IsNot Nothing Then
          Me.ProductionDocumentList.Update()
        End If
        If GetProperty(ProductionGeneralCommentListProperty) IsNot Nothing Then
          Me.ProductionGeneralCommentList.Update()
        End If
        If GetProperty(ProductionPettyCashListProperty) IsNot Nothing Then
          Me.ProductionPettyCashList.Update()
        End If
        If GetProperty(ProductionCommentSectionListProperty) IsNot Nothing Then
          Me.ProductionCommentSectionList.Update()
        End If
        If GetProperty(TravelRequisitionListProperty) IsNot Nothing Then
          Me.TravelRequisitionList.Update()
        End If
        If GetProperty(ProductionAudioConfigurationListProperty) IsNot Nothing Then
          Me.ProductionAudioConfigurationList.Update()
        End If
        If GetProperty(EmailListProperty) IsNot Nothing Then
          Me.EmailList.Save()
          Me.EmailList.Clear()
        End If
      End If

      'Clear the email list after saving
      Me.EmailList.Clear()

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionOld"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace