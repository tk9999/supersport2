﻿' Generated 24 Jul 2014 14:32 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Correspondence

  <Serializable()> _
  Public Class ProductionDocumentList
    Inherits Singular.Documents.DocumentProviderListBase(Of ProductionDocumentList, ProductionDocument)

#Region " Business Methods "

    Public Function GetItem(DocumentID As Integer) As ProductionDocument

      For Each child As ProductionDocument In Me
        If child.DocumentID = DocumentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ParentID As Integer? = Nothing
      Public Property ParentTable As String = ""
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing

      Public Sub New(ParentID As Integer?, ParentTable As String,
                     SystemID As Integer?, ProductionAreaID As Integer?)
        Me.ParentID = ParentID
        Me.ParentTable = ParentTable
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionDocumentList() As ProductionDocumentList

      Return New ProductionDocumentList()

    End Function

    Public Shared Sub BeginGetProductionDocumentList(CallBack As EventHandler(Of DataPortalResult(Of ProductionDocumentList)))

      Dim dp As New DataPortal(Of ProductionDocumentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionDocumentList() As ProductionDocumentList

      Return DataPortal.Fetch(Of ProductionDocumentList)(New Criteria())

    End Function

    Public Shared Function GetProductionDocumentList(ParentID As Integer?, ParentTable As String,
                                                     SystemID As Integer?, ProductionAreaID As Integer?) As ProductionDocumentList

      Return DataPortal.Fetch(Of ProductionDocumentList)(New Criteria(ParentID, ParentTable, SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionDocument.GetProductionDocument(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionDocumentList"
            cm.Parameters.AddWithValue("@ParentID", NothingDBNull(crit.ParentID))
            cm.Parameters.AddWithValue("@ParentTable", Strings.MakeEmptyDBNull(crit.ParentTable))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionDocument In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionDocument In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace