﻿' Generated 24 Jul 2014 16:52 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Correspondence.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Productions.Old

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Correspondence

  <Serializable()> _
  Public Class ProductionPettyCash
    Inherits SingularBusinessBase(Of ProductionPettyCash)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionPettyCashIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionPettyCashID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionPettyCashID() As Integer
      Get
        Return GetProperty(ProductionPettyCashIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionPettyCashTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionPettyCashTypeID, "Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Petty Cash Type value
    ''' </summary>
    <Display(Name:="Type", Description:=""),
    Required(ErrorMessage:="Petty Cash Type required"),
    DropDownWeb(GetType(ROProductionPettyCashTypeList))>
    Public Property ProductionPettyCashTypeID() As Integer?
      Get
        Return GetProperty(ProductionPettyCashTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionPettyCashTypeIDProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:=""),
    StringLength(100, ErrorMessage:="Description cannot be more than 100 characters")>
    Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Amount, "Amount", 0)
    ''' <summary>
    ''' Gets and sets the Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:=""),
    Required(ErrorMessage:="Amount required"),
    NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(ROSystemList))>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required"),
    DropDownWeb(GetType(ROProductionAreaList))>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionPettyCashIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Description.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Petty Cash")
        Else
          Return String.Format("Blank {0}", "Production Petty Cash")
        End If
      Else
        Return Me.Description
      End If

    End Function

    Public Function GetParent() As Production

      Return CType(CType(Me.Parent, ProductionPettyCashList).Parent, Production)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionPettyCash() method.

    End Sub

    Public Shared Function NewProductionPettyCash() As ProductionPettyCash

      Return DataPortal.CreateChild(Of ProductionPettyCash)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionPettyCash(dr As SafeDataReader) As ProductionPettyCash

      Dim p As New ProductionPettyCash()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionPettyCashIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionPettyCashTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DescriptionProperty, .GetString(3))
          LoadProperty(AmountProperty, .GetDecimal(4))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionPettyCash"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionPettyCash"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionPettyCashID As SqlParameter = .Parameters.Add("@ProductionPettyCashID", SqlDbType.Int)
          paramProductionPettyCashID.Value = GetProperty(ProductionPettyCashIDProperty)
          If Me.IsNew Then
            paramProductionPettyCashID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionID", Me.GetParent.ProductionID)
          .Parameters.AddWithValue("@ProductionPettyCashTypeID", GetProperty(ProductionPettyCashTypeIDProperty))
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          .Parameters.AddWithValue("@Amount", GetProperty(AmountProperty))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionPettyCashIDProperty, paramProductionPettyCashID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionPettyCash"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionPettyCashID", GetProperty(ProductionPettyCashIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace