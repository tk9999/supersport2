﻿' Generated 19 May 2014 13:23 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Productions.ReadOnly
Imports OBLib.Productions.Old

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions

  <Serializable()> _
  Public Class ProductionAudioConfiguration
    Inherits OBBusinessBase(Of ProductionAudioConfiguration)

#Region " Properties and Methods "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
    Public Property Expanded As Boolean = False

#Region " Properties "

    Public Shared ProductionAudioConfigurationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAudioConfigurationID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionAudioConfigurationID() As Integer
      Get
        Return GetProperty(ProductionAudioConfigurationIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="Link to the Production")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared AudioConfigurationIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.AudioConfigurationID) _
                                                                              .AddSetExpression(OBLib.JSCode.ProductionAudioConfigurationJS.SetAudioConfigDetails)
    ''' <summary>
    ''' Gets and sets the Audio Configuration value
    ''' </summary>
    <Display(Name:="Audio Configuration", Description:="Audio Configuration Name"),
    Required(ErrorMessage:="Audio Configuration required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROAudioConfigurationList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)>
    Public Property AudioConfigurationID() As Integer?
      Get
        Return GetProperty(AudioConfigurationIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AudioConfigurationIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionAudioConfigurationDetailListProperty As PropertyInfo(Of ProductionAudioConfigurationDetailList) = RegisterProperty(Of ProductionAudioConfigurationDetailList)(Function(c) c.ProductionAudioConfigurationDetailList, "Production Audio Configuration Detail List")

    Public ReadOnly Property ProductionAudioConfigurationDetailList() As ProductionAudioConfigurationDetailList
      Get
        If GetProperty(ProductionAudioConfigurationDetailListProperty) Is Nothing Then
          LoadProperty(ProductionAudioConfigurationDetailListProperty, Productions.ProductionAudioConfigurationDetailList.NewProductionAudioConfigurationDetailList())
        End If
        Return GetProperty(ProductionAudioConfigurationDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionAudioConfigurationIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AudioConfigurationID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Audio Configuration")
        Else
          Return String.Format("Blank {0}", "Production Audio Configuration")
        End If
      Else
        Return Me.AudioConfigurationID.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ProductionAudioConfigurationDetails"}
      End Get
    End Property

    Public Function GetParent() As OBLib.Productions.Old.Production

      Return CType(CType(Me.Parent, ProductionAudioConfigurationList).Parent, OBLib.Productions.Old.Production)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionAudioConfiguration() method.

    End Sub

    Public Shared Function NewProductionAudioConfiguration() As ProductionAudioConfiguration

      Return DataPortal.CreateChild(Of ProductionAudioConfiguration)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionAudioConfiguration(dr As SafeDataReader) As ProductionAudioConfiguration

      Dim p As New ProductionAudioConfiguration()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionAudioConfigurationIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AudioConfigurationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionAudioConfiguration"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionAudioConfiguration"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionAudioConfigurationID As SqlParameter = .Parameters.Add("@ProductionAudioConfigurationID", SqlDbType.Int)
          paramProductionAudioConfigurationID.Value = GetProperty(ProductionAudioConfigurationIDProperty)
          If Me.IsNew Then
            paramProductionAudioConfigurationID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionID", Me.GetParent.ProductionID)
          .Parameters.AddWithValue("@AudioConfigurationID", GetProperty(AudioConfigurationIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionAudioConfigurationIDProperty, paramProductionAudioConfigurationID.Value)
          End If
          ' update child objects
          If GetProperty(ProductionAudioConfigurationDetailListProperty) IsNot Nothing Then
            Me.ProductionAudioConfigurationDetailList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ProductionAudioConfigurationDetailListProperty) IsNot Nothing Then
          Me.ProductionAudioConfigurationDetailList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionAudioConfiguration"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionAudioConfigurationID", GetProperty(ProductionAudioConfigurationIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace