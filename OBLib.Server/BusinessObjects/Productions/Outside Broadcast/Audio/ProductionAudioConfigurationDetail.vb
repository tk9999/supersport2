﻿' Generated 19 May 2014 13:23 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions

  <Serializable()> _
  Public Class ProductionAudioConfigurationDetail
    Inherits OBBusinessBase(Of ProductionAudioConfigurationDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionAudioConfigurationDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAudioConfigurationDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionAudioConfigurationDetailID() As Integer
      Get
        Return GetProperty(ProductionAudioConfigurationDetailIDProperty)
      End Get
    End Property

    Public Shared ProductionAudioConfigurationIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAudioConfigurationID, "Production Audio Configuration", Nothing)
    ''' <summary>
    ''' Gets the Production Audio Configuration value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionAudioConfigurationID() As Integer?
      Get
        Return GetProperty(ProductionAudioConfigurationIDProperty)
      End Get
    End Property

    Public Shared AudioConfigurationDetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AudioConfigurationDetail, "Audio Configuration Detail", "")
    ''' <summary>
    ''' Gets and sets the Audio Configuration Detail value
    ''' </summary>
    <Display(Name:="Audio Configuration Detail", Description:="The detail for the audio configuration"),
    Required(ErrorMessage:="Audio Configuration Detail required"),
    StringLength(100, ErrorMessage:="Audio Configuration Detail cannot be more than 100 characters")>
    Public Property AudioConfigurationDetail() As String
      Get
        Return GetProperty(AudioConfigurationDetailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AudioConfigurationDetailProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionAudioConfiguration

      Return CType(CType(Me.Parent, ProductionAudioConfigurationDetailList).Parent, ProductionAudioConfiguration)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionAudioConfigurationDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AudioConfigurationDetail.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Audio Configuration Detail")
        Else
          Return String.Format("Blank {0}", "Production Audio Configuration Detail")
        End If
      Else
        Return Me.AudioConfigurationDetail
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionAudioConfigurationDetail() method.

    End Sub

    Public Shared Function NewProductionAudioConfigurationDetail() As ProductionAudioConfigurationDetail

      Return DataPortal.CreateChild(Of ProductionAudioConfigurationDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionAudioConfigurationDetail(dr As SafeDataReader) As ProductionAudioConfigurationDetail

      Dim p As New ProductionAudioConfigurationDetail()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionAudioConfigurationDetailIDProperty, .GetInt32(0))
          LoadProperty(ProductionAudioConfigurationIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AudioConfigurationDetailProperty, .GetString(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionAudioConfigurationDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionAudioConfigurationDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionAudioConfigurationDetailID As SqlParameter = .Parameters.Add("@ProductionAudioConfigurationDetailID", SqlDbType.Int)
          paramProductionAudioConfigurationDetailID.Value = GetProperty(ProductionAudioConfigurationDetailIDProperty)
          If Me.IsNew Then
            paramProductionAudioConfigurationDetailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionAudioConfigurationID", Me.GetParent().ProductionAudioConfigurationID)
          .Parameters.AddWithValue("@AudioConfigurationDetail", GetProperty(AudioConfigurationDetailProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionAudioConfigurationDetailIDProperty, paramProductionAudioConfigurationDetailID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionAudioConfigurationDetail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionAudioConfigurationDetailID", GetProperty(ProductionAudioConfigurationDetailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace