﻿' Generated 19 May 2014 13:23 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions

  <Serializable()> _
  Public Class ProductionAudioConfigurationList
    Inherits OBBusinessListBase(Of ProductionAudioConfigurationList, ProductionAudioConfiguration)

#Region " Business Methods "

    Public Function GetItem(ProductionAudioConfigurationID As Integer) As ProductionAudioConfiguration

      For Each child As ProductionAudioConfiguration In Me
        If child.ProductionAudioConfigurationID = ProductionAudioConfigurationID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Audio Configurations"

    End Function

    Public Function GetProductionAudioConfigurationDetail(ProductionAudioConfigurationDetailID As Integer) As ProductionAudioConfigurationDetail

      Dim obj As ProductionAudioConfigurationDetail = Nothing
      For Each parent As ProductionAudioConfiguration In Me
        obj = parent.ProductionAudioConfigurationDetailList.GetItem(ProductionAudioConfigurationDetailID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public ProductionID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(ProductionID As Integer?)

        Me.ProductionID = ProductionID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionAudioConfigurationList() As ProductionAudioConfigurationList

      Return New ProductionAudioConfigurationList()

    End Function

    Public Shared Sub BeginGetProductionAudioConfigurationList(CallBack As EventHandler(Of DataPortalResult(Of ProductionAudioConfigurationList)))

      Dim dp As New DataPortal(Of ProductionAudioConfigurationList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionAudioConfigurationList() As ProductionAudioConfigurationList

      Return DataPortal.Fetch(Of ProductionAudioConfigurationList)(New Criteria())

    End Function

    Public Shared Function GetProductionAudioConfigurationList(ProductionID As Integer?) As ProductionAudioConfigurationList

      Return DataPortal.Fetch(Of ProductionAudioConfigurationList)(New Criteria(ProductionID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionAudioConfiguration.GetProductionAudioConfiguration(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ProductionAudioConfiguration = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionAudioConfigurationID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionAudioConfigurationDetailList.RaiseListChangedEvents = False
          parent.ProductionAudioConfigurationDetailList.Add(ProductionAudioConfigurationDetail.GetProductionAudioConfigurationDetail(sdr))
          parent.ProductionAudioConfigurationDetailList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As ProductionAudioConfiguration In Me
        child.CheckRules()
        For Each ProductionAudioConfigurationDetail As ProductionAudioConfigurationDetail In child.ProductionAudioConfigurationDetailList
          ProductionAudioConfigurationDetail.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionAudioConfigurationList"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionAudioConfiguration In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionAudioConfiguration In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace