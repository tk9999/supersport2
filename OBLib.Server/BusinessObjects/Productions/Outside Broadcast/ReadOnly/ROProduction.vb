﻿' Generated 20 May 2014 16:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.ReadOnly

  <Serializable()>
  Public Class ROProduction
    Inherits OBReadOnlyBase(Of ROProduction)


#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="ID"), Key()>
    Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Production Description", "")
    ''' <summary>
    ''' Gets the Production Description value
    ''' </summary>
    <Display(Name:="Production Description", Description:="")>
    Public ReadOnly Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
    End Property

    Public Shared TeamsPlayingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamsPlaying, "Teams Playing", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Teams", Description:="")>
    Public ReadOnly Property TeamsPlaying() As String
      Get
        Return GetProperty(TeamsPlayingProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="The type of production")>
    Public ReadOnly Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventTypeID, "Event Type", Nothing)
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="The type of event that this production is part of (PSL Soccer, Super Rugby)")>
    Public ReadOnly Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
    End Property

    'Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSpecRequirementID, "Production Spec Requirement", Nothing)
    ' ''' <summary>
    ' ''' Gets the Production Spec Requirement value
    ' ''' </summary>
    '<Display(Name:="Production Spec Requirement", Description:="The Production Spec that this production is based on")>
    'Public ReadOnly Property ProductionSpecRequirementID() As Integer?
    '  Get
    '    Return GetProperty(ProductionSpecRequirementIDProperty)
    '  End Get
    'End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue", Nothing)
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="The venue that this production will be held at")>
    Public ReadOnly Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
    End Property

    Public Shared VenueConfirmedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.VenueConfirmedDate, "Venue Confirmed Date")
    ''' <summary>
    ''' Gets the Venue Confirmed Date value
    ''' </summary>
    <Display(Name:="Venue Confirmed Date", Description:="Tick indicates that the venue has been confirmed. NULL indicates that the venue has not been confirmed")>
    Public ReadOnly Property VenueConfirmedDate As DateTime?
      Get
        Return GetProperty(VenueConfirmedDateProperty)
      End Get
    End Property

    'Public Shared HDRequiredIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.HDRequiredInd, "HD Required", CType(Nothing, Boolean?))
    ' ''' <summary>
    ' ''' Gets the HD Required value
    ' ''' </summary>
    '<Display(Name:="HD Required", Description:="Tick indicates that HD is required. No tick indicates that HD is not required.")>
    'Public ReadOnly Property HDRequiredInd() As Boolean?
    '  Get
    '    Return GetProperty(HDRequiredIndProperty)
    '  End Get
    'End Property

    'Public Shared NotComplySpecReasonsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotComplySpecReasons, "Not Comply Spec Reasons", "")
    ' ''' <summary>
    ' ''' Gets the Not Comply Spec Reasons value
    ' ''' </summary>
    '<Display(Name:="Not Comply Spec Reasons", Description:="If a reason is captured, then the planner has decided to change the spec for this event")>
    'Public ReadOnly Property NotComplySpecReasons() As String
    '  Get
    '    Return GetProperty(NotComplySpecReasonsProperty)
    '  End Get
    'End Property

    'Public Shared CrewPlanningFinalisedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewPlanningFinalisedBy, "Crew Planning Finalised By", Nothing)
    ' ''' <summary>
    ' ''' Gets the Crew Planning Finalised By value
    ' ''' </summary>
    '<Display(Name:="Crew Planning Finalised By", Description:="")>
    'Public ReadOnly Property CrewPlanningFinalisedBy() As Integer?
    '  Get
    '    Return GetProperty(CrewPlanningFinalisedByProperty)
    '  End Get
    'End Property

    'Public Shared CrewPlanningFinalisedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CrewPlanningFinalisedDate, "Crew Planning Finalised Date")
    ' ''' <summary>
    ' ''' Gets the Crew Planning Finalised Date value
    ' ''' </summary>
    '<Display(Name:="Crew Planning Finalised Date", Description:="")>
    'Public ReadOnly Property CrewPlanningFinalisedDate As DateTime?
    '  Get
    '    Return GetProperty(CrewPlanningFinalisedDateProperty)
    '  End Get
    'End Property

    'Public Shared PlanningFinalisedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlanningFinalisedDate, "Planning Finalised Date")
    ' ''' <summary>
    ' ''' Gets the Planning Finalised Date value
    ' ''' </summary>
    '<Display(Name:="Planning Finalised Date", Description:="The date that this production plan is finalised")>
    'Public ReadOnly Property PlanningFinalisedDate As DateTime?
    '  Get
    '    Return GetProperty(PlanningFinalisedDateProperty)
    '  End Get
    'End Property

    'Public Shared CancelledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDate, "Cancelled Date")
    ' ''' <summary>
    ' ''' Gets the Cancelled Date value
    ' ''' </summary>
    '<Display(Name:="Cancelled Date", Description:="The date (and time) that the production was cancelled.")>
    'Public ReadOnly Property CancelledDate As DateTime?
    '  Get
    '    Return GetProperty(CancelledDateProperty)
    '  End Get
    'End Property

    'Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ' ''' <summary>
    ' ''' Gets the Cancelled Reason value
    ' ''' </summary>
    '<Display(Name:="Cancelled Reason", Description:="The reason for the cancellation")>
    'Public ReadOnly Property CancelledReason() As String
    '  Get
    '    Return GetProperty(CancelledReasonProperty)
    '  End Get
    'End Property

    'Public Shared PlanningFinalisedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PlanningFinalisedBy, "Planning Finalised By", Nothing)
    ' ''' <summary>
    ' ''' Gets the Planning Finalised By value
    ' ''' </summary>
    '<Display(Name:="Planning Finalised By", Description:="The user that this marked the production plan as finalised")>
    'Public ReadOnly Property PlanningFinalisedBy() As Integer?
    '  Get
    '    Return GetProperty(PlanningFinalisedByProperty)
    '  End Get
    'End Property

    'Public Shared ReconciledByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ReconciledBy, "Reconciled By", Nothing)
    ' ''' <summary>
    ' ''' Gets the Reconciled By value
    ' ''' </summary>
    '<Display(Name:="Reconciled By", Description:="The user that reconciled the production")>
    'Public ReadOnly Property ReconciledBy() As Integer?
    '  Get
    '    Return GetProperty(ReconciledByProperty)
    '  End Get
    'End Property

    'Public Shared ReconciledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ReconciledDate, "Reconciled Date")
    ' ''' <summary>
    ' ''' Gets the Reconciled Date value
    ' ''' </summary>
    '<Display(Name:="Reconciled Date", Description:="This is the date that the event planner confirms that the event has completed all the neccessary changes and that the crew timesheets are correct")>
    'Public ReadOnly Property ReconciledDate As DateTime?
    '  Get
    '    Return GetProperty(ReconciledDateProperty)
    '  End Get
    'End Property

    'Public Shared ProductionDerigTemplateIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionDerigTemplateID, "Production Derig Template", 0)
    ' ''' <summary>
    ' ''' Gets the Production Derig Template value
    ' ''' </summary>
    '<Display(Name:="Production Derig Template", Description:="")>
    'Public ReadOnly Property ProductionDerigTemplateID() As Integer
    '  Get
    '    Return GetProperty(ProductionDerigTemplateIDProperty)
    '  End Get
    'End Property

    'Public Shared AllowDuplicateHumanResourcesIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AllowDuplicateHumanResourcesInd, "Allow Duplicate Human Resources", False)
    ' ''' <summary>
    ' ''' Gets the Allow Duplicate Human Resources value
    ' ''' </summary>
    '<Display(Name:="Allow Duplicate Human Resources", Description:="Tick indicates that duplicate Human Resources may be added to this production")>
    'Public ReadOnly Property AllowDuplicateHumanResourcesInd() As Boolean
    '  Get
    '    Return GetProperty(AllowDuplicateHumanResourcesIndProperty)
    '  End Get
    'End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionRefNo, "Ref", "")
    ''' <summary>
    ''' Gets the Production Ref No value
    ''' </summary>
    <Display(Name:="Ref", Description:="")>
    Public ReadOnly Property ProductionRefNo() As String
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
    End Property

    Public Shared ParentProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentProductionID, "Parent Production", Nothing)
    ''' <summary>
    ''' Gets the Parent Production value
    ''' </summary>
    <Display(Name:="Parent Production", Description:="")>
    Public ReadOnly Property ParentProductionID() As Integer?
      Get
        Return GetProperty(ParentProductionIDProperty)
      End Get
    End Property

    'Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 1)
    ' ''' <summary>
    ' ''' Gets the System value
    ' ''' </summary>
    '<Display(Name:="System", Description:="")>
    'Public ReadOnly Property SystemID() As Integer
    '  Get
    '    Return GetProperty(SystemIDProperty)
    '  End Get
    'End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time")
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    <Display(Name:="Created Date", Description:="")>
    Public ReadOnly Property CreatedDateString As String
      Get
        If CreatedDateTime.HasValue Then
          Return CreatedDateTime.Value.ToString("dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    'Public Shared CommentatorCrewPlanningFinalisedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CommentatorCrewPlanningFinalisedBy, "Commentator Crew Planning Finalised By", 0)
    ' ''' <summary>
    ' ''' Gets the Commentator Crew Planning Finalised By value
    ' ''' </summary>
    '<Display(Name:="Commentator Crew Planning Finalised By", Description:="")>
    'Public ReadOnly Property CommentatorCrewPlanningFinalisedBy() As Integer
    '  Get
    '    Return GetProperty(CommentatorCrewPlanningFinalisedByProperty)
    '  End Get
    'End Property

    'Public Shared CommentatorCrewPlanningFinalisedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CommentatorCrewPlanningFinalisedDate, "Commentator Crew Planning Finalised Date")
    ' ''' <summary>
    ' ''' Gets the Commentator Crew Planning Finalised Date value
    ' ''' </summary>
    '<Display(Name:="Commentator Crew Planning Finalised Date", Description:="")>
    'Public ReadOnly Property CommentatorCrewPlanningFinalisedDate As DateTime?
    '  Get
    '    Return GetProperty(CommentatorCrewPlanningFinalisedDateProperty)
    '  End Get
    'End Property

    'Public Shared CommentatorPlanningFinalisedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CommentatorPlanningFinalisedBy, "Commentator Planning Finalised By", 0)
    ' ''' <summary>
    ' ''' Gets the Commentator Planning Finalised By value
    ' ''' </summary>
    '<Display(Name:="Commentator Planning Finalised By", Description:="")>
    'Public ReadOnly Property CommentatorPlanningFinalisedBy() As Integer
    '  Get
    '    Return GetProperty(CommentatorPlanningFinalisedByProperty)
    '  End Get
    'End Property

    'Public Shared CommentatorPlanningFinalisedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CommentatorPlanningFinalisedDate, "Commentator Planning Finalised Date")
    ' ''' <summary>
    ' ''' Gets the Commentator Planning Finalised Date value
    ' ''' </summary>
    '<Display(Name:="Commentator Planning Finalised Date", Description:="")>
    'Public ReadOnly Property CommentatorPlanningFinalisedDate As DateTime?
    '  Get
    '    Return GetProperty(CommentatorPlanningFinalisedDateProperty)
    '  End Get
    'End Property

    'Public Shared CommentatorReconciledByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CommentatorReconciledBy, "Commentator Reconciled By", 0)
    ' ''' <summary>
    ' ''' Gets the Commentator Reconciled By value
    ' ''' </summary>
    '<Display(Name:="Commentator Reconciled By", Description:="")>
    'Public ReadOnly Property CommentatorReconciledBy() As Integer
    '  Get
    '    Return GetProperty(CommentatorReconciledByProperty)
    '  End Get
    'End Property

    'Public Shared CommentatorReconciledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CommentatorReconciledDate, "Commentator Reconciled Date")
    ' ''' <summary>
    ' ''' Gets the Commentator Reconciled Date value
    ' ''' </summary>
    '<Display(Name:="Commentator Reconciled Date", Description:="")>
    'Public ReadOnly Property CommentatorReconciledDate As DateTime?
    '  Get
    '    Return GetProperty(CommentatorReconciledDateProperty)
    '  End Get
    'End Property

    Public Shared SynergyGenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.SynergyGenRefNo, "Gen Ref", Nothing)
    ''' <summary>
    ''' Gets the Synergy Gen Ref No value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:="")>
    Public ReadOnly Property SynergyGenRefNo() As Int64?
      Get
        Return GetProperty(SynergyGenRefNoProperty)
      End Get
    End Property



    'Public Shared ProductionGradeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionGradeID, "Production Grade", Nothing)
    ' ''' <summary>
    ' ''' Gets the Production Grade value
    ' ''' </summary>
    '<Display(Name:="Grade", Description:="")>
    'Public ReadOnly Property ProductionGradeID() As Integer?
    '  Get
    '    Return GetProperty(ProductionGradeIDProperty)
    '  End Get
    'End Property

    Public Shared PlayStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlayStartDateTime, "Play Start Date Time")
    ''' <summary>
    ''' Gets the Play Start Date Time value
    ''' </summary>
    <Display(Name:="Starts", Description:="")>
    Public ReadOnly Property PlayStartDateTime As DateTime?
      Get
        Return GetProperty(PlayStartDateTimeProperty)
      End Get
    End Property

    Public Shared PlayEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PlayEndDateTime, "Play End Date Time")
    ''' <summary>
    ''' Gets the Play End Date Time value
    ''' </summary>
    <Display(Name:="Ends", Description:="")>
    Public ReadOnly Property PlayEndDateTime As DateTime?
      Get
        Return GetProperty(PlayEndDateTimeProperty)
      End Get
    End Property

    <Display(Name:="Starts")>
    Public ReadOnly Property PlayStartsString As String
      Get
        If PlayStartDateTime IsNot Nothing Then
          Return PlayStartDateTime.Value.ToString("dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Ends")>
    Public ReadOnly Property PlayEndsString As String
      Get
        If PlayEndDateTime IsNot Nothing Then
          Return PlayEndDateTime.Value.ToString("dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "ProductionType", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "EventType", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Venue/Stadium", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Venue/Stadium", Description:="")>
    Public ReadOnly Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
    End Property



    'Public Shared ProductionGradeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionGrade, "ProductionGrade", "")
    ' ''' <summary>
    ' ''' Gets the Teams Playing value
    ' ''' </summary>
    '<Display(Name:="ProductionGrade", Description:="")>
    'Public ReadOnly Property ProductionGrade() As String
    '  Get
    '    Return GetProperty(ProductionGradeProperty)
    '  End Get
    'End Property

    Public Shared TxStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxStartDateTime, "Tx Start Date Time")
    ''' <summary>
    ''' Gets the Tx Start Date Time value
    ''' </summary>
    <Display(Name:="Tx Start", Description:="Tx Start Date Time")>
    Public ReadOnly Property TxStartDateTime As DateTime?
      Get
        Return GetProperty(TxStartDateTimeProperty)
      End Get
    End Property

    Public Shared TxEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxEndDateTime, "Tx End Date Time")
    ''' <summary>
    ''' Gets the Tx End Date Time value
    ''' </summary>
    <Display(Name:="Tx End", Description:="Tx End Date Time")>
    Public ReadOnly Property TxEndDateTime As DateTime?
      Get
        Return GetProperty(TxEndDateTimeProperty)
      End Get
    End Property

    Public Shared EventManagerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventManager, "Event Manager", "")
    ''' <summary>
    ''' Gets the Event Manager value
    ''' </summary>
    <Display(Name:="Event Manager", Description:="Event Manager of the Production")>
    Public ReadOnly Property EventManager() As String
      Get
        Return GetProperty(EventManagerProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "Production Area Status", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Status", "")
    ''' <summary>
    ''' Gets the ProductionAreaStatus value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room", "")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name", "")
    ''' <summary>
    ''' Gets the Vehicle Name value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="")>
    Public ReadOnly Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "ProductionSystemAreaID", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="ProductionSystemAreaID", Description:="")>
    Public ReadOnly Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared CreationTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreationTypeID, "CreationTypeID", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="CreationTypeID", Description:="")>
    Public ReadOnly Property CreationTypeID() As Integer
      Get
        Return GetProperty(CreationTypeIDProperty)
      End Get
    End Property

    Public Shared PlaceHolderIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.PlaceHolderInd, "PlaceHolder", False)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="PlaceHolder", Description:="")>
    Public ReadOnly Property PlaceHolderInd() As Boolean?
      Get
        Return GetProperty(PlaceHolderIndProperty)
      End Get
    End Property

    Public Shared VisionViewIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.VisionViewInd, "VisionView", False)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="VisionView", Description:="")>
    Public ReadOnly Property VisionViewInd() As Boolean?
      Get
        Return GetProperty(VisionViewIndProperty)
      End Get
    End Property

    Public Shared GraphicSuppliersProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GraphicSuppliers, "Graphics", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Graphics", Description:="")>
    Public ReadOnly Property GraphicSuppliers() As String
      Get
        Return GetProperty(GraphicSuppliersProperty)
      End Get
    End Property

    Public Shared OBFacilitySuppliersProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OBFacilitySuppliers, "OBFacility Suppliers")
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="OBFacility Suppliers", Description:="")>
    Public ReadOnly Property OBFacilitySuppliers() As String
      Get
        Return GetProperty(OBFacilitySuppliersProperty)
      End Get
    End Property

    Public Shared CreatedByUserProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByUser, "Created By User", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Created By", Description:="")>
    Public ReadOnly Property CreatedByUser() As String
      Get
        Return GetProperty(CreatedByUserProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public Shared IsBeingEditedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IsBeingEditedBy, "Is Being Edited By", Nothing)
    ''' <summary>
    ''' 
    ''' </summary>
    <Display(Name:="IsBeingEditedBy", Description:="")>
    Public ReadOnly Property IsBeingEditedBy() As Integer?
      Get
        Return GetProperty(IsBeingEditedByProperty)
      End Get
    End Property

    Public Shared InEditDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.InEditDateTime)
    ''' <summary>
    ''' 
    ''' </summary>
    <Display(Name:="IsBeingEditedBy", Description:="")>
    Public ReadOnly Property InEditDateTime() As DateTime?
      Get
        Return GetProperty(InEditDateTimeProperty)
      End Get
    End Property

    Public Shared InEditByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.InEditByName, "InEditByName")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="InEditByName", Description:="")>
    Public ReadOnly Property InEditByName() As String
      Get
        Return GetProperty(InEditByNameProperty)
      End Get
    End Property

    Public Shared EditImagePathProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EditImagePath, "EditImagePath")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="EditImagePath", Description:="")>
    Public ReadOnly Property EditImagePath() As String
      Get
        Return GetProperty(EditImagePathProperty)
      End Get
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingID, "ResourceBookingID", Nothing)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="ResourceBookingID", Description:="")>
    Public ReadOnly Property ResourceBookingID() As Integer?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
    End Property

    Public Shared FullVehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FullVehicleName, "Vehicle Name", "")
    ''' <summary>
    ''' Gets the Vehicle Name value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="")>
    Public ReadOnly Property FullVehicleName() As String
      Get
        Return GetProperty(FullVehicleNameProperty)
      End Get
    End Property

    Public Shared FullVenueNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FullVenueName, "Venue", "")
    ''' <summary>
    ''' Gets the Vehicle Name value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public ReadOnly Property FullVenueName() As String
      Get
        Return GetProperty(FullVenueNameProperty)
      End Get
    End Property

    Public Shared FullProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FullProductionDescription, "Description", "")
    ''' <summary>
    ''' Gets the Vehicle Name value
    ''' </summary>
    <Display(Name:="Description", Description:="")>
    Public ReadOnly Property FullProductionDescription() As String
      Get
        Return GetProperty(FullProductionDescriptionProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProduction(dr As SafeDataReader) As ROProduction

      Dim r As New ROProduction()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionIDProperty, .GetInt32(0))
        LoadProperty(ProductionDescriptionProperty, .GetString(1))
        LoadProperty(TeamsPlayingProperty, .GetString(2))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        'LoadProperty(ProductionSpecRequirementIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(VenueConfirmedDateProperty, .GetValue(6))
        'LoadProperty(HDRequiredIndProperty, .GetValue(8))
        'mHDRequiredInd = .GetValue(8)
        'LoadProperty(NotComplySpecReasonsProperty, .GetString(9))
        'LoadProperty(CrewPlanningFinalisedByProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        'LoadProperty(CrewPlanningFinalisedDateProperty, .GetValue(11))
        'LoadProperty(PlanningFinalisedDateProperty, .GetValue(12))
        'LoadProperty(CancelledDateProperty, .GetValue(13))
        'LoadProperty(CancelledReasonProperty, .GetString(14))
        'LoadProperty(PlanningFinalisedByProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
        'LoadProperty(ReconciledByProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
        'LoadProperty(ReconciledDateProperty, .GetValue(17))
        'LoadProperty(ProductionDerigTemplateIDProperty, .GetInt32(18))
        'LoadProperty(AllowDuplicateHumanResourcesIndProperty, .GetBoolean(19))
        LoadProperty(ProductionRefNoProperty, .GetString(7))
        LoadProperty(ParentProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        'LoadProperty(System.IO.Ports.Parity, .GetInt32(22))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetValue(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
        'LoadProperty(CommentatorCrewPlanningFinalisedByProperty, .GetInt32(27))
        'LoadProperty(CommentatorCrewPlanningFinalisedDateProperty, .GetValue(28))
        'LoadProperty(CommentatorPlanningFinalisedByProperty, .GetInt32(29))
        'LoadProperty(CommentatorPlanningFinalisedDateProperty, .GetValue(30))
        'LoadProperty(CommentatorReconciledByProperty, .GetInt32(31))
        'LoadProperty(CommentatorReconciledDateProperty, .GetValue(32))
        LoadProperty(SynergyGenRefNoProperty, ZeroNothing(.GetInt64(13)))
        'LoadProperty(ProductionGradeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(34)))
        LoadProperty(TitleProperty, .GetString(14))
        LoadProperty(ProductionTypeProperty, .GetString(15))
        LoadProperty(EventTypeProperty, .GetString(16))
        LoadProperty(ProductionVenueProperty, .GetString(17))
        LoadProperty(EventManagerProperty, .GetString(18))
        LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
        'LoadProperty(ProductionGradeProperty, .GetString(41))
        LoadProperty(PlayStartDateTimeProperty, .GetValue(20))
        LoadProperty(PlayEndDateTimeProperty, .GetValue(21))
        LoadProperty(TxStartDateTimeProperty, .GetValue(22))
        LoadProperty(TxEndDateTimeProperty, .GetValue(23))
        LoadProperty(VehicleNameProperty, .GetString(24))
        LoadProperty(ProductionAreaStatusProperty, .GetString(25))
        LoadProperty(RoomProperty, .GetString(26))
        LoadProperty(ProductionSystemAreaIDProperty, ZeroNothing(.GetInt32(28)))
        LoadProperty(CreationTypeIDProperty, .GetInt32(29))
        LoadProperty(PlaceHolderIndProperty, .GetBoolean(30))
        LoadProperty(VisionViewIndProperty, .GetBoolean(31))
        LoadProperty(GraphicSuppliersProperty, .GetString(32))
        LoadProperty(CreatedByUserProperty, .GetString(33))
        LoadProperty(OBFacilitySuppliersProperty, .GetString(34))
        LoadProperty(CityProperty, .GetString(35))
        LoadProperty(IsBeingEditedByProperty, ZeroNothing(.GetInt32(38)))
        LoadProperty(InEditDateTimeProperty, .GetValue(39))
        LoadProperty(InEditByNameProperty, .GetValue(40))
        LoadProperty(EditImagePathProperty, .GetString(41))
        LoadProperty(ResourceBookingIDProperty, NothingDBNull(.GetInt32(42)))
        LoadProperty(FullVehicleNameProperty, .GetString(43))
        LoadProperty(FullVenueNameProperty, .GetString(44))
        LoadProperty(FullProductionDescriptionProperty, .GetString(45))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace