﻿' Generated 20 May 2014 16:40 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly.Old

Namespace Productions.ReadOnly

  <Serializable()>
  Public Class ROProductionList
    Inherits OBReadOnlyListBase(Of ROProductionList, ROProduction)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROProduction

      For Each child As ROProduction In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Productions"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property ProductionServicesInd As Boolean = False
      Public Property ProductionContentInd As Boolean = False

      Public Property OutsideBroadcastInd As Boolean = False
      Public Property StudioInd As Boolean = False

      Public Property OBVan As Boolean = False
      Public Property Externalvan As Boolean = False

      Public Shared TxDateFromProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxDateFrom, "")
      <Display(Name:="Start Date", Description:="")>
      Public Overridable Property TxDateFrom As DateTime?
        Get
          Return ReadProperty(TxDateFromProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(TxDateFromProperty, Value)
        End Set
      End Property
      '      Singular.DataAnnotations.JSRule(JavascriptCode:=OBLib.JSCode.ROProductionJS.FilterIsValidFrom)

      Public Shared TxDateToProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TxDateTo, "")
      <Display(Name:="End Date", Description:="")>
      Public Overridable Property TxDateTo As DateTime?
        Get
          Return ReadProperty(TxDateToProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(TxDateToProperty, Value)
        End Set
      End Property
      '       Singular.DataAnnotations.JSRule(JavascriptCode:=OBLib.JSCode.ROProductionJS.FilterIsValidTo)

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", OBLib.Security.Settings.CurrentUser.SystemID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
      Required(ErrorMessage:="Sub-Dept is required")>
      Public Overridable Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area is required")>
      Public Overridable Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production Type", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete,
                                           UnselectedText:="Production Type",
                                           LookupMember:="ProductionType", ValueMember:="ProductionTypeID",
                                           BeforeFetchJS:="ROProductionBO.setProductionTypeIDCriteriaBeforeRefresh",
                                           PreFindJSFunction:="ROProductionBO.triggerProductionTypeIDAutoPopulate")>
      Public Overridable Property ProductionTypeID() As Integer?
        Get
          Return ReadProperty(ProductionTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionTypeIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Production Type", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production Type", Description:="")>
      Public Overridable Property ProductionType() As String
        Get
          Return ReadProperty(ProductionTypeProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionTypeProperty, Value)
        End Set
      End Property

      Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventTypeID, "Event Type", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Event Type", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROEventTypeList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete,
                                           UnselectedText:="Event Type", ThisFilterMember:="ProductionTypeID",
                                           LookupMember:="EventType", ValueMember:="EventTypeID",
                                           BeforeFetchJS:="ROProductionBO.setEventTypeIDCriteriaBeforeRefresh",
                                           PreFindJSFunction:="ROProductionBO.triggerEventTypeIDAutoPopulate")>
      Public Overridable Property EventTypeID() As Integer?
        Get
          Return ReadProperty(EventTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(EventTypeIDProperty, Value)
        End Set
      End Property

      Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "Event Type", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Event Type", Description:="")>
      Public Overridable Property EventType() As String
        Get
          Return ReadProperty(EventTypeProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(EventTypeProperty, Value)
        End Set
      End Property

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Keyword, "")
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="Keyword", Description:="")>
      Public Overridable Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property
      '     Singular.DataAnnotations.JSRule(JavascriptCode:=OBLib.JSCode.ROProductionJS.OtherFilterFieldsValid)

      Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production", Description:="")>
      Public Overridable Property ProductionID() As Integer?
        Get
          Return ReadProperty(ProductionIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionVenueID, "Production Venue", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production Venue", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(ROProductionVenueOldList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete,
                                           UnselectedText:="Production Venue",
                                           LookupMember:="ProductionVenue", ValueMember:="ProductionVenueID",
                                           BeforeFetchJS:="ROProductionBO.setProductionVenueIDCriteriaBeforeRefresh",
                                           PreFindJSFunction:="ROProductionBO.triggerProductionVenueIDAutoPopulate")>
      Public Overridable Property ProductionVenueID() As Integer?
        Get
          Return ReadProperty(ProductionVenueIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionVenueIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Venue", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Event Type", Description:="")>
      Public Overridable Property ProductionVenue() As String
        Get
          Return ReadProperty(ProductionVenueProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionVenueProperty, Value)
        End Set
      End Property

      Public Shared ProductionGradeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionGradeID, "Production Grade", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production Grade", Description:="")>
      Public Overridable Property ProductionGradeID() As Integer?
        Get
          Return ReadProperty(ProductionGradeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionGradeIDProperty, Value)
        End Set
      End Property

      Public Shared EventManagerIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventManagerID, "Event Manager", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Event Manager", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(ROEventManagerList), DisplayMember:="PreferredFirstSurname",
                                           DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.AutoComplete, UnselectedText:="Event Manager",
                                           LookupMember:="EventManager", ValueMember:="HumanResourceID",
                                           BeforeFetchJS:="ROProductionBO.setEventManagerCriteriaBeforeRefresh",
                                           PreFindJSFunction:="ROProductionBO.triggerEventManagerAutoPopulate",
                                           DropDownColumns:={"PreferredFirstSurname"})>
      Public Overridable Property EventManagerID() As Integer?
        Get
          Return ReadProperty(EventManagerIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(EventManagerIDProperty, Value)
        End Set
      End Property

      Public Shared EventManagerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventManager, "Event Manager", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Event Manager", Description:="")>
      Public Overridable Property EventManager() As String
        Get
          Return ReadProperty(EventManagerProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(EventManagerProperty, Value)
        End Set
      End Property

      Public Shared NotProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NotProductionAreaStatusID, "Not Status", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Not Status", Description:="")>
      Public Overridable Property NotProductionAreaStatusID() As Integer?
        Get
          Return ReadProperty(NotProductionAreaStatusIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(NotProductionAreaStatusIDProperty, Value)
        End Set
      End Property

      Public Shared SynergyGenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.SynergyGenRefNo, "Gen Ref", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Gen Ref", Description:="")>
      Public Overridable Property SynergyGenRefNo() As Int64?
        Get
          Return ReadProperty(SynergyGenRefNoProperty)
        End Get
        Set(ByVal Value As Int64?)
          LoadProperty(SynergyGenRefNoProperty, Value)
        End Set
      End Property

      Public Shared ProductionRefNoProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionRefNo, "Ref Num", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Ref Num", Description:="")>
      Public Overridable Property ProductionRefNo() As Integer?
        Get
          Return ReadProperty(ProductionRefNoProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionRefNoProperty, Value)
        End Set
      End Property
      'Singular.DataAnnotations.JSRule(JavascriptCode:=OBLib.JSCode.ROProductionJS.OtherFilterFieldsValid)

      Public Shared FetchAllIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FetchAllInd, "Fetch All Ind", False)
      ''' <summary>
      ''' Gets and sets the Fetch All Ind value
      ''' </summary>
      <Display(Name:="Fetch All Ind", Description:=""),
      Browsable(False)>
      Public Overridable Property FetchAllInd() As Boolean
        Get
          Return ReadProperty(FetchAllIndProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(FetchAllIndProperty, Value)
        End Set
      End Property

      Public Shared ProductionTypeEventTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionTypeEventType, "")
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="ProductionTypeEventType", Description:="")>
      Public Overridable Property ProductionTypeEventType() As String
        Get
          Return ReadProperty(ProductionTypeEventTypeProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionTypeEventTypeProperty, Value)
        End Set
      End Property

      <Display(AutoGenerateField:=False),
      Browsable(True)>
      Public Property LoggedInSystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID

      Public Sub New(ProductionTypeID As Integer?, EventTypeID As Integer?,
                     ProductionVenueID As Integer?, SynergyGenRefNo As Int64?, ProductionGradeID As Integer?,
                     PlayStartDateTime As DateTime?, PlayEndDateTime As DateTime?, SystemID As Integer?,
                     TxDateFrom As DateTime?, TxDateTo As DateTime?, EventManagerID As Integer?, ProductionRefNo As Integer?,
                     NotProductionAreaStatusID As Integer?, ProductionAreaID As Integer?,
                     PageNo As Integer, PageSize As Integer,
                     SortAsc As Boolean, SortColumn As String)

        Me.ProductionID = ProductionID
        Me.ProductionTypeID = ProductionTypeID
        Me.EventTypeID = EventTypeID
        Me.ProductionVenueID = ProductionVenueID
        Me.SynergyGenRefNo = SynergyGenRefNo
        Me.ProductionGradeID = ProductionGradeID
        'Me.PlayStartDateTime = PlayStartDateTime
        'Me.PlayEndDateTime = PlayEndDateTime
        Me.SystemID = SystemID
        Me.TxDateFrom = TxDateFrom
        Me.TxDateTo = TxDateTo
        Me.EventManagerID = EventManagerID
        Me.ProductionRefNo = ProductionRefNo
        Me.NotProductionAreaStatusID = NotProductionAreaStatusID
        Me.ProductionAreaID = ProductionAreaID
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn

      End Sub

      Public Sub New(ProductionID As Integer?)

        Me.ProductionID = ProductionID

      End Sub

      Public Sub New()


      End Sub

    End Class

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class ProductionReportCriteria
      Inherits Criteria

      <Display(Name:="Event Type", Description:=""),
      Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROEventTypeList),
                                           DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
                                           UnselectedText:="Event Type",
                                           Source:=Singular.DataAnnotations.DropDownWeb.SourceType.CommonData)>
      Public Overrides Property EventTypeID() As Integer?
        Get
          Return ReadProperty(EventTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(EventTypeIDProperty, Value)
        End Set
      End Property

      <Browsable(False)>
      Public Overrides Property SystemID() As Integer? = OBLib.Security.Settings.CurrentUser.SystemID

      <Browsable(False)>
      Public Overrides Property ProductionAreaID() As Integer? = OBLib.Security.Settings.CurrentUser.ProductionAreaID

      <Browsable(False)>
      Public Overrides Property ProductionID() As Integer? = Nothing

      <Browsable(False)>
      Public Overrides Property NotProductionAreaStatusID() As Integer? = Nothing

      <Browsable(False)>
      Public Overrides Property SynergyGenRefNo() As Int64? = Nothing

      <Browsable(False)>
      Public Overrides Property FetchAllInd As Boolean = True

      Public Sub New(ProductionTypeID As Integer?, EventTypeID As Integer?,
                     ProductionVenueID As Integer?, SynergyGenRefNo As Int64?, ProductionGradeID As Integer?,
                     PlayStartDateTime As DateTime?, PlayEndDateTime As DateTime?, SystemID As Integer?,
                     TxDateFrom As DateTime?, TxDateTo As DateTime?, EventManagerID As Integer?, ProductionRefNo As Integer?,
                     NotProductionAreaStatusID As Integer?, ProductionAreaID As Integer?,
                     PageNo As Integer, PageSize As Integer,
                     SortAsc As Boolean, SortColumn As String)

        Me.ProductionID = ProductionID
        Me.ProductionTypeID = ProductionTypeID
        Me.EventTypeID = EventTypeID
        Me.ProductionVenueID = ProductionVenueID
        Me.SynergyGenRefNo = SynergyGenRefNo
        Me.ProductionGradeID = ProductionGradeID
        'Me.PlayStartDateTime = PlayStartDateTime
        'Me.PlayEndDateTime = PlayEndDateTime
        Me.SystemID = SystemID
        Me.TxDateFrom = TxDateFrom
        Me.TxDateTo = TxDateTo
        Me.EventManagerID = EventManagerID
        Me.ProductionRefNo = ProductionRefNo
        Me.NotProductionAreaStatusID = NotProductionAreaStatusID
        Me.ProductionAreaID = ProductionAreaID
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn

      End Sub

      Public Sub New(ProductionID As Integer?)

        Me.ProductionID = ProductionID

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionList() As ROProductionList

      Return New ROProductionList()

    End Function

    Public Shared Sub BeginGetROProductionList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionList)))

      Dim dp As New DataPortal(Of ROProductionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionList)))

      Dim dp As New DataPortal(Of ROProductionList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionList(Optional FetchAllInd As Boolean = False, Optional TxDateFrom As DateTime? = Nothing, Optional TxDateTo As DateTime? = Nothing) As ROProductionList

      Return DataPortal.Fetch(Of ROProductionList)(New Criteria() With {.FetchAllInd = FetchAllInd, .TxDateFrom = TxDateFrom, .TxDateTo = TxDateTo})

    End Function

    Public Shared Function GetROProductionList(ProductionID As Integer) As ROProductionList

      Return DataPortal.Fetch(Of ROProductionList)(New Criteria(ProductionID))

    End Function

    Public Shared Function GetROProductionList(ProductionTypeID As Integer?, EventTypeID As Integer?,
                                               ProductionVenueID As Integer?, SynergyGenRefNo As Int64?, ProductionGradeID As Integer?,
                                               PlayStartDateTime As DateTime?, PlayEndDateTime As DateTime?, SystemID As Integer?,
                                               TxDateFrom As DateTime?, TxDateTo As DateTime?, EventManagerID As Integer?,
                                               ProductionRefNo As Integer?, NotProductionAreaStatusID As Integer?, ProductionAreaID As Integer?,
                                               PageNo As Integer, PageSize As Integer,
                                               SortAsc As Boolean, SortColumn As String) As ROProductionList

      Return DataPortal.Fetch(Of ROProductionList)(New Criteria(ProductionTypeID, EventTypeID, ProductionVenueID, SynergyGenRefNo,
                                                                ProductionGradeID, PlayStartDateTime, PlayEndDateTime, SystemID,
                                                                TxDateFrom, TxDateTo, EventManagerID, ProductionRefNo, NotProductionAreaStatusID, ProductionAreaID,
                                                                PageNo, PageSize,
                                                                SortAsc, SortColumn))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      'mTotalPages = sdr.GetInt32(1)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProduction.GetROProduction(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionList"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@EventTypeID", NothingDBNull(crit.EventTypeID))
            cm.Parameters.AddWithValue("@ProductionVenueID", NothingDBNull(crit.ProductionVenueID))
            cm.Parameters.AddWithValue("@SynergyGenRefNo", NothingDBNull(crit.SynergyGenRefNo))
            cm.Parameters.AddWithValue("@ProductionGradeID", NothingDBNull(crit.ProductionGradeID))
            cm.Parameters.AddWithValue("@PlayStartDateTime", NothingDBNull(crit.TxDateFrom))
            cm.Parameters.AddWithValue("@PlayEndDateTime", NothingDBNull(crit.TxDateTo))
            cm.Parameters.AddWithValue("@TxDateFrom", NothingDBNull(crit.TxDateFrom))
            cm.Parameters.AddWithValue("@TxDateTo", NothingDBNull(crit.TxDateTo))
            cm.Parameters.AddWithValue("@EventManagerID", NothingDBNull(crit.EventManagerID))
            cm.Parameters.AddWithValue("@ProductionRefNo", NothingDBNull(crit.ProductionRefNo))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@Keyword", Singular.Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@NotProductionAreaStatusID", NothingDBNull(crit.NotProductionAreaStatusID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@ProductionServicesInd", NothingDBNull(crit.ProductionServicesInd))
            cm.Parameters.AddWithValue("@ProductionContentInd", NothingDBNull(crit.ProductionContentInd))
            cm.Parameters.AddWithValue("@OutsideBroadcastInd", NothingDBNull(crit.OutsideBroadcastInd))
            cm.Parameters.AddWithValue("@StudioInd", NothingDBNull(crit.StudioInd))
            cm.Parameters.AddWithValue("@OBVan", NothingDBNull(crit.OBVan))
            cm.Parameters.AddWithValue("@Externalvan", NothingDBNull(crit.Externalvan))
            'cm.Parameters.AddWithValue("@FetchAllInd", NothingDBNull(crit.FetchAllInd))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace