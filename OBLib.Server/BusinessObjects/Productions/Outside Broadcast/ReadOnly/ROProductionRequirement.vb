﻿' Generated 29 Jan 2016 16:41 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Productions.ReadOnly

  <Serializable()> _
  Public Class ROOBProductionRequirement
    Inherits OBReadOnlyBase(Of ROOBProductionRequirement)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared FlightHRCostsValidProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FlightHRCostsValid, "ID", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property FlightHRCostsValid() As Boolean
      Get
        Return GetProperty(FlightHRCostsValidProperty)
      End Get
    End Property

    Public Shared ErrorMessageProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ErrorMessage, "Error Message")
    ''' <summary>
    ''' Gets the Error Message value
    ''' </summary>
    <Display(Name:="Error Message", Description:="")>
    Public ReadOnly Property ErrorMessage() As String
      Get
        Return GetProperty(ErrorMessageProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(FlightHRCostsValidProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ErrorMessage

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROOBProductionRequirement(dr As SafeDataReader) As ROOBProductionRequirement

      Dim r As New ROOBProductionRequirement()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(FlightHRCostsValidProperty, .GetBoolean(0))
        LoadProperty(ErrorMessageProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
