﻿' Generated 29 Jan 2016 16:40 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Productions.ReadOnly

  <Serializable()> _
  Public Class ROOBProductionRequirementList
    Inherits OBReadOnlyListBase(Of ROOBProductionRequirementList, ROOBProductionRequirement)

#Region " Business Methods "

    Public Function GetItem(FlightHRCostsValid As Boolean) As ROOBProductionRequirement

      For Each child As ROOBProductionRequirement In Me
        If child.FlightHRCostsValid = FlightHRCostsValid Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing

      Public Sub New(ProductionID As Integer?, SystemID As Integer?)
        Me.ProductionID = ProductionID
        Me.SystemID = SystemID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROOBProductionRequirementList() As ROOBProductionRequirementList

      Return New ROOBProductionRequirementList()

    End Function

    Public Shared Sub BeginGetROOBProductionRequirementList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROOBProductionRequirementList)))

      Dim dp As New DataPortal(Of ROOBProductionRequirementList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROOBProductionRequirementList(CallBack As EventHandler(Of DataPortalResult(Of ROOBProductionRequirementList)))

      Dim dp As New DataPortal(Of ROOBProductionRequirementList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROOBProductionRequirementList() As ROOBProductionRequirementList

      Return DataPortal.Fetch(Of ROOBProductionRequirementList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROOBProductionRequirement.GetROOBProductionRequirement(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROOBProductionRequirementList"
            cm.Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace