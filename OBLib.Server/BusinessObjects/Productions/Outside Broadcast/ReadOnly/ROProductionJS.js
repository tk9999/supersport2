﻿function FilterIsValidTo(Value, Element, Args) {
  var ROProdCrit = CtlError.Object;
  if (ROProdCrit.LoggedInSystemID() == 1 && ROProdCrit.TxDateTo() == null) {
    CtlError.AddError("Tx Date To Required")
  }
};

function FilterIsValidFrom(Value, Element, Args) {
  var ROProdCrit = CtlError.Object;
  if (ROProdCrit.LoggedInSystemID() == 1 && ROProdCrit.TxDateFrom() == null) {
    CtlError.AddError("Tx Date From Required");
  }
};

function OtherFilterFieldsValid(Value, Element, Args) {
  var ROProdCrit = CtlError.Object;
  if (ROProdCrit.LoggedInSystemID() == 2 &&
      ROProdCrit.TxDateTo() == null &&
      ROProdCrit.TxDateFrom() == null &&
      ROProdCrit.ProductionRefNo() == null &&
      ROProdCrit.EventManagerID() == null &&
      ROProdCrit.ProductionVenueID() == null &&
      ROProdCrit.EventTypeID() == null &&
      ROProdCrit.ProductionTypeID() == null &&
      ROProdCrit.TxDateTo() == null) {

    CtlError.AddError("At least one of these fields is required");

  }
};