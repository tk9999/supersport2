﻿' Generated 27 Jul 2015 09:45 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Productions.ContentOB

  <Serializable()> _
  Public Class GraphicsSupplier
    Inherits OBBusinessBase(Of GraphicsSupplier)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SupplierID() As Integer
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared SupplierProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Supplier, "Supplier")
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="")>
    Public ReadOnly Property Supplier() As String
      Get
        Return GetProperty(SupplierProperty)
      End Get
    End Property

    Public Shared TelNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TelNo, "Tel No")
    ''' <summary>
    ''' Gets the Tel No value
    ''' </summary>
    <Display(Name:="Tel No", Description:="")>
    Public ReadOnly Property TelNo() As String
      Get
        Return GetProperty(TelNoProperty)
      End Get
    End Property

    Public Shared FaxNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FaxNo, "Fax No")
    ''' <summary>
    ''' Gets the Fax No value
    ''' </summary>
    <Display(Name:="Fax No", Description:="")>
    Public ReadOnly Property FaxNo() As String
      Get
        Return GetProperty(FaxNoProperty)
      End Get
    End Property

    Public Shared ContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactName, "Contact Name")
    ''' <summary>
    ''' Gets the Contact Name value
    ''' </summary>
    <Display(Name:="Contact Name", Description:="")>
    Public ReadOnly Property ContactName() As String
      Get
        Return GetProperty(ContactNameProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared VatNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VatNo, "Vat No")
    ''' <summary>
    ''' Gets the Vat No value
    ''' </summary>
    <Display(Name:="Vat No", Description:="")>
    Public ReadOnly Property VatNo() As String
      Get
        Return GetProperty(VatNoProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SelectedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SelectedInd, "Selected Ind", False)
    Public Property SelectedInd() As Boolean
      Get
        Return GetProperty(SelectedIndProperty)
      End Get
      Set(value As Boolean)
        SetProperty(SelectedIndProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SupplierIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Supplier.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Graphics Supplier")
        Else
          Return String.Format("Blank {0}", "Graphics Supplier")
        End If
      Else
        Return Me.Supplier
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewGraphicsSupplier() method.

    End Sub

    Public Shared Function NewGraphicsSupplier() As GraphicsSupplier

      Return DataPortal.CreateChild(Of GraphicsSupplier)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetGraphicsSupplier(dr As SafeDataReader) As GraphicsSupplier

      Dim g As New GraphicsSupplier()
      g.Fetch(dr)
      Return g

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SupplierIDProperty, .GetInt32(0))
          LoadProperty(SupplierProperty, .GetString(1))
          LoadProperty(TelNoProperty, .GetString(2))
          LoadProperty(FaxNoProperty, .GetString(3))
          LoadProperty(ContactNameProperty, .GetString(4))
          LoadProperty(EmailAddressProperty, .GetString(5))
          LoadProperty(VatNoProperty, .GetString(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insGraphicsSupplier"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updGraphicsSupplier"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSupplierID As SqlParameter = .Parameters.Add("@SupplierID", SqlDbType.Int)
          paramSupplierID.Value = GetProperty(SupplierIDProperty)
          If Me.IsNew Then
            paramSupplierID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Supplier", GetProperty(SupplierProperty))
          .Parameters.AddWithValue("@TelNo", GetProperty(TelNoProperty))
          .Parameters.AddWithValue("@FaxNo", GetProperty(FaxNoProperty))
          .Parameters.AddWithValue("@ContactName", GetProperty(ContactNameProperty))
          .Parameters.AddWithValue("@EmailAddress", GetProperty(EmailAddressProperty))
          .Parameters.AddWithValue("@VatNo", GetProperty(VatNoProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SupplierIDProperty, paramSupplierID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delGraphicsSupplier"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SupplierID", GetProperty(SupplierIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class
End Namespace
