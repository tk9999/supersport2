﻿' Generated 27 Jul 2015 11:19 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Productions.ContentOB

  <Serializable()> _
  Public Class ProductionSupplierMapping
    Inherits OBBusinessBase(Of ProductionSupplierMapping)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionSupplierMappingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSupplierMappingID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ProductionSupplierMappingID() As Integer
      Get
        Return GetProperty(ProductionSupplierMappingIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="ID of Linked Production"),
    Required(ErrorMessage:="Production required")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="ID of Linked Supplier"),
    Required(ErrorMessage:="Supplier required")>
    Public Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' Required(ErrorMessage:="Production System Area required")
    ''' </summary>
    <Display(Name:="Production System Area", Description:="ID of Linked System Area")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared OutSourceServiceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OutSourceServiceTypeID, "Outsource Service Type", Nothing)
    ''' <summary>
    ''' Gets and sets the OutSource Service Type ID value
    ''' </summary>
    <Display(Name:="ID of Outsource Service Type", Description:="ID of Outsource Service Type"),
    Required(ErrorMessage:="Outsource Service Type required")>
    Public Property OutSourceServiceTypeID() As Integer?
      Get
        Return GetProperty(OutSourceServiceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OutSourceServiceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Supplier", OBLib.Security.Settings.CurrentUser.SystemID)
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area ID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSupplierMappingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionSupplierMappingID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Supplier Mapping")
        Else
          Return String.Format("Blank {0}", "Production Supplier Mapping")
        End If
      Else
        Return Me.ProductionSupplierMappingID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionSupplierMapping() method.

    End Sub

    Public Shared Function NewProductionSupplierMapping() As ProductionSupplierMapping

      Return DataPortal.CreateChild(Of ProductionSupplierMapping)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionSupplierMapping(dr As SafeDataReader) As ProductionSupplierMapping

      Dim p As New ProductionSupplierMapping()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          ' LoadProperty(ProductionSupplierMappingIDProperty, .GetInt32(0))
          ' LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          ' LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          ' LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          ' LoadProperty(OutSourceServiceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          ' LoadProperty(CreatedByProperty, .GetInt32(5))
          ' LoadProperty(CreatedDateTimeProperty, .GetDateTime(6))
          ' LoadProperty(ModifiedByProperty, .GetInt32(7))
          ' LoadProperty(ModifiedDateTimeProperty, .GetDateTime(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionSupplierMapping"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionSupplierMapping"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          'Dim paramProductionSupplierMappingID As SqlParameter = .Parameters.Add("@ProductionSupplierMappingID", SqlDbType.Int)
          'paramProductionSupplierMappingID.Value = GetProperty(ProductionSupplierMappingIDProperty)
          'If Me.IsNew Then
          '  paramProductionSupplierMappingID.Direction = ParameterDirection.Output
          'End If
          .Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
          .Parameters.AddWithValue("@SupplierID", GetProperty(SupplierIDProperty))
          .Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.ZeroNothingDBNull(GetProperty(ProductionSystemAreaIDProperty)))
          .Parameters.AddWithValue("@OutsourceServiceTypeID", GetProperty(OutSourceServiceTypeIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .ExecuteNonQuery()

          'If Me.IsNew Then
          '  LoadProperty(ProductionSupplierMappingIDProperty, paramProductionSupplierMappingID.Value)
          'End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionSupplierMapping"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionSupplierMappingID", GetProperty(ProductionSupplierMappingIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace