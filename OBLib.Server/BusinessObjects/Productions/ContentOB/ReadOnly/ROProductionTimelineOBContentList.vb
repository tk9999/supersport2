﻿Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Productions.Base
Imports OBLib.Productions.Base.ReadOnly

Namespace Productions.OB

  <Serializable()> _
  Public Class ROProductionTimelineListOBContent
    Inherits ROProductionTimelineBaseList(Of ROProductionTimelineListOBContent, ROProductionTimelineOBContent)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

    <Serializable(), Singular.Web.WebFetchable()> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property ProductionID As Integer? = Nothing
      Public Property ProductionSystemAreaID As Integer? = Nothing

      Public Sub New(ProductionID As Integer?, ProductionSystemAreaID As Integer?)
        Me.ProductionID = ProductionID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Overrides Sub Fetch(sdr As Csla.Data.SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionTimelineOBContent.GetROProductionTimelineOBContent(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionTimelineListOBContent"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

  End Class

End Namespace
