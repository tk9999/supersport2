﻿Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Productions.Base
Imports OBLib.Productions.Base.ReadOnly

Namespace Productions.OB

  <Serializable()> _
  Public Class ROProductionTimelineOBContent
    Inherits ROProductionTimelineBase(Of ROProductionTimelineOBContent)

    Public Shared MyAreaTimelineProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MyAreaTimeline, "My Area Timeline", False)
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="My Area Timeline", Description:="The type of timeline item")>
    Public ReadOnly Property MyAreaTimeline() As Boolean
      Get
        Return GetProperty(MyAreaTimelineProperty)
      End Get
    End Property

    <Browsable(False)>
    Public ReadOnly Property CanEditBuildUpForAllAreas As Boolean
      Get
        Return Singular.Security.HasAccess("Productions", "Can Edit Build-Up For All Areas")
      End Get
    End Property

    Public ReadOnly Property CanEdit As Boolean
      Get
        If MyAreaTimeline Then
          Return True
        Else
          If CompareSafe(ProductionTimelineTypeID, CType(OBLib.CommonData.Enums.ProductionTimelineType.Transmission, Integer)) _
             Or CompareSafe(ProductionTimelineTypeID, CType(OBLib.CommonData.Enums.ProductionTimelineType.BuildUp, Integer)) Then
            Return CanEditBuildUpForAllAreas
          Else
            Return False
          End If
        End If
      End Get
    End Property

    Public Overrides Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)

    End Sub

    Public Shared Function GetROProductionTimelineOBContent(dr As SafeDataReader) As ROProductionTimelineOBContent

      Dim r As New ROProductionTimelineOBContent
      r.Fetch(dr)
      Return r

    End Function

  End Class


End Namespace