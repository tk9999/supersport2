﻿' Generated 07 Sep 2015 17:12 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.OB

  <Serializable()> _
  Public Class ROProductionHumanResourceOBContent
    Inherits SingularReadOnlyBase(Of ROProductionHumanResourceOBContent)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionHumanResourceID() As Integer
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Production System Area")
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
    Public ReadOnly Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionID, "Position")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property PositionID() As Integer
      Get
        Return GetProperty(PositionIDProperty)
      End Get
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionTypeID, "Position Type")
    ''' <summary>
    ''' Gets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
    Public ReadOnly Property PositionTypeID() As Integer
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
    End Property

    Public Shared PreferredHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PreferredHumanResourceID, "Preferred Human Resource")
    ''' <summary>
    ''' Gets the Preferred Human Resource value
    ''' </summary>
    <Display(Name:="Preferred Human Resource", Description:="")>
    Public ReadOnly Property PreferredHumanResourceID() As Integer
      Get
        Return GetProperty(PreferredHumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared TrainingIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TrainingInd, "Training", False)
    ''' <summary>
    ''' Gets the Training value
    ''' </summary>
    <Display(Name:="Training", Description:="")>
    Public ReadOnly Property TrainingInd() As Boolean
      Get
        Return GetProperty(TrainingIndProperty)
      End Get
    End Property

    Public Shared TBCIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TBCInd, "TBC", False)
    ''' <summary>
    ''' Gets the TBC value
    ''' </summary>
    <Display(Name:="TBC", Description:="")>
    Public ReadOnly Property TBCInd() As Boolean
      Get
        Return GetProperty(TBCIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
    End Property

    Public Shared PositionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionType, "Position Type")
    ''' <summary>
    ''' Gets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
    Public ReadOnly Property PositionType() As String
      Get
        Return GetProperty(PositionTypeProperty)
      End Get
    End Property

    Public Shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceName, "Human Resource Name")
    ''' <summary>
    ''' Gets the Human Resource Name value
    ''' </summary>
    <Display(Name:="Human Resource Name", Description:="")>
    Public ReadOnly Property HumanResourceName() As String
      Get
        Return GetProperty(HumanResourceNameProperty)
      End Get
    End Property

    Public Shared PreferredHumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredHumanResourceName, "Preferred Human Resource Name")
    ''' <summary>
    ''' Gets the Preferred Human Resource Name value
    ''' </summary>
    <Display(Name:="Preferred Human Resource Name", Description:="")>
    Public ReadOnly Property PreferredHumanResourceName() As String
      Get
        Return GetProperty(PreferredHumanResourceNameProperty)
      End Get
    End Property

    Public Shared OrderNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNo, "Order No")
    ''' <summary>
    ''' Gets the Order No value
    ''' </summary>
    <Display(Name:="Order No", Description:="")>
    Public ReadOnly Property OrderNo() As Integer
      Get
        Return GetProperty(OrderNoProperty)
      End Get
    End Property

    Public Shared PriorityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Priority, "Priority")
    ''' <summary>
    ''' Gets the Priority value
    ''' </summary>
    <Display(Name:="Priority", Description:="")>
    Public ReadOnly Property Priority() As Integer
      Get
        Return GetProperty(PriorityProperty)
      End Get
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:="")>
    Public ReadOnly Property RowNo() As Integer
      Get
        Return GetProperty(RowNoProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractTypeID, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractTypeID() As Integer
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared FreelancerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FreelancerInd, "Freelancer", False)
    ''' <summary>
    ''' Gets the Freelancer value
    ''' </summary>
    <Display(Name:="Freelancer", Description:="")>
    Public ReadOnly Property FreelancerInd() As Boolean
      Get
        Return GetProperty(FreelancerIndProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Comments

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionHumanResourceOBContent(dr As SafeDataReader) As ROProductionHumanResourceOBContent

      Dim r As New ROProductionHumanResourceOBContent()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(PreferredHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(CommentsProperty, .GetString(9))
        LoadProperty(TrainingIndProperty, .GetBoolean(10))
        LoadProperty(TBCIndProperty, .GetBoolean(11))
        LoadProperty(CreatedByProperty, .GetInt32(12))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(13))
        LoadProperty(ModifiedByProperty, .GetInt32(14))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(15))
        LoadProperty(DisciplineProperty, .GetString(16))
        LoadProperty(PositionProperty, .GetString(17))
        LoadProperty(PositionTypeProperty, .GetString(18))
        LoadProperty(HumanResourceNameProperty, .GetString(19))
        LoadProperty(PreferredHumanResourceNameProperty, .GetString(20))
        LoadProperty(OrderNoProperty, .GetInt32(21))
        LoadProperty(PriorityProperty, .GetInt32(22))
        LoadProperty(RowNoProperty, .GetInt32(23))
        LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
        LoadProperty(FreelancerIndProperty, .GetBoolean(25))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace