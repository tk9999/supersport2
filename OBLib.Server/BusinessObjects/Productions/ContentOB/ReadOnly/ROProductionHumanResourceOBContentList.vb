﻿' Generated 07 Sep 2015 17:12 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.OB

  <Serializable()> _
  Public Class ROProductionHumanResourceOBContentList
    Inherits SingularReadOnlyListBase(Of ROProductionHumanResourceOBContentList, ROProductionHumanResourceOBContent)
    Implements Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionHumanResourceID As Integer) As ROProductionHumanResourceOBContent

      For Each child As ROProductionHumanResourceOBContent In Me
        If child.ProductionHumanResourceID = ProductionHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable()> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property ProductionSystemAreaID As Integer? = Nothing
      Public Property Discipline As String = ""
      Public Property Position As String = ""
      Public Property PositionType As String = ""
      Public Property HumanResourceName As String = ""

      Public Sub New(ProductionSystemAreaID As Integer?, Discipline As String, Position As String, _
                     PositionType As String, HumanResourceName As String)

        Me.ProductionSystemAreaID = ProductionSystemAreaID
        Me.Discipline = Discipline
        Me.Position = Position
        Me.PositionType = PositionType
        Me.HumanResourceName = HumanResourceName

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionHumanResourceOBContentList() As ROProductionHumanResourceOBContentList

      Return New ROProductionHumanResourceOBContentList()

    End Function

    Public Shared Sub BeginGetROProductionHumanResourceOBContentList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionHumanResourceOBContentList)))

      Dim dp As New DataPortal(Of ROProductionHumanResourceOBContentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionHumanResourceOBContentList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionHumanResourceOBContentList)))

      Dim dp As New DataPortal(Of ROProductionHumanResourceOBContentList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionHumanResourceOBContentList() As ROProductionHumanResourceOBContentList

      Return DataPortal.Fetch(Of ROProductionHumanResourceOBContentList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)


      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionHumanResourceOBContent.GetROProductionHumanResourceOBContent(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionHumanResourceListOBContent"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@Discipline", Singular.Strings.MakeEmptyDBNull(crit.Discipline))
            cm.Parameters.AddWithValue("@Position", Singular.Strings.MakeEmptyDBNull(crit.Position))
            cm.Parameters.AddWithValue("@PositionType", Singular.Strings.MakeEmptyDBNull(crit.PositionType))
            cm.Parameters.AddWithValue("@HumanResourceName", Singular.Strings.MakeEmptyDBNull(crit.HumanResourceName))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace