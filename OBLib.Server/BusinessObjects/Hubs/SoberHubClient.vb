﻿Imports System.IO
Imports System.Runtime.Serialization.Json
Imports OBLib.Resources
Imports System
Imports System.Web
Imports Microsoft.AspNet.SignalR
Imports System.Threading.Tasks
Imports Singular.Misc
Imports System.Reflection
Imports System.Text
Imports Microsoft.AspNet.SignalR.Client
Imports System.Net

Public Class SoberHubClient

  Private Shared mLiveHubConn As HubConnection
  Private Shared mLiveServerHub As Microsoft.AspNet.SignalR.Client.IHubProxy
  Public Shared ReadOnly Property LiveServerHub As Microsoft.AspNet.SignalR.Client.IHubProxy
    Get
      If mLiveHubConn Is Nothing Or mLiveServerHub Is Nothing Then
        mLiveHubConn = New HubConnection("https://soberms.supersport.com/")
        mLiveServerHub = mLiveHubConn.CreateHubProxy("SiteHub")
      End If
      If mLiveHubConn.State <> ConnectionState.Connected Then
        mLiveHubConn.Start.Wait()
        Return mLiveServerHub
      Else
        Return mLiveServerHub
      End If
    End Get
  End Property

  Private Shared mLiveBackupHubConn As HubConnection
  Private Shared mLiveBackupServerHub As Microsoft.AspNet.SignalR.Client.IHubProxy
  Public Shared ReadOnly Property LiveBackupServerHub As Microsoft.AspNet.SignalR.Client.IHubProxy
    Get
      If mLiveBackupHubConn Is Nothing Or mLiveBackupServerHub Is Nothing Then
        mLiveBackupHubConn = New HubConnection("https://internal-sober.supersport.com/")
        mLiveBackupServerHub = mLiveBackupHubConn.CreateHubProxy("SiteHub")
      End If
      If mLiveBackupHubConn.State <> ConnectionState.Connected Then
        mLiveBackupHubConn.Start.Wait()
        Return mLiveBackupServerHub
      Else
        Return mLiveBackupServerHub
      End If
    End Get
  End Property

  Private Shared mLocalHubConn As HubConnection
  Private Shared mLocalServerHub As Microsoft.AspNet.SignalR.Client.IHubProxy
  Public Shared ReadOnly Property LocalServerHub As Microsoft.AspNet.SignalR.Client.IHubProxy
    Get
      If mLocalHubConn Is Nothing Or mLocalServerHub Is Nothing Then
        mLocalHubConn = New HubConnection("http://localhost/NewOBWebNextVersion")
        mLocalServerHub = mLocalHubConn.CreateHubProxy("SiteHub")
      End If
      If mLocalHubConn.State <> ConnectionState.Connected Then
        mLocalHubConn.Start.Wait()
        Return mLocalServerHub
      Else
        Return mLocalServerHub
      End If
    End Get
  End Property

  Shared Sub SendFeedEscalations()

    Try
      Dim externalSiteRequest As HttpWebRequest = HttpWebRequest.Create("https://soberms.supersport.com/SigR/SignalRWindowsReceiver.aspx")
      Dim prxy1 As IWebProxy = WebRequest.GetSystemWebProxy()
      externalSiteRequest.Proxy = prxy1
      Dim extResponse As HttpWebResponse = externalSiteRequest.GetResponse()
    Catch ex As Exception
      OBMisc.LogClientError("SoberHubClient", "SoberHubClient", "SendFeedEscalations - External", ex.Message)
    End Try

    Try
      Dim internalSiteRequest As HttpWebRequest = HttpWebRequest.Create("https://internal-sober.supersport.com/SigR/SignalRWindowsReceiver.aspx")
      Dim prxy2 As IWebProxy = WebRequest.GetSystemWebProxy()
      internalSiteRequest.Proxy = prxy2
      Dim intResponse As HttpWebResponse = internalSiteRequest.GetResponse()
    Catch ex As Exception
      OBMisc.LogClientError("SoberHubClient", "SoberHubClient", "SendFeedEscalations - Internal", ex.Message)
    End Try

  End Sub

  Shared Sub AfterSynergyImportRun(SynergyImportID As Integer)

    Dim ID As String = SynergyImportID.ToString

    Try
      Dim externalSiteRequest As HttpWebRequest = HttpWebRequest.Create("https://soberms.supersport.com/SigR/SignalRWindowsReceiver.aspx?A=Synergy&ID=" & ID)
      Dim prxy1 As IWebProxy = WebRequest.GetSystemWebProxy()
      externalSiteRequest.Proxy = prxy1
      Dim extResponse As HttpWebResponse = externalSiteRequest.GetResponse()
    Catch ex As Exception
      OBMisc.LogClientError("SoberHubClient", "SoberHubClient", "AfterSynergyImportRun - External", ex.Message)
    End Try

    Try
      Dim internalSiteRequest As HttpWebRequest = HttpWebRequest.Create("https://internal-sober.supersport.com/SigR/SignalRWindowsReceiver.aspx?A=Synergy&ID=" & ID)
      Dim prxy2 As IWebProxy = WebRequest.GetSystemWebProxy()
      internalSiteRequest.Proxy = prxy2
      Dim intResponse As HttpWebResponse = internalSiteRequest.GetResponse()
    Catch ex As Exception
      OBMisc.LogClientError("SoberHubClient", "SoberHubClient", "AfterSynergyImportRun - Internal", ex.Message)
    End Try

    If Singular.Debug.InDebugMode Then
      Try
        Dim localSiteRequest As HttpWebRequest = HttpWebRequest.Create("http://localhost/NewOBWebNextVersion/SigR/SignalRWindowsReceiver.aspx?A=Synergy&ID=" & ID)
        Dim prxy3 As IWebProxy = WebRequest.GetSystemWebProxy()
        localSiteRequest.Proxy = prxy3
        Dim intResponse As HttpWebResponse = localSiteRequest.GetResponse()
      Catch ex As Exception
        OBMisc.LogClientError("SoberHubClient", "SoberHubClient", "AfterSynergyImportRun - Local", ex.Message)
      End Try
    End If

  End Sub

End Class
