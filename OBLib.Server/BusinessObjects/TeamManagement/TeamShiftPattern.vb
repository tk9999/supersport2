﻿' Generated 09 Dec 2015 18:10 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.PlayOutOps.ReadOnly
Imports OBLib.Maintenance.ICR.ReadOnly
Imports OBLib.Maintenance.ShiftPatterns.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace TeamManagement

  <Serializable()> _
  Public Class TeamShiftPattern
    Inherits OBBusinessBase(Of TeamShiftPattern)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TeamShiftPatternIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TeamShiftPatternID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property TeamShiftPatternID() As Integer
      Get
        Return GetProperty(TeamShiftPatternIDProperty)
      End Get
    End Property

    Public Shared SystemAreaShiftPatternIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemAreaShiftPatternID, "Pattern", Nothing)
    ''' <summary>
    ''' Gets and sets the System Area Shift Pattern value
    ''' </summary>
    <Display(Name:="Pattern", Description:=""), Required(ErrorMessage:="Shift Pattern Required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSystemAreaShiftPatternList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
      Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel, UnselectedText:="Shift Pattern", FilterMethodName:="TeamShiftPatternBO.FilterPatterns")>
    Public Property SystemAreaShiftPatternID() As Integer?
      Get
        Return GetProperty(SystemAreaShiftPatternIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemAreaShiftPatternIDProperty, Value)
      End Set
    End Property

    Public Shared PatternNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PatternName, "System Area Shift Pattern Name", "")
    ''' <summary>
    ''' Gets and sets the System Area Shift Pattern Name value
    ''' </summary>
    Public Property PatternName() As String
      Get
        Return GetProperty(PatternNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PatternNameProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemTeamID, "System Team", Nothing)
    ''' <summary>
    ''' Gets and sets the System Team value
    ''' </summary>
    '<Display(Name:="System Team", Description:=""),
    'Required(ErrorMessage:="Team required"),
    'Singular.DataAnnotations.DropDownWeb(GetType(ROTeamNPList), Source:=Singular.DataAnnotations.DropDownWeb.SourceType.ViewModel,
    '  DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, UnselectedText:="Team")>
    Public Property SystemTeamID() As Integer?
      Get
        Return GetProperty(SystemTeamIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemTeamIDProperty, Value)
      End Set
    End Property

    Public Shared TeamNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamName, "Team Name", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    ''' 
    <Display(Name:="Team Name", Description:=""),
    Required(ErrorMessage:="Team Name Required")>
    Public Property TeamName() As String
      Get
        Return GetProperty(TeamNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TeamNameProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    Required(ErrorMessage:="End Date required")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared HROnTeamProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HROnTeam, "HR On Team", Nothing)
    ''' <summary>
    ''' Gets the number of people on a Team
    ''' </summary>
    Public ReadOnly Property HROnTeam() As Integer?
      Get
        Return GetProperty(HROnTeamProperty)
      End Get
    End Property

    Public Shared SchedulesGeneratedProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SchedulesGenerated, "Schedules Generated", 0)
    ''' <summary>
    ''' Gets the number of Schedules Generated for HR on this team
    ''' </summary>
    <Display(Name:="Schedules Generated", Description:="")>
    Public ReadOnly Property SchedulesGenerated() As Integer
      Get
        Return GetProperty(SchedulesGeneratedProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TeamShiftPatternIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Team Shift Pattern")
        Else
          Return String.Format("Blank {0}", "Team Shift Pattern")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      'With AddWebRule(SystemTeamIDProperty)
      '  .AffectedProperties.Add(StartDateProperty)
      '  .AffectedProperties.Add(EndDateProperty)
      '  .JavascriptRuleFunctionName = "TeamShiftPatternBO.CheckforDuplicates"
      'End With

      With AddWebRule(EndDateProperty)
        .AffectedProperties.Add(StartDateProperty)
        .AddTriggerProperty(StartDateProperty)
        .JavascriptRuleFunctionName = "TeamShiftPatternBO.StartBeforeEnd"
        .ServerRuleFunction = AddressOf StartBeforeEnd
      End With

      With AddWebRule(SystemAreaShiftPatternIDProperty)
        .AffectedProperties.Add(PatternNameProperty)
        .JavascriptRuleFunctionName = "TeamShiftPatternBO.HasPatternName"
        .ServerRuleFunction = AddressOf HasPatternName
      End With

    End Sub

    Public Function StartBeforeEnd(TSP As TeamShiftPattern) As String
      Dim ErrString = ""
      If TSP.EndDate IsNot Nothing AndAlso TSP.StartDate IsNot Nothing Then
        If TSP.EndDate < TSP.StartDate Then
          ErrString = "Start date must be before end date"
        End If
      End If
      Return ErrString
    End Function

    Public Function HasPatternName(TSP As TeamShiftPattern) As String
      Dim ErrString = ""
      'If TSP.PatternName IsNot Nothing Then
      '  If TSP.PatternName.ToString.Trim = "" Then
      '    ErrString = "Pattern Name Required"
      '  End If
      'End If
      Return ErrString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTeamShiftPattern() method.

    End Sub

    Public Shared Function NewTeamShiftPattern() As TeamShiftPattern

      Return DataPortal.CreateChild(Of TeamShiftPattern)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTeamShiftPattern(dr As SafeDataReader) As TeamShiftPattern

      Dim t As New TeamShiftPattern()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TeamShiftPatternIDProperty, .GetInt32(0))
          LoadProperty(SystemAreaShiftPatternIDProperty, .GetInt32(1))
          LoadProperty(SystemTeamIDProperty, .GetInt32(2))
          LoadProperty(StartDateProperty, .GetValue(3))
          LoadProperty(EndDateProperty, .GetValue(4))
          LoadProperty(PatternNameProperty, .GetString(5))
          LoadProperty(TeamNameProperty, .GetString(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(HROnTeamProperty, .GetInt32(11))
          LoadProperty(SchedulesGeneratedProperty, .GetInt32(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTeamShiftPattern"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTeamShiftPattern"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTeamShiftPatternID As SqlParameter = .Parameters.Add("@TeamShiftPatternID", SqlDbType.Int)
          paramTeamShiftPatternID.Value = GetProperty(TeamShiftPatternIDProperty)
          If Me.IsNew Then
            paramTeamShiftPatternID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SystemAreaShiftPatternID", GetProperty(SystemAreaShiftPatternIDProperty))
          .Parameters.AddWithValue("@SystemTeamID", GetProperty(SystemTeamIDProperty))
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(TeamShiftPatternIDProperty, paramTeamShiftPatternID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTeamShiftPattern"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TeamShiftPatternID", GetProperty(TeamShiftPatternIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace