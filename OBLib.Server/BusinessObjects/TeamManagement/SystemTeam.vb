﻿' Generated 10 Dec 2015 09:50 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Shifts.ICR

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace TeamManagement

  <Serializable()> _
  Public Class SystemTeam
    Inherits OBBusinessBase(Of SystemTeam)

#Region " Properties and Methods "

#Region " Properties "

    <DefaultValue(False), Singular.DataAnnotations.ClientOnlyNoData()>
    Public Property IsExpanded As Boolean = False

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return TeamBO.ToString(self)")

    Public Shared SystemTeamIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTeamID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemTeamID() As Integer
      Get
        Return GetProperty(SystemTeamIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System ID value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept Required"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), UnselectedText:="Sub-Dept")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared TeamNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamName, "Team Name", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="Team Name", Description:="Team Name"), Required(ErrorMessage:="Team Name Required"),
    StringLength(100, ErrorMessage:="Team Name cannot be more than 100 characters")>
    Public Property TeamName() As String
      Get
        Return GetProperty(TeamNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TeamNameProperty, Value)
      End Set
    End Property

    'Public Shared TeamNoProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.TeamNo, 0)
    ' ''' <summary>
    ' ''' Gets and sets the Team No value
    ' ''' </summary>
    '<Display(Name:="Team Number", Description:=""), Required(ErrorMessage:="Team Number Required"),
    'Singular.DataAnnotations.DropDownWeb(GetType(ROTeamNumberList),
    '                                                DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
    '                                                UnselectedText:="Team Number",
    '                                                ValueMember:="TeamNumber",
    '                                                DisplayMember:="TeamNumberDescription")>
    'Public Property TeamNo() As Integer
    '  Get
    '    Return GetProperty(TeamNoProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(TeamNoProperty, Value)
    '  End Set
    'End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared TeamDisciplineHumanResourceListProperty As PropertyInfo(Of TeamDisciplineHumanResourceList) = RegisterProperty(Of TeamDisciplineHumanResourceList)(Function(c) c.TeamDisciplineHumanResourceList, "Team Discipline Human Resource List")

    Public ReadOnly Property TeamDisciplineHumanResourceList() As TeamDisciplineHumanResourceList
      Get
        If GetProperty(TeamDisciplineHumanResourceListProperty) Is Nothing Then
          LoadProperty(TeamDisciplineHumanResourceListProperty, OBLib.TeamManagement.TeamDisciplineHumanResourceList.NewTeamDisciplineHumanResourceList())
        End If
        Return GetProperty(TeamDisciplineHumanResourceListProperty)
      End Get
    End Property


    'Public Shared TeamRequirementListProperty As PropertyInfo(Of OBLib.Maintenance.PlayOutOps.TeamRequirementList) = RegisterProperty(Of OBLib.Maintenance.PlayOutOps.TeamRequirementList)(Function(c) c.TeamRequirementList, "Team Requirement List")

    'Public ReadOnly Property TeamRequirementList() As OBLib.Maintenance.PlayOutOps.TeamRequirementList
    '  Get
    '    If GetProperty(TeamRequirementListProperty) Is Nothing Then
    '      LoadProperty(TeamRequirementListProperty, Maintenance.PlayOutOps.TeamRequirementList.NewTeamRequirementList())
    '    End If
    '    Return GetProperty(TeamRequirementListProperty)
    '  End Get
    'End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemTeamIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Team")
        Else
          Return String.Format("Blank {0}", "System Team")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"TeamDisciplineHumanResources"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      'With AddWebRule(TeamNameProperty)
      '  .AffectedProperties.Add(TeamNoProperty)
      '  .JavascriptRuleFunctionName = "TeamBO.TeamNameValid"
      '  .ServerRuleFunction = AddressOf TeamNameValid
      'End With

      'With AddWebRule(TeamNoProperty)
      '  .JavascriptRuleFunctionName = "TeamBO.TeamNumberValid"
      '  '.ServerRuleFunction = AddressOf TeamNumberValid
      'End With

    End Sub

    Public Shared Function TeamNameValid(Team As SystemTeam) As String
      Dim ErrorString = ""
      If Team IsNot Nothing Then
        If Team.TeamName.Trim = "" Then
          ErrorString = "Team Name Required"
        End If
      End If
      Return ErrorString
    End Function

    Public Shared Function SystemValid(Team As SystemTeam) As String
      Dim ErrorString = ""
      If Team IsNot Nothing Then
        If Team.SystemID Is Nothing Then
          ErrorString = "Sub-Dept required"
        End If
      End If
      Return ErrorString
    End Function

    'Public Shared Function ProductionAreaValid(Team As SystemTeam) As String
    '  Dim ErrorString = ""
    '  If Team IsNot Nothing Then
    '    If Team.ProductionAreaID Is Nothing Then
    '      ErrorString = "Production Area required"
    '    End If
    '  End If
    '  Return ErrorString
    'End Function

    'Public Shared Function TeamNumberValid(Team As SystemTeam) As String
    '  Dim ErrorString = ""
    '  If Team IsNot Nothing Then
    '    If Team.TeamNo <= 0 Then
    '      ErrorString = "Team Number is Required"
    '    End If
    '  End If
    '  Return ErrorString
    'End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemTeam() method.

    End Sub

    Public Shared Function NewSystemTeam() As SystemTeam

      Return DataPortal.CreateChild(Of SystemTeam)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSystemTeam(dr As SafeDataReader) As SystemTeam

      Dim s As New SystemTeam()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemTeamIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, .GetInt32(1))
          LoadProperty(TeamNameProperty, .GetString(2))
          'LoadProperty(TeamNoProperty, .GetInt32(3))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSystemTeam"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSystemTeam"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSystemTeamID As SqlParameter = .Parameters.Add("@SystemTeamID", SqlDbType.Int)
          paramSystemTeamID.Value = GetProperty(SystemTeamIDProperty)
          If Me.IsNew Then
            paramSystemTeamID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@TeamName", GetProperty(TeamNameProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SystemTeamIDProperty, paramSystemTeamID.Value)
          End If
          ' update child objects
          If GetProperty(TeamDisciplineHumanResourceListProperty) IsNot Nothing Then
            Me.TeamDisciplineHumanResourceList.Update()
          End If
          'If GetProperty(TeamRequirementListProperty) IsNot Nothing Then
          '  Me.TeamRequirementList.Update()
          'End If
          ' mChildList.Update()
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(TeamDisciplineHumanResourceListProperty) IsNot Nothing Then
          Me.TeamDisciplineHumanResourceList.Update()
        End If
        'If GetProperty(TeamRequirementListProperty) IsNot Nothing Then
        '  Me.TeamRequirementList.Update()
        'End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSystemTeam"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SystemTeamID", GetProperty(SystemTeamIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
