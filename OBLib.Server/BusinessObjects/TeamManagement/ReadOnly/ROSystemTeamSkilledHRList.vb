﻿' Generated 05 Sep 2014 08:41 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace TeamManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemTeamSkilledHRList
    Inherits OBReadOnlyListBase(Of ROSystemTeamSkilledHRList, ROSystemTeamSkilledHR)

#Region " Business Methods "

    Public Function GetItem(OnTeamInd As Boolean) As ROSystemTeamSkilledHR

      For Each child As ROSystemTeamSkilledHR In Me
        If child.OnTeamInd = OnTeamInd Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property DisciplineID As Integer? = Nothing
      Public Property SystemTeamID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property HumanResourceIDs As String = ""

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSystemTeamSkilledHRList() As ROSystemTeamSkilledHRList

      Return New ROSystemTeamSkilledHRList()

    End Function

    Public Shared Sub BeginGetROSystemTeamSkilledHRList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSystemTeamSkilledHRList)))

      Dim dp As New DataPortal(Of ROSystemTeamSkilledHRList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSystemTeamSkilledHRList(CallBack As EventHandler(Of DataPortalResult(Of ROSystemTeamSkilledHRList)))

      Dim dp As New DataPortal(Of ROSystemTeamSkilledHRList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSystemTeamSkilledHRList() As ROSystemTeamSkilledHRList

      Return DataPortal.Fetch(Of ROSystemTeamSkilledHRList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemTeamSkilledHR.GetROSystemTeamSkilledHR(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemTeamsSkilledHRList"
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@SystemTeamID", NothingDBNull(crit.SystemTeamID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@HumanResourceIDs", Singular.Strings.MakeEmptyDBNull(crit.HumanResourceIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace