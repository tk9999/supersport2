﻿' Generated 10 Dec 2015 09:52 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.Misc

Namespace TeamManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemTeamList
    Inherits OBReadOnlyListBase(Of ROSystemTeamList, ROSystemTeam)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(SystemTeamID As Integer) As ROSystemTeam

      For Each child As ROSystemTeam In Me
        If child.SystemTeamID = SystemTeamID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Teams"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property SystemTeamID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property KeyWord As String = ""

      Public Sub New(SystemTeamID As Integer?, SystemID As Integer?)
        Me.SystemTeamID = SystemTeamID
        Me.SystemID = SystemID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSystemTeamList() As ROSystemTeamList

      Return New ROSystemTeamList()

    End Function

    Public Shared Sub BeginGetROSystemTeamList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSystemTeamList)))

      Dim dp As New DataPortal(Of ROSystemTeamList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSystemTeamList(CallBack As EventHandler(Of DataPortalResult(Of ROSystemTeamList)))

      Dim dp As New DataPortal(Of ROSystemTeamList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSystemTeamList() As ROSystemTeamList

      Return DataPortal.Fetch(Of ROSystemTeamList)(New Criteria())

    End Function

    Public Shared Function GetROSystemTeamList(SystemTeamID As Integer?, SystemID As Integer?) As ROSystemTeamList

      Return DataPortal.Fetch(Of ROSystemTeamList)(New Criteria(SystemTeamID, SystemID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemTeam.GetROSystemTeam(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemTeamList"
            cm.Parameters.AddWithValue("@SystemTeamID", Singular.Misc.NothingDBNull(crit.SystemTeamID))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@KeyWord", crit.KeyWord)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region
  End Class

End Namespace