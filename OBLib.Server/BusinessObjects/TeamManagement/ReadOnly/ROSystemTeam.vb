﻿' Generated 10 Dec 2015 09:52 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace TeamManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemTeam
    Inherits OBReadOnlyBase(Of ROSystemTeam)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemTeamIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTeamID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemTeamID() As Integer
      Get
        Return GetProperty(SystemTeamIDProperty)
      End Get
    End Property

    Public Shared TeamNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamName, "Team Name", "")
    ''' <summary>
    ''' Gets the Team Name value
    ''' </summary>
    <Display(Name:="Team Name", Description:="")>
    Public ReadOnly Property TeamName() As String
      Get
        Return GetProperty(TeamNameProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="The system the team belongs to")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared PatternNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PatternName, "Pattern Name", "")
    ''' <summary>
    ''' Gets the Pattern Name value
    ''' </summary>
    <Display(Name:="Pattern Name", Description:="")>
    Public ReadOnly Property PatternName() As String
      Get
        Return GetProperty(PatternNameProperty)
      End Get
    End Property

    Public Shared HROnTeamProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HROnTeam, 0)
    ''' <summary>
    ''' Gets the number of human resources on a team value
    ''' </summary>
    <Display(Name:="HR On Team", Description:="The number of Human Resources assigned to this Team")>
    Public ReadOnly Property HROnTeam() As Integer
      Get
        Return GetProperty(HROnTeamProperty)
      End Get
    End Property

    Public Shared ShedulesGeneratedForTeamProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShedulesGeneratedForTeam, "Schedules Generated for Team", False)
    ''' <summary>
    ''' Gets the Boolean Indicator if Shedules Generated For Team
    ''' </summary>
    <Display(Name:="Shedules Generated For Team?", Description:="Shedules Generated For Team?")>
    Public ReadOnly Property ShedulesGeneratedForTeam() As Boolean
      Get
        Return GetProperty(ShedulesGeneratedForTeamProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CreatedDateTime, "Created Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime?
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ModifiedDateTime, "Modified Date Time", DateTime.Now())
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime?
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared ModifiedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ModifiedByName, "Modified By", "")
    Public ReadOnly Property ModifiedByName() As String
      Get
        Return GetProperty(ModifiedByNameProperty)
      End Get
    End Property

    <Display(Name:="Creation Details")>
    Public ReadOnly Property CreateDetails() As String
      Get
        Return CreatedByName & " on " & CreatedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm")
      End Get
    End Property

    <Display(Name:="Modified Details")>
    Public ReadOnly Property ModifiedDetails() As String
      Get
        Return ModifiedByName & " on " & ModifiedDateTime.Value.ToString("ddd dd MMM yyyy HH:mm")
      End Get
    End Property

    Public Shared StartDateStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StartDateString, "Start", "")
    ''' <summary>
    ''' Gets the Pattern Name value
    ''' </summary>
    <Display(Name:="Start", Description:="")>
    Public ReadOnly Property StartDateString() As String
      Get
        Return GetProperty(StartDateStringProperty)
      End Get
    End Property

    Public Shared EndDateStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EndDateString, "End", "")
    ''' <summary>
    ''' Gets the Pattern Name value
    ''' </summary>
    <Display(Name:="End", Description:="")>
    Public ReadOnly Property EndDateString() As String
      Get
        Return GetProperty(EndDateStringProperty)
      End Get
    End Property

    Public Shared ShiftCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftCount, "Shifts", 0)
    ''' <summary>
    ''' Gets the Pattern Name value
    ''' </summary>
    <Display(Name:="Shifts", Description:="")>
    Public ReadOnly Property ShiftCount() As Integer
      Get
        Return GetProperty(ShiftCountProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemTeamIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemTeam(dr As SafeDataReader) As ROSystemTeam

      Dim r As New ROSystemTeam()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemTeamIDProperty, .GetInt32(0))
        LoadProperty(TeamNameProperty, .GetString(1))
        LoadProperty(SystemIDProperty, .GetInt32(2))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetValue(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetValue(6))
        LoadProperty(PatternNameProperty, .GetString(7))
        LoadProperty(HROnTeamProperty, .GetInt32(8))
        LoadProperty(ShedulesGeneratedForTeamProperty, .GetBoolean(9))
        LoadProperty(CreatedByNameProperty, .GetString(10))
        LoadProperty(ModifiedByNameProperty, .GetString(11))
        'RowNo  = 12
        LoadProperty(StartDateStringProperty, .GetString(13))
        LoadProperty(EndDateStringProperty, .GetString(14))
        LoadProperty(ShiftCountProperty, .GetInt32(15))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace