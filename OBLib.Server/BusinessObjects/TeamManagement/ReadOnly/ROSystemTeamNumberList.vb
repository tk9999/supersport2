﻿' Generated 21 Mar 2016 08:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace TeamManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemTeamNumberList
    Inherits OBReadOnlyListBase(Of ROSystemTeamNumberList, ROSystemTeamNumber)

#Region " Business Methods "

    Public Function GetItem(SystemTeamNumberID As Integer) As ROSystemTeamNumber

      For Each child As ROSystemTeamNumber In Me
        If child.SystemTeamNumberID = SystemTeamNumberID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Team Numbers"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer?

      <PrimarySearchField>
      Public Property SystemTeamNumberName As String

      Public Sub New(SystemID As Integer?)
        Me.SystemID = SystemID
        Me.SystemTeamNumberName = ""
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSystemTeamNumberList() As ROSystemTeamNumberList

      Return New ROSystemTeamNumberList()

    End Function

    Public Shared Sub BeginGetROSystemTeamNumberList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSystemTeamNumberList)))

      Dim dp As New DataPortal(Of ROSystemTeamNumberList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSystemTeamNumberList(CallBack As EventHandler(Of DataPortalResult(Of ROSystemTeamNumberList)))

      Dim dp As New DataPortal(Of ROSystemTeamNumberList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSystemTeamNumberList() As ROSystemTeamNumberList

      Return DataPortal.Fetch(Of ROSystemTeamNumberList)(New Criteria())

    End Function

    Public Shared Function GetROSystemTeamNumberList(SystemID As Integer?) As ROSystemTeamNumberList

      Return DataPortal.Fetch(Of ROSystemTeamNumberList)(New Criteria(SystemID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSystemTeamNumber.GetROSystemTeamNumber(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSystemTeamNumberList"
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@SystemTeamNumberName", Singular.Strings.MakeEmptyDBNull(crit.SystemTeamNumberName))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace