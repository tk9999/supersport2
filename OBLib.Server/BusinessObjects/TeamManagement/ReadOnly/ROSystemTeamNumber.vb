﻿' Generated 21 Mar 2016 08:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace TeamManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemTeamNumber
    Inherits OBReadOnlyBase(Of ROSystemTeamNumber)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemTeamNumberIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTeamNumberID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemTeamNumberID() As Integer
      Get
        Return GetProperty(SystemTeamNumberIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared SystemTeamNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTeamNumber, "Team Number", 0)
    ''' <summary>
    ''' Gets the System Team Number value
    ''' </summary>
    <Display(Name:="Team Number", Description:="")>
    Public ReadOnly Property SystemTeamNumber() As Integer
      Get
        Return GetProperty(SystemTeamNumberProperty)
      End Get
    End Property

    Public Shared SystemTeamNumberNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemTeamNumberName, "Team Name", "")
    ''' <summary>
    ''' Gets the System Team Number Name value
    ''' </summary>
    <Display(Name:="Team Name", Description:="")>
    Public ReadOnly Property SystemTeamNumberName() As String
      Get
        Return GetProperty(SystemTeamNumberNameProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemTeamNumberIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SystemTeamNumberName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemTeamNumber(dr As SafeDataReader) As ROSystemTeamNumber

      Dim r As New ROSystemTeamNumber()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemTeamNumberIDProperty, .GetInt32(0))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SystemTeamNumberProperty, .GetInt32(2))
        LoadProperty(SystemTeamNumberNameProperty, .GetString(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace