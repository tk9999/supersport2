﻿' Generated 09 Dec 2015 17:18 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.Misc
Imports Singular

Namespace TeamManagement.ReadOnly

  <Serializable()> _
  Public Class ROTeamShiftPatternList
    Inherits OBReadOnlyListBase(Of ROTeamShiftPatternList, ROTeamShiftPattern)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(TeamShiftPatternID As Integer) As ROTeamShiftPattern

      For Each child As ROTeamShiftPattern In Me
        If child.TeamShiftPatternID = TeamShiftPatternID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Team Shift Patterns"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList))>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Sub New(SystemID As Integer?)

        Me.SystemID = SystemID

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROTeamShiftPatternList() As ROTeamShiftPatternList

      Return New ROTeamShiftPatternList()

    End Function

    Public Shared Sub BeginGetROTeamShiftPatternList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROTeamShiftPatternList)))

      Dim dp As New DataPortal(Of ROTeamShiftPatternList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROTeamShiftPatternList(CallBack As EventHandler(Of DataPortalResult(Of ROTeamShiftPatternList)))

      Dim dp As New DataPortal(Of ROTeamShiftPatternList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROTeamShiftPatternList() As ROTeamShiftPatternList

      Return DataPortal.Fetch(Of ROTeamShiftPatternList)(New Criteria())

    End Function

    Public Shared Function GetROTeamShiftPatternList(SystemID As Integer?) As ROTeamShiftPatternList

      Return DataPortal.Fetch(Of ROTeamShiftPatternList)(New Criteria(SystemID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROTeamShiftPattern.GetROTeamShiftPattern(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROTeamShiftPatternList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
