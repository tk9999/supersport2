﻿' Generated 05 Sep 2014 08:41 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace TeamManagement.ReadOnly

  <Serializable()> _
  Public Class ROSystemTeamSkilledHR
    Inherits OBReadOnlyBase(Of ROSystemTeamSkilledHR)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared OnTeamIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnTeamInd, "On Team", False)
    ''' <summary>
    ''' Gets the On Team value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property OnTeamInd() As Boolean
      Get
        Return GetProperty(OnTeamIndProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared DisciplinesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Disciplines, "Disciplines", "")
    ''' <summary>
    ''' Gets the Disciplines value
    ''' </summary>
    <Display(Name:="Disciplines", Description:="")>
    Public ReadOnly Property Disciplines() As String
      Get
        Return GetProperty(DisciplinesProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource ID")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared VisibleProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Visible, "Visible", True)
    ''' <summary>
    ''' Gets and sets the Relief Crew value
    ''' </summary>
    <Display(Name:="Visible", Description:=""),
    ClientOnly()>
    Public ReadOnly Property Visible() As Boolean
      Get
        Return GetProperty(VisibleProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(OnTeamIndProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSystemTeamSkilledHR(dr As SafeDataReader) As ROSystemTeamSkilledHR

      Dim r As New ROSystemTeamSkilledHR()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(OnTeamIndProperty, .GetBoolean(0))
        LoadProperty(HumanResourceProperty, .GetString(1))
        LoadProperty(DisciplinesProperty, .GetString(2))
        LoadProperty(HumanResourceIDProperty, .GetInt32(3))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace