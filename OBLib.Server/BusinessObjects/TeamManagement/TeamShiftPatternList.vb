﻿' Generated 09 Dec 2015 18:10 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace TeamManagement

  <Serializable()> _
  Public Class TeamShiftPatternList
    Inherits OBBusinessListBase(Of TeamShiftPatternList, TeamShiftPattern)

#Region " Business Methods "

    Public Function GetItem(TeamShiftPatternID As Integer) As TeamShiftPattern

      For Each child As TeamShiftPattern In Me
        If child.TeamShiftPatternID = TeamShiftPatternID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Team Shift Patterns"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer? = Nothing
      Public Property SystemAreaShiftPatternID As Integer? = Nothing
      Public Property SystemTeamID As Integer? = Nothing

      Public Sub New(SystemID As Integer?, SystemAreaShiftPatternID As Integer?, SystemTeamID As Integer?)
        Me.SystemID = SystemID
        Me.SystemAreaShiftPatternID = SystemAreaShiftPatternID
        Me.SystemTeamID = SystemTeamID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewTeamShiftPatternList() As TeamShiftPatternList

      Return New TeamShiftPatternList()

    End Function

    Public Shared Sub BeginGetTeamShiftPatternList(CallBack As EventHandler(Of DataPortalResult(Of TeamShiftPatternList)))

      Dim dp As New DataPortal(Of TeamShiftPatternList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetTeamShiftPatternList() As TeamShiftPatternList

      Return DataPortal.Fetch(Of TeamShiftPatternList)(New Criteria())

    End Function

    Public Shared Function GetTeamShiftPatternList(SystemID As Integer?, SystemAreaShiftPatternID As Integer?, SystemTeamID As Integer?) As TeamShiftPatternList

      Return DataPortal.Fetch(Of TeamShiftPatternList)(New Criteria(SystemID, SystemAreaShiftPatternID, SystemTeamID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(TeamShiftPattern.GetTeamShiftPattern(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getTeamShiftPatternList"
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@SystemAreaShiftPatternID", Singular.Misc.NothingDBNull(crit.SystemAreaShiftPatternID))
            cm.Parameters.AddWithValue("@SystemTeamID", Singular.Misc.NothingDBNull(crit.SystemTeamID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As TeamShiftPattern In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As TeamShiftPattern In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
