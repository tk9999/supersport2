﻿' Generated 01 Sep 2014 14:41 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.SystemManagement.ReadOnly
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace TeamManagement

  <Serializable()> _
  Public Class TeamDisciplineHumanResource
    Inherits OBBusinessBase(Of TeamDisciplineHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TeamDisciplineHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TeamDisciplineHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property TeamDisciplineHumanResourceID() As Integer
      Get
        Return GetProperty(TeamDisciplineHumanResourceIDProperty)
      End Get
    End Property

    Public Shared SystemTeamIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemTeamID, "System Team ID", Nothing)
    ''' <summary>
    ''' Gets the System Team ID value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemTeamID() As Integer?
      Get
        Return GetProperty(SystemTeamIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="The discipline of the HR on the team"), Required(ErrorMessage:="Discipline Required"),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSystemProductionAreaDisciplineList),
      FilterMethodName:="TeamDisciplineHumanResourceBO.GetAllowedDisciplines", ValueMember:="DisciplineID", DisplayMember:="Discipline")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="Human resource that is chosen for the team"), Required(ErrorMessage:="Human Resource Required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared HasScheduleProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.HasSchedule, False)
    Public Property HasSchedule() As Boolean
      Get
        Return GetProperty(HasScheduleProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasScheduleProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of Date?) = RegisterSProperty(Of Date?)(Function(c) c.StartDate).DefaultValueExpression("TeamDisciplineHumanResourceBO.SetStartDate(self)")
    ' ''' <summary>
    ' ''' Gets the Start Date value
    ' ''' </summary>
    '<Display(Name:="Start Date", Description:="Start Date for the team member"), Required(ErrorMessage:="Start Date Required")>
    Public Property StartDate() As Date?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(value As Date?)
        If value IsNot Nothing Then
          SetProperty(StartDateProperty, value)
        End If
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of Date?) = RegisterSProperty(Of Date?)(Function(c) c.EndDate).DefaultValueExpression("TeamDisciplineHumanResourceBO.SetEndDate(self)")
    ' ''' <summary>
    ' ''' Gets the End Date  value
    ' ''' </summary>
    '<Display(Name:="End Date ", Description:="End Date for the team member"), Required(ErrorMessage:="End Date  Required")>
    Public Property EndDate() As Date?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(value As Date?)
        If value IsNot Nothing Then
          SetProperty(EndDateProperty, value)
        End If
      End Set
    End Property

    Public Shared ShiftCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftCount, "Shift Count", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    ''' 
    <Display(Name:="Shifts")>
    Public Property ShiftCount() As Integer?
      Get
        Return GetProperty(ShiftCountProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ShiftCountProperty, value)
      End Set
    End Property

    Public Shared RoomScheduleCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomBookings, "Room Bookings", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    ''' 
    <Display(Name:="Room Bookings")>
    Public Property RoomBookings() As Integer?
      Get
        Return GetProperty(RoomScheduleCountProperty)
      End Get
      Set(value As Integer?)
        SetProperty(RoomScheduleCountProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As OBLib.TeamManagement.SystemTeamShiftPattern

      Return CType(CType(Me.Parent, TeamDisciplineHumanResourceList).Parent, OBLib.TeamManagement.SystemTeamShiftPattern)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TeamDisciplineHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResourceID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Team Discipline Human Resource")
        Else
          Return String.Format("Blank {0}", "Team Discipline Human Resource")
        End If
      Else
        Return Me.HumanResourceID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTeamDisciplineHumanResource() method.

    End Sub

    Public Shared Function NewTeamDisciplineHumanResource() As TeamDisciplineHumanResource

      Return DataPortal.CreateChild(Of TeamDisciplineHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTeamDisciplineHumanResource(dr As SafeDataReader) As TeamDisciplineHumanResource

      Dim t As New TeamDisciplineHumanResource()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TeamDisciplineHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(SystemTeamIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
          LoadProperty(StartDateProperty, .GetValue(8))
          LoadProperty(EndDateProperty, .GetValue(9))
          LoadProperty(HumanResourceProperty, .GetString(10))
          LoadProperty(HasScheduleProperty, .GetBoolean(11))
          'LoadProperty(ShiftCountProperty, .GetInt32(12))
          'LoadProperty(RoomScheduleCountProperty, .GetInt32(13))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTeamDisciplineHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTeamDisciplineHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTeamDisciplineHumanResourceID As SqlParameter = .Parameters.Add("@TeamDisciplineHumanResourceID", SqlDbType.Int)
          paramTeamDisciplineHumanResourceID.Value = GetProperty(TeamDisciplineHumanResourceIDProperty)
          If Me.IsNew Then
            paramTeamDisciplineHumanResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SystemTeamID", Me.GetParent().SystemTeamID)
          .Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@StartDate", GetProperty(StartDateProperty))
          .Parameters.AddWithValue("@EndDate", GetProperty(EndDateProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(TeamDisciplineHumanResourceIDProperty, paramTeamDisciplineHumanResourceID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTeamDisciplineHumanResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TeamDisciplineHumanResourceID", GetProperty(TeamDisciplineHumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace