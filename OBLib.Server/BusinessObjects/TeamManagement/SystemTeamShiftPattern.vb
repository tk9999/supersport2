﻿' Generated 16 Feb 2016 17:30 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports Singular.Web
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.ShiftPatterns.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace TeamManagement

  <Serializable()> _
  Public Class SystemTeamShiftPattern
    Inherits OBBusinessBase(Of SystemTeamShiftPattern)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsExpanded, "Is Expanded", False)
    ''' <summary>
    ''' Gets and sets the Is Finalised value
    ''' </summary>
    <Display(Name:="Is Expanded", Description:=""), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return TeamBO.ToString(self)")

    Public Shared SystemTeamIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTeamID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property SystemTeamID() As Integer
      Get
        Return GetProperty(SystemTeamIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System ID", 0)
    ''' <summary>
    ''' Gets and sets the System ID value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept Required"),
    DropDownWeb(GetType(ROUserSystemList),
                UnselectedText:="Select Sub-Dept...",
                DisplayMember:="System", ValueMember:="SystemID")>
    Public Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROUserSystemAreaList),
                UnselectedText:="Area", ThisFilterMember:="SystemID",
                DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID"),
    Required(ErrorMessage:="Area is required")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared TeamNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamName, "Team Name", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="Team Name", Description:="Team Name"), Required(ErrorMessage:="Team Name Required"),
    StringLength(100, ErrorMessage:="Team Name cannot be more than 100 characters")>
    Public Property TeamName() As String
      Get
        Return GetProperty(TeamNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TeamNameProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared TeamShiftPatternIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TeamShiftPatternID, "Team Shift Pattern", Nothing)
    ''' <summary>
    ''' Gets and sets the Team Shift Pattern value
    ''' </summary>
    <Display(Name:="Team Shift Pattern", Description:="")>
    Public Property TeamShiftPatternID() As Integer
      Get
        Return GetProperty(TeamShiftPatternIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(TeamShiftPatternIDProperty, Value)
      End Set
    End Property

    Public Shared SystemAreaShiftPatternIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SystemAreaShiftPatternID, Nothing)
    ''' <summary>
    ''' Gets and sets the System Area Shift Pattern value
    ''' DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Template", Description:="Template to apply to a System Team on creation of a new System Area Shift Pattern"),
    DropDownWeb(GetType(ROSystemAreaShiftPatternTemplateList),
                 BeforeFetchJS:="SystemTeamShiftPatternBO.setCriteriaBeforeRefresh",
                 PreFindJSFunction:="SystemTeamShiftPatternBO.triggerAutoPopulate",
                 AfterFetchJS:="SystemTeamShiftPatternBO.afterPatternRefreshAjax",
                 LookupMember:="PatternName", ValueMember:="SystemAreaShiftPatternID",
                 DropDownColumns:={"PatternName"},
                 OnItemSelectJSFunction:="SystemTeamShiftPatternBO.onPatternSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("SystemTeamShiftPatternBO.SystemAreaShiftPatternWeekDaysSet(self)")>
    Public Property SystemAreaShiftPatternID() As Integer?
      Get
        Return GetProperty(SystemAreaShiftPatternIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemAreaShiftPatternIDProperty, Value)
      End Set
    End Property
    'FilterMethodName:="SystemTeamShiftPatternBO.AllowedSystemAreaShiftPatterns",
    '"SystemTeamShiftPatternBO.GetSystemAreaDropDown($data)"
    'DropDownWeb(GetType(ROSystemAreaShiftPatternList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
    '        ValueMember:="SystemAreaShiftPatternID",
    '        DisplayMember:="PatternName"),

    Public Shared PatternNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PatternName, "Pattern Name", "")
    ''' <summary>
    ''' Gets and sets the Pattern Name value
    ''' </summary>,
    <Display(Name:="Pattern Name", Description:=""), Required(ErrorMessage:="Pattern Name Required")>
    Public Property PatternName() As String
      Get
        Return GetProperty(PatternNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PatternNameProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of Date?) = RegisterProperty(Of Date?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""), Required(ErrorMessage:="Start Date Required"),
    Singular.DataAnnotations.DateField>
    Public Property StartDate As Date?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As Date?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of Date?) = RegisterProperty(Of Date?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:=""), Required(ErrorMessage:="End Date Required"),
    Singular.DataAnnotations.DateField>
    Public Property EndDate As Date?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As Date?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared HROnTeamProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HROnTeam, "HR On Team", Nothing)
    ''' <summary>
    ''' Gets the number of people on a Team
    ''' </summary>
    Public ReadOnly Property HROnTeam() As Integer?
      Get
        Return GetProperty(HROnTeamProperty)
      End Get
    End Property

    Public Shared SchedulesGeneratedProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SchedulesGenerated, "Schedules Generated", 0)
    ''' <summary>
    ''' Gets the number of Schedules Generated for HR on this team
    ''' </summary>
    <Display(Name:="Schedules Generated", Description:="")>
    Public ReadOnly Property SchedulesGenerated() As Integer
      Get
        Return GetProperty(SchedulesGeneratedProperty)
      End Get
    End Property

    Public Shared ShiftDurationProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.ShiftDuration, 0)
    ''' <summary>
    ''' Gets and Sets the Number of Weeks value
    ''' </summary>
    <Display(Name:="Number of Weeks", Description:=""),
    DropDownWeb(GetType(ROShiftDurationList),
                DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
                FilterMethodName:="SystemTeamShiftPatternBO.AllowedShiftDurations",
                ValueMember:="ShiftDuration", DisplayMember:="ShiftDurationName"),
    SetExpression("SystemTeamShiftPatternBO.UpdateNoWeekDays(self)")>
    Public Property ShiftDuration() As Integer
      Get
        Return GetProperty(ShiftDurationProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ShiftDurationProperty, Value)
      End Set
    End Property

    Public Shared IsTemplateProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTemplate, "IsTemplate", False)
    ''' <summary>
    ''' Is this a template pattern?
    ''' </summary>
    <Display(Name:="Schedules Generated", Description:="")>
    Public ReadOnly Property IsTemplate() As Boolean
      Get
        Return GetProperty(IsTemplateProperty)
      End Get
    End Property

    Public Shared AlreadyCreatedTSPProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.AlreadyCreatedTSP, Nothing)
    Public Property AlreadyCreatedTSP() As Integer?
      Get
        Return GetProperty(AlreadyCreatedTSPProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AlreadyCreatedTSPProperty, Value)
      End Set
    End Property

    'Public Shared SystemTeamNumberIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemTeamNumberID, "Team Number", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the ID value
    ' ''' </summary>
    'Public Property SystemTeamNumberID() As Integer?
    '  Get
    '    Return GetProperty(SystemTeamNumberIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(SystemTeamNumberIDProperty, Value)
    '  End Set
    'End Property

#End Region

#Region " Child Lists "

    Public Shared TeamDisciplineHumanResourceListProperty As PropertyInfo(Of OBLib.TeamManagement.TeamDisciplineHumanResourceList) = RegisterProperty(Of OBLib.TeamManagement.TeamDisciplineHumanResourceList)(Function(c) c.TeamDisciplineHumanResourceList, "Team Discipline Human Resource List")

    Public ReadOnly Property TeamDisciplineHumanResourceList() As OBLib.TeamManagement.TeamDisciplineHumanResourceList
      Get
        If GetProperty(TeamDisciplineHumanResourceListProperty) Is Nothing Then
          LoadProperty(TeamDisciplineHumanResourceListProperty, OBLib.TeamManagement.TeamDisciplineHumanResourceList.NewTeamDisciplineHumanResourceList())
        End If
        Return GetProperty(TeamDisciplineHumanResourceListProperty)
      End Get
    End Property

    Public Shared TeamShiftPatternPatternListProperty As PropertyInfo(Of OBLib.TeamManagement.TeamShiftPatternList) = RegisterProperty(Of OBLib.TeamManagement.TeamShiftPatternList)(Function(c) c.TeamShiftPatternList, "Team Shift Pattern Pattern List")

    Public ReadOnly Property TeamShiftPatternList() As OBLib.TeamManagement.TeamShiftPatternList
      Get
        If GetProperty(TeamShiftPatternPatternListProperty) Is Nothing Then
          LoadProperty(TeamShiftPatternPatternListProperty, OBLib.TeamManagement.TeamShiftPatternList.NewTeamShiftPatternList)
        End If
        Return GetProperty(TeamShiftPatternPatternListProperty)
      End Get
    End Property

    Public Shared SystemAreaShiftPatternListProperty As PropertyInfo(Of OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList) = RegisterProperty(Of OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList)(Function(c) c.SystemAreaShiftPatternList, "System Area Shift Pattern List")

    Public ReadOnly Property SystemAreaShiftPatternList() As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList
      Get
        If GetProperty(SystemAreaShiftPatternListProperty) Is Nothing Then
          LoadProperty(SystemAreaShiftPatternListProperty, OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternList.NewSystemAreaShiftPatternList)
        End If
        Return GetProperty(SystemAreaShiftPatternListProperty)
      End Get
    End Property

    Public Shared SystemAreaShiftPatternWeekDayListProperty As PropertyInfo(Of OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDayList) = RegisterProperty(Of OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDayList)(Function(c) c.SystemAreaShiftPatternWeekDayList, "System Area Shift Pattern Week Day List")

    Public ReadOnly Property SystemAreaShiftPatternWeekDayList() As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDayList
      Get
        If GetProperty(SystemAreaShiftPatternWeekDayListProperty) Is Nothing Then
          LoadProperty(SystemAreaShiftPatternWeekDayListProperty, OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDayList.NewSystemAreaShiftPatternWeekDayList)
        End If
        Return GetProperty(SystemAreaShiftPatternWeekDayListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemTeamIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.TeamName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Team Shift Pattern")
        Else
          Return String.Format("Blank {0}", "System Team Shift Pattern")
        End If
      Else
        Return Me.TeamName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(ShiftDurationProperty)
        .JavascriptRuleFunctionName = "SystemTeamShiftPatternBO.ShiftDurationValid"
        .ServerRuleFunction = AddressOf ShiftDurationValid
      End With

      With AddWebRule(StartDateProperty)
        .AffectedProperties.Add(EndDateProperty)
        .JavascriptRuleFunctionName = "SystemTeamShiftPatternBO.DatesValid"
        .ServerRuleFunction = AddressOf DatesValid
      End With

    End Sub

    Public Shared Function ShiftDurationValid(SASP As SystemTeamShiftPattern) As String

      Dim ErrorDescription As String = ""
      If Singular.Misc.CompareSafe(SASP.ShiftDuration, 0) Then
        ErrorDescription = "Shift Duration Required"
      End If
      Return ErrorDescription

    End Function

    Public Shared Function DatesValid(SASP As SystemTeamShiftPattern) As String

      Dim ErrorDescription As String = ""
      If SASP.StartDate IsNot Nothing AndAlso SASP.EndDate IsNot Nothing Then
        If SASP.EndDate < SASP.StartDate Then
          ErrorDescription = "Start Date must be before End Date"
        End If
      End If
      Return ErrorDescription

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemTeamShiftPattern() method.

    End Sub

    Public Shared Function NewSystemTeamShiftPattern() As SystemTeamShiftPattern

      Return DataPortal.CreateChild(Of SystemTeamShiftPattern)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSystemTeamShiftPattern(dr As SafeDataReader) As SystemTeamShiftPattern

      Dim s As New SystemTeamShiftPattern()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemTeamIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(TeamNameProperty, .GetString(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(6))
          LoadProperty(TeamShiftPatternIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(SystemAreaShiftPatternIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(PatternNameProperty, .GetString(9))
          LoadProperty(StartDateProperty, .GetValue(10))
          LoadProperty(EndDateProperty, .GetValue(11))
          LoadProperty(HROnTeamProperty, .GetInt32(12))
          LoadProperty(SchedulesGeneratedProperty, .GetInt32(13))
          LoadProperty(ShiftDurationProperty, .GetInt32(14))
          LoadProperty(IsTemplateProperty, .GetBoolean(15))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSystemTeamShiftPattern"
        ' Inserts new System Team
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSystemTeamShiftPattern"
        ' Updates System Team, and Team Shift Pattern. Inserts Team Shift Pattern if one does not exist. 
        ' System Team will already exist when this happens
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then
        With cm
          .CommandType = CommandType.StoredProcedure
          If Singular.Misc.CompareSafe(GetProperty(TeamShiftPatternIDProperty), 0) AndAlso Me.IsNew Then
            ' Add System Teams
            Dim paramSystemTeamID As SqlParameter = .Parameters.Add("@SystemTeamID", SqlDbType.Int)
            paramSystemTeamID.Value = GetProperty(SystemTeamIDProperty)
            paramSystemTeamID.Direction = ParameterDirection.Output
            .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
            .Parameters.AddWithValue("@RealSystemTeamID", Singular.Misc.ZeroNothingDBNull(GetProperty(SystemTeamIDProperty)))
            .Parameters.AddWithValue("@TeamName", GetProperty(TeamNameProperty))
            .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
            .Parameters.AddWithValue("@TeamShiftPatternID", Singular.Misc.ZeroNothingDBNull(GetProperty(TeamShiftPatternIDProperty)))
            .Parameters.AddWithValue("@SystemAreaShiftPatternID", Singular.Misc.NothingDBNull(GetProperty(SystemAreaShiftPatternIDProperty)))
            .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
            .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
            .Parameters.AddWithValue("@PatternName", GetProperty(PatternNameProperty))
            .Parameters.AddWithValue("@ShiftDuration", GetProperty(ShiftDurationProperty))
            .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
            If Me.IsNew Then
              LoadProperty(SystemTeamIDProperty, paramSystemTeamID.Value)
            End If
          Else
            'Add Team Shift Patterns after System Teams Added
            .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
            .Parameters.AddWithValue("@TeamName", GetProperty(TeamNameProperty))
            .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
            .Parameters.AddWithValue("@TeamShiftPatternID", Singular.Misc.ZeroNothingDBNull(GetProperty(TeamShiftPatternIDProperty)))
            .Parameters.AddWithValue("@SystemTeamID", Singular.Misc.ZeroNothingDBNull(GetProperty(SystemTeamIDProperty)))
            .Parameters.AddWithValue("@RealSystemTeamID", Singular.Misc.ZeroNothingDBNull(GetProperty(SystemTeamIDProperty)))
            .Parameters.AddWithValue("@SystemAreaShiftPatternID", Singular.Misc.NothingDBNull(GetProperty(SystemAreaShiftPatternIDProperty)))
            .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
            .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
            .Parameters.AddWithValue("@PatternName", GetProperty(PatternNameProperty))
            .Parameters.AddWithValue("@ShiftDuration", GetProperty(ShiftDurationProperty))
            .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
            .Parameters.AddWithValue("@AlreadyCreatedTSP", Singular.Misc.ZeroNothingDBNull(GetProperty(AlreadyCreatedTSPProperty)))
            SetProperty(AlreadyCreatedTSPProperty, 1)
          End If

          .ExecuteNonQuery()
          ' update child objects
          If GetProperty(TeamDisciplineHumanResourceListProperty) IsNot Nothing Then
            Me.TeamDisciplineHumanResourceList.Update()
          End If
          If GetProperty(SystemAreaShiftPatternListProperty) IsNot Nothing Then
            Me.SystemAreaShiftPatternList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(TeamDisciplineHumanResourceListProperty) IsNot Nothing Then
          Me.TeamDisciplineHumanResourceList.Update()
        End If
        If GetProperty(SystemAreaShiftPatternListProperty) IsNot Nothing Then
          Me.SystemAreaShiftPatternList.Update()
        End If
      End If

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
