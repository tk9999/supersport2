﻿' Generated 21 Mar 2016 07:58 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.Misc

Namespace TeamManagement

  <Serializable()> _
  Public Class SystemTeamNumberList
    Inherits OBBusinessListBase(Of SystemTeamNumberList, SystemTeamNumber)

#Region " Business Methods "

    Public Function GetItem(SystemTeamNumberID As Integer) As SystemTeamNumber

      For Each child As SystemTeamNumber In Me
        If child.SystemTeamNumberID = SystemTeamNumberID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "System Team Numbers"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemID As Integer?

      Public Sub New(SystemID As Integer?)
        Me.SystemID = SystemID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSystemTeamNumberList() As SystemTeamNumberList

      Return New SystemTeamNumberList()

    End Function

    Public Shared Sub BeginGetSystemTeamNumberList(CallBack As EventHandler(Of DataPortalResult(Of SystemTeamNumberList)))

      Dim dp As New DataPortal(Of SystemTeamNumberList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSystemTeamNumberList() As SystemTeamNumberList

      Return DataPortal.Fetch(Of SystemTeamNumberList)(New Criteria())

    End Function

    Public Shared Function GetSystemTeamNumberList(SystemID As Integer?) As SystemTeamNumberList

      Return DataPortal.Fetch(Of SystemTeamNumberList)(New Criteria(SystemID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemTeamNumber.GetSystemTeamNumber(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemTeamNumberList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SystemTeamNumber In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SystemTeamNumber In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace