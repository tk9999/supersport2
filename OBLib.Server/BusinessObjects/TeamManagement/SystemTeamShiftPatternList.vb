﻿' Generated 16 Feb 2016 17:30 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace TeamManagement

  <Serializable()> _
  Public Class SystemTeamShiftPatternList
    Inherits OBBusinessListBase(Of SystemTeamShiftPatternList, SystemTeamShiftPattern)

#Region " Business Methods "

    Public Function GetItem(SystemTeamID As Integer) As SystemTeamShiftPattern

      For Each child As SystemTeamShiftPattern In Me
        If child.SystemTeamID = SystemTeamID Then
          Return child
        End If
      Next
      Return Nothing

    End Function


    Public Function GetTeamShiftPattern(TeamShiftPatternID As Integer) As OBLib.TeamManagement.TeamShiftPattern

      Dim obj As OBLib.TeamManagement.TeamShiftPattern = Nothing
      For Each parent As SystemTeamShiftPattern In Me
        obj = parent.TeamShiftPatternList.GetItem(TeamShiftPatternID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemAreaShiftPattern(SystemAreaShiftPatternID As Integer) As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPattern

      Dim obj As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPattern = Nothing
      For Each parent As SystemTeamShiftPattern In Me
        obj = parent.SystemAreaShiftPatternList.GetItem(SystemAreaShiftPatternID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSystemAreaShiftPatternWeekDay(SystemAreaShiftPatternWeekDayID As Integer) As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay

      Dim obj As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay = Nothing
      For Each parent As SystemTeamShiftPattern In Me
        obj = parent.SystemAreaShiftPatternWeekDayList.GetItem(SystemAreaShiftPatternWeekDayID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemTeamID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing

      Public Sub New()


      End Sub

      Public Sub New(SystemTeamID As Integer?, SystemID As Integer?)
        Me.SystemTeamID = SystemTeamID
        Me.SystemID = SystemID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSystemTeamShiftPatternList() As SystemTeamShiftPatternList

      Return New SystemTeamShiftPatternList()

    End Function

    Public Shared Sub BeginGetSystemTeamShiftPatternList(CallBack As EventHandler(Of DataPortalResult(Of SystemTeamShiftPatternList)))

      Dim dp As New DataPortal(Of SystemTeamShiftPatternList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSystemTeamShiftPatternList(SystemTeamID As Integer?, SystemID As Integer?) As SystemTeamShiftPatternList

      Return DataPortal.Fetch(Of SystemTeamShiftPatternList)(New Criteria(SystemTeamID, SystemID))

    End Function

    Public Shared Function GetSystemTeamShiftPatternList() As SystemTeamShiftPatternList

      Return DataPortal.Fetch(Of SystemTeamShiftPatternList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemTeamShiftPattern.GetSystemTeamShiftPattern(sdr))
      End While
      Me.RaiseListChangedEvents = True

      If Me.Count > 0 Then
        Dim parent As SystemTeamShiftPattern = Nothing
        If sdr.NextResult() Then
          While sdr.Read
            If parent Is Nothing OrElse parent.SystemTeamID <> sdr.GetInt32(1) Then
              parent = Me.GetItem(sdr.GetInt32(1))
            End If
            If parent IsNot Nothing Then
              parent.TeamDisciplineHumanResourceList.RaiseListChangedEvents = False
              parent.TeamDisciplineHumanResourceList.Add(OBLib.TeamManagement.TeamDisciplineHumanResource.GetTeamDisciplineHumanResource(sdr))
              parent.TeamDisciplineHumanResourceList.RaiseListChangedEvents = True
            End If
          End While
        End If

        If sdr.NextResult() Then
          While sdr.Read
            If parent Is Nothing OrElse parent.TeamShiftPatternID <> sdr.GetInt32(0) Then
              parent = Me.GetItem(sdr.GetInt32(2))
            End If
            If parent IsNot Nothing Then
              parent.TeamShiftPatternList.RaiseListChangedEvents = False
              parent.TeamShiftPatternList.Add(OBLib.TeamManagement.TeamShiftPattern.GetTeamShiftPattern(sdr))
              parent.TeamShiftPatternList.RaiseListChangedEvents = True
            End If
          End While
        End If

        If sdr.NextResult() Then
          While sdr.Read
            If parent Is Nothing OrElse parent.SystemAreaShiftPatternID <> sdr.GetInt32(0) Then
              parent = Me.GetItem(parent.SystemTeamID)
            End If
            If parent IsNot Nothing Then
              parent.SystemAreaShiftPatternList.RaiseListChangedEvents = False
              parent.SystemAreaShiftPatternList.Add(OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPattern.GetSystemAreaShiftPattern(sdr))
              parent.SystemAreaShiftPatternList.RaiseListChangedEvents = True
            End If
          End While
        End If

        If sdr.NextResult() Then
          While sdr.Read
            If parent Is Nothing OrElse parent.SystemAreaShiftPatternID <> sdr.GetInt32(1) Then
              parent = Me.GetItem(parent.SystemTeamID)
            End If
            If parent IsNot Nothing Then
              parent.SystemAreaShiftPatternWeekDayList.RaiseListChangedEvents = False
              parent.SystemAreaShiftPatternWeekDayList.Add(OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay.GetSystemAreaShiftPatternWeekDay(sdr))
              parent.SystemAreaShiftPatternWeekDayList.RaiseListChangedEvents = True
            End If
          End While
        End If

        For Each child As SystemTeamShiftPattern In Me
          child.CheckRules()
          For Each TeamDisciplineHumanResource As OBLib.TeamManagement.TeamDisciplineHumanResource In child.TeamDisciplineHumanResourceList
            TeamDisciplineHumanResource.CheckRules()
          Next
          For Each TeamShiftPattern As OBLib.TeamManagement.TeamShiftPattern In child.TeamShiftPatternList
            TeamShiftPattern.CheckRules()
          Next
          For Each SystemAreaShiftPattern As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPattern In child.SystemAreaShiftPatternList
            SystemAreaShiftPattern.CheckRules()
          Next
          For Each SystemAreaShiftPatternWeekDay As OBLib.Maintenance.ShiftPatterns.SystemAreaShiftPatternWeekDay In child.SystemAreaShiftPatternWeekDayList
            SystemAreaShiftPatternWeekDay.CheckRules()
          Next
        Next
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemTeamShiftPatternList"
            cm.Parameters.AddWithValue("@SystemTeamID", Singular.Misc.NothingDBNull(crit.SystemTeamID))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        'For Each Child As SystemTeamShiftPattern In DeletedList
        '  Child.DeleteSelf()
        'Next

        ' Then clear the list of deleted objects because they are truly gone now.
        'DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SystemTeamShiftPattern In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
