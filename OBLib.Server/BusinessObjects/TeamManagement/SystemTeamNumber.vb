﻿' Generated 21 Mar 2016 07:59 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace TeamManagement

  <Serializable()> _
  Public Class SystemTeamNumber
    Inherits OBBusinessBase(Of SystemTeamNumber)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SystemTeamNumberIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTeamNumberID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SystemTeamNumberID() As Integer
      Get
        Return GetProperty(SystemTeamNumberIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), UnselectedText:="Sub-Dept", FilterMethodName:="GetAllowedSystems", ValueMember:="SystemID")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemTeamNumber, "Team Number", 0)
    ''' <summary>
    ''' Gets and sets the System Team Number value
    ''' </summary>
    <Display(Name:="Team Number", Description:="")>
    Public Property SystemTeamNumber() As Integer
      Get
        Return GetProperty(SystemTeamNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemTeamNumberProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamNumberNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemTeamNumberName, "Team Number Name", "")
    ''' <summary>
    ''' Gets and sets the System Team Number Name value
    ''' </summary>
    <Display(Name:="Team Number Name", Description:=""),
    StringLength(50, ErrorMessage:="Team Number Name cannot be more than 50 characters")>
    Public Property SystemTeamNumberName() As String
      Get
        Return GetProperty(SystemTeamNumberNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemTeamNumberNameProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemTeamNumberIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SystemTeamNumberName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "System Team Number")
        Else
          Return String.Format("Blank {0}", "System Team Number")
        End If
      Else
        Return Me.SystemTeamNumberName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(SystemTeamNumberProperty)
        .JavascriptRuleFunctionName = "SystemTeamNumberBO.SystemTeamNumberValid"
        .ServerRuleFunction = AddressOf SystemTeamNumberValid
      End With

    End Sub

    Public Shared Function SystemTeamNumberValid(STN As SystemTeamNumber) As String

      Dim ErrorDescription As String = ""
      If STN IsNot Nothing Then
        If STN.SystemTeamNumber <= 0 Then
          ErrorDescription = "Team Number Required"
        End If
      Else
        ErrorDescription = "Team Number Required"
      End If
      Return ErrorDescription

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSystemTeamNumber() method.

    End Sub

    Public Shared Function NewSystemTeamNumber() As SystemTeamNumber

      Return DataPortal.CreateChild(Of SystemTeamNumber)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSystemTeamNumber(dr As SafeDataReader) As SystemTeamNumber

      Dim s As New SystemTeamNumber()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemTeamNumberIDProperty, .GetInt32(0))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SystemTeamNumberProperty, .GetInt32(2))
          LoadProperty(SystemTeamNumberNameProperty, .GetString(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSystemTeamNumber"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSystemTeamNumber"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSystemTeamNumberID As SqlParameter = .Parameters.Add("@SystemTeamNumberID", SqlDbType.Int)
          paramSystemTeamNumberID.Value = GetProperty(SystemTeamNumberIDProperty)
          If Me.IsNew Then
            paramSystemTeamNumberID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@SystemTeamNumber", GetProperty(SystemTeamNumberProperty))
          .Parameters.AddWithValue("@SystemTeamNumberName", GetProperty(SystemTeamNumberNameProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SystemTeamNumberIDProperty, paramSystemTeamNumberID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSystemTeamNumber"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SystemTeamNumberID", GetProperty(SystemTeamNumberIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace