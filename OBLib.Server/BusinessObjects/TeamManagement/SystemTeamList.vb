﻿' Generated 10 Dec 2015 09:50 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Shifts.ICR

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace TeamManagement

  <Serializable()> _
  Public Class SystemTeamList
    Inherits OBBusinessListBase(Of SystemTeamList, SystemTeam)

#Region " Business Methods "

    Public Function GetItem(SystemTeamID As Integer) As SystemTeam

      For Each child As SystemTeam In Me
        If child.SystemTeamID = SystemTeamID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    'Public Function GetTeamRequirement(TeamRequirementID As Integer) As OBLib.Maintenance.PlayOutOps.TeamRequirement

    '  Dim obj As OBLib.Maintenance.PlayOutOps.TeamRequirement = Nothing
    '  For Each parent As SystemTeam In Me
    '    obj = parent.TeamRequirementList.GetItem(TeamRequirementID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

    Public Overrides Function ToString() As String

      Return "System Teams"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemTeamID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing

      Public Sub New()

      End Sub

      Public Sub New(SystemTeamID As Integer?, SystemID As Integer?)
        Me.SystemTeamID = SystemTeamID
        Me.SystemID = SystemID
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSystemTeamList() As SystemTeamList

      Return New SystemTeamList()

    End Function

    Public Shared Sub BeginGetSystemTeamList(CallBack As EventHandler(Of DataPortalResult(Of SystemTeamList)))

      Dim dp As New DataPortal(Of SystemTeamList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSystemTeamList() As SystemTeamList

      Return DataPortal.Fetch(Of SystemTeamList)(New Criteria())

    End Function

    Public Shared Function GetSystemTeamList(SystemTeamID As Integer?, SystemID As Integer?) As SystemTeamList

      Return DataPortal.Fetch(Of SystemTeamList)(New Criteria(SystemTeamID, SystemID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SystemTeam.GetSystemTeam(sdr))
      End While
      Me.RaiseListChangedEvents = True

      If Me.Count > 0 Then
        Dim parent As SystemTeam = Nothing
        If sdr.NextResult() Then
          While sdr.Read
            If parent Is Nothing OrElse parent.SystemTeamID <> sdr.GetInt32(1) Then
              parent = Me.GetItem(sdr.GetInt32(1))
            End If
            If parent IsNot Nothing Then
              parent.TeamDisciplineHumanResourceList.RaiseListChangedEvents = False
              parent.TeamDisciplineHumanResourceList.Add(TeamDisciplineHumanResource.GetTeamDisciplineHumanResource(sdr))
              parent.TeamDisciplineHumanResourceList.RaiseListChangedEvents = True
            End If
          End While
        End If

        'If sdr.NextResult() Then
        '  While sdr.Read
        '    If parent Is Nothing OrElse parent.SystemTeamID <> sdr.GetInt32(1) Then
        '      parent = Me.GetItem(sdr.GetInt32(1))
        '    End If
        '    If parent IsNot Nothing Then
        '      parent.TeamRequirementList.RaiseListChangedEvents = False
        '      parent.TeamRequirementList.Add(OBLib.Maintenance.PlayOutOps.TeamRequirement.GetTeamRequirement(sdr))
        '      parent.TeamRequirementList.RaiseListChangedEvents = True
        '    End If
        '  End While
        'End If

        'Dim parentTeamRequirementChild As OBLib.Maintenance.PlayOutOps.TeamRequirement = Nothing
        'If sdr.NextResult() Then
        '  While sdr.Read
        '    If parentTeamRequirementChild Is Nothing OrElse parentTeamRequirementChild.TeamRequirementID <> sdr.GetInt32(1) Then
        '      parentTeamRequirementChild = Me.GetTeamRequirement(sdr.GetInt32(1))
        '    End If
        '    If parentTeamRequirementChild IsNot Nothing Then
        '      parentTeamRequirementChild.TeamRequirementDetailList.RaiseListChangedEvents = False
        '      parentTeamRequirementChild.TeamRequirementDetailList.Add(OBLib.Maintenance.PlayOutOps.TeamRequirementDetail.GetTeamRequirementDetail(sdr))
        '      parentTeamRequirementChild.TeamRequirementDetailList.RaiseListChangedEvents = True
        '    End If
        '  End While
        'End If

        For Each child As SystemTeam In Me
          child.CheckRules()
          For Each TeamDisciplineHumanResource As TeamDisciplineHumanResource In child.TeamDisciplineHumanResourceList
            TeamDisciplineHumanResource.CheckRules()
          Next
          'For Each TeamRequirement As OBLib.Maintenance.PlayOutOps.TeamRequirement In child.TeamRequirementList
          '  TeamRequirement.CheckRules()
          '  For Each TeamRequirementDetail As OBLib.Maintenance.PlayOutOps.TeamRequirementDetail In TeamRequirement.TeamRequirementDetailList
          '    TeamRequirementDetail.CheckRules()
          '  Next
          'Next
        Next
      End If
    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSystemTeamList"
            cm.Parameters.AddWithValue("@SystemTeamID", Singular.Misc.NothingDBNull(crit.SystemTeamID))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SystemTeam In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SystemTeam In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
