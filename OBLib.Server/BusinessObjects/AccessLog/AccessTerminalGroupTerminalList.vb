﻿' Generated 05 Dec 2014 13:02 - Singular Systems Object Generator Version 2.1.664
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace AccessLogs

  <Serializable()> _
  Public Class AccessTerminalGroupTerminalList
    Inherits Singular.SingularBusinessListBase(Of AccessTerminalGroupTerminalList, AccessTerminalGroupTerminal)

#Region " Business Methods "

    Public Function GetItem(AccessTerminalGroupTerminalID As Integer) As AccessTerminalGroupTerminal

      For Each child As AccessTerminalGroupTerminal In Me
        If child.AccessTerminalGroupTerminalID = AccessTerminalGroupTerminalID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Access Terminal Group Terminals"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewAccessTerminalGroupTerminalList() As AccessTerminalGroupTerminalList

      Return New AccessTerminalGroupTerminalList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AccessTerminalGroupTerminal In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AccessTerminalGroupTerminal In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace