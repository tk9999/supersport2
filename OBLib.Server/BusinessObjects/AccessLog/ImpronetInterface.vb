﻿Imports System.Linq
Imports System.Data.SqlClient

Public Class ImpronetInterface

  Public Shared Sub GetData()

    Dim MaxDate As Csla.SmartDate = Nothing
    'Or make a stored proc??
    Using c As New SqlConnection(Singular.Settings.ConnectionString)
      c.Open()
      Dim cmd As New SqlCommand("SELECT MAX(AccessDateTime) FROM AccessTransactions", c)
      Dim sdr As SqlClient.SqlDataReader = cmd.ExecuteReader
      While sdr.Read
        If sdr.GetValue(0) Is DBNull.Value Then
          MaxDate = New Csla.SmartDate("2014-10-01 01:00:00")
        Else
          MaxDate = New Csla.SmartDate(sdr.GetDateTime(0))
        End If
      End While
      sdr.Close()
    End Using

    Dim StartDate = Now() 'New Csla.SmartDate("2014-11-01 01:00:00").Date 

    Using c As New SqlConnection(CommonData.Lists.MiscList(0).AccessLogConnectionString)
      c.Open()
      Dim str As String = ImpronetCommandWithoutWHERE
      If MaxDate.DBValue Is DBNull.Value Then
        str &= "WHERE t.TR_DateTime <= '" & StartDate.ToString("yyyy-MM-dd hh:mm:ss") & "'"
      Else
        str &= "WHERE t.TR_DateTime > '" & MaxDate.ToString("yyyy-MM-dd hh:mm:ss") & "'" & vbCrLf & "  AND t.TR_DateTime <= '" & StartDate.ToString("yyyy-MM-dd hh:mm:ss") & "'"
      End If
      Dim cmd As New SqlCommand(str, c)
      GetAndSaveData(cmd.ExecuteReader)

    End Using
    Using cn As New SqlConnection(Singular.Settings.ConnectionString)
      cn.Open()
      'Run matching procs
      Dim cmdSync As New SqlCommand("[CmdProcs].[cmdAccessDataSync]", cn)
      cmdSync.ExecuteNonQuery()
      Dim cmdShifts As New SqlCommand("[CmdProcs].[cmdAccessShiftSync] '" & StartDate.AddHours(-48).ToString("yyyy-MM-dd hh:mm:ss") & "'", cn)
      cmdShifts.ExecuteNonQuery()

    End Using


    Dim length = DateDiff(DateInterval.Second, StartDate, Now)

  End Sub

  Private Shared count As Integer = 0

  Public Shared Sub GetAndSaveData(sdr As SqlClient.SqlDataReader)
    Dim ROHRList As OBLib.HR.ReadOnly.ROHumanResourceList = OBLib.HR.ReadOnly.ROHumanResourceList.GetROHumanResourceList()
    Dim IDList As New Hashtable()
    ROHRList.Select(Function(c) c.IDNo).Distinct().OrderBy(Function(c) c).ToList.ForEach(Sub(c)
                                                                                           IDList.Add(c, "")
                                                                                         End Sub)
    Dim dt As DataTable = Nothing
    While sdr.Read()
      If dt Is Nothing Then
        'GetColumns
        dt = New DataTable
        For i = 0 To sdr.FieldCount - 1
          dt.Columns.Add(sdr.GetName(i), sdr.GetFieldType(i))
        Next
      End If
      Dim row As DataRow = dt.NewRow
      For i = 0 To sdr.FieldCount - 1
        row(i) = sdr.GetValue(i)
      Next
      If row("MST_ID") <> "" AndAlso IDList.Contains(row("MST_ID")) Then
        dt.Rows.Add(row)
      End If
      If dt.Rows.Count = 100 Then
        Dim tempDT = dt.Copy
        dt.Clear()
        Dim tempTask = New Threading.Thread(Sub()
                                              SaveData(tempDT) ', cn)
                                            End Sub)
        count += 1
        tempTask.Start()
      End If
    End While

    If dt.Rows.Count > 0 Then
      count += 1
      SaveData(dt) ', cn)
    End If

    While count > 0
      Threading.Thread.Sleep(5000)
    End While

    sdr.Close()

  End Sub

  Private Shared Sub SaveData(ByVal dt As DataTable)

    If dt.Columns.Contains("TR_TermSLA") AndAlso dt.Columns.Contains("TR_SQ") AndAlso dt.Columns.Contains("TR_DateTime") AndAlso dt.Columns.Contains("MST_ID") AndAlso dt.Columns.Contains("ET_Name") AndAlso
      dt.Columns.Contains("TERM_Name") AndAlso dt.Columns.Contains("TERM_Enabled") AndAlso dt.Columns.Contains("CTRL_SLA") AndAlso dt.Columns.Contains("LOC_No") AndAlso
      dt.Columns.Contains("LOC_Name") AndAlso dt.Columns.Contains("ZONE_No") AndAlso dt.Columns.Contains("ZONE_Name") Then
      'Has Required Columns
      Dim cmdstr As String = "INSERT INTO AccessStaging(TR_TermSLA, TR_SQ, TR_DateTime, UserIDNo, ET_Name, TERM_Name, TERM_Enabled, CTRL_SLA, LOC_No, LOC_Name, ZONE_No, ZONE_Name)" & vbCrLf


      For Each row As DataRow In dt.Rows
        cmdstr &= "SELECT '" & row.Item("TR_TermSLA").REPLACE("'", "''") & "', " & row.Item("TR_SQ") &
                  ", CONVERT(DATETIME, '" & New Csla.SmartDate(CStr(row.Item("TR_DateTime"))).ToString("yyyy-MM-dd hh:mm:ss") & "'), '" &
                  row.Item("MST_ID").REPLACE("'", "''") & "', '" &
                  row.Item("ET_Name").REPLACE("'", "''") & "', '" &
                  row.Item("TERM_Name").REPLACE("'", "''") &
                  "', CONVERT(BIT, " & row.Item("TERM_Enabled") & "), '" &
                  row.Item("CTRL_SLA").REPLACE("'", "''") & "', " &
                  row.Item("LOC_No") & ", '" &
                  row.Item("LOC_Name").REPLACE("'", "''") & "', " &
                  row.Item("ZONE_No") & ", '" &
                  row.Item("ZONE_Name").REPLACE("'", "''") & "' " & vbCrLf
        If dt.Rows.IndexOf(row) < dt.Rows.Count - 1 Then
          cmdstr &= "UNION ALL" & vbCrLf
        End If
      Next
      Using cn As New SqlClient.SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Dim cmd As New SqlClient.SqlCommand(cmdstr, cn)
        cmd.ExecuteNonQuery()
        count -= 1
      End Using
    End If

  End Sub

  Public Shared Property ImpronetCommandWithoutWHERE As String = "SELECT *" & vbCrLf &
                                                      "FROM (" & vbCrLf &
                                                      "     SELECT tr.TR_TermSLA, tr.TR_SQ," & vbCrLf &
                                                      "     				CONVERT(DATETIME, STUFF(STUFF(CONVERT(varchar(8), tr.TR_Date), 7, 0, '-'), 5, 0, '-') + ' ' + STUFF(STUFF(RIGHT('000000' + CONVERT(varchar(6), tr.TR_Time), 6), 5, 0, ':'), 3, 0, ':')) As TR_DateTime," & vbCrLf &
                                                      " 						m.MST_ID, et.ET_Name, t.TERM_Name, t.TERM_Enabled, " & vbCrLf &
                                                      "							t.CTRL_SLA, t.LOC_No, l.LOC_Name, l.ZONE_No, z.ZONE_Name" & vbCrLf &
                                                      "			FROM [TRANSACK] tr" & vbCrLf &
                                                      "			INNER JOIN [MASTER] m ON tr.TR_MSTSQ = m.MST_SQ" & vbCrLf &
                                                      "			INNER JOIN [EVENT_TYPE] et ON tr.TR_Event = et.ET_TypeNo" & vbCrLf &
                                                      "			INNER JOIN [TERMINAL] t ON tr.TR_TermSLA = t.TERM_SLA" & vbCrLf &
                                                      "			INNER JOIN [LOCATION] l ON t.LOC_No = l.LOC_No AND t.CTRL_SLA = l.CTRL_SLA" & vbCrLf &
                                                      "			INNER JOIN [ZONE] z ON l.ZONE_No = z.ZONE_No AND l.CTRL_SLA = z.CTRL_SLA" & vbCrLf &
                                                      ") t " '& vbCrLf &
  '"WHERE t.TR_DateTime > @Date" & vbCrLf &
  '"	AND t.TR_DateTime < GETDATE()"



End Class


