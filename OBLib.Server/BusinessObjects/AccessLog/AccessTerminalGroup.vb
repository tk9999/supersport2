﻿' Generated 05 Dec 2014 13:02 - Singular Systems Object Generator Version 2.1.664
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace AccessLogs

  <Serializable()> _
  Public Class AccessTerminalGroup
    Inherits OBBusinessBase(Of AccessTerminalGroup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessTerminalGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessTerminalGroupID, "Access Terminal Group", 0)
    ''' <summary>
    ''' Gets the Access Terminal Group value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccessTerminalGroupID() As Integer
      Get
        Return GetProperty(AccessTerminalGroupIDProperty)
      End Get
    End Property

    Public Shared AccessTerminalGroupProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccessTerminalGroup, "Access Terminal Group", "")
    ''' <summary>
    ''' Gets and sets the Access Terminal Group value
    ''' </summary>
    <Display(Name:="Access Terminal Group", Description:=""),
    StringLength(20, ErrorMessage:="Access Terminal Group cannot be more than 20 characters")>
  Public Property AccessTerminalGroup() As String
      Get
        Return GetProperty(AccessTerminalGroupProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AccessTerminalGroupProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    <Singular.DataAnnotations.ClientOnlyNoData()>
    Public Property IsExpanded As Boolean

#End Region

#Region " Child Lists "

    Public Shared AccessTerminalGroupTerminalListProperty As PropertyInfo(Of AccessTerminalGroupTerminalList) = RegisterProperty(Of AccessTerminalGroupTerminalList)(Function(c) c.AccessTerminalGroupTerminalList, "Access Terminal Group Terminal List")

    '<Display(AutoGenerateField:=False)> _
    Public ReadOnly Property AccessTerminalGroupTerminalList() As AccessTerminalGroupTerminalList
      Get
        If GetProperty(AccessTerminalGroupTerminalListProperty) Is Nothing Then
          LoadProperty(AccessTerminalGroupTerminalListProperty, AccessLogs.AccessTerminalGroupTerminalList.NewAccessTerminalGroupTerminalList())
        End If
        Return GetProperty(AccessTerminalGroupTerminalListProperty)
      End Get
    End Property

    Public Shared AccessTerminalGroupHumanResourceListProperty As PropertyInfo(Of AccessTerminalGroupHumanResourceList) = RegisterProperty(Of AccessTerminalGroupHumanResourceList)(Function(c) c.AccessTerminalGroupHumanResourceList, "Access Terminal Group Human Resource List")

    '<Display(AutoGenerateField:=False)> _
    Public ReadOnly Property AccessTerminalGroupHumanResourceList() As AccessTerminalGroupHumanResourceList
      Get
        If GetProperty(AccessTerminalGroupHumanResourceListProperty) Is Nothing Then
          LoadProperty(AccessTerminalGroupHumanResourceListProperty, AccessLogs.AccessTerminalGroupHumanResourceList.NewAccessTerminalGroupHumanResourceList())
        End If
        Return GetProperty(AccessTerminalGroupHumanResourceListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessTerminalGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AccessTerminalGroup.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Terminal Group")
        Else
          Return String.Format("Blank {0}", "Access Terminal Group")
        End If
      Else
        Return Me.AccessTerminalGroup
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"AccessTerminalGroupTerminals", "AccessTerminalGroupHumanResources"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(AccessTerminalGroupTerminalListProperty, Singular.Rules.RuleSeverity.Error)
        .AffectedProperties.Add(AccessTerminalGroupProperty)
        .JavascriptRuleCode = "var hasEntrance = false;" & vbCrLf &
                              "var hasExit = false;" & vbCrLf &
                              "for ( var i = 0; i < CtlError.Object.AccessTerminalGroupTerminalList().length; i++) {" & vbCrLf &
                              "  var obj = CtlError.Object.AccessTerminalGroupTerminalList()[i];" & vbCrLf &
                              "  if (obj.EntranceInd() == true){" & vbCrLf &
                              "    hasEntrance = true;" & vbCrLf &
                              "  } else {" & vbCrLf &
                              "    hasExit = true;" & vbCrLf &
                              "  }" & vbCrLf &
                              "}" & vbCrLf &
                              "if(!hasEntrance){" & vbCrLf &
                              "  CtlError.AddError(CtlError.Object.AccessTerminalGroup() + ' Terminal List must contain at least one Entrance.');" & vbCrLf &
                              "}" & vbCrLf &
                              "if(!hasExit){" & vbCrLf &
                              "  CtlError.AddError(CtlError.Object.AccessTerminalGroup() + ' Terminal List must contain at least one Exit.');" & vbCrLf &
                              "}"
      End With
    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessTerminalGroup() method.

    End Sub

    Public Shared Function NewAccessTerminalGroup() As AccessTerminalGroup

      Return DataPortal.CreateChild(Of AccessTerminalGroup)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessTerminalGroup(dr As SafeDataReader) As AccessTerminalGroup

      Dim a As New AccessTerminalGroup()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessTerminalGroupIDProperty, .GetInt32(0))
          LoadProperty(AccessTerminalGroupProperty, .GetString(1))
          LoadProperty(CreatedByProperty, .GetInt32(2))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(3))
          LoadProperty(ModifiedByProperty, .GetInt32(4))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insAccessTerminalGroup"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updAccessTerminalGroup"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessTerminalGroupID As SqlParameter = .Parameters.Add("@AccessTerminalGroupID", SqlDbType.Int)
          paramAccessTerminalGroupID.Value = GetProperty(AccessTerminalGroupIDProperty)
          If Me.IsNew Then
            paramAccessTerminalGroupID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccessTerminalGroup", GetProperty(AccessTerminalGroupProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessTerminalGroupIDProperty, paramAccessTerminalGroupID.Value)
          End If
          ' update child objects
          If GetProperty(AccessTerminalGroupTerminalListProperty) IsNot Nothing Then
            Me.AccessTerminalGroupTerminalList.Update()
          End If
          If GetProperty(AccessTerminalGroupHumanResourceListProperty) IsNot Nothing Then
            Me.AccessTerminalGroupHumanResourceList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(AccessTerminalGroupTerminalListProperty) IsNot Nothing Then
          Me.AccessTerminalGroupTerminalList.Update()
        End If
        If GetProperty(AccessTerminalGroupHumanResourceListProperty) IsNot Nothing Then
          Me.AccessTerminalGroupHumanResourceList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delAccessTerminalGroup"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccessTerminalGroupID", GetProperty(AccessTerminalGroupIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace