﻿' Generated 05 Dec 2014 13:03 - Singular Systems Object Generator Version 2.1.664
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace AccessLogs

  <Serializable()> _
  Public Class AccessTerminalGroupTerminal
    Inherits Singular.SingularBusinessBase(Of AccessTerminalGroupTerminal)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared AccessTerminalGroupTerminalIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessTerminalGroupTerminalID, "Access Terminal Group Terminal", 0)
    ''' <summary>
    ''' Gets the Access Terminal Group Terminal value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property AccessTerminalGroupTerminalID() As Integer
      Get
        Return GetProperty(AccessTerminalGroupTerminalIDProperty)
      End Get
    End Property

    Public Shared AccessTerminalGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessTerminalGroupID, "Access Terminal Group", Nothing)
    ''' <summary>
    ''' Gets the Access Terminal Group value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True)>
    Public ReadOnly Property AccessTerminalGroupID() As Integer?
      Get
        Return GetProperty(AccessTerminalGroupIDProperty)
      End Get
    End Property

    Public Shared AccessTerminalIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessTerminalID, "Access Terminal", Nothing)
    ''' <summary>
    ''' Gets and sets the Access Terminal value
    ''' </summary>
    <Display(Name:="Access Terminal", Description:=""),
    Required(ErrorMessage:="Access Terminal required"),
    Singular.DataAnnotations.DropDownWeb(GetType([ReadOnly].ROAccessTerminalList), ValueMember:="AccessTerminalID", DisplayMember:="DropDownDisplay")>
    Public Property AccessTerminalID() As Integer?
      Get
        Return GetProperty(AccessTerminalIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccessTerminalIDProperty, Value)
      End Set
    End Property

    Public Shared EntranceIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.EntranceInd, "Entrance", False)
    ''' <summary>
    ''' Gets and sets the Entrance value
    ''' </summary>
    <Display(Name:="Entrance", Description:=""),
    Required(ErrorMessage:="Entrance required")>
  Public Property EntranceInd() As Boolean
      Get
        Return GetProperty(EntranceIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(EntranceIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As AccessTerminalGroup

      Return CType(CType(Me.Parent, AccessTerminalGroupTerminalList).Parent, AccessTerminalGroup)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AccessTerminalGroupTerminalIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AccessTerminalID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Access Terminal Group Terminal")
        Else
          Return String.Format("Blank {0}", "Access Terminal Group Terminal")
        End If
      Else
        Return Me.AccessTerminalID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(AccessTerminalIDProperty, Singular.Rules.RuleSeverity.Warning)
        .JavascriptRuleCode = "if(Value){" & vbCrLf &
                              "  var obj = ClientData.ROAccessTerminalList.Find('AccessTerminalID', Value);" & vbCrLf &
                              "  if(obj){" & vbCrLf &
                              "    if(obj.EnabledInd == false){" & vbCrLf &
                              "      CtlError.AddWarning('This Terminal is not Enabled');" & vbCrLf &
                              "    }" & vbCrLf &
                              "  }" & vbCrLf &
                              "}"
      End With
      'With AddWebRule(EntranceIndProperty, Singular.Rules.RuleSeverity.Error)
      '  .JavascriptRuleCode = "var hasEntrance = false;" & vbCrLf &
      '                        "var hasExit = false;" & vbCrLf &
      '                        "for ( var i = 0; i < CtlError.Object.SInfo.ParentList().length; i++) {" & vbCrLf &
      '                        "  var obj = CtlError.Object.SInfo.ParentList()[i];" & vbCrLf &
      '                        "  if (obj.EntranceInd() == true){" & vbCrLf &
      '                        "    hasEntrance = true;" & vbCrLf &
      '                        "  } else {" & vbCrLf &
      '                        "    hasExit = true;" & vbCrLf &
      '                        "  }" & vbCrLf &
      '                        "}" & vbCrLf &
      '                        "if(!hasEntrance){" & vbCrLf &
      '                        "  CtlError.AddError('List must contain at least one Entrance.');" & vbCrLf &
      '                        "}" & vbCrLf &
      '                        "if(!hasExit){" & vbCrLf &
      '                        "  CtlError.AddError('List must contain at least one Exit.');" & vbCrLf &
      '                        "}"
      'End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAccessTerminalGroupTerminal() method.

    End Sub

    Public Shared Function NewAccessTerminalGroupTerminal() As AccessTerminalGroupTerminal

      Return DataPortal.CreateChild(Of AccessTerminalGroupTerminal)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetAccessTerminalGroupTerminal(dr As SafeDataReader) As AccessTerminalGroupTerminal

      Dim a As New AccessTerminalGroupTerminal()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AccessTerminalGroupTerminalIDProperty, .GetInt32(0))
          LoadProperty(AccessTerminalGroupIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AccessTerminalIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(EntranceIndProperty, .GetBoolean(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insAccessTerminalGroupTerminal"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updAccessTerminalGroupTerminal"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramAccessTerminalGroupTerminalID As SqlParameter = .Parameters.Add("@AccessTerminalGroupTerminalID", SqlDbType.Int)
          paramAccessTerminalGroupTerminalID.Value = GetProperty(AccessTerminalGroupTerminalIDProperty)
          If Me.IsNew Then
            paramAccessTerminalGroupTerminalID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@AccessTerminalGroupID", Me.GetParent().AccessTerminalGroupID)
          .Parameters.AddWithValue("@AccessTerminalID", GetProperty(AccessTerminalIDProperty))
          .Parameters.AddWithValue("@EntranceInd", GetProperty(EntranceIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(AccessTerminalGroupTerminalIDProperty, paramAccessTerminalGroupTerminalID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delAccessTerminalGroupTerminal"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@AccessTerminalGroupTerminalID", GetProperty(AccessTerminalGroupTerminalIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace