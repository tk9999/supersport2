﻿' Generated 10 Jun 2016 12:36 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.OutsideBroadcast
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace ProductionSystemAreas

  <Serializable()> _
  Public Class ProductionOutsourceService
    Inherits OBBusinessBase(Of ProductionOutsourceService)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsExpanded, "Is Expanded", False)
    ''' <summary>
    ''' Gets and sets the Is Finalised value
    ''' </summary>
    <Display(Name:="Is Expanded", Description:=""), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ProductionOutsourceServiceBO.ProductionOutsourceServiceBOToString(self)")

    Public Shared ProductionOutsourceServiceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionOutsourceServiceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ProductionOutsourceServiceID() As Integer
      Get
        Return GetProperty(ProductionOutsourceServiceIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The parent Production for this Outsource Service"),
    Required(ErrorMessage:="Production required")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared OutsourceServiceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OutsourceServiceTypeID, "Outsource Service Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Outsource Service Type value
    ''' </summary>
    <Display(Name:="Service Type", Description:="The type of outsource service that will be required at the production"),
    Required(ErrorMessage:="Service Type required"),
    DropDownWeb(GetType(ROOutsourceServiceTypeList))>
    Public Property OutsourceServiceTypeID() As Integer?
      Get
        Return GetProperty(OutsourceServiceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OutsourceServiceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierID, "Supplier", 0)
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="The supplier that this outsource service is from"),
    DropDownWeb(GetType(ROSupplierList))>
    Public Property SupplierID() As Integer
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SupplierIDProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="A description for the outsource service"),
    StringLength(200, ErrorMessage:="Description cannot be more than 200 characters")>
    Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared QuoteRequestedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteRequestedDate, "Requested Date")
    ''' <summary>
    ''' Gets and sets the Quote Requested Date value
    ''' </summary>
    <Display(Name:="Requested Date", Description:="The date that the quote was requested from the supplier")>
    Public Property QuoteRequestedDate As DateTime?
      Get
        Return GetProperty(QuoteRequestedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(QuoteRequestedDateProperty, Value)
      End Set
    End Property

    Public Shared QuoteReceivedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteReceivedDate, "Received Date")
    ''' <summary>
    ''' Gets and sets the Quote Received Date value
    ''' </summary>
    <Display(Name:="Received Date", Description:="The date that the quote was received from the supplier")>
    Public Property QuoteReceivedDate As DateTime?
      Get
        Return GetProperty(QuoteReceivedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(QuoteReceivedDateProperty, Value)
      End Set
    End Property

    Public Shared QuotedAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.QuotedAmount, "Amount", CDec(0))
    ''' <summary>
    ''' Gets and sets the Quoted Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:="The quoted amount for the outsource service from the supplier"),
    Required(ErrorMessage:="Amount required"),
    NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property QuotedAmount() As Decimal
      Get
        Return GetProperty(QuotedAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(QuotedAmountProperty, Value)
      End Set
    End Property

    Public Shared VATInclusiveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.VATInclusiveInd, "VAT?", False)
    ''' <summary>
    ''' Gets and sets the VAT Inclusive value
    ''' </summary>
    <Display(Name:="VAT?", Description:="Tick indicates that VAT is incuded in the quoted amount"),
    Required(ErrorMessage:="VAT? required")>
    Public Property VATInclusiveInd() As Boolean
      Get
        Return GetProperty(VATInclusiveIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(VATInclusiveIndProperty, Value)
      End Set
    End Property

    Public Shared NotRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.NotRequiredInd, "Not Required", False)
    ''' <summary>
    ''' Gets and sets the Not Required value
    ''' </summary>
    <Display(Name:="Not Required", Description:="True indicates that this outsource service is not required. A reason must be supplied if this is true"),
    Required(ErrorMessage:="Not Required required")>
    Public Property NotRequiredInd() As Boolean
      Get
        Return GetProperty(NotRequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(NotRequiredIndProperty, Value)
      End Set
    End Property

    Public Shared NotRequiredReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotRequiredReason, "Not Required Reason", "")
    ''' <summary>
    ''' Gets and sets the Not Required Reason value
    ''' </summary>
    <Display(Name:="Not Required Reason", Description:="Tick indicates that the outsource service was cancelled"),
    StringLength(100, ErrorMessage:="Not Required Reason cannot be more than 100 characters")>
    Public Property NotRequiredReason() As String
      Get
        Return GetProperty(NotRequiredReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotRequiredReasonProperty, Value)
      End Set
    End Property

    Public Shared NotRequiredByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NotRequiredBy, "Not Required By", Nothing)
    ''' <summary>
    ''' Gets and sets the Not Required By value
    ''' </summary>
    <Display(Name:="Not Required By", Description:="")>
    Public Property NotRequiredBy() As Integer?
      Get
        Return GetProperty(NotRequiredByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NotRequiredByProperty, Value)
      End Set
    End Property

    Public Shared SAPOrderNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SAPOrderNo, "SAP Order No", "")
    ''' <summary>
    ''' Gets and sets the SAP Order No value
    ''' </summary>
    <Display(Name:="SAP Order No", Description:="The SAP Order Number"),
    StringLength(50, ErrorMessage:="SAP Order No cannot be more than 50 characters")>
    Public Property SAPOrderNo() As String
      Get
        Return GetProperty(SAPOrderNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SAPOrderNoProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:=""),
    Required(ErrorMessage:="Production System Area required")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ProductionOutsourceServiceDetailListProperty As PropertyInfo(Of ProductionOutsourceServiceDetailList) = RegisterProperty(Of ProductionOutsourceServiceDetailList)(Function(c) c.ProductionOutsourceServiceDetailList, "Production Outsource Service Detail List")

    Public ReadOnly Property ProductionOutsourceServiceDetailList() As ProductionOutsourceServiceDetailList
      Get
        If GetProperty(ProductionOutsourceServiceDetailListProperty) Is Nothing Then
          LoadProperty(ProductionOutsourceServiceDetailListProperty, ProductionSystemAreas.ProductionOutsourceServiceDetailList.NewProductionOutsourceServiceDetailList())
        End If
        Return GetProperty(ProductionOutsourceServiceDetailListProperty)
      End Get
    End Property

    Public Shared ProductionOutsourceServiceTimelineListProperty As PropertyInfo(Of ProductionOutsourceServiceTimelineList) = RegisterProperty(Of ProductionOutsourceServiceTimelineList)(Function(c) c.ProductionOutsourceServiceTimelineList, "Production Outsource Service Timeline List")

    Public ReadOnly Property ProductionOutsourceServiceTimelineList() As ProductionOutsourceServiceTimelineList
      Get
        If GetProperty(ProductionOutsourceServiceTimelineListProperty) Is Nothing Then
          LoadProperty(ProductionOutsourceServiceTimelineListProperty, ProductionSystemAreas.ProductionOutsourceServiceTimelineList.NewProductionOutsourceServiceTimelineList())
        End If
        Return GetProperty(ProductionOutsourceServiceTimelineListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionOutsourceServiceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Description.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Outsource Service")
        Else
          Return String.Format("Blank {0}", "Production Outsource Service")
        End If
      Else
        Return Me.Description
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"ProductionOutsourceServiceDetails", "ProductionOutsourceServiceTimelines"}
      End Get
    End Property

    'Public Function GetParent() As PSAOutsideBroadcast
    '  Return CType(CType(Me.Parent, ProductionOutsourceServiceList).Parent, PSAOutsideBroadcast)
    'End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionOutsourceService() method.

    End Sub

    Public Shared Function NewProductionOutsourceService() As ProductionOutsourceService

      Return DataPortal.CreateChild(Of ProductionOutsourceService)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetProductionOutsourceService(dr As SafeDataReader) As ProductionOutsourceService

      Dim p As New ProductionOutsourceService()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionOutsourceServiceIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(OutsourceServiceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(SupplierIDProperty, .GetInt32(3))
          LoadProperty(DescriptionProperty, .GetString(4))
          LoadProperty(QuoteRequestedDateProperty, .GetValue(5))
          LoadProperty(QuoteReceivedDateProperty, .GetValue(6))
          LoadProperty(QuotedAmountProperty, .GetDecimal(7))
          LoadProperty(VATInclusiveIndProperty, .GetBoolean(8))
          LoadProperty(NotRequiredIndProperty, .GetBoolean(9))
          LoadProperty(NotRequiredReasonProperty, .GetString(10))
          LoadProperty(NotRequiredByProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(SAPOrderNoProperty, .GetString(12))
          LoadProperty(CreatedByProperty, .GetInt32(13))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(14))
          LoadProperty(ModifiedByProperty, .GetInt32(15))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(16))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionOutsourceServiceIDProperty)

      'Dim prnt As PSAOutsideBroadcast = Me.GetParent
      'If prnt Is Nothing Then
      '  cm.Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
      '  cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      '  cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionSystemAreaIDProperty)))
      'Else
      '  cm.Parameters.AddWithValue("@ProductionID", prnt.ProductionID)
      '  cm.Parameters.AddWithValue("@SystemID", prnt.SystemID)
      '  cm.Parameters.AddWithValue("@ProductionSystemAreaID", prnt.ProductionSystemAreaID)
      'End If
      'cm.Parameters.AddWithValue("@OutsourceServiceTypeID", GetProperty(OutsourceServiceTypeIDProperty))
      'cm.Parameters.AddWithValue("@SupplierID", GetProperty(SupplierIDProperty))
      'cm.Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
      'cm.Parameters.AddWithValue("@QuoteRequestedDate", Singular.Misc.NothingDBNull(QuoteRequestedDate))
      'cm.Parameters.AddWithValue("@QuoteReceivedDate", Singular.Misc.NothingDBNull(QuoteReceivedDate))
      'cm.Parameters.AddWithValue("@QuotedAmount", GetProperty(QuotedAmountProperty))
      'cm.Parameters.AddWithValue("@VATInclusiveInd", GetProperty(VATInclusiveIndProperty))
      'cm.Parameters.AddWithValue("@NotRequiredInd", GetProperty(NotRequiredIndProperty))
      'cm.Parameters.AddWithValue("@NotRequiredReason", GetProperty(NotRequiredReasonProperty))
      'cm.Parameters.AddWithValue("@NotRequiredBy", Singular.Misc.NothingDBNull(GetProperty(NotRequiredByProperty)))
      'cm.Parameters.AddWithValue("@SAPOrderNo", GetProperty(SAPOrderNoProperty))
      'cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionOutsourceServiceIDProperty, cm.Parameters("@ProductionOutsourceServiceID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(ProductionOutsourceServiceDetailListProperty))
      UpdateChild(GetProperty(ProductionOutsourceServiceTimelineListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ProductionOutsourceServiceID", GetProperty(ProductionOutsourceServiceIDProperty))
    End Sub

#End Region

  End Class

End Namespace