﻿' Generated 10 Jun 2016 12:37 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace ProductionSystemAreas

  <Serializable()> _
  Public Class ProductionOutsourceServiceTimelineList
    Inherits OBBusinessListBase(Of ProductionOutsourceServiceTimelineList, ProductionOutsourceServiceTimeline)

#Region " Business Methods "

    Public Function GetItem(ProductionOutsourceServiceTimelineID As Integer) As ProductionOutsourceServiceTimeline

      For Each child As ProductionOutsourceServiceTimeline In Me
        If child.ProductionOutsourceServiceTimelineID = ProductionOutsourceServiceTimelineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Outsource Service Timelines"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewProductionOutsourceServiceTimelineList() As ProductionOutsourceServiceTimelineList

      Return New ProductionOutsourceServiceTimelineList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace