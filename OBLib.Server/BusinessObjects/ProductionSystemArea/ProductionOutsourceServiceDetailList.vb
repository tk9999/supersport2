﻿' Generated 10 Jun 2016 12:36 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace ProductionSystemAreas

  <Serializable()> _
  Public Class ProductionOutsourceServiceDetailList
    Inherits OBBusinessListBase(Of ProductionOutsourceServiceDetailList, ProductionOutsourceServiceDetail)

#Region " Business Methods "

    Public Function GetItem(ProductionOutsourceServiceDetailID As Integer) As ProductionOutsourceServiceDetail

      For Each child As ProductionOutsourceServiceDetail In Me
        If child.ProductionOutsourceServiceDetailID = ProductionOutsourceServiceDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Outsource Service Details"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewProductionOutsourceServiceDetailList() As ProductionOutsourceServiceDetailList

      Return New ProductionOutsourceServiceDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace