﻿' Generated 10 Jun 2016 12:36 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace ProductionSystemAreas

  <Serializable()> _
  Public Class ProductionOutsourceServiceList
    Inherits OBBusinessListBase(Of ProductionOutsourceServiceList, ProductionOutsourceService)

#Region " Business Methods "

    Public Function GetItem(ProductionOutsourceServiceID As Integer) As ProductionOutsourceService

      For Each child As ProductionOutsourceService In Me
        If child.ProductionOutsourceServiceID = ProductionOutsourceServiceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Outsource Services"

    End Function

    Public Function GetProductionOutsourceServiceDetail(ProductionOutsourceServiceDetailID As Integer) As ProductionOutsourceServiceDetail

      Dim obj As ProductionOutsourceServiceDetail = Nothing
      For Each parent As ProductionOutsourceService In Me
        obj = parent.ProductionOutsourceServiceDetailList.GetItem(ProductionOutsourceServiceDetailID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetProductionOutsourceServiceTimeline(ProductionOutsourceServiceTimelineID As Integer) As ProductionOutsourceServiceTimeline

      Dim obj As ProductionOutsourceServiceTimeline = Nothing
      For Each parent As ProductionOutsourceService In Me
        obj = parent.ProductionOutsourceServiceTimelineList.GetItem(ProductionOutsourceServiceTimelineID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionSystemAreaID As Integer?

      Public Sub New(ProductionSystemAreaID As Integer?)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewProductionOutsourceServiceList() As ProductionOutsourceServiceList

      Return New ProductionOutsourceServiceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetProductionOutsourceServiceList() As ProductionOutsourceServiceList

      Return DataPortal.Fetch(Of ProductionOutsourceServiceList)(New Criteria())

    End Function

    Public Shared Function GetProductionOutsourceServiceList(ProductionSystemAreaID As Integer?) As ProductionOutsourceServiceList

      Return DataPortal.Fetch(Of ProductionOutsourceServiceList)(New Criteria(ProductionSystemAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionOutsourceService.GetProductionOutsourceService(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ProductionOutsourceService = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionOutsourceServiceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionOutsourceServiceDetailList.RaiseListChangedEvents = False
          parent.ProductionOutsourceServiceDetailList.Add(ProductionOutsourceServiceDetail.GetProductionOutsourceServiceDetail(sdr))
          parent.ProductionOutsourceServiceDetailList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionOutsourceServiceID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ProductionOutsourceServiceTimelineList.RaiseListChangedEvents = False
          parent.ProductionOutsourceServiceTimelineList.Add(ProductionOutsourceServiceTimeline.GetProductionOutsourceServiceTimeline(sdr))
          parent.ProductionOutsourceServiceTimelineList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As ProductionOutsourceService In Me
        child.CheckRules()
        For Each ProductionOutsourceServiceDetail As ProductionOutsourceServiceDetail In child.ProductionOutsourceServiceDetailList
          ProductionOutsourceServiceDetail.CheckRules()
        Next
        For Each ProductionOutsourceServiceTimeline As ProductionOutsourceServiceTimeline In child.ProductionOutsourceServiceTimelineList
          ProductionOutsourceServiceTimeline.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionOutsourceServiceList"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace