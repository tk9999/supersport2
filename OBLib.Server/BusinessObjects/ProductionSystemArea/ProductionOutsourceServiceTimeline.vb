﻿' Generated 10 Jun 2016 12:37 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace ProductionSystemAreas

  <Serializable()> _
  Public Class ProductionOutsourceServiceTimeline
    Inherits OBBusinessBase(Of ProductionOutsourceServiceTimeline)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ProductionOutsourceServiceTimelineBO.ProductionOutsourceServiceTimelineBOToString(self)")

    Public Shared ProductionOutsourceServiceTimelineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionOutsourceServiceTimelineID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionOutsourceServiceTimelineID() As Integer
      Get
        Return GetProperty(ProductionOutsourceServiceTimelineIDProperty)
      End Get
    End Property

    Public Shared ProductionOutsourceServiceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionOutsourceServiceID, "Production Outsource Service", Nothing)
    ''' <summary>
    ''' Gets the Production Outsource Service value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionOutsourceServiceID() As Integer?
      Get
        Return GetProperty(ProductionOutsourceServiceIDProperty)
      End Get
    End Property

    Public Shared ServiceStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ServiceStartDateTime, "Service Start Date Time")
    ''' <summary>
    ''' Gets and sets the Service Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:="The date that this service is required from")>
    Public Property ServiceStartDateTime As DateTime?
      Get
        Return GetProperty(ServiceStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ServiceStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ServiceEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ServiceEndDateTime, "Service End Date Time")
    ''' <summary>
    ''' Gets and sets the Service End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:="The date that this service is required until")>
    Public Property ServiceEndDateTime As DateTime?
      Get
        Return GetProperty(ServiceEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ServiceEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CateringBreakfastIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CateringBreakfastInd, "Catering Breakfast", False)
    ''' <summary>
    ''' Gets and sets the Catering Breakfast value
    ''' </summary>
    <Display(Name:="Breakfast", Description:="Tick indicated that this outsource service is supplying breakfasts")>
    Public Property CateringBreakfastInd() As Boolean
      Get
        Return GetProperty(CateringBreakfastIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CateringBreakfastIndProperty, Value)
      End Set
    End Property

    Public Shared CateringLunchIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CateringLunchInd, "Catering Lunch", False)
    ''' <summary>
    ''' Gets and sets the Catering Lunch value
    ''' </summary>
    <Display(Name:="Lunch", Description:="Tick indicated that this outsource service is supplying lunches")>
    Public Property CateringLunchInd() As Boolean
      Get
        Return GetProperty(CateringLunchIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CateringLunchIndProperty, Value)
      End Set
    End Property

    Public Shared CateringDinnerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CateringDinnerInd, "Catering Dinner", False)
    ''' <summary>
    ''' Gets and sets the Catering Dinner value
    ''' </summary>
    <Display(Name:="Dinner", Description:="Tick indicated that this outsource service is supplying dinners")>
    Public Property CateringDinnerInd() As Boolean
      Get
        Return GetProperty(CateringDinnerIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CateringDinnerIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionOutsourceService

      Return CType(CType(Me.Parent, ProductionOutsourceServiceTimelineList).Parent, ProductionOutsourceService)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionOutsourceServiceTimelineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Outsource Service Timeline")
        Else
          Return String.Format("Blank {0}", "Production Outsource Service Timeline")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionOutsourceServiceTimeline() method.

    End Sub

    Public Shared Function NewProductionOutsourceServiceTimeline() As ProductionOutsourceServiceTimeline

      Return DataPortal.CreateChild(Of ProductionOutsourceServiceTimeline)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetProductionOutsourceServiceTimeline(dr As SafeDataReader) As ProductionOutsourceServiceTimeline

      Dim p As New ProductionOutsourceServiceTimeline()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionOutsourceServiceTimelineIDProperty, .GetInt32(0))
          LoadProperty(ProductionOutsourceServiceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ServiceStartDateTimeProperty, .GetValue(2))
          LoadProperty(ServiceEndDateTimeProperty, .GetValue(3))
          LoadProperty(CateringBreakfastIndProperty, .GetBoolean(4))
          LoadProperty(CateringLunchIndProperty, .GetBoolean(5))
          LoadProperty(CateringDinnerIndProperty, .GetBoolean(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionOutsourceServiceTimelineIDProperty)

      cm.Parameters.AddWithValue("@ProductionOutsourceServiceID", Me.GetParent().ProductionOutsourceServiceID)
      cm.Parameters.AddWithValue("@ServiceStartDateTime", Singular.Misc.NothingDBNull(ServiceStartDateTime))
      cm.Parameters.AddWithValue("@ServiceEndDateTime", Singular.Misc.NothingDBNull(ServiceEndDateTime))
      cm.Parameters.AddWithValue("@CateringBreakfastInd", GetProperty(CateringBreakfastIndProperty))
      cm.Parameters.AddWithValue("@CateringLunchInd", GetProperty(CateringLunchIndProperty))
      cm.Parameters.AddWithValue("@CateringDinnerInd", GetProperty(CateringDinnerIndProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionOutsourceServiceTimelineIDProperty, cm.Parameters("@ProductionOutsourceServiceTimelineID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ProductionOutsourceServiceTimelineID", GetProperty(ProductionOutsourceServiceTimelineIDProperty))
    End Sub

#End Region

  End Class

End Namespace