﻿' Generated 12 Jun 2016 11:59 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.SystemManagement.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ProductionSystemAreas

  <Serializable()> _
  Public Class ProductionSystemAreaPosition
    Inherits OBBusinessBase(Of ProductionSystemAreaPosition)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ProductionSystemAreaPositionBO.ProductionSystemAreaPositionBOToString(self)")

    Public Shared ProductionSpecRequirementPositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSpecRequirementPositionID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ProductionSpecRequirementPositionID() As Integer
      Get
        Return GetProperty(ProductionSpecRequirementPositionIDProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position", Nothing)
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:=""),
    DropDownWeb(GetType(ROPositionList))>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentSubTypeID, "Sub Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Sub Type", Description:=""),
    DropDownWeb(GetType(ROEquipmentSubTypeList))>
    Public Property EquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(EquipmentSubTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentSubTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentQuantityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentQuantity, "Quantity")
    ''' <summary>
    ''' Gets and sets the Equipment Quantity value
    ''' </summary>
    <Display(Name:="Quantity", Description:=""),
    Required(ErrorMessage:="Quantity required")>
    Public Property EquipmentQuantity() As Integer
      Get
        Return GetProperty(EquipmentQuantityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EquipmentQuantityProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:=""),
    Required(ErrorMessage:="Production System Area required")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    Required(ErrorMessage:="Discipline required"),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplineList), DisplayMember:="Discipline", ValueMember:="DisciplineID")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSpecRequirementPositionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionSpecRequirementPositionID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production System Area Position")
        Else
          Return String.Format("Blank {0}", "Production System Area Position")
        End If
      Else
        Return Me.ProductionSpecRequirementPositionID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionSystemAreaPosition() method.

    End Sub

    Public Shared Function NewProductionSystemAreaPosition() As ProductionSystemAreaPosition

      Return DataPortal.CreateChild(Of ProductionSystemAreaPosition)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetProductionSystemAreaPosition(dr As SafeDataReader) As ProductionSystemAreaPosition

      Dim p As New ProductionSystemAreaPosition()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionSpecRequirementPositionIDProperty, .GetInt32(0))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(EquipmentQuantityProperty, .GetInt32(3))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(8))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionSpecRequirementPositionIDProperty)

      cm.Parameters.AddWithValue("@PositionID", NothingDBNull(GetProperty(PositionIDProperty)))
      cm.Parameters.AddWithValue("@EquipmentSubTypeID", NothingDBNull(GetProperty(EquipmentSubTypeIDProperty)))
      cm.Parameters.AddWithValue("@EquipmentQuantity", GetProperty(EquipmentQuantityProperty))
      cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(GetProperty(ProductionSystemAreaIDProperty)))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(GetProperty(DisciplineIDProperty)))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionSpecRequirementPositionIDProperty, cm.Parameters("@ProductionSpecRequirementPositionID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ProductionSpecRequirementPositionID", GetProperty(ProductionSpecRequirementPositionIDProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
    End Sub

#End Region

  End Class

End Namespace