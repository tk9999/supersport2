﻿' Generated 12 Jun 2016 11:12 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports OBLib

Namespace ProductionSystemAreas

  <Serializable()> _
  Public Class ProductionSystemAreaEquipmentTypeList
    Inherits OBBusinessListBase(Of ProductionSystemAreaEquipmentTypeList, ProductionSystemAreaEquipmentType)

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementEquipmentTypeID As Integer) As ProductionSystemAreaEquipmentType

      For Each child As ProductionSystemAreaEquipmentType In Me
        If child.ProductionSpecRequirementEquipmentTypeID = ProductionSpecRequirementEquipmentTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionSystemAreaID As Integer?

      Public Sub New(ProductionSystemAreaID As Integer?)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewProductionSystemAreaEquipmentTypeList() As ProductionSystemAreaEquipmentTypeList

      Return New ProductionSystemAreaEquipmentTypeList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetProductionSystemAreaEquipmentTypeList() As ProductionSystemAreaEquipmentTypeList

      Return DataPortal.Fetch(Of ProductionSystemAreaEquipmentTypeList)(New Criteria())

    End Function

    Public Shared Function GetProductionSystemAreaEquipmentTypeList(ProductionSystemAreaID As Integer?) As ProductionSystemAreaEquipmentTypeList

      Return DataPortal.Fetch(Of ProductionSystemAreaEquipmentTypeList)(New Criteria(ProductionSystemAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionSystemAreaEquipmentType.GetProductionSystemAreaEquipmentType(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionSystemAreaEquipmentTypeList"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace