﻿' Generated 10 Jun 2016 12:37 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace ProductionSystemAreas

  <Serializable()> _
  Public Class ProductionOutsourceServiceDetail
    Inherits OBBusinessBase(Of ProductionOutsourceServiceDetail)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ProductionOutsourceServiceDetailBO.ProductionOutsourceServiceDetailBOToString(self)")

    Public Shared ProductionOutsourceServiceDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionOutsourceServiceDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionOutsourceServiceDetailID() As Integer
      Get
        Return GetProperty(ProductionOutsourceServiceDetailIDProperty)
      End Get
    End Property

    Public Shared ProductionOutsourceServiceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionOutsourceServiceID, "Production Outsource Service", Nothing)
    ''' <summary>
    ''' Gets the Production Outsource Service value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionOutsourceServiceID() As Integer?
      Get
        Return GetProperty(ProductionOutsourceServiceIDProperty)
      End Get
    End Property

    Public Shared ProductionOutsourceServiceDetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionOutsourceServiceDetail, "Production Outsource Service Detail", "")
    ''' <summary>
    ''' Gets and sets the Production Outsource Service Detail value
    ''' </summary>
    <Display(Name:="Detail", Description:="The details of the outsource service"),
    Required(ErrorMessage:="Detail required"),
    StringLength(100, ErrorMessage:="Detail cannot be more than 100 characters")>
    Public Property ProductionOutsourceServiceDetail() As String
      Get
        Return GetProperty(ProductionOutsourceServiceDetailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionOutsourceServiceDetailProperty, Value)
      End Set
    End Property

    Public Shared RequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.RequiredInd, "Required", True)
    ''' <summary>
    ''' Gets and sets the Required value
    ''' </summary>
    <Display(Name:="Required", Description:="Tick indicates that this detail is required")>
    Public Property RequiredInd() As Boolean
      Get
        Return GetProperty(RequiredIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(RequiredIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As ProductionOutsourceService

      Return CType(CType(Me.Parent, ProductionOutsourceServiceDetailList).Parent, ProductionOutsourceService)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionOutsourceServiceDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ProductionOutsourceServiceDetail.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Outsource Service Detail")
        Else
          Return String.Format("Blank {0}", "Production Outsource Service Detail")
        End If
      Else
        Return Me.ProductionOutsourceServiceDetail
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionOutsourceServiceDetail() method.

    End Sub

    Public Shared Function NewProductionOutsourceServiceDetail() As ProductionOutsourceServiceDetail

      Return DataPortal.CreateChild(Of ProductionOutsourceServiceDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetProductionOutsourceServiceDetail(dr As SafeDataReader) As ProductionOutsourceServiceDetail

      Dim p As New ProductionOutsourceServiceDetail()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionOutsourceServiceDetailIDProperty, .GetInt32(0))
          LoadProperty(ProductionOutsourceServiceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionOutsourceServiceDetailProperty, .GetString(2))
          LoadProperty(RequiredIndProperty, .GetBoolean(3))
          LoadProperty(CreatedByProperty, .GetInt32(4))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(5))
          LoadProperty(ModifiedByProperty, .GetInt32(6))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionOutsourceServiceDetailIDProperty)

      cm.Parameters.AddWithValue("@ProductionOutsourceServiceID", Me.GetParent().ProductionOutsourceServiceID)
      cm.Parameters.AddWithValue("@ProductionOutsourceServiceDetail", GetProperty(ProductionOutsourceServiceDetailProperty))
      cm.Parameters.AddWithValue("@RequiredInd", GetProperty(RequiredIndProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionOutsourceServiceDetailIDProperty, cm.Parameters("@ProductionOutsourceServiceDetailID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ProductionOutsourceServiceDetailID", GetProperty(ProductionOutsourceServiceDetailIDProperty))
    End Sub

#End Region

  End Class

End Namespace