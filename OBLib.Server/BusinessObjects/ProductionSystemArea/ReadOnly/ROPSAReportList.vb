﻿' Generated 02 Sep 2016 18:40 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ProductionSystemAreas.ReadOnly

  <Serializable()> _
  Public Class ROPSAReportList
    Inherits OBReadOnlyListBase(Of ROPSAReportList, ROPSAReport)

#Region " Business Methods "

    Public Function GetItem(ProductionSystemAreaID As Integer) As ROPSAReport

      For Each child As ROPSAReport In Me
        If child.ProductionSystemAreaID = ProductionSystemAreaID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property ProductionSystemAreaName As String = ""
      Public Property SystemIDs As New List(Of Integer)
      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?
      Public Property ProductionAreaIDs As New List(Of Integer)
      Public Property StartDate As DateTime?
      Public Property EndDate As DateTime?

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROPSAReportList() As ROPSAReportList

      Return New ROPSAReportList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROPSAReportList() As ROPSAReportList

      Return DataPortal.Fetch(Of ROPSAReportList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROPSAReport.GetROPSAReport(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      'Dim parent As ROPSAReport = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.ProductionSystemAreaID <> sdr.GetInt32(0) Then
      '      parent = Me.GetItem(sdr.GetInt32(0))
      '    End If
      '    parent.ROPSADayList.RaiseListChangedEvents = False
      '    parent.ROPSADayList.Add(ROPSADay.GetROPSADay(sdr))
      '    parent.ROPSADayList.RaiseListChangedEvents = True
      '  End While
      'End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.[getROPSAListReport]"
            cm.Parameters.AddWithValue("@StartDate", crit.StartDate)
            cm.Parameters.AddWithValue("@EndDate", crit.EndDate)
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaName", Singular.Strings.MakeEmptyDBNull(crit.ProductionSystemAreaName))
            cm.Parameters.AddWithValue("@SystemIDs", Singular.Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.SystemIDs)))
            cm.Parameters.AddWithValue("@ProductionAreaIDs", Singular.Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.ProductionAreaIDs)))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace