﻿' Generated 14 Apr 2016 13:25 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ProductionSystemAreas.ReadOnly

  <Serializable()> _
  Public Class ROProductionSpecRequirementPositionList
    Inherits OBReadOnlyListBase(Of ROProductionSpecRequirementPositionList, ROProductionSpecRequirementPosition)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "


    ' ** Example Paging property declaration code to be implemented in the necessary VM **
    'Public Property ROProductionSpecRequirementPositionList As ROProductionSpecRequirementPositionList
    'Public Property ROProductionSpecRequirementPositionListCriteria As ROProductionSpecRequirementPositionList.Criteria
    'Public Property ROProductionSpecRequirementPositionListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    ' ** Example Paging definition code to be implemented in the necessary VM Setup Method **
    'ROProductionSpecRequirementPositionList = New ROProductionSpecRequirementPositionList
    'ROProductionSpecRequirementPositionListCriteria = New ROProductionSpecRequirementPositionList.Criteria
    'ROProductionSpecRequirementPositionListManager = New Singular.Web.Data.PagedDataManager(Of VMType)(Function(d) d.ROProductionSpecRequirementPositionList, Function(d) d.ROProductionSpecRequirementPositionListCriteria, "SortBy", PageSize, SortAsc)

    Public Function GetItem(ProductionSpecRequirementPositionID As Integer) As ROProductionSpecRequirementPosition

      For Each child As ROProductionSpecRequirementPosition In Me
        If child.ProductionSpecRequirementPositionID = ProductionSpecRequirementPositionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Production Spec Requirement Positions"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property ProductionSystemAreaID As Integer? = Nothing

      Public Sub New(ProductionSystemAreaID As Integer?)

        Me.ProductionSystemAreaID = ProductionSystemAreaID

      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROProductionSpecRequirementPositionList() As ROProductionSpecRequirementPositionList

      Return New ROProductionSpecRequirementPositionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROProductionSpecRequirementPositionList() As ROProductionSpecRequirementPositionList

      Return DataPortal.Fetch(Of ROProductionSpecRequirementPositionList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionSpecRequirementPosition.GetROProductionSpecRequirementPosition(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionSpecRequirementPositionList"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(crit.ProductionSystemAreaID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace