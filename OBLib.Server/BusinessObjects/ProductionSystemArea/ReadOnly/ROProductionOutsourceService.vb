﻿' Generated 01 Sep 2015 14:13 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace ProductionSystemAreas.ReadOnly

  <Serializable()> _
  Public Class ROProductionOutsourceService
    Inherits OBReadOnlyBase(Of ROProductionOutsourceService)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionOutsourceServiceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionOutsourceServiceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionOutsourceServiceID() As Integer
      Get
        Return GetProperty(ProductionOutsourceServiceIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The parent Production for this Outsource Service")>
    Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared OutsourceServiceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OutsourceServiceTypeID, "Outsource Service Type", Nothing)
    ''' <summary>
    ''' Gets the Outsource Service Type value
    ''' </summary>
    <Display(Name:="Outsource Service Type", Description:="The type of outsource service that will be required at the production")>
    Public ReadOnly Property OutsourceServiceTypeID() As Integer?
      Get
        Return GetProperty(OutsourceServiceTypeIDProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierID, "Supplier", 0)
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="The supplier that this outsource service is from")>
    Public ReadOnly Property SupplierID() As Integer
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:="A description for the outsource service")>
    Public ReadOnly Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
    End Property

    Public Shared QuoteRequestedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteRequestedDate, "Quote Requested Date")
    ''' <summary>
    ''' Gets the Quote Requested Date value
    ''' </summary>
    <Display(Name:="Quote Requested Date", Description:="The date that the quote was requested from the supplier")>
    Public ReadOnly Property QuoteRequestedDate As DateTime?
      Get
        Return GetProperty(QuoteRequestedDateProperty)
      End Get
    End Property

    Public Shared QuoteReceivedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteReceivedDate, "Quote Received Date")
    ''' <summary>
    ''' Gets the Quote Received Date value
    ''' </summary>
    <Display(Name:="Quote Received Date", Description:="The date that the quote was received from the supplier")>
    Public ReadOnly Property QuoteReceivedDate As DateTime?
      Get
        Return GetProperty(QuoteReceivedDateProperty)
      End Get
    End Property

    Public Shared QuotedAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.QuotedAmount, "Quoted Amount", CDec(0))
    ''' <summary>
    ''' Gets the Quoted Amount value
    ''' </summary>
    <Display(Name:="Quoted Amount", Description:="The quoted amount for the outsource service from the supplier")>
    Public ReadOnly Property QuotedAmount() As Decimal
      Get
        Return GetProperty(QuotedAmountProperty)
      End Get
    End Property

    Public Shared VATInclusiveIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.VATInclusiveInd, "VAT Inclusive", False)
    ''' <summary>
    ''' Gets the VAT Inclusive value
    ''' </summary>
    <Display(Name:="VAT Inclusive", Description:="Tick indicates that VAT is incuded in the quoted amount")>
    Public ReadOnly Property VATInclusiveInd() As Boolean
      Get
        Return GetProperty(VATInclusiveIndProperty)
      End Get
    End Property

    Public Shared NotRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.NotRequiredInd, "Not Required", False)
    ''' <summary>
    ''' Gets the Not Required value
    ''' </summary>
    <Display(Name:="Not Required", Description:="True indicates that this outsource service is not required. A reason must be supplied if this is true")>
    Public ReadOnly Property NotRequiredInd() As Boolean
      Get
        Return GetProperty(NotRequiredIndProperty)
      End Get
    End Property

    Public Shared NotRequiredReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotRequiredReason, "Not Required Reason", "")
    ''' <summary>
    ''' Gets the Not Required Reason value
    ''' </summary>
    <Display(Name:="Not Required Reason", Description:="Tick indicates that the outsource service was cancelled")>
    Public ReadOnly Property NotRequiredReason() As String
      Get
        Return GetProperty(NotRequiredReasonProperty)
      End Get
    End Property

    Public Shared NotRequiredByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NotRequiredBy, "Not Required By", Nothing)
    ''' <summary>
    ''' Gets the Not Required By value
    ''' </summary>
    <Display(Name:="Not Required By", Description:="")>
    Public ReadOnly Property NotRequiredBy() As Integer?
      Get
        Return GetProperty(NotRequiredByProperty)
      End Get
    End Property

    Public Shared SAPOrderNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SAPOrderNo, "SAP Order No", "")
    ''' <summary>
    ''' Gets the SAP Order No value
    ''' </summary>
    <Display(Name:="SAP Order No", Description:="The SAP Order Number")>
    Public ReadOnly Property SAPOrderNo() As String
      Get
        Return GetProperty(SAPOrderNoProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
    Public ReadOnly Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNo, "RowNo", 0)
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="RowNo", Description:="")>
    Public ReadOnly Property RowNo() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared OutsourceServiceTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OutsourceServiceType, "Service Type", "")
    ''' <summary>
    ''' Gets the SAP Order No value
    ''' </summary>
    <Display(Name:="Service Type")>
    Public ReadOnly Property OutsourceServiceType() As String
      Get
        Return GetProperty(OutsourceServiceTypeProperty)
      End Get
    End Property

    Public Shared SupplierProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Supplier, "Supplier", "")
    ''' <summary>
    ''' Gets the SAP Order No value
    ''' </summary>
    <Display(Name:="Supplier")>
    Public ReadOnly Property Supplier() As String
      Get
        Return GetProperty(SupplierProperty)
      End Get
    End Property

    Public Shared NotRequiredByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotRequiredByName, "Not Required By", "")
    ''' <summary>
    ''' Gets the SAP Order No value
    ''' </summary>
    <Display(Name:="Not Required By")>
    Public ReadOnly Property NotRequiredByName() As String
      Get
        Return GetProperty(NotRequiredByNameProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    ''' <summary>
    ''' Gets the SAP Order No value
    ''' </summary>
    <Display(Name:="Created By")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared LastModifiedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LastModifiedByName, "Last Modified By", "")
    ''' <summary>
    ''' Gets the SAP Order No value
    ''' </summary>
    <Display(Name:="Last Modified By")>
    Public ReadOnly Property LastModifiedByName() As String
      Get
        Return GetProperty(LastModifiedByNameProperty)
      End Get
    End Property

    <Display(Name:="Created")>
    Public ReadOnly Property CreationDetails As String
      Get
        If Not CreatedDateTime.IsEmpty Then
          Return CreatedByName & " " & CreatedDateTime.Date.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Last Modified")>
    Public ReadOnly Property LastModifiedDetails As String
      Get
        If Not ModifiedDateTime.IsEmpty Then
          Return LastModifiedByName & " " & ModifiedDateTime.Date.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionOutsourceServiceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Description

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionOutsourceService(dr As SafeDataReader) As ROProductionOutsourceService

      Dim r As New ROProductionOutsourceService()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionOutsourceServiceIDProperty, .GetInt32(0))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(OutsourceServiceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SupplierIDProperty, .GetInt32(3))
        LoadProperty(DescriptionProperty, .GetString(4))
        LoadProperty(QuoteRequestedDateProperty, .GetValue(5))
        LoadProperty(QuoteReceivedDateProperty, .GetValue(6))
        LoadProperty(QuotedAmountProperty, .GetDecimal(7))
        LoadProperty(VATInclusiveIndProperty, .GetBoolean(8))
        LoadProperty(NotRequiredIndProperty, .GetBoolean(9))
        LoadProperty(NotRequiredReasonProperty, .GetString(10))
        LoadProperty(NotRequiredByProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(SAPOrderNoProperty, .GetString(12))
        LoadProperty(CreatedByProperty, .GetInt32(13))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(14))
        LoadProperty(ModifiedByProperty, .GetInt32(15))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(16))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
        LoadProperty(RowNoProperty, .GetInt32(19))
        LoadProperty(OutsourceServiceTypeProperty, .GetString(20))
        LoadProperty(SupplierProperty, .GetString(21))
        LoadProperty(NotRequiredByNameProperty, .GetString(22))
        LoadProperty(CreatedByNameProperty, .GetString(23))
        LoadProperty(LastModifiedByNameProperty, .GetString(24))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace