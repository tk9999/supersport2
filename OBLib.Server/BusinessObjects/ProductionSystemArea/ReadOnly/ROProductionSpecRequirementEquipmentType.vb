﻿' Generated 14 Apr 2016 13:25 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ProductionSystemAreas.ReadOnly

  <Serializable()> _
  Public Class ROProductionSpecRequirementEquipmentType
    Inherits OBReadOnlyBase(Of ROProductionSpecRequirementEquipmentType)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROProductionSpecRequirementEquipmentTypeBO.ROProductionSpecRequirementEquipmentTypeBOToString(self)")

    Public Shared ProductionSpecRequirementEquipmentTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSpecRequirementEquipmentTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ProductionSpecRequirementEquipmentTypeID() As Integer
      Get
        Return GetProperty(ProductionSpecRequirementEquipmentTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSpecRequirementID, "Production Spec Requirement", Nothing)
    ''' <summary>
    ''' Gets the Production Spec Requirement value
    ''' </summary>
    <Display(Name:="Production Spec Requirement", Description:="The parent Production Spec for this Requirement Equipment Type")>
  Public ReadOnly Property ProductionSpecRequirementID() As Integer?
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
    End Property

    Public Shared EquipmentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentTypeID, "Type", Nothing)
    ''' <summary>
    ''' Gets the Equipment Type value
    ''' </summary>
    <Display(Name:="Type", Description:="The general type of equipment that is required for the production spec")>
    Public ReadOnly Property EquipmentTypeID() As Integer?
      Get
        Return GetProperty(EquipmentTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentSubTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentSubTypeID, "Sub Type", Nothing)
    ''' <summary>
    ''' Gets the Equipment Sub Type value
    ''' </summary>
    <Display(Name:="Sub Type", Description:="The specific type of equipment that is required for the production spec")>
    Public ReadOnly Property EquipmentSubTypeID() As Integer?
      Get
        Return GetProperty(EquipmentSubTypeIDProperty)
      End Get
    End Property

    Public Shared EquipmentQuantityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentQuantity, "Quantity", 0)
    ''' <summary>
    ''' Gets the Equipment Quantity value
    ''' </summary>
    <Display(Name:="Quantity", Description:="Number of a specific equipment type used for this production spec")>
    Public ReadOnly Property EquipmentQuantity() As Integer
      Get
        Return GetProperty(EquipmentQuantityProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The prodtion the the equipment type requirements relate to")>
  Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area", 0)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
  Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
  Public ReadOnly Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared EquipmentTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentType, "Type", "")
    ''' <summary>
    ''' Gets the Equipment Type value
    ''' </summary>
    <Display(Name:="Type", Description:="The general type of equipment that is required for the production spec")>
    Public ReadOnly Property EquipmentType() As String
      Get
        Return GetProperty(EquipmentTypeProperty)
      End Get
    End Property

    Public Shared EquipmentSubTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentSubType, "Sub Type", "")
    ''' <summary>
    ''' Gets the Equipment Type value
    ''' </summary>
    <Display(Name:="Sub Type", Description:="The general type of equipment that is required for the production spec")>
    Public ReadOnly Property EquipmentSubType() As String
      Get
        Return GetProperty(EquipmentSubTypeProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By", "")
    ''' <summary>
    ''' Gets the SAP Order No value
    ''' </summary>
    <Display(Name:="Created By")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared LastModifiedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LastModifiedByName, "Last Modified By", "")
    ''' <summary>
    ''' Gets the SAP Order No value
    ''' </summary>
    <Display(Name:="Last Modified By")>
    Public ReadOnly Property LastModifiedByName() As String
      Get
        Return GetProperty(LastModifiedByNameProperty)
      End Get
    End Property

    <Display(Name:="Created")>
    Public ReadOnly Property CreationDetails As String
      Get
        If Not CreatedDateTime.IsEmpty Then
          Return CreatedByName & " " & CreatedDateTime.Date.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Last Modified")>
    Public ReadOnly Property LastModifiedDetails As String
      Get
        If Not ModifiedDateTime.IsEmpty Then
          Return LastModifiedByName & " " & ModifiedDateTime.Date.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSpecRequirementEquipmentTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROProductionSpecRequirementEquipmentType(dr As SafeDataReader) As ROProductionSpecRequirementEquipmentType

      Dim r As New ROProductionSpecRequirementEquipmentType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionSpecRequirementEquipmentTypeIDProperty, .GetInt32(0))
        LoadProperty(ProductionSpecRequirementIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(EquipmentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(EquipmentSubTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(EquipmentQuantityProperty, .GetInt32(4))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(ProductionAreaIDProperty, .GetInt32(11))
        LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(EquipmentTypeProperty, .GetString(14))
        LoadProperty(EquipmentSubTypeProperty, .GetString(15))
        LoadProperty(CreatedByNameProperty, .GetString(16))
        LoadProperty(LastModifiedByNameProperty, .GetString(17))
      End With

    End Sub

#End Region

  End Class

End Namespace