﻿' Generated 26 Jun 2014 17:54 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base.ReadOnly

  <Serializable()> _
  Public MustInherit Class ROProductionTimelineBaseList(Of T As ROProductionTimelineBaseList(Of T, C), C As ROProductionTimelineBase(Of C))
    Inherits OBReadOnlyListBase(Of T, C)

#Region " Business Methods "

    Public Function GetItem(ProductionTimelineID As Integer) As ROProductionTimelineBase(Of C)

      For Each child As ROProductionTimelineBase(Of C) In Me
        If child.ProductionTimelineID = ProductionTimelineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Timelines"

    End Function

#End Region

#Region " Data Access "

    '<Serializable()> _
    'Public MustInherit Class Criteria
    '  Inherits CriteriaBase(Of Criteria)

    '  Public Property ProductionID As Integer? = Nothing
    '  Public Property SystemID As Integer? = Nothing
    '  Public Property ProductionAreaID As Integer? = Nothing

    '  Public Sub New(ProductionID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?)
    '    Me.ProductionID = ProductionID
    '    Me.SystemID = SystemID
    '    Me.ProductionAreaID = ProductionAreaID
    '  End Sub

    'End Class

#Region " Common "

    'Public Shared Function NewROProductionTimelineBaseList() As ROProductionTimelineBaseList(Of T, C)

    '  Return New ROProductionTimelineBaseList(Of T, C)()

    'End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    'Public Shared Function GetROProductionTimelineBaseList(ProductionID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?) As ROProductionTimelineBaseList(Of T, C)

    '  Return DataPortal.Fetch(Of ROProductionTimelineBaseList(Of T, C))(New Criteria(ProductionID, SystemID, ProductionAreaID))

    'End Function

    Public MustOverride Sub Fetch(sdr As SafeDataReader)

    Protected MustOverride Sub DataPortal_Fetch(criteria As Object)

    'Dim crit As Criteria = Criteria
    '  Using cn As New SqlConnection(Singular.Settings.ConnectionString)
    '    cn.Open()
    '    Try
    '      Using cm As SqlCommand = cn.CreateCommand
    '        cm.CommandType = CommandType.StoredProcedure
    '        cm.CommandText = "GetProcsWeb.getROProductionTimelineBaseList"
    '        cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
    '        cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
    '        cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
    '        Using sdr As New SafeDataReader(cm.ExecuteReader)
    '          Fetch(sdr)
    '        End Using
    '      End Using
    '    Finally
    '      cn.Close()
    '    End Try
    '  End Using

    'End Sub

#End If

#End Region

#End Region

  End Class

End Namespace