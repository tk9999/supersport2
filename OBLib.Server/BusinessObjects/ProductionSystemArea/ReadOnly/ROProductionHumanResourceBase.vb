﻿' Generated 15 Apr 2015 20:06 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Productions.Base.ReadOnly

  <Serializable()> _
  Public Class ROProductionHumanResourceBase
    Inherits SingularReadOnlyBase(Of ROProductionHumanResourceBase)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ProductionHumanResourceID() As Integer
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The parent Production for this Human resource")>
  Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="The discipline required")>
  Public ReadOnly Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position", Nothing)
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="The camera position that this human resource is allocated to")>
  Public ReadOnly Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionTypeID, "Position Type", Nothing)
    ''' <summary>
    ''' Gets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
  Public ReadOnly Property PositionTypeID() As Integer?
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
    End Property

    Public Shared FilteredProductionTypeIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FilteredProductionTypeInd, "Filtered Production Type", True)
    ''' <summary>
    ''' Gets the Filtered Production Type value
    ''' </summary>
    <Display(Name:="Filtered Production Type", Description:="True indicates that the HR we can pick for this discipline must be skilled in that discipline")>
  Public ReadOnly Property FilteredProductionTypeInd() As Boolean
      Get
        Return GetProperty(FilteredProductionTypeIndProperty)
      End Get
    End Property

    Public Shared FilteredPositionIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FilteredPositionInd, "Filtered Position", True)
    ''' <summary>
    ''' Gets the Filtered Position value
    ''' </summary>
    <Display(Name:="Filtered Position", Description:="True indicates that the HR we can pick for this position must be skilled in that position")>
  Public ReadOnly Property FilteredPositionInd() As Boolean
      Get
        Return GetProperty(FilteredPositionIndProperty)
      End Get
    End Property

    Public Shared IncludedOffIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IncludedOffInd, "Included Off", False)
    ''' <summary>
    ''' Gets the Included Off value
    ''' </summary>
    <Display(Name:="Included Off", Description:="Tick indicates that all human recsources will be included in the dropdown irrespective of whether they have an off weekend")>
  Public ReadOnly Property IncludedOffInd() As Boolean
      Get
        Return GetProperty(IncludedOffIndProperty)
      End Get
    End Property

    Public Shared PreferredHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PreferredHumanResourceID, "Preferred Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Preferred Human Resource value
    ''' </summary>
    <Display(Name:="Preferred Human Resource", Description:="This is the preferred resource that we want to allocate to this production/discipline/position. The HumanResourceID will default to this HR if they are available.")>
  Public ReadOnly Property PreferredHumanResourceID() As Integer?
      Get
        Return GetProperty(PreferredHumanResourceIDProperty)
      End Get
    End Property

    Public Shared SuppliedHumanResourceIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SuppliedHumanResourceInd, "Supplied Human Resource", False)
    ''' <summary>
    ''' Gets the Supplied Human Resource value
    ''' </summary>
    <Display(Name:="Supplied Human Resource", Description:="Tick indicates that this human resource is supplied")>
  Public ReadOnly Property SuppliedHumanResourceInd() As Boolean
      Get
        Return GetProperty(SuppliedHumanResourceIndProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource to be used")>
  Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SelectionReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectionReason, "Selection Reason", "")
    ''' <summary>
    ''' Gets the Selection Reason value
    ''' </summary>
    <Display(Name:="Selection Reason", Description:="This field needs to be filled in if the resource selected is not available through the default parameters")>
  Public ReadOnly Property SelectionReason() As String
      Get
        Return GetProperty(SelectionReasonProperty)
      End Get
    End Property

    Public Shared CancelledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDate, "Cancelled Date")
    ''' <summary>
    ''' Gets the Cancelled Date value
    ''' </summary>
    <Display(Name:="Cancelled Date", Description:="The date (and time) that the human resource was cancelled for a production. This is used to calculate the cancellation fee based on the day difference between the cancellation date and the transmission start date")>
  Public ReadOnly Property CancelledDate As DateTime?
      Get
        Return GetProperty(CancelledDateProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="The reason for the cancellation")>
  Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CrewLeaderIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CrewLeaderInd, "Crew Leader", False)
    ''' <summary>
    ''' Gets the Crew Leader value
    ''' </summary>
    <Display(Name:="Crew Leader", Description:="Tick indicates that this human resource is the crew leader for this discipline on this production")>
  Public ReadOnly Property CrewLeaderInd() As Boolean
      Get
        Return GetProperty(CrewLeaderIndProperty)
      End Get
    End Property

    Public Shared TrainingIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TrainingInd, "Training", False)
    ''' <summary>
    ''' Gets the Training value
    ''' </summary>
    <Display(Name:="Training", Description:="Tick indicates that this human resource is providing training for the discipline they have been loaded for")>
  Public ReadOnly Property TrainingInd() As Boolean
      Get
        Return GetProperty(TrainingIndProperty)
      End Get
    End Property

    Public Shared TBCIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TBCInd, "TBC", False)
    ''' <summary>
    ''' Gets the TBC value
    ''' </summary>
    <Display(Name:="TBC", Description:="")>
  Public ReadOnly Property TBCInd() As Boolean
      Get
        Return GetProperty(TBCIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="The vehicle that the human resource is driving. Can only have a value if the discipline is 'Driver'")>
  Public ReadOnly Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
  Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared ReliefCrewIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ReliefCrewInd, "Relief Crew", False)
    ''' <summary>
    ''' Gets the Relief Crew value
    ''' </summary>
    <Display(Name:="Relief Crew", Description:="Indicator for whether or not the Human Resource is part of the Relief Crew")>
  Public ReadOnly Property ReliefCrewInd() As Boolean
      Get
        Return GetProperty(ReliefCrewIndProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
  Public ReadOnly Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.HumanResource, "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared DisciplinePositionProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.DisciplinePosition, "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Role", Description:="")>
    Public ReadOnly Property DisciplinePosition() As String
      Get
        Return GetProperty(DisciplinePositionProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start", Description:=""),
    DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End", Description:=""),
    DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionHRID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="ProductionHR")>
    Public ReadOnly Property ProductionHRID() As Integer?
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SelectionReason

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionHumanResourceBase(dr As SafeDataReader) As ROProductionHumanResourceBase

      Dim r As New ROProductionHumanResourceBase()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(FilteredProductionTypeIndProperty, .GetBoolean(5))
        LoadProperty(FilteredPositionIndProperty, .GetBoolean(6))
        LoadProperty(IncludedOffIndProperty, .GetBoolean(7))
        LoadProperty(PreferredHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(SuppliedHumanResourceIndProperty, .GetBoolean(9))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(SelectionReasonProperty, .GetString(11))
        LoadProperty(CancelledDateProperty, .GetValue(12))
        LoadProperty(CancelledReasonProperty, .GetString(13))
        LoadProperty(CrewLeaderIndProperty, .GetBoolean(14))
        LoadProperty(TrainingIndProperty, .GetBoolean(15))
        LoadProperty(TBCIndProperty, .GetBoolean(16))
        LoadProperty(CreatedByProperty, .GetInt32(17))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(18))
        LoadProperty(ModifiedByProperty, .GetInt32(19))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(20))
        LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
        LoadProperty(CommentsProperty, .GetString(23))
        LoadProperty(ReliefCrewIndProperty, .GetBoolean(24))
        LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(25)))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(26)))
        LoadProperty(HumanResourceProperty, .GetString(27))
        'LoadProperty(PreferredHumanResourceProperty, .GetString(28))
        'LoadProperty(OrderNoProperty, .GetInt32(29))
        'LoadProperty(PriorityProperty, .GetInt32(30))
        LoadProperty(StartDateTimeProperty, .GetValue(31))
        LoadProperty(EndDateTimeProperty, .GetValue(32))
        'LoadProperty(DisciplineProperty, .GetString(33))
        'LoadProperty(PositionProperty, .GetString(34))
        LoadProperty(DisciplinePositionProperty, .GetString(35))
        'RowNo = 36
        LoadProperty(ProductionHRIDProperty, ZeroNothing(.GetInt32(37)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace