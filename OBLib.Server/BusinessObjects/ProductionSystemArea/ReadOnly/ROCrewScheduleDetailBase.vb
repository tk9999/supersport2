﻿' Generated 15 Apr 2015 19:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base.ReadOnly

  <Serializable()> _
  Public Class ROCrewScheduleDetailBase
    Inherits SingularReadOnlyBase(Of ROCrewScheduleDetailBase)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CrewScheduleDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewScheduleDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property CrewScheduleDetailID() As Integer
      Get
        Return GetProperty(CrewScheduleDetailIDProperty)
      End Get
    End Property

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionHumanResourceID, "Production Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Production Human Resource value
    ''' </summary>
    <Display(Name:="Production Human Resource", Description:="The human resource for the production that this detail relates to")>
  Public ReadOnly Property ProductionHumanResourceID() As Integer?
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ProductionTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTimelineID, "Production Timeline", Nothing)
    ''' <summary>
    ''' Gets the Production Timeline value
    ''' </summary>
    <Display(Name:="Production Timeline", Description:="The timeline item that this detail was created from. Can be NULL if the user needs to capture an unplanned resource.")>
  Public ReadOnly Property ProductionTimelineID() As Integer?
      Get
        Return GetProperty(ProductionTimelineIDProperty)
      End Get
    End Property

    Public Shared DetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Detail, "Detail", "")
    ''' <summary>
    ''' Gets the Detail value
    ''' </summary>
    <Display(Name:="Detail", Description:="Space to capture extra details")>
  Public ReadOnly Property Detail() As String
      Get
        Return GetProperty(DetailProperty)
      End Get
    End Property

    Public Shared StartTimeOffsetProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StartTimeOffset, "Start Time Offset", 0)
    ''' <summary>
    ''' Gets the Start Time Offset value
    ''' </summary>
    <Display(Name:="Start Time Offset", Description:="The number of hours and minutes to offset the calculated start time by")>
  Public ReadOnly Property StartTimeOffset() As Integer
      Get
        Return GetProperty(StartTimeOffsetProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="The start date and time for the scheduled task")>
  Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="The end date and time for the scheduled task")>
  Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared EndTimeOffsetProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EndTimeOffset, "End Time Offset", 0)
    ''' <summary>
    ''' Gets the End Time Offset value
    ''' </summary>
    <Display(Name:="End Time Offset", Description:="The number of hours and minutes to offset the calculated end time by")>
  Public ReadOnly Property EndTimeOffset() As Integer
      Get
        Return GetProperty(EndTimeOffsetProperty)
      End Get
    End Property

    Public Shared ProductionTimelineMatchIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ProductionTimelineMatchInd, "Production Timeline Match", True)
    ''' <summary>
    ''' Gets the Production Timeline Match value
    ''' </summary>
    <Display(Name:="Production Timeline Match", Description:="Tick indicates that this human resource's schedule must always match the production's schedule. Should this be unticked the system will not automatically update the HR schedule to match the producion's schedule")>
  Public ReadOnly Property ProductionTimelineMatchInd() As Boolean
      Get
        Return GetProperty(ProductionTimelineMatchIndProperty)
      End Get
    End Property

    Public Shared ExcludeIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ExcludeInd, "Exclude", False)
    ''' <summary>
    ''' Gets the Exclude value
    ''' </summary>
    <Display(Name:="Exclude", Description:="Tick indicates that this HR's time should not be allocated to them")>
  Public ReadOnly Property ExcludeInd() As Boolean
      Get
        Return GetProperty(ExcludeIndProperty)
      End Get
    End Property

    Public Shared ExcludeReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ExcludeReason, "Exclude Reason", "")
    ''' <summary>
    ''' Gets the Exclude Reason value
    ''' </summary>
    <Display(Name:="Exclude Reason", Description:="The reason this HR's time was marked as excluded")>
  Public ReadOnly Property ExcludeReason() As String
      Get
        Return GetProperty(ExcludeReasonProperty)
      End Get
    End Property

    Public Shared CrewTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTimesheetID, "Crew Timesheet", Nothing)
    ''' <summary>
    ''' Gets the Crew Timesheet value
    ''' </summary>
    <Display(Name:="Crew Timesheet", Description:="The timesheet record that will be created for this schedule detail one the production has finished")>
  Public ReadOnly Property CrewTimesheetID() As Integer?
      Get
        Return GetProperty(CrewTimesheetIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
  Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared TimesheetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TimesheetDate, "Timesheet Date")
    ''' <summary>
    ''' Gets the Timesheet Date value
    ''' </summary>
    <Display(Name:="Timesheet Date", Description:="")>
  Public ReadOnly Property TimesheetDate As DateTime?
      Get
        Return GetProperty(TimesheetDateProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
  Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceOffPeriodID, "Human Resource Off Period", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Off Period value
    ''' </summary>
    <Display(Name:="Human Resource Off Period", Description:="the off period (leave) that the scheduled item is linked to")>
  Public ReadOnly Property HumanResourceOffPeriodID() As Integer?
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
    End Property

    Public Shared HumanResourceScheduleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceScheduleTypeID, "Human Resource Schedule Type", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Schedule Type value
    ''' </summary>
    <Display(Name:="Human Resource Schedule Type", Description:="the type of booking")>
  Public ReadOnly Property HumanResourceScheduleTypeID() As Integer?
      Get
        Return GetProperty(HumanResourceScheduleTypeIDProperty)
      End Get
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Human Resource Shift", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:="")>
  Public ReadOnly Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
  Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
  Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AdHocBookingID, "Ad Hoc Booking", 0)
    ''' <summary>
    ''' Gets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking", Description:="")>
  Public ReadOnly Property AdHocBookingID() As Integer
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
    End Property

    Public Shared HumanResourceSecondmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceSecondmentID, "Human Resource Secondment", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Secondment value
    ''' </summary>
    <Display(Name:="Human Resource Secondment", Description:="")>
  Public ReadOnly Property HumanResourceSecondmentID() As Integer?
      Get
        Return GetProperty(HumanResourceSecondmentIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewScheduleDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Detail

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCrewScheduleDetailBase(dr As SafeDataReader) As ROCrewScheduleDetailBase

      Dim r As New ROCrewScheduleDetailBase()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CrewScheduleDetailIDProperty, .GetInt32(0))
        LoadProperty(ProductionHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(DetailProperty, .GetString(3))
        LoadProperty(StartTimeOffsetProperty, .GetInt32(4))
        LoadProperty(StartDateTimeProperty, .GetValue(5))
        LoadProperty(EndDateTimeProperty, .GetValue(6))
        LoadProperty(EndTimeOffsetProperty, .GetInt32(7))
        LoadProperty(ProductionTimelineMatchIndProperty, .GetBoolean(8))
        LoadProperty(ExcludeIndProperty, .GetBoolean(9))
        LoadProperty(ExcludeReasonProperty, .GetString(10))
        LoadProperty(CrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(CreatedByProperty, .GetInt32(12))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(13))
        LoadProperty(ModifiedByProperty, .GetInt32(14))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(15))
        LoadProperty(TimesheetDateProperty, .GetValue(16))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
        LoadProperty(HumanResourceOffPeriodIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
        LoadProperty(HumanResourceScheduleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
        LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(20)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
        LoadProperty(AdHocBookingIDProperty, .GetInt32(23))
        LoadProperty(HumanResourceSecondmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace