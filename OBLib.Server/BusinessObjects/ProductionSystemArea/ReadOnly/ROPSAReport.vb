﻿' Generated 02 Sep 2016 18:40 - Singular Systems Object Generator Version 2.2.677
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ProductionSystemAreas.ReadOnly

  <Serializable()> _
  Public Class ROPSAReport
    Inherits OBReadOnlyBase(Of ROPSAReport)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionSystemAreaName, "Event")
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Event", Description:="")>
    Public ReadOnly Property ProductionSystemAreaName() As String
      Get
        Return GetProperty(ProductionSystemAreaNameProperty)
      End Get
    End Property

    Public Shared MinSDProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.MinSD, "Start Date")
    ''' <summary>
    ''' Gets the Sent Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public ReadOnly Property MinSD As DateTime?
      Get
        Return GetProperty(MinSDProperty)
      End Get
    End Property

    Public Shared MaxEDProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.MaxED, "End Date")
    ''' <summary>
    ''' Gets the Sent Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public ReadOnly Property MaxED As DateTime?
      Get
        Return GetProperty(MaxEDProperty)
      End Get
    End Property

    Public Shared SubDeptProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SubDept, "Sub Dept")
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Sub Dept", Description:="")>
    Public ReadOnly Property SubDept() As String
      Get
        Return GetProperty(SubDeptProperty)
      End Get
    End Property

    Public Shared AreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Area, "Area")
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property Area() As String
      Get
        Return GetProperty(AreaProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSystemAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionSystemAreaName

    End Function

#End Region


    '#Region " Child List "

    '    Public Shared ROPSADayListProperty As PropertyInfo(Of ROPSADayList) = RegisterProperty(Of ROPSADayList)(Function(c) c.ROPSADayList, "Sms Batch PSAL")
    '    Public ReadOnly Property ROPSADayList() As ROPSADayList
    '      Get
    '        If GetProperty(ROPSADayListProperty) Is Nothing Then
    '          LoadProperty(ROPSADayListProperty, Notifications.Sms.ReadOnly.ROPSADayList.NewROPSADayList())
    '        End If
    '        Return GetProperty(ROPSADayListProperty)
    '      End Get
    '    End Property

    '#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROPSAReport(dr As SafeDataReader) As ROPSAReport

      Dim r As New ROPSAReport()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionSystemAreaIDProperty, .GetInt32(0))
        LoadProperty(ProductionSystemAreaNameProperty, .GetString(1))
        LoadProperty(MinSDProperty, .GetDateTime(2))
        LoadProperty(MaxEDProperty, .GetDateTime(3))
        LoadProperty(SubDeptProperty, .GetString(4))
        LoadProperty(AreaProperty, .GetString(5))
        LoadProperty(RoomProperty, .GetString(6))
      End With

    End Sub

#End Region

  End Class

End Namespace