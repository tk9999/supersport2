﻿' Generated 15 Apr 2015 20:06 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base.ReadOnly

  <Serializable()> _
  Public Class ROProductionHumanResourceBaseList
    Inherits SingularReadOnlyListBase(Of ROProductionHumanResourceBaseList, ROProductionHumanResourceBase)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ProductionHumanResourceID As Integer) As ROProductionHumanResourceBase

      For Each child As ROProductionHumanResourceBase In Me
        If child.ProductionHumanResourceID = ProductionHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Human Resources"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      'Public UseSearch As Boolean = False
      'Public ProductionID As Object = Nothing
      'Public DisciplineID As Object = Nothing
      'Public PositionID As Object = Nothing
      'Public PositionTypeID As Object = Nothing
      'Public HumanResourceID As Object = Nothing
      'Public VehicleID As Object = Nothing
      'Public SystemID As Object = Nothing
      Public Property ProductionSystemAreaID As Integer? = Nothing
      'Public RoomID As Object = Nothing

      Public Sub New(ProductionSystemAreaID As Integer?)

        'ProductionID As Object, DisciplineID As Object, PositionID As Object, _
        'PositionTypeID As Object, HumanResourceID As Object, VehicleID As Object, SystemID As Object, _
        ', RoomID As Object
        'UseSearch = True
        'Me.ProductionID = ProductionID
        'Me.DisciplineID = DisciplineID
        'Me.PositionID = PositionID
        'Me.PositionTypeID = PositionTypeID
        'Me.HumanResourceID = HumanResourceID
        'Me.VehicleID = VehicleID
        'Me.SystemID = SystemID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
        'Me.RoomID = RoomID

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionHumanResourceBaseList() As ROProductionHumanResourceBaseList

      Return New ROProductionHumanResourceBaseList()

    End Function

    Public Shared Sub BeginGetROProductionHumanResourceBaseList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionHumanResourceBaseList)))

      Dim dp As New DataPortal(Of ROProductionHumanResourceBaseList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionHumanResourceBaseList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionHumanResourceBaseList)))

      Dim dp As New DataPortal(Of ROProductionHumanResourceBaseList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionHumanResourceBaseList() As ROProductionHumanResourceBaseList

      Return DataPortal.Fetch(Of ROProductionHumanResourceBaseList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionHumanResourceBase.GetROProductionHumanResourceBase(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionHumanResourceBaseList"
            'If crit.UseSearch Then
            'cm.Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(crit.ProductionID))
            'cm.Parameters.AddWithValue("@DisciplineID", Singular.Misc.NothingDBNull(crit.DisciplineID))
            'cm.Parameters.AddWithValue("@PositionID", Singular.Misc.NothingDBNull(crit.PositionID))
            'cm.Parameters.AddWithValue("@PositionTypeID", Singular.Misc.NothingDBNull(crit.PositionTypeID))
            'cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
            'cm.Parameters.AddWithValue("@VehicleID", Singular.Misc.NothingDBNull(crit.VehicleID))
            'cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(crit.ProductionSystemAreaID))
            'cm.Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(crit.RoomID))
            crit.AddParameters(cm)
            'End If
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace