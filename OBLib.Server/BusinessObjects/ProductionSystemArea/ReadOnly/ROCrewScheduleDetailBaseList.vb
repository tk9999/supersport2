﻿' Generated 15 Apr 2015 19:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base.ReadOnly

  <Serializable()> _
  Public Class ROCrewScheduleDetailBaseList
    Inherits SingularReadOnlyListBase(Of ROCrewScheduleDetailBaseList, ROCrewScheduleDetailBase)

#Region " Business Methods "

    Public Function GetItem(CrewScheduleDetailID As Integer) As ROCrewScheduleDetailBase

      For Each child As ROCrewScheduleDetailBase In Me
        If child.CrewScheduleDetailID = CrewScheduleDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Crew Schedule Details"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public UseSearch As Boolean = False
      Public ProductionHumanResourceID As Object = Nothing
      Public ProductionTimelineID As Object = Nothing
      Public TimesheetDate As Object = Nothing
      Public HumanResourceID As Object = Nothing
      Public HumanResourceOffPeriodID As Object = Nothing
      Public HumanResourceScheduleTypeID As Object = Nothing
      Public HumanResourceShiftID As Object = Nothing
      Public ProductionAreaID As Object = Nothing
      Public SystemID As Object = Nothing
      Public AdHocBookingID As Object = Nothing
      Public HumanResourceSecondmentID As Object = Nothing

      Public Sub New(ProductionHumanResourceID As Object, ProductionTimelineID As Object, TimesheetDate As Object, _
          HumanResourceID As Object, HumanResourceOffPeriodID As Object, HumanResourceScheduleTypeID As Object, HumanResourceShiftID As Object, _
          ProductionAreaID As Object, SystemID As Object, AdHocBookingID As Object, HumanResourceSecondmentID As Object)

        UseSearch = True
        Me.ProductionHumanResourceID = ProductionHumanResourceID
        Me.ProductionTimelineID = ProductionTimelineID
        Me.TimesheetDate = TimesheetDate
        Me.HumanResourceID = HumanResourceID
        Me.HumanResourceOffPeriodID = HumanResourceOffPeriodID
        Me.HumanResourceScheduleTypeID = HumanResourceScheduleTypeID
        Me.HumanResourceShiftID = HumanResourceShiftID
        Me.ProductionAreaID = ProductionAreaID
        Me.SystemID = SystemID
        Me.AdHocBookingID = AdHocBookingID
        Me.HumanResourceSecondmentID = HumanResourceSecondmentID

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROCrewScheduleDetailBaseList() As ROCrewScheduleDetailBaseList

      Return New ROCrewScheduleDetailBaseList()

    End Function

    Public Shared Sub BeginGetROCrewScheduleDetailBaseList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROCrewScheduleDetailBaseList)))

      Dim dp As New DataPortal(Of ROCrewScheduleDetailBaseList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROCrewScheduleDetailBaseList(CallBack As EventHandler(Of DataPortalResult(Of ROCrewScheduleDetailBaseList)))

      Dim dp As New DataPortal(Of ROCrewScheduleDetailBaseList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROCrewScheduleDetailBaseList() As ROCrewScheduleDetailBaseList

      Return DataPortal.Fetch(Of ROCrewScheduleDetailBaseList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROCrewScheduleDetailBase.GetROCrewScheduleDetailBase(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROCrewScheduleDetailBaseList"

            If crit.UseSearch Then
              cm.Parameters.AddWithValue("@ProductionHumanResourceID", Singular.Misc.NothingDBNull(crit.ProductionHumanResourceID))
              cm.Parameters.AddWithValue("@ProductionTimelineID", Singular.Misc.NothingDBNull(crit.ProductionTimelineID))
              cm.Parameters.AddWithValue("@TimesheetDate", Singular.Misc.NothingDBNull(crit.TimesheetDate))
              cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
              cm.Parameters.AddWithValue("@HumanResourceOffPeriodID", Singular.Misc.NothingDBNull(crit.HumanResourceOffPeriodID))
              cm.Parameters.AddWithValue("@HumanResourceScheduleTypeID", Singular.Misc.NothingDBNull(crit.HumanResourceScheduleTypeID))
              cm.Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.NothingDBNull(crit.HumanResourceShiftID))
              cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
              cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
              cm.Parameters.AddWithValue("@AdHocBookingID", Singular.Misc.NothingDBNull(crit.AdHocBookingID))
              cm.Parameters.AddWithValue("@HumanResourceSecondmentID", Singular.Misc.NothingDBNull(crit.HumanResourceSecondmentID))
            End If

            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace