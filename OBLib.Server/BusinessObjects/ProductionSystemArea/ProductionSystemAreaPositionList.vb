﻿' Generated 12 Jun 2016 11:59 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace ProductionSystemAreas

  <Serializable()> _
  Public Class ProductionSystemAreaPositionList
    Inherits OBBusinessListBase(Of ProductionSystemAreaPositionList, ProductionSystemAreaPosition)

#Region " Business Methods "

    Public Function GetItem(ProductionSpecRequirementPositionID As Integer) As ProductionSystemAreaPosition

      For Each child As ProductionSystemAreaPosition In Me
        If child.ProductionSpecRequirementPositionID = ProductionSpecRequirementPositionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionSystemAreaID As Integer?

      Public Sub New(ProductionSystemAreaID As Integer?)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewProductionSystemAreaPositionList() As ProductionSystemAreaPositionList

      Return New ProductionSystemAreaPositionList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetProductionSystemAreaPositionList() As ProductionSystemAreaPositionList

      Return DataPortal.Fetch(Of ProductionSystemAreaPositionList)(New Criteria())

    End Function

    Public Shared Function GetProductionSystemAreaPositionList(ProductionSystemAreaID As Integer?) As ProductionSystemAreaPositionList

      Return DataPortal.Fetch(Of ProductionSystemAreaPositionList)(New Criteria(ProductionSystemAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionSystemAreaPosition.GetProductionSystemAreaPosition(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionSystemAreaPositionList"
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace