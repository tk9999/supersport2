﻿' Generated 05 Jan 2015 09:38 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Vehicles.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base

  <Serializable()> _
  Public MustInherit Class ProductionTimelineBase(Of T As ProductionTimelineBase(Of T))
    Inherits OBBusinessBase(Of T)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionTimelineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTimelineID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionTimelineID() As Integer
      Get
        Return GetProperty(ProductionTimelineIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The parent Production for this Timeline item")>
    Public Overridable Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTimelineTypeID, "Production Timeline Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Timeline Type", Description:="The type of timeline item"),
    Required(ErrorMessage:="Timeline Type required")>
    Public Overridable Property ProductionTimelineTypeID() As Integer?
      Get
        Return GetProperty(ProductionTimelineTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTimelineTypeIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:="The start date and time of this timeline item"),
    Required(ErrorMessage:="Start Time required")>
    Public Overridable Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:="The end date and time of this timeline item"),
    Required(ErrorMessage:="End Time required")>
    Public Overridable Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="The vehicle that the pre/post travel applies to. If left blank then the pre/post travel applies to all the vehicles on the production. Must be left blank from non vehicle pre/post travel types"),
    DropDownWeb(GetType(ROVehicleList))>
    Public Overridable Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="Any comment that the event planner wishes to add. Will be pulled through onto the production schedule"),
    StringLength(500, ErrorMessage:="Comments cannot be more than 500 characters")>
    Public Overridable Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ParentProductionTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentProductionTimelineID, "Parent Production Timeline", Nothing)
    ''' <summary>
    ''' Gets and sets the Parent Production Timeline value
    ''' </summary>
    <Display(Name:="Parent Production Timeline", Description:="")>
    Public Overridable Property ParentProductionTimelineID() As Integer?
      Get
        Return GetProperty(ParentProductionTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ParentProductionTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared CrewTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTypeID, "Crew Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Crew Type value
    ''' </summary>
    <Display(Name:="Crew Type", Description:=""),
    DropDownWeb(GetType(ROCrewTypeList))>
    Public Overridable Property CrewTypeID() As Integer?
      Get
        Return GetProperty(CrewTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
    Public Overridable Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    'Public Shared OvernightIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.OvernightInd, "Overnight", CType(Nothing, Boolean?))
    ' ''' <summary>
    ' ''' Gets and sets the Overnight value
    ' ''' </summary>
    '<Display(Name:="Overnight", Description:="Is the timline overlapping in to the next day")>
    'Public Overridable Property OvernightInd() As Boolean?
    '  Get
    '    Return GetProperty(OvernightIndProperty)
    '  End Get
    '  Set(ByVal Value As Boolean?)
    '    SetProperty(OvernightIndProperty, Value)
    '  End Set
    'End Property

    'Public Shared OvernightSetByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OvernightSetBy, "Overnight Set By", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Overnight Set By value
    ' ''' </summary>
    '<Display(Name:="Overnight Set By", Description:="the user who set it to overnight")>
    'Public Overridable Property OvernightSetBy() As Integer?
    '  Get
    '    Return GetProperty(OvernightSetByProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(OvernightSetByProperty, Value)
    '  End Set
    'End Property

    'Public Shared OvernightSetDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.OvernightSetDate, Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Overnight Set Date value
    ' ''' </summary>
    '<Display(Name:="Overnight Set Date", Description:="the date overnight was set")>
    'Public Overridable Property OvernightSetDate As DateTime?
    '  Get
    '    Return GetProperty(OvernightSetDateProperty)
    '  End Get
    '  Set(ByVal Value As DateTime?)
    '    SetProperty(OvernightSetDateProperty, Value)
    '  End Set
    'End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="area the timeline applies to"),
    Required(ErrorMessage:="Production Area required")>
    Public Overridable Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="sub-dept which booked the timeline"),
    Required(ErrorMessage:="System required")>
    Public Overridable Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared TimelineDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.TimelineDate)
    ''' <summary>
    ''' Gets and sets the Timeline Date value
    ''' </summary>
    <Display(Name:="Date", Description:="the start date of the timeline"),
     Required(ErrorMessage:="Start Date is required"),
     DateField(FormatString:="dd MMM yyyy")>
    Public Overridable Property TimelineDate As DateTime?
      Get
        Return GetProperty(TimelineDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TimelineDateProperty, Value)
      End Set
    End Property

    Public ReadOnly Property ProductionTimelineType As String
      Get
        If ProductionTimelineTypeID IsNot Nothing Then
          Dim ptt As ROProductionTimelineType = OBLib.CommonData.Lists.ROProductionTimelineTypeList.GetItem(ProductionTimelineTypeID)
          If ptt IsNot Nothing Then
            Return ptt.ProductionTimelineType
          End If
        End If
        Return ""
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionTimelineIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Comments.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Timeline Base")
        Else
          Return String.Format("Blank {0}", "Production Timeline Base")
        End If
      Else
        Return Me.Comments
      End If

    End Function

    'Public Function GetParentList() As Object
    '  Return CType(Me.Parent, ProductionTimelineBaseList)
    'End Function

    'Public Function GetParent() As Object
    '  If GetParentList() IsNot Nothing Then
    '    Return CType(GetParentList().Parent, OBLib.Productions.Base.ProductionSystemArea)
    '  End If
    '  Return Nothing
    'End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()
      'Me.AddMultiplePropertyRule(AddressOf Singular.CSLALib.Rules.CompareProperties, New Singular.CSLALib.Rules.Args.ComparePropertyArgs("StartDateTime", "EndDateTime", "<="), New String() {"StartDateTime", "EndDateTime"})

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionTimelineBase() method.

    End Sub

    'Public Shared Function NewProductionTimelineBase() As ProductionTimelineBase

    '  Return DataPortal.CreateChild(Of ProductionTimelineBase)()

    'End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    'Friend Shared Function GetProductionTimeline(dr As SafeDataReader) As T

    '  Dim p As New ProductionTimelineBaseList(Of 
    '  p.Fetch(dr)
    '  Return p

    'End Function

    Public Sub PublicFetch(sdr As SafeDataReader)
      Fetch(sdr)
    End Sub

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionTimelineIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionTimelineTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(StartDateTimeProperty, .GetValue(3))
          LoadProperty(EndDateTimeProperty, .GetValue(4))
          LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(CommentsProperty, .GetString(6))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ParentProductionTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(CrewTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          'LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          'Dim tmpOvernightInd = .GetValue(15)
          'If IsDBNull(tmpOvernightInd) Then
          '  LoadProperty(OvernightIndProperty, Nothing)
          'Else
          '  LoadProperty(OvernightIndProperty, tmpOvernightInd)
          'End If
          'LoadProperty(OvernightSetByProperty, .GetInt32(16))
          'LoadProperty(OvernightSetDateProperty, .GetValue(17))
          LoadProperty(ProductionAreaIDProperty, .GetInt32(14))
          LoadProperty(SystemIDProperty, .GetInt32(15))
          LoadProperty(TimelineDateProperty, .GetValue(16))
        End With
      End Using
      FetchExtraProperties(sdr)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = InsProcName
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = UpdProcName
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'Me.SystemID = Me.GetParent.SystemID
      'Me.ProductionAreaID = Me.GetParent.ProductionAreaID
      'Me.RoomID = Me.GetParent.RoomID
      'Me.ProductionSystemAreaID = Me.GetParent.ProductionSystemAreaID

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionTimelineID As SqlParameter = .Parameters.Add("@ProductionTimelineID", SqlDbType.Int)
          paramProductionTimelineID.Value = GetProperty(ProductionTimelineIDProperty)
          If Me.IsNew Then
            paramProductionTimelineID.Direction = ParameterDirection.Output
          End If
          If Me.GetParent Is Nothing Then
            .Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
          Else
            .Parameters.AddWithValue("@ProductionID", NothingDBNull(Me.GetParent.ProductionID))
          End If
          .Parameters.AddWithValue("@ProductionTimelineTypeID", GetProperty(ProductionTimelineTypeIDProperty))
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@VehicleID", Singular.Misc.NothingDBNull(GetProperty(VehicleIDProperty)))
          .Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@ParentProductionTimelineID", Singular.Misc.NothingDBNull(GetProperty(ParentProductionTimelineIDProperty)))
          .Parameters.AddWithValue("@CrewTypeID", Singular.Misc.NothingDBNull(GetProperty(CrewTypeIDProperty)))
          If Me.GetParent Is Nothing Then
            .Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))
          Else
            .Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(Me.GetParent.ProductionSystemAreaID))
          End If
          '.Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(GetProperty(RoomIDProperty)))
          '.Parameters.AddWithValue("@OvernightInd", Singular.Misc.NothingDBNull(GetProperty(OvernightIndProperty)))
          '.Parameters.AddWithValue("@OvernightSetBy", NothingDBNull(GetProperty(OvernightSetByProperty)))
          '.Parameters.AddWithValue("@OvernightSetDate", (New SmartDate(GetProperty(OvernightSetDateProperty))).DBValue)
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@SystemID", NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@TimelineDate", (New SmartDate(GetProperty(TimelineDateProperty))).DBValue)
          .Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUserID)
          AddExtraParameters(.Parameters)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionTimelineIDProperty, paramProductionTimelineID.Value)
          End If
          ' update child objects
          UpdateChildLists()
          MarkOld()
        End With
      Else
        UpdateChildLists()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub
      DeleteChildren()

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = DelProcName
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionTimelineID", GetProperty(ProductionTimelineIDProperty))
        'cm.Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))
        cm.Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUserID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

#Region " Overridables "

    <Browsable(False)>
    Public Overridable ReadOnly Property InsProcName As String
      Get
        Return "[InsProcsWeb].[insProductionTimelineBase]"
      End Get
    End Property

    <Browsable(False)>
    Public Overridable ReadOnly Property UpdProcName As String
      Get
        Return "[UpdProcsWeb].[updProductionTimelineBase]"
      End Get
    End Property

    <Browsable(False)>
    Public Overridable ReadOnly Property DelProcName As String
      Get
        Return "[DelProcsWeb].[delProductionTimelineBase]"
      End Get
    End Property

    Public MustOverride Function GetParentList() As Object
    Public MustOverride Function GetParent() As Object
    Public MustOverride Function CreateNewInstance() As T
    Public MustOverride Sub DeleteChildren()

    Public MustOverride Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)
    Public MustOverride Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)
    Public MustOverride Sub UpdateChildLists()

#End Region

  End Class

End Namespace