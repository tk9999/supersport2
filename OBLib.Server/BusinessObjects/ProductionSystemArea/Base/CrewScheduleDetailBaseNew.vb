﻿' Generated 15 Apr 2015 19:40 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base

  <Serializable()> _
  Public MustInherit Class CrewScheduleDetailBaseNew(Of T As CrewScheduleDetailBaseNew(Of T))
    Inherits OBBusinessBase(Of T)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CrewScheduleDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CrewScheduleDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key,
    Display(Name:="ID")>
    Public ReadOnly Property CrewScheduleDetailID() As Integer
      Get
        Return GetProperty(CrewScheduleDetailIDProperty)
      End Get
    End Property

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionHumanResourceID, "Production Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Human Resource value
    ''' </summary>
    <Display(Name:="Production Human Resource", Description:="The human resource for the production that this detail relates to")>
    Public Property ProductionHumanResourceID() As Integer?
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTimelineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTimelineID, "Production Timeline", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Timeline value
    ''' </summary>
    <Display(Name:="Production Timeline", Description:="The timeline item that this detail was created from. Can be NULL if the user needs to capture an unplanned resource.")>
    Public Property ProductionTimelineID() As Integer?
      Get
        Return GetProperty(ProductionTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared DetailProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Detail, "Detail", "")
    ''' <summary>
    ''' Gets and sets the Detail value
    ''' </summary>
    <Display(Name:="Detail", Description:="Space to capture extra details"),
    StringLength(200, ErrorMessage:="Detail cannot be more than 200 characters")>
    Public Property Detail() As String
      Get
        Return GetProperty(DetailProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DetailProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="The start date and time for the scheduled task"),
    Required(ErrorMessage:="Start Date Time required")>
    Public Overridable Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="The end date and time for the scheduled task"),
    Required(ErrorMessage:="End Date Time required")>
    Public Overridable Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ExcludeIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ExcludeInd, "Exclude", False)
    ''' <summary>
    ''' Gets and sets the Exclude value
    ''' </summary>
    <Display(Name:="Exclude", Description:="Tick indicates that this HR's time should not be allocated to them"),
    Required(ErrorMessage:="Exclude required")>
    Public Property ExcludeInd() As Boolean
      Get
        Return GetProperty(ExcludeIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ExcludeIndProperty, Value)
      End Set
    End Property

    Public Shared ExcludeReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ExcludeReason, "Exclude Reason", "")
    ''' <summary>
    ''' Gets and sets the Exclude Reason value
    ''' </summary>
    <Display(Name:="Exclude Reason", Description:="The reason this HR's time was marked as excluded"),
    StringLength(200, ErrorMessage:="Exclude Reason cannot be more than 200 characters")>
    Public Property ExcludeReason() As String
      Get
        Return GetProperty(ExcludeReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ExcludeReasonProperty, Value)
      End Set
    End Property

    Public Shared CrewTimesheetIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CrewTimesheetID, "Crew Timesheet", Nothing)
    ''' <summary>
    ''' Gets and sets the Crew Timesheet value
    ''' </summary>
    <Display(Name:="Crew Timesheet", Description:="The timesheet record that will be created for this schedule detail one the production has finished")>
    Public Property CrewTimesheetID() As Integer?
      Get
        Return GetProperty(CrewTimesheetIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CrewTimesheetIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared TimesheetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.TimesheetDate, "Timesheet Date")
    ''' <summary>
    ''' Gets and sets the Timesheet Date value
    ''' </summary>
    <Display(Name:="Timesheet Date", Description:=""),
    Required(ErrorMessage:="Timesheet Date required")>
    Public Overridable Property TimesheetDate As DateTime?
      Get
        Return GetProperty(TimesheetDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(TimesheetDateProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceOffPeriodID, "Human Resource Off Period", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Off Period value
    ''' </summary>
    <Display(Name:="Human Resource Off Period", Description:="the off period (leave) that the scheduled item is linked to")>
    Public Property HumanResourceOffPeriodID() As Integer?
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceOffPeriodIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceScheduleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceScheduleTypeID, "Sch. Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Schedule Type value
    ''' </summary>
    <Display(Name:="Sch. Type", Description:="the type of booking"),
    Required(ErrorMessage:="Sch. Type required")>
    Public Property HumanResourceScheduleTypeID() As Integer?
      Get
        Return GetProperty(HumanResourceScheduleTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceScheduleTypeIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Human Resource Shift", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:="")>
    Public Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:=""),
    Required>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:=""),
    Required>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingID, "Ad Hoc Booking", Nothing)
    ''' <summary>
    ''' Gets and sets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking", Description:="")>
    Public Property AdHocBookingID() As Integer?
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AdHocBookingIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceSecondmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceSecondmentID, "Human Resource Secondment", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Secondment value
    ''' </summary>
    <Display(Name:="Human Resource Secondment", Description:="")>
    Public Property HumanResourceSecondmentID() As Integer?
      Get
        Return GetProperty(HumanResourceSecondmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceSecondmentIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionHRID, Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ProductionHRID() As Integer?
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionHRIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ResourceBookingID, Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ResourceBookingID() As Integer?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared HasClashesIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasClashesInd, "HasClashesInd", False)
    ''' <summary>
    ''' Gets and sets the Exclude value
    ''' </summary>
    <Display(Name:="Has Clashes"), AlwaysClean>
    Public Property HasClashesInd() As Boolean
      Get
        Return GetProperty(HasClashesIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasClashesIndProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Private Property ProductionTimelineTypeID As Object

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CrewScheduleDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Detail.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Crew Schedule Detail Base")
        Else
          Return String.Format("Blank {0}", "Crew Schedule Detail Base")
        End If
      Else
        Return Me.Detail
      End If

    End Function

    'Public Function GetParentList() As CrewScheduleDetailBaseNewList
    '  Return CType(Me.Parent, CrewScheduleDetailBaseNewList)
    'End Function

    'Public Function GetParent() As Resources.ProductionHRResourceBooking
    '  If GetParentList() IsNot Nothing Then
    '    Return CType(GetParentList.Parent, Resources.ProductionHRResourceBooking)
    '  End If
    '  Return Nothing
    'End Function

    'Public Function GetProductionHumanResource() As ProductionHumanResourceBase

    '  Dim productionHRResourceBooking As Resources.ProductionHRResourceBooking = Me.GetParent
    '  If productionHRResourceBooking IsNot Nothing Then
    '    Dim productionHR As ProductionHRBase = productionHRResourceBooking.GetParent
    '    If productionHR IsNot Nothing Then
    '      If productionHR.ProductionHumanResourceList.Count = 1 Then
    '        Return productionHR.ProductionHumanResourceList(0)
    '      ElseIf productionHR.ProductionHumanResourceList.Count > 1 Then
    '        Return productionHR.ProductionHumanResourceList(0)
    '      Else
    '        Return Nothing
    '      End If
    '    End If
    '  End If

    '  Return Nothing

    'End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      'With AddWebRule(TimesheetDateProperty)
      '  .JavascriptRuleFunctionName = JSBusinessObjectName & ".TimesheetDateValid"
      '  .ServerRuleFunction = AddressOf TimesheetDateValid
      '  .AddTriggerProperty(StartDateTimeProperty)
      '  .AffectedProperties.Add(StartDateTimeProperty)
      'End With

      'With AddWebRule(StartDateTimeProperty)
      '  .JavascriptRuleFunctionName = JSBusinessObjectName & ".StartDateTimeValid"
      '  .ServerRuleFunction = AddressOf StartDateTimeValid
      '  .AddTriggerProperty(EndDateTimeProperty)
      '  .AffectedProperties.Add(EndDateTimeProperty)
      'End With

    End Sub

    Public Shared Function TimesheetDateValid(csd As T) As String
      Dim Errors As String = ""
      If csd.TimesheetDate Is Nothing Then
        Errors &= "Schedule Date is required"
      End If
      If csd.StartDateTime Is Nothing Then
        Errors &= "Start Time is required"
      End If
      'If Errors = "" Then
      '  If csd.TimesheetDate.Value.Date <> csd.StartDateTime.Value.Date Then
      '    Errors &= "Timesheet Date must be the same as Start Date"
      '  End If
      'End If
      Return Errors
    End Function

    Public Shared Function StartDateTimeValid(csd As T) As String
      Dim Errors As String = ""
      '1440 minutes = 24 hours
      '720 minutes = 12 hours
      If csd.TimesheetDate Is Nothing Then
        Errors &= "Schedule Date is required"
      End If
      If csd.StartDateTime Is Nothing Then
        Errors &= "Start Time is required"
      End If
      If csd.EndDateTime Is Nothing Then
        Errors &= "End Time is required"
      End If
      If Errors = "" Then
        If csd.EndDateTime.Value.Subtract(csd.StartDateTime.Value).TotalMinutes < 0 Then
          Errors &= "Start Time must be before End time"
        End If
      End If
      Return Errors
    End Function

    'Public Function GetLocalClashes(csd As CrewScheduleDetailBaseNew) As List(Of String)

    '  Dim cl As New List(Of String)
    '  For Each crewSched As CrewScheduleDetailBaseNew In csd.GetParent.CrewScheduleDetailBaseNewList
    '    If crewSched.Guid <> csd.Guid Then
    '      If crewSched.StartDateTime > csd.EndDateTime Then
    '        cl.Add("End Time Clash with CSD")
    '      End If
    '      If csd.EndDateTime < crewSched.StartDateTime Then
    '        cl.Add("Start Time Clash with CSD")
    '      End If
    '    End If
    '  Next
    '  Return cl

    'End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCrewScheduleDetailBaseNew() method.

    End Sub

    Public Shared Function NewCrewScheduleDetailBaseNew() As CrewScheduleDetailBaseNew(Of T)

      Return DataPortal.CreateChild(Of CrewScheduleDetailBaseNew(Of T))()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    'Friend Shared Function GetCrewScheduleDetailBaseNew(dr As SafeDataReader) As CrewScheduleDetailBaseNew

    '  Dim c As New CrewScheduleDetailBaseNew()
    '  c.Fetch(dr)
    '  Return c

    'End Function

    Public Sub PublicFetch(sdr As SafeDataReader)
      Fetch(sdr)
    End Sub

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CrewScheduleDetailIDProperty, .GetInt32(0))
          LoadProperty(ProductionHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DetailProperty, .GetString(3))
          LoadProperty(TimesheetDateProperty, .GetValue(4))
          LoadProperty(StartDateTimeProperty, .GetValue(5))
          LoadProperty(EndDateTimeProperty, .GetValue(6))
          LoadProperty(ExcludeIndProperty, .GetBoolean(7))
          LoadProperty(ExcludeReasonProperty, .GetString(8))
          LoadProperty(CrewTimesheetIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(CreatedByProperty, .GetInt32(10))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(11))
          LoadProperty(ModifiedByProperty, .GetInt32(12))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(13))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(HumanResourceOffPeriodIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(HumanResourceScheduleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
          LoadProperty(AdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(20)))
          LoadProperty(HumanResourceSecondmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
          LoadProperty(ProductionHRIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
          LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
        End With
      End Using
      FetchExtraProperties(sdr)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = InsProcName
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = UpdProcName
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)
      BeforeInsertUpdate()

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCrewScheduleDetailID As SqlParameter = .Parameters.Add("@CrewScheduleDetailID", SqlDbType.Int)
          paramCrewScheduleDetailID.Value = GetProperty(CrewScheduleDetailIDProperty)
          If Me.IsNew Then
            paramCrewScheduleDetailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionHumanResourceID", Singular.Misc.NothingDBNull(GetProperty(ProductionHumanResourceIDProperty)))
          .Parameters.AddWithValue("@ProductionTimelineID", Singular.Misc.NothingDBNull(GetProperty(ProductionTimelineIDProperty)))
          .Parameters.AddWithValue("@Detail", GetProperty(DetailProperty))
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ExcludeInd", GetProperty(ExcludeIndProperty))
          .Parameters.AddWithValue("@ExcludeReason", GetProperty(ExcludeReasonProperty))
          .Parameters.AddWithValue("@CrewTimesheetID", Singular.Misc.NothingDBNull(GetProperty(CrewTimesheetIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@TimesheetDate", (New SmartDate(GetProperty(TimesheetDateProperty))).DBValue)
          .Parameters.AddWithValue("@HumanResourceID", NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@HumanResourceOffPeriodID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceOffPeriodIDProperty)))
          .Parameters.AddWithValue("@HumanResourceScheduleTypeID", NothingDBNull(GetProperty(HumanResourceScheduleTypeIDProperty)))
          .Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceShiftIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@AdHocBookingID", NothingDBNull(GetProperty(AdHocBookingIDProperty)))
          .Parameters.AddWithValue("@HumanResourceSecondmentID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceSecondmentIDProperty)))
          If Me.GetParent IsNot Nothing Then
            .Parameters.AddWithValue("@ResourceBookingID", Me.GetParent.ResourceBookingID)
          Else
            .Parameters.AddWithValue("@ResourceBookingID", Singular.Misc.NothingDBNull(GetProperty(ResourceBookingIDProperty)))
          End If
          .Parameters.AddWithValue("@ProductionHRID", NothingDBNull(GetProperty(ProductionHRIDProperty)))
          AddExtraParameters(.Parameters)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CrewScheduleDetailIDProperty, paramCrewScheduleDetailID.Value)
          End If
          ' update child objects
          UpdateChildLists()
          MarkOld()
        End With
      Else
        UpdateChildLists()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = DelProcName
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CrewScheduleDetailID", GetProperty(CrewScheduleDetailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

#Region " Overridables "

    <Browsable(False)>
    Public Overridable ReadOnly Property InsProcName As String
      Get
        Return "InsProcsWeb.insCrewScheduleDetailBaseNew"
      End Get
    End Property

    <Browsable(False)>
    Public Overridable ReadOnly Property UpdProcName As String
      Get
        Return "UpdProcsWeb.updCrewScheduleDetailBaseNew"
      End Get
    End Property

    <Browsable(False)>
    Public Overridable ReadOnly Property DelProcName As String
      Get
        Return "DelProcsWeb.delCrewScheduleDetailBaseNew"
      End Get
    End Property

    Public MustOverride Function GetParentList() As Object
    Public MustOverride Function GetParent() As Object
    Public MustOverride Function CreateNewInstance() As T

    Public MustOverride Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)
    Public MustOverride Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)
    Public MustOverride Sub UpdateChildLists()

#End Region

    Public Overridable Sub BeforeInsertUpdate()

    End Sub

  End Class

End Namespace