﻿' Generated 15 Apr 2015 19:40 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base

  <Serializable()> _
  Public MustInherit Class CrewScheduleDetailBaseList(Of T As CrewScheduleDetailBaseList(Of T, C), C As CrewScheduleDetailBase(Of C))
    Inherits OBBusinessListBase(Of T, C)

#Region " Business Methods "

    Public Function GetItem(CrewScheduleDetailID As Integer) As CrewScheduleDetailBase(Of C)

      For Each child As CrewScheduleDetailBase(Of C) In Me
        If child.CrewScheduleDetailID = CrewScheduleDetailID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Crew Schedule Details"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      'Public UseSearch As Boolean = False
      Public Property ProductionHumanResourceID As Integer? = Nothing
      Public Property ProductionTimelineID As Integer? = Nothing
      Public Property TimesheetDate As DateTime? = Nothing
      Public Property HumanResourceID As Integer? = Nothing
      Public Property HumanResourceOffPeriodID As Integer? = Nothing
      Public Property HumanResourceScheduleTypeID As Integer? = Nothing
      Public Property HumanResourceShiftID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property AdHocBookingID As Integer? = Nothing
      Public Property HumanResourceSecondmentID As Integer? = Nothing
      Public Property ProductionSystemAreaID As Integer? = Nothing

      Public Sub New(ProductionHumanResourceID As Integer?, ProductionTimelineID As Integer?, TimesheetDate As DateTime?, _
                     HumanResourceID As Integer?, HumanResourceOffPeriodID As Integer?, HumanResourceScheduleTypeID As Integer?, HumanResourceShiftID As Integer?, _
                     ProductionAreaID As Integer?, SystemID As Integer?, AdHocBookingID As Integer?, HumanResourceSecondmentID As Integer?)

        'UseSearch = True
        Me.ProductionHumanResourceID = ProductionHumanResourceID
        Me.ProductionTimelineID = ProductionTimelineID
        Me.TimesheetDate = TimesheetDate
        Me.HumanResourceID = HumanResourceID
        Me.HumanResourceOffPeriodID = HumanResourceOffPeriodID
        Me.HumanResourceScheduleTypeID = HumanResourceScheduleTypeID
        Me.HumanResourceShiftID = HumanResourceShiftID
        Me.ProductionAreaID = ProductionAreaID
        Me.SystemID = SystemID
        Me.AdHocBookingID = AdHocBookingID
        Me.HumanResourceSecondmentID = HumanResourceSecondmentID

      End Sub

      Public Sub New(HumanResourceID As Integer?, ProductionSystemAreaID As Integer?)
        Me.HumanResourceID = HumanResourceID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    'Public Shared Function NewCrewScheduleDetailBaseList() As CrewScheduleDetailBaseList(Of T, C)

    '  Return New CrewScheduleDetailBaseList(Of T, C)()

    'End Function

    Public Shared Sub BeginGetCrewScheduleDetailBaseList(CallBack As EventHandler(Of DataPortalResult(Of CrewScheduleDetailBaseList(Of T, C))))

      Dim dp As New DataPortal(Of CrewScheduleDetailBaseList(Of T, C))()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetCrewScheduleDetailBaseList() As CrewScheduleDetailBaseList(Of T, C)

      Return DataPortal.Fetch(Of CrewScheduleDetailBaseList(Of T, C))(New Criteria())

    End Function

    Public Shared Function GetCrewScheduleDetailBaseList(HumanResourceID As Integer?, ProductionSystemAreaID As Integer?) As CrewScheduleDetailBaseList(Of T, C)

      Return DataPortal.Fetch(Of CrewScheduleDetailBaseList(Of T, C))(New Criteria(HumanResourceID, ProductionSystemAreaID))

    End Function

    Public Sub PublicFetch(sdr As SafeDataReader)
      Fetch(sdr)
    End Sub

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(FetchNewItem(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = GetProcName
            'If crit.UseSearch Then
            cm.Parameters.AddWithValue("@ProductionHumanResourceID", Singular.Misc.NothingDBNull(crit.ProductionHumanResourceID))
            cm.Parameters.AddWithValue("@ProductionTimelineID", Singular.Misc.NothingDBNull(crit.ProductionTimelineID))
            cm.Parameters.AddWithValue("@TimesheetDate", Singular.Misc.NothingDBNull(crit.TimesheetDate))
            cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@HumanResourceOffPeriodID", Singular.Misc.NothingDBNull(crit.HumanResourceOffPeriodID))
            cm.Parameters.AddWithValue("@HumanResourceScheduleTypeID", Singular.Misc.NothingDBNull(crit.HumanResourceScheduleTypeID))
            cm.Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.NothingDBNull(crit.HumanResourceShiftID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@AdHocBookingID", Singular.Misc.NothingDBNull(crit.AdHocBookingID))
            cm.Parameters.AddWithValue("@HumanResourceSecondmentID", Singular.Misc.NothingDBNull(crit.HumanResourceSecondmentID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(crit.ProductionSystemAreaID))
            'End If
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As CrewScheduleDetailBase(Of C) In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As CrewScheduleDetailBase(Of C) In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

#Region " Overridables "

    <Browsable(False)>
    Public Overridable Property GetProcName As String = "GetProcsWeb.getCrewScheduleDetailBaseList"
    Public MustOverride Function FetchNewItem(sdr As Csla.Data.SafeDataReader) As C
    Public MustOverride Function NewCrewScheduleDetailList() As T

#End Region

  End Class

End Namespace