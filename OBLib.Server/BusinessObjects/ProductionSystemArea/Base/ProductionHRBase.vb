﻿' Generated 18 Jun 2015 18:34 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Resources
Imports OBLib.HR.ReadOnly
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Productions.Base

  <Serializable()> _
  Public MustInherit Class ProductionHRBase(Of T As ProductionHRBase(Of T))
    Inherits OBBusinessBase(Of T)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Expanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHRID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public ReadOnly Property ProductionHRID() As Integer
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:=""),
    Required(ErrorMessage:="Production System Area is required")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human ResourceID is required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.HumanResource, "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Human Resource is required")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ResourceID", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="ResourceID", Description:=""),
    Required(ErrorMessage:="Resource is required")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared IsBusyUpdatingProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsBusyUpdating, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Is Busy Updating")>
    Public Property IsBusyUpdating() As Boolean
      Get
        Return GetProperty(IsBusyUpdatingProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsBusyUpdatingProperty, Value)
      End Set
    End Property

    '<Browsable(False)>
    'Public ReadOnly Property ProductionHumanResourceList As List(Of ProductionHumanResourceBase)
    '  Get
    '    If Me.GetParent IsNot Nothing Then
    '      Return Me.GetParent.ProductionHumanResourceBaseList.Where(Function(d) CompareSafe(d.HumanResourceID, Me.HumanResourceID)).ToList
    '    Else
    '      Return New List(Of ProductionHumanResourceBase)
    '    End If
    '  End Get
    'End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHRIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.IsNew Then
        Return Me.HumanResource & " - New"
      Else
        Return Me.HumanResource
      End If

    End Function

#End Region

    '#Region " Child Lists "

    '    Public Shared ProductionHRResourceBookingListProperty As PropertyInfo(Of RSPro) = RegisterProperty(Of ProductionHRResourceBookingList)(Function(c) c.ProductionHRResourceBookingList, "ResourceBookingList")
    '    Public ReadOnly Property ProductionHRResourceBookingList() As ProductionHRResourceBookingList
    '      Get
    '        If GetProperty(ProductionHRResourceBookingListProperty) Is Nothing Then
    '          LoadProperty(ProductionHRResourceBookingListProperty, Resources.ProductionHRResourceBookingList.NewProductionHRResourceBookingList())
    '        End If
    '        Return GetProperty(ProductionHRResourceBookingListProperty)
    '      End Get
    '    End Property

    '#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionHRBase() method.

    End Sub

    Public Shared Function NewProductionHRBase() As ProductionHRBase(Of T)

      Return DataPortal.CreateChild(Of ProductionHRBase(Of T))()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    'Friend Shared Function GetProductionHRBase(dr As SafeDataReader) As ProductionHRBase

    '  Dim p As New ProductionHRBase()
    '  p.Fetch(dr)
    '  Return p

    'End Function

    Public Sub PublicFetch(sdr As SafeDataReader)
      Fetch(sdr)
    End Sub

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionHRIDProperty, .GetInt32(0))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(HumanResourceProperty, .GetString(3))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          'LoadProperty(PreviousHumanResourceIDProperty, HumanResourceID)
          'LoadProperty(PreviousHumanResourceProperty, HumanResource)
        End With
      End Using
      FetchExtraProperties(sdr)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = InsProcName

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = UpdProcName

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)
      'EnsureResourceBookingCorrect()

      If Me.IsSelfDirty Then
        With cm
          .CommandType = CommandType.StoredProcedure
          Dim paramProductionHRID As SqlParameter = .Parameters.Add("@ProductionHRID", SqlDbType.Int)
          paramProductionHRID.Value = GetProperty(ProductionHRIDProperty)
          If Me.IsNew Then
            paramProductionHRID.Direction = ParameterDirection.Output
          End If
          If Me.GetParent IsNot Nothing Then
            .Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent.ProductionSystemAreaID)
          Else
            .Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(GetProperty(ProductionSystemAreaIDProperty)))
          End If
          .Parameters.AddWithValue("@HumanResourceID", NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@ResourceID", NothingDBNull(GetProperty(ResourceIDProperty)))
          AddExtraParameters(.Parameters)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionHRIDProperty, paramProductionHRID.Value)
          End If
          ' update child objects
          BeforeSaveChildren()
          UpdateChildLists()
          MarkOld()
        End With
      Else
        BeforeSaveChildren()
        UpdateChildLists()
        MarkOld()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub
      DeleteChildren()

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = DelProcName
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionHRID", GetProperty(ProductionHRIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

#Region " Overridables "

    <Browsable(False)>
    Public Overridable Property InsProcName As String = "InsProcsWeb.insProductionHRBase"
    <Browsable(False)>
    Public Overridable Property UpdProcName As String = "UpdProcsWeb.updProductionHRBase"
    <Browsable(False)>
    Public Overridable Property DelProcName As String = "DelProcsWeb.delProductionHRBase"

    Public MustOverride Function GetParentList() As Object
    Public MustOverride Function GetParent() As Object
    Public MustOverride Function CreateNewInstance() As T

    Public MustOverride Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)
    Public MustOverride Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)
    Public MustOverride Sub UpdateChildLists()

    Public MustOverride ReadOnly Property ClashList As List(Of OBLib.Helpers.ResourceHelpers.ClashDetail)
    Public MustOverride Function GetClashList() As List(Of OBLib.Helpers.ResourceHelpers.ClashDetail)
    Public MustOverride Sub DeleteChildren()

    Public Overridable Sub BeforeSaveChildren()
    End Sub

#End Region

  End Class

End Namespace