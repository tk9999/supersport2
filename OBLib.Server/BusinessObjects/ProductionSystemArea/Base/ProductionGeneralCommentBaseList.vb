﻿' Generated 24 Jul 2014 16:52 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base

  <Serializable()> _
  Public Class ProductionGeneralCommentBaseList
    Inherits SingularBusinessListBase(Of ProductionGeneralCommentBaseList, ProductionGeneralCommentBase)

#Region " Business Methods "

    Public Function GetItem(ProductionGeneralCommentID As Integer) As ProductionGeneralCommentBase

      For Each child As ProductionGeneralCommentBase In Me
        If child.ProductionGeneralCommentID = ProductionGeneralCommentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production General Comments"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing

      Public Sub New(ProductionID As Integer?,
                     SystemID As Integer?,
                     ProductionAreaID As Integer?)
        Me.ProductionID = ProductionID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewProductionGeneralCommentBaseList() As ProductionGeneralCommentBaseList

      Return New ProductionGeneralCommentBaseList()

    End Function

    Public Shared Sub BeginGetProductionGeneralCommentBaseList(CallBack As EventHandler(Of DataPortalResult(Of ProductionGeneralCommentBaseList)))

      Dim dp As New DataPortal(Of ProductionGeneralCommentBaseList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionGeneralCommentBaseList() As ProductionGeneralCommentBaseList

      Return DataPortal.Fetch(Of ProductionGeneralCommentBaseList)(New Criteria())

    End Function

    Public Shared Function GetProductionGeneralCommentBaseList(ProductionID As Integer?,
                                                           SystemID As Integer?,
                                                           ProductionAreaID As Integer?) As ProductionGeneralCommentBaseList

      Return DataPortal.Fetch(Of ProductionGeneralCommentBaseList)(New Criteria(ProductionID, SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductionGeneralCommentBase.GetProductionGeneralCommentBase(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getProductionGeneralCommentBaseList"
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionGeneralCommentBase In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionGeneralCommentBase In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace