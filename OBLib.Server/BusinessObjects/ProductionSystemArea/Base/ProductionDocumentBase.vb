﻿' Generated 24 Jul 2014 14:33 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.Documents.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base

  <Serializable()> _
  Public Class ProductionDocumentBase
    Inherits Singular.Documents.DocumentProviderBase(Of ProductionDocumentBase)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared DocumentTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.DocumentTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Document Type value
    ''' </summary>
    <Display(Name:="Type", Description:=""),
    Required(ErrorMessage:="Type required"),
    DropDownWeb(GetType(RODocumentTypeList))>
    Public Overridable Property DocumentTypeID() As Integer?
      Get
        Return GetProperty(DocumentTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DocumentTypeIDProperty, Value)
      End Set
    End Property
    '.AddSetExpression(OBLib.JSCode.ProductionDocumentBaseJS.DocumentTypeIDSet, False)

    Public Shared ParentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentID, "Parent", Nothing)
    ''' <summary>
    ''' Gets and sets the Parent value
    ''' </summary>
    <Display(Name:="Parent", Description:="")>
    Public Overridable Property ParentID() As Integer?
      Get
        Return GetProperty(ParentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ParentIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(ROSystemList))>
    Public Overridable Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required"),
    DropDownWeb(GetType(ROProductionAreaList))>
    Public Overridable Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ParentTableProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ParentTable, "Parent Table", "Productions")
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Parent Table", Description:=""),
    Required(ErrorMessage:="Parent Table required")>
    Public Overridable Property ParentTable() As String
      Get
        Return GetProperty(ParentTableProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ParentTableProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DocumentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.DocumentName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production Document")
        Else
          Return String.Format("Blank {0}", "Production Document")
        End If
      Else
        Return Me.DocumentName
      End If

    End Function

    Public Function GetParent() As Production

      Return CType(CType(Me.Parent, ProductionDocumentBaseList).Parent, Production)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionDocumentBase() method.

    End Sub

    Public Shared Function NewProductionDocumentBase() As ProductionDocumentBase

      Return DataPortal.CreateChild(Of ProductionDocumentBase)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Protected Overrides Sub CallSaveDocument()
      Me.SaveDocument()
    End Sub

    Friend Shared Function GetProductionDocumentBase(dr As SafeDataReader) As ProductionDocumentBase

      Dim p As New ProductionDocumentBase()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(DocumentIDProperty, .GetInt32(0))
          LoadProperty(DocumentTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DocumentNameProperty, .GetString(2))
          'LoadProperty(DocumentProperty, .GetInt32(3))
          LoadProperty(ParentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Overrides Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionDocumentBase"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Overrides Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionDocumentBase"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'CallSaveDocument()

      Dim MustSaveBinary As Boolean = False
      If Document IsNot Nothing AndAlso Document.IsDirty Then
        MustSaveBinary = True
        'If Document.IsDeleted Then
        '  LoadProperty(DocumentIDProperty, Nothing)
        '  Document = Nothing
        'Else
        '  LoadProperty(DocumentIDProperty, Document.DocumentID)
        'End If

      End If

      If Me.IsSelfDirty Or (Document IsNot Nothing AndAlso Document.IsDirty) Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramDocumentID As SqlParameter = .Parameters.Add("@DocumentID", SqlDbType.Int)
          paramDocumentID.Value = GetProperty(DocumentIDProperty)
          If Me.IsNew Then
            paramDocumentID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@DocumentTypeID", GetProperty(DocumentTypeIDProperty))
          .Parameters.AddWithValue("@DocumentName", GetProperty(DocumentNameProperty))
          .Parameters.AddWithValue("@MustSaveBinary", MustSaveBinary)
          If MustSaveBinary Then
            .Parameters.AddWithValue("@Document", Document.Document)
          Else
            .Parameters.AddWithValue("@Document", NothingDBNull(Nothing))
          End If
          .Parameters.AddWithValue("@ParentID", NothingDBNull(GetParent.ProductionID))
          .Parameters.AddWithValue("@ParentTable", GetProperty(ParentTableProperty))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(DocumentIDProperty, paramDocumentID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else

      End If

    End Sub

    Overrides Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionDocumentBase"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@DocumentID", GetProperty(DocumentIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace