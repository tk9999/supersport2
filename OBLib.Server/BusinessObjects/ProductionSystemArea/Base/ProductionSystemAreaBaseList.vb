﻿' Generated 05 Jan 2015 09:31 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Correspondence
Imports OBLib.Resources

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base

  <Serializable()> _
  Public MustInherit Class ProductionSystemAreaBaseList(Of T As ProductionSystemAreaBaseList(Of T, C), C As ProductionSystemAreaBase(Of C))
    Inherits OBBusinessListBase(Of T, C)

#Region " Business Methods "

    Public Function GetItem(ProductionSystemAreaID As Integer) As ProductionSystemAreaBase(Of C)

      For Each child As ProductionSystemAreaBase(Of C) In Me
        If child.ProductionSystemAreaID = ProductionSystemAreaID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production System Areas"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    <Serializable()> _
    Public Class Criteria
      Inherits SingularCriteriaBase(Of Criteria)

      Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "ID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="ID", Description:="")>
      Public Property ProductionSystemAreaID() As Integer?
        Get
          Return ReadProperty(ProductionSystemAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionSystemAreaIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "ProductionID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production", Description:="")>
      Public Property ProductionID() As Integer?
        Get
          Return ReadProperty(ProductionIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionIDProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", OBLib.Security.Settings.CurrentUser.SystemID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
      Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area is required")>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Room", Description:="")>
      Public Property RoomID() As Integer?
        Get
          Return ReadProperty(RoomIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(RoomIDProperty, Value)
        End Set
      End Property

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Room", Description:="")>
      Public Property StartDate() As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Room", Description:="")>
      Public Property EndDate() As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Property RoomIDs As String = ""
      Public Property RoomScheduleIDs As String = ""

      Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingID, "AdHocBookingID", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Room", Description:="")>
      Public Property AdHocBookingID() As Integer?
        Get
          Return ReadProperty(AdHocBookingIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(AdHocBookingIDProperty, Value)
        End Set
      End Property

      Public Sub New(ProductionSystemAreaID As Integer?)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
      End Sub

      Public Sub New(ProductionID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?)
        Me.ProductionID = ProductionID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?, RoomScheduleIDs As String)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.RoomScheduleIDs = RoomScheduleIDs
      End Sub

      Public Sub New()

      End Sub

    End Class

    'Public Shared Function NewProductionSystemAreaBaseList() As ProductionSystemAreaBaseList

    '  Return New ProductionSystemAreaBaseList()

    'End Function

    'Public Shared Sub BeginGetProductionSystemAreaBaseList(CallBack As EventHandler(Of DataPortalResult(Of ProductionSystemAreaBaseList)))

    '  Dim dp As New DataPortal(Of ProductionSystemAreaBaseList)()
    '  AddHandler dp.FetchCompleted, CallBack
    '  dp.BeginFetch(New Criteria())

    'End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Sub PublicFetch(sdr As SafeDataReader)
      Me.Fetch(sdr)
    End Sub

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(FetchNewItem(sdr))
      End While
      Me.RaiseListChangedEvents = True

      FetchChildLists(sdr)

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = GetProcName
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@RoomIDs", Strings.MakeEmptyDBNull(crit.RoomIDs))
            cm.Parameters.AddWithValue("@RoomScheduleIDs", Strings.MakeEmptyDBNull(crit.RoomScheduleIDs))
            cm.Parameters.AddWithValue("@AdHocBookingID", NothingDBNull(crit.AdHocBookingID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionSystemAreaBase(Of C) In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionSystemAreaBase(Of C) In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

    <Browsable(False)>
    Public Overridable ReadOnly Property GetProcName As String
      Get
        Return "GetProcsWeb.getProductionSystemAreaBaseList"
      End Get
    End Property

    Public MustOverride Function FetchNewItem(sdr As Csla.Data.SafeDataReader) As C
    Public MustOverride Function NewProductionSystemAreaList() As T
    Public Overridable Sub FetchChildLists(sdr As SafeDataReader)
    End Sub

#End Region

  End Class

End Namespace