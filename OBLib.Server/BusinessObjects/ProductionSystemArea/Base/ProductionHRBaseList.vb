﻿' Generated 18 Jun 2015 18:34 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Resources

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Productions.Base

  <Serializable()> _
  Public MustInherit Class ProductionHRBaseList(Of T As ProductionHRBaseList(Of T, C), C As ProductionHRBase(Of C))
    Inherits OBBusinessListBase(Of T, C)

#Region " Business Methods "

    Public Function GetItem(ProductionHRID As Integer) As ProductionHRBase(Of C)

      For Each child As ProductionHRBase(Of C) In Me
        If child.ProductionHRID = ProductionHRID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByGuid(ProductionHRGuid As Guid) As ProductionHRBase(Of C)

      For Each child As ProductionHRBase(Of C) In Me
        If child.Guid = ProductionHRGuid Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByHumanResourceID(HumanResourceID As Integer) As ProductionHRBase(Of C)

      For Each child As ProductionHRBase(Of C) In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByResourceID(ResourceID As Integer) As ProductionHRBase(Of C)

      For Each child As ProductionHRBase(Of C) In Me
        If child.ResourceID = ResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    'Public Function GetProductionHRResourceBookingItem(ResourceBookingID As Integer) As ProductionHRResourceBooking

    '  For Each child As ProductionHRBase(Of C) In Me
    '    For Each child2 As ProductionHRResourceBooking In child.ProductionHRResourceBookingList
    '      If child2.ResourceBookingID = ResourceBookingID Then
    '        Return child2
    '      End If
    '    Next
    '  Next
    '  Return Nothing

    'End Function

    Public Overrides Function ToString() As String

      Return "Production H Rs"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      'Public Property ProductionHumanResourceID As Integer?
      Public Property ProductionSystemAreaID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing
      'Public Property HumanResource As String = ""
      'Public Property SystemID As Integer?
      'Public Property ProductionHRID As Integer?

      Public Sub New(ProductionSystemAreaID As Integer?, HumanResourceID As Integer?)
        'Me.ProductionHumanResourceID = ProductionHumanResourceID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
        Me.HumanResourceID = HumanResourceID
        'Me.HumanResource = HumanResource
        'Me.SystemID = SystemID
        'Me.ProductionHRID = ProductionHRID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    'Public Shared Function NewProductionHRBaseList() As ProductionHRBaseList

    '  Return New ProductionHRBaseList()

    'End Function

    Public Shared Sub BeginGetProductionHRBaseList(CallBack As EventHandler(Of DataPortalResult(Of ProductionHRBaseList(Of T, C))))

      Dim dp As New DataPortal(Of ProductionHRBaseList(Of T, C))()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionHRBaseList() As ProductionHRBaseList(Of T, C)

      Return DataPortal.Fetch(Of ProductionHRBaseList(Of T, C))(New Criteria())

    End Function

    Public Shared Function GetProductionHRBaseList(ProductionSystemAreaID As Integer?, HumanResourceID As Integer?) As ProductionHRBaseList(Of T, C)

      Return DataPortal.Fetch(Of ProductionHRBaseList(Of T, C))(New Criteria(ProductionSystemAreaID, HumanResourceID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      'Production HR
      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(FetchNewItem(sdr))
      End While
      Me.RaiseListChangedEvents = True

      ''Resource Bookings
      'Dim parent As ProductionHRBase = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.ProductionHRID <> sdr.GetInt32(0) Then
      '      parent = Me.GetItem(sdr.GetInt32(0))
      '    End If
      '    parent.ProductionHRResourceBookingList.RaiseListChangedEvents = False
      '    parent.ProductionHRResourceBookingList.Add(ProductionHRResourceBooking.GetProductionHRResourceBooking(sdr))
      '    parent.ProductionHRResourceBookingList.RaiseListChangedEvents = True
      '  End While
      'End If

      ''Crew Schedule Details
      'Dim parentResourceBooking As ProductionHRResourceBooking = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentResourceBooking Is Nothing OrElse (parentResourceBooking.ResourceBookingID <> sdr.GetInt32(26)) Then
      '      parentResourceBooking = Me.GetProductionHRResourceBookingItem(sdr.GetInt32(26))
      '    End If
      '    parentResourceBooking.CrewScheduleDetailBaseList.RaiseListChangedEvents = False
      '    parentResourceBooking.CrewScheduleDetailBaseList.Add(CrewScheduleDetailBase.GetCrewScheduleDetailBase(sdr))
      '    parentResourceBooking.CrewScheduleDetailBaseList.RaiseListChangedEvents = True
      '  End While
      'End If

      ''Production Human Resource
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.ProductionHRID <> sdr.GetInt32(36) Then
      '      parent = Me.GetItem(sdr.GetInt32(36))
      '    End If
      '    parent.ProductionHumanResourceBaseList.RaiseListChangedEvents = False
      '    parent.ProductionHumanResourceBaseList.Add(ProductionHumanResourceBase.GetProductionHumanResourceBase(sdr))
      '    parent.ProductionHumanResourceBaseList.RaiseListChangedEvents = True
      '  End While
      'End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = GetProcName
            'cm.Parameters.AddWithValue("@ProductionHRID", Singular.Misc.NothingDBNull(crit.ProductionHRID))
            'cm.Parameters.AddWithValue("@ProductionHumanResourceID", Singular.Misc.NothingDBNull(crit.ProductionHumanResourceID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
            'cm.Parameters.AddWithValue("@HumanResource", Singular.Misc.NothingDBNull(crit.HumanResource))
            'cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionHRBase(Of C) In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionHRBase(Of C) In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

#Region " Overridables "

    <Browsable(False)>
    Public Overridable ReadOnly Property GetProcName As String
      Get
        Return "GetProcsWeb.getProductionHRBaseList"
      End Get
    End Property

    Public MustOverride Function FetchNewItem(sdr As Csla.Data.SafeDataReader) As C
    Public MustOverride Function NewProductionHRList() As T

#End Region

  End Class

End Namespace