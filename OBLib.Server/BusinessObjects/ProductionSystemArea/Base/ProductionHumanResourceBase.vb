﻿' Generated 05 Jan 2015 09:38 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Vehicles.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

Namespace Productions.Base

  <Serializable()> _
  Public MustInherit Class ProductionHumanResourceBase(Of T As ProductionHumanResourceBase(Of T))
    Inherits OBBusinessBase(Of T)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Expanded"),
    InitialDataOnly>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ProductionHumanResourceID() As Integer
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production")>
    Public Overridable Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.DisciplineID, Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline"),
    Required(ErrorMessage:="Discipline required")>
    Public Overridable Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property
    'DropDownWeb(GetType(RODisciplineList))

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.PositionID, Nothing)
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position"),
    DropDownWeb("ProductionHumanResourceBO.GetPositionList($data)", DisplayMember:="Position", ValueMember:="PositionID")>
    Public Overridable Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property
    'DropDownWeb(GetType(ROPositionList), ThisFilterMember:="DisciplineID")

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionTypeID, "Position Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type"),
    DropDownWeb(GetType(ROPositionTypeList))>
    Public Overridable Property PositionTypeID() As Integer?
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared FilteredProductionTypeIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FilteredProductionTypeInd, "Filtered Production Type", True)
    ''' <summary>
    ''' Gets and sets the Filtered Production Type value
    ''' </summary>
    <Display(Name:="Filtered Production Type"),
    Required(ErrorMessage:="Filtered Production Type required")>
    Public Overridable Property FilteredProductionTypeInd() As Boolean
      Get
        Return GetProperty(FilteredProductionTypeIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(FilteredProductionTypeIndProperty, Value)
      End Set
    End Property

    Public Shared FilteredPositionIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FilteredPositionInd, "Filtered Position", True)
    ''' <summary>
    ''' Gets and sets the Filtered Position value
    ''' </summary>
    <Display(Name:="Filtered Position"),
    Required(ErrorMessage:="Filtered Position required")>
    Public Overridable Property FilteredPositionInd() As Boolean
      Get
        Return GetProperty(FilteredPositionIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(FilteredPositionIndProperty, Value)
      End Set
    End Property

    Public Shared IncludedOffIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IncludedOffInd, "Included Off", False)
    ''' <summary>
    ''' Gets and sets the Included Off value
    ''' </summary>
    <Display(Name:="Included Off"),
    Required(ErrorMessage:="Included Off required")>
    Public Overridable Property IncludedOffInd() As Boolean
      Get
        Return GetProperty(IncludedOffIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IncludedOffIndProperty, Value)
      End Set
    End Property

    Public Shared PreferredHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PreferredHumanResourceID, "Preferred Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Preferred Human Resource value
    ''' </summary>
    <Display(Name:="Preferred Human Resource")>
    Public Overridable Property PreferredHumanResourceID() As Integer?
      Get
        Return GetProperty(PreferredHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PreferredHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared SuppliedHumanResourceIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SuppliedHumanResourceInd, "Supplied Human Resource", False)
    ''' <summary>
    ''' Gets and sets the Supplied Human Resource value
    ''' </summary>
    <Display(Name:="Supplied Human Resource"),
    Required(ErrorMessage:="Supplied Human Resource required")>
    Public Overridable Property SuppliedHumanResourceInd() As Boolean
      Get
        Return GetProperty(SuppliedHumanResourceIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SuppliedHumanResourceIndProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource")>
    Public Overridable Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared SelectionReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectionReason, "Selection Reason", "")
    ''' <summary>
    ''' Gets and sets the Selection Reason value
    ''' </summary>
    <Display(Name:="Selection Reason"),
    StringLength(200, ErrorMessage:="Selection Reason cannot be more than 200 characters")>
    Public Overridable Property SelectionReason() As String
      Get
        Return GetProperty(SelectionReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SelectionReasonProperty, Value)
      End Set
    End Property

    Public Shared CancelledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDate, "Cancelled Date")
    ''' <summary>
    ''' Gets and sets the Cancelled Date value
    ''' </summary>
    <Display(Name:="Cancelled Date")>
    Public Overridable Property CancelledDate As DateTime?
      Get
        Return GetProperty(CancelledDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CancelledDateProperty, Value)
      End Set
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets and sets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason"),
    StringLength(200, ErrorMessage:="Cancelled Reason cannot be more than 200 characters")>
    Public Overridable Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CancelledReasonProperty, Value)
      End Set
    End Property

    Public Shared CrewLeaderIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CrewLeaderInd, "Crew Leader", False)
    ''' <summary>
    ''' Gets and sets the Crew Leader value
    ''' </summary>
    <Display(Name:="Crew Leader"),
    Required(ErrorMessage:="Crew Leader required")>
    Public Overridable Property CrewLeaderInd() As Boolean
      Get
        Return GetProperty(CrewLeaderIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(CrewLeaderIndProperty, Value)
      End Set
    End Property

    Public Shared TrainingIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TrainingInd, "Training", False)
    ''' <summary>
    ''' Gets and sets the Training value
    ''' </summary>
    <Display(Name:="Training"),
    Required(ErrorMessage:="Training required")>
    Public Overridable Property TrainingInd() As Boolean
      Get
        Return GetProperty(TrainingIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(TrainingIndProperty, Value)
      End Set
    End Property

    Public Shared TBCIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TBCInd, "TBC", False)
    ''' <summary>
    ''' Gets and sets the TBC value
    ''' </summary>
    <Display(Name:="TBC"),
    Required(ErrorMessage:="TBC required")>
    Public Overridable Property TBCInd() As Boolean
      Get
        Return GetProperty(TBCIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(TBCIndProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle"),
    DropDownWeb(GetType(ROVehicleList))>
    Public Overridable Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System"),
    Required(ErrorMessage:="System required")>
    Public Overridable Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments"),
    StringLength(1000, ErrorMessage:="Comments cannot be more than 1000 characters")>
    Public Overridable Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

    Public Shared ReliefCrewIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ReliefCrewInd, "Relief Crew", False)
    ''' <summary>
    ''' Gets and sets the Relief Crew value
    ''' </summary>
    <Display(Name:="Relief Crew"),
    Required(ErrorMessage:="Relief Crew required")>
    Public Overridable Property ReliefCrewInd() As Boolean
      Get
        Return GetProperty(ReliefCrewIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ReliefCrewIndProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area")>
    Public Overridable Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    'Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Room value
    ' ''' </summary>
    '<Display(Name:="Room")>
    'Public Overridable Property RoomID() As Integer?
    '  Get
    '    Return GetProperty(RoomIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(RoomIDProperty, Value)
    '  End Set
    'End Property

#End Region

#Region " Non Database Properties "

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.HumanResource, "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource")>
    Public Overridable Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared DisciplinePositionProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.DisciplinePosition, "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Role")>
    Public Property DisciplinePosition() As String
      Get
        Return GetProperty(DisciplinePositionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplinePositionProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public Overridable Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsCancelled, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Cancellation Details")>
    Public Overridable Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeStringProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.StartDateTimeString, "")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start"), AlwaysClean>
    Public Overridable Property StartDateTimeString As String
      Get
        Return GetProperty(StartDateTimeStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StartDateTimeStringProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeStringProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.EndDateTimeString, "")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End"), AlwaysClean>
    Public Overridable Property EndDateTimeString As String
      Get
        Return GetProperty(EndDateTimeStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EndDateTimeStringProperty, Value)
      End Set
    End Property

    Public Shared PreferredHumanResourceProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.PreferredHumanResource, "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource")>
    Public Overridable Property PreferredHumanResource() As String
      Get
        Return GetProperty(PreferredHumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PreferredHumanResourceProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.IsNew AndAlso HumanResourceID Is Nothing AndAlso DisciplineID Is Nothing AndAlso PositionID Is Nothing Then
        Return "New Production Human Resource"
      ElseIf HumanResourceID IsNot Nothing Then
        Return CommonData.Lists.ROHumanResourceList.GetItem(HumanResourceID).PreferredFirstSurname
      ElseIf DisciplineID IsNot Nothing AndAlso PositionID IsNot Nothing Then
        Return CommonData.Lists.RODisciplineList.GetItem(DisciplineID).Discipline & ": " & CommonData.Lists.ROPositionList.GetItem(PositionID).Position
      ElseIf PositionID IsNot Nothing Then
        Return CommonData.Lists.ROPositionList.GetItem(PositionID).Position
      ElseIf DisciplineID IsNot Nothing Then
        Return CommonData.Lists.RODisciplineList.GetItem(DisciplineID).Discipline
      Else
        Return "Production Human Resource"
      End If

    End Function

    Public Sub SetDisciplinePosition()

      Dim DiscPos As String = ""
      If DisciplineID IsNot Nothing AndAlso PositionID IsNot Nothing Then
        DiscPos = CommonData.Lists.RODisciplineList.GetItem(DisciplineID).Discipline & " (" & CommonData.Lists.ROPositionList.GetItem(PositionID).Position & ")"
      ElseIf DisciplineID IsNot Nothing Then
        DiscPos = CommonData.Lists.RODisciplineList.GetItem(DisciplineID).Discipline
      ElseIf PositionID IsNot Nothing Then
        If DiscPos.Length > 0 Then
          DiscPos &= " (" & CommonData.Lists.ROPositionList.GetItem(PositionID).Position & ")"
        Else
          DiscPos = CommonData.Lists.ROPositionList.GetItem(PositionID).Position
        End If
      Else
        DiscPos = "Unspecified Role"
      End If
      DisciplinePosition = DiscPos

    End Sub

    Public Sub LoadDisciplinePosition()

      Dim DiscPos As String = ""
      If DisciplineID IsNot Nothing AndAlso PositionID IsNot Nothing Then
        DiscPos = CommonData.Lists.RODisciplineList.GetItem(DisciplineID).Discipline & " (" & CommonData.Lists.ROPositionList.GetItem(PositionID).Position & ")"
      ElseIf DisciplineID IsNot Nothing Then
        DiscPos = CommonData.Lists.RODisciplineList.GetItem(DisciplineID).Discipline
      ElseIf PositionID IsNot Nothing Then
        If DiscPos.Length > 0 Then
          DiscPos &= " (" & CommonData.Lists.ROPositionList.GetItem(PositionID).Position & ")"
        Else
          DiscPos = CommonData.Lists.ROPositionList.GetItem(PositionID).Position
        End If
      Else
        DiscPos = "Unspecified Role"
      End If
      LoadProperty(DisciplinePositionProperty, DiscPos)

    End Sub

    Public Sub LoadHumanResource()
      If HumanResourceID Is Nothing Then
        HumanResource = ""
      End If
    End Sub

    Public Sub LoadIsCancelled()
      If CancelledDate IsNot Nothing Then
        LoadProperty(IsCancelledProperty, True)
      Else
        LoadProperty(IsCancelledProperty, False)
      End If
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

    End Sub

    Public Shared Function CancelledDateValid(ProductionHumanResource As T) As String

      Dim ErrorString As String = ""
      If ProductionHumanResource.IsCancelled Then
        If ProductionHumanResource.CancelledDate Is Nothing Or ProductionHumanResource.CancelledReason.Trim.Length = 0 Then
          ErrorString = "Cancelled Date and Cancelled Reason are required"
        End If
      End If
      Return ErrorString

    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionHumanResourceBase() method.

    End Sub

    'Public Shared Function NewProductionHumanResourceBase() As ProductionHumanResourceBase

    '  Return DataPortal.CreateChild(Of ProductionHumanResourceBase)()

    'End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Sub PublicFetch(sdr As SafeDataReader)
      Fetch(sdr)
    End Sub

    'Friend Shared Function GetProductionHumanResourceBase(dr As SafeDataReader) As ProductionHumanResourceBase(Of C)

    '  Dim p As New ProductionHumanResourceBase(Of C)()
    '  p.Fetch(dr)
    '  Return p

    'End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionHumanResourceIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(FilteredProductionTypeIndProperty, .GetBoolean(5))
          LoadProperty(FilteredPositionIndProperty, .GetBoolean(6))
          LoadProperty(IncludedOffIndProperty, .GetBoolean(7))
          LoadProperty(PreferredHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(SuppliedHumanResourceIndProperty, .GetBoolean(9))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(SelectionReasonProperty, .GetString(11))
          LoadProperty(CancelledDateProperty, .GetValue(12))
          LoadProperty(CancelledReasonProperty, .GetString(13))
          LoadProperty(CrewLeaderIndProperty, .GetBoolean(14))
          LoadProperty(TrainingIndProperty, .GetBoolean(15))
          LoadProperty(TBCIndProperty, .GetBoolean(16))
          LoadProperty(CreatedByProperty, .GetInt32(17))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(18))
          LoadProperty(ModifiedByProperty, .GetInt32(19))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(20))
          LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
          LoadProperty(CommentsProperty, .GetString(23))
          LoadProperty(ReliefCrewIndProperty, .GetBoolean(24))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(25)))
          'LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(26)))
          LoadProperty(HumanResourceProperty, .GetString(27))
          LoadProperty(PreferredHumanResourceProperty, .GetString(28))
          'LoadProperty(OrderNoProperty, .GetInt32(29))
          'LoadProperty(PriorityProperty, .GetInt32(30))
          'LoadProperty(StartDateTimeProperty, .GetValue(31))
          'LoadProperty(EndDateTimeProperty, .GetValue(32))
          LoadProperty(ResourceIDProperty, ZeroNothing(.GetInt32(36)))
          LoadDisciplinePosition()
          LoadHumanResource()
          LoadIsCancelled()
        End With
      End Using
      FetchExtraProperties(sdr)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      'If Singular.Security.HasAccess("Productions", "Can Insert Production Human Resource") Then
      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = InsProcName
        DoInsertUpdateChild(cm)
      End Using
      'End If

    End Sub

    Friend Sub Update()

      'If Singular.Security.HasAccess("Productions", "Can Update Production Human Resource") Then
      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = UpdProcName
        DoInsertUpdateChild(cm)
      End Using
      'End If

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionHumanResourceID As SqlParameter = .Parameters.Add("@ProductionHumanResourceID", SqlDbType.Int)
          paramProductionHumanResourceID.Value = GetProperty(ProductionHumanResourceIDProperty)
          If Me.IsNew Then
            paramProductionHumanResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionID", NothingDBNull(GetProperty(ProductionIDProperty)))
          .Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
          .Parameters.AddWithValue("@PositionID", Singular.Misc.NothingDBNull(GetProperty(PositionIDProperty)))
          .Parameters.AddWithValue("@PositionTypeID", Singular.Misc.NothingDBNull(GetProperty(PositionTypeIDProperty)))
          .Parameters.AddWithValue("@FilteredProductionTypeInd", GetProperty(FilteredProductionTypeIndProperty))
          .Parameters.AddWithValue("@FilteredPositionInd", GetProperty(FilteredPositionIndProperty))
          .Parameters.AddWithValue("@IncludedOffInd", GetProperty(IncludedOffIndProperty))
          .Parameters.AddWithValue("@PreferredHumanResourceID", Singular.Misc.NothingDBNull(GetProperty(PreferredHumanResourceIDProperty)))
          .Parameters.AddWithValue("@SuppliedHumanResourceInd", GetProperty(SuppliedHumanResourceIndProperty))
          .Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@SelectionReason", GetProperty(SelectionReasonProperty))
          .Parameters.AddWithValue("@CancelledDate", (New SmartDate(GetProperty(CancelledDateProperty))).DBValue)
          .Parameters.AddWithValue("@CancelledReason", GetProperty(CancelledReasonProperty))
          .Parameters.AddWithValue("@CrewLeaderInd", GetProperty(CrewLeaderIndProperty))
          .Parameters.AddWithValue("@TrainingInd", GetProperty(TrainingIndProperty))
          .Parameters.AddWithValue("@TBCInd", GetProperty(TBCIndProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@VehicleID", Singular.Misc.NothingDBNull(GetProperty(VehicleIDProperty)))
          .Parameters.AddWithValue("@SystemID", NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
          .Parameters.AddWithValue("@ReliefCrewInd", GetProperty(ReliefCrewIndProperty))
          .Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionSystemAreaIDProperty)))
          .Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(Nothing)) 'GetProperty(RoomIDProperty)
          .Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUserID)
          AddExtraParameters(.Parameters)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionHumanResourceIDProperty, paramProductionHumanResourceID.Value)
          End If
          ' update child objects
          UpdateChildLists()
          MarkOld()
        End With
      Else
        UpdateChildLists()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub
      DeleteChildren()

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = DelProcName
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionHumanResourceID", GetProperty(ProductionHumanResourceIDProperty))
        cm.Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUser.UserID)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      'If Singular.Security.HasAccess("Productions", "Can Delete Production Human Resource") Then
      If Me.IsNew Then Exit Sub
      cm.ExecuteNonQuery()
      MarkNew()
      'End If

    End Sub

#End If

#End Region

#End Region

#Region " Overridables "

    <Browsable(False)>
    Public Overridable ReadOnly Property InsProcName As String
      Get
        Return "InsProcsWeb.insProductionHumanResourceBase"
      End Get
    End Property

    <Browsable(False)>
    Public Overridable ReadOnly Property UpdProcName As String
      Get
        Return "UpdProcsWeb.updProductionHumanResourceBase"
      End Get
    End Property

    <Browsable(False)>
    Public Overridable ReadOnly Property DelProcName As String
      Get
        Return "DelProcsWeb.delProductionHumanResourceBase"
      End Get
    End Property

    Public MustOverride Function GetParentList() As Object
    Public MustOverride Function GetParent() As Object
    Public MustOverride Function CreateNewInstance() As T

    Public MustOverride Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)
    Public MustOverride Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)
    Public MustOverride Sub UpdateChildLists()
    Public MustOverride Sub DeleteChildren()

#End Region

  End Class

End Namespace