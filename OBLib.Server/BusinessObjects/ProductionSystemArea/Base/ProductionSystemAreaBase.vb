﻿' Generated 05 Jan 2015 09:31 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions.Correspondence
Imports OBLib.Rooms
Imports OBLib.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Productions.Specs.ReadOnly

Namespace Productions.Base

  <Serializable()> _
  Public MustInherit Class ProductionSystemAreaBase(Of T As ProductionSystemAreaBase(Of T))
    Inherits OBBusinessBase(Of T)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="IsExpanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "ID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Overridable Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property
    '    Required(ErrorMessage:="Production required")

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required")>
    Public Overridable Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required")>
    Public Overridable Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Overridable Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property
    '.AddSetExpression("ProductionSystemAreaBaseBO.ProductionAreaStatusIDSet(self)")

    Public Shared StatusDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StatusDate, "Status Date")
    ''' <summary>
    ''' Gets and sets the Status Date value
    ''' </summary>
    <Display(Name:="Status Date", Description:=""),
    Required(ErrorMessage:="Status Date required")>
    Public Overridable Property StatusDate As DateTime?
      Get
        Return GetProperty(StatusDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StatusDateProperty, Value)
      End Set
    End Property

    Public Shared StatusSetByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.StatusSetByUserID, "Status Set By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Status Set By User value
    ''' </summary>
    <Display(Name:="Status Set By User", Description:=""),
    Required(ErrorMessage:="Status Set By User required")>
    Public Overridable Property StatusSetByUserID() As Integer?
      Get
        Return GetProperty(StatusSetByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(StatusSetByUserIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSpecRequirementID, "Spec", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Spec Requirement value
    ''' </summary>
    <Display(Name:="Spec", Description:="")>
    Public Overridable Property ProductionSpecRequirementID() As Integer?
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSpecRequirementIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatusName, "Status", "")
    ''' <summary>
    ''' Gets and sets the Feed value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property ProductionAreaStatusName() As String
      Get
        Return GetProperty(ProductionAreaStatusNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaStatusNameProperty, Value)
      End Set
    End Property

    Public Shared NotComplySpecReasonsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NotComplySpecReasons, "Not Comply Reason", "")
    ''' <summary>
    ''' Gets and sets the Not Comply Spec Reasons value
    ''' </summary>
    <Display(Name:="Not Comply Reason", Description:="If a reason is captured, then the planner has decided to change the spec for this event"),
    StringLength(500, ErrorMessage:="Not Comply Reason cannot be more than 500 characters")>
    Public Overridable Property NotComplySpecReasons() As String
      Get
        Return GetProperty(NotComplySpecReasonsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NotComplySpecReasonsProperty, Value)
      End Set
    End Property

    Public Shared StatusSetDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StatusSetDate, "Status Set Date")
    ''' <summary>
    ''' Gets and sets the Status Set Date value
    ''' </summary>
    <Display(Name:="Status Set Date", Description:=""),
    Required(ErrorMessage:="Status Set Date required")>
    Public Overridable Property StatusSetDate As DateTime?
      Get
        If Not FieldManager.FieldExists(StatusSetDateProperty) Then
          LoadProperty(StatusSetDateProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(StatusSetDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StatusSetDateProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Overridable Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "Room Schedule", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public Overridable Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSpecRequirementNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionSpecRequirementName, "Template", "")
    ''' <summary>
    ''' Gets and sets the Not Comply Spec Reasons value
    ''' </summary>
    <Display(Name:="Template", Description:="Crew Template used for this event")>
    Public Overridable Property ProductionSpecRequirementName() As String
      Get
        Return GetProperty(ProductionSpecRequirementNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionSpecRequirementNameProperty, Value)
      End Set
    End Property

    Public Shared HasSetupAreaIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasSetupAreaInd, "Area Setup?", False)
    ''' <summary>
    ''' Gets and sets the Not Comply Spec Reasons value
    ''' </summary>
    <Display(Name:="Area Setup?", Description:="")>
    Public Overridable Property HasSetupAreaInd() As Boolean
      Get
        Return GetProperty(HasSetupAreaIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasSetupAreaIndProperty, Value)
      End Set
    End Property

    Public Shared AreaCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AreaComments, "My Area Comments", "")
    ''' <summary>
    ''' Gets and sets the Not Comply Spec Reasons value
    ''' </summary>
    <Display(Name:="My Area Comments")>
    Public Overridable Property AreaComments() As String
      Get
        Return GetProperty(AreaCommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AreaCommentsProperty, Value)
      End Set
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingID, "AdHocBooking", Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="AdHocBooking", Description:="")>
    Public Overridable Property AdHocBookingID() As Integer?
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AdHocBookingIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Non-Database Properties"

    Public Shared BookingTimesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingTimes, "Booking Times", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Booking Times", Description:="")>
    Public Property BookingTimes() As String
      Get
        Return GetProperty(BookingTimesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BookingTimesProperty, Value)
      End Set
    End Property

    Public Sub LoadBookingTimes()
      LoadProperty(BookingTimesProperty, GetSummarisedBookingTimes)
    End Sub

    Public Sub SetBookingTimes()
      SetProperty(BookingTimesProperty, GetSummarisedBookingTimes)
    End Sub

    Public Sub UpdateBookingTimes()
      SetProperty(BookingTimesProperty, GetSummarisedBookingTimes)
    End Sub

    Public Overridable Function GetSummarisedBookingTimes() As String
      Return ""
    End Function

    <Browsable(False)>
    Protected Shared Property JSBusinessObjectName As String = "ProductionSystemAreaBaseBO"

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionSystemAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionID.ToString & " - " & Me.RoomScheduleID.ToString & " - " & Me.ProductionSystemAreaID.ToString

    End Function

    Public Overridable Sub CopyFrom(From As ProductionSystemAreaBase(Of T))

      LoadProperty(ProductionIDProperty, From.ProductionID)
      LoadProperty(ProductionSystemAreaIDProperty, From.ProductionSystemAreaID)
      LoadProperty(SystemIDProperty, From.SystemID)
      LoadProperty(ProductionAreaIDProperty, From.ProductionAreaID)
      LoadProperty(ProductionAreaStatusIDProperty, From.ProductionAreaStatusID)
      LoadProperty(StatusDateProperty, From.StatusDate)
      LoadProperty(StatusSetByUserIDProperty, From.StatusSetByUserID)
      LoadProperty(ProductionSpecRequirementIDProperty, From.ProductionSpecRequirementID)
      LoadProperty(ProductionAreaStatusNameProperty, From.ProductionAreaStatusName)
      LoadProperty(CreatedByProperty, From.CreatedBy)
      LoadProperty(CreatedDateTimeProperty, From.CreatedDateTime)
      LoadProperty(ModifiedByProperty, From.ModifiedBy)
      LoadProperty(ModifiedDateTimeProperty, From.ModifiedDateTime)
      LoadProperty(NotComplySpecReasonsProperty, From.NotComplySpecReasons)
      LoadProperty(StatusSetDateProperty, From.StatusSetDate)
      LoadProperty(RoomIDProperty, From.RoomID)
      LoadProperty(RoomScheduleIDProperty, From.RoomScheduleID)
      LoadProperty(ProductionSpecRequirementNameProperty, From.ProductionSpecRequirementName)
      LoadProperty(HasSetupAreaIndProperty, From.HasSetupAreaInd)
      MarkOld()

    End Sub

#End Region

#End Region

#Region " Child Lists "

    'Public Shared ProductionSpecRequirementPositionBaseListProperty As PropertyInfo(Of ProductionSpecRequirementPositionBaseList) = RegisterProperty(Of ProductionSpecRequirementPositionBaseList)(Function(c) c.ProductionSpecRequirementPositionBaseList, "Production Spec Requirement Equipment Type List")
    'Public ReadOnly Property ProductionSpecRequirementPositionBaseList() As ProductionSpecRequirementPositionBaseList
    '  Get
    '    If GetProperty(ProductionSpecRequirementPositionBaseListProperty) Is Nothing Then
    '      LoadProperty(ProductionSpecRequirementPositionBaseListProperty, Productions.Base.ProductionSpecRequirementPositionBaseList.NewProductionSpecRequirementPositionBaseList())
    '    End If
    '    Return GetProperty(ProductionSpecRequirementPositionBaseListProperty)
    '  End Get
    'End Property

    'Public Shared ProductionTimelineListProperty As PropertyInfo(Of TimelineListType) = RegisterProperty(Of TimelineListType)(Function(c) c.ProductionTimelineList, "Production Timeline List")
    'Public ReadOnly Property ProductionTimelineList() As TimelineListType
    '  Get
    '    If GetProperty(ProductionTimelineListProperty) Is Nothing Then
    '      LoadProperty(ProductionTimelineListProperty, Activator.CreateInstance(GetType(TimelineListType)))
    '    End If
    '    Return GetProperty(ProductionTimelineListProperty)
    '  End Get
    'End Property

    'Public Shared ProductionHumanResourceBaseListProperty As PropertyInfo(Of ProductionHumanResourceBaseList) = RegisterProperty(Of ProductionHumanResourceBaseList)(Function(c) c.ProductionHumanResourceBaseList, "Production Human Resource Base List")
    'Public ReadOnly Property ProductionHumanResourceBaseList() As ProductionHumanResourceBaseList
    '  Get
    '    If GetProperty(ProductionHumanResourceBaseListProperty) Is Nothing Then
    '      LoadProperty(ProductionHumanResourceBaseListProperty, Productions.Base.ProductionHumanResourceBaseList.NewProductionHumanResourceBaseList())
    '    End If
    '    Return GetProperty(ProductionHumanResourceBaseListProperty)
    '  End Get
    'End Property

    'Public Shared ProductionHRBaseListProperty As PropertyInfo(Of ProductionHRBaseList) = RegisterProperty(Of ProductionHRBaseList)(Function(c) c.ProductionHRBaseList, "Production HR Base List")
    'Public ReadOnly Property ProductionHRBaseList() As ProductionHRBaseList
    '  Get
    '    If GetProperty(ProductionHRBaseListProperty) Is Nothing Then
    '      LoadProperty(ProductionHRBaseListProperty, Productions.Base.ProductionHRBaseList.NewProductionHRBaseList())
    '    End If
    '    Return GetProperty(ProductionHRBaseListProperty)
    '  End Get
    'End Property

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      'With AddWebRule(ProductionAreaStatusIDProperty)
      '  .JavascriptRuleFunctionName = "ProductionSystemAreaBaseBO.ProductionAreaStatusIDValid"
      '  .ServerRuleFunction = AddressOf ProductionAreaStatusIDValid
      '  ' .AddTriggerProperty(ProductionAreaStatusIDProperty)
      '  .AffectedProperties.Add(ProductionAreaStatusNameProperty)
      'End With

      'With AddWebRule(HasSetupAreaIndProperty)
      '  .JavascriptRuleFunctionName = "ProductionSystemAreaBaseBO.HasSetupAreaIndValid"
      '  .ServerRuleFunction = AddressOf ProductionAreaStatusIDValid
      '  .AddTriggerProperty(ProductionSpecRequirementIDProperty)
      '  .AffectedProperties.Add(ProductionSpecRequirementNameProperty)
      'End With

      'With AddWebRule(HasSetupAreaIndProperty)
      '  .JavascriptRuleFunctionName = "ProductionSystemAreaBaseBO.ProductionSpecRequirementIDValid"
      '  .ServerRuleFunction = AddressOf ProductionSpecRequirementIDValid
      '  .AddTriggerProperty(ProductionSpecRequirementIDProperty)
      '  .AffectedProperties.Add(ProductionSpecRequirementNameProperty)
      'End With

    End Sub

    Public Shared Function ProductionAreaStatusIDValid(ProductionSystemAreaBase As T) As String
      Dim ErrorMsg As String = ""
      Dim RA As String = ""
      If ProductionSystemAreaBase.ProductionAreaStatusID Is Nothing Then
        ErrorMsg = "Status is required"
      End If
      Return ErrorMsg
    End Function

    Public Shared Function HasSetupAreaIndValid(ProductionSystemAreaBase As T) As String
      If ProductionSystemAreaBase.HasSetupAreaInd And ProductionSystemAreaBase.ProductionSpecRequirementID IsNot Nothing Then
        Return ""
      End If
      Return "Your area has not been setup, please select a template"
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionSystemAreaBase() method.

    End Sub

    Public Shared Function NewProductionSystemAreaBase() As ProductionSystemAreaBase(Of T)

      Return DataPortal.CreateChild(Of ProductionSystemAreaBase(Of T))()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Sub PublicFetch(sdr As SafeDataReader)
      Fetch(sdr)
    End Sub

    Protected Overridable Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(sdr.GetInt32(0)))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(StatusDateProperty, .GetValue(5))
          LoadProperty(StatusSetByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(CreatedByProperty, .GetInt32(7))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ProductionSpecRequirementIDProperty, .GetInt32(11))
          LoadProperty(NotComplySpecReasonsProperty, .GetString(12))
          LoadProperty(StatusSetDateProperty, .GetValue(13))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          'LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(ProductionSpecRequirementNameProperty, .GetString(15))
          LoadProperty(ProductionAreaStatusNameProperty, .GetString(16))
          LoadProperty(HasSetupAreaIndProperty, .GetBoolean(17))
          LoadProperty(AreaCommentsProperty, .GetString(18))
          LoadProperty(AdHocBookingIDProperty, ZeroNothing(.GetInt32(19)))
        End With
      End Using
      FetchExtraProperties(sdr, 20)
      LoadBookingTimes()

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Overridable Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = InsProcName
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Overridable Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = UpdProcName
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionSystemAreaID As SqlParameter = .Parameters.Add("@ProductionSystemAreaID", SqlDbType.Int)
          paramProductionSystemAreaID.Value = GetProperty(ProductionSystemAreaIDProperty)
          If Me.IsNew Then
            paramProductionSystemAreaID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionID", Singular.Misc.NothingDBNull(GetProperty(ProductionIDProperty)))
          .Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaStatusID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaStatusIDProperty)))
          .Parameters.AddWithValue("@StatusDate", (New SmartDate(GetProperty(StatusDateProperty))).DBValue)
          .Parameters.AddWithValue("@StatusSetByUserID", Singular.Misc.NothingDBNull(GetProperty(StatusSetByUserIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@ProductionSpecRequirementID", Singular.Misc.NothingDBNull(GetProperty(ProductionSpecRequirementIDProperty)))
          .Parameters.AddWithValue("@NotComplySpecReasons", GetProperty(NotComplySpecReasonsProperty))
          .Parameters.AddWithValue("@StatusSetDate", (New SmartDate(GetProperty(StatusSetDateProperty))).DBValue)
          '.Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(GetProperty(RoomIDProperty)))
          .Parameters.AddWithValue("@RoomScheduleID", Singular.Misc.NothingDBNull(GetProperty(RoomScheduleIDProperty)))
          .Parameters.AddWithValue("@HasSetupAreaInd", Singular.Misc.NothingDBNull(GetProperty(HasSetupAreaIndProperty)))
          .Parameters.AddWithValue("@AreaComments", GetProperty(AreaCommentsProperty))
          .Parameters.AddWithValue("@AdHocBookingID", Singular.Misc.NothingDBNull(GetProperty(AdHocBookingIDProperty)))
          AddExtraParameters(.Parameters)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionSystemAreaIDProperty, paramProductionSystemAreaID.Value)
          End If
          ' update child objects
          BeforeUpdateChildren()
          UpdateChildLists()
          MarkOld()
        End With
      Else
        ' update child objects
        BeforeUpdateChildren()
        UpdateChildLists()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub
      DeleteChildren()

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = DelProcName
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(ProductionSystemAreaIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

#Region " Overridables "

    <Browsable(False)>
    Public Overridable Property InsProcName As String = "[InsProcsWeb].[insProductionSystemAreaBase]"
    <Browsable(False)>
    Public Overridable Property UpdProcName As String = "[UpdProcsWeb].[updProductionSystemAreaBase]"
    <Browsable(False)>
    Public Overridable Property DelProcName As String = "DelProcsWeb.delProductionSystemAreaBase"

    Public MustOverride Function CreateNewInstance() As T
    Public MustOverride Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader, StartIndex As Integer)
    Public MustOverride Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)
    Public MustOverride Sub UpdateChildLists()
    Public MustOverride Function GetParent() As Object
    Public MustOverride Function GetParentList() As Object
    'Public MustOverride Sub DeleteSelectedProductionHumanResources()
    Public MustOverride Sub DeleteChildren()
    Public MustOverride Sub BeforeUpdateChildren()

#End Region

  End Class

End Namespace