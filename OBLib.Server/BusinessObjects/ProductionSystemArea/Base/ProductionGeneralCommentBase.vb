﻿' Generated 24 Jul 2014 16:52 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Productions
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base

  <Serializable()> _
  Public Class ProductionGeneralCommentBase
    Inherits SingularBusinessBase(Of ProductionGeneralCommentBase)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionGeneralCommentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionGeneralCommentID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property ProductionGeneralCommentID() As Integer
      Get
        Return GetProperty(ProductionGeneralCommentIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared CommentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comment, "Comment", "")
    ''' <summary>
    ''' Gets and sets the Comment value
    ''' </summary>
    <Display(Name:="Comment", Description:=""),
    Required(ErrorMessage:="Comment required"),
    StringLength(500, ErrorMessage:="Comment cannot be more than 500 characters")>
    Public Property Comment() As String
      Get
        Return GetProperty(CommentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentProperty, Value)
      End Set
    End Property

    Public Shared CommentDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CommentDate, "Comment Date")
    ''' <summary>
    ''' Gets and sets the Comment Date value
    ''' </summary>
    <Display(Name:="Comment Date", Description:=""),
    Required(ErrorMessage:="Comment Date required"),
    DateField(FormatString:="dd MMM yyyy")>
    Public Property CommentDate As DateTime?
      Get
        Return GetProperty(CommentDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CommentDateProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(ROSystemList))>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required"),
    DropDownWeb(GetType(ROProductionAreaList))>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionGeneralCommentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Comment.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Production General Comment")
        Else
          Return String.Format("Blank {0}", "Production General Comment")
        End If
      Else
        Return Me.Comment
      End If

    End Function

    Public Function GetParent() As Production

      Return CType(CType(Me.Parent, ProductionGeneralCommentBaseList).Parent, Production)

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewProductionGeneralCommentBase() method.

    End Sub

    Public Shared Function NewProductionGeneralCommentBase() As ProductionGeneralCommentBase

      Return DataPortal.CreateChild(Of ProductionGeneralCommentBase)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetProductionGeneralCommentBase(dr As SafeDataReader) As ProductionGeneralCommentBase

      Dim p As New ProductionGeneralCommentBase()
      p.Fetch(dr)
      Return p

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionGeneralCommentIDProperty, .GetInt32(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(CommentProperty, .GetString(2))
          LoadProperty(CommentDateProperty, .GetValue(3))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insProductionGeneralCommentBase"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updProductionGeneralCommentBase"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramProductionGeneralCommentID As SqlParameter = .Parameters.Add("@ProductionGeneralCommentID", SqlDbType.Int)
          paramProductionGeneralCommentID.Value = GetProperty(ProductionGeneralCommentIDProperty)
          If Me.IsNew Then
            paramProductionGeneralCommentID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ProductionID", NothingDBNull(Me.GetParent.ProductionID))
          .Parameters.AddWithValue("@Comment", GetProperty(CommentProperty))
          .Parameters.AddWithValue("@CommentDate", (New SmartDate(GetProperty(CommentDateProperty))).DBValue)
          .Parameters.AddWithValue("@SystemID", NothingDBNull(GetProperty(SystemIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(GetProperty(ProductionAreaIDProperty)))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ProductionGeneralCommentIDProperty, paramProductionGeneralCommentID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delProductionGeneralCommentBase"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ProductionGeneralCommentID", GetProperty(ProductionGeneralCommentIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace