﻿' Generated 05 Jan 2015 09:38 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Productions.Base

  <Serializable()> _
  Public MustInherit Class ProductionHumanResourceBaseList(Of T As ProductionHumanResourceBaseList(Of T, C), C As ProductionHumanResourceBase(Of C))
    Inherits OBBusinessListBase(Of T, C)

#Region " Business Methods "

    Public Function GetItem(ProductionHumanResourceID As Integer) As ProductionHumanResourceBase(Of C)

      For Each child As ProductionHumanResourceBase(Of C) In Me
        If child.ProductionHumanResourceID = ProductionHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByGuid(Guid As Guid) As ProductionHumanResourceBase(Of C)

      For Each child As ProductionHumanResourceBase(Of C) In Me
        If child.Guid = Guid Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Production Human Resources"

    End Function

    Sub CreateFromSelectedDiscipline(selectedAllowedDiscipline As SelectedAllowedDiscipline)

      Dim phr As ProductionHumanResourceBase(Of C) = Me.AddNew
      phr.DisciplineID = selectedAllowedDiscipline.DisciplineID
      phr.PositionID = selectedAllowedDiscipline.PositionID
      phr.SystemID = selectedAllowedDiscipline.SystemID
      phr.ProductionID = selectedAllowedDiscipline.ProductionID
      phr.DisciplinePosition = selectedAllowedDiscipline.Discipline
      If selectedAllowedDiscipline.Position <> "" Then
        phr.DisciplinePosition = phr.DisciplinePosition & " (" & selectedAllowedDiscipline.Position & ")"
      End If

    End Sub

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionSystemAreaID As Integer? = Nothing
      Public Property ProductionID As Integer? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property RoomIDs As String = ""
      Public Property RoomScheduleIDs As String = ""
      Public Property ProductionHumanResourceID As Integer?

      Public Sub New(ProductionSystemAreaID As Integer?,
                     ProductionID As Integer?,
                     SystemID As Integer?,
                     ProductionAreaID As Integer?,
                     RoomIDs As String,
                     RoomScheduleIDs As String,
                     ProductionHumanResourceID As Integer?)
        Me.ProductionSystemAreaID = ProductionSystemAreaID
        Me.ProductionID = ProductionID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.RoomIDs = RoomIDs
        Me.RoomScheduleIDs = RoomScheduleIDs
        Me.ProductionHumanResourceID = ProductionHumanResourceID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    'Public Shared Function NewProductionHumanResourceBaseList() As ProductionHumanResourceBaseList(Of T, C)

    '  Return New ProductionHumanResourceBaseList(Of T, C)()

    'End Function

    Public Shared Sub BeginGetProductionHumanResourceBaseList(CallBack As EventHandler(Of DataPortalResult(Of ProductionHumanResourceBaseList(Of T, C))))

      Dim dp As New DataPortal(Of ProductionHumanResourceBaseList(Of T, C))()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetProductionHumanResourceList() As T

      Return DataPortal.Fetch(Of T)(New Criteria())

    End Function

    Public Shared Function GetProductionHumanResourceList(ProductionSystemAreaID As Integer?,
                                                              ProductionID As Integer?,
                                                              SystemID As Integer?,
                                                              ProductionAreaID As Integer?,
                                                              RoomIDs As String,
                                                              RoomScheduleIDs As String,
                                                              ProductionHumanResourceID As Integer?) As T

      Return DataPortal.Fetch(Of T)(New Criteria(ProductionSystemAreaID, ProductionID, SystemID,
                                                 ProductionAreaID, RoomIDs, RoomScheduleIDs,
                                                 ProductionHumanResourceID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(FetchNewItem(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = GetProcName
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(crit.ProductionID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@RoomIDs", Strings.MakeEmptyDBNull(crit.RoomIDs))
            cm.Parameters.AddWithValue("@RoomScheduleIDs", Strings.MakeEmptyDBNull(crit.RoomScheduleIDs))
            cm.Parameters.AddWithValue("@ProductionHumanResourceID", NothingDBNull(crit.ProductionHumanResourceID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ProductionHumanResourceBase(Of C) In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ProductionHumanResourceBase(Of C) In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

#Region " Overridables "

    <Browsable(False)>
    Public Overridable ReadOnly Property GetProcName As String
      Get
        Return "GetProcsWeb.getProductionHumanResourceBaseList"
      End Get
    End Property

    Public MustOverride Function FetchNewItem(sdr As Csla.Data.SafeDataReader) As C
    Public MustOverride Function NewProductionHumanResourceList() As T

#End Region

  End Class

End Namespace