﻿' Generated 02 Mar 2016 05:14 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.DataAnnotations
Imports Singular.Misc

Namespace Slugs

  <Serializable()> _
  Public Class SlugEditorList
    Inherits OBBusinessListBase(Of SlugEditorList, SlugEditor)

#Region " Business Methods "

    Public Function GetItem(SlugID As Integer) As SlugEditor

      For Each child As SlugEditor In Me
        If child.SlugID = SlugID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Slug Items"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property GenRefNumber As Int64?

      Public Sub New(GenRefNumber As Int64?)
        Me.GenRefNumber = GenRefNumber
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSlugEditorList() As SlugEditorList

      Return New SlugEditorList()

    End Function

    Public Shared Sub BeginGetSlugEditorList(CallBack As EventHandler(Of DataPortalResult(Of SlugEditorList)))

      Dim dp As New DataPortal(Of SlugEditorList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSlugEditorList() As SlugEditorList

      Return DataPortal.Fetch(Of SlugEditorList)(New Criteria())

    End Function

    Public Shared Function GetSlugEditorList(GenRefNumber As Int64?) As SlugEditorList

      Return DataPortal.Fetch(Of SlugEditorList)(New Criteria(GenRefNumber))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SlugEditor.GetSlugEditor(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSlugEditorList"
            cm.Parameters.AddWithValue("@GenRefNumber", NothingDBNull(crit.GenRefNumber))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SlugEditor In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SlugEditor In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace