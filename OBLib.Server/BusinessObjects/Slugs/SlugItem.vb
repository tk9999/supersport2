﻿' Generated 02 Mar 2016 05:14 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.DataAnnotations
Imports Singular.Misc

Namespace Slugs

  <Serializable()> _
  Public Class SlugItem
    Inherits OBBusinessBase(Of SlugItem)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SlugItemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SlugItemID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public ReadOnly Property SlugItemID() As Integer
      Get
        Return GetProperty(SlugItemIDProperty)
      End Get
    End Property

    Public Shared SlugIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SlugID, "Slug", 0)
    ''' <summary>
    ''' Gets and sets the Slug value
    ''' </summary>
    <Display(Name:="Slug", Description:=""),
    Required(ErrorMessage:="Slug required")>
    Public Property SlugID() As Integer
      Get
        Return GetProperty(SlugIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SlugIDProperty, Value)
      End Set
    End Property

    Public Shared SlugNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SlugName, "Slug Name", "")
    ''' <summary>
    ''' Gets and sets the Slug Name value
    ''' </summary>
    <Display(Name:="Name", Description:=""),
    StringLength(250, ErrorMessage:="Name cannot be more than 250 characters")>
    Public Property SlugName() As String
      Get
        Return GetProperty(SlugNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SlugNameProperty, Value)
      End Set
    End Property

    Public Shared SlugTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SlugType, "Type", "")
    ''' <summary>
    ''' Gets and sets the Slug Type value
    ''' </summary>
    <Display(Name:="Type", Description:=""),
    StringLength(50, ErrorMessage:="Type cannot be more than 50 characters")>
    Public Property SlugType() As String
      Get
        Return GetProperty(SlugTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SlugTypeProperty, Value)
      End Set
    End Property

    Public Shared SlugDurationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SlugDuration, "Duration", "")
    ''' <summary>
    ''' Gets and sets the Slug Duration value
    ''' </summary>
    <Display(Name:="Duration", Description:=""),
    StringLength(10, ErrorMessage:="Slug Duration cannot be more than 10 characters")>
    Public Property SlugDuration() As String
      Get
        Return GetProperty(SlugDurationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SlugDurationProperty, Value)
      End Set
    End Property

    Public Shared TypeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TypeCode, "Code", "")
    ''' <summary>
    ''' Gets and sets the Type Code value
    ''' </summary>
    <Display(Name:="Code", Description:=""),
    StringLength(25, ErrorMessage:="Code cannot be more than 25 characters")>
    Public Property TypeCode() As String
      Get
        Return GetProperty(TypeCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TypeCodeProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNumber, "Gen Ref Number", Nothing)
    ''' <summary>
    ''' Gets and sets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref Number", Description:=""),
    Required(ErrorMessage:="Gen Ref Number required")>
    Public Property GenRefNumber() As Int64?
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared ImportedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ImportedDateTime, "Imported Date Time", Now)
    ''' <summary>
    ''' Gets and sets the Imported Date Time value
    ''' </summary>
    <Display(Name:="Imported Date Time", Description:="")>
    Public ReadOnly Property ImportedDateTime As DateTime?
      Get
        If Not FieldManager.FieldExists(ImportedDateTimeProperty) Then
          LoadProperty(ImportedDateTimeProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(ImportedDateTimeProperty)
      End Get
    End Property

    Public Shared ImportedByUserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedByUserID, "Imported By User", 0)
    ''' <summary>
    ''' Gets and sets the Imported By User value
    ''' </summary>
    <Display(Name:="Imported By User", Description:="")>
    Public ReadOnly Property ImportedByUserID() As Integer
      Get
        Return GetProperty(ImportedByUserIDProperty)
      End Get
    End Property

    Public Shared ModifedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifedBy, "Modifed By", 0)
    ''' <summary>
    ''' Gets and sets the Modifed By value
    ''' </summary>
    <Display(Name:="Modifed By", Description:="")>
    Public ReadOnly Property ModifedBy() As Integer
      Get
        Return GetProperty(ModifedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared HasPendingPushProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasPendingPush, "Has Pending Push", False)
    ''' <summary>
    ''' Gets and sets the Has Pending Push value
    ''' </summary>
    <Display(Name:="Has Pending Push", Description:="indicates whether this slug item has been updated in Sober and the update is still to be sent back to the primary slug system managed by the programming department"),
    Required(ErrorMessage:="Has Pending Push required")>
    Public Property HasPendingPush() As Boolean
      Get
        Return GetProperty(HasPendingPushProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasPendingPushProperty, Value)
      End Set
    End Property

    Public Shared HasPendingDeleteProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasPendingDelete, "Has Pending Delete", False)
    ''' <summary>
    ''' Gets and sets the Has Pending Delete value
    ''' </summary>
    <Display(Name:="Has Pending Delete", Description:="indicates whether this slug has been deleted by the maintainers of the slugs (programming dept)"),
    Required(ErrorMessage:="Has Pending Delete required")>
    Public Property HasPendingDelete() As Boolean
      Get
        Return GetProperty(HasPendingDeleteProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasPendingDeleteProperty, Value)
      End Set
    End Property

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionHumanResourceID, Nothing) _
                                                                                   .AddSetExpression("SlugItemBO.ProductionHumanResourceIDSet(self)", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Display(Name:="Editor"), DropDownWeb("SlugItemBO.getEditorList($data)", DisplayMember:="HumanResourceName", ValueMember:="ProductionHumanResourceID")>
    Public Property ProductionHumanResourceID() As Integer?
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ProductionHumanResourceIDProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SlugItemIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SlugName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Slug Item")
        Else
          Return String.Format("Blank {0}", "Slug Item")
        End If
      Else
        Return Me.SlugName
      End If

    End Function

    'Public Function GetParent() As RoomScheduling.RoomSchedule
    '  If Me.GetParentList IsNot Nothing Then
    '    Return CType(Me.GetParentList.Parent, RoomScheduling.RoomSchedule)
    '  End If
    '  Return Nothing
    'End Function

    Public Function GetParentList() As SlugItemList
      Return CType(Me.Parent, Slugs.SlugItemList)
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSlugItem() method.

    End Sub

    Public Shared Function NewSlugItem() As SlugItem

      Return DataPortal.CreateChild(Of SlugItem)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSlugItem(dr As SafeDataReader) As SlugItem

      Dim s As New SlugItem()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SlugItemIDProperty, .GetInt32(0))
          LoadProperty(SlugIDProperty, .GetInt32(1))
          LoadProperty(SlugNameProperty, .GetString(2))
          LoadProperty(SlugTypeProperty, .GetString(3))
          LoadProperty(SlugDurationProperty, .GetString(4))
          LoadProperty(TypeCodeProperty, .GetString(5))
          LoadProperty(GenRefNumberProperty, ZeroNothing(.GetInt64(6)))
          LoadProperty(ImportedDateTimeProperty, .GetValue(7))
          LoadProperty(ImportedByUserIDProperty, ZeroNothing(.GetInt32(8)))
          LoadProperty(ModifedByProperty, ZeroNothing(.GetInt32(9)))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(HasPendingPushProperty, .GetBoolean(11))
          LoadProperty(HasPendingDeleteProperty, .GetBoolean(12))
          LoadProperty(ProductionHumanResourceIDProperty, ZeroNothing(.GetInt32(13)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSlugItem"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSlugItem"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSlugItemID As SqlParameter = .Parameters.Add("@SlugItemID", SqlDbType.Int)
          paramSlugItemID.Value = GetProperty(SlugItemIDProperty)
          If Me.IsNew Then
            paramSlugItemID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SlugID", GetProperty(SlugIDProperty))
          .Parameters.AddWithValue("@SlugName", GetProperty(SlugNameProperty))
          .Parameters.AddWithValue("@SlugType", GetProperty(SlugTypeProperty))
          .Parameters.AddWithValue("@SlugDuration", GetProperty(SlugDurationProperty))
          .Parameters.AddWithValue("@TypeCode", GetProperty(TypeCodeProperty))
          .Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
          .Parameters.AddWithValue("@ImportedDateTime", (New SmartDate(GetProperty(ImportedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ImportedByUserID", GetProperty(ImportedByUserIDProperty))
          .Parameters.AddWithValue("@ModifedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@HasPendingPush", GetProperty(HasPendingPushProperty))
          .Parameters.AddWithValue("@HasPendingDelete", GetProperty(HasPendingDeleteProperty))
          .Parameters.AddWithValue("@ProductionHumanResourceID", NothingDBNull(GetProperty(ProductionHumanResourceIDProperty)))
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SlugItemIDProperty, paramSlugItemID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSlugItem"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SlugItemID", GetProperty(SlugItemIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace