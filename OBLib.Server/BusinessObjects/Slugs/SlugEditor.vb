﻿' Generated 02 Mar 2016 05:14 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.DataAnnotations
Imports Singular.Misc

Namespace Slugs

  <Serializable()> _
  Public Class SlugEditor
    Inherits OBBusinessBase(Of SlugEditor)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SlugIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SlugID, "Slug", 0)
    ''' <summary>
    ''' Gets and sets the Slug value
    ''' </summary>
    <Display(Name:="Slug"), Key, Browsable(True),
    Required(ErrorMessage:="Slug required")>
    Public Property SlugID() As Integer
      Get
        Return GetProperty(SlugIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SlugIDProperty, Value)
      End Set
    End Property

    Public Shared SlugNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SlugName, "Slug", "")
    ''' <summary>
    ''' Gets and sets the Slug Name value
    ''' </summary>
    <Display(Name:="Slug")>
    Public Property SlugName() As String
      Get
        Return GetProperty(SlugNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SlugNameProperty, Value)
      End Set
    End Property

    Public Shared SlugTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SlugType, "Type", "")
    ''' <summary>
    ''' Gets and sets the Slug Type value
    ''' </summary>
    <Display(Name:="Type")>
    Public Property SlugType() As String
      Get
        Return GetProperty(SlugTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SlugTypeProperty, Value)
      End Set
    End Property

    Public Shared SlugDurationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SlugDuration, "Duration", "")
    ''' <summary>
    ''' Gets and sets the Slug Duration value
    ''' </summary>
    <Display(Name:="Duration")>
    Public Property SlugDuration() As String
      Get
        Return GetProperty(SlugDurationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SlugDurationProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "EditorID", 0)
    ''' <summary>
    ''' Gets and sets the Slug value
    ''' </summary>
    <Display(Name:="EditorID"),
    Required(ErrorMessage:="EditorID is required")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceName, "Editor", "")
    ''' <summary>
    ''' Gets and sets the Slug Name value
    ''' </summary>
    <Display(Name:="Editor")>
    Public Property HumanResourceName() As String
      Get
        Return GetProperty(HumanResourceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceNameProperty, Value)
      End Set
    End Property

    Public Shared HasPendingDeleteProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasPendingDelete, "Has Pending Delete", False)
    ''' <summary>
    ''' Gets and sets the Has Pending Delete value
    ''' </summary>
    <Display(Name:="Has Pending Delete")>
    Public Property HasPendingDelete() As Boolean
      Get
        Return GetProperty(HasPendingDeleteProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasPendingDeleteProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SlugIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SlugName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Slug Item")
        Else
          Return String.Format("Blank {0}", "Slug Item")
        End If
      Else
        Return Me.SlugName
      End If

    End Function

    'Public Function GetParent() As RoomScheduling.RoomSchedule
    '  If Me.GetParentList IsNot Nothing Then
    '    Return CType(Me.GetParentList.Parent, RoomScheduling.RoomSchedule)
    '  End If
    '  Return Nothing
    'End Function

    Public Function GetParentList() As SlugEditorList
      Return CType(Me.Parent, Slugs.SlugEditorList)
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSlugEditor() method.

    End Sub

    Public Shared Function NewSlugEditor() As SlugEditor

      Return DataPortal.CreateChild(Of SlugEditor)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSlugEditor(dr As SafeDataReader) As SlugEditor

      Dim s As New SlugEditor()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      'Using BypassPropertyChecks
      '  With sdr
      '    LoadProperty(SlugEditorIDProperty, .GetInt32(0))
      '    LoadProperty(SlugIDProperty, .GetInt32(1))
      '    LoadProperty(SlugNameProperty, .GetString(2))
      '    LoadProperty(SlugTypeProperty, .GetString(3))
      '    LoadProperty(SlugDurationProperty, .GetString(4))
      '    LoadProperty(TypeCodeProperty, .GetString(5))
      '    LoadProperty(GenRefNumberProperty, ZeroNothing(.GetInt64(6)))
      '    LoadProperty(ImportedDateTimeProperty, .GetValue(7))
      '    LoadProperty(ImportedByUserIDProperty, ZeroNothing(.GetInt32(8)))
      '    LoadProperty(ModifedByProperty, ZeroNothing(.GetInt32(9)))
      '    LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(10))
      '    LoadProperty(HasPendingPushProperty, .GetBoolean(11))
      '    LoadProperty(HasPendingDeleteProperty, .GetBoolean(12))
      '  End With
      'End Using

      'MarkAsChild()
      'MarkOld()
      'BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      '' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "InsProcsWeb.insSlugEditor"
      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Friend Sub Update()

      '' if we're not dirty then don't update the database
      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "UpdProcsWeb.updSlugEditor"
      '  DoInsertUpdateChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then

      '  With cm
      '    .CommandType = CommandType.StoredProcedure

      '    Dim paramSlugEditorID As SqlParameter = .Parameters.Add("@SlugEditorID", SqlDbType.Int)
      '    paramSlugEditorID.Value = GetProperty(SlugEditorIDProperty)
      '    If Me.IsNew Then
      '      paramSlugEditorID.Direction = ParameterDirection.Output
      '    End If
      '    .Parameters.AddWithValue("@SlugID", GetProperty(SlugIDProperty))
      '    .Parameters.AddWithValue("@SlugName", GetProperty(SlugNameProperty))
      '    .Parameters.AddWithValue("@SlugType", GetProperty(SlugTypeProperty))
      '    .Parameters.AddWithValue("@SlugDuration", GetProperty(SlugDurationProperty))
      '    .Parameters.AddWithValue("@TypeCode", GetProperty(TypeCodeProperty))
      '    .Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
      '    .Parameters.AddWithValue("@ImportedDateTime", (New SmartDate(GetProperty(ImportedDateTimeProperty))).DBValue)
      '    .Parameters.AddWithValue("@ImportedByUserID", GetProperty(ImportedByUserIDProperty))
      '    .Parameters.AddWithValue("@ModifedBy", OBLib.Security.Settings.CurrentUserID)
      '    .Parameters.AddWithValue("@HasPendingPush", GetProperty(HasPendingPushProperty))
      '    .Parameters.AddWithValue("@HasPendingDelete", GetProperty(HasPendingDeleteProperty))

      '    .ExecuteNonQuery()

      '    If Me.IsNew Then
      '      LoadProperty(SlugEditorIDProperty, paramSlugEditorID.Value)
      '    End If
      '    ' update child objects
      '    ' mChildList.Update()
      '    MarkOld()
      '  End With
      'Else
      'End If

    End Sub

    Friend Sub DeleteSelf()

      '' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delSlugEditor"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@SlugEditorID", GetProperty(SlugEditorIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      'If Me.IsNew Then Exit Sub

      'cm.ExecuteNonQuery()
      'MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace