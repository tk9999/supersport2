﻿' Generated 02 Mar 2016 05:14 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.DataAnnotations
Imports Singular.Misc

Namespace Slugs

  <Serializable()> _
  Public Class SlugItemList
    Inherits OBBusinessListBase(Of SlugItemList, SlugItem)

#Region " Business Methods "

    Public Function GetItem(SlugItemID As Integer) As SlugItem

      For Each child As SlugItem In Me
        If child.SlugItemID = SlugItemID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Slug Items"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property GenRefNumber As Int64?

      Public Sub New(GenRefNumber As Int64?)
        Me.GenRefNumber = GenRefNumber
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSlugItemList() As SlugItemList

      Return New SlugItemList()

    End Function

    Public Shared Sub BeginGetSlugItemList(CallBack As EventHandler(Of DataPortalResult(Of SlugItemList)))

      Dim dp As New DataPortal(Of SlugItemList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSlugItemList() As SlugItemList

      Return DataPortal.Fetch(Of SlugItemList)(New Criteria())

    End Function

    Public Shared Function GetSlugItemList(GenRefNumber As Int64?) As SlugItemList

      Return DataPortal.Fetch(Of SlugItemList)(New Criteria(GenRefNumber))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SlugItem.GetSlugItem(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSlugItemList"
            cm.Parameters.AddWithValue("@GenRefNumber", NothingDBNull(crit.GenRefNumber))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SlugItem In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SlugItem In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace