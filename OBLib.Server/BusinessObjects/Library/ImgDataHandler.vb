﻿Imports System.Drawing
Imports System.Drawing.Imaging
Imports Singular.Misc
Imports System.Web
Imports System.IO

Namespace Images

  Public Class ImgDataHandler
    Implements System.Web.IHttpHandler

    Public ReadOnly Property IsReusable As Boolean Implements Web.IHttpHandler.IsReusable
      Get
        Return True
      End Get
    End Property

    Public Sub ProcessRequest(context As Web.HttpContext) Implements Web.IHttpHandler.ProcessRequest

      Try

        Singular.Web.WebServices.HttpHandlerHelper.AddGZipHeaders(context)

        Dim cProc As New Singular.CommandProc("ApiProcs.getImageData", {"ImageID"}, {context.Request.QueryString("ImageID")})
        cProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
        cProc = cProc.Execute
        Singular.Web.WebServices.FileDownloadHandler.SendFile(context.Response, cProc.DataRow("FileName"), cProc.DataRow("ImageData"), False, 180)

      Catch ex As Exception
        Singular.Web.WebServices.HttpHandlerHelper.HandleException(context, ex, "DocID: " & Singular.Misc.IsNullNothing(context.Request.QueryString("DocumentID"), "None"))
      End Try

    End Sub

    Public Shared Lock As New Object

    Public Shared Function InsertImage(FileName As String, FileBytes As Byte(), Type As OBLib.CommonData.Enums.ImageType, ForUserID As Integer, ForHumanResourceID As Integer, context As System.Web.HttpContext) As Integer
      SyncLock Lock

        Dim ReturnValue As Integer = 0
        Select Case Type
          Case CommonData.Enums.ImageType.ProfilePhotoLarge
            ReturnValue = SaveProfilePhotoLarge(FileName, FileBytes, Type, ForUserID, context)
          Case CommonData.Enums.ImageType.AccreditationPhoto
            ReturnValue = SaveAccreditationPhoto(FileName, FileBytes, Type, ForHumanResourceID, context)
          Case CommonData.Enums.ImageType.InternalStaffPhoto
            ReturnValue = SaveInternalStaffPhoto(FileName, FileBytes, Type, ForHumanResourceID, context)
        End Select
        Return ReturnValue

      End SyncLock

    End Function

    Private Shared Function SaveAccreditationPhoto(FileName As String, FileBytes As Byte(), Type As OBLib.CommonData.Enums.ImageType, ForHumanResourceID As Integer, context As System.Web.HttpContext) As Integer
      Dim ReturnValue As Integer = 0
      Try
        'Resize and Save Large Profile Photo
        Dim AccredImageType As OBLib.Maintenance.Images.ReadOnly.ROImageType = OBLib.CommonData.Lists.ROImageTypeList.GetItem(OBLib.CommonData.Enums.ImageType.AccreditationPhoto)

        'Resize and Save Large Profile Photo
        Dim MaxWidth As Integer? = AccredImageType.Width
        Dim MaxHeight As Integer? = AccredImageType.Height

        If MaxWidth IsNot Nothing And MaxHeight IsNot Nothing Then
          Dim Resizer As New Singular.Imaging.Resizer(MaxWidth, MaxHeight, Singular.Imaging.Resizer.ImageResizeMode.KeepOriginalAspect)
          FileBytes = Resizer.GetResizedImage(FileBytes)
          IO.Path.ChangeExtension(FileName, Resizer.FileExtension)
        End If

        'Create new name for image
        Dim AccredFilename As String = ForHumanResourceID.ToString & "_" & AccredImageType.ImageTypeID.ToString & System.IO.Path.GetExtension(FileName).ToString().ToLower()
        Dim cProc As New Singular.CommandProc("InsProcs.insImage", {"@ImageData", "@FileName",
                                                                    "@ImageHash", "@CreatedBy",
                                                                    "@ImageTypeID", "@ForUserID", "@ForHumanResourceID"},
                                              {FileBytes, FileName,
                                               System.Security.Cryptography.SHA256.Create.ComputeHash(FileBytes), Singular.Settings.CurrentUserID,
                                               AccredImageType.ImageTypeID, NothingDBNull(Nothing), ZeroDBNull(ForHumanResourceID)})
        cProc.Parameters.Add(New Singular.CommandProc.Parameter() With {.Name = "@ImageID", .Direction = ParameterDirection.Output, .SqlType = SqlDbType.Int})
        cProc = cProc.Execute
        ReturnValue = cProc.Parameters("@ImageID").Value

        'Save to file path
        Dim fs As MemoryStream = New MemoryStream(FileBytes)
        Dim img As Image = System.Drawing.Image.FromStream(fs)
        If System.IO.File.Exists("~/Images/Accreditation/" & AccredFilename) Then
          System.IO.File.Delete("~/Images/Accreditation/" & AccredFilename)
        End If
        img.Save(context.Server.MapPath("~/Images/Accreditation/") & AccredFilename)

      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("User - " & Singular.Settings.CurrentUserID.ToString, "ImageDataHandler.ashx", "SaveAccreditationPhoto", ex.Message)
      End Try
      Return ReturnValue
    End Function

    Private Shared Function SaveProfilePhotoLarge(FileName As String, FileBytes As Byte(), Type As OBLib.CommonData.Enums.ImageType, ForUserID As Integer, context As System.Web.HttpContext) As Integer

      Dim ReturnValue As Integer? = Nothing

      'Create Large Version
      Try
        Dim LargeImageType As OBLib.Maintenance.Images.ReadOnly.ROImageType = OBLib.CommonData.Lists.ROImageTypeList.GetItem(OBLib.CommonData.Enums.ImageType.ProfilePhotoLarge)

        'Resize and Save Large Profile Photo
        Dim MaxWidth As Integer = LargeImageType.Width
        Dim MaxHeight As Integer = LargeImageType.Height
        Dim Resizer As New Singular.Imaging.Resizer(MaxWidth, MaxHeight, Singular.Imaging.Resizer.ImageResizeMode.KeepOriginalAspect)
        FileBytes = Resizer.GetResizedImage(FileBytes)
        IO.Path.ChangeExtension(FileName, Resizer.FileExtension)

        'Create new name for image
        Dim LargeFilename As String = Singular.Settings.CurrentUserID.ToString & "_" & LargeImageType.ImageTypeID.ToString & System.IO.Path.GetExtension(FileName).ToString().ToLower()

        Dim cProc As New Singular.CommandProc("InsProcs.insImage", {"@ImageData", "@FileName",
                                                                    "@ImageHash", "@CreatedBy",
                                                                    "@ImageTypeID", "@ForUserID", "@ForHumanResourceID"},
                                              {FileBytes, LargeFilename,
                                               System.Security.Cryptography.SHA256.Create.ComputeHash(FileBytes), Singular.Settings.CurrentUserID,
                                               LargeImageType.ImageTypeID, ZeroDBNull(Singular.Settings.CurrentUserID), NothingDBNull(Nothing)})
        cProc.Parameters.Add(New Singular.CommandProc.Parameter() With {.Name = "@ImageID", .Direction = ParameterDirection.Output, .SqlType = SqlDbType.Int})
        cProc = cProc.Execute
        ReturnValue = cProc.Parameters("@ImageID").Value

        'Save to file path
        Dim fs As MemoryStream = New MemoryStream(FileBytes)
        Dim img As Image = System.Drawing.Image.FromStream(fs)
        If System.IO.File.Exists("~/Images/Profile/" & LargeFilename) Then
          System.IO.File.Delete("~/Images/Profile/" & LargeFilename)
        End If
        img.Save(context.Server.MapPath("~/Images/Profile/") & LargeFilename)

      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("User - " & Singular.Settings.CurrentUserID.ToString, "ImageDataHandler.ashx", "SaveProfilePhotoLarge", ex.Message)
      End Try

      'Create Small Version
      Try
        Dim SmallImageType As OBLib.Maintenance.Images.ReadOnly.ROImageType = OBLib.CommonData.Lists.ROImageTypeList.GetItem(OBLib.CommonData.Enums.ImageType.ProfilePhotoSmall)

        'Resize and Save Small Profile Photo
        Dim SmallImageMaxWidth As Integer = SmallImageType.Width
        Dim SmallImageMaxHeight As Integer = SmallImageType.Height
        Dim SmallImageResizer As New Singular.Imaging.Resizer(SmallImageMaxWidth, SmallImageMaxHeight, Singular.Imaging.Resizer.ImageResizeMode.KeepOriginalAspect)
        Dim SmallImageFileBytes As Byte()
        SmallImageFileBytes = SmallImageResizer.GetResizedImage(FileBytes)
        IO.Path.ChangeExtension(FileName, SmallImageResizer.FileExtension)

        'Create new name for image
        Dim SmallFilename As String = Singular.Settings.CurrentUserID.ToString & "_" & SmallImageType.ImageTypeID.ToString & System.IO.Path.GetExtension(FileName).ToString().ToLower()

        Dim SmallImagecProc As New Singular.CommandProc("InsProcs.insImage", {"@ImageData", "@FileName",
                                                                              "@ImageHash", "@CreatedBy",
                                                                              "@ImageTypeID", "@ForUserID", "@ForHumanResourceID"},
                                                        {SmallImageFileBytes, SmallFilename,
                                                         System.Security.Cryptography.SHA256.Create.ComputeHash(SmallImageFileBytes), Singular.Settings.CurrentUserID,
                                                         SmallImageType.ImageTypeID, ZeroDBNull(Singular.Settings.CurrentUserID), NothingDBNull(Nothing)})
        SmallImagecProc.Parameters.Add(New Singular.CommandProc.Parameter() With {.Name = "@ImageID", .Direction = ParameterDirection.Output, .SqlType = SqlDbType.Int})
        SmallImagecProc = SmallImagecProc.Execute

        'Save to file path
        Dim fs As MemoryStream = New MemoryStream(SmallImageFileBytes)
        Dim img As Image = System.Drawing.Image.FromStream(fs)
        If System.IO.File.Exists("~/Images/Profile/" & SmallFilename) Then
          System.IO.File.Delete("~/Images/Profile/" & SmallFilename)
        End If
        img.Save(context.Server.MapPath("~/Images/Profile/") & SmallFilename)

      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("User - " & Singular.Settings.CurrentUserID.ToString, "ImageDataHandler.ashx", "SaveProfilePhotoSmall", ex.Message)
      End Try

      'Create Medium Version
      Try
        Dim MediumImageType As OBLib.Maintenance.Images.ReadOnly.ROImageType = OBLib.CommonData.Lists.ROImageTypeList.GetItem(OBLib.CommonData.Enums.ImageType.ProfilePhotoMedium)

        'Resize and Save Medium Profile Photo
        Dim MediumImageMaxWidth As Integer = MediumImageType.Width
        Dim MediumImageMaxHeight As Integer = MediumImageType.Height
        Dim MediumImageResizer As New Singular.Imaging.Resizer(MediumImageMaxWidth, MediumImageMaxHeight, Singular.Imaging.Resizer.ImageResizeMode.KeepOriginalAspect)
        Dim MediumImageFileBytes As Byte()
        MediumImageFileBytes = MediumImageResizer.GetResizedImage(FileBytes)
        IO.Path.ChangeExtension(FileName, MediumImageResizer.FileExtension)

        'Create new name for image
        Dim MediumFilename As String = Singular.Settings.CurrentUserID.ToString & "_" & MediumImageType.ImageTypeID.ToString & System.IO.Path.GetExtension(FileName).ToString().ToLower()

        Dim MediumImagecProc As New Singular.CommandProc("InsProcs.insImage", {"@ImageData", "@FileName",
                                                                               "@ImageHash", "@CreatedBy",
                                                                               "@ImageTypeID", "@ForUserID", "@ForHumanResourceID"},
                                                        {MediumImageFileBytes, MediumFilename,
                                                         System.Security.Cryptography.SHA256.Create.ComputeHash(MediumImageFileBytes), Singular.Settings.CurrentUserID,
                                                         MediumImageType.ImageTypeID, ZeroDBNull(Singular.Settings.CurrentUserID), NothingDBNull(Nothing)})
        MediumImagecProc.Parameters.Add(New Singular.CommandProc.Parameter() With {.Name = "@ImageID", .Direction = ParameterDirection.Output, .SqlType = SqlDbType.Int})
        MediumImagecProc = MediumImagecProc.Execute

        'Save to file path
        Dim fs As MemoryStream = New MemoryStream(MediumImageFileBytes)
        Dim img As Image = System.Drawing.Image.FromStream(fs)
        If System.IO.File.Exists("~/Images/Profile/" & MediumFilename) Then
          System.IO.File.Delete("~/Images/Profile/" & MediumFilename)
        End If
        img.Save(context.Server.MapPath("~/Images/Profile/") & MediumFilename)

      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("User - " & Singular.Settings.CurrentUserID.ToString, "ImageDataHandler.ashx", "SaveProfilePhotoSmall", ex.Message)
      End Try

      Return ReturnValue

    End Function

    Private Shared Function SaveInternalStaffPhoto(FileName As String, FileBytes As Byte(), Type As OBLib.CommonData.Enums.ImageType, ForHumanResourceID As Integer, context As System.Web.HttpContext) As Integer
      Dim ReturnValue As Integer = 0
      Try
        'Resize and Save Large Profile Photo
        Dim InternalPhotoType As OBLib.Maintenance.Images.ReadOnly.ROImageType = OBLib.CommonData.Lists.ROImageTypeList.GetItem(OBLib.CommonData.Enums.ImageType.InternalStaffPhoto)

        'Resize and Save Large Profile Photo
        Dim MaxWidth As Integer? = InternalPhotoType.Width
        Dim MaxHeight As Integer? = InternalPhotoType.Height

        If MaxWidth IsNot Nothing And MaxHeight IsNot Nothing Then
          Dim Resizer As New Singular.Imaging.Resizer(MaxWidth, MaxHeight, Singular.Imaging.Resizer.ImageResizeMode.KeepOriginalAspect)
          FileBytes = Resizer.GetResizedImage(FileBytes)
          IO.Path.ChangeExtension(FileName, Resizer.FileExtension)
        End If

        'Create new name for image
        Dim InternalPhotoFilename As String = ForHumanResourceID.ToString & "_" & InternalPhotoType.ImageTypeID.ToString & System.IO.Path.GetExtension(FileName).ToString().ToLower()
        Dim cProc As New Singular.CommandProc("InsProcs.insImage", {"@ImageData", "@FileName",
                                                                    "@ImageHash", "@CreatedBy",
                                                                    "@ImageTypeID", "@ForUserID", "@ForHumanResourceID"},
                                              {FileBytes, FileName,
                                               System.Security.Cryptography.SHA256.Create.ComputeHash(FileBytes), Singular.Settings.CurrentUserID,
                                               InternalPhotoType.ImageTypeID, NothingDBNull(Nothing), ZeroDBNull(ForHumanResourceID)})
        cProc.Parameters.Add(New Singular.CommandProc.Parameter() With {.Name = "@ImageID", .Direction = ParameterDirection.Output, .SqlType = SqlDbType.Int})
        cProc = cProc.Execute
        ReturnValue = cProc.Parameters("@ImageID").Value

        'Save to file path
        Dim fs As MemoryStream = New MemoryStream(FileBytes)
        Dim img As Image = System.Drawing.Image.FromStream(fs)
        If System.IO.File.Exists("~/Images/InternalPhotos/" & InternalPhotoFilename) Then
          System.IO.File.Delete("~/Images/InternalPhotos/" & InternalPhotoFilename)
        End If
        img.Save(context.Server.MapPath("~/Images/InternalPhotos/") & InternalPhotoFilename)

      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("User - " & Singular.Settings.CurrentUserID.ToString, "ImageDataHandler.ashx", "SaveAccreditationPhoto", ex.Message)
      End Try
      Return ReturnValue
    End Function

  End Class

End Namespace
