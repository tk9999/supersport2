﻿Imports Csla.Serialization
Imports Csla
Imports Singular.DataAnnotations
Imports System.ComponentModel.DataAnnotations

<Serializable()> _
Public Class OBReadOnlyBase(Of C As OBReadOnlyBase(Of C))
  Inherits Singular.SingularReadOnlyBase(Of C)

  Public Shared IsSelectedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(d) d.IsSelected, False)
  ''' <summary>
  ''' Gets the Country value
  ''' </summary>
  ''' 
  Public Overridable ReadOnly Property IsSelected() As Boolean
    Get
      Return GetProperty(IsSelectedProperty)
    End Get
  End Property

  Public Shared ProcessingErrorTextProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(d) d.ProcessingErrorText, False)
  ''' <summary>
  ''' Gets the Country value
  ''' </summary>
  Public ReadOnly Property ProcessingErrorText() As String
    Get
      Return GetProperty(ProcessingErrorTextProperty)
    End Get
  End Property

  Public Shared IsProcessingProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(d) d.IsProcessing, False)
  ''' <summary>
  ''' Gets the IsProcessing value
  ''' </summary>
  ''' 
  Public ReadOnly Property IsProcessing() As Boolean
    Get
      Return GetProperty(IsProcessingProperty)
    End Get
  End Property

  Public Sub ResetSelected()
    LoadProperty(IsSelectedProperty, False)
  End Sub

End Class
