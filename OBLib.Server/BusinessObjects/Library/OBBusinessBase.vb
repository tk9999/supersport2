﻿Imports Csla.Serialization
Imports Csla
Imports Singular.DataAnnotations
Imports System.ComponentModel.DataAnnotations

<Serializable()> _
Public Class OBBusinessBase(Of C As OBBusinessBase(Of C))
  Inherits Singular.SingularBusinessBase(Of C)

  Public Function CurrentUser() As OBLib.Security.User

    Return OBLib.Security.Settings.CurrentUser

  End Function

  Protected Overrides Function GetSPPrefix() As String
    Return If(IsNew, "InsProcsWeb.ins", "UpdProcsWeb.upd")
  End Function

  Protected Overrides Function GetSPDeletePrefix() As String
    Return "DelProcsWeb.del"
  End Function

  Public Shared IsProcessingProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(d) d.IsProcessing, False)
  ''' <summary>
  ''' Gets the IsProcessing value
  ''' </summary>
  ''' 
  <AlwaysClean>
  Public Overridable Property IsProcessing() As Boolean
    Get
      Return GetProperty(IsProcessingProperty)
    End Get
    Set(value As Boolean)
      SetProperty(IsProcessingProperty, value)
    End Set
  End Property

  Public Shared IsSelectedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(d) d.IsSelected, False)
  ''' <summary>
  ''' Gets the IsProcessing value
  ''' </summary>
  ''' 
  Public Overridable Property IsSelected() As Boolean
    Get
      Return GetProperty(IsSelectedProperty)
    End Get
    Set(value As Boolean)
      SetProperty(IsSelectedProperty, value)
    End Set
  End Property

  Protected Function AddOutputParam(cm As SqlClient.SqlCommand, OutputProperty As Csla.PropertyInfo(Of Integer)) As SqlClient.SqlParameter
    If IsNew Then
      Dim param = cm.Parameters.Add("@" & OutputProperty.Name, SqlDbType.Int)
      param.Direction = ParameterDirection.Output
      Return param
    Else
      Return cm.Parameters.AddWithValue("@" & OutputProperty.Name, GetProperty(OutputProperty))
    End If
  End Function

  Protected Function AddOutputParamNullable(cm As SqlClient.SqlCommand, OutputProperty As Csla.PropertyInfo(Of Integer?)) As SqlClient.SqlParameter
    If IsNew Then
      Dim param = cm.Parameters.Add("@" & OutputProperty.Name, SqlDbType.Int)
      param.Direction = ParameterDirection.Output
      Return param
    Else
      Return cm.Parameters.AddWithValue("@" & OutputProperty.Name, Singular.Misc.NothingDBNull(GetProperty(OutputProperty)))
    End If
  End Function

  Protected Function AddInputOutputParam(cm As SqlClient.SqlCommand, OutputProperty As Csla.PropertyInfo(Of Integer)) As SqlClient.SqlParameter
    Dim param = cm.Parameters.Add("@" & OutputProperty.Name, SqlDbType.Int)
    param.Direction = ParameterDirection.InputOutput
    param.Value = Singular.Misc.ZeroDBNull(GetProperty(OutputProperty))
    Return param
  End Function

  Protected Function AddInputOutputParam(cm As SqlClient.SqlCommand, OutputProperty As Csla.PropertyInfo(Of Integer?)) As SqlClient.SqlParameter
    Dim param = cm.Parameters.Add("@" & OutputProperty.Name, SqlDbType.Int)
    param.Direction = ParameterDirection.InputOutput
    param.Value = Singular.Misc.NothingDBNull(GetProperty(OutputProperty))
    Return param
  End Function

  Protected Function AddInputOutputString(cm As SqlClient.SqlCommand, OutputProperty As Csla.PropertyInfo(Of String), Size As Integer) As SqlClient.SqlParameter
    Dim param = cm.Parameters.Add("@" & OutputProperty.Name, SqlDbType.VarChar)
    param.Direction = ParameterDirection.InputOutput
    param.Size = Size
    param.Value = GetProperty(OutputProperty)
    Return param
  End Function

End Class
