﻿Imports System.ComponentModel.DataAnnotations
Imports Csla
Imports Singular.DataAnnotations

<Serializable()> _
Public Class OBPagedCriteriaBase(Of CritType As OBPagedCriteriaBase(Of CritType))
  Inherits Singular.Paging.PageCriteria(Of CritType)

  Public Shared InstanceNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.InstanceName, "")
  ''' <summary>
  ''' Gets and sets the Genre value
  ''' </summary>
  <Display(Name:="InstanceName"), InitialDataOnly, AlwaysClean>
  Public Property InstanceName() As String
    Get
      Return ReadProperty(InstanceNameProperty)
    End Get
    Set(ByVal Value As String)
      LoadProperty(InstanceNameProperty, Value)
    End Set
  End Property

End Class
