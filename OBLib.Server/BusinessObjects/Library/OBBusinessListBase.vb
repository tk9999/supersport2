﻿Imports Csla.Serialization
Imports System.Linq

<Serializable()> _
Public Class OBBusinessListBase(Of T As OBBusinessListBase(Of T, C), C As OBBusinessBase(Of C))
  Inherits Singular.SingularBusinessListBase(Of T, C)


  Public Shared Function AccessConnectionString(ByVal Path As String, Optional ByVal Password As String = "") As String
    If Path.ToLower.EndsWith(".accdb") Then
      ' Microsoft.ACE.OLEDB.12.0
      Return "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Path & ";" & IIf(Password = "", "", "Database Password=" & Password)
    Else
      Return "Provider=Microsoft.Jet.OLEDB.4.0;Data source=" & Path & ";Jet OLEDB:Database Password=" & Password
    End If
  End Function

  Public Function CurrentUser() As OBLib.Security.User

    Return OBLib.Security.Settings.CurrentUser

  End Function

  Public Function IDsToXML() As String

    If Me.Count = 0 Then
      Return ""
    Else
      Dim StringList As List(Of String) = Me.Select(Of String)(Function(d) d.GetId.ToString).ToList
      Return OBMisc.StringArrayToXML(StringList.ToArray)
    End If

    'Me.[Select](Function(i) New XElement("ID", i.GetId)))

  End Function

  Public Overridable Sub BulkSave(action As System.Action(Of SqlClient.SqlCommand))
    Me.RaiseListChangedEvents = False
    Dim cn As New SqlClient.SqlConnection(Singular.Settings.ConnectionString)
    Using cn
      cn.Open()
      Using cm As SqlClient.SqlCommand = cn.CreateCommand
        cm.CommandType = CommandType.StoredProcedure
        If action IsNot Nothing Then action(cm)
        Try
          cm.ExecuteNonQuery()
        Finally
          'If cm.Transaction IsNot Nothing Then
          '  cm.Transaction.Rollback()
          'End If
          cn.Close()
          Me.RaiseListChangedEvents = True
        End Try
      End Using
    End Using
  End Sub

End Class
