﻿Imports System.ComponentModel.DataAnnotations
Imports Csla
Imports Singular.DataAnnotations

Public Class OBCriteriaBase(Of CritType As OBCriteriaBase(Of CritType))
  Inherits Singular.SingularCriteriaBase(Of CritType)

  Public Shared InstanceNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.InstanceName, "")
  ''' <summary>
  ''' Gets and sets the Genre value
  ''' </summary>
  <Display(Name:="InstanceName"), InitialDataOnly, AlwaysClean>
  Public Property InstanceName() As String
    Get
      Return ReadProperty(InstanceNameProperty)
    End Get
    Set(ByVal Value As String)
      LoadProperty(InstanceNameProperty, Value)
    End Set
  End Property

End Class
