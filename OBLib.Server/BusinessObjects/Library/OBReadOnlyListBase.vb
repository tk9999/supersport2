﻿Imports Csla.Serialization

<Serializable()> _
Public Class OBReadOnlyListBase(Of T As OBReadOnlyListBase(Of T, C), C As OBReadOnlyBase(Of C))
  Inherits Singular.SingularReadOnlyListBase(Of T, C)

  Public Function CurrentUser() As OBLib.Security.User

    Return OBLib.Security.Settings.CurrentUser

  End Function

End Class
