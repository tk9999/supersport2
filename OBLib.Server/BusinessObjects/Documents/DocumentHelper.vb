﻿Public Class DocumentHelper

  Public Shared Function GetDocumentBytes(DocumentID As Integer) As Byte()

    Dim cProc As New Singular.CommandProc("CmdProcs.cmdGetDocumentBytes")
    cProc.Parameters.AddWithValue("@DocumentID", DocumentID)
    cProc.CommandType = CommandType.StoredProcedure
    cProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
    cProc = cProc.Execute
    Return CType(cProc.DataRow(0), Byte())

  End Function

End Class
