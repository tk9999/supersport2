﻿' Generated 04 Nov 2014 08:48 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace RoomScheduling.Changes

  <Serializable()> _
  Public Class RoomScheduleChange
    Inherits SingularBusinessBase(Of RoomScheduleChange)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RoomScheduleChangeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleChangeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property RoomScheduleChangeID() As Integer
      Get
        Return GetProperty(RoomScheduleChangeIDProperty)
      End Get
    End Property

    Public Shared RoomScheduleChangeTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleChangeTypeID, "Room Schedule Change Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Schedule Change Type value
    ''' </summary>
    <Display(Name:="Room Schedule Change Type", Description:=""),
    Required(ErrorMessage:="Room Schedule Change Type required")>
    Public Property RoomScheduleChangeTypeID() As Integer?
      Get
        Return GetProperty(RoomScheduleChangeTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleChangeTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ChangedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChangedBy, "Changed By", 0)
    ''' <summary>
    ''' Gets and sets the Changed By value
    ''' </summary>
    <Display(Name:="Changed By", Description:=""),
    Required(ErrorMessage:="Changed By required")>
    Public Property ChangedBy() As Integer
      Get
        Return GetProperty(ChangedByProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ChangedByProperty, Value)
      End Set
    End Property

    Public Shared ChangedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ChangedDateTime, "Changed Date Time")
    ''' <summary>
    ''' Gets and sets the Changed Date Time value
    ''' </summary>
    <Display(Name:="Changed Date Time", Description:=""),
    Required(ErrorMessage:="Changed Date Time required")>
    Public Property ChangedDateTime As DateTime?
      Get
        If Not FieldManager.FieldExists(ChangedDateTimeProperty) Then
          LoadProperty(ChangedDateTimeProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(ChangedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ChangedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "Room Schedule", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:=""),
    Required(ErrorMessage:="Room Schedule required")>
    Public Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared OldRoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OldRoomID, "Old Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Old Room value
    ''' </summary>
    <Display(Name:="Old Room", Description:="")>
    Public Property OldRoomID() As Integer?
      Get
        Return GetProperty(OldRoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(OldRoomIDProperty, Value)
      End Set
    End Property

    Public Shared NewRoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NewRoomID, "New Room", Nothing)
    ''' <summary>
    ''' Gets and sets the New Room value
    ''' </summary>
    <Display(Name:="New Room", Description:="")>
    Public Property NewRoomID() As Integer?
      Get
        Return GetProperty(NewRoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NewRoomIDProperty, Value)
      End Set
    End Property

    Public Shared OldStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OldStartDateTime, "Old Start Date Time")
    ''' <summary>
    ''' Gets and sets the Old Start Date Time value
    ''' </summary>
    <Display(Name:="Old Start Date Time", Description:="")>
    Public Property OldStartDateTime As DateTime?
      Get
        Return GetProperty(OldStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OldStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared NewStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.NewStartDateTime, "New Start Date Time")
    ''' <summary>
    ''' Gets and sets the New Start Date Time value
    ''' </summary>
    <Display(Name:="New Start Date Time", Description:="")>
    Public Property NewStartDateTime As DateTime?
      Get
        Return GetProperty(NewStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(NewStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared OldEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OldEndDateTime, "Old End Date Time")
    ''' <summary>
    ''' Gets and sets the Old End Date Time value
    ''' </summary>
    <Display(Name:="Old End Date Time", Description:="")>
    Public Property OldEndDateTime As DateTime?
      Get
        Return GetProperty(OldEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OldEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared NewEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.NewEndDateTime, "New End Date Time")
    ''' <summary>
    ''' Gets and sets the New End Date Time value
    ''' </summary>
    <Display(Name:="New End Date Time", Description:="")>
    Public Property NewEndDateTime As DateTime?
      Get
        Return GetProperty(NewEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(NewEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared OldCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OldComments, "Old Comments", "")
    ''' <summary>
    ''' Gets and sets the Old Comments value
    ''' </summary>
    <Display(Name:="Old Comments", Description:=""),
    StringLength(1024, ErrorMessage:="Old Comments cannot be more than 1024 characters")>
    Public Property OldComments() As String
      Get
        Return GetProperty(OldCommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OldCommentsProperty, Value)
      End Set
    End Property

    Public Shared NewCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NewComments, "New Comments", "")
    ''' <summary>
    ''' Gets and sets the New Comments value
    ''' </summary>
    <Display(Name:="New Comments", Description:=""),
    StringLength(1024, ErrorMessage:="New Comments cannot be more than 1024 characters")>
    Public Property NewComments() As String
      Get
        Return GetProperty(NewCommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NewCommentsProperty, Value)
      End Set
    End Property

    Public Shared ReleasedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ReleasedInd, "Released", False)
    ''' <summary>
    ''' Gets and sets the Released value
    ''' </summary>
    <Display(Name:="Released", Description:="")>
    Public Property ReleasedInd() As Boolean
      Get
        Return GetProperty(ReleasedIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ReleasedIndProperty, Value)
      End Set
    End Property

    Public Shared ReleasedByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ReleasedBy, "Released By", Nothing)
    ''' <summary>
    ''' Gets and sets the Released By value
    ''' </summary>
    <Display(Name:="Released By", Description:="")>
    Public Property ReleasedBy() As Integer?
      Get
        Return GetProperty(ReleasedByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ReleasedByProperty, Value)
      End Set
    End Property

    Public Shared ReleasedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ReleasedDateTime, "Released Date Time")
    ''' <summary>
    ''' Gets and sets the Released Date Time value
    ''' </summary>
    <Display(Name:="Released Date Time", Description:="")>
    Public Property ReleasedDateTime As DateTime?
      Get
        Return GetProperty(ReleasedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ReleasedDateTimeProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleChangeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.OldComments.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Room Schedule Change")
        Else
          Return String.Format("Blank {0}", "Room Schedule Change")
        End If
      Else
        Return Me.OldComments
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomScheduleChange() method.

    End Sub

    Public Shared Function NewRoomScheduleChange() As RoomScheduleChange

      Return DataPortal.CreateChild(Of RoomScheduleChange)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRoomScheduleChange(dr As SafeDataReader) As RoomScheduleChange

      Dim r As New RoomScheduleChange()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RoomScheduleChangeIDProperty, .GetInt32(0))
          LoadProperty(RoomScheduleChangeTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ChangedByProperty, .GetInt32(2))
          LoadProperty(ChangedDateTimeProperty, .GetValue(3))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(OldRoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(NewRoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(OldStartDateTimeProperty, .GetValue(7))
          LoadProperty(NewStartDateTimeProperty, .GetValue(8))
          LoadProperty(OldEndDateTimeProperty, .GetValue(9))
          LoadProperty(NewEndDateTimeProperty, .GetValue(10))
          LoadProperty(OldCommentsProperty, .GetString(11))
          LoadProperty(NewCommentsProperty, .GetString(12))
          LoadProperty(ReleasedIndProperty, .GetBoolean(13))
          LoadProperty(ReleasedByProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(ReleasedDateTimeProperty, .GetValue(15))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insRoomScheduleChange"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updRoomScheduleChange"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramRoomScheduleChangeID As SqlParameter = .Parameters.Add("@RoomScheduleChangeID", SqlDbType.Int)
          paramRoomScheduleChangeID.Value = GetProperty(RoomScheduleChangeIDProperty)
          If Me.IsNew Then
            paramRoomScheduleChangeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@RoomScheduleChangeTypeID", GetProperty(RoomScheduleChangeTypeIDProperty))
          .Parameters.AddWithValue("@ChangedBy", GetProperty(ChangedByProperty))
          cm.Parameters.AddWithValue("@ChangedDateTime", (New SmartDate(GetProperty(ChangedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@RoomScheduleID", GetProperty(RoomScheduleIDProperty))
          .Parameters.AddWithValue("@OldRoomID", Singular.Misc.NothingDBNull(GetProperty(OldRoomIDProperty)))
          .Parameters.AddWithValue("@NewRoomID", Singular.Misc.NothingDBNull(GetProperty(NewRoomIDProperty)))
          cm.Parameters.AddWithValue("@OldStartDateTime", (New SmartDate(GetProperty(OldStartDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@NewStartDateTime", (New SmartDate(GetProperty(NewStartDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@OldEndDateTime", (New SmartDate(GetProperty(OldEndDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@NewEndDateTime", (New SmartDate(GetProperty(NewEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@OldComments", GetProperty(OldCommentsProperty))
          .Parameters.AddWithValue("@NewComments", GetProperty(NewCommentsProperty))
          .Parameters.AddWithValue("@ReleasedInd", Singular.Misc.NothingDBNull(GetProperty(ReleasedIndProperty)))
          .Parameters.AddWithValue("@ReleasedBy", Singular.Misc.NothingDBNull(GetProperty(ReleasedByProperty)))
          cm.Parameters.AddWithValue("@ReleasedDateTime", (New SmartDate(GetProperty(ReleasedDateTimeProperty))).DBValue)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(RoomScheduleChangeIDProperty, paramRoomScheduleChangeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delRoomScheduleChange"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@RoomScheduleChangeID", GetProperty(RoomScheduleChangeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace