﻿' Generated 19 Dec 2015 11:24 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomScheduleInfoDetail
    Inherits OBReadOnlyBase(Of RORoomScheduleInfoDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Production System Area")
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
    Public ReadOnly Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "Sub-Dept")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "Sub-Dept")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="")>
    Public ReadOnly Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaStatusID, "Status")
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatusID() As Integer
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Status")
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
    End Property

    Public Shared PersonnelCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PersonnelCount, "Personnel")
    ''' <summary>
    ''' Gets the Personnel Count value
    ''' </summary>
    <Display(Name:="Personnel", Description:="")>
    Public ReadOnly Property PersonnelCount() As Integer
      Get
        Return GetProperty(PersonnelCountProperty)
      End Get
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartTime, "Start Time")
    ''' <summary>
    ''' Gets the Start Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property StartTime As DateTime?
      Get
        Return GetProperty(StartTimeProperty)
      End Get
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndTime, "End Time")
    ''' <summary>
    ''' Gets the End Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property EndTime As DateTime?
      Get
        Return GetProperty(EndTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.System

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORoomScheduleInfoDetail(dr As SafeDataReader) As RORoomScheduleInfoDetail

      Dim r As New RORoomScheduleInfoDetail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
        LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SystemProperty, .GetString(3))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(ProductionAreaProperty, .GetString(5))
        LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(ProductionAreaStatusProperty, .GetString(7))
        LoadProperty(PersonnelCountProperty, .GetInt32(8))
        LoadProperty(StartTimeProperty, .GetValue(9))
        LoadProperty(EndTimeProperty, .GetValue(10))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace