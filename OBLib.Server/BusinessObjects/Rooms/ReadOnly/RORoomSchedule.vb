﻿' Generated 15 Jan 2015 13:05 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomSchedule
    Inherits SingularReadOnlyBase(Of RORoomSchedule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="The room which the booking is for")>
    Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared RoomScheduleTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleTypeID, "Room Schedule Type", Nothing)
    ''' <summary>
    ''' Gets the Room Schedule Type value
    ''' </summary>
    <Display(Name:="Room Schedule Type", Description:="The type of booking")>
    Public ReadOnly Property RoomScheduleTypeID() As Integer?
      Get
        Return GetProperty(RoomScheduleTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="The production linked to the booking, if linked")>
    Public ReadOnly Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "On Air Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="On Air Time", Description:="The start date and time of the booking"),
    DateField(FormatString:="dd MMM yyyy HH:mm")>
    Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "On Air End Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="On Air End Time", Description:="The end date and time of the booking")>
    Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared AdHocBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AdHocBookingID, "Ad Hoc Booking", Nothing)
    ''' <summary>
    ''' Gets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Ad Hoc Booking", Description:="")>
    Public ReadOnly Property AdHocBookingID() As Integer?
      Get
        Return GetProperty(AdHocBookingIDProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared CallTimeStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CallTimeStart, "Call Time")
    ''' <summary>
    ''' Gets the Call Time Start value
    ''' </summary>
    <Display(Name:="Time", Description:="")>
    Public ReadOnly Property CallTimeStart As DateTime?
      Get
        Return GetProperty(CallTimeStartProperty)
      End Get
    End Property

    Public Shared WrapTimeEndProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.WrapTimeEnd, "Wrap Time End")
    ''' <summary>
    ''' Gets the Wrap Time End value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public ReadOnly Property WrapTimeEnd As DateTime?
      Get
        Return GetProperty(WrapTimeEndProperty)
      End Get
    End Property

    <Display(Name:="Call Time", Description:="")>
    Public ReadOnly Property CallDate As String
      Get
        Return CallTimeStart.Value.ToString("dd MMM yy")
      End Get
    End Property

    <Display(Name:="Start Time", Description:="")>
    Public ReadOnly Property CallTime As String
      Get
        Return CallTimeStart.Value.ToString("HH:mm")
      End Get
    End Property

    <Display(Name:="Wrap Time", Description:="")>
    Public ReadOnly Property WrapTime As String
      Get
        Return WrapTimeEnd.Value.ToString("HH:mm")
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "My Area Status", Nothing)
    ''' <summary>
    ''' Gets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="My Area Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "My Area Status", "")
    ''' <summary>
    ''' Gets the ProductionAreaStatus value
    ''' </summary>
    <Display(Name:="My Area Status", Description:="")>
    Public ReadOnly Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room", "")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared RoomScheduleTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomScheduleType, "Booking Type", "")
    ''' <summary>
    ''' Gets the RoomScheduleType value
    ''' </summary>
    <Display(Name:="Booking Type", Description:="")>
    Public ReadOnly Property RoomScheduleType() As String
      Get
        Return GetProperty(RoomScheduleTypeProperty)
      End Get
    End Property

    Public Shared CssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CssClass, "CssClass", "")
    ''' <summary>
    ''' Gets the CssClass value
    ''' </summary>
    <Display(Name:="CssClass", Description:="")>
    Public ReadOnly Property CssClass() As String
      Get
        Return GetProperty(CssClassProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets the Ad Hoc Booking value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
    Public ReadOnly Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Comments

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORoomSchedule(dr As SafeDataReader) As RORoomSchedule

      Dim r As New RORoomSchedule()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(RoomScheduleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(StartDateTimeProperty, .GetValue(4))
        LoadProperty(EndDateTimeProperty, .GetValue(5))
        LoadProperty(CreatedByProperty, .GetInt32(6))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(7))
        LoadProperty(ModifiedByProperty, .GetInt32(8))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(AdHocBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(CommentsProperty, .GetString(11))
        LoadProperty(CallTimeStartProperty, .GetValue(12))
        LoadProperty(WrapTimeEndProperty, .GetValue(13))
        LoadProperty(TitleProperty, .GetString(14))
        LoadProperty(ProductionAreaStatusIDProperty, .GetInt32(15))
        LoadProperty(ProductionAreaStatusProperty, .GetString(16))
        LoadProperty(RoomProperty, .GetString(17))
        LoadProperty(RoomScheduleTypeProperty, .GetString(18))
        LoadProperty(CssClassProperty, .GetString(19))
        'Row No = 20
        LoadProperty(ProductionSystemAreaIDProperty, ZeroNothing(.GetInt32(21)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace