﻿' Generated 19 Dec 2015 11:24 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomScheduleInfo
    Inherits OBReadOnlyBase(Of RORoomScheduleInfo)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public ReadOnly Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared RoomBookingTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomBookingTitle, "Room Booking Title")
    ''' <summary>
    ''' Gets the Room Booking Title value
    ''' </summary>
    <Display(Name:="Room Booking Title", Description:="")>
  Public ReadOnly Property RoomBookingTitle() As String
      Get
        Return GetProperty(RoomBookingTitleProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    <Display(Name:="Area Count", Description:="")>
    Public ReadOnly Property AreaCount() As Integer
      Get
        Return RORoomScheduleInfoDetailList.Count
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared RORoomScheduleInfoDetailListProperty As PropertyInfo(Of RORoomScheduleInfoDetailList) = RegisterProperty(Of RORoomScheduleInfoDetailList)(Function(c) c.RORoomScheduleInfoDetailList, "RO Room Schedule Info Detail List")

    Public ReadOnly Property RORoomScheduleInfoDetailList() As RORoomScheduleInfoDetailList
      Get
        If GetProperty(RORoomScheduleInfoDetailListProperty) Is Nothing Then
          LoadProperty(RORoomScheduleInfoDetailListProperty, Rooms.ReadOnly.RORoomScheduleInfoDetailList.NewRORoomScheduleInfoDetailList())
        End If
        Return GetProperty(RORoomScheduleInfoDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.RoomBookingTitle

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRORoomScheduleInfo(dr As SafeDataReader) As RORoomScheduleInfo

      Dim r As New RORoomScheduleInfo()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
        LoadProperty(RoomBookingTitleProperty, .GetString(1))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(RoomProperty, .GetString(3))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace