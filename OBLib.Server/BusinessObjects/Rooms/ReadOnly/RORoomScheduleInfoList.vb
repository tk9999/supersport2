﻿' Generated 19 Dec 2015 11:23 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomScheduleInfoList
    Inherits OBReadOnlyListBase(Of RORoomScheduleInfoList, RORoomScheduleInfo)

#Region " Business Methods "

    Public Function GetItem(RoomScheduleID As Integer) As RORoomScheduleInfo

      For Each child As RORoomScheduleInfo In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetRORoomScheduleInfoDetail(RoomScheduleID As Integer) As RORoomScheduleInfoDetail

      Dim obj As RORoomScheduleInfoDetail = Nothing
      For Each parent As RORoomScheduleInfo In Me
        obj = parent.RORoomScheduleInfoDetailList.GetItem(RoomScheduleID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property RoomScheduleID As Integer? = Nothing

      Public Sub New(RoomScheduleID As Integer?)
        Me.RoomScheduleID = RoomScheduleID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORoomScheduleInfoList() As RORoomScheduleInfoList

      Return New RORoomScheduleInfoList()

    End Function

    'Public Shared Sub BeginGetRORoomScheduleInfoList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORoomScheduleInfoList)))

    '  Dim dp As New DataPortal(Of RORoomScheduleInfoList)()
    '  AddHandler dp.FetchCompleted, CallBack
    '  dp.BeginFetch(criteria)

    'End Sub


    'Public Shared Sub BeginGetRORoomScheduleInfoList(CallBack As EventHandler(Of DataPortalResult(Of RORoomScheduleInfoList)))

    '  Dim dp As New DataPortal(Of RORoomScheduleInfoList)()
    '  AddHandler dp.FetchCompleted, CallBack
    '  dp.BeginFetch(New Criteria())

    'End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORoomScheduleInfoList(RoomScheduleID As Integer?) As RORoomScheduleInfoList

      Return DataPortal.Fetch(Of RORoomScheduleInfoList)(New Criteria(RoomScheduleID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORoomScheduleInfo.GetRORoomScheduleInfo(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As RORoomScheduleInfo = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.RoomScheduleID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.RORoomScheduleInfoDetailList.RaiseListChangedEvents = False
          parent.RORoomScheduleInfoDetailList.Add(RORoomScheduleInfoDetail.GetRORoomScheduleInfoDetail(sdr))
          parent.RORoomScheduleInfoDetailList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORoomScheduleInfoList"
            cm.Parameters.AddWithValue("@RoomScheduleID", NothingDBNull(crit.RoomScheduleID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace