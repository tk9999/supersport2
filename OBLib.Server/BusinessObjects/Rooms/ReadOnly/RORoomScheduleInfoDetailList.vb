﻿' Generated 19 Dec 2015 11:24 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomScheduleInfoDetailList
    Inherits OBReadOnlyListBase(Of RORoomScheduleInfoDetailList, RORoomScheduleInfoDetail)

#Region " Parent "

    <NotUndoable()> Private mParent As RORoomScheduleInfo
#End Region

#Region " Business Methods "

    Public Function GetItem(RoomScheduleID As Integer) As RORoomScheduleInfoDetail

      For Each child As RORoomScheduleInfoDetail In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewRORoomScheduleInfoDetailList() As RORoomScheduleInfoDetailList

      Return New RORoomScheduleInfoDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace