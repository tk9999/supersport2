﻿' Generated 15 Jan 2015 13:04 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms.ReadOnly

  <Serializable()> _
  Public Class RORoomScheduleList
    Inherits SingularReadOnlyListBase(Of RORoomScheduleList, RORoomSchedule)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

    'Private mTotalPages As Integer = 0
    'Public ReadOnly Property TotalPages As Integer
    '  Get
    '    Return mTotalPages
    '  End Get
    'End Property

#Region " Business Methods "

    Public Function GetItem(RoomScheduleID As Integer) As RORoomSchedule

      For Each child As RORoomSchedule In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Room Schedules"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared RoomIDsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomIDs, "Sort Column", "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Sort Column", Description:="")>
      Public Property RoomIDs() As String
        Get
          Return ReadProperty(RoomIDsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(RoomIDsProperty, Value)
        End Set
      End Property

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Start Date", Description:=""),
      Required(ErrorMessage:="Start Date required"),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property StartDate As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate)
      ''' <summary>
      ''' Gets and sets the End Date value
      ''' </summary>
      <Display(Name:="End Date", Description:=""),
      Required(ErrorMessage:="End Date required"),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property EndDate As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
      ''' <summary>
      ''' Gets and sets the System value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""),
      Required(ErrorMessage:="Sub-Dept required"),
      DropDownWeb(GetType(ROSystemList))>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      ''' <summary>
      ''' Gets and sets the Production Area value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area required"),
      DropDownWeb(GetType(ROProductionAreaList))>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared SystemNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemName, "System Name", "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="System Name", Description:="")>
      Public Property SystemName() As String
        Get
          Return ReadProperty(SystemNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SystemNameProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaName, "Area Name", "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Area Name", Description:="")>
      Public Property ProductionAreaName() As String
        Get
          Return ReadProperty(ProductionAreaNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionAreaNameProperty, Value)
        End Set
      End Property

      Public Shared RoomScheduleIDsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomScheduleIDs, "Sort Column", "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Sort Column", Description:="")>
      Public Property RoomScheduleIDs() As String
        Get
          Return ReadProperty(RoomScheduleIDsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(RoomScheduleIDsProperty, Value)
        End Set
      End Property

      'Public Shared PageNoProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PageNo, "Page No", 1)
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="Page No", Description:="")>
      'Public Property PageNo() As Integer?
      '  Get
      '    Return ReadProperty(PageNoProperty)
      '  End Get
      '  Set(ByVal Value As Integer?)
      '    LoadProperty(PageNoProperty, Value)
      '  End Set
      'End Property

      'Public Shared PageSizeProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PageSize, "Page No", 20)
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="Page No", Description:="")>
      'Public Property PageSize() As Integer?
      '  Get
      '    Return ReadProperty(PageSizeProperty)
      '  End Get
      '  Set(ByVal Value As Integer?)
      '    LoadProperty(PageSizeProperty, Value)
      '  End Set
      'End Property

      'Public Shared SortAscProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SortAsc, "Sort Asc?", True)
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="Sort Asc?", Description:="")>
      'Public Property SortAsc() As Boolean
      '  Get
      '    Return ReadProperty(SortAscProperty)
      '  End Get
      '  Set(ByVal Value As Boolean)
      '    LoadProperty(SortAscProperty, Value)
      '  End Set
      'End Property

      'Public Shared SortColumnProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SortColumn, "Sort Column", "")
      ' ''' <summary>
      ' ''' Gets and sets the Genre value
      ' ''' </summary>
      '<Display(Name:="Sort Column", Description:="")>
      'Public Property SortColumn() As String
      '  Get
      '    Return ReadProperty(SortColumnProperty)
      '  End Get
      '  Set(ByVal Value As String)
      '    LoadProperty(SortColumnProperty, Value)
      '  End Set
      'End Property

      Public Sub New(RoomIDs As String, StartDate As DateTime?, EndDate As DateTime?,
                     SystemID As Integer?, ProductionAreaID As Integer?,
                     RoomScheduleIDs As String,
                     PageNo As Integer, PageSize As Integer,
                     SortAsc As Boolean, SortColumn As String)
        Me.RoomIDs = RoomIDs
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.RoomScheduleIDs = RoomScheduleIDs
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRORoomScheduleList() As RORoomScheduleList

      Return New RORoomScheduleList()

    End Function

    Public Shared Sub BeginGetRORoomScheduleList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RORoomScheduleList)))

      Dim dp As New DataPortal(Of RORoomScheduleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    Public Shared Sub BeginGetRORoomScheduleList(CallBack As EventHandler(Of DataPortalResult(Of RORoomScheduleList)))

      Dim dp As New DataPortal(Of RORoomScheduleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRORoomScheduleList() As RORoomScheduleList

      Return DataPortal.Fetch(Of RORoomScheduleList)(New Criteria())

    End Function

    Public Shared Function GetRORoomScheduleList(RoomIDs As String, StartDate As DateTime?, EndDate As DateTime?,
                                                 SystemID As Integer?, ProductionAreaID As Integer?,
                                                 RoomScheduleIDs As String,
                                                         PageNo As Integer, PageSize As Integer,
                                                         SortAsc As Boolean, SortColumn As String) As RORoomScheduleList

      Return DataPortal.Fetch(Of RORoomScheduleList)(New Criteria(RoomIDs, StartDate, EndDate,
                                                                  SystemID, ProductionAreaID,
                                                                  RoomScheduleIDs,
                                                                          PageNo, PageSize,
                                                                          SortAsc, SortColumn))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RORoomSchedule.GetRORoomSchedule(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORoomScheduleList"
            cm.Parameters.AddWithValue("@RoomIDs", Strings.MakeEmptyDBNull(crit.RoomIDs))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@RoomScheduleIDs", Strings.MakeEmptyDBNull(crit.RoomScheduleIDs))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace