﻿' Generated 27 Jan 2016 09:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms

  <Serializable()> _
  Public Class RoomAllocatorRoomSchedule
    Inherits OBBusinessBase(Of RoomAllocatorRoomSchedule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RoomAllocatorRoomScheduleBO.GetToString(self)")

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "RoomScheduleID", 0)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="RoomScheduleID", Description:="")>
    Public Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref")
    ''' <summary>
    ''' Gets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:="")>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared GenreSeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreSeries, "Genre (Series)")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Genre (Series)")>
    Public Property GenreSeries() As String
      Get
        Return GetProperty(GenreSeriesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreSeriesProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production", 0)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ResourceBookingID", 0)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="ResourceBookingID", Description:="")>
    Public Property ResourceBookingID() As Integer?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.RoomID, Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room"),
     DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="RoomAllocatorRoomScheduleBO.setRoomIDCriteriaBeforeRefresh",
                 PreFindJSFunction:="RoomAllocatorRoomScheduleBO.triggerRoomIDAutoPopulate",
                 AfterFetchJS:="RoomAllocatorRoomScheduleBO.afterRoomIDRefreshAjax",
                 LookupMember:="Room", ValueMember:="RoomID",
                 DropDownColumns:={"Room", "IsAvailableString"},
                 OnItemSelectJSFunction:="RoomAllocatorRoomScheduleBO.onRoomSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("RoomAllocatorRoomScheduleBO.RoomIDSet(self)"),
    Required(ErrorMessage:="Room is required")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Room")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CallTime, "Call Time")
    ''' <summary>
    ''' Gets and sets the Call Time value
    ''' </summary>
    <Display(Name:="Call Time"),
    SetExpression("RoomAllocatorRoomScheduleBO.CallTimeSet(self)"),
    Required(ErrorMessage:="Call Time is required")>
    Public Property CallTime As DateTime?
      Get
        Return GetProperty(CallTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Event Start")
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time is required"),
    SetExpression("RoomAllocatorRoomScheduleBO.StartDateTimeSet(self)")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "Event End")
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="End Time"),
    Required(ErrorMessage:="End Time is required"),
    SetExpression("RoomAllocatorRoomScheduleBO.EndDateTimeSet(self)")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.WrapTime, "Wrap Time")
    ''' <summary>
    ''' Gets and sets the Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time"),
    SetExpression("RoomAllocatorRoomScheduleBO.WrapTimeSet(self)"),
    Required(ErrorMessage:="Wrap Time is required")>
    Public Property WrapTime As DateTime?
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept is required"),
    DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area is required"),
    DropDownWeb(GetType(ROUserSystemAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", ThisFilterMember:="SystemID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Area", 0)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Area")>
    Public Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared RoomClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomClashCount, "Room", 0)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Room")>
    Public Property RoomClashCount() As Integer
      Get
        Return GetProperty(RoomClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomClashCountProperty, Value)
      End Set
    End Property

    Public Shared ScheduleNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ScheduleNumber, "Schedule Number")
    ''' <summary>
    ''' Gets and sets the Schedule Number value
    ''' </summary>
    <Display(Name:="Schedule Number", Description:="")>
    Public Property ScheduleNumber() As Integer
      Get
        Return GetProperty(ScheduleNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ScheduleNumberProperty, Value)
      End Set
    End Property

    Public Shared OwnerCommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OwnerComments, "Special Instructions")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Special Instructions", Description:="")>
    Public Property OwnerComments() As String
      Get
        Return GetProperty(OwnerCommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(OwnerCommentsProperty, Value)
      End Set
    End Property

    Public Shared DeleteRoomScheduleProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DeleteRoomSchedule, "Delete RoomSchedule", False)
    ''' <summary>
    ''' Gets and sets the Schedule Number value
    ''' </summary>
    <Display(Name:="Delete RoomSchedule", Description:="")>
    Public Property DeleteRoomSchedule() As Boolean
      Get
        Return GetProperty(DeleteRoomScheduleProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DeleteRoomScheduleProperty, Value)
      End Set
    End Property

    Public Shared BookingTimesMatchScheduledTimesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.BookingTimesMatchScheduledTimes, "BookingTimesMatchScheduledTimes")
    ''' <summary>
    ''' Gets the Booking Count value
    ''' </summary>
    <Display(Name:="BookingTimesMatchScheduledTimes", Description:="")>
    Public Property BookingTimesMatchScheduledTimes() As Boolean
      Get
        Return GetProperty(BookingTimesMatchScheduledTimesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(BookingTimesMatchScheduledTimesProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      'If Me.EventStatus.Length = 0 Then
      '  If Me.IsNew Then
      '    Return String.Format("New {0}", "Room Allocator")
      '  Else
      '    Return String.Format("Blank {0}", "Room Allocator")
      '  End If
      'Else
      '  Return Me.EventStatus
      'End If
      Return ""

    End Function

    Public Function AllocateRoom() As Singular.Web.Result

      'Dim cmd As New Singular.CommandProc("CmdProcs.cmdResourceSchedulerAllocateRoom",
      '                                   {"@RoomID",
      '                                    "@CallTime",
      '                                    "@OnAirTimeStart",
      '                                    "@OnAirTimeEnd",
      '                                    "@WrapTime",
      '                                    "@GenreSeries",
      '                                    "@Title",
      '                                    "@ModifiedBy",
      '                                    "@SystemID",
      '                                    "@ProductionAreaID",
      '                                    "@ProductionID",
      '                                    "@ImportedEventID",
      '                                    "@GenRefNumber",
      '                                    "@SuppressFeedback"
      '                                   },
      '                                   {NothingDBNull(RoomID),
      '                                    NothingDBNull(CallTime),
      '                                    NothingDBNull(OnAirStartDateTime),
      '                                    NothingDBNull(OnAirEndDateTime),
      '                                    NothingDBNull(WrapTime),
      '                                    GenreSeries,
      '                                    Title,
      '                                    OBLib.Security.Settings.CurrentUserID,
      '                                    NothingDBNull(SystemID),
      '                                    NothingDBNull(ProductionAreaID),
      '                                    NothingDBNull(ProductionID),
      '                                    NothingDBNull(ImportedEventID),
      '                                    NothingDBNull(GenRefNumber),
      '                                    False})

      'cmd.FetchType = CommandProc.FetchTypes.DataSet
      'cmd = cmd.Execute

      ''Header Results
      'Dim headerRow = cmd.Dataset.Tables(0).Rows(0)
      'Dim Success As Boolean = False
      'Dim HasClashes As Boolean = False
      'Dim RoomScheduleAdded As Boolean = False
      'Dim AreaAdded As Boolean = False
      'Dim BookingAdded As Boolean = False

      'Success = headerRow(0)
      'HasClashes = headerRow(1)
      'RoomScheduleAdded = headerRow(2)
      'AreaAdded = headerRow(3)
      'BookingAdded = headerRow(4)

      'If Success Then
      '  Return New Singular.Web.Result(True)
      'Else
      '  If HasClashes Then
      '    Return New Singular.Web.Result(False) With {.ErrorText = "Clashes were found"}
      '  ElseIf RoomScheduleAdded Then
      '    Return New Singular.Web.Result(False) With {.ErrorText = "Room Booking could not be created"}
      '  ElseIf AreaAdded Then
      '    Return New Singular.Web.Result(False) With {.ErrorText = "Area could not be added"}
      '  ElseIf BookingAdded Then
      '    Return New Singular.Web.Result(False) With {.ErrorText = "Resource Booking could not be created"}
      '  End If
      'End If

      'Return New Singular.Web.Result(False) With {.ErrorText = "Error could not be determined"}

      ''"@ImportedEventChannelIDs" ,
      ''"@ResourceIDSCCROperator",
      ''"@ResourceIDMCRController",
      ''"@HumanResourceShiftIDMCRController",
      ''"@HumanResourceShiftIDSCCROperator",
      ''"@ImportedEventChannelID"

      ''NothingDBNull(Nothing),
      ''NothingDBNull(Nothing),
      ''NothingDBNull(Nothing),
      ''NothingDBNull(Nothing),
      ''NothingDBNull(Nothing),
      ''NothingDBNull(Nothing)

    End Function

    Public Function DoSetup() As Singular.Web.Result

      Dim cmd As New Singular.CommandProc("cmdProcs.cmdRoomAllocatorSetupRoomSchedule",
                            New String() {"@GenRefNumber", "@GenreSeries", "@Title", "@RoomID", "@StartDateTime", "@EndDateTime", "@SystemID", "@ProductionAreaID", "@CurrentUserID", "@ScheduleNumber"},
                            New Object() {Me.GenRefNumber, Me.GenreSeries, Me.Title, Me.RoomID, Me.StartDateTime, Me.EndDateTime, Me.SystemID, Me.ProductionAreaID, OBLib.Security.Settings.CurrentUserID, Me.ScheduleNumber})
      cmd.FetchType = CommandProc.FetchTypes.DataRow
      cmd.UseTransaction = True

      Try
        cmd = cmd.Execute

        Dim resultsRow = cmd.DataRow
        Dim CallTime As DateTime? = resultsRow(0)
        Dim WrapTime As DateTime? = resultsRow(1)
        Dim Title As String = resultsRow(2)
        Dim ClashCount As Integer = resultsRow(3)
        Dim ProductionID As Integer? = resultsRow(4)

        Me.CallTime = CallTime
        Me.WrapTime = WrapTime
        Me.Title = Title
        Me.RoomClashCount = ClashCount
        Me.ProductionID = ProductionID

        Return New Singular.Web.Result(True)
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

    End Function

    Public Sub SetChannels(channels As OBLib.Rooms.RoomAllocatorRoomScheduleChannelList)
      SetProperty(RoomAllocatorRoomScheduleChannelListProperty, channels)
    End Sub

#End Region

#Region " Child Lists "

    Public Shared RoomAllocatorRoomScheduleChannelListProperty As PropertyInfo(Of Rooms.RoomAllocatorRoomScheduleChannelList) = RegisterProperty(Of Rooms.RoomAllocatorRoomScheduleChannelList)(Function(c) c.RoomAllocatorRoomScheduleChannelList, "RoomAllocatorRoomScheduleChannelList")
    Public ReadOnly Property RoomAllocatorRoomScheduleChannelList() As Rooms.RoomAllocatorRoomScheduleChannelList
      Get
        If GetProperty(RoomAllocatorRoomScheduleChannelListProperty) Is Nothing Then
          LoadProperty(RoomAllocatorRoomScheduleChannelListProperty, Rooms.RoomAllocatorRoomScheduleChannelList.NewRoomAllocatorRoomScheduleChannelList())
        End If
        Return GetProperty(RoomAllocatorRoomScheduleChannelListProperty)
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(RoomIDProperty)
        .AddTriggerProperties({CallTimeProperty, StartDateTimeProperty, EndDateTimeProperty, WrapTimeProperty})
        .JavascriptRuleFunctionName = "RoomAllocatorRoomScheduleBO.RoomIDValid"
        .AffectedProperties.AddRange({CallTimeProperty, StartDateTimeProperty, EndDateTimeProperty, WrapTimeProperty})
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomAllocatorRoomSchedule() method.

    End Sub

    Public Shared Function NewRoomAllocatorRoomSchedule() As RoomAllocatorRoomSchedule

      Return DataPortal.CreateChild(Of RoomAllocatorRoomSchedule)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRoomAllocatorRoomSchedule(dr As SafeDataReader) As RoomAllocatorRoomSchedule

      Dim r As New RoomAllocatorRoomSchedule()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(GenRefNumberProperty, .GetInt64(0))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionSystemAreaIDProperty, ZeroNothing(.GetInt32(3)))
          LoadProperty(TitleProperty, .GetString(4))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(RoomProperty, .GetString(6))
          LoadProperty(CallTimeProperty, .GetValue(7))
          LoadProperty(StartDateTimeProperty, .GetValue(8))
          LoadProperty(EndDateTimeProperty, .GetValue(9))
          LoadProperty(WrapTimeProperty, .GetValue(10))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(11)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(12)))
          LoadProperty(ScheduleNumberProperty, .GetInt32(13))
          LoadProperty(OwnerCommentsProperty, .GetString(14))
          LoadProperty(BookingTimesMatchScheduledTimesProperty, .GetBoolean(15))
        End With
      End Using
      'LoadOnAir()
      'LoadBookingTimes()

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insRoomAllocatorRoomSchedule"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updRoomAllocatorRoomSchedule"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          AddInputOutputParam(cm, RoomScheduleIDProperty)
          AddInputOutputParam(cm, ResourceBookingIDProperty)
          AddInputOutputParam(cm, ProductionSystemAreaIDProperty)
          AddInputOutputParam(cm, ProductionIDProperty)


          .Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
          .Parameters.AddWithValue("@Room", GetProperty(RoomProperty))
          .Parameters.AddWithValue("@CallTime", (New SmartDate(GetProperty(CallTimeProperty))).DBValue)
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@WrapTime", (New SmartDate(GetProperty(WrapTimeProperty))).DBValue)
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          '.Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
          .Parameters.AddWithValue("@OwnerComments", GetProperty(OwnerCommentsProperty))
          .Parameters.AddWithValue("@ScheduleNumber", GetProperty(ScheduleNumberProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .ExecuteNonQuery()

          Dim rsID As Object = cm.Parameters("@RoomScheduleID").Value
          Dim rbID As Object = cm.Parameters("@ResourceBookingID").Value
          Dim psaID As Object = cm.Parameters("@ProductionSystemAreaID").Value
          Dim pID As Object = cm.Parameters("@ProductionID").Value

          LoadProperty(RoomScheduleIDProperty, IIf(IsDBNull(rsID), 0, rsID))
          LoadProperty(ResourceBookingIDProperty, IIf(IsDBNull(rbID), 0, rbID))
          LoadProperty(ProductionSystemAreaIDProperty, IIf(IsDBNull(psaID), 0, psaID))
          LoadProperty(ProductionIDProperty, IIf(IsDBNull(pID), 0, pID))
          SaveChildren()
          MarkOld()
        End With
      Else
        SaveChildren()
      End If

    End Sub

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(RoomAllocatorRoomScheduleChannelListProperty))

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delRoomAllocatorRoomSchedule"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ImportedEventChannelID", GetProperty(RoomScheduleIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace