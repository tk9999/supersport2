﻿' Generated 14 Sep 2016 16:37 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class RoomAllocatorRoomScheduleChannelList
    Inherits OBBusinessListBase(Of RoomAllocatorRoomScheduleChannelList, RoomAllocatorRoomScheduleChannel)

#Region " Business Methods "

    Public Function GetItem(ScheduleNumber As Integer) As RoomAllocatorRoomScheduleChannel

      For Each child As RoomAllocatorRoomScheduleChannel In Me
        If child.ScheduleNumber = ScheduleNumber Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property GenRefNumber As Int64?
      Public Property RoomScheduleID As Integer?
      Public Property ScheduleNumber As Integer?

      Public Sub New(GenRefNumber As Int64?, RoomScheduleID As Integer?, ScheduleNumber As Integer?)
        Me.GenRefNumber = GenRefNumber
        Me.RoomScheduleID = RoomScheduleID
        Me.ScheduleNumber = ScheduleNumber
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewRoomAllocatorRoomScheduleChannelList() As RoomAllocatorRoomScheduleChannelList

      Return New RoomAllocatorRoomScheduleChannelList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetRoomAllocatorRoomScheduleChannelList() As RoomAllocatorRoomScheduleChannelList

      Return DataPortal.Fetch(Of RoomAllocatorRoomScheduleChannelList)(New Criteria())

    End Function

    Public Shared Function GetRoomAllocatorRoomScheduleChannelList(GenRefNumber As Int64?, RoomScheduleID As Integer?, ScheduleNumber As Integer?) As RoomAllocatorRoomScheduleChannelList

      Return DataPortal.Fetch(Of RoomAllocatorRoomScheduleChannelList)(New Criteria(GenRefNumber, RoomScheduleID, ScheduleNumber))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(RoomAllocatorRoomScheduleChannel.GetRoomAllocatorRoomScheduleChannel(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRoomAllocatorRoomScheduleChannelList"
            cm.Parameters.AddWithValue("@GenRefNumber", NothingDBNull(crit.GenRefNumber))
            cm.Parameters.AddWithValue("@RoomScheduleID", NothingDBNull(crit.RoomScheduleID))
            cm.Parameters.AddWithValue("@ScheduleNumber", NothingDBNull(crit.ScheduleNumber))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace