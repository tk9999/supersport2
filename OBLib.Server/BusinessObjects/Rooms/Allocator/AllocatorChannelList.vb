﻿' Generated 15 Mar 2016 10:56 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms

  <Serializable()> _
  Public Class AllocatorChannelList
    Inherits OBBusinessListBase(Of AllocatorChannelList, AllocatorChannel)

#Region " Business Methods "

    Public Function GetItem(ChannelID As Integer) As AllocatorChannel

      For Each child As AllocatorChannel In Me
        If child.ChannelID = ChannelID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetAllocatorChannelEvent(ImportedEventChannelID As Integer) As AllocatorChannelEvent

      Dim obj As AllocatorChannelEvent = Nothing
      For Each parent As AllocatorChannel In Me
        obj = parent.AllocatorChannelEventList.GetItem(ImportedEventChannelID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Singular.DataAnnotations.DropDownWeb(GetType(ROUserSystemList),
                                            DisplayMember:="System", ValueMember:="SystemID"),
      Display(Name:="Sub-Dept"),
      Required(ErrorMessage:="Sub-Dept is required"),
      SetExpression("AllocatorChannelListCriteriaBO.SystemIDSet(self)")>
      Public Property SystemID As Integer?

      <Singular.DataAnnotations.DropDownWeb(GetType(ROUserSystemAreaList),
                                            DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", Source:=Singular.DataAnnotations.DropDownWeb.SourceType.CommonData,
                                            ThisFilterMember:="SystemID"),
      Display(Name:="Area"),
      Required(ErrorMessage:="Area is required"),
      SetExpression("AllocatorChannelListCriteriaBO.ProductionAreaIDSet(self)")>
      Public Property ProductionAreaID As Integer?

      <Required(ErrorMessage:="Start Date is required"),
      SetExpression("AllocatorChannelListCriteriaBO.StartDateSet(self)", , )>
      Public Property StartDate As DateTime?

      <Required(ErrorMessage:="End Date is required"),
      SetExpression("AllocatorChannelListCriteriaBO.EndDateSet(self)", , )>
      Public Property EndDate As DateTime?

      Public Shared LiveProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Live, "Live", True)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Live", Description:=""),
      SetExpression("AllocatorChannelListCriteriaBO.LiveSet(self)")>
      Public Property Live() As Boolean
        Get
          Return ReadProperty(LiveProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(LiveProperty, Value)
        End Set
      End Property

      Public Shared DelayedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Delayed, "Delayed", True)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Delayed", Description:=""),
      SetExpression("AllocatorChannelListCriteriaBO.DelayedSet(self)")>
      Public Property Delayed() As Boolean
        Get
          Return ReadProperty(DelayedProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(DelayedProperty, Value)
        End Set
      End Property

      Public Shared PremierProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Premier, "Premier", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Premier", Description:=""),
      SetExpression("AllocatorChannelListCriteriaBO.PremierSet(self)")>
      Public Property Premier() As Boolean
        Get
          Return ReadProperty(PremierProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(PremierProperty, Value)
        End Set
      End Property

      Public Shared GenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNo, "Gen Ref No", Nothing)
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Gen Ref No", Description:=""),
      SetExpression("AllocatorChannelListCriteriaBO.GenRefNoSet(self)", , 250)>
      Public Property GenRefNo() As Int64?
        Get
          Return ReadProperty(GenRefNoProperty)
        End Get
        Set(ByVal Value As Int64?)
          LoadProperty(GenRefNoProperty, Value)
        End Set
      End Property

      Public Shared GenRefNoStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenRefNoString, "Gen Ref No", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Gen Ref No", Description:=""),
      TextField(False, , , ),
      SetExpression("AllocatorChannelListCriteriaBO.GenRefNoStringSet(self)", , 250)>
      Public Property GenRefNoString() As String
        Get
          Return ReadProperty(GenRefNoStringProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(GenRefNoStringProperty, Value)
        End Set
      End Property

      Public Shared PrimaryChannelsOnlyProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryChannelsOnly, "Primary Only", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Primary Channels Only?"),
      SetExpression("AllocatorChannelListCriteriaBO.PrimaryChannelsOnlySet(self)", , )>
      Public Property PrimaryChannelsOnly() As Boolean
        Get
          Return ReadProperty(PrimaryChannelsOnlyProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(PrimaryChannelsOnlyProperty, Value)
        End Set
      End Property

      Public Shared AllChannelsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AllChannels, "All Channels", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="All Channels"),
      SetExpression("AllocatorChannelListCriteriaBO.AllChannelsSet(self)", , )>
      Public Property AllChannels() As Boolean
        Get
          Return ReadProperty(AllChannelsProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(AllChannelsProperty, Value)
        End Set
      End Property

      Public Shared SelectedChannelsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedChannelsDisplay, "Selected Channels", "Select Channels")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Channels")>
      Public Property SelectedChannelsDisplay() As String
        Get
          Return ReadProperty(SelectedChannelsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SelectedChannelsProperty, Value)
        End Set
      End Property

      Public Shared SelectedChannelIDsXMLProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedChannelIDsXML, "Selected Channels", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Channels")>
      Public Property SelectedChannelIDsXML() As String
        Get
          Return ReadProperty(SelectedChannelIDsXMLProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SelectedChannelIDsXMLProperty, Value)
        End Set
      End Property

      Public Shared GenreProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Genre, "Genre", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Genre"),
      TextField(False, , , ),
      SetExpression("AllocatorChannelListCriteriaBO.GenreSet(self)", , 250)>
      Public Property Genre() As String
        Get
          Return ReadProperty(GenreProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(GenreProperty, Value)
        End Set
      End Property

      Public Shared SeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Series, "Series", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Series"),
      TextField(False, , , ),
      SetExpression("AllocatorChannelListCriteriaBO.SeriesSet(self)", , 250)>
      Public Property Series() As String
        Get
          Return ReadProperty(SeriesProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SeriesProperty, Value)
        End Set
      End Property

      Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Title"),
      TextField(False, , , ),
      SetExpression("AllocatorChannelListCriteriaBO.TitleSet(self)", , 250)>
      Public Property Title() As String
        Get
          Return ReadProperty(TitleProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(TitleProperty, Value)
        End Set
      End Property

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Keyword, "Keyword", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Keyword"),
      TextField(False, , , ),
      SetExpression("AllocatorChannelListCriteriaBO.KeywordSet(self)", , 250)>
      Public Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewAllocatorChannelList() As AllocatorChannelList

      Return New AllocatorChannelList()

    End Function

    Public Shared Sub BeginGetAllocatorChannelList(CallBack As EventHandler(Of DataPortalResult(Of AllocatorChannelList)))

      Dim dp As New DataPortal(Of AllocatorChannelList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetAllocatorChannelList() As AllocatorChannelList

      Return DataPortal.Fetch(Of AllocatorChannelList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AllocatorChannel.GetAllocatorChannel(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As AllocatorChannel = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ChannelID <> sdr.GetInt32(5) Then
            parent = Me.GetItem(sdr.GetInt32(5))
          End If
          parent.AllocatorChannelEventList.RaiseListChangedEvents = False
          parent.AllocatorChannelEventList.Add(AllocatorChannelEvent.GetAllocatorChannelEvent(sdr))
          parent.AllocatorChannelEventList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As AllocatorChannel In Me
        child.CheckRules()
        For Each AllocatorChannelEvent As AllocatorChannelEvent In child.AllocatorChannelEventList
          AllocatorChannelEvent.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getRoomAllocatorListByChannelGraphical]"
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@GenRefNo", NothingDBNull(crit.GenRefNo))
            cm.Parameters.AddWithValue("@CurrentUserID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
            cm.Parameters.AddWithValue("@SelectedChannelIDs", Strings.MakeEmptyDBNull(crit.SelectedChannelIDsXML))
            cm.Parameters.AddWithValue("@Genre", Strings.MakeEmptyDBNull(crit.Genre))
            cm.Parameters.AddWithValue("@Series", Strings.MakeEmptyDBNull(crit.Series))
            cm.Parameters.AddWithValue("@Title", Strings.MakeEmptyDBNull(crit.Title))
            cm.Parameters.AddWithValue("@Live", crit.Live)
            cm.Parameters.AddWithValue("@Delayed", crit.Delayed)
            cm.Parameters.AddWithValue("@Premier", crit.Premier)
            cm.Parameters.AddWithValue("@GenRefNoString", Strings.MakeEmptyDBNull(crit.GenRefNoString))
            cm.Parameters.AddWithValue("@Keyword", Strings.MakeEmptyDBNull(crit.Keyword))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AllocatorChannel In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AllocatorChannel In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace