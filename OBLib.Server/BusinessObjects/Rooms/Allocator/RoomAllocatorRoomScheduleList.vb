﻿' Generated 27 Jan 2016 09:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly
Imports Singular.DataAnnotations
Imports Singular.Web.Data

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms

  <Serializable()> _
  Public Class RoomAllocatorRoomScheduleList
    Inherits OBBusinessListBase(Of RoomAllocatorRoomScheduleList, RoomAllocatorRoomSchedule)

#Region " Business Methods "

    Public Function GetItem(RoomScheduleID As Integer) As RoomAllocatorRoomSchedule

      For Each child As RoomAllocatorRoomSchedule In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Singular.Paging.PageCriteria(Of Criteria)

      <Display(Name:="Sub-Dept")>
      Public Property RoomScheduleID As Integer?

      <Display(Name:="Area")>
      Public Property ProductionSystemAreaID As Integer?

      <Display(Name:="Area")>
      Public Property ScheduleNumber As Integer?

      '<Required(ErrorMessage:="Start Date is required")>
      'Public Property StartDate As DateTime?

      '<Required(ErrorMessage:="End Date is required")>
      'Public Property EndDate As DateTime?

      'Public Shared LiveProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Live, "Live", True)
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="Live", Description:="")>
      'Public Property Live() As Boolean
      '  Get
      '    Return ReadProperty(LiveProperty)
      '  End Get
      '  Set(ByVal Value As Boolean)
      '    LoadProperty(LiveProperty, Value)
      '  End Set
      'End Property

      'Public Shared DelayedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Delayed, "Delayed", True)
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="Delayed", Description:="")>
      'Public Property Delayed() As Boolean
      '  Get
      '    Return ReadProperty(DelayedProperty)
      '  End Get
      '  Set(ByVal Value As Boolean)
      '    LoadProperty(DelayedProperty, Value)
      '  End Set
      'End Property

      'Public Shared PremierProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Premier, "Premier", False)
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="Premier", Description:="")>
      'Public Property Premier() As Boolean
      '  Get
      '    Return ReadProperty(PremierProperty)
      '  End Get
      '  Set(ByVal Value As Boolean)
      '    LoadProperty(PremierProperty, Value)
      '  End Set
      'End Property

      'Public Shared GenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNo, "Gen Ref No", Nothing)
      ' ''' <summary>
      ' ''' Gets and sets the Gen Ref No value
      ' ''' </summary>
      '<Display(Name:="Gen Ref No", Description:="")>
      'Public Property GenRefNo() As Int64?
      '  Get
      '    Return ReadProperty(GenRefNoProperty)
      '  End Get
      '  Set(ByVal Value As Int64?)
      '    LoadProperty(GenRefNoProperty, Value)
      '  End Set
      'End Property

      'Public Shared GenRefNoStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenRefNoString, "Gen Ref No", "")
      ' ''' <summary>
      ' ''' Gets and sets the Gen Ref No value
      ' ''' </summary>
      '<Display(Name:="Gen Ref No", Description:="")>
      'Public Property GenRefNoString() As String
      '  Get
      '    Return ReadProperty(GenRefNoStringProperty)
      '  End Get
      '  Set(ByVal Value As String)
      '    LoadProperty(GenRefNoStringProperty, Value)
      '  End Set
      'End Property

      'Public Shared PrimaryChannelsOnlyProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.PrimaryChannelsOnly, False) _
      '                                                                        .AddSetExpression("RoomAllocatorRoomScheduleListCriteriaBO.PrimaryChannelsOnlySet(self)")
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="Primary Channels Only?", Description:="")>
      'Public Property PrimaryChannelsOnly() As Boolean
      '  Get
      '    Return ReadProperty(PrimaryChannelsOnlyProperty)
      '  End Get
      '  Set(ByVal Value As Boolean)
      '    LoadProperty(PrimaryChannelsOnlyProperty, Value)
      '  End Set
      'End Property

      'Public Shared SelectedChannelsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedChannelsDisplay, "Selected Channels", "Select Channels")
      ' ''' <summary>
      ' ''' Gets and sets the Gen Ref No value
      ' ''' </summary>
      '<Display(Name:="Channels")>
      'Public Property SelectedChannelsDisplay() As String
      '  Get
      '    Return ReadProperty(SelectedChannelsProperty)
      '  End Get
      '  Set(ByVal Value As String)
      '    LoadProperty(SelectedChannelsProperty, Value)
      '  End Set
      'End Property

      'Public Shared SelectedChannelIDsXMLProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedChannelIDsXML, "Selected Channels", "")
      ' ''' <summary>
      ' ''' Gets and sets the Gen Ref No value
      ' ''' </summary>
      '<Display(Name:="Channels")>
      'Public Property SelectedChannelIDsXML() As String
      '  Get
      '    Return ReadProperty(SelectedChannelIDsXMLProperty)
      '  End Get
      '  Set(ByVal Value As String)
      '    LoadProperty(SelectedChannelIDsXMLProperty, Value)
      '  End Set
      'End Property

      'Public Shared GenreProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Genre, "Genre", "")
      ' ''' <summary>
      ' ''' Gets and sets the Gen Ref No value
      ' ''' </summary>
      '<Display(Name:="Genre")>
      'Public Property Genre() As String
      '  Get
      '    Return ReadProperty(GenreProperty)
      '  End Get
      '  Set(ByVal Value As String)
      '    LoadProperty(GenreProperty, Value)
      '  End Set
      'End Property

      'Public Shared SeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Series, "Series", "")
      ' ''' <summary>
      ' ''' Gets and sets the Gen Ref No value
      ' ''' </summary>
      '<Display(Name:="Series")>
      'Public Property Series() As String
      '  Get
      '    Return ReadProperty(SeriesProperty)
      '  End Get
      '  Set(ByVal Value As String)
      '    LoadProperty(SeriesProperty, Value)
      '  End Set
      'End Property

      'Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
      ' ''' <summary>
      ' ''' Gets and sets the Gen Ref No value
      ' ''' </summary>
      '<Display(Name:="Title")>
      'Public Property Title() As String
      '  Get
      '    Return ReadProperty(TitleProperty)
      '  End Get
      '  Set(ByVal Value As String)
      '    LoadProperty(TitleProperty, Value)
      '  End Set
      'End Property

      'Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Keyword, "Keyword", "")
      ' ''' <summary>
      ' ''' Gets and sets the Gen Ref No value
      ' ''' </summary>
      '<Display(Name:="Keyword")>
      'Public Property Keyword() As String
      '  Get
      '    Return ReadProperty(KeywordProperty)
      '  End Get
      '  Set(ByVal Value As String)
      '    LoadProperty(KeywordProperty, Value)
      '  End Set
      'End Property

      'Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?,
      '               StartDate As DateTime?, EndDate As DateTime?)

      '  Me.SystemID = SystemID
      '  Me.ProductionAreaID = ProductionAreaID
      '  Me.StartDate = StartDate
      '  Me.EndDate = EndDate

      'End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRoomAllocatorRoomScheduleList() As RoomAllocatorRoomScheduleList

      Return New RoomAllocatorRoomScheduleList()

    End Function

    Public Shared Sub BeginGetRoomAllocatorRoomScheduleList(CallBack As EventHandler(Of DataPortalResult(Of RoomAllocatorRoomScheduleList)))

      Dim dp As New DataPortal(Of RoomAllocatorRoomScheduleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRoomAllocatorRoomScheduleList() As RoomAllocatorRoomScheduleList

      Return DataPortal.Fetch(Of RoomAllocatorRoomScheduleList)(New Criteria())

    End Function

    Public Shared Function GetRoomAllocatorRoomScheduleList(Criteria As OBLib.Rooms.RoomAllocatorRoomScheduleList.Criteria) As RoomAllocatorRoomScheduleList

      Return DataPortal.Fetch(Of RoomAllocatorRoomScheduleList)(Criteria)

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(RoomAllocatorRoomSchedule.GetRoomAllocatorRoomSchedule(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As RoomAllocatorRoomSchedule = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.RoomScheduleID <> sdr.GetInt32(1) Then
            parent = Me(0) 'Me.GetItem(sdr.GetInt32(1))
          End If
          parent.RoomAllocatorRoomScheduleChannelList.RaiseListChangedEvents = False
          parent.RoomAllocatorRoomScheduleChannelList.Add(RoomAllocatorRoomScheduleChannel.GetRoomAllocatorRoomScheduleChannel(sdr))
          parent.RoomAllocatorRoomScheduleChannelList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRoomAllocatorRoomScheduleList"
            cm.Parameters.AddWithValue("@RoomScheduleID", NothingDBNull(crit.RoomScheduleID))
            cm.Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(crit.ProductionSystemAreaID))
            cm.Parameters.AddWithValue("@ScheduleNumber", NothingDBNull(crit.ScheduleNumber))
            'cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            'cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            'cm.Parameters.AddWithValue("@GenRefNo", NothingDBNull(crit.GenRefNo))
            'cm.Parameters.AddWithValue("@Live", crit.Live)
            'cm.Parameters.AddWithValue("@Delayed", crit.Delayed)
            'cm.Parameters.AddWithValue("@Premier", crit.Premier)
            'cm.Parameters.AddWithValue("@PrimaryChannelsOnly", crit.PrimaryChannelsOnly)
            'cm.Parameters.AddWithValue("@SelectedChannelIDs", Strings.MakeEmptyDBNull(crit.SelectedChannelIDsXML))
            'cm.Parameters.AddWithValue("@Genre", Strings.MakeEmptyDBNull(crit.Genre))
            'cm.Parameters.AddWithValue("@Series", Strings.MakeEmptyDBNull(crit.Series))
            'cm.Parameters.AddWithValue("@Title", Strings.MakeEmptyDBNull(crit.Title))
            'cm.Parameters.AddWithValue("@GenRefNoString", Strings.MakeEmptyDBNull(crit.GenRefNoString))
            'cm.Parameters.AddWithValue("@Keyword", Strings.MakeEmptyDBNull(crit.Keyword))
            'crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As RoomAllocatorRoomSchedule In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As RoomAllocatorRoomSchedule In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace