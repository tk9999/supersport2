﻿' Generated 21 Nov 2016 13:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class RoomSchedCh
    Inherits OBBusinessBase(Of RoomSchedCh)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RoomSchedChBO.RoomSchedChBOToString(self)")

    Public Shared RoomScheduleChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleChannelID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property RoomScheduleChannelID() As Integer
      Get
        Return GetProperty(RoomScheduleChannelIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleChannelIDProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared ScheduleNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ScheduleNumber, "Schedule Number")
    ''' <summary>
    ''' Gets the Schedule Number value
    ''' </summary>
    Public Property ScheduleNumber() As Integer
      Get
        Return GetProperty(ScheduleNumberProperty)
      End Get
      Set(value As Integer)
        SetProperty(ScheduleNumberProperty, value)
      End Set
    End Property

    Public Shared ProductionChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionChannelID, "Production Channel")
    ''' <summary>
    ''' Gets and sets the Production Channel value
    ''' </summary>
    <Display(Name:="Production Channel", Description:="")>
    Public Property ProductionChannelID() As Integer
      Get
        Return GetProperty(ProductionChannelIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionChannelIDProperty, Value)
      End Set
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel")
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelShortNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared RoomSchedChHrListProperty As PropertyInfo(Of SchEvtChRmHrList) = RegisterProperty(Of SchEvtChRmHrList)(Function(c) c.SchEvtChRmHrList, "Sch Evt Ch Rm Hr List")

    Public ReadOnly Property SchEvtChRmHrList() As SchEvtChRmHrList
      Get
        If GetProperty(RoomSchedChHrListProperty) Is Nothing Then
          LoadProperty(RoomSchedChHrListProperty, Rooms.SchEvtChRmHrList.NewSchEvtChRmHrList())
        End If
        Return GetProperty(RoomSchedChHrListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As RoomSched

      Return CType(CType(Me.Parent, RoomSchedChList).Parent, RoomSched)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleChannelIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ChannelShortName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sch Evt Ch Rm")
        Else
          Return String.Format("Blank {0}", "Sch Evt Ch Rm")
        End If
      Else
        Return Me.ChannelShortName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      'With AddWebRule(RoomIDProperty)
      '  .AddTriggerProperties({CallTimeProperty, StartDateTimeProperty, EndDateTimeProperty, WrapTimeProperty})
      '  .JavascriptRuleFunctionName = "RoomSchedChBO.RoomIDValid"
      '  .AffectedProperties.AddRange({CallTimeProperty, StartDateTimeProperty, EndDateTimeProperty, WrapTimeProperty})
      'End With

      'With AddWebRule(StartDateTimeProperty)
      '  .AddTriggerProperties({CallTimeProperty, StartDateTimeProperty, EndDateTimeProperty, WrapTimeProperty, BookingReasonProperty})
      '  .JavascriptRuleFunctionName = "RoomAllocatorBO.DatesValid"
      '  .AffectedProperties.AddRange({CallTimeProperty, StartDateTimeProperty, EndDateTimeProperty, WrapTimeProperty})
      'End With

    End Sub

    'Private Function CreateResourceBookingTableParameter() As DataTable

    '  Dim RBTable As New DataTable
    '  RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("ResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("ResourceBookingID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("StartDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("EndDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("RoomSchedChuleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("ProductionHRID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("EquipmentScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("HumanResourceShiftID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("HumanResourceOffPeriodID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
    '  RBTable.Columns.Add("HumanResourceSecondmentID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

    '  'Room
    '  Dim rrow As DataRow = RBTable.NewRow
    '  rrow("ObjectGuid") = Me.Guid
    '  rrow("ResourceID") = NothingDBNull(Me.ResourceID)
    '  rrow("ResourceBookingID") = NothingDBNull(Me.ResourceBookingID)
    '  rrow("StartDateTimeBuffer") = NothingDBNull(Me.CallTime)
    '  rrow("StartDateTime") = Me.StartDateTime.Value.ToString
    '  rrow("EndDateTime") = Me.EndDateTime.Value.ToString
    '  rrow("EndDateTimeBuffer") = NothingDBNull(Me.WrapTime)
    '  rrow("RoomSchedChuleID") = NothingDBNull(Nothing)
    '  rrow("ProductionHRID") = NothingDBNull(Nothing)
    '  rrow("EquipmentScheduleID") = NothingDBNull(Nothing)
    '  rrow("HumanResourceShiftID") = NothingDBNull(Nothing)
    '  rrow("HumanResourceOffPeriodID") = NothingDBNull(Nothing)
    '  rrow("HumanResourceSecondmentID") = NothingDBNull(Nothing)
    '  RBTable.Rows.Add(rrow)
    '  ''Crew
    '  'For Each rb As RoomSchedChHr In Me.RoomSchedChHrList
    '  '  Dim row As DataRow = RBTable.NewRow
    '  '  row("ObjectGuid") = rb.Guid
    '  '  row("ResourceID") = NothingDBNull(rb.ResourceID)
    '  '  row("ResourceBookingID") = NothingDBNull(rb.ResourceBookingID)
    '  '  row("StartDateTimeBuffer") = NothingDBNull(rb.CallTime)
    '  '  row("StartDateTime") = rb.StartDateTime.Value.ToString
    '  '  row("EndDateTime") = rb.EndDateTime.Value.ToString
    '  '  row("EndDateTimeBuffer") = NothingDBNull(rb.WrapTime)
    '  '  RBTable.Rows.Add(row)
    '  'Next

    '  Return RBTable

    'End Function

    'Private Function GetClashesDataSet() As DataSet

    '  Dim RBTable As DataTable = CreateResourceBookingTableParameter()
    '  Dim cmd As New Singular.CommandProc("RuleProcs.ruleResourcesAvailable")
    '  Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
    '  ResourceBookingsParam.Name = "@ResourceBookings"
    '  ResourceBookingsParam.SqlType = SqlDbType.Structured
    '  ResourceBookingsParam.Value = RBTable
    '  cmd.Parameters.Add(ResourceBookingsParam)
    '  cmd.FetchType = CommandProc.FetchTypes.DataSet
    '  cmd = cmd.Execute(0)
    '  Return cmd.Dataset

    'End Function

    'Public Sub UpdateClashes()
    '  Dim clashData As DataSet = GetClashesDataSet()
    '  'Clear
    '  Me.Clashes = ""
    '  'Update
    '  For Each dr As DataRow In clashData.Tables(0).Rows
    '    If CompareSafe(dr(0), Me.Guid) Then
    '      'Room
    '      Me.Clashes &= vbCrLf & dr(12)
    '      'Else
    '      '  'Crew
    '      '  For Each rb As RoomSchedChHr In Me.RoomSchedChHrList
    '      '    If CompareSafe(dr(0), rb.Guid) Then
    '      '      rb.Clashes &= vbCrLf & dr(12)
    '      '    End If
    '      '  Next
    '    End If
    '  Next
    'End Sub


    Private Function CreateCrewResourceBookingTableParameter() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceBookingID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("RoomSchedChuleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionHRID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EquipmentScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceShiftID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceOffPeriodID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceSecondmentID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

      'Crew
      For Each rb As SchEvtChRmHr In Me.SchEvtChRmHrList
        Dim row As DataRow = RBTable.NewRow
        row("ObjectGuid") = rb.Guid
        row("ResourceID") = NothingDBNull(rb.ResourceID)
        row("ResourceBookingID") = NothingDBNull(rb.ResourceBookingID)
        row("StartDateTimeBuffer") = NothingDBNull(rb.CallTime)
        row("StartDateTime") = rb.StartDateTime.Value.ToString
        row("EndDateTime") = rb.EndDateTime.Value.ToString
        row("EndDateTimeBuffer") = NothingDBNull(rb.WrapTime)
        row("RoomSchedChuleID") = NothingDBNull(Nothing)
        row("ProductionHRID") = NothingDBNull(Nothing)
        row("EquipmentScheduleID") = NothingDBNull(Nothing)
        row("HumanResourceShiftID") = NothingDBNull(Nothing)
        row("HumanResourceOffPeriodID") = NothingDBNull(Nothing)
        row("HumanResourceSecondmentID") = NothingDBNull(Nothing)
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

    Private Function GetCrewClashesDataSet() As DataSet

      Dim RBTable As DataTable = CreateCrewResourceBookingTableParameter()
      Dim cmd As New Singular.CommandProc("RuleProcs.ruleResourcesAvailable")
      Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      ResourceBookingsParam.Name = "@ResourceBookings"
      ResourceBookingsParam.SqlType = SqlDbType.Structured
      ResourceBookingsParam.Value = RBTable
      cmd.Parameters.Add(ResourceBookingsParam)
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute(0)
      Return cmd.Dataset

    End Function

    Public Sub UpdateCrewClashes()
      Dim clashData As DataSet = GetCrewClashesDataSet()
      'Clear
      For Each rb As SchEvtChRmHr In Me.SchEvtChRmHrList
        rb.Clashes = ""
      Next
      'Update
      For Each dr As DataRow In clashData.Tables(0).Rows
        For Each rb As SchEvtChRmHr In Me.SchEvtChRmHrList
          If CompareSafe(dr(0), rb.Guid) Then
            rb.Clashes &= vbCrLf & dr(12)
          End If
        Next
      Next
    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomSchedCh() method.

    End Sub

    Public Shared Function NewRoomSchedCh() As RoomSchedCh

      Return DataPortal.CreateChild(Of RoomSchedCh)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetRoomSchedCh(dr As SafeDataReader) As RoomSchedCh

      Dim s As New RoomSchedCh()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RoomScheduleChannelIDProperty, .GetInt32(0))
          LoadProperty(RoomScheduleIDProperty, .GetInt32(1))
          LoadProperty(ProductionChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ScheduleNumberProperty, .GetInt32(3))
          LoadProperty(ChannelShortNameProperty, .GetString(4))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Function AddInputOutputString(cm As SqlClient.SqlCommand, OutputProperty As Csla.PropertyInfo(Of String)) As SqlClient.SqlParameter
      Dim param = cm.Parameters.Add("@" & OutputProperty.Name, SqlDbType.VarChar)
      param.Direction = ParameterDirection.InputOutput
      param.Value = GetProperty(OutputProperty)
      Return param
    End Function

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      If Me.IsNew Then
        cm.CommandText = "[InsProcsWeb].[insRoomSchedChPlayout]"
      Else
        cm.CommandText = "[UpdProcsWeb].[updRoomSchedChPlayout]"
      End If

      AddInputOutputParam(cm, RoomScheduleChannelIDProperty)
      AddInputOutputParam(cm, ProductionChannelIDProperty)
      cm.Parameters.AddWithValue("@RoomScheduleID", Me.GetParent.RoomScheduleID)
      'cm.Parameters.AddWithValue("@ProductionChannelID", ProductionChannelID)
      cm.Parameters.AddWithValue("@ScheduleNumber", ScheduleNumber)
      cm.Parameters.AddWithValue("@ChannelShortName", ChannelShortName)
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      Return Sub()
               'Post Save
               'If Me.IsNew Then
               LoadProperty(RoomScheduleChannelIDProperty, cm.Parameters("@RoomScheduleChannelID").Value)
               LoadProperty(ProductionChannelIDProperty, cm.Parameters("@ProductionChannelID").Value)
               'End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(RoomSchedChHrListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.CommandText = "[DelProcsWeb].[delRoomSchedChPlayout]"
      cm.Parameters.AddWithValue("@RoomScheduleChannelID", GetProperty(RoomScheduleChannelIDProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
    End Sub

#End Region

  End Class

End Namespace