﻿' Generated 21 Nov 2016 13:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class SchEvtChRmHrList
    Inherits OBBusinessListBase(Of SchEvtChRmHrList, SchEvtChRmHr)

#Region " Business Methods "

    Public Function GetItem(ProductionHRID As Integer) As SchEvtChRmHr

      For Each child As SchEvtChRmHr In Me
        If child.ProductionHRID = ProductionHRID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSchEvtChRmHrList() As SchEvtChRmHrList

      Return New SchEvtChRmHrList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace