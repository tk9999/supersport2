﻿' Generated 21 Nov 2016 13:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class RoomSched
    Inherits OBBusinessBase(Of RoomSched)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RoomSchedBO.RoomSchedBOToString(self)")

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    'Public Shared ScheduleNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ScheduleNumber, "Schedule Number")
    ' ''' <summary>
    ' ''' Gets the Schedule Number value
    ' ''' </summary>
    'Public Property ScheduleNumber() As Integer
    '  Get
    '    Return GetProperty(ScheduleNumberProperty)
    '  End Get
    '  Set(value As Integer)
    '    SetProperty(ScheduleNumberProperty, value)
    '  End Set
    'End Property

    'Public Shared ProductionChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionChannelID, "Production Channel")
    ' ''' <summary>
    ' ''' Gets and sets the Production Channel value
    ' ''' </summary>
    '<Display(Name:="Production Channel", Description:="")>
    'Public Property ProductionChannelID() As Integer
    '  Get
    '    Return GetProperty(ProductionChannelIDProperty)
    '  End Get
    '  Set(ByVal Value As Integer)
    '    SetProperty(ProductionChannelIDProperty, Value)
    '  End Set
    'End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
    Required(ErrorMessage:="Room required"),
    DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="RoomSchedBO.setRoomIDCriteriaBeforeRefresh",
                 PreFindJSFunction:="RoomSchedBO.triggerRoomIDAutoPopulate",
                 AfterFetchJS:="RoomSchedBO.afterRoomIDRefreshAjax",
                 LookupMember:="Room", ValueMember:="RoomID", DropDownColumns:={"Room", "IsAvailableString"},
                 OnItemSelectJSFunction:="RoomSchedBO.onRoomSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("RoomSchedBO.RoomIDSet(self)")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared OwnerPSAIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OwnerPSAID, "Owner PSA")
    ''' <summary>
    ''' Gets and sets the Owner PSA value
    ''' </summary>
    <Display(Name:="Owner PSA", Description:="")>
    Public Property OwnerPSAID() As Integer
      Get
        Return GetProperty(OwnerPSAIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OwnerPSAIDProperty, Value)
      End Set
    End Property

    Public Shared RoomTimesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomTimes, "Room Times")
    ''' <summary>
    ''' Gets and sets the Room Times value
    ''' </summary>
    <Display(Name:="Room Times", Description:="")>
    Public Property RoomTimes() As String
      Get
        Return GetProperty(RoomTimesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomTimesProperty, Value)
      End Set
    End Property

    Public Shared StatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Status, "Status")
    ''' <summary>
    ''' Gets and sets the Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property Status() As String
      Get
        Return GetProperty(StatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusProperty, Value)
      End Set
    End Property

    Public Shared CssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CssClass, "Css Class")
    ''' <summary>
    ''' Gets and sets the Css Class value
    ''' </summary>
    <Display(Name:="Css Class", Description:="")>
    Public Property CssClass() As String
      Get
        Return GetProperty(CssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CssClassProperty, Value)
      End Set
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.CallTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Call Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    Required(ErrorMessage:="Call Time required"),
    SetExpression("RoomSchedBO.CallTimeSet(self)")>
    Public Property CallTime As DateTime?
      Get
        Return GetProperty(CallTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
    SetExpression("RoomSchedBO.StartTimeSet(self)")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
    SetExpression("RoomSchedBO.EndTimeSet(self)")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.WrapTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    SetExpression("RoomSchedBO.WrapTimeSet(self)")>
    Public Property WrapTime As DateTime?
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeProperty, Value)
      End Set
    End Property

    Public Shared CallTimeTimelineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CallTimeTimelineID, "Call Time Timeline")
    ''' <summary>
    ''' Gets and sets the Call Time Timeline value
    ''' </summary>
    <Display(Name:="Call Time Timeline", Description:="")>
    Public Property CallTimeTimelineID() As Integer
      Get
        Return GetProperty(CallTimeTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CallTimeTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared OnAirTimeTimelineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OnAirTimeTimelineID, "On Air Time Timeline")
    ''' <summary>
    ''' Gets and sets the On Air Time Timeline value
    ''' </summary>
    <Display(Name:="On Air Time Timeline", Description:="")>
    Public Property OnAirTimeTimelineID() As Integer
      Get
        Return GetProperty(OnAirTimeTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OnAirTimeTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeTimelineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WrapTimeTimelineID, "Wrap Time Timeline")
    ''' <summary>
    ''' Gets and sets the Wrap Time Timeline value
    ''' </summary>
    <Display(Name:="Wrap Time Timeline", Description:="")>
    Public Property WrapTimeTimelineID() As Integer
      Get
        Return GetProperty(WrapTimeTimelineIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WrapTimeTimelineIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the Owner PSA value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept is required"),
    DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Owner PSA value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area is required"),
    DropDownWeb(GetType(ROUserSystemAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", ThisFilterMember:="SystemID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "ResourceBookingDescription")
    ''' <summary>
    ''' Gets and sets the Booking Times value
    ''' </summary>
    <Display(Name:="ResourceBookingDescription", Description:="")>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ClashesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Clashes, "Clashes")
    ''' <summary>
    ''' Gets and sets the Booking Times value
    ''' </summary>
    <Display(Name:="Clashes", Description:=""), AlwaysClean>
    Public Property Clashes() As String
      Get
        Return GetProperty(ClashesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ClashesProperty, Value)
      End Set
    End Property

    Public Shared BookingReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingReason, "BookingReason")
    ''' <summary>
    ''' Gets and sets the Booking Times value
    ''' </summary>
    <Display(Name:="BookingReason", Description:="")>
    Public Property BookingReason() As String
      Get
        Return GetProperty(BookingReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BookingReasonProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref Number")
    ''' <summary>
    ''' Gets the Gen Ref Number value
    ''' </summary>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(value As Int64)
        SetProperty(GenRefNumberProperty, value)
      End Set
    End Property

    Public Shared ChannelCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelCount, "ChannelCount")
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public Property ChannelCount() As Integer
      Get
        Return GetProperty(ChannelCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ChannelCountProperty, Value)
      End Set
    End Property

    Public Shared ClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ClashCount, "Clash Count", 0)
    Public Property ClashCount() As Integer
      Get
        Return GetProperty(ClashCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(ClashCountProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared RoomSchedHrListProperty As PropertyInfo(Of SchEvtChRmHrList) = RegisterProperty(Of SchEvtChRmHrList)(Function(c) c.SchEvtChRmHrList, "Sch Evt Ch Rm Hr List")
    Public ReadOnly Property SchEvtChRmHrList() As SchEvtChRmHrList
      Get
        If GetProperty(RoomSchedHrListProperty) Is Nothing Then
          LoadProperty(RoomSchedHrListProperty, Rooms.SchEvtChRmHrList.NewSchEvtChRmHrList())
        End If
        Return GetProperty(RoomSchedHrListProperty)
      End Get
    End Property

    Public Shared RoomSchedChListProperty As PropertyInfo(Of RoomSchedChList) = RegisterProperty(Of RoomSchedChList)(Function(c) c.RoomSchedChList, "RoomSchedChList")
    Public ReadOnly Property RoomSchedChList() As RoomSchedChList
      Get
        If GetProperty(RoomSchedChListProperty) Is Nothing Then
          LoadProperty(RoomSchedChListProperty, Rooms.RoomSchedChList.NewRoomSchedChList())
        End If
        Return GetProperty(RoomSchedChListProperty)
      End Get
    End Property

    Public Shared ClashListProperty As PropertyInfo(Of List(Of String)) = RegisterProperty(Of List(Of String))(Function(c) c.ClashList, "Clash List")
    Public ReadOnly Property ClashList() As List(Of String)
      Get
        If GetProperty(ClashListProperty) Is Nothing Then
          LoadProperty(ClashListProperty, New List(Of String))
        End If
        Return GetProperty(ClashListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SchEvt

      Return CType(CType(Me.Parent, RoomSchedList).Parent, SchEvt)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Room.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sch Evt Ch Rm")
        Else
          Return String.Format("Blank {0}", "Sch Evt Ch Rm")
        End If
      Else
        Return Me.Room
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(RoomIDProperty)
        .AddTriggerProperties({CallTimeProperty, StartDateTimeProperty, EndDateTimeProperty, WrapTimeProperty})
        .JavascriptRuleFunctionName = "RoomSchedBO.RoomIDValid"
        .ServerRuleFunction = AddressOf ClashesValid
        .AffectedProperties.AddRange({CallTimeProperty, StartDateTimeProperty, EndDateTimeProperty, WrapTimeProperty})
      End With

      With AddWebRule(ChannelCountProperty)
        .JavascriptRuleFunctionName = "RoomSchedBO.ChannelCountValid"
        .ServerRuleFunction = AddressOf ChannelCountValid
        .AddTriggerProperties({RoomSchedChListProperty})
      End With

      'With AddWebRule(StartDateTimeProperty)
      '  .AddTriggerProperties({CallTimeProperty, StartDateTimeProperty, EndDateTimeProperty, WrapTimeProperty, BookingReasonProperty})
      '  .JavascriptRuleFunctionName = "RoomAllocatorBO.DatesValid"
      '  .AffectedProperties.AddRange({CallTimeProperty, StartDateTimeProperty, EndDateTimeProperty, WrapTimeProperty})
      'End With

    End Sub

    Public Shared Function ClashesValid(RoomSched As RoomSched) As String

      If RoomSched.Clashes.Length > 0 Then
        Return "Clashes in " & RoomSched.Room & " were detected"
      End If
      Return ""

    End Function

    Public Shared Function ChannelCountValid(RoomSched As RoomSched) As String

      If RoomSched.RoomSchedChList.Count = 0 Then
        Return "Channel count cannot be 0"
      End If
      Return ""

    End Function


    Private Function CreateResourceBookingTableParameter() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceBookingID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("RoomScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionHRID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EquipmentScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceShiftID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceOffPeriodID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceSecondmentID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionSystemAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

      'Room
      Dim rrow As DataRow = RBTable.NewRow
      rrow("ObjectGuid") = Me.Guid
      rrow("ResourceID") = NothingDBNull(Me.ResourceID)
      rrow("ResourceBookingID") = NothingDBNull(Me.ResourceBookingID)
      rrow("StartDateTimeBuffer") = NothingDBNull(Me.CallTime)
      rrow("StartDateTime") = Me.StartDateTime.Value.ToString
      rrow("EndDateTime") = Me.EndDateTime.Value.ToString
      rrow("EndDateTimeBuffer") = NothingDBNull(Me.WrapTime)
      rrow("RoomScheduleID") = NothingDBNull(Me.RoomScheduleID)
      rrow("ProductionHRID") = NothingDBNull(Nothing)
      rrow("EquipmentScheduleID") = NothingDBNull(Nothing)
      rrow("HumanResourceShiftID") = NothingDBNull(Nothing)
      rrow("HumanResourceOffPeriodID") = NothingDBNull(Nothing)
      rrow("HumanResourceSecondmentID") = NothingDBNull(Nothing)
      rrow("ProductionSystemAreaID") = NothingDBNull(Me.OwnerPSAID)
      RBTable.Rows.Add(rrow)
      ''Crew
      'For Each rb As RoomSchedHr In Me.RoomSchedHrList
      '  Dim row As DataRow = RBTable.NewRow
      '  row("ObjectGuid") = rb.Guid
      '  row("ResourceID") = NothingDBNull(rb.ResourceID)
      '  row("ResourceBookingID") = NothingDBNull(rb.ResourceBookingID)
      '  row("StartDateTimeBuffer") = NothingDBNull(rb.CallTime)
      '  row("StartDateTime") = rb.StartDateTime.Value.ToString
      '  row("EndDateTime") = rb.EndDateTime.Value.ToString
      '  row("EndDateTimeBuffer") = NothingDBNull(rb.WrapTime)
      '  RBTable.Rows.Add(row)
      'Next

      Return RBTable

    End Function

    Private Function GetClashesDataSet() As DataSet

      Dim RBTable As DataTable = CreateResourceBookingTableParameter()
      Dim cmd As New Singular.CommandProc("RuleProcs.ruleResourcesAvailable")
      Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      ResourceBookingsParam.Name = "@ResourceBookings"
      ResourceBookingsParam.SqlType = SqlDbType.Structured
      ResourceBookingsParam.Value = RBTable
      cmd.Parameters.Add(ResourceBookingsParam)
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute(0)
      Return cmd.Dataset

    End Function

    Public Sub UpdateClashes()
      Dim clashData As DataSet = GetClashesDataSet()
      'Clear
      Me.Clashes = ""
      Me.ClashList.Clear()
      'Update
      Dim index = 0
      For Each dr As DataRow In clashData.Tables(0).Rows
        If CompareSafe(dr(0), Me.Guid) Then
          'Room
          If index = 0 Then
            Me.Clashes = "Clashes detected: " & vbCrLf & dr(12)
          Else
            Me.Clashes &= vbCrLf & dr(12)
          End If
        End If
        Dim d1 As Date = dr(8)
        Dim d2 As Date = dr(9)
        Me.ClashList.Add(dr(12) & ":" & d1.ToString("dd MMM yy HH:mm") & " - " & d2.ToString("dd MMM yy HH:mm"))
      Next
      Me.ClashCount = Me.ClashList.Count
    End Sub


    Private Function CreateCrewResourceBookingTableParameter() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceBookingID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("RoomScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionHRID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EquipmentScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceShiftID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceOffPeriodID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceSecondmentID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionSystemAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

      'Crew
      For Each rb As SchEvtChRmHr In Me.SchEvtChRmHrList
        If Not IsNullNothing(rb.ResourceID, True) AndAlso Not IsNullNothing(rb.StartDateTime) AndAlso Not IsNullNothing(rb.EndDateTime) Then
          Dim row As DataRow = RBTable.NewRow
          row("ObjectGuid") = rb.Guid
          row("ResourceID") = NothingDBNull(rb.ResourceID)
          row("ResourceBookingID") = NothingDBNull(rb.ResourceBookingID)
          row("StartDateTimeBuffer") = NothingDBNull(rb.CallTime)
          row("StartDateTime") = rb.StartDateTime.Value.ToString
          row("EndDateTime") = rb.EndDateTime.Value.ToString
          row("EndDateTimeBuffer") = NothingDBNull(rb.WrapTime)
          row("RoomScheduleID") = NothingDBNull(rb.GetParent.RoomScheduleID)
          row("ProductionHRID") = NothingDBNull(Nothing)
          row("EquipmentScheduleID") = NothingDBNull(Nothing)
          row("HumanResourceShiftID") = NothingDBNull(rb.HumanResourceShiftID)
          row("HumanResourceOffPeriodID") = NothingDBNull(Nothing)
          row("HumanResourceSecondmentID") = NothingDBNull(Nothing)
          row("ProductionSystemAreaID") = NothingDBNull(Nothing)
          RBTable.Rows.Add(row)
        End If
      Next

      Return RBTable

    End Function

    Private Function GetCrewClashesDataSet() As DataSet

      Dim RBTable As DataTable = CreateCrewResourceBookingTableParameter()
      Dim cmd As New Singular.CommandProc("RuleProcs.ruleResourcesAvailable")
      Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      ResourceBookingsParam.Name = "@ResourceBookings"
      ResourceBookingsParam.SqlType = SqlDbType.Structured
      ResourceBookingsParam.Value = RBTable
      cmd.Parameters.Add(ResourceBookingsParam)
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute(0)
      Return cmd.Dataset

    End Function

    Public Sub UpdateCrewClashes()
      Dim clashData As DataSet = GetCrewClashesDataSet()
      'Clear
      For Each rb As SchEvtChRmHr In Me.SchEvtChRmHrList
        rb.Clashes = ""
        rb.ClashList.Clear()
      Next
      'Update
      For Each dr As DataRow In clashData.Tables(0).Rows
        For Each rb As SchEvtChRmHr In Me.SchEvtChRmHrList
          If CompareSafe(dr(0), rb.Guid) Then
            rb.Clashes &= vbCrLf & dr(12)
            Dim d1 As Date = dr(8)
            Dim d2 As Date = dr(9)
            rb.ClashList.Add(dr(12) & ":" & d1.ToString("dd MMM yy HH:mm") & " - " & d2.ToString("dd MMM yy HH:mm"))
          End If
          rb.ClashCount = rb.ClashList.Count
        Next
      Next
    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomSched() method.

    End Sub

    Public Shared Function NewRoomSched() As RoomSched

      Return DataPortal.CreateChild(Of RoomSched)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetRoomSched(dr As SafeDataReader) As RoomSched

      Dim s As New RoomSched()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
          'LoadProperty(ScheduleNumberProperty, .GetInt32(1))
          'LoadProperty(ProductionChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(RoomProperty, .GetString(3))
          LoadProperty(OwnerPSAIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(RoomTimesProperty, .GetString(5))
          LoadProperty(StatusProperty, .GetString(6))
          LoadProperty(CssClassProperty, .GetString(7))
          LoadProperty(CallTimeProperty, .GetValue(8))
          LoadProperty(StartDateTimeProperty, .GetValue(9))
          LoadProperty(EndDateTimeProperty, .GetValue(10))
          LoadProperty(WrapTimeProperty, .GetValue(11))
          LoadProperty(CallTimeTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(OnAirTimeTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(WrapTimeTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
          LoadProperty(GenRefNumberProperty, .GetInt64(19))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Public Function GetChannelNames() As String
      Dim ChannelString As String = ""
      For Each ch As RoomSchedCh In Me.RoomSchedChList
        If ChannelString.Length = 0 Then
          ChannelString = ch.ChannelShortName
        Else
          ChannelString = ChannelString & ", " & ch.ChannelShortName
        End If
      Next
      Return ChannelString
    End Function

    Public Function GetCrewDescriptions() As String
      Dim HRString As String = ""
      For Each ch As SchEvtChRmHr In Me.SchEvtChRmHrList
        If HRString.Length = 0 Then
          HRString = ch.HRName & " " & ch.CallTime.Value.ToString("HH:mm") & " - " & ch.WrapTime.Value.ToString("HH:mm")
        Else
          HRString = HRString & vbCrLf & ch.HRName & " " & ch.StartDateTime.Value.ToString("HH:mm") & " - " & ch.EndDateTime.Value.ToString("HH:mm")
        End If
      Next
      Return HRString
    End Function

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      If Me.IsNew Then
        cm.CommandText = "[InsProcsWeb].[insRoomScheduleAreaPlayout]"
      Else
        cm.CommandText = "[UpdProcsWeb].[updRoomScheduleAreaPlayout]"
      End If

      AddPrimaryKeyParam(cm, RoomScheduleIDProperty)
      AddInputOutputParam(cm, OwnerPSAIDProperty)
      'AddOutputParam(cm, ProductionChannelIDProperty)
      'AddOutputParam(cm, ProductionIDProperty)
      AddInputOutputParam(cm, CallTimeTimelineIDProperty)
      AddInputOutputParam(cm, OnAirTimeTimelineIDProperty)
      AddInputOutputParam(cm, WrapTimeTimelineIDProperty)
      AddInputOutputParam(cm, ResourceBookingIDProperty)
      AddInputOutputString(cm, ResourceBookingDescriptionProperty, 500)
      AddInputOutputString(cm, CssClassProperty, 250)

      'cm.Parameters.AddWithValue("@RoomScheduleTypeID", OBLib.CommonData.Enums.RoomScheduleType.Production)
      'cm.Parameters.AddWithValue("@AdhocBookingID", NothingDBNull(Nothing))
      'cm.Parameters.AddWithValue("@Title", "") 'this will get set in the proc
      'cm.Parameters.AddWithValue("@ProductionChannelID", Me.GetParent().ProductionChannelID)
      'cm.Parameters.AddWithValue("@ScheduleNumber", Me.GetParent().ScheduleNumber)
      'cm.Parameters.AddWithValue("@ChannelShortName", Me.GetParent.ChannelShortName)
      cm.Parameters.AddWithValue("@ProductionID", Me.GetParent.ProductionID)
      cm.Parameters.AddWithValue("@Series", Me.GetParent.Series)
      cm.Parameters.AddWithValue("@EventTitle", Me.GetParent.Title)
      'cm.Parameters.AddWithValue("@ProductionChannelID", GetProperty(ProductionChannelIDProperty))
      'cm.Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
      cm.Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
      cm.Parameters.AddWithValue("@Room", GetProperty(RoomProperty))
      'cm.Parameters.AddWithValue("@OwnerPSAID", GetProperty(OwnerPSAIDProperty))
      cm.Parameters.AddWithValue("@RoomTimes", GetProperty(RoomTimesProperty))
      cm.Parameters.AddWithValue("@Status", GetProperty(StatusProperty))
      'cm.Parameters.AddWithValue("@CssClass", GetProperty(CssClassProperty))
      cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      cm.Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@CallTime", GetProperty(CallTimeProperty))
      cm.Parameters.AddWithValue("@StartDateTime", GetProperty(StartDateTimeProperty))
      cm.Parameters.AddWithValue("@EndDateTime", GetProperty(EndDateTimeProperty))
      cm.Parameters.AddWithValue("@WrapTime", GetProperty(EndDateTimeProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@ModifiedByName", OBLib.Security.Settings.CurrentUser.FirstNameSurname)
      cm.Parameters.AddWithValue("@ChannelNames", GetChannelNames)
      cm.Parameters.AddWithValue("@CrewDescriptions", GetCrewDescriptions)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(RoomScheduleIDProperty, cm.Parameters("@RoomScheduleID").Value)
                 LoadProperty(OwnerPSAIDProperty, cm.Parameters("@OwnerPSAID").Value)
                 LoadProperty(CallTimeTimelineIDProperty, cm.Parameters("@CallTimeTimelineID").Value)
                 LoadProperty(OnAirTimeTimelineIDProperty, cm.Parameters("@OnAirTimeTimelineID").Value)
                 LoadProperty(WrapTimeTimelineIDProperty, cm.Parameters("@WrapTimeTimelineID").Value)
                 LoadProperty(ResourceBookingIDProperty, cm.Parameters("@ResourceBookingID").Value)
               End If
               LoadProperty(CssClassProperty, cm.Parameters("@CssClass").Value)
               LoadProperty(ResourceBookingDescriptionProperty, cm.Parameters("@ResourceBookingDescription").Value)
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(RoomSchedHrListProperty))
      UpdateChild(GetProperty(RoomSchedChListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@RoomScheduleID", GetProperty(RoomScheduleIDProperty))
      cm.Parameters.AddWithValue("@ProductionSystemAreaID", GetProperty(OwnerPSAIDProperty))
      cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
    End Sub

#End Region

    Sub LoadChannelCount()
      LoadProperty(ChannelCountProperty, Me.RoomSchedChList.Count)
    End Sub

  End Class

End Namespace