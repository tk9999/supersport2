﻿' Generated 21 Nov 2016 13:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class RoomSchedList
    Inherits OBBusinessListBase(Of RoomSchedList, RoomSched)

#Region " Business Methods "

    Public Function GetItem(RoomScheduleID As Integer) As RoomSched

      For Each child As RoomSched In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByPSAID(ProductionSystemAreaID As Integer) As RoomSched

      For Each child As RoomSched In Me
        If child.OwnerPSAID = ProductionSystemAreaID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetRoomSchedHr(ProductionHRID As Integer) As SchEvtChRmHr

      Dim obj As SchEvtChRmHr = Nothing
      For Each parent As RoomSched In Me
        obj = parent.SchEvtChRmHrList.GetItem(ProductionHRID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewRoomSchedList() As RoomSchedList

      Return New RoomSchedList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace