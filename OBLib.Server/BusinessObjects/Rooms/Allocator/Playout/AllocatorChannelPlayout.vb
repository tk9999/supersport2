﻿' Generated 28 Jul 2016 08:01 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class AllocatorChannelPlayout
    Inherits OBBusinessBase(Of AllocatorChannelPlayout)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
    Public Property ChannelID() As Integer
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ChannelIDProperty, Value)
      End Set
    End Property

    Public Shared ChannelNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelName, "Channel Name")
    ''' <summary>
    ''' Gets and sets the Channel Name value
    ''' </summary>
    <Display(Name:="Channel Name", Description:="")>
    Public Property ChannelName() As String
      Get
        Return GetProperty(ChannelNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelNameProperty, Value)
      End Set
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel Short Name")
    ''' <summary>
    ''' Gets and sets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel Short Name", Description:="")>
    Public Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelShortNameProperty, Value)
      End Set
    End Property

    Public Shared HDIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HDInd, "HD", False)
    ''' <summary>
    ''' Gets and sets the HD value
    ''' </summary>
    <Display(Name:="HD", Description:=""),
    Required(ErrorMessage:="HD required")>
    Public Property HDInd() As Boolean
      Get
        Return GetProperty(HDIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HDIndProperty, Value)
      End Set
    End Property

    Public Shared PriorityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Priority, "Priority")
    ''' <summary>
    ''' Gets and sets the Priority value
    ''' </summary>
    <Display(Name:="Priority", Description:=""),
    Required(ErrorMessage:="Priority required")>
    Public Property Priority() As Integer
      Get
        Return GetProperty(PriorityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PriorityProperty, Value)
      End Set
    End Property

    Public Shared PrimaryIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryInd, "Primary", False)
    ''' <summary>
    ''' Gets and sets the Primary value
    ''' </summary>
    <Display(Name:="Primary", Description:=""),
    Required(ErrorMessage:="Primary required")>
    Public Property PrimaryInd() As Boolean
      Get
        Return GetProperty(PrimaryIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(PrimaryIndProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared AllocatorChannelPlayoutEventListProperty As PropertyInfo(Of AllocatorChannelPlayoutEventList) = RegisterProperty(Of AllocatorChannelPlayoutEventList)(Function(c) c.AllocatorChannelPlayoutEventList, "Allocator Channel Event List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property AllocatorChannelPlayoutEventList() As AllocatorChannelPlayoutEventList
      Get
        If GetProperty(AllocatorChannelPlayoutEventListProperty) Is Nothing Then
          LoadProperty(AllocatorChannelPlayoutEventListProperty, Rooms.AllocatorChannelPlayoutEventList.NewAllocatorChannelPlayoutEventList())
        End If
        Return GetProperty(AllocatorChannelPlayoutEventListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ChannelIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ChannelName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Allocator Channel Playout")
        Else
          Return String.Format("Blank {0}", "Allocator Channel Playout")
        End If
      Else
        Return Me.ChannelName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAllocatorChannelPlayout() method.

    End Sub

    Public Shared Function NewAllocatorChannelPlayout() As AllocatorChannelPlayout

      Return DataPortal.CreateChild(Of AllocatorChannelPlayout)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetAllocatorChannelPlayout(dr As SafeDataReader) As AllocatorChannelPlayout

      Dim a As New AllocatorChannelPlayout()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ChannelIDProperty, .GetInt32(0))
          LoadProperty(ChannelNameProperty, .GetString(1))
          LoadProperty(ChannelShortNameProperty, .GetString(2))
          LoadProperty(HDIndProperty, .GetBoolean(3))
          LoadProperty(PriorityProperty, .GetInt32(4))
          LoadProperty(PrimaryIndProperty, .GetBoolean(5))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAllocatorChannelPlayout"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAllocatorChannelPlayout"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramChannelID As SqlParameter = .Parameters.Add("@ChannelID", SqlDbType.Int)
          paramChannelID.Value = GetProperty(ChannelIDProperty)
          If Me.IsNew Then
            paramChannelID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ChannelName", GetProperty(ChannelNameProperty))
          .Parameters.AddWithValue("@ChannelShortName", GetProperty(ChannelShortNameProperty))
          .Parameters.AddWithValue("@HDInd", GetProperty(HDIndProperty))
          .Parameters.AddWithValue("@Priority", GetProperty(PriorityProperty))
          .Parameters.AddWithValue("@PrimaryInd", GetProperty(PrimaryIndProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ChannelIDProperty, paramChannelID.Value)
          End If
          ' update child objects
          If GetProperty(AllocatorChannelPlayoutEventListProperty) IsNot Nothing Then
            Me.AllocatorChannelPlayoutEventList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(AllocatorChannelPlayoutEventListProperty) IsNot Nothing Then
          Me.AllocatorChannelPlayoutEventList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAllocatorChannelPlayout"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ChannelID", GetProperty(ChannelIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End Region

  End Class

End Namespace
