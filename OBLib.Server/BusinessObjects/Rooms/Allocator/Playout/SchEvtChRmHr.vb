﻿' Generated 21 Nov 2016 13:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class SchEvtChRmHr
    Inherits OBBusinessBase(Of SchEvtChRmHr)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SchEvtChRmHrBO.SchEvtChRmHrBOToString(self)")

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHRID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property ProductionHRID() As Integer
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionHRIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required"),
    DropDownWeb(GetType(ROPlayoutOperatorAvailabilityList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SchEvtChRmHrBO.setHRCriteriaBeforeRefresh", PreFindJSFunction:="SchEvtChRmHrBO.triggerHRAutoPopulate", AfterFetchJS:="SchEvtChRmHrBO.afterHRRefreshAjax",
                OnItemSelectJSFunction:="SchEvtChRmHrBO.onHRSelected", LookupMember:="HRName", DisplayMember:="HRName", ValueMember:="HumanResourceID",
                DropDownCssClass:="mcrc-dropdown", DropDownColumns:={"HRName", "TotalHours", "IsAvailableString", "ShiftType", "ShiftTimesString", "ShiftDurationString",
                                                                     "Room", "RoomScheduleTitle", "RoomChannels"}),
    SetExpression("SchEvtChRmHrBO.HumanResourceIDSet(self)")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property
    ',
    '"ShiftStartString", "ShiftEndString",
    '"CurrentShiftDuration", "ExpectedShiftDuration",
    '"NextShift", "TimetoNextShiftString"

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "Resource Booking")
    ''' <summary>
    ''' Gets and sets the Resource Booking value
    ''' </summary>
    <Display(Name:="Resource Booking", Description:="")>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CallTime, "Call Time")
    ''' <summary>
    ''' Gets and sets the Call Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    Required(ErrorMessage:="Call Time required"),
    SetExpression("SchEvtChRmHrBO.CallTimeSet(self)")>
    Public Property CallTime As DateTime?
      Get
        Return GetProperty(CallTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
    SetExpression("SchEvtChRmHrBO.StartTimeSet(self)")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
    SetExpression("SchEvtChRmHrBO.EndTimeSet(self)")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.WrapTime, "Wrap Time")
    ''' <summary>
    ''' Gets and sets the Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    SetExpression("SchEvtChRmHrBO.WrapTimeSet(self)")>
    Public Property WrapTime As DateTime?
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeProperty, Value)
      End Set
    End Property

    Public Shared HRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRName, "HR Name")
    ''' <summary>
    ''' Gets and sets the HR Name value
    ''' </summary>
    <Display(Name:="HR Name", Description:="")>
    Public Property HRName() As String
      Get
        Return GetProperty(HRNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HRNameProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    Required(ErrorMessage:="Discipline required"),
    DropDownWeb(GetType(RODisciplineSelectList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SchEvtChRmHrBO.setDisciplineCriteriaBeforeRefresh", PreFindJSFunction:="SchEvtChRmHrBO.triggerDisciplineAutoPopulate", AfterFetchJS:="SchEvtChRmHrBO.afterDisciplineRefreshAjax",
                OnItemSelectJSFunction:="SchEvtChRmHrBO.onDisciplineSelected", LookupMember:="Discipline", DisplayMember:="Discipline", ValueMember:="DisciplineID",
                DropDownCssClass:="mcrc-dropdown", DropDownColumns:={"Discipline"}),
    SetExpression("SchEvtChRmHrBO.DisciplineIDSet(self)")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets and sets the Disciplines value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared CallTimeCSDIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CallTimeCSDID, "Call Time CSD")
    ''' <summary>
    ''' Gets and sets the Call Time CSD value
    ''' </summary>
    <Display(Name:="Call Time CSD", Description:=""),
    Required(ErrorMessage:="Call Time CSD required")>
    Public Property CallTimeCSDID() As Integer
      Get
        Return GetProperty(CallTimeCSDIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CallTimeCSDIDProperty, Value)
      End Set
    End Property

    Public Shared OnAirTimeCSDIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OnAirTimeCSDID, "On Air Time CSD")
    ''' <summary>
    ''' Gets and sets the On Air Time CSD value
    ''' </summary>
    <Display(Name:="On Air Time CSD", Description:=""),
    Required(ErrorMessage:="On Air Time CSD required")>
    Public Property OnAirTimeCSDID() As Integer
      Get
        Return GetProperty(OnAirTimeCSDIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OnAirTimeCSDIDProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeCSDIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WrapTimeCSDID, "Wrap Time CSD")
    ''' <summary>
    ''' Gets and sets the Wrap Time CSD value
    ''' </summary>
    <Display(Name:="Wrap Time CSD", Description:=""),
    Required(ErrorMessage:="Wrap Time CSD required")>
    Public Property WrapTimeCSDID() As Integer
      Get
        Return GetProperty(WrapTimeCSDIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WrapTimeCSDIDProperty, Value)
      End Set
    End Property

    Public Shared BookingTimesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BookingTimes, "Booking Times")
    ''' <summary>
    ''' Gets and sets the Booking Times value
    ''' </summary>
    <Display(Name:="Booking Times", Description:="")>
    Public Property BookingTimes() As String
      Get
        Return GetProperty(BookingTimesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BookingTimesProperty, Value)
      End Set
    End Property

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property ProductionHumanResourceID() As Integer
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ClashesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Clashes, "Clashes")
    ''' <summary>
    ''' Gets and sets the Booking Times value
    ''' </summary>
    <Display(Name:="Clashes", Description:=""), AlwaysClean>
    Public Property Clashes() As String
      Get
        Return GetProperty(ClashesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ClashesProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "HumanResourceShiftID", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="HumanResourceShiftID", Description:="")>
    Public Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceShiftRoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftRoomScheduleID, "HumanResourceShiftRoomScheduleID", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="HumanResourceShiftRoomScheduleID", Description:="")>
    Public Property HumanResourceShiftRoomScheduleID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftRoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftRoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared EditTimesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.EditTimes, "EditTimes", False)
    ''' <summary>
    ''' Gets and sets the Wrap Time CSD value
    ''' </summary>
    <Display(Name:="EditTimes", Description:="")>
    Public Property EditTimes() As Boolean
      Get
        Return GetProperty(EditTimesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(EditTimesProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Shared ClashListProperty As PropertyInfo(Of List(Of String)) = RegisterProperty(Of List(Of String))(Function(c) c.ClashList, "Clash List")
    Public Property ClashList() As List(Of String)
      Get
        If GetProperty(ClashListProperty) Is Nothing Then
          LoadProperty(ClashListProperty, New List(Of String))
        End If
        Return GetProperty(ClashListProperty)
      End Get
      Set(value As List(Of String))
        SetProperty(ClashListProperty, value)
      End Set
    End Property

    Public Shared ClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ClashCount, "Clash Count", 0)
    Public Property ClashCount() As Integer
      Get
        Return GetProperty(ClashCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(ClashCountProperty, value)
      End Set
    End Property

    Public Function GetParent() As RoomSched

      Return CType(CType(Me.Parent, SchEvtChRmHrList).Parent, RoomSched)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHRIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HRName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sch Evt Ch Rm Hr")
        Else
          Return String.Format("Blank {0}", "Sch Evt Ch Rm Hr")
        End If
      Else
        Return Me.HRName
      End If

    End Function

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "ResourceBookingDescription")
    ''' <summary>
    ''' Gets and sets the Booking Times value
    ''' </summary>
    <Display(Name:="ResourceBookingDescription", Description:="")>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(HumanResourceIDProperty)
        .AddTriggerProperties({CallTimeProperty, StartDateTimeProperty, EndDateTimeProperty, WrapTimeProperty})
        .JavascriptRuleFunctionName = "SchEvtChRmHrBO.HumanResourceIDValid"
        .ServerRuleFunction = AddressOf ClashesValid
        .AffectedProperties.AddRange({CallTimeProperty, StartDateTimeProperty, EndDateTimeProperty, WrapTimeProperty})
      End With

    End Sub

    Public Shared Function ClashesValid(SchEvtChRmHr As SchEvtChRmHr) As String

      If SchEvtChRmHr.Clashes.Length > 0 Then
        Return "Clashes for " & SchEvtChRmHr.HRName & " were detected"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSchEvtChRmHr() method.

    End Sub

    Public Shared Function NewSchEvtChRmHr() As SchEvtChRmHr

      Return DataPortal.CreateChild(Of SchEvtChRmHr)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSchEvtChRmHr(dr As SafeDataReader) As SchEvtChRmHr

      Dim s As New SchEvtChRmHr()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionHRIDProperty, .GetInt32(0))
          'LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          'LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(CallTimeProperty, .GetValue(4))
          LoadProperty(StartDateTimeProperty, .GetValue(5))
          LoadProperty(EndDateTimeProperty, .GetValue(6))
          LoadProperty(WrapTimeProperty, .GetValue(7))
          LoadProperty(HRNameProperty, .GetString(8))
          LoadProperty(DisciplineProperty, .GetString(9))
          LoadProperty(CallTimeCSDIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(OnAirTimeCSDIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(WrapTimeCSDIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          'LoadProperty(CallTimeTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          'LoadProperty(OnAirTimeTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          'LoadProperty(WrapTimeTimelineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(BookingTimesProperty, .GetString(13))
          LoadProperty(ProductionHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(HumanResourceShiftRoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      If Me.IsNew Then
        cm.CommandText = "[InsProcsWeb].[insRoomScheduleAreaProductionHRBookingPlayout]"
      Else
        cm.CommandText = "[UpdProcsWeb].[updRoomScheduleAreaProductionHRBookingPlayout]"
      End If

      AddPrimaryKeyParam(cm, ProductionHRIDProperty)
      AddInputOutputParam(cm, ProductionHumanResourceIDProperty)
      AddInputOutputParam(cm, ResourceBookingIDProperty)
      AddInputOutputParam(cm, CallTimeCSDIDProperty)
      AddInputOutputParam(cm, OnAirTimeCSDIDProperty)
      AddInputOutputParam(cm, WrapTimeCSDIDProperty)
      AddInputOutputParam(cm, HumanResourceShiftIDProperty)
      AddInputOutputParam(cm, HumanResourceShiftRoomScheduleIDProperty)
      SetProperty(ResourceBookingDescriptionProperty, Me.GetParent.ResourceBookingDescription)
      AddInputOutputString(cm, ResourceBookingDescriptionProperty, 500)

      cm.Parameters.AddWithValue("@RoomScheduleID", Me.GetParent().RoomScheduleID)
      cm.Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent.OwnerPSAID)
      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
      cm.Parameters.AddWithValue("@CallTime", CallTime)
      cm.Parameters.AddWithValue("@StartDateTime", StartDateTime)
      cm.Parameters.AddWithValue("@EndDateTime", EndDateTime)
      cm.Parameters.AddWithValue("@WrapTime", EndDateTime)
      cm.Parameters.AddWithValue("@HRName", GetProperty(HRNameProperty))
      cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(GetProperty(DisciplineIDProperty)))
      cm.Parameters.AddWithValue("@Discipline", GetProperty(DisciplineProperty))
      cm.Parameters.AddWithValue("@CallTimeTimelineID", Me.GetParent.CallTimeTimelineID)
      cm.Parameters.AddWithValue("@OnAirTimeTimelineID", Me.GetParent.OnAirTimeTimelineID)
      cm.Parameters.AddWithValue("@WrapTimeTimelineID", Me.GetParent.WrapTimeTimelineID)
      'cm.Parameters.AddWithValue("@BookingTimes", GetProperty(BookingTimesProperty))
      cm.Parameters.AddWithValue("@StatusCssClass", Me.GetParent.CssClass)
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@SystemID", Me.GetParent.SystemID)
      cm.Parameters.AddWithValue("@ProductionAreaID", Me.GetParent.ProductionAreaID)
      cm.Parameters.AddWithValue("@ProductionID", Me.GetParent.GetParent.ProductionID)
      cm.Parameters.AddWithValue("@RoomID", Me.GetParent.RoomID)
      ' cm.Parameters.AddWithValue("@ResourceBookingDescription", Me.GetParent.ResourceBookingDescription)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionHumanResourceIDProperty, cm.Parameters("@ProductionHumanResourceID").Value)
                 LoadProperty(ProductionHRIDProperty, cm.Parameters("@ProductionHRID").Value)
                 LoadProperty(ResourceBookingIDProperty, cm.Parameters("@ResourceBookingID").Value)
                 LoadProperty(CallTimeCSDIDProperty, cm.Parameters("@CallTimeCSDID").Value)
                 LoadProperty(OnAirTimeCSDIDProperty, cm.Parameters("@OnAirTimeCSDID").Value)
                 LoadProperty(WrapTimeCSDIDProperty, cm.Parameters("@WrapTimeCSDID").Value)
                 LoadProperty(ProductionSystemAreaIDProperty, Me.GetParent.OwnerPSAID)
                 LoadProperty(HumanResourceShiftIDProperty, OBLib.OBMisc.DBNullNothing(cm.Parameters("@HumanResourceShiftID").Value))
                 LoadProperty(HumanResourceShiftRoomScheduleIDProperty, OBLib.OBMisc.DBNullNothing(cm.Parameters("@HumanResourceShiftRoomScheduleID").Value))
                 LoadProperty(ResourceBookingDescriptionProperty, cm.Parameters("@ResourceBookingDescription").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.CommandText = "[DelProcsWeb].[delRoomScheduleAreaProductionHRBookingPlayout]"
      cm.Parameters.AddWithValue("@ProductionHRID", GetProperty(ProductionHRIDProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@HumanResourceShiftID", GetProperty(HumanResourceShiftIDProperty))
    End Sub

#End Region

  End Class

End Namespace