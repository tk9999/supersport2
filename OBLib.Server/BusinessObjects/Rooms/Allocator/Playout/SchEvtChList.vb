﻿' Generated 21 Nov 2016 13:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class SchEvtChList
    Inherits OBBusinessListBase(Of SchEvtChList, SchEvtCh)

#Region " Business Methods "

    Public Function GetItem(ScheduleNumber As Integer) As SchEvtCh

      For Each child As SchEvtCh In Me
        If child.ScheduleNumber = ScheduleNumber Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSchEvtChList() As SchEvtChList

      Return New SchEvtChList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace