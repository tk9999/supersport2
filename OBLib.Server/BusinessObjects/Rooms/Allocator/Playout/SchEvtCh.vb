﻿' Generated 21 Nov 2016 13:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()>
  Public Class SchEvtCh
    Inherits OBBusinessBase(Of SchEvtCh)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SchEvtChBO.SchEvtChBOToString(self)")

    Public Shared ScheduleNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ScheduleNumber, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property ScheduleNumber() As Integer
      Get
        Return GetProperty(ScheduleNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ScheduleNumberProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref Number")
    ''' <summary>
    ''' Gets the Gen Ref Number value
    ''' </summary>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(value As Int64)
        SetProperty(GenRefNumberProperty, value)
      End Set
    End Property

    Public Shared EventStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventStatus, "Event Status")
    ''' <summary>
    ''' Gets and sets the Event Status value
    ''' </summary>
    <Display(Name:="Event Status", Description:="")>
    Public Property EventStatus() As String
      Get
        Return GetProperty(EventStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventStatusProperty, Value)
      End Set
    End Property

    Public Shared ScheduleDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ScheduleDateTime, "Schedule Date Time")
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Schedule Date Time", Description:=""),
    Required(ErrorMessage:="Schedule Date Time required")>
    Public Property ScheduleDateTime As Date
      Get
        Return GetProperty(ScheduleDateTimeProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(ScheduleDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ScheduleEndDate, "Schedule End Date")
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="Schedule End Date", Description:=""),
    Required(ErrorMessage:="Schedule End Date required")>
    Public Property ScheduleEndDate As Date
      Get
        Return GetProperty(ScheduleEndDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(ScheduleEndDateProperty, Value)
      End Set
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel Short Name")
    ''' <summary>
    ''' Gets and sets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel Short Name", Description:="")>
    Public Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelShortNameProperty, Value)
      End Set
    End Property

    Public Shared IsPrimaryChannelProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsPrimaryChannel, "Is Primary Channel", False)
    ''' <summary>
    ''' Gets and sets the Is Primary Channel value
    ''' </summary>
    <Display(Name:="Is Primary Channel", Description:=""),
    Required(ErrorMessage:="Is Primary Channel required")>
    Public Property IsPrimaryChannel() As Boolean
      Get
        Return GetProperty(IsPrimaryChannelProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsPrimaryChannelProperty, Value)
      End Set
    End Property

    Public Shared GenreSeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreSeries, "Genre Series")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Genre Series", Description:="")>
    Public Property GenreSeries() As String
      Get
        Return GetProperty(GenreSeriesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreSeriesProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared IsHighlightsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsHighlights, "Is Highlights", False)
    ''' <summary>
    ''' Gets and sets the Is Highlights value
    ''' </summary>
    <Display(Name:="Is Highlights", Description:=""),
    Required(ErrorMessage:="Is Highlights required")>
    Public Property IsHighlights() As Boolean
      Get
        Return GetProperty(IsHighlightsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsHighlightsProperty, Value)
      End Set
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.LiveDate, "Live Date")
    ''' <summary>
    ''' Gets and sets the Live Date value
    ''' </summary>
    <Display(Name:="Live Date", Description:=""),
    Required(ErrorMessage:="Live Date required")>
    Public Property LiveDate As Date
      Get
        Return GetProperty(LiveDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(LiveDateProperty, Value)
      End Set
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue")
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VenueProperty, Value)
      End Set
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationProperty, Value)
      End Set
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets and sets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:=""),
    Required(ErrorMessage:="Row No required")>
    Public Property RowNo() As Integer
      Get
        Return GetProperty(RowNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RowNoProperty, Value)
      End Set
    End Property

    Public Shared RepRnkProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RepRnk, "Rep Rnk")
    ''' <summary>
    ''' Gets and sets the Rep Rnk value
    ''' </summary>
    <Display(Name:="Rep Rnk", Description:=""),
    Required(ErrorMessage:="Rep Rnk required")>
    Public Property RepRnk() As Integer
      Get
        Return GetProperty(RepRnkProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RepRnkProperty, Value)
      End Set
    End Property

    Public Shared ButtonStyleCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ButtonStyleCssClass, "Button Style Css Class")
    ''' <summary>
    ''' Gets and sets the Button Style Css Class value
    ''' </summary>
    <Display(Name:="Button Style Css Class", Description:="")>
    Public Property ButtonStyleCssClass() As String
      Get
        Return GetProperty(ButtonStyleCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ButtonStyleCssClassProperty, Value)
      End Set
    End Property

    Public Shared ScheduleCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ScheduleCssClass, "Schedule Css Class")
    ''' <summary>
    ''' Gets and sets the Schedule Css Class value
    ''' </summary>
    <Display(Name:="Schedule Css Class", Description:="")>
    Public Property ScheduleCssClass() As String
      Get
        Return GetProperty(ScheduleCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ScheduleCssClassProperty, Value)
      End Set
    End Property

    Public Shared ProductionChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionChannelID, "Production")
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:=""),
    Required(ErrorMessage:="Production required")>
    Public Property ProductionChannelID() As Integer
      Get
        Return GetProperty(ProductionChannelIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionChannelIDProperty, Value)
      End Set
    End Property

    Public Shared BookingCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BookingCount, "Booking Count")
    ''' <summary>
    ''' Gets and sets the Booking Count value
    ''' </summary>
    <Display(Name:="Booking Count", Description:=""),
    Required(ErrorMessage:="Booking Count required")>
    Public Property BookingCount() As Integer
      Get
        Return GetProperty(BookingCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(BookingCountProperty, Value)
      End Set
    End Property

    Public Shared SponsorshipCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SponsorshipCount, "Sponsorship Count")
    ''' <summary>
    ''' Gets and sets the Booking Count value
    ''' </summary>
    <Display(Name:="Sponsorship Count", Description:="")>
    Public Property SponsorshipCount() As Integer
      Get
        Return GetProperty(SponsorshipCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SponsorshipCountProperty, Value)
      End Set
    End Property

    Public Shared SponsorshipButtonCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SponsorshipButtonCss, "SponsorshipButtonCss")
    ''' <summary>
    ''' Gets and sets the Button Style Css Class value
    ''' </summary>
    <Display(Name:="sponsorshipButtonCss", Description:="")>
    Public Property SponsorshipButtonCss() As String
      Get
        Return GetProperty(SponsorshipButtonCssProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SponsorshipButtonCssProperty, Value)
      End Set
    End Property

    Public Shared ScheduledTimesStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ScheduledTimesString, "Times")
    ''' <summary>
    ''' Gets and sets the Button Style Css Class value
    ''' </summary>
    <Display(Name:="Times", Description:="")>
    Public Property ScheduledTimesString() As String
      Get
        Return GetProperty(ScheduledTimesStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ScheduledTimesStringProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Quick Room", Description:=""),
    DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="SchEvtChBO.setRoomIDCriteriaBeforeRefresh",
                 PreFindJSFunction:="SchEvtChBO.triggerRoomIDAutoPopulate",
                 AfterFetchJS:="SchEvtChBO.afterRoomIDRefreshAjax",
                 LookupMember:="Room", ValueMember:="RoomID", DropDownColumns:={"Room", "IsAvailableString"},
                 OnItemSelectJSFunction:="SchEvtChBO.onRoomSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("SchEvtChBO.RoomIDSet(self)")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared ChangeAlertProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangeAlert, "ChangeAlert")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="ChangeAlert")>
    Public Property ChangeAlert() As String
      Get
        Return GetProperty(ChangeAlertProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChangeAlertProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SchEvt

      Return CType(CType(Me.Parent, SchEvtChList).Parent, SchEvt)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ScheduleNumberProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EventStatus.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sch Evt Ch")
        Else
          Return String.Format("Blank {0}", "Sch Evt Ch")
        End If
      Else
        Return Me.EventStatus
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSchEvtCh() method.

    End Sub

    Public Shared Function NewSchEvtCh() As SchEvtCh

      Return DataPortal.CreateChild(Of SchEvtCh)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSchEvtCh(dr As SafeDataReader) As SchEvtCh

      Dim s As New SchEvtCh()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ScheduleNumberProperty, .GetInt32(0))
          LoadProperty(GenRefNumberProperty, .GetInt64(1))
          LoadProperty(EventStatusProperty, .GetString(2))
          LoadProperty(ScheduleDateTimeProperty, .GetValue(3))
          LoadProperty(ScheduleEndDateProperty, .GetValue(4))
          LoadProperty(ChannelShortNameProperty, .GetString(5))
          LoadProperty(IsPrimaryChannelProperty, .GetBoolean(6))
          LoadProperty(GenreSeriesProperty, .GetString(7))
          LoadProperty(TitleProperty, .GetString(8))
          LoadProperty(IsHighlightsProperty, .GetBoolean(9))
          LoadProperty(LiveDateProperty, .GetValue(10))
          LoadProperty(VenueProperty, .GetString(11))
          LoadProperty(LocationProperty, .GetString(12))
          LoadProperty(RowNoProperty, .GetInt32(13))
          LoadProperty(RepRnkProperty, .GetInt32(14))
          LoadProperty(ButtonStyleCssClassProperty, .GetString(15))
          LoadProperty(ProductionChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          LoadProperty(BookingCountProperty, .GetInt32(17))
          LoadProperty(SponsorshipCountProperty, .GetInt32(18))
          LoadProperty(ProductionChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
          LoadProperty(SponsorshipButtonCssProperty, .GetString(20))
          LoadProperty(ScheduledTimesStringProperty, .GetString(21))
          LoadProperty(ScheduleCssClassProperty, .GetString(22))
          LoadProperty(ChangeAlertProperty, .GetString(24))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      cm.CommandText = "[InsProcsWeb].[insSchEvtCh]"

      AddPrimaryKeyParam(cm, ScheduleNumberProperty)
      AddInputOutputParam(cm, ProductionChannelIDProperty)
      If CType(Me.Parent, SchEvtChList).Parent IsNot Nothing Then
        cm.Parameters.AddWithValue("@ProductionID", Me.GetParent.ProductionID)
      Else
        cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(Nothing))
      End If
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      cm.Parameters.AddWithValue("@ChangeAlert", GetProperty(ChangeAlertProperty))

      'cm.Parameters.AddWithValue("@GenRefNumber", Me.GetParent().GenRefNumber)
      'cm.Parameters.AddWithValue("@EventStatus", GetProperty(EventStatusProperty))
      'cm.Parameters.AddWithValue("@ScheduleDateTime", ScheduleDateTime)
      'cm.Parameters.AddWithValue("@ScheduleEndDate", ScheduleEndDate)
      'cm.Parameters.AddWithValue("@ChannelShortName", GetProperty(ChannelShortNameProperty))
      'cm.Parameters.AddWithValue("@IsPrimaryChannel", GetProperty(IsPrimaryChannelProperty))
      'cm.Parameters.AddWithValue("@GenreSeries", GetProperty(GenreSeriesProperty))
      'cm.Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      'cm.Parameters.AddWithValue("@IsHighlights", GetProperty(IsHighlightsProperty))
      'cm.Parameters.AddWithValue("@LiveDate", LiveDate)
      'cm.Parameters.AddWithValue("@Venue", GetProperty(VenueProperty))
      'cm.Parameters.AddWithValue("@Location", GetProperty(LocationProperty))
      'cm.Parameters.AddWithValue("@RowNo", GetProperty(RowNoProperty))
      'cm.Parameters.AddWithValue("@RepRnk", GetProperty(RepRnkProperty))
      'cm.Parameters.AddWithValue("@ButtonStyleCssClass", GetProperty(ButtonStyleCssClassProperty))
      'cm.Parameters.AddWithValue("@BookingCount", GetProperty(BookingCountProperty))

      Return Sub()
               'Post Save
               'If Me.IsNew Then
               LoadProperty(ScheduleNumberProperty, cm.Parameters("@ScheduleNumber").Value)
               LoadProperty(ProductionChannelIDProperty, cm.Parameters("@ProductionChannelID").Value)
               'End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()


    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ScheduleNumber", GetProperty(ScheduleNumberProperty))
    End Sub

#End Region

  End Class

End Namespace