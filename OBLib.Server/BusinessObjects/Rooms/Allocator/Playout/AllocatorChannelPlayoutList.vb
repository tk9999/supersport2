﻿' Generated 28 Jul 2016 08:01 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.General.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class AllocatorChannelPlayoutList
    Inherits OBBusinessListBase(Of AllocatorChannelPlayoutList, AllocatorChannelPlayout)

#Region " Business Methods "

    Public Function GetItem(ChannelID As Integer) As AllocatorChannelPlayout

      For Each child As AllocatorChannelPlayout In Me
        If child.ChannelID = ChannelID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetAllocatorChannelPlayoutEvent(ImportedEventChannelID As Integer) As AllocatorChannelPlayoutEvent

      Dim obj As AllocatorChannelPlayoutEvent = Nothing
      For Each parent As AllocatorChannelPlayout In Me
        obj = parent.AllocatorChannelPlayoutEventList.GetItem(ImportedEventChannelID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Singular.DataAnnotations.DropDownWeb(GetType(ROUserSystemList),
                                            DisplayMember:="System", ValueMember:="SystemID"),
      Display(Name:="Sub-Dept"),
      Required(ErrorMessage:="Sub-Dept is required"),
      SetExpression("AllocatorChannelListCriteriaBO.SystemIDSet(self)")>
      Public Property SystemID As Integer?

      <Singular.DataAnnotations.DropDownWeb(GetType(ROUserSystemAreaList),
                                            DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", Source:=Singular.DataAnnotations.DropDownWeb.SourceType.CommonData,
                                            ThisFilterMember:="SystemID"),
      Display(Name:="Area"),
      Required(ErrorMessage:="Area is required"),
      SetExpression("AllocatorChannelListCriteriaBO.ProductionAreaIDSet(self)")>
      Public Property ProductionAreaID As Integer?

      <Required(ErrorMessage:="Start Date is required"),
      SetExpression("AllocatorChannelListCriteriaBO.StartDateSet(self)", , )>
      Public Property StartDate As DateTime?

      <Required(ErrorMessage:="End Date is required"),
      SetExpression("AllocatorChannelListCriteriaBO.EndDateSet(self)", , )>
      Public Property EndDate As DateTime?

      Public Shared LiveProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Live, "Live", True)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Live", Description:=""),
      SetExpression("AllocatorChannelListCriteriaBO.LiveSet(self)")>
      Public Property Live() As Boolean
        Get
          Return ReadProperty(LiveProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(LiveProperty, Value)
        End Set
      End Property

      Public Shared DelayedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Delayed, "Delayed", True)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Delayed", Description:=""),
      SetExpression("AllocatorChannelListCriteriaBO.DelayedSet(self)")>
      Public Property Delayed() As Boolean
        Get
          Return ReadProperty(DelayedProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(DelayedProperty, Value)
        End Set
      End Property

      Public Shared PremierProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Premier, "Premier", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Premier", Description:=""),
      SetExpression("AllocatorChannelListCriteriaBO.PremierSet(self)")>
      Public Property Premier() As Boolean
        Get
          Return ReadProperty(PremierProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(PremierProperty, Value)
        End Set
      End Property

      Public Shared GenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNo, "Gen Ref No", Nothing)
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Gen Ref No", Description:=""),
      SetExpression("AllocatorChannelListCriteriaBO.GenRefNoSet(self)", , 250)>
      Public Property GenRefNo() As Int64?
        Get
          Return ReadProperty(GenRefNoProperty)
        End Get
        Set(ByVal Value As Int64?)
          LoadProperty(GenRefNoProperty, Value)
        End Set
      End Property

      Public Shared GenRefNoStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenRefNoString, "Gen Ref No", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Gen Ref No", Description:=""),
      TextField(False, , , ),
      SetExpression("AllocatorChannelListCriteriaBO.GenRefNoStringSet(self)", , 250)>
      Public Property GenRefNoString() As String
        Get
          Return ReadProperty(GenRefNoStringProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(GenRefNoStringProperty, Value)
        End Set
      End Property

      Public Shared PrimaryChannelsOnlyProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryChannelsOnly, "Primary Only", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Primary Channels Only?"),
      SetExpression("AllocatorChannelListCriteriaBO.PrimaryChannelsOnlySet(self)", , )>
      Public Property PrimaryChannelsOnly() As Boolean
        Get
          Return ReadProperty(PrimaryChannelsOnlyProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(PrimaryChannelsOnlyProperty, Value)
        End Set
      End Property

      Public Shared AllChannelsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AllChannels, "All Channels", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="All Channels"),
      SetExpression("AllocatorChannelListCriteriaBO.AllChannelsSet(self)", , )>
      Public Property AllChannels() As Boolean
        Get
          Return ReadProperty(AllChannelsProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(AllChannelsProperty, Value)
        End Set
      End Property

      Public Shared SelectedChannelsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedChannelsDisplay, "Selected Channels", "Select Channels")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Channels")>
      Public Property SelectedChannelsDisplay() As String
        Get
          Return ReadProperty(SelectedChannelsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SelectedChannelsProperty, Value)
        End Set
      End Property

      Public Shared SelectedChannelIDsXMLProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedChannelIDsXML, "Selected Channels", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Channels")>
      Public Property SelectedChannelIDsXML() As String
        Get
          Return ReadProperty(SelectedChannelIDsXMLProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SelectedChannelIDsXMLProperty, Value)
        End Set
      End Property

      Public Shared GenreProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Genre, "Genre", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Genre"),
      TextField(False, , , ),
      SetExpression("AllocatorChannelListCriteriaBO.GenreSet(self)", , 250)>
      Public Property Genre() As String
        Get
          Return ReadProperty(GenreProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(GenreProperty, Value)
        End Set
      End Property

      Public Shared SeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Series, "Series", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Series"),
      TextField(False, , , ),
      SetExpression("AllocatorChannelListCriteriaBO.SeriesSet(self)", , 250)>
      Public Property Series() As String
        Get
          Return ReadProperty(SeriesProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SeriesProperty, Value)
        End Set
      End Property

      Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Title"),
      TextField(False, , , ),
      SetExpression("AllocatorChannelListCriteriaBO.TitleSet(self)", , 250)>
      Public Property Title() As String
        Get
          Return ReadProperty(TitleProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(TitleProperty, Value)
        End Set
      End Property

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Keyword, "Keyword", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Keyword"),
      TextField(False, , , ),
      SetExpression("AllocatorChannelListCriteriaBO.KeywordSet(self)", , 250)>
      Public Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewAllocatorChannelPlayoutList() As AllocatorChannelPlayoutList

      Return New AllocatorChannelPlayoutList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetAllocatorChannelPlayoutList() As AllocatorChannelPlayoutList

      Return DataPortal.Fetch(Of AllocatorChannelPlayoutList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AllocatorChannelPlayout.GetAllocatorChannelPlayout(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As AllocatorChannelPlayout = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ChannelID <> sdr.GetInt32(5) Then
            parent = Me.GetItem(sdr.GetInt32(5))
          End If
          parent.AllocatorChannelPlayoutEventList.RaiseListChangedEvents = False
          parent.AllocatorChannelPlayoutEventList.Add(AllocatorChannelPlayoutEvent.GetAllocatorChannelPlayoutEvent(sdr))
          parent.AllocatorChannelPlayoutEventList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As AllocatorChannelPlayout In Me
        child.CheckRules()
        For Each AllocatorChannelPlayoutEvent As AllocatorChannelPlayoutEvent In child.AllocatorChannelPlayoutEventList
          AllocatorChannelPlayoutEvent.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getRoomAllocatorListByChannelGraphicalPlayout]"
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@GenRefNo", NothingDBNull(crit.GenRefNo))
            cm.Parameters.AddWithValue("@CurrentUserID", NothingDBNull(OBLib.Security.Settings.CurrentUserID))
            cm.Parameters.AddWithValue("@SelectedChannelIDs", Strings.MakeEmptyDBNull(crit.SelectedChannelIDsXML))
            cm.Parameters.AddWithValue("@Genre", Strings.MakeEmptyDBNull(crit.Genre))
            cm.Parameters.AddWithValue("@Series", Strings.MakeEmptyDBNull(crit.Series))
            cm.Parameters.AddWithValue("@Title", Strings.MakeEmptyDBNull(crit.Title))
            cm.Parameters.AddWithValue("@Live", crit.Live)
            cm.Parameters.AddWithValue("@Delayed", crit.Delayed)
            cm.Parameters.AddWithValue("@Premier", crit.Premier)
            cm.Parameters.AddWithValue("@GenRefNoString", Strings.MakeEmptyDBNull(crit.GenRefNoString))
            cm.Parameters.AddWithValue("@Keyword", Strings.MakeEmptyDBNull(crit.Keyword))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace
