﻿' Generated 21 Nov 2016 13:22 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class SchEvtList
    Inherits OBBusinessListBase(Of SchEvtList, SchEvt)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(GenRefNumber As Int64, Optional IsDeleted As Boolean = False) As SchEvt

      For Each child As SchEvt In Me
        If child.GenRefNumber = GenRefNumber AndAlso child.IsDeletedEvent = IsDeleted Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetSchEvtCh(ScheduleNumber As Integer) As SchEvtCh

      Dim obj As SchEvtCh = Nothing
      For Each parent As SchEvt In Me
        obj = parent.SchEvtChList.GetItem(ScheduleNumber)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSchEvtChRm(ProductionSystemAreaID As Integer) As RoomSched

      Dim obj As RoomSched = Nothing
      For Each parent As SchEvt In Me
        obj = parent.RoomSchedList.GetItemByPSAID(ProductionSystemAreaID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSchEvtChRmByRoomScheduleID(RoomScheduleID As Integer) As RoomSched

      Dim obj As RoomSched = Nothing
      For Each parent As SchEvt In Me
        obj = parent.RoomSchedList.GetItem(RoomScheduleID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function



    'Public Function GetRoomSched(GenRefNumber As Int64) As RoomSched

    '  Dim obj As RoomSched = Nothing
    '  For Each parent As SchEvt In Me
    '    obj = parent.RoomSchedList.GetItem(GenRefNumber)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Singular.Paging.PageCriteria(Of Criteria)

      <Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), DisplayMember:="System", ValueMember:="SystemID"),
      Display(Name:="Sub-Dept"),
      Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID As Integer?

      <Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", Source:=Singular.DataAnnotations.DropDownWeb.SourceType.CommonData, ThisFilterMember:="SystemID"),
      Display(Name:="Area"),
      Required(ErrorMessage:="Area is required")>
      Public Property ProductionAreaID As Integer?

      <Required(ErrorMessage:="Start Date is required")>
      Public Property StartDate As DateTime?

      <Required(ErrorMessage:="End Date is required")>
      Public Property EndDate As DateTime?

      Public Shared LiveProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Live, "Live", True)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Live", Description:="")>
      Public Property Live() As Boolean
        Get
          Return ReadProperty(LiveProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(LiveProperty, Value)
        End Set
      End Property

      Public Shared DelayedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Delayed, "Delayed", True)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Delayed", Description:="")>
      Public Property Delayed() As Boolean
        Get
          Return ReadProperty(DelayedProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(DelayedProperty, Value)
        End Set
      End Property

      Public Shared PremierProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Premier, "Premier", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Premier", Description:="")>
      Public Property Premier() As Boolean
        Get
          Return ReadProperty(PremierProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(PremierProperty, Value)
        End Set
      End Property

      Public Shared GenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNo, "Gen Ref No", Nothing)
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Gen Ref No", Description:="")>
      Public Property GenRefNo() As Int64?
        Get
          Return ReadProperty(GenRefNoProperty)
        End Get
        Set(ByVal Value As Int64?)
          LoadProperty(GenRefNoProperty, Value)
        End Set
      End Property

      Public Shared GenRefNoStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenRefNoString, "Gen Ref No", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Gen Ref No", Description:="")>
      Public Property GenRefNoString() As String
        Get
          Return ReadProperty(GenRefNoStringProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(GenRefNoStringProperty, Value)
        End Set
      End Property

      Public Shared PrimaryChannelsOnlyProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.PrimaryChannelsOnly, False) _
                                                                              .AddSetExpression("RoomAllocatorListCriteriaBO.PrimaryChannelsOnlySet(self)")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Primary Channels Only?", Description:="")>
      Public Property PrimaryChannelsOnly() As Boolean
        Get
          Return ReadProperty(PrimaryChannelsOnlyProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(PrimaryChannelsOnlyProperty, Value)
        End Set
      End Property

      Public Shared SelectedChannelsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedChannelsDisplay, "Selected Channels", "Select Channels")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Channels")>
      Public Property SelectedChannelsDisplay() As String
        Get
          Return ReadProperty(SelectedChannelsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SelectedChannelsProperty, Value)
        End Set
      End Property

      Public Shared SelectedChannelIDsXMLProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedChannelIDsXML, "Selected Channels", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Channels")>
      Public Property SelectedChannelIDsXML() As String
        Get
          Return ReadProperty(SelectedChannelIDsXMLProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SelectedChannelIDsXMLProperty, Value)
        End Set
      End Property

      Public Shared GenreProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Genre, "Genre", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Genre")>
      Public Property Genre() As String
        Get
          Return ReadProperty(GenreProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(GenreProperty, Value)
        End Set
      End Property

      Public Shared SeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Series, "Series", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Series")>
      Public Property Series() As String
        Get
          Return ReadProperty(SeriesProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SeriesProperty, Value)
        End Set
      End Property

      Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Title")>
      Public Property Title() As String
        Get
          Return ReadProperty(TitleProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(TitleProperty, Value)
        End Set
      End Property

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Keyword, "Keyword", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Keyword")>
      Public Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?,
                     StartDate As DateTime?, EndDate As DateTime?)

        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.StartDate = StartDate
        Me.EndDate = EndDate

      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewSchEvtList() As SchEvtList

      Return New SchEvtList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSchEvtList() As SchEvtList

      Return DataPortal.Fetch(Of SchEvtList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      While sdr.Read
        Me.Add(SchEvt.GetSchEvt(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SchEvt = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.GenRefNumber <> sdr.GetInt64(1) OrElse parent.IsDeletedEvent <> sdr.GetBoolean(23) Then
            parent = Me.GetItem(sdr.GetInt64(1), sdr.GetBoolean(23))
          End If
          parent.SchEvtChList.RaiseListChangedEvents = False
          parent.SchEvtChList.Add(SchEvtCh.GetSchEvtCh(sdr))
          parent.SchEvtChList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.GenRefNumber <> sdr.GetInt64(19) OrElse parent.IsDeletedEvent <> sdr.GetBoolean(20) Then
            parent = Me.GetItem(sdr.GetInt64(19), sdr.GetBoolean(20))
          End If
          parent.RoomSchedList.RaiseListChangedEvents = False
          parent.RoomSchedList.Add(RoomSched.GetRoomSched(sdr))
          parent.RoomSchedList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChildChild As RoomSched = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChildChild Is Nothing OrElse parentChildChild.OwnerPSAID <> sdr.GetInt32(15) Then
            parentChildChild = Me.GetSchEvtChRm(sdr.GetInt32(15))
          End If
          If parentChildChild IsNot Nothing Then
            parentChildChild.SchEvtChRmHrList.RaiseListChangedEvents = False
            parentChildChild.SchEvtChRmHrList.Add(SchEvtChRmHr.GetSchEvtChRmHr(sdr))
            parentChildChild.SchEvtChRmHrList.RaiseListChangedEvents = True
          End If
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parentChildChild Is Nothing OrElse parentChildChild.RoomScheduleID <> sdr.GetInt32(1) Then
            parentChildChild = Me.GetSchEvtChRmByRoomScheduleID(sdr.GetInt32(1))
          End If
          If parentChildChild IsNot Nothing Then
            parentChildChild.RoomSchedChList.RaiseListChangedEvents = False
            parentChildChild.RoomSchedChList.Add(RoomSchedCh.GetRoomSchedCh(sdr))
            parentChildChild.RoomSchedChList.RaiseListChangedEvents = True
          End If
        End While
      End If

      For Each child As SchEvt In Me
        child.CheckRules()
        For Each SchEvtCh As SchEvtCh In child.SchEvtChList
          SchEvtCh.CheckRules()
        Next
        For Each RoomSched As RoomSched In child.RoomSchedList
          RoomSched.LoadChannelCount()
          RoomSched.CheckRules()
          For Each SchEvtChRmHr As SchEvtChRmHr In RoomSched.SchEvtChRmHrList
            SchEvtChRmHr.CheckRules()
          Next
          For Each RoomSchedCh As RoomSchedCh In RoomSched.RoomSchedChList
            RoomSchedCh.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRoomAllocatorPlayoutList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@GenRefNo", NothingDBNull(crit.GenRefNo))
            cm.Parameters.AddWithValue("@Live", crit.Live)
            cm.Parameters.AddWithValue("@Delayed", crit.Delayed)
            cm.Parameters.AddWithValue("@Premier", crit.Premier)
            cm.Parameters.AddWithValue("@PrimaryChannelsOnly", crit.PrimaryChannelsOnly)
            cm.Parameters.AddWithValue("@SelectedChannelIDs", Strings.MakeEmptyDBNull(crit.SelectedChannelIDsXML))
            cm.Parameters.AddWithValue("@Genre", Strings.MakeEmptyDBNull(crit.Genre))
            cm.Parameters.AddWithValue("@Series", Strings.MakeEmptyDBNull(crit.Series))
            cm.Parameters.AddWithValue("@Title", Strings.MakeEmptyDBNull(crit.Title))
            cm.Parameters.AddWithValue("@GenRefNoString", Strings.MakeEmptyDBNull(crit.GenRefNoString))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace