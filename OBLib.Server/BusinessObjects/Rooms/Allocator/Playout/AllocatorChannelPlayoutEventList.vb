﻿' Generated 28 Jul 2016 08:01 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class AllocatorChannelPlayoutEventList
    Inherits OBBusinessListBase(Of AllocatorChannelPlayoutEventList, AllocatorChannelPlayoutEvent)

#Region " Business Methods "

    Public Function GetItem(ImportedEventChannelID As Integer) As AllocatorChannelPlayoutEvent

      For Each child As AllocatorChannelPlayoutEvent In Me
        If child.ImportedEventChannelID = ImportedEventChannelID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewAllocatorChannelPlayoutEventList() As AllocatorChannelPlayoutEventList

      Return New AllocatorChannelPlayoutEventList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As AllocatorChannelPlayoutEvent In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As AllocatorChannelPlayoutEvent In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End Region

  End Class

End Namespace
