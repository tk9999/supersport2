﻿' Generated 21 Nov 2016 13:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class SchEvt
    Inherits OBBusinessBase(Of SchEvt)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SchEvtBO.SchEvtBOToString(self)")
    Public Shared OtherGenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.OtherGenRefNumber, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property OtherGenRefNumber() As Int64
      Get
        Return GetProperty(OtherGenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(OtherGenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared GenreSeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreSeries, "Genre Series")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Genre Series", Description:="")>
    Public Property GenreSeries() As String
      Get
        Return GetProperty(GenreSeriesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreSeriesProperty, Value)
      End Set
    End Property

    Public Shared SeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Series, "Series")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Series", Description:="")>
    Public Property Series() As String
      Get
        Return GetProperty(SeriesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SeriesProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared IsHighlightsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsHighlights, "Is Highlights", False)
    ''' <summary>
    ''' Gets and sets the Is Highlights value
    ''' </summary>
    <Display(Name:="Is Highlights", Description:=""),
    Required(ErrorMessage:="Is Highlights required")>
    Public Property IsHighlights() As Boolean
      Get
        Return GetProperty(IsHighlightsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsHighlightsProperty, Value)
      End Set
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.LiveDate, "Live Date")
    ''' <summary>
    ''' Gets and sets the Live Date value
    ''' </summary>
    <Display(Name:="Live Date", Description:=""),
    Required(ErrorMessage:="Live Date required")>
    Public Property LiveDate As Date
      Get
        Return GetProperty(LiveDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(LiveDateProperty, Value)
      End Set
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue")
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VenueProperty, Value)
      End Set
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:=""),
    Required(ErrorMessage:="Production required")>
    Public Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared BookingCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BookingCount, "Booking Count")
    ''' <summary>
    ''' Gets and sets the Booking Count value
    ''' </summary>
    <Display(Name:="Booking Count", Description:=""),
    Required(ErrorMessage:="Booking Count required")>
    Public Property BookingCount() As Integer
      Get
        Return GetProperty(BookingCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(BookingCountProperty, Value)
      End Set
    End Property

    Public Shared FirstStartProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.FirstStart, "First Start")
    ''' <summary>
    ''' Gets and sets the First Start value
    ''' </summary>
    <Display(Name:="First Start", Description:=""),
    Required(ErrorMessage:="First Start required")>
    Public Property FirstStart As Date
      Get
        Return GetProperty(FirstStartProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(FirstStartProperty, Value)
      End Set
    End Property

    Public Shared LiveTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LiveTimeString, "First Broadcast")
    ''' <summary>
    ''' Gets and sets the Button Style Css Class value
    ''' </summary>
    <Display(Name:="First Broadcast", Description:="")>
    Public Property LiveTimeString() As String
      Get
        Return GetProperty(LiveTimeStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LiveTimeStringProperty, Value)
      End Set
    End Property

    Public Shared IsDeletedEventProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsDeletedEvent, "Is Deleted Event", False)
    ''' <summary>
    ''' Gets and sets the Is Deleted Event value
    ''' </summary>
    <Display(Name:="Is DeletedEvent", Description:=""),
    Required(ErrorMessage:="Is Deleted Event required")>
    Public Property IsDeletedEvent() As Boolean
      Get
        Return GetProperty(IsDeletedEventProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsDeletedEventProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SchEvtChListProperty As PropertyInfo(Of SchEvtChList) = RegisterProperty(Of SchEvtChList)(Function(c) c.SchEvtChList, "Sch Evt Ch List")
    Public ReadOnly Property SchEvtChList() As SchEvtChList
      Get
        If GetProperty(SchEvtChListProperty) Is Nothing Then
          LoadProperty(SchEvtChListProperty, Rooms.SchEvtChList.NewSchEvtChList())
        End If
        Return GetProperty(SchEvtChListProperty)
      End Get
    End Property

    Public Shared RoomSchedListProperty As PropertyInfo(Of RoomSchedList) = RegisterProperty(Of RoomSchedList)(Function(c) c.RoomSchedList, "RoomSchedList")
    Public ReadOnly Property RoomSchedList() As RoomSchedList
      Get
        If GetProperty(RoomSchedListProperty) Is Nothing Then
          LoadProperty(RoomSchedListProperty, Rooms.RoomSchedList.NewRoomSchedList())
        End If
        Return GetProperty(RoomSchedListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(OtherGenRefNumberProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.GenreSeries.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Sch Evt")
        Else
          Return String.Format("Blank {0}", "Sch Evt")
        End If
      Else
        Return Me.GenreSeries
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSchEvt() method.

    End Sub

    Public Shared Function NewSchEvt() As SchEvt

      Return DataPortal.CreateChild(Of SchEvt)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSchEvt(dr As SafeDataReader) As SchEvt

      Dim s As New SchEvt()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(GenRefNumberProperty, .GetInt64(0))
          LoadProperty(OtherGenRefNumberProperty, .GetInt64(0))
          LoadProperty(GenreSeriesProperty, .GetString(1))
          LoadProperty(TitleProperty, .GetString(2))
          LoadProperty(IsHighlightsProperty, .GetBoolean(3))
          LoadProperty(LiveDateProperty, .GetValue(4))
          LoadProperty(VenueProperty, .GetString(5))
          LoadProperty(LocationProperty, .GetString(6))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(BookingCountProperty, .GetInt32(8))
          LoadProperty(FirstStartProperty, .GetValue(9))
          LoadProperty(SeriesProperty, .GetString(10))
          LoadProperty(LiveTimeStringProperty, .GetString(11))
          LoadProperty(IsDeletedEventProperty, .GetBoolean(13))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Function AddPrimaryKeyParam2(cm As SqlClient.SqlCommand, KeyProperty As Csla.PropertyInfo(Of Int64)) As SqlClient.SqlParameter
      If IsNew Then
        Dim param = cm.Parameters.Add("@" & KeyProperty.Name, SqlDbType.Int)
        param.Direction = ParameterDirection.Output
        Return param
      Else
        Return cm.Parameters.AddWithValue("@" & KeyProperty.Name, GetProperty(KeyProperty))
      End If
    End Function

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      cm.CommandText = "[InsProcsWeb].[insSchEvt]"
      AddPrimaryKeyParam2(cm, GenRefNumberProperty)
      AddInputOutputParam(cm, ProductionIDProperty)
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      'cm.Parameters.AddWithValue("@GenreSeries", GetProperty(GenreSeriesProperty))
      'cm.Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      'cm.Parameters.AddWithValue("@IsHighlights", GetProperty(IsHighlightsProperty))
      'cm.Parameters.AddWithValue("@LiveDate", LiveDate)
      'cm.Parameters.AddWithValue("@Venue", GetProperty(VenueProperty))
      'cm.Parameters.AddWithValue("@Location", GetProperty(LocationProperty))
      'cm.Parameters.AddWithValue("@BookingCount", GetProperty(BookingCountProperty))
      'cm.Parameters.AddWithValue("@FirstStart", FirstStart)

      Return Sub()
               'Post Save
               'If Me.IsNew Then
               LoadProperty(GenRefNumberProperty, cm.Parameters("@GenRefNumber").Value)
               LoadProperty(ProductionIDProperty, cm.Parameters("@ProductionID").Value)
               'End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(SchEvtChListProperty))
      UpdateChild(GetProperty(RoomSchedListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      'cm.Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
    End Sub

#End Region

    Sub CheckAll()

      Me.RoomSchedList.ToList.ForEach(Sub(rm As RoomSched)
                                        rm.UpdateClashes()
                                        rm.UpdateCrewClashes()
                                        rm.CheckAllRules()
                                      End Sub)

    End Sub

    Sub MarkAll()
      Me.MarkDirty()
      'Me.RoomSchedList.ToList.ForEach(Sub(rm As RoomSched)
      '                                  rm.MarkDirty()
      '                                  rm.SchEvtChRmHrList.ToList.ForEach(Sub(hr)
      '                                                                       hr.MarkDirty()
      '                                                                     End Sub)
      '                                End Sub)
      Me.SchEvtChList.ToList.ForEach(Sub(ch)
                                       ch.MarkDirty()
                                     End Sub)
    End Sub

  End Class

End Namespace