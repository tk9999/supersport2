﻿' Generated 28 Jul 2016 08:01 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.Security
Imports OBLib.Slugs

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Rooms

  <Serializable()> _
  Public Class AllocatorChannelPlayoutEvent
    Inherits OBBusinessBase(Of AllocatorChannelPlayoutEvent)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ImportedEventChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventChannelID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
    Public Property ImportedEventChannelID() As Integer
      Get
        Return GetProperty(ImportedEventChannelIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ImportedEventChannelIDProperty, Value)
      End Set
    End Property

    Public Shared ImportedEventIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventID, "Imported Event")
    ''' <summary>
    ''' Gets and sets the Imported Event value
    ''' </summary>
    <Display(Name:="Imported Event", Description:=""),
    Required(ErrorMessage:="Imported Event required")>
    Public Property ImportedEventID() As Integer
      Get
        Return GetProperty(ImportedEventIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ImportedEventIDProperty, Value)
      End Set
    End Property

    Public Shared EventStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventStatus, "Event Status")
    ''' <summary>
    ''' Gets and sets the Event Status value
    ''' </summary>
    <Display(Name:="Event Status", Description:="")>
    Public Property EventStatus() As String
      Get
        Return GetProperty(EventStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventStatusProperty, Value)
      End Set
    End Property

    Public Shared ScheduleDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleDateTime, "Start Time")
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
    SetExpression("AllocatorChannelEventBO.StartDateSet(self)")>
    Public Property ScheduleDateTime As DateTime?
      Get
        Return GetProperty(ScheduleDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleEndDate, "End Time")
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
    SetExpression("AllocatorChannelEventBO.EndDateSet(self)")>
    Public Property ScheduleEndDate As DateTime?
      Get
        Return GetProperty(ScheduleEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleEndDateProperty, Value)
      End Set
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelID, "Channel")
    ''' <summary>
    ''' Gets the Channel value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ChannelID() As Integer
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel Short Name")
    ''' <summary>
    ''' Gets and sets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel Short Name", Description:="")>
    Public Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelShortNameProperty, Value)
      End Set
    End Property

    Public Shared IsPrimaryChannelProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsPrimaryChannel, "Is Primary Channel", False)
    ''' <summary>
    ''' Gets and sets the Is Primary Channel value
    ''' </summary>
    <Display(Name:="Is Primary Channel", Description:=""),
    Required(ErrorMessage:="Is Primary Channel required")>
    Public Property IsPrimaryChannel() As Boolean
      Get
        Return GetProperty(IsPrimaryChannelProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsPrimaryChannelProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref Number")
    ''' <summary>
    ''' Gets and sets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref Number", Description:=""),
    Required(ErrorMessage:="Gen Ref Number required")>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared GenreSeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreSeries, "Genre Series")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Genre Series", Description:="")>
    Public Property GenreSeries() As String
      Get
        Return GetProperty(GenreSeriesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreSeriesProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared HighlightsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HighlightsInd, "Highlights", False)
    ''' <summary>
    ''' Gets and sets the Highlights value
    ''' </summary>
    <Display(Name:="Highlights", Description:=""),
    Required(ErrorMessage:="Highlights required")>
    Public Property HighlightsInd() As Boolean
      Get
        Return GetProperty(HighlightsIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HighlightsIndProperty, Value)
      End Set
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LiveDate, "Live Date")
    ''' <summary>
    ''' Gets and sets the Live Date value
    ''' </summary>
    <Display(Name:="Live Date", Description:=""),
    Required(ErrorMessage:="Live Date required")>
    Public Property LiveDate As DateTime?
      Get
        Return GetProperty(LiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LiveDateProperty, Value)
      End Set
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue")
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VenueProperty, Value)
      End Set
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System ID value
    ''' </summary>
    <Display(Name:="Sub-Dept"),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(UserSystemList), ValueMember:="SystemID", DisplayMember:="SystemName"),
    SetExpression("AllocatorChannelEventBO.SystemIDSet(self)")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Area"),
    Required(ErrorMessage:="Area required"),
    DropDownWeb(GetType(ROSystemProductionAreaList), ThisFilterMember:="SystemID"),
    SetExpression("AllocatorChannelEventBO.ProductionAreaIDSet(self)")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
    Required(ErrorMessage:="Room required"),
    SetExpression("AllocatorChannelEventBO.RoomIDSet(self)"),
    DropDownWeb(GetType(RORoomList), FilterMethodName:="AllocatorChannelEventBO.FilterByOwner")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.CallTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Call Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    Required(ErrorMessage:="Call Time required"),
    SetExpression("AllocatorChannelEventBO.CallTimeSet(self)")>
    Public Property CallTime() As DateTime?
      Get
        Return GetProperty(CallTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.WrapTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    Required(ErrorMessage:="Wrap Time required"),
    SetExpression("AllocatorChannelEventBO.WrapTimeSet(self)")>
    Public Property WrapTime() As DateTime?
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeProperty, Value)
      End Set
    End Property

    Public Shared BookingCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BookingCount, "Booking Count")
    ''' <summary>
    ''' Gets and sets the Imported Event value
    ''' </summary>
    <Display(Name:="Booking Count", Description:="")>
    Public Property BookingCount() As Integer
      Get
        Return GetProperty(BookingCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(BookingCountProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets and sets the Imported Event value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared ClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ClashCount, "Imported Event")
    ''' <summary>
    ''' Gets and sets the Imported Event value
    ''' </summary>
    <Display(Name:="ClashCount")>
    Public Property ClashCount() As Integer
      Get
        Return GetProperty(ClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ClashCountProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDMCRControllerProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceIDMCRController, "MCR Controller", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="MCR Controller"),
    DropDownWeb(GetType(ROMCRControllerAvailabilityList),
                BeforeFetchJS:="AllocatorChannelEventBO.setMCRControllerCriteriaBeforeRefresh", PreFindJSFunction:="AllocatorChannelEventBO.triggerMCRControllerAutoPopulate", AfterFetchJS:="AllocatorChannelEventBO.afterMCRControllerRefreshAjax",
                OnItemSelectJSFunction:="AllocatorChannelEventBO.onMCRControllerSelected",
                LookupMember:="MCRController", DisplayMember:="HRName", ValueMember:="ResourceID",
                DropDownCssClass:="mcrc-dropdown", DropDownColumns:={"HRName", "IsAvailableString"}),
    SetExpression("AllocatorChannelEventBO.ResourceIDMCRControllerSet(self)")>
    Public Property ResourceIDMCRController() As Integer?
      Get
        Return GetProperty(ResourceIDMCRControllerProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDMCRControllerProperty, Value)
      End Set
    End Property
    '

    Public Shared MCRControllerProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.MCRController, "")
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="MCR Controller", Description:="")>
    Public Property MCRController As String
      Get
        Return GetProperty(MCRControllerProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MCRControllerProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Room, "")
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared MCRControllerClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MCRControllerClashCount, "MCR Controller", 0)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="MCR Controller")>
    Public Property MCRControllerClashCount() As Integer
      Get
        Return GetProperty(MCRControllerClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(MCRControllerClashCountProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDSCCROperatorProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceIDSCCROperator, "SCCR Operator", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="SCCR Operator"),
    DropDownWeb(GetType(ROSCCROperatorAvailabilityList), DropDownType:=DropDownWeb.SelectType.FindScreen,
                BeforeFetchJS:="AllocatorChannelEventBO.setSCCROperatorCriteriaBeforeRefresh", PreFindJSFunction:="AllocatorChannelEventBO.triggerSCCROperatorAutoPopulate", AfterFetchJS:="AllocatorChannelEventBO.afterSCCROperatorRefreshAjax",
                OnItemSelectJSFunction:="AllocatorChannelEventBO.onSCCROperatorSelected",
                LookupMember:="SCCROperator", DisplayMember:="HRName", ValueMember:="ResourceID",
                DropDownCssClass:="sccro-dropdown", DropDownColumns:={"HRName", "IsAvailableString"}, OnCellCreateFunction:="AllocatorChannelEventBO.onCellCreate"),
    SetExpression("AllocatorChannelEventBO.ResourceIDSCCROperatorSet(self)")>
    Public Property ResourceIDSCCROperator() As Integer?
      Get
        Return GetProperty(ResourceIDSCCROperatorProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDSCCROperatorProperty, Value)
      End Set
    End Property
    '

    Public Shared SCCROperatorProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.SCCROperator, "")
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="SCCR Controller", Description:="")>
    Public Property SCCROperator As String
      Get
        Return GetProperty(SCCROperatorProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SCCROperatorProperty, Value)
      End Set
    End Property

    Public Shared SCCROperatorClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SCCROperatorClashCount, "SCCR Operator", 0)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="SCCR Operator")>
    Public Property SCCROperatorClashCount() As Integer
      Get
        Return GetProperty(SCCROperatorClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SCCROperatorClashCountProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SlugItemListProperty As PropertyInfo(Of SlugItemList) = RegisterProperty(Of SlugItemList)(Function(c) c.SlugItemList, "SlugItemList")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property SlugItemList() As SlugItemList
      Get
        If GetProperty(SlugItemListProperty) Is Nothing Then
          LoadProperty(SlugItemListProperty, New SlugItemList)
        End If
        Return GetProperty(SlugItemListProperty)
      End Get
    End Property

    Public Shared ClashDetailListProperty As PropertyInfo(Of List(Of Helpers.ResourceHelpers.ClashDetail)) = RegisterProperty(Of List(Of Helpers.ResourceHelpers.ClashDetail))(Function(c) c.ClashDetailList, "Clash List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ClashDetailList() As List(Of Helpers.ResourceHelpers.ClashDetail)
      Get
        If GetProperty(ClashDetailListProperty) Is Nothing Then
          LoadProperty(ClashDetailListProperty, New List(Of Helpers.ResourceHelpers.ClashDetail))
        End If
        Return GetProperty(ClashDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As AllocatorChannelPlayout

      Return CType(CType(Me.Parent, AllocatorChannelPlayoutEventList).Parent, AllocatorChannelPlayout)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ImportedEventChannelIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EventStatus.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Allocator Channel Event")
        Else
          Return String.Format("Blank {0}", "Allocator Channel Event")
        End If
      Else
        Return Me.EventStatus
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(RoomIDProperty)
        .AddTriggerProperties({CallTimeProperty, ScheduleDateTimeProperty, ScheduleEndDateProperty, WrapTimeProperty})
        .JavascriptRuleFunctionName = "AllocatorChannelEventBO.RoomIDValid"
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAllocatorChannelPlayoutEvent() method.

    End Sub

    Public Shared Function NewAllocatorChannelPlayoutEvent() As AllocatorChannelPlayoutEvent

      Return DataPortal.CreateChild(Of AllocatorChannelPlayoutEvent)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetAllocatorChannelPlayoutEvent(dr As SafeDataReader) As AllocatorChannelPlayoutEvent

      Dim a As New AllocatorChannelPlayoutEvent()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ImportedEventChannelIDProperty, .GetInt32(0))
          LoadProperty(ImportedEventIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EventStatusProperty, .GetString(2))
          LoadProperty(ScheduleDateTimeProperty, .GetValue(3))
          LoadProperty(ScheduleEndDateProperty, .GetValue(4))
          LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(ChannelShortNameProperty, .GetString(6))
          LoadProperty(IsPrimaryChannelProperty, .GetBoolean(7))
          LoadProperty(GenRefNumberProperty, .GetInt64(8))
          LoadProperty(GenreSeriesProperty, .GetString(9))
          LoadProperty(TitleProperty, .GetString(10))
          LoadProperty(HighlightsIndProperty, .GetBoolean(11))
          LoadProperty(LiveDateProperty, .GetValue(12))
          LoadProperty(VenueProperty, .GetString(13))
          LoadProperty(LocationProperty, .GetString(14))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(RoomProperty, .GetString(18))
          LoadProperty(SCCROperatorProperty, .GetString(19))
          LoadProperty(MCRControllerProperty, .GetString(20))
          LoadProperty(BookingCountProperty, .GetInt32(21))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insAllocatorChannelPlayoutEvent"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updAllocatorChannelPlayoutEvent"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramImportedEventChannelID As SqlParameter = .Parameters.Add("@ImportedEventChannelID", SqlDbType.Int)
          paramImportedEventChannelID.Value = GetProperty(ImportedEventChannelIDProperty)
          If Me.IsNew Then
            paramImportedEventChannelID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ImportedEventID", GetProperty(ImportedEventIDProperty))
          .Parameters.AddWithValue("@EventStatus", GetProperty(EventStatusProperty))
          .Parameters.AddWithValue("@ScheduleDateTime", (New SmartDate(GetProperty(ScheduleDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ScheduleEndDate", (New SmartDate(GetProperty(ScheduleEndDateProperty))).DBValue)
          .Parameters.AddWithValue("@ChannelID", Me.GetParent().ChannelID)
          .Parameters.AddWithValue("@ChannelShortName", GetProperty(ChannelShortNameProperty))
          .Parameters.AddWithValue("@IsPrimaryChannel", GetProperty(IsPrimaryChannelProperty))
          .Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
          .Parameters.AddWithValue("@GenreSeries", GetProperty(GenreSeriesProperty))
          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@HighlightsInd", GetProperty(HighlightsIndProperty))
          .Parameters.AddWithValue("@LiveDate", (New SmartDate(GetProperty(LiveDateProperty))).DBValue)
          .Parameters.AddWithValue("@Venue", GetProperty(VenueProperty))
          .Parameters.AddWithValue("@Location", GetProperty(LocationProperty))
          .Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
          .Parameters.AddWithValue("@CallTime", GetProperty(CallTimeProperty))
          .Parameters.AddWithValue("@WrapTime", GetProperty(WrapTimeProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ImportedEventChannelIDProperty, paramImportedEventChannelID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delAllocatorChannelPlayoutEvent"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ImportedEventChannelID", GetProperty(ImportedEventChannelIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub


    Public Function AllocateRoom() As Web.Result

      Dim rm As RORoom = OBLib.CommonData.Lists.RORoomList.GetItem(RoomID)

      Dim cmd As New Singular.CommandProc("CmdProcs.cmdResourceSchedulerAllocateRoomPlayout",
                                         {"@RoomID",
                                          "@CallTime",
                                          "@OnAirTimeStart",
                                          "@OnAirTimeEnd",
                                          "@WrapTime",
                                          "@GenreSeries",
                                          "@Title",
                                          "@ModifiedBy",
                                          "@SystemID",
                                          "@ProductionAreaID",
                                          "@ProductionID",
                                          "@ImportedEventID",
                                          "@GenRefNumber",
                                          "@SuppressFeedback"},
                                         {NothingDBNull(RoomID),
                                          NothingDBNull(CallTime),
                                          NothingDBNull(ScheduleDateTime),
                                          NothingDBNull(ScheduleEndDate),
                                          NothingDBNull(WrapTime),
                                          GenreSeries,
                                          Title,
                                          OBLib.Security.Settings.CurrentUserID,
                                          NothingDBNull(OBLib.Security.Settings.CurrentUser.SystemID),
                                          NothingDBNull(rm.OwningProductionAreaID),
                                          NothingDBNull(ProductionID),
                                          NothingDBNull(ImportedEventID),
                                          NothingDBNull(GenRefNumber),
                                          False})

      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute

      'Header Results
      Dim headerRow = cmd.Dataset.Tables(0).Rows(0)
      Dim Success As Boolean = False
      Dim HasClashes As Boolean = False
      Dim RoomScheduleAdded As Boolean = False
      Dim AreaAdded As Boolean = False
      Dim BookingAdded As Boolean = False

      Success = headerRow(0)
      HasClashes = headerRow(1)
      RoomScheduleAdded = headerRow(2)
      AreaAdded = headerRow(3)
      BookingAdded = headerRow(4)

      If Success Then
        Return New Singular.Web.Result(True)
      Else
        If HasClashes Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Clashes were found"}
        ElseIf RoomScheduleAdded Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Room Booking could not be created"}
        ElseIf AreaAdded Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Area could not be added"}
        ElseIf BookingAdded Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Resource Booking could not be created"}
        End If
      End If

      Return New Singular.Web.Result(False) With {.ErrorText = "Error could not be determined"}

    End Function

#End Region

  End Class

End Namespace