﻿' Generated 27 Jan 2016 09:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Rooms

  <Serializable()> _
  Public Class RoomAllocator
    Inherits OBBusinessBase(Of RoomAllocator)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RoomAllocatorBO.GetToString(self)")

    Public Shared ImportedEventChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventChannelID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public Property ImportedEventChannelID() As Integer
      Get
        Return GetProperty(ImportedEventChannelIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ImportedEventChannelIDProperty, Value)
      End Set
    End Property

    Public Shared ImportedEventIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventID, "Imported Event")
    ''' <summary>
    ''' Gets the Imported Event value
    ''' </summary>
    <Display(Name:="Imported Event", Description:="")>
    Public Property ImportedEventID() As Integer
      Get
        Return GetProperty(ImportedEventIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ImportedEventIDProperty, Value)
      End Set
    End Property

    Public Shared EventStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventStatus, "Status")
    ''' <summary>
    ''' Gets the Event Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property EventStatus() As String
      Get
        Return GetProperty(EventStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventStatusProperty, Value)
      End Set
    End Property

    Public Shared ScheduleDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleDateTime, "Schedule Start")
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Schedule Start", Description:=""),
    Required(ErrorMessage:="Schedule Date Time required")>
    Public Property ScheduleDateTime As DateTime?
      Get
        Return GetProperty(ScheduleDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleEndDate, "Schedule End")
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="Schedule"),
    Required(ErrorMessage:="Schedule End Date required")>
    Public Property ScheduleEndDate As DateTime?
      Get
        Return GetProperty(ScheduleEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleEndDateProperty, Value)
      End Set
    End Property

    Public ReadOnly Property EndsNextDay As Boolean
      Get
        If ScheduleDateTime IsNot Nothing AndAlso ScheduleEndDate IsNot Nothing Then
          If ScheduleDateTime.Value.Date < ScheduleEndDate.Value.Date Then
            Return True
          End If
        End If
        Return False
      End Get
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel")
    ''' <summary>
    ''' Gets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelShortNameProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref")
    ''' <summary>
    ''' Gets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:="")>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared GenRefDisplayProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenRefDisplay, "Gen Ref")
    <Display(Name:="Gen Ref")>
    Public Property GenRefDisplay() As String
      Get
        Return GetProperty(GenRefDisplayProperty)
        'If RepRnk = 1 Then
        '  Return GenRefNumber.ToString
        'End If
        'Return ""
      End Get
      Set(ByVal Value As String)
        SetProperty(GenRefDisplayProperty, Value)
      End Set
    End Property

    Public Shared GenreSeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreSeries, "Genre (Series)")
    ''' <summary>
    ''' Gets the Genre Series value
    ''' </summary>
    <Display(Name:="Genre (Series)", Description:="")>
    Public Property GenreSeries() As String
      Get
        Return GetProperty(GenreSeriesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreSeriesProperty, Value)
      End Set
    End Property

    Public Shared GenreSeriesDisplayProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreSeriesDisplay, "Genre (Series)")
    <Display(Name:="Genre (Series)", Description:="")>
    Public Property GenreSeriesDisplay() As String
      Get
        Return GetProperty(GenreSeriesDisplayProperty)
        'If RepRnk = 1 Then
        '  If GenreSeries.Length > 40 Then
        '    Return GenreSeries.Substring(0, 37) & "..."
        '  End If
        '  Return GenreSeries
        'End If
        'Return ""
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreSeriesDisplayProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared TitleDisplayProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TitleDisplay, "Title")
    <Display(Name:="Title", Description:="")>
    Public Property TitleDisplay() As String
      Get
        Return GetProperty(TitleDisplayProperty)
        'If RepRnk = 1 Then
        '  If Title.Length > 40 Then
        '    Return Title.Substring(0, 37) & "..."
        '  End If
        '  Return Title
        'End If
        'Return ""
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleDisplayProperty, Value)
      End Set
    End Property

    Public Shared HighlightsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HighlightsInd, "Highlights", False)
    ''' <summary>
    ''' Gets the Highlights value
    ''' </summary>
    <Display(Name:="Highlights", Description:="")>
    Public Property HighlightsInd() As Boolean
      Get
        Return GetProperty(HighlightsIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HighlightsIndProperty, Value)
      End Set
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LiveDate, "Live")
    ''' <summary>
    ''' Gets the Live Date value
    ''' </summary>
    <Display(Name:="Live", Description:="")>
    Public Property LiveDate As DateTime?
      Get
        Return GetProperty(LiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LiveDateProperty, Value)
      End Set
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue")
    ''' <summary>
    ''' Gets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VenueProperty, Value)
      End Set
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationProperty, Value)
      End Set
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:="")>
    Public Property RowNo() As Int64
      Get
        Return GetProperty(RowNoProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(RowNoProperty, Value)
      End Set
    End Property

    Public Shared RepRnkProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.RepRnk, "Row No")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:="")>
    Public Property RepRnk() As Int64
      Get
        Return GetProperty(RepRnkProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(RepRnkProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared BookingCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BookingCount, "Bookings")
    ''' <summary>
    ''' Gets the Booking Count value
    ''' </summary>
    <Display(Name:="Bookings", Description:="")>
    Public ReadOnly Property BookingCount() As Integer
      Get
        Return GetProperty(BookingCountProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.RoomID)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room"),
     DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityTimeSlotList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="RoomAllocatorBO.setRoomIDCriteriaBeforeRefresh",
                 PreFindJSFunction:="RoomAllocatorBO.triggerRoomIDAutoPopulate",
                 AfterFetchJS:="RoomAllocatorBO.afterRoomIDRefreshAjax",
                 LookupMember:="Room", ValueMember:="RoomID",
                 DropDownColumns:={"RoomDescription", "StartTimeDescription", "EndTimeDescription", "BookingDescription"},
                 OnItemSelectJSFunction:="RoomAllocatorBO.onRoomSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("RoomAllocatorBO.RoomIDSet(self)")>
    Public Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Room")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared RoomClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomClashCount, "Room", 0)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Room")>
    Public Property RoomClashCount() As Integer
      Get
        Return GetProperty(RoomClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomClashCountProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROUserSystemAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", ThisFilterMember:="SystemID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared BookingTimesProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.BookingTimes, "")
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Booking Times", Description:=""), AlwaysClean>
    Public Property BookingTimes As String
      Get
        Return GetProperty(BookingTimesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BookingTimesProperty, Value)
      End Set
    End Property

    Public Shared TimesValidProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.TimesValid, True)
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Booking Times", Description:=""), AlwaysClean>
    Public Property TimesValid As Boolean
      Get
        Return GetProperty(TimesValidProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(TimesValidProperty, Value)
      End Set
    End Property

    Public Shared ButtonStyleCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ButtonStyleCssClass, "ButtonStyleCssClass")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="ButtonStyleCssClass", Description:="")>
    Public Property ButtonStyleCssClass() As String
      Get
        Return GetProperty(ButtonStyleCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ButtonStyleCssClassProperty, Value)
      End Set
    End Property

    Public Shared ChannelEventOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelEventOrder, "ChannelEventOrder", 0)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="ChannelEventOrder", Description:="")>
    Public Property ChannelEventOrder() As Integer
      Get
        Return GetProperty(ChannelEventOrderProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ChannelEventOrderProperty, Value)
      End Set
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ChannelID, "ChannelID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="ChannelID", Description:="")>
    Public Property ChannelID() As Integer?
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ChannelIDProperty, Value)
      End Set
    End Property

    Public Shared Property ChannelButtonVisibleProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ChannelButtonVisible, "ChannelButtonVisible", True)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="ChannelButtonVisible", Description:="")>
    Public Property ChannelButtonVisible() As Boolean
      Get
        Return GetProperty(ChannelButtonVisibleProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ChannelButtonVisibleProperty, Value)
      End Set
    End Property

    Public Shared ScheduleNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ScheduleNumber, "Sch.Num")
    ''' <summary>
    ''' Gets and sets the Schedule Number value
    ''' </summary>
    <Display(Name:="Sch.Num", Description:="")>
    Public Property ScheduleNumber() As Integer
      Get
        Return GetProperty(ScheduleNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ScheduleNumberProperty, Value)
      End Set
    End Property

    Public Shared ScheduleNumberDisplayProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ScheduleNumberDisplay, "Sch.Num")
    <Display(Name:="Sch.Num")>
    Public Property ScheduleNumberDisplay() As String
      Get
        Return ScheduleNumber.ToString
      End Get
      Set(ByVal Value As String)
        'SetProperty(ScheduleNumberDisplayProperty, Value)
      End Set
    End Property

    Public Shared SponsoredProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Sponsored, "Sponsored")
    ''' <summary>
    ''' Gets the Booking Count value
    ''' </summary>
    <Display(Name:="Sponsored", Description:="")>
    Public Property Sponsored() As Boolean
      Get
        Return GetProperty(SponsoredProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SponsoredProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared RoomAllocatorRoomScheduleListProperty As PropertyInfo(Of RoomAllocatorRoomScheduleList) = RegisterProperty(Of RoomAllocatorRoomScheduleList)(Function(c) c.RoomAllocatorRoomScheduleList, "RoomAllocatorRoomScheduleList")
    Public ReadOnly Property RoomAllocatorRoomScheduleList() As RoomAllocatorRoomScheduleList
      Get
        If GetProperty(RoomAllocatorRoomScheduleListProperty) Is Nothing Then
          LoadProperty(RoomAllocatorRoomScheduleListProperty, OBLib.Rooms.RoomAllocatorRoomScheduleList.NewRoomAllocatorRoomScheduleList())
        End If
        Return GetProperty(RoomAllocatorRoomScheduleListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ScheduleNumberProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EventStatus.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Room Allocator")
        Else
          Return String.Format("Blank {0}", "Room Allocator")
        End If
      Else
        Return Me.EventStatus
      End If

    End Function

    Public Function AllocateRoom() As Singular.Web.Result

      'Dim cmd As New Singular.CommandProc("CmdProcs.cmdResourceSchedulerAllocateRoom",
      '                                   {"@RoomID",
      '                                    "@CallTime",
      '                                    "@OnAirTimeStart",
      '                                    "@OnAirTimeEnd",
      '                                    "@WrapTime",
      '                                    "@GenreSeries",
      '                                    "@Title",
      '                                    "@ModifiedBy",
      '                                    "@SystemID",
      '                                    "@ProductionAreaID",
      '                                    "@ProductionID",
      '                                    "@ImportedEventID",
      '                                    "@GenRefNumber",
      '                                    "@SuppressFeedback"
      '                                   },
      '                                   {NothingDBNull(RoomID),
      '                                    NothingDBNull(CallTime),
      '                                    NothingDBNull(OnAirStartDateTime),
      '                                    NothingDBNull(OnAirEndDateTime),
      '                                    NothingDBNull(WrapTime),
      '                                    GenreSeries,
      '                                    Title,
      '                                    OBLib.Security.Settings.CurrentUserID,
      '                                    NothingDBNull(SystemID),
      '                                    NothingDBNull(ProductionAreaID),
      '                                    NothingDBNull(ProductionID),
      '                                    NothingDBNull(ImportedEventID),
      '                                    NothingDBNull(GenRefNumber),
      '                                    False})

      'cmd.FetchType = CommandProc.FetchTypes.DataSet
      'cmd = cmd.Execute

      ''Header Results
      'Dim headerRow = cmd.Dataset.Tables(0).Rows(0)
      'Dim Success As Boolean = False
      'Dim HasClashes As Boolean = False
      'Dim RoomScheduleAdded As Boolean = False
      'Dim AreaAdded As Boolean = False
      'Dim BookingAdded As Boolean = False

      'Success = headerRow(0)
      'HasClashes = headerRow(1)
      'RoomScheduleAdded = headerRow(2)
      'AreaAdded = headerRow(3)
      'BookingAdded = headerRow(4)

      'If Success Then
      '  Return New Singular.Web.Result(True)
      'Else
      '  If HasClashes Then
      '    Return New Singular.Web.Result(False) With {.ErrorText = "Clashes were found"}
      '  ElseIf RoomScheduleAdded Then
      '    Return New Singular.Web.Result(False) With {.ErrorText = "Room Booking could not be created"}
      '  ElseIf AreaAdded Then
      '    Return New Singular.Web.Result(False) With {.ErrorText = "Area could not be added"}
      '  ElseIf BookingAdded Then
      '    Return New Singular.Web.Result(False) With {.ErrorText = "Resource Booking could not be created"}
      '  End If
      'End If

      Return New Singular.Web.Result(False) With {.ErrorText = "Error could not be determined"}

    End Function

    Public Sub LoadBookingTimes()
      Dim s As String = ""
      s = ScheduleDateTime.Value.ToString("dd MMM HH:mm")
      s = s & " - " & ScheduleEndDate.Value.ToString("HH:mm")
      LoadProperty(BookingTimesProperty, s)
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      'With AddWebRule(RoomIDProperty)
      '  .AddTriggerProperties({CallTimeProperty, OnAirStartDateTimeProperty, OnAirEndDateTimeProperty, WrapTimeProperty})
      '  .JavascriptRuleFunctionName = "RoomAllocatorBO.RoomIDValid"
      '  .AffectedProperties.AddRange({CallTimeProperty, OnAirStartDateTimeProperty, OnAirEndDateTimeProperty, WrapTimeProperty})
      'End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomAllocator() method.

    End Sub

    Public Shared Function NewRoomAllocator() As RoomAllocator

      Return DataPortal.CreateChild(Of RoomAllocator)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRoomAllocator(dr As SafeDataReader) As RoomAllocator

      Dim r As New RoomAllocator()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ImportedEventChannelIDProperty, .GetInt32(0))
          LoadProperty(ImportedEventIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EventStatusProperty, .GetString(2))
          LoadProperty(ScheduleDateTimeProperty, .GetValue(3))
          LoadProperty(ScheduleEndDateProperty, .GetValue(4))
          LoadProperty(ChannelShortNameProperty, .GetString(5))
          LoadProperty(GenRefNumberProperty, .GetInt64(6))
          LoadProperty(GenreSeriesProperty, .GetString(7))
          LoadProperty(TitleProperty, .GetString(8))
          LoadProperty(HighlightsIndProperty, .GetBoolean(9))
          LoadProperty(LiveDateProperty, .GetValue(10))
          LoadProperty(VenueProperty, .GetString(11))
          LoadProperty(LocationProperty, .GetString(12))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(RowNoProperty, .GetInt32(14))
          LoadProperty(RepRnkProperty, .GetInt32(15))
          LoadProperty(ButtonStyleCssClassProperty, .GetString(16))
          LoadProperty(BookingTimesProperty, .GetString(17))
          'ChannelRowNum
          LoadProperty(ChannelIDProperty, ZeroNothing(.GetInt32(19)))
          LoadProperty(ChannelEventOrderProperty, .GetInt32(20))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
          LoadProperty(BookingCountProperty, .GetInt32(22))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(23)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(24)))
          LoadProperty(GenRefDisplayProperty, .GetString(25))
          LoadProperty(GenreSeriesDisplayProperty, .GetString(26))
          LoadProperty(TitleDisplayProperty, .GetString(27))
          LoadProperty(ChannelButtonVisibleProperty, .GetBoolean(28))
          LoadProperty(ScheduleNumberProperty, .GetInt32(29))
          LoadProperty(SponsoredProperty, .GetBoolean(30))
        End With
      End Using
      LoadBookingTimes()

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "cmdProcs.cmdResourceSchedulerAllocateRoom"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.cmdResourceSchedulerAllocateRoom"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          'Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          'paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
          'If Me.IsNew Then
          '  paramResourceBookingID.Direction = ParameterDirection.Output
          'End If
          'Dim paramRoomScheduleID As SqlParameter = .Parameters.Add("@RoomScheduleID", SqlDbType.Int)
          'paramRoomScheduleID.Value = GetProperty(RoomScheduleIDProperty)
          'If Me.IsNew Then
          '  paramRoomScheduleID.Direction = ParameterDirection.Output
          'End If
          .Parameters.AddWithValue("@ImportedEventChannelID", GetProperty(ImportedEventChannelIDProperty))
          .Parameters.AddWithValue("@ImportedEventID", GetProperty(ImportedEventIDProperty))
          .Parameters.AddWithValue("@EventStatus", GetProperty(EventStatusProperty))
          .Parameters.AddWithValue("@ScheduleDateTime", (New SmartDate(GetProperty(ScheduleDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ScheduleEndDate", (New SmartDate(GetProperty(ScheduleEndDateProperty))).DBValue)
          .Parameters.AddWithValue("@ChannelShortName", GetProperty(ChannelShortNameProperty))
          .Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
          .Parameters.AddWithValue("@GenreSeries", GetProperty(GenreSeriesProperty))
          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@HighlightsInd", GetProperty(HighlightsIndProperty))
          .Parameters.AddWithValue("@LiveDate", (New SmartDate(GetProperty(LiveDateProperty))).DBValue)
          .Parameters.AddWithValue("@Venue", GetProperty(VenueProperty))
          .Parameters.AddWithValue("@Location", GetProperty(LocationProperty))
          .Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
          '.Parameters.AddWithValue("@CallTime", (New SmartDate(GetProperty(CallTimeProperty))).DBValue)
          '.Parameters.AddWithValue("@WrapTime", (New SmartDate(GetProperty(WrapTimeProperty))).DBValue)
          .Parameters.AddWithValue("@RowNo", GetProperty(RowNoProperty))
          .Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
          .Parameters.AddWithValue("@BookingCount", GetProperty(BookingCountProperty))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@Room", GetProperty(RoomProperty))
          .Parameters.AddWithValue("@ModifiedByName", OBLib.Security.Settings.CurrentUser.FirstNameSurname)
          .ExecuteNonQuery()

          'If Me.IsNew Then
          '  LoadProperty(ImportedEventChannelIDProperty, paramImportedEventChannelID.Value)
          'End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delRoomAllocator"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ImportedEventChannelID", GetProperty(ImportedEventChannelIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace