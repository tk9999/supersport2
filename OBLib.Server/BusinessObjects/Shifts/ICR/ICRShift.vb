﻿' Generated 08 Dec 2015 15:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.ICR

  <Serializable>
  Public Class ICRShift
    Inherits Base.HumanResourceShiftBase(Of ICRShift)

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    <Display(Name:="Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    'Public Shared StaffAcknowledgeByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StaffAcknowledgeByName, "StaffAcknowledgeByName", "")
    ' ''' <summary>
    ' ''' Gets and sets the Team Name value
    ' ''' </summary>
    '<Display(Name:="StaffAcknowledgeByName")>
    'Public ReadOnly Property StaffAcknowledgeByName() As String
    '  Get
    '    Return GetProperty(StaffAcknowledgeByNameProperty)
    '  End Get
    'End Property

    'Public Shared SupervisorAuthByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupervisorAuthByName, "SupervisorAuthByName", "")
    ' ''' <summary>
    ' ''' Gets and sets the Team Name value
    ' ''' </summary>
    '<Display(Name:="SupervisorAuthByName")>
    'Public ReadOnly Property SupervisorAuthByName() As String
    '  Get
    '    Return GetProperty(SupervisorAuthByNameProperty)
    '  End Get
    'End Property

    'Public Shared ManagerAuthByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerAuthByName, "ManagerAuthByName", "")
    ' ''' <summary>
    ' ''' Gets and sets the Team Name value
    ' ''' </summary>
    '<Display(Name:="ManagerAuthByName")>
    'Public ReadOnly Property ManagerAuthByName() As String
    '  Get
    '    Return GetProperty(ManagerAuthByNameProperty)
    '  End Get
    'End Property

    <Display(Name:="StaffAcknowledgeDetails")>
    Public ReadOnly Property StaffAcknowledgeDetails() As String
      Get
        If StaffAcknowledgeInd Is Nothing Then
          Return ""
        Else
          If StaffAcknowledgeDateTime IsNot Nothing Then
            Return StaffAcknowledgeByName & " " & StaffAcknowledgeDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          End If
        End If
        Return ""
      End Get
    End Property

    Public Shared SupervisorAuthDetailsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupervisorAuthDetails, "SupervisorAuthDetails", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="SupervisorAuthDetails")>
    Public ReadOnly Property SupervisorAuthDetails() As String
      Get
        If SupervisorAuthInd Is Nothing Then
          Return ""
        Else
          If SuperAuthDateTime IsNot Nothing Then
            Return SupervisorAuthByName & " " & SuperAuthDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          End If
        End If
        Return ""
      End Get
    End Property

    Public Shared ManagerAuthDetailsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerAuthDetails, "ManagerAuthDetails", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="ManagerAuthDetails")>
    Public ReadOnly Property ManagerAuthDetails() As String
      Get
        If ManagerAuthInd Is Nothing Then
          Return ""
        Else
          If ManagerAuthDateTime IsNot Nothing Then
            Return ManagerAuthByName & " " & ManagerAuthDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          End If
        End If
        Return ""
      End Get
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Title", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Title"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Title is required"),
    StringLength(1024, ErrorMessage:="Title cannot be more than 1024 characters")>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Friend Shared Function GetICRShift(dr As SafeDataReader) As ICRShift

      Dim r As New ICRShift()
      r.Fetch(dr)
      Return r

    End Function

#Region " Child Lists "

    Public Shared ClashListProperty As PropertyInfo(Of List(Of Helpers.ResourceHelpers.ClashDetail)) = RegisterProperty(Of List(Of Helpers.ResourceHelpers.ClashDetail))(Function(c) c.ClashList, "Clash List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ClashList() As List(Of Helpers.ResourceHelpers.ClashDetail)
      Get
        If GetProperty(ClashListProperty) Is Nothing Then
          LoadProperty(ClashListProperty, New List(Of Helpers.ResourceHelpers.ClashDetail))
        End If
        Return GetProperty(ClashListProperty)
      End Get
    End Property

#End Region

    Public Sub New()

    End Sub

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(StartDateTimeProperty)
        .AddTriggerProperty(EndDateTimeProperty)
        .AddTriggerProperty(HumanResourceIDProperty)
        .AffectedProperties.Add(EndDateTimeProperty)
        .JavascriptRuleFunctionName = "ICRShiftBO.ShiftValid"
        .ServerRuleFunction = AddressOf CheckClashes
      End With

    End Sub

    Public Shared Function CheckClashes(PLOS As ICRShift) As String
      Dim ErrorString = ""
      Return ErrorString
    End Function

#End Region

    Public Overrides Sub FetchExtraProperties(StartIndex As Integer, sdr As Csla.Data.SafeDataReader)
      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceBookingDescriptionProperty, .GetString(StartIndex))
        End With
      End Using
    End Sub

    Public Overrides Sub UpdateChildList()
      'Me.ResourceBookingList.Update()
    End Sub

    Public Overrides Sub DeleteChildren()

    End Sub

    <Browsable(False)>
    Public Overrides ReadOnly Property UpdProcName As String
      Get
        Return "updProcsWeb.updHumanResourceShiftICR"
      End Get
    End Property

    <Browsable(False)>
    Public Overrides ReadOnly Property InsProcName As String
      Get
        Return "insProcsWeb.insHumanResourceShiftICR"
      End Get
    End Property

    <Browsable(False)>
    Public Overrides ReadOnly Property DelProcName As String
      Get
        Return "DelProcsWeb.delHumanResourceShiftICR"
      End Get
    End Property

    '<Browsable(False), NonSerialized>
    'Private mparamResourceBookingID As SqlParameter

    Public Overrides Sub AddExtraParameters(Parameters As SqlClient.SqlParameterCollection)
      Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
    End Sub

    Public Overrides Sub AfterInsertUpdateExecuted()
      'If Me.IsNew Then
      '  LoadProperty(ResourceBookingIDProperty, mparamResourceBookingID.Value)
      'End If
    End Sub

    Public Overrides Sub AddExtraDeleteParameters(Parameters As SqlParameterCollection)
      Parameters.AddWithValue("@ResourceBookingID", Singular.Misc.NothingDBNull(GetProperty(ResourceBookingIDProperty)))
    End Sub

  End Class

End Namespace
