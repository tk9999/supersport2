﻿' Generated 19 Jul 2016 20:43 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Shifts.ReadOnly

  <Serializable()> _
  Public Class RORoomScheduleSelectShiftChannel
    Inherits OBReadOnlyBase(Of RORoomScheduleSelectShiftChannel)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RORoomScheduleSelectShiftChannelBO.RORoomScheduleSelectShiftChannelBOToString(self)")

    Public Shared RoomScheduleChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleChannelID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property RoomScheduleChannelID() As Integer
      Get
        Return GetProperty(RoomScheduleChannelIDProperty)
      End Get
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "Room Schedule")
    ''' <summary>
    ''' Gets the Room Schedule value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared ProductionChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionChannelID, "Channel")
    ''' <summary>
    ''' Gets the Production Channel value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public ReadOnly Property ProductionChannelID() As Integer
      Get
        Return GetProperty(ProductionChannelIDProperty)
      End Get
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChannelID, "Channel")
    ''' <summary>
    ''' Gets the Channel value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public ReadOnly Property ChannelID() As Integer
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel")
    ''' <summary>
    ''' Gets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public ReadOnly Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
    End Property

    Public Shared ProductionChannelStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionChannelStatusID, "Status")
    ''' <summary>
    ''' Gets the Production Channel Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property ProductionChannelStatusID() As Integer
      Get
        Return GetProperty(ProductionChannelStatusIDProperty)
      End Get
    End Property

    Public Shared ProductionChannelStatusCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionChannelStatusCode, "Status")
    ''' <summary>
    ''' Gets the Production Channel Status Code value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property ProductionChannelStatusCode() As String
      Get
        Return GetProperty(ProductionChannelStatusCodeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleChannelIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ChannelShortName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetRORoomScheduleSelectShiftChannel(dr As SafeDataReader) As RORoomScheduleSelectShiftChannel

      Dim r As New RORoomScheduleSelectShiftChannel()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RoomScheduleChannelIDProperty, .GetInt32(0))
        LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(ChannelShortNameProperty, .GetString(4))
        LoadProperty(ProductionChannelStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(ProductionChannelStatusCodeProperty, .GetString(6))
      End With

    End Sub

#End Region

  End Class

End Namespace