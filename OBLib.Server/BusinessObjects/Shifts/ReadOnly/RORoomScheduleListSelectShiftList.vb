﻿' Generated 19 Jul 2016 19:21 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Shifts.ReadOnly

  <Serializable()> _
  Public Class RORoomScheduleListSelectShiftList
    Inherits OBReadOnlyListBase(Of RORoomScheduleListSelectShiftList, RORoomScheduleListSelectShift)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "


    ' ** Example Paging property declaration code to be implemented in the necessary VM **
    'Public Property RORoomScheduleListSelectShiftList As RORoomScheduleListSelectShiftList
    'Public Property RORoomScheduleListSelectShiftListCriteria As RORoomScheduleListSelectShiftList.Criteria
    'Public Property RORoomScheduleListSelectShiftListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    ' ** Example Paging definition code to be implemented in the necessary VM Setup Method **
    'RORoomScheduleListSelectShiftList = New RORoomScheduleListSelectShiftList
    'RORoomScheduleListSelectShiftListCriteria = New RORoomScheduleListSelectShiftList.Criteria
    'RORoomScheduleListSelectShiftListManager = New Singular.Web.Data.PagedDataManager(Of VMType)(Function(d) d.RORoomScheduleListSelectShiftList, Function(d) d.RORoomScheduleListSelectShiftListCriteria, "SortBy", PageSize, SortAsc)

    Public Function GetItem(RoomScheduleID As Integer) As RORoomScheduleListSelectShift

      For Each child As RORoomScheduleListSelectShift In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property StartDateTime As DateTime?
      Public Property EndDateTime As DateTime?
      Public Property HumanResourceShiftID As Integer?
      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?
      'Public Property RoomScheduleIDs As New List(Of Integer)
      Public Property Keyword As String = ""

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewRORoomScheduleListSelectShiftList() As RORoomScheduleListSelectShiftList

      Return New RORoomScheduleListSelectShiftList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetRORoomScheduleListSelectShiftList() As RORoomScheduleListSelectShiftList

      Return DataPortal.Fetch(Of RORoomScheduleListSelectShiftList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False

      While sdr.Read
        Me.Add(RORoomScheduleListSelectShift.GetRORoomScheduleListSelectShift(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As RORoomScheduleListSelectShift = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.RoomScheduleID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.RORoomScheduleSelectShiftChannelList.RaiseListChangedEvents = False
          parent.RORoomScheduleSelectShiftChannelList.Add(RORoomScheduleSelectShiftChannel.GetRORoomScheduleSelectShiftChannel(sdr))
          parent.RORoomScheduleSelectShiftChannelList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRORoomScheduleListSelectShift"
            cm.Parameters.AddWithValue("@StartDateTime", NothingDBNull(crit.StartDateTime))
            cm.Parameters.AddWithValue("@EndDateTime", NothingDBNull(crit.EndDateTime))
            cm.Parameters.AddWithValue("@HumanResourceShiftID", NothingDBNull(crit.HumanResourceShiftID))
            cm.Parameters.AddWithValue("@Keyword", Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@SystemID", Strings.MakeEmptyDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Strings.MakeEmptyDBNull(crit.ProductionAreaID))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace