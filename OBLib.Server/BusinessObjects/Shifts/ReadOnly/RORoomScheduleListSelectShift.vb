﻿' Generated 19 Jul 2016 19:21 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Shifts.ReadOnly

  <Serializable()> _
  Public Class RORoomScheduleListSelectShift
    Inherits OBReadOnlyBase(Of RORoomScheduleListSelectShift)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RORoomScheduleListSelectShiftBO.RORoomScheduleListSelectShiftBOToString(self)")

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared RoomScheduleTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleTypeID, "Room Schedule Type")
    ''' <summary>
    ''' Gets the Room Schedule Type value
    ''' </summary>
    <Display(Name:="Room Schedule Type", Description:="")>
  Public ReadOnly Property RoomScheduleTypeID() As Integer
      Get
        Return GetProperty(RoomScheduleTypeIDProperty)
      End Get
    End Property

    Public Shared CallTimeStartProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.CallTimeStart, "Call Time Start")
    ''' <summary>
    ''' Gets the Call Time Start value
    ''' </summary>
    <Display(Name:="Call Time Start", Description:="")>
  Public ReadOnly Property CallTimeStart As Date
      Get
        Return GetProperty(CallTimeStartProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
  Public ReadOnly Property StartDateTime As Date
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
  Public ReadOnly Property EndDateTime As Date
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared WrapTimeEndProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.WrapTimeEnd, "Wrap Time End")
    ''' <summary>
    ''' Gets the Wrap Time End value
    ''' </summary>
    <Display(Name:="Wrap Time End", Description:="")>
  Public ReadOnly Property WrapTimeEnd As Date
      Get
        Return GetProperty(WrapTimeEndProperty)
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
  Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
  Public ReadOnly Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
    End Property

    Public Shared CallDateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CallDate, "Call Date")
    ''' <summary>
    ''' Gets the Call Date value
    ''' </summary>
    <Display(Name:="Call Date", Description:="")>
  Public ReadOnly Property CallDate() As String
      Get
        Return GetProperty(CallDateProperty)
      End Get
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CallTime, "Time")
    ''' <summary>
    ''' Gets the Call Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:="")>
  Public ReadOnly Property CallTime() As String
      Get
        Return GetProperty(CallTimeProperty)
      End Get
    End Property

    Public Shared WrapDateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.WrapDate, "Date")
    ''' <summary>
    ''' Gets the Wrap Date value
    ''' </summary>
    <Display(Name:="Wrap Date", Description:="")>
  Public ReadOnly Property WrapDate() As String
      Get
        Return GetProperty(WrapDateProperty)
      End Get
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.WrapTime, "Wrap Time")
    ''' <summary>
    ''' Gets the Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:="")>
  Public ReadOnly Property WrapTime() As String
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
    End Property

    Public Shared IsOvernightProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.IsOvernight, "Is Overnight")
    ''' <summary>
    ''' Gets the Is Overnight value
    ''' </summary>
    <Display(Name:="Is Overnight", Description:="")>
  Public ReadOnly Property IsOvernight() As Integer
      Get
        Return GetProperty(IsOvernightProperty)
      End Get
    End Property

    <SetExpression("RORoomScheduleListSelectShiftBO.IsSelectedSet(self)")>
    Public Overrides ReadOnly Property IsSelected As Boolean
      Get
        Return MyBase.IsSelected
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared RORoomScheduleSelectShiftChannelListProperty As PropertyInfo(Of RORoomScheduleSelectShiftChannelList) = RegisterProperty(Of RORoomScheduleSelectShiftChannelList)(Function(c) c.RORoomScheduleSelectShiftChannelList, "RO Room Schedule List Select Shift Channel  List")

    Public ReadOnly Property RORoomScheduleSelectShiftChannelList() As RORoomScheduleSelectShiftChannelList
      Get
        If GetProperty(RORoomScheduleSelectShiftChannelListProperty) Is Nothing Then
          LoadProperty(RORoomScheduleSelectShiftChannelListProperty, Shifts.ReadOnly.RORoomScheduleSelectShiftChannelList.NewRORoomScheduleSelectShiftChannelList())
        End If
        Return GetProperty(RORoomScheduleSelectShiftChannelListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Title

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetRORoomScheduleListSelectShift(dr As SafeDataReader) As RORoomScheduleListSelectShift

      Dim r As New RORoomScheduleListSelectShift()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(RoomScheduleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(CallTimeStartProperty, .GetValue(3))
        LoadProperty(StartDateTimeProperty, .GetValue(4))
        LoadProperty(EndDateTimeProperty, .GetValue(5))
        LoadProperty(WrapTimeEndProperty, .GetValue(6))
        LoadProperty(TitleProperty, .GetString(7))
        LoadProperty(RoomProperty, .GetString(8))
        LoadProperty(CallDateProperty, .GetString(9))
        LoadProperty(CallTimeProperty, .GetString(10))
        LoadProperty(WrapDateProperty, .GetString(11))
        LoadProperty(WrapTimeProperty, .GetString(12))
        LoadProperty(IsOvernightProperty, .GetInt32(13))
        'RowNo = 14
        LoadProperty(IsSelectedProperty, .GetBoolean(15))
      End With

    End Sub

#End Region

  End Class

End Namespace