﻿' Generated 19 Jul 2016 20:43 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Shifts.ReadOnly

  <Serializable()> _
  Public Class RORoomScheduleSelectShiftChannelList
    Inherits OBReadOnlyListBase(Of RORoomScheduleSelectShiftChannelList, RORoomScheduleSelectShiftChannel)

#Region " Parent "

    <NotUndoable()> Private mParent As RORoomScheduleListSelectShift
#End Region

#Region " Business Methods "

    Public Function GetItem(RoomScheduleChannelID As Integer) As RORoomScheduleSelectShiftChannel

      For Each child As RORoomScheduleSelectShiftChannel In Me
        If child.RoomScheduleChannelID = RoomScheduleChannelID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewRORoomScheduleSelectShiftChannelList() As RORoomScheduleSelectShiftChannelList

      Return New RORoomScheduleSelectShiftChannelList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace