﻿' Generated 08 Dec 2015 15:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts

  <Serializable>
  Public Class GenericShiftList
    Inherits Base.HumanResourceShiftBaseList(Of GenericShiftList, GenericShift)

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property HumanResourceID As Integer?
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property HumanResourceShiftID As Integer?
      Public Property HumanResourceShiftIDs As String = ""

      Public Sub New(HumanResourceID As Integer?, StartDate As DateTime?, EndDate As DateTime?, SystemID As Integer?, ProductionAreaID As Integer?,
                     HumanResourceShiftID As Integer?)

        Me.HumanResourceID = HumanResourceID
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.HumanResourceShiftID = HumanResourceShiftID

      End Sub

      Public Sub New(HumanResourceShiftIDs As String)
        Me.HumanResourceShiftIDs = HumanResourceShiftIDs
      End Sub

      Public Sub New(HumanResourceShiftID As Integer?)
        Me.HumanResourceShiftID = HumanResourceShiftID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewGenericShiftList() As GenericShiftList

      Return New GenericShiftList()

    End Function

    Public Shared Sub BeginGetGenericShiftList(CallBack As EventHandler(Of DataPortalResult(Of GenericShiftList)))

      Dim dp As New DataPortal(Of GenericShiftList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetGenericShiftList() As GenericShiftList

      Return DataPortal.Fetch(Of GenericShiftList)(New Criteria())

    End Function

    Public Shared Function GetGenericShiftList(HumanResourceID As Integer?,
                                                         StartDate As DateTime?, EndDate As DateTime?,
                                                         SystemID As Integer?, ProductionAreaID As Integer?,
                                                         HUmanResourceShiftID As Integer?) As GenericShiftList

      Return DataPortal.Fetch(Of GenericShiftList)(New Criteria(HumanResourceID, StartDate, EndDate, SystemID, ProductionAreaID, HUmanResourceShiftID))

    End Function

    Public Shared Function GetGenericShiftList(HUmanResourceShiftID As Integer?) As GenericShiftList

      Return DataPortal.Fetch(Of GenericShiftList)(New Criteria(HUmanResourceShiftID))

    End Function

    Public Shared Function GetGenericShiftList(ShiftIDsXml As String) As GenericShiftList

      Return DataPortal.Fetch(Of GenericShiftList)(New Criteria(ShiftIDsXml))

    End Function

    Public Overrides Sub FetchChildLists(sdr As Csla.Data.SafeDataReader)

    End Sub

    Public Overrides Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(GenericShift.GetGenericShift(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'Dim parent As GenericShift = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceShiftID <> sdr.GetInt32(15) Then
      '      parent = Me.GetItem(sdr.GetInt32(15))
      '    End If
      '    parent.ResourceBookingList.RaiseListChangedEvents = False
      '    parent.ResourceBookingList.Add(PlayoutOperationsBooking.GetPlayoutOperationsBooking(sdr))
      '    parent.ResourceBookingList.RaiseListChangedEvents = True
      '  End While
      'End If

      For Each PLO As GenericShift In Me
        PLO.CheckRules()
        'For Each child As PlayoutOperationsBooking In PLO.ResourceBookingList
        '  child.CheckRules()
        'Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceShiftListGeneric"
            cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.NothingDBNull(crit.HumanResourceShiftID))
            cm.Parameters.AddWithValue("@HumanResourceShiftIDs", Singular.Strings.MakeEmptyDBNull(crit.HumanResourceShiftIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
