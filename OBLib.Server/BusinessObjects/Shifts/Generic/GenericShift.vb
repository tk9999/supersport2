﻿' Generated 08 Dec 2015 15:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports OBLib.Shifts.ReadOnly

Namespace Shifts

  <Serializable>
  Public Class GenericShift
    Inherits Base.HumanResourceShiftBase(Of GenericShift)

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    <Display(Name:="Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Title", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Title"),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Title is required"),
    StringLength(1024, ErrorMessage:="Title cannot be more than 1024 characters")>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Friend Shared Function GetGenericShift(dr As SafeDataReader) As GenericShift

      Dim r As New GenericShift()
      r.Fetch(dr)
      Return r

    End Function

#Region " Child Lists "

    Public Shared ClashListProperty As PropertyInfo(Of List(Of Helpers.ResourceHelpers.ClashDetail)) = RegisterProperty(Of List(Of Helpers.ResourceHelpers.ClashDetail))(Function(c) c.ClashList, "Clash List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ClashList() As List(Of Helpers.ResourceHelpers.ClashDetail)
      Get
        If GetProperty(ClashListProperty) Is Nothing Then
          LoadProperty(ClashListProperty, New List(Of Helpers.ResourceHelpers.ClashDetail))
        End If
        Return GetProperty(ClashListProperty)
      End Get
    End Property

#End Region

    Public Sub New()

    End Sub

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(StartDateTimeProperty)
        .AddTriggerProperty(EndDateTimeProperty)
        .AddTriggerProperty(HumanResourceIDProperty)
        .AddTriggerProperty(ClashListProperty)
        .AffectedProperties.Add(EndDateTimeProperty)
        .JavascriptRuleFunctionName = "GenericShiftBO.ShiftValid"
        .ServerRuleFunction = AddressOf CheckClashes
      End With

    End Sub

    Public Shared Function CheckClashes(PLOS As GenericShift) As String
      Dim clashes As List(Of String) = PLOS.ClashList.Select(Of String)(Function(d) d.ClashString).ToList
      Dim ErrorString = String.Join(vbCrLf, clashes)
      Return ErrorString
    End Function

    Public Sub UpdateClashes()
      'setting this property will cause the rule to be checked
      SetProperty(ClashListProperty, Me.GetClashes())
    End Sub

#End Region

    Public Overrides Sub FetchExtraProperties(StartIndex As Integer, sdr As Csla.Data.SafeDataReader)
      LoadProperty(ResourceBookingDescriptionProperty, sdr.GetString(StartIndex))
      'Using BypassPropertyChecks
      '  With sdr
      '    LoadProperty(HumanResourceProperty, .GetString(33))
      '    LoadProperty(TeamNameProperty, .GetString(34))
      '    LoadProperty(StaffAcknowledgeByNameProperty, .GetString(35))
      '    LoadProperty(SupervisorAuthByNameProperty, .GetString(36))
      '    LoadProperty(ManagerAuthByNameProperty, .GetString(37))
      '    LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(38)))
      '    LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(39)))
      '  End With
      'End Using
    End Sub

    Public Overrides Sub UpdateChildList()
      'Me.ResourceBookingList.Update()
    End Sub

    Public Overrides Sub DeleteChildren()

    End Sub

    <Browsable(False)>
    Public Overrides ReadOnly Property UpdProcName As String
      Get
        Return "updProcsWeb.updHumanResourceShiftGeneric"
      End Get
    End Property

    <Browsable(False)>
    Public Overrides ReadOnly Property InsProcName As String
      Get
        Return "insProcsWeb.insHumanResourceShiftGeneric"
      End Get
    End Property

    <Browsable(False)>
    Public Overrides ReadOnly Property DelProcName As String
      Get
        Return "DelProcsWeb.delHumanResourceShiftGeneric"
      End Get
    End Property

    Public Overrides Sub AddExtraParameters(Parameters As SqlClient.SqlParameterCollection)
      'Parameters.AddWithValue("@ResourceID", Singular.Misc.NothingDBNull(GetProperty(ResourceIDProperty)))
      Parameters.AddWithValue("@HumanResource", GetProperty(HumanResourceProperty))
      Parameters.AddWithValue("@ShiftType", GetProperty(ShiftTypeProperty))
      Parameters.AddWithValue("@ProductionAreaStatus", GetProperty(ProductionAreaStatusProperty))
      Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
      'mparamResourceBookingID = Parameters.Add("@ResourceBookingID", SqlDbType.Int)
      'mparamResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
      'If Me.IsNew Then
      '  mparamResourceBookingID.Direction = ParameterDirection.Output
      'End If
    End Sub

    Public Overrides Sub AfterInsertUpdateExecuted()
      'If Me.IsNew Then
      '  LoadProperty(ResourceBookingIDProperty, mparamResourceBookingID.Value)
      'End If
    End Sub

    Public Overrides Sub AddExtraDeleteParameters(Parameters As SqlParameterCollection)
      Parameters.AddWithValue("@ResourceBookingID", Singular.Misc.NothingDBNull(GetProperty(ResourceBookingIDProperty)))
    End Sub

  End Class

End Namespace
