﻿' Generated 06 Nov 2017 13:45 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Patterns

  <Serializable()> _
  Public Class SAShiftPatternDayList
    Inherits OBBusinessListBase(Of SAShiftPatternDayList, SAShiftPatternDay)

#Region " Business Methods "

    Public Function GetItem(SystemAreaShiftPatternWeekDayID As Integer) As SAShiftPatternDay

      For Each child As SAShiftPatternDay In Me
        If child.SystemAreaShiftPatternWeekDayID = SystemAreaShiftPatternWeekDayID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSAShiftPatternDayList() As SAShiftPatternDayList

      Return New SAShiftPatternDayList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace