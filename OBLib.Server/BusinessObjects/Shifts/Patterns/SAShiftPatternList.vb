﻿' Generated 06 Nov 2017 13:45 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Patterns

  <Serializable()>
  Public Class SAShiftPatternList
    Inherits OBBusinessListBase(Of SAShiftPatternList, SAShiftPattern)

#Region " Business Methods "

    Public Function GetItem(SystemAreaShiftPatternID As Integer) As SAShiftPattern

      For Each child As SAShiftPattern In Me
        If child.SystemAreaShiftPatternID = SystemAreaShiftPatternID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetSAShiftPatternDay(SystemAreaShiftPatternWeekDayID As Integer) As SAShiftPatternDay

      Dim obj As SAShiftPatternDay = Nothing
      For Each parent As SAShiftPattern In Me
        obj = parent.SAShiftPatternDayList.GetItem(SystemAreaShiftPatternWeekDayID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetSAShiftPatternHR(SystemAreaShiftPatternID As Integer) As SAShiftPatternHR

      Dim obj As SAShiftPatternHR = Nothing
      For Each parent As SAShiftPattern In Me
        obj = parent.SAShiftPatternHRList.GetItem(SystemAreaShiftPatternID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SystemAreaShiftPatternID As Integer? = Nothing

      <Display(Name:="Sub-Dept", Description:="The sub-dept this record belongs to"),
       Required(ErrorMessage:="Sub-Dept required"),
       DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID"),
        SetExpression("SAShiftPatternCriteriaBO.SystemIDSet(self)")>
      Public Property SystemID() As Integer?

      <Display(Name:="Area", Description:="The area this record belongs to"),
       DropDownWeb(GetType(ROUserSystemAreaList), ThisFilterMember:="SystemID", DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID"),
        SetExpression("SAShiftPatternCriteriaBO.ProductionAreaIDSet(self)")>
      Public Property ProductionAreaID() As Integer?

      <Required(ErrorMessage:="Start Date is required"),
        SetExpression("SAShiftPatternCriteriaBO.StartDateSet(self)")>
      Public Property StartDate As Date? = Now

      <Required(ErrorMessage:="End Date is required"),
        SetExpression("SAShiftPatternCriteriaBO.EndDateSet(self)")>
      Public Property EndDate As Date? = Now

      <ClientOnly>
      Public Property FetchChildren As Boolean = False

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewSAShiftPatternList() As SAShiftPatternList

      Return New SAShiftPatternList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSAShiftPatternList() As SAShiftPatternList

      Return DataPortal.Fetch(Of SAShiftPatternList)(New Criteria())

    End Function

    Public Shared Function GetSAShiftPatternList(crit As Criteria) As SAShiftPatternList

      Return DataPortal.Fetch(Of SAShiftPatternList)(crit)

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SAShiftPattern.GetSAShiftPattern(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SAShiftPattern = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemAreaShiftPatternID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SAShiftPatternDayList.RaiseListChangedEvents = False
          parent.SAShiftPatternDayList.Add(SAShiftPatternDay.GetSAShiftPatternDay(sdr))
          parent.SAShiftPatternDayList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemAreaShiftPatternID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.SAShiftPatternHRList.RaiseListChangedEvents = False
          parent.SAShiftPatternHRList.Add(SAShiftPatternHR.GetSAShiftPatternHR(sdr))
          parent.SAShiftPatternHRList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As SAShiftPattern In Me
        child.CheckRules()
        For Each SAShiftPatternDay As SAShiftPatternDay In child.SAShiftPatternDayList
          SAShiftPatternDay.CheckRules()
        Next
        For Each SAShiftPatternHR As SAShiftPatternHR In child.SAShiftPatternHRList
          SAShiftPatternHR.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSAShiftPatternList"
            cm.Parameters.AddWithValue("@SystemAreaShiftPatternID", NothingDBNull(crit.SystemAreaShiftPatternID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
            cm.Parameters.AddWithValue("@FetchChildren", crit.FetchChildren)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace