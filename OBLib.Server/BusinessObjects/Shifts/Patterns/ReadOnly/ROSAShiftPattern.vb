﻿' Generated 06 Nov 2017 13:45 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Patterns.ReadOnly

  <Serializable()>
  Public Class ROSAShiftPattern
    Inherits OBReadOnlyBase(Of ROSAShiftPattern)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSAShiftPatternBO.ROSAShiftPatternBOToString(self)")

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    <Display(Name:="Expanded")>
    Public ReadOnly Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
    End Property

    Public Shared SystemAreaShiftPatternIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftPatternID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
    Public ReadOnly Property SystemAreaShiftPatternID() As Integer
      Get
        Return GetProperty(SystemAreaShiftPatternIDProperty)
      End Get
    End Property

    Public Shared PatternNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PatternName, "Name")
    ''' <summary>
    ''' Gets and sets the Name value
    ''' </summary>
    <Display(Name:="Name", Description:="")>
    Public ReadOnly Property PatternName() As String
      Get
        Return GetProperty(PatternNameProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area")
    ''' <summary>
    ''' Gets and sets the Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

    Public Shared NumWeeksProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NumWeeks, "Weeks", Nothing)
    ''' <summary>
    ''' Gets and sets the Weeks value
    ''' </summary>
    <Display(Name:="Weeks", Description:="")>
    Public ReadOnly Property NumWeeks() As Integer?
      Get
        Return GetProperty(NumWeeksProperty)
      End Get
    End Property

    Public Shared ShiftCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftCount, "Shifs")
    ''' <summary>
    ''' Gets and sets the Shifs value
    ''' </summary>
    <Display(Name:="Shifs", Description:=""),
    Required(ErrorMessage:="Shifs required")>
    Public ReadOnly Property ShiftCount() As Integer
      Get
        Return GetProperty(ShiftCountProperty)
      End Get
    End Property

    Public Shared IsTemplateProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTemplate, "Is Template", False)
    ''' <summary>
    ''' Gets and sets the Is Template value
    ''' </summary>
    <Display(Name:="Is Template", Description:="")>
    Public ReadOnly Property IsTemplate() As Boolean
      Get
        Return GetProperty(IsTemplateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaShiftPatternIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.PatternName

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Public Shared Function NewROSAShiftPattern() As ROSAShiftPattern

      Return DataPortal.CreateChild(Of ROSAShiftPattern)()

    End Function

    Public Sub New()

    End Sub

    Friend Shared Function GetROSAShiftPattern(dr As SafeDataReader) As ROSAShiftPattern

      Dim s As New ROSAShiftPattern()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemAreaShiftPatternIDProperty, .GetInt32(0))
        LoadProperty(PatternNameProperty, .GetString(1))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SystemProperty, .GetString(3))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(ProductionAreaProperty, .GetString(5))
        LoadProperty(NumWeeksProperty, .GetInt32(6))
        LoadProperty(ShiftCountProperty, .GetInt32(7))
        LoadProperty(IsTemplateProperty, .GetBoolean(8))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(12))
      End With

      BusinessRules.CheckRules()

    End Sub

#End Region

  End Class

End Namespace