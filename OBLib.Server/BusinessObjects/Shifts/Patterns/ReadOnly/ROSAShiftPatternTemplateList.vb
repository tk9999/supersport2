﻿' Generated 06 Nov 2017 14:19 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Patterns.ReadOnly

  <Serializable()> _
  Public Class ROSAShiftPatternTemplateList
    Inherits OBReadOnlyListBase(Of ROSAShiftPatternTemplateList, ROSAShiftPatternTemplate)

#Region " Business Methods "

    Public Function GetItem(SystemAreaShiftPatternID As Integer) As ROSAShiftPatternTemplate

      For Each child As ROSAShiftPatternTemplate In Me
        If child.SystemAreaShiftPatternID = SystemAreaShiftPatternID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROSAShiftPatternTemplateDay(SystemAreaShiftPatternWeekDayID As Integer) As ROSAShiftPatternTemplateDay

      Dim obj As ROSAShiftPatternTemplateDay = Nothing
      For Each parent As ROSAShiftPatternTemplate In Me
        obj = parent.ROSAShiftPatternTemplateDayList.GetItem(SystemAreaShiftPatternWeekDayID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Display(Name:="Sub-Dept", Description:="The sub-dept this record belongs to"),
       Required(ErrorMessage:="Sub-Dept required"),
       DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID")>
      Public Property SystemID() As Integer?

      <Display(Name:="Area", Description:="The area this record belongs to"),
       DropDownWeb(GetType(ROUserSystemAreaList), ThisFilterMember:="SystemID", DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID")>
      Public Property ProductionAreaID() As Integer?

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROSAShiftPatternTemplateList() As ROSAShiftPatternTemplateList

      Return New ROSAShiftPatternTemplateList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSAShiftPatternTemplateList() As ROSAShiftPatternTemplateList

      Return DataPortal.Fetch(Of ROSAShiftPatternTemplateList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSAShiftPatternTemplate.GetROSAShiftPatternTemplate(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROSAShiftPatternTemplate = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SystemAreaShiftPatternID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROSAShiftPatternTemplateDayList.RaiseListChangedEvents = False
          parent.ROSAShiftPatternTemplateDayList.Add(ROSAShiftPatternTemplateDay.GetROSAShiftPatternTemplateDay(sdr))
          parent.ROSAShiftPatternTemplateDayList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSAShiftPatternTemplateList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace