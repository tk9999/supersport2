﻿' Generated 06 Nov 2017 13:45 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports Singular.Paging
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Patterns.ReadOnly

  <Serializable()>
  Public Class ROSAShiftPatternList
    Inherits OBReadOnlyListBase(Of ROSAShiftPatternList, ROSAShiftPattern)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(SystemAreaShiftPatternID As Integer) As ROSAShiftPattern

      For Each child As ROSAShiftPattern In Me
        If child.SystemAreaShiftPatternID = SystemAreaShiftPatternID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property SystemAreaShiftPatternID As Integer? = Nothing

      <Display(Name:="Sub-Dept", Description:="The sub-dept this record belongs to"),
       Required(ErrorMessage:="Sub-Dept required"),
       DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID"),
       SetExpression("ROSAShiftPatternCriteriaBO.SystemIDSet(self)")>
      Public Property SystemID() As Integer?

      <Display(Name:="Area", Description:="The area this record belongs to"),
       DropDownWeb(GetType(ROUserSystemAreaList), ThisFilterMember:="SystemID", DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID"),
       SetExpression("ROSAShiftPatternCriteriaBO.ProductionAreaIDSet(self)")>
      Public Property ProductionAreaID() As Integer?

      <Required(ErrorMessage:="Start Date is required"),
       SetExpression("ROSAShiftPatternCriteriaBO.StartDateSet(self)")>
      Public Property StartDate As Date? = Now

      <Required(ErrorMessage:="End Date is required"),
       SetExpression("ROSAShiftPatternCriteriaBO.EndDateSet(self)")>
      Public Property EndDate As Date? = Now

      <ClientOnly>
      Public Property FetchChildren As Boolean = False

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROSAShiftPatternList() As ROSAShiftPatternList

      Return New ROSAShiftPatternList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSAShiftPatternList() As ROSAShiftPatternList

      Return DataPortal.Fetch(Of ROSAShiftPatternList)(New Criteria())

    End Function

    Public Shared Function GetROSAShiftPatternList(crit As Criteria) As ROSAShiftPatternList

      Return DataPortal.Fetch(Of ROSAShiftPatternList)(crit)

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSAShiftPattern.GetROSAShiftPattern(sdr))
      End While
      Me.IsReadOnly = True

      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSAShiftPatternList"
            cm.Parameters.AddWithValue("@SystemAreaShiftPatternID", NothingDBNull(crit.SystemAreaShiftPatternID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
            cm.Parameters.AddWithValue("@FetchChildren", crit.FetchChildren)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace