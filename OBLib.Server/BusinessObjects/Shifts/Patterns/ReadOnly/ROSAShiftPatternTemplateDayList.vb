﻿' Generated 06 Nov 2017 14:19 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Patterns.ReadOnly

  <Serializable()> _
  Public Class ROSAShiftPatternTemplateDayList
    Inherits OBReadOnlyListBase(Of ROSAShiftPatternTemplateDayList, ROSAShiftPatternTemplateDay)

#Region " Parent "

    <NotUndoable()> Private mParent As ROSAShiftPatternTemplate
#End Region

#Region " Business Methods "

    Public Function GetItem(SystemAreaShiftPatternWeekDayID As Integer) As ROSAShiftPatternTemplateDay

      For Each child As ROSAShiftPatternTemplateDay In Me
        If child.SystemAreaShiftPatternWeekDayID = SystemAreaShiftPatternWeekDayID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewROSAShiftPatternTemplateDayList() As ROSAShiftPatternTemplateDayList

      Return New ROSAShiftPatternTemplateDayList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace