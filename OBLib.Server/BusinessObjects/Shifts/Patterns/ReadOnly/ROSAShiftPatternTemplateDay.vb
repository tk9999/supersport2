﻿' Generated 06 Nov 2017 14:19 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Patterns.ReadOnly

  <Serializable()> _
  Public Class ROSAShiftPatternTemplateDay
    Inherits OBReadOnlyBase(Of ROSAShiftPatternTemplateDay)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSAShiftPatternTemplateDayBO.ROSAShiftPatternTemplateDayBOToString(self)")

    Public Shared SystemAreaShiftPatternWeekDayIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftPatternWeekDayID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property SystemAreaShiftPatternWeekDayID() As Integer
      Get
        Return GetProperty(SystemAreaShiftPatternWeekDayIDProperty)
      End Get
    End Property

    Public Shared SystemAreaShiftPatternIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftPatternID, "System Area Shift Pattern")
    ''' <summary>
    ''' Gets the System Area Shift Pattern value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemAreaShiftPatternID() As Integer
      Get
        Return GetProperty(SystemAreaShiftPatternIDProperty)
      End Get
    End Property

    Public Shared WeekNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WeekNo, "Week")
    ''' <summary>
    ''' Gets the Week value
    ''' </summary>
    <Display(Name:="Week", Description:="")>
    Public ReadOnly Property WeekNo() As Integer
      Get
        Return GetProperty(WeekNoProperty)
      End Get
    End Property

    Public Shared WeekDayProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WeekDay, "Day")
    ''' <summary>
    ''' Gets the Day value
    ''' </summary>
    <Display(Name:="Day", Description:="")>
    Public ReadOnly Property WeekDay() As Integer
      Get
        Return GetProperty(WeekDayProperty)
      End Get
    End Property

    Public Shared WeekdayNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.WeekdayName, "Day")
    ''' <summary>
    ''' Gets the Day value
    ''' </summary>
    <Display(Name:="Day", Description:="")>
    Public ReadOnly Property WeekdayName() As String
      Get
        Return GetProperty(WeekdayNameProperty)
      End Get
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of TimeSpan) = RegisterProperty(Of TimeSpan)(Function(c) c.StartTime, "Start Time")
    ''' <summary>
    ''' Gets the Start Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:="")>
    Public ReadOnly Property StartTime() As TimeSpan
      Get
        Return GetProperty(StartTimeProperty)
      End Get
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of TimeSpan) = RegisterProperty(Of TimeSpan)(Function(c) c.EndTime, "End Time")
    ''' <summary>
    ''' Gets the End Time value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public ReadOnly Property EndTime() As TimeSpan
      Get
        Return GetProperty(EndTimeProperty)
      End Get
    End Property

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftTypeID, "Shift Type")
    ''' <summary>
    ''' Gets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
    Public ReadOnly Property ShiftTypeID() As Integer
      Get
        Return GetProperty(ShiftTypeIDProperty)
      End Get
    End Property

    Public Shared ShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftType, "Shift Type")
    ''' <summary>
    ''' Gets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
    Public ReadOnly Property ShiftType() As String
      Get
        Return GetProperty(ShiftTypeProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By Name")
    ''' <summary>
    ''' Gets the Created By Name value
    ''' </summary>
    <Display(Name:="Created By Name", Description:="")>
    Public ReadOnly Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
    End Property

    Public Shared ModifiedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ModifiedByName, "Modified By Name")
    ''' <summary>
    ''' Gets the Modified By Name value
    ''' </summary>
    <Display(Name:="Modified By Name", Description:="")>
    Public ReadOnly Property ModifiedByName() As String
      Get
        Return GetProperty(ModifiedByNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaShiftPatternWeekDayIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.WeekdayName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSAShiftPatternTemplateDay(dr As SafeDataReader) As ROSAShiftPatternTemplateDay

      Dim r As New ROSAShiftPatternTemplateDay()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SystemAreaShiftPatternWeekDayIDProperty, .GetInt32(0))
        LoadProperty(SystemAreaShiftPatternIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(WeekNoProperty, .GetInt32(2))
        LoadProperty(WeekDayProperty, .GetInt32(3))
        LoadProperty(WeekdayNameProperty, .GetString(4))
        LoadProperty(StartTimeProperty, .GetValue(5))
        LoadProperty(EndTimeProperty, .GetValue(6))
        LoadProperty(ShiftTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(ShiftTypeProperty, .GetString(8))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(12))
        LoadProperty(CreatedByNameProperty, .GetString(13))
        LoadProperty(ModifiedByNameProperty, .GetString(14))
      End With

    End Sub

#End Region

  End Class

End Namespace