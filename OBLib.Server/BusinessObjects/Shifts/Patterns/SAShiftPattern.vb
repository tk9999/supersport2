﻿' Generated 06 Nov 2017 13:45 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Patterns

  <Serializable()>
  Public Class SAShiftPattern
    Inherits OBBusinessBase(Of SAShiftPattern)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SAShiftPatternBO.SAShiftPatternBOToString(self)")

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    <Display(Name:="Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared SystemAreaShiftPatternIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftPatternID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
    Public Property SystemAreaShiftPatternID() As Integer
      Get
        Return GetProperty(SystemAreaShiftPatternIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemAreaShiftPatternIDProperty, Value)
      End Set
    End Property

    Public Shared PatternNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PatternName, "Name")
    ''' <summary>
    ''' Gets and sets the Name value
    ''' </summary>
    <Display(Name:="Name", Description:=""),
    Required(ErrorMessage:="Pattern name is required"),
    SetExpression("SAShiftPatternBO.PatternNameSet(self)")>
    Public Property PatternName() As String
      Get
        Return GetProperty(PatternNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PatternNameProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID"),
    SetExpression("SAShiftPatternBO.SystemIDSet(self)")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required"),
    DropDownWeb(GetType(ROUserSystemAreaList), ThisFilterMember:="SystemID", DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID"),
    SetExpression("SAShiftPatternBO.ProductionAreaIDSet(self)")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area")
    ''' <summary>
    ''' Gets and sets the Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaProperty, Value)
      End Set
    End Property

    Public Shared NumWeeksProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.NumWeeks, "Weeks", Nothing)
    ''' <summary>
    ''' Gets and sets the Weeks value
    ''' </summary>
    <Display(Name:="Weeks", Description:=""),
    Required(ErrorMessage:="Weeks required"),
    DropDownWeb(GetType(ROShiftDurationList),
                DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
                ValueMember:="ShiftDuration", DisplayMember:="ShiftDurationName"),
      SetExpression("SAShiftPatternBO.NumWeeksSet(self)")>
    Public Property NumWeeks() As Integer?
      Get
        Return GetProperty(NumWeeksProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(NumWeeksProperty, Value)
      End Set
    End Property

    Public Shared ShiftCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftCount, "Shifs")
    ''' <summary>
    ''' Gets and sets the Shifs value
    ''' </summary>
    <Display(Name:="Shifs", Description:=""),
    Required(ErrorMessage:="Shifs required")>
    Public Property ShiftCount() As Integer
      Get
        Return GetProperty(ShiftCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ShiftCountProperty, Value)
      End Set
    End Property

    Public Shared IsTemplateProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTemplate, "Is Template", False)
    ''' <summary>
    ''' Gets and sets the Is Template value
    ''' </summary>
    <Display(Name:="Is Template", Description:=""),
    Required(ErrorMessage:="Is Template required"),
      SetExpression("SAShiftPatternBO.IsTemplateSet(self)")>
    Public Property IsTemplate() As Boolean
      Get
        Return GetProperty(IsTemplateProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsTemplateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared SAShiftPatternDayListProperty As PropertyInfo(Of SAShiftPatternDayList) = RegisterProperty(Of SAShiftPatternDayList)(Function(c) c.SAShiftPatternDayList, "SA Shift Pattern Day List")

    Public ReadOnly Property SAShiftPatternDayList() As SAShiftPatternDayList
      Get
        If GetProperty(SAShiftPatternDayListProperty) Is Nothing Then
          LoadProperty(SAShiftPatternDayListProperty, Shifts.Patterns.SAShiftPatternDayList.NewSAShiftPatternDayList())
        End If
        Return GetProperty(SAShiftPatternDayListProperty)
      End Get
    End Property

    Public Shared SAShiftPatternHRListProperty As PropertyInfo(Of SAShiftPatternHRList) = RegisterProperty(Of SAShiftPatternHRList)(Function(c) c.SAShiftPatternHRList, "SA Shift Pattern HR List")

    Public ReadOnly Property SAShiftPatternHRList() As SAShiftPatternHRList
      Get
        If GetProperty(SAShiftPatternHRListProperty) Is Nothing Then
          LoadProperty(SAShiftPatternHRListProperty, Shifts.Patterns.SAShiftPatternHRList.NewSAShiftPatternHRList())
        End If
        Return GetProperty(SAShiftPatternHRListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaShiftPatternIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.PatternName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "SA Shift Pattern")
        Else
          Return String.Format("Blank {0}", "SA Shift Pattern")
        End If
      Else
        Return Me.PatternName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSAShiftPattern() method.

    End Sub

    Public Shared Function NewSAShiftPattern() As SAShiftPattern

      Return DataPortal.CreateChild(Of SAShiftPattern)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSAShiftPattern(dr As SafeDataReader) As SAShiftPattern

      Dim s As New SAShiftPattern()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemAreaShiftPatternIDProperty, .GetInt32(0))
          LoadProperty(PatternNameProperty, .GetString(1))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(SystemProperty, .GetString(3))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ProductionAreaProperty, .GetString(5))
          LoadProperty(NumWeeksProperty, .GetInt32(6))
          LoadProperty(ShiftCountProperty, .GetInt32(7))
          LoadProperty(IsTemplateProperty, .GetBoolean(8))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(10))
          LoadProperty(ModifiedByProperty, .GetInt32(11))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemAreaShiftPatternIDProperty)

      cm.Parameters.AddWithValue("@PatternName", GetProperty(PatternNameProperty))
      cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      cm.Parameters.AddWithValue("@System", GetProperty(SystemProperty))
      cm.Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@ProductionArea", GetProperty(ProductionAreaProperty))
      cm.Parameters.AddWithValue("@NumWeeks", GetProperty(NumWeeksProperty))
      cm.Parameters.AddWithValue("@ShiftCount", GetProperty(ShiftCountProperty))
      cm.Parameters.AddWithValue("@IsTemplate", GetProperty(IsTemplateProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemAreaShiftPatternIDProperty, cm.Parameters("@SystemAreaShiftPatternID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(SAShiftPatternDayListProperty))
      UpdateChild(GetProperty(SAShiftPatternHRListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemAreaShiftPatternID", GetProperty(SystemAreaShiftPatternIDProperty))
    End Sub

#End Region

  End Class

End Namespace