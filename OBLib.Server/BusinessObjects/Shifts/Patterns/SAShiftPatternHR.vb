﻿' Generated 06 Nov 2017 13:46 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.HR.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SystemManagement.ReadOnly
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Patterns

  <Serializable()>
  Public Class SAShiftPatternHR
    Inherits OBBusinessBase(Of SAShiftPatternHR)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SAShiftPatternHRBO.SAShiftPatternHRBOToString(self)")

    Public Shared SystemAreaShiftPatternIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftPatternID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property SystemAreaShiftPatternID() As Integer
      Get
        Return GetProperty(SystemAreaShiftPatternIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(SystemAreaShiftPatternIDProperty, value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "HR Name", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="HR Name", Description:=""),
    Required(ErrorMessage:="Resource required"),
    DropDownWeb(GetType(ROHumanResourceFindDropDownList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SAShiftPatternHRBO.setHumanResourceIDCriteriaBeforeRefresh",
                PreFindJSFunction:="SAShiftPatternHRBO.triggerHumanResourceIDAutoPopulate",
                AfterFetchJS:="SAShiftPatternHRBO.afterHumanResourceIDRefreshAjax",
                OnItemSelectJSFunction:="SAShiftPatternHRBO.onHumanResourceIDSelected",
                LookupMember:="HRName", ValueMember:="HumanResourceID", DisplayMember:="FullName", DropDownColumns:={"FullName"},
                DropDownCssClass:="room-dropdown"),
    SetExpression("SAShiftPatternHRBO.HumanResourceIDSet(self)")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property
    ', OnCellCreateFunction:="SAShiftPatternHRBO.onCellCreate"

    Public Shared HRNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HRName, "HR Name")
    ''' <summary>
    ''' Gets and sets the HR Name value
    ''' </summary>
    <Display(Name:="HR Name", Description:="")>
    Public Property HRName() As String
      Get
        Return GetProperty(HRNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HRNameProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of Date?) = RegisterSProperty(Of Date?)(Function(c) c.StartDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDate As Date?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As Date?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of Date?) = RegisterSProperty(Of Date?)(Function(c) c.EndDate, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    Required(ErrorMessage:="End Date required"),
    DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As Date?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
     DropDownWeb(GetType(ROSystemProductionAreaDisciplineListSelect), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="SAShiftPatternHRBO.setDisciplineIDCriteriaBeforeRefresh",
                 PreFindJSFunction:="SAShiftPatternHRBO.triggerDisciplineIDAutoPopulate",
                 AfterFetchJS:="SAShiftPatternHRBO.afterDisciplineIDRefreshAjax",
                 LookupMember:="Discipline", ValueMember:="DisciplineID", DisplayMember:="Discipline", DropDownColumns:={"Discipline"},
                 OnItemSelectJSFunction:="SAShiftPatternHRBO.onDisciplineIDSelected", DropDownCssClass:="room-dropdown"),
    Required(ErrorMessage:="Discipline required")>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared ShiftCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftCount, "Shifts")
    ''' <summary>
    ''' Gets and sets the Shifs value
    ''' </summary>
    <Display(Name:="Shifts", Description:="")>
    Public Property ShiftCount() As Integer
      Get
        Return GetProperty(ShiftCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ShiftCountProperty, Value)
      End Set
    End Property

    Public Shared OriginalEndDateProperty As PropertyInfo(Of Date?) = RegisterSProperty(Of Date?)(Function(c) c.OriginalEndDate, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public Property OriginalEndDate As Date?
      Get
        Return GetProperty(OriginalEndDateProperty)
      End Get
      Set(ByVal Value As Date?)
        SetProperty(OriginalEndDateProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SAShiftPattern

      Return CType(CType(Me.Parent, SAShiftPatternHRList).Parent, SAShiftPattern)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaShiftPatternIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HRName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "SA Shift Pattern HR")
        Else
          Return String.Format("Blank {0}", "SA Shift Pattern HR")
        End If
      Else
        Return Me.HRName
      End If

    End Function

    Public Function GenerateShifts() As Singular.Web.Result
      Try
        Dim cmd As New Singular.CommandProc("CmdProcs.cmdShiftsGenerate",
            New String() {
                         "@UserID",
                         "@SystemAreaShiftPatternID",
                         "@HumanResourceID",
                         "@DisciplineID",
                         "@StartDate",
                         "@EndDate"
                         },
            New Object() {
                          OBLib.Security.Settings.CurrentUser.UserID,
                          SystemAreaShiftPatternID,
                          HumanResourceID,
                          DisciplineID,
                          StartDate,
                          EndDate
                         })
        cmd.FetchType = CommandProc.FetchTypes.DataRow
        cmd.CommandTimeout = 0
        cmd.UseTransaction = True
        cmd = cmd.Execute()
        Me.ShiftCount = cmd.DataRow(0)
        MarkOld()
        Return New Singular.Web.Result(True) With {.Data = New With {.ClashCount = cmd.DataRow(1),
                                                                      .BusinessObject = Me}}
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      'With AddWebRule(EndDateProperty)
      '  .ServerRuleFunction = AddressOf EndDateValid
      '  .JavascriptRuleFunctionName = "SAShiftPatternHRBO.EndDateValid"
      '  .AddTriggerProperty(StartDateProperty)
      '  .AffectedProperties.Add(StartDateProperty)
      'End With

    End Sub

    Public Shared Function EndDateValid(PatternHR As SAShiftPatternHR) As String

      If PatternHR.EndDate = PatternHR.StartDate Then
        Return "Start Date and End Date cannot be the same"
      End If
      Return ""

    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSAShiftPatternHR() method.

    End Sub

    Public Shared Function NewSAShiftPatternHR() As SAShiftPatternHR

      Return DataPortal.CreateChild(Of SAShiftPatternHR)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSAShiftPatternHR(dr As SafeDataReader) As SAShiftPatternHR

      Dim s As New SAShiftPatternHR()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemAreaShiftPatternIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HRNameProperty, .GetString(2))
          LoadProperty(StartDateProperty, .GetValue(3))
          LoadProperty(EndDateProperty, .GetValue(4))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(DisciplineProperty, .GetString(6))
          LoadProperty(ShiftCountProperty, .GetInt32(7))
          LoadProperty(OriginalEndDateProperty, EndDate)
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemAreaShiftPatternIDProperty)

      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@HRName", GetProperty(HRNameProperty))
      cm.Parameters.AddWithValue("@StartDate", StartDate)
      cm.Parameters.AddWithValue("@EndDate", EndDate)
      cm.Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
      cm.Parameters.AddWithValue("@Discipline", GetProperty(DisciplineProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
      cm.Parameters.AddWithValue("@OriginalEndDate", NothingDBNull(OriginalEndDate))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemAreaShiftPatternIDProperty, cm.Parameters("@SystemAreaShiftPatternID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemAreaShiftPatternID", GetProperty(SystemAreaShiftPatternIDProperty))
      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@HRName", GetProperty(HRNameProperty))
      cm.Parameters.AddWithValue("@StartDate", StartDate)
      cm.Parameters.AddWithValue("@EndDate", EndDate)
      cm.Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
      cm.Parameters.AddWithValue("@Discipline", GetProperty(DisciplineProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
      cm.Parameters.AddWithValue("@OriginalEndDate", NothingDBNull(OriginalEndDate))
    End Sub

#End Region

  End Class

End Namespace