﻿' Generated 06 Nov 2017 13:45 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ShiftPatterns.ReadOnly
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Patterns

  <Serializable()>
  Public Class SAShiftPatternDay
    Inherits OBBusinessBase(Of SAShiftPatternDay)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SAShiftPatternDayBO.SAShiftPatternDayBOToString(self)")

    Public Shared SystemAreaShiftPatternWeekDayIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftPatternWeekDayID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
    Public Property SystemAreaShiftPatternWeekDayID() As Integer
      Get
        Return GetProperty(SystemAreaShiftPatternWeekDayIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemAreaShiftPatternWeekDayIDProperty, Value)
      End Set
    End Property

    Public Shared SystemAreaShiftPatternIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemAreaShiftPatternID, "System Area Shift Pattern")
    ''' <summary>
    ''' Gets the System Area Shift Pattern value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SystemAreaShiftPatternID() As Integer
      Get
        Return GetProperty(SystemAreaShiftPatternIDProperty)
      End Get
    End Property

    Public Shared WeekNoProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.WeekNo, "Week", Nothing)
    ''' <summary>
    ''' Gets and sets the Week value
    ''' </summary>
    <Display(Name:="Week", Description:=""),
    Required(ErrorMessage:="Week required"),
      SetExpression("SAShiftPatternDayBO.WeekNoSet(self)")>
    Public Property WeekNo() As Integer?
      Get
        Return GetProperty(WeekNoProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(WeekNoProperty, Value)
      End Set
    End Property

    Public Shared WeekDayProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.WeekDay, "Day", Nothing)
    ''' <summary>
    ''' Gets and sets the Day value
    ''' </summary>
    <Display(Name:="Day", Description:=""),
    Required(ErrorMessage:="Day required"),
    DropDownWeb(GetType(ROShiftPatternWeekDayList),
                DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo,
                ValueMember:="WeekDay", DisplayMember:="WeekDayName"),
      SetExpression("SAShiftPatternDayBO.WeekDaySet(self)")>
    Public Property WeekDay() As Integer?
      Get
        Return GetProperty(WeekDayProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(WeekDayProperty, Value)
      End Set
    End Property

    Public Shared WeekdayNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.WeekdayName, "Day")
    ''' <summary>
    ''' Gets and sets the Day value
    ''' </summary>
    <Display(Name:="Day", Description:="")>
    Public Property WeekdayName() As String
      Get
        Return GetProperty(WeekdayNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(WeekdayNameProperty, Value)
      End Set
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.StartTime)
    ''' <summary>
    ''' Gets and sets the Start Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
      SetExpression("SAShiftPatternDayBO.StartTimeSet(self)"),
      TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Property StartTime() As DateTime
      Get
        Dim value = GetProperty(StartTimeProperty)
        If value = DateTime.MinValue Or IsNullNothing(value) Then
          Return Nothing
        Else
          Return CDate(value).ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As DateTime)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(StartTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(StartTimeProperty, Nothing)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(StartTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.EndTime)
    ''' <summary>
    ''' Gets and sets the End Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
      SetExpression("SAShiftPatternDayBO.EndTimeSet(self)"),
      TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Property EndTime() As Object
      Get
        Dim value = GetProperty(EndTimeProperty)
        If value = DateTime.MinValue Or IsNullNothing(value) Then
          Return Nothing
        Else
          Return CDate(value).ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(EndTimeProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(EndTimeProperty, Nothing)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(EndTimeProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared OffDayProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OffDay, "Off Day", False)
    ''' <summary>
    ''' Gets and sets the Off Day value
    ''' </summary>
    <Display(Name:="Off Day", Description:=""),
    Required(ErrorMessage:="Off Day required")>
    Public Property OffDay() As Boolean
      Get
        Return GetProperty(OffDayProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(OffDayProperty, Value)
      End Set
    End Property

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ShiftTypeID, "Shift Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:=""),
    Required(ErrorMessage:="Shift Type required"),
    DropDownWeb(GetType(ROSystemAreaShiftTypeList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SAShiftPatternDayBO.setShiftTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="SAShiftPatternDayBO.triggerShiftTypeIDAutoPopulate",
                AfterFetchJS:="SAShiftPatternDayBO.afterShiftTypeIDRefreshAjax",
                LookupMember:="ShiftType", ValueMember:="ShiftTypeID", DisplayMember:="ShiftType", DropDownColumns:={"ShiftType"},
                OnItemSelectJSFunction:="SAShiftPatternDayBO.onShiftTypeIDSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("SAShiftPatternDayBO.ShiftTypeIDSet(self)")>
    Public Property ShiftTypeID() As Integer?
      Get
        Return GetProperty(ShiftTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ShiftTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftType, "Shift Type")
    ''' <summary>
    ''' Gets and sets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="")>
    Public Property ShiftType() As String
      Get
        Return GetProperty(ShiftTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ShiftTypeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared CreatedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CreatedByName, "Created By Name")
    ''' <summary>
    ''' Gets and sets the Created By Name value
    ''' </summary>
    <Display(Name:="Created By Name", Description:="")>
    Public Property CreatedByName() As String
      Get
        Return GetProperty(CreatedByNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CreatedByNameProperty, Value)
      End Set
    End Property

    Public Shared ModifiedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ModifiedByName, "Modified By Name")
    ''' <summary>
    ''' Gets and sets the Modified By Name value
    ''' </summary>
    <Display(Name:="Modified By Name", Description:="")>
    Public Property ModifiedByName() As String
      Get
        Return GetProperty(ModifiedByNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ModifiedByNameProperty, Value)
      End Set
    End Property

    Public Shared ShiftCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ShiftCount, "Shifs")
    ''' <summary>
    ''' Gets and sets the Shifs value
    ''' </summary>
    <Display(Name:="Shifs", Description:="")>
    Public Property ShiftCount() As Integer
      Get
        Return GetProperty(ShiftCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ShiftCountProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SAShiftPattern

      Return CType(CType(Me.Parent, SAShiftPatternDayList).Parent, SAShiftPattern)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SystemAreaShiftPatternWeekDayIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.WeekdayName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "SA Shift Pattern Day")
        Else
          Return String.Format("Blank {0}", "SA Shift Pattern Day")
        End If
      Else
        Return Me.WeekdayName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSAShiftPatternDay() method.

    End Sub

    Public Shared Function NewSAShiftPatternDay() As SAShiftPatternDay

      Return DataPortal.CreateChild(Of SAShiftPatternDay)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSAShiftPatternDay(dr As SafeDataReader) As SAShiftPatternDay

      Dim s As New SAShiftPatternDay()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SystemAreaShiftPatternWeekDayIDProperty, .GetInt32(0))
          LoadProperty(SystemAreaShiftPatternIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(WeekNoProperty, .GetInt32(2))
          LoadProperty(WeekDayProperty, .GetInt32(3))
          LoadProperty(WeekdayNameProperty, .GetString(4))
          If .IsDBNull(5) Then
            LoadProperty(StartTimeProperty, DateTime.MinValue)
          Else
            LoadProperty(StartTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(5))))
          End If
          If .IsDBNull(6) Then
            LoadProperty(EndTimeProperty, DateTime.MinValue)
          Else
            LoadProperty(EndTimeProperty, (New DateTime(2000, 1, 1).Add(.GetValue(6))))
          End If
          LoadProperty(OffDayProperty, .GetBoolean(7))
          LoadProperty(ShiftTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(ShiftTypeProperty, .GetString(9))
          LoadProperty(CreatedByProperty, .GetInt32(10))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(11))
          LoadProperty(ModifiedByProperty, .GetInt32(12))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(13))
          LoadProperty(CreatedByNameProperty, .GetString(14))
          LoadProperty(ModifiedByNameProperty, .GetString(15))
          LoadProperty(ShiftCountProperty, .GetInt32(16))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SystemAreaShiftPatternWeekDayIDProperty)

      cm.Parameters.AddWithValue("@SystemAreaShiftPatternID", Me.GetParent().SystemAreaShiftPatternID)
      cm.Parameters.AddWithValue("@WeekNo", GetProperty(WeekNoProperty))
      cm.Parameters.AddWithValue("@WeekDay", GetProperty(WeekDayProperty))
      cm.Parameters.AddWithValue("@WeekdayName", GetProperty(WeekdayNameProperty))
      cm.Parameters.AddWithValue("@StartTime", GetProperty(StartTimeProperty))
      cm.Parameters.AddWithValue("@EndTime", GetProperty(EndTimeProperty))
      cm.Parameters.AddWithValue("@OffDay", GetProperty(OffDayProperty))
      cm.Parameters.AddWithValue("@ShiftTypeID", GetProperty(ShiftTypeIDProperty))
      cm.Parameters.AddWithValue("@ShiftType", GetProperty(ShiftTypeProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)
      cm.Parameters.AddWithValue("@CreatedByName", GetProperty(CreatedByNameProperty))
      cm.Parameters.AddWithValue("@ModifiedByName", GetProperty(ModifiedByNameProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SystemAreaShiftPatternWeekDayIDProperty, cm.Parameters("@SystemAreaShiftPatternWeekDayID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SystemAreaShiftPatternWeekDayID", GetProperty(SystemAreaShiftPatternWeekDayIDProperty))
    End Sub

#End Region

  End Class

End Namespace