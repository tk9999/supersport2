﻿' Generated 08 Dec 2015 15:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.PlayoutOperations

  <Serializable>
  Public Class PlayoutOperationsShiftList
    Inherits Base.HumanResourceShiftBaseList(Of PlayoutOperationsShiftList, PlayoutOperationsShift)

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      'Public Property HumanResourceID As Integer?
      'Public Property StartDate As DateTime? = Nothing
      'Public Property EndDate As DateTime? = Nothing
      'Public Property SystemID As Integer? = Nothing
      'Public Property ProductionAreaID As Integer? = Nothing
      Public Property HUmanResourceShiftID As Integer?
      Public Property HumanResourceShiftIDs As String = ""

      'Public Sub New(HumanResourceID As Integer?, StartDate As DateTime?, EndDate As DateTime?, SystemID As Integer?, ProductionAreaID As Integer?,
      '               HumanResourceShiftID As Integer?)

      '  Me.HumanResourceID = HumanResourceID
      '  Me.StartDate = StartDate
      '  Me.EndDate = EndDate
      '  Me.SystemID = SystemID
      '  Me.ProductionAreaID = ProductionAreaID
      '  Me.HUmanResourceShiftID = HumanResourceShiftID

      'End Sub


      Public Sub New(HumanResourceShiftID As Integer?)
        Me.HUmanResourceShiftID = HumanResourceShiftID
      End Sub

      Public Sub New(HumanResourceShiftIDs As String)
        Me.HumanResourceShiftIDs = HumanResourceShiftIDs
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewPlayoutOperationsShiftList() As PlayoutOperationsShiftList

      Return New PlayoutOperationsShiftList()

    End Function

    Public Shared Sub BeginGetPlayoutOperationsShiftList(CallBack As EventHandler(Of DataPortalResult(Of PlayoutOperationsShiftList)))

      Dim dp As New DataPortal(Of PlayoutOperationsShiftList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetPlayoutOperationsShiftList() As PlayoutOperationsShiftList

      Return DataPortal.Fetch(Of PlayoutOperationsShiftList)(New Criteria())

    End Function

    Public Shared Function GetPlayoutOperationsShiftList(HUmanResourceShiftID As Integer?) As PlayoutOperationsShiftList
      'HumanResourceID As Integer?,
      '                                                   StartDate As DateTime?, EndDate As DateTime?,
      '                                                   SystemID As Integer?, ProductionAreaID As Integer?,
      'HumanResourceID, StartDate, EndDate, SystemID, ProductionAreaID, 
      Return DataPortal.Fetch(Of PlayoutOperationsShiftList)(New Criteria(HUmanResourceShiftID))

    End Function

    Public Shared Function GetPlayoutOperationsShiftList(ShiftIDsXml As String) As PlayoutOperationsShiftList

      Return DataPortal.Fetch(Of PlayoutOperationsShiftList)(New Criteria(ShiftIDsXml))

    End Function

    Public Overrides Sub FetchChildLists(sdr As Csla.Data.SafeDataReader)

    End Sub

    Public Overrides Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(PlayoutOperationsShift.GetPlayoutOperationsShift(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As PlayoutOperationsShift = Nothing
      If sdr.NextResult Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceShiftID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.HumanResourceShiftRoomScheduleList.RaiseListChangedEvents = False
          parent.HumanResourceShiftRoomScheduleList.Add(HumanResourceShiftRoomSchedule.GetHumanResourceShiftRoomSchedule(sdr))
          parent.HumanResourceShiftRoomScheduleList.RaiseListChangedEvents = True
        End While
      End If

      For Each PLO As PlayoutOperationsShift In Me
        PLO.CheckRules()
        For Each child As HumanResourceShiftRoomSchedule In PLO.HumanResourceShiftRoomScheduleList
          child.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceShiftListPlayoutOperations"
            'cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
            'cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            'cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            'cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            'cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@HUmanResourceShiftID", Singular.Misc.NothingDBNull(crit.HUmanResourceShiftID))
            cm.Parameters.AddWithValue("@HumanResourceShiftIDs", Singular.Strings.MakeEmptyDBNull(crit.HumanResourceShiftIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Public Sub DeleteShifts()

      Dim shiftIDs As List(Of Integer) = Me.Select(Function(d) d.HumanResourceShiftID).ToList
      If shiftIDs.Count = 0 Then
        Throw New Exception("At least on shift must be provided for deletion")
      Else
        Dim xmlIDs As String = OBLib.OBMisc.IntegerListToXML(shiftIDs)
        Using cn As New SqlConnection(Singular.Settings.ConnectionString)
          cn.Open()
          Try
            Using cm As SqlCommand = cn.CreateCommand
              cm.CommandType = CommandType.StoredProcedure
              cm.CommandText = "DelProcsWeb.delHumanResourceShiftPlayoutOperations"
              cm.Parameters.AddWithValue("@HumanResourceShiftIDs", xmlIDs)
              cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
              cm.ExecuteNonQuery()
            End Using
          Finally
            cn.Close()
          End Try
        End Using
      End If

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
