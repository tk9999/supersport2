﻿' Generated 08 Dec 2015 15:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.PlayoutOperations

  <Serializable>
  Public Class SCCRShift
    Inherits Base.HumanResourceShiftBase(Of SCCRShift)

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    <Display(Name:="Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Friend Shared Function GetSCCRShift(dr As SafeDataReader) As SCCRShift

      Dim r As New SCCRShift()
      r.Fetch(dr)
      Return r

    End Function

#Region " Child Lists "

    Public Shared ClashListProperty As PropertyInfo(Of List(Of Helpers.ResourceHelpers.ClashDetail)) = RegisterProperty(Of List(Of Helpers.ResourceHelpers.ClashDetail))(Function(c) c.ClashList, "Clash List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ClashList() As List(Of Helpers.ResourceHelpers.ClashDetail)
      Get
        If GetProperty(ClashListProperty) Is Nothing Then
          LoadProperty(ClashListProperty, New List(Of Helpers.ResourceHelpers.ClashDetail))
        End If
        Return GetProperty(ClashListProperty)
      End Get
    End Property

    Public Shared HumanResourceShiftRoomScheduleListProperty As PropertyInfo(Of HumanResourceShiftRoomScheduleList) = RegisterProperty(Of HumanResourceShiftRoomScheduleList)(Function(c) c.HumanResourceShiftRoomScheduleList, "Room Schedule List")
    Public ReadOnly Property HumanResourceShiftRoomScheduleList() As HumanResourceShiftRoomScheduleList
      Get
        If GetProperty(HumanResourceShiftRoomScheduleListProperty) Is Nothing Then
          LoadProperty(HumanResourceShiftRoomScheduleListProperty, New HumanResourceShiftRoomScheduleList)
        End If
        Return GetProperty(HumanResourceShiftRoomScheduleListProperty)
      End Get
    End Property

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(StartDateTimeProperty)
        .AddTriggerProperty(EndDateTimeProperty)
        .AddTriggerProperty(HumanResourceIDProperty)
        .AffectedProperties.Add(EndDateTimeProperty)
        .JavascriptRuleFunctionName = "SCCRShiftBO.ShiftValid"
        .ServerRuleFunction = AddressOf CheckClashes
      End With

    End Sub

    Public Shared Function CheckClashes(PLOS As SCCRShift) As String
      Dim ErrorString = ""
      Return ErrorString
    End Function

    Public Function GetPopoverContent() As String
      Dim poc As String = ""
      For Each rs As HumanResourceShiftRoomSchedule In Me.HumanResourceShiftRoomScheduleList
        If poc.Length = 0 Then
          poc = rs.Room & ": " & rs.StartTimeString & " - " & rs.EndTimeString & " (" & rs.RoomScheduleTitle & ")"
        Else
          poc &= vbCrLf & rs.Room & ": " & rs.StartTimeString & " - " & rs.EndTimeString & " (" & rs.RoomScheduleTitle & ")"
        End If
      Next
      Return poc
    End Function

#End Region

    Public Overrides Sub FetchExtraProperties(StartIndex As Integer, sdr As Csla.Data.SafeDataReader)
      Using BypassPropertyChecks
        With sdr

        End With
      End Using
    End Sub

    Public Overrides Sub UpdateChildList()
      'Me.ResourceBookingList.Update()
    End Sub

    Public Overrides Sub DeleteChildren()

    End Sub

    <Browsable(False)>
    Public Overrides ReadOnly Property UpdProcName As String
      Get
        Return "updProcsWeb.updHumanResourceShiftPlayoutOperationsSCCR"
      End Get
    End Property

    <Browsable(False)>
    Public Overrides ReadOnly Property InsProcName As String
      Get
        Return "insProcsWeb.insHumanResourceShiftPlayoutOperationsSCCR"
      End Get
    End Property

    <Browsable(False)>
    Public Overrides ReadOnly Property DelProcName As String
      Get
        Return "DelProcsWeb.delHumanResourceShiftPlayoutOperationsSCCR"
      End Get
    End Property

    Public Sub New()

    End Sub

    '<Browsable(False), NonSerialized>
    'Private mparamResourceBookingID As SqlParameter

    Public Overrides Sub AddExtraParameters(Parameters As SqlClient.SqlParameterCollection)
      Parameters.AddWithValue("@PopoverContent", GetPopoverContent)
    End Sub

    Public Overrides Sub AfterInsertUpdateExecuted()
      'If Me.IsNew Then
      '  LoadProperty(ResourceBookingIDProperty, mparamResourceBookingID.Value)
      'End If
    End Sub

    Public Overrides Sub AddExtraDeleteParameters(Parameters As SqlParameterCollection)
      ' Parameters.AddWithValue("@ResourceBookingID", Singular.Misc.NothingDBNull(GetProperty(ResourceBookingIDProperty)))
    End Sub

  End Class

End Namespace
