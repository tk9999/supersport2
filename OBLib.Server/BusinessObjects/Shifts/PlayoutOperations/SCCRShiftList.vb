﻿' Generated 08 Dec 2015 15:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.PlayoutOperations

  <Serializable>
  Public Class SCCRShiftList
    Inherits Base.HumanResourceShiftBaseList(Of SCCRShiftList, SCCRShift)

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      'Public Property HumanResourceID As Integer?
      'Public Property StartDate As DateTime? = Nothing
      'Public Property EndDate As DateTime? = Nothing
      'Public Property SystemID As Integer? = Nothing
      'Public Property ProductionAreaID As Integer? = Nothing
      Public Property HumanResourceShiftID As Integer? = Nothing
      Public Property HumanResourceShiftIDs As String = ""

      'Public Sub New(HumanResourceID As Integer?, StartDate As DateTime?, EndDate As DateTime?, SystemID As Integer?, ProductionAreaID As Integer?,
      '               HumanResourceShiftID As Integer?)

      '  Me.HumanResourceID = HumanResourceID
      '  Me.StartDate = StartDate
      '  Me.EndDate = EndDate
      '  Me.SystemID = SystemID
      '  Me.ProductionAreaID = ProductionAreaID
      '  Me.HUmanResourceShiftID = HumanResourceShiftID

      'End Sub

      Public Sub New(HumanResourceShiftIDs As String)
        Me.HumanResourceShiftIDs = HumanResourceShiftIDs
      End Sub

      Public Sub New(HumanResourceShiftID As Integer?)
        Me.HumanResourceShiftID = HumanResourceShiftID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSCCRShiftList() As SCCRShiftList

      Return New SCCRShiftList()

    End Function

    Public Shared Sub BeginGetSCCRShiftList(CallBack As EventHandler(Of DataPortalResult(Of SCCRShiftList)))

      Dim dp As New DataPortal(Of SCCRShiftList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSCCRShiftList() As SCCRShiftList

      Return DataPortal.Fetch(Of SCCRShiftList)(New Criteria())

    End Function

    Public Shared Function GetSCCRShiftList(HumanResourceShiftID As Integer?) As SCCRShiftList

      Return DataPortal.Fetch(Of SCCRShiftList)(New Criteria(HumanResourceShiftID))

    End Function

    Public Shared Function GetSCCRShiftList(HumanResourceShiftIDs As String) As SCCRShiftList

      Return DataPortal.Fetch(Of SCCRShiftList)(New Criteria(HumanResourceShiftIDs))

    End Function

    Public Overrides Sub FetchChildLists(sdr As Csla.Data.SafeDataReader)

    End Sub

    Public Overrides Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SCCRShift.GetSCCRShift(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'FetchRoomScheduleList(sdr)

      Dim parent As SCCRShift = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.HumanResourceShiftID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.HumanResourceShiftRoomScheduleList.RaiseListChangedEvents = False
          parent.HumanResourceShiftRoomScheduleList.Add(HumanResourceShiftRoomSchedule.GetHumanResourceShiftRoomSchedule(sdr))
          parent.HumanResourceShiftRoomScheduleList.RaiseListChangedEvents = True
        End While
      End If

      For Each PLO As SCCRShift In Me
        PLO.CheckRules()
        For Each child As HumanResourceShiftRoomSchedule In PLO.HumanResourceShiftRoomScheduleList
          child.CheckRules()
        Next
        'For Each child As PlayoutOperationsBooking In PLO.ResourceBookingList
        '  child.CheckRules()
        'Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceShiftListPlayoutOperationsSCCR"
            cm.Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.NothingDBNull(crit.HumanResourceShiftID))
            cm.Parameters.AddWithValue("@HumanResourceShiftIDs", Singular.Strings.MakeEmptyDBNull(crit.HumanResourceShiftIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
