﻿' Generated 08 Dec 2015 15:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.PlayoutOperations

  <Serializable>
  Public Class MCRShift
    Inherits Base.HumanResourceShiftBase(Of MCRShift)

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    <Display(Name:="Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceShiftRoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftRoomScheduleID, "ID", Nothing)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public Property HumanResourceShiftRoomScheduleID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftRoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftRoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "Room Schedule", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.RoomID, Nothing)
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
     DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityTimeSlotList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="MCRShiftBO.setCriteriaBeforeRefresh",
                 PreFindJSFunction:="MCRShiftBO.triggerAutoPopulate",
                 AfterFetchJS:="MCRShiftBO.afterRoomRefreshAjax",
                 LookupMember:="Room", ValueMember:="RoomID",
                 DropDownColumns:={"RoomDescription", "BookingDescription", "SlotTimeDescription", "DurationDescription", "BookingDurationDescription"},
                 OnItemSelectJSFunction:="MCRShiftBO.onRoomSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("MCRShiftBO.RoomIDSet(self)")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property
    ',
    'Required(ErrorMessage:="Room is required")

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Room, "")
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared RoomClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomClashCount, "Room", 0)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Room")>
    Public Property RoomClashCount() As Integer
      Get
        Return GetProperty(RoomClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomClashCountProperty, Value)
      End Set
    End Property

    Friend Shared Function GetMCRShift(dr As SafeDataReader) As MCRShift

      Dim r As New MCRShift()
      r.Fetch(dr)
      Return r

    End Function

#Region " Child Lists "

    Public Shared ClashListProperty As PropertyInfo(Of List(Of Helpers.ResourceHelpers.ClashDetail)) = RegisterProperty(Of List(Of Helpers.ResourceHelpers.ClashDetail))(Function(c) c.ClashList, "Clash List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ClashList() As List(Of Helpers.ResourceHelpers.ClashDetail)
      Get
        If GetProperty(ClashListProperty) Is Nothing Then
          LoadProperty(ClashListProperty, New List(Of Helpers.ResourceHelpers.ClashDetail))
        End If
        Return GetProperty(ClashListProperty)
      End Get
    End Property

    'Public Shared RORoomChannelListProperty As PropertyInfo(Of OBLib.Maintenance.Rooms.ReadOnly.RORoomChannelList) = RegisterProperty(Of OBLib.Maintenance.Rooms.ReadOnly.RORoomChannelList)(Function(c) c.RORoomChannelList, "Clash List")
    'Public ReadOnly Property RORoomChannelList() As OBLib.Maintenance.Rooms.ReadOnly.RORoomChannelList
    '  Get
    '    If GetProperty(RORoomChannelListProperty) Is Nothing Then
    '      LoadProperty(RORoomChannelListProperty, New OBLib.Maintenance.Rooms.ReadOnly.RORoomChannelList)
    '    End If
    '    Return GetProperty(RORoomChannelListProperty)
    '  End Get
    'End Property

    Public Shared HumanResourceShiftRoomScheduleListProperty As PropertyInfo(Of HumanResourceShiftRoomScheduleList) = RegisterProperty(Of HumanResourceShiftRoomScheduleList)(Function(c) c.HumanResourceShiftRoomScheduleList, "Room Schedule List")
    Public ReadOnly Property HumanResourceShiftRoomScheduleList() As HumanResourceShiftRoomScheduleList
      Get
        If GetProperty(HumanResourceShiftRoomScheduleListProperty) Is Nothing Then
          LoadProperty(HumanResourceShiftRoomScheduleListProperty, New HumanResourceShiftRoomScheduleList)
        End If
        Return GetProperty(HumanResourceShiftRoomScheduleListProperty)
      End Get
    End Property

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(StartDateTimeProperty)
        .AddTriggerProperty(EndDateTimeProperty)
        .AddTriggerProperty(HumanResourceIDProperty)
        .AffectedProperties.Add(EndDateTimeProperty)
        .JavascriptRuleFunctionName = "MCRShiftBO.ShiftValid"
        .ServerRuleFunction = AddressOf CheckClashes
      End With

    End Sub

    Public Shared Function CheckClashes(PLOS As MCRShift) As String
      Dim ErrorString = ""
      Return ErrorString
    End Function

#End Region

    Public Overrides Sub FetchExtraProperties(StartIndex As Integer, sdr As Csla.Data.SafeDataReader)
      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceShiftRoomScheduleIDProperty, Singular.Misc.ZeroNothing(sdr.GetInt32(StartIndex)))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(sdr.GetInt32(StartIndex + 1)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(sdr.GetInt32(StartIndex + 2)))
          LoadProperty(RoomProperty, sdr.GetString(StartIndex + 3))
        End With
      End Using
    End Sub

    Public Overrides Sub UpdateChildList()
      'Me.ResourceBookingList.Update()
    End Sub

    Public Overrides Sub DeleteChildren()

    End Sub

    <Browsable(False)>
    Public Overrides ReadOnly Property UpdProcName As String
      Get
        Return "updProcsWeb.updHumanResourceShiftPlayoutOperationsMCR"
      End Get
    End Property

    <Browsable(False)>
    Public Overrides ReadOnly Property InsProcName As String
      Get
        Return "insProcsWeb.insHumanResourceShiftPlayoutOperationsMCR"
      End Get
    End Property

    <Browsable(False)>
    Public Overrides ReadOnly Property DelProcName As String
      Get
        Return "DelProcsWeb.delHumanResourceShiftPlayoutOperationsMCR"
      End Get
    End Property

    <Browsable(False), NonSerialized>
    Private mHumanResourceShiftRoomScheduleID As SqlParameter
    <Browsable(False), NonSerialized>
    Private mRoomScheduleID As SqlParameter

    Public Overrides Sub AddExtraParameters(Parameters As SqlClient.SqlParameterCollection)
      mHumanResourceShiftRoomScheduleID = Parameters.Add("@HumanResourceShiftRoomScheduleID", SqlDbType.Int)
      mHumanResourceShiftRoomScheduleID.Value = Singular.Misc.NothingDBNull(GetProperty(HumanResourceShiftRoomScheduleIDProperty))
      mRoomScheduleID = Parameters.Add("@RoomScheduleID", SqlDbType.Int)
      mRoomScheduleID.Value = Singular.Misc.NothingDBNull(GetProperty(RoomScheduleIDProperty))
      'If Me.IsNew Then
      mHumanResourceShiftRoomScheduleID.Direction = ParameterDirection.InputOutput
      mRoomScheduleID.Direction = ParameterDirection.InputOutput
      'End If
      Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(GetProperty(RoomIDProperty)))
      Parameters.AddWithValue("@Room", Singular.Misc.NothingDBNull(GetProperty(RoomProperty)))
    End Sub

    Public Overrides Sub AfterInsertUpdateExecuted()
      'If Me.IsNew Then
      LoadProperty(HumanResourceShiftRoomScheduleIDProperty, IIf(IsDBNull(mHumanResourceShiftRoomScheduleID.Value), Nothing, mHumanResourceShiftRoomScheduleID.Value))
      LoadProperty(RoomScheduleIDProperty, IIf(IsDBNull(mRoomScheduleID.Value), Nothing, mRoomScheduleID.Value))
      'RoomID is not required here
      'Room is not required here
      'End If
    End Sub

    Public Overrides Sub AddExtraDeleteParameters(Parameters As SqlParameterCollection)
      'Parameters.AddWithValue("@RoomScheduleID", Singular.Misc.NothingDBNull(GetProperty(RoomScheduleIDProperty)))
    End Sub

  End Class

End Namespace
