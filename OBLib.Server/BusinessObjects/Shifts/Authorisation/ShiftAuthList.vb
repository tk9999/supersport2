﻿' Generated 14 Sep 2014 14:17 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Shifts.Auth

  <Serializable()> _
  Public Class ShiftAuthList
    Inherits OBBusinessListBase(Of ShiftAuthList, ShiftAuth)

#Region " Business Methods "

    Public Function GetItem(IcrShiftID As Integer) As ShiftAuth

      'For Each child As ShiftAuth In Me
      '  If child.IcrShiftID = IcrShiftID Then
      '    Return child
      '  End If
      'Next
      'Return Nothing
      Return Me(0)

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

    Public Function GetShiftAuthHumanResource(HumanResourceID As Integer) As ShiftAuthHumanResource

      Dim obj As ShiftAuthHumanResource = Nothing
      For Each parent As ShiftAuth In Me
        obj = parent.ShiftAuthHumanResourceList.GetItem(HumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True, Roles:={"Shifts.Can Access Shift Authorisation", "Shifts.Can Access Shift Authorisation Playouts"})> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property UserID As Integer?
      Public Property SystemTeamID As Integer?

      <Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROUserSystemList), ValueMember:="SystemID", DisplayMember:="System"),
      Display(Name:="HR Sub-Dept"), Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID As Integer?

      <Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.General.ReadOnly.ROUserSystemAreaList), ThisFilterMember:="SystemID", ValueMember:="ProductionAreaID", DisplayMember:="ProductionArea"),
      Display(Name:="HR Area")>
      Public Property ProductionAreaID As Integer?

      Public Property ContractTypeIDs As List(Of Integer)

      <Display(Name:="Start Date"), Required>
      Public Property StartDate As DateTime?

      <Display(Name:="End Date"), Required>
      Public Property EndDate As DateTime?

      <Display(Name:="HR")>
      Public Property HumanResourceID As Integer?

      <Display(Name:="HR")>
      Public Property HumanResourceShiftID As Integer?

      Public Sub New()

      End Sub

      Public Sub New(UserID As Integer?, TeamID As Integer?,
                     SystemID As Integer?, ProductionAreaID As Integer?,
                     StartDate As DateTime?, EndDate As DateTime?,
                      HumanResourceID As Integer?, HumanResourceShiftID As Integer?)

        Me.UserID = UserID
        Me.SystemTeamID = SystemTeamID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.HumanResourceID = HumanResourceID
        Me.HumanResourceShiftID = HumanResourceShiftID

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewShiftAuthList() As ShiftAuthList

      Return New ShiftAuthList()

    End Function

    Public Shared Sub BeginGetShiftAuthList(CallBack As EventHandler(Of DataPortalResult(Of ShiftAuthList)))

      Dim dp As New DataPortal(Of ShiftAuthList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetShiftAuthList(UserID As Integer?, TeamID As Integer?,
                                             SystemID As Integer?, ProductionAreaID As Integer?,
                                             StartDate As DateTime?, EndDate As DateTime?,
                                             HumanResourceID As Integer?, HumanResourceShiftID As Integer?) As ShiftAuthList

      Return DataPortal.Fetch(Of ShiftAuthList)(New Criteria(UserID, TeamID, SystemID, ProductionAreaID, StartDate, EndDate, HumanResourceID, HumanResourceShiftID))

    End Function

    Public Shared Function GetShiftAuthList(ShiftAuthCriteria As OBLib.Shifts.Auth.ShiftAuthList.Criteria) As ShiftAuthList

      Return DataPortal.Fetch(Of ShiftAuthList)(ShiftAuthCriteria)

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ShiftAuth.GetShiftAuth(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ShiftAuth = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          parent = Me(0)
          parent.ShiftAuthHumanResourceList.RaiseListChangedEvents = False
          parent.ShiftAuthHumanResourceList.Add(ShiftAuthHumanResource.GetShiftAuthHumanResource(sdr))
          parent.ShiftAuthHumanResourceList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As ShiftAuthHumanResource = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.HumanResourceID <> sdr.GetInt32(1) Then
            parentChild = Me.GetShiftAuthHumanResource(sdr.GetInt32(1))
          End If
          parentChild.ShiftAuthHumanResourceShiftList.RaiseListChangedEvents = False
          parentChild.ShiftAuthHumanResourceShiftList.Add(ShiftAuthHumanResourceShift.GetShiftAuthHumanResourceShift(sdr))
          parentChild.ShiftAuthHumanResourceShiftList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing AndAlso Me.Count > 0 Then
            parent = Me(0)
          End If
          If parent IsNot Nothing Then
            parent.AccessMissingShiftList.RaiseListChangedEvents = False
            parent.AccessMissingShiftList.Add(Biometrics.AccessShift.GetAccessShift(sdr, OBLib.CommonData.Enums.AccessTerminalGroup.ICR))
            parent.AccessMissingShiftList.RaiseListChangedEvents = True
          End If
        End While
      End If

      For Each child As ShiftAuth In Me
        child.CheckRules()
        For Each ShiftAuthHumanResource As ShiftAuthHumanResource In child.ShiftAuthHumanResourceList
          ShiftAuthHumanResource.CheckRules()

          For Each ShiftAuthHumanResourceShift As ShiftAuthHumanResourceShift In ShiftAuthHumanResource.ShiftAuthHumanResourceShiftList
            ShiftAuthHumanResourceShift.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getShiftAuthList"
            cm.Parameters.AddWithValue("@UserID", Security.Settings.CurrentUser.UserID)
            cm.Parameters.AddWithValue("@SystemTeamID", NothingDBNull(crit.SystemTeamID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@HumanResourceID", NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@HumanResourceShiftID", NothingDBNull(crit.HumanResourceShiftID))
            cm.Parameters.AddWithValue("@ContractTypeIDs", Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.ContractTypeIDs)))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ShiftAuth In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ShiftAuth In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace