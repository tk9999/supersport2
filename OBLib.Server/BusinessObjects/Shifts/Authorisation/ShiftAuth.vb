﻿' Generated 14 Sep 2014 14:17 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Shifts.Auth

  <Serializable()> _
  Public Class ShiftAuth
    Inherits OBBusinessBase(Of ShiftAuth)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TeamProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Team, "Team", "")
    ''' <summary>
    ''' Gets the Year value
    ''' </summary>
    <Display(Name:="Team", Description:="")>
    Public ReadOnly Property Team() As String
      Get
        Return GetProperty(TeamProperty)
      End Get
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StartDate, "Start Date", "")
    ''' <summary>
    ''' Gets the Year value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public ReadOnly Property StartDate() As String
      Get
        Return GetProperty(StartDateProperty)
      End Get
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EndDate, "End Date", "")
    ''' <summary>
    ''' Gets the Year value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public ReadOnly Property EndDate() As String
      Get
        Return GetProperty(EndDateProperty)
      End Get
    End Property

    'Public Shared MRMonthIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.MRMonthID, "MRMonthID", 0)
    ' ''' <summary>
    ' ''' Gets the Year value
    ' ''' </summary>
    '<Display(Name:="MRMonthID", Description:="")>
    'Public ReadOnly Property MRMonthID() As Integer
    '  Get
    '    Return GetProperty(MRMonthIDProperty)
    '  End Get
    'End Property

    'Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ' ''' <summary>
    ' ''' Gets the Start Date value
    ' ''' </summary>
    '<Display(Name:="Start Date", Description:="")>
    'Public ReadOnly Property StartDate As DateTime?
    '  Get
    '    Return GetProperty(StartDateProperty)
    '  End Get
    'End Property

    'Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ' ''' <summary>
    ' ''' Gets the End Date value
    ' ''' </summary>
    '<Display(Name:="End Date", Description:="")>
    'Public ReadOnly Property EndDate As DateTime?
    '  Get
    '    Return GetProperty(EndDateProperty)
    '  End Get
    'End Property

    'Public Shared SupervisorIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SupervisorInd, "Supervisor Indicator", False)
    ' ''' <summary>
    ' ''' Gets the Supervisor Ind value
    ' ''' </summary>
    '<Display(Name:="Supervisor Indicator", Description:="")>
    'Public ReadOnly Property SupervisorInd() As Boolean
    '  Get
    '    Return GetProperty(SupervisorIndProperty)
    '  End Get
    'End Property

    'Public Shared ManagerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ManagerInd, "Manager Indicator", False)
    ' ''' <summary>
    ' ''' Gets the Supervisor Ind value
    ' ''' </summary>
    '<Display(Name:="Manager Indicator", Description:="")>
    'Public ReadOnly Property ManagerInd() As Boolean
    '  Get
    '    Return GetProperty(ManagerIndProperty)
    '  End Get
    'End Property

#End Region

#Region " Child Lists "

    Public Shared ShiftAuthHumanResourceListProperty As PropertyInfo(Of ShiftAuthHumanResourceList) = RegisterProperty(Of ShiftAuthHumanResourceList)(Function(c) c.ShiftAuthHumanResourceList, "Shift Auth Human Resource List")

    Public ReadOnly Property ShiftAuthHumanResourceList() As ShiftAuthHumanResourceList
      Get
        If GetProperty(ShiftAuthHumanResourceListProperty) Is Nothing Then
          LoadProperty(ShiftAuthHumanResourceListProperty, Shifts.Auth.ShiftAuthHumanResourceList.NewShiftAuthHumanResourceList())
        End If
        Return GetProperty(ShiftAuthHumanResourceListProperty)
      End Get
    End Property

    Public Shared AccessShiftListProperty As PropertyInfo(Of Biometrics.AccessShiftList) = RegisterProperty(Of Biometrics.AccessShiftList)(Function(c) c.AccessMissingShiftList, "Shift Missing list")

    Public ReadOnly Property AccessMissingShiftList() As Biometrics.AccessShiftList
      Get
        If GetProperty(AccessShiftListProperty) Is Nothing Then
          LoadProperty(AccessShiftListProperty, Biometrics.AccessShiftList.NewAccessShiftList)
        End If
        Return GetProperty(AccessShiftListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(StartDateProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.StartDate

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

    'Public Function GetSupervisorInd() As Boolean
    '  Return GetProperty(SupervisorIndProperty)
    'End Function

    'Public Function GetTeamLeaderInd() As Boolean
    '  Return GetProperty(TeamLeaderIndProperty)
    'End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewShiftAuth() method.

    End Sub

    Public Shared Function NewShiftAuth() As ShiftAuth

      Return DataPortal.CreateChild(Of ShiftAuth)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetShiftAuth(dr As SafeDataReader) As ShiftAuth

      Dim m As New ShiftAuth()
      m.Fetch(dr)
      Return m

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          'LoadProperty(MRMonthIDProperty, .GetInt32(0))
          'LoadProperty(ShiftProperty, .GetInt32(1))
          'LoadProperty(YearProperty, .GetInt32(2))
          'LoadProperty(StartDateProperty, .GetValue(3))
          'LoadProperty(EndDateProperty, .GetValue(4))
          'LoadProperty(SupervisorIndProperty, .GetBoolean(5))
          ''LoadProperty(ManagerIndProperty, .GetBoolean(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insShiftAuth"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updShiftAuth"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If GetProperty(ShiftAuthHumanResourceListProperty) IsNot Nothing Then
        Me.ShiftAuthHumanResourceList.Update()
      End If
      'If Me.IsSelfDirty Then

      '  With cm
      '    .CommandType = CommandType.StoredProcedure

      '    Dim paramMRMonthID As SqlParameter = .Parameters.Add("@MRMonthID", SqlDbType.Int)
      '    paramMRMonthID.Value = GetProperty(MRMonthIDProperty)
      '    If Me.IsNew Then
      '      paramMRMonthID.Direction = ParameterDirection.Output
      '    End If
      '    .Parameters.AddWithValue("@Shift", GetProperty(ShiftProperty))
      '    .Parameters.AddWithValue("@Year", GetProperty(YearProperty))
      '    .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
      '    .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)

      '    .ExecuteNonQuery()

      '    If Me.IsNew Then
      '      LoadProperty(MRMonthIDProperty, paramMRMonthID.Value)
      '    End If
      '    ' update child objects
      '    If GetProperty(ShiftAuthHumanResourceListProperty) IsNot Nothing Then
      '      Me.ShiftAuthHumanResourceList.Update()
      '    End If
      '    MarkOld()
      '  End With
      'Else
      '  ' update child objects
      '  If GetProperty(ShiftAuthHumanResourceListProperty) IsNot Nothing Then
      '    Me.ShiftAuthHumanResourceList.Update()
      '  End If
      'End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delShiftAuth"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@MRMonthID", GetProperty(MRMonthIDProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace