﻿' Generated 14 Sep 2014 14:17 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Shifts.Auth

  <Serializable()> _
  Public Class ShiftAuthHumanResourceList
    Inherits OBBusinessListBase(Of ShiftAuthHumanResourceList, ShiftAuthHumanResource)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ShiftAuthHumanResource

      For Each child As ShiftAuthHumanResource In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

    Public Function GetShiftAuthHumanResourceShift(HumanResourceShiftID As Integer) As ShiftAuthHumanResourceShift

      Dim obj As ShiftAuthHumanResourceShift = Nothing
      For Each parent As ShiftAuthHumanResource In Me
        obj = parent.ShiftAuthHumanResourceShiftList.GetItem(HumanResourceShiftID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetShiftAuthHumanResourceShiftByGuid(Guid As Guid) As ShiftAuthHumanResourceShift

      Dim obj As ShiftAuthHumanResourceShift = Nothing
      For Each parent As ShiftAuthHumanResource In Me
        obj = parent.ShiftAuthHumanResourceShiftList.GetItemByGuid(Guid)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewShiftAuthHumanResourceList() As ShiftAuthHumanResourceList

      Return New ShiftAuthHumanResourceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ShiftAuthHumanResource In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ShiftAuthHumanResource In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace