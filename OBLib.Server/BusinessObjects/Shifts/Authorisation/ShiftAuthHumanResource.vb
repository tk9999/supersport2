﻿' Generated 14 Sep 2014 14:17 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.Security

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Shifts.Auth

  <Serializable()> _
  Public Class ShiftAuthHumanResource
    Inherits OBBusinessBase(Of ShiftAuthHumanResource)

#Region " Properties and Methods "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsExpanded, "Is Expanded", False)
    ''' <summary>
    ''' Gets and sets the Is Finalised value
    ''' </summary>
    <Display(Name:="Is Expanded", Description:=""), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

#Region " Properties "

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    'Public Shared MRMonthProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MRMonth, "MR Month", "")
    ' ''' <summary>
    ' ''' Gets the Human Resource value
    ' ''' </summary>
    '<Display(Name:="MR Month", Description:="")>
    'Public ReadOnly Property MRMonth() As String
    '  Get
    '    Return GetProperty(MRMonthProperty)
    '  End Get
    'End Property

    Public Shared NoOfShiftsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfShifts, "No. Of Shifts", 0)
    ''' <summary>
    ''' Gets the Shift Count value
    ''' </summary>
    <Display(Name:="Shift Count", Description:="")>
    Public ReadOnly Property NoOfShifts() As Integer
      Get
        Return ShiftAuthHumanResourceShiftList.Count 'GetProperty(NoOfShiftsProperty)
      End Get
    End Property

    Public Shared TotalShiftHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalShiftHours, "Shift Hrs", 0)
    ''' <summary>
    ''' Gets the Total Hours Worked value
    ''' </summary>
    <Display(Name:="Shift Hrs", Description:="")>
    Public ReadOnly Property TotalShiftHours() As Decimal
      Get
        Return GetProperty(TotalShiftHoursProperty)
      End Get
    End Property

    Public Shared ExtraShiftHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ExtraShiftHours, "Extra Hrs", 0)
    ''' <summary>
    ''' Gets the Extra Shift Hours value
    ''' </summary>
    <Display(Name:="Extra Hrs", Description:="")>
    Public ReadOnly Property ExtraShiftHours() As Decimal
      Get
        Return GetProperty(ExtraShiftHoursProperty)
      End Get
    End Property

    Public Shared PublicHolidayHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.PublicHolidayHours, "PH Hrs", 0)
    ''' <summary>
    ''' Gets the Extra Shift Hours value
    ''' </summary>
    <Display(Name:="PH Hrs", Description:="")>
    Public ReadOnly Property PublicHolidayHours() As Decimal
      Get
        Return GetProperty(PublicHolidayHoursProperty)
      End Get
    End Property

    Public Shared WeekendHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.WeekendHours, "Wknd Hrs", 0)
    ''' <summary>
    ''' Gets the Extra Shift Hours value
    ''' </summary>
    <Display(Name:="Wknd Hrs", Description:="")>
    Public ReadOnly Property WeekendHours() As Decimal
      Get
        Return GetProperty(WeekendHoursProperty)
      End Get
    End Property

    'Public Shared MRMonthIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.MRMonthID, "Icr Shift", Nothing)
    ' ''' <summary>
    ' ''' Gets the Icr Shift value
    ' ''' </summary>
    '<Display(AutoGenerateField:=False), Browsable(True)>
    'Public ReadOnly Property MRMonthID() As Integer?
    '  Get
    '    Return GetProperty(MRMonthIDProperty)
    '  End Get
    'End Property

    Public Shared SupervisorPendShiftCountProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.SupervisorPendShiftCount, 0)
    ''' <summary>
    ''' Gets the Supervisor Authorised Shift Count value
    ''' </summary>
    <Display(Name:="Sup. Pend", Description:="")>
    Public ReadOnly Property SupervisorPendShiftCount() As Integer
      Get
        Return ShiftAuthHumanResourceShiftList.Where(Function(c) c.SupervisorAuthInd Is Nothing).Count
      End Get
    End Property

    Public Shared SupervisorAuthShiftCountProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.SupervisorAuthShiftCount, 0)
    ''' <summary>
    ''' Gets the Supervisor Authorised Shift Count value
    ''' </summary>
    <Display(Name:="Sup. Auth", Description:="")>
    Public ReadOnly Property SupervisorAuthShiftCount() As Integer
      Get
        Return ShiftAuthHumanResourceShiftList.Where(Function(c) c.SupervisorAuthInd IsNot Nothing AndAlso c.SupervisorAuthInd = True).Count
      End Get
    End Property

    Public Shared SupervisorRejectShiftCountProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.SupervisorRejectShiftCount, 0)
    ''' <summary>
    ''' Gets the Supervisor Authorised Shift Count value
    ''' </summary>
    <Display(Name:="Sup. Rej", Description:="")>
    Public ReadOnly Property SupervisorRejectShiftCount() As Integer
      Get
        Return ShiftAuthHumanResourceShiftList.Where(Function(c) c.SupervisorAuthInd IsNot Nothing AndAlso c.SupervisorAuthInd = False).Count
      End Get
    End Property

    Public Shared ManagerPendShiftCountProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.ManagerPendShiftCount, 0)
    ''' <summary>
    ''' Gets the Extra Shift Hours value
    ''' </summary>
    <Display(Name:="Man. Pend", Description:="")>
    Public ReadOnly Property ManagerPendShiftCount() As Integer
      Get
        Return ShiftAuthHumanResourceShiftList.Where(Function(c) c.ManagerAuthInd Is Nothing).Count
      End Get
    End Property

    Public Shared ManagerAuthShiftCountProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.ManagerAuthShiftCount, 0)
    ''' <summary>
    ''' Gets the Extra Shift Hours value
    ''' </summary>
    <Display(Name:="Man. Auth", Description:="")>
    Public ReadOnly Property ManagerAuthShiftCount() As Integer
      Get
        Return ShiftAuthHumanResourceShiftList.Where(Function(c) c.ManagerAuthInd IsNot Nothing AndAlso c.ManagerAuthInd = True).Count
      End Get
    End Property

    Public Shared ManagerRejectShiftCountProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.ManagerRejectShiftCount, 0)
    ''' <summary>
    ''' Gets the Extra Shift Hours value
    ''' </summary>
    <Display(Name:="Man. Rej", Description:="")>
    Public ReadOnly Property ManagerRejectShiftCount() As Integer
      Get
        Return ShiftAuthHumanResourceShiftList.Where(Function(c) c.ManagerAuthInd IsNot Nothing AndAlso c.ManagerAuthInd = False).Count
      End Get
    End Property

    Public Shared AuthorisedNoOfMealsProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.AuthorisedNoOfMeals, 0)
    ''' <summary>
    ''' Gets and sets the Authorised Meal Vouchers value
    ''' </summary>
    <Display(Name:="Meals", Description:="")>
    Public ReadOnly Property AuthorisedNoOfMeals() As Integer
      Get
        Return GetProperty(AuthorisedNoOfMealsProperty)
      End Get
    End Property

    Public Shared AuthorisedReimbursementAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AuthorisedReimbursementAmount, "Mls Amnt", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Authorised Reimbursement Amount value
    ''' </summary>
    <Display(Name:="Mls Amnt", Description:=""),
    Required(ErrorMessage:="Mls Amnt required")>
    Public ReadOnly Property AuthorisedReimbursementAmount() As Decimal
      Get
        Return GetProperty(AuthorisedReimbursementAmountProperty)
      End Get
    End Property

    Public Shared AccessFlagCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessFlagCount, "Flags", 0)
    ''' <summary>
    ''' Gets total Access flag count
    ''' </summary>
    <Display(Name:="Flags", Description:="Total Access Flags")>
    Public ReadOnly Property AccessFlagCount() As Integer
      Get
        Return ShiftAuthHumanResourceShiftList.Where(Function(c) c.AccessFlagID > 0).Count
      End Get
    End Property

    <Singular.DataAnnotations.ClientOnly()>
    Public Property IsLoading As Boolean = False

    <Singular.DataAnnotations.ClientOnly()>
    Public Property SyncID As Integer = 0

    Public Shared AllowanceAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AllowanceAmount, "Allowance", 0)
    ''' <summary>
    ''' Gets and sets the Authorised Reimbursement Amount value
    ''' </summary>
    <Display(Name:="Allowance", Description:="")>
    Public ReadOnly Property AllowanceAmount() As Decimal
      Get
        Return GetProperty(AllowanceAmountProperty)
      End Get
    End Property

    Public ReadOnly Property IsICRManager As Boolean
      Get
        Dim SecurityGroupUser As SecurityGroupUser = Nothing
        SecurityGroupUser = OBLib.Security.Settings.CurrentUser.SecurityGroupUserList.Where(Function(d) CompareSafe(d.SecurityGroupID, CType(OBLib.CommonData.Enums.SecurityGroup.ICRManager, Integer))).FirstOrDefault()
        If SecurityGroupUser IsNot Nothing Then
          Return True
        Else
          Return False
        End If
      End Get
    End Property

    Public Shared AnnualLeaveAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AnnualLeaveAmount, "Annual", 0)
    ''' <summary>
    ''' Gets and sets Annual Leave
    ''' </summary>
    <Display(Name:="Annual", Description:="Annual Leave")>
    Public ReadOnly Property AnnualLeaveAmount() As Decimal
      Get
        Return GetProperty(AnnualLeaveAmountProperty)
      End Get
    End Property

    Public Shared SickLeaveAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.SickLeaveAmount, "Sick", 0)
    ''' <summary>
    ''' Gets and sets Annual Leave
    ''' </summary>
    <Display(Name:="Sick", Description:="Sick Leave")>
    Public ReadOnly Property SickLeaveAmount() As Decimal
      Get
        Return GetProperty(SickLeaveAmountProperty)
      End Get
    End Property

    Public Shared WeekdayHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.WeekdayHours, "Wkday Hrs", 0)
    ''' <summary>
    ''' Gets the Weekday hours value
    ''' </summary>
    <Display(Name:="Wkday Hrs", Description:="")>
    Public ReadOnly Property WeekdayHours() As Decimal
      Get
        Return GetProperty(WeekdayHoursProperty)
      End Get
    End Property

    Public Shared AllHoursProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AllHours, "Total Hrs", 0)
    ''' <summary>
    ''' Gets the Weekday hours value
    ''' </summary>
    <Display(Name:="Total Hrs", Description:="")>
    Public ReadOnly Property AllHours() As Decimal
      Get
        Return GetProperty(AllHoursProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ShiftAuthHumanResourceShiftListProperty As PropertyInfo(Of ShiftAuthHumanResourceShiftList) = RegisterProperty(Of ShiftAuthHumanResourceShiftList)(Function(c) c.ShiftAuthHumanResourceShiftList, "Shift Auth Human Resource Shift List")
    Public ReadOnly Property ShiftAuthHumanResourceShiftList() As ShiftAuthHumanResourceShiftList
      Get
        If GetProperty(ShiftAuthHumanResourceShiftListProperty) Is Nothing Then
          LoadProperty(ShiftAuthHumanResourceShiftListProperty, Shifts.Auth.ShiftAuthHumanResourceShiftList.NewShiftAuthHumanResourceShiftList())
        End If
        Return GetProperty(ShiftAuthHumanResourceShiftListProperty)
      End Get
    End Property


#End Region

#Region " Methods "

    Public Function GetParent() As ShiftAuth

      Return CType(CType(Me.Parent, ShiftAuthHumanResourceList).Parent, ShiftAuth)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.HumanResource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Shift Auth Human Resource")
        Else
          Return String.Format("Blank {0}", "Shift Auth Human Resource")
        End If
      Else
        Return Me.HumanResource
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewShiftAuthHumanResource() method.

    End Sub

    Public Shared Function NewShiftAuthHumanResource() As ShiftAuthHumanResource

      Return DataPortal.CreateChild(Of ShiftAuthHumanResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetShiftAuthHumanResource(dr As SafeDataReader) As ShiftAuthHumanResource

      Dim m As New ShiftAuthHumanResource()
      m.Fetch(dr)
      Return m

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceIDProperty, .GetInt32(0))
          'LoadProperty(MRMonthIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceProperty, .GetString(1))
          LoadProperty(TotalShiftHoursProperty, .GetDecimal(2))
          LoadProperty(ExtraShiftHoursProperty, .GetDecimal(3))
          LoadProperty(PublicHolidayHoursProperty, .GetDecimal(4))
          LoadProperty(WeekendHoursProperty, .GetDecimal(5))
          LoadProperty(AuthorisedNoOfMealsProperty, .GetInt32(6))
          LoadProperty(AuthorisedReimbursementAmountProperty, .GetDecimal(7))
          LoadProperty(AllowanceAmountProperty, .GetDecimal(8))
          LoadProperty(AnnualLeaveAmountProperty, .GetDecimal(9))
          LoadProperty(SickLeaveAmountProperty, .GetDecimal(10))
          LoadProperty(WeekdayHoursProperty, .GetDecimal(11))
          LoadProperty(AllHoursProperty, .GetDecimal(12))
          'LoadProperty(MRMonthProperty, .GetString(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insShiftAuthHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updShiftAuthHumanResource"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceID As SqlParameter = .Parameters.Add("@HumanResourceID", SqlDbType.Int)
          paramHumanResourceID.Value = GetProperty(HumanResourceIDProperty)
          If Me.IsNew Then
            paramHumanResourceID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@HumanResource", GetProperty(HumanResourceProperty))
          .Parameters.AddWithValue("@TotalHoursWorked", GetProperty(TotalShiftHoursProperty))
          .Parameters.AddWithValue("@ExtraShiftHours", GetProperty(ExtraShiftHoursProperty))
          '.Parameters.AddWithValue("@MRMonthID", GetProperty(MRMonthIDProperty))

          '.ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceIDProperty, paramHumanResourceID.Value)
          End If
          ' update child objects
          If GetProperty(ShiftAuthHumanResourceShiftListProperty) IsNot Nothing Then
            Me.ShiftAuthHumanResourceShiftList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(ShiftAuthHumanResourceShiftListProperty) IsNot Nothing Then
          Me.ShiftAuthHumanResourceShiftList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delShiftAuthHumanResource"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace