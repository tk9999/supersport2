﻿' Generated 14 Sep 2014 14:17 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.ICR.ReadOnly
Imports OBLib.Maintenance.ShiftPatterns
Imports OBLib.Biometrics
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Shifts.Auth

  <Serializable()> _
  Public Class ShiftAuthHumanResourceShift
    Inherits OBBusinessBase(Of ShiftAuthHumanResourceShift)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceShiftID, "ID", 0)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key(), Browsable(True)>
    Public Property HumanResourceShiftID() As Integer
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SwappedHumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SwappedHumanResourceShiftID, "Swapped Human Resource Shift", Nothing)
    ''' <summary>
    ''' Gets and sets the Swapped Human Resource Shift value
    ''' </summary>
    <Display(Name:="Swapped Human Resource Shift", Description:="")>
    Public Property SwappedHumanResourceShiftID() As Integer?
      Get
        Return GetProperty(SwappedHumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SwappedHumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared SystemAreaShiftPatternWeekDayIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemAreaShiftPatternWeekDayID, "System Area Shift Pattern Week Day", Nothing)
    ''' <summary>
    ''' Gets and sets the System Area Shift Pattern Week Day value
    ''' </summary>
    <Display(Name:="System Area Shift Pattern Week Day", Description:="")>
    Public Property SystemAreaShiftPatternWeekDayID() As Integer?
      Get
        Return GetProperty(SystemAreaShiftPatternWeekDayIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemAreaShiftPatternWeekDayIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Location", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROSystemAllowedAreaList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo, ValueMember:="ProductionAreaID", DisplayMember:="ProductionArea", FilterMethodName:="AuthHelper.Methods.FilterSystemAllowedAreas")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ShiftTypeID, "Shift Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Shift No value
    ''' </summary>
    <Display(Name:="Shift Type", Description:=""),
    Singular.DataAnnotations.DropDownWeb(GetType(ROShiftTypeList))>
    Public Property ShiftTypeID() As Integer?
      Get
        Return GetProperty(ShiftTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ShiftTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ShiftDateProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftDate, "Date", "")
    ''' <summary>
    ''' Gets and sets the Shift No value
    ''' </summary>
    <Display(Name:="Date", Description:="")>
    Public Property ShiftDate() As String
      Get
        Return GetProperty(ShiftDateProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ShiftDateProperty, Value)
      End Set
    End Property

    Public Shared ScheduleDatePlayoutProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleDatePlayout, "Date")
    ''' <summary>
    ''' Gets and sets the Schedule Date value
    ''' </summary>
    <Display(Name:="Date", Description:=""),
    DateField(FormatString:="dd-MMM-yy")>
    Public Property ScheduleDatePlayout As DateTime?
      Get
        SetProperty(ScheduleDatePlayoutProperty, GetProperty(ScheduleDateProperty))
        Return GetProperty(ScheduleDatePlayoutProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleDatePlayoutProperty, GetProperty(ScheduleDateProperty))
      End Set
    End Property

    Public Shared ScheduleDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleDate, "Date")
    ''' <summary>
    ''' Gets and sets the Schedule Date value
    ''' </summary>
    <Display(Name:="Date", Description:=""),
    DateField(FormatString:="ddd dd")>
    Public Property ScheduleDate As DateTime?
      Get
        Return GetProperty(ScheduleDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleDateProperty, Value)
      End Set
    End Property
    'MMM yyyy

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    TimeField(TimeFormat:=TimeFormats.ShortTime),
    SetExpression("ShiftAuthHumanResourceShiftBO.SetScheduleDate(self)")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ExtraShiftIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ExtraShiftInd, "Extra Shift", False)
    ''' <summary>
    ''' Gets and sets the Extra Shift value
    ''' </summary>
    <Display(Name:="Extra Shift", Description:="")>
    Public Property ExtraShiftInd() As Boolean
      Get
        Return GetProperty(ExtraShiftIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ExtraShiftIndProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemTeamID, "System Team", Nothing)
    ''' <summary>
    ''' Gets and sets the System Team value
    ''' </summary>
    <Display(Name:="System Team", Description:="")>
    Public Property SystemTeamID() As Integer?
      Get
        Return GetProperty(SystemTeamIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemTeamIDProperty, Value)
      End Set
    End Property
    'Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemTeamList), DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.Combo)

    Public Shared SupervisorAuthIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.SupervisorAuthInd, "Supervisor Auth", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Supervisor Auth value
    ''' </summary>
    <Display(Name:="Supervisor Auth.", Description:="")>
    Public Property SupervisorAuthInd() As Boolean?
      Get
        Return GetProperty(SupervisorAuthIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        'If Singular.Security.HasAccess("ICR", "Can Authorise HR Shift - Supervisor") Then
        SetProperty(SupervisorAuthIndProperty, Value)
        SetSupervisorAuthBy(Value)
        'End If
      End Set
    End Property

    Public Shared SupervisorAuthByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupervisorAuthBy, "Supervisor Auth By", Nothing)
    ''' <summary>
    ''' Gets and sets the Supervisor Auth By value
    ''' </summary>
    <Display(Name:="Supervisor Auth By", Description:="")>
    Public ReadOnly Property SupervisorAuthBy() As Integer?
      Get
        Return GetProperty(SupervisorAuthByProperty)
      End Get
    End Property

    Public Shared SuperAuthDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SuperAuthDateTime, "Super Auth Date Time")
    ''' <summary>
    ''' Gets and sets the Super Auth Date Time value
    ''' </summary>
    <Display(Name:="Super Auth Date Time", Description:="")>
    Public ReadOnly Property SuperAuthDateTime As DateTime?
      Get
        Return GetProperty(SuperAuthDateTimeProperty)
      End Get
    End Property

    Public Shared ManagerAuthIndProperty As PropertyInfo(Of Boolean?) = RegisterSProperty(Of Boolean?)(Function(c) c.ManagerAuthInd, CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Manager Auth value
    ''' </summary>
    <Display(Name:="Manager Auth.", Description:=""),
    SetExpressionBeforeChange("ShiftAuthHumanResourceShiftBO.PreAuthChanged(self, args)"),
    SetExpression("ShiftAuthHumanResourceShiftBO.AuthChanged(self)")>
    Public Property ManagerAuthInd() As Boolean?
      Get
        Return GetProperty(ManagerAuthIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        'If Singular.Security.HasAccess("ICR", "Can Authorise HR Shift - Manager") Then
        SetProperty(ManagerAuthIndProperty, Value)
        SetManagerAuthBy(Value)
        'End If
      End Set
    End Property

    Public Shared ManagerAuthByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerAuthBy, "Line Manager Auth By", Nothing)
    ''' <summary>
    ''' Gets and sets the Manager Auth By value
    ''' </summary>
    <Display(Name:="Line Manager Auth By", Description:="")>
    Public ReadOnly Property ManagerAuthBy() As Integer?
      Get
        Return GetProperty(ManagerAuthByProperty)
      End Get
    End Property

    Public Shared ManagerAuthDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ManagerAuthDateTime, "Line Manager Auth Date Time")
    ''' <summary>
    ''' Gets and sets the Manager Auth Date Time value
    ''' </summary>
    <Display(Name:="Line Manager Auth Date Time", Description:="")>
    Public Property ManagerAuthDateTime As DateTime?
      Get
        Return GetProperty(ManagerAuthDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ManagerAuthDateTimeProperty, Value)
      End Set
    End Property

    'Public Shared OffDayIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OffDayInd, "Extra Shift", False)
    ' ''' <summary>
    ' ''' Gets and sets the Extra Shift value
    ' ''' </summary>
    '<Display(Name:="Off Day", Description:="Indicating if this is an Off Day")>
    'Public Property OffDayInd() As Boolean
    '  Get
    '    Return GetProperty(OffDayIndProperty)
    '  End Get
    '  Set(ByVal Value As Boolean)
    '    SetProperty(OffDayIndProperty, Value)
    '  End Set
    'End Property

    'Public Shared TeamProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Team, "Team Name", "")
    ' ''' <summary>
    ' ''' Gets the Team Name value
    ' ''' </summary>
    '<Display(Name:="Team Name", Description:="")>
    'Public ReadOnly Property Team() As String
    '  Get
    '    Return GetProperty(TeamProperty)
    '  End Get
    'End Property

    'Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceOffPeriodID, "Human Resource Off Period", Nothing)
    ' ''' <summary>
    ' ''' Gets the Human Resource Off Period value
    ' ''' </summary>
    '<Display(Name:="Human Resource Off Period", Description:="")>
    'Public ReadOnly Property HumanResourceOffPeriodID() As Integer?
    '  Get
    '    Return GetProperty(HumanResourceOffPeriodIDProperty)
    '  End Get
    'End Property

    'Public Shared OffReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.OffReason, "Off Reason", "")
    ' ''' <summary>
    ' ''' Gets the Off Reason value
    ' ''' </summary>
    '<Display(Name:="Off Reason", Description:="")>
    'Public ReadOnly Property OffReason() As String
    '  Get
    '    Return GetProperty(OffReasonProperty)
    '  End Get
    'End Property

    'Public Shared OffReasonIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.OffReasonID, "Off Reason", Nothing)
    ' ''' <summary>
    ' ''' Gets the Off Reason value
    ' ''' </summary>
    '<Display(Name:="Off Reason", Description:="")>
    'Public ReadOnly Property OffReasonID() As Integer?
    '  Get
    '    Return GetProperty(OffReasonIDProperty)
    '  End Get
    'End Property

    Public Shared StaffAcknowledgeIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.StaffAcknowledgeInd, "Staff Acknowledgement", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Manager Auth value
    ''' </summary>
    <Display(Name:="Staff Acknowledgement", Description:="")>
    Public Property StaffAcknowledgeInd() As Boolean?
      Get
        Return GetProperty(StaffAcknowledgeIndProperty)
      End Get
      Set(value As Boolean?)
        SetProperty(StaffAcknowledgeIndProperty, value)
        SetStaffAck(value)
      End Set
    End Property

    Public Shared StaffAcknowledgeByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.StaffAcknowledgeBy, "Staff Acknowledgement By", Nothing)
    ''' <summary>
    ''' Gets and sets the Manager Auth By value
    ''' </summary>
    <Display(Name:="Staff Acknowledgement By", Description:="")>
    Public ReadOnly Property StaffAcknowledgeBy() As Integer?
      Get
        Return GetProperty(StaffAcknowledgeByProperty)
      End Get
    End Property

    Public Shared StaffAcknowledgeDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StaffAcknowledgeDateTime, "Staff Ackn Date")
    ''' <summary>
    ''' Gets and sets the Manager Auth Date Time value
    ''' </summary>
    <Display(Name:="Staff Ackn Date", Description:="")>
    Public ReadOnly Property StaffAcknowledgeDateTime As DateTime?
      Get
        Return GetProperty(StaffAcknowledgeDateTimeProperty)
      End Get
    End Property

    Public Shared ShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftType, "Type", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Type", Description:="")>
    Public ReadOnly Property ShiftType() As String
      Get
        Return GetProperty(ShiftTypeProperty)
      End Get
    End Property

    Public Shared ShiftDurationProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.ShiftDuration, "Duration", 0)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Duration", Description:="")>
    Public ReadOnly Property ShiftDuration() As Decimal
      Get
        Return GetProperty(ShiftDurationProperty)
      End Get
    End Property

    Public Shared PublicHolidayIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PublicHolidayInd, "Supervisor Auth", False)
    ''' <summary>
    ''' Gets and sets the Supervisor Auth value
    ''' </summary>
    <Display(Name:="Public Holiday?", Description:="")>
    Public ReadOnly Property PublicHolidayInd() As Boolean
      Get
        Return GetProperty(PublicHolidayIndProperty)
      End Get
    End Property

    Public Shared SupervisorRejectedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupervisorRejectedReason, "Sup. Rej Reason", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="S. Reason", Description:="")>
    Public Property SupervisorRejectedReason() As String
      Get
        Return GetProperty(SupervisorRejectedReasonProperty)
      End Get
      Set(value As String)
        SetProperty(SupervisorRejectedReasonProperty, value)
      End Set
    End Property

    Public Shared ManagerRejectedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerRejectedReason, "Line Man. Rej Reason", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="LM. Reason", Description:="")>
    Public Property ManagerRejectedReason() As String
      Get
        Return GetProperty(ManagerRejectedReasonProperty)
      End Get
      Set(value As String)
        SetProperty(ManagerRejectedReasonProperty, value)
      End Set
    End Property

    Public Shared AccessFlagIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AccessFlagID, "Access Flag", Nothing)
    ''' <summary>
    ''' Gets he total of Biomentics Access logs
    ''' </summary>
    <Display(Name:="Access Flag", Description:="Biometrics Access Flag (Total)")>
    Public ReadOnly Property AccessFlagID() As Integer
      Get
        Return GetProperty(AccessFlagIDProperty)
      End Get
    End Property

    Public Shared AccessTerminalGroupIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessTerminalGroupID, "AccessTerminalGroupID", Nothing)
    ''' <summary>
    ''' Gets and sets the Manager Auth By value
    ''' </summary>
    <Display(Name:="AccessTerminalGroupID", Description:="")>
    Public ReadOnly Property AccessTerminalGroupID() As Integer?
      Get
        Return GetProperty(AccessTerminalGroupIDProperty)
      End Get
    End Property

    Public Shared AllowanceAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.AllowanceAmount, "Allowance", 0)
    ''' <summary>
    ''' Gets and sets the Authorised Reimbursement Amount value
    ''' </summary>
    <Display(Name:="Allowance", Description:="")>
    Public ReadOnly Property AllowanceAmount() As Decimal
      Get
        Return GetProperty(AllowanceAmountProperty)
      End Get
    End Property

    Public Shared MRMonthIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.MRMonthID, "Icr Shift", Nothing)
    ''' <summary>
    ''' Gets the MRMonth ID value
    ''' </summary>
    Public Property MRMonthID() As Integer?
      Get
        Return GetProperty(MRMonthIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(MRMonthIDProperty, value)
      End Set
    End Property

    Public Shared StaffDisputeReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StaffDisputeReason, "Emp. Comments", "")
    ''' <summary>
    ''' Gets and sets the Staff Dispute Reason value
    ''' </summary>
    <Display(Name:="Emp. Comments", Description:=""),
    StringLength(1024, ErrorMessage:="Dispute Reason cannot be more than 1024 characters")>
    Public Property StaffDisputeReason() As String
      Get
        Return GetProperty(StaffDisputeReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StaffDisputeReasonProperty, Value)
      End Set
    End Property

    Public Shared MRMonthProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MRMonth, "MR Month", "")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="MR Month", Description:="")>
    Public ReadOnly Property MRMonth() As String
      Get
        Return GetProperty(MRMonthProperty)
      End Get
    End Property

    Public Shared ENAProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ENA, "ENA", Nothing)
    ''' <summary>
    ''' Gets the ENA value
    ''' </summary>
    Public ReadOnly Property ENA() As Integer?
      Get
        Return GetProperty(ENAProperty)
      End Get
    End Property

    Public Shared LNAProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.LNA, "LNA", Nothing)
    ''' <summary>
    ''' Gets the LNA value
    ''' </summary>
    Public ReadOnly Property LNA() As Integer?
      Get
        Return GetProperty(LNAProperty)
      End Get
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StartTime, "Start Time", "")
    ''' <summary>
    ''' Gets and sets the Staff Dispute Reason value
    ''' </summary>
    Public ReadOnly Property StartTime() As String
      Get
        Return GetProperty(StartTimeProperty)
      End Get
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EndTime, "End Time", "")
    ''' <summary>
    ''' Gets and sets the Staff Dispute Reason value
    ''' </summary>
    Public ReadOnly Property EndTime() As String
      Get
        Return GetProperty(EndTimeProperty)
      End Get
    End Property



#End Region

#Region " Methods "

    Public Function GetParent() As ShiftAuthHumanResource

      Return CType(CType(Me.Parent, ShiftAuthHumanResourceShiftList).Parent, ShiftAuthHumanResource)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceShiftIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.GetParent.HumanResource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Shift Auth Human Resource Shift")
        Else
          Return String.Format("Blank {0}", "Shift Auth Human Resource Shift")
        End If
      Else
        Return Me.GetParent.HumanResource
      End If

    End Function

    Private Sub SetSupervisorAuthBy(ByVal Value As Boolean?)
      If Value IsNot Nothing Then
        SetProperty(SupervisorAuthByProperty, Security.Settings.CurrentUser.UserID)
        SetProperty(SuperAuthDateTimeProperty, Now)
      Else
        SetProperty(SupervisorAuthByProperty, Nothing)
        SetProperty(SuperAuthDateTimeProperty, Nothing)
      End If
    End Sub

    Private Sub SetManagerAuthBy(ByVal Value As Boolean?)
      If Value IsNot Nothing Then
        SetProperty(ManagerAuthByProperty, Security.Settings.CurrentUser.UserID)
        SetProperty(ManagerAuthDateTimeProperty, Now)
      Else
        SetProperty(SupervisorAuthByProperty, Nothing)
        SetProperty(SuperAuthDateTimeProperty, Nothing)
      End If
    End Sub

    Private Sub SetStaffAck(ByVal Value As Boolean?)
      If Value IsNot Nothing Then
        SetProperty(StaffAcknowledgeByProperty, Security.Settings.CurrentUser.UserID)
        SetProperty(StaffAcknowledgeDateTimeProperty, Now)
      Else
        SetProperty(StaffAcknowledgeByProperty, Nothing)
        SetProperty(StaffAcknowledgeDateTimeProperty, Nothing)
      End If
    End Sub

#End Region

#Region " Child Lists "

    Public Shared AccessFlagListProperty As PropertyInfo(Of ROAccessFlagList) = RegisterProperty(Of ROAccessFlagList)(Function(c) c.AccessFlagList, "Flags")

    Public ReadOnly Property AccessFlagList() As ROAccessFlagList
      Get
        If GetProperty(AccessFlagListProperty) Is Nothing Then

          LoadProperty(AccessFlagListProperty, Biometrics.ROAccessFlagList.GetROAccessFlagList(AccessFlagID))
        End If
        Return GetProperty(AccessFlagListProperty)
      End Get
    End Property


#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(SupervisorRejectedReasonProperty)
        .JavascriptRuleFunctionName = "CheckSupervisorRejectedReason"
        .ServerRuleFunction = Function(ts)
                                If CompareSafe(ts.SystemID, CType(OBLib.CommonData.Enums.System.ICR, Integer)) Then
                                  If ts.SupervisorAuthInd = False AndAlso Singular.Misc.IsNullNothingOrEmpty(ts.SupervisorRejectedReason) Then
                                    Return "Rejected Reason is required"
                                  End If
                                  If ts.SupervisorAuthInd AndAlso Singular.Misc.IsNullNothingOrEmpty(ts.SupervisorRejectedReason) AndAlso ts.AccessFlagID > 0 Then
                                    Return "Authorising a flagged Shift, Reason is required"
                                  End If
                                End If
                                Return ""
                              End Function
        .AffectedProperties.Add(SupervisorAuthIndProperty)
        .AddTriggerProperty(SupervisorAuthIndProperty)
      End With

      With AddWebRule(ManagerRejectedReasonProperty)
        .JavascriptRuleFunctionName = "CheckManagerRejectedReason"
        .ServerRuleFunction = Function(ts)
                                If ts.ManagerAuthInd = False AndAlso Singular.Misc.IsNullNothingOrEmpty(ts.ManagerRejectedReason) Then
                                  Return "Rejected Reason is required"
                                End If
                                Return ""
                                If ts.ManagerAuthInd = True AndAlso Singular.Misc.IsNullNothingOrEmpty(ts.ManagerRejectedReason) AndAlso ts.AccessFlagID > 0 Then
                                  Return "Authorising a flagged Shift, Reason is required"
                                End If
                                Return ""
                              End Function
        .AffectedProperties.Add(ManagerAuthIndProperty)
        .AddTriggerProperty(ManagerAuthIndProperty)
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewShiftAuthHumanResourceShift() method.

    End Sub

    Public Shared Function NewShiftAuthHumanResourceShift() As ShiftAuthHumanResourceShift

      Return DataPortal.CreateChild(Of ShiftAuthHumanResourceShift)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetShiftAuthHumanResourceShift(dr As SafeDataReader) As ShiftAuthHumanResourceShift

      Dim m As New ShiftAuthHumanResourceShift()
      m.Fetch(dr)
      Return m

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceShiftIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SwappedHumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(SystemAreaShiftPatternWeekDayIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(ShiftTypeIDProperty, .GetInt32(6))
          LoadProperty(ScheduleDateProperty, .GetValue(7))
          LoadProperty(StartDateTimeProperty, .GetValue(8))
          LoadProperty(EndDateTimeProperty, .GetValue(9))
          LoadProperty(CreatedByProperty, .GetInt32(10))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(11))
          LoadProperty(ModifiedByProperty, .GetInt32(12))
          LoadProperty(ModifiedDateTimeProperty, .GetDateTime(13))
          LoadProperty(ExtraShiftIndProperty, .GetBoolean(14))
          LoadProperty(SystemTeamIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(SupervisorAuthIndProperty, .GetValue(16))
          LoadProperty(SupervisorAuthByProperty, ZeroNothing(.GetInt32(17)))
          LoadProperty(SuperAuthDateTimeProperty, .GetValue(18))
          LoadProperty(ManagerAuthIndProperty, .GetValue(19))
          LoadProperty(ManagerAuthByProperty, ZeroNothing(.GetInt32(20)))
          LoadProperty(ManagerAuthDateTimeProperty, .GetValue(21))
          'LoadProperty(HumanResourceProperty, .GetString(22))
          'LoadProperty(OffDayIndProperty, .GetBoolean(22))
          'LoadProperty(TeamProperty, .GetString(23))
          'LoadProperty(HumanResourceOffPeriodIDProperty, ZeroNothing(.GetInt32(24)))
          'LoadProperty(OffReasonProperty, .GetString(25))
          'LoadProperty(OffReasonIDProperty, ZeroNothing(.GetInt32(26)))
          LoadProperty(StaffAcknowledgeIndProperty, .GetValue(22))
          LoadProperty(StaffAcknowledgeByProperty, ZeroNothing(.GetInt32(23)))
          LoadProperty(StaffAcknowledgeDateTimeProperty, .GetValue(24))
          LoadProperty(ShiftTypeProperty, .GetString(25))
          LoadProperty(ShiftDurationProperty, .GetDecimal(26))
          LoadProperty(PublicHolidayIndProperty, .GetBoolean(27))
          LoadProperty(SupervisorRejectedReasonProperty, .GetString(28))
          LoadProperty(ManagerRejectedReasonProperty, .GetString(29))
          LoadProperty(AccessFlagIDProperty, .GetInt32(30))
          LoadProperty(AccessTerminalGroupIDProperty, ZeroNothing(.GetInt32(31)))
          LoadProperty(AllowanceAmountProperty, .GetDecimal(32))
          LoadProperty(MRMonthProperty, .GetString(33))
          LoadProperty(MRMonthIDProperty, ZeroNothing(.GetInt32(34)))
          LoadProperty(StaffDisputeReasonProperty, .GetString(35))
          LoadProperty(ENAProperty, .GetInt32(36))
          LoadProperty(LNAProperty, .GetInt32(37))
          LoadProperty(StartTimeProperty, .GetString(38))
          LoadProperty(EndTimeProperty, .GetString(39))
        End With
      End Using

      LoadProperty(ShiftDateProperty, If(Me.ScheduleDate Is Nothing OrElse Me.StartDateTime Is Nothing OrElse Me.EndDateTime Is Nothing, "", _
                                               Me.ScheduleDate.Value.ToString("ddd dd") & " " & Me.StartDateTime.Value.ToString("HH:mm") & " - " & Me.EndDateTime.Value.ToString("HH:mm")))

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insShiftAuthHumanResourceShift"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updShiftAuthHumanResourceShift"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceShiftID As SqlParameter = .Parameters.Add("@HumanResourceShiftID", SqlDbType.Int)
          paramHumanResourceShiftID.Value = GetProperty(HumanResourceShiftIDProperty)
          If Me.IsNew Then
            paramHumanResourceShiftID.Direction = ParameterDirection.Output
          End If
          '.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          '.Parameters.AddWithValue("@SwappedHumanResourceShiftID", NothingDBNull(GetProperty(SwappedHumanResourceShiftIDProperty)))
          '.Parameters.AddWithValue("@SystemAreaShiftPatternWeekDayID", NothingDBNull(GetProperty(SystemAreaShiftPatternWeekDayIDProperty)))
          '.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          '.Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          '.Parameters.AddWithValue("@ShiftTypeID", GetProperty(ShiftTypeIDProperty))
          '.Parameters.AddWithValue("@ScheduleDate", (New SmartDate(GetProperty(ScheduleDateProperty))).DBValue)
          '.Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          '.Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", Security.Settings.CurrentUser.UserID)
          '.Parameters.AddWithValue("@ExtraShiftInd", GetProperty(ExtraShiftIndProperty))
          '.Parameters.AddWithValue("@SystemTeamID", GetProperty(SystemTeamIDProperty))
          .Parameters.AddWithValue("@StaffAcknowledgeInd", NothingDBNull(GetProperty(StaffAcknowledgeIndProperty)))
          .Parameters.AddWithValue("@StaffAcknowledgeBy", NothingDBNull(GetProperty(StaffAcknowledgeByProperty)))
          .Parameters.AddWithValue("@StaffAcknowledgeDateTime", (New SmartDate(GetProperty(StaffAcknowledgeDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@SupervisorAuthInd", NothingDBNull(GetProperty(SupervisorAuthIndProperty)))
          .Parameters.AddWithValue("@SupervisorAuthBy", NothingDBNull(GetProperty(SupervisorAuthByProperty)))
          .Parameters.AddWithValue("@SuperAuthDateTime", (New SmartDate(GetProperty(SuperAuthDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ManagerAuthInd", NothingDBNull(GetProperty(ManagerAuthIndProperty)))
          .Parameters.AddWithValue("@ManagerAuthBy", NothingDBNull(GetProperty(ManagerAuthByProperty)))
          .Parameters.AddWithValue("@ManagerAuthDateTime", (New SmartDate(GetProperty(ManagerAuthDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@SupervisorRejectedReason", GetProperty(SupervisorRejectedReasonProperty))
          .Parameters.AddWithValue("@ManagerRejectedReason", GetProperty(ManagerRejectedReasonProperty))
          .Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
          .Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
          .Parameters.AddWithValue("@MRMonthID", GetProperty(MRMonthIDProperty))
          .Parameters.AddWithValue("@StaffDisputeReason", GetProperty(StaffDisputeReasonProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceShiftIDProperty, paramHumanResourceShiftID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delShiftAuthHumanResourceShift"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceShiftID", GetProperty(HumanResourceShiftIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace