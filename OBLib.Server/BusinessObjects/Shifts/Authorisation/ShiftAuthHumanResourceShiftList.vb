﻿' Generated 14 Sep 2014 14:17 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Shifts.Auth

  <Serializable()> _
  Public Class ShiftAuthHumanResourceShiftList
    Inherits OBBusinessListBase(Of ShiftAuthHumanResourceShiftList, ShiftAuthHumanResourceShift)

#Region " Business Methods "

    Public Function GetItem(HumanResourceShiftID As Integer) As ShiftAuthHumanResourceShift

      For Each child As ShiftAuthHumanResourceShift In Me
        If child.HumanResourceShiftID = HumanResourceShiftID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

    Public Function GetItemByGuid(Guid As Guid) As ShiftAuthHumanResourceShift

      For Each child As ShiftAuthHumanResourceShift In Me
        If child.Guid = Guid Then
          Return child
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewShiftAuthHumanResourceShiftList() As ShiftAuthHumanResourceShiftList

      Return New ShiftAuthHumanResourceShiftList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ShiftAuthHumanResourceShift In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ShiftAuthHumanResourceShift In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

    Public Shared Function GetReimbursementValues(HRID As Integer, JSon As String, SyncID As Integer, SystemID As Integer) As Helpers.MRValues
      Dim mResults As New Helpers.MRValues

      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            If CompareSafe(SystemID, CType(OBLib.CommonData.Enums.System.PlayoutOperations, Integer)) Then
              cm.CommandText = "[GetProcsWeb].[GetCalcMealReimbursementValuesPlayoutOps]"
            ElseIf CompareSafe(SystemID, CType(OBLib.CommonData.Enums.System.ICR, Integer)) Then
              cm.CommandText = "[GetProcsWeb].[GetCalcICRMealReimbursementValues]"
            Else
              cm.CommandText = "[GetProcsWeb].[GetCalcMealReimbursementValuesGeneric]"
            End If
            cm.Parameters.AddWithValue("@HumanResourceID", HRID)
            cm.Parameters.AddWithValue("@JsonOverridingAuthorisations", JSon)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              If sdr.Read Then
                mResults.NoOfMeals = sdr.GetInt32(1)
                mResults.MRAmount = sdr.GetDecimal(2)
              End If
            End Using
          End Using
        Finally
          cn.Close()
        End Try

      End Using

      mResults.SyncID = SyncID
      Return mResults
    End Function

#End If

#End Region

#End Region

  End Class

End Namespace