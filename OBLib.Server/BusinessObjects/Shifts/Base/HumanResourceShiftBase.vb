﻿' Generated 08 Dec 2015 14:02 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.ShiftPatterns.ReadOnly
Imports OBLib.Maintenance.ShiftPatterns
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.HR.ReadOnly
Imports Singular.Misc
Imports OBLib.Biometrics

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Base

  <Serializable()> _
  Public MustInherit Class HumanResourceShiftBase(Of T As HumanResourceShiftBase(Of T))
    Inherits OBBusinessBase(Of T)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceShiftID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Browsable(True), Key>
    Public ReadOnly Property HumanResourceShiftID() As Integer
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="The human resource for which the pattern exists for"),
    Required(ErrorMessage:="Human Resource is required required"),
    DropDownWeb(GetType(ROHumanResourceAvailabilityList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="HumanResourceShiftBaseBO.setHumanResourceIDCriteriaBeforeRefresh",
                PreFindJSFunction:="HumanResourceShiftBaseBO.triggerHumanResourceIDAutoPopulate",
                AfterFetchJS:="HumanResourceShiftBaseBO.afterHumanResourceIDRefreshAjax",
                OnItemSelectJSFunction:="HumanResourceShiftBaseBO.onHumanResourceIDSelected",
                LookupMember:="HumanResource", ValueMember:="HumanResourceID", DisplayMember:="HRName", DropDownColumns:={"HRName"},
                DropDownCssClass:="room-dropdown", OnCellCreateFunction:="HumanResourceShiftBaseBO.onCellCreate"),
    SetExpression("HumanResourceShiftBaseBO.HumanResourceIDSet(self)")>
    Public Overridable Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared SwappedHumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SwappedHumanResourceShiftID, "Swapped Human Resource Shift", Nothing)
    ''' <summary>
    ''' Gets and sets the Swapped Human Resource Shift value
    ''' </summary>
    <Display(Name:="Swapped Human Resource Shift", Description:="link to the shift that has been swapped with")>
    Public Property SwappedHumanResourceShiftID() As Integer?
      Get
        Return GetProperty(SwappedHumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SwappedHumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared SystemAreaShiftPatternWeekDayIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemAreaShiftPatternWeekDayID, "System Area Shift Pattern Week Day", Nothing)
    ''' <summary>
    ''' Gets and sets the System Area Shift Pattern Week Day value
    ''' </summary>
    <Display(Name:="System Area Shift Pattern Week Day", Description:="A link to the system area shift pattern week day record")>
    Public Property SystemAreaShiftPatternWeekDayID() As Integer?
      Get
        Return GetProperty(SystemAreaShiftPatternWeekDayIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemAreaShiftPatternWeekDayIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="The sub-dept this record belongs to"),
    Required(ErrorMessage:="Sub-Dept required"), DropDownWeb(GetType(ROUserSystemList),
             DisplayMember:="System", ValueMember:="SystemID")>
    Public Overridable Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:="The area this record belongs to"),
    Required(ErrorMessage:="Area required"), DropDownWeb(GetType(ROUserSystemAreaList),
             ThisFilterMember:="SystemID", DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID")>
    Public Overridable Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ShiftTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ShiftTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Shift Type value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="the sequential number of shifts worked"),
    Required(ErrorMessage:="Shift Type required"),
    DropDownWeb(GetType(ROSystemAreaShiftTypeList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="HumanResourceShiftBaseBO.setShiftTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="HumanResourceShiftBaseBO.triggerShiftTypeIDAutoPopulate",
                AfterFetchJS:="HumanResourceShiftBaseBO.afterShiftTypeIDRefreshAjax",
                LookupMember:="ShiftType", ValueMember:="ShiftTypeID", DisplayMember:="ShiftType", DropDownColumns:={"ShiftType"},
                OnItemSelectJSFunction:="HumanResourceShiftBaseBO.onShiftTypeIDSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("HumanResourceShiftBaseBO.ShiftTypeIDSet(self)")>
    Public Overridable Property ShiftTypeID() As Integer?
      Get
        Return GetProperty(ShiftTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ShiftTypeIDProperty, Value)
      End Set
    End Property
    '.AddSetExpression("HumanResourceShiftBaseBO.ShiftTypeIDSet(self)")

    Public Shared ScheduleDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleDate, "Schedule Date")
    ' .AddSetExpression("PlayoutOperationsBookingBO.SetScheduleDate(self)")
    ''' <summary>
    ''' Gets and sets the Schedule Date value
    ''' </summary>
    <Display(Name:="Shift Date"),
    Required(ErrorMessage:="Shift Date required")>
    Public Overridable Property ScheduleDate As DateTime?
      Get
        Return GetProperty(ScheduleDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleDateProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Shift Start"),
    Required(ErrorMessage:="Shift Start required"),
    SetExpression("HumanResourceShiftBaseBO.ShiftStartDateSet(self)")>
    Public Overridable Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="Shift End"),
    Required(ErrorMessage:="Shift End required"),
    SetExpression("HumanResourceShiftBaseBO.ShiftEndDateSet(self)")>
    Public Overridable Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property
    '.AddSetExpression("HumanResourceShiftBaseBO.ShiftEndDateSet(self)")

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ExtraShiftIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ExtraShiftInd, "Is Extra Shift?", False)
    ''' <summary>
    ''' Gets and sets the Extra Shift value
    ''' </summary>
    <Display(Name:="Is Extra Shift?", Description:="Indicating if this is an extra shift"),
    Required(ErrorMessage:="Extra Shift required")>
    Public Property ExtraShiftInd() As Boolean
      Get
        Return GetProperty(ExtraShiftIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ExtraShiftIndProperty, Value)
      End Set
    End Property

    Public Shared SystemTeamIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemTeamID, "Team", Nothing)
    ''' <summary>
    ''' Gets and sets the Team value
    ''' </summary>
    <Display(Name:="Team", Description:="")>
    Public Overridable Property SystemTeamID() As Integer?
      Get
        Return GetProperty(SystemTeamIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemTeamIDProperty, Value)
      End Set
    End Property

    Public Shared SupervisorAuthIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.SupervisorAuthInd, "Super. Auth Status", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Supervisor Auth value
    ''' </summary>
    <Display(Name:="Super. Auth Status", Description:="True if shift is authorised by a supervisor"),
    SetExpression("HumanResourceShiftBaseBO.SupervisorAuthIndSet(self)")>
    Public Property SupervisorAuthInd() As Boolean?
      Get
        Return GetProperty(SupervisorAuthIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(SupervisorAuthIndProperty, Value)
      End Set
    End Property

    Public Shared SupervisorAuthByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupervisorAuthBy, "Supervisor Auth By", Nothing)
    ''' <summary>
    ''' Gets and sets the Supervisor Auth By value
    ''' </summary>
    <Display(Name:="Supervisor Auth By", Description:="")>
    Public Property SupervisorAuthBy() As Integer?
      Get
        Return GetProperty(SupervisorAuthByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupervisorAuthByProperty, Value)
      End Set
    End Property

    Public Shared SuperAuthDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.SuperAuthDateTime, "Sup Auth Time")
    ''' <summary>
    ''' Gets and sets the Super Auth Date Time value
    ''' </summary>
    <Display(Name:="Sup Auth Time", Description:=""),
     DateField(FormatString:="ddd dd MMM yy HH:mm")>
    Public Property SuperAuthDateTime As DateTime?
      Get
        Return GetProperty(SuperAuthDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(SuperAuthDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ManagerAuthIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.ManagerAuthInd, "Man. Auth Status", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Manager Auth value
    ''' </summary>
    <Display(Name:="Man. Auth Status", Description:="True if shift is authorised by a manager"),
    SetExpression("HumanResourceShiftBaseBO.ManagerAuthIndSet(self)")>
    Public Property ManagerAuthInd() As Boolean?
      Get
        Return GetProperty(ManagerAuthIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(ManagerAuthIndProperty, Value)
      End Set
    End Property

    Public Shared ManagerAuthByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ManagerAuthBy, "Manager Auth By", Nothing)
    ''' <summary>
    ''' Gets and sets the Manager Auth By value
    ''' </summary>
    <Display(Name:="Manager Auth By", Description:="")>
    Public Property ManagerAuthBy() As Integer?
      Get
        Return GetProperty(ManagerAuthByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ManagerAuthByProperty, Value)
      End Set
    End Property

    Public Shared ManagerAuthDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ManagerAuthDateTime, "Man Auth Time")
    ''' <summary>
    ''' Gets and sets the Manager Auth Date Time value
    ''' </summary>
    <Display(Name:="Man Auth Time", Description:=""),
     DateField(FormatString:="ddd dd MMM yy HH:mm")>
    Public Property ManagerAuthDateTime As DateTime?
      Get
        Return GetProperty(ManagerAuthDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ManagerAuthDateTimeProperty, Value)
      End Set
    End Property

    Public Shared StaffAcknowledgeIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.StaffAcknowledgeInd, "Staff Ack. Status", CType(Nothing, Boolean?))
    ''' <summary>
    ''' Gets and sets the Staff Acknowledge value
    ''' </summary>
    <Display(Name:="Staff Ack. Status", Description:=""),
    SetExpression("HumanResourceShiftBaseBO.StaffAcknowledgeIndSet(self)")>
    Public Property StaffAcknowledgeInd() As Boolean?
      Get
        Return GetProperty(StaffAcknowledgeIndProperty)
      End Get
      Set(ByVal Value As Boolean?)
        SetProperty(StaffAcknowledgeIndProperty, Value)
      End Set
    End Property

    Public Shared StaffAcknowledgeByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.StaffAcknowledgeBy, "Staff Acknowledge By", Nothing)
    ''' <summary>
    ''' Gets and sets the Staff Acknowledge By value
    ''' </summary>
    <Display(Name:="Staff Acknowledge By", Description:="")>
    Public Property StaffAcknowledgeBy() As Integer?
      Get
        Return GetProperty(StaffAcknowledgeByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(StaffAcknowledgeByProperty, Value)
      End Set
    End Property

    Public Shared StaffAcknowledgeDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StaffAcknowledgeDateTime, "Ack Time")
    ''' <summary>
    ''' Gets and sets the Staff Acknowledge Date Time value
    ''' </summary>
    <Display(Name:="Ack Time", Description:=""),
     DateField(FormatString:="ddd dd MMM yy HH:mm")>
    Public Property StaffAcknowledgeDateTime As DateTime?
      Get
        Return GetProperty(StaffAcknowledgeDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StaffAcknowledgeDateTimeProperty, Value)
      End Set
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets and sets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="Comments made for the shift"),
    StringLength(1024, ErrorMessage:="Comments cannot be more than 1024 characters")>
    Public Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CommentsProperty, Value)
      End Set
    End Property

    Public Shared SupervisorRejectedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupervisorRejectedReason, "Supervisor Rejected Reason", "")
    ''' <summary>
    ''' Gets and sets the Supervisor Rejected Reason value
    ''' </summary>
    <Display(Name:="Supervisor Rejected Reason", Description:="''"),
    StringLength(1024, ErrorMessage:="Supervisor Rejected Reason cannot be more than 1024 characters")>
    Public Property SupervisorRejectedReason() As String
      Get
        Return GetProperty(SupervisorRejectedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SupervisorRejectedReasonProperty, Value)
      End Set
    End Property

    Public Shared ManagerRejectedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerRejectedReason, "Manager Rejected Reason", "")
    ''' <summary>
    ''' Gets and sets the Manager Rejected Reason value
    ''' </summary>
    <Display(Name:="Manager Rejected Reason", Description:=""),
    StringLength(1024, ErrorMessage:="Manager Rejected Reason cannot be more than 1024 characters")>
    Public Property ManagerRejectedReason() As String
      Get
        Return GetProperty(ManagerRejectedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ManagerRejectedReasonProperty, Value)
      End Set
    End Property

    Public Shared StaffDisputeReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StaffDisputeReason, "Staff Dispute Reason", "")
    ''' <summary>
    ''' Gets and sets the Staff Dispute Reason value
    ''' </summary>
    <Display(Name:="Staff Dispute Reason", Description:=""),
    StringLength(1024, ErrorMessage:="Staff Dispute Reason cannot be more than 1024 characters")>
    Public Property StaffDisputeReason() As String
      Get
        Return GetProperty(StaffDisputeReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StaffDisputeReasonProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="Discpline the Human Resource worked as on the shift"),
    DropDownWeb(GetType(ROHumanResourceSkillDropDownList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="HumanResourceShiftBaseBO.setDisciplineIDCriteriaBeforeRefresh",
                PreFindJSFunction:="HumanResourceShiftBaseBO.triggerDisciplineIDAutoPopulate",
                AfterFetchJS:="HumanResourceShiftBaseBO.afterDisciplineIDRefreshAjax",
                LookupMember:="Discipline", ValueMember:="DisciplineID", DisplayMember:="Discipline", DropDownColumns:={"Discipline"},
                OnItemSelectJSFunction:="HumanResourceShiftBaseBO.onDisciplineIDSelected", DropDownCssClass:="room-dropdown"),
    Required(ErrorMessage:="Discipline is required")>
    Public Overridable Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared MealReimbursementIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.MealReimbursementInd, "Meal Reimb?", True)
    ''' <summary>
    ''' Gets and sets the Meal Reimbursement value
    ''' </summary>
    <Display(Name:="Meal Reimb?", Description:="True if the shift qaulifies for meal reimbursement calculation"),
    Required(ErrorMessage:="Meal Reimbursement required")>
    Public Property MealReimbursementInd() As Boolean
      Get
        Return GetProperty(MealReimbursementIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(MealReimbursementIndProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "Status", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Status"),
    DropDownWeb(GetType(ROSystemProductionAreaStatusSelectList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="HumanResourceShiftBaseBO.setProductionAreaStatusIDCriteriaBeforeRefresh",
                PreFindJSFunction:="HumanResourceShiftBaseBO.triggerProductionAreaStatusIDAutoPopulate",
                AfterFetchJS:="HumanResourceShiftBaseBO.afterProductionAreaStatusIDRefreshAjax",
                LookupMember:="ProductionAreaStatus", ValueMember:="ProductionAreaStatusID", DisplayMember:="ProductionAreaStatus", DropDownColumns:={"ProductionAreaStatus"},
                OnItemSelectJSFunction:="HumanResourceShiftBaseBO.onProductionAreaStatusIDSelected", DropDownCssClass:="room-dropdown"),
    Required(ErrorMessage:="Status is required")>
    Public Overridable Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Status", "")
    ''' <summary>
    ''' Gets and sets the Staff Dispute Reason value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaStatusProperty, Value)
      End Set
    End Property

    Public Shared BiometricChecksEnabledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.BiometricChecksEnabled, "Biometric Checks?", True)
    ''' <summary>
    ''' Gets and sets the Meal Reimbursement value
    ''' </summary>
    <Display(Name:="Biometric Checks?", Description:="True if the shift qaulifies for biometric checks")>
    Public Property BiometricChecksEnabled() As Boolean
      Get
        Return GetProperty(BiometricChecksEnabledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(BiometricChecksEnabledProperty, Value)
      End Set
    End Property

    Public Shared AccessFlagIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AccessFlagID, "Access Flag", Nothing)
    ''' <summary>
    ''' Gets he total of Biomentics Access logs
    ''' </summary>
    <Display(Name:="Access Flag", Description:="Biometrics Access Flag (Total)")>
    Public Property AccessFlagID() As Integer?
      Get
        Return GetProperty(AccessFlagIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AccessFlagIDProperty, Value)
      End Set
    End Property

    Public Shared AccessFlagDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AccessFlagDescription, "Biometric Flag", "")
    ''' <summary>
    ''' Gets and sets the Staff Dispute Reason value
    ''' </summary>
    <Display(Name:="Biometric Flag", Description:="")>
    Public Property AccessFlagDescription() As String
      Get
        Return GetProperty(AccessFlagDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AccessFlagDescriptionProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROAccessTerminalShiftListProperty As PropertyInfo(Of ROAccessTerminalShiftList) = RegisterProperty(Of ROAccessTerminalShiftList)(Function(c) c.ROAccessTerminalShiftList, "ROAccessTerminalShiftList")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ROAccessTerminalShiftList() As ROAccessTerminalShiftList
      Get
        If GetProperty(ROAccessTerminalShiftListProperty) Is Nothing Then
          LoadProperty(ROAccessTerminalShiftListProperty, New ROAccessTerminalShiftList)
        End If
        Return GetProperty(ROAccessTerminalShiftListProperty)
      End Get
    End Property

    Public Shared ROObjectAuditTrailListProperty As PropertyInfo(Of OBLib.Audit.ROObjectAuditTrailList) = RegisterProperty(Of OBLib.Audit.ROObjectAuditTrailList)(Function(c) c.ROObjectAuditTrailList, "ROObjectAuditTrailList")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ROObjectAuditTrailList() As OBLib.Audit.ROObjectAuditTrailList
      Get
        If GetProperty(ROObjectAuditTrailListProperty) Is Nothing Then
          LoadProperty(ROObjectAuditTrailListProperty, New OBLib.Audit.ROObjectAuditTrailList)
        End If
        Return GetProperty(ROObjectAuditTrailListProperty)
      End Get
    End Property

#End Region

#Region " Non-Database Properties "

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ResourceID, Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="ResourceID"), Required>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ResourceBookingID, Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="ResourceID")>
    Public Property ResourceBookingID() As Integer?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared StaffAcknowledgeByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StaffAcknowledgeByName, "StaffAcknowledgeByName", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="StaffAcknowledgeByName")>
    Public ReadOnly Property StaffAcknowledgeByName() As String
      Get
        Return GetProperty(StaffAcknowledgeByNameProperty)
      End Get
    End Property

    Public Shared SupervisorAuthByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupervisorAuthByName, "SupervisorAuthByName", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="SupervisorAuthByName")>
    Public ReadOnly Property SupervisorAuthByName() As String
      Get
        Return GetProperty(SupervisorAuthByNameProperty)
      End Get
    End Property

    Public Shared ManagerAuthByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerAuthByName, "ManagerAuthByName", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="ManagerAuthByName")>
    Public ReadOnly Property ManagerAuthByName() As String
      Get
        Return GetProperty(ManagerAuthByNameProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="Human Resource on the shift")>
    Public Overridable Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(value As String)
        SetProperty(HumanResourceProperty, value)
      End Set
    End Property

    Public Shared ShiftTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ShiftType, "Shift Type", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="Shift Type", Description:="Shift Type")>
    Public Property ShiftType() As String
      Get
        Return GetProperty(ShiftTypeProperty)
      End Get
      Set(value As String)
        SetProperty(ShiftTypeProperty, value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="Discipline", Description:="Discipline")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(value As String)
        SetProperty(DisciplineProperty, value)
      End Set
    End Property

    Public Shared TeamNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamName, "Primary Team", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="Primary Team", Description:="Team")>
    Public ReadOnly Property TeamName() As String
      Get
        Return GetProperty(TeamNameProperty)
      End Get
    End Property

    Public Shared ShowRoomScheduleListProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ShowRoomScheduleList, "Show Room Schedule List", False)
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="Show Room Schedule List")>
    Public Overridable Property ShowRoomScheduleList() As Boolean
      Get
        Return GetProperty(ShowRoomScheduleListProperty)
      End Get
      Set(value As Boolean)
        SetProperty(ShowRoomScheduleListProperty, value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceShiftIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Comments.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Shift Base")
        Else
          Return String.Format("Blank {0}", "Human Resource Shift Base")
        End If
      Else
        Return Me.Comments
      End If

    End Function

    Public Function GetClashes() As List(Of OBLib.Helpers.ResourceHelpers.ClashDetail)
      Dim rb As New OBLib.Helpers.ResourceHelpers.ResourceBooking(Me.Guid, Me.ResourceID, Me.ResourceBookingID,
                                                                  Nothing, Me.StartDateTime, Me.EndDateTime, Nothing,
                                                                  Nothing, Me.HumanResourceShiftID, Nothing,
                                                                  Nothing, Nothing, Nothing, Nothing)
      Dim rbl As New List(Of OBLib.Helpers.ResourceHelpers.ResourceBooking)
      rbl.Add(rb)
      OBLib.Helpers.ResourceHelpers.GetResourceClashes(rbl)
      Return rbl(0).ClashDetails
    End Function

#End Region

#Region " Overridables "

    <Browsable(False)>
    Public Overridable ReadOnly Property InsProcName As String
      Get
        Return "InsProcsWeb.insHumanResourceShiftBase"
      End Get
    End Property

    <Browsable(False)>
    Public Overridable ReadOnly Property UpdProcName As String
      Get
        Return "UpdProcsWeb.updHumanResourceShiftBase"
      End Get
    End Property

    <Browsable(False)>
    Public Overridable ReadOnly Property DelProcName As String
      Get
        Return "DelProcsWeb.delHumanResourceShiftBase"
      End Get
    End Property

    Public MustOverride Sub FetchExtraProperties(StartIndex As Integer, sdr As Csla.Data.SafeDataReader)
    Public MustOverride Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)
    Public MustOverride Sub UpdateChildList()
    Public MustOverride Sub DeleteChildren()
    Public Overridable Sub AfterInsertUpdateExecuted()
    End Sub
    Public Overridable Sub AddExtraDeleteParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(EndDateTimeProperty)
        .AffectedProperties.Add(StartDateTimeProperty)
        .AddTriggerProperty(StartDateTimeProperty)
        .JavascriptRuleFunctionName = "HumanResourceShiftBaseBO.StartBeforeEnd"
        .ServerRuleFunction = AddressOf StartBeforeEnd
      End With

      With AddWebRule(SupervisorRejectedReasonProperty)
        .AffectedProperties.Add(SupervisorAuthIndProperty)
        .AddTriggerProperty(SupervisorAuthIndProperty)
        .JavascriptRuleFunctionName = "HumanResourceShiftBaseBO.SupervisorAuthIndValid"
        .ServerRuleFunction = AddressOf SupervisorAuthIndValid
      End With

      With AddWebRule(ManagerRejectedReasonProperty)
        .AffectedProperties.Add(ManagerAuthIndProperty)
        .AddTriggerProperty(ManagerAuthIndProperty)
        .JavascriptRuleFunctionName = "HumanResourceShiftBaseBO.ManagerAuthIndValid"
        .ServerRuleFunction = AddressOf ManagerAuthIndValid
      End With

      With AddWebRule(StaffDisputeReasonProperty)
        .AffectedProperties.Add(StaffAcknowledgeIndProperty)
        .AddTriggerProperty(StaffAcknowledgeIndProperty)
        .JavascriptRuleFunctionName = "HumanResourceShiftBaseBO.StaffAckIndValid"
        .ServerRuleFunction = AddressOf StaffAckIndValid
      End With

      'With AddWebRule(SupervisorRejectedReasonProperty)
      '  .JavascriptRuleFunctionName = "CheckSupervisorRejectedReason"
      '  .ServerRuleFunction = Function(ts)
      '                          If CompareSafe(ts.SystemID, CType(OBLib.CommonData.Enums.System.ICR, Integer)) Then
      '                            If ts.SupervisorAuthInd = False AndAlso Singular.Misc.IsNullNothingOrEmpty(ts.SupervisorRejectedReason) Then
      '                              Return "Rejected Reason is required"
      '                            End If
      '                            If ts.SupervisorAuthInd AndAlso Singular.Misc.IsNullNothingOrEmpty(ts.SupervisorRejectedReason) AndAlso ts.AccessFlagID > 0 Then
      '                              Return "Authorising a flagged Shift, Reason is required"
      '                            End If
      '                          End If
      '                          Return ""
      '                        End Function
      '  .AffectedProperties.Add(SupervisorAuthIndProperty)
      '  .AddTriggerProperty(SupervisorAuthIndProperty)
      'End With

      'With AddWebRule(ManagerRejectedReasonProperty)
      '  .JavascriptRuleFunctionName = "CheckManagerRejectedReason"
      '  .ServerRuleFunction = Function(ts)
      '                          If ts.ManagerAuthInd = False AndAlso Singular.Misc.IsNullNothingOrEmpty(ts.ManagerRejectedReason) Then
      '                            Return "Rejected Reason is required"
      '                          End If
      '                          Return ""
      '                          If ts.ManagerAuthInd = True AndAlso Singular.Misc.IsNullNothingOrEmpty(ts.ManagerRejectedReason) AndAlso ts.AccessFlagID > 0 Then
      '                            Return "Authorising a flagged Shift, Reason is required"
      '                          End If
      '                          Return ""
      '                        End Function
      '  .AffectedProperties.Add(ManagerAuthIndProperty)
      '  .AddTriggerProperty(ManagerAuthIndProperty)
      'End With

    End Sub

    Public Function StartBeforeEnd(HRSB As HumanResourceShiftBase(Of T)) As String
      Dim ErrString = ""
      If HRSB.EndDateTime IsNot Nothing AndAlso HRSB.StartDateTime IsNot Nothing Then
        If HRSB.EndDateTime < HRSB.StartDateTime Then
          ErrString = "Start date must be before end date"
        End If
      End If
      Return ErrString
    End Function

    Public Function SupervisorAuthIndValid(HRSB As HumanResourceShiftBase(Of T)) As String
      Dim ErrString = ""
      If HRSB.SupervisorAuthInd IsNot Nothing AndAlso HRSB.SupervisorAuthInd = False AndAlso HRSB.SupervisorRejectedReason.Trim = "" Then
        ErrString = "Sup Reject Reason is required"
      End If
      If HRSB.BiometricChecksEnabled Then
        If HRSB.SupervisorAuthInd AndAlso HRSB.AccessFlagID IsNot Nothing AndAlso HRSB.AccessFlagID > 0 AndAlso HRSB.SupervisorRejectedReason.Trim = "" Then
          ErrString = "Authorising a flagged Shift, Reason is required"
        End If
      End If
      Return ErrString
    End Function

    Public Function ManagerAuthIndValid(HRSB As HumanResourceShiftBase(Of T)) As String
      Dim ErrString = ""
      If HRSB.ManagerAuthInd IsNot Nothing AndAlso HRSB.ManagerAuthInd = False AndAlso HRSB.ManagerRejectedReason.Trim = "" Then
        ErrString = "Man Reject Reason is required"
      End If
      If HRSB.BiometricChecksEnabled Then
        If HRSB.ManagerAuthInd AndAlso HRSB.AccessFlagID IsNot Nothing AndAlso HRSB.AccessFlagID > 0 AndAlso HRSB.ManagerRejectedReason.Trim = "" Then
          ErrString = "Authorising a flagged Shift, Reason is required"
        End If
      End If
      Return ErrString
    End Function

    Public Function StaffAckIndValid(HRSB As HumanResourceShiftBase(Of T)) As String
      Dim ErrString = ""
      If HRSB.StaffAcknowledgeInd IsNot Nothing AndAlso HRSB.StaffAcknowledgeInd = False AndAlso HRSB.StaffDisputeReason.Trim = "" Then
        ErrString = "Dispute Reason is required"
      End If
      Return ErrString
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceShiftBase() method.

    End Sub

    Public Shared Function NewHumanResourceShiftBase() As HumanResourceShiftBase(Of T)

      Return DataPortal.CreateChild(Of HumanResourceShiftBase(Of T))()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceShiftIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SwappedHumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(SystemAreaShiftPatternWeekDayIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(ShiftTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(ScheduleDateProperty, .GetValue(7))
          LoadProperty(StartDateTimeProperty, .GetValue(8))
          LoadProperty(EndDateTimeProperty, .GetValue(9))
          LoadProperty(CreatedByProperty, .GetInt32(10))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(11))
          LoadProperty(ModifiedByProperty, .GetInt32(12))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(13))
          LoadProperty(ExtraShiftIndProperty, .GetBoolean(14))
          LoadProperty(SystemTeamIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          Dim tmpSupervisorAuthInd = .GetValue(16)
          If IsDBNull(tmpSupervisorAuthInd) Then
            LoadProperty(SupervisorAuthIndProperty, Nothing)
          Else
            LoadProperty(SupervisorAuthIndProperty, tmpSupervisorAuthInd)
          End If
          LoadProperty(SupervisorAuthByProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(SuperAuthDateTimeProperty, .GetValue(18))
          Dim tmpManagerAuthInd = .GetValue(19)
          If IsDBNull(tmpManagerAuthInd) Then
            LoadProperty(ManagerAuthIndProperty, Nothing)
          Else
            LoadProperty(ManagerAuthIndProperty, tmpManagerAuthInd)
          End If
          LoadProperty(ManagerAuthByProperty, Singular.Misc.ZeroNothing(.GetInt32(20)))
          LoadProperty(ManagerAuthDateTimeProperty, .GetValue(21))
          Dim tmpStaffAcknowledgeInd = .GetValue(22)
          If IsDBNull(tmpStaffAcknowledgeInd) Then
            LoadProperty(StaffAcknowledgeIndProperty, Nothing)
          Else
            LoadProperty(StaffAcknowledgeIndProperty, tmpStaffAcknowledgeInd)
          End If
          LoadProperty(StaffAcknowledgeByProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
          LoadProperty(StaffAcknowledgeDateTimeProperty, .GetValue(24))
          LoadProperty(CommentsProperty, .GetString(25))
          LoadProperty(SupervisorRejectedReasonProperty, .GetString(26))
          LoadProperty(ManagerRejectedReasonProperty, .GetString(27))
          LoadProperty(StaffDisputeReasonProperty, .GetString(28))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(29)))
          LoadProperty(MealReimbursementIndProperty, .GetBoolean(30))
          LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(31)))
          LoadProperty(DisciplineProperty, .GetString(32))
          LoadProperty(HumanResourceProperty, .GetString(33))
          LoadProperty(TeamNameProperty, .GetString(34))
          LoadProperty(StaffAcknowledgeByNameProperty, .GetString(35))
          LoadProperty(SupervisorAuthByNameProperty, .GetString(36))
          LoadProperty(ManagerAuthByNameProperty, .GetString(37))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(38)))
          LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(39)))
          LoadProperty(ShiftTypeProperty, .GetString(40))
          LoadProperty(ProductionAreaStatusProperty, .GetString(41))
          LoadProperty(BiometricChecksEnabledProperty, .GetBoolean(42))
          LoadProperty(AccessFlagIDProperty, Singular.Misc.ZeroNothing(.GetInt32(43)))
          LoadProperty(AccessFlagDescriptionProperty, .GetString(44))
        End With
      End Using
      FetchExtraProperties(45, sdr)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = InsProcName
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = UpdProcName
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramHumanResourceShiftID As SqlParameter = .Parameters.Add("@HumanResourceShiftID", SqlDbType.Int)
          paramHumanResourceShiftID.Value = GetProperty(HumanResourceShiftIDProperty)

          Dim paramResourceBookingID = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)

          If Me.IsNew Then
            paramHumanResourceShiftID.Direction = ParameterDirection.Output
            paramResourceBookingID.Direction = ParameterDirection.Output
          End If

          .Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
          .Parameters.AddWithValue("@SwappedHumanResourceShiftID", Singular.Misc.NothingDBNull(GetProperty(SwappedHumanResourceShiftIDProperty)))
          .Parameters.AddWithValue("@SystemAreaShiftPatternWeekDayID", Singular.Misc.NothingDBNull(GetProperty(SystemAreaShiftPatternWeekDayIDProperty)))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@ShiftTypeID", GetProperty(ShiftTypeIDProperty))
          .Parameters.AddWithValue("@ScheduleDate", (New SmartDate(GetProperty(ScheduleDateProperty))).DBValue)
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
          .Parameters.AddWithValue("@ExtraShiftInd", GetProperty(ExtraShiftIndProperty))
          .Parameters.AddWithValue("@SystemTeamID", Singular.Misc.NothingDBNull(GetProperty(SystemTeamIDProperty)))
          .Parameters.AddWithValue("@SupervisorAuthInd", Singular.Misc.NothingDBNull(GetProperty(SupervisorAuthIndProperty)))
          .Parameters.AddWithValue("@SupervisorAuthBy", Singular.Misc.NothingDBNull(GetProperty(SupervisorAuthByProperty)))
          .Parameters.AddWithValue("@SuperAuthDateTime", (New SmartDate(GetProperty(SuperAuthDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ManagerAuthInd", Singular.Misc.NothingDBNull(GetProperty(ManagerAuthIndProperty)))
          .Parameters.AddWithValue("@ManagerAuthBy", Singular.Misc.NothingDBNull(GetProperty(ManagerAuthByProperty)))
          .Parameters.AddWithValue("@ManagerAuthDateTime", (New SmartDate(GetProperty(ManagerAuthDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@StaffAcknowledgeInd", Singular.Misc.NothingDBNull(GetProperty(StaffAcknowledgeIndProperty)))
          .Parameters.AddWithValue("@StaffAcknowledgeBy", Singular.Misc.NothingDBNull(GetProperty(StaffAcknowledgeByProperty)))
          .Parameters.AddWithValue("@StaffAcknowledgeDateTime", (New SmartDate(GetProperty(StaffAcknowledgeDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@Comments", GetProperty(CommentsProperty))
          .Parameters.AddWithValue("@SupervisorRejectedReason", GetProperty(SupervisorRejectedReasonProperty))
          .Parameters.AddWithValue("@ManagerRejectedReason", GetProperty(ManagerRejectedReasonProperty))
          .Parameters.AddWithValue("@StaffDisputeReason", GetProperty(StaffDisputeReasonProperty))
          .Parameters.AddWithValue("@DisciplineID", Singular.Misc.NothingDBNull(GetProperty(DisciplineIDProperty)))
          .Parameters.AddWithValue("@MealReimbursementInd", GetProperty(MealReimbursementIndProperty))
          .Parameters.AddWithValue("@ProductionAreaStatusID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaStatusIDProperty)))
          .Parameters.AddWithValue("@ResourceID", Singular.Misc.NothingDBNull(GetProperty(ResourceIDProperty)))
          AddExtraParameters(.Parameters)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(HumanResourceShiftIDProperty, paramHumanResourceShiftID.Value)
            LoadProperty(ResourceBookingIDProperty, paramResourceBookingID.Value)
          End If
          AfterInsertUpdateExecuted()
          ' update child objects
          UpdateChildList()
          MarkOld()
        End With
      Else
        UpdateChildList()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = DelProcName
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@HumanResourceShiftID", GetProperty(HumanResourceShiftIDProperty))
        cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
        AddExtraDeleteParameters(cm.Parameters)
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
