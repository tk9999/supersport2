﻿' Generated 08 Dec 2015 14:02 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Base

  <Serializable()> _
  Public MustInherit Class HumanResourceShiftBaseList(Of T As HumanResourceShiftBaseList(Of T, C), C As HumanResourceShiftBase(Of C))
    Inherits OBBusinessListBase(Of T, C)

#Region " Business Methods "

    Public Function GetItem(HumanResourceShiftID As Integer) As HumanResourceShiftBase(Of C)

      For Each child As HumanResourceShiftBase(Of C) In Me
        If child.HumanResourceShiftID = HumanResourceShiftID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Shifts"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As HumanResourceShiftBase(Of C) In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As HumanResourceShiftBase(Of C) In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#Region " Based Methods "

    Public MustOverride Sub Fetch(sdr As SafeDataReader)
    Public MustOverride Sub FetchChildLists(sdr As Csla.Data.SafeDataReader)

#End Region

#End Region

  End Class

End Namespace
