﻿' Generated 14 Jun 2016 22:36 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Shifts

  <Serializable()> _
  Public Class HumanResourceShiftRoomSchedule
    Inherits OBBusinessBase(Of HumanResourceShiftRoomSchedule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return HumanResourceShiftRoomScheduleBO.HumanResourceShiftRoomScheduleBOToString(self)")

    Public Shared HumanResourceShiftRoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceShiftRoomScheduleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property HumanResourceShiftRoomScheduleID() As Integer
      Get
        Return GetProperty(HumanResourceShiftRoomScheduleIDProperty)
      End Get
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Human Resource Shift", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:=""),
    Required(ErrorMessage:="Human Resource Shift required")>
    Public Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "Room Schedule", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:=""),
    Required(ErrorMessage:="Room Schedule required")>
    Public Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared RoomScheduleTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomScheduleTitle, "Room Schedule", "")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public Property RoomScheduleTitle() As String
      Get
        Return GetProperty(RoomScheduleTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomScheduleTitleProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room", "")
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared RoomStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.RoomStartDateTime)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time"), TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Overridable Property RoomStartDateTime As DateTime?
      Get
        Return GetProperty(RoomStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(RoomStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared RoomEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.RoomEndDateTime)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time"), TimeField(TimeFormat:=TimeFormats.ShortTime)>
    Public Overridable Property RoomEndDateTime As DateTime?
      Get
        Return GetProperty(RoomEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(RoomEndDateTimeProperty, Value)
      End Set
    End Property

    <Display(Name:="Start Time")>
    Public ReadOnly Property StartTimeString As String
      Get
        Return Me.RoomStartDateTime.Value.ToString("HH:mm")
      End Get
    End Property

    <Display(Name:="End Time")>
    Public ReadOnly Property EndTimeString As String
      Get
        Return Me.RoomEndDateTime.Value.ToString("HH:mm")
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceShiftRoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Human Resource Shift Room Schedule")
        Else
          Return String.Format("Blank {0}", "Human Resource Shift Room Schedule")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

    'Public Function GetParent() As HumanResourceShiftRoomSchedule
    '  Return CType(CType(Me.Parent, HumanResourceShiftRoomScheduleList), HumanResourceShiftBase)
    'End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewHumanResourceShiftRoomSchedule() method.

    End Sub

    Public Shared Function NewHumanResourceShiftRoomSchedule() As HumanResourceShiftRoomSchedule

      Return DataPortal.CreateChild(Of HumanResourceShiftRoomSchedule)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetHumanResourceShiftRoomSchedule(dr As SafeDataReader) As HumanResourceShiftRoomSchedule

      Dim h As New HumanResourceShiftRoomSchedule()
      h.Fetch(dr)
      Return h

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(HumanResourceShiftRoomScheduleIDProperty, .GetInt32(0))
          LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(RoomScheduleTitleProperty, .GetString(7))
          LoadProperty(RoomProperty, .GetString(8))
          LoadProperty(RoomStartDateTimeProperty, .GetValue(9))
          LoadProperty(RoomEndDateTimeProperty, .GetValue(10))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, HumanResourceShiftRoomScheduleIDProperty)

      cm.Parameters.AddWithValue("@HumanResourceShiftID", GetProperty(HumanResourceShiftIDProperty))
      cm.Parameters.AddWithValue("@RoomScheduleID", GetProperty(RoomScheduleIDProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(HumanResourceShiftRoomScheduleIDProperty, cm.Parameters("@HumanResourceShiftRoomScheduleID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@HumanResourceShiftRoomScheduleID", GetProperty(HumanResourceShiftRoomScheduleIDProperty))
    End Sub

#End Region

  End Class

End Namespace