﻿' Generated 14 Jun 2016 22:36 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Shifts

  <Serializable()> _
  Public Class HumanResourceShiftRoomScheduleList
    Inherits OBBusinessListBase(Of HumanResourceShiftRoomScheduleList, HumanResourceShiftRoomSchedule)

#Region " Business Methods "

    Public Function GetItem(HumanResourceShiftRoomScheduleID As Integer) As HumanResourceShiftRoomSchedule

      For Each child As HumanResourceShiftRoomSchedule In Me
        If child.HumanResourceShiftRoomScheduleID = HumanResourceShiftRoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Human Resource Shift Room Schedules"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewHumanResourceShiftRoomScheduleList() As HumanResourceShiftRoomScheduleList

      Return New HumanResourceShiftRoomScheduleList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetHumanResourceShiftRoomScheduleList() As HumanResourceShiftRoomScheduleList

      Return DataPortal.Fetch(Of HumanResourceShiftRoomScheduleList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(HumanResourceShiftRoomSchedule.GetHumanResourceShiftRoomSchedule(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceShiftRoomScheduleList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace