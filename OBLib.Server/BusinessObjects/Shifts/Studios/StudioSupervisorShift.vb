﻿' Generated 08 Dec 2015 15:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.SystemManagement.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports OBLib.HR.ReadOnly

Namespace Shifts.Studios

  <Serializable>
  Public Class StudioSupervisorShift
    Inherits Base.HumanResourceShiftBase(Of StudioSupervisorShift)

#Region " Properties "

    <Display(Name:="Status"),
    DropDownWeb(GetType(ROSystemProductionAreaStatusSelectList), DisplayMember:="ProductionAreaStatus", ValueMember:="ProductionAreaStatusID",
                FilterMethodName:="HumanResourceShiftBaseBO.FilterStatusList")>
    Public Overrides Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared SupervisorIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SupervisorID, Nothing) _
                                                                  .AddSetExpression("StudioSupervisorShiftBO.SupervisorIDSet(self)", False)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Supervisor"),
    Required(ErrorMessage:="Supervisor required"),
    DropDownWeb(GetType(ROStudioSupervisorList), DisplayMember:="HRName", ValueMember:="HumanResourceID")>
    Public Property SupervisorID() As Integer?
      Get
        Return GetProperty(SupervisorIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupervisorIDProperty, Value)
      End Set
    End Property

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    <Display(Name:="Expanded")>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    <Display(Name:="StaffAcknowledgeDetails")>
    Public ReadOnly Property StaffAcknowledgeDetails() As String
      Get
        If StaffAcknowledgeInd Is Nothing Then
          Return ""
        Else
          If StaffAcknowledgeDateTime IsNot Nothing Then
            Return StaffAcknowledgeByName & " " & StaffAcknowledgeDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          End If
        End If
        Return ""
      End Get
    End Property

    Public Shared SupervisorAuthDetailsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupervisorAuthDetails, "SupervisorAuthDetails", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="SupervisorAuthDetails")>
    Public ReadOnly Property SupervisorAuthDetails() As String
      Get
        If SupervisorAuthInd Is Nothing Then
          Return ""
        Else
          If SuperAuthDateTime IsNot Nothing Then
            Return SupervisorAuthByName & " " & SuperAuthDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          End If
        End If
        Return ""
      End Get
    End Property

    Public Shared ManagerAuthDetailsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerAuthDetails, "ManagerAuthDetails", "")
    ''' <summary>
    ''' Gets and sets the Team Name value
    ''' </summary>
    <Display(Name:="ManagerAuthDetails")>
    Public ReadOnly Property ManagerAuthDetails() As String
      Get
        If ManagerAuthInd Is Nothing Then
          Return ""
        Else
          If ManagerAuthDateTime IsNot Nothing Then
            Return ManagerAuthByName & " " & ManagerAuthDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          End If
        End If
        Return ""
      End Get
    End Property

    Friend Shared Function GetStudioSupervisorShift(dr As SafeDataReader) As StudioSupervisorShift

      Dim r As New StudioSupervisorShift()
      r.Fetch(dr)
      Return r

    End Function

#End Region

#Region " Child Lists "

    Public Shared ClashListProperty As PropertyInfo(Of List(Of Helpers.ResourceHelpers.ClashDetail)) = RegisterProperty(Of List(Of Helpers.ResourceHelpers.ClashDetail))(Function(c) c.ClashList, "Clash List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ClashList() As List(Of Helpers.ResourceHelpers.ClashDetail)
      Get
        If GetProperty(ClashListProperty) Is Nothing Then
          LoadProperty(ClashListProperty, New List(Of Helpers.ResourceHelpers.ClashDetail))
        End If
        Return GetProperty(ClashListProperty)
      End Get
    End Property

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(StartDateTimeProperty)
        .AddTriggerProperty(EndDateTimeProperty)
        .AddTriggerProperty(HumanResourceIDProperty)
        .AffectedProperties.Add(SupervisorIDProperty)
        .AffectedProperties.Add(EndDateTimeProperty)
        .JavascriptRuleFunctionName = "StudioSupervisorShiftBO.ShiftValid"
        .ServerRuleFunction = AddressOf CheckClashes
      End With

    End Sub

    Public Shared Function CheckClashes(PLOS As StudioSupervisorShift) As String
      Dim ErrorString = ""
      Return ErrorString
    End Function

#End Region

#Region " Overrides "

    <Browsable(False)>
    Public Overrides ReadOnly Property DelProcName As String
      Get
        Return "DelProcsWeb.delHumanResourceShiftStudioSupervisor"
      End Get
    End Property

    <Browsable(False)>
    Public Overrides ReadOnly Property InsProcName As String
      Get
        Return "InsProcsWeb.insHumanResourceShiftStudioSupervisor"
      End Get
    End Property

    <Browsable(False)>
    Public Overrides ReadOnly Property UpdProcName As String
      Get
        Return "UpdProcsWeb.updHumanResourceShiftStudioSupervisor"
      End Get
    End Property

#End Region

    <Browsable(False), NonSerialized>
    Private mparamResourceBookingID As SqlParameter

    Public Overrides Sub AddExtraParameters(Parameters As SqlClient.SqlParameterCollection)
      'Parameters.AddWithValue("@ResourceID", Singular.Misc.NothingDBNull(GetProperty(ResourceIDProperty)))
      'mparamResourceBookingID = Parameters.Add("@ResourceBookingID", SqlDbType.Int)
      'mparamResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
      'If Me.IsNew Then
      '  mparamResourceBookingID.Direction = ParameterDirection.Output
      'End If
    End Sub

    Public Overrides Sub AfterInsertUpdateExecuted()
      'If Me.IsNew Then
      '  LoadProperty(ResourceBookingIDProperty, mparamResourceBookingID.Value)
      'End If
    End Sub

    Public Overrides Sub FetchExtraProperties(StartIndex As Integer, sdr As Csla.Data.SafeDataReader)
      With sdr

      End With
      LoadProperty(SupervisorIDProperty, HumanResourceID)
    End Sub

    Public Overrides Sub AddExtraDeleteParameters(Parameters As SqlParameterCollection)
      Parameters.AddWithValue("@ResourceBookingID", Singular.Misc.NothingDBNull(GetProperty(ResourceBookingIDProperty)))
    End Sub

    Public Overrides Sub UpdateChildList()

    End Sub

    Public Overrides Sub DeleteChildren()

    End Sub

  End Class

End Namespace
