﻿' Generated 08 Dec 2015 15:37 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Shifts.Studios

  <Serializable>
  Public Class StudioSupervisorShiftList
    Inherits Base.HumanResourceShiftBaseList(Of StudioSupervisorShiftList, StudioSupervisorShift)

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      'Public Property HumanResourceID As Integer?
      'Public Property StartDate As DateTime? = Nothing
      'Public Property EndDate As DateTime? = Nothing
      'Public Property SystemID As Integer? = Nothing
      'Public Property ProductionAreaID As Integer? = Nothing
      Public Property HumanResourceShiftID As Integer?
      Public Property HumanResourceShiftIDs As String = ""

      Public Sub New(HumanResourceShiftID As Integer?)
        'StartDate As DateTime?, EndDate As DateTime?, SystemID As Integer?, ProductionAreaID As Integer?,

        'Me.HumanResourceID = HumanResourceID
        'Me.StartDate = StartDate
        'Me.EndDate = EndDate
        'Me.SystemID = SystemID
        'Me.ProductionAreaID = ProductionAreaID
        Me.HUmanResourceShiftID = HumanResourceShiftID

      End Sub

      Public Sub New(HumanResourceShiftIDs As String)
        Me.HumanResourceShiftIDs = HumanResourceShiftIDs
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewStudioSupervisorShiftList() As StudioSupervisorShiftList

      Return New StudioSupervisorShiftList()

    End Function

    Public Shared Sub BeginGetStudioSupervisorShiftList(CallBack As EventHandler(Of DataPortalResult(Of StudioSupervisorShiftList)))

      Dim dp As New DataPortal(Of StudioSupervisorShiftList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetStudioSupervisorShiftList() As StudioSupervisorShiftList

      Return DataPortal.Fetch(Of StudioSupervisorShiftList)(New Criteria())

    End Function

    Public Shared Function GetStudioSupervisorShiftList(HumanResourceShiftID As Integer?) As StudioSupervisorShiftList

      'StartDate As DateTime?, EndDate As DateTime?,
      'SystemID As Integer?, ProductionAreaID As Integer?,
      'StartDate, EndDate, SystemID, ProductionAreaID,
      Return DataPortal.Fetch(Of StudioSupervisorShiftList)(New Criteria(HumanResourceShiftID))

    End Function

    Public Shared Function GetStudioSupervisorShiftList(ShiftIDsXml As String) As StudioSupervisorShiftList

      Return DataPortal.Fetch(Of StudioSupervisorShiftList)(New Criteria(ShiftIDsXml))

    End Function

    Public Overrides Sub FetchChildLists(sdr As Csla.Data.SafeDataReader)

    End Sub

    Public Overrides Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(StudioSupervisorShift.GetStudioSupervisorShift(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'Dim parent As StudioSupervisorShift = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.HumanResourceShiftID <> sdr.GetInt32(15) Then
      '      parent = Me.GetItem(sdr.GetInt32(15))
      '    End If
      '    parent.ResourceBookingList.RaiseListChangedEvents = False
      '    parent.ResourceBookingList.Add(StudioSupervisorBooking.GetStudioSupervisorBooking(sdr))
      '    parent.ResourceBookingList.RaiseListChangedEvents = True
      '  End While
      'End If

      For Each PLO As StudioSupervisorShift In Me
        PLO.CheckRules()
        'For Each child As StudioSupervisorBooking In PLO.ResourceBookingList
        '  child.CheckRules()
        'Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getHumanResourceShiftListStudioSupervisor"
            'cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
            'cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            'cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            'cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            'cm.Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.NothingDBNull(crit.HumanResourceShiftID))
            cm.Parameters.AddWithValue("@HumanResourceShiftIDs", Singular.Strings.MakeEmptyDBNull(crit.HumanResourceShiftIDs))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
