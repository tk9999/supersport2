﻿' Generated 17 Mar 2016 10:19 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Reports

  <Serializable()> _
  Public Class ROProductionHumanResourceHistory
    Inherits OBReadOnlyBase(Of ROProductionHumanResourceHistory)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionHumanResourceHistoryIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceHistoryID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionHumanResourceHistoryID() As Integer
      Get
        Return GetProperty(ProductionHumanResourceHistoryIDProperty)
      End Get
    End Property

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceID, "Production Human Resource", 0)
    ''' <summary>
    ''' Gets the Production Human Resource value
    ''' </summary>
    <Display(Name:="Production Human Resource", Description:="")>
    Public ReadOnly Property ProductionHumanResourceID() As Integer
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production", 0)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline", 0)
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionID, "Position", 0)
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property PositionID() As Integer
      Get
        Return GetProperty(PositionIDProperty)
      End Get
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionTypeID, "Position Type", 0)
    ''' <summary>
    ''' Gets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
    Public ReadOnly Property PositionTypeID() As Integer
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
    End Property

    Public Shared FilteredProductionTypeIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FilteredProductionTypeInd, "Filtered Production Type", False)
    ''' <summary>
    ''' Gets the Filtered Production Type value
    ''' </summary>
    <Display(Name:="Filtered Production Type", Description:="")>
    Public ReadOnly Property FilteredProductionTypeInd() As Boolean
      Get
        Return GetProperty(FilteredProductionTypeIndProperty)
      End Get
    End Property

    Public Shared FilteredPositionIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FilteredPositionInd, "Filtered Position", False)
    ''' <summary>
    ''' Gets the Filtered Position value
    ''' </summary>
    <Display(Name:="Filtered Position", Description:="")>
    Public ReadOnly Property FilteredPositionInd() As Boolean
      Get
        Return GetProperty(FilteredPositionIndProperty)
      End Get
    End Property

    Public Shared IncludedOffIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IncludedOffInd, "Included Off", False)
    ''' <summary>
    ''' Gets the Included Off value
    ''' </summary>
    <Display(Name:="Included Off", Description:="")>
    Public ReadOnly Property IncludedOffInd() As Boolean
      Get
        Return GetProperty(IncludedOffIndProperty)
      End Get
    End Property

    Public Shared PreferredHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PreferredHumanResourceID, "Preferred Human Resource", 0)
    ''' <summary>
    ''' Gets the Preferred Human Resource value
    ''' </summary>
    <Display(Name:="Preferred Human Resource", Description:="")>
    Public ReadOnly Property PreferredHumanResourceID() As Integer
      Get
        Return GetProperty(PreferredHumanResourceIDProperty)
      End Get
    End Property

    Public Shared SuppliedHumanResourceIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SuppliedHumanResourceInd, "Supplied Human Resource", False)
    ''' <summary>
    ''' Gets the Supplied Human Resource value
    ''' </summary>
    <Display(Name:="Supplied Human Resource", Description:="")>
    Public ReadOnly Property SuppliedHumanResourceInd() As Boolean
      Get
        Return GetProperty(SuppliedHumanResourceIndProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource", 0)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SelectionReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectionReason, "Selection Reason", "")
    ''' <summary>
    ''' Gets the Selection Reason value
    ''' </summary>
    <Display(Name:="Selection Reason", Description:="")>
    Public ReadOnly Property SelectionReason() As String
      Get
        Return GetProperty(SelectionReasonProperty)
      End Get
    End Property

    Public Shared CancelledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDate, "Cancelled Date", New SmartDate())
    ''' <summary>
    ''' Gets the Cancelled Date value
    ''' </summary>
    <Display(Name:="Cancelled Date", Description:="")>
    Public ReadOnly Property CancelledDate As DateTime?
      Get
        Return GetProperty(CancelledDateProperty)
      End Get
    End Property

    Public Shared CancelledReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CancelledReason, "Cancelled Reason", "")
    ''' <summary>
    ''' Gets the Cancelled Reason value
    ''' </summary>
    <Display(Name:="Cancelled Reason", Description:="")>
    Public ReadOnly Property CancelledReason() As String
      Get
        Return GetProperty(CancelledReasonProperty)
      End Get
    End Property

    Public Shared CrewLeaderIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CrewLeaderInd, "Crew Leader", False)
    ''' <summary>
    ''' Gets the Crew Leader value
    ''' </summary>
    <Display(Name:="Crew Leader", Description:="")>
    Public ReadOnly Property CrewLeaderInd() As Boolean
      Get
        Return GetProperty(CrewLeaderIndProperty)
      End Get
    End Property

    Public Shared TrainingIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TrainingInd, "Training", False)
    ''' <summary>
    ''' Gets the Training value
    ''' </summary>
    <Display(Name:="Training", Description:="")>
    Public ReadOnly Property TrainingInd() As Boolean
      Get
        Return GetProperty(TrainingIndProperty)
      End Get
    End Property

    Public Shared TBCIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TBCInd, "TBC", False)
    ''' <summary>
    ''' Gets the TBC value
    ''' </summary>
    <Display(Name:="TBC", Description:="")>
    Public ReadOnly Property TBCInd() As Boolean
      Get
        Return GetProperty(TBCIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleID, "Vehicle", 0)
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
    Public ReadOnly Property VehicleID() As Integer
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System", 0)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
    ''' <summary>
    ''' Gets the Comments value
    ''' </summary>
    <Display(Name:="Comments", Description:="")>
    Public ReadOnly Property Comments() As String
      Get
        Return GetProperty(CommentsProperty)
      End Get
    End Property

    Public Shared ChangeTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChangeTypeID, "Change Type", 0)
    ''' <summary>
    ''' Gets the Change Type value
    ''' </summary>
    <Display(Name:="Change Type", Description:="")>
    Public ReadOnly Property ChangeTypeID() As Integer
      Get
        Return GetProperty(ChangeTypeIDProperty)
      End Get
    End Property

    Public Shared ReliefCrewIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ReliefCrewInd, "Relief Crew", False)
    ''' <summary>
    ''' Gets the Relief Crew value
    ''' </summary>
    <Display(Name:="Relief Crew", Description:="")>
    Public ReadOnly Property ReliefCrewInd() As Boolean
      Get
        Return GetProperty(ReliefCrewIndProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHumanResourceHistoryIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SelectionReason

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionHumanResourceHistory(dr As SafeDataReader) As ROProductionHumanResourceHistory

      Dim r As New ROProductionHumanResourceHistory()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionHumanResourceHistoryIDProperty, .GetInt32(0))
        LoadProperty(ProductionHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(FilteredProductionTypeIndProperty, .GetBoolean(6))
        LoadProperty(FilteredPositionIndProperty, .GetBoolean(7))
        LoadProperty(IncludedOffIndProperty, .GetBoolean(8))
        LoadProperty(PreferredHumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
        LoadProperty(SuppliedHumanResourceIndProperty, .GetBoolean(10))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(SelectionReasonProperty, .GetString(12))
        LoadProperty(CancelledDateProperty, .GetValue(13))
        LoadProperty(CancelledReasonProperty, .GetString(14))
        LoadProperty(CrewLeaderIndProperty, .GetBoolean(15))
        LoadProperty(TrainingIndProperty, .GetBoolean(16))
        LoadProperty(TBCIndProperty, .GetBoolean(17))
        LoadProperty(CreatedByProperty, .GetInt32(18))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(19))
        LoadProperty(ModifiedByProperty, .GetInt32(20))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(21))
        LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
        LoadProperty(CommentsProperty, .GetString(24))
        LoadProperty(ChangeTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(25)))
        LoadProperty(ReliefCrewIndProperty, .GetBoolean(26))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace