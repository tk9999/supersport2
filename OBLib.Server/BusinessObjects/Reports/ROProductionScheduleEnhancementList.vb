﻿' Generated 24 Feb 2016 16:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Reports

  <Serializable()> _
  Public Class ROProductionScheduleEnhancementList
    Inherits OBReadOnlyListBase(Of ROProductionScheduleEnhancementList, ROProductionScheduleEnhancement)

#Region " Parent "

    <NotUndoable()> Private mParent As ROProductionSchedule
#End Region

#Region " Business Methods "

    Public Function GetStringValue() As String

      If Me.Count = 0 Then Return ""

      Dim str As String = ""
      Dim list As New List(Of String)
      For Each item As ROProductionScheduleEnhancement In Me
        If Not list.Contains(item.Enhancement) Then
          list.Add(item.Enhancement)
        End If
      Next
      For i As Integer = 0 To list.Count - 1
        Dim cnt As Integer = CountEnhancements(list(i))
        If cnt > 1 Then
          list(i) &= " (" & cnt & ")"
        End If
      Next

      For Each est As String In list
        str &= est & ", "
      Next
      Return Left(str, str.Length - 2)

    End Function

    Private Function CountEnhancements(Enhancement As String) As Integer

      Dim cnt As Integer = 0
      For Each child As ROProductionScheduleEnhancement In Me
        If child.Enhancement = Enhancement Then
          cnt += IIf(child.NoOfEnhancementItems = 0, 1, child.NoOfEnhancementItems)
        End If
      Next
      Return cnt

    End Function

    Public Function GetItem(ProductionID As Integer) As ROProductionScheduleEnhancement

      For Each child As ROProductionScheduleEnhancement In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROProductionScheduleEnhancementList() As ROProductionScheduleEnhancementList

      Return New ROProductionScheduleEnhancementList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace
