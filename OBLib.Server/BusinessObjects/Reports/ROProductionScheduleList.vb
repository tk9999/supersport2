﻿' Generated 24 Feb 2016 16:57 - Singular Systems Object Generator Version 2.1.676
Imports System.ComponentModel
Imports Csla
Imports Csla.Data
Imports Csla.Core
Imports OBLib.HR.ReadOnly
Imports Singular.Misc
Imports OBLib
Imports System.Reflection

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Reports

  <Serializable()> _
  Public Class ROProductionScheduleList
    Inherits OBReadOnlyListBase(Of ROProductionScheduleList, ROProductionSchedule)

#Region "Custom Classes"
    Public Class FontData

      Public Font As Font
      Public Row As Integer
      Public Column As Integer
      Public Sub New(Font As Font, Row As Integer, Column As Integer)
        Me.Font = Font
        Me.Row = Row
        Me.Column = Column
      End Sub

    End Class

    Public Class FontDataList

      Private mList As New List(Of FontData)
      Public Sub Add(Font As Font, Row As Integer, Column As Integer)

        Dim fd As New FontData(Font, Row, Column)
        mList.Add(fd)

      End Sub

      Public Function GetItem(Row As Integer, Column As Integer) As FontData

        For Each fd As FontData In mList
          If fd.Row = Row And fd.Column = Column Then
            Return fd
          End If
        Next
        Return Nothing

      End Function

    End Class
#End Region

#Region " Business Methods "

    Public Const PrimaryKeyDiscipline As String = "Discipline"
    Public Const PrimaryKeyPosition As String = "Position"
    Public Const RowHeaderColName As String = "Trvl req nbr"
    Public Const SortOrderColName As String = "Sort Order"
    Public Const StatusRowNumber As Integer = 10
    Public Const HeaderRows As Integer = 11
    Public mSystemID As Integer = 0
    Public HROffLeaveList As List(Of OBLib.Helpers.ProductionScheduleHRLeave) = New List(Of OBLib.Helpers.ProductionScheduleHRLeave)

    Public mReportDataset As DataSet
    Public ReportDatasetFontData As FontDataList = New FontDataList

    Public Function GetItem(ProductionID As Integer) As ROProductionSchedule

      For Each child As ROProductionSchedule In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetROProductionScheduleDetail(ProductionHumanResourceID As Integer) As ROProductionScheduleDetail

      Dim obj As ROProductionScheduleDetail = Nothing
      For Each parent As ROProductionSchedule In Me
        obj = parent.ROProductionScheduleDetailList.GetItem(ProductionHumanResourceID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROProductionScheduleTxTimeline(ProductionTimelineID As Integer) As ROProductionScheduleTxTimeline

      Dim obj As ROProductionScheduleTxTimeline = Nothing
      For Each parent As ROProductionSchedule In Me
        obj = parent.ROProductionScheduleTxTimelineList.GetItem(ProductionTimelineID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROProductionScheduleEnhancement(ProductionID As Integer) As ROProductionScheduleEnhancement

      Dim obj As ROProductionScheduleEnhancement = Nothing
      For Each parent As ROProductionSchedule In Me
        obj = parent.ROProductionScheduleEnhancementList.GetItem(ProductionID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing
    End Function

    Public Function GetReport(ExcludeOff As Boolean, ExcludeHistory As Boolean, SystemID As Integer) As DataSet

      CommonData.Refresh("ProductionTypeList")

      'ReportDatasetFontData = New FontDataList
      mSystemID = SystemID

      'these are the list of disciplines that will be displayed on this report. Add here at add a discipline to the report
      'they will appear in the order they added here
      Dim ListOfDisciplines As New List(Of CommonData.Enums.Discipline)
      ListOfDisciplines.Add(CommonData.Enums.Discipline.EventManager)
      ListOfDisciplines.Add(CommonData.Enums.Discipline.EventManagerIntern)

      If SystemID = 0 Then
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Director)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Producer)
      End If

      If SystemID = CommonData.Enums.System.ProductionServices OrElse SystemID = 0 Then
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Engineer)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Driver)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.AudioMixer)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.AudioAssistant)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.CameraOperator)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.VisionController)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.EVSOperator)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ReliefCrewDriver)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ReliefCrewOps)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ReliefCrewRig)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.HandHeldMicCarrier)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Bashers)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Trainee)
        'ListOfDisciplines.Add(CommonData.Enums.Discipline.VisionControlSupervisor)
      End If

      If SystemID = CommonData.Enums.System.ProductionContent OrElse SystemID = 0 Then
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ProductionManager)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ProductionCoOrdinator)
        'ListOfDisciplines.Add(CommonData.Enums.Discipline.HeadofProduction)
        'ListOfDisciplines.Add(CommonData.Enums.Discipline.OperationsManager)
        'ListOfDisciplines.Add(CommonData.Enums.Discipline.LogisticsManager)
        If SystemID <> 0 Then
          ListOfDisciplines.Add(CommonData.Enums.Discipline.Producer)
          ListOfDisciplines.Add(CommonData.Enums.Discipline.Director)
        End If
        ListOfDisciplines.Add(CommonData.Enums.Discipline.VTDirector)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.FloorManager)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ProductionAssistant)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.VisionMixer)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Stats)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Graphics)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.MakeUpArtist)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Commentator)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.SidelineCommentator)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.OBFieldPresenter)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.StudioPresenter)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.StudioGuest)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ENGCameraman)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Editor)

      End If
      mReportDataset = New DataSet
      Dim dt As New DataTable("Table")
      mReportDataset.Tables.Add(dt)

      'add the primary key columns ("Discipline" and "Position") to enable uniqueness and allow us to use the datatable's fetch methods
      'these columns will be hidden at the end
      dt.Columns.Add(PrimaryKeyDiscipline, GetType(System.String))
      dt.Columns.Add(PrimaryKeyPosition, GetType(System.String))
      dt.PrimaryKey = New DataColumn() {dt.Columns(PrimaryKeyDiscipline), dt.Columns(PrimaryKeyPosition)}

      dt.Columns.Add(RowHeaderColName, GetType(System.String))
      dt.Columns.Add(SortOrderColName, GetType(System.Int32))

      Dim AddStatusRow As Boolean = HasAtLeastOneCancelledProduction()
      Dim ProductionIDList As String() = Me.Select(Function(pd) pd.ProductionID.ToString).Distinct.ToArray
      Dim ProductionIDXML As Object = Singular.Data.XML.StringArrayToXML(ProductionIDList)

      'add the production metadata to the report first

      For Each item As ROProductionSchedule In Me.OrderBy(Function(f) f.SortID)
        dt.Columns.Add(item.ProductionPK, GetType(System.String))
        GetDataRow(dt, "Day", "", "Day")(item.ProductionPK) = CDate(item.EventStart).ToString("dddd")
        GetDataRow(dt, "Date", "", "Date")(item.ProductionPK) = item.ROProductionScheduleTxTimelineList.GetStringValue
        GetDataRow(dt, "Production", "", "Production")(item.ProductionPK) = item.ProductionDescription
        GetDataRow(dt, "Venue", "", "Venue")(item.ProductionPK) = item.ProductionVenue & " " & item.City
        GetDataRow(dt, "OB Unit", "", "OB Unit")(item.ProductionPK) = item.VehicleName
        GetDataRow(dt, "CAM SPEC", "", "CAM SPEC")(item.ProductionPK) = IIf(Not IsNullNothing(item.TotalCameras), item.TotalCameras, "")
        GetDataRow(dt, "ENHANCEMENT", "", "ENHANCEMENT")(item.ProductionPK) = item.ROProductionScheduleEnhancementList.GetStringValue
        GetDataRow(dt, "HD/SD", "", "HD/SD")(item.ProductionPK) = IIf(item.HDRequiredInd, "HD BROADCAST", "")

        'Commentator specific code
        Dim colour As System.Drawing.Color = System.Drawing.Color.Black
        If Not IsNullNothing(ZeroDBNull(SystemID)) Then
          colour = System.Drawing.Color.FromName(OBLib.CommonData.Lists.ROProductionTypeList.GetItem(item.ProductionTypeID).TextColour)
        End If
        If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CommonData.Enums.System.ProductionServices) OrElse SystemID = 0 Then
          'set the colors
          If SystemID <> 0 Then
            SetCellFont(GetDataRow(dt, "Day", "", "Day"), item.ProductionPK, colour, True)
            SetCellFont(GetDataRow(dt, "Date", "", "Date"), item.ProductionPK, colour, True)
            SetCellFont(GetDataRow(dt, "Production", "", "Production"), item.ProductionPK, colour, True)
            SetCellFont(GetDataRow(dt, "Venue", "", "Venue"), item.ProductionPK, colour, True)
            SetCellFont(GetDataRow(dt, "OB Unit", "", "OB Unit"), item.ProductionPK, colour, True)
            SetCellFont(GetDataRow(dt, "CAM SPEC", "", "CAM SPEC"), item.ProductionPK, colour, True)
            SetCellFont(GetDataRow(dt, "ENHANCEMENT", "", "ENHANCEMENT"), item.ProductionPK, colour, True)
            SetCellFont(GetDataRow(dt, "HD/SD", "", "HD/SD"), item.ProductionPK, colour, True)
          End If
          'set the text          
          GetDataRow(dt, "Production Call Time", "", "Production Call Time")(item.ProductionPK) = If(item.ProductionCallTime = "", "", DateTime.Parse(item.ProductionCallTime).ToString("HH:mm"))
          SetCellFont(GetDataRow(dt, "Production Call Time", "", "Production Call Time"), item.ProductionPK, colour, True)
          GetDataRow(dt, "Talent Call Time", "", "Talent Call Time")(item.ProductionPK) = If(item.TalentCallTime = "", "", DateTime.Parse(item.TalentCallTime).ToString("HH:mm"))
          SetCellFont(GetDataRow(dt, "Talent Call Time", "", "Talent Call Time"), item.ProductionPK, colour, True)
          GetDataRow(dt, "Facility Checks Start", "", "Facility Checks Start")(item.ProductionPK) = If(item.FacilityChecks = "", "", DateTime.Parse(item.FacilityChecks).ToString("HH:mm"))
          SetCellFont(GetDataRow(dt, "Facility Checks Start", "", "Facility Checks Start"), item.ProductionPK, colour, True)
          GetDataRow(dt, "Build Up Start", "", "Build Up Start")(item.ProductionPK) = If(item.BuildUp = "", "", DateTime.Parse(item.BuildUp).ToString("HH:mm"))
          SetCellFont(GetDataRow(dt, "Build Up Start", "", "Build Up Start"), item.ProductionPK, colour, True)
          GetDataRow(dt, "Event Start", "", "Event Start")(item.ProductionPK) = CDate(item.EventStart).ToString("HH:mm")
          SetCellFont(GetDataRow(dt, "Event Start", "", "Event Start"), item.ProductionPK, colour, True)
        End If

        GetDataRow(dt, "ADDITIONAL MATCHES", "", "ADDITIONAL MATCHES")(item.ProductionPK) = item.AdditionalMatches
        If AddStatusRow Then
          GetDataRow(dt, "Status", "", "Status")(item.ProductionPK) = IIf(item.CancelledDate.HasValue, "CANCELLED", "")
        End If

        If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CommonData.Enums.System.ProductionServices) Then
          SetCellFont(GetDataRow(dt, "ADDITIONAL MATCHES", "", "ADDITIONAL MATCHES"), item.ProductionPK, colour, True)
          SetCellFont(GetDataRow(dt, "Status", "", "Status"), item.ProductionPK, colour, True)
        End If
      Next

      Dim BlankRowCount As Integer = 1
      AddBlankRow(dt, BlankRowCount)

      Dim MergeChildList As ROProductionScheduleDetailList = Me.GetAllROProductionScheduleDetailList

      'now loop through each discipline and the HRs to the report
      For Each Discipline As CommonData.Enums.Discipline In ListOfDisciplines
        Dim FirstDiscipline As Boolean = True
        Dim CrewLeadersAdded As Boolean = False
        Dim DisciplineRowsAdded As Boolean = False
        Dim OverridePosition As Boolean = False
        Dim DummyPositionID As Integer = 1


        'these are the disciplines that we don't want to show the position or that don't have positions
        'so I give them a dummy position (that will be hidden) to keep the datatable's primary key happy
        Select Case Discipline
          Case CommonData.Enums.Discipline.Bashers, CommonData.Enums.Discipline.Engineer, CommonData.Enums.Discipline.Driver, _
                CommonData.Enums.Discipline.AudioAssistant, CommonData.Enums.Discipline.AudioMixer, CommonData.Enums.Discipline.EVSOperator, _
                CommonData.Enums.Discipline.Trainee, CommonData.Enums.Discipline.ReliefCrewDriver, CommonData.Enums.Discipline.ReliefCrewOps, _
                CommonData.Enums.Discipline.ReliefCrewRig, CommonData.Enums.Discipline.HandHeldMicCarrier, CommonData.Enums.Discipline.EventManager, _
                CommonData.Enums.Discipline.Commentator, CommonData.Enums.Discipline.Graphics, CommonData.Enums.Discipline.VisionController, _
                CommonData.Enums.Discipline.EventManagerIntern
            OverridePosition = True
        End Select

        'if there are going to be crew leaders we add the heading now to make sure it's there and on top
        If MergeChildList.DisciplineHasCrewLeaders(Discipline) Then
          GetDataRow(dt, Discipline, "Crew Leader", "Crew Leader")
        End If

        Dim DisciplineMergeChildList As ROProductionScheduleDetailList = New ROProductionScheduleDetailList
        Dim DisciplineMergeChildListOrdered As List(Of ROProductionScheduleDetail) = New List(Of ROProductionScheduleDetail)
        If Discipline <> CommonData.Enums.Discipline.Commentator Then
          DisciplineMergeChildListOrdered = MergeChildList.FilterList(Discipline).ToList.OrderBy(Function(x) x.SortOrder).ToList '.Sort("SortOrder", ListSortDirection.Ascending)
        Else
          DisciplineMergeChildListOrdered = MergeChildList.FilterList(Discipline).ToList
        End If


        If DisciplineMergeChildListOrdered.Count > 0 Then

          For i = 0 To DisciplineMergeChildListOrdered.Count - 1
            DisciplineMergeChildList.Add(DisciplineMergeChildListOrdered(i))
          Next

          For Each HR As ROProductionScheduleDetail In DisciplineMergeChildList

            '------------------------------------------
            Dim ProductionTypeID As Integer = Nothing
            Dim colour As System.Drawing.Color = System.Drawing.Color.Black

            DisciplineRowsAdded = True
            Dim Position As String = ""
            Dim RowHeader As String = ""

            'Determine what position to use for the HR
            If OverridePosition Then
              'GetNextDummyPositionKey(dt, HR.ProductionPK, DummyPositionID, Discipline)
            End If
            Position = IIf(OverridePosition, DummyPositionID, HR.Position)

            'If Not CrewLeadersAdded Then
            If DisciplineMergeChildList.DisciplineHasCrewLeadersForProduction(HR.ProductionID, Discipline) Then
              RowHeader = "Crew Leader"
              Dim row As DataRow = GetDataRow(dt, Discipline, "Crew Leader", RowHeader)
              row(HR.ProductionID) = DisciplineMergeChildList.GetCrewLeadersNamesForProduction(HR.ProductionID, Discipline)
              If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CommonData.Enums.System.ProductionServices) Then
                SetFont(row, HR.ProductionID, True, colour)
              Else
                SetFont(row, HR.ProductionID, True, System.Drawing.Color.Black)
              End If

            End If
            CrewLeadersAdded = True

            If Not HR.TrainingInd Then
              If FirstDiscipline Then

                RowHeader = HR.Discipline & " " & HR.PositionCode
                FirstDiscipline = False

              Else
                If Discipline <> CommonData.Enums.Discipline.Commentator Then
                  RowHeader = HR.PositionCode
                Else
                  RowHeader = HR.Discipline & " " & HR.PositionCode
                End If

              End If

              DummyPositionID += 1
              Dim row As DataRow = GetDataRow(dt, Discipline, Position, RowHeader)
              'Dim History As ROProductionHumanResourceHistory = Nothing
              'Try
              '  History = HistoryList.GetHistoryItem(HR.ProductionID, HR.DisciplineID, HR.PositionID, CommonData.Enums.ChangeType.Deleted)
              'Catch ex As Exception
              '  History = Nothing
              'End Try
              Dim clr As System.Drawing.Color = System.Drawing.Color.Black
              'If History IsNot Nothing AndAlso Not IsNullNothing(History.HumanResourceID, True) AndAlso Not ExcludeHistory Then
              '  Dim HResource As OBLib.HR.ReadOnly.ROHumanResource = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(History.HumanResourceID)
              '  If HResource IsNot Nothing AndAlso Not Singular.Misc.CompareSafe(HR.HumanResourceID, HResource.HumanResourceID) Then
              '    row(HR.ProductionID) = HR.HumanResource & " ( previously: " & HResource.FirstPreferredSurname.ToString & " )"
              '    clr = System.Drawing.Color.Red
              '  Else
              '    row(HR.ProductionID) = HR.HumanResource
              '  End If
              'Else
              '  row(HR.ProductionID) = HR.HumanResource
              'End If
              If Singular.Misc.CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CommonData.Enums.System.ProductionServices) Then
                SetFont(row, HR.ProductionID, If(CompareSafe(SystemID, CommonData.Enums.System.ProductionServices), True, Not HR.FreelancerInd) And Not HR.NoHumanResourceInd, colour)
              Else
                SetFont(row, HR.ProductionID, If(CompareSafe(SystemID, CommonData.Enums.System.ProductionServices), True, Not HR.FreelancerInd) And Not HR.NoHumanResourceInd, clr)
              End If

            End If

          Next

        Else
        End If

        If DisciplineRowsAdded Then
          AddBlankRow(dt, BlankRowCount)
        End If

        'before we go onto the next discipline lets add all the off people
        If Not ExcludeOff Then
          Dim HasAddedOffRows As Boolean = False

          'For Each ROProductionSchedule As ROProductionSchedule In Me
          '  Dim RowID As Integer = 1
          '  Dim ROProductionScheduleOffResourceList As ROProductionScheduleOffResourceList = ROProductionScheduleOffResourceList.GetROProductionScheduleOffResourceList(CDate(ROProductionSchedule.TxStart), Discipline, SystemID)
          '  For Each ROProductionScheduleOffResource As ROProductionScheduleOffResource In ROProductionScheduleOffResourceList

          '    Dim colour As System.Drawing.Color = System.Drawing.Color.Black
          '    If Not IsNullNothing(ZeroDBNull(SystemID)) Then
          '      colour = CommonData.Lists.ProductionTypeList.GetItem(ROProductionSchedule.ProductionTypeID).TextColour
          '    End If

          '    Select Case Discipline
          '      Case CommonData.Enums.Discipline.CameraOperator, CommonData.Enums.Discipline.Bashers, CommonData.Enums.Discipline.EVSOperator

          '        Dim row As DataRow = GetDataRow(dt, Discipline.ToString & ":Off", RowID, "Off / Leave")
          '        row(ROProductionSchedule.ProductionPK) = ROProductionScheduleOffResource.HumanResource
          '        HasAddedOffRows = True
          '        RowID += 1
          '        SetFont(row, ROProductionSchedule.ProductionPK, True, colour)

          '        Dim OtherRow = FindDataRow(dt, Discipline.ToString, ROProductionSchedule.ProductionPK, ROProductionScheduleOffResource.HumanResource)
          '        If OtherRow IsNot Nothing Then
          '          OtherRow(ROProductionSchedule.ProductionPK) &= " (Working off Weekend)"
          '        End If

          '      Case CommonData.Enums.Discipline.EventManager, CommonData.Enums.Discipline.EventManagerIntern
          '        'do nothing

          '      Case Else
          '        If Not String.IsNullOrEmpty(ROProductionScheduleOffResource.OffReason) Then
          '          Dim row As DataRow = GetDataRow(dt, Discipline.ToString & ":Off", RowID, "Off / Leave")
          '          row(ROProductionSchedule.ProductionPK) = ROProductionScheduleOffResource.HumanResource
          '          HasAddedOffRows = True
          '          RowID += 1
          '          SetFont(row, ROProductionSchedule.ProductionPK, True, colour)
          '        End If
          '    End Select

          '    HROffLeaveList.Add(New HelperObjects.ProductionScheduleHRLeave(ROProductionSchedule.ProductionID, ROProductionScheduleOffResource.HumanResource))

          '  Next
          'Next

          If HasAddedOffRows Then
            AddBlankRow(dt, BlankRowCount)
          End If
        End If

      Next

      Return mReportDataset

    End Function

    Public Function GetNumberOfSchedules() As Integer
      Return Me.Count
    End Function

    Public Function HumanResourceName(Item As ROProductionSchedule, DisciplineID As Integer) As String

      Dim HRName As String = ""

      HRName = If(Item.ROProductionScheduleDetailList.Where(Function(d) CompareSafe(d.DisciplineID, DisciplineID)).Count > 0, Item.ROProductionScheduleDetailList.Where(Function(d) CompareSafe(d.DisciplineID, DisciplineID)).FirstOrDefault().HumanResource, "")

      Return HRName

    End Function

    Public Sub SetFont(Row As DataRow, ByVal ProductionID As String, Bold As Boolean, Color As System.Drawing.Color)

      Dim RowIndex As Integer = Row.Table.Rows.IndexOf(Row)
      Dim ColumnIndex As Integer = Row.Table.Columns.IndexOf(ProductionID.ToString)
      Dim font As Font
      If Bold Then
        font = New Font With {.Font = New System.Drawing.Font("Microsoft Sans Serif", 8, System.Drawing.FontStyle.Bold)}
      Else
        font = New Font With {.Font = New System.Drawing.Font("Microsoft Sans Serif", 8)}
      End If
      font.Color = Color

      ReportDatasetFontData.Add(font, RowIndex, ColumnIndex)

    End Sub

    Public Sub SetCellFont(Row As DataRow, ByVal ProductionID As String, Color As System.Drawing.Color, Bold As Boolean)

      Dim RowIndex As Integer = Row.Table.Rows.IndexOf(Row)
      Dim ColumnIndex As Integer = Row.Table.Columns.IndexOf(ProductionID.ToString)
      Dim font As Font
      If Bold Then
        font = New Font With {.Font = New System.Drawing.Font("Microsoft Sans Serif", 8, System.Drawing.FontStyle.Bold)}
      Else
        font = New Font With {.Font = New System.Drawing.Font("Microsoft Sans Serif", 8)}
      End If
      font.Color = Color
      ReportDatasetFontData.Add(font, RowIndex, ColumnIndex)

    End Sub

    Public Class Font

      Public Property Font As System.Drawing.Font
      Public Property Color As System.Drawing.Color = System.Drawing.Color.Black

    End Class

    Public Sub GetNextDummyPositionKey(ByRef dt As DataTable, ByVal ProductionID As String, ByRef DummyPositionID As Integer, ByVal Discipline As CommonData.Enums.Discipline)

      DummyPositionID = 0
      For Each row As DataRow In dt.Rows
        If row(PrimaryKeyDiscipline).ToString.Contains(Discipline.ToString) Then
          Dim StrPos As String = row(PrimaryKeyPosition)
          Dim Pos As Integer = 1
          If Not Singular.Misc.IsNullNothing(row(ProductionID)) AndAlso Integer.TryParse(StrPos, Pos) AndAlso DummyPositionID < Pos Then
            DummyPositionID = Pos
          End If
        End If
      Next
      DummyPositionID += 1

    End Sub

    Public Sub AddBlankRow(ByRef dt As DataTable, ByRef BlankRowNumber As Integer)
      'Production Side doesn't want blank lines
      If Not CompareSafe(mSystemID, CommonData.Enums.System.ProductionServices) Then
        Dim BlankRow As DataRow = dt.NewRow
        BlankRow(PrimaryKeyDiscipline) = "Blank"
        BlankRow(PrimaryKeyPosition) = BlankRowNumber
        dt.Rows.Add(BlankRow)
        BlankRow(SortOrderColName) = dt.Rows.Count
        BlankRowNumber += 1
      End If

    End Sub

    Public Function FindDataRow(dt As DataTable, PrimaryKeyDiscipline As Object, ProductionKey As String, HumanResource As String) As DataRow

      For Each dr As DataRow In dt.Rows
        If Not Singular.Misc.IsNullNothing(dr(ROProductionScheduleList.PrimaryKeyDiscipline)) AndAlso Not Singular.Misc.IsNullNothing(dr(ProductionKey)) AndAlso
          dr(ROProductionScheduleList.PrimaryKeyDiscipline) = PrimaryKeyDiscipline AndAlso dr(ProductionKey) = HumanResource Then
          Return dr
        End If
      Next

      Return Nothing

    End Function

    Public Function GetDataRow(dt As DataTable,
                               PrimaryKeyDiscipline As Object, _
                               PrimaryKeyPosition As Object,
                               RowHeader As String) As DataRow

      Dim row As DataRow = dt.Rows.Find(New Object() {PrimaryKeyDiscipline, PrimaryKeyPosition})
      If row Is Nothing Then
        row = dt.LoadDataRow(New Object() {PrimaryKeyDiscipline, PrimaryKeyPosition}, True)
        row(ROProductionScheduleList.RowHeaderColName) = RowHeader
        row(SortOrderColName) = dt.Rows.Count
      End If
      Return row

    End Function

    Public Function HasAtLeastOneCancelledProduction() As Boolean

      For Each obj As ROProductionSchedule In Me
        If obj.CancelledDate.HasValue Then
          Return True
        End If
      Next
      Return False

    End Function

    Public Function GetAllROProductionScheduleDetailList() As ROProductionScheduleDetailList

      Dim list As ROProductionScheduleDetailList = ROProductionScheduleDetailList.NewROProductionScheduleDetailList()
      'list.IsReadOnly = False
      For Each ROProductionSchedule As ROProductionSchedule In Me
        For Each ROProductionScheduleDetail As ROProductionScheduleDetail In ROProductionSchedule.ROProductionScheduleDetailList
          list.Add(ROProductionScheduleDetail)
        Next
      Next
      'list.IsReadOnly = True
      Return list

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)
      Public ProductionTypeID As Integer? = Nothing
      Public EventTypeID As Integer? = Nothing
      Public StartDate As DateTime? = Nothing
      Public EndDate As DateTime? = Nothing
      Public EventManagerHumanResourceID As Integer? = Nothing
      Public SystemID As Integer? = Nothing

      Public Sub New(StartDate As DateTime?, EndDate As DateTime?, ProductionTypeID As Integer?, EventTypeID As Integer?, EventManagerHumanResourceID As Integer?, SystemID As Integer?)
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.ProductionTypeID = ProductionTypeID
        Me.EventTypeID = EventTypeID
        Me.EventManagerHumanResourceID = EventManagerHumanResourceID
        Me.SystemID = SystemID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionScheduleList() As ROProductionScheduleList

      Return New ROProductionScheduleList()

    End Function

    Public Shared Sub BeginGetROProductionScheduleList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionScheduleList)))

      Dim dp As New DataPortal(Of ROProductionScheduleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionScheduleList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionScheduleList)))

      Dim dp As New DataPortal(Of ROProductionScheduleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionScheduleList() As ROProductionScheduleList

      Return DataPortal.Fetch(Of ROProductionScheduleList)(New Criteria())

    End Function

    Public Shared Function GetROProductionScheduleList(StartDate As DateTime?, EndDate As DateTime?, ProductionTypeID As Integer?, EventTypeID As Integer?, EventManagerHumanResourceID As Integer?, SystemID As Integer?) As ROProductionScheduleList

      Return DataPortal.Fetch(Of ROProductionScheduleList)(New Criteria(StartDate, EndDate, ProductionTypeID, EventTypeID, EventManagerHumanResourceID, SystemID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionSchedule.GetROProductionSchedule(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROProductionSchedule = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROProductionScheduleDetailList.RaiseListChangedEvents = False
          parent.ROProductionScheduleDetailList.Add(ROProductionScheduleDetail.GetROProductionScheduleDetail(sdr))
          parent.ROProductionScheduleDetailList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROProductionScheduleTxTimelineList.RaiseListChangedEvents = False
          parent.ROProductionScheduleTxTimelineList.Add(ROProductionScheduleTxTimeline.GetROProductionScheduleTxTimeline(sdr))
          parent.ROProductionScheduleTxTimelineList.RaiseListChangedEvents = True
        End While
      End If


      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ProductionID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.ROProductionScheduleEnhancementList.RaiseListChangedEvents = False
          parent.ROProductionScheduleEnhancementList.Add(ROProductionScheduleEnhancement.GetROProductionScheduleEnhancement(sdr))
          parent.ROProductionScheduleEnhancementList.RaiseListChangedEvents = True
        End While
      End If



    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionScheduleList"
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@ProductionTypeID", Singular.Misc.NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@EventTypeID", Singular.Misc.NothingDBNull(crit.EventTypeID))
            cm.Parameters.AddWithValue("@EventManagerHumanResourceID", Singular.Misc.NothingDBNull(crit.EventManagerHumanResourceID))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
