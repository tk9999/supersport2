﻿' Generated 24 Feb 2016 16:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Reports

  <Serializable()> _
  Public Class ROProductionScheduleDetail
    Inherits OBReadOnlyBase(Of ROProductionScheduleDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionHumanResourceID() As Integer
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property DisciplineID() As Integer
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
    End Property

    Public Shared PositionCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionCode, "Position Code")
    ''' <summary>
    ''' Gets the Position Code value
    ''' </summary>
    <Display(Name:="Position Code", Description:="")>
    Public ReadOnly Property PositionCode() As String
      Get
        Return GetProperty(PositionCodeProperty)
      End Get
    End Property

    Public Shared CrewLeaderIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CrewLeaderInd, "Crew Leader", False)
    ''' <summary>
    ''' Gets the Crew Leader value
    ''' </summary>
    <Display(Name:="Crew Leader", Description:="")>
    Public ReadOnly Property CrewLeaderInd() As Boolean
      Get
        Return GetProperty(CrewLeaderIndProperty)
      End Get
    End Property

    Public Shared PositionRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PositionRequiredInd, "Position Required", False)
    ''' <summary>
    ''' Gets the Position Required value
    ''' </summary>
    <Display(Name:="Position Required", Description:="")>
    Public ReadOnly Property PositionRequiredInd() As Boolean
      Get
        Return GetProperty(PositionRequiredIndProperty)
      End Get
    End Property

    Public Shared TrainingIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TrainingInd, "Training", False)
    ''' <summary>
    ''' Gets the Training value
    ''' </summary>
    <Display(Name:="Training", Description:="")>
    Public ReadOnly Property TrainingInd() As Boolean
      Get
        Return GetProperty(TrainingIndProperty)
      End Get
    End Property

    Public Shared SortOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SortOrder, "Sort Order")
    ''' <summary>
    ''' Gets the Sort Order value
    ''' </summary>
    <Display(Name:="Sort Order", Description:="")>
    Public ReadOnly Property SortOrder() As Integer
      Get
        Return GetProperty(SortOrderProperty)
      End Get
    End Property

    Public Shared HRCityIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HRCityID, "HR City")
    ''' <summary>
    ''' Gets the HR City value
    ''' </summary>
    <Display(Name:="HR City", Description:="")>
    Public ReadOnly Property HRCityID() As Integer
      Get
        Return GetProperty(HRCityIDProperty)
      End Get
    End Property

    Public Shared TBCIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TBCInd, "TBC", False)
    ''' <summary>
    ''' Gets the TBC value
    ''' </summary>
    <Display(Name:="TBC", Description:="")>
    Public ReadOnly Property TBCInd() As Boolean
      Get
        Return GetProperty(TBCIndProperty)
      End Get
    End Property

    Public Shared FreelancerIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FreelancerInd, "Freelancer", False)
    ''' <summary>
    ''' Gets the Freelancer value
    ''' </summary>
    <Display(Name:="Freelancer", Description:="")>
    Public ReadOnly Property FreelancerInd() As Boolean
      Get
        Return GetProperty(FreelancerIndProperty)
      End Get
    End Property

    Public Shared NoHumanResourceIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.NoHumanResourceInd, "No Human Resource", False)
    ''' <summary>
    ''' Gets the No Human Resource value
    ''' </summary>
    <Display(Name:="No Human Resource", Description:="")>
    Public ReadOnly Property NoHumanResourceInd() As Boolean
      Get
        Return GetProperty(NoHumanResourceIndProperty)
      End Get
    End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionRefNo, "Production Ref No")
    ''' <summary>
    ''' Gets the Production Ref No value
    ''' </summary>
    <Display(Name:="Production Ref No", Description:="")>
    Public ReadOnly Property ProductionRefNo() As String
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionID, "Position")
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property PositionID() As Integer
      Get
        Return GetProperty(PositionIDProperty)
      End Get
    End Property

    <DisplayNameAttribute("ProductionPK")> _
    Public ReadOnly Property ProductionPK As String
      Get
        Return IIf(String.IsNullOrEmpty(ProductionRefNo), ProductionID, ProductionRefNo)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHumanResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.HumanResource

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionScheduleDetail(dr As SafeDataReader) As ROProductionScheduleDetail

      Dim r As New ROProductionScheduleDetail()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(HumanResourceProperty, .GetString(3))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(DisciplineProperty, .GetString(5))
        LoadProperty(PositionProperty, .GetString(6))
        LoadProperty(PositionCodeProperty, .GetString(7))
        LoadProperty(CrewLeaderIndProperty, .GetBoolean(8))
        LoadProperty(PositionRequiredIndProperty, .GetBoolean(9))
        LoadProperty(TrainingIndProperty, .GetBoolean(10))
        LoadProperty(SortOrderProperty, .GetInt32(11))
        LoadProperty(HRCityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(TBCIndProperty, .GetBoolean(13))
        LoadProperty(FreelancerIndProperty, .GetBoolean(14))
        LoadProperty(NoHumanResourceIndProperty, .GetBoolean(15))
        LoadProperty(ProductionRefNoProperty, .GetString(16))
        LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace