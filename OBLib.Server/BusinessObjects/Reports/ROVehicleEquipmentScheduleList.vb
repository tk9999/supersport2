﻿' Generated 17 Apr 2015 12:41 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Reports

  <Serializable()> _
  Public Class ROVehicleEquipmentScheduleList
    Inherits SingularReadOnlyListBase(Of ROVehicleEquipmentScheduleList, ROVehicleEquipmentSchedule)

#Region " Business Methods "

    Public Function GetItem(ProductionID As Integer) As ROVehicleEquipmentSchedule

      For Each child As ROVehicleEquipmentSchedule In Me
        If child.ProductionID = ProductionID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Vehicle Equipment Schedules"

    End Function

    Public Function GetListOfItems() As List(Of String)

      Dim query = From ves In Me
                  Order By ves.VehicleTypeID, ves.SuppliedInd, ves.Item
                  Group By ves.Item
                  Into x = Sum(ves.TotalMinutes)
                  Select Item


      Dim lst As New List(Of String)
      For Each obj In query
        If Not lst.Contains(obj) Then
          lst.Add(obj)
        End If
      Next
      Return lst

    End Function

    Public Function AllOnSameDay(Item As String) As Boolean

      Dim FirstDate As Date = CDate(Me(0).StartDateTime).Date
      For Each lstItem As ROVehicleEquipmentSchedule In Me
        If FirstDate <> CDate(lstItem.StartDateTime).Date OrElse FirstDate <> CDate(lstItem.EndDateTime).Date Then
          Return False
        End If
      Next
      Return True

    End Function

    Public Function GetFreeDays(ReportMonthStartDate As DateTime, ReportMonthEndDate As DateTime) As List(Of DateTime)

      Dim results As List(Of DateTime) = New List(Of DateTime)

      While ReportMonthStartDate <= ReportMonthEndDate

        Dim obj As ROVehicleEquipmentSchedule = Me.Where(Function(d) d.StartDateTime.Value.Date = ReportMonthStartDate.Date).FirstOrDefault()

        If obj Is Nothing Then
          results.Add(ReportMonthStartDate.Date)
        End If

        ReportMonthStartDate = ReportMonthStartDate.Date.AddDays(1)
      End While

      Return results

    End Function

    Public Function GetPreviousItem(ByVal ItemDate As DateTime) As ROVehicleEquipmentSchedule

      Dim result As ROVehicleEquipmentSchedule = Nothing

      result = Me.OrderBy(Function(z) z.StartDateTime).Where(Function(d) d.StartDateTime.Value.Date < ItemDate And Not d.CancelInd).LastOrDefault()

      Return result

    End Function

    Public Function GetNextItem(ByVal ItemDate As DateTime) As ROVehicleEquipmentSchedule

      Dim result As ROVehicleEquipmentSchedule = Nothing

      result = Me.OrderBy(Function(z) z.StartDateTime).Where(Function(d) d.StartDateTime.Value.Date > ItemDate And Not d.CancelInd).FirstOrDefault()

      Return result

    End Function

    Public Overloads Function FilterList(ItemName As String, Year As Integer, Month As Integer)

      Dim list As New ROVehicleEquipmentScheduleList
      'list.IsReadOnly = False
      For Each item As ROVehicleEquipmentSchedule In Me
        If item.Item = ItemName AndAlso CDate(item.StartDateTime).Year = Year AndAlso CDate(item.StartDateTime).Month = Month Then
          list.Add(item)
        End If
      Next
      'list.IsReadOnly = True
      Return list

    End Function

    Private Sub Replace(NewItem As ROVehicleEquipmentSchedule)

      Dim itemFound As Boolean = False
      Dim itemFoundDontAdd As Boolean = False
      Dim itemFoundAtIndex As Integer = -1

      For i As Integer = Me.Count - 1 To 0 Step -1
        Dim Item As ROVehicleEquipmentSchedule = Me(i)
        If Item.Item = NewItem.Item AndAlso CDate(Item.StartDateTime).Date = CDate(NewItem.StartDateTime).Date Then
          If Item.ProductionTimelineTypeID = CInt(CommonData.Enums.ProductionTimelineType.Transmission) Then
            itemFoundDontAdd = True
            Exit For
          ElseIf Not Item.TravelAndRigInd AndAlso _
                (Item.ProductionTimelineTypeID = CInt(CommonData.Enums.ProductionTimelineType.VehiclePreTravel) OrElse Item.ProductionTimelineTypeID = CInt(CommonData.Enums.ProductionTimelineType.Rig)) AndAlso _
                (NewItem.ProductionTimelineTypeID = CInt(CommonData.Enums.ProductionTimelineType.VehiclePreTravel) OrElse NewItem.ProductionTimelineTypeID = CInt(CommonData.Enums.ProductionTimelineType.Rig)) Then
            Item.TravelAndRigInd = True
            itemFoundDontAdd = True
          ElseIf Item.TotalMinutes < NewItem.TotalMinutes OrElse NewItem.ProductionTimelineTypeID = CInt(CommonData.Enums.ProductionTimelineType.Transmission) Then
            itemFound = True
            itemFoundAtIndex = i
            Exit For
          End If
        End If
      Next

      'Me.IsReadOnly = False
      If itemFound Then
        Me.RemoveAt(itemFoundAtIndex)
        'Me.IsReadOnly = False
        Me.InsertItem(itemFoundAtIndex, NewItem)
        'Me.IsReadOnly = True
      ElseIf Not itemFoundDontAdd Then
        Me.Add(NewItem)
      End If
      'Me.IsReadOnly = True

    End Sub

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public ProductionTypeID As Object = DBNull.Value
      Public EventTypeID As Object = DBNull.Value
      Public StartDate As New SmartDate
      Public EndDate As New SmartDate

      Public Sub New()

      End Sub

      Public Sub New(StartDate As Date, EndDate As Date, ProductionTypeID As Object, EventTypeID As Object)

        Me.StartDate = New Csla.SmartDate(StartDate)
        Me.EndDate = New Csla.SmartDate(EndDate)
        Me.ProductionTypeID = Singular.Misc.IsNull(ProductionTypeID, DBNull.Value)
        Me.EventTypeID = Singular.Misc.IsNull(EventTypeID, DBNull.Value)

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROVehicleEquipmentScheduleList() As ROVehicleEquipmentScheduleList

      Return New ROVehicleEquipmentScheduleList()

    End Function

    Public Shared Sub BeginGetROVehicleEquipmentScheduleList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROVehicleEquipmentScheduleList)))

      Dim dp As New DataPortal(Of ROVehicleEquipmentScheduleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    Public Shared Sub BeginGetROVehicleEquipmentScheduleList(CallBack As EventHandler(Of DataPortalResult(Of ROVehicleEquipmentScheduleList)))

      Dim dp As New DataPortal(Of ROVehicleEquipmentScheduleList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROVehicleEquipmentScheduleList(StartDate As Date, EndDate As Date, ProductionTypeID As Object, EventTypeID As Object) As ROVehicleEquipmentScheduleList

      Return DataPortal.Fetch(Of ROVehicleEquipmentScheduleList)(New Criteria(StartDate, EndDate, ProductionTypeID, EventTypeID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Replace(ROVehicleEquipmentSchedule.GetROVehicleEquipmentSchedule(sdr))
        'Me.Add(ROVehicleEquipmentSchedule.GetROVehicleEquipmentSchedule(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROVehicleEquipmentScheduleListNew"
            cm.Parameters.AddWithValue("@StartDate", crit.StartDate.DBValue)
            cm.Parameters.AddWithValue("@EndDate", crit.EndDate.DBValue)
            cm.Parameters.AddWithValue("@ProductionTypeID", crit.ProductionTypeID)
            cm.Parameters.AddWithValue("@EventTypeID", crit.EventTypeID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace