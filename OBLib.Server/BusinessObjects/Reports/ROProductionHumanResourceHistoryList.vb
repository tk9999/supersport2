﻿' Generated 17 Mar 2016 10:19 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Reports

  <Serializable()> _
  Public Class ROProductionHumanResourceHistoryList
    Inherits OBReadOnlyListBase(Of ROProductionHumanResourceHistoryList, ROProductionHumanResourceHistory)

#Region " Business Methods "

    Public Function GetItem(ProductionHumanResourceHistoryID As Integer) As ROProductionHumanResourceHistory

      For Each child As ROProductionHumanResourceHistory In Me
        If child.ProductionHumanResourceHistoryID = ProductionHumanResourceHistoryID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetHistoryItem(ProductionID As Integer, DisciplineID As Integer, PositionID As Integer, ChangeTypeID As Integer) As ROProductionHumanResourceHistory

      'change type id 1 = update, 2 = delete, 3 = insert
      'If Not Singular.Misc.IsNullNothing(DisciplineID) Then
      '  DisciplineID = CType(DisciplineID, Integer)
      'Else
      '  DisciplineID = Nothing
      'End If

      'If Not Singular.Misc.IsNullNothing(PositionID) Then
      '  PositionID = CType(PositionID, Integer)
      'Else
      '  PositionID = Nothing
      'End If

      ' Dim IntPositionID As Integer = CType(PositionID, Integer)
      Dim ProductionHistory As List(Of ROProductionHumanResourceHistory) = Me.Where(Function(data) Singular.Misc.CompareSafe(data.ProductionID, ProductionID) And (Singular.Misc.CompareSafe(data.ChangeTypeID, ChangeTypeID))).ToList()
      Dim DisciplineHistory As List(Of ROProductionHumanResourceHistory) = ProductionHistory.Where(Function(data) (Singular.Misc.CompareSafe(data.DisciplineID, DisciplineID))).ToList()
      Dim PositionHistory As List(Of ROProductionHumanResourceHistory) = DisciplineHistory.Where(Function(data) (Singular.Misc.CompareSafe(data.PositionID, PositionID))).ToList()
      Dim Result As ROProductionHumanResourceHistory = Nothing

      If Singular.Misc.CompareSafe(DisciplineID, CInt(OBLib.CommonData.Enums.Discipline.CameraOperator)) Or Singular.Misc.CompareSafe(DisciplineID, OBLib.CommonData.Enums.Discipline.EVSOperator) Or Singular.Misc.CompareSafe(DisciplineID, CInt(OBLib.CommonData.Enums.Discipline.VisionController)) Then
        Result = PositionHistory.OrderByDescending(Function(d) d.CreatedDateTime).FirstOrDefault()
        If Result IsNot Nothing Then
          Dim n = 0
        End If
      End If

      Return Result

    End Function

    Public Overrides Function ToString() As String

      Return "Production Human Resource Historys"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ProductionIDXML As String = ""

      Public Sub New()

      End Sub

      Public Sub New(ProductionIDXML As String)
        Me.ProductionIDXML = ProductionIDXML
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROProductionHumanResourceHistoryList() As ROProductionHumanResourceHistoryList

      Return New ROProductionHumanResourceHistoryList()

    End Function

    Public Shared Sub BeginGetROProductionHumanResourceHistoryList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionHumanResourceHistoryList)))

      Dim dp As New DataPortal(Of ROProductionHumanResourceHistoryList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionHumanResourceHistoryList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionHumanResourceHistoryList)))

      Dim dp As New DataPortal(Of ROProductionHumanResourceHistoryList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROProductionHumanResourceHistoryList() As ROProductionHumanResourceHistoryList

      Return DataPortal.Fetch(Of ROProductionHumanResourceHistoryList)(New Criteria())

    End Function

    Public Shared Function GetROProductionHumanResourceHistoryList(ByVal ProductionIDXML As String) As ROProductionHumanResourceHistoryList

      Return DataPortal.Fetch(Of ROProductionHumanResourceHistoryList)(New Criteria(ProductionIDXML))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionHumanResourceHistory.GetROProductionHumanResourceHistory(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionHumanResourceHistoryList"
            cm.Parameters.AddWithValue("@ProductionIDXML", Singular.Strings.MakeEmptyDBNull(crit.ProductionIDXML))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
