﻿' Generated 24 Feb 2016 16:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Reports

  <Serializable()> _
  Public Class ROProductionScheduleTxTimelineList
    Inherits OBReadOnlyListBase(Of ROProductionScheduleTxTimelineList, ROProductionScheduleTxTimeline)

#Region " Parent "

    <NotUndoable()> Private mParent As ROProductionSchedule
#End Region

#Region " Business Methods "

    Public Function GetStringValue() As String

      If Me.Count = 0 Then Return ""

      Dim AllOnSameDay As Boolean = True
      Dim AllOnConsecutiveDays As Boolean = True
      Dim AllStartTimesTheSame As Boolean = True
      Dim HasMonthBetweenDays As Boolean = False
      Dim item As ROProductionScheduleTxTimeline
      Dim nextItem As ROProductionScheduleTxTimeline
      For i As Integer = 0 To Me.Count - 2
        item = Me(i)
        nextItem = Me(i + 1)
        If Not (AllOnSameDay AndAlso item.StartDateTime.Value.DayOfYear = item.EndDateTime.Value.DayOfYear AndAlso _
          nextItem.StartDateTime.Value.DayOfYear = nextItem.EndDateTime.Value.DayOfYear AndAlso item.StartDateTime.Value.DayOfYear = nextItem.StartDateTime.Value.DayOfYear) Then
          AllOnSameDay = False
        End If
        If AllOnConsecutiveDays AndAlso DateDiff(DateInterval.Day, item.StartDateTime.Value, nextItem.StartDateTime.Value) <> 1 Then
          AllOnConsecutiveDays = False
        End If
        If AllStartTimesTheSame AndAlso item.StartDateTime.Value.TimeOfDay <> nextItem.StartDateTime.Value.TimeOfDay Then
          AllStartTimesTheSame = False
        End If
        If Not HasMonthBetweenDays AndAlso item.StartDateTime.Value.Month <> nextItem.StartDateTime.Value.Month Then
          HasMonthBetweenDays = True
        End If
      Next

      'create the date(s) portion of the string
      Dim str As String = ""
      If AllOnSameDay Then
        'all TxTimes on the same day
        str = DateTime.Parse(Me(0).StartDateTime).ToString("dd MMMM yyyy") & " "

      Else
        If AllOnConsecutiveDays And Not HasMonthBetweenDays Then
          'example: 05-10 August 2011
          str = DateTime.Parse(Me(0).StartDateTime).ToString("dd") & "-" & DateTime.Parse(Me(Me.Count - 1).StartDateTime).ToString("dd") & " " & DateTime.Parse(Me(0).StartDateTime).ToString("MMMM yyyy") & " "

        ElseIf AllOnConsecutiveDays And HasMonthBetweenDays Then
          'example: 26-31 August, 01-05 September
          For Each MonthYear As String In GetMonthYearsStrList()
            Dim d1 As New SmartDate
            Dim d2 As New SmartDate
            For i As Integer = 0 To Me.Count - 1
              If DateTime.Parse(Me(i).StartDateTime).ToString("MMMM yyyy") <> MonthYear Then Continue For
              item = Me(i)
              If d1.IsEmpty Then
                d1.Date = item.StartDateTime
              End If
              d2.Date = item.StartDateTime
            Next
            str &= IIf(d1.Date <> d2.Date, d1.ToString("dd") & "-" & d2.ToString("dd"), d1.ToString("dd")) & " " & MonthYear & ", "
          Next
          str = Left(str, str.Length - 2) & " "
        Else
          'example 01 Mar 11 - 06 Jul 11
          'this is for strange productions that may have days of in between Tx times
          str = DateTime.Parse(Me(0).StartDateTime).ToString("dd MMM yy") & " - " & DateTime.Parse(Me(Me.Count - 1).StartDateTime).ToString("dd MMM yy") & " "

        End If
      End If

      'create the time(s) portion of the string
      If AllStartTimesTheSame Then
        str &= DateTime.Parse(Me(0).StartDateTime).ToString("HH") & "h" & DateTime.Parse(Me(0).StartDateTime).ToString("mm")

      ElseIf AllOnSameDay Then
        For Each item In Me
          str &= DateTime.Parse(item.StartDateTime).ToString("HH") & "h" & DateTime.Parse(item.StartDateTime).ToString("mm") & "/"
        Next
        str = Left(str, str.Length - 1)
      End If

      Return str

    End Function


    Public Function GetMonthYearsStrList() As List(Of String)

      Dim list As New List(Of String)
      Dim item As ROProductionScheduleTxTimeline
      For i As Integer = 0 To Me.Count - 1
        item = Me(i)
        Dim str As String = DateTime.Parse(item.StartDateTime).ToString("MMMM yyyy")
        If Not list.Contains(str) Then
          list.Add(str)
        End If
      Next
      Return list

    End Function

    Public Function GetMinTxDate() As Date

      Dim minDate As Date = Date.MaxValue
      For Each item As ROProductionScheduleTxTimeline In Me
        If minDate > item.StartDateTime Then
          minDate = item.StartDateTime
        End If
      Next
      Return minDate

    End Function

    Public Function GetItem(ProductionTimelineID As Integer) As ROProductionScheduleTxTimeline

      For Each child As ROProductionScheduleTxTimeline In Me
        If child.ProductionTimelineID = ProductionTimelineID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROProductionScheduleTxTimelineList() As ROProductionScheduleTxTimelineList

      Return New ROProductionScheduleTxTimelineList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace