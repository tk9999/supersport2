﻿' Generated 24 Feb 2016 16:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Csla.Core

Namespace Reports

  <Serializable()> _
  Public Class ROProductionScheduleDetailList
    Inherits OBReadOnlyListBase(Of ROProductionScheduleDetailList, ROProductionScheduleDetail)

#Region " Parent "

    <NotUndoable()> Private mParent As ROProductionSchedule
#End Region

#Region " Business Methods "

    Public Function DisciplineHasCrewLeaders(Discipline As CommonData.Enums.Discipline) As Boolean

      For Each item As ROProductionScheduleDetail In Me
        If item.CrewLeaderInd AndAlso Singular.Misc.CompareSafe(item.DisciplineID, Discipline) Then
          Return True
        End If
      Next
      Return False

    End Function

    Public Function GetItem(ProductionHumanResourceID As Integer) As ROProductionScheduleDetail

      For Each child As ROProductionScheduleDetail In Me
        If child.ProductionHumanResourceID = ProductionHumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overloads Function FilterList(Discipline As CommonData.Enums.Discipline) As ROProductionScheduleDetailList

      Dim list As New ROProductionScheduleDetailList
      list.IsReadOnly = False
      For Each item As ROProductionScheduleDetail In Me
        If item.DisciplineID = Discipline Then
          list.Add(item)
        End If
      Next
      list.IsReadOnly = True
      Return list

    End Function

    Public Function DisciplineHasCrewLeadersForProduction(ProductionPK As Integer, Discipline As CommonData.Enums.Discipline) As Boolean

      Dim ItemProductionPK As Integer
      For Each item As ROProductionScheduleDetail In Me
        Int32.TryParse(item.ProductionPK, ItemProductionPK)
        If item.CrewLeaderInd AndAlso Singular.Misc.CompareSafe(ItemProductionPK, ProductionPK) AndAlso Singular.Misc.CompareSafe(item.DisciplineID, CInt(Discipline)) Then
          Return True
        End If
      Next
      Return False

    End Function

    Public Function GetCrewLeadersNamesForProduction(ProductionPK As Integer, Discipline As CommonData.Enums.Discipline) As String

      Dim names As String = ""
      Dim HRProductionPK As Integer
      For i As Integer = 0 To Me.Count - 1
        Dim item As ROProductionScheduleDetail = Me(i)
        Int32.TryParse(item.ProductionPK, HRProductionPK)
        If item.CrewLeaderInd AndAlso Singular.Misc.CompareSafe(HRProductionPK, ProductionPK) AndAlso Singular.Misc.CompareSafe(item.DisciplineID, CInt(Discipline)) Then
          names &= item.HumanResource & ", "
        End If
      Next
      names = names.Trim
      If names.EndsWith(",") Then
        names = names.Substring(0, names.LastIndexOf(","))
      End If
      Return names

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewROProductionScheduleDetailList() As ROProductionScheduleDetailList

      Return New ROProductionScheduleDetailList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

#End If

#End Region

#End Region

  End Class

End Namespace
