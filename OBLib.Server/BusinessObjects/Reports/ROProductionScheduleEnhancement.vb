﻿' Generated 24 Feb 2016 16:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Reports

  <Serializable()> _
  Public Class ROProductionScheduleEnhancement
    Inherits OBReadOnlyBase(Of ROProductionScheduleEnhancement)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared EnhancementProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Enhancement, "Enhancement")
    ''' <summary>
    ''' Gets the Enhancement value
    ''' </summary>
    <Display(Name:="Enhancement", Description:="")>
    Public ReadOnly Property Enhancement() As String
      Get
        Return GetProperty(EnhancementProperty)
      End Get
    End Property

    Public Shared NoOfEnhancementItemsProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NoOfEnhancementItems, "No Of Enhancement Items")
    ''' <summary>
    ''' Gets the No Of Enhancement Items value
    ''' </summary>
    <Display(Name:="No Of Enhancement Items", Description:="")>
    Public ReadOnly Property NoOfEnhancementItems() As Integer
      Get
        Return GetProperty(NoOfEnhancementItemsProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Enhancement

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionScheduleEnhancement(dr As SafeDataReader) As ROProductionScheduleEnhancement

      Dim r As New ROProductionScheduleEnhancement()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionIDProperty, .GetInt32(0))
        LoadProperty(EnhancementProperty, .GetString(1))
        LoadProperty(NoOfEnhancementItemsProperty, .GetInt32(2))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
