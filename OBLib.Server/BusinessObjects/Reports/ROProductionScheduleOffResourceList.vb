﻿Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Reports

  <Serializable()> _
  Public Class ROProductionScheduleOffResourceList
    Inherits OBReadOnlyListBase(Of ROProductionScheduleOffResourceList, ROProductionScheduleOffResource)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROProductionScheduleOffResource

      For Each child As ROProductionScheduleOffResource In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "s"

    End Function

#End Region

#Region " Common "

    Public Shared Function NewROProductionScheduleOffResourceList() As ROProductionScheduleOffResourceList

      Return New ROProductionScheduleOffResourceList()

    End Function

    Public Shared Sub BeginGetROProductionScheduleOffResourceList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROProductionScheduleOffResourceList)))

      Dim dp As New DataPortal(Of ROProductionScheduleOffResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROProductionScheduleOffResourceList(CallBack As EventHandler(Of DataPortalResult(Of ROProductionScheduleOffResourceList)))

      Dim dp As New DataPortal(Of ROProductionScheduleOffResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(loggedinonly:=True)> _
    Public Class Criteria

      Public AtDate As Date
      'Public ProductionIDs As String = ""
      'Public DisciplineID As Integer = 0
      Public DisciplineIDs As String = ""
      Public SystemID As Integer = 0

      Public Sub New(ByVal AtDate As String, ByVal DisciplineIDs As String, ByVal SystemID As Integer)

        Me.AtDate = AtDate
        Me.DisciplineIDs = DisciplineIDs
        Me.SystemID = SystemID

      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function GetROProductionScheduleOffResourceList(AtDate As Date, DisciplineIDs As String, SystemID As Integer) As ROProductionScheduleOffResourceList

      Return DataPortal.Fetch(Of ROProductionScheduleOffResourceList)(New Criteria(AtDate, DisciplineIDs, SystemID))

    End Function

    Private Sub Fetch(ByVal sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROProductionScheduleOffResource.GetROProductionScheduleOffResource(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True


    End Sub

    Protected Overrides Sub DataPortal_Fetch(ByVal criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROProductionScheduleOffResourceList"
            cm.Parameters.AddWithValue("@AtDate", crit.AtDate)
            cm.Parameters.AddWithValue("@DisciplineIDs", Singular.Strings.MakeEmptyDBNull(crit.DisciplineIDs))
            cm.Parameters.AddWithValue("@SystemID", crit.SystemID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class


End Namespace