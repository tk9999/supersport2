﻿' Generated 24 Feb 2016 16:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Reports

  <Serializable()> _
  Public Class ROProductionSchedule
    Inherits OBReadOnlyBase(Of ROProductionSchedule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared EventStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EventStart, "Event Start")
    ''' <summary>
    ''' Gets the Event Start value
    ''' </summary>
    <Display(Name:="Event Start", Description:="")>
    Public ReadOnly Property EventStart As DateTime?
      Get
        Return GetProperty(EventStartProperty)
      End Get
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Production Description")
    ''' <summary>
    ''' Gets the Production Description value
    ''' </summary>
    <Display(Name:="Production Description", Description:="")>
    Public ReadOnly Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Production Venue")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="")>
    Public ReadOnly Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
    End Property

    Public Shared CityIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CityID, "City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property CityID() As Integer
      Get
        Return GetProperty(CityIDProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
    Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City Code")
    ''' <summary>
    ''' Gets the City Code value
    ''' </summary>
    <Display(Name:="City Code", Description:="")>
    Public ReadOnly Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name")
    ''' <summary>
    ''' Gets the Vehicle Name value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="")>
    Public ReadOnly Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
    End Property

    Public Shared VehicleDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleDescription, "Vehicle Description")
    ''' <summary>
    ''' Gets the Vehicle Description value
    ''' </summary>
    <Display(Name:="Vehicle Description", Description:="")>
    Public ReadOnly Property VehicleDescription() As String
      Get
        Return GetProperty(VehicleDescriptionProperty)
      End Get
    End Property

    Public Shared HDRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HDRequiredInd, "HD Required", False)
    ''' <summary>
    ''' Gets the HD Required value
    ''' </summary>
    <Display(Name:="HD Required", Description:="")>
    Public ReadOnly Property HDRequiredInd() As Boolean
      Get
        Return GetProperty(HDRequiredIndProperty)
      End Get
    End Property

    Public Shared TotalCamerasProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TotalCameras, "Total Cameras")
    ''' <summary>
    ''' Gets the Total Cameras value
    ''' </summary>
    <Display(Name:="Total Cameras", Description:="")>
    Public ReadOnly Property TotalCameras() As String
      Get
        Return GetProperty(TotalCamerasProperty)
      End Get
    End Property

    Public Shared CancelledDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CancelledDate, "Cancelled Date")
    ''' <summary>
    ''' Gets the Cancelled Date value
    ''' </summary>
    <Display(Name:="Cancelled Date", Description:="")>
    Public ReadOnly Property CancelledDate As DateTime?
      Get
        Return GetProperty(CancelledDateProperty)
      End Get
    End Property

    Public Shared AdditionalMatchesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdditionalMatches, "Additional Matches")
    ''' <summary>
    ''' Gets the Additional Matches value
    ''' </summary>
    <Display(Name:="Additional Matches", Description:="")>
    Public ReadOnly Property AdditionalMatches() As String
      Get
        Return GetProperty(AdditionalMatchesProperty)
      End Get
    End Property

    Public Shared SortIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SortID, "Sort")
    ''' <summary>
    ''' Gets the Sort value
    ''' </summary>
    <Display(Name:="Sort", Description:="")>
    Public ReadOnly Property SortID() As Integer
      Get
        Return GetProperty(SortIDProperty)
      End Get
    End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionRefNo, "Production Ref No")
    ''' <summary>
    ''' Gets the Production Ref No value
    ''' </summary>
    <Display(Name:="Production Ref No", Description:="")>
    Public ReadOnly Property ProductionRefNo() As String
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
    End Property

    Public Shared ProductionMeetingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionMeeting, "Production Meeting")
    ''' <summary>
    ''' Gets the Production Meeting value
    ''' </summary>
    <Display(Name:="Production Meeting", Description:="")>
    Public ReadOnly Property ProductionMeeting() As String
      Get
        Return GetProperty(ProductionMeetingProperty)
      End Get
    End Property

    Public Shared FacilityChecksProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FacilityChecks, "Facility Checks")
    ''' <summary>
    ''' Gets the Facility Checks value
    ''' </summary>
    <Display(Name:="Facility Checks", Description:="")>
    Public ReadOnly Property FacilityChecks() As String
      Get
        Return GetProperty(FacilityChecksProperty)
      End Get
    End Property

    Public Shared CommentatorMeetingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CommentatorMeeting, "Commentator Meeting")
    ''' <summary>
    ''' Gets the Commentator Meeting value
    ''' </summary>
    <Display(Name:="Commentator Meeting", Description:="")>
    Public ReadOnly Property CommentatorMeeting() As String
      Get
        Return GetProperty(CommentatorMeetingProperty)
      End Get
    End Property

    Public Shared BuildUpProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.BuildUp, "Build Up")
    ''' <summary>
    ''' Gets the Build Up value
    ''' </summary>
    <Display(Name:="Build Up", Description:="")>
    Public ReadOnly Property BuildUp() As String
      Get
        Return GetProperty(BuildUpProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeID, "Production Type")
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionTypeID() As Integer
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionCallTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionCallTime, "Production Call Time")
    ''' <summary>
    ''' Gets the Production Call Time value
    ''' </summary>
    <Display(Name:="Production Call Time", Description:="")>
    Public ReadOnly Property ProductionCallTime() As String
      Get
        Return GetProperty(ProductionCallTimeProperty)
      End Get
    End Property

    Public Shared TalentCallTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TalentCallTime, "Talent Call Time")
    ''' <summary>
    ''' Gets the Talent Call Time value
    ''' </summary>
    <Display(Name:="Talent Call Time", Description:="")>
    Public ReadOnly Property TalentCallTime() As String
      Get
        Return GetProperty(TalentCallTimeProperty)
      End Get
    End Property

    <DisplayNameAttribute("ProductionPK")> _
    Public ReadOnly Property ProductionPK As String
      Get
        Return IIf(String.IsNullOrEmpty(ProductionRefNo), ProductionID, ProductionRefNo)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROProductionScheduleDetailListProperty As PropertyInfo(Of ROProductionScheduleDetailList) = RegisterProperty(Of ROProductionScheduleDetailList)(Function(c) c.ROProductionScheduleDetailList, "RO Production Schedule Detail List")

    Public ReadOnly Property ROProductionScheduleDetailList() As ROProductionScheduleDetailList
      Get
        If GetProperty(ROProductionScheduleDetailListProperty) Is Nothing Then
          LoadProperty(ROProductionScheduleDetailListProperty, ROProductionScheduleDetailList.NewROProductionScheduleDetailList())
        End If
        Return GetProperty(ROProductionScheduleDetailListProperty)
      End Get
    End Property

    Public Shared ROProductionScheduleTxTimelineListProperty As PropertyInfo(Of ROProductionScheduleTxTimelineList) = RegisterProperty(Of ROProductionScheduleTxTimelineList)(Function(c) c.ROProductionScheduleTxTimelineList, "RO Production Schedule Tx Timeline List")

    Public ReadOnly Property ROProductionScheduleTxTimelineList() As ROProductionScheduleTxTimelineList
      Get
        If GetProperty(ROProductionScheduleTxTimelineListProperty) Is Nothing Then
          LoadProperty(ROProductionScheduleTxTimelineListProperty, ROProductionScheduleTxTimelineList.NewROProductionScheduleTxTimelineList())
        End If
        Return GetProperty(ROProductionScheduleTxTimelineListProperty)
      End Get
    End Property

    Public Shared ROProductionScheduleEnhancementListProperty As PropertyInfo(Of ROProductionScheduleEnhancementList) = RegisterProperty(Of ROProductionScheduleEnhancementList)(Function(c) c.ROProductionScheduleEnhancementList, "RO Production Schedule Enhancement List")

    Public ReadOnly Property ROProductionScheduleEnhancementList() As ROProductionScheduleEnhancementList
      Get
        If GetProperty(ROProductionScheduleEnhancementListProperty) Is Nothing Then
          LoadProperty(ROProductionScheduleEnhancementListProperty, ROProductionScheduleEnhancementList.NewROProductionScheduleEnhancementList())
        End If
        Return GetProperty(ROProductionScheduleEnhancementListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ProductionDescription

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROProductionSchedule(dr As SafeDataReader) As ROProductionSchedule

      Dim r As New ROProductionSchedule()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionIDProperty, .GetInt32(0))
        LoadProperty(EventStartProperty, .GetValue(1))
        LoadProperty(ProductionDescriptionProperty, .GetString(2))
        LoadProperty(ProductionVenueProperty, .GetString(3))
        LoadProperty(CityIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(CityProperty, .GetString(5))
        LoadProperty(CityCodeProperty, .GetString(6))
        LoadProperty(VehicleNameProperty, .GetString(7))
        LoadProperty(VehicleDescriptionProperty, .GetString(8))
        LoadProperty(HDRequiredIndProperty, .GetBoolean(9))
        LoadProperty(TotalCamerasProperty, .GetString(10))
        LoadProperty(CancelledDateProperty, .GetValue(11))
        LoadProperty(AdditionalMatchesProperty, .GetString(12))
        LoadProperty(SortIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(ProductionRefNoProperty, .GetString(14))
        LoadProperty(ProductionMeetingProperty, .GetString(15))
        LoadProperty(FacilityChecksProperty, .GetString(16))
        LoadProperty(CommentatorMeetingProperty, .GetString(17))
        LoadProperty(BuildUpProperty, .GetString(18))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
        LoadProperty(ProductionCallTimeProperty, .GetString(20))
        LoadProperty(TalentCallTimeProperty, .GetString(21))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace