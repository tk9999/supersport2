﻿' Generated 17 Apr 2015 12:41 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Reports

  <Serializable()> _
  Public Class ROVehicleEquipmentSchedule
    Inherits SingularReadOnlyBase(Of ROVehicleEquipmentSchedule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared CityProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.City, "City")
    ''' <summary>
    ''' Gets the City value
    ''' </summary>
    <Display(Name:="City", Description:="")>
  Public ReadOnly Property City() As String
      Get
        Return GetProperty(CityProperty)
      End Get
    End Property

    Public Shared CityCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CityCode, "City Code")
    ''' <summary>
    ''' Gets the City Code value
    ''' </summary>
    <Display(Name:="City Code", Description:="")>
  Public ReadOnly Property CityCode() As String
      Get
        Return GetProperty(CityCodeProperty)
      End Get
    End Property

    Public Shared ItemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Item, "Item")
    ''' <summary>
    ''' Gets the Item value
    ''' </summary>
    <Display(Name:="Item", Description:="")>
  Public ReadOnly Property Item() As String
      Get
        Return GetProperty(ItemProperty)
      End Get
    End Property

    Public Shared ItemDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ItemDescription, "Item Description")
    ''' <summary>
    ''' Gets the Item Description value
    ''' </summary>
    <Display(Name:="Item Description", Description:="")>
  Public ReadOnly Property ItemDescription() As String
      Get
        Return GetProperty(ItemDescriptionProperty)
      End Get
    End Property

    Public Shared ProductionDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionDescription, "Production Description")
    ''' <summary>
    ''' Gets the Production Description value
    ''' </summary>
    <Display(Name:="Production Description", Description:="")>
  Public ReadOnly Property ProductionDescription() As String
      Get
        Return GetProperty(ProductionDescriptionProperty)
      End Get
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Production Venue")
    ''' <summary>
    ''' Gets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="")>
  Public ReadOnly Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
  Public ReadOnly Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
  Public ReadOnly Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared TotalMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TotalMinutes, "Total Minutes")
    ''' <summary>
    ''' Gets the Total Minutes value
    ''' </summary>
    <Display(Name:="Total Minutes", Description:="")>
  Public ReadOnly Property TotalMinutes() As Integer
      Get
        Return GetProperty(TotalMinutesProperty)
      End Get
    End Property

    Public Shared ProductionTimelineTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTimelineTypeID, "Production Timeline Type")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Production Timeline Type", Description:="")>
  Public ReadOnly Property ProductionTimelineTypeID() As Integer
      Get
        Return GetProperty(ProductionTimelineTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionTimelineTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionTimelineType, "Production Timeline Type")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Production Timeline Type", Description:="")>
  Public ReadOnly Property ProductionTimelineType() As String
      Get
        Return GetProperty(ProductionTimelineTypeProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeID, "Production Type")
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
  Public ReadOnly Property ProductionTypeID() As Integer
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDEventManagerProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceIDEventManager, "Human Resource ID Event Manager")
    ''' <summary>
    ''' Gets the Human Resource ID Event Manager value
    ''' </summary>
    <Display(Name:="Human Resource ID Event Manager", Description:="")>
  Public ReadOnly Property HumanResourceIDEventManager() As Integer
      Get
        Return GetProperty(HumanResourceIDEventManagerProperty)
      End Get
    End Property

    Public Shared CancelIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CancelInd, "Cancel", False)
    ''' <summary>
    ''' Gets the Cancel value
    ''' </summary>
    <Display(Name:="Cancel", Description:="")>
  Public ReadOnly Property CancelInd() As Boolean
      Get
        Return GetProperty(CancelIndProperty)
      End Get
    End Property

    Public Shared HDRequiredIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HDRequiredInd, "HD Required", False)
    ''' <summary>
    ''' Gets the HD Required value
    ''' </summary>
    <Display(Name:="HD Required", Description:="")>
  Public ReadOnly Property HDRequiredInd() As Boolean
      Get
        Return GetProperty(HDRequiredIndProperty)
      End Get
    End Property

    Public Shared VehicleTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VehicleTypeID, "Vehicle Type")
    ''' <summary>
    ''' Gets the Vehicle Type value
    ''' </summary>
    <Display(Name:="Vehicle Type", Description:="")>
  Public ReadOnly Property VehicleTypeID() As Integer
      Get
        Return GetProperty(VehicleTypeIDProperty)
      End Get
    End Property

    Public Shared SuppliedIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SuppliedInd, "Supplied", False)
    ''' <summary>
    ''' Gets the Supplied value
    ''' </summary>
    <Display(Name:="Supplied", Description:="")>
  Public ReadOnly Property SuppliedInd() As Boolean
      Get
        Return GetProperty(SuppliedIndProperty)
      End Get
    End Property

    Public Shared ServiceIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ServiceInd, "Service", False)
    ''' <summary>
    ''' Gets the Service value
    ''' </summary>
    <Display(Name:="Service", Description:="")>
  Public ReadOnly Property ServiceInd() As Boolean
      Get
        Return GetProperty(ServiceIndProperty)
      End Get
    End Property

    Public Shared FacsStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.FacsStartDateTime, "Facs Start Date Time")
    ''' <summary>
    ''' Gets the Facs Start Date Time value
    ''' </summary>
    <Display(Name:="Facs Start Date Time", Description:="")>
  Public ReadOnly Property FacsStartDateTime As DateTime?
      Get
        Return GetProperty(FacsStartDateTimeProperty)
      End Get
    End Property

    Public Shared SortOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SortOrder, "Sort Order")
    ''' <summary>
    ''' Gets the Sort Order value
    ''' </summary>
    <Display(Name:="Sort Order", Description:="")>
  Public ReadOnly Property SortOrder() As Integer
      Get
        Return GetProperty(SortOrderProperty)
      End Get
    End Property

    <DisplayNameAttribute("Location")> _
    Public ReadOnly Property Location() As String
      Get
        Return GetProperty(ProductionVenueProperty) & " " & GetProperty(CityProperty)
      End Get
    End Property

    Public Shared TravelAndRigIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TravelAndRigInd, "Travel & Rig?", False)
    ''' <summary>
    ''' Gets the Sort Order value
    ''' </summary>
    ''' 
    <DisplayNameAttribute("Travel & Rig?")> _
    Public Property TravelAndRigInd() As Boolean
      Get
        Return GetProperty(TravelAndRigIndProperty)
      End Get
      Set(value As Boolean)
        LoadProperty(TravelAndRigIndProperty, value)
        If GetProperty(TravelAndRigIndProperty) Then
          LoadProperty(ProductionTimelineTypeProperty, "Trav/Rig")
        Else
          LoadProperty(ProductionTimelineTypeProperty, CommonData.Lists.ROProductionTimelineTypeList.Where(Function(c) CompareSafe(c.ProductionTimelineTypeID, ProductionTimelineTypeID)).FirstOrDefault.ProductionTimelineType) '.FindFast("ProductionTimelineTypeID", mProductionTimelineTypeID).ProductionTimelineType)
        End If
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.City

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROVehicleEquipmentSchedule(dr As SafeDataReader) As ROVehicleEquipmentSchedule

      Dim r As New ROVehicleEquipmentSchedule()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ProductionIDProperty, .GetInt32(0))
        LoadProperty(CityProperty, .GetString(1))
        LoadProperty(CityCodeProperty, .GetString(2))
        LoadProperty(ItemProperty, .GetString(3))
        LoadProperty(ItemDescriptionProperty, .GetString(4))
        LoadProperty(ProductionDescriptionProperty, .GetString(5))
        LoadProperty(ProductionVenueProperty, .GetString(6))
        LoadProperty(StartDateTimeProperty, .GetValue(7))
        LoadProperty(EndDateTimeProperty, .GetValue(8))
        LoadProperty(TotalMinutesProperty, .GetInt32(9))
        LoadProperty(ProductionTimelineTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(ProductionTimelineTypeProperty, .GetString(11))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(HumanResourceIDEventManagerProperty, .GetInt32(13))
        LoadProperty(CancelIndProperty, .GetBoolean(14))
        LoadProperty(HDRequiredIndProperty, .GetBoolean(15))
        LoadProperty(VehicleTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
        LoadProperty(SuppliedIndProperty, .GetBoolean(17))
        LoadProperty(ServiceIndProperty, .GetBoolean(18))
        LoadProperty(FacsStartDateTimeProperty, .GetValue(19))
        LoadProperty(SortOrderProperty, .GetInt32(20))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace