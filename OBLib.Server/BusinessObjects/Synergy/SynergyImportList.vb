﻿' Generated 08 Apr 2014 09:59 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Synergy.Importer.New

  <Serializable()> _
  Public Class SynergyImportList
    Inherits OBBusinessListBase(Of SynergyImportList, SynergyImport)

#Region " Business Methods "

    Public Function GetItem(SynergyImportID As Integer) As SynergyImport

      For Each child As SynergyImport In Me
        If child.SynergyImportID = SynergyImportID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Synergy Imports"

    End Function

    Public Function GetSynergyImportSchedule(SynergyImportScheduleID As Integer) As SynergyImportSchedule

      Dim obj As SynergyImportSchedule = Nothing
      For Each parent As SynergyImport In Me
        obj = parent.SynergyImportScheduleList.GetItem(SynergyImportScheduleID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property FetchLatestImport As Boolean = False

      Public Sub New(FetchLatestImport As Boolean)
        Me.FetchLatestImport = FetchLatestImport
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewSynergyImportList() As SynergyImportList

      Return New SynergyImportList()

    End Function

    Public Shared Sub BeginGetSynergyImportList(CallBack As EventHandler(Of DataPortalResult(Of SynergyImportList)))

      Dim dp As New DataPortal(Of SynergyImportList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetSynergyImportList() As SynergyImportList

      Return DataPortal.Fetch(Of SynergyImportList)(New Criteria())

    End Function

    Public Shared Function GetSynergyImportList(FetchLatestImport As Boolean) As SynergyImportList

      Return DataPortal.Fetch(Of SynergyImportList)(New Criteria(FetchLatestImport))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SynergyImport.GetSynergyImport(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SynergyImport = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.SynergyImportID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.SynergyImportScheduleList.RaiseListChangedEvents = False
          parent.SynergyImportScheduleList.Add(SynergyImportSchedule.GetSynergyImportSchedule(sdr))
          parent.SynergyImportScheduleList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As SynergyImport In Me
        child.CheckRules()
        For Each SynergyImportSchedule As SynergyImportSchedule In child.SynergyImportScheduleList
          SynergyImportSchedule.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcs.getSynergyImportList"
            cm.Parameters.AddWithValue("@FetchLatestImport", crit.FetchLatestImport)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SynergyImport In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SynergyImport In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace