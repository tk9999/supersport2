﻿' Generated 22 Dec 2016 07:40 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class RemovedSchedule
    Inherits OBBusinessBase(Of RemovedSchedule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SynergyImportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SynergyImportID, "Synergy Import")
    ''' <summary>
    ''' Gets and sets the Synergy Import value
    ''' </summary>
    <Display(Name:="Synergy Import", Description:="")>
    Public Property SynergyImportID() As Integer
      Get
        Return GetProperty(SynergyImportIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SynergyImportIDProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref Number")
    ''' <summary>
    ''' Gets and sets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref Number", Description:="")>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared ScheduleNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ScheduleNumber, "Schedule Number")
    ''' <summary>
    ''' Gets and sets the Schedule Number value
    ''' </summary>
    <Display(Name:="Schedule Number", Description:="")>
    Public Property ScheduleNumber() As Integer
      Get
        Return GetProperty(ScheduleNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ScheduleNumberProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    'type = object because we can use anything as a parent
    Public Function GetParent() As Object

      Return CType(CType(Me.Parent, RemovedScheduleList).Parent, Object)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ScheduleNumberProperty)

    End Function

    Public Overrides Function ToString() As String

      Return ""

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRemovedSchedule() method.

    End Sub

    Public Shared Function NewRemovedSchedule() As RemovedSchedule

      Return DataPortal.CreateChild(Of RemovedSchedule)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetRemovedSchedule(dr As SafeDataReader) As RemovedSchedule

      Dim s As New RemovedSchedule()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SynergyImportIDProperty, Singular.Misc.ZeroNothing(.GetInt32(0)))
          LoadProperty(GenRefNumberProperty, .GetInt64(1))
          LoadProperty(ScheduleNumberProperty, .GetInt32(2))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      'BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      Return Sub()
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
    End Sub

#End Region

  End Class

End Namespace