﻿' Generated 09 May 2016 07:44 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Synergy

  <Serializable()> _
  Public Class SynergyEventList
    Inherits OBBusinessListBase(Of SynergyEventList, SynergyEvent)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ImportedEventID As Integer) As SynergyEvent

      For Each child As SynergyEvent In Me
        If child.ImportedEventID = ImportedEventID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Singular.Paging.PageCriteria(Of Criteria)

      <Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), DisplayMember:="System", ValueMember:="SystemID"),
      Display(Name:="Sub-Dept"),
      Required(ErrorMessage:="Sub-Dept is required"),
      SetExpression("SynergyEventListCriteriaBO.SystemIDSet(self)")>
      Public Property SystemID As Integer?

      <Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", Source:=Singular.DataAnnotations.DropDownWeb.SourceType.CommonData, ThisFilterMember:="SystemID"),
      Display(Name:="Area"),
      Required(ErrorMessage:="Area is required"),
      SetExpression("SynergyEventListCriteriaBO.ProductionAreaIDSet(self)")>
      Public Property ProductionAreaID As Integer?

      <Required(ErrorMessage:="Start Date is required"),
      SetExpression("SynergyEventListCriteriaBO.StartDateSet(self)", , )>
      Public Property StartDate As DateTime?

      <Required(ErrorMessage:="End Date is required"),
      SetExpression("SynergyEventListCriteriaBO.EndDateSet(self)", , )>
      Public Property EndDate As DateTime?

      Public Shared LiveProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Live, "Live", True)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Live", Description:=""),
      SetExpression("SynergyEventListCriteriaBO.LiveSet(self)")>
      Public Property Live() As Boolean
        Get
          Return ReadProperty(LiveProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(LiveProperty, Value)
        End Set
      End Property

      Public Shared DelayedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Delayed, "Delayed", True)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Delayed", Description:=""),
      SetExpression("SynergyEventListCriteriaBO.DelayedSet(self)")>
      Public Property Delayed() As Boolean
        Get
          Return ReadProperty(DelayedProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(DelayedProperty, Value)
        End Set
      End Property

      Public Shared PremierProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Premier, "Premier", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Premier", Description:=""),
      SetExpression("SynergyEventListCriteriaBO.PremierSet(self)")>
      Public Property Premier() As Boolean
        Get
          Return ReadProperty(PremierProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(PremierProperty, Value)
        End Set
      End Property

      Public Shared GenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNo, "Gen Ref No", Nothing)
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Gen Ref No", Description:=""),
      SetExpression("SynergyEventListCriteriaBO.GenRefNoSet(self)", , 250)>
      Public Property GenRefNo() As Int64?
        Get
          Return ReadProperty(GenRefNoProperty)
        End Get
        Set(ByVal Value As Int64?)
          LoadProperty(GenRefNoProperty, Value)
        End Set
      End Property

      Public Shared GenRefNoStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenRefNoString, "Gen Ref No", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Gen Ref No", Description:=""),
      TextField(False, , , ),
      SetExpression("SynergyEventListCriteriaBO.GenRefNoStringSet(self)", , 250)>
      Public Property GenRefNoString() As String
        Get
          Return ReadProperty(GenRefNoStringProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(GenRefNoStringProperty, Value)
        End Set
      End Property

      Public Shared PrimaryChannelsOnlyProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryChannelsOnly, "Primary Only", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Primary Channels Only?"),
      SetExpression("SynergyEventListCriteriaBO.PrimaryChannelsOnlySet(self)", , )>
      Public Property PrimaryChannelsOnly() As Boolean
        Get
          Return ReadProperty(PrimaryChannelsOnlyProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(PrimaryChannelsOnlyProperty, Value)
        End Set
      End Property

      Public Shared AllChannelsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AllChannels, "All Channels", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="All Channels"),
      SetExpression("SynergyEventListCriteriaBO.AllChannelsSet(self)", , )>
      Public Property AllChannels() As Boolean
        Get
          Return ReadProperty(AllChannelsProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(AllChannelsProperty, Value)
        End Set
      End Property

      Public Shared SelectedChannelsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedChannelsDisplay, "Selected Channels", "Select Channels")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Channels")>
      Public Property SelectedChannelsDisplay() As String
        Get
          Return ReadProperty(SelectedChannelsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SelectedChannelsProperty, Value)
        End Set
      End Property

      Public Shared SelectedChannelIDsXMLProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedChannelIDsXML, "Selected Channels", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Channels")>
      Public Property SelectedChannelIDsXML() As String
        Get
          Return ReadProperty(SelectedChannelIDsXMLProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SelectedChannelIDsXMLProperty, Value)
        End Set
      End Property

      Public Shared GenreProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Genre, "Genre", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Genre"),
      TextField(False, , , ),
      SetExpression("SynergyEventListCriteriaBO.GenreSet(self)", , 250)>
      Public Property Genre() As String
        Get
          Return ReadProperty(GenreProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(GenreProperty, Value)
        End Set
      End Property

      Public Shared SeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Series, "Series", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Series"),
      TextField(False, , , ),
      SetExpression("SynergyEventListCriteriaBO.SeriesSet(self)", , 250)>
      Public Property Series() As String
        Get
          Return ReadProperty(SeriesProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SeriesProperty, Value)
        End Set
      End Property

      Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Title"),
      TextField(False, , , ),
      SetExpression("SynergyEventListCriteriaBO.TitleSet(self)", , 250)>
      Public Property Title() As String
        Get
          Return ReadProperty(TitleProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(TitleProperty, Value)
        End Set
      End Property

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Keyword, "Keyword", "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Keyword"),
      TextField(False, , , ),
      SetExpression("SynergyEventListCriteriaBO.KeywordSet(self)", , 250)>
      Public Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Shared OBOnlyProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBOnly, "OB Only", False)
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="OB Only")>
      Public Property OBOnly() As Boolean
        Get
          Return ReadProperty(OBOnlyProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(OBOnlyProperty, Value)
        End Set
      End Property

      Public Shared IsProcessingProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsProcessing, "IsProcessing", False)
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="IsProcessing")>
      Public Property IsProcessing() As Boolean
        Get
          Return ReadProperty(IsProcessingProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(IsProcessingProperty, Value)
        End Set
      End Property

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?,
                     StartDate As DateTime?, EndDate As DateTime?)

        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.StartDate = StartDate
        Me.EndDate = EndDate

      End Sub

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?, GenRefNumber As Int64?,
                     StartDate As Date?, EndDate As Date?, Live As Boolean, Delayed As Boolean, Premier As Boolean,
                     PrimaryChannelsOnly As Boolean)
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.GenRefNo = GenRefNumber
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.Live = Live
        Me.Delayed = Delayed
        Me.Premier = Premier
        Me.PrimaryChannelsOnly = PrimaryChannelsOnly
        Me.PageNo = 1
        Me.PageSize = 1000
        Me.SortColumn = ""
        Me.SortAsc = True
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewSynergyEventList() As SynergyEventList

      Return New SynergyEventList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSynergyEventList() As SynergyEventList

      Return DataPortal.Fetch(Of SynergyEventList)(New Criteria())

    End Function

    Public Shared Function GetSynergyEventList(SystemID As Integer?, ProductionAreaID As Integer?, GenRefNumber As Int64?,
                                               StartDate As Date?, EndDate As Date?, Live As Boolean, Delayed As Boolean, Premier As Boolean,
                                               PrimaryChannelsOnly As Boolean) As SynergyEventList

      Return DataPortal.Fetch(Of SynergyEventList)(New Criteria(SystemID, ProductionAreaID, GenRefNumber, StartDate, EndDate, Live, Delayed, Premier, PrimaryChannelsOnly))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      While sdr.Read
        Me.Add(SynergyEvent.GetSynergyEvent(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'Dim parent As SynergyEvent = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.ImportedEventID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.SynergyEventChannelList.RaiseListChangedEvents = False
      '    parent.SynergyEventChannelList.Add(SynergyEventChannel.GetSynergyEventChannel(sdr))
      '    parent.SynergyEventChannelList.RaiseListChangedEvents = True
      '  End While
      'End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSynergyEventList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@GenRefNo", NothingDBNull(crit.GenRefNo))
            cm.Parameters.AddWithValue("@Live", crit.Live)
            cm.Parameters.AddWithValue("@Delayed", crit.Delayed)
            cm.Parameters.AddWithValue("@Premier", crit.Premier)
            cm.Parameters.AddWithValue("@PrimaryChannelsOnly", crit.PrimaryChannelsOnly)
            cm.Parameters.AddWithValue("@SelectedChannelIDs", Strings.MakeEmptyDBNull(crit.SelectedChannelIDsXML))
            cm.Parameters.AddWithValue("@Genre", Strings.MakeEmptyDBNull(crit.Genre))
            cm.Parameters.AddWithValue("@Series", Strings.MakeEmptyDBNull(crit.Series))
            cm.Parameters.AddWithValue("@Title", Strings.MakeEmptyDBNull(crit.Title))
            cm.Parameters.AddWithValue("@GenRefNoString", Strings.MakeEmptyDBNull(crit.GenRefNoString))
            cm.Parameters.AddWithValue("@OBOnly", crit.OBOnly)
            cm.Parameters.AddWithValue("@Keyword", Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace