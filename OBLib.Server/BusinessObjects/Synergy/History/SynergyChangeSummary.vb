﻿' Generated 22 Dec 2016 07:40 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class SynergyChangeSummary
    Inherits OBBusinessBase(Of SynergyChangeSummary)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SynergyChangeSummaryBO.SynergyChangeSummaryBOToString(self)")

    Public Shared SynergyImportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SynergyImportID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property SynergyImportID() As Integer
      Get
        Return GetProperty(SynergyImportIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SynergyImportIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SynergyScheduleListProperty As PropertyInfo(Of SynergyScheduleList) = RegisterProperty(Of SynergyScheduleList)(Function(c) c.SynergyScheduleList, "Synergy Schedule List")
    Public ReadOnly Property SynergyScheduleList() As SynergyScheduleList
      Get
        If GetProperty(SynergyScheduleListProperty) Is Nothing Then
          LoadProperty(SynergyScheduleListProperty, Synergy.SynergyScheduleList.NewSynergyScheduleList())
        End If
        Return GetProperty(SynergyScheduleListProperty)
      End Get
    End Property

    Public Shared RemovedScheduleListProperty As PropertyInfo(Of RemovedScheduleList) = RegisterProperty(Of RemovedScheduleList)(Function(c) c.RemovedScheduleList, "RemovedScheduleList")
    Public ReadOnly Property RemovedScheduleList() As RemovedScheduleList
      Get
        If GetProperty(RemovedScheduleListProperty) Is Nothing Then
          LoadProperty(RemovedScheduleListProperty, Synergy.RemovedScheduleList.NewRemovedScheduleList())
        End If
        Return GetProperty(RemovedScheduleListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SynergyImportIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SynergyImportID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "S Allocator")
        Else
          Return String.Format("Blank {0}", "S Allocator")
        End If
      Else
        Return Me.SynergyImportID.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSynergyChangeSummary() method.

    End Sub

    Public Shared Function NewSynergyChangeSummary() As SynergyChangeSummary

      Return DataPortal.CreateChild(Of SynergyChangeSummary)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSynergyChangeSummary(dr As SafeDataReader) As SynergyChangeSummary

      Dim s As New SynergyChangeSummary()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SynergyImportIDProperty, .GetInt32(0))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      'BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      'AddPrimaryKeyParam(cm, ParentIDProperty)
      'cm.Parameters.AddWithValue("@StartDate", StartDate)
      'cm.Parameters.AddWithValue("@EndDate", EndDate)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SynergyImportIDProperty, cm.Parameters("@SynergyImportID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      'UpdateChild(GetProperty(SAllChannelListProperty))
      'UpdateChild(GetProperty(SAllTimeRangeListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      'cm.Parameters.AddWithValue("@SynergyImportID", GetProperty(SynergyImportIDProperty))
    End Sub

#End Region

  End Class

End Namespace