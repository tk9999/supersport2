﻿' Generated 22 Dec 2016 07:40 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class SynergyChangeSummaryList
    Inherits OBBusinessListBase(Of SynergyChangeSummaryList, SynergyChangeSummary)

#Region " Business Methods "

    Public Function GetItem(SynergyImportID As Integer) As SynergyChangeSummary

      For Each child As SynergyChangeSummary In Me
        If child.SynergyImportID = SynergyImportID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property SynergyImportID As Integer?

      Public Sub New(SynergyImportID)
        Me.SynergyImportID = SynergyImportID
      End Sub

    End Class

    Public Shared Function NewSynergyChangeSummaryList() As SynergyChangeSummaryList

      Return New SynergyChangeSummaryList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSynergyChangeSummaryList(SynergyImportID As Integer) As SynergyChangeSummaryList

      Return DataPortal.Fetch(Of SynergyChangeSummaryList)(New Criteria(SynergyImportID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SynergyChangeSummary.GetSynergyChangeSummary(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SynergyChangeSummary = Me(0)
      If sdr.NextResult() Then
        While sdr.Read
          parent.SynergyScheduleList.RaiseListChangedEvents = False
          parent.SynergyScheduleList.Add(SynergySchedule.GetSynergySchedule(sdr))
          parent.SynergyScheduleList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          parent.RemovedScheduleList.RaiseListChangedEvents = False
          parent.RemovedScheduleList.Add(RemovedSchedule.GetRemovedSchedule(sdr))
          parent.RemovedScheduleList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSynergyChangeSummaryList"
            cm.Parameters.AddWithValue("@SynergyImportID", NothingDBNull(crit.SynergyImportID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace