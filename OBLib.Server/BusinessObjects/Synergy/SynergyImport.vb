﻿' Generated 08 Apr 2014 09:59 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.SynergyWebService

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Synergy.Importer.New

  <Serializable()> _
  Public Class SynergyImport
    Inherits OBBusinessBase(Of SynergyImport)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SynergyImportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SynergyImportID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    ''' 
    <Key()>
    Public ReadOnly Property SynergyImportID() As Integer
      Get
        Return GetProperty(SynergyImportIDProperty)
      End Get
    End Property
    '<Display(AutoGenerateField:=False), Key()>

    Public Shared ImportDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ImportDateTime, "Import Date Time")
    ''' <summary>
    ''' Gets and sets the Import Date Time value
    ''' </summary>
    <Display(Name:="Import Date Time", Description:=""),
    Required(ErrorMessage:="Import Date Time required")>
    Public Property ImportDateTime As DateTime?
      Get
        Return GetProperty(ImportDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ImportDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ImportedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ImportedByUserID, "Imported By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Imported By User value
    ''' </summary>
    <Display(Name:="Imported By User", Description:=""),
    Required(ErrorMessage:="Imported By User required")>
    Public Property ImportedByUserID() As Integer?
      Get
        Return GetProperty(ImportedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ImportedByUserIDProperty, Value)
      End Set
    End Property

    Public Shared ImportedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImportedBy, "Imported By")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Imported By", Description:="")>
    Public Property ImportedBy() As String
      Get
        Return GetProperty(ImportedByProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ImportedByProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Import Date Time value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDate As DateTime?
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the Import Date Time value
    ''' </summary>
    <Display(Name:="End Date", Description:=""),
    Required(ErrorMessage:="End Date required")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared ChangeCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ChangeCount, "", 0)
    ''' <summary>
    ''' Gets and sets the Imported By User value
    ''' </summary>
    <Display(Name:="")>
    Public Property ChangeCount() As Integer
      Get
        Return GetProperty(ChangeCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ChangeCountProperty, Value)
      End Set
    End Property

    'Private mGeneratedID As Integer = 0
    'Public Property GeneratedID As Integer
    '  Get
    '    Return mGeneratedID
    '  End Get
    '  Set(value As Integer)
    '    mGeneratedID = value
    '  End Set
    'End Property

#End Region

#Region " Child Lists "

    Public Shared SynergyImportScheduleListProperty As PropertyInfo(Of SynergyImportScheduleList) = RegisterProperty(Of SynergyImportScheduleList)(Function(c) c.SynergyImportScheduleList, "Synergy Import Schedule List")
    Public ReadOnly Property SynergyImportScheduleList() As SynergyImportScheduleList
      Get
        If GetProperty(SynergyImportScheduleListProperty) Is Nothing Then
          LoadProperty(SynergyImportScheduleListProperty, Synergy.Importer.[New].SynergyImportScheduleList.NewSynergyImportScheduleList())
        End If
        Return GetProperty(SynergyImportScheduleListProperty)
      End Get
    End Property

    Public Property ScheduleVOList As New List(Of ScheduleVO)

    Public Property SponsorshipList As New List(Of SSWSD.SponsorshipItem)

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SynergyImportIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SynergyImportID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Synergy Import")
        Else
          Return String.Format("Blank {0}", "Synergy Import")
        End If
      Else
        Return Me.SynergyImportID.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"SynergyImportSchedules"}
      End Get
    End Property

    Private Function CreateScheduleTableParameter() As SqlParameter

      Dim RBTable As DataTable = CreateScheduleTableParameterValue()
      Dim ResourceBookingsParam As SqlParameter = New SqlParameter("@Schedules", System.Data.SqlDbType.Structured)
      ResourceBookingsParam.Value = RBTable
      Return ResourceBookingsParam

    End Function

    Private Function CreateScheduleTableParameterValue() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ChannelName", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ChannelShortName", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Duration", GetType(System.TimeSpan)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EventStatus", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EventStatusDesc", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("GenRefNumber", GetType(System.Int64)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Genre", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("GenreDesc", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("LiveDate", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("LiveTime", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Location", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("LocationCode", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("MediaPlatform", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProgramType", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ScheduleDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ScheduleEndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ScheduleNumber", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SeasonNumber", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Season", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SeriesNumber", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SeriesTitle", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Title", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Venue", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("VenueCode", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Country", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IsTbc", GetType(System.Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ParentGenRefNumber", GetType(System.Int64)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IsBuildUp", GetType(System.Boolean)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("IsWrapUp", GetType(System.Boolean)).ColumnMapping = MappingType.Attribute


      Dim rowList As New List(Of DataRow)
      For Each rb As ScheduleVO In ScheduleVOList
        'Dim dur As DateTime = New DateTime(1970, 1, 1, rb.Duration.Hours, rb.Duration.Minutes, rb.Duration.Seconds)
        Dim row As DataRow = RBTable.NewRow
        row("ChannelName") = rb.ChannelName
        row("ChannelShortName") = rb.ChannelShortName
        If IsNullNothing(rb.Duration) Then
          row("Duration") = ""
        Else
          row("Duration") = NothingDBNull(rb.Duration)
        End If
        row("EventStatus") = rb.EventStatus
        row("EventStatusDesc") = rb.EventStatusDesc
        row("GenRefNumber") = rb.GenRefNumber
        row("Genre") = rb.Genre
        row("GenreDesc") = rb.GenreDesc
        If IsNullNothing(rb.LiveDate) Then
          row("LiveDate") = DBNull.Value
        Else
          Dim dt As Date = Nothing
          Dim dtString As String = New Csla.SmartDate(rb.LiveDate).Date.ToString("dd-MMM-yyy HH:mm")
          DateTime.TryParse(dtString, dt)
          If dt.Year < 1753 Then
            row("LiveDate") = DBNull.Value
          Else
            row("LiveDate") = New Csla.SmartDate(rb.LiveDate).Date.ToString("dd-MMM-yyy HH:mm")
          End If
        End If
        row("LiveTime") = ""
        row("Location") = IsNull(rb.Location, "")
        row("LocationCode") = IsNull(rb.LocationCode, "")
        row("MediaPlatform") = rb.MediaPlatform
        row("ProgramType") = rb.ProgramType
        row("ScheduleDateTime") = New Csla.SmartDate(rb.ScheduleDateTime).Date.ToString("dd-MMM-yyy HH:mm")
        row("ScheduleEndDateTime") = New Csla.SmartDate(rb.ScheduleEndDateTime).Date.ToString("dd-MMM-yyy HH:mm")
        row("ScheduleNumber") = rb.ScheduleNumber
        row("SeasonNumber") = 0
        row("Season") = rb.Season
        row("SeriesNumber") = rb.SeriesNumber
        row("SeriesTitle") = rb.SeriesTitle
        row("Title") = rb.Title
        row("Venue") = rb.Venue
        row("VenueCode") = rb.VenueCode
        row("Country") = IsNull(rb.Country, "")
        row("IsTBC") = rb.IsTbc
        row("ParentGenRefNumber") = ZeroDBNull(rb.ParentGenRefNumber)
        row("IsBuildUp") = rb.IsBuildUp
        row("IsWrapUp") = rb.IsWrapUp
        RBTable.Rows.Add(row)
      Next
      Return RBTable

    End Function

    Private Function CreateSponsorshipTableParameter() As SqlParameter

      Dim RBTable As DataTable = CreateSponsorshipTableParameterValue()
      Dim ResourceBookingsParam As SqlParameter = New SqlParameter("@Sponsorship", System.Data.SqlDbType.Structured)
      ResourceBookingsParam.Value = RBTable
      Return ResourceBookingsParam

    End Function

    Private Function CreateSponsorshipTableParameterValue() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("Channel", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Comments", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Created", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("CreatedBy", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Description", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Id", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Sport", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SynRefId", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SynEventId", GetType(System.Int64)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As SSWSD.SponsorshipItem In SponsorshipList
        'Dim dur As DateTime = New DateTime(1970, 1, 1, rb.Duration.Hours, rb.Duration.Minutes, rb.Duration.Seconds)
        Dim row As DataRow = RBTable.NewRow
        row("Channel") = IIf(IsNullNothing(rb.Channel), "", rb.Channel)
        row("Comments") = IIf(IsNullNothing(rb.Comments), "", rb.Comments)
        row("Created") = New Csla.SmartDate(rb.Created).Date.ToString("dd-MMM-yyy HH:mm")
        row("CreatedBy") = IIf(IsNullNothing(rb.CreatedBy), "", rb.CreatedBy)
        row("Description") = IIf(IsNullNothing(rb.Description), "", rb.Description)
        row("Id") = rb.Id
        row("Sport") = IIf(IsNullNothing(rb.Sport), "", rb.Sport)
        row("SynRefId") = rb.SynRefId
        row("SynEventId") = rb.SynEventId
        RBTable.Rows.Add(row)
      Next
      Return RBTable

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSynergyImport() method.

    End Sub

    Public Shared Function NewSynergyImport() As SynergyImport

      Return DataPortal.CreateChild(Of SynergyImport)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSynergyImport(dr As SafeDataReader) As SynergyImport

      Dim s As New SynergyImport()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SynergyImportIDProperty, .GetInt32(0))
          LoadProperty(ImportDateTimeProperty, .GetValue(1))
          LoadProperty(ImportedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ImportedByProperty, .GetString(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSynergyImport"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updSynergyImport"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSynergyImportID As SqlParameter = .Parameters.Add("@SynergyImportID", SqlDbType.Int)
          paramSynergyImportID.Value = GetProperty(SynergyImportIDProperty)
          Dim paramChangeCount As SqlParameter = .Parameters.Add("@ChangeCount", SqlDbType.Int)
          paramChangeCount.Value = GetProperty(ChangeCountProperty)

          If Me.IsNew Then
            paramSynergyImportID.Direction = ParameterDirection.Output
            paramChangeCount.Direction = ParameterDirection.Output
          End If

          .Parameters.AddWithValue("@ImportDateTime", (New SmartDate(GetProperty(ImportDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ImportedByUserID", GetProperty(ImportedByUserIDProperty))
          .Parameters.Add(CreateScheduleTableParameter)
          .Parameters.Add(CreateSponsorshipTableParameter)
          .Parameters.AddWithValue("@StartDate", (New SmartDate(GetProperty(StartDateProperty))).DBValue)
          .Parameters.AddWithValue("@EndDate", (New SmartDate(GetProperty(EndDateProperty))).DBValue)
          .CommandTimeout = 0
          .ExecuteNonQuery()
          If Me.IsNew Then
            LoadProperty(SynergyImportIDProperty, paramSynergyImportID.Value)
            LoadProperty(ChangeCountProperty, paramChangeCount.Value)
          End If
          '' update child objects
          'If GetProperty(SynergyImportScheduleListProperty) IsNot Nothing Then
          '  Me.SynergyImportScheduleList.Update()
          'End If
          MarkOld()
        End With
      Else
        '' update child objects
        'If GetProperty(SynergyImportScheduleListProperty) IsNot Nothing Then
        '  Me.SynergyImportScheduleList.Update()
        'End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delSynergyImport"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SynergyImportID", GetProperty(SynergyImportIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Public Sub SendChangesEmail(ReportBytes As Byte())

      Dim cmdProc As New Singular.CommandProc("cmdProcs.cmdEmailSynergyChangesSimple")
      cmdProc.Parameters.AddWithValue("@ReportBytes", ReportBytes).SqlType = SqlDbType.VarBinary
      cmdProc.Parameters.AddWithValue("@EmailID", 0).Direction = ParameterDirection.Output
      cmdProc.Parameters.AddWithValue("@EmailAttachmentID", 0).Direction = ParameterDirection.Output
      cmdProc.Parameters.AddWithValue("@AttachmentDocumentID", 0).Direction = ParameterDirection.Output

      cmdProc.UseTransaction = True
      cmdProc.FetchType = CommandProc.FetchTypes.DataRow
      cmdProc = cmdProc.Execute(0)
      Dim dr As DataRow = cmdProc.DataRow
      Dim emailID As Object = dr(0)
      Dim emailAttachmentID As Object = dr(1)
      Dim attachmentDocumentID As Object = dr(2)
      Dim x As Object = Nothing

    End Sub

    Shared Sub PostChanges(SynergyImportID As String)

      SoberHubClient.AfterSynergyImportRun(SynergyImportID)

    End Sub

  End Class

End Namespace