﻿' Generated 30 Sep 2017 17:59 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.HR.ReadOnly
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()>
  Public Class AutomaticImportCrew
    Inherits OBBusinessBase(Of AutomaticImportCrew)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return AutomaticImportCrewBO.AutomaticImportCrewBOToString(self)")

    Public Shared AutomaticImportCrewIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AutomaticImportCrewID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AutomaticImportCrewID() As Integer
      Get
        Return GetProperty(AutomaticImportCrewIDProperty)
      End Get
    End Property

    Public Shared AutomaticImportIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AutomaticImportID, "Automatic Import", Nothing)
    ''' <summary>
    ''' Gets the Automatic Import value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property AutomaticImportID() As Integer?
      Get
        Return GetProperty(AutomaticImportIDProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    Required(ErrorMessage:="Discipline required"),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplineListSelect),
                BeforeFetchJS:="AutomaticImportCrewBO.setDisciplineIDCriteriaBeforeRefresh",
                PreFindJSFunction:="AutomaticImportCrewBO.triggerDisciplineIDAutoPopulate",
                AfterFetchJS:="AutomaticImportCrewBO.afterDisciplineIDRefreshAjax",
                OnItemSelectJSFunction:="AutomaticImportCrewBO.onDisciplineIDSelected",
                LookupMember:="Discipline", DisplayMember:="Discipline", ValueMember:="DisciplineID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Discipline"})>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionTypeID, "Position Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:=""),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplinePositionTypeListSelect),
                BeforeFetchJS:="AutomaticImportCrewBO.setPositionTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="AutomaticImportCrewBO.triggerPositionTypeIDAutoPopulate",
                AfterFetchJS:="AutomaticImportCrewBO.afterPositionTypeIDRefreshAjax",
                OnItemSelectJSFunction:="AutomaticImportCrewBO.onPositionTypeIDSelected",
                LookupMember:="PositionType", DisplayMember:="PositionType", ValueMember:="PositionTypeID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Position"})>
    Public Property PositionTypeID() As Integer?
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared PositionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionType, "Position Type", "")
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
    Public Property PositionType() As String
      Get
        Return GetProperty(PositionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionTypeProperty, Value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:=""),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplinePositionTypeListSelect),
                BeforeFetchJS:="AutomaticImportCrewBO.setPositionIDCriteriaBeforeRefresh",
                PreFindJSFunction:="AutomaticImportCrewBO.triggerPositionIDAutoPopulate",
                AfterFetchJS:="AutomaticImportCrewBO.afterPositionIDRefreshAjax",
                OnItemSelectJSFunction:="AutomaticImportCrewBO.onPositionIDSelected",
                LookupMember:="Position", DisplayMember:="Position", ValueMember:="PositionID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Position"})>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position", "")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
     Required(ErrorMessage:="Human Resource is required"),
    DropDownWeb(GetType(ROHumanResourceAvailabilityList),
                BeforeFetchJS:="AutomaticImportCrewBO.setHumanResourceCriteriaBeforeRefresh",
                PreFindJSFunction:="AutomaticImportCrewBO.triggerHumanResourceAutoPopulate",
                AfterFetchJS:="AutomaticImportCrewBO.afterHumanResourceRefreshAjax",
                OnItemSelectJSFunction:="AutomaticImportCrewBO.onHumanResourceSelected",
                LookupMember:="HumanResource", DisplayMember:="HRName", ValueMember:="HumanResourceID",
                DropDownCssClass:="hr-availability-dropdown", DropDownColumns:={"HRName"},
                OnCellCreateFunction:="AutomaticImportCrewBO.onCellCreate")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As AutomaticImport

      Return CType(CType(Me.Parent, AutomaticImportCrewList).Parent, AutomaticImport)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AutomaticImportCrewIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Automatic Import Crew")
        Else
          Return String.Format("Blank {0}", "Automatic Import Crew")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAutomaticImportCrew() method.

    End Sub

    Public Shared Function NewAutomaticImportCrew() As AutomaticImportCrew

      Return DataPortal.CreateChild(Of AutomaticImportCrew)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetAutomaticImportCrew(dr As SafeDataReader) As AutomaticImportCrew

      Dim a As New AutomaticImportCrew()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AutomaticImportCrewIDProperty, .GetInt32(0))
          LoadProperty(AutomaticImportIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DisciplineProperty, .GetString(3))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(PositionProperty, .GetString(5))
          LoadProperty(PositionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(PositionTypeProperty, .GetString(7))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(HumanResourceProperty, .GetString(9))
          LoadProperty(CreatedByProperty, .GetInt32(10))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(11))
          LoadProperty(ModifiedByProperty, .GetInt32(12))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(13))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, AutomaticImportCrewIDProperty)

      cm.Parameters.AddWithValue("@AutomaticImportID", Me.GetParent().AutomaticImportID)
      cm.Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
      cm.Parameters.AddWithValue("@PositionID", Singular.Misc.NothingDBNull(GetProperty(PositionIDProperty)))
      cm.Parameters.AddWithValue("@PositionTypeID", Singular.Misc.NothingDBNull(GetProperty(PositionTypeIDProperty)))
      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(AutomaticImportCrewIDProperty, cm.Parameters("@AutomaticImportCrewID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@AutomaticImportCrewID", GetProperty(AutomaticImportCrewIDProperty))
    End Sub

#End Region

  End Class

End Namespace