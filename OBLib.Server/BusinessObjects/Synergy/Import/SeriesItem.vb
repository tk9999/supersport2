﻿' Generated 11 Feb 2016 00:12 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.Equipment
Imports OBLib.Equipment.ReadOnly
Imports System.Data.SqlClient

Namespace Synergy

  <Serializable()>
  Public Class SeriesItem
    Inherits OBBusinessBase(Of SeriesItem)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="IsExpanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared SeriesNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SeriesNumber, "SeriesNumber")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Series Number", Description:="")>
    Public Property SeriesNumber() As Integer
      Get
        Return GetProperty(SeriesNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SeriesNumberProperty, Value)
      End Set
    End Property

    Public Shared SeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Series, "Series")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Series", Description:="")>
    Public Property Series() As String
      Get
        Return GetProperty(SeriesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SeriesProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LiveDate, "Live Date")
    ''' <summary>
    ''' Gets and sets the Live Date value
    ''' </summary>
    <Display(Name:="Live Date", Description:="")>
    Public Property LiveDate As DateTime?
      Get
        Return GetProperty(LiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LiveDateProperty, Value)
      End Set
    End Property

    Public Shared FirstChannelStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.FirstChannelStart, "First Channel Start")
    ''' <summary>
    ''' Gets and sets the First Channel Start value
    ''' </summary>
    <Display(Name:="First Channel Start", Description:=""),
    Required(ErrorMessage:="First Channel Start required")>
    Public Property FirstChannelStart As DateTime?
      Get
        Return GetProperty(FirstChannelStartProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(FirstChannelStartProperty, Value)
      End Set
    End Property

    Public Shared DefaultEquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DefaultEquipmentID, "Default Circuit")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Default Circuit", Description:=""),
     DropDownWeb(GetType(ROCircuitAvailabilityList),
                 BeforeFetchJS:="SeriesItemBO.setCircuitCriteriaBeforeRefresh",
                 PreFindJSFunction:="SeriesItemBO.triggerCircuitAutoPopulate",
                 AfterFetchJS:="SeriesItemBO.afterCircuitRefreshAjax",
                 OnItemSelectJSFunction:="SeriesItemBO.onCircuitSelected",
                 LookupMember:="EquipmentName", DisplayMember:="EquipmentName", ValueMember:="ResourceID",
                 DropDownCssClass:="circuit-dropdown", DropDownColumns:={"EquipmentName", "IsAvailableString"}),
     SetExpression("SeriesItemBO.EquipmentIDSet(self)")>
    Public Property DefaultEquipmentID() As Integer?
      Get
        Return GetProperty(DefaultEquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DefaultEquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentName, "Circuit")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Circuit", Description:="")>
    Public Property EquipmentName() As String
      Get
        Return GetProperty(EquipmentNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EquipmentNameProperty, Value)
      End Set
    End Property

    Public Shared DefaultResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DefaultResourceID, "Default Circuit")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Default Circuit", Description:="")>
    Public Property DefaultResourceID() As Integer?
      Get
        Return GetProperty(DefaultResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DefaultResourceIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared EventItemListProperty As PropertyInfo(Of EventItemList) = RegisterProperty(Of EventItemList)(Function(c) c.EventItemList, "EventItemList")

    Public ReadOnly Property EventItemList() As EventItemList
      Get
        If GetProperty(EventItemListProperty) Is Nothing Then
          LoadProperty(EventItemListProperty, OBLib.Synergy.EventItemList.NewEventItemList())
        End If
        Return GetProperty(EventItemListProperty)
      End Get
    End Property

    Public Shared EquipmentScheduleBasicListProperty As PropertyInfo(Of EquipmentScheduleBasicList) = RegisterProperty(Of EquipmentScheduleBasicList)(Function(c) c.EquipmentScheduleBasicList, "EquipmentScheduleBasicList")

    Public ReadOnly Property EquipmentScheduleBasicList() As EquipmentScheduleBasicList
      Get
        If GetProperty(EquipmentScheduleBasicListProperty) Is Nothing Then
          LoadProperty(EquipmentScheduleBasicListProperty, OBLib.Equipment.EquipmentScheduleBasicList.NewEquipmentScheduleBasicList())
        End If
        Return GetProperty(EquipmentScheduleBasicListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SeriesNumberProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Series.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Equipment Allocator Event")
        Else
          Return String.Format("Blank {0}", "Equipment Allocator Event")
        End If
      Else
        Return Me.Series
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSeriesItem() method.

    End Sub

    Public Shared Function NewSeriesItem() As SeriesItem

      Return DataPortal.CreateChild(Of SeriesItem)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSeriesItem(dr As SafeDataReader) As SeriesItem

      Dim e As New SeriesItem()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      'Using BypassPropertyChecks
      '  With sdr
      '    LoadProperty(VenueDayGroupProperty, .GetInt32(0))
      '    LoadProperty(GenRefNumberProperty, .GetInt64(1))
      '    LoadProperty(ImportedEventIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
      '    LoadProperty(GenreSeriesProperty, .GetString(3))
      '    LoadProperty(TitleProperty, .GetString(4))
      '    LoadProperty(HighlightsIndProperty, .GetBoolean(5))
      '    LoadProperty(LiveDateProperty, .GetValue(6))
      '    LoadProperty(FirstChannelStartProperty, .GetValue(7))
      '    LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
      '  End With
      'End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSeriesItem"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSeriesItem"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then

      '  With cm
      '    .CommandType = CommandType.StoredProcedure

      '    Dim paramVenueDayGroup As SqlParameter = .Parameters.Add("@VenueDayGroup", SqlDbType.Int)
      '    paramVenueDayGroup.Value = GetProperty(VenueDayGroupProperty)
      '    If Me.IsNew Then
      '      paramVenueDayGroup.Direction = ParameterDirection.Output
      '    End If
      '    .Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
      '    .Parameters.AddWithValue("@ImportedEventID", GetProperty(ImportedEventIDProperty))
      '    .Parameters.AddWithValue("@GenreSeries", GetProperty(GenreSeriesProperty))
      '    .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      '    .Parameters.AddWithValue("@HighlightsInd", GetProperty(HighlightsIndProperty))
      '    .Parameters.AddWithValue("@LiveDate", (New SmartDate(GetProperty(LiveDateProperty))).DBValue)
      '    .Parameters.AddWithValue("@FirstChannelStart", (New SmartDate(GetProperty(FirstChannelStartProperty))).DBValue)

      '    .ExecuteNonQuery()

      '    If Me.IsNew Then
      '      LoadProperty(VenueDayGroupProperty, paramVenueDayGroup.Value)
      '    End If
      '    ' update child objects
      '    If GetProperty(SeriesItemChannelListProperty) IsNot Nothing Then
      '      Me.SeriesItemChannelList.Update()
      '    End If
      '    MarkOld()
      '  End With
      'Else
      '  ' update child objects
      '  If GetProperty(SeriesItemChannelListProperty) IsNot Nothing Then
      '    Me.SeriesItemChannelList.Update()
      '  End If
      'End If

    End Sub

    Friend Sub DeleteSelf()

      '' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delSeriesItem"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@VenueDayGroup", GetProperty(VenueDayGroupProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace