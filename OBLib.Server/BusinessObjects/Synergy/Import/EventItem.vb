﻿' Generated 11 Feb 2016 00:12 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()>
  Public Class EventItem
    Inherits OBBusinessBase(Of EventItem)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="IsExpanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "GenRefNumber")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="GenRefNumber", Description:="")>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared GenreDescProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreDesc, "Genre")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Genre", Description:="")>
    Public Property GenreDesc() As String
      Get
        Return GetProperty(GenreDescProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreDescProperty, Value)
      End Set
    End Property

    Public Shared SeriesNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SeriesNumber, "SeriesNumber")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Series Number", Description:="")>
    Public Property SeriesNumber() As Integer
      Get
        Return GetProperty(SeriesNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SeriesNumberProperty, Value)
      End Set
    End Property

    Public Shared SeriesTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SeriesTitle, "Series")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Series", Description:="")>
    Public Property SeriesTitle() As String
      Get
        Return GetProperty(SeriesTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SeriesTitleProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LiveDate, "Live Start Time")
    ''' <summary>
    ''' Gets and sets the Live Date value
    ''' </summary>
    <Display(Name:="Live Start Time", Description:="")>
    Public Property LiveDate As DateTime?
      Get
        Return GetProperty(LiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LiveDateProperty, Value)
      End Set
    End Property

    Public Shared LiveEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LiveEndDate, "Live End Time")
    ''' <summary>
    ''' Gets and sets the Live End Date value
    ''' </summary>
    <Display(Name:="Live End Time", Description:="")>
    Public Property LiveEndDate As DateTime?
      Get
        Return GetProperty(LiveEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LiveEndDateProperty, Value)
      End Set
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:="")>
    Public Property StartTime As DateTime?
      Get
        Return GetProperty(StartTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartTimeProperty, Value)
      End Set
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public Property EndTime As DateTime?
      Get
        Return GetProperty(EndTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndTimeProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SynergyScheduleListProperty As PropertyInfo(Of SynergyScheduleList) = RegisterProperty(Of SynergyScheduleList)(Function(c) c.SynergyScheduleList, "SynergyScheduleList")

    Public ReadOnly Property SynergyScheduleList() As SynergyScheduleList
      Get
        If GetProperty(SynergyScheduleListProperty) Is Nothing Then
          LoadProperty(SynergyScheduleListProperty, OBLib.Synergy.SynergyScheduleList.NewSynergyScheduleList())
        End If
        Return GetProperty(SynergyScheduleListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(GenRefNumberProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Title.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Equipment Allocator Event")
        Else
          Return String.Format("Blank {0}", "Equipment Allocator Event")
        End If
      Else
        Return Me.Title
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEventItem() method.

    End Sub

    Public Shared Function NewEventItem() As EventItem

      Return DataPortal.CreateChild(Of EventItem)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEventItem(dr As SafeDataReader) As EventItem

      Dim e As New EventItem()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      'Using BypassPropertyChecks
      '  With sdr
      '    LoadProperty(VenueDayGroupProperty, .GetInt32(0))
      '    LoadProperty(GenRefNumberProperty, .GetInt64(1))
      '    LoadProperty(ImportedEventIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
      '    LoadProperty(GenreSeriesProperty, .GetString(3))
      '    LoadProperty(TitleProperty, .GetString(4))
      '    LoadProperty(HighlightsIndProperty, .GetBoolean(5))
      '    LoadProperty(LiveDateProperty, .GetValue(6))
      '    LoadProperty(FirstChannelStartProperty, .GetValue(7))
      '    LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
      '  End With
      'End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEventItem"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEventItem"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then

      '  With cm
      '    .CommandType = CommandType.StoredProcedure

      '    Dim paramVenueDayGroup As SqlParameter = .Parameters.Add("@VenueDayGroup", SqlDbType.Int)
      '    paramVenueDayGroup.Value = GetProperty(VenueDayGroupProperty)
      '    If Me.IsNew Then
      '      paramVenueDayGroup.Direction = ParameterDirection.Output
      '    End If
      '    .Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
      '    .Parameters.AddWithValue("@ImportedEventID", GetProperty(ImportedEventIDProperty))
      '    .Parameters.AddWithValue("@GenreSeries", GetProperty(GenreSeriesProperty))
      '    .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      '    .Parameters.AddWithValue("@HighlightsInd", GetProperty(HighlightsIndProperty))
      '    .Parameters.AddWithValue("@LiveDate", (New SmartDate(GetProperty(LiveDateProperty))).DBValue)
      '    .Parameters.AddWithValue("@FirstChannelStart", (New SmartDate(GetProperty(FirstChannelStartProperty))).DBValue)

      '    .ExecuteNonQuery()

      '    If Me.IsNew Then
      '      LoadProperty(VenueDayGroupProperty, paramVenueDayGroup.Value)
      '    End If
      '    ' update child objects
      '    If GetProperty(EventItemChannelListProperty) IsNot Nothing Then
      '      Me.EventItemChannelList.Update()
      '    End If
      '    MarkOld()
      '  End With
      'Else
      '  ' update child objects
      '  If GetProperty(EventItemChannelListProperty) IsNot Nothing Then
      '    Me.EventItemChannelList.Update()
      '  End If
      'End If

    End Sub

    Friend Sub DeleteSelf()

      '' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delEventItem"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@VenueDayGroup", GetProperty(VenueDayGroupProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace