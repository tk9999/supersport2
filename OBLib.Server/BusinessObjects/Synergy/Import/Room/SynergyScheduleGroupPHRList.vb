﻿' Generated 16 Jun 2016 14:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Synergy

  <Serializable()> _
  Public Class SynergyScheduleGroupPHRList
    Inherits OBBusinessListBase(Of SynergyScheduleGroupPHRList, SynergyScheduleGroupPHR)

#Region " Business Methods "

    Public Function GetItem(ProductionHRID As Integer) As SynergyScheduleGroupPHR

      For Each child As SynergyScheduleGroupPHR In Me
        If child.ProductionHRID = ProductionHRID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItemByHumanResourceID(HumanResourceID As Integer) As SynergyScheduleGroupPHR

      For Each child As SynergyScheduleGroupPHR In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewSynergyScheduleGroupPHRList() As SynergyScheduleGroupPHRList

      Return New SynergyScheduleGroupPHRList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace