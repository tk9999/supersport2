﻿' Generated 22 Dec 2016 07:40 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class SAllocator
    Inherits OBBusinessBase(Of SAllocator)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SAllocatorBO.SAllocatorBOToString(self)")

    Public Shared ParentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ParentID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property ParentID() As Integer
      Get
        Return GetProperty(ParentIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ParentIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:="")>
    Public Property StartDate As Date
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public Property EndDate As Date
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SynergyScheduleListProperty As PropertyInfo(Of SynergyScheduleList) = RegisterProperty(Of SynergyScheduleList)(Function(c) c.SynergyScheduleList, "Synergy Schedule List")
    Public ReadOnly Property SynergyScheduleList() As SynergyScheduleList
      Get
        If GetProperty(SynergyScheduleListProperty) Is Nothing Then
          LoadProperty(SynergyScheduleListProperty, Synergy.SynergyScheduleList.NewSynergyScheduleList())
        End If
        Return GetProperty(SynergyScheduleListProperty)
      End Get
    End Property

    'Public Shared SAllRoomScheduleListProperty As PropertyInfo(Of SAllRoomScheduleList) = RegisterProperty(Of SAllRoomScheduleList)(Function(c) c.SAllRoomScheduleList, "S All Room Schedule List")
    'Public ReadOnly Property SAllRoomScheduleList() As SAllRoomScheduleList
    '  Get
    '    If GetProperty(SAllRoomScheduleListProperty) Is Nothing Then
    '      LoadProperty(SAllRoomScheduleListProperty, Synergy.SAllRoomScheduleList.NewSAllRoomScheduleList())
    '    End If
    '    Return GetProperty(SAllRoomScheduleListProperty)
    '  End Get
    'End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ParentIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ParentID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "S Allocator")
        Else
          Return String.Format("Blank {0}", "S Allocator")
        End If
      Else
        Return Me.ParentID.ToString()
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSAllocator() method.

    End Sub

    Public Shared Function NewSAllocator() As SAllocator

      Return DataPortal.CreateChild(Of SAllocator)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSAllocator(dr As SafeDataReader) As SAllocator

      Dim s As New SAllocator()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ParentIDProperty, .GetInt32(0))
          LoadProperty(StartDateProperty, .GetValue(1))
          LoadProperty(EndDateProperty, .GetValue(2))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ParentIDProperty)

      cm.Parameters.AddWithValue("@StartDate", StartDate)
      cm.Parameters.AddWithValue("@EndDate", EndDate)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ParentIDProperty, cm.Parameters("@ParentID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      'UpdateChild(GetProperty(SAllChannelListProperty))
      'UpdateChild(GetProperty(SAllTimeRangeListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ParentID", GetProperty(ParentIDProperty))
    End Sub

#End Region

  End Class

End Namespace