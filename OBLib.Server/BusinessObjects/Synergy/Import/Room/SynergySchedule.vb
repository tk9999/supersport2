﻿' Generated 22 Dec 2016 07:40 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class SynergySchedule
    Inherits OBBusinessBase(Of SynergySchedule)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SynergyScheduleBO.SynergyScheduleBOToString(self)")

    Public Shared SynergyScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SynergyScheduleID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property SynergyScheduleID() As Integer
      Get
        Return GetProperty(SynergyScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SynergyScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared SynergyImportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SynergyImportID, "Synergy Import")
    ''' <summary>
    ''' Gets and sets the Synergy Import value
    ''' </summary>
    <Display(Name:="Synergy Import", Description:="")>
    Public Property SynergyImportID() As Integer
      Get
        Return GetProperty(SynergyImportIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SynergyImportIDProperty, Value)
      End Set
    End Property

    Public Shared ChannelNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelName, "Channel Name")
    ''' <summary>
    ''' Gets and sets the Channel Name value
    ''' </summary>
    <Display(Name:="Channel Name", Description:="")>
    Public Property ChannelName() As String
      Get
        Return GetProperty(ChannelNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelNameProperty, Value)
      End Set
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel Short Name")
    ''' <summary>
    ''' Gets the Channel Short Name value
    ''' </summary>
    Public Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelShortNameProperty, Value)
      End Set
    End Property

    Public Shared DurationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Duration, "Duration")
    ''' <summary>
    ''' Gets and sets the Duration value
    ''' </summary>
    <Display(Name:="Duration", Description:="")>
    Public Property Duration() As String
      Get
        Return GetProperty(DurationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DurationProperty, Value)
      End Set
    End Property

    Public Shared EventStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventStatus, "Event Status")
    ''' <summary>
    ''' Gets and sets the Event Status value
    ''' </summary>
    <Display(Name:="Event Status", Description:="")>
    Public Property EventStatus() As String
      Get
        Return GetProperty(EventStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventStatusProperty, Value)
      End Set
    End Property

    Public Shared EventStatusDescProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventStatusDesc, "Event Status Desc")
    ''' <summary>
    ''' Gets and sets the Event Status Desc value
    ''' </summary>
    <Display(Name:="Event Status Desc", Description:="")>
    Public Property EventStatusDesc() As String
      Get
        Return GetProperty(EventStatusDescProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventStatusDescProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref Number")
    ''' <summary>
    ''' Gets and sets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref Number", Description:="")>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared GenreProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Genre, "Genre")
    ''' <summary>
    ''' Gets and sets the Genre value
    ''' </summary>
    <Display(Name:="Genre", Description:="")>
    Public Property Genre() As String
      Get
        Return GetProperty(GenreProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreProperty, Value)
      End Set
    End Property

    Public Shared GenreDescProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreDesc, "Genre Desc")
    ''' <summary>
    ''' Gets and sets the Genre Desc value
    ''' </summary>
    <Display(Name:="Genre Desc", Description:="")>
    Public Property GenreDesc() As String
      Get
        Return GetProperty(GenreDescProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreDescProperty, Value)
      End Set
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.LiveDate, "Live Date")
    ''' <summary>
    ''' Gets and sets the Live Date value
    ''' </summary>
    <Display(Name:="Live Date", Description:="")>
    Public Property LiveDate As Date
      Get
        Return GetProperty(LiveDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(LiveDateProperty, Value)
      End Set
    End Property

    Public Shared LiveTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LiveTime, "Live Time")
    ''' <summary>
    ''' Gets and sets the Live Time value
    ''' </summary>
    <Display(Name:="Live Time", Description:="")>
    Public Property LiveTime() As String
      Get
        Return GetProperty(LiveTimeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LiveTimeProperty, Value)
      End Set
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationProperty, Value)
      End Set
    End Property

    Public Shared LocationCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LocationCode, "Location Code")
    ''' <summary>
    ''' Gets and sets the Location Code value
    ''' </summary>
    <Display(Name:="Location Code", Description:="")>
    Public Property LocationCode() As String
      Get
        Return GetProperty(LocationCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationCodeProperty, Value)
      End Set
    End Property

    Public Shared MediaPlatformProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MediaPlatform, "Media Platform")
    ''' <summary>
    ''' Gets and sets the Media Platform value
    ''' </summary>
    <Display(Name:="Media Platform", Description:="")>
    Public Property MediaPlatform() As String
      Get
        Return GetProperty(MediaPlatformProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MediaPlatformProperty, Value)
      End Set
    End Property

    Public Shared ProgramTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProgramType, "Program Type")
    ''' <summary>
    ''' Gets and sets the Program Type value
    ''' </summary>
    <Display(Name:="Program Type", Description:="")>
    Public Property ProgramType() As String
      Get
        Return GetProperty(ProgramTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProgramTypeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ScheduleDateTime, "Schedule Date Time")
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Schedule Date Time", Description:="")>
    Public Property ScheduleDateTime As Date
      Get
        Return GetProperty(ScheduleDateTimeProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(ScheduleDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleEndDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ScheduleEndDate, "Schedule End Date")
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="Schedule End Date", Description:="")>
    Public Property ScheduleEndDate As Date
      Get
        Return GetProperty(ScheduleEndDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(ScheduleEndDateProperty, Value)
      End Set
    End Property

    Public Shared ScheduleNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ScheduleNumber, "Schedule Number")
    ''' <summary>
    ''' Gets and sets the Schedule Number value
    ''' </summary>
    <Display(Name:="Schedule Number", Description:="")>
    Public Property ScheduleNumber() As Integer
      Get
        Return GetProperty(ScheduleNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ScheduleNumberProperty, Value)
      End Set
    End Property

    Public Shared SeasonNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SeasonNumber, "Season Number")
    ''' <summary>
    ''' Gets and sets the Season Number value
    ''' </summary>
    <Display(Name:="Season Number", Description:="")>
    Public Property SeasonNumber() As Integer
      Get
        Return GetProperty(SeasonNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SeasonNumberProperty, Value)
      End Set
    End Property

    Public Shared SeasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Season, "Season")
    ''' <summary>
    ''' Gets and sets the Season value
    ''' </summary>
    <Display(Name:="Season", Description:="")>
    Public Property Season() As String
      Get
        Return GetProperty(SeasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SeasonProperty, Value)
      End Set
    End Property

    Public Shared SeriesNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SeriesNumber, "Series Number")
    ''' <summary>
    ''' Gets and sets the Series Number value
    ''' </summary>
    <Display(Name:="Series Number", Description:="")>
    Public Property SeriesNumber() As Integer
      Get
        Return GetProperty(SeriesNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SeriesNumberProperty, Value)
      End Set
    End Property

    Public Shared SeriesTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SeriesTitle, "Series Title")
    ''' <summary>
    ''' Gets and sets the Series Title value
    ''' </summary>
    <Display(Name:="Series Title", Description:="")>
    Public Property SeriesTitle() As String
      Get
        Return GetProperty(SeriesTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SeriesTitleProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue")
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VenueProperty, Value)
      End Set
    End Property

    Public Shared VenueCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VenueCode, "Venue Code")
    ''' <summary>
    ''' Gets and sets the Venue Code value
    ''' </summary>
    <Display(Name:="Venue Code", Description:="")>
    Public Property VenueCode() As String
      Get
        Return GetProperty(VenueCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VenueCodeProperty, Value)
      End Set
    End Property

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country")
    ''' <summary>
    ''' Gets and sets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="")>
    Public Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CountryProperty, Value)
      End Set
    End Property

    Public Shared IsTBCProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTBC, "Is TBC", False)
    ''' <summary>
    ''' Gets and sets the Is TBC value
    ''' </summary>
    <Display(Name:="Is TBC", Description:="")>
    Public Property IsTBC() As Boolean
      Get
        Return GetProperty(IsTBCProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsTBCProperty, Value)
      End Set
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared TotalMinutesProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalMinutes, "Total Minutes")
    ''' <summary>
    ''' Gets and sets the Total Minutes value
    ''' </summary>
    <Display(Name:="Total Minutes", Description:="")>
    Public Property TotalMinutes() As Decimal
      Get
        Return GetProperty(TotalMinutesProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TotalMinutesProperty, Value)
      End Set
    End Property

    Public Shared StartMinuteProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.StartMinute, "Start Minute")
    ''' <summary>
    ''' Gets and sets the Start Minute value
    ''' </summary>
    <Display(Name:="Start Minute", Description:="")>
    Public Property StartMinute() As Decimal
      Get
        Return GetProperty(StartMinuteProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(StartMinuteProperty, Value)
      End Set
    End Property

    Public Shared TotalMinutesDisplayedProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.TotalMinutesDisplayed, "Total Minutes")
    ''' <summary>
    ''' Gets and sets the Total Minutes value
    ''' </summary>
    <Display(Name:="Total Minutes", Description:="")>
    Public Property TotalMinutesDisplayed() As Decimal
      Get
        Return GetProperty(TotalMinutesDisplayedProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(TotalMinutesDisplayedProperty, Value)
      End Set
    End Property

    Public Shared StartMinutesProperty As PropertyInfo(Of Int32) = RegisterProperty(Of Int32)(Function(c) c.StartMinutes, "Start Minutes")
    ''' <summary>
    ''' Gets and sets the Start Minute value
    ''' </summary>
    <Display(Name:="Start Minute", Description:="")>
    Public Property StartMinutes() As Int32
      Get
        Return GetProperty(StartMinutesProperty)
      End Get
      Set(ByVal Value As Int32)
        SetProperty(StartMinutesProperty, Value)
      End Set
    End Property

    Public Shared EndMinutesProperty As PropertyInfo(Of Int32) = RegisterProperty(Of Int32)(Function(c) c.EndMinutes, "End Minutes")
    ''' <summary>
    ''' Gets and sets the Start Minute value
    ''' </summary>
    <Display(Name:="End Minutes", Description:="")>
    Public Property EndMinutes() As Int32
      Get
        Return GetProperty(EndMinutesProperty)
      End Get
      Set(ByVal Value As Int32)
        SetProperty(EndMinutesProperty, Value)
      End Set
    End Property

    Public Shared SponsorshipCountProperty As PropertyInfo(Of Int32) = RegisterProperty(Of Int32)(Function(c) c.SponsorshipCount, "Sponsorship Count")
    ''' <summary>
    ''' Gets and sets the Start Minute value
    ''' </summary>
    <Display(Name:="Sponsorship Count", Description:="")>
    Public Property SponsorshipCount() As Int32
      Get
        Return GetProperty(SponsorshipCountProperty)
      End Get
      Set(ByVal Value As Int32)
        SetProperty(SponsorshipCountProperty, Value)
      End Set
    End Property

    Public Shared ParentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ParentID, "ParentID")
    ''' <summary>
    ''' Gets and sets the Synergy Import value
    ''' </summary>
    <Display(Name:="ParentID", Description:="")>
    Public Property ParentID() As Integer
      Get
        Return GetProperty(ParentIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ParentIDProperty, Value)
      End Set
    End Property

    Public Shared StartTimeEndTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StartTimeEndTime, "Start Time End Time")
    ''' <summary>
    ''' Gets and sets the Venue Code value
    ''' </summary>
    <Display(Name:="Start Time End Time", Description:="")>
    Public Property StartTimeEndTime() As String
      Get
        Return GetProperty(StartTimeEndTimeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StartTimeEndTimeProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    'type = object because we can use anything as a parent
    Public Function GetParent() As Object

      Return CType(CType(Me.Parent, SynergyScheduleList).Parent, Object)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SynergyScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ChannelName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "S All Channel Sched")
        Else
          Return String.Format("Blank {0}", "S All Channel Sched")
        End If
      Else
        Return Me.ChannelName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSynergySchedule() method.

    End Sub

    Public Shared Function NewSynergySchedule() As SynergySchedule

      Return DataPortal.CreateChild(Of SynergySchedule)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSynergySchedule(dr As SafeDataReader) As SynergySchedule

      Dim s As New SynergySchedule()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SynergyScheduleIDProperty, .GetInt32(0))
          LoadProperty(SynergyImportIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ChannelNameProperty, .GetString(2))
          LoadProperty(ChannelShortNameProperty, .GetString(3))
          LoadProperty(DurationProperty, .GetString(4))
          LoadProperty(EventStatusProperty, .GetString(5))
          LoadProperty(EventStatusDescProperty, .GetString(6))
          LoadProperty(GenRefNumberProperty, .GetInt64(7))
          LoadProperty(GenreProperty, .GetString(8))
          LoadProperty(GenreDescProperty, .GetString(9))
          LoadProperty(LiveDateProperty, .GetValue(10))
          LoadProperty(LiveTimeProperty, .GetString(11))
          LoadProperty(LocationProperty, .GetString(12))
          LoadProperty(LocationCodeProperty, .GetString(13))
          LoadProperty(MediaPlatformProperty, .GetString(14))
          LoadProperty(ProgramTypeProperty, .GetString(15))
          LoadProperty(ScheduleDateTimeProperty, .GetValue(16))
          LoadProperty(ScheduleEndDateProperty, .GetValue(17))
          LoadProperty(ScheduleNumberProperty, .GetInt32(18))
          LoadProperty(SeasonNumberProperty, .GetInt32(19))
          LoadProperty(SeasonProperty, .GetString(20))
          LoadProperty(SeriesNumberProperty, .GetInt32(21))
          LoadProperty(SeriesTitleProperty, .GetString(22))
          LoadProperty(TitleProperty, .GetString(23))
          LoadProperty(VenueProperty, .GetString(24))
          LoadProperty(VenueCodeProperty, .GetString(25))
          LoadProperty(CountryProperty, .GetString(26))
          LoadProperty(IsTBCProperty, .GetBoolean(27))
          LoadProperty(CreatedDateTimeProperty, .GetDateTime(28))
          LoadProperty(TotalMinutesProperty, .GetDecimal(29))
          LoadProperty(StartMinuteProperty, .GetDecimal(30))
          LoadProperty(TotalMinutesDisplayedProperty, .GetDecimal(31))
          LoadProperty(StartMinutesProperty, .GetInt32(32))
          LoadProperty(EndMinutesProperty, .GetInt32(33))
          LoadProperty(SponsorshipCountProperty, .GetInt32(34))
          LoadProperty(StartTimeEndTimeProperty, .GetString(35))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, SynergyScheduleIDProperty)

      cm.Parameters.AddWithValue("@SynergyImportID", GetProperty(SynergyImportIDProperty))
      cm.Parameters.AddWithValue("@ChannelName", GetProperty(ChannelNameProperty))
      cm.Parameters.AddWithValue("@ChannelShortName", Me.GetParent().ChannelShortName)
      cm.Parameters.AddWithValue("@Duration", GetProperty(DurationProperty))
      cm.Parameters.AddWithValue("@EventStatus", GetProperty(EventStatusProperty))
      cm.Parameters.AddWithValue("@EventStatusDesc", GetProperty(EventStatusDescProperty))
      cm.Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
      cm.Parameters.AddWithValue("@Genre", GetProperty(GenreProperty))
      cm.Parameters.AddWithValue("@GenreDesc", GetProperty(GenreDescProperty))
      cm.Parameters.AddWithValue("@LiveDate", LiveDate)
      cm.Parameters.AddWithValue("@LiveTime", GetProperty(LiveTimeProperty))
      cm.Parameters.AddWithValue("@Location", GetProperty(LocationProperty))
      cm.Parameters.AddWithValue("@LocationCode", GetProperty(LocationCodeProperty))
      cm.Parameters.AddWithValue("@MediaPlatform", GetProperty(MediaPlatformProperty))
      cm.Parameters.AddWithValue("@ProgramType", GetProperty(ProgramTypeProperty))
      cm.Parameters.AddWithValue("@ScheduleDateTime", ScheduleDateTime)
      cm.Parameters.AddWithValue("@ScheduleEndDate", ScheduleEndDate)
      cm.Parameters.AddWithValue("@ScheduleNumber", GetProperty(ScheduleNumberProperty))
      cm.Parameters.AddWithValue("@SeasonNumber", GetProperty(SeasonNumberProperty))
      cm.Parameters.AddWithValue("@Season", GetProperty(SeasonProperty))
      cm.Parameters.AddWithValue("@SeriesNumber", GetProperty(SeriesNumberProperty))
      cm.Parameters.AddWithValue("@SeriesTitle", GetProperty(SeriesTitleProperty))
      cm.Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      cm.Parameters.AddWithValue("@Venue", GetProperty(VenueProperty))
      cm.Parameters.AddWithValue("@VenueCode", GetProperty(VenueCodeProperty))
      cm.Parameters.AddWithValue("@Country", GetProperty(CountryProperty))
      cm.Parameters.AddWithValue("@IsTBC", GetProperty(IsTBCProperty))
      cm.Parameters.AddWithValue("@TotalMinutes", GetProperty(TotalMinutesProperty))
      cm.Parameters.AddWithValue("@StartMinute", GetProperty(StartMinuteProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(SynergyScheduleIDProperty, cm.Parameters("@SynergyScheduleID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@SynergyScheduleID", GetProperty(SynergyScheduleIDProperty))
    End Sub

#End Region

  End Class

End Namespace