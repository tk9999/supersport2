﻿' Generated 22 Dec 2016 07:40 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Scheduling.Rooms

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class SynergyScheduleGroup
    Inherits OBBusinessBase(Of SynergyScheduleGroup)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SynergyScheduleGroupIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SynergyScheduleGroupID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property SynergyScheduleGroupID() As Integer
      Get
        Return GetProperty(SynergyScheduleGroupIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SynergyScheduleGroupIDProperty, Value)
      End Set
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.CallTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    SetExpression("SynergyScheduleGroupBO.CallTimeStartSet(self)")>
    Public Property CallTime As DateTime?
      Get
        Return GetProperty(CallTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeProperty, Value)
      End Set
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    SetExpression("SynergyScheduleGroupBO.StartDateTimeSet(self)")>
    Public Property StartTime As DateTime?
      Get
        Return GetProperty(StartTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartTimeProperty, Value)
      End Set
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    SetExpression("SynergyScheduleGroupBO.EndDateTimeSet(self)")>
    Public Property EndTime As DateTime?
      Get
        Return GetProperty(EndTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndTimeProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.WrapTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    SetExpression("SynergyScheduleGroupBO.WrapTimeEndSet(self)")>
    Public Property WrapTime As DateTime?
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeProperty, Value)
      End Set
    End Property

    Public Shared DurationMinutesProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.DurationMinutes, "Duration Minutes", CDec(0.0))
    ''' <summary>
    ''' Gets and sets the Total Minutes value
    ''' </summary>
    <Display(Name:="Duration Minutes", Description:="")>
    Public Property DurationMinutes() As Decimal
      Get
        Return GetProperty(DurationMinutesProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(DurationMinutesProperty, Value)
      End Set
    End Property

    Public Shared DurationStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DurationString, "Duration String", "")
    ''' <summary>
    ''' Gets and sets the Total Minutes value
    ''' </summary>
    <Display(Name:="Duration Minutes", Description:="")>
    Public Property DurationString() As String
      Get
        Return GetProperty(DurationStringProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DurationStringProperty, Value)
      End Set
    End Property

    Public Shared DurationWarningMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DurationWarningMinutes, "Duration String", 600)
    ''' <summary>
    ''' Gets and sets the Total Minutes value
    ''' </summary>
    <Display(Name:="Duration Minutes", Description:="")>
    Public Property DurationWarningMinutes() As Integer
      Get
        Return GetProperty(DurationWarningMinutesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(DurationWarningMinutesProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, " Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the  System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(ROUserSystemList), DisplayMember:="System", ValueMember:="SystemID"),
    SetExpression("SynergyScheduleGroupBO.SystemIDSet(self)")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, " Area", Nothing)
    ''' <summary>
    ''' Gets and sets the  Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required"),
    DropDownWeb(GetType(ROUserSystemAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID", ThisFilterMember:="SystemID"),
    SetExpression("SynergyScheduleGroupBO.ProductionAreaIDSet(self)")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "ProductionArea")
    ''' <summary>
    ''' Gets and sets the ProductionArea value
    ''' </summary>
    <Display(Name:="ProductionArea", Description:="")>
    Public Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaProperty, Value)
      End Set
    End Property


    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
    Required(ErrorMessage:="Room required"),
    DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                BeforeFetchJS:="SynergyScheduleGroupBO.setRoomCriteriaBeforeRefresh",
                PreFindJSFunction:="SynergyScheduleGroupBO.triggerRoomAutoPopulate",
                AfterFetchJS:="SynergyScheduleGroupBO.afterRoomRefreshAjax",
                LookupMember:="Room", ValueMember:="RoomID", DropDownColumns:={"Room", "IsAvailableString"},
                OnItemSelectJSFunction:="SynergyScheduleGroupBO.onRoomSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("SynergyScheduleGroupBO.RoomIDSet(self)")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "ID", Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared SynergyScheduleListProperty As PropertyInfo(Of SynergyScheduleList) = RegisterProperty(Of SynergyScheduleList)(Function(c) c.SynergyScheduleList, "Synergy Schedule List")
    Public ReadOnly Property SynergyScheduleList() As SynergyScheduleList
      Get
        If GetProperty(SynergyScheduleListProperty) Is Nothing Then
          LoadProperty(SynergyScheduleListProperty, Synergy.SynergyScheduleList.NewSynergyScheduleList())
        End If
        Return GetProperty(SynergyScheduleListProperty)
      End Get
    End Property

    Public Shared RoomScheduleListProperty As PropertyInfo(Of RoomScheduleList) = RegisterProperty(Of RoomScheduleList)(Function(c) c.RoomScheduleList, "RoomScheduleList")
    Public ReadOnly Property RoomScheduleList() As RoomScheduleList
      Get
        If GetProperty(RoomScheduleListProperty) Is Nothing Then
          LoadProperty(RoomScheduleListProperty, OBLib.Scheduling.Rooms.RoomScheduleList.NewRoomScheduleList())
        End If
        Return GetProperty(RoomScheduleListProperty)
      End Get
    End Property

    Public Shared SynergyScheduleGroupPHRListProperty As PropertyInfo(Of SynergyScheduleGroupPHRList) = RegisterProperty(Of SynergyScheduleGroupPHRList)(Function(c) c.SynergyScheduleGroupPHRList, "SynergyScheduleGroupPHRList")
    Public ReadOnly Property SynergyScheduleGroupPHRList() As SynergyScheduleGroupPHRList
      Get
        If GetProperty(SynergyScheduleGroupPHRListProperty) Is Nothing Then
          LoadProperty(SynergyScheduleGroupPHRListProperty, Synergy.SynergyScheduleGroupPHRList.NewSynergyScheduleGroupPHRList())
        End If
        Return GetProperty(SynergyScheduleGroupPHRListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Object

      Return CType(CType(Me.Parent, SynergyScheduleGroupList).Parent, Object)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SynergyScheduleGroupIDProperty)

    End Function

    Public Overrides Function ToString() As String

      'If Me.ChannelName.Length = 0 Then
      '  If Me.IsNew Then
      '    Return String.Format("New {0}", "S All Channel Sched")
      '  Else
      '    Return String.Format("Blank {0}", "S All Channel Sched")
      '  End If
      'Else
      '  Return Me.ChannelName
      'End If
      Return ""

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSynergyScheduleGroup() method.

    End Sub

    Public Shared Function NewSynergyScheduleGroup() As SynergyScheduleGroup

      Return DataPortal.CreateChild(Of SynergyScheduleGroup)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSynergyScheduleGroup(dr As SafeDataReader) As SynergyScheduleGroup

      Dim s As New SynergyScheduleGroup()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      'Using BypassPropertyChecks
      '  With sdr
      '    LoadProperty(SynergyScheduleGroupIDProperty, .GetInt32(0))
      '    LoadProperty(SynergyImportIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
      '    LoadProperty(ChannelNameProperty, .GetString(2))
      '    LoadProperty(ChannelShortNameProperty, .GetString(3))
      '    LoadProperty(DurationProperty, .GetString(4))
      '    LoadProperty(EventStatusProperty, .GetString(5))
      '    LoadProperty(EventStatusDescProperty, .GetString(6))
      '    LoadProperty(GenRefNumberProperty, .GetInt64(7))
      '    LoadProperty(GenreProperty, .GetString(8))
      '    LoadProperty(GenreDescProperty, .GetString(9))
      '    LoadProperty(LiveDateProperty, .GetValue(10))
      '    LoadProperty(LiveTimeProperty, .GetString(11))
      '    LoadProperty(LocationProperty, .GetString(12))
      '    LoadProperty(LocationCodeProperty, .GetString(13))
      '    LoadProperty(MediaPlatformProperty, .GetString(14))
      '    LoadProperty(ProgramTypeProperty, .GetString(15))
      '    LoadProperty(ScheduleDateTimeProperty, .GetValue(16))
      '    LoadProperty(ScheduleEndDateProperty, .GetValue(17))
      '    LoadProperty(ScheduleNumberProperty, .GetInt32(18))
      '    LoadProperty(SeasonNumberProperty, .GetInt32(19))
      '    LoadProperty(SeasonProperty, .GetString(20))
      '    LoadProperty(SeriesNumberProperty, .GetInt32(21))
      '    LoadProperty(SeriesTitleProperty, .GetString(22))
      '    LoadProperty(TitleProperty, .GetString(23))
      '    LoadProperty(VenueProperty, .GetString(24))
      '    LoadProperty(VenueCodeProperty, .GetString(25))
      '    LoadProperty(CountryProperty, .GetString(26))
      '    LoadProperty(IsTBCProperty, .GetBoolean(27))
      '    LoadProperty(CreatedDateTimeProperty, .GetDateTime(28))
      '    LoadProperty(TotalMinutesProperty, .GetDecimal(29))
      '    LoadProperty(StartMinuteProperty, .GetDecimal(30))
      '    LoadProperty(TotalMinutesDisplayedProperty, .GetDecimal(31))
      '    LoadProperty(StartMinutesProperty, .GetInt32(32))
      '    LoadProperty(EndMinutesProperty, .GetInt32(33))
      '    LoadProperty(SponsorshipCountProperty, .GetInt32(34))
      '    LoadProperty(StartTimeEndTimeProperty, .GetString(35))
      '  End With
      'End Using

      MarkAsChild()
      MarkOld()
      'BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      'AddPrimaryKeyParam(cm, SynergyScheduleGroupIDProperty)

      'cm.Parameters.AddWithValue("@SynergyImportID", GetProperty(SynergyImportIDProperty))
      'cm.Parameters.AddWithValue("@ChannelName", GetProperty(ChannelNameProperty))
      'cm.Parameters.AddWithValue("@ChannelShortName", Me.GetParent().ChannelShortName)
      'cm.Parameters.AddWithValue("@Duration", GetProperty(DurationProperty))
      'cm.Parameters.AddWithValue("@EventStatus", GetProperty(EventStatusProperty))
      'cm.Parameters.AddWithValue("@EventStatusDesc", GetProperty(EventStatusDescProperty))
      'cm.Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
      'cm.Parameters.AddWithValue("@Genre", GetProperty(GenreProperty))
      'cm.Parameters.AddWithValue("@GenreDesc", GetProperty(GenreDescProperty))
      'cm.Parameters.AddWithValue("@LiveDate", LiveDate)
      'cm.Parameters.AddWithValue("@LiveTime", GetProperty(LiveTimeProperty))
      'cm.Parameters.AddWithValue("@Location", GetProperty(LocationProperty))
      'cm.Parameters.AddWithValue("@LocationCode", GetProperty(LocationCodeProperty))
      'cm.Parameters.AddWithValue("@MediaPlatform", GetProperty(MediaPlatformProperty))
      'cm.Parameters.AddWithValue("@ProgramType", GetProperty(ProgramTypeProperty))
      'cm.Parameters.AddWithValue("@ScheduleDateTime", ScheduleDateTime)
      'cm.Parameters.AddWithValue("@ScheduleEndDate", ScheduleEndDate)
      'cm.Parameters.AddWithValue("@ScheduleNumber", GetProperty(ScheduleNumberProperty))
      'cm.Parameters.AddWithValue("@SeasonNumber", GetProperty(SeasonNumberProperty))
      'cm.Parameters.AddWithValue("@Season", GetProperty(SeasonProperty))
      'cm.Parameters.AddWithValue("@SeriesNumber", GetProperty(SeriesNumberProperty))
      'cm.Parameters.AddWithValue("@SeriesTitle", GetProperty(SeriesTitleProperty))
      'cm.Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      'cm.Parameters.AddWithValue("@Venue", GetProperty(VenueProperty))
      'cm.Parameters.AddWithValue("@VenueCode", GetProperty(VenueCodeProperty))
      'cm.Parameters.AddWithValue("@Country", GetProperty(CountryProperty))
      'cm.Parameters.AddWithValue("@IsTBC", GetProperty(IsTBCProperty))
      'cm.Parameters.AddWithValue("@TotalMinutes", GetProperty(TotalMinutesProperty))
      'cm.Parameters.AddWithValue("@StartMinute", GetProperty(StartMinuteProperty))

      Return Sub()
               ''Post Save
               'If Me.IsNew Then
               '  LoadProperty(SynergyScheduleGroupIDProperty, cm.Parameters("@SynergyScheduleGroupID").Value)
               'End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
      UpdateChild(GetProperty(RoomScheduleListProperty))
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      'cm.Parameters.AddWithValue("@SynergyScheduleGroupID", GetProperty(SynergyScheduleGroupIDProperty))
    End Sub

#End Region

    Public Sub CheckClashesClient()

      'Dim ds As New DataSet("DataSet")
      'Dim dt As DataTable = ds.Tables.Add("Table")
      'dt.Columns.Add("GUID", GetType(Guid)).ColumnMapping = MappingType.Attribute
      'dt.Columns.Add("ResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      'dt.Columns.Add("RoomScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      'dt.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      'dt.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute


      'For Each rs As RoomSchedule In Me.RoomScheduleList
      '  Dim row As DataRow = dt.NewRow
      '  row("Guid") = rs.Guid
      '  row("ResourceID") = rs.ResourceID
      '  row("RoomScheduleID") = rs.RoomScheduleID
      '  row("StartDateTime") = rs.StartTime
      '  row("EndDateTime") = rs.EndTime
      '  dt.Rows.Add(row)
      'Next

      'Dim cmd As New Singular.CommandProc("[RuleProcs].[ruleResourceAvailable]",
      '                                    "@AreaScheduleXML",
      '                                    ds.GetXml)
      'cmd.FetchType = CommandProc.FetchTypes.DataSet
      'cmd = cmd.Execute

      ''Reset the clashes
      'Me.AllClashes = ""

      'Dim HRProductionSchedule As HRProductionSchedule = Nothing
      'Dim HumanResourceID As Integer? = Nothing
      'Dim GuidString As String = ""
      'Dim GUid As Guid = Nothing
      'Dim AreaScheduleDetail As HRProductionScheduleDetail = Nothing

      'For Each dr As DataRow In cmd.Dataset.Tables(0).Rows
      '  Dim Clash As String = dr(4)
      '  GUid = dr(1)
      '  AreaScheduleDetail = Me.HRProductionScheduleDetailList.GetItemByGuid(GUid)
      '  If AreaScheduleDetail IsNot Nothing Then
      '    AreaScheduleDetail.Clash = Clash
      '  End If
      'Next

    End Sub

    Function CheckClashes() As Web.Result

      Try
        Dim rbl As New List(Of OBLib.Helpers.ResourceHelpers.ResourceBooking)

        'RoomSchedules
        For Each rs As RoomSchedule In Me.RoomScheduleList
          Dim rb As New OBLib.Helpers.ResourceHelpers.ResourceBooking(rs.Guid, rs.ResourceID, rs.ResourceBookingID,
                                                                      rs.CallTime, rs.StartTime, rs.EndTime, rs.WrapTime,
                                                                      rs.RoomScheduleID, Nothing, rs.ProductionSystemAreaIDOwner,
                                                                      Nothing, Nothing, Nothing, Nothing)
          rbl.Add(rb)
          'RoomSchedulePHR
          For Each phr As RoomSchedulePHR In rs.RoomSchedulePHRList
            Dim phrRB As New OBLib.Helpers.ResourceHelpers.ResourceBooking(phr.Guid, phr.ResourceID, phr.ResourceBookingID,
                                                                           phr.CallTime, phr.StartTime, phr.EndTime, phr.WrapTime,
                                                                           rs.RoomScheduleID, Nothing, rs.ProductionSystemAreaIDOwner,
                                                                           Nothing, Nothing, Nothing, Nothing)
            rbl.Add(phrRB)
          Next
        Next

        'get the clashes
        rbl = OBLib.Helpers.ResourceHelpers.GetResourceClashes(rbl)

        'Update the business objects with the clashes
        For Each rs As RoomSchedule In Me.RoomScheduleList
          'RoomSchedules
          rs.ResetClashList()
          Dim bookingList As List(Of OBLib.Helpers.ResourceHelpers.ResourceBooking) = rbl.Where(Function(d) CompareSafe(d.ObjectGuid, rs.Guid)).ToList
          bookingList.ForEach(Sub(rb)
                                rb.ClashDetails.ForEach(Sub(cls)
                                                          rs.ClashList.Add(cls.ResourceBookingDescription)
                                                          rs.Clashes = String.Join(vbCrLf, cls.ResourceBookingDescription)
                                                        End Sub)
                              End Sub)
          'RoomSchedulePHR
          For Each phr As RoomSchedulePHR In rs.RoomSchedulePHRList
            phr.ResetClashList()
            Dim phrBookingList As List(Of OBLib.Helpers.ResourceHelpers.ResourceBooking) = rbl.Where(Function(d) CompareSafe(d.ObjectGuid, phr.Guid)).ToList
            phrBookingList.ForEach(Sub(phrRB)
                                     phrRB.ClashDetails.ForEach(Sub(cls)
                                                                  rs.ClashList.Add(cls.ResourceBookingDescription)
                                                                  rs.Clashes = String.Join(vbCrLf, cls.ResourceBookingDescription)
                                                                End Sub)
                                   End Sub)
          Next
        Next

      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

      Return New Singular.Web.Result(True)

    End Function

    Public Function DoSetup() As Web.Result

      Try
        Dim ds As DataSet = Me.GetSetupDataSet
        ProcessSetupDataSet(ds)
        Return New Singular.Web.Result(True)
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

    End Function

    Private Function GetSetupDataSet() As DataSet

      Dim RBTable As DataTable = CreateRoomScheduleTableParameter()
      Dim cmd As New Singular.CommandProc("CmdProcs.cmdSetupRoomScheduleGroup")
      Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      ResourceBookingsParam.Name = "@RoomSchedules"
      ResourceBookingsParam.SqlType = SqlDbType.Structured
      ResourceBookingsParam.Value = RBTable
      cmd.Parameters.Add(ResourceBookingsParam)
      cmd.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute(0)
      Return cmd.Dataset

    End Function

    Private Function CreateRoomScheduleTableParameter() As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("GenRefNumber", GetType(Int64)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceBookingID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("RoomScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionSystemAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("RoomID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Room", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("CallTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("WrapTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("Title", GetType(String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SystemID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As OBLib.Scheduling.Rooms.RoomSchedule In Me.RoomScheduleList
        Dim row As DataRow = RBTable.NewRow
        row("ObjectGuid") = rb.Guid
        row("GenRefNumber") = rb.GenRefNumber
        row("ResourceBookingID") = ZeroDBNull(rb.ResourceBookingID)
        row("RoomScheduleID") = ZeroDBNull(rb.RoomScheduleID)
        row("ProductionSystemAreaID") = NothingDBNull(rb.ProductionSystemAreaID)
        row("RoomID") = NothingDBNull(rb.RoomID)
        row("Room") = NothingDBNull(rb.Room)
        row("ResourceID") = NothingDBNull(rb.ResourceID)
        row("CallTime") = NothingDBNull(rb.CallTime)
        row("StartTime") = rb.StartTime.Value.ToString
        row("EndTime") = rb.EndTime.Value.ToString
        row("WrapTime") = NothingDBNull(rb.WrapTime)
        row("Title") = rb.Title
        row("SystemID") = NothingDBNull(rb.SystemID)
        row("ProductionAreaID") = NothingDBNull(rb.ProductionAreaID)
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

    Private Sub ProcessSetupDataSet(ds As DataSet)

      For Each dr As DataRow In ds.Tables(0).Rows
        Dim guid As Guid = dr(0)
        Dim bo As RoomSchedule = Me.RoomScheduleList.Where(Function(d) CompareSafe(d.Guid, guid)).FirstOrDefault
        If bo IsNot Nothing Then
          bo.CallTime = dr(1)
          bo.WrapTime = dr(2)
        End If
      Next
      Me.CallTime = Me.RoomScheduleList.Min(Function(d) d.CallTime)
      Me.WrapTime = Me.RoomScheduleList.Max(Function(d) d.WrapTime)

    End Sub

  End Class

End Namespace