﻿' Generated 16 Jun 2016 14:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.HR.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Synergy

  <Serializable()> _
  Public Class SynergyScheduleGroupPHR
    Inherits OBBusinessBase(Of SynergyScheduleGroupPHR)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SynergyScheduleGroupPHRBO.SynergyScheduleGroupPHRBOToString(self)")


    Public Shared ProductionHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHumanResourceID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property ProductionHumanResourceID() As Integer
      Get
        Return GetProperty(ProductionHumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionHumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionHRID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property ProductionHRID() As Integer
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionHRIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "Resource Booking")
    ''' <summary>
    ''' Gets and sets the Resource Booking value
    ''' </summary>
    <Display(Name:="Resource Booking", Description:="")>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Production System Area")
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:=""),
    Required(ErrorMessage:="Production System Area required")>
    Public Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    Required(ErrorMessage:="Human Resource required"),
    DropDownWeb(GetType(ROHumanResourceAvailabilityList),
                BeforeFetchJS:="SynergyScheduleGroupPHRBO.setHumanResourceCriteriaBeforeRefresh",
                PreFindJSFunction:="SynergyScheduleGroupPHRBO.triggerHumanResourceAutoPopulate",
                AfterFetchJS:="SynergyScheduleGroupPHRBO.afterHumanResourceRefreshAjax",
                OnItemSelectJSFunction:="SynergyScheduleGroupPHRBO.onHumanResourceSelected",
                LookupMember:="HumanResource", DisplayMember:="HRName", ValueMember:="HumanResourceID",
                DropDownCssClass:="hr-availability-dropdown", DropDownColumns:={"HRName"},
                OnCellCreateFunction:="SynergyScheduleGroupPHRBO.onCellCreate"),
    SetExpression("SynergyScheduleGroupPHRBO.HumanResourceIDSet(self)")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
    Public Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Call Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    Required(ErrorMessage:="Call Time required"),
    SetExpression("SynergyScheduleGroupPHRBO.StartDateTimeBufferSet(self)")>
    Public Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
    SetExpression("SynergyScheduleGroupPHRBO.StartDateTimeSet(self)")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
    SetExpression("SynergyScheduleGroupPHRBO.EndDateTimeSet(self)")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "Wrap Time")
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    Required(ErrorMessage:="Wrap Time required"),
    SetExpression("SynergyScheduleGroupPHRBO.EndDateTimeBufferSet(self)")>
    Public Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeBufferProperty, Value)
      End Set
    End Property

    'Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Resource Booking Description")
    ' ''' <summary>
    ' ''' Gets and sets the Resource Booking Description value
    ' ''' </summary>
    '<Display(Name:="Resource Booking Description", Description:="")>
    'Public Property ResourceBookingDescription() As String
    '  Get
    '    Return GetProperty(ResourceBookingDescriptionProperty)
    '  End Get
    '  Set(ByVal Value As String)
    '    SetProperty(ResourceBookingDescriptionProperty, Value)
    '  End Set
    'End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    Required(ErrorMessage:="Discipline required"),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplineListSelect),
                BeforeFetchJS:="SynergyScheduleGroupPHRBO.setDisciplineIDCriteriaBeforeRefresh",
                PreFindJSFunction:="SynergyScheduleGroupPHRBO.triggerDisciplineIDAutoPopulate",
                AfterFetchJS:="SynergyScheduleGroupPHRBO.afterDisciplineIDRefreshAjax",
                OnItemSelectJSFunction:="SynergyScheduleGroupPHRBO.onDisciplineIDSelected",
                LookupMember:="Discipline", DisplayMember:="Discipline", ValueMember:="DisciplineID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Discipline"})>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared PositionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionTypeID, "Position Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:=""),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplinePositionTypeListSelect),
                BeforeFetchJS:="SynergyScheduleGroupPHRBO.setPositionTypeIDCriteriaBeforeRefresh",
                PreFindJSFunction:="SynergyScheduleGroupPHRBO.triggerPositionTypeIDAutoPopulate",
                AfterFetchJS:="SynergyScheduleGroupPHRBO.afterPositionTypeIDRefreshAjax",
                OnItemSelectJSFunction:="SynergyScheduleGroupPHRBO.onPositionTypeIDSelected",
                LookupMember:="PositionType", DisplayMember:="PositionType", ValueMember:="PositionTypeID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Position"})>
    Public Property PositionTypeID() As Integer?
      Get
        Return GetProperty(PositionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared PositionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PositionType, "Position Type", "")
    ''' <summary>
    ''' Gets and sets the Position Type value
    ''' </summary>
    <Display(Name:="Position Type", Description:="")>
    Public Property PositionType() As String
      Get
        Return GetProperty(PositionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionTypeProperty, Value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:=""),
    DropDownWeb(GetType(ROSystemProductionAreaDisciplinePositionTypeListSelect),
                BeforeFetchJS:="SynergyScheduleGroupPHRBO.setPositionIDCriteriaBeforeRefresh",
                PreFindJSFunction:="SynergyScheduleGroupPHRBO.triggerPositionIDAutoPopulate",
                AfterFetchJS:="SynergyScheduleGroupPHRBO.afterPositionIDRefreshAjax",
                OnItemSelectJSFunction:="SynergyScheduleGroupPHRBO.onPositionIDSelected",
                LookupMember:="Position", DisplayMember:="Position", ValueMember:="PositionID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"Position"})>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position", "")
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PositionProperty, Value)
      End Set
    End Property

    Public Shared CallTimeCSDIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CallTimeCSDID, "Call Time CSD")
    ''' <summary>
    ''' Gets and sets the Call Time CSD value
    ''' </summary>
    <Display(Name:="Call Time CSD", Description:="")>
    Public Property CallTimeCSDID() As Integer
      Get
        Return GetProperty(CallTimeCSDIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CallTimeCSDIDProperty, Value)
      End Set
    End Property

    Public Shared OnAirTimeCSDIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OnAirTimeCSDID, "On Air Time CSD")
    ''' <summary>
    ''' Gets and sets the On Air Time CSD value
    ''' </summary>
    <Display(Name:="On Air Time CSD", Description:="")>
    Public Property OnAirTimeCSDID() As Integer
      Get
        Return GetProperty(OnAirTimeCSDIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OnAirTimeCSDIDProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeCSDIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.WrapTimeCSDID, "Wrap Time CSD")
    ''' <summary>
    ''' Gets and sets the Wrap Time CSD value
    ''' </summary>
    <Display(Name:="Wrap Time CSD", Description:="")>
    Public Property WrapTimeCSDID() As Integer
      Get
        Return GetProperty(WrapTimeCSDIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(WrapTimeCSDIDProperty, Value)
      End Set
    End Property

    'Public Shared CallTimeMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.CallTimeMatch, "CallTimeMatch", False)
    ' ''' <summary>
    ' ''' Gets and sets the Slug Layout In Scheduler value
    ' ''' </summary>
    '<Display(Name:="CallTimeMatch", Description:="")>
    'Public ReadOnly Property CallTimeMatch() As Boolean
    '  Get
    '    Return GetProperty(CallTimeMatchProperty)
    '  End Get
    'End Property

    'Public Shared OnAirStartMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnAirStartMatch, "OnAirStartMatch", False)
    ' ''' <summary>
    ' ''' Gets and sets the Slug Layout In Scheduler value
    ' ''' </summary>
    '<Display(Name:="OnAirStartMatch", Description:="")>
    'Public ReadOnly Property OnAirStartMatch() As Boolean
    '  Get
    '    Return GetProperty(OnAirStartMatchProperty)
    '  End Get
    'End Property

    'Public Shared OnAirEndMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OnAirEndMatch, "OnAirEndMatch", False)
    ' ''' <summary>
    ' ''' Gets and sets the Slug Layout In Scheduler value
    ' ''' </summary>
    '<Display(Name:="OnAirEndMatch", Description:="")>
    'Public ReadOnly Property OnAirEndMatch() As Boolean
    '  Get
    '    Return GetProperty(OnAirEndMatchProperty)
    '  End Get
    'End Property

    'Public Shared WrapTimeMatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.WrapTimeMatch, "WrapTimeMatch", False)
    ' ''' <summary>
    ' ''' Gets and sets the Slug Layout In Scheduler value
    ' ''' </summary>
    '<Display(Name:="WrapTimeMatch", Description:="")>
    'Public ReadOnly Property WrapTimeMatch() As Boolean
    '  Get
    '    Return GetProperty(WrapTimeMatchProperty)
    '  End Get
    'End Property

#End Region

#Region " Other Properties "

    Public Shared FreezeSetExpressionsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FreezeSetExpressions, "FreezeSetExpressions", False)
    ''' <summary>
    ''' Gets and sets the Is TBC value
    ''' </summary>
    <Display(Name:="FreezeSetExpressions", Description:=""), AlwaysClean>
    Public Property FreezeSetExpressions() As Boolean
      Get
        Return GetProperty(FreezeSetExpressionsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(FreezeSetExpressionsProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ClashDetailListProperty As PropertyInfo(Of List(Of Helpers.ResourceHelpers.ClashDetail)) = RegisterProperty(Of List(Of Helpers.ResourceHelpers.ClashDetail))(Function(c) c.ClashDetailList, "Clash List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ClashDetailList() As List(Of Helpers.ResourceHelpers.ClashDetail)
      Get
        If GetProperty(ClashDetailListProperty) Is Nothing Then
          LoadProperty(ClashDetailListProperty, New List(Of Helpers.ResourceHelpers.ClashDetail))
        End If
        Return GetProperty(ClashDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SynergyScheduleGroup

      Return CType(CType(Me.Parent, SynergyScheduleGroupPHRList).Parent, SynergyScheduleGroup)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ProductionHRIDProperty)

    End Function

    Public Overrides Function ToString() As String

      'If Me.IgnoreClashesReason.Length = 0 Then
      '  If Me.IsNew Then
      '    Return String.Format("New {0}", "Room Schedule Area Production HR Booking")
      '  Else
      '    Return String.Format("Blank {0}", "Room Schedule Area Production HR Booking")
      '  End If
      'Else
      '  Return Me.IgnoreClashesReason
      'End If
      Return ""

    End Function

    Public Function GetProductionHRID() As Integer?

      Dim phrList As List(Of SynergyScheduleGroupPHR) = Me.GetParent.SynergyScheduleGroupPHRList.Where(Function(d) CompareSafe(d.HumanResourceID, Me.HumanResourceID)).ToList
      If phrList.Count = 0 Then
        Return Nothing
      ElseIf phrList.Count = 1 Then
        Return phrList(0).ProductionHRID
      ElseIf phrList.Count > 1 Then
        Return phrList(0).ProductionHRID
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(StartDateTimeBufferProperty)
        .ServerRuleFunction = AddressOf CallTimeValid
        .JavascriptRuleFunctionName = "SynergyScheduleGroupPHRBO.CallTimeValid"
        .AffectedProperties.AddRange({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
        .AddTriggerProperties({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
      End With

      With AddWebRule(StartDateTimeProperty)
        .ServerRuleFunction = AddressOf StartTimeValid
        .JavascriptRuleFunctionName = "SynergyScheduleGroupPHRBO.StartTimeValid"
        .AffectedProperties.AddRange({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
        .AddTriggerProperties({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
      End With

      With AddWebRule(EndDateTimeProperty)
        .ServerRuleFunction = AddressOf EndTimeValid
        .JavascriptRuleFunctionName = "SynergyScheduleGroupPHRBO.EndTimeValid"
        .AffectedProperties.AddRange({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
        .AddTriggerProperties({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
      End With

      With AddWebRule(EndDateTimeBufferProperty)
        .ServerRuleFunction = AddressOf WrapTimeValid
        .JavascriptRuleFunctionName = "SynergyScheduleGroupPHRBO.WrapTimeValid"
        .AffectedProperties.AddRange({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
        .AddTriggerProperties({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
      End With

      'With AddWebRule(EndDateTimeBufferProperty)
      '  .ServerRuleFunction = AddressOf WrapTimeValid
      '  .JavascriptRuleFunctionName = "SynergyScheduleGroupPHRBO.WrapTimeValid"
      '  .AffectedProperties.AddRange({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
      '  .AddTriggerProperties({StartDateTimeBufferProperty, StartDateTimeProperty, EndDateTimeProperty, EndDateTimeBufferProperty})
      'End With

    End Sub

    Public Shared Function CallTimeValid(SynergyScheduleGroupPHR As SynergyScheduleGroupPHR) As String
      If SynergyScheduleGroupPHR.StartDateTimeBuffer Is Nothing Then
        Return "Call time is required"
      Else
        If SynergyScheduleGroupPHR.StartDateTime IsNot Nothing Then
          If SynergyScheduleGroupPHR.StartDateTimeBuffer.Value.Subtract(SynergyScheduleGroupPHR.StartDateTime.Value).TotalMinutes > 0 Then
            Return "Call time must be before Start time"
          End If
        End If
      End If
      Return ""
    End Function

    Public Shared Function StartTimeValid(SynergyScheduleGroupPHR As SynergyScheduleGroupPHR) As String
      If SynergyScheduleGroupPHR.StartDateTime Is Nothing Then
        Return "Start time is required"
      Else
        If SynergyScheduleGroupPHR.EndDateTime IsNot Nothing Then
          If SynergyScheduleGroupPHR.StartDateTime.Value.Subtract(SynergyScheduleGroupPHR.EndDateTime.Value).TotalMinutes > 0 Then
            Return "Start time must be before End time"
          End If
        End If
      End If
      Return ""
    End Function

    Public Shared Function EndTimeValid(SynergyScheduleGroupPHR As SynergyScheduleGroupPHR) As String
      If SynergyScheduleGroupPHR.EndDateTime Is Nothing Then
        Return "End time is required"
      Else
        If SynergyScheduleGroupPHR.EndDateTimeBuffer IsNot Nothing Then
          If SynergyScheduleGroupPHR.EndDateTime.Value.Subtract(SynergyScheduleGroupPHR.EndDateTimeBuffer.Value).TotalMinutes > 0 Then
            Return "End time must be before Wrap time"
          End If
        End If
      End If
      Return ""
    End Function

    Public Shared Function WrapTimeValid(SynergyScheduleGroupPHR As SynergyScheduleGroupPHR) As String
      If SynergyScheduleGroupPHR.EndDateTimeBuffer Is Nothing Then
        Return "Wrap time is required"
      Else
        If SynergyScheduleGroupPHR.EndDateTime IsNot Nothing Then
          If SynergyScheduleGroupPHR.EndDateTime.Value.Subtract(SynergyScheduleGroupPHR.EndDateTimeBuffer.Value).TotalMinutes > 0 Then
            Return "End time must be before Wrap time"
          End If
        End If
      End If
      Return ""
    End Function

    Public Shared Function HasClashesValid(SynergyScheduleGroupPHR As SynergyScheduleGroupPHR) As String
      If SynergyScheduleGroupPHR.ClashDetailList.Count > 0 Then
        Return SynergyScheduleGroupPHR.ClashDetailList.Count.ToString & " clashes detected"
      Else
        Return ""
      End If
    End Function

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSynergyScheduleGroupPHR() method.

    End Sub

    Public Shared Function NewSynergyScheduleGroupPHR() As SynergyScheduleGroupPHR

      Return DataPortal.CreateChild(Of SynergyScheduleGroupPHR)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSynergyScheduleGroupPHR(dr As SafeDataReader) As SynergyScheduleGroupPHR

      Dim r As New SynergyScheduleGroupPHR()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ProductionHRIDProperty, .GetInt32(0))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          'LoadProperty(ResourceBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          'LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(StartDateTimeBufferProperty, .GetValue(7))
          LoadProperty(StartDateTimeProperty, .GetValue(8))
          LoadProperty(EndDateTimeProperty, .GetValue(9))
          LoadProperty(EndDateTimeBufferProperty, .GetValue(10))
          'LoadProperty(IgnoreClashesReasonProperty, .GetString(11))
          'LoadProperty(IsCancelledProperty, .GetBoolean(12))
          'LoadProperty(IsTBCProperty, .GetBoolean(13))
          'LoadProperty(ResourceBookingDescriptionProperty, .GetString(14))
          'LoadProperty(StatusCssClassProperty, .GetString(15))
          LoadProperty(CallTimeCSDIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          LoadProperty(OnAirTimeCSDIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(WrapTimeCSDIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
          'LoadProperty(CallTimeTimelineIDProperty, ZeroNothing(.GetInt32(19)))
          'LoadProperty(OnAirTimeTimelineIDProperty, ZeroNothing(.GetInt32(20)))
          'LoadProperty(WrapTimeTimelineIDProperty, ZeroNothing(.GetInt32(21)))
          'LoadProperty(CallTimeMatchProperty, .GetBoolean(22))
          'LoadProperty(OnAirStartMatchProperty, .GetBoolean(23))
          'LoadProperty(OnAirEndMatchProperty, .GetBoolean(24))
          'LoadProperty(WrapTimeMatchProperty, .GetBoolean(25))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ProductionHumanResourceIDProperty)
      AddInputOutputParam(cm, ProductionHRIDProperty)
      AddInputOutputParam(cm, ResourceBookingIDProperty)
      AddInputOutputParam(cm, CallTimeCSDIDProperty)
      AddInputOutputParam(cm, OnAirTimeCSDIDProperty)
      AddInputOutputParam(cm, WrapTimeCSDIDProperty)

      cm.Parameters.AddWithValue("@HumanResourceID", GetProperty(HumanResourceIDProperty))
      cm.Parameters.AddWithValue("@ResourceID", NothingDBNull(GetProperty(ResourceIDProperty)))
      cm.Parameters.AddWithValue("@StartDateTimeBuffer", StartDateTimeBuffer)
      cm.Parameters.AddWithValue("@StartDateTime", StartDateTime)
      cm.Parameters.AddWithValue("@EndDateTime", EndDateTime)
      cm.Parameters.AddWithValue("@EndDateTimeBuffer", EndDateTimeBuffer)
      cm.Parameters.AddWithValue("@SystemID", NothingDBNull(Me.GetParent.SystemID))
      cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(Me.GetParent.ProductionAreaID))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)
      'cm.Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent().ProductionSystemAreaIDOwner)
      'cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
      'cm.Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
      'cm.Parameters.AddWithValue("@RoomScheduleID", Me.GetParent().RoomScheduleID)
      'cm.Parameters.AddWithValue("@IgnoreClashesReason", GetProperty(IgnoreClashesReasonProperty))
      'cm.Parameters.AddWithValue("@IsCancelled", GetProperty(IsCancelledProperty))
      'cm.Parameters.AddWithValue("@IsTBC", GetProperty(IsTBCProperty))
      'cm.Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
      'cm.Parameters.AddWithValue("@StatusCssClass", GetProperty(StatusCssClassProperty))
      'cm.Parameters.AddWithValue("@CallTimeCSDID", GetProperty(CallTimeCSDIDProperty))
      'cm.Parameters.AddWithValue("@OnAirTimeCSDID", GetProperty(OnAirTimeCSDIDProperty))
      'cm.Parameters.AddWithValue("@WrapTimeCSDID", GetProperty(WrapTimeCSDIDProperty))
      'cm.Parameters.AddWithValue("@CallTimeTimelineID", Me.GetParent.CallTimeTimelineID)
      'cm.Parameters.AddWithValue("@OnAirTimeTimelineID", Me.GetParent.OnAirTimeTimelineID)
      'cm.Parameters.AddWithValue("@WrapTimeTimelineID", Me.GetParent.WrapTimeTimelineID)
      'cm.Parameters.AddWithValue("@ProductionSystemAreaID", Me.GetParent().ProductionSystemAreaID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ProductionHRIDProperty, cm.Parameters("@ProductionHRID").Value)
                 LoadProperty(ProductionHRIDProperty, cm.Parameters("@ResourceBookingID").Value)
                 LoadProperty(ProductionHRIDProperty, cm.Parameters("@CallTimeCSDID").Value)
                 LoadProperty(ProductionHRIDProperty, cm.Parameters("@OnAirTimeCSDID").Value)
                 LoadProperty(ProductionHRIDProperty, cm.Parameters("@WrapTimeCSDID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ProductionHRID", GetProperty(ProductionHRIDProperty))
    End Sub

#End Region

  End Class

End Namespace