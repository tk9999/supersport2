﻿' Generated 30 Dec 2016 09:35 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class RORoomSchedule
    Inherits OBReadOnlyBase(Of RORoomSchedule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return RORoomScheduleBO.RORoomScheduleBOToString(self)")

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomScheduleID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property RoomScheduleID() As Integer
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref Number")
    ''' <summary>
    ''' Gets and sets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref Number", Description:="")>
    Public ReadOnly Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public ReadOnly Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomID, "Room")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property RoomID() As Integer
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared RoomNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RoomName, "Room Name")
    ''' <summary>
    ''' Gets and sets the Room Name value
    ''' </summary>
    <Display(Name:="Room Name", Description:="")>
    Public ReadOnly Property RoomName() As String
      Get
        Return GetProperty(RoomNameProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionSystemAreaID, "Production System Area")
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="")>
    Public ReadOnly Property ProductionSystemAreaID() As Integer
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared AreaStartTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.AreaStartTime, "Area Start Time")
    ''' <summary>
    ''' Gets and sets the Area Start Time value
    ''' </summary>
    <Display(Name:="Area Start Time", Description:="")>
    Public ReadOnly Property AreaStartTime As DateTime
      Get
        Return GetProperty(AreaStartTimeProperty)
      End Get
    End Property

    Public Shared AreaEndTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.AreaEndTime, "Area End Time")
    ''' <summary>
    ''' Gets and sets the Area End Time value
    ''' </summary>
    <Display(Name:="Area End Time", Description:="")>
    Public ReadOnly Property AreaEndTime As DateTime
      Get
        Return GetProperty(AreaEndTimeProperty)
      End Get
    End Property

    Public Shared RSCreatedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RSCreatedBy, "RS Created By")
    ''' <summary>
    ''' Gets and sets the RS Created By value
    ''' </summary>
    <Display(Name:="RS Created By", Description:="")>
    Public ReadOnly Property RSCreatedBy() As String
      Get
        Return GetProperty(RSCreatedByProperty)
      End Get
    End Property

    Public Shared PSACreatedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PSACreatedBy, "PSA Created By")
    ''' <summary>
    ''' Gets and sets the PSA Created By value
    ''' </summary>
    <Display(Name:="PSA Created By", Description:="")>
    Public ReadOnly Property PSACreatedBy() As String
      Get
        Return GetProperty(PSACreatedByProperty)
      End Get
    End Property

    ''' <summary>
    ''' This property is set in the allocator, not in the fetch method
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared TimeMismatchProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.TimeMismatch, "TimeMismatch")
    ''' <summary>
    ''' Gets the Parent value
    ''' </summary>
    Public ReadOnly Property TimeMismatch() As Boolean
      Get
        Return GetProperty(TimeMismatchProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(RoomScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.RoomName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Public Shared Function NewRORoomSchedule() As RORoomSchedule

      Return DataPortal.CreateChild(Of RORoomSchedule)()

    End Function

    Public Sub New()

    End Sub

    Friend Shared Function GetRORoomSchedule(dr As SafeDataReader) As RORoomSchedule

      Dim s As New RORoomSchedule()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(RoomScheduleIDProperty, .GetInt32(0))
        LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(GenRefNumberProperty, .GetInt64(2))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(RoomNameProperty, .GetString(4))
        'LoadProperty(RoomShortNameProperty, .GetString(5))
        LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(AreaStartTimeProperty, .GetValue(7))
        LoadProperty(AreaEndTimeProperty, .GetValue(8))
        LoadProperty(RSCreatedByProperty, .GetString(9))
        LoadProperty(PSACreatedByProperty, .GetString(10))
        'LoadProperty(TimeMismatchProperty, .GetBoolean(12))
      End With

      BusinessRules.CheckRules()

    End Sub

#End Region

  End Class

End Namespace