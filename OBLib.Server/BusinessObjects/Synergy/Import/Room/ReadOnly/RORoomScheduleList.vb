﻿' Generated 30 Dec 2016 09:35 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class RORoomScheduleList
    Inherits OBReadOnlyListBase(Of RORoomScheduleList, RORoomSchedule)

#Region " Business Methods "

    Public Function GetItem(RoomScheduleID As Integer) As RORoomSchedule

      For Each child As RORoomSchedule In Me
        If child.RoomScheduleID = RoomScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewRORoomScheduleList() As RORoomScheduleList

      Return New RORoomScheduleList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace