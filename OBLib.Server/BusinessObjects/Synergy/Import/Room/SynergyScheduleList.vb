﻿' Generated 22 Dec 2016 07:40 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class SynergyScheduleList
    Inherits OBBusinessListBase(Of SynergyScheduleList, SynergySchedule)

#Region " Business Methods "

    Public Function GetItem(SynergyScheduleID As Integer) As SynergySchedule

      For Each child As SynergySchedule In Me
        If child.SynergyScheduleID = SynergyScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ChannelIDs As New List(Of Integer)
      Public Property ChannelShortNames As New List(Of String)
      Public Property StartDate As DateTime?
      Public Property EndDate As DateTime?
      Public Property ExcludeHighlights As Boolean = False

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewSynergyScheduleList() As SynergyScheduleList

      Return New SynergyScheduleList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSynergyScheduleList() As SynergyScheduleList

      Return DataPortal.Fetch(Of SynergyScheduleList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SynergySchedule.GetSynergySchedule(sdr))
      End While
      Me.RaiseListChangedEvents = True

      'not concerned with checking rules as this editable object is used as a read-only object because we need the "read-only" fields to be sent to/from the server without the values being lost

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSynergyScheduleList"
            cm.Parameters.AddWithValue("@ChannelIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ChannelIDs)))
            cm.Parameters.AddWithValue("@ChannelShortNames", Strings.MakeEmptyDBNull(OBLib.OBMisc.StringListToXML(crit.ChannelShortNames)))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@ExcludeHighlights", crit.ExcludeHighlights)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace