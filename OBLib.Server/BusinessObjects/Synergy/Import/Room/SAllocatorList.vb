﻿' Generated 22 Dec 2016 07:40 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class SAllocatorList
    Inherits OBBusinessListBase(Of SAllocatorList, SAllocator)

#Region " Business Methods "

    Public Function GetItem(ParentID As Integer) As SAllocator

      For Each child As SAllocator In Me
        If child.ParentID = ParentID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ChannelIDs As New List(Of Integer)
      Public Property ChannelShortNames As New List(Of String)
      Public Property StartDate As DateTime?
      Public Property EndDate As DateTime?
      Public Property Statuses As New List(Of String)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewSAllocatorList() As SAllocatorList

      Return New SAllocatorList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetSAllocatorList() As SAllocatorList

      Return DataPortal.Fetch(Of SAllocatorList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(SAllocator.GetSAllocator(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As SAllocator = Me(0)
      If sdr.NextResult() Then
        While sdr.Read
          'If parent Is Nothing OrElse parent.ParentID <> sdr.GetInt32(35) Then
          '  parent = Me.GetItem(sdr.GetInt32(35))
          'End If
          parent.SynergyScheduleList.RaiseListChangedEvents = False
          parent.SynergyScheduleList.Add(SynergySchedule.GetSynergySchedule(sdr))
          parent.SynergyScheduleList.RaiseListChangedEvents = True
        End While
      End If

      'If sdr.NextResult() Then
      '  While sdr.Read
      '    'If parent Is Nothing OrElse parent.ParentID <> sdr.GetInt32(11) Then
      '    '  parent = Me.GetItem(sdr.GetInt32(11))
      '    'End If
      '    parent.SAllRoomScheduleList.RaiseListChangedEvents = False
      '    parent.SAllRoomScheduleList.Add(SAllRoomSchedule.GetSAllRoomSchedule(sdr))
      '    parent.SAllRoomScheduleList.RaiseListChangedEvents = True
      '  End While
      'End If

      'For Each child As SAllocator In Me
      '  child.CheckRules()
      '  For Each SAllChannel As SAllChannel In child.SAllChannelList
      '    SAllChannel.CheckRules()
      '    For Each SAllChannelSched As SAllChannelSched In SAllChannel.SAllChannelSchedList
      '      SAllChannelSched.CheckRules()
      '    Next
      '  Next
      '  For Each SAllTimeRange As SAllTimeRange In child.SAllTimeRangeList
      '    SAllTimeRange.CheckRules()
      '  Next
      'Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getSynergyAllocatorList"
            cm.Parameters.AddWithValue("@ChannelIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ChannelIDs)))
            cm.Parameters.AddWithValue("@ChannelShortNames", Strings.MakeEmptyDBNull(OBLib.OBMisc.StringListToXML(crit.ChannelShortNames)))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            cm.Parameters.AddWithValue("@Statuses", Strings.MakeEmptyDBNull(OBLib.OBMisc.StringListToXML(crit.Statuses)))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace