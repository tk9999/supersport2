﻿' Generated 30 Sep 2017 17:59 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class AutomaticImportCrewList
    Inherits OBBusinessListBase(Of AutomaticImportCrewList, AutomaticImportCrew)

#Region " Business Methods "

    Public Function GetItem(AutomaticImportCrewID As Integer) As AutomaticImportCrew

      For Each child As AutomaticImportCrew In Me
        If child.AutomaticImportCrewID = AutomaticImportCrewID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Automatic Import Crews"

    End Function

#End Region

#Region " Data Access "

    Public Shared Function NewAutomaticImportCrewList() As AutomaticImportCrewList

      Return New AutomaticImportCrewList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

  End Class

End Namespace