﻿' Generated 27 Jan 2016 09:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Equipment

  <Serializable()> _
  Public Class EquipmentAllocatorList
    Inherits OBBusinessListBase(Of EquipmentAllocatorList, EquipmentAllocator)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ImportedEventChannelID As Integer) As EquipmentAllocator

      For Each child As EquipmentAllocator In Me
        If child.ImportedEventChannelID = ImportedEventChannelID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SystemID, Nothing) _
                                                             .AddSetExpression("EquipmentAllocatorListCriteriaBO.SystemIDSet(self)")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept"),
      Required(ErrorMessage:="Sub-Dept is required"),
      Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), Source:=Singular.DataAnnotations.DropDownWeb.SourceType.CommonData)>
      Public Property SystemID() As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionAreaID, Nothing) _
                                                             .AddSetExpression("EquipmentAllocatorListCriteriaBO.ProductionAreaIDSet(self)")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area"),
      Required(ErrorMessage:="Area is required"),
      Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaList), Source:=Singular.DataAnnotations.DropDownWeb.SourceType.CommonData, ThisFilterMember:="SystemID")>
      Public Property ProductionAreaID() As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Nothing) _
                                                                   .AddSetExpression("EquipmentAllocatorListCriteriaBO.StartDateSet(self)")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Start Date"),
      Required(ErrorMessage:="Start Date is required")>
      Public Property StartDate() As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Nothing) _
                                                                    .AddSetExpression("EquipmentAllocatorListCriteriaBO.EndDateSet(self)")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="End Date"),
      Required(ErrorMessage:="End Date is required")>
      Public Property EndDate() As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Shared LiveProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.Live, True) _
                                                                    .AddSetExpression("EquipmentAllocatorListCriteriaBO.LiveSet(self)")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Live", Description:="")>
      Public Property Live() As Boolean
        Get
          Return ReadProperty(LiveProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(LiveProperty, Value)
        End Set
      End Property

      Public Shared DelayedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.Delayed, True) _
                                                                    .AddSetExpression("EquipmentAllocatorListCriteriaBO.DelayedSet(self)")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Delayed", Description:="")>
      Public Property Delayed() As Boolean
        Get
          Return ReadProperty(DelayedProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(DelayedProperty, Value)
        End Set
      End Property

      Public Shared PremierProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.Premier, False) _
                                                                    .AddSetExpression("EquipmentAllocatorListCriteriaBO.PremierSet(self)")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Premier", Description:="")>
      Public Property Premier() As Boolean
        Get
          Return ReadProperty(PremierProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(PremierProperty, Value)
        End Set
      End Property

      Public Shared GenRefNoProperty As PropertyInfo(Of Int64?) = RegisterSProperty(Of Int64?)(Function(c) c.GenRefNo, Nothing) _
                                                                    .AddSetExpression("EquipmentAllocatorListCriteriaBO.GenRefNoSet(self)")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Gen Ref No", Description:="")>
      Public Property GenRefNo() As Int64?
        Get
          Return ReadProperty(GenRefNoProperty)
        End Get
        Set(ByVal Value As Int64?)
          LoadProperty(GenRefNoProperty, Value)
        End Set
      End Property

      Public Shared PrimaryChannelsOnlyProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.PrimaryChannelsOnly, False) _
                                                                              .AddSetExpression("EquipmentAllocatorListCriteriaBO.PrimaryChannelsOnlySet(self)")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Primary Channels Only?", Description:="")>
      Public Property PrimaryChannelsOnly() As Boolean
        Get
          Return ReadProperty(PrimaryChannelsOnlyProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(PrimaryChannelsOnlyProperty, Value)
        End Set
      End Property

      Public Shared SelectedChannelsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedChannelsDisplay, "Selected Channels", "Select Channels")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Channels")>
      Public Property SelectedChannelsDisplay() As String
        Get
          Return ReadProperty(SelectedChannelsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SelectedChannelsProperty, Value)
        End Set
      End Property

      Public Shared SelectedChannelIDsXMLProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.SelectedChannelIDsXML, "") _
                                                                              .AddSetExpression("EquipmentAllocatorListCriteriaBO.SelectedChannelIDsXMLSet(self)")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Selected Channels")>
      Public Property SelectedChannelIDsXML() As String
        Get
          Return ReadProperty(SelectedChannelIDsXMLProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SelectedChannelIDsXMLProperty, Value)
        End Set
      End Property

      Public Shared GenreProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Genre, "") _
                                                               .AddSetExpression("EquipmentAllocatorListCriteriaBO.GenreSet(self)")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Genre")>
      Public Property Genre() As String
        Get
          Return ReadProperty(GenreProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(GenreProperty, Value)
        End Set
      End Property

      Public Shared SeriesProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Series, "") _
                                                                .AddSetExpression("EquipmentAllocatorListCriteriaBO.SeriesSet(self)")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Series")>
      Public Property Series() As String
        Get
          Return ReadProperty(SeriesProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SeriesProperty, Value)
        End Set
      End Property

      Public Shared TitleProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Title, "") _
                                                               .AddSetExpression("EquipmentAllocatorListCriteriaBO.TitleSet(self)")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Title")>
      Public Property Title() As String
        Get
          Return ReadProperty(TitleProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(TitleProperty, Value)
        End Set
      End Property

      Public Shared AllChannelsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.AllChannels, "All Channels", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="All Channels"),
      SetExpression("EquipmentAllocatorListCriteriaBO.AllChannelsSet(self)")>
      Public Property AllChannels() As Boolean
        Get
          Return ReadProperty(AllChannelsProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(AllChannelsProperty, Value)
        End Set
      End Property

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?,
                     StartDate As DateTime?, EndDate As DateTime?)

        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.StartDate = StartDate
        Me.EndDate = EndDate

      End Sub

      Public Sub New(StartDate As Date?, EndDate As Date?,
                     GenRefNo As Int64?, Live As Boolean, Delayed As Boolean,
                     Premier As Boolean, PageNo As Integer,
                     PageSize As Integer, SortColumn As String,
                     SortAsc As Boolean)
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.GenRefNo = GenRefNo
        Me.Live = Live
        Me.Delayed = Delayed
        Me.Premier = Premier
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortColumn = SortColumn
        Me.SortAsc = SortAsc
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewEquipmentAllocatorList() As EquipmentAllocatorList

      Return New EquipmentAllocatorList()

    End Function

    Public Shared Sub BeginGetEquipmentAllocatorList(CallBack As EventHandler(Of DataPortalResult(Of EquipmentAllocatorList)))

      Dim dp As New DataPortal(Of EquipmentAllocatorList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetEquipmentAllocatorList() As EquipmentAllocatorList

      Return DataPortal.Fetch(Of EquipmentAllocatorList)(New Criteria())

    End Function

    Public Shared Function GetEquipmentAllocatorList(Criteria As OBLib.Equipment.EquipmentAllocatorList.Criteria) As EquipmentAllocatorList

      Return DataPortal.Fetch(Of EquipmentAllocatorList)(Criteria)

    End Function

    Public Shared Function GetEquipmentAllocatorList(StartDate As Date?, EndDate As Date?,
                                                          GenRefNo As Int64?, Live As Boolean, Delayed As Boolean,
                                                          Premier As Boolean, PageNo As Integer,
                                                          PageSize As Integer, SortColumn As String,
                                                          SortAsc As Boolean) As EquipmentAllocatorList

      Return DataPortal.Fetch(Of EquipmentAllocatorList)(New Criteria(StartDate, EndDate, GenRefNo, Live, Delayed, Premier,
                                                                           PageNo, PageSize, SortColumn, SortAsc))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      While sdr.Read
        Me.Add(EquipmentAllocator.GetEquipmentAllocator(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getEquipmentAllocatorList"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@GenRefNo", NothingDBNull(crit.GenRefNo))
            cm.Parameters.AddWithValue("@Live", crit.Live)
            cm.Parameters.AddWithValue("@Delayed", crit.Delayed)
            cm.Parameters.AddWithValue("@Premier", crit.Premier)
            cm.Parameters.AddWithValue("@PrimaryChannelsOnly", crit.PrimaryChannelsOnly)
            cm.Parameters.AddWithValue("@SelectedChannelIDs", Strings.MakeEmptyDBNull(crit.SelectedChannelIDsXML))
            cm.Parameters.AddWithValue("@Genre", Strings.MakeEmptyDBNull(crit.Genre))
            cm.Parameters.AddWithValue("@Series", Strings.MakeEmptyDBNull(crit.Series))
            cm.Parameters.AddWithValue("@Title", Strings.MakeEmptyDBNull(crit.Title))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As EquipmentAllocator In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As EquipmentAllocator In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace