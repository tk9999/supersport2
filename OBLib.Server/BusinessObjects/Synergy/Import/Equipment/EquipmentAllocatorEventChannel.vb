﻿' Generated 11 Feb 2016 00:12 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Equipment

  <Serializable()> _
  Public Class EquipmentAllocatorEventChannel
    Inherits OBBusinessBase(Of EquipmentAllocatorEventChannel)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ImportedEventIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ImportedEventID() As Integer
      Get
        Return GetProperty(ImportedEventIDProperty)
      End Get
    End Property

    Public Shared ImportedEventChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventChannelID, "Imported Event Channel")
    ''' <summary>
    ''' Gets and sets the Imported Event Channel value
    ''' </summary>
    <Display(Name:="Imported Event Channel", Description:=""),
    Required(ErrorMessage:="Imported Event Channel required")>
  Public Property ImportedEventChannelID() As Integer
      Get
        Return GetProperty(ImportedEventChannelIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ImportedEventChannelIDProperty, Value)
      End Set
    End Property

    Public Shared ScheduleDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleDateTime, "Schedule Date Time")
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Schedule Date Time", Description:=""),
    Required(ErrorMessage:="Schedule Date Time required")>
  Public Property ScheduleDateTime As DateTime?
      Get
        Return GetProperty(ScheduleDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleEndDate, "Schedule End Date")
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="Schedule End Date", Description:=""),
    Required(ErrorMessage:="Schedule End Date required")>
  Public Property ScheduleEndDate As DateTime?
      Get
        Return GetProperty(ScheduleEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleEndDateProperty, Value)
      End Set
    End Property

    Public Shared EventStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventStatus, "Event Status")
    ''' <summary>
    ''' Gets and sets the Event Status value
    ''' </summary>
    <Display(Name:="Event Status", Description:="")>
  Public Property EventStatus() As String
      Get
        Return GetProperty(EventStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventStatusProperty, Value)
      End Set
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel Short Name")
    ''' <summary>
    ''' Gets and sets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel Short Name", Description:="")>
  Public Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelShortNameProperty, Value)
      End Set
    End Property

    Public Shared IsPrimaryChannelProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsPrimaryChannel, "Is Primary Channel", False)
    ''' <summary>
    ''' Gets and sets the Is Primary Channel value
    ''' </summary>
    <Display(Name:="Is Primary Channel", Description:=""),
    Required(ErrorMessage:="Is Primary Channel required")>
  Public Property IsPrimaryChannel() As Boolean
      Get
        Return GetProperty(IsPrimaryChannelProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsPrimaryChannelProperty, Value)
      End Set
    End Property

    Public Shared ButtonStyleCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ButtonStyleCssClass, "Button Style Css Class")
    ''' <summary>
    ''' Gets and sets the Button Style Css Class value
    ''' </summary>
    <Display(Name:="Button Style Css Class", Description:="")>
  Public Property ButtonStyleCssClass() As String
      Get
        Return GetProperty(ButtonStyleCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ButtonStyleCssClassProperty, Value)
      End Set
    End Property

    <Display(Name:="Broadcast Start")>
    Public ReadOnly Property StartDateString As String
      Get
        If ScheduleDateTime IsNot Nothing Then
          Return ScheduleDateTime.Value.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Broadcast End")>
    Public ReadOnly Property EndDateString As String
      Get
        If ScheduleEndDate IsNot Nothing Then
          Return ScheduleEndDate.Value.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As EquipmentAllocatorEvent

      Return CType(CType(Me.Parent, EquipmentAllocatorEventChannelList).Parent, EquipmentAllocatorEvent)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ImportedEventIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EventStatus.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Equipment Allocator Event Channel")
        Else
          Return String.Format("Blank {0}", "Equipment Allocator Event Channel")
        End If
      Else
        Return Me.EventStatus
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipmentAllocatorEventChannel() method.

    End Sub

    Public Shared Function NewEquipmentAllocatorEventChannel() As EquipmentAllocatorEventChannel

      Return DataPortal.CreateChild(Of EquipmentAllocatorEventChannel)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEquipmentAllocatorEventChannel(dr As SafeDataReader) As EquipmentAllocatorEventChannel

      Dim e As New EquipmentAllocatorEventChannel()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ImportedEventIDProperty, .GetInt32(0))
          LoadProperty(ImportedEventChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ScheduleDateTimeProperty, .GetValue(2))
          LoadProperty(ScheduleEndDateProperty, .GetValue(3))
          LoadProperty(EventStatusProperty, .GetString(4))
          LoadProperty(ChannelShortNameProperty, .GetString(5))
          LoadProperty(IsPrimaryChannelProperty, .GetBoolean(6))
          LoadProperty(ButtonStyleCssClassProperty, .GetString(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEquipmentAllocatorEventChannel"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEquipmentAllocatorEventChannel"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramImportedEventID As SqlParameter = .Parameters.Add("@ImportedEventID", SqlDbType.Int)
          paramImportedEventID.Value = GetProperty(ImportedEventIDProperty)
          If Me.IsNew Then
            paramImportedEventID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ImportedEventChannelID", GetProperty(ImportedEventChannelIDProperty))
          .Parameters.AddWithValue("@ScheduleDateTime", (New SmartDate(GetProperty(ScheduleDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ScheduleEndDate", (New SmartDate(GetProperty(ScheduleEndDateProperty))).DBValue)
          .Parameters.AddWithValue("@EventStatus", GetProperty(EventStatusProperty))
          .Parameters.AddWithValue("@ChannelShortName", GetProperty(ChannelShortNameProperty))
          .Parameters.AddWithValue("@IsPrimaryChannel", GetProperty(IsPrimaryChannelProperty))
          .Parameters.AddWithValue("@ButtonStyleCssClass", GetProperty(ButtonStyleCssClassProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ImportedEventIDProperty, paramImportedEventID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEquipmentAllocatorEventChannel"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ImportedEventID", GetProperty(ImportedEventIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace