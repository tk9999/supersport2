﻿' Generated 11 Feb 2016 00:12 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Equipment

  <Serializable()> _
  Public Class EquipmentAllocatorEvent
    Inherits OBBusinessBase(Of EquipmentAllocatorEvent)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="IsExpanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared VenueDayGroupProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VenueDayGroup, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property VenueDayGroup() As Integer
      Get
        Return GetProperty(VenueDayGroupProperty)
      End Get
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref Number")
    ''' <summary>
    ''' Gets and sets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref Number", Description:=""),
    Required(ErrorMessage:="Gen Ref Number required")>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared ImportedEventIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventID, "Imported Event")
    ''' <summary>
    ''' Gets and sets the Imported Event value
    ''' </summary>
    <Display(Name:="Imported Event", Description:="")>
    Public Property ImportedEventID() As Integer
      Get
        Return GetProperty(ImportedEventIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ImportedEventIDProperty, Value)
      End Set
    End Property

    Public Shared GenreSeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreSeries, "Genre Series")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Genre Series", Description:="")>
    Public Property GenreSeries() As String
      Get
        Return GetProperty(GenreSeriesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreSeriesProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared HighlightsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HighlightsInd, "Highlights", False)
    ''' <summary>
    ''' Gets and sets the Highlights value
    ''' </summary>
    <Display(Name:="Highlights", Description:="")>
    Public Property HighlightsInd() As Boolean
      Get
        Return GetProperty(HighlightsIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HighlightsIndProperty, Value)
      End Set
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LiveDate, "Live Date")
    ''' <summary>
    ''' Gets and sets the Live Date value
    ''' </summary>
    <Display(Name:="Live Date", Description:="")>
    Public Property LiveDate As DateTime?
      Get
        Return GetProperty(LiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LiveDateProperty, Value)
      End Set
    End Property

    Public Shared FirstChannelStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.FirstChannelStart, "First Channel Start")
    ''' <summary>
    ''' Gets and sets the First Channel Start value
    ''' </summary>
    <Display(Name:="First Channel Start", Description:=""),
    Required(ErrorMessage:="First Channel Start required")>
    Public Property FirstChannelStart As DateTime?
      Get
        Return GetProperty(FirstChannelStartProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(FirstChannelStartProperty, Value)
      End Set
    End Property

    <Display(Name:="Live")>
    Public ReadOnly Property LiveDateString As String
      Get
        If LiveDate IsNot Nothing Then
          Return LiveDate.Value.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="First Broadcast")>
    Public ReadOnly Property FirstBroadcastString As String
      Get
        If FirstChannelStart IsNot Nothing Then
          Return FirstChannelStart.Value.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "", Nothing)
    ''' <summary>
    ''' Gets and sets the Imported Event value
    ''' </summary>
    <Display(Name:="ProductionID", Description:="")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared IsSamrandFeedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsSamrandFeed, "Samrand Feed?", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Samrand Feed?")>
    Public Property IsSamrandFeed() As Boolean
      Get
        Return GetProperty(IsSamrandFeedProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsSamrandFeedProperty, value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared EquipmentAllocatorEventChannelListProperty As PropertyInfo(Of EquipmentAllocatorEventChannelList) = RegisterProperty(Of EquipmentAllocatorEventChannelList)(Function(c) c.EquipmentAllocatorEventChannelList, "Equipment Allocator Event Channel List")

    Public ReadOnly Property EquipmentAllocatorEventChannelList() As EquipmentAllocatorEventChannelList
      Get
        If GetProperty(EquipmentAllocatorEventChannelListProperty) Is Nothing Then
          LoadProperty(EquipmentAllocatorEventChannelListProperty, OBLib.Equipment.EquipmentAllocatorEventChannelList.NewEquipmentAllocatorEventChannelList())
        End If
        Return GetProperty(EquipmentAllocatorEventChannelListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As EquipmentAllocatorVenue

      Return CType(CType(Me.Parent, EquipmentAllocatorEventList).Parent, EquipmentAllocatorVenue)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VenueDayGroupProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.GenreSeries.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Equipment Allocator Event")
        Else
          Return String.Format("Blank {0}", "Equipment Allocator Event")
        End If
      Else
        Return Me.GenreSeries
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipmentAllocatorEvent() method.

    End Sub

    Public Shared Function NewEquipmentAllocatorEvent() As EquipmentAllocatorEvent

      Return DataPortal.CreateChild(Of EquipmentAllocatorEvent)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEquipmentAllocatorEvent(dr As SafeDataReader) As EquipmentAllocatorEvent

      Dim e As New EquipmentAllocatorEvent()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(VenueDayGroupProperty, .GetInt32(0))
          LoadProperty(GenRefNumberProperty, .GetInt64(1))
          LoadProperty(ImportedEventIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(GenreSeriesProperty, .GetString(3))
          LoadProperty(TitleProperty, .GetString(4))
          LoadProperty(HighlightsIndProperty, .GetBoolean(5))
          LoadProperty(LiveDateProperty, .GetValue(6))
          LoadProperty(FirstChannelStartProperty, .GetValue(7))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(IsSamrandFeedProperty, .GetBoolean(9))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEquipmentAllocatorEvent"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEquipmentAllocatorEvent"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramVenueDayGroup As SqlParameter = .Parameters.Add("@VenueDayGroup", SqlDbType.Int)
          paramVenueDayGroup.Value = GetProperty(VenueDayGroupProperty)
          If Me.IsNew Then
            paramVenueDayGroup.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
          .Parameters.AddWithValue("@ImportedEventID", GetProperty(ImportedEventIDProperty))
          .Parameters.AddWithValue("@GenreSeries", GetProperty(GenreSeriesProperty))
          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@HighlightsInd", GetProperty(HighlightsIndProperty))
          .Parameters.AddWithValue("@LiveDate", (New SmartDate(GetProperty(LiveDateProperty))).DBValue)
          .Parameters.AddWithValue("@FirstChannelStart", (New SmartDate(GetProperty(FirstChannelStartProperty))).DBValue)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(VenueDayGroupProperty, paramVenueDayGroup.Value)
          End If
          ' update child objects
          If GetProperty(EquipmentAllocatorEventChannelListProperty) IsNot Nothing Then
            Me.EquipmentAllocatorEventChannelList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(EquipmentAllocatorEventChannelListProperty) IsNot Nothing Then
          Me.EquipmentAllocatorEventChannelList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEquipmentAllocatorEvent"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@VenueDayGroup", GetProperty(VenueDayGroupProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace