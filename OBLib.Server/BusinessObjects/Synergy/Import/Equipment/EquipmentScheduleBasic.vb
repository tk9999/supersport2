﻿' Generated 27 Jan 2016 09:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.Equipment.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Equipment

  <Serializable()>
  Public Class EquipmentScheduleBasic
    Inherits OBBusinessBase(Of EquipmentScheduleBasic)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return EquipmentScheduleBasicBO.GetToString(self)")

    Public Shared EquipmentScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentScheduleID, "EquipmentScheduleID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="EquipmentScheduleID", Description:=""), Key>
    Public Property EquipmentScheduleID() As Integer?
      Get
        Return GetProperty(EquipmentScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingID, "ResourceBookingID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="ResourceBookingID", Description:="")>
    Public Property ResourceBookingID() As Integer?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref")
    ''' <summary>
    ''' Gets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:="")>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared GenreDescProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreDesc, "Genre")
    ''' <summary>
    ''' Gets the Genre Series value
    ''' </summary>
    <Display(Name:="Genre", Description:="")>
    Public Property GenreDesc() As String
      Get
        Return GetProperty(GenreDescProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreDescProperty, Value)
      End Set
    End Property

    Public Shared SeriesTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SeriesTitle, "Series")
    ''' <summary>
    ''' Gets the Genre Series value
    ''' </summary>
    <Display(Name:="Series", Description:="")>
    Public Property SeriesTitle() As String
      Get
        Return GetProperty(SeriesTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SeriesTitleProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EquipmentID, Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Circuit"),
     Required(ErrorMessage:="Circuit is required"),
     DropDownWeb(GetType(ROCircuitAvailabilityList),
                 BeforeFetchJS:="EquipmentScheduleBasicBO.setCircuitCriteriaBeforeRefresh",
                 PreFindJSFunction:="EquipmentScheduleBasicBO.triggerCircuitAutoPopulate",
                 AfterFetchJS:="EquipmentScheduleBasicBO.afterCircuitRefreshAjax",
                 OnItemSelectJSFunction:="EquipmentScheduleBasicBO.onCircuitSelected",
                 LookupMember:="EquipmentName", DisplayMember:="EquipmentName", ValueMember:="ResourceID",
                 DropDownCssClass:="circuit-dropdown", DropDownColumns:={"EquipmentName", "IsAvailableString"}),
    SetExpression("EquipmentScheduleBasicBO.EquipmentIDSet(self)")>
    Public Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentName, "Circuit")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Circuit", Description:="")>
    Public Property EquipmentName() As String
      Get
        Return GetProperty(EquipmentNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EquipmentNameProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ResourceIDProperty, value)
      End Set
    End Property

    Public Shared FeedIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedID, "Feed", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Feed")>
    Public Property FeedID() As Integer?
      Get
        Return GetProperty(FeedIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedIDProperty, Value)
      End Set
    End Property

    Public Shared FeedTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedTypeID, "Feed Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Feed Type"), DropDownWeb(GetType(ROFeedTypeList))>
    Public Property FeedTypeID() As Integer?
      Get
        Return GetProperty(FeedTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedTypeIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "SystemID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="SystemID", Description:="")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "ProductionAreaID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="ProductionAreaID", Description:="")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CallTime, "Start Buffer")
    ''' <summary>
    ''' Gets and sets the Call Time value
    ''' </summary>
    <Display(Name:="Start Buffer"),
    Singular.DataAnnotations.DateAndTimeField(FormatString:="HH:mm", ViewMode:="days")>
    Public Property CallTime As DateTime?
      Get
        Return GetProperty(CallTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeProperty, Value)
      End Set
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartTime, "Start Time")
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Singular.DataAnnotations.DateAndTimeField(FormatString:="HH:mm", ViewMode:="days"),
    Required(ErrorMessage:="Start Time is required"),
    SetExpression("EquipmentScheduleBasicBO.StartTimeSet(self)")>
    Public Property StartTime As DateTime?
      Get
        Return GetProperty(StartTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartTimeProperty, Value)
      End Set
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndTime, "End Time")
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="End Time"),
    Required(ErrorMessage:="End Time is required"),
    Singular.DataAnnotations.DateAndTimeField(FormatString:="HH:mm", ViewMode:="days"),
    SetExpression("EquipmentScheduleBasicBO.EndTimeSet(self)")>
    Public Property EndTime As DateTime?
      Get
        Return GetProperty(EndTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndTimeProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.WrapTime, "End Buffer")
    ''' <summary>
    ''' Gets and sets the Wrap Time value
    ''' </summary>
    <Display(Name:="End Buffer"),
    Singular.DataAnnotations.DateAndTimeField(FormatString:="HH:mm", ViewMode:="days")>
    Public Property WrapTime As DateTime?
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeProperty, Value)
      End Set
    End Property

    Public Shared EventStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventStatus, "Status")
    ''' <summary>
    ''' Gets the Event Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property EventStatus() As String
      Get
        Return GetProperty(EventStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventStatusProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared ClashesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Clashes, "Clashes", "")
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Clashes")>
    Public Property Clashes() As String
      Get
        Return GetProperty(ClashesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ClashesProperty, Value)
      End Set
    End Property

    Public Property ImportResult As Singular.Web.Result

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(EquipmentScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EventStatus.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Room Allocator")
        Else
          Return String.Format("Blank {0}", "Room Allocator")
        End If
      Else
        Return Me.EventStatus
      End If

    End Function

    Public Sub AllocateEquipment()

      Dim cmd As New Singular.CommandProc("CmdProcs.cmdImportEquipmentScheduleBasic",
                                         {"@EquipmentID", "@CallTime", "@StartTime",
                                          "@EndTime", "@WrapTime", "@GenreSeries",
                                          "@Title", "@ModifiedBy", "@SystemID",
                                          "@ProductionAreaID", "@ProductionID",
                                          "@GenRefNumber", "@FeedTypeID",
                                          "@SuppressFeedback", "@ResourceID"},
                                         {NothingDBNull(EquipmentID), NothingDBNull(CallTime),
                                          NothingDBNull(StartTime), NothingDBNull(EndTime),
                                          NothingDBNull(WrapTime), GenreDesc & " (" & SeriesTitle & ")", Title,
                                          OBLib.Security.Settings.CurrentUserID,
                                          NothingDBNull(SystemID), NothingDBNull(ProductionAreaID),
                                          NothingDBNull(ProductionID), NothingDBNull(GenRefNumber),
                                          NothingDBNull(FeedTypeID), False,
                                          NothingDBNull(ResourceID)})

      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd.UseTransaction = True
      cmd = cmd.Execute

      'Header Results
      Dim headerRow = cmd.Dataset.Tables(0).Rows(0)
      Dim Success As Boolean = False
      Dim HasClashes As Boolean = False

      Success = headerRow(0)
      HasClashes = headerRow(1)
      LoadProperty(EquipmentScheduleIDProperty, headerRow(2))
      LoadProperty(FeedIDProperty, headerRow(3))
      LoadProperty(ResourceBookingIDProperty, headerRow(4))

      If Success Then
        Me.ImportResult = New Singular.Web.Result(True)
      Else
        If HasClashes Then
          Me.ImportResult = New Singular.Web.Result(False) With {.ErrorText = "Clashes were found"}
        ElseIf EquipmentScheduleID Is Nothing Then
          Me.ImportResult = New Singular.Web.Result(False) With {.ErrorText = "Equipment Booking could not be created"}
        ElseIf FeedID Is Nothing Then
          Me.ImportResult = New Singular.Web.Result(False) With {.ErrorText = "Feed could not be added"}
        ElseIf ResourceBookingID Is Nothing Then
          Me.ImportResult = New Singular.Web.Result(False) With {.ErrorText = "Resource Booking could not be created"}
        Else
          Me.ImportResult = New Singular.Web.Result(False) With {.ErrorText = "Error could not be determined"}
        End If
      End If

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(EquipmentIDProperty)
        .AddTriggerProperties({FeedTypeIDProperty})
        .JavascriptRuleFunctionName = "EquipmentScheduleBasicBO.EquipmentIDValid"
        .AffectedProperties.Add(FeedTypeIDProperty)
      End With

      With AddWebRule(CallTimeProperty)
        .AffectedProperties.AddRange({StartTimeProperty, EndTimeProperty, WrapTimeProperty})
        .AddTriggerProperties({StartTimeProperty, EndTimeProperty, WrapTimeProperty})
        .JavascriptRuleFunctionName = "EquipmentScheduleBasicBO.CallTimeValid"
        .ServerRuleFunction = AddressOf CallTimeValid
      End With

      With AddWebRule(ClashesProperty)
        .AffectedProperties.AddRange({EquipmentIDProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
        .AddTriggerProperties({EquipmentIDProperty, StartTimeProperty, EndTimeProperty, WrapTimeProperty})
        .JavascriptRuleFunctionName = "EquipmentScheduleBasicBO.ClashesValid"
        .ServerRuleFunction = AddressOf ClashesValid
      End With

    End Sub

    Public Shared Function CallTimeValid(EquipmentSchedule As EquipmentScheduleBasic) As String
      'If Helpers Then
      '  EquipmentSchedule
      'End If
      Return ""
    End Function

    Public Shared Function ClashesValid(EquipmentSchedule As EquipmentScheduleBasic) As String
      If EquipmentSchedule.Clashes.Trim.Length > 0 Then
        Return EquipmentSchedule.Clashes
      End If
      Return ""
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipmentScheduleBasic() method.

    End Sub

    Public Shared Function NewEquipmentScheduleBasic() As EquipmentScheduleBasic

      Return DataPortal.CreateChild(Of EquipmentScheduleBasic)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEquipmentScheduleBasic(dr As SafeDataReader) As EquipmentScheduleBasic

      Dim r As New EquipmentScheduleBasic()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(EventStatusProperty, .GetString(2))
          LoadProperty(GenRefNumberProperty, .GetInt64(7))
          'LoadProperty(GenreSeriesProperty, .GetString(8))
          LoadProperty(TitleProperty, .GetString(9))
          LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(CallTimeProperty, .GetValue(15))
          LoadProperty(WrapTimeProperty, .GetValue(16))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(20)))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(22)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(23)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "cmdProcs.cmdResourceSchedulerAllocateRoom"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.cmdResourceSchedulerAllocateRoom"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
          If Me.IsNew Then
            paramResourceBookingID.Direction = ParameterDirection.Output
          End If
          Dim paramRoomScheduleID As SqlParameter = .Parameters.Add("@RoomScheduleID", SqlDbType.Int)
          paramRoomScheduleID.Value = GetProperty(EquipmentScheduleIDProperty)
          If Me.IsNew Then
            paramRoomScheduleID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@EventStatus", GetProperty(EventStatusProperty))
          .Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
          .Parameters.AddWithValue("@GenreSeries", GenreDesc & " (" & SeriesTitle & ")")
          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@EquipmentID", GetProperty(EquipmentIDProperty))
          .Parameters.AddWithValue("@CallTime", (New SmartDate(GetProperty(CallTimeProperty))).DBValue)
          .Parameters.AddWithValue("@WrapTime", (New SmartDate(GetProperty(WrapTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ProductionID", NothingDBNull(GetProperty(ProductionIDProperty)))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .ExecuteNonQuery()

          'If Me.IsNew Then
          '  LoadProperty(ImportedEventChannelIDProperty, paramImportedEventChannelID.Value)
          'End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEquipmentScheduleBasic"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@EquipmentScheduleID", GetProperty(EquipmentScheduleIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace