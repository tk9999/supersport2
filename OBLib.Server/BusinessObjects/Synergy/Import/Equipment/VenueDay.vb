﻿' Generated 06 May 2017 21:53 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy.Equipment

  <Serializable()> _
  Public Class VenueDay
    Inherits OBBusinessBase(Of VenueDay)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return VenueDayBO.VenueDayBOToString(self)")

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VenueProperty, Value)
      End Set
    End Property

    Public Shared VenueDayProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.VenueDay, "Venue Day")
    ''' <summary>
    ''' Gets and sets the Venue Day value
    ''' </summary>
    <Display(Name:="Venue Day", Description:=""),
    Required(ErrorMessage:="Venue Day required")>
  Public Property VenueDay As Date
      Get
        Return GetProperty(VenueDayProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(VenueDayProperty, Value)
      End Set
    End Property

    Public Shared StartBufferProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartBuffer, "Start Buffer")
    ''' <summary>
    ''' Gets and sets the Start Buffer value
    ''' </summary>
    <Display(Name:="Start Buffer", Description:=""),
    Required(ErrorMessage:="Start Buffer required")>
  Public Property StartBuffer As Date
      Get
        Return GetProperty(StartBufferProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(StartBufferProperty, Value)
      End Set
    End Property

    Public Shared FirstEventStartProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.FirstEventStart, "First Event Start")
    ''' <summary>
    ''' Gets and sets the First Event Start value
    ''' </summary>
    <Display(Name:="First Event Start", Description:=""),
    Required(ErrorMessage:="First Event Start required")>
  Public Property FirstEventStart As Date
      Get
        Return GetProperty(FirstEventStartProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(FirstEventStartProperty, Value)
      End Set
    End Property

    Public Shared LastEventEndProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.LastEventEnd, "Last Event End")
    ''' <summary>
    ''' Gets and sets the Last Event End value
    ''' </summary>
    <Display(Name:="Last Event End", Description:=""),
    Required(ErrorMessage:="Last Event End required")>
  Public Property LastEventEnd As Date
      Get
        Return GetProperty(LastEventEndProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(LastEventEndProperty, Value)
      End Set
    End Property

    Public Shared EndBufferProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndBuffer, "End Buffer")
    ''' <summary>
    ''' Gets and sets the End Buffer value
    ''' </summary>
    <Display(Name:="End Buffer", Description:=""),
    Required(ErrorMessage:="End Buffer required")>
  Public Property EndBuffer As Date
      Get
        Return GetProperty(EndBufferProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(EndBufferProperty, Value)
      End Set
    End Property

    Public Shared EventCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventCount, "Event Count")
    ''' <summary>
    ''' Gets and sets the Event Count value
    ''' </summary>
    <Display(Name:="Event Count", Description:="")>
  Public Property EventCount() As Integer
      Get
        Return GetProperty(EventCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EventCountProperty, Value)
      End Set
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentID, "Equipment", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:=""),
    Required(ErrorMessage:="Equipment required")>
    Public Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared VenueDayGroupProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VenueDayGroup, "Venue Day Group")
    ''' <summary>
    ''' Gets and sets the Venue Day Group value
    ''' </summary>
    <Display(Name:="Venue Day Group", Description:="")>
  Public Property VenueDayGroup() As Integer
      Get
        Return GetProperty(VenueDayGroupProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(VenueDayGroupProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VenueProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Venue.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Venue Day")
        Else
          Return String.Format("Blank {0}", "Venue Day")
        End If
      Else
        Return Me.Venue
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewVenueDay() method.

    End Sub

    Public Shared Function NewVenueDay() As VenueDay

      Return DataPortal.CreateChild(Of VenueDay)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetVenueDay(dr As SafeDataReader) As VenueDay

      Dim v As New VenueDay()
      v.Fetch(dr)
      Return v

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(VenueProperty, .GetString(0))
          LoadProperty(VenueDayProperty, .GetValue(1))
          LoadProperty(StartBufferProperty, .GetValue(2))
          LoadProperty(FirstEventStartProperty, .GetValue(3))
          LoadProperty(LastEventEndProperty, .GetValue(4))
          LoadProperty(EndBufferProperty, .GetValue(5))
          LoadProperty(EventCountProperty, .GetInt32(6))
          LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(VenueDayGroupProperty, .GetInt32(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      'AddPrimaryKeyParam(cm, VenueProperty)

      'cm.Parameters.AddWithValue("@VenueDay", VenueDay)
      'cm.Parameters.AddWithValue("@StartBuffer", StartBuffer)
      'cm.Parameters.AddWithValue("@FirstEventStart", FirstEventStart)
      'cm.Parameters.AddWithValue("@LastEventEnd", LastEventEnd)
      'cm.Parameters.AddWithValue("@EndBuffer", EndBuffer)
      'cm.Parameters.AddWithValue("@EventCount", GetProperty(EventCountProperty))
      'cm.Parameters.AddWithValue("@EquipmentID", GetProperty(EquipmentIDProperty))
      'cm.Parameters.AddWithValue("@VenueDayGroup", GetProperty(VenueDayGroupProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(VenueProperty, cm.Parameters("@Venue").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@Venue", GetProperty(VenueProperty))
    End Sub

#End Region

  End Class

End Namespace