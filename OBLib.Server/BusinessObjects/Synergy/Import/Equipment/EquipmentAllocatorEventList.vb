﻿' Generated 11 Feb 2016 00:12 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Equipment

  <Serializable()> _
  Public Class EquipmentAllocatorEventList
    Inherits OBBusinessListBase(Of EquipmentAllocatorEventList, EquipmentAllocatorEvent)

#Region " Business Methods "

    Public Function GetItem(VenueDayGroup As Integer) As EquipmentAllocatorEvent

      For Each child As EquipmentAllocatorEvent In Me
        If child.VenueDayGroup = VenueDayGroup Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetEquipmentAllocatorEventChannel(ImportedEventID As Integer) As EquipmentAllocatorEventChannel

      Dim obj As EquipmentAllocatorEventChannel = Nothing
      For Each parent As EquipmentAllocatorEvent In Me
        obj = parent.EquipmentAllocatorEventChannelList.GetItem(ImportedEventID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewEquipmentAllocatorEventList() As EquipmentAllocatorEventList

      Return New EquipmentAllocatorEventList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As EquipmentAllocatorEvent In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As EquipmentAllocatorEvent In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace