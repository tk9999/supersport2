﻿' Generated 27 Jan 2016 09:57 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.Equipment.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Equipment

  <Serializable()> _
  Public Class EquipmentAllocator
    Inherits OBBusinessBase(Of EquipmentAllocator)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ImportedEventChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventChannelID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public Property ImportedEventChannelID() As Integer
      Get
        Return GetProperty(ImportedEventChannelIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ImportedEventChannelIDProperty, Value)
      End Set
    End Property

    Public Shared ImportedEventIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventID, "Imported Event")
    ''' <summary>
    ''' Gets the Imported Event value
    ''' </summary>
    <Display(Name:="Imported Event", Description:="")>
    Public Property ImportedEventID() As Integer
      Get
        Return GetProperty(ImportedEventIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ImportedEventIDProperty, Value)
      End Set
    End Property

    Public Shared EventStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventStatus, "Status")
    ''' <summary>
    ''' Gets the Event Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property EventStatus() As String
      Get
        Return GetProperty(EventStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventStatusProperty, Value)
      End Set
    End Property

    Public Shared ScheduleDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleDateTime, "Schedule Start")
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Schedule Start", Description:=""),
    Required(ErrorMessage:="Schedule Date Time required")>
    Public Property ScheduleDateTime As DateTime?
      Get
        Return GetProperty(ScheduleDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleEndDate, "Schedule End")
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="Schedule"),
    Required(ErrorMessage:="Schedule End Date required")>
    Public Property ScheduleEndDate As DateTime?
      Get
        Return GetProperty(ScheduleEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleEndDateProperty, Value)
      End Set
    End Property

    <Display(Name:="Start")>
    Public Property ScheduleStartString As String
      Get
        If ScheduleDateTime IsNot Nothing Then
          Return ScheduleDateTime.Value.ToString("dd MMM yy HH:mm")
        End If
        Return ""
      End Get
      Set(ByVal Value As String)

      End Set
    End Property

    <Display(Name:="End")>
    Public Property ScheduleEndString As String
      Get
        If ScheduleEndDate IsNot Nothing Then
          Return ScheduleEndDate.Value.ToString("dd MMM yy HH:mm")
        End If
        Return ""
      End Get
      Set(ByVal Value As String)

      End Set
    End Property

    Public Shared OnAirStringProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.OnAirString, "")
    <Display(Name:="On Air")>
    Public Property OnAirString As String
      Get
        Return GetProperty(OnAirStringProperty)
      End Get
      Set(value As String)
        SetProperty(OnAirStringProperty, value)
      End Set
    End Property

    Public ReadOnly Property EndsNextDay As Boolean
      Get
        If ScheduleDateTime IsNot Nothing AndAlso ScheduleEndDate IsNot Nothing Then
          If ScheduleDateTime.Value.Date < ScheduleEndDate.Value.Date Then
            Return True
          End If
        End If
        Return False
      End Get
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel")
    ''' <summary>
    ''' Gets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelShortNameProperty, Value)
      End Set
    End Property

    Public Shared IsPrimaryChannelProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsPrimaryChannel, "Is Primary Channel", False)
    ''' <summary>
    ''' Gets the Is Primary Channel value
    ''' </summary>
    <Display(Name:="Is Primary Channel", Description:="")>
    Public Property IsPrimaryChannel() As Boolean
      Get
        Return GetProperty(IsPrimaryChannelProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsPrimaryChannelProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref")
    ''' <summary>
    ''' Gets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:="")>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    <Display(Name:="Gen Ref")>
    Public Property GenRefDisplay() As String
      Get
        If RepRnk = 1 Then
          Return GenRefNumber.ToString
        End If
        Return ""
      End Get
      Set(ByVal Value As String)

      End Set
    End Property

    Public Shared GenreSeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreSeries, "Genre (Series)")
    ''' <summary>
    ''' Gets the Genre Series value
    ''' </summary>
    <Display(Name:="Genre (Series)", Description:="")>
    Public Property GenreSeries() As String
      Get
        Return GetProperty(GenreSeriesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreSeriesProperty, Value)
      End Set
    End Property

    <Display(Name:="Genre (Series)", Description:="")>
    Public Property GenreSeriesDisplay() As String
      Get
        If RepRnk = 1 Then
          If GenreSeries.Length > 40 Then
            Return GenreSeries.Substring(0, 37) & "..."
          End If
          Return GenreSeries
        End If
        Return ""
      End Get
      Set(ByVal Value As String)

      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    <Display(Name:="Title", Description:="")>
    Public Property TitleDisplay() As String
      Get
        If RepRnk = 1 Then
          If Title.Length > 40 Then
            Return Title.Substring(0, 37) & "..."
          End If
          Return Title
        End If
        Return ""
      End Get
      Set(ByVal Value As String)

      End Set
    End Property

    Public Shared HighlightsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HighlightsInd, "Highlights", False)
    ''' <summary>
    ''' Gets the Highlights value
    ''' </summary>
    <Display(Name:="Highlights", Description:="")>
    Public Property HighlightsInd() As Boolean
      Get
        Return GetProperty(HighlightsIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HighlightsIndProperty, Value)
      End Set
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LiveDate, "Live")
    ''' <summary>
    ''' Gets the Live Date value
    ''' </summary>
    <Display(Name:="Live", Description:="")>
    Public Property LiveDate As DateTime?
      Get
        Return GetProperty(LiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LiveDateProperty, Value)
      End Set
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue")
    ''' <summary>
    ''' Gets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VenueProperty, Value)
      End Set
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationProperty, Value)
      End Set
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:="")>
    Public Property RowNo() As Int64
      Get
        Return GetProperty(RowNoProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(RowNoProperty, Value)
      End Set
    End Property

    Public Shared RepRnkProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.RepRnk, "Row No")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:="")>
    Public Property RepRnk() As Int64
      Get
        Return GetProperty(RepRnkProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(RepRnkProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionID, "Production", Nothing)
    ''' <summary>
    ''' Gets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared BookingCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BookingCount, "Bookings")
    ''' <summary>
    ''' Gets the Booking Count value
    ''' </summary>
    <Display(Name:="Bookings", Description:="")>
    Public ReadOnly Property BookingCount() As Integer
      Get
        Return GetProperty(BookingCountProperty)
      End Get
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.EquipmentID, Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Circuit"),
    DropDownWeb(GetType(ROCircuitAvailabilityList),
                BeforeFetchJS:="EquipmentAllocatorBO.setCircuitCriteriaBeforeRefresh",
                PreFindJSFunction:="EquipmentAllocatorBO.triggerCircuitAutoPopulate",
                AfterFetchJS:="EquipmentAllocatorBO.afterCircuitRefreshAjax",
                OnItemSelectJSFunction:="EquipmentAllocatorBO.onCircuitSelected",
                LookupMember:="EquipmentName", DisplayMember:="EquipmentName", ValueMember:="ResourceID",
                DropDownCssClass:="circuit-dropdown", DropDownColumns:={"EquipmentName", "IsAvailableString"}),
    SetExpression("EquipmentAllocatorBO.EquipmentIDSet(self)")>
    Public Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(ResourceIDProperty, value)
      End Set
    End Property

    Public Shared EquipmentNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EquipmentName, "Circuit")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Circuit", Description:="")>
    Public Property EquipmentName() As String
      Get
        Return GetProperty(EquipmentNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EquipmentNameProperty, Value)
      End Set
    End Property

    Public Shared CircuitClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CircuitClashCount, "Circuit", 0)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Circuit")>
    Public Property CircuitClashCount() As Integer
      Get
        Return GetProperty(CircuitClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CircuitClashCountProperty, Value)
      End Set
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.CallTime, "Call Time")
    ''' <summary>
    ''' Gets and sets the Call Time value
    ''' </summary>
    <Display(Name:="Call Time")>
    Public Property CallTime As DateTime?
      Get
        Return GetProperty(CallTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeProperty, Value)
      End Set
    End Property

    Public Shared OnAirStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OnAirStartDateTime, "Event Start")
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Event Start", Description:=""),
    Required(ErrorMessage:="Event Start is required")>
    Public Property OnAirStartDateTime As DateTime?
      Get
        Return GetProperty(OnAirStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OnAirStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared OnAirEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OnAirEndDateTime, "Event End")
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="Event End"),
    Required(ErrorMessage:="Event End is required")>
    Public Property OnAirEndDateTime As DateTime?
      Get
        Return GetProperty(OnAirEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(OnAirEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.WrapTime, "Wrap Time")
    ''' <summary>
    ''' Gets and sets the Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time")>
    Public Property WrapTime As DateTime?
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return EquipmentAllocatorBO.GetToString(self)")

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingID, "ResourceBookingID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="ResourceBookingID", Description:="")>
    Public Property ResourceBookingID() As Integer?
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentScheduleID, "EquipmentScheduleID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="EquipmentScheduleID", Description:="")>
    Public Property EquipmentScheduleID() As Integer?
      Get
        Return GetProperty(EquipmentScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared FeedTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedTypeID, "Feed Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Feed Type"), DropDownWeb(GetType(ROFeedTypeList))>
    Public Property FeedTypeID() As Integer?
      Get
        Return GetProperty(FeedTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedTypeIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "SystemID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="SystemID", Description:="")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "ProductionAreaID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="ProductionAreaID", Description:="")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ButtonStyleCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ButtonStyleCssClass, "ButtonStyleCssClass")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="ButtonStyleCssClass", Description:=""), AlwaysClean>
    Public Property ButtonStyleCssClass() As String
      Get
        Return GetProperty(ButtonStyleCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ButtonStyleCssClassProperty, Value)
      End Set
    End Property

    Public Shared BookingTimesProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.BookingTimes, "")
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Booking Times", Description:=""), AlwaysClean>
    Public Property BookingTimes As String
      Get
        Return GetProperty(BookingTimesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BookingTimesProperty, Value)
      End Set
    End Property

    Public Shared TimesValidProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.TimesValid, True)
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Booking Times", Description:=""), AlwaysClean>
    Public Property TimesValid As Boolean
      Get
        Return GetProperty(TimesValidProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(TimesValidProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ImportedEventChannelIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EventStatus.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Room Allocator")
        Else
          Return String.Format("Blank {0}", "Room Allocator")
        End If
      Else
        Return Me.EventStatus
      End If

    End Function

    Public Sub LoadOnAir()

      If OnAirStartDateTime IsNot Nothing AndAlso OnAirEndDateTime IsNot Nothing Then
        LoadProperty(OnAirStringProperty, OnAirStartDateTime.Value.ToString("dd MMM yy HH:mm") & " - " & OnAirEndDateTime.Value.ToString("HH:mm"))
      Else
        LoadProperty(OnAirStringProperty, "")
      End If

    End Sub

    Public Function AllocateEquipment() As Singular.Web.Result

      Dim cmd As New Singular.CommandProc("CmdProcs.cmdResourceSchedulerAllocateEquipment",
                                         {"@EquipmentID",
                                          "@CallTime",
                                          "@OnAirTimeStart",
                                          "@OnAirTimeEnd",
                                          "@WrapTime",
                                          "@GenreSeries",
                                          "@Title",
                                          "@ModifiedBy",
                                          "@SystemID",
                                          "@ProductionAreaID",
                                          "@ProductionID",
                                          "@ImportedEventID",
                                          "@GenRefNumber",
                                          "@FeedTypeID",
                                          "@SuppressFeedback",
                                          "@ResourceID"},
                                         {NothingDBNull(EquipmentID),
                                          NothingDBNull(CallTime),
                                          NothingDBNull(OnAirStartDateTime),
                                          NothingDBNull(OnAirEndDateTime),
                                          NothingDBNull(WrapTime),
                                          GenreSeries,
                                          Title,
                                          OBLib.Security.Settings.CurrentUserID,
                                          NothingDBNull(SystemID),
                                          NothingDBNull(ProductionAreaID),
                                          NothingDBNull(ProductionID),
                                          NothingDBNull(ImportedEventID),
                                          NothingDBNull(GenRefNumber),
                                          NothingDBNull(FeedTypeID),
                                          False,
                                          NothingDBNull(ResourceID)})

      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute

      'Header Results
      Dim headerRow = cmd.Dataset.Tables(0).Rows(0)
      Dim Success As Boolean = False
      Dim HasClashes As Boolean = False
      Dim EquipmentScheduleAdded As Boolean = False
      Dim FeedAdded As Boolean = False
      Dim BookingAdded As Boolean = False

      Success = headerRow(0)
      HasClashes = headerRow(1)
      EquipmentScheduleAdded = headerRow(2)
      FeedAdded = headerRow(3)
      BookingAdded = headerRow(4)

      If Success Then
        Return New Singular.Web.Result(True)
      Else
        If HasClashes Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Clashes were found"}
        ElseIf EquipmentScheduleAdded Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Equipment Booking could not be created"}
        ElseIf FeedAdded Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Feed could not be added"}
        ElseIf BookingAdded Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Resource Booking could not be created"}
        End If
      End If

      Return New Singular.Web.Result(False) With {.ErrorText = "Error could not be determined"}

    End Function

    Public Sub LoadBookingTimes()
      Dim s As String = ""
      If CallTime IsNot Nothing Then
        s = CallTime.Value.ToString("dd MMM HH:mm")
      Else
        s = OnAirStartDateTime.Value.ToString("dd MMM HH:mm")
      End If
      If WrapTime IsNot Nothing Then
        s = s & " - " & WrapTime.Value.ToString("dd MMM HH:mm")
      Else
        s = s & " - " & OnAirEndDateTime.Value.ToString("dd MMM HH:mm")
      End If
      LoadProperty(BookingTimesProperty, s)
    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(EquipmentIDProperty)
        .AddTriggerProperties({FeedTypeIDProperty, CallTimeProperty, OnAirStartDateTimeProperty, OnAirEndDateTimeProperty, WrapTimeProperty})
        .JavascriptRuleFunctionName = "EquipmentAllocatorBO.EquipmentIDValid"
        .AffectedProperties.Add(FeedTypeIDProperty)
        .AffectedProperties.Add(CallTimeProperty)
        .AffectedProperties.Add(OnAirStartDateTimeProperty)
        .AffectedProperties.Add(OnAirEndDateTimeProperty)
        .AffectedProperties.Add(WrapTimeProperty)
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipmentAllocator() method.

    End Sub

    Public Shared Function NewEquipmentAllocator() As EquipmentAllocator

      Return DataPortal.CreateChild(Of EquipmentAllocator)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEquipmentAllocator(dr As SafeDataReader) As EquipmentAllocator

      Dim r As New EquipmentAllocator()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ImportedEventChannelIDProperty, .GetInt32(0))
          LoadProperty(ImportedEventIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EventStatusProperty, .GetString(2))
          LoadProperty(ScheduleDateTimeProperty, .GetValue(3))
          LoadProperty(ScheduleEndDateProperty, .GetValue(4))
          LoadProperty(ChannelShortNameProperty, .GetString(5))
          LoadProperty(IsPrimaryChannelProperty, .GetBoolean(6))
          LoadProperty(GenRefNumberProperty, .GetInt64(7))
          LoadProperty(GenreSeriesProperty, .GetString(8))
          LoadProperty(TitleProperty, .GetString(9))
          LoadProperty(HighlightsIndProperty, .GetBoolean(10))
          LoadProperty(LiveDateProperty, .GetValue(11))
          LoadProperty(VenueProperty, .GetString(12))
          LoadProperty(LocationProperty, .GetString(13))
          LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(CallTimeProperty, .GetValue(15))
          LoadProperty(WrapTimeProperty, .GetValue(16))
          LoadProperty(RowNoProperty, .GetInt32(17))
          LoadProperty(RepRnkProperty, .GetInt32(18))
          LoadProperty(ButtonStyleCssClassProperty, .GetString(19))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(20)))
          LoadProperty(BookingCountProperty, .GetInt32(21))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(22)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(23)))
          LoadProperty(OnAirStartDateTimeProperty, ScheduleDateTime)
          LoadProperty(OnAirEndDateTimeProperty, ScheduleEndDate)
        End With
      End Using
      LoadOnAir()
      LoadBookingTimes()

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "cmdProcs.cmdResourceSchedulerAllocateRoom"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.cmdResourceSchedulerAllocateRoom"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
          If Me.IsNew Then
            paramResourceBookingID.Direction = ParameterDirection.Output
          End If
          Dim paramRoomScheduleID As SqlParameter = .Parameters.Add("@RoomScheduleID", SqlDbType.Int)
          paramRoomScheduleID.Value = GetProperty(EquipmentScheduleIDProperty)
          If Me.IsNew Then
            paramRoomScheduleID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ImportedEventChannelID", GetProperty(ImportedEventChannelIDProperty))
          .Parameters.AddWithValue("@ImportedEventID", GetProperty(ImportedEventIDProperty))
          .Parameters.AddWithValue("@EventStatus", GetProperty(EventStatusProperty))
          .Parameters.AddWithValue("@ScheduleDateTime", (New SmartDate(GetProperty(ScheduleDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ScheduleEndDate", (New SmartDate(GetProperty(ScheduleEndDateProperty))).DBValue)
          .Parameters.AddWithValue("@ChannelShortName", GetProperty(ChannelShortNameProperty))
          .Parameters.AddWithValue("@IsPrimaryChannel", GetProperty(IsPrimaryChannelProperty))
          .Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
          .Parameters.AddWithValue("@GenreSeries", GetProperty(GenreSeriesProperty))
          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@HighlightsInd", GetProperty(HighlightsIndProperty))
          .Parameters.AddWithValue("@LiveDate", (New SmartDate(GetProperty(LiveDateProperty))).DBValue)
          .Parameters.AddWithValue("@Venue", GetProperty(VenueProperty))
          .Parameters.AddWithValue("@Location", GetProperty(LocationProperty))
          .Parameters.AddWithValue("@EquipmentID", GetProperty(EquipmentIDProperty))
          .Parameters.AddWithValue("@CallTime", (New SmartDate(GetProperty(CallTimeProperty))).DBValue)
          .Parameters.AddWithValue("@WrapTime", (New SmartDate(GetProperty(WrapTimeProperty))).DBValue)
          .Parameters.AddWithValue("@RowNo", GetProperty(RowNoProperty))
          .Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
          .Parameters.AddWithValue("@BookingCount", GetProperty(BookingCountProperty))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .ExecuteNonQuery()

          'If Me.IsNew Then
          '  LoadProperty(ImportedEventChannelIDProperty, paramImportedEventChannelID.Value)
          'End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEquipmentAllocator"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ImportedEventChannelID", GetProperty(ImportedEventChannelIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace