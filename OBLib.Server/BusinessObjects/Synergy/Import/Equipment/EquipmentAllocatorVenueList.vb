﻿' Generated 11 Feb 2016 00:12 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Company.ReadOnly
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Equipment

  <Serializable()> _
  Public Class EquipmentAllocatorVenueList
    Inherits OBBusinessListBase(Of EquipmentAllocatorVenueList, EquipmentAllocatorVenue)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(VenueDayGroup As Integer) As EquipmentAllocatorVenue

      For Each child As EquipmentAllocatorVenue In Me
        If child.VenueDayGroup = VenueDayGroup Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Public Function GetEquipmentAllocatorEvent(ImportedEventID As Integer) As EquipmentAllocatorEvent

      Dim obj As EquipmentAllocatorEvent = Nothing
      For Each parent As EquipmentAllocatorVenue In Me
        For Each child As EquipmentAllocatorEvent In parent.EquipmentAllocatorEventList
          If CompareSafe(child.ImportedEventID, ImportedEventID) Then
            Return child
          End If
        Next
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      <Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), Source:=Singular.DataAnnotations.DropDownWeb.SourceType.CommonData),
      Display(Name:="Sub-Dept"),
      Required(ErrorMessage:="Sub-Dept is required")>
      Public Property SystemID As Integer?

      <Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaList), Source:=Singular.DataAnnotations.DropDownWeb.SourceType.CommonData, ThisFilterMember:="SystemID"),
      Display(Name:="Area"),
      Required(ErrorMessage:="Area is required")>
      Public Property ProductionAreaID As Integer?

      <Required(ErrorMessage:="Start Date is required")>
      Public Property StartDate As DateTime?

      <Required(ErrorMessage:="End Date is required")>
      Public Property EndDate As DateTime?

      Public Shared LiveProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Live, "Live", True)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Live", Description:="")>
      Public Property Live() As Boolean
        Get
          Return ReadProperty(LiveProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(LiveProperty, Value)
        End Set
      End Property

      Public Shared DelayedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Delayed, "Delayed", True)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Delayed", Description:="")>
      Public Property Delayed() As Boolean
        Get
          Return ReadProperty(DelayedProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(DelayedProperty, Value)
        End Set
      End Property

      Public Shared PremierProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Premier, "Premier", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Premier", Description:="")>
      Public Property Premier() As Boolean
        Get
          Return ReadProperty(PremierProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(PremierProperty, Value)
        End Set
      End Property

      Public Shared GenRefNoProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNo, "Gen Ref No", Nothing)
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Gen Ref No", Description:="")>
      Public Property GenRefNo() As Int64?
        Get
          Return ReadProperty(GenRefNoProperty)
        End Get
        Set(ByVal Value As Int64?)
          LoadProperty(GenRefNoProperty, Value)
        End Set
      End Property

      Public Shared PrimaryChannelsOnlyProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.PrimaryChannelsOnly, False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Primary Channels Only?", Description:="")>
      Public Property PrimaryChannelsOnly() As Boolean
        Get
          Return ReadProperty(PrimaryChannelsOnlyProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(PrimaryChannelsOnlyProperty, Value)
        End Set
      End Property

      Public Shared SelectedChannelsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SelectedChannelsDisplay, "Selected Channels", "Select Channels")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Channels")>
      Public Property SelectedChannelsDisplay() As String
        Get
          Return ReadProperty(SelectedChannelsProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SelectedChannelsProperty, Value)
        End Set
      End Property

      Public Shared SelectedChannelIDsXMLProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.SelectedChannelIDsXML, "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Selected Channels")>
      Public Property SelectedChannelIDsXML() As String
        Get
          Return ReadProperty(SelectedChannelIDsXMLProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SelectedChannelIDsXMLProperty, Value)
        End Set
      End Property

      Public Shared GenreProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Genre, "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Genre")>
      Public Property Genre() As String
        Get
          Return ReadProperty(GenreProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(GenreProperty, Value)
        End Set
      End Property

      Public Shared SeriesProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Series, "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Series")>
      Public Property Series() As String
        Get
          Return ReadProperty(SeriesProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(SeriesProperty, Value)
        End Set
      End Property

      Public Shared TitleProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Title, "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Title")>
      Public Property Title() As String
        Get
          Return ReadProperty(TitleProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(TitleProperty, Value)
        End Set
      End Property

      Public Shared VenueProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Venue, "")
      ''' <summary>
      ''' Gets and sets the Gen Ref No value
      ''' </summary>
      <Display(Name:="Venue")>
      Public Property Venue() As String
        Get
          Return ReadProperty(VenueProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(VenueProperty, Value)
        End Set
      End Property

      Public Sub New(SystemID As Integer?, ProductionAreaID As Integer?,
                     StartDate As DateTime?, EndDate As DateTime?)

        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.StartDate = StartDate
        Me.EndDate = EndDate

      End Sub

      Public Sub New(StartDate As Date?, EndDate As Date?,
                     GenRefNo As Int64?, Live As Boolean, Delayed As Boolean,
                     Premier As Boolean, PageNo As Integer,
                     PageSize As Integer, SortColumn As String,
                     SortAsc As Boolean)
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.GenRefNo = GenRefNo
        Me.Live = Live
        Me.Delayed = Delayed
        Me.Premier = Premier
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortColumn = SortColumn
        Me.SortAsc = SortAsc
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewEquipmentAllocatorVenueList() As EquipmentAllocatorVenueList

      Return New EquipmentAllocatorVenueList()

    End Function

    Public Shared Sub BeginGetEquipmentAllocatorVenueList(CallBack As EventHandler(Of DataPortalResult(Of EquipmentAllocatorVenueList)))

      Dim dp As New DataPortal(Of EquipmentAllocatorVenueList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetEquipmentAllocatorVenueList() As EquipmentAllocatorVenueList

      Return DataPortal.Fetch(Of EquipmentAllocatorVenueList)(New Criteria())

    End Function

    Public Shared Function GetEquipmentAllocatorVenueList(StartDate As Date?, EndDate As Date?,
                                                          GenRefNo As Int64?, Live As Boolean, Delayed As Boolean,
                                                          Premier As Boolean, PageNo As Integer,
                                                          PageSize As Integer, SortColumn As String,
                                                          SortAsc As Boolean) As EquipmentAllocatorVenueList

      Return DataPortal.Fetch(Of EquipmentAllocatorVenueList)(New Criteria(StartDate, EndDate, GenRefNo, Live, Delayed, Premier,
                                                                           PageNo, PageSize, SortColumn, SortAsc))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      While sdr.Read
        Me.Add(EquipmentAllocatorVenue.GetEquipmentAllocatorVenue(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As EquipmentAllocatorVenue = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.VenueDayGroup <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.EquipmentAllocatorEventList.RaiseListChangedEvents = False
          parent.EquipmentAllocatorEventList.Add(EquipmentAllocatorEvent.GetEquipmentAllocatorEvent(sdr))
          parent.EquipmentAllocatorEventList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentChild As EquipmentAllocatorEvent = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.ImportedEventID <> sdr.GetInt32(0) Then
            parentChild = Me.GetEquipmentAllocatorEvent(sdr.GetInt32(0))
          End If
          parentChild.EquipmentAllocatorEventChannelList.RaiseListChangedEvents = False
          parentChild.EquipmentAllocatorEventChannelList.Add(EquipmentAllocatorEventChannel.GetEquipmentAllocatorEventChannel(sdr))
          parentChild.EquipmentAllocatorEventChannelList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As EquipmentAllocatorVenue In Me
        child.CheckRules()
        For Each EquipmentAllocatorEvent As EquipmentAllocatorEvent In child.EquipmentAllocatorEventList
          EquipmentAllocatorEvent.CheckRules()

          For Each EquipmentAllocatorEventChannel As EquipmentAllocatorEventChannel In EquipmentAllocatorEvent.EquipmentAllocatorEventChannelList
            EquipmentAllocatorEventChannel.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getEquipmentAllocatorListByVenue]"
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@GenRefNo", NothingDBNull(crit.GenRefNo))
            cm.Parameters.AddWithValue("@Live", crit.Live)
            cm.Parameters.AddWithValue("@Delayed", crit.Delayed)
            cm.Parameters.AddWithValue("@Premier", crit.Premier)
            cm.Parameters.AddWithValue("@PrimaryChannelsOnly", crit.PrimaryChannelsOnly)
            cm.Parameters.AddWithValue("@SelectedChannelIDs", Strings.MakeEmptyDBNull(crit.SelectedChannelIDsXML))
            cm.Parameters.AddWithValue("@Genre", Strings.MakeEmptyDBNull(crit.Genre))
            cm.Parameters.AddWithValue("@Series", Strings.MakeEmptyDBNull(crit.Series))
            cm.Parameters.AddWithValue("@Title", Strings.MakeEmptyDBNull(crit.Title))
            cm.Parameters.AddWithValue("@Venue", Strings.MakeEmptyDBNull(crit.Venue))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As EquipmentAllocatorVenue In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As EquipmentAllocatorVenue In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace