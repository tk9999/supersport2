﻿' Generated 06 May 2017 21:53 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy.Equipment

  <Serializable()> _
  Public Class VenueDayEvent
    Inherits OBBusinessBase(Of VenueDayEvent)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return VenueDayEventBO.VenueDayEventBOToString(self)")

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key,
    Required(ErrorMessage:="ID required")>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared GenreSeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreSeries, "Genre Series")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Genre Series", Description:="")>
    Public Property GenreSeries() As String
      Get
        Return GetProperty(GenreSeriesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreSeriesProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.LiveDate, "Live Date")
    ''' <summary>
    ''' Gets and sets the Live Date value
    ''' </summary>
    <Display(Name:="Live Date", Description:="")>
    Public Property LiveDate As Date
      Get
        Return GetProperty(LiveDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(LiveDateProperty, Value)
      End Set
    End Property

    Public Shared FirstChannelStartProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.FirstChannelStart, "First Channel Start")
    ''' <summary>
    ''' Gets and sets the First Channel Start value
    ''' </summary>
    <Display(Name:="First Channel Start", Description:=""),
    Required(ErrorMessage:="First Channel Start required")>
    Public Property FirstChannelStart As Date
      Get
        Return GetProperty(FirstChannelStartProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(FirstChannelStartProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionID, "Production")
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:=""),
    Required(ErrorMessage:="Production required")>
    Public Property ProductionID() As Integer
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentID, "Equipment")
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:=""),
    Required(ErrorMessage:="Equipment required")>
    Public Property EquipmentID() As Integer
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared VenueDayGroupProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VenueDayGroup, "Venue Day Group")
    ''' <summary>
    ''' Gets and sets the Venue Day Group value
    ''' </summary>
    <Display(Name:="Venue Day Group", Description:="")>
    Public Property VenueDayGroup() As Integer
      Get
        Return GetProperty(VenueDayGroupProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(VenueDayGroupProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(GenRefNumberProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.GenreSeries.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Venue Day Event")
        Else
          Return String.Format("Blank {0}", "Venue Day Event")
        End If
      Else
        Return Me.GenreSeries
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewVenueDayEvent() method.

    End Sub

    Public Shared Function NewVenueDayEvent() As VenueDayEvent

      Return DataPortal.CreateChild(Of VenueDayEvent)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetVenueDayEvent(dr As SafeDataReader) As VenueDayEvent

      Dim v As New VenueDayEvent()
      v.Fetch(dr)
      Return v

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(GenRefNumberProperty, .GetInt32(0))
          LoadProperty(GenreSeriesProperty, .GetString(1))
          LoadProperty(TitleProperty, .GetString(2))
          LoadProperty(LiveDateProperty, .GetValue(3))
          LoadProperty(FirstChannelStartProperty, .GetValue(4))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(VenueDayGroupProperty, .GetInt32(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      ' AddPrimaryKeyParam(cm, GenRefNumberProperty)

      'cm.Parameters.AddWithValue("@GenreSeries", GetProperty(GenreSeriesProperty))
      'cm.Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      'cm.Parameters.AddWithValue("@LiveDate", LiveDate)
      'cm.Parameters.AddWithValue("@FirstChannelStart", FirstChannelStart)
      'cm.Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
      'cm.Parameters.AddWithValue("@EquipmentID", GetProperty(EquipmentIDProperty))
      'cm.Parameters.AddWithValue("@VenueDayGroup", GetProperty(VenueDayGroupProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(GenRefNumberProperty, cm.Parameters("@GenRefNumber").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
    End Sub

#End Region

  End Class

End Namespace