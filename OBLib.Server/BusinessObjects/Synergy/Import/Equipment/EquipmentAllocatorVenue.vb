﻿' Generated 11 Feb 2016 00:12 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.Equipment.ReadOnly
Imports Singular
Imports Singular.Misc

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Equipment

  <Serializable()> _
  Public Class EquipmentAllocatorVenue
    Inherits OBBusinessBase(Of EquipmentAllocatorVenue)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="IsExpanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared VenueDayGroupProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VenueDayGroup, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key>
    Public Property VenueDayGroup() As Integer
      Get
        Return GetProperty(VenueDayGroupProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(VenueDayGroupProperty, Value)
      End Set
    End Property

    Public Shared VenueDayProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.VenueDay, "Day")
    ''' <summary>
    ''' Gets and sets the Venue Day value
    ''' </summary>
    <Display(Name:="Day", Description:=""),
    Required(ErrorMessage:="Day required")>
    Public Property VenueDay As DateTime?
      Get
        Return GetProperty(VenueDayProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(VenueDayProperty, Value)
      End Set
    End Property

    <Display(Name:="Day")>
    Public ReadOnly Property VenueDayString As String
      Get
        If VenueDayRowNum = 1 Then
          Return VenueDay.Value.ToString("ddd dd MMM yy")
        End If
        Return ""
      End Get
    End Property

    Public Shared StartBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartBuffer, "Start Buffer")
    ''' <summary>
    ''' Gets and sets the Start Buffer value
    ''' </summary>
    <Display(Name:="Start Buffer", Description:=""),
    Required(ErrorMessage:="Start Buffer required")>
    Public Property StartBuffer As DateTime?
      Get
        Return GetProperty(StartBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartBufferProperty, Value)
      End Set
    End Property

    Public Shared FirstEventStartProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.FirstEventStart, "First Event Start")
    ''' <summary>
    ''' Gets and sets the First Event Start value
    ''' </summary>
    <Display(Name:="First Event Start", Description:=""),
    Required(ErrorMessage:="First Event Start required")>
    Public Property FirstEventStart As DateTime?
      Get
        Return GetProperty(FirstEventStartProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(FirstEventStartProperty, Value)
      End Set
    End Property

    Public Shared LastEventEndProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LastEventEnd, "Last Event End")
    ''' <summary>
    ''' Gets and sets the Last Event End value
    ''' </summary>
    <Display(Name:="Last Event End", Description:=""),
    Required(ErrorMessage:="Last Event End required")>
    Public Property LastEventEnd As DateTime?
      Get
        Return GetProperty(LastEventEndProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LastEventEndProperty, Value)
      End Set
    End Property

    Public Shared EndBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndBuffer, "End Buffer")
    ''' <summary>
    ''' Gets and sets the End Buffer value
    ''' </summary>
    <Display(Name:="End Buffer", Description:=""),
    Required(ErrorMessage:="End Buffer required")>
    Public Property EndBuffer As DateTime?
      Get
        Return GetProperty(EndBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndBufferProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionVenueID, "Production Venue")
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' </summary>
    <Display(Name:="Production Venue", Description:="")>
    Public Property ProductionVenueID() As Integer
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue")
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VenueProperty, Value)
      End Set
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationProperty, Value)
      End Set
    End Property

    Public Shared EventCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventCount, "Event Count")
    ''' <summary>
    ''' Gets and sets the Event Count value
    ''' </summary>
    <Display(Name:="Event Count", Description:=""),
    Required(ErrorMessage:="Event Count required")>
    Public Property EventCount() As Integer
      Get
        Return GetProperty(EventCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EventCountProperty, Value)
      End Set
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EquipmentID, "Equipment")
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Circuit"),
    DropDownWeb(GetType(ROSatOpsEquipmentList))>
    Public Property EquipmentID() As Integer
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared FeedTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.FeedTypeID, "Feed Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Feed Type"), DropDownWeb(GetType(ROFeedTypeList))>
    Public Property FeedTypeID() As Integer?
      Get
        Return GetProperty(FeedTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(FeedTypeIDProperty, Value)
      End Set
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets and sets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:="")>
    Public Property RowNo() As Integer
      Get
        Return GetProperty(RowNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RowNoProperty, Value)
      End Set
    End Property

    <Display(Name:="Start and End Times")>
    Public ReadOnly Property StartEndTimesString As String
      Get
        If FirstEventStart IsNot Nothing And LastEventEnd IsNot Nothing Then
          Return FirstEventStart.Value.ToString("ddd dd MMM yy HH:mm") & " - " & LastEventEnd.Value.ToString("ddd dd MMM yy HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared VenueDayRowNumProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VenueDayRowNum, "Row No")
    ''' <summary>
    ''' Gets and sets the Row No value
    ''' </summary>
    <Display(Name:="Row No")>
    Public Property VenueDayRowNum() As Integer
      Get
        Return GetProperty(VenueDayRowNumProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(VenueDayRowNumProperty, Value)
      End Set
    End Property

    Public Shared FeedNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FeedName, "Feed Name")
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="Feed Name", Description:="")>
    Public Property FeedName() As String
      Get
        Return GetProperty(FeedNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(FeedNameProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "SystemID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="SystemID", Description:="")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "ProductionAreaID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="ProductionAreaID", Description:="")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return EquipmentAllocatorVenueBO.GetToString(self)")


#End Region

#Region " Child Lists "

    Public Shared EquipmentAllocatorEventListProperty As PropertyInfo(Of EquipmentAllocatorEventList) = RegisterProperty(Of EquipmentAllocatorEventList)(Function(c) c.EquipmentAllocatorEventList, "Equipment Allocator Event List")

    Public ReadOnly Property EquipmentAllocatorEventList() As EquipmentAllocatorEventList
      Get
        If GetProperty(EquipmentAllocatorEventListProperty) Is Nothing Then
          LoadProperty(EquipmentAllocatorEventListProperty, OBLib.Equipment.EquipmentAllocatorEventList.NewEquipmentAllocatorEventList())
        End If
        Return GetProperty(EquipmentAllocatorEventListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(VenueDayGroupProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Venue.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Equipment Allocator Venue")
        Else
          Return String.Format("Blank {0}", "Equipment Allocator Venue")
        End If
      Else
        Return Me.Venue
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

    Public Function AllocateEquipment() As Singular.Web.Result

      Dim GenRefNumbers As String
      Dim GenRefList As New List(Of Int64)
      For Each p As EquipmentAllocatorEvent In Me.EquipmentAllocatorEventList
        GenRefList.Add(p.GenRefNumber)
      Next
      GenRefNumbers = OBMisc.Int64ListToXML(GenRefList)

      Dim cmd As New Singular.CommandProc("CmdProcs.cmdResourceSchedulerAllocateEquipmentVenue",
                                         {"@EquipmentID",
                                          "@CallTime",
                                          "@OnAirTimeStart",
                                          "@OnAirTimeEnd",
                                          "@WrapTime",
                                          "@ModifiedBy",
                                          "@SystemID",
                                          "@ProductionAreaID",
                                          "@GenRefNumbers",
                                          "@FeedTypeID",
                                          "@FeedName",
                                          "@SuppressFeedback"},
                                         {NothingDBNull(EquipmentID),
                                          NothingDBNull(StartBuffer),
                                          NothingDBNull(FirstEventStart),
                                          NothingDBNull(LastEventEnd),
                                          NothingDBNull(EndBuffer),
                                          OBLib.Security.Settings.CurrentUserID,
                                          NothingDBNull(SystemID),
                                          NothingDBNull(ProductionAreaID),
                                          Singular.Strings.MakeEmptyDBNull(GenRefNumbers),
                                          NothingDBNull(FeedTypeID),
                                          Singular.Strings.MakeEmptyDBNull(Me.FeedName),
                                          False})

      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute

      'Header Results
      Dim headerRow = cmd.Dataset.Tables(0).Rows(0)
      Dim Success As Boolean = False
      Dim HasClashes As Boolean = False
      Dim EquipmentScheduleAdded As Boolean = False
      Dim FeedAdded As Boolean = False
      Dim BookingAdded As Boolean = False

      Success = headerRow(0)
      HasClashes = headerRow(1)
      EquipmentScheduleAdded = headerRow(2)
      FeedAdded = headerRow(3)
      BookingAdded = headerRow(4)

      If Success Then
        Return New Singular.Web.Result(True)
      Else
        If HasClashes Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Clashes were found"}
        ElseIf EquipmentScheduleAdded Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Equipment Booking could not be created"}
        ElseIf FeedAdded Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Feed could not be added"}
        ElseIf BookingAdded Then
          Return New Singular.Web.Result(False) With {.ErrorText = "Resource Booking could not be created"}
        End If
      End If

      Return New Singular.Web.Result(False) With {.ErrorText = "Error could not be determined"}

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(EquipmentIDProperty)
        .AddTriggerProperties({FeedTypeIDProperty, FeedNameProperty, StartBufferProperty, FirstEventStartProperty, LastEventEndProperty, EndBufferProperty})
        .JavascriptRuleFunctionName = "EquipmentAllocatorVenueBO.EquipmentIDValid"
        .AffectedProperties.Add(FeedTypeIDProperty)
        .AffectedProperties.Add(StartBufferProperty)
        .AffectedProperties.Add(FirstEventStartProperty)
        .AffectedProperties.Add(LastEventEndProperty)
        .AffectedProperties.Add(EndBufferProperty)
        .AffectedProperties.Add(FeedNameProperty)
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewEquipmentAllocatorVenue() method.

    End Sub

    Public Shared Function NewEquipmentAllocatorVenue() As EquipmentAllocatorVenue

      Return DataPortal.CreateChild(Of EquipmentAllocatorVenue)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetEquipmentAllocatorVenue(dr As SafeDataReader) As EquipmentAllocatorVenue

      Dim e As New EquipmentAllocatorVenue()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(VenueDayGroupProperty, .GetInt32(0))
          LoadProperty(VenueDayProperty, .GetValue(1))
          LoadProperty(StartBufferProperty, .GetValue(2))
          LoadProperty(FirstEventStartProperty, .GetValue(3))
          LoadProperty(LastEventEndProperty, .GetValue(4))
          LoadProperty(EndBufferProperty, .GetValue(5))
          LoadProperty(ProductionVenueIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(VenueProperty, .GetString(7))
          LoadProperty(LocationProperty, .GetString(8))
          LoadProperty(EventCountProperty, .GetInt32(9))
          LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(RowNoProperty, .GetInt32(11))
          LoadProperty(VenueDayRowNumProperty, .GetInt32(12))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(13)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(14)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insEquipmentAllocatorVenue"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updEquipmentAllocatorVenue"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramVenueDayGroup As SqlParameter = .Parameters.Add("@VenueDayGroup", SqlDbType.Int)
          paramVenueDayGroup.Value = GetProperty(VenueDayGroupProperty)
          If Me.IsNew Then
            paramVenueDayGroup.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@VenueDay", (New SmartDate(GetProperty(VenueDayProperty))).DBValue)
          .Parameters.AddWithValue("@StartBuffer", (New SmartDate(GetProperty(StartBufferProperty))).DBValue)
          .Parameters.AddWithValue("@FirstEventStart", (New SmartDate(GetProperty(FirstEventStartProperty))).DBValue)
          .Parameters.AddWithValue("@LastEventEnd", (New SmartDate(GetProperty(LastEventEndProperty))).DBValue)
          .Parameters.AddWithValue("@EndBuffer", (New SmartDate(GetProperty(EndBufferProperty))).DBValue)
          .Parameters.AddWithValue("@ProductionVenueID", GetProperty(ProductionVenueIDProperty))
          .Parameters.AddWithValue("@Venue", GetProperty(VenueProperty))
          .Parameters.AddWithValue("@Location", GetProperty(LocationProperty))
          .Parameters.AddWithValue("@EventCount", GetProperty(EventCountProperty))
          .Parameters.AddWithValue("@EquipmentID", GetProperty(EquipmentIDProperty))
          .Parameters.AddWithValue("@RowNo", GetProperty(RowNoProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(VenueDayGroupProperty, paramVenueDayGroup.Value)
          End If
          ' update child objects
          If GetProperty(EquipmentAllocatorEventListProperty) IsNot Nothing Then
            Me.EquipmentAllocatorEventList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(EquipmentAllocatorEventListProperty) IsNot Nothing Then
          Me.EquipmentAllocatorEventList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delEquipmentAllocatorVenue"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@VenueDayGroup", GetProperty(VenueDayGroupProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace