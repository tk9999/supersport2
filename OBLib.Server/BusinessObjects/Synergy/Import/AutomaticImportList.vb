﻿' Generated 30 Sep 2017 17:59 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class AutomaticImportList
    Inherits OBBusinessListBase(Of AutomaticImportList, AutomaticImport)

#Region " Business Methods "

    Public Function GetItem(AutomaticImportID As Integer) As AutomaticImport

      For Each child As AutomaticImport In Me
        If child.AutomaticImportID = AutomaticImportID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Automatic Imports"

    End Function

    Public Function GetAutomaticImportCrew(AutomaticImportCrewID As Integer) As AutomaticImportCrew

      Dim obj As AutomaticImportCrew = Nothing
      For Each parent As AutomaticImport In Me
        obj = parent.AutomaticImportCrewList.GetItem(AutomaticImportCrewID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewAutomaticImportList() As AutomaticImportList

      Return New AutomaticImportList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetAutomaticImportList() As AutomaticImportList

      Return DataPortal.Fetch(Of AutomaticImportList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(AutomaticImport.GetAutomaticImport(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As AutomaticImport = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.AutomaticImportID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.AutomaticImportCrewList.RaiseListChangedEvents = False
          parent.AutomaticImportCrewList.Add(AutomaticImportCrew.GetAutomaticImportCrew(sdr))
          parent.AutomaticImportCrewList.RaiseListChangedEvents = True
        End While
      End If


      For Each child As AutomaticImport In Me
        child.CheckRules()
        For Each AutomaticImportCrew As AutomaticImportCrew In child.AutomaticImportCrewList
          AutomaticImportCrew.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getAutomaticImportList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace