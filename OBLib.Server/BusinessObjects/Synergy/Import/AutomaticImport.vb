﻿' Generated 30 Sep 2017 17:59 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.SystemManagement
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Synergy

  <Serializable()>
  Public Class AutomaticImport
    Inherits OBBusinessBase(Of AutomaticImport)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(Name:="Expanded"), AlwaysClean>
    Public Overridable Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return AutomaticImportBO.AutomaticImportBOToString(self)")

    Public Shared AutomaticImportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AutomaticImportID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property AutomaticImportID() As Integer
      Get
        Return GetProperty(AutomaticImportIDProperty)
      End Get
    End Property

    Public Shared SystemProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemProductionAreaID, "System Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the System Production Area value
    ''' </summary>
    <Display(Name:="System Production Area", Description:="")>
    Public Property SystemProductionAreaID() As Integer?
      Get
        Return GetProperty(SystemProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared SeriesNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SeriesNumber, "Series")
    ''' <summary>
    ''' Gets the Series Number value
    ''' </summary>
    <Display(Name:="Series", Description:=""),
     DropDownWeb(GetType(OBLib.Synergy.ROSynergySeriesList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  BeforeFetchJS:="AutomaticImportBO.setSeriesNumberCriteriaBeforeRefresh",
                  PreFindJSFunction:="AutomaticImportBO.triggerSeriesNumberAutoPopulate",
                  AfterFetchJS:="AutomaticImportBO.afterSeriesNumberRefreshAjax",
                  LookupMember:="SeriesTitle", ValueMember:="SeriesNumber", DropDownColumns:={"SeriesTitle", "SeriesNumber", "Season"},
                  OnItemSelectJSFunction:="AutomaticImportBO.onSeriesNumberSelected", DropDownCssClass:="room-dropdown")>
    Public Property SeriesNumber() As Integer
      Get
        Return GetProperty(SeriesNumberProperty)
      End Get
      Set(value As Integer)
        SetProperty(SeriesNumberProperty, value)
      End Set
    End Property

    Public Shared SeriesTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SeriesTitle, "Series Title", "")
    ''' <summary>
    ''' Gets and sets the Series Title value
    ''' </summary>
    <Display(Name:="Series Title", Description:=""),
    StringLength(150, ErrorMessage:="Series Title cannot be more than 150 characters")>
    Public Property SeriesTitle() As String
      Get
        Return GetProperty(SeriesTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SeriesTitleProperty, Value)
      End Set
    End Property

    Public Shared StartDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDate, "Start Date")
    ''' <summary>
    ''' Gets and sets the Start Date value
    ''' </summary>
    <Display(Name:="Start Date", Description:=""),
    Required(ErrorMessage:="Start Date required")>
    Public Property StartDate As Date
      Get
        Return GetProperty(StartDateProperty)
      End Get
      Set(ByVal Value As Date)
        SetProperty(StartDateProperty, Value)
      End Set
    End Property

    Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDate, "End Date")
    ''' <summary>
    ''' Gets and sets the End Date value
    ''' </summary>
    <Display(Name:="End Date", Description:="")>
    Public Property EndDate As DateTime?
      Get
        Return GetProperty(EndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
     Required(ErrorMessage:="Room is required"),
     DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                  BeforeFetchJS:="AutomaticImportBO.setRoomCriteriaBeforeRefresh",
                  PreFindJSFunction:="AutomaticImportBO.triggerRoomAutoPopulate",
                  AfterFetchJS:="AutomaticImportBO.afterRoomRefreshAjax",
                  LookupMember:="Room", ValueMember:="RoomID", DropDownColumns:={"Room"},
                  OnItemSelectJSFunction:="AutomaticImportBO.onRoomSelected", DropDownCssClass:="room-dropdown")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room", "")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared AutomaticImportCrewListProperty As PropertyInfo(Of AutomaticImportCrewList) = RegisterProperty(Of AutomaticImportCrewList)(Function(c) c.AutomaticImportCrewList, "Automatic Import Crew List")

    Public ReadOnly Property AutomaticImportCrewList() As AutomaticImportCrewList
      Get
        If GetProperty(AutomaticImportCrewListProperty) Is Nothing Then
          LoadProperty(AutomaticImportCrewListProperty, Synergy.AutomaticImportCrewList.NewAutomaticImportCrewList())
        End If
        Return GetProperty(AutomaticImportCrewListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SystemProductionArea

      Return CType(CType(Me.Parent, AutomaticImportList).Parent, SystemProductionArea)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(AutomaticImportIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SeriesTitle.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Automatic Import")
        Else
          Return String.Format("Blank {0}", "Automatic Import")
        End If
      Else
        Return Me.SeriesTitle
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"AutomaticImportCrew"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewAutomaticImport() method.

    End Sub

    Public Shared Function NewAutomaticImport() As AutomaticImport

      Return DataPortal.CreateChild(Of AutomaticImport)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetAutomaticImport(dr As SafeDataReader) As AutomaticImport

      Dim a As New AutomaticImport()
      a.Fetch(dr)
      Return a

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(AutomaticImportIDProperty, .GetInt32(0))
          LoadProperty(SystemProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SeriesNumberProperty, .GetInt32(2))
          LoadProperty(SeriesTitleProperty, .GetString(3))
          LoadProperty(StartDateProperty, .GetValue(4))
          LoadProperty(EndDateProperty, .GetValue(5))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(RoomProperty, .GetString(7))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, AutomaticImportIDProperty)

      cm.Parameters.AddWithValue("@SystemProductionAreaID", Me.GetParent.SystemProductionAreaID)
      cm.Parameters.AddWithValue("@SeriesNumber", GetProperty(SeriesNumberProperty))
      cm.Parameters.AddWithValue("@SeriesTitle", GetProperty(SeriesTitleProperty))
      cm.Parameters.AddWithValue("@StartDate", StartDate)
      cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(EndDate))
      cm.Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(GetProperty(RoomIDProperty)))
      cm.Parameters.AddWithValue("@Room", GetProperty(RoomProperty))
      cm.Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(AutomaticImportIDProperty, cm.Parameters("@AutomaticImportID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()

      UpdateChild(GetProperty(AutomaticImportCrewListProperty))

    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@AutomaticImportID", GetProperty(AutomaticImportIDProperty))
    End Sub

#End Region

  End Class

End Namespace