﻿Imports OBLib
Imports OBLib.SynergyWebService
Imports Singular

Namespace Synergy.Importer.New

  Public Class SynergyImporter

    Public Shared Sub ImportData(StartDate As Date?,
                                 EndDate As Date?,
                                 Location As String,
                                 SeriesTitle As String,
                                 Title As String, Genres As String,
                                 Country As String,
                                 Live As Boolean, Delayed As Boolean, Premier As Boolean)

      Try
        ScheduledImport(StartDate, EndDate, Nothing)
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, "Importer.vb", "ImportData(Lots of params)", ex.Message)
      End Try

    End Sub

    Public Shared Sub ImportData(GenRefNumber As Int64)

      Try
        ScheduledImport(Nothing, Nothing, GenRefNumber)
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, "Importer.vb", "ImportData(GenRefNumber As Int64)", ex.Message)
      End Try

    End Sub

    Public Shared Sub ImportData(GenRefNumbers As List(Of Int64))

      ''Scheduled Events--------------------------------------------------------------------------------------------------------------------------
      'Dim mScheduledEvents As ScheduleVO()
      'Dim ErrorMessage As String = ""
      'Dim genRefs As String = ""
      'GenRefNumbers.ForEach(Sub(gr)
      '                        genRefs += gr.ToString + ","
      '                      End Sub)
      'If genRefs.EndsWith(",") Then
      '  genRefs = genRefs.Substring(0, genRefs.Length - 2)
      'End If
      'mScheduledEvents = FetchData(genRefs, ErrorMessage)

      'Try
      '  If ErrorMessage = "" And mScheduledEvents IsNot Nothing Then
      '    Dim SynergyImportID As Integer? = Nothing
      '    SaveData(mScheduledEvents, SynergyImportID)
      '  Else
      '    OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, "Importer.vb", "ImportData(GenRefNumbers As List(Of Int64))", ErrorMessage)
      '  End If
      'Catch ex As Exception
      '  OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, "Importer.vb", "ImportData(GenRefNumbers As List(Of Int64))", ex.Message)
      'End Try

    End Sub

    'Public Shared Function FetchData(StartDate As Date?, EndDate As Date?, Location As String, SeriesTitle As String,
    '                                 Title As String, Genres As String, Country As String, Live As Boolean, Delayed As Boolean, Premier As Boolean,
    '                                 ByRef ErrorMessage As String) As ScheduleVO()

    '  Try

    '    Dim mScheduledEvents As ScheduleVO()
    '    Dim WebService As SynergyWebService.ProductionInvoiceServiceClient = New SynergyWebService.ProductionInvoiceServiceClient
    '    Dim SearchCriteria As New ScheduleSearchVO()

    '    SearchCriteria.ScheduleDateTime = StartDate
    '    SearchCriteria.ScheduleEndDateTime = EndDate
    '    SearchCriteria.EventStatus = ""
    '    If Live Then
    '      SearchCriteria.EventStatus = "L"
    '    End If
    '    If Delayed Then
    '      If SearchCriteria.EventStatus = "" Then
    '        SearchCriteria.EventStatus = "D"
    '      Else
    '        SearchCriteria.EventStatus &= ",D"
    '      End If
    '    End If
    '    If Premier Then
    '      If SearchCriteria.EventStatus = "" Then
    '        SearchCriteria.EventStatus = "P"
    '      Else
    '        SearchCriteria.EventStatus &= ",P"
    '      End If
    '    End If

    '    mScheduledEvents = WebService.GetSchedule(SearchCriteria)
    '    ErrorMessage = ""
    '    Return mScheduledEvents

    '  Catch ex As Exception
    '    ErrorMessage = ex.Message
    '    OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, "Importer.vb", "FetchData(lots of params)", ErrorMessage)
    '    Return Nothing
    '  End Try

    'End Function

    'Public Shared Function FetchData(GenRefnumber As Int64, ByRef ErrorMessage As String) As ScheduleVO()

    '  Try

    '    Dim mScheduledEvents As ScheduleVO()
    '    Dim WebService As SynergyWebService.ProductionInvoiceServiceClient = New SynergyWebService.ProductionInvoiceServiceClient
    '    Dim StartDate As Date = Singular.Dates.DateMonthStart(Date.Now.AddMonths(-2))
    '    Dim EndDate As Date = Singular.Dates.DateMonthEnd(Date.Now.AddYears(2))
    '    Dim SearchCriteria As New ScheduleSearchVO()
    '    SearchCriteria.GenRefNumbers = GenRefnumber.ToString
    '    SearchCriteria.ScheduleDateTime = StartDate
    '    SearchCriteria.ScheduleEndDateTime = EndDate
    '    mScheduledEvents = WebService.GetSchedule(SearchCriteria)
    '    ErrorMessage = ""
    '    Return mScheduledEvents
    '  Catch ex As Exception
    '    ErrorMessage = ex.Message
    '    OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, "Importer.vb", "FetchData(GenRefnumber As Int64, ByRef ErrorMessage As String)", ErrorMessage)
    '    Return Nothing
    '  End Try

    'End Function

    'Public Shared Function FetchData(GenRefnumbers As String, ByRef ErrorMessage As String) As ScheduleVO()

    '  Try
    '    Dim mScheduledEvents As ScheduleVO()
    '    Dim WebService As SynergyWebService.ProductionInvoiceServiceClient = New SynergyWebService.ProductionInvoiceServiceClient
    '    Dim StartDate As Date = Singular.Dates.DateMonthStart(Date.Now.AddMonths(-2))
    '    Dim EndDate As Date = Singular.Dates.DateMonthEnd(Date.Now.AddYears(2))
    '    Dim SearchCriteria As New ScheduleSearchVO()
    '    SearchCriteria.GenRefNumbers = GenRefnumbers
    '    SearchCriteria.ScheduleDateTime = StartDate
    '    SearchCriteria.ScheduleEndDateTime = EndDate
    '    mScheduledEvents = WebService.GetSchedule(SearchCriteria)
    '    ErrorMessage = ""
    '    Return mScheduledEvents
    '  Catch ex As Exception
    '    ErrorMessage = ex.Message
    '    OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, "Importer.vb", "FetchData(GenRefnumbers As String, ByRef ErrorMessage As String)", ErrorMessage)
    '    Return Nothing
    '  End Try

    'End Function

    'Public Shared Function SaveData(ScheduledEvents As ScheduleVO(), ByRef SynergyImportID As Integer?) As String

    '  Dim SynergyImportList As New SynergyImportList
    '  Dim SynergyImport = SynergyImportList.AddNew
    '  If OBLib.Security.Settings.CurrentUser Is Nothing Then
    '    SynergyImport.ImportedByUserID = 1 'OBLib.Security.Settings.CurrentUserID
    '  Else
    '    SynergyImport.ImportedByUserID = OBLib.Security.Settings.CurrentUserID
    '  End If
    '  SynergyImport.ImportDateTime = Now
    '  SynergyImport.ScheduleVOList = ScheduledEvents.ToList
    '  Try
    '    SynergyImportList = SynergyImportList.Save()
    '    SynergyImport = SynergyImportList(0)
    '    SynergyImportID = SynergyImport.SynergyImportID
    '    Return "SaveData completed successfully"
    '  Catch ex As Exception
    '    OBLib.Helpers.ErrorHelpers.LogClientError("Scheduler", "Importer.vb", "SaveData(GenRefnumbers As String, ByRef ErrorMessage As String) - SAVE LIST", ex.Message & " - " & ex.StackTrace)
    '    Return ex.Message
    '  End Try

    'End Function

    Public Shared Sub CreateProduction(CurrentUserID As Integer?, SynergyGenRefNo As Int64?, ImportedEventID As Integer?)

      Try
        Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdSynergyImporterCreateProduction]",
                                            New String() {"@CurrentUserID", "@ImportedEventID", "@SystemID", "@SynergyGenRefNo"},
                                            New Object() {CurrentUserID, Singular.Misc.NothingDBNull(ImportedEventID), Singular.Misc.NothingDBNull(Nothing), Singular.Misc.NothingDBNull(SynergyGenRefNo)})
        cmd.CommandTimeout = 0
        cmd = cmd.Execute()
        'Return "SendEmail Completed Successfully"
      Catch ex As Exception
        'Return ex.Message
      End Try

    End Sub

    Public Shared Sub CreateProductions(CurrentUserID As Integer?, SynergyGenRefNumbers As List(Of Int64))

      If SynergyGenRefNumbers.Count > 0 Then
        Try
          Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdSynergyImporterCreateProductions]",
                                              New String() {"@CurrentUserID", "@GenRefNumbers"},
                                              New Object() {CurrentUserID, OBMisc.Int64ListToXML(SynergyGenRefNumbers)})
          cmd.CommandTimeout = 0
          cmd = cmd.Execute()
          'Return "SendEmail Completed Successfully"
        Catch ex As Exception
          'Return ex.Message
        End Try
      End If

    End Sub

    '        If FetchDataErrorMessage = "" And mScheduledEvents IsNot Nothing Then
    'Dim SynergyImportID As Integer? = Nothing
    '      OBLib.Synergy.Importer.[New].SynergyImporter.SaveData(mScheduledEvents, SynergyImportID)
    '    End If

    Public Shared Function ScheduledImport(StartDate As Date?, EndDate As Date?, Optional GenRefNumber As Int64? = Nothing) As SynergyImport

      Dim mScheduledEvents As ScheduleVO()
      Dim mSponsorship As List(Of SSWSD.SponsorshipItem) = New List(Of SSWSD.SponsorshipItem)
      Try

        'Get the data from Synergy
        Dim WebService As SynergyWebService.ProductionInvoiceServiceClient = New SynergyWebService.ProductionInvoiceServiceClient
        Dim SearchCriteria As New ScheduleSearchVO()
        SearchCriteria.ScheduleDateTime = StartDate
        SearchCriteria.ScheduleEndDateTime = EndDate
        SearchCriteria.EventStatus = "L,D,P,R"
        If GenRefNumber IsNot Nothing Then
          SearchCriteria.GenRefNumbers = GenRefNumber.Value.ToString
        End If
        mScheduledEvents = WebService.GetSchedule(SearchCriteria)

      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "Importer.vb", "ScheduledImport - Synergy", ex.Message)
      End Try
      'Get the sponsorship info for the ScheduleNumbers from the other System

      Dim SponsorshipService As SSWSD.SSWSDSVCClient
      Try
        SponsorshipService = New SSWSD.SSWSDSVCClient
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "Importer.vb", "ScheduledImport - Sponsorship Client", ex.Message)
      End Try

      Dim returnedList As New SSWSD.RetObj_GetSponsorshipItemList
      Dim param As String = ""

      Try
        If SponsorshipService IsNot Nothing Then
          Dim allScheduleNumbers As New List(Of Integer)
          Dim currentScheduleNumbers As New List(Of Integer)
          allScheduleNumbers = mScheduledEvents.Select(Function(n) n.ScheduleNumber).ToList
          While allScheduleNumbers.Count > 0
            currentScheduleNumbers = allScheduleNumbers.Take(500).ToList
            If currentScheduleNumbers.Count > 0 Then
              param = String.Join(",", currentScheduleNumbers.Select(Function(n) n.ToString()).ToArray())
              returnedList = SponsorshipService.GetSponsorshipItemList("94208fee-5615-11e5-a800-005056943beb", "SSWSD_Sober", param)
              If returnedList.SvcFeedback = "Success!" Or returnedList.SvcFeedback = "No sponsorship front sheets found." Then
                mSponsorship.AddRange(returnedList.FrontSheetItems)
              Else
                OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "Importer.vb", "ScheduledImport - Sponsorship Loop - SVC FEEDBACK", returnedList.SvcFeedback)
                Exit While
              End If
              allScheduleNumbers.RemoveAll(Function(d) currentScheduleNumbers.Contains(d))
            End If
          End While
        End If
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "Importer.vb", "ScheduledImport - Sponsorship Loop", ex.Message)
      End Try

      Try
        'Create new SynergyImport and save the data
        Dim SynergyImport = New SynergyImport
        SynergyImport.ImportedByUserID = 1
        SynergyImport.ImportDateTime = Now
        SynergyImport.StartDate = StartDate
        SynergyImport.EndDate = EndDate
        SynergyImport.ScheduleVOList = mScheduledEvents.ToList
        SynergyImport.SponsorshipList = mSponsorship
        Dim sh As Singular.SaveHelper = SynergyImport.TrySave(GetType(SynergyImportList))
        If sh.Success Then
          SynergyImport = sh.SavedObject
          Return SynergyImport
        Else
          OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "Importer.vb", "ScheduledImport - Saving Saved Object", sh.ErrorText)
        End If
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "Importer.vb", "ScheduledImport - Saving", ex.Message)
      End Try

      Return Nothing

    End Function

    Public Shared Function GetSponsorship(SchedulerNumbers As List(Of Integer))

      Try
        Dim mScheduledEvents As SSWSD.RetObj_GetSponsorshipItemList
        Dim WebService As SSWSD.SSWSDSVCClient = New SSWSD.SSWSDSVCClient
        Dim scheduleNumbers As String = String.Join(",", SchedulerNumbers.Select(Function(n) n.ToString()).ToArray())
        mScheduledEvents = WebService.GetSponsorshipItemList("94208fee-5615-11e5-a800-005056943beb", "SSWSD_Sober", scheduleNumbers)
        Return mScheduledEvents
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, "Importer.vb", "GetSponsorship(SchedulerNumbers As List(Of Integer))", ex.Message)
      End Try

    End Function

  End Class

  'Unscheduled Events------------------------------------------------------------------------------------------------------------------------
  'Dim mUnscheduledEvents As OBLib.SynergyWebService.ScheduleVO()
  'Dim UnscheduledWebService As SynergyWebService.ProductionInvoiceServiceClient = New SynergyWebService.ProductionInvoiceServiceClient
  'Dim objSearch As New SynergyWebService.EpisodeSearchVO With {.LiveDateTime = StartDate, .LiveEndDateTime = EndDate}
  'mUnscheduledEvents = UnscheduledWebService.GetNonScheduledEpisodes(objSearch)
  'mSynergyImportList = New SynergyScheduleImportList()
  'For Each ScheduleVO As SynergyWebService.ScheduleVO In mUnscheduledEvents
  '  mSynergyImportList.Add(New SynergyScheduleImport(ScheduleVO))
  'Next
  'mSynergyImportList.Save()

  'Unscheduled Events------------------------------------------------------------------------------------------------------------------------
  'Synergy.Importer.IdentifyErrors(mTotalItems)
  'Synergy.Importer.CreateNewProductions(mSuccessfulItems, mFailedItems, Settings.CurrentUserID)
  ''Synergy.Importer.IdentifyTags(mTotalItems)
  'mScheduledEvents = Nothing 'save on data transfer
  'mSynergyImportList = Nothing 'save on data transfer
  'ImportCompleteInd = True

End Namespace
