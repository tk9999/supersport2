﻿' Generated 11 Feb 2016 00:12 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.SatOps.ReadOnly
Imports OBLib.Equipment
Imports OBLib.Equipment.ReadOnly
Imports System.Data.SqlClient
Imports OBLib.Scheduling.Rooms

Namespace Synergy

  <Serializable()>
  Public Class ByEventTemplate
    Inherits OBBusinessBase(Of ByEventTemplate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IsExpandedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsExpanded, False)
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="IsExpanded"), AlwaysClean>
    Public Property IsExpanded() As Boolean
      Get
        Return GetProperty(IsExpandedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsExpandedProperty, Value)
      End Set
    End Property

    Public Shared DefaultRoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DefaultRoomID, "Default Circuit")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Default Room", Description:=""),
     DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityList),
                 BeforeFetchJS:="ByEventTemplateBO.setRoomCriteriaBeforeRefresh",
                 PreFindJSFunction:="ByEventTemplateBO.triggerRoomAutoPopulate",
                 AfterFetchJS:="ByEventTemplateBO.afterRoomRefreshAjax",
                 OnItemSelectJSFunction:="ByEventTemplateBO.onRoomSelected",
                 LookupMember:="Room", DisplayMember:="Room", ValueMember:="ResourceID",
                 DropDownCssClass:="circuit-dropdown", DropDownColumns:={"Room", "IsAvailableString"}),
     SetExpression("ByEventTemplateBO.RoomIDSet(self)")>
    Public Property DefaultRoomID() As Integer?
      Get
        Return GetProperty(DefaultRoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DefaultRoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Room, "Room")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room() As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared DefaultResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DefaultResourceID, "Default Circuit")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Default Room", Description:="")>
    Public Property DefaultResourceID() As Integer?
      Get
        Return GetProperty(DefaultResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DefaultResourceIDProperty, Value)
      End Set
    End Property

    Public Shared DefaultSystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DefaultSystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required")>
    Public Property DefaultSystemID() As Integer?
      Get
        Return GetProperty(DefaultSystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DefaultSystemIDProperty, Value)
      End Set
    End Property

    Public Shared DefaultProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DefaultProductionAreaID, "Area")
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required")>
    Public Property DefaultProductionAreaID() As Integer?
      Get
        Return GetProperty(DefaultProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DefaultProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ModeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Mode, "Mode", "Room")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Mode", Description:="")>
    Public Property Mode() As String
      Get
        Return GetProperty(ModeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ModeProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared EventItemListProperty As PropertyInfo(Of EventItemList) = RegisterProperty(Of EventItemList)(Function(c) c.EventItemList, "EventItemList")

    Public ReadOnly Property EventItemList() As EventItemList
      Get
        If GetProperty(EventItemListProperty) Is Nothing Then
          LoadProperty(EventItemListProperty, OBLib.Synergy.EventItemList.NewEventItemList())
        End If
        Return GetProperty(EventItemListProperty)
      End Get
    End Property

    Public Shared EquipmentScheduleBasicListProperty As PropertyInfo(Of EquipmentScheduleBasicList) = RegisterProperty(Of EquipmentScheduleBasicList)(Function(c) c.EquipmentScheduleBasicList, "EquipmentScheduleBasicList")

    Public ReadOnly Property EquipmentScheduleBasicList() As EquipmentScheduleBasicList
      Get
        If GetProperty(EquipmentScheduleBasicListProperty) Is Nothing Then
          LoadProperty(EquipmentScheduleBasicListProperty, OBLib.Equipment.EquipmentScheduleBasicList.NewEquipmentScheduleBasicList())
        End If
        Return GetProperty(EquipmentScheduleBasicListProperty)
      End Get
    End Property

    Public Shared RoomScheduleListProperty As PropertyInfo(Of RoomScheduleList) = RegisterProperty(Of RoomScheduleList)(Function(c) c.RoomScheduleList, "RoomScheduleList")

    Public ReadOnly Property RoomScheduleList() As RoomScheduleList
      Get
        If GetProperty(RoomScheduleListProperty) Is Nothing Then
          LoadProperty(RoomScheduleListProperty, OBLib.Scheduling.Rooms.RoomScheduleList.NewRoomScheduleList())
        End If
        Return GetProperty(RoomScheduleListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return 0 'GetProperty(GenRefNumberProperty)

    End Function

    Public Overrides Function ToString() As String

      Return "Day Template"

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {""}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewByEventTemplate() method.

    End Sub

    Public Shared Function NewByEventTemplate() As ByEventTemplate

      Return DataPortal.CreateChild(Of ByEventTemplate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetByEventTemplate(dr As SafeDataReader) As ByEventTemplate

      Dim e As New ByEventTemplate()
      e.Fetch(dr)
      Return e

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      'Using BypassPropertyChecks
      '  With sdr
      '    LoadProperty(VenueDayGroupProperty, .GetInt32(0))
      '    LoadProperty(GenRefNumberProperty, .GetInt64(1))
      '    LoadProperty(ImportedEventIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
      '    LoadProperty(GenreSeriesProperty, .GetString(3))
      '    LoadProperty(TitleProperty, .GetString(4))
      '    LoadProperty(HighlightsIndProperty, .GetBoolean(5))
      '    LoadProperty(LiveDateProperty, .GetValue(6))
      '    LoadProperty(FirstChannelStartProperty, .GetValue(7))
      '    LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
      '  End With
      'End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insByEventTemplate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updByEventTemplate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      'If Me.IsSelfDirty Then

      '  With cm
      '    .CommandType = CommandType.StoredProcedure

      '    Dim paramVenueDayGroup As SqlParameter = .Parameters.Add("@VenueDayGroup", SqlDbType.Int)
      '    paramVenueDayGroup.Value = GetProperty(VenueDayGroupProperty)
      '    If Me.IsNew Then
      '      paramVenueDayGroup.Direction = ParameterDirection.Output
      '    End If
      '    .Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
      '    .Parameters.AddWithValue("@ImportedEventID", GetProperty(ImportedEventIDProperty))
      '    .Parameters.AddWithValue("@GenreSeries", GetProperty(GenreSeriesProperty))
      '    .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      '    .Parameters.AddWithValue("@HighlightsInd", GetProperty(HighlightsIndProperty))
      '    .Parameters.AddWithValue("@LiveDate", (New SmartDate(GetProperty(LiveDateProperty))).DBValue)
      '    .Parameters.AddWithValue("@FirstChannelStart", (New SmartDate(GetProperty(FirstChannelStartProperty))).DBValue)

      '    .ExecuteNonQuery()

      '    If Me.IsNew Then
      '      LoadProperty(VenueDayGroupProperty, paramVenueDayGroup.Value)
      '    End If
      '    ' update child objects
      '    If GetProperty(ByEventTemplateChannelListProperty) IsNot Nothing Then
      '      Me.ByEventTemplateChannelList.Update()
      '    End If
      '    MarkOld()
      '  End With
      'Else
      '  ' update child objects
      '  If GetProperty(ByEventTemplateChannelListProperty) IsNot Nothing Then
      '    Me.ByEventTemplateChannelList.Update()
      '  End If
      'End If

    End Sub

    Friend Sub DeleteSelf()

      '' if we're not dirty then don't update the database
      'If Me.IsNew Then Exit Sub

      'Using cm As SqlCommand = New SqlCommand
      '  cm.CommandText = "DelProcsWeb.delByEventTemplate"
      '  cm.CommandType = CommandType.StoredProcedure
      '  cm.Parameters.AddWithValue("@VenueDayGroup", GetProperty(VenueDayGroupProperty))
      '  DoDeleteChild(cm)
      'End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace