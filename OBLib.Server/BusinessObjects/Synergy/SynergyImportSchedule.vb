﻿' Generated 08 Apr 2014 09:59 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.SynergyWebService

Namespace Synergy.Importer.New

  <Serializable()> _
  Public Class SynergyImportSchedule
    Inherits OBBusinessBase(Of SynergyImportSchedule)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SynergyImportScheduleIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SynergyImportScheduleID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SynergyImportScheduleID() As Integer
      Get
        Return GetProperty(SynergyImportScheduleIDProperty)
      End Get
    End Property

    Public Shared SynergyImportIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SynergyImportID, "Synergy Import", Nothing)
    ''' <summary>
    ''' Gets the Synergy Import value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property SynergyImportID() As Integer?
      Get
        Return GetProperty(SynergyImportIDProperty)
      End Get
    End Property

    Public Shared ImportDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ImportDateTime, "Import Date Time")
    ''' <summary>
    ''' Gets and sets the Import Date Time value
    ''' </summary>
    <Display(Name:="Import Date Time", Description:="")>
    Public Property ImportDateTime As DateTime?
      Get
        Return GetProperty(ImportDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ImportDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ChannelNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelName, "Channel Name", "")
    ''' <summary>
    ''' Gets and sets the Channel Name value
    ''' </summary>
    <Display(Name:="Channel Name", Description:="")>
    Public Property ChannelName() As String
      Get
        Return GetProperty(ChannelNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelNameProperty, Value)
      End Set
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel Short Name", "")
    ''' <summary>
    ''' Gets and sets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel Short Name", Description:="")>
    Public Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ChannelShortNameProperty, Value)
      End Set
    End Property

    Public Shared DurationProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.Duration, "Duration")
    ''' <summary>
    ''' Gets and sets the Duration value
    ''' </summary>
    <Display(Name:="Duration", Description:="")>
    Public Property Duration() As Object
      Get
        Dim value = GetProperty(DurationProperty)
        If value = DateTime.MinValue Then
          Return Nothing
        Else
          Return value.ToString("HH:mm")
        End If
      End Get
      Set(ByVal Value As Object)
        If Not Singular.Misc.DateTimeEqualsTimeValue(GetProperty(DurationProperty), Value, Singular.Misc.TimeStringSingleNumberType.Hours) Then
          If Singular.Misc.IsNullNothing(Value) Then
            SetProperty(DurationProperty, DateTime.MinValue)
          Else
            Dim Time As TimeSpan = Singular.Misc.GetTimeSpanFromString(CStr(Value), Singular.Misc.TimeStringSingleNumberType.Hours)
            SetProperty(DurationProperty, (New DateTime(2000, 1, 1)).Add(Time))
          End If
        End If
      End Set
    End Property

    Public Shared EventStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventStatus, "Event Status", "")
    ''' <summary>
    ''' Gets and sets the Event Status value
    ''' </summary>
    <Display(Name:="Event Status", Description:="")>
    Public Property EventStatus() As String
      Get
        Return GetProperty(EventStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventStatusProperty, Value)
      End Set
    End Property

    Public Shared EventStatusDescProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventStatusDesc, "Event Status Desc", "")
    ''' <summary>
    ''' Gets and sets the Event Status Desc value
    ''' </summary>
    <Display(Name:="Event Status Desc", Description:="")>
    Public Property EventStatusDesc() As String
      Get
        Return GetProperty(EventStatusDescProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventStatusDescProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNumber, "Gen Ref Number", Nothing)
    ''' <summary>
    ''' Gets and sets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref Number", Description:="")>
    Public Property GenRefNumber() As Int64?
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared GenreProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Genre, "Genre", "")
    ''' <summary>
    ''' Gets and sets the Genre value
    ''' </summary>
    <Display(Name:="Genre", Description:="")>
    Public Property Genre() As String
      Get
        Return GetProperty(GenreProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreProperty, Value)
      End Set
    End Property

    Public Shared GenreDescProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreDesc, "Genre Desc", "")
    ''' <summary>
    ''' Gets and sets the Genre Desc value
    ''' </summary>
    <Display(Name:="Genre Desc", Description:="")>
    Public Property GenreDesc() As String
      Get
        Return GetProperty(GenreDescProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreDescProperty, Value)
      End Set
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LiveDate, "Live Date")
    ''' <summary>
    ''' Gets and sets the Live Date value
    ''' </summary>
    <Display(Name:="Live Date", Description:="")>
    Public Property LiveDate As DateTime?
      Get
        Return GetProperty(LiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LiveDateProperty, Value)
      End Set
    End Property

    Public Shared LiveTimeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LiveTime, "Live Time", "")
    ''' <summary>
    ''' Gets and sets the Live Time value
    ''' </summary>
    <Display(Name:="Live Time", Description:="")>
    Public Property LiveTime() As String
      Get
        Return GetProperty(LiveTimeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LiveTimeProperty, Value)
      End Set
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location", "")
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationProperty, Value)
      End Set
    End Property

    Public Shared LocationCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LocationCode, "Location Code", "")
    ''' <summary>
    ''' Gets and sets the Location Code value
    ''' </summary>
    <Display(Name:="Location Code", Description:="")>
    Public Property LocationCode() As String
      Get
        Return GetProperty(LocationCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationCodeProperty, Value)
      End Set
    End Property

    Public Shared MediaPlatformProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.MediaPlatform, "Media Platform", "")
    ''' <summary>
    ''' Gets and sets the Media Platform value
    ''' </summary>
    <Display(Name:="Media Platform", Description:="")>
    Public Property MediaPlatform() As String
      Get
        Return GetProperty(MediaPlatformProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(MediaPlatformProperty, Value)
      End Set
    End Property

    Public Shared ProgramTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProgramType, "Program Type", "")
    ''' <summary>
    ''' Gets and sets the Program Type value
    ''' </summary>
    <Display(Name:="Program Type", Description:="")>
    Public Property ProgramType() As String
      Get
        Return GetProperty(ProgramTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProgramTypeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleDateTime, "Schedule Date Time")
    ''' <summary>
    ''' Gets and sets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Schedule Date Time", Description:="")>
    Public Property ScheduleDateTime As DateTime?
      Get
        Return GetProperty(ScheduleDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ScheduleEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleEndDate, "Schedule End Date")
    ''' <summary>
    ''' Gets and sets the Schedule End Date value
    ''' </summary>
    <Display(Name:="Schedule End Date", Description:="")>
    Public Property ScheduleEndDate As DateTime?
      Get
        Return GetProperty(ScheduleEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ScheduleEndDateProperty, Value)
      End Set
    End Property

    Public Shared ScheduleNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ScheduleNumber, "Schedule Number", 0)
    ''' <summary>
    ''' Gets and sets the Schedule Number value
    ''' </summary>
    <Display(Name:="Schedule Number", Description:="")>
    Public Property ScheduleNumber() As Integer
      Get
        Return GetProperty(ScheduleNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ScheduleNumberProperty, Value)
      End Set
    End Property

    Public Shared SeasonNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SeasonNumber, "Season Number", 0)
    ''' <summary>
    ''' Gets and sets the Season Number value
    ''' </summary>
    <Display(Name:="Season Number", Description:="")>
    Public Property SeasonNumber() As Integer
      Get
        Return GetProperty(SeasonNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SeasonNumberProperty, Value)
      End Set
    End Property

    Public Shared SeasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Season, "Season", "")
    ''' <summary>
    ''' Gets and sets the Season value
    ''' </summary>
    <Display(Name:="Season", Description:="")>
    Public Property Season() As String
      Get
        Return GetProperty(SeasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SeasonProperty, Value)
      End Set
    End Property

    Public Shared SeriesNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SeriesNumber, "Series Number", 0)
    ''' <summary>
    ''' Gets and sets the Series Number value
    ''' </summary>
    <Display(Name:="Series Number", Description:="")>
    Public Property SeriesNumber() As Integer
      Get
        Return GetProperty(SeriesNumberProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SeriesNumberProperty, Value)
      End Set
    End Property

    Public Shared SeriesTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SeriesTitle, "Series Title", "")
    ''' <summary>
    ''' Gets and sets the Series Title value
    ''' </summary>
    <Display(Name:="Series Title", Description:="")>
    Public Property SeriesTitle() As String
      Get
        Return GetProperty(SeriesTitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SeriesTitleProperty, Value)
      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title", "")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue", "")
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VenueProperty, Value)
      End Set
    End Property

    Public Shared VenueCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VenueCode, "Venue Code", "")
    ''' <summary>
    ''' Gets and sets the Venue Code value
    ''' </summary>
    <Display(Name:="Venue Code", Description:="")>
    Public Property VenueCode() As String
      Get
        Return GetProperty(VenueCodeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VenueCodeProperty, Value)
      End Set
    End Property

    Public Shared ErrorIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ErrorInd, "Error", False)
    ''' <summary>
    ''' Gets and sets the Error value
    ''' </summary>
    <Display(Name:="Error", Description:="")>
    Public Property ErrorInd() As Boolean
      Get
        Return GetProperty(ErrorIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ErrorIndProperty, Value)
      End Set
    End Property

    Public Shared ErrorDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ErrorDescription, "Error Description", "")
    ''' <summary>
    ''' Gets and sets the Error Description value
    ''' </summary>
    <Display(Name:="Error Description", Description:="")>
    Public Property ErrorDescription() As String
      Get
        Return GetProperty(ErrorDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ErrorDescriptionProperty, Value)
      End Set
    End Property

    Public Shared UpdateIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.UpdateInd, "Update", False)
    ''' <summary>
    ''' Gets and sets the Update value
    ''' </summary>
    <Display(Name:="Update", Description:="")>
    Public Property UpdateInd() As Boolean
      Get
        Return GetProperty(UpdateIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(UpdateIndProperty, Value)
      End Set
    End Property

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country", "")
    ''' <summary>
    ''' Gets and sets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="")>
    Public Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CountryProperty, Value)
      End Set
    End Property

    Public Shared IsTBCProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTBC, "Is TBC", False)
    ''' <summary>
    ''' Gets and sets the Is TBC value
    ''' </summary>
    <Display(Name:="Is TBC", Description:="")>
    Public Property IsTBC() As Boolean
      Get
        Return GetProperty(IsTBCProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsTBCProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As SynergyImport

      Return CType(CType(Me.Parent, SynergyImportScheduleList).Parent, SynergyImport)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SynergyImportScheduleIDProperty)

    End Function

    Public Overrides Function ToString() As String

      'If Me.ChannelName.Length = 0 Then
      '  If Me.IsNew Then
      '    Return String.Format("New {0}", "Synergy Import Schedule")
      '  Else
      '    Return String.Format("Blank {0}", "Synergy Import Schedule")
      '  End If
      'Else
      '  Return Me.ChannelName
      'End If
      Return Me.SeriesTitle & " - " & Me.Title

    End Function

    Public Sub New(ScheduleVO As ScheduleVO)

      ImportDateTime = Now
      With ScheduleVO
        ChannelName = .ChannelName
        ChannelShortName = .ChannelShortName
        'Duration = .Duration
        'Duration = Singular.Misc.GetTimeSpanFromString(.Duration, TimeStringSingleNumberType.Minutes)
        Duration = New DateTime(2000, 1, 1).Add(.Duration)
        EventStatus = .EventStatus
        EventStatusDesc = .EventStatusDesc
        GenRefNumber = .GenRefNumber
        Genre = .Genre
        GenreDesc = .GenreDesc
        LiveDate = .LiveDate
        LiveTime = .LiveTime
        Location = .Location
        LocationCode = .LocationCode
        MediaPlatform = .MediaPlatform
        ProgramType = .ProgramType
        ScheduleDateTime = .ScheduleDateTime
        ScheduleEndDate = .ScheduleEndDateTime
        ScheduleNumber = .ScheduleNumber
        Season = .Season
        SeriesNumber = .SeriesNumber
        SeriesTitle = .SeriesTitle
        Title = .Title
        Venue = .Venue
        VenueCode = .VenueCode
        Country = .Country
        IsTBC = .IsTbc
      End With


    End Sub

    'Public Sub New(UnScheduledScheduleVO As UnScheduledEventsWebService.ScheduleVO)

    '  ImportDateTime = Now
    '  With UnScheduledScheduleVO
    '    ChannelName = .ChannelName
    '    ChannelShortName = .ChannelShortName
    '    'Duration = .Duration
    '    'Duration = Singular.Misc.GetTimeSpanFromString(.Duration, TimeStringSingleNumberType.Minutes)
    '    Duration = New DateTime(2000, 1, 1).Add(.Duration)
    '    EventStatus = .EventStatus
    '    EventStatusDesc = .EventStatusDesc
    '    GenRefNumber = .GenRefNumber
    '    Genre = .Genre
    '    GenreDesc = .GenreDesc
    '    LiveDate = .LiveDate
    '    LiveTime = .LiveTime
    '    Location = .Location
    '    LocationCode = .LocationCode
    '    MediaPlatform = .MediaPlatform
    '    ProgramType = .ProgramType
    '    ScheduleDateTime = .ScheduleDateTime
    '    ScheduleEndDate = .ScheduleEndDateTime
    '    ScheduleNumber = .ScheduleNumber
    '    Season = .Season
    '    SeriesNumber = .SeriesNumber
    '    SeriesTitle = .SeriesTitle
    '    Title = .Title
    '    Venue = .Venue
    '    VenueCode = .VenueCode
    '    Country = .Country
    '    IsTBC = .IsTbc
    '  End With

    'End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSynergyImportSchedule() method.

    End Sub

    Public Shared Function NewSynergyImportSchedule() As SynergyImportSchedule

      Return DataPortal.CreateChild(Of SynergyImportSchedule)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSynergyImportSchedule(dr As SafeDataReader) As SynergyImportSchedule

      Dim s As New SynergyImportSchedule()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SynergyImportScheduleIDProperty, .GetInt32(0))
          LoadProperty(SynergyImportIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ImportDateTimeProperty, .GetValue(2))
          LoadProperty(ChannelNameProperty, .GetString(3))
          LoadProperty(ChannelShortNameProperty, .GetString(4))
          If .IsDBNull(5) Then
            LoadProperty(DurationProperty, DateTime.MinValue)
          Else
            LoadProperty(DurationProperty, (New DateTime(2000, 1, 1).Add(.GetValue(5))))
          End If
          LoadProperty(EventStatusProperty, .GetString(6))
          LoadProperty(EventStatusDescProperty, .GetString(7))
          LoadProperty(GenRefNumberProperty, ZeroNothing(.GetInt64(8)))
          LoadProperty(GenreProperty, .GetString(9))
          LoadProperty(GenreDescProperty, .GetString(10))
          LoadProperty(LiveDateProperty, .GetValue(11))
          LoadProperty(LiveTimeProperty, .GetString(12))
          LoadProperty(LocationProperty, .GetString(13))
          LoadProperty(LocationCodeProperty, .GetString(14))
          LoadProperty(MediaPlatformProperty, .GetString(15))
          LoadProperty(ProgramTypeProperty, .GetString(16))
          LoadProperty(ScheduleDateTimeProperty, .GetValue(17))
          LoadProperty(ScheduleEndDateProperty, .GetValue(18))
          LoadProperty(ScheduleNumberProperty, .GetInt32(19))
          LoadProperty(SeasonNumberProperty, .GetInt32(20))
          LoadProperty(SeasonProperty, .GetString(21))
          LoadProperty(SeriesNumberProperty, .GetInt32(22))
          LoadProperty(SeriesTitleProperty, .GetString(23))
          LoadProperty(TitleProperty, .GetString(24))
          LoadProperty(VenueProperty, .GetString(25))
          LoadProperty(VenueCodeProperty, .GetString(26))
          LoadProperty(ErrorIndProperty, .GetBoolean(27))
          LoadProperty(ErrorDescriptionProperty, .GetString(28))
          LoadProperty(UpdateIndProperty, .GetBoolean(29))
          LoadProperty(CountryProperty, .GetString(30))
          LoadProperty(IsTBCProperty, .GetBoolean(31))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insSynergyImportSchedule"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updSynergyImportSchedule"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSynergyImportScheduleID As SqlParameter = .Parameters.Add("@SynergyImportScheduleID", SqlDbType.Int)
          paramSynergyImportScheduleID.Value = GetProperty(SynergyImportScheduleIDProperty)
          If Me.IsNew Then
            paramSynergyImportScheduleID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SynergyImportID", Me.GetParent().SynergyImportID)
          cm.Parameters.AddWithValue("@ImportDateTime", (New SmartDate(GetProperty(ImportDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ChannelName", GetProperty(ChannelNameProperty))
          .Parameters.AddWithValue("@ChannelShortName", GetProperty(ChannelShortNameProperty))
          cm.Parameters.AddWithValue("@Duration", (New SmartDate(GetProperty(DurationProperty))).DBValue)
          .Parameters.AddWithValue("@EventStatus", GetProperty(EventStatusProperty))
          .Parameters.AddWithValue("@EventStatusDesc", GetProperty(EventStatusDescProperty))
          .Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
          .Parameters.AddWithValue("@Genre", GetProperty(GenreProperty))
          .Parameters.AddWithValue("@GenreDesc", GetProperty(GenreDescProperty))
          cm.Parameters.AddWithValue("@LiveDate", (New SmartDate(GetProperty(LiveDateProperty))).DBValue)
          .Parameters.AddWithValue("@LiveTime", GetProperty(LiveTimeProperty))
          .Parameters.AddWithValue("@Location", GetProperty(LocationProperty))
          .Parameters.AddWithValue("@LocationCode", GetProperty(LocationCodeProperty))
          .Parameters.AddWithValue("@MediaPlatform", GetProperty(MediaPlatformProperty))
          .Parameters.AddWithValue("@ProgramType", GetProperty(ProgramTypeProperty))
          cm.Parameters.AddWithValue("@ScheduleDateTime", (New SmartDate(GetProperty(ScheduleDateTimeProperty))).DBValue)
          cm.Parameters.AddWithValue("@ScheduleEndDate", (New SmartDate(GetProperty(ScheduleEndDateProperty))).DBValue)
          .Parameters.AddWithValue("@ScheduleNumber", GetProperty(ScheduleNumberProperty))
          .Parameters.AddWithValue("@SeasonNumber", GetProperty(SeasonNumberProperty))
          .Parameters.AddWithValue("@Season", GetProperty(SeasonProperty))
          .Parameters.AddWithValue("@SeriesNumber", GetProperty(SeriesNumberProperty))
          .Parameters.AddWithValue("@SeriesTitle", GetProperty(SeriesTitleProperty))
          .Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
          .Parameters.AddWithValue("@Venue", GetProperty(VenueProperty))
          .Parameters.AddWithValue("@VenueCode", GetProperty(VenueCodeProperty))
          .Parameters.AddWithValue("@ErrorInd", GetProperty(ErrorIndProperty))
          .Parameters.AddWithValue("@ErrorDescription", GetProperty(ErrorDescriptionProperty))
          .Parameters.AddWithValue("@UpdateInd", GetProperty(UpdateIndProperty))
          .Parameters.AddWithValue("@Country", GetProperty(CountryProperty))
          .Parameters.AddWithValue("@IsTBC", GetProperty(IsTBCProperty))
          .CommandTimeout = 0
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SynergyImportScheduleIDProperty, paramSynergyImportScheduleID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delSynergyImportSchedule"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SynergyImportScheduleID", GetProperty(SynergyImportScheduleIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace