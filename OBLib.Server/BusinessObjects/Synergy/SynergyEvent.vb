﻿' Generated 09 May 2016 07:44 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Security
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.HR.ReadOnly
Imports OBLib.Slugs
Imports OBLib.Maintenance.Vehicles.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.ProductionSpecs.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class SynergyEvent
    Inherits OBBusinessBase(Of SynergyEvent)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return SynergyEventBO.SynergyEventBOToString(self)")

    Public Shared ImportedEventIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key, Required(ErrorMessage:="ID required")>
    Public Property ImportedEventID() As Integer
      Get
        Return GetProperty(ImportedEventIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ImportedEventIDProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref")
    ''' <summary>
    ''' Gets and sets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:=""),
    Required(ErrorMessage:="Gen Ref required")>
    Public Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    Public Shared GenreSeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreSeries, "Genre Series")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Genre Series", Description:="")>
    Public Property GenreSeries() As String
      Get
        Return GetProperty(GenreSeriesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(GenreSeriesProperty, Value)
      End Set
    End Property

    <Display(Name:="Genre (Series)", Description:="")>
    Public Property GenreSeriesDisplay() As String
      Get
        If GenreSeries.Length > 40 Then
          Return GenreSeries.Substring(0, 37) & "..."
        End If
        Return GenreSeries
      End Get
      Set(ByVal Value As String)

      End Set
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets and sets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TitleProperty, Value)
      End Set
    End Property

    <Display(Name:="Title", Description:="")>
    Public Property TitleDisplay() As String
      Get
        If Title.Length > 40 Then
          Return Title.Substring(0, 37) & "..."
        End If
        Return Title
      End Get
      Set(ByVal Value As String)

      End Set
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.LiveDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Live Date value
    ''' </summary>
    <Display(Name:="Live Start", Description:="")>
    Public Property LiveDate As DateTime?
      Get
        Return GetProperty(LiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LiveDateProperty, Value)
      End Set
    End Property

    Public Shared HighlightsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HighlightsInd, "Highlights", False)
    ''' <summary>
    ''' Gets and sets the Highlights value
    ''' </summary>
    <Display(Name:="Highlights", Description:=""),
    Required(ErrorMessage:="Highlights required")>
    Public Property HighlightsInd() As Boolean
      Get
        Return GetProperty(HighlightsIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HighlightsIndProperty, Value)
      End Set
    End Property

    Public Shared LiveEndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.LiveEndDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Live Date value
    ''' </summary>
    <Display(Name:="Live End", Description:=""),
    Required(ErrorMessage:="Live End required")>
    Public Property LiveEndDate As DateTime?
      Get
        Return GetProperty(LiveEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LiveEndDateProperty, Value)
      End Set
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue")
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(VenueProperty, Value)
      End Set
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets and sets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(LocationProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.RoomID, Nothing)
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Room", Description:=""),
     DropDownWeb(GetType(OBLib.Maintenance.Rooms.ReadOnly.RORoomAvailabilityTimeSlotList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                 BeforeFetchJS:="SynergyEventBO.setCriteriaBeforeRefresh", PreFindJSFunction:="SynergyEventBO.triggerAutoPopulate", AfterFetchJS:="SynergyEventBO.afterRoomRefreshAjax",
                 LookupMember:="Room", ValueMember:="RoomID", DropDownColumns:={"RoomDescription", "BookingDescription", "SlotTimeDescription", "DurationDescription", "BookingDurationDescription"},
                 OnItemSelectJSFunction:="SynergyEventBO.onRoomSelected", DropDownCssClass:="room-dropdown"),
    SetExpression("SynergyEventBO.RoomIDSet(self)")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared RoomProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Room, "")
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property Room As String
      Get
        Return GetProperty(RoomProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(RoomProperty, Value)
      End Set
    End Property

    Public Shared RoomClashCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RoomClashCount, "Room", 0)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Room")>
    Public Property RoomClashCount() As Integer
      Get
        Return GetProperty(RoomClashCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RoomClashCountProperty, Value)
      End Set
    End Property

    Public Shared CallTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.CallTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Call Time value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    SetExpression("SynergyEventBO.CallTimeSet(self)")>
    Public Property CallTime() As DateTime?
      Get
        Return GetProperty(CallTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(CallTimeProperty, Value)
      End Set
    End Property

    Public Shared WrapTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.WrapTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Wrap Time value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    SetExpression("SynergyEventBO.WrapTimeSet(self)")>
    Public Property WrapTime() As DateTime?
      Get
        Return GetProperty(WrapTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(WrapTimeProperty, Value)
      End Set
    End Property

    Public Shared FirstStartProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.FirstStart, Nothing)
    ''' <summary>
    ''' Gets and sets the First Start value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required"),
    SetExpression("SynergyEventBO.FirstStartSet(self)")>
    Public Property FirstStart As DateTime?
      Get
        Return GetProperty(FirstStartProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(FirstStartProperty, Value)
      End Set
    End Property

    Public Shared LastEndProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.LastEnd, Nothing)
    ''' <summary>
    ''' Gets and sets the Last End value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required"),
    SetExpression("SynergyEventBO.LastEndSet(self)")>
    Public Property LastEnd As DateTime?
      Get
        Return GetProperty(LastEndProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LastEndProperty, Value)
      End Set
    End Property

    Public Shared ProductionIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production value
    ''' </summary>
    <Display(Name:="Production", Description:="")>
    Public Property ProductionID() As Integer?
      Get
        Return GetProperty(ProductionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionIDProperty, Value)
      End Set
    End Property

    Public Shared BookingCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BookingCount, "Bookings")
    ''' <summary>
    ''' Gets and sets the Booking Count value
    ''' </summary>
    <Display(Name:="Bookings", Description:="")>
    Public Property BookingCount() As Integer
      Get
        Return GetProperty(BookingCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(BookingCountProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System ID value
    ''' </summary>
    <Display(Name:="Sub-Dept"),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(UserSystemList), ValueMember:="SystemID", DisplayMember:="SystemName"),
    SetExpression("SynergyEventBO.SystemIDSet(self)")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Area"),
    Required(ErrorMessage:="Area required"),
    DropDownWeb(GetType(ROSystemProductionAreaList), ThisFilterMember:="SystemID", ValueMember:="ProductionAreaID", DisplayMember:="ProductionArea"),
    SetExpression("SynergyEventBO.ProductionAreaIDSet(self)")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared RefreshCounterProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RefreshCounter, "Room", 1)
    ''' <summary>
    ''' Gets and sets the Production Area ID value
    ''' </summary>
    <Display(Name:="Room"), AlwaysClean>
    Public Property RefreshCounter() As Integer
      Get
        Return GetProperty(RefreshCounterProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(RefreshCounterProperty, Value)
      End Set
    End Property

    Public Shared BookingTimesProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.BookingTimes, "")
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Booking Times", Description:=""), AlwaysClean>
    Public Property BookingTimes As String
      Get
        Return GetProperty(BookingTimesProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(BookingTimesProperty, Value)
      End Set
    End Property

    Public Shared TimesValidProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.TimesValid, True)
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Booking Times", Description:=""), AlwaysClean>
    Public Property TimesValid As Boolean
      Get
        Return GetProperty(TimesValidProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(TimesValidProperty, Value)
      End Set
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Vehicle", Description:="The vehicle that is assigned to the production"),
    DropDownWeb(GetType(ROVehicleList),
                BeforeFetchJS:="SynergyEventBO.setVehicleCriteriaBeforeRefresh",
                PreFindJSFunction:="SynergyEventBO.triggerVehicleAutoPopulate",
                AfterFetchJS:="SynergyEventBO.afterVehicleRefreshAjax",
                OnItemSelectJSFunction:="SynergyEventBO.onVehicleSelected",
                LookupMember:="VehicleName", DisplayMember:="VehicleName", ValueMember:="VehicleID",
                DropDownCssClass:="vehicle-dropdown", DropDownColumns:={"VehicleName", "PreviousProduction", "CurrentProduction", "NextProduction"}),
    SetExpressionBeforeChange("SynergyEventBO.VehicleIDBeforeSet(self)"),
    SetExpression("SynergyEventBO.VehicleIDSet(self)")>
    Public Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared VehicleNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.VehicleName, "Vehicle Name", "")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Vehicle Name", Description:="Name of the vehicle")>
    Public Property VehicleName() As String
      Get
        Return GetProperty(VehicleNameProperty)
      End Get
      Set(value As String)
        SetProperty(VehicleNameProperty, value)
      End Set
    End Property

    Public Shared ProductionVenueIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionVenueID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Venue", Description:=""),
    DropDownWeb(GetType(ROProductionVenueList),
                 BeforeFetchJS:="SynergyEventBO.setProductionVenueCriteriaBeforeRefresh",
                 PreFindJSFunction:="SynergyEventBO.triggerProductionVenueAutoPopulate",
                 AfterFetchJS:="SynergyEventBO.afterProductionVenueRefreshAjax",
                 OnItemSelectJSFunction:="SynergyEventBO.onProductionVenueSelected",
                 LookupMember:="ProductionVenue", DisplayMember:="ProductionVenue", ValueMember:="ProductionVenueID",
                 DropDownCssClass:="hr-dropdown", DropDownColumns:={"ProductionVenue", "City"}),
    SetExpression("SynergyEventBO.ProductionVenueIDSet(self)")>
    Public Property ProductionVenueID() As Integer?
      Get
        Return GetProperty(ProductionVenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionVenueIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionVenue, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionVenueProperty, Value)
      End Set
    End Property

    Public Shared TeamsPlayingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamsPlaying, "Teams", "")
    ''' <summary>
    ''' Gets the Production Timeline Type value
    ''' </summary>
    <Display(Name:="Teams")>
    Public Property TeamsPlaying() As String
      Get
        Return GetProperty(TeamsPlayingProperty)
      End Get
      Set(value As String)
        SetProperty(TeamsPlayingProperty, value)
      End Set
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionTypeID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Genre", Description:="")>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSpecRequirementIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionSpecRequirementID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="Spec", Description:=""),
    DropDownWeb(GetType(ROProductionSpecRequirementList),
                BeforeFetchJS:="SynergyEventBO.setProductionSpecRequirementCriteriaBeforeRefresh",
                PreFindJSFunction:="SynergyEventBO.triggerProductionSpecRequirementAutoPopulate",
                AfterFetchJS:="SynergyEventBO.afterProductionSpecRequirementRefreshAjax",
                OnItemSelectJSFunction:="SynergyEventBO.onProductionSpecRequirementSelected",
                LookupMember:="ProductionSpecRequirementName", DisplayMember:="ProductionSpecRequirementName", ValueMember:="ProductionSpecRequirementID",
                DropDownCssClass:="hr-dropdown", DropDownColumns:={"ProductionSpecRequirementName", "ProductionType", "EventType", "StartDate", "EndDate"}),
    SetExpression("SynergyEventBO.ProductionSpecRequirementIDSet(self)")>
    Public Property ProductionSpecRequirementID() As Integer?
      Get
        Return GetProperty(ProductionSpecRequirementIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSpecRequirementIDProperty, Value)
      End Set
    End Property
    ', "SubDept", "Area"

    Public Shared ProductionSpecRequirementNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionSpecRequirementName, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Spec", Description:="")>
    Public Property ProductionSpecRequirementName() As String
      Get
        Return GetProperty(ProductionSpecRequirementNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionSpecRequirementNameProperty, Value)
      End Set
    End Property

    Public Shared ProductionRefNoProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionRefNo, "")
    ''' <summary>
    ''' Gets and sets the Teams Playing value
    ''' </summary>
    <Display(Name:="Ref", Description:="")>
    Public Property ProductionRefNo() As String
      Get
        Return GetProperty(ProductionRefNoProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionRefNoProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, Nothing)
    ''' <summary>
    ''' Gets and sets the Production Venue value
    ''' , DropDownType:=DropDownWeb.SelectType.AutoComplete
    ''' </summary>
    <Display(Name:="ID", Description:="")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared IsVisionViewProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsVisionView, False)
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Vis. View", Description:="")>
    Public Property IsVisionView As Boolean
      Get
        Return GetProperty(IsVisionViewProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsVisionViewProperty, Value)
      End Set
    End Property

    Public Shared IsAlstonElliotProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsAlstonElliot, False)
    ''' <summary>
    ''' Gets and sets the Room ID value
    ''' </summary>
    <Display(Name:="Al. Elliot", Description:="")>
    Public Property IsAlstonElliot As Boolean
      Get
        Return GetProperty(IsAlstonElliotProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsAlstonElliotProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ImportedEventIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.GenreSeries.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Synergy Event")
        Else
          Return String.Format("Blank {0}", "Synergy Event")
        End If
      Else
        Return Me.GenreSeries
      End If

    End Function

    'Public Function AllocateRoom() As Singular.Web.Result

    '  Dim ImportedEventChannels As New List(Of Integer)

    '  For Each ch As SynergyEventChannel In Me.SynergyEventChannelList
    '    If ch.IsSelected Then
    '      ImportedEventChannels.Add(ch.ImportedEventChannelID)
    '    End If
    '  Next

    '  Dim cmd As New Singular.CommandProc("CmdProcs.cmdResourceSchedulerAllocateRoom",
    '                                     {"@RoomID",
    '                                      "@CallTime",
    '                                      "@OnAirTimeStart",
    '                                      "@OnAirTimeEnd",
    '                                      "@WrapTime",
    '                                      "@GenreSeries",
    '                                      "@Title",
    '                                      "@ModifiedBy",
    '                                      "@SystemID",
    '                                      "@ProductionAreaID",
    '                                      "@ProductionID",
    '                                      "@ImportedEventID",
    '                                      "@GenRefNumber",
    '                                      "@SuppressFeedback",
    '                                      "@ImportedEventChannelIDs"},
    '                                     {NothingDBNull(RoomID),
    '                                      NothingDBNull(CallTime),
    '                                      NothingDBNull(FirstStart),
    '                                      NothingDBNull(LastEnd),
    '                                      NothingDBNull(WrapTime),
    '                                      GenreSeries,
    '                                      Title,
    '                                      OBLib.Security.Settings.CurrentUserID,
    '                                      NothingDBNull(SystemID),
    '                                      NothingDBNull(ProductionAreaID),
    '                                      NothingDBNull(ProductionID),
    '                                      NothingDBNull(ImportedEventID),
    '                                      NothingDBNull(GenRefNumber),
    '                                      False,
    '                                      OBMisc.IntegerListToXML(ImportedEventChannels)})

    '  cmd.FetchType = CommandProc.FetchTypes.DataSet
    '  cmd = cmd.Execute

    '  'Header Results
    '  Dim headerRow = cmd.Dataset.Tables(0).Rows(0)
    '  Dim Success As Boolean = False
    '  Dim HasClashes As Boolean = False
    '  Dim RoomScheduleAdded As Boolean = False
    '  Dim AreaAdded As Boolean = False
    '  Dim BookingAdded As Boolean = False

    '  Success = headerRow(0)
    '  HasClashes = headerRow(1)
    '  RoomScheduleAdded = headerRow(2)
    '  AreaAdded = headerRow(3)
    '  BookingAdded = headerRow(4)

    '  If Success Then
    '    Return New Singular.Web.Result(True)
    '  Else
    '    If HasClashes Then
    '      Return New Singular.Web.Result(False) With {.ErrorText = "Clashes were found"}
    '    ElseIf RoomScheduleAdded Then
    '      Return New Singular.Web.Result(False) With {.ErrorText = "Room Booking could not be created"}
    '    ElseIf AreaAdded Then
    '      Return New Singular.Web.Result(False) With {.ErrorText = "Area could not be added"}
    '    ElseIf BookingAdded Then
    '      Return New Singular.Web.Result(False) With {.ErrorText = "Resource Booking could not be created"}
    '    End If
    '  End If

    '  Return New Singular.Web.Result(False) With {.ErrorText = "Error could not be determined"}

    'End Function

    Public Function AllocateOB() As Singular.Web.Result

      'Dim ImportedEventChannels As New List(Of Integer)
      'For Each ch As SynergyEventChannel In Me.SynergyEventChannelList
      '  If ch.IsSelected Then
      '    ImportedEventChannels.Add(ch.ImportedEventChannelID)
      '  End If
      'Next

      Dim cmd As New Singular.CommandProc("CmdProcs.cmdSynergyImporterSetupOBBooking",
                                         {"@CurrentUserID",
                                          "@ImportedEventID",
                                          "@UserSystemID",
                                          "@UserProductionAreaID",
                                          "@VehicleID",
                                          "@ProductionVenueID",
                                          "@ProductionSpecRequirementID",
                                          "@CreateProductionSystemArea",
                                          "@LiveDateStartTime",
                                          "@LiveDateEndTime",
                                          "@ProductionTypeID",
                                          "@Title",
                                          "@ProductionID",
                                          "@ProductionSystemAreaID",
                                          "@TeamsPlaying",
                                          "@IsVisionView",
                                          "@IsAlstonElliot"},
                                         {OBLib.Security.Settings.CurrentUserID,
                                          NothingDBNull(ImportedEventID),
                                          NothingDBNull(SystemID),
                                          NothingDBNull(ProductionAreaID),
                                          NothingDBNull(VehicleID),
                                          NothingDBNull(ProductionVenueID),
                                          NothingDBNull(ProductionSpecRequirementID),
                                          True,
                                          LiveDate,
                                          LiveEndDate,
                                          NothingDBNull(ProductionTypeID),
                                          Title,
                                          NothingDBNull(ProductionID),
                                          NothingDBNull(ProductionSystemAreaID),
                                          TeamsPlaying,
                                          IsVisionView,
                                          IsAlstonElliot})

      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute

      'Header Results
      Dim headerRow = cmd.Dataset.Tables(0).Rows(0)
      Dim Success As Boolean = False
      Dim ProductionRefNo As String = ""

      Success = (Not IsNullNothing(headerRow(1)))
      ProductionID = ZeroNothing(headerRow(0))
      ProductionSystemAreaID = ZeroNothing(headerRow(1))
      ProductionRefNo = headerRow(2)

      If Success Then
        Return New Singular.Web.Result(True)
      Else
        Return New Singular.Web.Result(False) With {.ErrorText = "Could not create OB"}
      End If

      Return New Singular.Web.Result(False) With {.ErrorText = "Error could not be determined"}

    End Function

    Public Sub LoadBookingTimes()
      Dim s As String = ""
      If CallTime IsNot Nothing Then
        s = CallTime.Value.ToString("dd MMM HH:mm")
      Else
        s = FirstStart.Value.ToString("dd MMM HH:mm")
      End If
      If WrapTime IsNot Nothing Then
        s = s & " - " & WrapTime.Value.ToString("dd MMM HH:mm")
      Else
        s = s & " - " & LastEnd.Value.ToString("dd MMM HH:mm")
      End If
      LoadProperty(BookingTimesProperty, s)
    End Sub

#End Region

#Region " Child Lists "

    'Public Shared SynergyEventChannelListProperty As PropertyInfo(Of SynergyEventChannelList) = RegisterProperty(Of SynergyEventChannelList)(Function(c) c.SynergyEventChannelList, "Imported Event Channel List")
    'Public ReadOnly Property SynergyEventChannelList() As SynergyEventChannelList
    '  Get
    '    If GetProperty(SynergyEventChannelListProperty) Is Nothing Then
    '      LoadProperty(SynergyEventChannelListProperty, Synergy.SynergyEventChannelList.NewSynergyEventChannelList())
    '    End If
    '    Return GetProperty(SynergyEventChannelListProperty)
    '  End Get
    'End Property

    Public Shared SlugItemListProperty As PropertyInfo(Of SlugItemList) = RegisterProperty(Of SlugItemList)(Function(c) c.SlugItemList, "SlugItemList")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property SlugItemList() As SlugItemList
      Get
        If GetProperty(SlugItemListProperty) Is Nothing Then
          LoadProperty(SlugItemListProperty, New SlugItemList)
        End If
        Return GetProperty(SlugItemListProperty)
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(RoomIDProperty)
        .AddTriggerProperties({CallTimeProperty, FirstStartProperty, LastEndProperty, WrapTimeProperty, RoomClashCountProperty})
        .JavascriptRuleFunctionName = "SynergyEventBO.RoomIDValid"
      End With

      With AddWebRule(TeamsPlayingProperty)
        .JavascriptRuleFunctionName = "SynergyEventBO.TeamsPlayingValid"
      End With

      With AddWebRule(LiveDateProperty)
        .AddTriggerProperties({LiveEndDateProperty})
        .AffectedProperties.Add(LiveEndDateProperty)
        .JavascriptRuleFunctionName = "SynergyEventBO.OBViewDateValid"
      End With

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSynergyEvent() method.

    End Sub

    Public Shared Function NewSynergyEvent() As SynergyEvent

      Return DataPortal.CreateChild(Of SynergyEvent)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetSynergyEvent(dr As SafeDataReader) As SynergyEvent

      Dim s As New SynergyEvent()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ImportedEventIDProperty, .GetInt32(0))
          LoadProperty(GenRefNumberProperty, .GetInt64(1))
          LoadProperty(GenreSeriesProperty, .GetString(2))
          LoadProperty(TitleProperty, .GetString(3))
          LoadProperty(LiveDateProperty, .GetValue(4))
          LoadProperty(HighlightsIndProperty, .GetBoolean(5))
          LoadProperty(LiveEndDateProperty, .GetValue(6))
          LoadProperty(VenueProperty, .GetString(7))
          LoadProperty(LocationProperty, .GetString(8))
          LoadProperty(RoomIDProperty, ZeroNothing(.GetInt32(9)))
          LoadProperty(CallTimeProperty, .GetValue(10))
          LoadProperty(WrapTimeProperty, .GetValue(11))
          LoadProperty(FirstStartProperty, .GetValue(12))
          LoadProperty(LastEndProperty, .GetValue(13))
          LoadProperty(ProductionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(BookingCountProperty, .GetInt32(15))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(16)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(17)))
          LoadProperty(ProductionVenueIDProperty, ZeroNothing(.GetInt32(18)))
          LoadProperty(ProductionVenueProperty, .GetString(19))
          LoadProperty(TeamsPlayingProperty, .GetString(20))
          LoadProperty(ProductionTypeIDProperty, ZeroNothing(.GetInt32(21)))
          LoadProperty(ProductionIDProperty, ZeroNothing(.GetInt32(22)))
          LoadProperty(ProductionRefNoProperty, .GetString(23))
          LoadProperty(ProductionSystemAreaIDProperty, ZeroNothing(.GetInt32(24)))
          '25
          LoadProperty(IsVisionViewProperty, .GetBoolean(26))
          LoadProperty(IsAlstonElliotProperty, .GetBoolean(27))
        End With
      End Using
      LoadBookingTimes()

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ImportedEventIDProperty)
      AddOutputParamNullable(cm, ProductionSystemAreaIDProperty)

      cm.Parameters.AddWithValue("@GenRefNumber", GetProperty(GenRefNumberProperty))
      cm.Parameters.AddWithValue("@GenreSeries", GetProperty(GenreSeriesProperty))
      cm.Parameters.AddWithValue("@Title", GetProperty(TitleProperty))
      cm.Parameters.AddWithValue("@LiveDate", LiveDate)
      cm.Parameters.AddWithValue("@HighlightsInd", GetProperty(HighlightsIndProperty))
      cm.Parameters.AddWithValue("@LiveDate", LiveDate)
      cm.Parameters.AddWithValue("@Venue", GetProperty(VenueProperty))
      cm.Parameters.AddWithValue("@Location", GetProperty(LocationProperty))
      cm.Parameters.AddWithValue("@RoomID", GetProperty(RoomIDProperty))
      cm.Parameters.AddWithValue("@CallTime", GetProperty(CallTimeProperty))
      cm.Parameters.AddWithValue("@WrapTime", GetProperty(WrapTimeProperty))
      cm.Parameters.AddWithValue("@FirstStart", FirstStart)
      cm.Parameters.AddWithValue("@LastEnd", LastEnd)
      cm.Parameters.AddWithValue("@ProductionID", GetProperty(ProductionIDProperty))
      cm.Parameters.AddWithValue("@BookingCount", GetProperty(BookingCountProperty))
      cm.Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      cm.Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
      cm.Parameters.AddWithValue("@ProductionVenueID", NothingDBNull(GetProperty(ProductionVenueIDProperty)))
      cm.Parameters.AddWithValue("@ProductionVenue", GetProperty(ProductionVenueProperty))
      cm.Parameters.AddWithValue("@TeamsPlaying", GetProperty(TeamsPlayingProperty))
      cm.Parameters.AddWithValue("@ProductionID", NothingDBNull(GetProperty(ProductionIDProperty)))
      cm.Parameters.AddWithValue("@ProductionRefNo", GetProperty(ProductionRefNoProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ImportedEventIDProperty, cm.Parameters("@ImportedEventID").Value)
                 LoadProperty(ProductionSystemAreaIDProperty, cm.Parameters("@ProductionSystemAreaID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ImportedEventID", GetProperty(ImportedEventIDProperty))
    End Sub

#End Region

  End Class

End Namespace