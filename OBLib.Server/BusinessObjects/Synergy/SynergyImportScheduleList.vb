﻿' Generated 08 Apr 2014 09:59 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.SynergyWebService

Namespace Synergy.Importer.New

  <Serializable()> _
  Public Class SynergyImportScheduleList
    Inherits OBBusinessListBase(Of SynergyImportScheduleList, SynergyImportSchedule)

#Region " Business Methods "

    Public Function GetItem(SynergyImportScheduleID As Integer) As SynergyImportSchedule

      For Each child As SynergyImportSchedule In Me
        If child.SynergyImportScheduleID = SynergyImportScheduleID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetItems(Keyword As String) As SynergyImportScheduleList

      Dim lst As New SynergyImportScheduleList
      For Each child As SynergyImportSchedule In Me
        If child.SeriesTitle.ToLower.Contains(Keyword.ToLower) Or child.Title.ToLower.Contains(Keyword.ToLower) Then
          lst.Add(child)
        End If
      Next
      Return lst

    End Function

    Public Overrides Function ToString() As String

      Return "Synergy Import Schedules"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewSynergyImportScheduleList() As SynergyImportScheduleList

      Return New SynergyImportScheduleList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As SynergyImportSchedule In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As SynergyImportSchedule In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace