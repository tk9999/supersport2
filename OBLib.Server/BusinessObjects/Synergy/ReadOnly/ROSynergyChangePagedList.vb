﻿' Generated 31 Aug 2016 14:00 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy.ReadOnly

  <Serializable()> _
  Public Class ROSynergyChangePagedList
    Inherits OBReadOnlyListBase(Of ROSynergyChangePagedList, ROSynergyChangePaged)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "


    ' ** Example Paging property declaration code to be implemented in the necessary VM **
    'Public Property ROSynergyChangePagedList As ROSynergyChangePagedList
    'Public Property ROSynergyChangePagedListCriteria As ROSynergyChangePagedList.Criteria
    'Public Property ROSynergyChangePagedListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    ' ** Example Paging definition code to be implemented in the necessary VM Setup Method **
    'ROSynergyChangePagedList = New ROSynergyChangePagedList
    'ROSynergyChangePagedListCriteria = New ROSynergyChangePagedList.Criteria
    'ROSynergyChangePagedListManager = New Singular.Web.Data.PagedDataManager(Of VMType)(Function(d) d.ROSynergyChangePagedList, Function(d) d.ROSynergyChangePagedListCriteria, "SortBy", PageSize, SortAsc)

    Public Function GetItem(SynergyChangeID As Integer) As ROSynergyChangePaged

      For Each child As ROSynergyChangePaged In Me
        If child.SynergyChangeID = SynergyChangeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Synergy Changes"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property SynergyImportID As Integer? = Nothing

      <Display(Name:="Gen Ref"), SetExpression("ROSynergyChangePagedListCriteriaBO.GenRefNumberSet(self)")>
      Public Property GenRefNumber As Integer? = Nothing

      <Display(Name:="Schedule Num."), SetExpression("ROSynergyChangePagedListCriteriaBO.ScheduleNumberSet(self)")>
      Public Property ScheduleNumber As Integer? = Nothing

      <Display(Name:="Start Date"), SetExpression("ROSynergyChangePagedListCriteriaBO.ChangeStartDateTimeSet(self)")>
      Public Property ChangeStartDateTime As DateTime? = Nothing

      <Display(Name:="End Date"), SetExpression("ROSynergyChangePagedListCriteriaBO.ChangeEndDateTimeSet(self)")>
      Public Property ChangeEndDateTime As DateTime? = Nothing

      <Display(Name:="Change Type"), SetExpression("ROSynergyChangePagedListCriteriaBO.SynergyChangeTypeIDSet(self)"),
      DropDownWeb(GetType(ROSynergyChangeTypeList), DisplayMember:="SynergyChangeType", ValueMember:="SynergyChangeTypeID")>
      Public Property SynergyChangeTypeID As Integer? = Nothing

      <Display(Name:="Field"), SetExpression("ROSynergyChangePagedListCriteriaBO.ColumnNameSet(self)")>
      Public Property ColumnName As String = ""

      Public Sub New(SynergyImportID As Integer?, GenRefNumber As Integer?, ScheduleNumber As Integer?, _
                     ChangeStartDateTime As DateTime?, SynergyChangeTypeID As Integer?, ColumnName As Integer?,
                     ChangeEndDateTime As DateTime?)

        Me.SynergyImportID = SynergyImportID
        Me.GenRefNumber = GenRefNumber
        Me.ScheduleNumber = ScheduleNumber
        Me.ChangeStartDateTime = ChangeStartDateTime
        Me.SynergyChangeTypeID = SynergyChangeTypeID
        Me.ColumnName = ColumnName
        Me.ChangeEndDateTime = ChangeEndDateTime

      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROSynergyChangePagedList() As ROSynergyChangePagedList

      Return New ROSynergyChangePagedList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSynergyChangePagedList() As ROSynergyChangePagedList

      Return DataPortal.Fetch(Of ROSynergyChangePagedList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSynergyChangePaged.GetROSynergyChangePaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSynergyChangePagedList"
            cm.Parameters.AddWithValue("@SynergyImportID", Singular.Misc.NothingDBNull(crit.SynergyImportID))
            cm.Parameters.AddWithValue("@GenRefNumber", Singular.Misc.NothingDBNull(crit.GenRefNumber))
            cm.Parameters.AddWithValue("@ScheduleNumber", Singular.Misc.NothingDBNull(crit.ScheduleNumber))
            cm.Parameters.AddWithValue("@ChangeStartDateTime", Singular.Misc.NothingDBNull(crit.ChangeStartDateTime))
            cm.Parameters.AddWithValue("@ChangeEndDateTime", Singular.Misc.NothingDBNull(crit.ChangeEndDateTime))
            cm.Parameters.AddWithValue("@SynergyChangeTypeID", Singular.Misc.NothingDBNull(crit.SynergyChangeTypeID))
            cm.Parameters.AddWithValue("@ColumnName", Strings.MakeEmptyDBNull(crit.ColumnName))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace