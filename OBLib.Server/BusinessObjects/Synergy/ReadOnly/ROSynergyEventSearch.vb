﻿' Generated 21 Apr 2017 14:44 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy.ReadOnly

  <Serializable()> _
  Public Class ROSynergyEventSearch
    Inherits OBReadOnlyBase(Of ROSynergyEventSearch)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSynergyEventSearchBO.ROSynergyEventSearchBOToString(self)")

    <Display(Name:="Is Selected"),
    SetExpression("ROSynergyEventSearchBO.IsSelectedSet(self)")>
    Public Overrides ReadOnly Property IsSelected() As Boolean
      Get
        Return GetProperty(IsSelectedProperty)
      End Get
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
    End Property

    Public Shared GenreSeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreSeries, "Genre Series")
    ''' <summary>
    ''' Gets the Genre Series value
    ''' </summary>
    <Display(Name:="Genre Series", Description:="")>
    Public ReadOnly Property GenreSeries() As String
      Get
        Return GetProperty(GenreSeriesProperty)
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.LiveDate, "Live Date")
    ''' <summary>
    ''' Gets the Live Date value
    ''' </summary>
    <Display(Name:="Live Date", Description:="")>
    Public ReadOnly Property LiveDate As Date
      Get
        Return GetProperty(LiveDateProperty)
      End Get
    End Property

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country")
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="")>
    Public ReadOnly Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public ReadOnly Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue")
    ''' <summary>
    ''' Gets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public ReadOnly Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
    End Property

    Public Shared StringRowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StringRowNo, "String Row No")
    ''' <summary>
    ''' Gets the String Row No value
    ''' </summary>
    <Display(Name:="String Row No", Description:="")>
    Public ReadOnly Property StringRowNo() As Integer
      Get
        Return GetProperty(StringRowNoProperty)
      End Get
    End Property

    Public Shared DateRowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DateRowNo, "Date Row No")
    ''' <summary>
    ''' Gets the Date Row No value
    ''' </summary>
    <Display(Name:="Date Row No", Description:="")>
    Public ReadOnly Property DateRowNo() As Integer
      Get
        Return GetProperty(DateRowNoProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public ReadOnly Property StartDateTime As Date
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
    Public ReadOnly Property EndDateTime As Date
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:="")>
    Public ReadOnly Property RowNo() As Integer
      Get
        Return GetProperty(RowNoProperty)
      End Get
    End Property

    Public Shared StartTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StartTimeString, "Start Time")
    ''' <summary>
    ''' Gets the Start Time String value
    ''' </summary>
    <Display(Name:="Start Time", Description:="")>
    Public ReadOnly Property StartTimeString() As String
      Get
        Return GetProperty(StartTimeStringProperty)
      End Get
    End Property

    Public Shared EndTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EndTimeString, "End Time")
    ''' <summary>
    ''' Gets the End Time String value
    ''' </summary>
    <Display(Name:="End Time", Description:="")>
    Public ReadOnly Property EndTimeString() As String
      Get
        Return GetProperty(EndTimeStringProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(GenRefNumberProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Title

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSynergyEventSearch(dr As SafeDataReader) As ROSynergyEventSearch

      Dim r As New ROSynergyEventSearch()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(GenRefNumberProperty, .GetInt64(0))
        LoadProperty(GenreSeriesProperty, .GetString(1))
        LoadProperty(TitleProperty, .GetString(2))
        LoadProperty(LiveDateProperty, .GetValue(3))
        LoadProperty(CountryProperty, .GetString(4))
        LoadProperty(LocationProperty, .GetString(5))
        LoadProperty(VenueProperty, .GetString(6))
        LoadProperty(StringRowNoProperty, .GetInt32(7))
        LoadProperty(DateRowNoProperty, .GetInt32(8))
        LoadProperty(StartDateTimeProperty, .GetValue(9))
        LoadProperty(EndDateTimeProperty, .GetValue(10))
        LoadProperty(RowNoProperty, .GetInt32(11))
        LoadProperty(StartTimeStringProperty, .GetString(12))
        LoadProperty(EndTimeStringProperty, .GetString(13))
      End With

    End Sub

#End Region

  End Class

End Namespace