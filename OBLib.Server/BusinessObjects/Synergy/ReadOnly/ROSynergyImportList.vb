﻿' Generated 17 Jul 2015 07:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Synergy.ReadOnly

  <Serializable()> _
  Public Class ROSynergyImportList
    Inherits SingularReadOnlyListBase(Of ROSynergyImportList, ROSynergyImport)

#Region " Business Methods "

    Public Function GetItem(SynergyImportID As Integer) As ROSynergyImport

      For Each child As ROSynergyImport In Me
        If child.SynergyImportID = SynergyImportID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Synergy Imports"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property FetchLatestImport As Boolean = False

      Public Sub New(FetchLatestImport As Boolean)
        Me.FetchLatestImport = FetchLatestImport
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSynergyImportList() As ROSynergyImportList

      Return New ROSynergyImportList()

    End Function

    Public Shared Sub BeginGetROSynergyImportList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSynergyImportList)))

      Dim dp As New DataPortal(Of ROSynergyImportList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSynergyImportList(CallBack As EventHandler(Of DataPortalResult(Of ROSynergyImportList)))

      Dim dp As New DataPortal(Of ROSynergyImportList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSynergyImportList() As ROSynergyImportList

      Return DataPortal.Fetch(Of ROSynergyImportList)(New Criteria())

    End Function

    Public Shared Function GetROSynergyImportList(FetchLatestImport As Boolean) As ROSynergyImportList

      Return DataPortal.Fetch(Of ROSynergyImportList)(New Criteria(FetchLatestImport))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSynergyImport.GetROSynergyImport(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSynergyImportList"
            cm.Parameters.AddWithValue("@FetchLatestImport", crit.FetchLatestImport)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace