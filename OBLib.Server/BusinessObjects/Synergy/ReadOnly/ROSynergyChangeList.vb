﻿' Generated 31 Aug 2016 14:00 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy.ReadOnly

  <Serializable()> _
  Public Class ROSynergyChangeList
    Inherits OBReadOnlyListBase(Of ROSynergyChangeList, ROSynergyChange)

#Region " Business Methods "


    ' ** Example Paging property declaration code to be implemented in the necessary VM **
    'Public Property ROSynergyChangeList As ROSynergyChangeList
    'Public Property ROSynergyChangeListCriteria As ROSynergyChangeList.Criteria
    'Public Property ROSynergyChangeListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    ' ** Example Paging definition code to be implemented in the necessary VM Setup Method **
    'ROSynergyChangeList = New ROSynergyChangeList
    'ROSynergyChangeListCriteria = New ROSynergyChangeList.Criteria
    'ROSynergyChangeListManager = New Singular.Web.Data.PagedDataManager(Of VMType)(Function(d) d.ROSynergyChangeList, Function(d) d.ROSynergyChangeListCriteria, "SortBy", PageSize, SortAsc)

    Public Function GetItem(SynergyChangeID As Integer) As ROSynergyChange

      For Each child As ROSynergyChange In Me
        If child.SynergyChangeID = SynergyChangeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Synergy Changes"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property SynergyImportID As Integer? = Nothing

      <Display(Name:="Gen Ref"), SetExpression("ROSynergyChangeListCriteriaBO.GenRefNumberSet(self)")>
      Public Property GenRefNumber As Integer? = Nothing

      <Display(Name:="Schedule Num."), SetExpression("ROSynergyChangeListCriteriaBO.ScheduleNumberSet(self)")>
      Public Property ScheduleNumber As Integer? = Nothing

      <Display(Name:="Start Date"), SetExpression("ROSynergyChangeListCriteriaBO.ChangeStartDateTimeSet(self)")>
      Public Property ChangeStartDateTime As DateTime? = Nothing

      <Display(Name:="End Date"), SetExpression("ROSynergyChangeListCriteriaBO.ChangeEndDateTimeSet(self)")>
      Public Property ChangeEndDateTime As DateTime? = Nothing

      <Display(Name:="Change Type"), SetExpression("ROSynergyChangeListCriteriaBO.SynergyChangeTypeIDSet(self)"),
      DropDownWeb(GetType(ROSynergyChangeTypeList), DisplayMember:="SynergyChangeType", ValueMember:="SynergyChangeTypeID")>
      Public Property SynergyChangeTypeID As Integer? = Nothing

      <Display(Name:="Field"), SetExpression("ROSynergyChangeListCriteriaBO.ColumnNameSet(self)")>
      Public Property ColumnName As String = ""

      Public Sub New(SynergyImportID As Integer?, GenRefNumber As Integer?, ScheduleNumber As Integer?, _
                     ChangeStartDateTime As DateTime?, SynergyChangeTypeID As Integer?, ColumnName As String,
                     ChangeEndDateTime As DateTime?)

        Me.SynergyImportID = SynergyImportID
        Me.GenRefNumber = GenRefNumber
        Me.ScheduleNumber = ScheduleNumber
        Me.ChangeStartDateTime = ChangeStartDateTime
        Me.SynergyChangeTypeID = SynergyChangeTypeID
        Me.ColumnName = ColumnName
        Me.ChangeEndDateTime = ChangeEndDateTime

      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROSynergyChangeList() As ROSynergyChangeList

      Return New ROSynergyChangeList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSynergyChangeList() As ROSynergyChangeList

      Return DataPortal.Fetch(Of ROSynergyChangeList)(New Criteria())

    End Function

    Public Shared Function GetROSynergyChangeList(SynergyChangeID As Integer?) As ROSynergyChangeList

      Return DataPortal.Fetch(Of ROSynergyChangeList)(New Criteria(SynergyChangeID, Nothing, Nothing, Nothing, Nothing, "", Nothing))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSynergyChange.GetROSynergyChange(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSynergyChangeList"
            cm.Parameters.AddWithValue("@SynergyImportID", Singular.Misc.NothingDBNull(crit.SynergyImportID))
            cm.Parameters.AddWithValue("@GenRefNumber", Singular.Misc.NothingDBNull(crit.GenRefNumber))
            cm.Parameters.AddWithValue("@ScheduleNumber", Singular.Misc.NothingDBNull(crit.ScheduleNumber))
            cm.Parameters.AddWithValue("@ChangeStartDateTime", Singular.Misc.NothingDBNull(crit.ChangeStartDateTime))
            cm.Parameters.AddWithValue("@ChangeEndDateTime", Singular.Misc.NothingDBNull(crit.ChangeEndDateTime))
            cm.Parameters.AddWithValue("@SynergyChangeTypeID", Singular.Misc.NothingDBNull(crit.SynergyChangeTypeID))
            cm.Parameters.AddWithValue("@ColumnName", Strings.MakeEmptyDBNull(crit.ColumnName))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace