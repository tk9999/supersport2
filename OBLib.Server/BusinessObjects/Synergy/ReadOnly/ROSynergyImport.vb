﻿' Generated 17 Jul 2015 07:42 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Synergy.ReadOnly

  <Serializable()> _
  Public Class ROSynergyImport
    Inherits SingularReadOnlyBase(Of ROSynergyImport)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SynergyImportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SynergyImportID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property SynergyImportID() As Integer
      Get
        Return GetProperty(SynergyImportIDProperty)
      End Get
    End Property

    Public Shared ImportDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ImportDateTime, "Import Date Time")
    ''' <summary>
    ''' Gets the Import Date Time value
    ''' </summary>
    <Display(Name:="Import Date Time", Description:="")>
    Public ReadOnly Property ImportDateTime As DateTime?
      Get
        Return GetProperty(ImportDateTimeProperty)
      End Get
    End Property

    Public Shared ImportedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ImportedByUserID, "Imported By User", Nothing)
    ''' <summary>
    ''' Gets the Imported By User value
    ''' </summary>
    <Display(Name:="Imported By User", Description:="")>
    Public ReadOnly Property ImportedByUserID() As Integer?
      Get
        Return GetProperty(ImportedByUserIDProperty)
      End Get
    End Property

    Public Shared ImportedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ImportedBy, "Imported By")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Imported By", Description:="")>
    Public ReadOnly Property ImportedBy() As String
      Get
        Return GetProperty(ImportedByProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SynergyImportIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SynergyImportID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSynergyImport(dr As SafeDataReader) As ROSynergyImport

      Dim r As New ROSynergyImport()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SynergyImportIDProperty, .GetInt32(0))
        LoadProperty(ImportDateTimeProperty, .GetValue(1))
        LoadProperty(ImportedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ImportedByProperty, .GetString(3))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace