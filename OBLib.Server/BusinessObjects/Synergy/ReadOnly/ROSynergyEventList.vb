﻿' Generated 12 May 2015 15:25 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Synergy.ReadOnly

  <Serializable()> _
  Public Class ROSynergyEventList
    Inherits SingularReadOnlyListBase(Of ROSynergyEventList, ROSynergyEvent)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

    Private mLastImportDateTime As DateTime? = Nothing
    Public ReadOnly Property LastImportDateTime As DateTime?
      Get
        Return mLastImportDateTime
      End Get
    End Property

    Public ReadOnly Property LastImportDateDescription As String
      Get
        If mLastImportDateTime IsNot Nothing Then
          Dim diffInMinutes As Integer = Now.Subtract(mLastImportDateTime).TotalMinutes
          If diffInMinutes <= 60 Then
            Return "This data is " & Math.Round(diffInMinutes, 0).ToString & " minutes old"
          ElseIf diffInMinutes <= 1440 Then
            Return "This data is " & Math.Round((diffInMinutes / 60)).ToString & " hours old"
          ElseIf diffInMinutes <= 10080 Then
            Return "This data is " & Math.Round((diffInMinutes / 1440)).ToString & " days old"
          ElseIf diffInMinutes <= 282240 Then
            Return "This data is " & Math.Round((diffInMinutes / 10080)).ToString & " weeks old"
          Else
            Return "This data is " & Math.Round((diffInMinutes / 282240)).ToString & " month/s old"
          End If
        End If
        Return "Unknown"
      End Get
    End Property

    Private mTotalPages As Integer = 0
    Public ReadOnly Property TotalPages As Integer
      Get
        Return mTotalPages
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ImportedEventID As Integer) As ROSynergyEvent

      For Each child As ROSynergyEvent In Me
        If child.ImportedEventID = ImportedEventID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "R Os"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property Keyword As String = Nothing
      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate, Now)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Start Date", Description:=""),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property StartDate As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate, Now)
      ''' <summary>
      ''' Gets and sets the End Date value
      ''' </summary>
      <Display(Name:="End Date", Description:=""),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property EndDate As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Property PlanningMode As String = ""

      Public Shared IncludeUnknownDatesProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IncludeUnknownDates)
      ''' <summary>
      ''' Gets and sets the End Date value
      ''' </summary>
      <Display(Name:="Include Unknown Dates", Description:="")>
      Public Property IncludeUnknownDates As Boolean
        Get
          Return ReadProperty(IncludeUnknownDatesProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(IncludeUnknownDatesProperty, Value)
        End Set
      End Property

      Public Shared LiveProperty As PropertyInfo(Of Boolean?) = RegisterSProperty(Of Boolean?)(Function(c) c.Live, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Live", Description:="")>
      Public Property Live() As Boolean?
        Get
          Return ReadProperty(LiveProperty)
        End Get
        Set(ByVal Value As Boolean?)
          LoadProperty(LiveProperty, Value)
        End Set
      End Property

      Public Shared DelayedProperty As PropertyInfo(Of Boolean?) = RegisterSProperty(Of Boolean?)(Function(c) c.Delayed, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Delayed", Description:="")>
      Public Property Delayed() As Boolean?
        Get
          Return ReadProperty(DelayedProperty)
        End Get
        Set(ByVal Value As Boolean?)
          LoadProperty(DelayedProperty, Value)
        End Set
      End Property

      Public Shared PremierProperty As PropertyInfo(Of Boolean?) = RegisterSProperty(Of Boolean?)(Function(c) c.Premier, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Premier", Description:="")>
      Public Property Premier() As Boolean?
        Get
          Return ReadProperty(PremierProperty)
        End Get
        Set(ByVal Value As Boolean?)
          LoadProperty(PremierProperty, Value)
        End Set
      End Property

      Public Shared HighlightsIndProperty As PropertyInfo(Of Boolean?) = RegisterSProperty(Of Boolean?)(Function(c) c.HighlightsInd, Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Highlights", Description:="")>
      Public Property HighlightsInd() As Boolean?
        Get
          Return ReadProperty(HighlightsIndProperty)
        End Get
        Set(ByVal Value As Boolean?)
          LoadProperty(HighlightsIndProperty, Value)
        End Set
      End Property

      Public Shared SAIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SAInd, "SA", False)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="SA", Description:="")>
      Public Property SAInd() As Boolean
        Get
          Return ReadProperty(SAIndProperty)
        End Get
        Set(ByVal Value As Boolean)
          LoadProperty(SAIndProperty, Value)
        End Set
      End Property

      'Public Shared OBIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.OBInd, "OB", False)
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="OB", Description:="")>
      'Public Property OBInd() As Boolean
      '  Get
      '    Return ReadProperty(OBIndProperty)
      '  End Get
      '  Set(ByVal Value As Boolean)
      '    LoadProperty(OBIndProperty, Value)
      '  End Set
      'End Property

      'Public Shared StudioIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.StudioInd, "Studio", False)
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="Studio", Description:="")>
      'Public Property StudioInd() As Boolean
      '  Get
      '    Return ReadProperty(StudioIndProperty)
      '  End Get
      '  Set(ByVal Value As Boolean)
      '    LoadProperty(StudioIndProperty, Value)
      '  End Set
      'End Property

      'Public Shared SatOpsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SatOpsInd, "SatOps", False)
      ' ''' <summary>
      ' ''' Gets and sets the Production Venue value
      ' ''' </summary>
      '<Display(Name:="SatOps", Description:="")>
      'Public Property SatOpsInd() As Boolean
      '  Get
      '    Return ReadProperty(SatOpsIndProperty)
      '  End Get
      '  Set(ByVal Value As Boolean)
      '    LoadProperty(SatOpsIndProperty, Value)
      '  End Set
      'End Property

      'Public Property CurrentSystemID As Integer? = Nothing
      'Public Property CurrentProductionAreaID As Integer? = Nothing
      Public Property ImportedEventID As Integer? = Nothing
      Public Property GenRefNumber As Int64? = Nothing

      Public Shared ViewByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ViewBy, "SatOps", "Event")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="ViewBy", Description:="")>
      Public Property ViewBy() As String
        Get
          Return ReadProperty(ViewByProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ViewByProperty, Value)
        End Set
      End Property

      Public Shared ByDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.ByDate) _
                                                                   .AddSetExpression("ROSynergyEventListCriteriaBO.ByDateSet(self)", False)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Date", Description:=""),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property ByDate As DateTime?
        Get
          Return ReadProperty(ByDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(ByDateProperty, Value)
        End Set
      End Property

      <Browsable(True)>
      Public Overloads Property PageNo As Integer = 1

      <Browsable(True)>
      Public Overloads Property PageSize As Integer = 25

      <Browsable(True)>
      Public Overloads Property SortColumn As String = "StartDateTime"

      <Browsable(True)>
      Public Overloads Property SortAsc As Boolean = False

      Public Sub New(Keyword As String)

        Me.Keyword = Keyword
        ',CurrentSystemID As Integer?, CurrentProductionAreaID As Integer?
        'Me.CurrentSystemID = CurrentSystemID
        'Me.CurrentProductionAreaID = CurrentProductionAreaID

      End Sub

      Public Sub New(ImportedEventID As Integer?)
        Me.ImportedEventID = ImportedEventID
      End Sub

      Public Sub New(Keyword As String, StartDate As DateTime?, EndDate As DateTime,
                     IncludeUnknownDates As Boolean, Live As Boolean, Delayed As Boolean, Premier As Boolean,
                     HighlightsInd As Boolean, SAInd As Boolean,
                     ImportedEventID As Integer?, GenRefNumber As Int64?,
                     ViewBy As String,
                     PageNo As Integer, PageSize As Integer, SortAsc As Boolean, SortColumn As String)
        Me.Keyword = Keyword
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.IncludeUnknownDates = IncludeUnknownDates
        Me.Live = Live
        Me.Delayed = Delayed
        Me.Premier = Premier
        Me.HighlightsInd = HighlightsInd
        Me.SAInd = SAInd
        Me.ImportedEventID = ImportedEventID
        Me.GenRefNumber = GenRefNumber
        Me.ViewBy = ViewBy
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn
        MyBase.PageNo = PageNo
        MyBase.PageSize = PageSize
        MyBase.SortColumn = SortColumn
        MyBase.SortAsc = SortAsc
      End Sub

      Public Sub New(GenRefNumber As Int64)
        Me.StartDate = Nothing
        Me.EndDate = Nothing
        Me.GenRefNumber = GenRefNumber
        MyBase.PageNo = 1
        MyBase.PageSize = 20
        MyBase.SortAsc = True
        MyBase.SortColumn = "GenreSeries"
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSynergyEventList() As ROSynergyEventList

      Return New ROSynergyEventList()

    End Function

    Public Shared Sub BeginGetROSynergyEventList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSynergyEventList)))

      Dim dp As New DataPortal(Of ROSynergyEventList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub

    Public Shared Sub BeginGetROSynergyEventList(CallBack As EventHandler(Of DataPortalResult(Of ROSynergyEventList)))

      Dim dp As New DataPortal(Of ROSynergyEventList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSynergyEventList() As ROSynergyEventList

      Return DataPortal.Fetch(Of ROSynergyEventList)(New Criteria())

    End Function

    Public Shared Function GetROSynergyEventList(GenRefNumber As Int64) As ROSynergyEventList

      Return DataPortal.Fetch(Of ROSynergyEventList)(New Criteria(GenRefNumber))

    End Function

    Public Shared Function GetROSynergyEventList(Keyword As String, StartDate As DateTime?, EndDate As DateTime,
                                                 IncludeUnknownDates As Boolean, Live As Boolean, Delayed As Boolean, Premier As Boolean,
                                                 HighlightsInd As Boolean, SAInd As Boolean,
                                                 ImportedEventID As Integer?, GenRefNumber As Int64?,
                                                 ViewBy As String,
                                                 PageNo As Integer, PageSize As Integer, SortAsc As Boolean, SortColumn As String) As ROSynergyEventList

      Return DataPortal.Fetch(Of ROSynergyEventList)(New Criteria(Keyword, StartDate, EndDate,
                                                                  IncludeUnknownDates, Live, Delayed, Premier,
                                                                  HighlightsInd, SAInd,
                                                                  ImportedEventID, GenRefNumber,
                                                                  ViewBy,
                                                                  PageNo, PageSize, SortAsc, SortColumn))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      mLastImportDateTime = sdr.GetValue(1)
      mTotalPages = sdr.GetInt32(2)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSynergyEvent.GetROSynergyEvent(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSynergyEventList"
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@Keyword", Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@IncludeUnknownDates", crit.IncludeUnknownDates)
            'cm.Parameters.AddWithValue("@CurrentSystemID", NothingDBNull(crit.CurrentSystemID))
            'cm.Parameters.AddWithValue("@CurrentProductionAreaID", NothingDBNull(crit.CurrentProductionAreaID))
            cm.Parameters.AddWithValue("@Live", NothingDBNull(crit.Live))
            cm.Parameters.AddWithValue("@Delayed", NothingDBNull(crit.Delayed))
            cm.Parameters.AddWithValue("@Premier", NothingDBNull(crit.Premier))
            cm.Parameters.AddWithValue("@HighlightsInd", NothingDBNull(crit.HighlightsInd))
            cm.Parameters.AddWithValue("@SAInd", crit.SAInd)
            'cm.Parameters.AddWithValue("@OBInd", crit.OBInd)
            'cm.Parameters.AddWithValue("@StudioInd", crit.StudioInd)
            'cm.Parameters.AddWithValue("@SatOpsInd", crit.SatOpsInd)
            cm.Parameters.AddWithValue("@ImportedEventID", NothingDBNull(crit.ImportedEventID))
            cm.Parameters.AddWithValue("@GenRefNumber", NothingDBNull(crit.GenRefNumber))
            cm.Parameters.AddWithValue("@ViewBy", crit.ViewBy)
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace