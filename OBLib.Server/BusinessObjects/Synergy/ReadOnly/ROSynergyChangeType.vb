﻿' Generated 31 Aug 2016 17:14 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy.ReadOnly

  <Serializable()> _
  Public Class ROSynergyChangeType
    Inherits OBReadOnlyBase(Of ROSynergyChangeType)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSynergyChangeTypeBO.ROSynergyChangeTypeBOToString(self)")

    Public Shared SynergyChangeTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SynergyChangeTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SynergyChangeTypeID() As Integer
      Get
        Return GetProperty(SynergyChangeTypeIDProperty)
      End Get
    End Property

    Public Shared SynergyChangeTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SynergyChangeType, "Synergy Change Type", "")
    ''' <summary>
    ''' Gets the Synergy Change Type value
    ''' </summary>
    <Display(Name:="Synergy Change Type", Description:="")>
  Public ReadOnly Property SynergyChangeType() As String
      Get
        Return GetProperty(SynergyChangeTypeProperty)
      End Get
    End Property

    Public Shared ChangeTypeIconNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangeTypeIconName, "Change Type Icon Name", "")
    ''' <summary>
    ''' Gets the Change Type Icon Name value
    ''' </summary>
    <Display(Name:="Change Type Icon Name", Description:="")>
  Public ReadOnly Property ChangeTypeIconName() As String
      Get
        Return GetProperty(ChangeTypeIconNameProperty)
      End Get
    End Property

    Public Shared ChangeTypeCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangeTypeCss, "Change Type Css", "")
    ''' <summary>
    ''' Gets the Change Type Css value
    ''' </summary>
    <Display(Name:="Change Type Css", Description:="")>
  Public ReadOnly Property ChangeTypeCss() As String
      Get
        Return GetProperty(ChangeTypeCssProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SynergyChangeTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SynergyChangeType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSynergyChangeType(dr As SafeDataReader) As ROSynergyChangeType

      Dim r As New ROSynergyChangeType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SynergyChangeTypeIDProperty, .GetInt32(0))
        LoadProperty(SynergyChangeTypeProperty, .GetString(1))
        LoadProperty(ChangeTypeIconNameProperty, .GetString(2))
        LoadProperty(ChangeTypeCssProperty, .GetString(3))
      End With

    End Sub

#End Region

  End Class

End Namespace