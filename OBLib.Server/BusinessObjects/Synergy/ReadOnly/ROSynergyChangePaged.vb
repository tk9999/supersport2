﻿' Generated 31 Aug 2016 14:00 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy.ReadOnly

  <Serializable()> _
  Public Class ROSynergyChangePaged
    Inherits OBReadOnlyBase(Of ROSynergyChangePaged)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSynergyChangePagedBO.ROSynergyChangePagedBOToString(self)")

    Public Shared SynergyChangeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SynergyChangeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property SynergyChangeID() As Integer
      Get
        Return GetProperty(SynergyChangeIDProperty)
      End Get
    End Property

    Public Shared SynergyImportIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SynergyImportID, "Synergy Import", 0)
    ''' <summary>
    ''' Gets the Synergy Import value
    ''' </summary>
    <Display(Name:="Synergy Import", Description:="")>
  Public ReadOnly Property SynergyImportID() As Integer
      Get
        Return GetProperty(SynergyImportIDProperty)
      End Get
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.GenRefNumber, "Gen Ref Number", 0)
    ''' <summary>
    ''' Gets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref Number", Description:="")>
  Public ReadOnly Property GenRefNumber() As Integer
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
    End Property

    Public Shared ScheduleNumberProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ScheduleNumber, "Schedule Number", Nothing)
    ''' <summary>
    ''' Gets the Schedule Number value
    ''' </summary>
    <Display(Name:="Schedule Number", Description:="")>
    Public ReadOnly Property ScheduleNumber() As Integer?
      Get
        Return GetProperty(ScheduleNumberProperty)
      End Get
    End Property

    Public Shared ChangeDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.ChangeDateTime, "Change Date Time")
    ''' <summary>
    ''' Gets the Change Date Time value
    ''' </summary>
    <Display(Name:="Change Date Time", Description:="")>
  Public ReadOnly Property ChangeDateTime As Date
      Get
        If Not FieldManager.FieldExists(ChangeDateTimeProperty) Then
          LoadProperty(ChangeDateTimeProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(ChangeDateTimeProperty)
      End Get
    End Property

    Public Shared SynergyChangeTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SynergyChangeTypeID, "Synergy Change Type", Nothing)
    ''' <summary>
    ''' Gets the Synergy Change Type value
    ''' </summary>
    <Display(Name:="Synergy Change Type", Description:="")>
  Public ReadOnly Property SynergyChangeTypeID() As Integer?
      Get
        Return GetProperty(SynergyChangeTypeIDProperty)
      End Get
    End Property

    Public Shared ChangeDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangeDescription, "Change Description", "")
    ''' <summary>
    ''' Gets the Change Description value
    ''' </summary>
    <Display(Name:="Change Description", Description:="")>
  Public ReadOnly Property ChangeDescription() As String
      Get
        Return GetProperty(ChangeDescriptionProperty)
      End Get
    End Property

    Public Shared ColumnNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ColumnName, "Column Name", "")
    ''' <summary>
    ''' Gets the Column Name value
    ''' </summary>
    <Display(Name:="Column Name", Description:="")>
  Public ReadOnly Property ColumnName() As String
      Get
        Return GetProperty(ColumnNameProperty)
      End Get
    End Property

    Public Shared PreviousValueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreviousValue, "Previous Value", "")
    ''' <summary>
    ''' Gets the Previous Value value
    ''' </summary>
    <Display(Name:="Previous Value", Description:="")>
  Public ReadOnly Property PreviousValue() As String
      Get
        Return GetProperty(PreviousValueProperty)
      End Get
    End Property

    Public Shared NewValueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.NewValue, "New Value", "")
    ''' <summary>
    ''' Gets the New Value value
    ''' </summary>
    <Display(Name:="New Value", Description:="")>
  Public ReadOnly Property NewValue() As String
      Get
        Return GetProperty(NewValueProperty)
      End Get
    End Property

    Public Shared ChangeDateTimeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangeDateTimeString, "Change Detected Time", "")
    ''' <summary>
    ''' Gets the New Value value
    ''' </summary>
    <Display(Name:="Change Detected Time", Description:="")>
    Public ReadOnly Property ChangeDateTimeString() As String
      Get
        Return GetProperty(ChangeDateTimeStringProperty)
      End Get
    End Property

    Public Shared SynergyChangeTypeStringProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangeTypeString, "Change Type", "")
    ''' <summary>
    ''' Gets the New Value value
    ''' </summary>
    <Display(Name:="Change Type", Description:="")>
    Public ReadOnly Property ChangeTypeString() As String
      Get
        Return GetProperty(SynergyChangeTypeStringProperty)
      End Get
    End Property

    Public Shared ChangeTypeIconNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangeTypeIconName, "Icon Name", "")
    ''' <summary>
    ''' Gets the New Value value
    ''' </summary>
    <Display(Name:="Icon Name", Description:="")>
    Public ReadOnly Property ChangeTypeIconName() As String
      Get
        Return GetProperty(ChangeTypeIconNameProperty)
      End Get
    End Property

    Public Shared ChangeTypeCssProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChangeTypeCss, "ChangeTypeCss", "")
    ''' <summary>
    ''' Gets the New Value value
    ''' </summary>
    <Display(Name:="ChangeTypeCss", Description:="")>
    Public ReadOnly Property ChangeTypeCss() As String
      Get
        Return GetProperty(ChangeTypeCssProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SynergyChangeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ChangeDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSynergyChangePaged(dr As SafeDataReader) As ROSynergyChangePaged

      Dim r As New ROSynergyChangePaged()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SynergyChangeIDProperty, .GetInt32(0))
        LoadProperty(SynergyImportIDProperty, .GetInt32(1))
        LoadProperty(GenRefNumberProperty, .GetInt32(2))
        LoadProperty(ScheduleNumberProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(ChangeDateTimeProperty, .GetValue(4))
        LoadProperty(SynergyChangeTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(ChangeDescriptionProperty, .GetString(6))
        LoadProperty(ColumnNameProperty, .GetString(7))
        LoadProperty(PreviousValueProperty, .GetString(8))
        LoadProperty(NewValueProperty, .GetString(9))
        LoadProperty(ChangeDateTimeStringProperty, .GetString(10))
        LoadProperty(SynergyChangeTypeStringProperty, .GetString(11))
        LoadProperty(ChangeTypeIconNameProperty, .GetString(12))
        LoadProperty(ChangeTypeCssProperty, .GetString(13))
      End With

    End Sub

#End Region

  End Class

End Namespace