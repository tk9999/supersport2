﻿' Generated 21 Apr 2017 14:44 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy.ReadOnly

  <Serializable()> _
  Public Class ROSynergyEventSearchList
    Inherits OBReadOnlyListBase(Of ROSynergyEventSearchList, ROSynergyEventSearch)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(GenRefNumber As Int64) As ROSynergyEventSearch

      For Each child As ROSynergyEventSearch In Me
        If child.GenRefNumber = GenRefNumber Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate)
      ''' <summary>
      ''' Gets and sets the Start Date value
      ''' </summary>
      <Display(Name:="Start Date", Description:=""),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property StartDate As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate)
      ''' <summary>
      ''' Gets and sets the End Date value
      ''' </summary>
      <Display(Name:="End Date", Description:=""),
      DateField(FormatString:="dd MMM yyyy")>
      Public Property EndDate As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Keyword, "")
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="Keyword", Description:="")>
      Public Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Shared GenRefNumberProperty As PropertyInfo(Of Int64?) = RegisterSProperty(Of Int64?)(Function(c) c.GenRefNumber, Nothing)
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="GenRef", Description:="")>
      Public Property GenRefNumber() As Int64?
        Get
          Return ReadProperty(GenRefNumberProperty)
        End Get
        Set(ByVal Value As Int64?)
          LoadProperty(GenRefNumberProperty, Value)
        End Set
      End Property

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROSynergyEventSearchList() As ROSynergyEventSearchList

      Return New ROSynergyEventSearchList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSynergyEventSearchList() As ROSynergyEventSearchList

      Return DataPortal.Fetch(Of ROSynergyEventSearchList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSynergyEventSearch.GetROSynergyEventSearch(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSynergyEventListSearch"
            cm.Parameters.AddWithValue("@UserID", OBLib.Security.Settings.CurrentUserID)
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@Keyword", Strings.MakeEmptyDBNull(crit.Keyword))
            cm.Parameters.AddWithValue("@GenRefNumber", NothingDBNull(crit.GenRefNumber))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace