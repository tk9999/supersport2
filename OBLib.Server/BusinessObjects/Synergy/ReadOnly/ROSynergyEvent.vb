﻿' Generated 12 May 2015 15:25 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Web

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Synergy.ReadOnly

  <Serializable()> _
  Public Class ROSynergyEvent
    Inherits SingularReadOnlyBase(Of ROSynergyEvent)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared IDValueProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IDValue, "Channel")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Key>
    Public ReadOnly Property IDValue() As Integer?
      Get
        Return GetProperty(IDValueProperty)
      End Get
    End Property

    Public Shared ImportedEventIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    Public ReadOnly Property ImportedEventID() As Integer
      Get
        Return GetProperty(ImportedEventIDProperty)
      End Get
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.GenRefNumber, "Gen Ref")
    ''' <summary>
    ''' Gets the Gen Ref Number value
    ''' </summary>
    <Display(Name:="Gen Ref", Description:="")>
    Public ReadOnly Property GenRefNumber() As Int64
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
    End Property

    Public Shared GenreSeriesProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.GenreSeries, "Genre (Series)")
    ''' <summary>
    ''' Gets the Genre Series value
    ''' </summary>
    <Display(Name:="Genre (Series)", Description:="")>
    Public ReadOnly Property GenreSeries() As String
      Get
        Return GetProperty(GenreSeriesProperty)
      End Get
    End Property

    Public Shared TitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Title, "Title")
    ''' <summary>
    ''' Gets the Title value
    ''' </summary>
    <Display(Name:="Title", Description:="")>
    Public ReadOnly Property Title() As String
      Get
        Return GetProperty(TitleProperty)
      End Get
    End Property

    Public Shared TeamsPlayingProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TeamsPlaying, "Teams")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Teams", Description:="")>
    Public ReadOnly Property TeamsPlaying() As String
      Get
        Return GetProperty(TeamsPlayingProperty)
      End Get
    End Property

    Public Shared LiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LiveDate, "Live Start")
    ''' <summary>
    ''' Gets the Live Date value
    ''' </summary>
    <Display(Name:="Live Start", Description:=""),
     DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property LiveDate As DateTime?
      Get
        Return GetProperty(LiveDateProperty)
      End Get
    End Property

    Public Shared LiveEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LiveEndDateTime, "Live End")
    ''' <summary>
    ''' Gets the Live End Date Time value
    ''' </summary>
    <Display(Name:="Live End", Description:=""),
     DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property LiveEndDateTime As DateTime?
      Get
        Return GetProperty(LiveEndDateTimeProperty)
      End Get
    End Property

    <Display(Name:="Live Start", Description:="")>
    Public ReadOnly Property LiveDateString As String
      Get
        If LiveDate IsNot Nothing Then
          Return LiveDate.Value.ToString("dd MMM")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Start Time", Description:="")>
    Public ReadOnly Property LiveStartTimeString As String
      Get
        If LiveDate IsNot Nothing Then
          Return LiveDate.Value.ToString("HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Live End", Description:="")>
    Public ReadOnly Property LiveEndDateTimeString As String
      Get
        If LiveEndDateTime IsNot Nothing Then
          Return LiveEndDateTime.Value.ToString("HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared CountryProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Country, "Country")
    ''' <summary>
    ''' Gets the Country value
    ''' </summary>
    <Display(Name:="Country", Description:="")>
    Public ReadOnly Property Country() As String
      Get
        Return GetProperty(CountryProperty)
      End Get
    End Property

    Public Shared LocationProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Location, "Location")
    ''' <summary>
    ''' Gets the Location value
    ''' </summary>
    <Display(Name:="Location", Description:="")>
    Public ReadOnly Property Location() As String
      Get
        Return GetProperty(LocationProperty)
      End Get
    End Property

    Public Shared VenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Venue, "Venue")
    ''' <summary>
    ''' Gets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public ReadOnly Property Venue() As String
      Get
        Return GetProperty(VenueProperty)
      End Get
    End Property

    Public Shared ImportedEventChannelIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ImportedEventChannelID, "Imported Event Channel")
    ''' <summary>
    ''' Gets the Imported Event Channel value
    ''' </summary>
    <Display(Name:="Imported Event Channel", Description:="")>
    Public ReadOnly Property ImportedEventChannelID() As Integer
      Get
        Return GetProperty(ImportedEventChannelIDProperty)
      End Get
    End Property

    Public Shared ChannelShortNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelShortName, "Channel")
    ''' <summary>
    ''' Gets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public ReadOnly Property ChannelShortName() As String
      Get
        Return GetProperty(ChannelShortNameProperty)
      End Get
    End Property

    Public Shared EventStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventStatus, "Status")
    ''' <summary>
    ''' Gets the Event Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public ReadOnly Property EventStatus() As String
      Get
        Return GetProperty(EventStatusProperty)
      End Get
    End Property

    Public Shared ScheduleDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleDateTime, "Channel Start")
    ''' <summary>
    ''' Gets the Schedule Date Time value
    ''' </summary>
    <Display(Name:="Channel Start", Description:=""),
    DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property ScheduleDateTime As DateTime?
      Get
        Return GetProperty(ScheduleDateTimeProperty)
      End Get
    End Property

    Public Shared ScheduleEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ScheduleEndDate, "Channel End")
    ''' <summary>
    ''' Gets the Schedule End Date value
    ''' </summary>
    <Display(Name:="Channel End", Description:=""),
    DateField(FormatString:="dd MMM yy HH:mm")>
    Public ReadOnly Property ScheduleEndDate As DateTime?
      Get
        Return GetProperty(ScheduleEndDateProperty)
      End Get
    End Property

    Public Shared IsSelectedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsSelectedBy, "Is Selected By")
    ''' <summary>
    ''' Gets the Is Selected By value
    ''' </summary>
    <Display(Name:="Is Selected By", Description:="")>
    Public ReadOnly Property IsSelectedBy() As String
      Get
        Return GetProperty(IsSelectedByProperty)
      End Get
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Int64) = RegisterProperty(Of Int64)(Function(c) c.RowNo, "Row No")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Row No", Description:="")>
    Public ReadOnly Property RowNo() As Int64
      Get
        Return GetProperty(RowNoProperty)
      End Get
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ChannelID, "Channel")
    ''' <summary>
    ''' Gets the Row No value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public ReadOnly Property ChannelID() As Integer?
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
    End Property

    Public Shared PrimaryChannelIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.PrimaryChannelInd, "Primary Channel", False)
    ''' <summary>
    ''' Gets the Event Status value
    ''' </summary>
    <Display(Name:=" Primary Channel", Description:="")>
    Public ReadOnly Property PrimaryChannelInd() As Boolean
      Get
        Return GetProperty(PrimaryChannelIndProperty)
      End Get
    End Property

    Public Shared HighlightsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HighlightsInd, "Highlights", False)
    ''' <summary>
    ''' Gets the Event Status value
    ''' </summary>
    <Display(Name:="Highlights", Description:="")>
    Public ReadOnly Property HighlightsInd() As Boolean
      Get
        Return GetProperty(HighlightsIndProperty)
      End Get
    End Property

    Public Shared SAIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SAInd, "SA", False)
    ''' <summary>
    ''' Gets the Event Status value
    ''' </summary>
    <Display(Name:="SA", Description:="")>
    Public ReadOnly Property SAInd() As Boolean
      Get
        Return GetProperty(SAIndProperty)
      End Get
    End Property

    Public Shared MyAreaAddedIndProperty As PropertyInfo(Of Boolean?) = RegisterProperty(Of Boolean?)(Function(c) c.MyAreaAddedInd, "MyAreaAddedInd", False)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="MyAreaAddedInd", Description:="")>
    Public ReadOnly Property MyAreaAddedInd() As Boolean?
      Get
        Return GetProperty(MyAreaAddedIndProperty)
      End Get
    End Property

    Public Shared AreaCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AreaCount, "Areas", 0)
    ''' <summary>
    ''' Gets the Production Area Status value
    ''' </summary>
    <Display(Name:="Areas", Description:="")>
    Public ReadOnly Property AreaCount() As Integer
      Get
        Return GetProperty(AreaCountProperty)
      End Get
    End Property

    <Display(Name:="Date")>
    Public ReadOnly Property ScheduleDateString As String
      Get
        If ScheduleDateTime IsNot Nothing Then
          Return ScheduleDateTime.Value.ToString("dd MMM")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="Start", Description:="")>
    Public ReadOnly Property ScheduleStartString As String
      Get
        If ScheduleDateTime IsNot Nothing Then
          Return ScheduleDateTime.Value.ToString("HH:mm")
        End If
        Return ""
      End Get
    End Property

    <Display(Name:="End", Description:="")>
    Public ReadOnly Property ScheduleEndString As String
      Get
        If ScheduleEndDate IsNot Nothing Then
          Return ScheduleEndDate.Value.ToString("HH:mm")
        End If
        Return ""
      End Get
    End Property

    Public Shared ChannelNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ChannelName, "Channel")
    ''' <summary>
    ''' Gets the Channel Short Name value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public ReadOnly Property ChannelName() As String
      Get
        Return GetProperty(ChannelNameProperty)
      End Get
    End Property

    '<Display(Name:="Time", Description:="")>
    'Public ReadOnly Property ScheduledTime As String
    '  Get
    '    Return ScheduleDateString & " " & ScheduleStartString & " - " & ScheduleEndString
    '  End Get
    'End Property

    '<Display(Name:="Time", Description:="")>
    'Public ReadOnly Property ScheduledTimeHTML As String
    '  Get
    '    If ScheduleEndDate.Value.Date > ScheduleDateTime.Value.Date Then
    '      HttpUtility.HtmlEncode("<label>" & ScheduleDateString & " " & ScheduleStartString & " </label> - " & "<label>" & ScheduleEndString & "</label>")
    '    Else
    '      Return HttpUtility.HtmlEncode("<label>" & ScheduleDateString & " " & ScheduleStartString & " </label> - " & "<label class='red'>" & ScheduleEndString & "</label>")
    '    End If
    '    Return ""
    '  End Get
    'End Property

    Public Shared DragTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DragType, "Drag Type")
    ''' <summary>
    ''' Gets and sets the Genre Series value
    ''' </summary>
    <Display(Name:="Drag Type", Description:="")>
    Public ReadOnly Property DragType() As String
      Get
        Return GetProperty(DragTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ImportedEventIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Title

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSynergyEvent(dr As SafeDataReader) As ROSynergyEvent

      Dim r As New ROSynergyEvent()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ImportedEventIDProperty, .GetInt32(0))
        LoadProperty(GenRefNumberProperty, ZeroNothing(.GetInt64(1)))
        LoadProperty(GenreSeriesProperty, .GetString(2))
        LoadProperty(TitleProperty, .GetString(3))
        LoadProperty(TeamsPlayingProperty, .GetString(4))
        LoadProperty(LiveDateProperty, .GetValue(5))
        LoadProperty(LiveEndDateTimeProperty, .GetValue(6))
        LoadProperty(CountryProperty, .GetString(7))
        LoadProperty(LocationProperty, .GetString(8))
        LoadProperty(VenueProperty, .GetString(9))
        LoadProperty(ImportedEventChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(ChannelShortNameProperty, .GetString(11))
        LoadProperty(EventStatusProperty, .GetString(12))
        LoadProperty(ScheduleDateTimeProperty, .GetValue(13))
        LoadProperty(ScheduleEndDateProperty, .GetValue(14))
        LoadProperty(IsSelectedByProperty, .GetString(15))
        LoadProperty(ChannelIDProperty, ZeroNothing(.GetInt32(16)))
        LoadProperty(PrimaryChannelIndProperty, .GetBoolean(17))
        LoadProperty(HighlightsIndProperty, .GetBoolean(18))
        LoadProperty(SAIndProperty, .GetBoolean(19))
        LoadProperty(RowNoProperty, .GetInt32(20))
        LoadProperty(IDValueProperty, .GetInt32(21))
        LoadProperty(ChannelNameProperty, .GetString(22))
        LoadProperty(MyAreaAddedIndProperty, .GetBoolean(23))
        LoadProperty(AreaCountProperty, .GetInt32(24))
        LoadProperty(DragTypeProperty, .GetString(25))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace