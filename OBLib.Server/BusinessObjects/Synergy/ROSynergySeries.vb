﻿' Generated 30 Sep 2017 18:14 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()>
  Public Class ROSynergySeries
    Inherits OBReadOnlyBase(Of ROSynergySeries)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROSynergySeriesBO.ROSynergySeriesBOToString(self)")

    Public Shared SeriesTitleProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SeriesTitle, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Series", Description:="")>
    Public ReadOnly Property SeriesTitle() As String
      Get
        Return GetProperty(SeriesTitleProperty)
      End Get
    End Property

    Public Shared SeriesNumberProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SeriesNumber, "Series Number")
    ''' <summary>
    ''' Gets the Series Number value
    ''' </summary>
    <Display(Name:="Number", Description:=""), Key>
    Public ReadOnly Property SeriesNumber() As Integer
      Get
        Return GetProperty(SeriesNumberProperty)
      End Get
    End Property

    Public Shared SeasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Season, "Season")
    ''' <summary>
    ''' Gets the Season value
    ''' </summary>
    <Display(Name:="Season", Description:="")>
    Public ReadOnly Property Season() As String
      Get
        Return GetProperty(SeasonProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SeriesTitleProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SeriesTitle

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROSynergySeries(dr As SafeDataReader) As ROSynergySeries

      Dim r As New ROSynergySeries()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SeriesTitleProperty, .GetString(0))
        LoadProperty(SeriesNumberProperty, .GetInt32(1))
        LoadProperty(SeasonProperty, .GetString(2))
      End With

    End Sub

#End Region

  End Class

End Namespace