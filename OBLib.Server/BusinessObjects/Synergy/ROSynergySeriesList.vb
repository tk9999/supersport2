﻿' Generated 30 Sep 2017 18:14 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Synergy

  <Serializable()> _
  Public Class ROSynergySeriesList
    Inherits OBReadOnlyListBase(Of ROSynergySeriesList, ROSynergySeries)

#Region " Business Methods "

    Public Function GetItem(SeriesTitle As String) As ROSynergySeries

      For Each child As ROSynergySeries In Me
        If child.SeriesTitle = SeriesTitle Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <PrimarySearchField>
      Public Property SeriesTitle As String

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROSynergySeriesList() As ROSynergySeriesList

      Return New ROSynergySeriesList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROSynergySeriesList() As ROSynergySeriesList

      Return DataPortal.Fetch(Of ROSynergySeriesList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSynergySeries.GetROSynergySeries(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSynergySeriesList"
            cm.Parameters.AddWithValue("@SeriesTitle", Strings.MakeEmptyDBNull(crit.SeriesTitle))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace