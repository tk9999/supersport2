﻿' Generated 08 Nov 2014 18:16 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Company.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.HR.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly.Old

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting

  <Serializable()> _
  Public Class CompanyRateCard
    Inherits SingularBusinessBase(Of CompanyRateCard)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CompanyRateCardIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CompanyRateCardID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CompanyRateCardID() As Integer
      Get
        Return GetProperty(CompanyRateCardIDProperty)
      End Get
    End Property

    Public Shared CompanyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CompanyID, "Company", Nothing)
    ''' <summary>
    ''' Gets and sets the Company value
    ''' </summary>
    <Display(Name:="Company", Description:=""),
    Required(ErrorMessage:="Company required"),
    DropDownWeb(GetType(ROCompanyList))>
    Public Property CompanyID() As Integer?
      Get
        Return GetProperty(CompanyIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CompanyIDProperty, Value)
      End Set
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets and sets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:=""),
    Required(ErrorMessage:="Effective Date required")>
    Public Property EffectiveDate As DateTime?
      Get
        If Not FieldManager.FieldExists(EffectiveDateProperty) Then
          LoadProperty(EffectiveDateProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(EffectiveDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EffectiveDateProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area", 0)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:=""),
    DropDownWeb(GetType(ROProductionAreaList))>
    Public Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:=""),
    DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionTypeList), DropDownType:=DropDownWeb.SelectType.FindScreen)>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventTypeID, "Event Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:=""),
    DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROEventTypeList), ThisFilterMember:="ProductionTypeID", DropDownType:=DropDownWeb.SelectType.FindScreen)>
    Public Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EventTypeIDProperty, Value)
      End Set
    End Property

    Public Shared RateTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RateTypeID, "Rate Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Rate Type value
    ''' </summary>
    <Display(Name:="Rate Type", Description:=""),
    DropDownWeb(GetType(RORateTypeList))>
    Public Property RateTypeID() As Integer?
      Get
        Return GetProperty(RateTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RateTypeIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:=""),
    DropDownWeb(GetType(ROHumanResourceList), DropDownType:=DropDownWeb.SelectType.FindScreen)>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    DropDownWeb(GetType(RODisciplineList), DropDownType:=DropDownWeb.SelectType.FindScreen)>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position", Nothing)
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:=""),
    DropDownWeb(GetType(ROPositionList), ThisFilterMember:="DisciplineID")>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared AdHocDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdHocDescription, "Ad Hoc Description", "")
    ''' <summary>
    ''' Gets and sets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Ad Hoc Description", Description:=""),
    StringLength(200, ErrorMessage:="Ad Hoc Description cannot be more than 200 characters")>
    Public Property AdHocDescription() As String
      Get
        Return GetProperty(AdHocDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AdHocDescriptionProperty, Value)
      End Set
    End Property

    Public Shared RateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Rate, "Rate", 0)
    ''' <summary>
    ''' Gets and sets the Rate value
    ''' </summary>
    <Display(Name:="Rate", Description:=""),
    Singular.DataAnnotations.NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property Rate() As Decimal
      Get
        Return GetProperty(RateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CompanyRateCardIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.AdHocDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Company Rate Card")
        Else
          Return String.Format("Blank {0}", "Company Rate Card")
        End If
      Else
        Return Me.AdHocDescription
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCompanyRateCard() method.

    End Sub

    Public Shared Function NewCompanyRateCard() As CompanyRateCard

      Return DataPortal.CreateChild(Of CompanyRateCard)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetCompanyRateCard(dr As SafeDataReader) As CompanyRateCard

      Dim c As New CompanyRateCard()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CompanyRateCardIDProperty, .GetInt32(0))
          LoadProperty(CompanyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(EffectiveDateProperty, .GetValue(2))
          LoadProperty(ProductionAreaIDProperty, .GetInt32(3))
          LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(RateTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(AdHocDescriptionProperty, .GetString(10))
          LoadProperty(RateProperty, .GetDecimal(11))
          LoadProperty(CreatedByProperty, .GetInt32(12))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(13))
          LoadProperty(ModifiedByProperty, .GetInt32(14))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(15))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insCompanyRateCard"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updCompanyRateCard"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramCompanyRateCardID As SqlParameter = .Parameters.Add("@CompanyRateCardID", SqlDbType.Int)
          paramCompanyRateCardID.Value = GetProperty(CompanyRateCardIDProperty)
          If Me.IsNew Then
            paramCompanyRateCardID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@CompanyID", GetProperty(CompanyIDProperty))
          cm.Parameters.AddWithValue("@EffectiveDate", (New SmartDate(GetProperty(EffectiveDateProperty))).DBValue)
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@ProductionTypeID", Singular.Misc.NothingDBNull(GetProperty(ProductionTypeIDProperty)))
          .Parameters.AddWithValue("@EventTypeID", Singular.Misc.NothingDBNull(GetProperty(EventTypeIDProperty)))
          .Parameters.AddWithValue("@RateTypeID", Singular.Misc.NothingDBNull(GetProperty(RateTypeIDProperty)))
          .Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@DisciplineID", Singular.Misc.NothingDBNull(GetProperty(DisciplineIDProperty)))
          .Parameters.AddWithValue("@PositionID", Singular.Misc.NothingDBNull(GetProperty(PositionIDProperty)))
          .Parameters.AddWithValue("@AdHocDescription", GetProperty(AdHocDescriptionProperty))
          .Parameters.AddWithValue("@Rate", GetProperty(RateProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(CompanyRateCardIDProperty, paramCompanyRateCardID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delCompanyRateCard"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@CompanyRateCardID", GetProperty(CompanyRateCardIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace