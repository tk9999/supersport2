﻿function QuantitySet(self) {
  Quotes.QuantitySet(self);
}

function FlightsSet(self) {
  Quotes.FlightsSet(self);
}

function AccommodationSet(self) {
  Quotes.AccommodationSet(self);
}

function CarHireSet(self) {
  Quotes.CarHireSet(self);
}

function SnTSet(self) {
  Quotes.SnTSet(self);
}

function FeeSet(self) {
  Quotes.FeeSet(self);
}

function CompanyRateCardIDSet(self) {
  Quotes.CompanyRateCardIDSet(self);
}

function SupplierRateCardIDSet(self) {
  Quotes.SupplierRateCardIDSet(self);
}

function RateSet(self) {
  Quotes.RateSet(self);
}

function SupplierIDSet(self) {
  Quotes.SupplierIDSet(self);
}

function QuoteHeadingDetailToString(self) {
  return Quotes.QuoteHeadingDetailToString(self);
}