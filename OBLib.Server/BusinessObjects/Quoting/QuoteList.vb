﻿' Generated 10 Nov 2014 16:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting

  <Serializable()> _
  Public Class QuoteList
    Inherits SingularBusinessListBase(Of QuoteList, Quote)

#Region " Business Methods "

    Public Function GetItem(QuoteID As Integer) As Quote

      For Each child As Quote In Me
        If child.QuoteID = QuoteID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Quotes"

    End Function

    Public Function GetQuoteHeading(QuoteHeadingID As Integer) As QuoteHeading

      Dim obj As QuoteHeading = Nothing
      For Each parent As Quote In Me
        obj = parent.QuoteHeadingList.GetItem(QuoteHeadingID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property QuoteID As Integer? = Nothing

      Public Sub New(QuoteID As Integer?)
        Me.QuoteID = QuoteID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewQuoteList() As QuoteList

      Return New QuoteList()

    End Function

    Public Shared Sub BeginGetQuoteList(CallBack As EventHandler(Of DataPortalResult(Of QuoteList)))

      Dim dp As New DataPortal(Of QuoteList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetQuoteList() As QuoteList

      Return DataPortal.Fetch(Of QuoteList)(New Criteria())

    End Function

    Public Shared Function GetQuoteList(QuoteID As Integer?) As QuoteList

      Return DataPortal.Fetch(Of QuoteList)(New Criteria(QuoteID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(Quote.GetQuote(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As Quote = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.QuoteID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          If parent IsNot Nothing Then
            parent.QuoteHeadingList.RaiseListChangedEvents = False
            parent.QuoteHeadingList.Add(QuoteHeading.GetQuoteHeading(sdr))
            parent.QuoteHeadingList.RaiseListChangedEvents = True
          End If
        End While
      End If

      Dim parentChild As QuoteHeading = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentChild Is Nothing OrElse parentChild.QuoteHeadingID <> sdr.GetInt32(1) Then
            parentChild = Me.GetQuoteHeading(sdr.GetInt32(1))
          End If
          parentChild.QuoteHeadingDetailList.RaiseListChangedEvents = False
          parentChild.QuoteHeadingDetailList.Add(QuoteHeadingDetail.GetQuoteHeadingDetail(sdr))
          parentChild.QuoteHeadingDetailList.RaiseListChangedEvents = True
        End While
      End If

      For Each child As Quote In Me
        child.CheckRules()
        For Each QuoteHeading As QuoteHeading In child.QuoteHeadingList
          QuoteHeading.CheckRules()

          For Each QuoteHeadingDetail As QuoteHeadingDetail In QuoteHeading.QuoteHeadingDetailList
            QuoteHeadingDetail.CheckRules()
          Next
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getQuoteList"
            cm.Parameters.AddWithValue("@QuoteID", NothingDBNull(crit.QuoteID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As Quote In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As Quote In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace