﻿function QuoteOverrideByValid(Value, Rule, CtlError) {
  Quotes.QuoteHeadingAmountOverrideByValid(Value, Rule, CtlError);
}

function QuoteOverrideDateTimeValid(Value, Rule, CtlError) {
  Quotes.QuoteHeadingAmountOverrideDateTimeValid(Value, Rule, CtlError);
}

function QuoteOverrideReasonValid(Value, Rule, CtlError) {
  Quotes.QuoteHeadingAmountOverrideReasonValid(Value, Rule, CtlError);
}

function QuoteHeadingOverrideAmountSet(self) {
  Quotes.QuoteHeadingOverrideAmountSet(self);
}

function QuoteHeadingToString(self) {
  return Quotes.QuoteHeadingToString(self);
}

function QuoteHeadingAmountSet(self) {
  Quotes.QuoteHeadingAmountSet(self);
}

