﻿' Generated 08 Nov 2014 18:16 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Travel.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting

  <Serializable()> _
  Public Class TravelRate
    Inherits SingularBusinessBase(Of TravelRate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TravelRateIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRateID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property TravelRateID() As Integer
      Get
        Return GetProperty(TravelRateIDProperty)
      End Get
    End Property

    Public Shared TravelTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelTypeID, "Travel Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Travel Type value
    ''' </summary>
    <Display(Name:="Travel Type", Description:=""),
    Required(ErrorMessage:="Travel Type required"),
    DropDownWeb(GetType(ROTravelTypeList))>
    Public Property TravelTypeID() As Integer?
      Get
        Return GetProperty(TravelTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TravelTypeIDProperty, Value)
      End Set
    End Property

    Public Shared DefaultDailyRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.DefaultDailyRate, "Default Daily Rate", 0)
    ''' <summary>
    ''' Gets and sets the Default Daily Rate value
    ''' </summary>
    <Display(Name:="Default Daily Rate", Description:=""),
    Required(ErrorMessage:="Default Daily Rate required")>
    Public Property DefaultDailyRate() As Decimal
      Get
        Return GetProperty(DefaultDailyRateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(DefaultDailyRateProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TravelRateIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Travel Rate")
        Else
          Return String.Format("Blank {0}", "Travel Rate")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewTravelRate() method.

    End Sub

    Public Shared Function NewTravelRate() As TravelRate

      Return DataPortal.CreateChild(Of TravelRate)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetTravelRate(dr As SafeDataReader) As TravelRate

      Dim t As New TravelRate()
      t.Fetch(dr)
      Return t

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(TravelRateIDProperty, .GetInt32(0))
          LoadProperty(TravelTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(DefaultDailyRateProperty, .GetDecimal(2))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insTravelRate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updTravelRate"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramTravelRateID As SqlParameter = .Parameters.Add("@TravelRateID", SqlDbType.Int)
          paramTravelRateID.Value = GetProperty(TravelRateIDProperty)
          If Me.IsNew Then
            paramTravelRateID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@TravelTypeID", GetProperty(TravelTypeIDProperty))
          .Parameters.AddWithValue("@DefaultDailyRate", GetProperty(DefaultDailyRateProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(TravelRateIDProperty, paramTravelRateID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delTravelRate"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@TravelRateID", GetProperty(TravelRateIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace