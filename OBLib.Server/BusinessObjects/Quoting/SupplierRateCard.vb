﻿' Generated 08 Nov 2014 18:16 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly.Old

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting

  <Serializable()> _
  Public Class SupplierRateCard
    Inherits SingularBusinessBase(Of SupplierRateCard)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SupplierRateCardIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierRateCardID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SupplierRateCardID() As Integer
      Get
        Return GetProperty(SupplierRateCardIDProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets and sets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:=""),
    Required(ErrorMessage:="Supplier required"),
    DropDownWeb(GetType(ROSupplierList))>
    Public Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeID, "Production Type", 0)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:=""),
    DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionTypeList), DropDownType:=DropDownWeb.SelectType.FindScreen)>
    Public Property ProductionTypeID() As Integer
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventTypeID, "Event Type", 0)
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:=""),
    DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.Old.ROEventTypeList), ThisFilterMember:="ProductionTypeID", DropDownType:=DropDownWeb.SelectType.FindScreen)>
    Public Property EventTypeID() As Integer
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EventTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area", 0)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:=""),
    DropDownWeb(GetType(ROProductionAreaList))>
    Public Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared CostTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CostTypeID, "Cost Type", 0)
    ''' <summary>
    ''' Gets and sets the Cost Type value
    ''' </summary>
    <Display(Name:="Cost Type", Description:=""),
    DropDownWeb(GetType(ROCostTypeList))>
    Public Property CostTypeID() As Integer
      Get
        Return GetProperty(CostTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(CostTypeIDProperty, Value)
      End Set
    End Property

    Public Shared QuantityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Quantity, "Quantity", 0)
    ''' <summary>
    ''' Gets and sets the Quantity value
    ''' </summary>
    <Display(Name:="Quantity", Description:=""),
    Required(ErrorMessage:="Quantity required")>
    Public Property Quantity() As Integer
      Get
        Return GetProperty(QuantityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(QuantityProperty, Value)
      End Set
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Amount, "Amount", 0)
    ''' <summary>
    ''' Gets and sets the Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:=""),
    Required(ErrorMessage:="Amount required")>
    Public Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SupplierRateCardIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.SupplierRateCardID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Supplier Rate Card")
        Else
          Return String.Format("Blank {0}", "Supplier Rate Card")
        End If
      Else
        Return Me.SupplierRateCardID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewSupplierRateCard() method.

    End Sub

    Public Shared Function NewSupplierRateCard() As SupplierRateCard

      Return DataPortal.CreateChild(Of SupplierRateCard)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetSupplierRateCard(dr As SafeDataReader) As SupplierRateCard

      Dim s As New SupplierRateCard()
      s.Fetch(dr)
      Return s

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(SupplierRateCardIDProperty, .GetInt32(0))
          LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionTypeIDProperty, .GetInt32(2))
          LoadProperty(EventTypeIDProperty, .GetInt32(3))
          LoadProperty(ProductionAreaIDProperty, .GetInt32(4))
          LoadProperty(CostTypeIDProperty, .GetInt32(5))
          LoadProperty(QuantityProperty, .GetInt32(6))
          LoadProperty(AmountProperty, .GetDecimal(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insSupplierRateCard"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updSupplierRateCard"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramSupplierRateCardID As SqlParameter = .Parameters.Add("@SupplierRateCardID", SqlDbType.Int)
          paramSupplierRateCardID.Value = GetProperty(SupplierRateCardIDProperty)
          If Me.IsNew Then
            paramSupplierRateCardID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@SupplierID", GetProperty(SupplierIDProperty))
          .Parameters.AddWithValue("@ProductionTypeID", GetProperty(ProductionTypeIDProperty))
          .Parameters.AddWithValue("@EventTypeID", GetProperty(EventTypeIDProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@CostTypeID", GetProperty(CostTypeIDProperty))
          .Parameters.AddWithValue("@Quantity", GetProperty(QuantityProperty))
          .Parameters.AddWithValue("@Amount", GetProperty(AmountProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(SupplierRateCardIDProperty, paramSupplierRateCardID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delSupplierRateCard"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@SupplierRateCardID", GetProperty(SupplierRateCardIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace