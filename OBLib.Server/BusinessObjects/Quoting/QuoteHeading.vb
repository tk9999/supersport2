﻿' Generated 10 Nov 2014 16:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Quoting.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting

  <Serializable()> _
  Public Class QuoteHeading
    Inherits SingularBusinessBase(Of QuoteHeading)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared QuoteHeadingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.QuoteHeadingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property QuoteHeadingID() As Integer
      Get
        Return GetProperty(QuoteHeadingIDProperty)
      End Get
    End Property

    Public Shared QuoteIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.QuoteID, "Quote", Nothing)
    ''' <summary>
    ''' Gets the Quote value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property QuoteID() As Integer?
      Get
        Return GetProperty(QuoteIDProperty)
      End Get
    End Property

    Public Shared CostTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CostTypeID, "Cost Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Cost Type value
    ''' </summary>
    <Display(Name:="Cost Type", Description:=""),
    Required(ErrorMessage:="Cost Type required"),
    DropDownWeb(GetType(ROCostTypeList))>
    Public Property CostTypeID() As Integer?
      Get
        Return GetProperty(CostTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CostTypeIDProperty, Value)
      End Set
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.Amount, 0) _
                                                               .AddSetExpression(OBLib.JSCode.QuoteHeadingJS.QuoteHeadingAmountSet, False)
    ''' <summary>
    ''' Gets and sets the Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:=""),
    Required(ErrorMessage:="Amount required"),
    NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountProperty, Value)
      End Set
    End Property

    Public Shared DescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Description, "Description", "")
    ''' <summary>
    ''' Gets and sets the Description value
    ''' </summary>
    <Display(Name:="Description", Description:=""),
    StringLength(500, MinimumLength:=1, ErrorMessage:="Description cannot be more than 500 characters")>
    Public Property Description() As String
      Get
        Return GetProperty(DescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DescriptionProperty, Value)
      End Set
    End Property

    Public Shared AmountOverrideProperty As PropertyInfo(Of Decimal?) = RegisterSProperty(Of Decimal?)(Function(c) c.AmountOverride, Nothing) _
                                                                        .AddSetExpression(OBLib.JSCode.QuoteHeadingJS.QuoteHeadingOverrideAmountSet, False)
    ''' <summary>
    ''' Gets and sets the Amount Override value
    ''' </summary>
    <Display(Name:="Amount Override", Description:="")>
    Public Property AmountOverride() As Decimal?
      Get
        Return GetProperty(AmountOverrideProperty)
      End Get
      Set(ByVal Value As Decimal?)
        SetProperty(AmountOverrideProperty, Value)
      End Set
    End Property

    Public Shared AmountOverrideByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.AmountOverrideBy, "Override By", Nothing)
    ''' <summary>
    ''' Gets and sets the Amount Override By value
    ''' </summary>
    <Display(Name:="Override By", Description:="")>
    Public Property AmountOverrideBy() As Integer?
      Get
        Return GetProperty(AmountOverrideByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(AmountOverrideByProperty, Value)
      End Set
    End Property

    Public Shared AmountOverrideDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.AmountOverrideDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Amount Override Date Time value
    ''' </summary>
    <Display(Name:="Override Date", Description:="")>
    Public Property AmountOverrideDateTime As DateTime?
      Get
        Return GetProperty(AmountOverrideDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(AmountOverrideDateTimeProperty, Value)
      End Set
    End Property

    Public Shared AmountOverrideReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AmountOverrideReason, "Override Reason", "")
    ''' <summary>
    ''' Gets and sets the Amount Override Reason value
    ''' </summary>
    <Display(Name:="Override Reason", Description:=""),
    StringLength(150, ErrorMessage:="Amount Override Reason cannot be more than 150 characters")>
    Public Property AmountOverrideReason() As String
      Get
        Return GetProperty(AmountOverrideReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AmountOverrideReasonProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", OBLib.Security.Settings.CurrentUser.UserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    <Display(Name:="Override By")>
    Public ReadOnly Property AmountOverrideByName As String
      Get
        If AmountOverrideBy Is Nothing Then
          Return ""
        Else
          Return OBLib.CommonData.Lists.ROUserList.GetItem(AmountOverrideBy).LoginName
        End If
      End Get
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, OBLib.JSCode.QuoteHeadingJS.QuoteHeadingToString)

#End Region

#Region " Child Lists "

    Public Shared QuoteHeadingDetailListProperty As PropertyInfo(Of QuoteHeadingDetailList) = RegisterProperty(Of QuoteHeadingDetailList)(Function(c) c.QuoteHeadingDetailList, "Quote Heading Detail List")

    Public ReadOnly Property QuoteHeadingDetailList() As QuoteHeadingDetailList
      Get
        If GetProperty(QuoteHeadingDetailListProperty) Is Nothing Then
          LoadProperty(QuoteHeadingDetailListProperty, Quoting.QuoteHeadingDetailList.NewQuoteHeadingDetailList())
        End If
        Return GetProperty(QuoteHeadingDetailListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As Quote

      Return CType(CType(Me.Parent, QuoteHeadingList).Parent, Quote)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(QuoteHeadingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Description.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Quote Heading")
        Else
          Return String.Format("Blank {0}", "Quote Heading")
        End If
      Else
        Return Me.Description
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"QuoteHeadingDetails"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      With AddWebRule(AmountOverrideByProperty)
        .ServerRuleFunction = AddressOf QuoteOverrideByValid
        .JavascriptRuleCode = OBLib.JSCode.QuoteHeadingJS.QuoteOverrideByValid
        .AddTriggerProperty(AmountOverrideProperty)
      End With

      With AddWebRule(AmountOverrideDateTimeProperty)
        .ServerRuleFunction = AddressOf QuoteOverrideDateTimeValid
        .JavascriptRuleCode = OBLib.JSCode.QuoteHeadingJS.QuoteOverrideDateTimeValid
        .AddTriggerProperty(AmountOverrideProperty)
      End With

      With AddWebRule(AmountOverrideReasonProperty)
        .ServerRuleFunction = AddressOf QuoteOverrideReasonValid
        .JavascriptRuleCode = OBLib.JSCode.QuoteHeadingJS.QuoteOverrideReasonValid
        .AddTriggerProperty(AmountOverrideProperty)
      End With

    End Sub

    Public Function QuoteOverrideByValid(QuoteHeading As QuoteHeading) As String
      If Not Singular.Misc.IsNullNothing(QuoteHeading.AmountOverride, True) AndAlso QuoteHeading.AmountOverrideBy Is Nothing Then
        Return "Override By is required"
      End If
      Return ""
    End Function

    Public Function QuoteOverrideDateTimeValid(QuoteHeading As QuoteHeading) As String
      If Not Singular.Misc.IsNullNothing(QuoteHeading.AmountOverride, True) AndAlso QuoteHeading.AmountOverrideDateTime Is Nothing Then
        Return "Override Date is required"
      End If
      Return ""
    End Function

    Public Function QuoteOverrideReasonValid(QuoteHeading As QuoteHeading) As String
      If Not Singular.Misc.IsNullNothing(QuoteHeading.AmountOverride, True) AndAlso QuoteHeading.AmountOverrideReason.Trim.Length = 0 Then
        Return "Override Reason is required"
      End If
      Return ""
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewQuoteHeading() method.

    End Sub

    Public Shared Function NewQuoteHeading() As QuoteHeading

      Return DataPortal.CreateChild(Of QuoteHeading)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetQuoteHeading(dr As SafeDataReader) As QuoteHeading

      Dim q As New QuoteHeading()
      q.Fetch(dr)
      Return q

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(QuoteHeadingIDProperty, .GetInt32(0))
          LoadProperty(QuoteIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(CostTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(AmountProperty, .GetDecimal(3))
          LoadProperty(DescriptionProperty, .GetString(4))
          LoadProperty(AmountOverrideProperty, .GetDecimal(5))
          LoadProperty(AmountOverrideByProperty, ZeroNothing(.GetInt32(6)))
          LoadProperty(AmountOverrideDateTimeProperty, .GetValue(7))
          LoadProperty(AmountOverrideReasonProperty, .GetString(8))
          LoadProperty(CreatedByProperty, .GetInt32(9))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(10))
          LoadProperty(ModifiedByProperty, .GetInt32(11))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(12))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insQuoteHeading"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updQuoteHeading"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramQuoteHeadingID As SqlParameter = .Parameters.Add("@QuoteHeadingID", SqlDbType.Int)
          paramQuoteHeadingID.Value = GetProperty(QuoteHeadingIDProperty)
          If Me.IsNew Then
            paramQuoteHeadingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@QuoteID", Me.GetParent().QuoteID)
          .Parameters.AddWithValue("@CostTypeID", NothingDBNull(GetProperty(CostTypeIDProperty)))
          .Parameters.AddWithValue("@Amount", GetProperty(AmountProperty))
          .Parameters.AddWithValue("@Description", GetProperty(DescriptionProperty))
          .Parameters.AddWithValue("@AmountOverride", NothingDBNull(GetProperty(AmountOverrideProperty)))
          .Parameters.AddWithValue("@AmountOverrideBy", NothingDBNull(GetProperty(AmountOverrideByProperty)))
          .Parameters.AddWithValue("@AmountOverrideDateTime", (New SmartDate(GetProperty(AmountOverrideDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@AmountOverrideReason", GetProperty(AmountOverrideReasonProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(QuoteHeadingIDProperty, paramQuoteHeadingID.Value)
          End If
          ' update child objects
          If GetProperty(QuoteHeadingDetailListProperty) IsNot Nothing Then
            Me.QuoteHeadingDetailList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(QuoteHeadingDetailListProperty) IsNot Nothing Then
          Me.QuoteHeadingDetailList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delQuoteHeading"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@QuoteHeadingID", GetProperty(QuoteHeadingIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace