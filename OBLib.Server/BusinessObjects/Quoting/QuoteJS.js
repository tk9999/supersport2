﻿function QuoteOverrideByValid(Value, Rule, CtlError) {
  Quotes.QuoteOverrideByValid(Value, Rule, CtlError)
}

function QuoteOverrideDateTimeValid(Value, Rule, CtlError) {
  Quotes.QuoteOverrideDateTimeValid(Value, Rule, CtlError)
}

function QuoteOverrideReasonValid(Value, Rule, CtlError) {
  Quotes.QuoteOverrideReasonValid(Value, Rule, CtlError)
}

function QuoteOverrideAmountSet(self) {
  Quotes.QuoteOverrideAmountSet(self);
}

function QuoteToString(self) {
  return Quotes.QuoteToString(self);
}