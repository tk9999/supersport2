﻿' Generated 08 Nov 2014 18:16 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting

  <Serializable()> _
  Public Class Debtor
    Inherits SingularBusinessBase(Of Debtor)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DebtorID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property DebtorID() As Integer
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
    End Property

    Public Shared DebtorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Debtor, "Debtor", "")
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
        <Display(Name:="Debtor", Description:=""),
        StringLength(150, ErrorMessage:="Debtor cannot be more than 150 characters"),
        Required(ErrorMessage:="A debtor is required")>
        Public Property Debtor() As String
            Get
                Return GetProperty(DebtorProperty)
            End Get
            Set(ByVal Value As String)
                SetProperty(DebtorProperty, Value)
            End Set
        End Property

    Public Shared ContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactName, "Contact Name", "")
    ''' <summary>
    ''' Gets and sets the Contact Name value
    ''' </summary>
        <Display(Name:="Contact Name", Description:=""),
        StringLength(150, ErrorMessage:="Contact Name cannot be more than 150 characters"),
        Required(ErrorMessage:="A contact name is required")>
        Public Property ContactName() As String
            Get
                Return GetProperty(ContactNameProperty)
            End Get
            Set(ByVal Value As String)
                SetProperty(ContactNameProperty, Value)
            End Set
        End Property

    Public Shared ContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactNumber, "Contact Number", "")
    ''' <summary>
    ''' Gets and sets the Contact Number value
    ''' </summary>
    <Display(Name:="Contact Number", Description:=""),
    StringLength(20, ErrorMessage:="Contact Number cannot be more than 20 characters")>
    Public Property ContactNumber() As String
      Get
        Return GetProperty(ContactNumberProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ContactNumberProperty, Value)
      End Set
    End Property

    Public Shared AddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Address, "Address", "")
    ''' <summary>
    ''' Gets and sets the Address value
    ''' </summary>
    <Display(Name:="Address", Description:=""),
    StringLength(500, ErrorMessage:="Address cannot be more than 500 characters")>
    Public Property Address() As String
      Get
        Return GetProperty(AddressProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AddressProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DebtorIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.Debtor.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Debtor")
        Else
          Return String.Format("Blank {0}", "Debtor")
        End If
      Else
        Return Me.Debtor
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewDebtor() method.

    End Sub

    Public Shared Function NewDebtor() As Debtor

      Return DataPortal.CreateChild(Of Debtor)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetDebtor(dr As SafeDataReader) As Debtor

      Dim d As New Debtor()
      d.Fetch(dr)
      Return d

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(DebtorIDProperty, .GetInt32(0))
          LoadProperty(DebtorProperty, .GetString(1))
          LoadProperty(ContactNameProperty, .GetString(2))
          LoadProperty(ContactNumberProperty, .GetString(3))
          LoadProperty(AddressProperty, .GetString(4))
          LoadProperty(CreatedByProperty, .GetInt32(5))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ModifiedByProperty, .GetInt32(7))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insDebtor"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updDebtor"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramDebtorID As SqlParameter = .Parameters.Add("@DebtorID", SqlDbType.Int)
          paramDebtorID.Value = GetProperty(DebtorIDProperty)
          If Me.IsNew Then
            paramDebtorID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@Debtor", GetProperty(DebtorProperty))
          .Parameters.AddWithValue("@ContactName", GetProperty(ContactNameProperty))
          .Parameters.AddWithValue("@ContactNumber", GetProperty(ContactNumberProperty))
          .Parameters.AddWithValue("@Address", GetProperty(AddressProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(DebtorIDProperty, paramDebtorID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delDebtor"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@DebtorID", GetProperty(DebtorIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace