' Singular Systems .js CodeGen Utility version 1.0.2.
' This code is generated by an automated tool. Edit the .js file this was generated from, not this file.
' Generated from C:\Clients\OutsideBroadcast\Development\OutsideBroadcast-Development\OBLib.Server\BusinessObjects\Quoting\QuoteHeadingJS.js at 18 Aug 2015 - 06:53:02.

Namespace JSCode

Public Class QuoteHeadingJS

Public Const QuoteOverrideByValid As String = "Quotes.QuoteHeadingAmountOverrideByValid(Value, Rule, CtlError);"

Public Const QuoteOverrideDateTimeValid As String = "Quotes.QuoteHeadingAmountOverrideDateTimeValid(Value, Rule, CtlError);"

Public Const QuoteOverrideReasonValid As String = "Quotes.QuoteHeadingAmountOverrideReasonValid(Value, Rule, CtlError);"

Public Const QuoteHeadingOverrideAmountSet As String = "Quotes.QuoteHeadingOverrideAmountSet(self);"

Public Const QuoteHeadingToString As String = "return Quotes.QuoteHeadingToString(self);"

Public Const QuoteHeadingAmountSet As String = "Quotes.QuoteHeadingAmountSet(self);"

end class
end namespace
