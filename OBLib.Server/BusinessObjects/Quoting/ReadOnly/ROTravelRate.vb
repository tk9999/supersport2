﻿' Generated 08 Nov 2014 18:16 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting.ReadOnly

  <Serializable()> _
  Public Class ROTravelRate
    Inherits SingularReadOnlyBase(Of ROTravelRate)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared TravelRateIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TravelRateID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property TravelRateID() As Integer
      Get
        Return GetProperty(TravelRateIDProperty)
      End Get
    End Property

    Public Shared TravelTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.TravelTypeID, "Travel Type", Nothing)
    ''' <summary>
    ''' Gets the Travel Type value
    ''' </summary>
    <Display(Name:="Travel Type", Description:="")>
    Public ReadOnly Property TravelTypeID() As Integer?
      Get
        Return GetProperty(TravelTypeIDProperty)
      End Get
    End Property

    Public Shared DefaultDailyRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.DefaultDailyRate, "Default Daily Rate", 0)
    ''' <summary>
    ''' Gets the Default Daily Rate value
    ''' </summary>
    <Display(Name:="Default Daily Rate", Description:="")>
    Public ReadOnly Property DefaultDailyRate() As Decimal
      Get
        Return GetProperty(DefaultDailyRateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(TravelRateIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CreatedDateTime.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROTravelRate(dr As SafeDataReader) As ROTravelRate

      Dim r As New ROTravelRate()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(TravelRateIDProperty, .GetInt32(0))
        LoadProperty(TravelTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(DefaultDailyRateProperty, .GetDecimal(2))
        LoadProperty(CreatedByProperty, .GetInt32(3))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
        LoadProperty(ModifiedByProperty, .GetInt32(5))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace