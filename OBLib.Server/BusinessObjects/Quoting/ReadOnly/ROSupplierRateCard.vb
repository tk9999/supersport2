﻿' Generated 08 Nov 2014 18:17 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting.ReadOnly

  <Serializable()> _
  Public Class ROSupplierRateCard
    Inherits SingularReadOnlyBase(Of ROSupplierRateCard)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SupplierRateCardIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SupplierRateCardID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property SupplierRateCardID() As Integer
      Get
        Return GetProperty(SupplierRateCardIDProperty)
      End Get
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SupplierID, "Supplier", Nothing)
    ''' <summary>
    ''' Gets the Supplier value
    ''' </summary>
    <Display(Name:="Supplier", Description:="")>
    Public ReadOnly Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
    End Property

    Public Shared SupplierProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Supplier, "Supplier", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Supplier", Description:="")>
    Public ReadOnly Property Supplier() As String
      Get
        Return GetProperty(SupplierProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeID, "Production Type", 0)
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionTypeID() As Integer
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "ProductionType", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventTypeID, "Event Type", 0)
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public ReadOnly Property EventTypeID() As Integer
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "EventType", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area", 0)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared AreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Area, "Area", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property Area() As String
      Get
        Return GetProperty(AreaProperty)
      End Get
    End Property

    Public Shared CostTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CostTypeID, "Cost Type", 0)
    ''' <summary>
    ''' Gets the Cost Type value
    ''' </summary>
    <Display(Name:="Cost Type", Description:="")>
    Public ReadOnly Property CostTypeID() As Integer
      Get
        Return GetProperty(CostTypeIDProperty)
      End Get
    End Property

    Public Shared CostTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CostType, "CostType", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Cost Type", Description:="")>
    Public ReadOnly Property CostType() As String
      Get
        Return GetProperty(CostTypeProperty)
      End Get
    End Property

    Public Shared QuantityProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.Quantity, "Quantity", 0)
    ''' <summary>
    ''' Gets the Quantity value
    ''' </summary>
    <Display(Name:="Quantity", Description:="")>
    Public ReadOnly Property Quantity() As Integer
      Get
        Return GetProperty(QuantityProperty)
      End Get
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Amount, "Amount", 0)
    ''' <summary>
    ''' Gets the Amount value
    ''' </summary>
    <Display(Name:="Amount", Description:="")>
    Public ReadOnly Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(SupplierRateCardIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.SupplierRateCardID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROSupplierRateCard(dr As SafeDataReader) As ROSupplierRateCard

      Dim r As New ROSupplierRateCard()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(SupplierRateCardIDProperty, .GetInt32(0))
        LoadProperty(SupplierIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ProductionTypeIDProperty, .GetInt32(2))
        LoadProperty(EventTypeIDProperty, .GetInt32(3))
        LoadProperty(ProductionAreaIDProperty, .GetInt32(4))
        LoadProperty(CostTypeIDProperty, .GetInt32(5))
        LoadProperty(QuantityProperty, .GetInt32(6))
        LoadProperty(AmountProperty, .GetDecimal(7))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace