﻿' Generated 08 Nov 2014 18:16 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Quoting.ReadOnly

  <Serializable()> _
  Public Class RODebtorFindList
    Inherits OBReadOnlyListBase(Of RODebtorFindList, RODebtorFind)

#Region " Business Methods "

    Public Function GetItem(DebtorID As Integer) As RODebtorFind

      For Each child As RODebtorFind In Me
        If child.DebtorID = DebtorID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Debtors"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits SingularCriteriaBase(Of Criteria)


      Public Shared DebtorNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.DebtorName, "")
      ''' <summary>
      ''' Gets and sets the Debtor value
      ''' </summary>
      <Display(Name:="Debtor", Description:=""),
      PrimarySearchField>
      Public Overridable Property DebtorName() As String
        Get
          Return ReadProperty(DebtorNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(DebtorNameProperty, Value)
        End Set
      End Property
      'SetExpression("RODebtorFindListCriteriaBO.DebtorSet(self)", , 250)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRODebtorFindList() As RODebtorFindList

      Return New RODebtorFindList()

    End Function

    Public Shared Sub BeginGetRODebtorFindList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RODebtorFindList)))

      Dim dp As New DataPortal(Of RODebtorFindList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRODebtorFindList(CallBack As EventHandler(Of DataPortalResult(Of RODebtorFindList)))

      Dim dp As New DataPortal(Of RODebtorFindList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRODebtorFindList() As RODebtorFindList

      Return DataPortal.Fetch(Of RODebtorFindList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RODebtorFind.GetRODebtorFind(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRODebtorListFind"
            'cm.Parameters.AddWithValue("@DebtorID", NothingDBNull(crit.DebtorID))
            cm.Parameters.AddWithValue("@DebtorName", Singular.Strings.MakeEmptyDBNull(crit.DebtorName))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace