﻿' Generated 08 Nov 2014 18:16 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting.ReadOnly

  <Serializable()> _
  Public Class ROCompanyRateCard
    Inherits SingularReadOnlyBase(Of ROCompanyRateCard)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared CompanyRateCardIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CompanyRateCardID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property CompanyRateCardID() As Integer
      Get
        Return GetProperty(CompanyRateCardIDProperty)
      End Get
    End Property

    Public Shared CompanyIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CompanyID, "Company", Nothing)
    ''' <summary>
    ''' Gets the Company value
    ''' </summary>
    <Display(Name:="Company", Description:="")>
    Public ReadOnly Property CompanyID() As Integer?
      Get
        Return GetProperty(CompanyIDProperty)
      End Get
    End Property

    Public Shared CompanyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Company, "Company", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="CompanyProperty", Description:="")>
    Public ReadOnly Property Company() As String
      Get
        Return GetProperty(CompanyProperty)
      End Get
    End Property

    Public Shared EffectiveDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EffectiveDate, "Effective Date")
    ''' <summary>
    ''' Gets the Effective Date value
    ''' </summary>
    <Display(Name:="Effective Date", Description:="")>
    Public ReadOnly Property EffectiveDate As DateTime?
      Get
        If Not FieldManager.FieldExists(EffectiveDateProperty) Then
          LoadProperty(EffectiveDateProperty, New SmartDate(DateTime.Now))
        End If
        Return GetProperty(EffectiveDateProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area", 0)
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared AreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Area, "Area", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property Area() As String
      Get
        Return GetProperty(AreaProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "ProductionType", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventTypeID, "Event Type", Nothing)
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public ReadOnly Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "EventType", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

    Public Shared RateTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RateTypeID, "Rate Type", Nothing)
    ''' <summary>
    ''' Gets the Rate Type value
    ''' </summary>
    <Display(Name:="Rate Type", Description:="")>
    Public ReadOnly Property RateTypeID() As Integer?
      Get
        Return GetProperty(RateTypeIDProperty)
      End Get
    End Property

    Public Shared RateTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.RateType, "RateType", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Rate Type", Description:="")>
    Public ReadOnly Property RateType() As String
      Get
        Return GetProperty(RateTypeProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "HumanResource", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="HR", Description:="")>
    Public ReadOnly Property HumanResource() As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position", Nothing)
    ''' <summary>
    ''' Gets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
    End Property

    Public Shared AdHocDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AdHocDescription, "Ad Hoc Description", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Ad Hoc Description", Description:="")>
    Public ReadOnly Property AdHocDescription() As String
      Get
        Return GetProperty(AdHocDescriptionProperty)
      End Get
    End Property

    Public Shared RateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Rate, "Rate", 0)
    ''' <summary>
    ''' Gets the Rate value
    ''' </summary>
    <Display(Name:="Rate", Description:="")>
    Public ReadOnly Property Rate() As Decimal
      Get
        Return GetProperty(RateProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, OBLib.JSCode.ROCompanyRateCardJS.ROCompanyRateCardToString)

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CompanyRateCardIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.AdHocDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROCompanyRateCard(dr As SafeDataReader) As ROCompanyRateCard

      Dim r As New ROCompanyRateCard()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CompanyRateCardIDProperty, .GetInt32(0))
        LoadProperty(CompanyIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(EffectiveDateProperty, .GetValue(2))
        LoadProperty(ProductionAreaIDProperty, .GetInt32(3))
        LoadProperty(ProductionTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(EventTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(RateTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(8)))
        LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
        LoadProperty(AdHocDescriptionProperty, .GetString(10))
        LoadProperty(RateProperty, .GetDecimal(11))
        LoadProperty(CreatedByProperty, .GetInt32(12))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(13))
        LoadProperty(ModifiedByProperty, .GetInt32(14))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(15))

        LoadProperty(CompanyProperty, .GetString(16))
        LoadProperty(AreaProperty, .GetString(17))
        LoadProperty(ProductionTypeProperty, .GetString(18))
        LoadProperty(EventTypeProperty, .GetString(19))
        LoadProperty(RateTypeProperty, .GetString(20))
        LoadProperty(HumanResourceProperty, .GetString(21))
        LoadProperty(DisciplineProperty, .GetString(22))
        LoadProperty(PositionProperty, .GetString(23))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace