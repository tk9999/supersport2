﻿' Generated 10 Nov 2014 16:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting.ReadOnly

  <Serializable()> _
  Public Class ROQuoteList
    Inherits SingularReadOnlyListBase(Of ROQuoteList, ROQuote)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

    Private mTotalPages As Integer = 0
    Public ReadOnly Property TotalPages As Integer
      Get
        Return mTotalPages
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(QuoteID As Integer) As ROQuote

      For Each child As ROQuote In Me
        If child.QuoteID = QuoteID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Quotes"

    End Function

    'Public Function GetROQuoteHeading(QuoteHeadingID As Integer) As ROQuoteHeading

    '  Dim obj As ROQuoteHeading = Nothing
    '  For Each parent As ROQuote In Me
    '    obj = parent.ROQuoteHeadingList.GetItem(QuoteHeadingID)
    '    If obj IsNot Nothing Then
    '      Return obj
    '    End If
    '  Next
    '  Return Nothing

    'End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared StartDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDate) _
                                                                      .AddSetExpression("Quotes.SetSartDate(self)", False, 250)
      <Display(Name:="Start Date", Description:="", Order:=2)>
      Public Property StartDate As DateTime?
        Get
          Return ReadProperty(StartDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(StartDateProperty, Value)
        End Set
      End Property

      Public Shared EndDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDate) _
                                                                    .AddSetExpression("Quotes.SetEndDate(self)", False, 250)
      <Display(Name:="End Date", Description:="", Order:=3)>
      Public Property EndDate As DateTime?
        Get
          Return ReadProperty(EndDateProperty)
        End Get
        Set(ByVal Value As DateTime?)
          LoadProperty(EndDateProperty, Value)
        End Set
      End Property

      Public Shared EventNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.EventName, Nothing) _
                                                                   .AddSetExpression("Quotes.SetEventName(self)", False, 250)

      ''' <summary>
      ''' Gets and sets the Location value
      ''' </summary>
      <Display(Name:="Event Name", Description:="", Order:=1)>
      Public Property EventName() As String
        Get
          Return ReadProperty(EventNameProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(EventNameProperty, Value)
        End Set
      End Property

      Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Production Type", Description:="", Order:=4)>
      Public Property ProductionTypeID() As Integer?
        Get
          Return ReadProperty(ProductionTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionTypeIDProperty, Value)
        End Set
      End Property

      Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventTypeID, "Event Type", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Event Type", Description:="")>
      Public Property EventTypeID() As Integer?
        Get
          Return ReadProperty(EventTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(EventTypeIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionTypeEventTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionTypeEventType) _
                                                                                 .AddSetExpression("Quotes.SetProductionTypeEventType(self)", False, 250)
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="ProductionTypeEventType", Description:="")>
      Public Overridable Property ProductionTypeEventType() As String
        Get
          Return ReadProperty(ProductionTypeEventTypeProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(ProductionTypeEventTypeProperty, Value)
        End Set
      End Property


      Public Sub New(StartDate As DateTime?, EndDate As DateTime?,
                     ProductionTypeID As Integer?, EventTypeID As Integer?,
                     EventName As String, PageNo As Integer?,
                     PageSize As Integer?, SortAsc As Boolean,
                     SortColumn As String)

        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.ProductionTypeID = ProductionTypeID
        Me.EventTypeID = EventTypeID
        Me.EventName = EventName
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortAsc = SortAsc
        Me.SortColumn = SortColumn

      End Sub

      Public Sub New(StartDate As DateTime?, EndDate As DateTime?,
                     PageSize As Integer, SortColumn As String)

        Me.StartDate = Now.AddDays(-30)
        Me.EndDate = Now.AddDays(30)
        Me.PageSize = PageSize
        Me.SortColumn = SortColumn

      End Sub

      Public Sub New()
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROQuoteList() As ROQuoteList

      Return New ROQuoteList()

    End Function

    Public Shared Sub BeginGetROQuoteList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROQuoteList)))

      Dim dp As New DataPortal(Of ROQuoteList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROQuoteList(CallBack As EventHandler(Of DataPortalResult(Of ROQuoteList)))

      Dim dp As New DataPortal(Of ROQuoteList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROQuoteList() As ROQuoteList

      Return DataPortal.Fetch(Of ROQuoteList)(New Criteria())

    End Function

    Public Shared Function GetROQuoteList(ROQuoteListCriteria As Criteria) As ROQuoteList

      Return DataPortal.Fetch(Of ROQuoteList)(ROQuoteListCriteria)

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROQuote.GetROQuote(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      'Dim parent As ROQuote = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parent Is Nothing OrElse parent.QuoteID <> sdr.GetInt32(1) Then
      '      parent = Me.GetItem(sdr.GetInt32(1))
      '    End If
      '    parent.ROQuoteHeadingList.RaiseListChangedEvents = False
      '    parent.ROQuoteHeadingList.Add(ROQuoteHeading.GetROQuoteHeading(sdr))
      '    parent.ROQuoteHeadingList.RaiseListChangedEvents = True
      '  End While
      'End If

      'Dim parentChild As ROQuoteHeading = Nothing
      'If sdr.NextResult() Then
      '  While sdr.Read
      '    If parentChild Is Nothing OrElse parentChild.QuoteHeadingID <> sdr.GetInt32(1) Then
      '      parentChild = Me.GetROQuoteHeading(sdr.GetInt32(1))
      '    End If
      '    parentChild.ROQuoteHeadingDetailList.RaiseListChangedEvents = False
      '    parentChild.ROQuoteHeadingDetailList.Add(ROQuoteHeadingDetail.GetROQuoteHeadingDetail(sdr))
      '    parentChild.ROQuoteHeadingDetailList.RaiseListChangedEvents = True
      '  End While
      'End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROQuoteList"
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(crit.ProductionTypeID))
            cm.Parameters.AddWithValue("@EventTypeID", NothingDBNull(crit.EventTypeID))
            cm.Parameters.AddWithValue("@EventName", Strings.MakeEmptyDBNull(crit.EventName))
            cm.Parameters.AddWithValue("@PageNo", crit.PageNo)
            cm.Parameters.AddWithValue("@PageSize", crit.PageSize)
            cm.Parameters.AddWithValue("@SortColumn", crit.SortColumn)
            cm.Parameters.AddWithValue("@SortAsc", crit.SortAsc)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region
  End Class

End Namespace