﻿' Generated 10 Nov 2014 16:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting.ReadOnly

  <Serializable()> _
  Public Class ROQuote
    Inherits SingularReadOnlyBase(Of ROQuote)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared QuoteIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.QuoteID, "Ref No", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Ref No"), Key()>
    Public ReadOnly Property QuoteID() As Integer
      Get
        Return GetProperty(QuoteIDProperty)
      End Get
    End Property

    Public Shared QuotedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuotedDate, "Quoted Date")
    ''' <summary>
    ''' Gets the Quoted Date value
    ''' </summary>
    <Display(Name:="Quoted Date", Description:="")>
    Public ReadOnly Property QuotedDate As DateTime?
      Get
        Return GetProperty(QuotedDateProperty)
      End Get
    End Property

    Public Shared EventNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventName, "Event Name", "")
    ''' <summary>
    ''' Gets the Event Name value
    ''' </summary>
    <Display(Name:="Event Name", Description:="")>
    Public ReadOnly Property EventName() As String
      Get
        Return GetProperty(EventNameProperty)
      End Get
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionTypeID, "Production Type", 0)
    ''' <summary>
    ''' Gets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public ReadOnly Property ProductionTypeID() As Integer
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EventTypeID, "Event Type", 0)
    ''' <summary>
    ''' Gets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public ReadOnly Property EventTypeID() As Integer
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
    End Property

    Public Shared VenueIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VenueID, "Venue", 0)
    ''' <summary>
    ''' Gets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public ReadOnly Property VenueID() As Integer
      Get
        Return GetProperty(VenueIDProperty)
      End Get
    End Property

    Public Shared EventDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EventDate, "Event Date")
    ''' <summary>
    ''' Gets the Event Date value
    ''' </summary>
    <Display(Name:="Event Date", Description:="")>
    Public ReadOnly Property EventDate As DateTime?
      Get
        Return GetProperty(EventDateProperty)
      End Get
    End Property

    Public Shared TechnicalSpecProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TechnicalSpec, "Technical Spec", "")
    ''' <summary>
    ''' Gets the Technical Spec value
    ''' </summary>
    <Display(Name:="Technical Spec", Description:="")>
    Public ReadOnly Property TechnicalSpec() As String
      Get
        Return GetProperty(TechnicalSpecProperty)
      End Get
    End Property

    Public Shared TxDaysProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TxDays, "Tx Days", "")
    ''' <summary>
    ''' Gets the Tx Days value
    ''' </summary>
    <Display(Name:="Tx Days", Description:="")>
    Public ReadOnly Property TxDays() As String
      Get
        Return GetProperty(TxDaysProperty)
      End Get
    End Property

    Public Shared InternalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InternalInd, "Internal", False)
    ''' <summary>
    ''' Gets the Internal value
    ''' </summary>
    <Display(Name:="Internal", Description:="")>
    Public ReadOnly Property InternalInd() As Boolean
      Get
        Return GetProperty(InternalIndProperty)
      End Get
    End Property

    Public Shared ValidEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ValidEndDate, "Valid End Date")
    ''' <summary>
    ''' Gets the Valid End Date value
    ''' </summary>
    <Display(Name:="Valid End Date", Description:="")>
    Public ReadOnly Property ValidEndDate As DateTime?
      Get
        Return GetProperty(ValidEndDateProperty)
      End Get
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Debtor", Nothing)
    ''' <summary>
    ''' Gets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="")>
    Public ReadOnly Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
    End Property

    Public Shared QuoteOverrideAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.QuoteOverrideAmount, "Override Amount", 0)
    ''' <summary>
    ''' Gets the Quote Override Amount value
    ''' </summary>
    <Display(Name:="Override Amount", Description:="")>
    Public ReadOnly Property QuoteOverrideAmount() As Decimal
      Get
        Return GetProperty(QuoteOverrideAmountProperty)
      End Get
    End Property

    Public Shared QuoteOverrideReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.QuoteOverrideReason, "Override Reason", "")
    ''' <summary>
    ''' Gets the Quote Override Reason value
    ''' </summary>
    <Display(Name:="Override Reason", Description:="")>
    Public ReadOnly Property QuoteOverrideReason() As String
      Get
        Return GetProperty(QuoteOverrideReasonProperty)
      End Get
    End Property

    Public Shared QuoteOverrideDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteOverrideDateTime, "Override Date Time")
    ''' <summary>
    ''' Gets the Quote Override Date Time value
    ''' </summary>
    <Display(Name:="Override Date Time", Description:="")>
    Public ReadOnly Property QuoteOverrideDateTime As DateTime?
      Get
        Return GetProperty(QuoteOverrideDateTimeProperty)
      End Get
    End Property

    Public Shared QuoteOverrideByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.QuoteOverrideBy, "Quote Override By", OBLib.Security.Settings.CurrentUser.UserID)
    ''' <summary>
    ''' Gets the Quote Override By value
    ''' </summary>
    <Display(Name:="Override By", Description:="")>
    Public ReadOnly Property QuoteOverrideBy() As Integer
      Get
        Return GetProperty(QuoteOverrideByProperty)
      End Get
    End Property

    Public Shared QuoteAcceptedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteAcceptedDateTime, "Quote Accepted Date Time")
    ''' <summary>
    ''' Gets the Quote Accepted Date Time value
    ''' </summary>
    <Display(Name:="Quote Accepted Date Time", Description:="")>
    Public ReadOnly Property QuoteAcceptedDateTime As DateTime?
      Get
        Return GetProperty(QuoteAcceptedDateTimeProperty)
      End Get
    End Property

    Public Shared QuoteDeclinedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteDeclinedDateTime, "Quote Declined Date Time")
    ''' <summary>
    ''' Gets the Quote Declined Date Time value
    ''' </summary>
    <Display(Name:="Declined Date Time", Description:="")>
    Public ReadOnly Property QuoteDeclinedDateTime As DateTime?
      Get
        Return GetProperty(QuoteDeclinedDateTimeProperty)
      End Get
    End Property

    Public Shared PreviousQuoteIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PreviousQuoteID, "Previous Quote", 0)
    ''' <summary>
    ''' Gets the Previous Quote value
    ''' </summary>
    <Display(Name:="Previous Quote", Description:="")>
    Public ReadOnly Property PreviousQuoteID() As Integer
      Get
        Return GetProperty(PreviousQuoteIDProperty)
      End Get
    End Property

    Public Shared VersionNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VersionNo, "Version No", 1)
    ''' <summary>
    ''' Gets the Version No value
    ''' </summary>
    <Display(Name:="Version No", Description:="")>
    Public ReadOnly Property VersionNo() As Integer
      Get
        Return GetProperty(VersionNoProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", OBLib.Security.Settings.CurrentUser.UserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DebtorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Debtor, "Debtor", "")
    ''' <summary>
    ''' Gets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="")>
    Public ReadOnly Property Debtor() As String
      Get
        Return GetProperty(DebtorProperty)
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventType, "EventType", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Event Type", Description:="")>
    Public ReadOnly Property EventType() As String
      Get
        Return GetProperty(EventTypeProperty)
      End Get
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionVenue, "Venue/Stadium", "")
    ''' <summary>
    ''' Gets the Teams Playing value
    ''' </summary>
    <Display(Name:="Venue/Stadium", Description:="")>
    Public ReadOnly Property ProductionVenue() As String
      Get
        Return GetProperty(ProductionVenueProperty)
      End Get
    End Property

#End Region

    '#Region " Child Lists "

    '    Public Shared ROQuoteHeadingListProperty As PropertyInfo(Of ROQuoteHeadingList) = RegisterProperty(Of ROQuoteHeadingList)(Function(c) c.ROQuoteHeadingList, "RO Quote Heading List")

    '    Public ReadOnly Property ROQuoteHeadingList() As ROQuoteHeadingList
    '      Get
    '        If GetProperty(ROQuoteHeadingListProperty) Is Nothing Then
    '          LoadProperty(ROQuoteHeadingListProperty, Quoting.ReadOnly.ROQuoteHeadingList.NewROQuoteHeadingList())
    '        End If
    '        Return GetProperty(ROQuoteHeadingListProperty)
    '      End Get
    '    End Property

    '#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(QuoteIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.EventName

    End Function


#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROQuote(dr As SafeDataReader) As ROQuote

      Dim r As New ROQuote()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(QuoteIDProperty, .GetInt32(0))
        LoadProperty(QuotedDateProperty, .GetValue(1))
        LoadProperty(EventNameProperty, .GetString(2))
        LoadProperty(ProductionTypeIDProperty, .GetInt32(3))
        LoadProperty(EventTypeIDProperty, .GetInt32(4))
        LoadProperty(VenueIDProperty, .GetInt32(5))
        LoadProperty(EventDateProperty, .GetValue(6))
        LoadProperty(TechnicalSpecProperty, .GetString(7))
        LoadProperty(TxDaysProperty, .GetString(8))
        LoadProperty(InternalIndProperty, .GetBoolean(9))
        LoadProperty(ValidEndDateProperty, .GetValue(10))
        LoadProperty(DebtorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
        LoadProperty(QuoteOverrideAmountProperty, .GetDecimal(12))
        LoadProperty(QuoteOverrideReasonProperty, .GetString(13))
        LoadProperty(QuoteOverrideDateTimeProperty, .GetValue(14))
        LoadProperty(QuoteOverrideByProperty, .GetInt32(15))
        LoadProperty(QuoteAcceptedDateTimeProperty, .GetValue(16))
        LoadProperty(QuoteDeclinedDateTimeProperty, .GetValue(17))
        LoadProperty(PreviousQuoteIDProperty, .GetInt32(18))
        LoadProperty(VersionNoProperty, .GetInt32(19))
        LoadProperty(CreatedByProperty, .GetInt32(20))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(21))
        LoadProperty(ModifiedByProperty, .GetInt32(22))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(23))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace