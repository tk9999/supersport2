﻿' Generated 08 Nov 2014 18:17 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting.ReadOnly

  <Serializable()> _
  Public Class ROSupplierRateCardList
    Inherits SingularReadOnlyListBase(Of ROSupplierRateCardList, ROSupplierRateCard)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(SupplierRateCardID As Integer) As ROSupplierRateCard

      For Each child As ROSupplierRateCard In Me
        If child.SupplierRateCardID = SupplierRateCardID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Supplier Rate Cards"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Keyword, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Keyword", Description:="")>
      Public Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROSupplierRateCardList() As ROSupplierRateCardList

      Return New ROSupplierRateCardList()

    End Function

    Public Shared Sub BeginGetROSupplierRateCardList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROSupplierRateCardList)))

      Dim dp As New DataPortal(Of ROSupplierRateCardList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROSupplierRateCardList(CallBack As EventHandler(Of DataPortalResult(Of ROSupplierRateCardList)))

      Dim dp As New DataPortal(Of ROSupplierRateCardList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROSupplierRateCardList() As ROSupplierRateCardList

      Return DataPortal.Fetch(Of ROSupplierRateCardList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROSupplierRateCard.GetROSupplierRateCard(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROSupplierRateCardList"
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using
    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace