﻿' Generated 08 Nov 2014 18:16 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting.ReadOnly

  <Serializable()> _
  Public Class RODebtorFind
    Inherits OBReadOnlyBase(Of RODebtorFind)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DebtorID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property DebtorID() As Integer
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
    End Property

    Public Shared DebtorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Debtor, "Debtor", "")
    ''' <summary>
    ''' Gets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="Debtor"), Required(ErrorMessage:="A debtor is required")>
    Public ReadOnly Property Debtor() As String
      Get
        Return GetProperty(DebtorProperty)
      End Get
    End Property

    Public Shared ContactNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactName, "Contact Name", "")
    ''' <summary>
    ''' Gets the Contact Name value
    ''' </summary>
    <Display(Name:="Contact Name", Description:="Name of contact"),
    Required(ErrorMessage:="A contact name is required")>
    Public ReadOnly Property ContactName() As String
      Get
        Return GetProperty(ContactNameProperty)
      End Get
    End Property

    Public Shared ContactNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContactNumber, "Contact Number", "")
    ''' <summary>
    ''' Gets the Contact Number value
    ''' </summary>
    <Display(Name:="Contact Number", Description:="")>
    Public ReadOnly Property ContactNumber() As String
      Get
        Return GetProperty(ContactNumberProperty)
      End Get
    End Property

    Public Shared AddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Address, "Address", "")
    ''' <summary>
    ''' Gets the Address value
    ''' </summary>
    <Display(Name:="Address", Description:="")>
    Public ReadOnly Property Address() As String
      Get
        Return GetProperty(AddressProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(DebtorIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.Debtor

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "


#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRODebtorFind(dr As SafeDataReader) As RODebtorFind

      Dim r As New RODebtorFind()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(DebtorIDProperty, .GetInt32(0))
        LoadProperty(DebtorProperty, .GetString(1))
        LoadProperty(ContactNameProperty, .GetString(2))
        LoadProperty(ContactNumberProperty, .GetString(3))
        LoadProperty(AddressProperty, .GetString(4))
        LoadProperty(CreatedByProperty, .GetInt32(5))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(6))
        LoadProperty(ModifiedByProperty, .GetInt32(7))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace