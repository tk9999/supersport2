﻿' Generated 08 Nov 2014 18:16 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting.ReadOnly

  <Serializable()> _
  Public Class RODebtorList
    Inherits OBReadOnlyListBase(Of RODebtorList, RODebtor)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(DebtorID As Integer) As RODebtor

      For Each child As RODebtor In Me
        If child.DebtorID = DebtorID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Debtors"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Debtor ID", Nothing)

      <Display(Name:="Debtor ID", Description:="")>
      Public Property DebtorID() As Integer?
        Get
          Return ReadProperty(DebtorIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(DebtorIDProperty, Value)
        End Set
      End Property


      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Keyword, "") _
                                                                 .AddSetExpression("RODebtorListCriteriaBO.KeywordSet(self)", , "250")
      ''' <summary>
      ''' Gets and sets the Keyword value
      ''' </summary>
      <Display(Name:="Keyword", Description:="")>
      Public Overridable Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Sub New(PageNo As Integer, PageSize As Integer)
        Me.PageNo = PageNo
        Me.PageSize = PageSize
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewRODebtorList() As RODebtorList

      Return New RODebtorList()

    End Function

    Public Shared Sub BeginGetRODebtorList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of RODebtorList)))

      Dim dp As New DataPortal(Of RODebtorList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetRODebtorList(CallBack As EventHandler(Of DataPortalResult(Of RODebtorList)))

      Dim dp As New DataPortal(Of RODebtorList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetRODebtorList() As RODebtorList

      Return DataPortal.Fetch(Of RODebtorList)(New Criteria())

    End Function

    Public Shared Function GetRODebtorListCommonData() As RODebtorList

      Return DataPortal.Fetch(Of RODebtorList)(New Criteria(1, 500))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      'mTotalPages = sdr.GetInt32(1)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(RODebtor.GetRODebtor(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getRODebtorList"
            cm.Parameters.AddWithValue("@DebtorID", NothingDBNull(crit.DebtorID))
            cm.Parameters.AddWithValue("@Keyword", Singular.Strings.MakeEmptyDBNull(crit.Keyword))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace