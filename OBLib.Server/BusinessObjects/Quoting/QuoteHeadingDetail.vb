﻿' Generated 10 Nov 2014 16:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting

  <Serializable()> _
  Public Class QuoteHeadingDetail
    Inherits SingularBusinessBase(Of QuoteHeadingDetail)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared QuoteHeadingDetailIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.QuoteHeadingDetailID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property QuoteHeadingDetailID() As Integer
      Get
        Return GetProperty(QuoteHeadingDetailIDProperty)
      End Get
    End Property

    Public Shared QuoteHeadingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.QuoteHeadingID, "Quote Heading", Nothing)
    ''' <summary>
    ''' Gets the Quote Heading value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property QuoteHeadingID() As Integer?
      Get
        Return GetProperty(QuoteHeadingIDProperty)
      End Get
    End Property

    Public Shared CostTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.CostTypeID, "Cost Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Cost Type value
    ''' </summary>
    <Display(Name:="Cost Type", Description:=""),
    Required(ErrorMessage:="Cost Type required"),
    DropDownWeb(GetType(ROCostTypeList))>
    Public Property CostTypeID() As Integer?
      Get
        Return GetProperty(CostTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CostTypeIDProperty, Value)
      End Set
    End Property

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline", Nothing)
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:=""),
    Required(ErrorMessage:="Discipline required"),
    DropDownWeb(GetType(RODisciplineList))>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared PositionIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PositionID, "Position", Nothing)
    ''' <summary>
    ''' Gets and sets the Position value
    ''' </summary>
    <Display(Name:="Position", Description:=""),
    DropDownWeb(GetType(ROPositionList), ThisFilterMember:="DisciplineID")>
    Public Property PositionID() As Integer?
      Get
        Return GetProperty(PositionIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PositionIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Production Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    DropDownWeb(GetType(ROProductionAreaList))>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared QuantityProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.Quantity, 0) _
                                                                 .AddSetExpression(OBLib.JSCode.QuoteHeadingDetailJS.QuantitySet, False)
    ''' <summary>
    ''' Gets and sets the Quantity value
    ''' </summary>
    <Display(Name:="Quantity", Description:=""),
    Required(ErrorMessage:="Quantity required")>
    Public Property Quantity() As Integer
      Get
        Return GetProperty(QuantityProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(QuantityProperty, Value)
      End Set
    End Property

    Public Shared FlightsProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.Flights, 0) _
                                                                 .AddSetExpression(OBLib.JSCode.QuoteHeadingDetailJS.FlightsSet, False)
    ''' <summary>
    ''' Gets and sets the Flights value
    ''' </summary>
    <Display(Name:="Flights", Description:=""),
    Required(ErrorMessage:="Flights required"),
    NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property Flights() As Decimal
      Get
        Return GetProperty(FlightsProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(FlightsProperty, Value)
      End Set
    End Property

    Public Shared AccommodationProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.Accommodation, 0) _
                                                                 .AddSetExpression(OBLib.JSCode.QuoteHeadingDetailJS.AccommodationSet, False)
    ''' <summary>
    ''' Gets and sets the Accommodation value
    ''' </summary>
    <Display(Name:="Accommodation", Description:=""),
    Required(ErrorMessage:="Accommodation required"),
    NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property Accommodation() As Decimal
      Get
        Return GetProperty(AccommodationProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AccommodationProperty, Value)
      End Set
    End Property

    Public Shared CarHireProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.CarHire, 0) _
                                                                 .AddSetExpression(OBLib.JSCode.QuoteHeadingDetailJS.CarHireSet, False)
    ''' <summary>
    ''' Gets and sets the Car Hire value
    ''' </summary>
    <Display(Name:="Car Hire", Description:=""),
    Required(ErrorMessage:="Car Hire required"),
    NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property CarHire() As Decimal
      Get
        Return GetProperty(CarHireProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(CarHireProperty, Value)
      End Set
    End Property

    Public Shared SnTProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.SnT, 0) _
                                                                 .AddSetExpression(OBLib.JSCode.QuoteHeadingDetailJS.SnTSet, False)
    ''' <summary>
    ''' Gets and sets the Sn T value
    ''' </summary>
    <Display(Name:="SnT", Description:=""),
    Required(ErrorMessage:="SnT required"),
    NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property SnT() As Decimal
      Get
        Return GetProperty(SnTProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(SnTProperty, Value)
      End Set
    End Property

    Public Shared FeeProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.Fee, 0) _
                                                                 .AddSetExpression(OBLib.JSCode.QuoteHeadingDetailJS.FeeSet, False)
    ''' <summary>
    ''' Gets and sets the Fee value
    ''' </summary>
    <Display(Name:="Fee", Description:=""),
    Required(ErrorMessage:="Fee required"),
    NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property Fee() As Decimal
      Get
        Return GetProperty(FeeProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(FeeProperty, Value)
      End Set
    End Property

    Public Shared CompanyRateCardIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.CompanyRateCardID, Nothing) _
                                                                           .AddSetExpression(OBLib.JSCode.QuoteHeadingDetailJS.CompanyRateCardIDSet, False)
    ''' <summary>
    ''' Gets and sets the Company Rate Card value
    ''' </summary>
    <Display(Name:="Company Rate Card", Description:=""),
    DropDownWeb(GetType(ROCompanyRateCardList), DropDownColumns:={"Discipline", "Position", "HumanResource", "ProductionType", "EventType", "Area", "RateType", "Rate"},
                FilterMethodName:="FilterCompanyRateCards", ValueMember:="CompanyRateCardID")>
    Public Property CompanyRateCardID() As Integer?
      Get
        Return GetProperty(CompanyRateCardIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(CompanyRateCardIDProperty, Value)
      End Set
    End Property

    Public Shared SupplierRateCardIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SupplierRateCardID, Nothing) _
                                                                           .AddSetExpression(OBLib.JSCode.QuoteHeadingDetailJS.SupplierRateCardIDSet, False)
    ''' <summary>
    ''' Gets and sets the Supplier Rate Card value
    ''' </summary>
    <Display(Name:="Supplier Rate Card", Description:=""),
    DropDownWeb(GetType(ROSupplierRateCardList), DropDownColumns:={"ProductionType", "EventType", "Area", "CostType", "Quantity", "Amount"},
                FilterMethodName:="FilterSupplierRateCards", ValueMember:="SupplierRateCardID")>
    Public Property SupplierRateCardID() As Integer?
      Get
        Return GetProperty(SupplierRateCardIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierRateCardIDProperty, Value)
      End Set
    End Property

    Public Shared RateProperty As PropertyInfo(Of Decimal) = RegisterSProperty(Of Decimal)(Function(c) c.Rate, 0) _
                                                             .AddSetExpression(OBLib.JSCode.QuoteHeadingDetailJS.RateSet, False)
    ''' <summary>
    ''' Gets and sets the Rate value
    ''' </summary>
    <Display(Name:="Rate", Description:=""),
    Required(ErrorMessage:="Rate required"),
    NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property Rate() As Decimal
      Get
        Return GetProperty(RateProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(RateProperty, Value)
      End Set
    End Property

    Public Shared SupplierIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.SupplierID, Nothing) _
                                                                    .AddSetExpression(OBLib.JSCode.QuoteHeadingDetailJS.SupplierIDSet, False)
    ''' <summary>
    ''' Gets and sets the Supplier Rate Card value
    ''' </summary>
    <Display(Name:="Supplier", Description:=""),
    DropDownWeb(GetType(ROSupplierList))>
    Public Property SupplierID() As Integer?
      Get
        Return GetProperty(SupplierIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SupplierIDProperty, Value)
      End Set
    End Property

    Public Shared AmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.Amount, "Rate", 0)
    ''' <summary>
    ''' Gets and sets the Rate value
    ''' </summary>
    <Display(Name:="Amount", Description:=""),
    Required(ErrorMessage:="Amount required"),
    NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property Amount() As Decimal
      Get
        Return GetProperty(AmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(AmountProperty, Value)
      End Set
    End Property

    Public Shared DisciplinePositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.DisciplinePosition, "Debtor", "")
    ''' <summary>
    ''' Gets the Debtor value
    ''' </summary>
    <Display(Name:="Discipline Position", Description:="")>
    Public ReadOnly Property DisciplinePosition() As String
      Get
        Return Position & " (" & Discipline & ")"
      End Get
    End Property

    Public Shared CompanyRateCardProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CompanyRateCard, "Company Rate Card", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Company Rate Card", Description:="")>
    Public ReadOnly Property CompanyRateCard() As String
      Get
        Return Company & " - (" & CompanyRate & ")"
      End Get
    End Property

    Public Shared SupplierRateCardProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SupplierRateCard, "Supplier Rate Card", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Supplier Rate Card", Description:="")>
    Public ReadOnly Property SupplierRateCard() As String
      Get
        Return Supplier & " - (" & SupplierAmount & ")"
      End Get
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public ReadOnly Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
    End Property

    Public Shared PositionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Position, "Position", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Position", Description:="")>
    Public ReadOnly Property Position() As String
      Get
        Return GetProperty(PositionProperty)
      End Get
    End Property

    Public Shared CompanyProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Company, "Company", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Company", Description:="")>
    Public ReadOnly Property Company() As String
      Get
        Return GetProperty(CompanyProperty)
      End Get
    End Property

    Public Shared CompanyRateProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.CompanyRate, "CompanyRate")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    Public ReadOnly Property CompanyRate() As Decimal
      Get
        Return GetProperty(CompanyRateProperty)
      End Get

    End Property

    Public Shared SupplierAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.SupplierAmount, "SupplierAmount")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    Public ReadOnly Property SupplierAmount() As Decimal
      Get
        Return GetProperty(SupplierAmountProperty)
      End Get

    End Property

    Public Shared SupplierProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Supplier, "Supplier", "")
    ''' <summary>
    ''' Gets the Ad Hoc Description value
    ''' </summary>
    <Display(Name:="Supplier", Description:="")>
    Public ReadOnly Property Supplier() As String
      Get
        Return GetProperty(SupplierProperty)
      End Get
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, OBLib.JSCode.QuoteHeadingDetailJS.QuoteHeadingDetailToString)

#End Region

#Region " Methods "

    Public Function GetParent() As QuoteHeading

      Return CType(CType(Me.Parent, QuoteHeadingDetailList).Parent, QuoteHeading)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(QuoteHeadingDetailIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.QuoteHeadingDetailID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Quote Heading Detail")
        Else
          Return String.Format("Blank {0}", "Quote Heading Detail")
        End If
      Else
        Return Me.QuoteHeadingDetailID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewQuoteHeadingDetail() method.

    End Sub

    Public Shared Function NewQuoteHeadingDetail() As QuoteHeadingDetail

      Return DataPortal.CreateChild(Of QuoteHeadingDetail)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetQuoteHeadingDetail(dr As SafeDataReader) As QuoteHeadingDetail

      Dim q As New QuoteHeadingDetail()
      q.Fetch(dr)
      Return q

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(QuoteHeadingDetailIDProperty, .GetInt32(0))
          LoadProperty(QuoteHeadingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(CostTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(DisciplineIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(PositionIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(QuantityProperty, .GetInt32(6))
          LoadProperty(FlightsProperty, .GetDecimal(7))
          LoadProperty(AccommodationProperty, .GetDecimal(8))
          LoadProperty(CarHireProperty, .GetDecimal(9))
          LoadProperty(SnTProperty, .GetDecimal(10))
          LoadProperty(FeeProperty, .GetDecimal(11))
          LoadProperty(CompanyRateCardIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(SupplierRateCardIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(RateProperty, .GetDecimal(14))
          LoadProperty(SupplierIDProperty, ZeroNothing(.GetInt32(15)))
          LoadProperty(AmountProperty, .GetDecimal(16))
          LoadProperty(PositionProperty, .GetString(17))
          LoadProperty(DisciplineProperty, .GetString(18))
          LoadProperty(SupplierAmountProperty, .GetDecimal(19))
          LoadProperty(CompanyRateProperty, .GetDecimal(20))
          LoadProperty(CompanyProperty, .GetString(21))
          LoadProperty(SupplierProperty, .GetString(22))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insQuoteHeadingDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updQuoteHeadingDetail"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramQuoteHeadingDetailID As SqlParameter = .Parameters.Add("@QuoteHeadingDetailID", SqlDbType.Int)
          paramQuoteHeadingDetailID.Value = GetProperty(QuoteHeadingDetailIDProperty)
          If Me.IsNew Then
            paramQuoteHeadingDetailID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@QuoteHeadingID", Me.GetParent().QuoteHeadingID)
          .Parameters.AddWithValue("@CostTypeID", GetProperty(CostTypeIDProperty))
          .Parameters.AddWithValue("@DisciplineID", GetProperty(DisciplineIDProperty))
          .Parameters.AddWithValue("@PositionID", Singular.Misc.NothingDBNull(GetProperty(PositionIDProperty)))
          .Parameters.AddWithValue("@ProductionAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaIDProperty)))
          .Parameters.AddWithValue("@Quantity", GetProperty(QuantityProperty))
          .Parameters.AddWithValue("@Flights", GetProperty(FlightsProperty))
          .Parameters.AddWithValue("@Accommodation", GetProperty(AccommodationProperty))
          .Parameters.AddWithValue("@CarHire", GetProperty(CarHireProperty))
          .Parameters.AddWithValue("@SnT", GetProperty(SnTProperty))
          .Parameters.AddWithValue("@Fee", GetProperty(FeeProperty))
          .Parameters.AddWithValue("@CompanyRateCardID", Singular.Misc.NothingDBNull(GetProperty(CompanyRateCardIDProperty)))
          .Parameters.AddWithValue("@SupplierRateCardID", Singular.Misc.NothingDBNull(GetProperty(SupplierRateCardIDProperty)))
          .Parameters.AddWithValue("@Rate", GetProperty(RateProperty))
          .Parameters.AddWithValue("@SupplierID", NothingDBNull(GetProperty(SupplierIDProperty)))
          .Parameters.AddWithValue("@Amount", GetProperty(AmountProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(QuoteHeadingDetailIDProperty, paramQuoteHeadingDetailID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delQuoteHeadingDetail"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@QuoteHeadingDetailID", GetProperty(QuoteHeadingDetailIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace