﻿' Generated 10 Nov 2014 16:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting

  <Serializable()> _
  Public Class QuoteHeadingList
    Inherits SingularBusinessListBase(Of QuoteHeadingList, QuoteHeading)

#Region " Business Methods "

    Public Function GetItem(QuoteHeadingID As Integer) As QuoteHeading

      For Each child As QuoteHeading In Me
        If child.QuoteHeadingID = QuoteHeadingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Quote Headings"

    End Function

    Public Function GetQuoteHeadingDetail(QuoteHeadingDetailID As Integer) As QuoteHeadingDetail

      Dim obj As QuoteHeadingDetail = Nothing
      For Each parent As QuoteHeading In Me
        obj = parent.QuoteHeadingDetailList.GetItem(QuoteHeadingDetailID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewQuoteHeadingList() As QuoteHeadingList

      Return New QuoteHeadingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As QuoteHeading In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As QuoteHeading In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace