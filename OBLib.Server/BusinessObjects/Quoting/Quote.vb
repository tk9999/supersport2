﻿' Generated 10 Nov 2014 16:24 - Singular Systems Object Generator Version 2.1.667
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Quoting.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Quoting

  <Serializable()> _
  Public Class Quote
    Inherits SingularBusinessBase(Of Quote)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared QuoteIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.QuoteID, "Ref No", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Ref No"), Key()>
    Public ReadOnly Property QuoteID() As Integer
      Get
        Return GetProperty(QuoteIDProperty)
      End Get
    End Property

    Public Shared QuotedDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuotedDate, "Quoted Date")
    ''' <summary>
    ''' Gets and sets the Quoted Date value
    ''' </summary>
    <Display(Name:="Quoted Date", Description:=""),
    Required(ErrorMessage:="Quoted Date required"),
    Singular.DataAnnotations.DateField(FormatString:="dd MMMM yyyy")>
    Public Property QuotedDate As DateTime?
      Get
        Return GetProperty(QuotedDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(QuotedDateProperty, Value)
      End Set
    End Property

    Public Shared EventNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EventName, "Event Name", "")
    ''' <summary>
    ''' Gets and sets the Event Name value
    ''' </summary>
    <Display(Name:="Event Name", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Event Name is required"),
    StringLength(500, ErrorMessage:="Event Name cannot be more than 500 characters")>
    Public Property EventName() As String
      Get
        Return GetProperty(EventNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(EventNameProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionTypeID, "Production Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:=""),
    Required(ErrorMessage:="Production Type required")>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared EventTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EventTypeID, "Event Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Event Type value
    ''' </summary>
    <Display(Name:="Event Type", Description:=""),
    Required(ErrorMessage:="Event Type required")>
    Public Property EventTypeID() As Integer?
      Get
        Return GetProperty(EventTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EventTypeIDProperty, Value)
      End Set
    End Property

    Public Shared VenueIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VenueID, "Venue", Nothing)
    ''' <summary>
    ''' Gets and sets the Venue value
    ''' </summary>
    <Display(Name:="Venue", Description:="")>
    Public Property VenueID() As Integer?
      Get
        Return GetProperty(VenueIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VenueIDProperty, Value)
      End Set
    End Property

    Public Shared EventDateProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EventDate, Nothing)
    ''' <summary>
    ''' Gets and sets the Event Date value
    ''' </summary>
    <Display(Name:="Event Date", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd MMMM yyyy")>
    Public Property EventDate As DateTime?
      Get
        Return GetProperty(EventDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EventDateProperty, Value)
      End Set
    End Property

    Public Shared TechnicalSpecProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TechnicalSpec, "Technical Spec", "")
    ''' <summary>
    ''' Gets and sets the Technical Spec value
    ''' </summary>
    <Display(Name:="Technical Spec", Description:=""),
    Required(AllowEmptyStrings:=False, ErrorMessage:="Technical Spec is required"),
    StringLength(200, ErrorMessage:="Technical Spec cannot be more than 200 characters")>
    Public Property TechnicalSpec() As String
      Get
        Return GetProperty(TechnicalSpecProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TechnicalSpecProperty, Value)
      End Set
    End Property

    Public Shared TxDaysProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TxDays, "Tx Days", "")
    ''' <summary>
    ''' Gets and sets the Tx Days value
    ''' </summary>
    <Display(Name:="Tx Days", Description:=""),
    StringLength(200, ErrorMessage:="Tx Days cannot be more than 200 characters")>
    Public Property TxDays() As String
      Get
        Return GetProperty(TxDaysProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TxDaysProperty, Value)
      End Set
    End Property

    Public Shared InternalIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.InternalInd, "Internal", False)
    ''' <summary>
    ''' Gets and sets the Internal value
    ''' </summary>
    <Display(Name:="Internal", Description:="")>
    Public Property InternalInd() As Boolean
      Get
        Return GetProperty(InternalIndProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(InternalIndProperty, Value)
      End Set
    End Property

    Public Shared ValidEndDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.ValidEndDate, "Valid End Date")
    ''' <summary>
    ''' Gets and sets the Valid End Date value
    ''' </summary>
    <Display(Name:="Valid End Date", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd MMMM yyyy")>
    Public Property ValidEndDate As DateTime?
      Get
        Return GetProperty(ValidEndDateProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(ValidEndDateProperty, Value)
      End Set
    End Property

    Public Shared DebtorIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DebtorID, "Debtor", Nothing)
    ''' <summary>
    ''' Gets and sets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:="")>
    Public Property DebtorID() As Integer?
      Get
        Return GetProperty(DebtorIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DebtorIDProperty, Value)
      End Set
    End Property
    '    DropDownWeb(GetType(RODebtorList))

    Public Shared QuoteOverrideAmountProperty As PropertyInfo(Of Decimal?) = RegisterSProperty(Of Decimal?)(Function(c) c.QuoteOverrideAmount, Nothing) _
                                                                             .AddSetExpression(OBLib.JSCode.QuoteJS.QuoteOverrideAmountSet, False)
    ''' <summary>
    ''' Gets and sets the Quote Override Amount value
    ''' </summary>
    <Display(Name:="Override Amount", Description:=""),
    NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property QuoteOverrideAmount() As Decimal?
      Get
        Return GetProperty(QuoteOverrideAmountProperty)
      End Get
      Set(ByVal Value As Decimal?)
        SetProperty(QuoteOverrideAmountProperty, Value)
      End Set
    End Property

    Public Shared QuoteOverrideReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.QuoteOverrideReason, "Override Reason", "")
    ''' <summary>
    ''' Gets and sets the Quote Override Reason value
    ''' </summary>
    <Display(Name:="Override Reason", Description:=""),
    StringLength(500, ErrorMessage:="Quote Override Reason cannot be more than 500 characters")>
    Public Property QuoteOverrideReason() As String
      Get
        Return GetProperty(QuoteOverrideReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(QuoteOverrideReasonProperty, Value)
      End Set
    End Property

    Public Shared QuoteOverrideDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.QuoteOverrideDateTime, "Override Date")
    ''' <summary>
    ''' Gets and sets the Quote Override Date Time value
    ''' </summary>
    <Display(Name:="Override Date", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd-MMM-yy HH:mm")>
    Public Property QuoteOverrideDateTime As DateTime?
      Get
        Return GetProperty(QuoteOverrideDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(QuoteOverrideDateTimeProperty, Value)
      End Set
    End Property

    Public Shared QuoteOverrideByProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.QuoteOverrideBy, "Override By", Nothing)
    ''' <summary>
    ''' Gets and sets the Quote Override By value
    ''' </summary>
    <Display(Name:="Override By", Description:="")>
    Public Property QuoteOverrideBy() As Integer?
      Get
        Return GetProperty(QuoteOverrideByProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(QuoteOverrideByProperty, Value)
      End Set
    End Property

    Public Shared QuoteAcceptedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.QuoteAcceptedDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Quote Accepted Date Time value
    ''' </summary>
    <Display(Name:="Accepted Date", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd-MMM-yy HH:mm")>
    Public Property QuoteAcceptedDateTime As DateTime?
      Get
        Return GetProperty(QuoteAcceptedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(QuoteAcceptedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared QuoteDeclinedDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.QuoteDeclinedDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Quote Declined Date Time value
    ''' </summary>
    <Display(Name:="Declined Date", Description:=""),
    Singular.DataAnnotations.DateField(FormatString:="dd-MMM-yy HH:mm")>
    Public Property QuoteDeclinedDateTime As DateTime?
      Get
        Return GetProperty(QuoteDeclinedDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(QuoteDeclinedDateTimeProperty, Value)
      End Set
    End Property

    Public Shared PreviousQuoteIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PreviousQuoteID, "Previous Quote", Nothing)
    ''' <summary>
    ''' Gets and sets the Previous Quote value
    ''' </summary>
    <Display(Name:="Previous Quote", Description:="")>
    Public Property PreviousQuoteID() As Integer?
      Get
        Return GetProperty(PreviousQuoteIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PreviousQuoteIDProperty, Value)
      End Set
    End Property

    Public Shared VersionNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.VersionNo, "Version No", 1)
    ''' <summary>
    ''' Gets and sets the Version No value
    ''' </summary>
    <Display(Name:="Version No", Description:=""),
    Required(ErrorMessage:="Version No required")>
    Public Property VersionNo() As Integer
      Get
        Return GetProperty(VersionNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(VersionNoProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", OBLib.Security.Settings.CurrentUser.UserID)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared QuoteAmountProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.QuoteAmount, "Quote Amount", 0)
    ''' <summary>
    ''' Gets and sets the Quote Override Amount value
    ''' </summary>
    <Display(Name:="Quote Amount", Description:=""),
    NumberField(Formatting.NumberFormatType.CurrencyDecimals)>
    Public Property QuoteAmount() As Decimal
      Get
        Return GetProperty(QuoteAmountProperty)
      End Get
      Set(ByVal Value As Decimal)
        SetProperty(QuoteAmountProperty, Value)
      End Set
    End Property

    <Display(Name:="Override By")>
    Public ReadOnly Property QuoteOverrideByName As String
      Get
        If QuoteOverrideBy Is Nothing Then
          Return ""
        Else
          Dim usr As Maintenance.General.ReadOnly.ROUser = OBLib.CommonData.Lists.ROUserList.GetItem(QuoteOverrideBy)
          If usr IsNot Nothing Then
            Return usr.LoginName
          End If
          Return ""
        End If
      End Get
    End Property

    Public Shared EventTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.EventType, "")
    ''' <summary>
    ''' Gets and sets the Keyword value
    ''' </summary>
    <Display(Name:="EventType", Description:="")>
    Public Overridable Property EventType() As String
      Get
        Return ReadProperty(EventTypeProperty)
      End Get
      Set(ByVal Value As String)
        LoadProperty(EventTypeProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeEventTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionTypeEventType, "")
    ''' <summary>
    ''' Gets and sets the ProductionType value
    ''' </summary>
    <Display(Name:="Genre (Series)", Description:=""),
    Required(AllowEmptyStrings:=False)>
    Public Property ProductionTypeEventType() As String
      Get
        If GetProperty(ProductionTypeEventTypeProperty) = "" Then
          Return ProductionType & " (" & EventType & ")"
        End If
        Return GetProperty(ProductionTypeEventTypeProperty)
      End Get
      Set(value As String)
        SetProperty(ProductionTypeEventTypeProperty, value)
      End Set
    End Property

    Public Shared ProductionVenueProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionVenue, "")
    ''' <summary>
    ''' Gets and sets the ProductionVenue value
    ''' </summary>
    <Display(Name:="ProductionVenue", Description:=""),
    Required(AllowEmptyStrings:=False)>
    Public Overridable Property ProductionVenue() As String
      Get
        Return ReadProperty(ProductionVenueProperty)
      End Get
      Set(ByVal Value As String)
        LoadProperty(ProductionVenueProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.ProductionType, "")
    ''' <summary>
    ''' Gets and sets the ProductionType value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public Overridable Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTypeProperty, Value)
      End Set
    End Property

    Public Shared DebtorProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Debtor, "Debtor", "")
    ''' <summary>
    ''' Gets the Debtor value
    ''' </summary>
    <Display(Name:="Debtor", Description:=""),
    Required(AllowEmptyStrings:=False)>
    Public Property Debtor() As String
      Get
        Return GetProperty(DebtorProperty)
      End Get
      Set(value As String)
        SetProperty(DebtorProperty, value)
      End Set
    End Property

    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, OBLib.JSCode.QuoteJS.QuoteToString)

#End Region

#Region " Child Lists "

    Public Shared QuoteHeadingListProperty As PropertyInfo(Of QuoteHeadingList) = RegisterProperty(Of QuoteHeadingList)(Function(c) c.QuoteHeadingList, "Quote Heading List")

    Public ReadOnly Property QuoteHeadingList() As QuoteHeadingList
      Get
        If GetProperty(QuoteHeadingListProperty) Is Nothing Then
          LoadProperty(QuoteHeadingListProperty, Quoting.QuoteHeadingList.NewQuoteHeadingList())
        End If
        Return GetProperty(QuoteHeadingListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(QuoteIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.EventName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Quote")
        Else
          Return String.Format("Blank {0}", "Quote")
        End If
      Else
        Return Me.EventName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"QuoteHeadings"}
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

      With AddWebRule(QuoteOverrideByProperty)
        .ServerRuleFunction = AddressOf QuoteOverrideByValid
        .JavascriptRuleCode = OBLib.JSCode.QuoteJS.QuoteOverrideByValid
        .AddTriggerProperty(QuoteOverrideAmountProperty)
      End With

      With AddWebRule(QuoteOverrideDateTimeProperty)
        .ServerRuleFunction = AddressOf QuoteOverrideDateTimeValid
        .JavascriptRuleCode = OBLib.JSCode.QuoteJS.QuoteOverrideDateTimeValid
        .AddTriggerProperty(QuoteOverrideAmountProperty)
      End With

      With AddWebRule(QuoteOverrideReasonProperty)
        .ServerRuleFunction = AddressOf QuoteOverrideReasonValid
        .JavascriptRuleCode = OBLib.JSCode.QuoteJS.QuoteOverrideReasonValid
        .AddTriggerProperty(QuoteOverrideAmountProperty)
      End With

    End Sub

    Public Function QuoteOverrideByValid(Quote As Quote) As String
      If Not Singular.Misc.IsNullNothing(Quote.QuoteOverrideAmount, True) AndAlso Quote.QuoteOverrideBy Is Nothing Then
        Return "Quote Override By is required"
      End If
      Return ""
    End Function

    Public Function QuoteOverrideDateTimeValid(Quote As Quote) As String
      If Not Singular.Misc.IsNullNothing(Quote.QuoteOverrideAmount, True) AndAlso Quote.QuoteOverrideDateTime Is Nothing Then
        Return "Quote Override Date is required"
      End If
      Return ""
    End Function

    Public Function QuoteOverrideReasonValid(Quote As Quote) As String
      If Not Singular.Misc.IsNullNothing(Quote.QuoteOverrideAmount, True) AndAlso Quote.QuoteOverrideReason.Trim.Length = 0 Then
        Return "Quote Override Reason is required"
      End If
      Return ""
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewQuote() method.

    End Sub

    Public Shared Function NewQuote() As Quote

      Return DataPortal.CreateChild(Of Quote)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetQuote(dr As SafeDataReader) As Quote

      Dim q As New Quote()
      q.Fetch(dr)
      Return q

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(QuoteIDProperty, .GetInt32(0))
          LoadProperty(QuotedDateProperty, .GetValue(1))
          LoadProperty(EventNameProperty, .GetString(2))
          LoadProperty(ProductionTypeIDProperty, ZeroNothing(.GetInt32(3)))
          LoadProperty(EventTypeIDProperty, ZeroNothing(.GetInt32(4)))
          LoadProperty(VenueIDProperty, ZeroNothing(.GetInt32(5)))
          LoadProperty(EventDateProperty, .GetValue(6))
          LoadProperty(TechnicalSpecProperty, .GetString(7))
          LoadProperty(TxDaysProperty, .GetString(8))
          LoadProperty(InternalIndProperty, .GetBoolean(9))
          LoadProperty(ValidEndDateProperty, .GetValue(10))
          LoadProperty(DebtorIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(QuoteOverrideAmountProperty, .GetDecimal(12))
          LoadProperty(QuoteOverrideReasonProperty, .GetString(13))
          LoadProperty(QuoteOverrideDateTimeProperty, .GetValue(14))
          LoadProperty(QuoteOverrideByProperty, ZeroNothing(.GetInt32(15)))
          LoadProperty(QuoteAcceptedDateTimeProperty, .GetValue(16))
          LoadProperty(QuoteDeclinedDateTimeProperty, .GetValue(17))
          LoadProperty(PreviousQuoteIDProperty, ZeroNothing(.GetInt32(18)))
          LoadProperty(VersionNoProperty, .GetInt32(19))
          LoadProperty(CreatedByProperty, .GetInt32(20))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(21))
          LoadProperty(ModifiedByProperty, .GetInt32(22))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(23))
          LoadProperty(EventTypeProperty, .GetString(24))
          LoadProperty(ProductionVenueProperty, .GetString(25))
          LoadProperty(ProductionTypeProperty, .GetString(26))
          LoadProperty(DebtorProperty, .GetString(27))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insQuote"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updQuote"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramQuoteID As SqlParameter = .Parameters.Add("@QuoteID", SqlDbType.Int)
          paramQuoteID.Value = GetProperty(QuoteIDProperty)
          If Me.IsNew Then
            paramQuoteID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@QuotedDate", (New SmartDate(GetProperty(QuotedDateProperty))).DBValue)
          .Parameters.AddWithValue("@EventName", GetProperty(EventNameProperty))
          .Parameters.AddWithValue("@ProductionTypeID", NothingDBNull(GetProperty(ProductionTypeIDProperty)))
          .Parameters.AddWithValue("@EventTypeID", NothingDBNull(GetProperty(EventTypeIDProperty)))
          .Parameters.AddWithValue("@VenueID", NothingDBNull(GetProperty(VenueIDProperty)))
          .Parameters.AddWithValue("@EventDate", (New SmartDate(GetProperty(EventDateProperty))).DBValue)
          .Parameters.AddWithValue("@TechnicalSpec", GetProperty(TechnicalSpecProperty))
          .Parameters.AddWithValue("@TxDays", GetProperty(TxDaysProperty))
          .Parameters.AddWithValue("@InternalInd", GetProperty(InternalIndProperty))
          .Parameters.AddWithValue("@ValidEndDate", (New SmartDate(GetProperty(ValidEndDateProperty))).DBValue)
          .Parameters.AddWithValue("@DebtorID", NothingDBNull(Singular.Misc.NothingDBNull(GetProperty(DebtorIDProperty))))
          .Parameters.AddWithValue("@QuoteOverrideAmount", NothingDBNull(GetProperty(QuoteOverrideAmountProperty)))
          .Parameters.AddWithValue("@QuoteOverrideReason", GetProperty(QuoteOverrideReasonProperty))
          .Parameters.AddWithValue("@QuoteOverrideDateTime", (New SmartDate(GetProperty(QuoteOverrideDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@QuoteOverrideBy", NothingDBNull(GetProperty(QuoteOverrideByProperty)))
          .Parameters.AddWithValue("@QuoteAcceptedDateTime", (New SmartDate(GetProperty(QuoteAcceptedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@QuoteDeclinedDateTime", (New SmartDate(GetProperty(QuoteDeclinedDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@PreviousQuoteID", NothingDBNull(GetProperty(PreviousQuoteIDProperty)))
          .Parameters.AddWithValue("@VersionNo", GetProperty(VersionNoProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(QuoteIDProperty, paramQuoteID.Value)
          End If
          ' update child objects
          If GetProperty(QuoteHeadingListProperty) IsNot Nothing Then
            Me.QuoteHeadingList.Update()
          End If
          MarkOld()
        End With
      Else
        ' update child objects
        If GetProperty(QuoteHeadingListProperty) IsNot Nothing Then
          Me.QuoteHeadingList.Update()
        End If
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delQuote"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@QuoteID", GetProperty(QuoteIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace