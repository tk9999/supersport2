﻿' Generated 25 May 2017 06:20 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Resources

  <Serializable()> _
  Public Class ResourceBookingGenRefNumberList
    Inherits OBBusinessListBase(Of ResourceBookingGenRefNumberList, ResourceBookingGenRefNumber)

#Region " Business Methods "

    Public Function GetItem(GenRefNumber As Int64?) As ResourceBookingGenRefNumber

      For Each child As ResourceBookingGenRefNumber In Me
        If child.GenRefNumber = GenRefNumber Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Bookings"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ResourceID As Integer? = Nothing
      Public Property ResourceIDs As New List(Of Integer)
      Public Property ResourceBookingGenRefNumberID As Integer? = Nothing
      Public Property ResourceBookingGenRefNumberIDs As New List(Of Integer)
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing
      Public Property ResourceTypeIDs As New List(Of Integer)

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewResourceBookingGenRefNumberList() As ResourceBookingGenRefNumberList

      Return New ResourceBookingGenRefNumberList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetResourceBookingGenRefNumberList() As ResourceBookingGenRefNumberList

      Return DataPortal.Fetch(Of ResourceBookingGenRefNumberList)(New Criteria())

    End Function

    Public Shared Function GetResourceBookingGenRefNumberList(crit As Criteria) As ResourceBookingGenRefNumberList

      Return DataPortal.Fetch(Of ResourceBookingGenRefNumberList)(crit)

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ResourceBookingGenRefNumber.GetResourceBookingGenRefNumber(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      'Using cn As New SqlConnection(Singular.Settings.ConnectionString)
      '  cn.Open()
      '  Try
      '    Using cm As SqlCommand = cn.CreateCommand
      '      cm.CommandType = CommandType.StoredProcedure
      '      cm.CommandText = "GetProcsWeb.getResourceBookingGenRefNumberList"
      '      cm.Parameters.AddWithValue("@ResourceID", NothingDBNull(crit.ResourceID))
      '      cm.Parameters.AddWithValue("@ResourceIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ResourceIDs)))
      '      cm.Parameters.AddWithValue("@ResourceBookingGenRefNumberID", NothingDBNull(crit.ResourceBookingGenRefNumberID))
      '      cm.Parameters.AddWithValue("@ResourceBookingGenRefNumberIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ResourceBookingGenRefNumberIDs)))
      '      cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
      '      cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
      '      cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      '      cm.Parameters.AddWithValue("@ResourceTypeIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ResourceTypeIDs)))
      '      Using sdr As New SafeDataReader(cm.ExecuteReader)
      '        Fetch(sdr)
      '      End Using
      '    End Using
      '  Finally
      '    cn.Close()
      '  End Try
      'End Using

    End Sub

#End Region

  End Class

End Namespace