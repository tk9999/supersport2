﻿Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Resources

  Public Class ResourceBookingCleanList
    Inherits Resources.ResourceBookingBaseList(Of ResourceBookingCleanList, ResourceBookingClean)

    '<Serializable()> _
    'Public Class Criteria
    '  Inherits CriteriaBase(Of Criteria)

    '  Public Property ResourceBookingID As Integer? = Nothing
    '  Public Property ResourceIDs As String = ""
    '  Public Property StartDate As DateTime? = Nothing
    '  Public Property EndDate As DateTime? = Nothing
    '  Public Property ResourceBookingIDs As String = ""
    '  Public Property SystemID As Integer? = Nothing
    '  Public Property ProductionAreaID As Integer? = Nothing

    '  Public Sub New(ResourceIDs As String, StartDate As DateTime?, EndDate As DateTime?,
    '                 ResourceBookingIDs As String,
    '                 SystemID As Integer?, ProductionAreaID As Integer?)
    '    Me.ResourceIDs = ResourceIDs
    '    Me.StartDate = StartDate
    '    Me.EndDate = EndDate
    '    Me.ResourceBookingIDs = ResourceBookingIDs
    '    Me.SystemID = SystemID
    '    Me.ProductionAreaID = ProductionAreaID
    '  End Sub

    '  Public Sub New(ResourceBookingID As Integer?)
    '    Me.ResourceBookingID = ResourceBookingID
    '  End Sub

    '  Public Sub New()

    '  End Sub

    'End Class

    Public Shared Function GetResourceBookingCleanList(ResourceBookingID As Integer?,
                                                       SystemID As Integer?, ProductionAreaID As Integer?) As ResourceBookingCleanList

      Dim SelectedResourceIDs As New List(Of Integer)
      SelectedResourceIDs.Add(ResourceBookingID)
      Dim SelectedIDs As String = OBLib.OBMisc.IntegerListToXML(SelectedResourceIDs)
      Return DataPortal.Fetch(Of ResourceBookingCleanList)(New Criteria("", Nothing, Nothing, SelectedIDs,
                                                                        SystemID, ProductionAreaID))

    End Function

    Public Shared Function GetResourceBookingCleanList(ResourceIDs As String, StartDate As DateTime?, EndDate As DateTime?, ResourceBookingIDs As String,
                                                       SystemID As Integer?, ProductionAreaID As Integer?) As ResourceBookingCleanList

      Return DataPortal.Fetch(Of ResourceBookingCleanList)(New Criteria(ResourceIDs, StartDate, EndDate, ResourceBookingIDs, SystemID, ProductionAreaID))

    End Function

    Public Overrides Sub FetchChildLists(sdr As Csla.Data.SafeDataReader)

      Dim parentResourceBooking As ResourceBookingClean = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentResourceBooking Is Nothing OrElse parentResourceBooking.ResourceBookingID <> sdr.GetInt32(0) Then
            parentResourceBooking = Me.GetItem(sdr.GetInt32(0))
          End If
          parentResourceBooking.RSResourceBookingAreaList.RaiseListChangedEvents = False
          parentResourceBooking.RSResourceBookingAreaList.Add(RoomBookingArea.GetRoomBookingArea(sdr))
          parentResourceBooking.RSResourceBookingAreaList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    'Protected Overrides Sub Fetch(sdr As SafeDataReader)

    '  Me.RaiseListChangedEvents = False
    '  While sdr.Read
    '    Me.Add(ResourceBookingClean.GetResourceBookingClean(sdr))
    '  End While
    '  Me.RaiseListChangedEvents = True

    '  FetchChildLists(sdr)

    'End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "[GetProcsWeb].[getResourceBookingCleanList]"
            cm.Parameters.AddWithValue("@ResourceBookingID", NothingDBNull(crit.ResourceBookingID))
            cm.Parameters.AddWithValue("@ResourceIDs", Strings.MakeEmptyDBNull(crit.ResourceIDs))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@ResourceBookingIDs", Strings.MakeEmptyDBNull(crit.ResourceBookingIDs))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

  End Class

End Namespace