﻿' Generated 25 May 2017 06:20 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Resources

  <Serializable()>
  Public Class ResourceBooking
    Inherits OBBusinessBase(Of ResourceBooking)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ResourceBookingBO.ResourceBookingBOToString(self)")

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingTypeID, "Resource Booking Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type", Description:=""),
    Required(ErrorMessage:="Resource Booking Type required")>
    Public Property ResourceBookingTypeID() As Integer?
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Resource Booking Description", "")
    ''' <summary>
    ''' Gets and sets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Resource Booking Description", Description:=""),
    StringLength(500, ErrorMessage:="Resource Booking Description cannot be more than 500 characters")>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Date Time Buffer", Description:="")>
    Public Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:=""),
    Required(ErrorMessage:="Start Date Time required")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:=""),
    Required(ErrorMessage:="End Date Time required")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Date Time Buffer", Description:="")>
    Public Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared IsBeingEditedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsBeingEditedBy, "Is Being Edited By", "")
    ''' <summary>
    ''' Gets and sets the Is Being Edited By value
    ''' </summary>
    <Display(Name:="Is Being Edited By", Description:=""),
    StringLength(100, ErrorMessage:="Is Being Edited By cannot be more than 100 characters")>
    Public Property IsBeingEditedBy() As String
      Get
        Return GetProperty(IsBeingEditedByProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IsBeingEditedByProperty, Value)
      End Set
    End Property

    Public Shared InEditDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.InEditDateTime, "In Edit Date Time")
    ''' <summary>
    ''' Gets and sets the In Edit Date Time value
    ''' </summary>
    <Display(Name:="In Edit Date Time", Description:="")>
    Public Property InEditDateTime As DateTime?
      Get
        Return GetProperty(InEditDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(InEditDateTimeProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Human Resource Shift", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:="")>
    Public Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Is Cancelled value
    ''' </summary>
    <Display(Name:="Is Cancelled", Description:="")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceOffPeriodID, "Human Resource Off Period", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Off Period value
    ''' </summary>
    <Display(Name:="Human Resource Off Period", Description:="")>
    Public Property HumanResourceOffPeriodID() As Integer?
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceOffPeriodIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceSecondmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceSecondmentID, "Human Resource Secondment", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource Secondment value
    ''' </summary>
    <Display(Name:="Human Resource Secondment", Description:="")>
    Public Property HumanResourceSecondmentID() As Integer?
      Get
        Return GetProperty(HumanResourceSecondmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceSecondmentIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionHRID, "Production HR", Nothing)
    ''' <summary>
    ''' Gets and sets the Production HR value
    ''' </summary>
    <Display(Name:="Production HR", Description:="")>
    Public Property ProductionHRID() As Integer?
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionHRIDProperty, Value)
      End Set
    End Property

    Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "Css Class", "")
    ''' <summary>
    ''' Gets and sets the Status Css Class value
    ''' </summary>
    <Display(Name:="Css Class", Description:=""),
    StringLength(150, ErrorMessage:="Css Class cannot be more than 150 characters")>
    Public Property StatusCssClass() As String
      Get
        Return GetProperty(StatusCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCssClassProperty, Value)
      End Set
    End Property

    Public Shared IgnoreClashesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IgnoreClashes, "Ignore Clashes", False)
    ''' <summary>
    ''' Gets and sets the Ignore Clashes value
    ''' </summary>
    <Display(Name:="Ignore Clashes", Description:="")>
    Public Property IgnoreClashes() As Boolean
      Get
        Return GetProperty(IgnoreClashesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IgnoreClashesProperty, Value)
      End Set
    End Property

    Public Shared IgnoreClashesReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IgnoreClashesReason, "Ignore Clashes Reason", "")
    ''' <summary>
    ''' Gets and sets the Ignore Clashes Reason value
    ''' </summary>
    <Display(Name:="Ignore Clashes Reason", Description:=""),
    StringLength(500, ErrorMessage:="Ignore Clashes Reason cannot be more than 500 characters")>
    Public Property IgnoreClashesReason() As String
      Get
        Return GetProperty(IgnoreClashesReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IgnoreClashesReasonProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "Room Schedule", Nothing)
    ''' <summary>
    ''' Gets and sets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared IsLockedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsLocked, "Is Locked", False)
    ''' <summary>
    ''' Gets and sets the Is Locked value
    ''' </summary>
    <Display(Name:="Is Locked", Description:="")>
    Public Property IsLocked() As Boolean
      Get
        Return GetProperty(IsLockedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsLockedProperty, Value)
      End Set
    End Property

    Public Shared IsLockedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsLockedReason, "Is Locked Reason", "")
    ''' <summary>
    ''' Gets and sets the Is Locked Reason value
    ''' </summary>
    <Display(Name:="Is Locked Reason", Description:=""),
    StringLength(500, ErrorMessage:="Is Locked Reason cannot be more than 500 characters")>
    Public Property IsLockedReason() As String
      Get
        Return GetProperty(IsLockedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IsLockedReasonProperty, Value)
      End Set
    End Property

    Public Shared IsTBCProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTBC, "Is TBC", False)
    ''' <summary>
    ''' Gets and sets the Is TBC value
    ''' </summary>
    <Display(Name:="Is TBC", Description:="")>
    Public Property IsTBC() As Boolean
      Get
        Return GetProperty(IsTBCProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsTBCProperty, Value)
      End Set
    End Property

    Public Shared EquipmentScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentScheduleID, "Equipment Schedule", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment Schedule value
    ''' </summary>
    <Display(Name:="Equipment Schedule", Description:="")>
    Public Property EquipmentScheduleID() As Integer?
      Get
        Return GetProperty(EquipmentScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="Link to a specific ProductionSystemAreaID, current used only to link to PSA's for RoomSchedules, but can be expanded upon")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ParentResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentResourceBookingID, "Parent Resource Booking", Nothing)
    ''' <summary>
    ''' Gets and sets the Parent Resource Booking value
    ''' </summary>
    <Display(Name:="Parent Resource Booking", Description:="Link to a parent ResourceBooking, currently only used for RoomSchedules in order to created child bookings for each area linked to the RoomSchedule")>
    Public Property ParentResourceBookingID() As Integer?
      Get
        Return GetProperty(ParentResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ParentResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared PopoverContentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PopoverContent, "Popover Content", "")
    ''' <summary>
    ''' Gets and sets the Popover Content value
    ''' </summary>
    <Display(Name:="Popover Content", Description:=""),
    StringLength(1024, ErrorMessage:="Popover Content cannot be more than 1024 characters")>
    Public Property PopoverContent() As String
      Get
        Return GetProperty(PopoverContentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PopoverContentProperty, Value)
      End Set
    End Property

    Public Shared DisplayInBackgroundProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DisplayInBackground, "Display In Background", False)
    ''' <summary>
    ''' Gets and sets the Display In Background value
    ''' </summary>
    <Display(Name:="Display In Background", Description:="")>
    Public Property DisplayInBackground() As Boolean
      Get
        Return GetProperty(DisplayInBackgroundProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DisplayInBackgroundProperty, Value)
      End Set
    End Property

    Public Shared DisplayResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisplayResourceID, "Display Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Display Resource value
    ''' </summary>
    <Display(Name:="Display Resource", Description:="")>
    Public Property DisplayResourceID() As Integer?
      Get
        Return GetProperty(DisplayResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisplayResourceIDProperty, Value)
      End Set
    End Property

    Public Shared IsBeingEditedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IsBeingEditedByUserID, "Is Being Edited By User", Nothing)
    ''' <summary>
    ''' Gets and sets the Is Being Edited By User value
    ''' </summary>
    <Display(Name:="Is Being Edited By User", Description:="")>
    Public Property IsBeingEditedByUserID() As Integer?
      Get
        Return GetProperty(IsBeingEditedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(IsBeingEditedByUserIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingType, "ResourceBookingType")
    ''' <summary>
    ''' Gets and sets the Is Locked Reason value
    ''' </summary>
    <Display(Name:="ResourceBookingType", Description:="")>
    Public Property ResourceBookingType() As String
      Get
        Return GetProperty(ResourceBookingTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingTypeProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaStatusID, "ProductionAreaStatusID", Nothing)
    ''' <summary>
    ''' Gets and sets the Is Being Edited By User value
    ''' </summary>
    <Display(Name:="ProductionAreaStatusID", Description:="")>
    Public Property ProductionAreaStatusID() As Integer?
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared StatusNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusName, "StatusName")
    ''' <summary>
    ''' Gets and sets the Is Locked Reason value
    ''' </summary>
    <Display(Name:="StatusName", Description:="")>
    Public Property StatusName() As String
      Get
        Return GetProperty(StatusNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusNameProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNumber, "GenRefNumber", Nothing)
    ''' <summary>
    ''' Gets and sets the Is Being Edited By User value
    ''' </summary>
    <Display(Name:="GenRefNumber", Description:="")>
    Public Property GenRefNumber() As Int64?
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

    'I don't know if this is going to be required anymore
    'Public Shared ScheduleNumberProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ScheduleNumber, "ScheduleNumber", Nothing)
    ' ''' <summary>
    ' ''' Gets and sets the Is Being Edited By User value
    ' ''' </summary>
    '<Display(Name:="ScheduleNumber", Description:="")>
    'Public Property ScheduleNumber() As Integer?
    '  Get
    '    Return GetProperty(ScheduleNumberProperty)
    '  End Get
    '  Set(ByVal Value As Integer?)
    '    SetProperty(ScheduleNumberProperty, Value)
    '  End Set
    'End Property

    Public Shared ResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceName, "Resource")
    ''' <summary>
    ''' Gets and sets the Is Locked Reason value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public Property ResourceName() As String
      Get
        Return GetProperty(ResourceNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceNameProperty, Value)
      End Set
    End Property

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceTypeID, "ResourceTypeID", Nothing)
    ''' <summary>
    ''' Gets and sets the Is Being Edited By User value
    ''' </summary>
    <Display(Name:="ResourceTypeID", Description:="")>
    Public Property ResourceTypeID() As Integer?
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceTypeIDProperty, Value)
      End Set
    End Property

    Public Shared StartTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    Required(ErrorMessage:="Start Time required")>
    Public Property StartTime As DateTime?
      Get
        Return GetProperty(StartTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartTimeProperty, Value)
      End Set
    End Property

    Public Shared EndTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndTime, Nothing)
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    Required(ErrorMessage:="End Time required")>
    Public Property EndTime As DateTime?
      Get
        Return GetProperty(EndTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndTimeProperty, Value)
      End Set
    End Property

    Public Shared HasAreaAccessProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasAreaAccess, "Has Area Access", False)
    ''' <summary>
    ''' Gets and sets the Is Locked value
    ''' </summary>
    <Display(Name:="Has Area Access", Description:="")>
    Public Property HasAreaAccess() As Boolean
      Get
        Return GetProperty(HasAreaAccessProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasAreaAccessProperty, Value)
      End Set
    End Property

    Public Shared AreaNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.AreaName, "Area Name", "")
    ''' <summary>
    ''' Gets and sets the Is Locked Reason value
    ''' </summary>
    <Display(Name:="Area Name", Description:="")>
    Public Property AreaName() As String
      Get
        Return GetProperty(AreaNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(AreaNameProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "SystemID", Nothing)
    ''' <summary>
    ''' Gets and sets the Is Being Edited By User value
    ''' </summary>
    <Display(Name:="SystemID", Description:="")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "ProductionAreaID", Nothing)
    ''' <summary>
    ''' Gets and sets the Is Being Edited By User value
    ''' </summary>
    <Display(Name:="ProductionAreaID", Description:="")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared ResourceBookingGenRefNumberListProperty As PropertyInfo(Of ResourceBookingGenRefNumberList) = RegisterProperty(Of ResourceBookingGenRefNumberList)(Function(c) c.ResourceBookingGenRefNumberList, "ResourceBookingGenRefNumberList")
    Public ReadOnly Property ResourceBookingGenRefNumberList() As ResourceBookingGenRefNumberList
      Get
        If GetProperty(ResourceBookingGenRefNumberListProperty) Is Nothing Then
          LoadProperty(ResourceBookingGenRefNumberListProperty, Resources.ResourceBookingGenRefNumberList.NewResourceBookingGenRefNumberList())
        End If
        Return GetProperty(ResourceBookingGenRefNumberListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceBookingDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Resource Booking")
        Else
          Return String.Format("Blank {0}", "Resource Booking")
        End If
      Else
        Return Me.ResourceBookingDescription
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceBooking() method.

    End Sub

    Public Shared Function NewResourceBooking() As ResourceBooking

      Return DataPortal.CreateChild(Of ResourceBooking)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetResourceBooking(dr As SafeDataReader) As ResourceBooking

      Dim r As New ResourceBooking()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceBookingIDProperty, .GetInt32(0))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ResourceBookingDescriptionProperty, .GetString(3))
          LoadProperty(StartDateTimeBufferProperty, .GetValue(4))
          LoadProperty(StartDateTimeProperty, .GetValue(5))
          LoadProperty(EndDateTimeProperty, .GetValue(6))
          LoadProperty(EndDateTimeBufferProperty, .GetValue(7))
          LoadProperty(IsBeingEditedByProperty, .GetString(8))
          LoadProperty(InEditDateTimeProperty, .GetValue(9))
          LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(IsCancelledProperty, .GetBoolean(11))
          LoadProperty(HumanResourceOffPeriodIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(HumanResourceSecondmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(ProductionHRIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(StatusCssClassProperty, .GetString(15))
          LoadProperty(IgnoreClashesProperty, .GetBoolean(16))
          LoadProperty(IgnoreClashesReasonProperty, .GetString(17))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
          LoadProperty(IsLockedProperty, .GetBoolean(19))
          LoadProperty(IsLockedReasonProperty, .GetString(20))
          LoadProperty(IsTBCProperty, .GetBoolean(21))
          LoadProperty(EquipmentScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
          LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
          LoadProperty(ParentResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
          LoadProperty(PopoverContentProperty, .GetString(25))
          LoadProperty(DisplayInBackgroundProperty, .GetBoolean(26))
          LoadProperty(DisplayResourceIDProperty, .GetInt32(27))
          LoadProperty(IsBeingEditedByUserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(28)))
          LoadProperty(ResourceBookingTypeProperty, .GetString(29))
          LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(30)))
          LoadProperty(StatusNameProperty, .GetString(31))
          LoadProperty(GenRefNumberProperty, ZeroNothing(.GetInt64(32)))
          'LoadProperty(ScheduleNumberProperty, .GetInt32(33))
          LoadProperty(ResourceNameProperty, .GetString(34))
          LoadProperty(ResourceTypeIDProperty, ZeroNothing(.GetInt32(35)))
          LoadProperty(StartTimeProperty, .GetValue(36))
          LoadProperty(EndTimeProperty, .GetValue(37))
          LoadProperty(HasAreaAccessProperty, .GetBoolean(38))
          LoadProperty(AreaNameProperty, .GetString(39))
          LoadProperty(SystemIDProperty, ZeroNothing(.GetInt32(40)))
          LoadProperty(ProductionAreaIDProperty, ZeroNothing(.GetInt32(41)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ResourceBookingIDProperty)

      cm.Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
      cm.Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
      cm.Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
      cm.Parameters.AddWithValue("@StartDateTimeBuffer", Singular.Misc.NothingDBNull(StartDateTimeBuffer))
      cm.Parameters.AddWithValue("@StartDateTime", StartDateTime)
      cm.Parameters.AddWithValue("@EndDateTime", EndDateTime)
      cm.Parameters.AddWithValue("@EndDateTimeBuffer", Singular.Misc.NothingDBNull(EndDateTimeBuffer))
      cm.Parameters.AddWithValue("@IsBeingEditedBy", GetProperty(IsBeingEditedByProperty))
      cm.Parameters.AddWithValue("@InEditDateTime", Singular.Misc.NothingDBNull(InEditDateTime))
      cm.Parameters.AddWithValue("@HumanResourceShiftID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceShiftIDProperty)))
      cm.Parameters.AddWithValue("@IsCancelled", GetProperty(IsCancelledProperty))
      cm.Parameters.AddWithValue("@HumanResourceOffPeriodID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceOffPeriodIDProperty)))
      cm.Parameters.AddWithValue("@HumanResourceSecondmentID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceSecondmentIDProperty)))
      cm.Parameters.AddWithValue("@ProductionHRID", Singular.Misc.NothingDBNull(GetProperty(ProductionHRIDProperty)))
      cm.Parameters.AddWithValue("@StatusCssClass", GetProperty(StatusCssClassProperty))
      cm.Parameters.AddWithValue("@IgnoreClashes", GetProperty(IgnoreClashesProperty))
      cm.Parameters.AddWithValue("@IgnoreClashesReason", GetProperty(IgnoreClashesReasonProperty))
      cm.Parameters.AddWithValue("@RoomScheduleID", Singular.Misc.NothingDBNull(GetProperty(RoomScheduleIDProperty)))
      cm.Parameters.AddWithValue("@IsLocked", GetProperty(IsLockedProperty))
      cm.Parameters.AddWithValue("@IsLockedReason", GetProperty(IsLockedReasonProperty))
      cm.Parameters.AddWithValue("@IsTBC", GetProperty(IsTBCProperty))
      cm.Parameters.AddWithValue("@EquipmentScheduleID", Singular.Misc.NothingDBNull(GetProperty(EquipmentScheduleIDProperty)))
      cm.Parameters.AddWithValue("@ProductionSystemAreaID", Singular.Misc.NothingDBNull(GetProperty(ProductionSystemAreaIDProperty)))
      cm.Parameters.AddWithValue("@ParentResourceBookingID", Singular.Misc.NothingDBNull(GetProperty(ParentResourceBookingIDProperty)))
      cm.Parameters.AddWithValue("@PopoverContent", GetProperty(PopoverContentProperty))
      cm.Parameters.AddWithValue("@DisplayInBackground", GetProperty(DisplayInBackgroundProperty))
      cm.Parameters.AddWithValue("@DisplayResourceID", GetProperty(DisplayResourceIDProperty))
      cm.Parameters.AddWithValue("@IsBeingEditedByUserID", Singular.Misc.NothingDBNull(GetProperty(IsBeingEditedByUserIDProperty)))
      cm.Parameters.AddWithValue("@ResourceBookingType", GetProperty(ResourceBookingTypeProperty))
      cm.Parameters.AddWithValue("@ProductionAreaStatusID", Singular.Misc.NothingDBNull(GetProperty(ProductionAreaStatusIDProperty)))
      cm.Parameters.AddWithValue("@StatusName", GetProperty(StatusNameProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ResourceBookingIDProperty, cm.Parameters("@ResourceBookingID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
    End Sub

#End Region

  End Class

End Namespace