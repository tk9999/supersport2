﻿' Generated 20 Jul 2016 07:23 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Resources

  <Serializable()> _
  Public Class CustomResource
    Inherits OBBusinessBase(Of CustomResource)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return CustomResourceBO.CustomResourceBOToString(self)")

    Public Shared CustomResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CustomResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property CustomResourceID() As Integer
      Get
        Return GetProperty(CustomResourceIDProperty)
      End Get
    End Property

    Public Shared CustomResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CustomResource, "Custom Resource", "")
    ''' <summary>
    ''' Gets and sets the Custom Resource value
    ''' </summary>
    <Display(Name:="Custom Resource", Description:=""),
    StringLength(50, ErrorMessage:="Custom Resource cannot be more than 50 characters")>
  Public Property CustomResource() As String
      Get
        Return GetProperty(CustomResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(CustomResourceProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CustomResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CustomResource.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Custom Resource")
        Else
          Return String.Format("Blank {0}", "Custom Resource")
        End If
      Else
        Return Me.CustomResource
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewCustomResource() method.

    End Sub

    Public Shared Function NewCustomResource() As CustomResource

      Return DataPortal.CreateChild(Of CustomResource)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetCustomResource(dr As SafeDataReader) As CustomResource

      Dim c As New CustomResource()
      c.Fetch(dr)
      Return c

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(CustomResourceIDProperty, .GetInt32(0))
          LoadProperty(CustomResourceProperty, .GetString(1))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, CustomResourceIDProperty)

      cm.Parameters.AddWithValue("@CustomResource", GetProperty(CustomResourceProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(CustomResourceIDProperty, cm.Parameters("@CustomResourceID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@CustomResourceID", GetProperty(CustomResourceIDProperty))
    End Sub

#End Region

  End Class

End Namespace