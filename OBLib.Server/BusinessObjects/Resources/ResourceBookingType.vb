﻿' Generated 13 Jun 2015 16:27 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Maintenance.Resources

  <Serializable()> _
  Public Class ResourceBookingType
    Inherits SingularBusinessBase(Of ResourceBookingType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ResourceBookingTypeID() As Integer
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
    End Property

    Public Shared ResourceBookingTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingType, "Resource Booking Type", "")
    ''' <summary>
    ''' Gets and sets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type", Description:=""),
    StringLength(50, ErrorMessage:="Resource Booking Type cannot be more than 50 characters")>
    Public Property ResourceBookingType() As String
      Get
        Return GetProperty(ResourceBookingTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingTypeProperty, Value)
      End Set
    End Property

    Public Shared IsSharedTypeProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsSharedType, "Is Shared Type", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Shared Type")>
    Public Property IsSharedType() As Boolean
      Get
        Return GetProperty(IsSharedTypeProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsSharedTypeProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceBookingType.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Resource Booking Type")
        Else
          Return String.Format("Blank {0}", "Resource Booking Type")
        End If
      Else
        Return Me.ResourceBookingType
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceBookingType() method.

    End Sub

    Public Shared Function NewResourceBookingType() As ResourceBookingType

      Return DataPortal.CreateChild(Of ResourceBookingType)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetResourceBookingType(dr As SafeDataReader) As ResourceBookingType

      Dim r As New ResourceBookingType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceBookingTypeIDProperty, .GetInt32(0))
          LoadProperty(ResourceBookingTypeProperty, .GetString(1))
          LoadProperty(IsSharedTypeProperty, .GetBoolean(3))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insResourceBookingType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updResourceBookingType"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramResourceBookingTypeID As SqlParameter = .Parameters.Add("@ResourceBookingTypeID", SqlDbType.Int)
          paramResourceBookingTypeID.Value = GetProperty(ResourceBookingTypeIDProperty)
          If Me.IsNew Then
            paramResourceBookingTypeID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ResourceBookingType", GetProperty(ResourceBookingTypeProperty))
          .Parameters.AddWithValue("@IsSharedType", GetProperty(IsSharedTypeProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ResourceBookingTypeIDProperty, paramResourceBookingTypeID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delResourceBookingType"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace