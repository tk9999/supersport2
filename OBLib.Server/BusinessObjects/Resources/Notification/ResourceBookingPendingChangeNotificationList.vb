﻿' Generated 18 Aug 2015 10:49 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources

  <Serializable()>
  Public Class ResourceBookingPendingChangeNotificationList
    Inherits SingularBusinessListBase(Of ResourceBookingPendingChangeNotificationList, ResourceBookingPendingChangeNotification)

#Region " Business Methods "

    Public Function GetItem(ResourceBookingPendingChangeNotificationID As Integer) As ResourceBookingPendingChangeNotification

      For Each child As ResourceBookingPendingChangeNotification In Me
        If child.ResourceBookingPendingChangeNotificationID = ResourceBookingPendingChangeNotificationID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Booking Pending Change Notifications"

    End Function

#End Region

#Region " Data Access "

    <Serializable()>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)
      Public Property Clear As Boolean
      Public Sub New(Clear As Boolean)
        Me.Clear = Clear
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewResourceBookingPendingChangeNotificationList() As ResourceBookingPendingChangeNotificationList

      Return New ResourceBookingPendingChangeNotificationList()

    End Function

    Public Shared Sub BeginGetResourceBookingPendingChangeNotificationList(CallBack As EventHandler(Of DataPortalResult(Of ResourceBookingPendingChangeNotificationList)))

      Dim dp As New DataPortal(Of ResourceBookingPendingChangeNotificationList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria(True))

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetResourceBookingPendingChangeNotificationList(Clear As Boolean) As ResourceBookingPendingChangeNotificationList

      Return DataPortal.Fetch(Of ResourceBookingPendingChangeNotificationList)(New Criteria(Clear))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ResourceBookingPendingChangeNotification.GetResourceBookingPendingChangeNotification(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getResourceBookingPendingChangeNotificationList"
            cm.Parameters.AddWithValue("@Clear", crit.Clear)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ResourceBookingPendingChangeNotification In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ResourceBookingPendingChangeNotification In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace