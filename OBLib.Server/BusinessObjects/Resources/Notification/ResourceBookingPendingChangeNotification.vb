﻿' Generated 18 Aug 2015 10:49 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources

  <Serializable()> _
  Public Class ResourceBookingPendingChangeNotification
    Inherits SingularBusinessBase(Of ResourceBookingPendingChangeNotification)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceBookingPendingChangeNotificationIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingPendingChangeNotificationID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property ResourceBookingPendingChangeNotificationID() As Integer
      Get
        Return GetProperty(ResourceBookingPendingChangeNotificationIDProperty)
      End Get
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "Resource Booking", 0)
    ''' <summary>
    ''' Gets and sets the Resource Booking value
    ''' </summary>
    <Display(Name:="Resource Booking", Description:=""),
    Required(ErrorMessage:="Resource Booking required")>
  Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared ActionTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ActionTypeID, "Action Type", 0)
    ''' <summary>
    ''' Gets and sets the Action Type value
    ''' </summary>
    <Display(Name:="Action Type", Description:="1= Insert, 2=Update, 3=Delete"),
    Required(ErrorMessage:="Action Type required")>
  Public Property ActionTypeID() As Integer
      Get
        Return GetProperty(ActionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ActionTypeIDProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingPendingChangeNotificationIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceBookingPendingChangeNotificationID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Resource Booking Pending Change Notification")
        Else
          Return String.Format("Blank {0}", "Resource Booking Pending Change Notification")
        End If
      Else
        Return Me.ResourceBookingPendingChangeNotificationID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceBookingPendingChangeNotification() method.

    End Sub

    Public Shared Function NewResourceBookingPendingChangeNotification() As ResourceBookingPendingChangeNotification

      Return DataPortal.CreateChild(Of ResourceBookingPendingChangeNotification)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetResourceBookingPendingChangeNotification(dr As SafeDataReader) As ResourceBookingPendingChangeNotification

      Dim r As New ResourceBookingPendingChangeNotification()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceBookingPendingChangeNotificationIDProperty, .GetInt32(0))
          LoadProperty(ResourceBookingIDProperty, .GetInt32(1))
          LoadProperty(ActionTypeIDProperty, .GetInt32(2))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insResourceBookingPendingChangeNotification"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updResourceBookingPendingChangeNotification"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramResourceBookingPendingChangeNotificationID As SqlParameter = .Parameters.Add("@ResourceBookingPendingChangeNotificationID", SqlDbType.Int)
          paramResourceBookingPendingChangeNotificationID.Value = GetProperty(ResourceBookingPendingChangeNotificationIDProperty)
          If Me.IsNew Then
            paramResourceBookingPendingChangeNotificationID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
          .Parameters.AddWithValue("@ActionTypeID", GetProperty(ActionTypeIDProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ResourceBookingPendingChangeNotificationIDProperty, paramResourceBookingPendingChangeNotificationID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delResourceBookingPendingChangeNotification"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ResourceBookingPendingChangeNotificationID", GetProperty(ResourceBookingPendingChangeNotificationIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace