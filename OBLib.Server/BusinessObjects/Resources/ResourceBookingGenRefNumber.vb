﻿' Generated 25 May 2017 06:20 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Resources

  <Serializable()> _
  Public Class ResourceBookingGenRefNumber
    Inherits OBBusinessBase(Of ResourceBookingGenRefNumber)

#Region " Properties and Methods "

#Region " Properties "

    'Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ResourceBookingGenRefNumberBO.ResourceBookingGenRefNumberBOToString(self)")

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared GenRefNumberProperty As PropertyInfo(Of Int64?) = RegisterProperty(Of Int64?)(Function(c) c.GenRefNumber, "GenRefNumber", Nothing)
    ''' <summary>
    ''' Gets and sets the Is Being Edited By User value
    ''' </summary>
    <Display(Name:="GenRefNumber", Description:="")>
    Public Property GenRefNumber() As Int64?
      Get
        Return GetProperty(GenRefNumberProperty)
      End Get
      Set(ByVal Value As Int64?)
        SetProperty(GenRefNumberProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return ""

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceBookingGenRefNumber() method.

    End Sub

    Public Shared Function NewResourceBookingGenRefNumber() As ResourceBookingGenRefNumber

      Return DataPortal.CreateChild(Of ResourceBookingGenRefNumber)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetResourceBookingGenRefNumber(dr As SafeDataReader) As ResourceBookingGenRefNumber

      Dim r As New ResourceBookingGenRefNumber()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceBookingIDProperty, .GetInt32(0))
          LoadProperty(GenRefNumberProperty, ZeroNothing(.GetInt64(1)))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, ResourceBookingIDProperty)

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(ResourceBookingIDProperty, cm.Parameters("@ResourceBookingID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
    End Sub

#End Region

  End Class

End Namespace