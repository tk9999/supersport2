﻿' Generated 20 Jul 2016 07:24 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Resources.ReadOnly

  <Serializable()> _
  Public Class ROCustomResource
    Inherits OBReadOnlyBase(Of ROCustomResource)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROCustomResourceBO.ROCustomResourceBOToString(self)")

    Public Shared CustomResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CustomResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property CustomResourceID() As Integer
      Get
        Return GetProperty(CustomResourceIDProperty)
      End Get
    End Property

    Public Shared CustomResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.CustomResource, "Custom Resource", "")
    ''' <summary>
    ''' Gets the Custom Resource value
    ''' </summary>
    <Display(Name:="Custom Resource", Description:="")>
    Public ReadOnly Property CustomResource() As String
      Get
        Return GetProperty(CustomResourceProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(CustomResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.CustomResource

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROCustomResource(dr As SafeDataReader) As ROCustomResource

      Dim r As New ROCustomResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(CustomResourceIDProperty, .GetInt32(0))
        LoadProperty(CustomResourceProperty, .GetString(1))
      End With

    End Sub

#End Region

  End Class

End Namespace