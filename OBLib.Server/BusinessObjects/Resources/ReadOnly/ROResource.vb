﻿' Generated 30 Apr 2015 10:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Resources.ReadOnly

  <Serializable()> _
  Public Class ROResource
    Inherits OBReadOnlyBase(Of ROResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared SelectedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Selected, "Selected?", False)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Selected?")>
    Public ReadOnly Property Selected() As Boolean
      Get
        Return GetProperty(SelectedProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared ResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceName, "Resource Name", "")
    ''' <summary>
    ''' Gets the Resource Name value
    ''' </summary>
    <Display(Name:="Resource Name", Description:="")>
    Public ReadOnly Property ResourceName() As String
      Get
        Return GetProperty(ResourceNameProperty)
      End Get
    End Property

    Public Shared ResourceTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceTypeID, "Resource Type", Nothing)
    ''' <summary>
    ''' Gets the Resource Type value
    ''' </summary>
    <Display(Name:="Resource Type", Description:="")>
    Public ReadOnly Property ResourceTypeID() As Integer?
      Get
        Return GetProperty(ResourceTypeIDProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public ReadOnly Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
    Public ReadOnly Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentID, "Equipment", Nothing)
    ''' <summary>
    ''' Gets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:="")>
    Public ReadOnly Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ChannelID, "Channel", Nothing)
    ''' <summary>
    ''' Gets the Channel value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public ReadOnly Property ChannelID() As Integer?
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared TypeOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.TypeOrder, "RowNo", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    Public ReadOnly Property TypeOrder() As Integer?
      Get
        Return GetProperty(TypeOrderProperty)
      End Get
    End Property

    Public Shared ResourceOrderProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceOrder, "RowNo", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    Public ReadOnly Property ResourceOrder() As Integer?
      Get
        Return GetProperty(ResourceOrderProperty)
      End Get
    End Property

    Public Shared CombinedOrderProperty As PropertyInfo(Of Decimal) = RegisterProperty(Of Decimal)(Function(c) c.CombinedOrder, "RowNo", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    Public ReadOnly Property CombinedOrder() As Decimal
      Get
        Return GetProperty(CombinedOrderProperty)
      End Get
    End Property

    Public Shared RowNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.RowNo, "RowNo", 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    Public ReadOnly Property RowNo() As Integer?
      Get
        Return GetProperty(RowNoProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROResource(dr As SafeDataReader) As ROResource

      Dim r As New ROResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ResourceIDProperty, .GetInt32(0))
        LoadProperty(ResourceNameProperty, .GetString(1))
        LoadProperty(ResourceTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
        LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
        LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
        LoadProperty(TypeOrderProperty, .GetInt32(12))
        LoadProperty(ResourceOrderProperty, .GetInt32(13))
        LoadProperty(CombinedOrderProperty, .GetDecimal(14))
        LoadProperty(RowNoProperty, .GetInt32(15))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace