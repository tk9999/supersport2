﻿' Generated 11 May 2016 09:12 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Resources.ReadOnly

  <Serializable()> _
  Public Class ROResourceBookingList
    Inherits OBReadOnlyListBase(Of ROResourceBookingList, ROResourceBooking)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "


    ' ** Example Paging property declaration code to be implemented in the necessary VM **
    'Public Property ROResourceBookingList As ROResourceBookingList
    'Public Property ROResourceBookingListCriteria As ROResourceBookingList.Criteria
    'Public Property ROResourceBookingListManager As Singular.Web.Data.PagedDataManager(Of VMType)

    ' ** Example Paging definition code to be implemented in the necessary VM Setup Method **
    'ROResourceBookingList = New ROResourceBookingList
    'ROResourceBookingListCriteria = New ROResourceBookingList.Criteria
    'ROResourceBookingListManager = New Singular.Web.Data.PagedDataManager(Of VMType)(Function(d) d.ROResourceBookingList, Function(d) d.ROResourceBookingListCriteria, "SortBy", PageSize, SortAsc)

    Public Function GetItem(ResourceBookingID As Integer) As ROResourceBooking

      For Each child As ROResourceBooking In Me
        If child.ResourceBookingID = ResourceBookingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO Resource Bookings"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property ResourceID As Object = Nothing
      Public Property ResourceIDs As String = ""
      Public Property ResourceBookingID As Integer? = Nothing
      Public Property ResourceBookingIDs As String = ""
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing
      Public Property ExcludeResourceBookingID As Integer? = Nothing
      Public Property ExcludeResourceBookingIDs As String = ""

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROResourceBookingList() As ROResourceBookingList

      Return New ROResourceBookingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROResourceBookingList() As ROResourceBookingList

      Return DataPortal.Fetch(Of ROResourceBookingList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROResourceBooking.GetROResourceBooking(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROResourceBookingList"
            cm.Parameters.AddWithValue("@ResourceID", Singular.Misc.NothingDBNull(crit.ResourceID))
            cm.Parameters.AddWithValue("@ResourceIDs", Singular.Strings.MakeEmptyDBNull(crit.ResourceIDs))
            cm.Parameters.AddWithValue("@ResourceBookingID", Singular.Misc.NothingDBNull(crit.ResourceBookingID))
            cm.Parameters.AddWithValue("@ResourceBookingIDs", Singular.Strings.MakeEmptyDBNull(crit.ResourceBookingIDs))
            cm.Parameters.AddWithValue("@StartDate", Singular.Misc.NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", Singular.Misc.NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            cm.Parameters.AddWithValue("@ExcludeResourceBookingID", Singular.Misc.NothingDBNull(crit.ResourceBookingID))
            cm.Parameters.AddWithValue("@ExcludeResourceBookingIDs", Singular.Strings.MakeEmptyDBNull(crit.ResourceBookingIDs))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace