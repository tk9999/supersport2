﻿' Generated 11 May 2016 09:12 - Singular Systems Object Generator Version 2.2.682
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations

Namespace Resources.ReadOnly

  <Serializable()> _
  Public Class ROResourceBooking
    Inherits OBReadOnlyBase(Of ROResourceBooking)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROResourceBookingBO.ROResourceBookingBOToString(self)")

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource", Nothing)
    ''' <summary>
    ''' Gets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:="")>
    Public ReadOnly Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
    End Property

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingTypeID, "Resource Booking Type", Nothing)
    ''' <summary>
    ''' Gets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type", Description:="")>
    Public ReadOnly Property ResourceBookingTypeID() As Integer?
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Resource Booking Description", "")
    ''' <summary>
    ''' Gets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Resource Booking Description", Description:="")>
    Public ReadOnly Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Start Date Time Buffer")
    ''' <summary>
    ''' Gets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Date Time Buffer", Description:="")>
    Public ReadOnly Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public ReadOnly Property StartDateTime As Date
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
    Public ReadOnly Property EndDateTime As Date
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "End Date Time Buffer")
    ''' <summary>
    ''' Gets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Date Time Buffer", Description:="")>
    Public ReadOnly Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
    End Property

    Public Shared IsBeingEditedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsBeingEditedBy, "Is Being Edited By", "")
    ''' <summary>
    ''' Gets the Is Being Edited By value
    ''' </summary>
    <Display(Name:="Is Being Edited By", Description:="")>
    Public ReadOnly Property IsBeingEditedBy() As String
      Get
        Return GetProperty(IsBeingEditedByProperty)
      End Get
    End Property

    Public Shared InEditDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.InEditDateTime, "In Edit Date Time")
    ''' <summary>
    ''' Gets the In Edit Date Time value
    ''' </summary>
    <Display(Name:="In Edit Date Time", Description:="")>
    Public ReadOnly Property InEditDateTime As DateTime?
      Get
        Return GetProperty(InEditDateTimeProperty)
      End Get
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "Human Resource Shift", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Shift value
    ''' </summary>
    <Display(Name:="Human Resource Shift", Description:="")>
    Public ReadOnly Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Is Cancelled", False)
    ''' <summary>
    ''' Gets the Is Cancelled value
    ''' </summary>
    <Display(Name:="Is Cancelled", Description:="")>
    Public ReadOnly Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
    End Property

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceOffPeriodID, "Human Resource Off Period", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Off Period value
    ''' </summary>
    <Display(Name:="Human Resource Off Period", Description:="")>
    Public ReadOnly Property HumanResourceOffPeriodID() As Integer?
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
    End Property

    Public Shared HumanResourceSecondmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceSecondmentID, "Human Resource Secondment", Nothing)
    ''' <summary>
    ''' Gets the Human Resource Secondment value
    ''' </summary>
    <Display(Name:="Human Resource Secondment", Description:="")>
    Public ReadOnly Property HumanResourceSecondmentID() As Integer?
      Get
        Return GetProperty(HumanResourceSecondmentIDProperty)
      End Get
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionHRID, "Production HR", Nothing)
    ''' <summary>
    ''' Gets the Production HR value
    ''' </summary>
    <Display(Name:="Production HR", Description:="")>
    Public ReadOnly Property ProductionHRID() As Integer?
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
    End Property

    Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "Status Css Class", "")
    ''' <summary>
    ''' Gets the Status Css Class value
    ''' </summary>
    <Display(Name:="Status Css Class", Description:="")>
    Public ReadOnly Property StatusCssClass() As String
      Get
        Return GetProperty(StatusCssClassProperty)
      End Get
    End Property

    Public Shared IgnoreClashesProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IgnoreClashes, "Ignore Clashes", False)
    ''' <summary>
    ''' Gets the Ignore Clashes value
    ''' </summary>
    <Display(Name:="Ignore Clashes", Description:="")>
    Public ReadOnly Property IgnoreClashes() As Boolean
      Get
        Return GetProperty(IgnoreClashesProperty)
      End Get
    End Property

    Public Shared IgnoreClashesReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IgnoreClashesReason, "Ignore Clashes Reason", "")
    ''' <summary>
    ''' Gets the Ignore Clashes Reason value
    ''' </summary>
    <Display(Name:="Ignore Clashes Reason", Description:="")>
    Public ReadOnly Property IgnoreClashesReason() As String
      Get
        Return GetProperty(IgnoreClashesReasonProperty)
      End Get
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "Room Schedule", Nothing)
    ''' <summary>
    ''' Gets the Room Schedule value
    ''' </summary>
    <Display(Name:="Room Schedule", Description:="")>
    Public ReadOnly Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
    End Property

    Public Shared IsLockedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsLocked, "Is Locked", False)
    ''' <summary>
    ''' Gets the Is Locked value
    ''' </summary>
    <Display(Name:="Is Locked", Description:="")>
    Public ReadOnly Property IsLocked() As Boolean
      Get
        Return GetProperty(IsLockedProperty)
      End Get
    End Property

    Public Shared IsLockedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsLockedReason, "Is Locked Reason", "")
    ''' <summary>
    ''' Gets the Is Locked Reason value
    ''' </summary>
    <Display(Name:="Is Locked Reason", Description:="")>
    Public ReadOnly Property IsLockedReason() As String
      Get
        Return GetProperty(IsLockedReasonProperty)
      End Get
    End Property

    Public Shared IsTBCProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsTBC, "Is TBC", False)
    ''' <summary>
    ''' Gets the Is TBC value
    ''' </summary>
    <Display(Name:="Is TBC", Description:="")>
    Public ReadOnly Property IsTBC() As Boolean
      Get
        Return GetProperty(IsTBCProperty)
      End Get
    End Property

    Public Shared EquipmentScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentScheduleID, "Equipment Schedule", Nothing)
    ''' <summary>
    ''' Gets the Equipment Schedule value
    ''' </summary>
    <Display(Name:="Equipment Schedule", Description:="")>
    Public ReadOnly Property EquipmentScheduleID() As Integer?
      Get
        Return GetProperty(EquipmentScheduleIDProperty)
      End Get
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "Production System Area", Nothing)
    ''' <summary>
    ''' Gets the Production System Area value
    ''' </summary>
    <Display(Name:="Production System Area", Description:="Link to a specific ProductionSystemAreaID, current used only to link to PSA's for RoomSchedules, but can be expanded upon")>
    Public ReadOnly Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
    End Property

    Public Shared ParentResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentResourceBookingID, "Parent Resource Booking", Nothing)
    ''' <summary>
    ''' Gets the Parent Resource Booking value
    ''' </summary>
    <Display(Name:="Parent Resource Booking", Description:="Link to a parent ResourceBooking, currently only used for RoomSchedules in order to created child bookings for each area linked to the RoomSchedule")>
    Public ReadOnly Property ParentResourceBookingID() As Integer?
      Get
        Return GetProperty(ParentResourceBookingIDProperty)
      End Get
    End Property

    Public Shared PopoverContentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PopoverContent, "Popover Content", "")
    ''' <summary>
    ''' Gets the Popover Content value
    ''' </summary>
    <Display(Name:="Popover Content", Description:="")>
    Public ReadOnly Property PopoverContent() As String
      Get
        Return GetProperty(PopoverContentProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceBookingDescription

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROResourceBooking(dr As SafeDataReader) As ROResourceBooking

      Dim r As New ROResourceBooking()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(ResourceBookingIDProperty, .GetInt32(0))
        LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ResourceBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(ResourceBookingDescriptionProperty, .GetString(3))
        LoadProperty(StartDateTimeBufferProperty, .GetValue(4))
        LoadProperty(StartDateTimeProperty, .GetValue(5))
        LoadProperty(EndDateTimeProperty, .GetValue(6))
        LoadProperty(EndDateTimeBufferProperty, .GetValue(7))
        LoadProperty(IsBeingEditedByProperty, .GetString(8))
        LoadProperty(InEditDateTimeProperty, .GetValue(9))
        LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
        LoadProperty(IsCancelledProperty, .GetBoolean(11))
        LoadProperty(HumanResourceOffPeriodIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(HumanResourceSecondmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(ProductionHRIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
        LoadProperty(StatusCssClassProperty, .GetString(15))
        LoadProperty(IgnoreClashesProperty, .GetBoolean(16))
        LoadProperty(IgnoreClashesReasonProperty, .GetString(17))
        LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
        LoadProperty(IsLockedProperty, .GetBoolean(19))
        LoadProperty(IsLockedReasonProperty, .GetString(20))
        LoadProperty(IsTBCProperty, .GetBoolean(21))
        LoadProperty(EquipmentScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(22)))
        LoadProperty(ProductionSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
        LoadProperty(ParentResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(24)))
        LoadProperty(PopoverContentProperty, .GetString(25))
      End With

    End Sub

#End Region

  End Class

End Namespace