﻿' Generated 30 Apr 2015 10:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Resources.ReadOnly

  <Serializable()> _
  Public Class ROResourceList
    Inherits OBReadOnlyListBase(Of ROResourceList, ROResource)
    Implements Singular.Paging.IPagedList

    Private mTotalRecords As Integer = 0
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property

#Region " Business Methods "

    Public Function GetItem(ResourceID As Integer) As ROResource

      For Each child As ROResource In Me
        If child.ResourceID = ResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetRoomIDs() As List(Of Integer?)

      Dim result As New List(Of Integer?)
      For Each child As ROResource In Me
        If child.ResourceTypeID = OBLib.CommonData.Enums.ResourceType.Room Then
          result.Add(child.RoomID)
        End If
      Next
      Return result

    End Function

    Public Overrides Function ToString() As String

      Return "Resources"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Shared KeywordProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.Keyword, "")
      ''' <summary>
      ''' Gets and sets the Genre value
      ''' </summary>
      <Display(Name:="Keyword")>
      Public Property Keyword() As String
        Get
          Return ReadProperty(KeywordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeywordProperty, Value)
        End Set
      End Property

      Public Property UseSearch As Boolean = False
      Public Property ResourceName As String = Nothing
      Public Property ResourceTypeID As Integer? = Nothing
      Public Property HumanResourceID As Integer? = Nothing
      Public Property RoomID As Integer? = Nothing
      Public Property VehicleID As Integer? = Nothing
      Public Property EquipmentID As Integer? = Nothing
      Public Property ChannelID As Integer? = Nothing
      'Public Property SelectedResourceIDs As List(Of String) = New List(Of String)
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing
      Public Property ResourceTypeIDs As List(Of Integer) = New List(Of Integer)


      Public Sub New(ResourceName As String, ResourceTypeID As Integer?, HumanResourceID As Integer?, _
                     RoomID As Integer?, VehicleID As Integer?, EquipmentID As Integer?, ChannelID As Integer?)

        UseSearch = True
        Me.ResourceName = ResourceName
        Me.ResourceTypeID = ResourceTypeID
        Me.HumanResourceID = HumanResourceID
        Me.RoomID = RoomID
        Me.VehicleID = VehicleID
        Me.EquipmentID = EquipmentID
        Me.ChannelID = ChannelID

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROResourceList() As ROResourceList

      Return New ROResourceList()

    End Function

    Public Shared Sub BeginGetROResourceList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROResourceList)))

      Dim dp As New DataPortal(Of ROResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROResourceList(CallBack As EventHandler(Of DataPortalResult(Of ROResourceList)))

      Dim dp As New DataPortal(Of ROResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROResourceList() As ROResourceList

      Return DataPortal.Fetch(Of ROResourceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROResource.GetROResource(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROResourceList"

            'If crit.UseSearch Then
            cm.Parameters.AddWithValue("@ResourceName", Singular.Strings.MakeEmptyDBNull(crit.ResourceName))
            cm.Parameters.AddWithValue("@ResourceTypeID", Singular.Misc.NothingDBNull(crit.ResourceTypeID))
            cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
            cm.Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(crit.RoomID))
            cm.Parameters.AddWithValue("@VehicleID", Singular.Misc.NothingDBNull(crit.VehicleID))
            cm.Parameters.AddWithValue("@EquipmentID", Singular.Misc.NothingDBNull(crit.EquipmentID))
            cm.Parameters.AddWithValue("@ChannelID", Singular.Misc.NothingDBNull(crit.ChannelID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@SelectedResourceIDs", NothingDBNull(Nothing))
            cm.Parameters.AddWithValue("@ResourceTypeIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ResourceTypeIDs)))
            cm.Parameters.AddWithValue("@Keyword", Strings.MakeEmptyDBNull(crit.Keyword))
            'Singular.Strings.MakeEmptyDBNull(OBLib.OBMisc.StringArrayToXML(crit.SelectedResourceIDs.ToArray))
            crit.AddParameters(cm)
            'End If

            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace