﻿' Generated 30 Apr 2015 10:00 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources

  <Serializable()> _
  Public Class ResourceBase
    Inherits OBBusinessBase(Of ResourceBase)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceBaseIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBaseID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ResourceBaseID() As Integer
      Get
        Return GetProperty(ResourceBaseIDProperty)
      End Get
    End Property

    Public Shared ResourceBaseNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBaseName, "ResourceBase Name", "")
    ''' <summary>
    ''' Gets and sets the ResourceBase Name value
    ''' </summary>
    <Display(Name:="ResourceBase Name", Description:=""),
    StringLength(250, ErrorMessage:="ResourceBase Name cannot be more than 250 characters")>
    Public Property ResourceBaseName() As String
      Get
        Return GetProperty(ResourceBaseNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBaseNameProperty, Value)
      End Set
    End Property

    Public Shared ResourceBaseTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBaseTypeID, "ResourceBase Type", Nothing)
    ''' <summary>
    ''' Gets and sets the ResourceBase Type value
    ''' </summary>
    <Display(Name:="ResourceBase Type", Description:=""),
    Required(ErrorMessage:="ResourceBase Type required")>
    Public Property ResourceBaseTypeID() As Integer?
      Get
        Return GetProperty(ResourceBaseTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBaseTypeIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceBaseIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceBaseID, "Human ResourceBase", Nothing)
    ''' <summary>
    ''' Gets and sets the Human ResourceBase value
    ''' </summary>
    <Display(Name:="Human ResourceBase", Description:="")>
    Public Property HumanResourceBaseID() As Integer?
      Get
        Return GetProperty(HumanResourceBaseIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceBaseIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
    Public Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentID, "Equipment", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:="")>
    Public Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ChannelID, "Channel", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public Property ChannelID() As Integer?
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ChannelIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBaseIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceBaseName.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "ResourceBase")
        Else
          Return String.Format("Blank {0}", "ResourceBase")
        End If
      Else
        Return Me.ResourceBaseName
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceBase() method.

    End Sub

    Public Shared Function NewResourceBase() As ResourceBase

      Return DataPortal.CreateChild(Of ResourceBase)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetResourceBase(dr As SafeDataReader) As ResourceBase

      Dim r As New ResourceBase()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceBaseIDProperty, .GetInt32(0))
          LoadProperty(ResourceBaseNameProperty, .GetString(1))
          LoadProperty(ResourceBaseTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(HumanResourceBaseIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
          LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(CreatedByProperty, .GetInt32(8))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(9))
          LoadProperty(ModifiedByProperty, .GetInt32(10))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(11))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insResourceBase"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updResourceBase"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramResourceBaseID As SqlParameter = .Parameters.Add("@ResourceBaseID", SqlDbType.Int)
          paramResourceBaseID.Value = GetProperty(ResourceBaseIDProperty)
          If Me.IsNew Then
            paramResourceBaseID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ResourceBaseName", GetProperty(ResourceBaseNameProperty))
          .Parameters.AddWithValue("@ResourceBaseTypeID", GetProperty(ResourceBaseTypeIDProperty))
          .Parameters.AddWithValue("@HumanResourceBaseID", Singular.Misc.NothingDBNull(GetProperty(HumanResourceBaseIDProperty)))
          .Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(GetProperty(RoomIDProperty)))
          .Parameters.AddWithValue("@VehicleID", Singular.Misc.NothingDBNull(GetProperty(VehicleIDProperty)))
          .Parameters.AddWithValue("@EquipmentID", Singular.Misc.NothingDBNull(GetProperty(EquipmentIDProperty)))
          .Parameters.AddWithValue("@ChannelID", Singular.Misc.NothingDBNull(GetProperty(ChannelIDProperty)))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUser.UserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ResourceBaseIDProperty, paramResourceBaseID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delResourceBase"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ResourceBaseID", GetProperty(ResourceBaseIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace