﻿' Generated 30 Apr 2015 09:59 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance.Resources

  <Serializable()> _
  Public Class ResourceList
    Inherits OBBusinessListBase(Of ResourceList, Resource)

#Region " Business Methods "

    Public Function GetItem(ResourceID As Integer) As Resource

      For Each child As Resource In Me
        If child.ResourceID = ResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetRoomIDs() As List(Of Integer?)

      Dim result As New List(Of Integer?)
      For Each child As Resource In Me
        If child.ResourceTypeID = OBLib.CommonData.Enums.ResourceType.Room Then
          result.Add(child.RoomID)
        End If
      Next
      Return result

    End Function

    Public Overrides Function ToString() As String

      Return "Resources"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public UseSearch As Boolean = False
      Public ResourceName As Object = Nothing
      Public ResourceTypeID As Object = Nothing
      Public HumanResourceID As Object = Nothing
      Public RoomID As Object = Nothing
      Public VehicleID As Object = Nothing
      Public EquipmentID As Object = Nothing
      Public ChannelID As Object = Nothing

      Public Sub New(ResourceName As Object, ResourceTypeID As Object, HumanResourceID As Object, _
          RoomID As Object, VehicleID As Object, EquipmentID As Object, ChannelID As Object)

        UseSearch = True
        Me.ResourceName = ResourceName
        Me.ResourceTypeID = ResourceTypeID
        Me.HumanResourceID = HumanResourceID
        Me.RoomID = RoomID
        Me.VehicleID = VehicleID
        Me.EquipmentID = EquipmentID
        Me.ChannelID = ChannelID

      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewResourceList() As ResourceList

      Return New ResourceList()

    End Function

    Public Shared Sub BeginGetResourceList(CallBack As EventHandler(Of DataPortalResult(Of ResourceList)))

      Dim dp As New DataPortal(Of ResourceList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetResourceList() As ResourceList

      Return DataPortal.Fetch(Of ResourceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(Resource.GetResource(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getResourceList"

            If crit.UseSearch Then
              cm.Parameters.AddWithValue("@ResourceName", Singular.Misc.NothingDBNull(crit.ResourceName))
              cm.Parameters.AddWithValue("@ResourceTypeID", Singular.Misc.NothingDBNull(crit.ResourceTypeID))
              cm.Parameters.AddWithValue("@HumanResourceID", Singular.Misc.NothingDBNull(crit.HumanResourceID))
              cm.Parameters.AddWithValue("@RoomID", Singular.Misc.NothingDBNull(crit.RoomID))
              cm.Parameters.AddWithValue("@VehicleID", Singular.Misc.NothingDBNull(crit.VehicleID))
              cm.Parameters.AddWithValue("@EquipmentID", Singular.Misc.NothingDBNull(crit.EquipmentID))
              cm.Parameters.AddWithValue("@ChannelID", Singular.Misc.NothingDBNull(crit.ChannelID))
            End If

            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As Resource In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As Resource In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace