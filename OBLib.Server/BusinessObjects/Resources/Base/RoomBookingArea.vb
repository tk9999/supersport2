﻿' Generated 21 Dec 2015 15:50 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Resources

  <Serializable()> _
  Public Class RoomBookingArea '(Of ParentType As ResourceBookingBase(Of ParentType))
    Inherits OBBusinessBase(Of RoomBookingArea) '(Of ParentType))

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
    End Property

    Public Shared BookingPSAIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.BookingPSAID, "Booking PSA")
    ''' <summary>
    ''' Gets and sets the Booking PSA value
    ''' </summary>
    <Display(Name:="Booking PSA", Description:=""),
    Required(ErrorMessage:="Booking PSA required")>
    Public Property BookingPSAID() As Integer
      Get
        Return GetProperty(BookingPSAIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(BookingPSAIDProperty, Value)
      End Set
    End Property

    Public Shared AreaPSAIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AreaPSAID, "Area PSA")
    ''' <summary>
    ''' Gets and sets the Area PSA value
    ''' </summary>
    <Display(Name:="Area PSA", Description:=""),
    Required(ErrorMessage:="Area PSA required")>
    Public Property AreaPSAID() As Integer
      Get
        Return GetProperty(AreaPSAIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AreaPSAIDProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "Sub-Dept")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="System required")>
    Public Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "Sub-Dept")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="")>
    Public Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Area")
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:=""),
    Required(ErrorMessage:="Area required")>
    Public Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area")
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaStatusID, "Status")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:=""),
    Required(ErrorMessage:="Status required")>
    Public Property ProductionAreaStatusID() As Integer
      Get
        Return GetProperty(ProductionAreaStatusIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ProductionAreaStatusIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaStatusProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionAreaStatus, "Status")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="Status", Description:="")>
    Public Property ProductionAreaStatus() As String
      Get
        Return GetProperty(ProductionAreaStatusProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaStatusProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "RoomScheduleID", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="RoomScheduleID", Description:="")>
    Public Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared PositionCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.PositionCount, "PositionCount")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="PositionCount", Description:="")>
    Public Property PositionCount() As Integer
      Get
        Return GetProperty(PositionCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(PositionCountProperty, Value)
      End Set
    End Property

    Public Shared NotAssignedCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.NotAssignedCount, "NotAssignedCount")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="NotAssignedCount", Description:="")>
    Public Property NotAssignedCount() As Integer
      Get
        Return GetProperty(NotAssignedCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(NotAssignedCountProperty, Value)
      End Set
    End Property

    Public Shared AssignedCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.AssignedCount, "AssignedCount")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="AssignedCount", Description:="")>
    Public Property AssignedCount() As Integer
      Get
        Return GetProperty(AssignedCountProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(AssignedCountProperty, Value)
      End Set
    End Property

    Public Shared HasMissingRequirementsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasMissingRequirements, "HasMissingRequirements")
    ''' <summary>
    ''' Gets and sets the Production Area Status value
    ''' </summary>
    <Display(Name:="HasMissingRequirements", Description:="")>
    Public Property HasMissingRequirements() As Boolean
      Get
        Return GetProperty(HasMissingRequirementsProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasMissingRequirementsProperty, Value)
      End Set
    End Property



#End Region

#Region " Methods "

    Public Function GetParent() As Object

      Return CType(Me.Parent, RoomBookingAreaList).Parent

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.System.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Room Booking Area")
        Else
          Return String.Format("Blank {0}", "Room Booking Area")
        End If
      Else
        Return Me.System
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewRoomBookingArea() method.

    End Sub

    Public Shared Function NewRoomBookingArea() As RoomBookingArea

      Return DataPortal.CreateChild(Of RoomBookingArea)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetRoomBookingArea(dr As SafeDataReader) As RoomBookingArea

      Dim r As New RoomBookingArea()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceBookingIDProperty, .GetInt32(0))
          LoadProperty(BookingPSAIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(AreaPSAIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
          LoadProperty(SystemProperty, .GetString(4))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(ProductionAreaProperty, .GetString(6))
          LoadProperty(ProductionAreaStatusIDProperty, Singular.Misc.ZeroNothing(.GetInt32(7)))
          LoadProperty(ProductionAreaStatusProperty, .GetString(8))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(9)))
          LoadProperty(PositionCountProperty, .GetInt32(10))
          LoadProperty(NotAssignedCountProperty, .GetInt32(11))
          LoadProperty(AssignedCountProperty, .GetInt32(12))
          LoadProperty(HasMissingRequirementsProperty, .GetBoolean(13))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insRoomBookingArea"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updRoomBookingArea"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
          If Me.IsNew Then
            paramResourceBookingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@BookingPSAID", GetProperty(BookingPSAIDProperty))
          .Parameters.AddWithValue("@AreaPSAID", GetProperty(AreaPSAIDProperty))
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@System", GetProperty(SystemProperty))
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@ProductionArea", GetProperty(ProductionAreaProperty))
          .Parameters.AddWithValue("@ProductionAreaStatusID", GetProperty(ProductionAreaStatusIDProperty))
          .Parameters.AddWithValue("@ProductionAreaStatus", GetProperty(ProductionAreaStatusProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ResourceBookingIDProperty, paramResourceBookingID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delRoomBookingArea"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace