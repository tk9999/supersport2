﻿' Generated 13 Jun 2015 16:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Resources.ReadOnly
Imports OBLib.Productions.Base

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Resources

  <Serializable()> _
  Public MustInherit Class ResourceBookingBase(Of T As ResourceBookingBase(Of T))
    Inherits OBBusinessBase(Of T)
    'Implements IResourceBooking

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceID, "Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
    Public Property ResourceID() As Integer?
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceBookingTypeID, "Resource Booking Type", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type", Description:=""),
    Required(ErrorMessage:="Resource Booking Type required")>
    Public Property ResourceBookingTypeID() As Integer?
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceBookingTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Booking Description", "")
    ''' <summary>
    ''' Gets and sets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Booking Description", Description:=""),
    StringLength(500, ErrorMessage:="Booking Description cannot be more than 500 characters")>
    Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Call Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Call Time", Description:=""),
    DateField(FormatString:="ddd dd MMM yy HH:mm")>
    Public Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Time", Description:=""),
    DateField(FormatString:="ddd dd MMM yy HH:mm"),
    Required(ErrorMessage:="Start Time required")>
    Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Time", Description:=""),
    DateField(FormatString:="ddd dd MMM yy HH:mm"),
    Required(ErrorMessage:="End Date Time required")>
    Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "Wrap Time")
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="Wrap Time", Description:=""),
    DateField(FormatString:="ddd dd MMM yy HH:mm")>
    Public Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared IsBeingEditedByProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsBeingEditedBy, "Is Being Edited By", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Being Edited", Description:="")>
    Public Property IsBeingEditedBy() As String
      Get
        Return GetProperty(IsBeingEditedByProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IsBeingEditedByProperty, Value)
      End Set
    End Property

    Public Shared InEditDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.InEditDateTime, Nothing)
    ''' <summary>
    ''' Gets and sets the Call Time Start value
    ''' </summary>
    <Display(Name:="Edit Time", Description:="")>
    Public Property InEditDateTime As DateTime?
      Get
        Return GetProperty(InEditDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(InEditDateTimeProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared RoomIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomID, "Room", Nothing)
    ''' <summary>
    ''' Gets and sets the Room value
    ''' </summary>
    <Display(Name:="Room", Description:="")>
    Public Property RoomID() As Integer?
      Get
        Return GetProperty(RoomIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomIDProperty, Value)
      End Set
    End Property

    Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.VehicleID, "Vehicle", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="Vehicle", Description:="")>
    Public Property VehicleID() As Integer?
      Get
        Return GetProperty(VehicleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(VehicleIDProperty, Value)
      End Set
    End Property

    Public Shared EquipmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentID, "Equipment", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:="")>
    Public Property EquipmentID() As Integer?
      Get
        Return GetProperty(EquipmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentIDProperty, Value)
      End Set
    End Property

    Public Shared ChannelIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ChannelID, "Channel", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="Channel", Description:="")>
    Public Property ChannelID() As Integer?
      Get
        Return GetProperty(ChannelIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ChannelIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceShiftIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceShiftID, "HumanResourceShiftID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="HumanResourceShiftID", Description:="")>
    Public Property HumanResourceShiftID() As Integer?
      Get
        Return GetProperty(HumanResourceShiftIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceShiftIDProperty, Value)
      End Set
    End Property

    Public Shared IsCancelledProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsCancelled, "Is Cancelled", False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Being Edited", Description:="")>
    Public Property IsCancelled() As Boolean
      Get
        Return GetProperty(IsCancelledProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsCancelledProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceOffPeriodIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceOffPeriodID, "HumanResourceOffPeriodID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="HumanResourceShiftID", Description:="")>
    Public Property HumanResourceOffPeriodID() As Integer?
      Get
        Return GetProperty(HumanResourceOffPeriodIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceOffPeriodIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceSecondmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceSecondmentID, "HumanResourceSecondmentID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="HumanResourceSecondmentID", Description:="")>
    Public Property HumanResourceSecondmentID() As Integer?
      Get
        Return GetProperty(HumanResourceSecondmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceSecondmentIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionHRIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionHRID, "ProductionHRID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="ProductionHRID", Description:="")>
    Public Property ProductionHRID() As Integer?
      Get
        Return GetProperty(ProductionHRIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionHRIDProperty, Value)
      End Set
    End Property

    Public Shared StatusCssClassProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.StatusCssClass, "StatusCssClass", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="StatusCssClass", Description:="")>
    Public Property StatusCssClass() As String
      Get
        Return GetProperty(StatusCssClassProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(StatusCssClassProperty, Value)
      End Set
    End Property

    Public Shared IgnoreClashesProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IgnoreClashes, False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="IgnoreClashes", Description:="")>
    Public Property IgnoreClashes() As Boolean
      Get
        Return GetProperty(IgnoreClashesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IgnoreClashesProperty, Value)
      End Set
    End Property

    Public Shared IgnoreClashesReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IgnoreClashesReason, "IgnoreClashesReason", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="StatusCssClass", Description:="")>
    Public Property IgnoreClashesReason() As String
      Get
        Return GetProperty(IgnoreClashesReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IgnoreClashesReasonProperty, Value)
      End Set
    End Property

    Public Shared RoomScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.RoomScheduleID, "RoomScheduleID", Nothing)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="RoomScheduleID", Description:="")>
    Public Property RoomScheduleID() As Integer?
      Get
        Return GetProperty(RoomScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(RoomScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared IsLockedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsLocked, False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="IsLocked", Description:="")>
    Public Property IsLocked() As Boolean
      Get
        Return GetProperty(IsLockedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsLockedProperty, Value)
      End Set
    End Property

    Public Shared IsLockedReasonProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsLockedReason, "IsLockedReason", "")
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="StatusCssClass", Description:="")>
    Public Property IsLockedReason() As String
      Get
        Return GetProperty(IsLockedReasonProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IsLockedReasonProperty, Value)
      End Set
    End Property

    Public Shared IsTBCProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsTBC, False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="IsTBC", Description:="")>
    Public Property IsTBC() As Boolean
      Get
        Return GetProperty(IsTBCProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsTBCProperty, Value)
      End Set
    End Property

    Public Shared EquipmentScheduleIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.EquipmentScheduleID, "Equipment", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="Equipment", Description:="")>
    Public Property EquipmentScheduleID() As Integer?
      Get
        Return GetProperty(EquipmentScheduleIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(EquipmentScheduleIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionSystemAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionSystemAreaID, "ProductionSystemAreaID", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="ProductionSystemAreaID", Description:="")>
    Public Property ProductionSystemAreaID() As Integer?
      Get
        Return GetProperty(ProductionSystemAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionSystemAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ParentResourceBookingIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ParentResourceBookingID, "ParentResourceBookingID", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="ParentResourceBookingID", Description:="")>
    Public Property ParentResourceBookingID() As Integer?
      Get
        Return GetProperty(ParentResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ParentResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared PopoverContentProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PopoverContent, "Popover Content", "")
    ''' <summary>
    ''' Gets and sets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Popover Content")>
    Public Property PopoverContent() As String
      Get
        Return GetProperty(PopoverContentProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(PopoverContentProperty, Value)
      End Set
    End Property

#Region " Non Database Properties "

    Public Shared ResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceName, "Resource", "")
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Resource")>
    Public Property ResourceName() As String
      Get
        Return GetProperty(ResourceNameProperty)
      End Get
      Set(value As String)
        SetProperty(ResourceNameProperty, value)
      End Set
    End Property

    Public Shared ResourceBookingBaseTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingBaseType, "Type", "")
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(Name:="Type")>
    Public Property ResourceBookingBaseType() As String
      Get
        Return GetProperty(ResourceBookingBaseTypeProperty)
      End Get
      Set(value As String)
        SetProperty(ResourceBookingBaseTypeProperty, value)
      End Set
    End Property

    Public Shared IsReceivingUpdateProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsReceivingUpdate, False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Receiving Update", Description:="")>
    Public Property IsReceivingUpdate() As Boolean
      Get
        Return GetProperty(IsReceivingUpdateProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsReceivingUpdateProperty, Value)
      End Set
    End Property

    Public Shared HasClashesProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.HasClashes, False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="HasClashes"),
    InitialDataOnly>
    Public Property HasClashes() As Boolean
      Get
        Return GetProperty(HasClashesProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasClashesProperty, Value)
      End Set
    End Property

    <InitialDataOnly>
    Public Property IsClientValid As Boolean
      Get
        Return Me.IsValid
      End Get
      Set(value As Boolean)

      End Set
    End Property

    Public Shared IsSharedTypeProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.IsSharedType, False)
    ''' <summary>
    ''' Gets and sets the Can Move value
    ''' </summary>
    <Display(Name:="Is Shared Type", Description:="")>
    Public Property IsSharedType() As Boolean
      Get
        Return GetProperty(IsSharedTypeProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsSharedTypeProperty, Value)
      End Set
    End Property

    Public Shared OriginalStartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalStartDateTimeBuffer, "Original Start Date Time Buffer")
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Original Start Date Time Buffer")>
    Public ReadOnly Property OriginalStartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(OriginalStartDateTimeBufferProperty)
      End Get
      'Set(ByVal Value As DateTime?)
      '  SetProperty(OriginalStartDateTimeBufferProperty, Value)
      'End Set
    End Property

    Public Shared OriginalStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalStartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:="")>
    Public ReadOnly Property OriginalStartDateTime As DateTime?
      Get
        Return GetProperty(OriginalStartDateTimeProperty)
      End Get
      'Set(ByVal Value As DateTime?)
      '  SetProperty(StartDateTimeProperty, Value)
      'End Set
    End Property

    Public Shared OriginalEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalEndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:="")>
    Public ReadOnly Property OriginalEndDateTime As DateTime?
      Get
        Return GetProperty(OriginalEndDateTimeProperty)
      End Get
      'Set(ByVal Value As DateTime?)
      '  SetProperty(EndDateTimeProperty, Value)
      'End Set
    End Property

    Public Shared OriginalEndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.OriginalEndDateTimeBuffer, "Original End Date Time Buffer")
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="Original End Date Time Buffer")>
    Public ReadOnly Property OriginalEndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(OriginalEndDateTimeBufferProperty)
      End Get
      'Set(ByVal Value As DateTime?)
      '  SetProperty(OriginalEndDateTimeBufferProperty, Value)
      'End Set
    End Property

#End Region

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ResourceBookingIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceBookingDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Resource Booking")
        Else
          Return String.Format("Blank {0}", "Resource Booking")
        End If
      Else
        Return Me.ResourceBookingDescription
      End If

    End Function

#End Region

#Region " Child Lists "

    Public Shared ClashDetailListProperty As PropertyInfo(Of List(Of Helpers.ResourceHelpers.ClashDetail)) = RegisterProperty(Of List(Of Helpers.ResourceHelpers.ClashDetail))(Function(c) c.ClashDetailList, "Clash List")
    <InitialDataOnly, AlwaysClean>
    Public ReadOnly Property ClashDetailList() As List(Of Helpers.ResourceHelpers.ClashDetail)
      Get
        If GetProperty(ClashDetailListProperty) Is Nothing Then
          LoadProperty(ClashDetailListProperty, New List(Of Helpers.ResourceHelpers.ClashDetail))
        End If
        Return GetProperty(ClashDetailListProperty)
      End Get
    End Property

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      'With AddWebRule(StartDateTimeProperty)
      '  .JavascriptRuleFunctionName = "ResourceBookingBaseBO.StartDateTimeValid"
      '  .ServerRuleFunction = AddressOf StartDateTimeValid
      '  .AddTriggerProperty(EndDateTimeProperty)
      '  .AffectedProperties.Add(EndDateTimeProperty)
      'End With

      'With AddWebRule(ResourceIDProperty)
      '  .JavascriptRuleFunctionName = "ResourceBookingBaseBO.ResourceIDValid"
      '  .ServerRuleFunction = AddressOf ResourceIDValid
      '  .AddTriggerProperty(HumanResourceIDProperty)
      '  .AddTriggerProperty(RoomIDProperty)
      '  .AddTriggerProperty(VehicleIDProperty)
      '  .AddTriggerProperty(EquipmentIDProperty)
      '  .AddTriggerProperty(ChannelIDProperty)
      '  .AffectedProperties.Add(HumanResourceIDProperty)
      '  .AffectedProperties.Add(RoomIDProperty)
      '  .AffectedProperties.Add(VehicleIDProperty)
      '  .AffectedProperties.Add(EquipmentIDProperty)
      '  .AffectedProperties.Add(ChannelIDProperty)
      'End With

      'With AddWebRule(ResourceIDProperty)
      '  .JavascriptRuleFunctionName = "ResourceBookingBaseBO.ResourceBookingTypeIDValid"
      '  .ServerRuleFunction = AddressOf ResourceBookingTypeIDValid
      '  .AddTriggerProperty(HumanResourceIDProperty)
      '  .AddTriggerProperty(RoomIDProperty)
      '  .AddTriggerProperty(VehicleIDProperty)
      '  .AddTriggerProperty(EquipmentIDProperty)
      '  .AddTriggerProperty(ChannelIDProperty)
      '  .AffectedProperties.Add(HumanResourceIDProperty)
      '  .AffectedProperties.Add(RoomIDProperty)
      '  .AffectedProperties.Add(VehicleIDProperty)
      '  .AffectedProperties.Add(EquipmentIDProperty)
      '  .AffectedProperties.Add(ChannelIDProperty)
      'End With

    End Sub

    Public Shared Function StartDateTimeValid(ResourceBookingBase As T) As String
      Dim ErrorMessage As String = ""
      If ResourceBookingBase.StartDateTime IsNot Nothing And ResourceBookingBase.EndDateTime IsNot Nothing Then
        If Not (ResourceBookingBase.StartDateTime.Value <= ResourceBookingBase.EndDateTime.Value) Then
          ErrorMessage = "Start Time must be before End Time"
        End If
      End If
      Return ErrorMessage
    End Function

    Public Shared Function ResourceIDValid(ResourceBookingBase As T) As String
      Dim ErrorMessage As String = ""
      If ResourceBookingBase.ResourceID IsNot Nothing Then
        If (ResourceBookingBase.HumanResourceID Is Nothing _
            And ResourceBookingBase.RoomID Is Nothing _
            And ResourceBookingBase.VehicleID Is Nothing _
            And ResourceBookingBase.EquipmentID Is Nothing _
            And ResourceBookingBase.ChannelID Is Nothing) Then
          ErrorMessage = "HumanResourceID, RoomID, VehicleID, EquipmentID or ChannelID is required"
        Else
          'Which one exactly is required, this will be determined based on ResourceBookingTypeID
        End If
      End If
      Return ErrorMessage
    End Function

    Public Shared Function ResourceBookingTypeIDValid(ResourceBookingBase As T) As String
      Dim ErrorMessage As String = ""
      If ResourceBookingBase.ResourceBookingTypeID IsNot Nothing Then
        If (ResourceBookingBase.ProductionHRID Is Nothing _
            And ResourceBookingBase.HumanResourceOffPeriodID Is Nothing _
            And ResourceBookingBase.HumanResourceSecondmentID Is Nothing _
            And ResourceBookingBase.HumanResourceShiftID Is Nothing _
            And ResourceBookingBase.RoomScheduleID Is Nothing) Then
          ErrorMessage = "ProductionHRID, HumanResourceOffPeriodID, HumanResourceSecondmentID, HumanResourceShiftID or RoomScheduleID is required"
        Else
          'Which one exactly is required, this will be determined based on ResourceBookingTypeID
        End If
      End If
      Return ErrorMessage
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceBookingBase() method.

    End Sub

    Public Shared Function NewResourceBookingBase() As ResourceBookingBase(Of T)

      Return DataPortal.CreateChild(Of ResourceBookingBase(Of T))()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Overridable Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ResourceBookingIDProperty, .GetInt32(0))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(ResourceBookingDescriptionProperty, .GetString(3))
          LoadProperty(StartDateTimeBufferProperty, .GetValue(4))
          LoadProperty(StartDateTimeProperty, .GetValue(5))
          LoadProperty(EndDateTimeProperty, .GetValue(6))
          LoadProperty(EndDateTimeBufferProperty, .GetValue(7))
          LoadProperty(IsBeingEditedByProperty, .GetString(8))
          LoadProperty(InEditDateTimeProperty, .GetValue(9))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(10)))
          LoadProperty(RoomIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(VehicleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(EquipmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
          LoadProperty(ChannelIDProperty, Singular.Misc.ZeroNothing(.GetInt32(14)))
          LoadProperty(HumanResourceShiftIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(IsCancelledProperty, .GetBoolean(16))
          LoadProperty(HumanResourceOffPeriodIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(HumanResourceSecondmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(18)))
          LoadProperty(ProductionHRIDProperty, Singular.Misc.ZeroNothing(.GetInt32(19)))
          LoadProperty(StatusCssClassProperty, .GetString(20))
          LoadProperty(IgnoreClashesProperty, .GetBoolean(21))
          LoadProperty(IgnoreClashesReasonProperty, .GetString(22))
          LoadProperty(RoomScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(23)))
          LoadProperty(IsLockedProperty, .GetBoolean(24))
          LoadProperty(IsLockedReasonProperty, .GetString(25))
          LoadProperty(IsTBCProperty, .GetBoolean(26))
          LoadProperty(EquipmentScheduleIDProperty, Singular.Misc.ZeroNothing(.GetInt32(27)))
          LoadProperty(ProductionSystemAreaIDProperty, ZeroNothing(.GetInt32(28)))
          LoadProperty(ParentResourceBookingIDProperty, ZeroNothing(.GetInt32(29)))
          LoadProperty(PopoverContentProperty, .GetString(30))
          LoadProperty(IsSharedTypeProperty, .GetBoolean(31))
        End With
      End Using
      LoadProperty(OriginalStartDateTimeBufferProperty, StartDateTimeBuffer)
      LoadProperty(OriginalStartDateTimeProperty, StartDateTime)
      LoadProperty(OriginalEndDateTimeProperty, EndDateTime)
      LoadProperty(OriginalEndDateTimeBufferProperty, EndDateTimeBuffer)
      FetchExtraProperties(sdr)

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Overridable Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = InsProcName
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Overridable Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = UpdProcName
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)
      BeforeInsertUpdate()

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramResourceBookingID As SqlParameter = .Parameters.Add("@ResourceBookingID", SqlDbType.Int)
          paramResourceBookingID.Value = GetProperty(ResourceBookingIDProperty)
          If Me.IsNew Then
            paramResourceBookingID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
          .Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
          .Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
          .Parameters.AddWithValue("@StartDateTimeBuffer", (New SmartDate(GetProperty(StartDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTimeBuffer", (New SmartDate(GetProperty(EndDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@HumanResourceID", NothingDBNull(GetProperty(HumanResourceIDProperty)))
          .Parameters.AddWithValue("@RoomID", NothingDBNull(GetProperty(RoomIDProperty)))
          .Parameters.AddWithValue("@VehicleID", NothingDBNull(GetProperty(VehicleIDProperty)))
          .Parameters.AddWithValue("@EquipmentID", NothingDBNull(GetProperty(EquipmentIDProperty)))
          .Parameters.AddWithValue("@ChannelID", NothingDBNull(GetProperty(ChannelIDProperty)))
          .Parameters.AddWithValue("ModifiedBy", CurrentUser.UserID)
          .Parameters.AddWithValue("@HumanResourceShiftID", NothingDBNull(GetProperty(HumanResourceShiftIDProperty)))
          .Parameters.AddWithValue("@IsCancelled", GetProperty(IsCancelledProperty))
          .Parameters.AddWithValue("@HumanResourceOffPeriodID", NothingDBNull(GetProperty(HumanResourceOffPeriodIDProperty)))
          .Parameters.AddWithValue("@HumanResourceSecondmentID", NothingDBNull(GetProperty(HumanResourceSecondmentIDProperty)))
          .Parameters.AddWithValue("@ProductionHRID", NothingDBNull(GetProperty(ProductionHRIDProperty)))
          .Parameters.AddWithValue("@StatusCssClass", GetProperty(StatusCssClassProperty))
          .Parameters.AddWithValue("@IgnoreClashes", GetProperty(IgnoreClashesProperty))
          .Parameters.AddWithValue("@IgnoreClashesReason", GetProperty(IgnoreClashesReasonProperty))
          .Parameters.AddWithValue("@RoomScheduleID", NothingDBNull(GetProperty(RoomScheduleIDProperty)))
          .Parameters.AddWithValue("@IsLocked", GetProperty(IsLockedProperty))
          .Parameters.AddWithValue("@IsLockedReason", GetProperty(IsLockedReasonProperty))
          .Parameters.AddWithValue("@IsTBC", GetProperty(IsTBCProperty))
          .Parameters.AddWithValue("@IsBeingEditedBy", GetProperty(IsBeingEditedByProperty))
          .Parameters.AddWithValue("@InEditDateTime", NothingDBNull(GetProperty(InEditDateTimeProperty)))
          .Parameters.AddWithValue("@EquipmentScheduleID", NothingDBNull(GetProperty(EquipmentScheduleIDProperty)))
          .Parameters.AddWithValue("@ProductionSystemAreaID", NothingDBNull(GetProperty(ProductionSystemAreaIDProperty)))
          .Parameters.AddWithValue("@ParentResourceBookingID", NothingDBNull(GetProperty(ParentResourceBookingIDProperty)))
          .Parameters.AddWithValue("@PopoverContent", GetProperty(PopoverContentProperty))
          AddExtraParameters(.Parameters)
          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ResourceBookingIDProperty, paramResourceBookingID.Value)
          End If
          ' update child objects
          BeforeSaveChildren()
          UpdateChildList()
          MarkOld()
        End With
      Else
        BeforeSaveChildren()
        UpdateChildList()
      End If

    End Sub

    Friend Overridable Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub
      DeleteChildren()

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = DelProcName
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
        'cm.Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Public MustOverride Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)
    Public MustOverride Sub AddExtraParameters(Parameters As System.Data.SqlClient.SqlParameterCollection)
    Public MustOverride Sub UpdateChildList()
    Public MustOverride Sub DeleteChildren()

    <Browsable(False)>
    Public Overridable ReadOnly Property InsProcName As String
      Get
        Return "InsProcsWeb.insResourceBookingBase"
      End Get
    End Property
    <Browsable(False)>
    Public Overridable ReadOnly Property UpdProcName As String
      Get
        Return "UpdProcsWeb.updResourceBookingBase"
      End Get
    End Property
    <Browsable(False)>
    Public Overridable ReadOnly Property DelProcName As String
      Get
        Return "DelProcsWeb.delResourceBookingBase"
      End Get
    End Property

    Public Overridable Sub BeforeInsertUpdate()
    End Sub

    'Public Function GetResourceBookingHelper() As Helpers.ResourceHelpers.ResourceBooking Implements IResourceBooking.GetResourceBookingHelper
    '  Return New Helpers.ResourceHelpers.ResourceBooking(Me.Guid, Me.ResourceID, Me.ResourceBookingID, Me.StartDateTimeBuffer, Me.StartDateTime, Me.EndDateTime, Me.EndDateTimeBuffer)
    'End Function

    Public Overridable Sub BeforeSaveChildren()
    End Sub

  End Class

End Namespace