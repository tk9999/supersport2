﻿' Generated 13 Jun 2015 16:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Resources

  <Serializable()> _
  Public MustInherit Class ResourceBookingBaseList(Of T As ResourceBookingBaseList(Of T, C), C As ResourceBookingBase(Of C))
    Inherits OBBusinessListBase(Of T, C)
    'Implements IResourceBookingList

#Region " Business Methods "

    Public Function GetItem(ResourceBookingID As Integer) As C

      For Each child As C In Me
        If child.ResourceBookingID = ResourceBookingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Bookings"

    End Function

    'Public Function GetResourceBookingHelperList() As List(Of Helpers.ResourceHelpers.ResourceBooking) Implements IResourceBookingList.GetResourceBookingHelperList
    '  Dim l As New List(Of Helpers.ResourceHelpers.ResourceBooking)
    '  For Each rb As ResourceBookingBase(Of C) In Me
    '    l.Add(rb.GetResourceBookingHelper())
    '  Next
    '  Return l
    'End Function

    'Public Overridable Sub GetClashes() 'As List(Of Helpers.ResourceHelpers.ResourceBooking)
    '  'Build up the list to pass to the database
    '  Dim rbl As List(Of Helpers.ResourceHelpers.ResourceBooking) = GetResourceBookingHelperList()
    '  'get the clashes from the database
    '  Dim newRBL As List(Of Helpers.ResourceHelpers.ResourceBooking) = OBLib.Helpers.ResourceHelpers.GetResourceClashes(rbl)
    '  ProcessClashes(newRBL)
    'End Sub

    'oldList As List(Of OBLib.Helpers.ResourceHelpers.ResourceBooking), 
    Public Overridable Sub ProcessClashes(ClashesList As List(Of OBLib.Helpers.ResourceHelpers.ResourceBooking)) 'Implements IResourceBookingList.ProcessClashes
      InternalProcessClashes(ClashesList)
    End Sub

    Private Sub InternalProcessClashes(ClashesList As List(Of OBLib.Helpers.ResourceHelpers.ResourceBooking))
      For Each resourceBooking As ResourceBookingBase(Of C) In Me
        resourceBooking.ClashDetailList.Clear()
      Next
      For Each returnedObject As OBLib.Helpers.ResourceHelpers.ResourceBooking In ClashesList
        For Each resourceBooking As ResourceBookingBase(Of C) In Me
          If resourceBooking.Guid = returnedObject.ObjectGuid Then
            resourceBooking.ClashDetailList.AddRange(returnedObject.ClashDetails)
          End If
        Next
      Next
    End Sub

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ResourceBookingID As Integer? = Nothing
      Public Property ResourceIDs As String = ""
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing
      Public Property ResourceBookingIDs As String = ""
      Public Property SystemID As Integer? = Nothing
      Public Property ProductionAreaID As Integer? = Nothing

      Public Sub New(ResourceIDs As String, StartDate As DateTime?, EndDate As DateTime?,
                     ResourceBookingIDs As String,
                     SystemID As Integer?, ProductionAreaID As Integer?)
        Me.ResourceIDs = ResourceIDs
        Me.StartDate = StartDate
        Me.EndDate = EndDate
        Me.ResourceBookingIDs = ResourceBookingIDs
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New(ResourceBookingID As Integer?)
        Me.ResourceBookingID = ResourceBookingID
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    'Public Shared Function NewResourceBookingBaseList() As ResourceBookingBaseList

    '  Return New ResourceBookingBaseList()

    'End Function

    'Public Shared Sub BeginGetResourceBookingBaseList(CallBack As EventHandler(Of DataPortalResult(Of ResourceBookingBaseList)))

    '  Dim dp As New DataPortal(Of ResourceBookingBaseList)()
    '  AddHandler dp.FetchCompleted, CallBack
    '  dp.BeginFetch(New Criteria())

    'End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " .Net Data Access "

    'Public Shared Function GetResourceBookingBaseList(ResourceBookingID As Integer?) As ResourceBookingBaseList

    '  Return DataPortal.Fetch(Of ResourceBookingBaseList)(New Criteria(ResourceBookingID))

    'End Function

    'Public Shared Function GetResourceBookingBaseList(ResourceIDs As String, StartDate As DateTime?, EndDate As DateTime?) As ResourceBookingBaseList

    '  Return DataPortal.Fetch(Of ResourceBookingBaseList)(New Criteria(ResourceIDs, StartDate, EndDate))

    'End Function

    Protected Overridable Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Dim tmp As C = Activator.CreateInstance(GetType(C)) 'GetType(C).GetConstructor(New System.Type() {}).Invoke(New Object() {})
        tmp.Fetch(sdr)
        Me.Add(tmp)
      End While
      Me.RaiseListChangedEvents = True

      FetchChildLists(sdr)

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getResourceBookingBaseList"
            cm.Parameters.AddWithValue("@ResourceBookingID", NothingDBNull(crit.ResourceBookingID))
            cm.Parameters.AddWithValue("@ResourceIDs", Strings.MakeEmptyDBNull(crit.ResourceIDs))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@ResourceBookingIDs", Strings.MakeEmptyDBNull(crit.ResourceBookingIDs))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As ResourceBookingBase(Of C) In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As ResourceBookingBase(Of C) In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

    Public MustOverride Sub FetchChildLists(sdr As Csla.Data.SafeDataReader)

#End Region

#End Region

  End Class

End Namespace