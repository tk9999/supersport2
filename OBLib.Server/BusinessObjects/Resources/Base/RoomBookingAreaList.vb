﻿' Generated 21 Dec 2015 15:50 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Resources
  'ResourceBookingBaseList(Of T As ResourceBookingBaseList(Of T, C), C As ResourceBookingBase(Of C))
  <Serializable()> _
  Public Class RoomBookingAreaList '(Of ParentTypeListType As ResourceBookingBaseList(Of ParentTypeListType, ParentType), ParentType As ResourceBookingBase(Of ParentType))
    Inherits OBBusinessListBase(Of RoomBookingAreaList, 
                                   RoomBookingArea) '(Of ParentTypeListType, ParentType), '(Of ParentType))

#Region " Business Methods "

    Public Function GetItem(ResourceBookingID As Integer) As RoomBookingArea

      For Each child As RoomBookingArea In Me
        If child.ResourceBookingID = ResourceBookingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

#Region " Common "

    Public Shared Function NewRoomBookingAreaList() As RoomBookingAreaList

      Return New RoomBookingAreaList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As RoomBookingArea In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()

        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As RoomBookingArea In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace