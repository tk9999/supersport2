﻿' Generated 25 May 2017 06:20 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Helpers.ResourceHelpers

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Resources

  <Serializable()>
  Public Class ResourceBookingList
    Inherits OBBusinessListBase(Of ResourceBookingList, ResourceBooking)

#Region " Business Methods "

    Public Function GetItem(ResourceBookingID As Integer) As ResourceBooking

      For Each child As ResourceBooking In Me
        If child.ResourceBookingID = ResourceBookingID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Resource Bookings"

    End Function

    Public Shared Function GetResourceClashes(ResourceBookings As List(Of ResourceBooking)) As List(Of ClashDetail)

      Dim clashList As New List(Of ClashDetail)
      Dim ds As DataSet = GetClashesDataSet(ResourceBookings)
      For Each dr As DataRow In ds.Tables(0).Rows
        Dim rb As ResourceBooking = ResourceBookings.Where(Function(d) d.Guid = dr(0)).FirstOrDefault
        If rb IsNot Nothing Then
          Dim cd As New ClashDetail
          cd.ResourceBookingGuid = dr(0)
          'cd.LResourceID = dr(1)
          'cd.LResourceBookingID = dr(2)
          'cd.LStartDateTime = dr(3)
          'cd.LEndDateTime = dr(4)
          cd.ResourceID = dr(5)
          cd.ResourceBookingID = dr(6)
          cd.StartDateTimeBuffer = IIf(IsNullNothing(dr(7)), Nothing, dr(7))
          cd.StartDateTime = dr(8)
          cd.StartDateTimeDisplay = cd.StartDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          cd.EndDateTime = dr(9)
          cd.EndDateTimeDisplay = cd.EndDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          cd.EndDateTimeBuffer = IIf(IsNullNothing(dr(10)), Nothing, dr(10))
          cd.ResourceBookingTypeID = dr(11)
          cd.ResourceBookingDescription = dr(12)
          cd.ResourceBookingType = dr(13)
          clashList.Add(cd)
        End If
      Next
      Return clashList

    End Function

    Private Shared Function GetClashesDataSet(ResourceBookings As List(Of ResourceBooking)) As DataSet

      Dim RBTable As DataTable = CreateResourceBookingTableParameter(ResourceBookings)
      Dim cmd As New Singular.CommandProc("RuleProcs.ruleResourcesAvailable")
      Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      ResourceBookingsParam.Name = "@ResourceBookings"
      ResourceBookingsParam.SqlType = SqlDbType.Structured
      ResourceBookingsParam.Value = RBTable
      cmd.Parameters.Add(ResourceBookingsParam)
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute(0)
      Return cmd.Dataset

    End Function

    Private Shared Function CreateResourceBookingTableParameter(ResourceBookings As List(Of ResourceBooking)) As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceBookingID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("RoomScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionHRID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EquipmentScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceShiftID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceOffPeriodID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceSecondmentID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionSystemAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As ResourceBooking In ResourceBookings
        Dim row As DataRow = RBTable.NewRow
        row("ObjectGuid") = rb.Guid
        row("ResourceID") = NothingDBNull(rb.ResourceID)
        row("ResourceBookingID") = NothingDBNull(rb.ResourceBookingID)
        row("StartDateTimeBuffer") = NothingDBNull(rb.StartDateTimeBuffer)
        row("StartDateTime") = rb.StartDateTime.Value.ToString
        row("EndDateTime") = rb.EndDateTime.Value.ToString
        row("EndDateTimeBuffer") = NothingDBNull(rb.EndDateTimeBuffer)
        row("RoomScheduleID") = NothingDBNull(rb.RoomScheduleID)
        row("ProductionHRID") = NothingDBNull(rb.ProductionHRID)
        row("EquipmentScheduleID") = NothingDBNull(rb.EquipmentScheduleID)
        row("HumanResourceShiftID") = NothingDBNull(rb.HumanResourceShiftID)
        row("HumanResourceOffPeriodID") = NothingDBNull(rb.HumanResourceOffPeriodID)
        row("HumanResourceSecondmentID") = NothingDBNull(rb.HumanResourceSecondmentID)
        row("ProductionSystemAreaID") = NothingDBNull(rb.ProductionSystemAreaID)
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function


#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property ResourceID As Integer? = Nothing
      Public Property ResourceIDs As New List(Of Integer)
      Public Property ResourceBookingID As Integer? = Nothing
      Public Property ResourceBookingIDs As New List(Of Integer)
      Public Property StartDate As DateTime? = Nothing
      Public Property EndDate As DateTime? = Nothing
      Public Property ResourceTypeIDs As New List(Of Integer)

      Public Property IgnoreUserAccess As Boolean = False

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewResourceBookingList() As ResourceBookingList

      Return New ResourceBookingList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetResourceBookingList() As ResourceBookingList

      Return DataPortal.Fetch(Of ResourceBookingList)(New Criteria())

    End Function

    Public Shared Function GetResourceBookingList(crit As Criteria) As ResourceBookingList

      Return DataPortal.Fetch(Of ResourceBookingList)(crit)

    End Function

    Public Shared Function GetSerialisedChanges(Clear As Boolean, IgnoreUserAccess As Boolean) As String

      Dim changedResourceBookings As OBLib.Resources.ResourceBookingPendingChangeNotificationList = OBLib.Resources.ResourceBookingPendingChangeNotificationList.GetResourceBookingPendingChangeNotificationList(Clear)

      Dim AddedResourceBookingIDs As List(Of Integer) = changedResourceBookings.Where(Function(d) d.ActionTypeID = 1).Select(Function(d) d.ResourceBookingID).ToList
      Dim UpdatedResourceBookingIDs As List(Of Integer) = changedResourceBookings.Where(Function(d) {2, 4, 5}.Contains(d.ActionTypeID)).Select(Function(d) d.ResourceBookingID).ToList
      Dim DeletedResourceBookingIDs As List(Of Integer) = changedResourceBookings.Where(Function(d) d.ActionTypeID = 3).Select(Function(d) d.ResourceBookingID).ToList

      Dim addedBookings As ResourceBookingList = New ResourceBookingList
      Dim updatedBookings As ResourceBookingList = New ResourceBookingList

      If AddedResourceBookingIDs.Count > 0 Then
        addedBookings = OBLib.Resources.ResourceBookingList.GetResourceBookingList(New Criteria With {.ResourceBookingIDs = AddedResourceBookingIDs, .IgnoreUserAccess = IgnoreUserAccess})
      End If

      If UpdatedResourceBookingIDs.Count > 0 Then
        updatedBookings = OBLib.Resources.ResourceBookingList.GetResourceBookingList(New Criteria With {.ResourceBookingIDs = UpdatedResourceBookingIDs, .IgnoreUserAccess = IgnoreUserAccess})
      End If

      Dim wr As Singular.Web.Result = New Singular.Web.Result(True)
      wr.Data = New With {
                          .AddedBookings = addedBookings,
                          .UpdatedBookings = updatedBookings,
                          .RemovedBookingIDs = DeletedResourceBookingIDs
      }
      Dim SerialisedChanges As String = Singular.Web.Data.JSonWriter.SerialiseObject(wr)
      Return SerialisedChanges

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ResourceBooking.GetResourceBooking(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As ResourceBooking = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.ResourceBookingID <> sdr.GetInt32(0) Then
            parent = Me.GetItem(sdr.GetInt32(0))
          End If
          parent.ResourceBookingGenRefNumberList.RaiseListChangedEvents = False
          parent.ResourceBookingGenRefNumberList.Add(ResourceBookingGenRefNumber.GetResourceBookingGenRefNumber(sdr))
          parent.ResourceBookingGenRefNumberList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getResourceBookingList"
            cm.Parameters.AddWithValue("@ResourceID", NothingDBNull(crit.ResourceID))
            cm.Parameters.AddWithValue("@ResourceIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ResourceIDs)))
            cm.Parameters.AddWithValue("@ResourceBookingID", NothingDBNull(crit.ResourceBookingID))
            cm.Parameters.AddWithValue("@ResourceBookingIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ResourceBookingIDs)))
            cm.Parameters.AddWithValue("@StartDate", NothingDBNull(crit.StartDate))
            cm.Parameters.AddWithValue("@EndDate", NothingDBNull(crit.EndDate))
            cm.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
            cm.Parameters.AddWithValue("@ResourceTypeIDs", Strings.MakeEmptyDBNull(OBLib.OBMisc.IntegerListToXML(crit.ResourceTypeIDs)))
            cm.Parameters.AddWithValue("@IgnoreUserAccess", crit.IgnoreUserAccess)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace