﻿' Generated 16 Nov 2015 06:19 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Resources.Clashes

  <Serializable()> _
  Public Class ResourceClash
    Inherits OBBusinessBase(Of ResourceClash)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared ObjectGuidProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ObjectGuid, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key,
    Required(ErrorMessage:="ID required")>
  Public Property ObjectGuid() As Integer
      Get
        Return GetProperty(ObjectGuidProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ObjectGuidProperty, Value)
      End Set
    End Property

    Public Shared LResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.LResourceID, "L Resource")
    ''' <summary>
    ''' Gets and sets the L Resource value
    ''' </summary>
    <Display(Name:="L Resource", Description:=""),
    Required(ErrorMessage:="L Resource required")>
  Public Property LResourceID() As Integer
      Get
        Return GetProperty(LResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(LResourceIDProperty, Value)
      End Set
    End Property

    Public Shared LResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.LResourceBookingID, "L Resource Booking")
    ''' <summary>
    ''' Gets and sets the L Resource Booking value
    ''' </summary>
    <Display(Name:="L Resource Booking", Description:=""),
    Required(ErrorMessage:="L Resource Booking required")>
  Public Property LResourceBookingID() As Integer
      Get
        Return GetProperty(LResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(LResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared LStartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LStartDateTime, "L Start Date Time")
    ''' <summary>
    ''' Gets and sets the L Start Date Time value
    ''' </summary>
    <Display(Name:="L Start Date Time", Description:=""),
    Required(ErrorMessage:="L Start Date Time required")>
  Public Property LStartDateTime As DateTime?
      Get
        Return GetProperty(LStartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LStartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared LEndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.LEndDateTime, "L End Date Time")
    ''' <summary>
    ''' Gets and sets the L End Date Time value
    ''' </summary>
    <Display(Name:="L End Date Time", Description:=""),
    Required(ErrorMessage:="L End Date Time required")>
  Public Property LEndDateTime As DateTime?
      Get
        Return GetProperty(LEndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(LEndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared ResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceID, "Resource")
    ''' <summary>
    ''' Gets and sets the Resource value
    ''' </summary>
    <Display(Name:="Resource", Description:=""),
    Required(ErrorMessage:="Resource required")>
  Public Property ResourceID() As Integer
      Get
        Return GetProperty(ResourceIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingID, "Resource Booking")
    ''' <summary>
    ''' Gets and sets the Resource Booking value
    ''' </summary>
    <Display(Name:="Resource Booking", Description:=""),
    Required(ErrorMessage:="Resource Booking required")>
  Public Property ResourceBookingID() As Integer
      Get
        Return GetProperty(ResourceBookingIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingIDProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTimeBuffer, "Start Date Time Buffer")
    ''' <summary>
    ''' Gets and sets the Start Date Time Buffer value
    ''' </summary>
    <Display(Name:="Start Date Time Buffer", Description:=""),
    Required(ErrorMessage:="Start Date Time Buffer required")>
  Public Property StartDateTimeBuffer As DateTime?
      Get
        Return GetProperty(StartDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.StartDateTime, "Start Date Time")
    ''' <summary>
    ''' Gets and sets the Start Date Time value
    ''' </summary>
    <Display(Name:="Start Date Time", Description:=""),
    Required(ErrorMessage:="Start Date Time required")>
  Public Property StartDateTime As DateTime?
      Get
        Return GetProperty(StartDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(StartDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTime, "End Date Time")
    ''' <summary>
    ''' Gets and sets the End Date Time value
    ''' </summary>
    <Display(Name:="End Date Time", Description:=""),
    Required(ErrorMessage:="End Date Time required")>
  Public Property EndDateTime As DateTime?
      Get
        Return GetProperty(EndDateTimeProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeProperty, Value)
      End Set
    End Property

    Public Shared EndDateTimeBufferProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.EndDateTimeBuffer, "End Date Time Buffer")
    ''' <summary>
    ''' Gets and sets the End Date Time Buffer value
    ''' </summary>
    <Display(Name:="End Date Time Buffer", Description:=""),
    Required(ErrorMessage:="End Date Time Buffer required")>
  Public Property EndDateTimeBuffer As DateTime?
      Get
        Return GetProperty(EndDateTimeBufferProperty)
      End Get
      Set(ByVal Value As DateTime?)
        SetProperty(EndDateTimeBufferProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceBookingTypeID, "Resource Booking Type")
    ''' <summary>
    ''' Gets and sets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type", Description:=""),
    Required(ErrorMessage:="Resource Booking Type required")>
  Public Property ResourceBookingTypeID() As Integer
      Get
        Return GetProperty(ResourceBookingTypeIDProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(ResourceBookingTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingDescriptionProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingDescription, "Resource Booking Description")
    ''' <summary>
    ''' Gets and sets the Resource Booking Description value
    ''' </summary>
    <Display(Name:="Resource Booking Description", Description:="")>
  Public Property ResourceBookingDescription() As String
      Get
        Return GetProperty(ResourceBookingDescriptionProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingDescriptionProperty, Value)
      End Set
    End Property

    Public Shared ResourceBookingTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceBookingType, "Resource Booking Type")
    ''' <summary>
    ''' Gets and sets the Resource Booking Type value
    ''' </summary>
    <Display(Name:="Resource Booking Type", Description:="")>
  Public Property ResourceBookingType() As String
      Get
        Return GetProperty(ResourceBookingTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ResourceBookingTypeProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(ObjectGuidProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.ResourceBookingDescription.Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "Resource Clash")
        Else
          Return String.Format("Blank {0}", "Resource Clash")
        End If
      Else
        Return Me.ResourceBookingDescription
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewResourceClash() method.

    End Sub

    Public Shared Function NewResourceClash() As ResourceClash

      Return DataPortal.CreateChild(Of ResourceClash)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetResourceClash(dr As SafeDataReader) As ResourceClash

      Dim r As New ResourceClash()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ObjectGuidProperty, .GetInt32(0))
          LoadProperty(LResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(LResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(LStartDateTimeProperty, .GetValue(3))
          LoadProperty(LEndDateTimeProperty, .GetValue(4))
          LoadProperty(ResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(5)))
          LoadProperty(ResourceBookingIDProperty, Singular.Misc.ZeroNothing(.GetInt32(6)))
          LoadProperty(StartDateTimeBufferProperty, .GetValue(7))
          LoadProperty(StartDateTimeProperty, .GetValue(8))
          LoadProperty(EndDateTimeProperty, .GetValue(9))
          LoadProperty(EndDateTimeBufferProperty, .GetValue(10))
          LoadProperty(ResourceBookingTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(ResourceBookingDescriptionProperty, .GetString(12))
          LoadProperty(ResourceBookingTypeProperty, .GetString(13))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insResourceClash"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updResourceClash"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramObjectGuid As SqlParameter = .Parameters.Add("@ObjectGuid", SqlDbType.Int)
          paramObjectGuid.Value = GetProperty(ObjectGuidProperty)
          If Me.IsNew Then
            paramObjectGuid.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@LResourceID", GetProperty(LResourceIDProperty))
          .Parameters.AddWithValue("@LResourceBookingID", GetProperty(LResourceBookingIDProperty))
          .Parameters.AddWithValue("@LStartDateTime", (New SmartDate(GetProperty(LStartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@LEndDateTime", (New SmartDate(GetProperty(LEndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@ResourceID", GetProperty(ResourceIDProperty))
          .Parameters.AddWithValue("@ResourceBookingID", GetProperty(ResourceBookingIDProperty))
          .Parameters.AddWithValue("@StartDateTimeBuffer", (New SmartDate(GetProperty(StartDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@StartDateTime", (New SmartDate(GetProperty(StartDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTime", (New SmartDate(GetProperty(EndDateTimeProperty))).DBValue)
          .Parameters.AddWithValue("@EndDateTimeBuffer", (New SmartDate(GetProperty(EndDateTimeBufferProperty))).DBValue)
          .Parameters.AddWithValue("@ResourceBookingTypeID", GetProperty(ResourceBookingTypeIDProperty))
          .Parameters.AddWithValue("@ResourceBookingDescription", GetProperty(ResourceBookingDescriptionProperty))
          .Parameters.AddWithValue("@ResourceBookingType", GetProperty(ResourceBookingTypeProperty))

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(ObjectGuidProperty, paramObjectGuid.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delResourceClash"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@ObjectGuid", GetProperty(ObjectGuidProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace