﻿' Generated 13 Jun 2015 16:26 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Resources.ReadOnly
Imports OBLib.Productions.Base

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations

Namespace Resources

  Public Class ResourceBookingClean
    Inherits Resources.ResourceBookingBase(Of ResourceBookingClean)

#Region " Properties "

    Public Shared ResourceIDOverrideProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceIDOverride, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public Property ResourceIDOverride() As Integer?
      Get
        Return GetProperty(ResourceIDOverrideProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceIDOverrideProperty, Value)
      End Set
    End Property

    Public Shared HasAreaAccessProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasAreaAccess, "HasAreaAccess", False)
    ''' <summary>
    ''' Gets and sets the Is Shared Type value
    ''' </summary>
    <Display(Name:="HasAreaAccess", Description:="")>
    Public Property HasAreaAccess() As Boolean
      Get
        Return GetProperty(HasAreaAccessProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(HasAreaAccessProperty, Value)
      End Set
    End Property

    Public Shared PSASystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PSASystemID, "PSASystemID", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="PSASystemID", Description:="")>
    Public Property PSASystemID() As Integer?
      Get
        Return GetProperty(PSASystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PSASystemIDProperty, Value)
      End Set
    End Property

    Public Shared PSAProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.PSAProductionAreaID, "PSAProductionAreaID", Nothing)
    ''' <summary>
    ''' Gets and sets the Equipment value
    ''' </summary>
    <Display(Name:="PSAProductionAreaID", Description:="")>
    Public Property PSAProductionAreaID() As Integer?
      Get
        Return GetProperty(PSAProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(PSAProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared DisplayInBackgroundProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.DisplayInBackground, "DisplayInBackground", False)
    ''' <summary>
    ''' Gets and sets the Is Shared Type value
    ''' </summary>
    <Display(Name:="DisplayInBackground", Description:="")>
    Public Property DisplayInBackground() As Boolean
      Get
        Return GetProperty(DisplayInBackgroundProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(DisplayInBackgroundProperty, Value)
      End Set
    End Property

    Public Shared IsBeingEditedByUserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.IsBeingEditedByUserID, "IsBeingEditedByUserID", Nothing)
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="IsBeingEditedByUserID", Description:="")>
    Public Property IsBeingEditedByUserID() As Integer?
      Get
        Return GetProperty(IsBeingEditedByUserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(IsBeingEditedByUserIDProperty, Value)
      End Set
    End Property

    Public Shared IsBeingEditedByNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsBeingEditedByName, "IsBeingEditedByName", "")
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="IsBeingEditedByName", Description:="")>
    Public Property IsBeingEditedByName() As String
      Get
        Return GetProperty(IsBeingEditedByNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IsBeingEditedByNameProperty, Value)
      End Set
    End Property

    Public Shared IsBeingEditedByImagePathProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IsBeingEditedByImagePath, "IsBeingEditedByImagePath", "")
    ''' <summary>
    ''' Gets and sets the Vehicle value
    ''' </summary>
    <Display(Name:="IsBeingEditedByImagePath", Description:="")>
    Public Property IsBeingEditedByImagePath() As String
      Get
        Return GetProperty(IsBeingEditedByImagePathProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(IsBeingEditedByImagePathProperty, Value)
      End Set
    End Property

#End Region

#Region " Non-database Properties "

    Public Shared StartBufferMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.StartBufferMinutes, "StartBufferMinutes", 0)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="StartBufferMinutes", Description:="")>
    Public Property StartBufferMinutes() As Integer
      Get
        Return GetProperty(StartBufferMinutesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(StartBufferMinutesProperty, Value)
      End Set
    End Property

    Public Shared EndBufferMinutesProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.EndBufferMinutes, "EndBufferMinutes", 0)
    ''' <summary>
    ''' Gets and sets the Channel value
    ''' </summary>
    <Display(Name:="EndBufferMinutes", Description:="")>
    Public Property EndBufferMinutes() As Integer
      Get
        Return GetProperty(EndBufferMinutesProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(EndBufferMinutesProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared RoomBookingAreaListProperty As PropertyInfo(Of OBLib.Resources.RoomBookingAreaList) = RegisterProperty(Of OBLib.Resources.RoomBookingAreaList)(Function(c) c.RSResourceBookingAreaList, "Clash List")
    Public ReadOnly Property RSResourceBookingAreaList() As OBLib.Resources.RoomBookingAreaList
      Get
        If GetProperty(RoomBookingAreaListProperty) Is Nothing Then
          LoadProperty(RoomBookingAreaListProperty, New OBLib.Resources.RoomBookingAreaList)
        End If
        Return GetProperty(RoomBookingAreaListProperty)
      End Get
    End Property

#End Region

    Friend Shared Function GetResourceBookingClean(dr As SafeDataReader) As ResourceBookingClean

      Dim r As New ResourceBookingClean()
      r.Fetch(dr)
      Return r

    End Function

    Public Overrides Sub AddExtraParameters(Parameters As SqlClient.SqlParameterCollection)

    End Sub

    Public Overrides Sub FetchExtraProperties(sdr As Csla.Data.SafeDataReader)
      LoadProperty(StartBufferMinutesProperty, GetStartBufferMinutes)
      LoadProperty(EndBufferMinutesProperty, GetEndBufferMinutes)
      LoadProperty(ResourceIDOverrideProperty, ZeroNothing(sdr.GetInt32(32)))
      LoadProperty(HasAreaAccessProperty, sdr.GetBoolean(33))
      LoadProperty(PSASystemIDProperty, ZeroNothing(sdr.GetInt32(34)))
      LoadProperty(PSAProductionAreaIDProperty, ZeroNothing(sdr.GetInt32(35)))
      LoadProperty(DisplayInBackgroundProperty, sdr.GetBoolean(36))
      LoadProperty(IsBeingEditedByUserIDProperty, ZeroNothing(sdr.GetInt32(37)))
      LoadProperty(IsBeingEditedByNameProperty, sdr.GetString(38))
      LoadProperty(IsBeingEditedByImagePathProperty, sdr.GetString(39))
    End Sub

    Public Overrides Sub UpdateChildList()

    End Sub

    Public Overrides Sub DeleteChildren()

    End Sub

    Public Function GetStartBufferMinutes() As Integer
      If Me.StartDateTimeBuffer IsNot Nothing Then
        Return Me.StartDateTime.Value.Subtract(Me.StartDateTimeBuffer.Value).TotalMinutes
      Else
        Return 0
      End If
    End Function

    Public Function GetEndBufferMinutes() As Integer
      If Me.EndDateTimeBuffer IsNot Nothing Then
        Return Me.EndDateTimeBuffer.Value.Subtract(Me.EndDateTime.Value).TotalMinutes
      Else
        Return 0
      End If
    End Function

  End Class

End Namespace
