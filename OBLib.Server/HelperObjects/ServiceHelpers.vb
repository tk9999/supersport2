﻿Imports Singular
Imports System.Reflection
Imports System.ComponentModel

Public Class ServiceHelpers

  Public Class SlugServiceHelpers

    Public Shared Sub RefreshSlugs(GenRef As Int64?)

      Dim msc = OBLib.CommonData.Misc
      Dim ws As New OBLib.SlugsPublic.SlugsPublicSvcClient()
      Dim reto As OBLib.SlugsPublic.RetObj_GetSlugs = ws.GetSlugs(msc.SlugsPullKey.ToString, msc.SlugsCallerID, GenRef.ToString, True, True, True, True, True)
      SaveSlugs(GenRef, reto)


    End Sub

    Public Shared Function SaveSlugs(GenRef As Int64?, fetchResult As OBLib.SlugsPublic.RetObj_GetSlugs) As Singular.Web.Result

      Dim RBTable As DataTable = CreateSlugItemTableParameter(fetchResult.SlugList.ToList)
      Dim cmd As New Singular.CommandProc("InsProcsWeb.insSlugItems")
      cmd.Parameters.AddWithValue("@GenRefNumber", GenRef)
      cmd.Parameters.AddWithValue("@CurrentUserID", OBLib.Security.Settings.CurrentUserID)
      Dim slugsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      slugsParam.Name = "@SlugItems"
      slugsParam.SqlType = SqlDbType.Structured
      slugsParam.Value = RBTable
      cmd.Parameters.Add(slugsParam)
      cmd.FetchType = CommandProc.FetchTypes.None

      Try
        cmd = cmd.Execute(0)
        Return New Singular.Web.Result(True)
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

    End Function

    Private Shared Function CreateSlugItemTableParameter(slugs As List(Of OBLib.SlugsPublic.SlugItem)) As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("SlugID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SlugName", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SlugType", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("SlugDuration", GetType(System.String)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("TypeCode", GetType(System.String)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As OBLib.SlugsPublic.SlugItem In slugs
        Dim row As DataRow = RBTable.NewRow
        row("SlugID") = rb.SlugID
        row("SlugName") = rb.SlugName
        row("SlugType") = rb.SlugType
        row("SlugDuration") = rb.SlugDuration
        row("TypeCode") = rb.TypeCode
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

  End Class

  Public Class SSWSDHelpers

    'Public Shared Function CreateDataTable(Of T)(list As IEnumerable(Of T)) As DataTable
    '  Dim tp As Type = GetType(T)
    '  Dim allProperties = tp.GetProperties()
    '  Dim dataTable As DataTable = New DataTable()
    '  Dim propToInclude As New List(Of PropertyInfo)

    '  For Each propInfo As PropertyInfo In allProperties
    '    If propInfo.Name <> "ExtensionData" AndAlso propInfo.Name <> "_CreatorDetails" Then
    '      dataTable.Columns.Add(New DataColumn(propInfo.Name, propInfo.PropertyType))
    '      propToInclude.Add(propInfo)
    '    End If
    '  Next

    '  For Each entity As T In list
    '    Dim values(propToInclude.Count - 1) As Object '= {}
    '    For i As Integer = 0 To propToInclude.Count - 1 Step 1
    '      'If Singular.Reflection.IsBrowsable(propToInclude(i)) Then
    '      values(i) = propToInclude(i).GetValue(entity)
    '      'End If
    '    Next
    '    dataTable.Rows.Add(values)
    '  Next

    '  Return dataTable
    'End Function

    Public Shared Sub SendFeedChangeNotification(FeedID As Integer, FeedDate As DateTime, ChangeDescription As String)

      OBLib.OBMisc.LogICRServiceFeedback(OBLib.Security.Settings.CurrentUser.LoginName, FeedID, "SendChangeNotification", "Sending Feedback Started")
      Dim SSWSDService As SSWSD.SSWSDSVCClient
      Try
        SSWSDService = New SSWSD.SSWSDSVCClient
        Dim request As SSWSD.RetObj_SatOpsUpdateAlert = SSWSDService.SatOpsUpdateAlert("94208fee-5615-11e5-a800-005056943beb", "SSWSD_Sober", FeedID.ToString, FeedDate.ToString("dd MMM yyyy"), ChangeDescription)
        OBLib.OBMisc.LogICRServiceFeedback(OBLib.Security.Settings.CurrentUser.LoginName, FeedID, "SatOpsUpdateAlert", request.SvcFeedback)
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "EquipmentSchedule.vb", "SendChangeNotification", ex.Message)
      End Try

    End Sub

    Public Shared Sub UpdateSatOpsReportsByDate(StartDate As Date, EndDate As Date)

      Dim svc As SSWSD.SSWSDSVCClient
      Try
        svc = New SSWSD.SSWSDSVCClient
        Dim x As OBLib.SSWSD.RetObj_GetBookingItemList = svc.GetBookingItemList("94208fee-5615-11e5-a800-005056943beb", "SSWSD_Sober", StartDate.ToString("yyyy-MM-dd"), EndDate.ToString("yyyy-MM-dd"))
        If x.SvcFeedback <> "Success!" Then
          OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "SSWSDHelpers", "SendChangeNotification - Feedback", x.SvcFeedback)
        Else
          Dim ni As New OBLib.Scheduling.Equipment.IncidentItemList
          ni.UpdateIncidents(x.BookingItemList.ToList)
        End If
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "SSWSDHelpers", "SendChangeNotification - Error", ex.Message)
      End Try

    End Sub

    Public Shared Sub UpdateSatOpsReportsByFeedIDs(FeedIDsCSV As String)

      Dim svc As SSWSD.SSWSDSVCClient
      Try
        svc = New SSWSD.SSWSDSVCClient
        Dim x As OBLib.SSWSD.RetObj_GetBookingsByIDList = svc.GetBookingsByIDList("94208fee-5615-11e5-a800-005056943beb", "SSWSD_Sober", FeedIDsCSV, SSWSD.SvcParametersIDTypes.SRC)
        If x.SvcFeedback <> "Success!" Then
          OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "SSWSDHelpers", "SendChangeNotification - Feedback", x.SvcFeedback)
        Else
          'do stuff
          Dim incidentsListFeed As List(Of OBLib.SSWSD.IncidentItem) = x.BookingItemList.SelectMany(Function(d As OBLib.SSWSD.FeedItem) d._IncidentItemList).ToList
          Dim incidentsListFeedProduction As List(Of OBLib.SSWSD.IncidentItem) = x.BookingItemList.SelectMany(Function(d As OBLib.SSWSD.FeedItem) d._FeedProdItems.SelectMany(Function(c As OBLib.SSWSD.FeedProdItem) c._IncidentItemList).ToList).ToList
          Dim z As Object = Nothing
          'Dim feedIncidents As DataTable = OBMisc.CreateDataTable(Of OBLib.SSWSD.FeedItem)()
        End If
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError("SuperUser", "SSWSDHelpers", "SendChangeNotification - Error", ex.Message)
      End Try

    End Sub

  End Class

End Class
