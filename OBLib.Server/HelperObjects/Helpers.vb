﻿Imports OBLib.Maintenance.Invoicing.ReadOnly
Imports OBLib.Maintenance.Invoicing
Imports Singular
Imports Singular.Misc
Imports Csla
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Productions.Areas
Imports OBLib.Productions.Vehicles.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly
Imports System.ComponentModel
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Productions.Specs.Old
Imports OBLib.Productions.Schedules
Imports System.Data.SqlClient
Imports Csla.Data
Imports OBLib.Productions.Temp
Imports OBLib.Maintenance.Rooms
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Resources
Imports OBLib.Invoicing.ReadOnly
Imports OBLib.Invoicing.New
Imports System.Web
Imports OBLib.Productions.Old
Imports OBLib.Travel.OB.SnTs.BulkEdit

Namespace Helpers

  <Serializable()>
  Public Class ProductionScheduleHRLeave

    Public Property ProductionID As Integer
    Public Property HumanResourceName As String

    Public Sub New(ProductionID As Integer, HumanResourceName As String)

      Me.ProductionID = ProductionID
      Me.HumanResourceName = HumanResourceName

    End Sub


  End Class

  <Serializable>
  Public Class SiteInfo

    'Private mPage As System.Web.UI.Page
    Private mRequest As System.Web.HttpRequest

    Public ReadOnly Property IsPublicSite As Boolean
      Get
        If mRequest IsNot Nothing Then
          If mRequest.Url.AbsoluteUri.IndexOf("soberms.supersport.com") > 0 Then
            Return True
          End If
        End If
        Return False
      End Get
    End Property

    Public ReadOnly Property IsInternalSite As Boolean
      Get
        If mRequest IsNot Nothing Then
          If mRequest.Url.AbsoluteUri.IndexOf("internal-sober.supersport.com") > 0 Then
            Return True
          End If
        End If
        Return False
      End Get
    End Property

    Public ReadOnly Property IsQASite As Boolean
      Get
        If mRequest IsNot Nothing Then
          If mRequest.Url.AbsoluteUri.IndexOf("03rnb-qasober02") > 0 _
             Or mRequest.Url.AbsoluteUri.IndexOf("10.100.26.207") > 0 Then
            Return True
          End If
        End If
        Return False
      End Get
    End Property

    Public ReadOnly Property IsDevSite As Boolean
      Get
        Return Singular.Debug.InDebugMode
      End Get
    End Property

    Public ReadOnly Property InternalSitePath As String
      Get
        Return "https://internal-sober.supersport.com"
      End Get
    End Property

    Public ReadOnly Property PublicSitePath As String
      Get
        Return "https://soberms.supersport.com"
      End Get
    End Property

    Public ReadOnly Property QASitePath As String
      Get
        If mRequest.Url.AbsoluteUri.IndexOf("03rnb-qasober02") > 0 Then
          Return "http://03rnb-qasober02"
        ElseIf mRequest.Url.AbsoluteUri.IndexOf("10.100.26.207/Content") > 0 Then
          Return "http://10.100.26.207/Content"
        End If
        Return ""
      End Get
    End Property

    Public ReadOnly Property DebugSitePath As String
      Get
        Return "https://soberms.supersport.com"
      End Get
    End Property

    Public ReadOnly Property DevSitePath As String
      Get
        If mRequest IsNot Nothing Then
          Dim base As String = mRequest.Url.Scheme & "://" & mRequest.Url.Host
          Dim theRest = mRequest.Url.ToString.Remove(0, base.Length)
          Dim split As String() = theRest.Split("/")
          If split.Length > 1 Then
            Dim DevSiteName As String = split(1)
            Return base & "/" & DevSiteName
          End If
          Return base
        End If
        Return ""
      End Get
    End Property

    Public ReadOnly Property SitePath As String
      Get
        If IsPublicSite Then
          Return PublicSitePath
        ElseIf IsInternalSite Then
          Return InternalSitePath
        ElseIf IsQASite Then
          Return QASitePath
        ElseIf IsDevSite Then
          Return DevSitePath
        End If
        Return ""
      End Get
    End Property

    Public ReadOnly Property InternalPhotosPath As String
      Get
        If IsPublicSite Then
          Return PublicSitePath & "/Images/InternalPhotos/"
        ElseIf IsInternalSite Then
          Return InternalSitePath & "/Images/InternalPhotos/"
        ElseIf IsQASite Then
          Return QASitePath & "/Images/InternalPhotos/"
        ElseIf IsDevSite Then
          Return DevSitePath & "/Images/InternalPhotos/"
        End If
        Return ""
      End Get
    End Property

    Public ReadOnly Property AccreditationPhotosPath As String
      Get
        If IsPublicSite Then
          Return PublicSitePath & "/Images/Accreditation/"
        ElseIf IsInternalSite Then
          Return InternalSitePath & "/Images/Accreditation/"
        ElseIf IsQASite Then
          Return QASitePath & "/Images/Accreditation/"
        ElseIf IsDevSite Then
          Return DevSitePath & "/Images/Accreditation/"
        End If
        Return ""
      End Get
    End Property

    Public ReadOnly Property ProfilePhotosPath As String
      Get
        If IsPublicSite Then
          Return PublicSitePath & "/Images/Profile/"
        ElseIf IsInternalSite Then
          Return InternalSitePath & "/Images/Profile/"
        ElseIf IsQASite Then
          Return QASitePath & "/Images/Profile/"
        ElseIf IsDevSite Then
          Return DevSitePath & "/Images/Profile/"
        End If
        Return ""
      End Get
    End Property

    Public Sub New(Request As HttpRequest)
      'Page As System.Web.UI.Page, 
      'mPage = Page
      mRequest = Request
    End Sub

  End Class

  Public Class ApplyMessage
    Inherits OBBusinessBase(Of ApplyMessage)

    'Public Property ParentGuid As String
    Public Property AlertCss As String
    Public Property IconCssClass As String
    Public Property Title As String
    Public Property ShortDescription As String
    Public Property LongDescription As String
    'Public Property ChildList As List(Of ObjectMessage)

    Public Sub New(Title As String, ShortDescription As String, LongDescription As String, AlertCss As String, IconCssClass As String)
      Me.Title = Title
      Me.ShortDescription = ShortDescription
      Me.LongDescription = LongDescription
      Me.AlertCss = AlertCss
      Me.IconCssClass = IconCssClass
    End Sub

  End Class

  Public Class TimesheetsHelper

    Public Shared Function GetCurrentPaymentRun(SystemID As Integer) As OBLib.Invoicing.ReadOnly.ROPaymentRun

      Dim ropl As ROPaymentRunList = OBLib.Invoicing.ReadOnly.ROPaymentRunList.GetROLatestPaymentRunForSystem(SystemID)
      If ropl.Count = 1 Then
        Return ropl(0)
      End If
      Return Nothing

    End Function

    Public Shared Function GetCurrentTimesheetMonth() As OBLib.Maintenance.Timesheets.ReadOnly.ROTimesheetMonth

      Return OBLib.CommonData.Lists.ROTimesheetMonthList.GetCurrentTimesheetMonth()

    End Function

    Public Shared Function UpdateHumanResourceTimesheet(AtDate As Date?, HumanResourceID As Integer?, UserID As Integer?) As Singular.Web.Result

      Try
        Dim cmd As New Singular.CommandProc("cmdProcs.cmdTimesheetsRecalculateAll",
                                  New String() {"@AtDate", "@HumanResourceID", "@TimesheetRequirementID",
                                                "@CurrentUserID"},
                                  New Object() {NothingDBNull(AtDate), NothingDBNull(HumanResourceID), NothingDBNull(Nothing),
                                                UserID})
        cmd = cmd.Execute()
        Return New Singular.Web.Result(True)
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

    End Function

  End Class

  Public Class ResourceHelper

    Public Shared Function GetHumanResourceTimesheetRequirementStats(HumanResourceID As Integer?, ResourceID As Integer?,
                                                                     TimesheetRequirementID As Integer?,
                                                                     AtDate As Date?) As Singular.Web.Result

      Try
        Dim statList As OBLib.HR.ReadOnly.ROHRTimesheetRequirementStatList = OBLib.HR.ReadOnly.ROHRTimesheetRequirementStatList.GetROHRTimesheetRequirementStatList(HumanResourceID, TimesheetRequirementID, AtDate)
        Return New Singular.Web.Result(True) With {.Data = statList}
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try

    End Function

  End Class

  Public Class Templates

    <Serializable()>
    Public Class SystemAreaTemplate
      Inherits SingularBusinessBase(Of SystemAreaTemplate)

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Department", OBLib.Security.Settings.CurrentUser.SystemID)
      ''' <summary>
      ''' Gets and sets the System value
      ''' </summary>
      <Display(Name:="Sub-Department", Description:=""),
      Required(ErrorMessage:="Sub-Department required"),
      DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), Source:=DropDownWeb.SourceType.ViewModel)>
      Public Property SystemID() As Integer?
        Get
          Return GetProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          SetProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Area value
      ''' </summary>
      <Display(Name:="Area", Description:=""),
      Required(ErrorMessage:="Area required"),
      DropDownWeb(GetType(OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaList), Source:=DropDownWeb.SourceType.ViewModel)>
      Public Property ProductionAreaID() As Integer?
        Get
          Return GetProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          SetProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

    End Class

    <Serializable()>
    Public Class AddSystemAreaTemplateResponse
      Public Property Exists As Boolean = False
      Public Property Message As String = ""
      Public Property AddedBy As String = ""
      Public Property AddedDate As DateTime? = Nothing
    End Class

    <Serializable()>
    Public Class CarRentalCrew

      Public Property DriverInd As Boolean = False
      Public Property HumanResourceName As String = ""
      Public Property HumanResourceID As Integer
      Public Property Discipline As String = ""

      Public Sub New(DriverInd As Boolean, HumanResourceName As String, HumanResourceID As Integer, Discipline As String)
        Me.DriverInd = DriverInd
        Me.HumanResourceName = HumanResourceName
        Me.HumanResourceID = HumanResourceID
        Me.Discipline = Discipline
      End Sub

      Public Sub New()

      End Sub

    End Class

    <Serializable()>
    Public Class TimelineTemplate
      Inherits SingularBusinessBase(Of TimelineTemplate)

      Public Shared ProductionTimelineTypeIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.ProductionTimelineTypeID, Nothing)

      ''' <summary>
      ''' Gets and sets the Production Timeline Type value
      ''' </summary>
      <Display(Name:="Timeline Type", Description:="The type of timeline item"),
      Required(ErrorMessage:="Timeline Type required"),
      DropDownWeb(GetType(ROProductionAreaAllowedTimelineTypeList),
                  ValueMember:="ProductionTimelineTypeID", DisplayMember:="ProductionTimelineType", Source:=DropDownWeb.SourceType.ViewModel,
                  FilterMethodName:="FilterAllowedTimelineTypes")>
      Public Property ProductionTimelineTypeID() As Integer?
        Get
          Return GetProperty(ProductionTimelineTypeIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          SetProperty(ProductionTimelineTypeIDProperty, Value)
        End Set
      End Property

      Public Shared StartDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.StartDateTime, Now)
      ''' <summary>
      ''' Gets and sets the Start Date Time value
      ''' </summary>
      <Display(Name:="Start Date", Description:="The start date and time of this timeline item"),
      Required(ErrorMessage:="Start Date and Time required"),
      Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy", MaxDateProperty:="EndDateTime")>
      Public Property StartDateTime As DateTime?
        Get
          Return GetProperty(StartDateTimeProperty)
        End Get
        Set(ByVal Value As DateTime?)
          SetProperty(StartDateTimeProperty, Value)
        End Set
      End Property

      Public Shared EndDateTimeProperty As PropertyInfo(Of DateTime?) = RegisterSProperty(Of DateTime?)(Function(c) c.EndDateTime, Now)
      ''' <summary>
      ''' Gets and sets the End Date Time value
      ''' </summary>
      <Display(Name:="End Date", Description:="The end date and time of this timeline item"),
      Required(ErrorMessage:="End Date and Time required"),
      Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy", MinDateProperty:="StartDateTime")>
      Public Property EndDateTime As DateTime?
        Get
          Return GetProperty(EndDateTimeProperty)
        End Get
        Set(ByVal Value As DateTime?)
          SetProperty(EndDateTimeProperty, Value)
        End Set
      End Property

      Public Shared VehicleIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.VehicleID, Nothing)
      ''' <summary>
      ''' Gets and sets the Vehicle value
      ''' </summary>
      <Display(Name:="Vehicle", Description:="The vehicle that the pre/post travel applies to. If left blank then the pre/post travel applies to all the vehicles on the production. Must be left blank from non vehicle pre/post travel types"),
      DropDownWeb(GetType(ROProductionVehicleList), ValueMember:="VehicleID", DisplayMember:="VehicleName", Source:=DropDownWeb.SourceType.ViewModel)>
      Public Property VehicleID() As Integer?
        Get
          Return GetProperty(VehicleIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          SetProperty(VehicleIDProperty, Value)
        End Set
      End Property

      Public Shared CommentsProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Comments, "Comments", "")
      ''' <summary>
      ''' Gets and sets the Comments value
      ''' </summary>
      <Display(Name:="Comments", Description:="Any comment that the event planner wishes to add. Will be pulled through onto the production schedule"),
      StringLength(500, ErrorMessage:="Comments cannot be more than 500 characters")>
      Public Property Comments() As String
        Get
          Return GetProperty(CommentsProperty)
        End Get
        Set(ByVal Value As String)
          SetProperty(CommentsProperty, Value)
        End Set
      End Property

    End Class

    <Serializable()>
    Public Class HRTimelineSelect

      Public Property HRSelectList As List(Of HRSelect)
      Public Property TimelineSelectList As List(Of TimelineSelect)
      Public Property Mode As String = ""

      Public Sub New(ProductionSystemArea As Productions.Areas.ProductionSystemArea, Mode As String, HumanResourceID As Integer?)

        'If HumanResourceID IsNot Nothing Then
        '                           Where (HumanResourceID Is Nothing Or hrs.HumanResourceID = HumanResourceID)
        'End If

        Me.Mode = Mode

        Me.HRSelectList = (From hrs As Productions.Schedules.HRProductionSchedule In ProductionSystemArea.HRProductionScheduleList
                           Select New HRSelect(hrs)).ToList

        Me.TimelineSelectList = (From pts As Productions.Areas.ProductionTimeline In ProductionSystemArea.ProductionTimelineList
                                 Select New TimelineSelect(pts, HumanResourceID)).ToList

      End Sub

    End Class

    Public Class HRSelect

      <Browsable(False)>
      Public Property HRProductionSchedule As Productions.Schedules.HRProductionSchedule

      Public Property SelectInd As Boolean = False

      Public ReadOnly Property HumanResourceID As Integer
        Get
          Return HRProductionSchedule.HumanResourceID
        End Get
      End Property

      <Display(Name:="Name")>
      Public ReadOnly Property HumanResource As String
        Get
          Return HRProductionSchedule.HumanResource
        End Get
      End Property

      <Display(Name:="Clashes")>
      Public ReadOnly Property Clashes As String
        Get
          Return HRProductionSchedule.AllClashes
        End Get
      End Property

      <ClientOnly()>
      Public Property Visible As Boolean = False

      Public Sub New(HRProductionSchedule As Productions.Schedules.HRProductionSchedule)
        Me.HRProductionSchedule = HRProductionSchedule
      End Sub

    End Class

    Public Class TimelineSelect

      <Browsable(False)>
      Public Property ProductionTimeline As ProductionTimeline

      Public Property SelectInd As Boolean = False

      <Display(Name:="Timeline Type")>
      Public ReadOnly Property ProductionTimelineType As String
        Get
          Return ProductionTimeline.ProductionTimelineType
        End Get
      End Property

      <Display(Name:="Date"), DateField(FormatString:="dd MMM yyyy")>
      Public ReadOnly Property TimesheetDate As Date
        Get
          Return ProductionTimeline.StartDateTime
        End Get
      End Property

      <Display(Name:="Start Time"), DateField(FormatString:="HH:mm")>
      Public ReadOnly Property StartTime As DateTime
        Get
          Return ProductionTimeline.StartDateTime
        End Get
      End Property

      <Display(Name:="End Time"), DateField(FormatString:="HH:mm")>
      Public ReadOnly Property EndTime As DateTime
        Get
          Return ProductionTimeline.EndDateTime
        End Get
      End Property

      <ClientOnly()>
      Public Property Visible As Boolean = False

      Public Sub New(ProductionTimeline As ProductionTimeline, HumanResourceID As Integer?)
        Me.ProductionTimeline = ProductionTimeline
      End Sub

    End Class

    <Serializable()>
    Public Class SelectTimeline
      Inherits SingularBusinessBase(Of SelectTimeline)

      Public Shared SelectedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Selected, "Selected", False)
      ''' <summary>
      ''' Gets and sets the Exclude value
      ''' </summary>
      <Display(Name:="Selected")>
      Public Property Selected() As Boolean
        Get
          Return GetProperty(SelectedProperty)
        End Get
        Set(ByVal Value As Boolean)
          SetProperty(SelectedProperty, Value)
        End Set
      End Property
      <Display(Name:="Guid")>
      Public Property TimelineGuid As String
      <Display(Name:="TypeID")>
      Public Property ProductionTimelineID As Integer?
      <Display(Name:="TypeID")>
      Public Property ProductionTimelineTypeID As Integer?
      <Display(Name:="Type")>
      Public Property ProductionTimelineType As String
      <Display(Name:="Date")>
      Public Property TimelineDate As Date
      <Display(Name:="Start Time")>
      Public Property StartDateTime As DateTime
      <Display(Name:="End Time")>
      Public Property EndDateTime As DateTime
      <Display(Name:="VehicleID")>
      Public Property VehicleID As Integer?
      <Display(Name:="Vehicle Name")>
      Public Property VehicleName As String
      <Display(Name:="SystID")>
      Public Property SystemID As Integer?
      <Display(Name:="AreaID")>
      Public Property ProductionAreaID As Integer?

      Public Sub New(ProductionTimeline As ProductionTimeline)

        TimelineGuid = ProductionTimeline.Guid.ToString
        ProductionTimelineID = ProductionTimeline.ProductionTimelineID
        ProductionTimelineTypeID = ProductionTimeline.ProductionTimelineTypeID
        ProductionTimelineType = ProductionTimeline.ProductionTimelineType
        TimelineDate = ProductionTimeline.TimelineDate
        StartDateTime = ProductionTimeline.StartDateTime
        EndDateTime = ProductionTimeline.EndDateTime
        VehicleID = ProductionTimeline.VehicleID
        VehicleName = ProductionTimeline.VehicleName
        SystemID = ProductionTimeline.SystemID
        ProductionAreaID = ProductionTimeline.ProductionAreaID

      End Sub

      Public Sub New()

      End Sub

    End Class

    <Serializable()>
    Public Class UnSelectedTimeline
      Inherits SingularBusinessBase(Of UnSelectedTimeline)

      <Browsable(False)>
      Public Property ProductionTimeline As ProductionTimeline

      <DefaultValue(False)>
      Public Property Selected As Boolean = False

      <Browsable(False)>
      Public Property ContractTypeID As Integer?

      Public ReadOnly Property ProductionTimelineGuid As String
        Get
          If ProductionTimeline IsNot Nothing Then
            Return ProductionTimeline.Guid.ToString
          End If
          Return Nothing
        End Get
      End Property

      <Display(Name:="Timeline Type")>
      Public ReadOnly Property ProductionTimelineTypeID As Integer
        Get
          If ProductionTimeline IsNot Nothing Then
            Return ProductionTimeline.ProductionTimelineTypeID
          End If
          Return Nothing
        End Get
      End Property

      <Display(Name:="Timeline Type")>
      Public ReadOnly Property ProductionTimelineType As String
        Get
          If ProductionTimeline IsNot Nothing Then
            Return ProductionTimeline.ProductionTimelineType
          End If
          Return Nothing
        End Get
      End Property

      <Display(Name:="Date"), DateField(FormatString:="dd MMM yyyy")>
      Public ReadOnly Property TimesheetDate As Date
        Get
          If ProductionTimeline IsNot Nothing Then
            Return ProductionTimeline.StartDateTime
          End If
          Return Nothing
        End Get
      End Property

      <Display(Name:="Start Date"), DateField(FormatString:="dd MMM yyyy")>
      Public ReadOnly Property StartDate As DateTime
        Get
          If ProductionTimeline IsNot Nothing Then
            Return ProductionTimeline.StartDateTime
          End If
          Return Nothing
        End Get
      End Property

      <Display(Name:="Start Time"), DateField(FormatString:="HH:mm")>
      Public ReadOnly Property StartDateTime As DateTime
        Get
          If ProductionTimeline IsNot Nothing Then
            Return ProductionTimeline.StartDateTime
          End If
          Return Nothing
        End Get
      End Property

      <Display(Name:="End Time"), DateField(FormatString:="HH:mm")>
      Public ReadOnly Property EndDateTime As DateTime
        Get
          If ProductionTimeline IsNot Nothing Then
            Return ProductionTimeline.EndDateTime
          End If
          Return Nothing
        End Get
      End Property

      <Display(Name:="Month Closed")>
      Public ReadOnly Property TimesheetMonthClosedInd As Boolean
        Get
          If Not CompareSafe(ContractTypeID, CInt(CommonData.Enums.ContractType.Freeleancer)) Then
            Dim tm As OBLib.Maintenance.General.ReadOnly.ROTimesheetMonthOld = OBLib.CommonData.Lists.ROTimesheetMonthListOld.GetItem(StartDate.Date)
            If tm IsNot Nothing AndAlso tm.ClosedInd Then
              Return True
            End If
          End If
          Return False
        End Get
      End Property

      <DefaultValue(True)>
      Public Property Visible As Boolean = True

      Public Sub New(ProductionTimeline As ProductionTimeline, ContractTypeID As Integer?)
        Me.ProductionTimeline = ProductionTimeline
        Me.ContractTypeID = ContractTypeID
      End Sub

      Public Sub New()

      End Sub

    End Class

    <Serializable()>
    Public Class HRSchedule

      Public Property Expanded As Boolean = True
      Public Property HumanResourceID As Integer
      Public Property HumanResource As String = ""
      Public Property CrewTypeID As Integer
      Public Property CrewType As String = ""
      Public Property Disciplines As String = ""
      Public Property CityID As Integer?
      Public Property CityCode As String
      Public Property Local As Boolean
      Public Property HRScheduleTemplateList As List(Of HRScheduleTemplate)

      ', CityID As Integer?, CityCode As String, Local As Boolean
      Public Sub New(HumanResourceID As Integer, HumanResource As String, VenueCityID As Integer?, HRScheduleTemplateList As IEnumerable(Of HRScheduleTemplate))

        Dim ROHR As OBLib.HR.ReadOnly.ROHumanResource = CommonData.Lists.ROHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, HumanResourceID)).FirstOrDefault
        If ROHR IsNot Nothing Then
          Dim City As OBLib.Maintenance.Locations.ReadOnly.ROCity = CommonData.Lists.ROCityList.Where(Function(d) CompareSafe(d.CityID, ROHR.CityID)).FirstOrDefault
          Me.CityID = City.CityID
          Me.CityCode = City.CityCode
          If CompareSafe(VenueCityID, ROHR.CityID) Then
            Me.Local = True
          Else
            Me.Local = False
          End If
        End If
        Me.HumanResourceID = HumanResourceID
        Me.HumanResource = HumanResource
        Me.HRScheduleTemplateList = HRScheduleTemplateList.OrderBy(Function(d) d.StartDateTime).ToList()

        Dim DisciplineIDs As List(Of Integer?) = Me.HRScheduleTemplateList.Select(Function(d As HRScheduleTemplate) d.DisciplineID).Distinct().ToList
        DisciplineIDs.ForEach(Sub(DisciplineID)
                                Me.Disciplines &= IIf(Me.Disciplines.Length = 0, "", ", ") & CommonData.Lists.RODisciplineList.Where(Function(d) d.DisciplineID = DisciplineID).FirstOrDefault.Discipline
                              End Sub)

        'If DisciplineIDs.Count > 1 Then
        'Else
        '  Me.Disciplines = CommonData.Lists.RODisciplineList.Where(Function(d) d.DisciplineID = DisciplineID).FirstOrDefault.Discipline
        'End If
        'If Me.HRScheduleTemplateList.Where(Function(d) d.DisciplineID = CommonData.Enums.Discipline.EventManager).Count > 1 Then
        'End If

      End Sub

    End Class

    <Serializable()>
    Public Class HRScheduleTemplate

      Public Property HumanResourceID As Integer?
      Public Property HumanResource As String = ""
      Public Property ProductionHumanResourceID As Integer?
      Public Property DisciplineID As Integer?
      Public Property Discipline As String = ""
      Public Property VehicleID As Integer?
      Public Property ProductionTimelineID As Integer?
      Public Property ProductionTimelineTypeID As Integer?
      Public Property ProductionTimelineType As String
      Public Property PreTransmissionInd As Boolean?
      Public Property FreeInd As Boolean
      <DateField(FormatString:="dd MMM yyyy")>
      Public Property TimesheetDate As Date?
      <DateField(FormatString:="HH:mm")>
      Public Property StartDateTime As DateTime?
      <DateField(FormatString:="HH:mm")>
      Public Property EndDateTime As DateTime?
      Public Property ProductionTimeline As ProductionTimeline
      Public Property ProductionHumanResource As ProductionHumanResource
      Public Property CityID As Integer
      Public Property CityCode As String
      Public Property VenueCityID As Integer?
      Public Property Local As Boolean = False
      Public Property Remove As Boolean = False
      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?

      Public Sub New(HumanResourceID As Integer?, HumanResource As String,
                     DisciplineID As Integer?, VehicleID As Integer?,
                     ProductionTimelineTypeID As Integer,
                     StartDateTime As DateTime?, EndDateTime As DateTime?,
                     ProductionTimeline As ProductionTimeline, ProductionHumanResource As ProductionHumanResource)


        ',ProductionTimelineType As String,
        'PreTransmissionInd As Boolean?, FreeInd As Boolean,
        'ProductionTimelineID As Integer?, 
        ' ProductionHumanResourceID As Integer?,
        ', CityID As Integer?, CityCode As String, VenueCityID As Integer?
        'Me.ProductionHumanResourceID = ProductionHumanResourceID
        'Me.ProductionTimelineID = ProductionTimelineID
        Me.HumanResourceID = HumanResourceID
        Me.HumanResource = HumanResource
        Me.DisciplineID = DisciplineID
        Dim ROD As OBLib.Maintenance.General.ReadOnly.RODiscipline = CommonData.Lists.RODisciplineList.Where(Function(d) CompareSafe(d.DisciplineID, Me.DisciplineID)).FirstOrDefault
        If ROD IsNot Nothing Then
          Me.Discipline = ROD.Discipline
        End If
        Dim ROPTT As OBLib.Maintenance.Productions.ReadOnly.ROProductionTimelineType = CommonData.Lists.ROProductionTimelineTypeList.Where(Function(d) CompareSafe(d.ProductionTimelineTypeID, ProductionTimelineTypeID)).FirstOrDefault
        If ROPTT IsNot Nothing Then
          Me.ProductionTimelineType = ROPTT.ProductionTimelineType
        End If
        Me.VehicleID = VehicleID
        Me.ProductionTimelineTypeID = ProductionTimelineTypeID
        Me.PreTransmissionInd = PreTransmissionInd
        Me.FreeInd = FreeInd
        Me.StartDateTime = StartDateTime
        Me.EndDateTime = EndDateTime
        Me.ProductionTimeline = ProductionTimeline
        Me.ProductionHumanResource = ProductionHumanResource
        Me.SystemID = ProductionTimeline.SystemID
        Me.ProductionAreaID = ProductionTimeline.ProductionAreaID
        TimesheetDate = ProductionTimeline.CalculateTimesheetDate()

      End Sub

    End Class

    '<Serializable()>
    'Public Class StudioHRSchedule

    '  Public Property Expanded As Boolean = True
    '  Public Property HumanResourceID As Integer
    '  Public Property HumanResource As String = ""
    '  Public Property CrewTypeID As Integer
    '  Public Property CrewType As String = ""
    '  Public Property Disciplines As String = ""
    '  Public Property StudioHRScheduleTemplateList As List(Of StudioHRScheduleDetail)

    '  Public Sub New(HumanResourceID As Integer, HumanResource As String, StudioHRScheduleTemplateList As IEnumerable(Of StudioHRScheduleDetail))
    '    Dim ROHR As OBLib.HR.ReadOnly.ROHumanResource = CommonData.Lists.ROHumanResourceList.Where(Function(d) CompareSafe(d.HumanResourceID, HumanResourceID)).FirstOrDefault
    '    Me.HumanResourceID = HumanResourceID
    '    Me.HumanResource = HumanResource
    '    Me.StudioHRScheduleTemplateList = StudioHRScheduleTemplateList.OrderBy(Function(d) d.StartDateTime).ToList()
    '    Dim DisciplineIDs As List(Of Integer?) = Me.StudioHRScheduleTemplateList.Select(Function(d As StudioHRScheduleDetail) d.DisciplineID).Distinct().ToList
    '    DisciplineIDs.ForEach(Sub(DisciplineID)
    '                            Me.Disciplines &= IIf(Me.Disciplines.Length = 0, "", ", ") & CommonData.Lists.RODisciplineList.Where(Function(d) d.DisciplineID = DisciplineID).FirstOrDefault.Discipline
    '                          End Sub)
    '  End Sub

    'End Class

    '<Serializable()>
    'Public Class StudioHRScheduleDetail

    '  Public Property HumanResourceID As Integer?
    '  Public Property HumanResource As String = ""
    '  Public Property ProductionHumanResourceID As Integer?
    '  Public Property DisciplineID As Integer?
    '  Public Property Discipline As String = ""
    '  Public Property VehicleID As Integer?
    '  Public Property ProductionTimelineID As Integer?
    '  Public Property ProductionTimelineTypeID As Integer?
    '  Public Property ProductionTimelineType As String
    '  Public Property PreTransmissionInd As Boolean?
    '  <DateField(FormatString:="dd MMM yyyy")>
    '  Public Property ScheduleDate As Date?
    '  <DateField(FormatString:="HH:mm")>
    '  Public Property StartDateTime As DateTime?
    '  <DateField(FormatString:="HH:mm")>
    '  Public Property EndDateTime As DateTime?
    '  Public Property ProductionTimeline As StudioProductionTimeline
    '  Public Property ProductionHumanResource As StudioProductionHumanResource
    '  Public Property Remove As Boolean = False
    '  Public Property SystemID As Integer?
    '  Public Property ProductionAreaID As Integer?

    '  Public Sub New(HumanResourceID As Integer?, HumanResource As String,
    '                 DisciplineID As Integer?, VehicleID As Integer?,
    '                 ProductionTimelineTypeID As Integer,
    '                 StartDateTime As DateTime?, EndDateTime As DateTime?,
    '                 ProductionTimeline As StudioProductionTimeline, ProductionHumanResource As StudioProductionHumanResource, SystemID As Integer?, ProductionAreaID As Integer?)

    '    Me.HumanResourceID = HumanResourceID
    '    Me.HumanResource = HumanResource
    '    Me.DisciplineID = DisciplineID
    '    Dim ROD As OBLib.Maintenance.General.ReadOnly.RODiscipline = CommonData.Lists.RODisciplineList.Where(Function(d) CompareSafe(d.DisciplineID, Me.DisciplineID)).FirstOrDefault
    '    If ROD IsNot Nothing Then
    '      Me.Discipline = ROD.Discipline
    '    End If
    '    Dim ROPTT As OBLib.Maintenance.Productions.ReadOnly.ROProductionTimelineType = CommonData.Lists.ROProductionTimelineTypeList.Where(Function(d) CompareSafe(d.ProductionTimelineTypeID, ProductionTimelineTypeID)).FirstOrDefault
    '    If ROPTT IsNot Nothing Then
    '      Me.ProductionTimelineType = ROPTT.ProductionTimelineType
    '    End If
    '    Me.VehicleID = VehicleID
    '    Me.ProductionTimelineTypeID = ProductionTimelineTypeID
    '    Me.PreTransmissionInd = PreTransmissionInd
    '    Me.StartDateTime = StartDateTime
    '    Me.EndDateTime = EndDateTime
    '    Me.ProductionTimeline = ProductionTimeline
    '    Me.ProductionHumanResource = ProductionHumanResource
    '    Me.SystemID = SystemID
    '    Me.ProductionAreaID = ProductionAreaID
    '    ScheduleDate = Me.StartDateTime

    '  End Sub

    'End Class

    <Serializable()>
    Public Class DisciplineGroup

      Public Property Expanded As Boolean = False

      Private mUpdateInd As Boolean
      <Display(Name:="Update?")>
      Public Property UpdateInd As Boolean
        Get
          Return mUpdateInd
        End Get
        Set(value As Boolean)
          If mUpdateInd <> value Then
            mUpdateInd = value
            UpdateChildren(mUpdateInd)
          End If
        End Set
      End Property

      Private mDisciplineID As Integer = 0
      Public ReadOnly Property DisciplineID As Integer
        Get
          Return mDisciplineID
        End Get
      End Property

      Private mDiscipline As String = ""
      <Display(Name:="Discipline")>
      Public ReadOnly Property Discipline As String
        Get
          Return mDiscipline
        End Get
      End Property

      Public Property HumanResources As List(Of HumanResourceItem) = New List(Of HumanResourceItem)

      Public Sub UpdateChildren(UpdateInd As Boolean)
        HumanResources.ForEach(Sub(Child)
                                 If Not Child.HiddenInd Then
                                   Child.UpdateInd = UpdateInd
                                 End If
                               End Sub)
      End Sub

      Public Sub New(DisciplineID As Integer, Discipline As String)

        mDisciplineID = DisciplineID
        mDiscipline = Discipline

      End Sub

    End Class

    <Serializable()>
    Public Class HumanResourceItem

      <Display(Name:="Update")>
      Public Property UpdateInd As Boolean

      <Display(Name:="Human Resource ID")>
      Public Property HumanResourceID As Integer

      <Display(Name:="Human Resource")>
      Public Property HumanResource As String

      <Display(Name:="Discipline ID")>
      Public Property DisciplineID As Integer

      <Display(Name:="Discipline")>
      Public Property Discipline As String

      <DisplayName("Has Errors?")>
      Public Property HasErrorsInd As Boolean

      <DisplayName("Hidden?")>
      Public Property HiddenInd As Boolean

      '<DisplayName("Errors")>
      'Public Property Errors As String

      Public Sub New(HumanResourceID As Integer, HumanResource As String, DisciplineID As Integer, Discipline As String, Errors As String, HasErrorsInd As Boolean)

        Me.HumanResourceID = HumanResourceID
        Me.HumanResource = HumanResource
        Me.DisciplineID = DisciplineID
        Me.Discipline = Discipline
        'Me.Errors = Errors
        Me.HasErrorsInd = HasErrorsInd
        Me.HiddenInd = False

      End Sub

    End Class

    <Serializable()>
    Public Class ProductionTimelineItem

      Private mUpdateInd As Boolean = False
      Public Property UpdateInd As Boolean
        Get
          Return mUpdateInd
        End Get
        Set(value As Boolean)
          If mUpdateInd <> value Then
            mUpdateInd = value
          End If
        End Set
      End Property

      Private mProductionTimelineID As Integer = 0
      Public ReadOnly Property ProductionTimelineID As Integer
        Get
          Return mProductionTimelineID
        End Get
      End Property

      Private mDay As Date = Now
      Public ReadOnly Property Day As Date
        Get
          Return mDay
        End Get
      End Property

      Private mTimelineType As String = ""
      <Display(Name:="Timeline Type")>
      Public ReadOnly Property TimelineType As String
        Get
          Return mTimelineType
        End Get
      End Property

      Private mTimelineTypeID As Integer = 0
      Public ReadOnly Property TimelineTypeID As Integer
        Get
          Return TimelineTypeID
        End Get
      End Property

      Private mStartDateTime As DateTime = Now
      <Display(Name:="Start"), Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy")>
      Public Property StartDateTime As DateTime
        Get
          Return mStartDateTime
        End Get
        Set(value As DateTime)
          mStartDateTime = value
        End Set
      End Property

      Private mEndDateTime As DateTime = Now
      <Display(Name:="End"), Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy")>
      Public Property EndDateTime As DateTime
        Get
          Return mEndDateTime
        End Get
        Set(value As DateTime)
          mEndDateTime = value
        End Set
      End Property

      Private mStartTime As String
      Public Property StartTime As String
        Get
          Return mStartTime
        End Get
        Set(value As String)
          mStartTime = GetTimeSpanFromString(value, TimeStringSingleNumberType.Hours).ToString
        End Set
      End Property

      Private mEndTime As String
      Public Property EndTime As String
        Get
          Return mEndTime
        End Get
        Set(value As String)
          mEndTime = GetTimeSpanFromString(value, TimeStringSingleNumberType.Hours).ToString
        End Set
      End Property

      Private mProductionTimeline As ProductionTimeline
      <Browsable(False)>
      Public ReadOnly Property ProductionTimeline As ProductionTimeline
        Get
          Return mProductionTimeline
        End Get
      End Property

      Public Sub New(ProductionTimelineID As Integer, Day As Date,
                     TimelineType As String, TimelineTypeID As Integer,
                     StartDateTime As DateTime, EndDateTime As DateTime,
                     StartTime As String, EndTime As String,
                     ByRef ProductionTimeline As ProductionTimeline)

        mProductionTimelineID = ProductionTimelineID
        mDay = Day
        mTimelineType = TimelineType
        mTimelineTypeID = TimelineTypeID
        mStartDateTime = StartDateTime
        mEndDateTime = EndDateTime
        mStartTime = StartTime
        mEndTime = EndTime
        mProductionTimeline = ProductionTimeline

      End Sub

    End Class

    <Serializable()>
    Public Class ProductionTimelineDay

      <DefaultValue(False), Singular.DataAnnotations.ClientOnly()>
      Public Property Expanded As Boolean = False

      Private mUpdateInd As Boolean = False
      <DisplayName("Update")>
      Public Property UpdateInd As Boolean
        Get
          Return mUpdateInd
        End Get
        Set(value As Boolean)
          If mUpdateInd <> value Then
            mUpdateInd = value
            UpdateChildren(mUpdateInd)
          End If
        End Set
      End Property

      Private mDay As Date = Now
      <Display(Name:="Day"), Singular.DataAnnotations.DateField(FormatString:="dd MMM yyyy")>
      Public ReadOnly Property Day As Date
        Get
          Return mDay
        End Get
      End Property

      Private mDayString As String = mDay.ToString("dd MMMM yyyy")
      Public ReadOnly Property DayString As String
        Get
          Return mDayString
        End Get
      End Property

      Public Property ProductionTimelineItems As List(Of ProductionTimelineItem) = New List(Of ProductionTimelineItem)

      Public Sub UpdateChildren(UpdateInd As Boolean)
        ProductionTimelineItems.ForEach(Sub(Child)
                                          Child.UpdateInd = UpdateInd
                                        End Sub)
      End Sub

      Public Sub New(Day As Date, DayString As String)

        mDay = Day
        mDayString = DayString

      End Sub

    End Class

    <Serializable()>
    Public Class ProductionHumanResourceBulkEditDetails

      Private mDisciplineGroupList As List(Of DisciplineGroup)
      Public ReadOnly Property DisciplineGroupList As List(Of DisciplineGroup)
        Get
          Return mDisciplineGroupList
        End Get
      End Property

      Private mHumanResourceItemList As List(Of HumanResourceItem)
      Public ReadOnly Property HumanResourceItemList As List(Of HumanResourceItem)
        Get
          Return mHumanResourceItemList
        End Get
      End Property

      Private mProductionTimelineDayList As List(Of ProductionTimelineDay)
      Public ReadOnly Property ProductionTimelineDayList As List(Of ProductionTimelineDay)
        Get
          Return mProductionTimelineDayList
        End Get
      End Property

      Private mProductionTimelineItemList As List(Of ProductionTimelineItem)
      Public ReadOnly Property ProductionTimelineItemList As List(Of ProductionTimelineItem)
        Get
          Return mProductionTimelineItemList
        End Get
      End Property

      Private Sub BuildProductionHumanResources(Production As Productions.Old.Production)

        mDisciplineGroupList = New List(Of DisciplineGroup)

        'Get the distinct discipline id's
        Dim disciplines As List(Of Integer?) = (From data In Production.ProductionSystemAreaList(0).ProductionHumanResourceList
                                                Where (Not IsNullNothing(data.DisciplineID, True))
                                                Select data.DisciplineID).Distinct.ToList

        'for each unique id, get the description and create a custom discipline group object
        disciplines.ForEach(Sub(disc)
                              If mDisciplineGroupList.Where(Function(dis) dis.DisciplineID = disc).FirstOrDefault Is Nothing Then
                                mDisciplineGroupList.Add(New DisciplineGroup(disc, CommonData.Lists.RODisciplineList.GetItem(disc).Discipline))
                              End If
                            End Sub)


        'get the disciplines for each human resource
        Dim HumanResources As List(Of HumanResourceItem) = (From data In Production.ProductionSystemAreaList(0).ProductionHumanResourceList
                                                            Join hr In OBLib.CommonData.Lists.ROHumanResourceList On hr.HumanResourceID Equals data.HumanResourceID
                                                            Where Not IsNullNothing(data.HumanResourceID, True) AndAlso Not IsNullNothing(data.DisciplineID)
                                                            Group data By HumanResourceID = data.HumanResourceID, HumanResource = hr.HumanResource, DisciplineID = data.DisciplineID, IsValid = data.IsValid, Errors = data.GetErrorsAsHTMLString Into Group
                                                            Select New HumanResourceItem(HumanResourceID,
                                                                                          HumanResource,
                                                                                          DisciplineID,
                                                                                          OBLib.CommonData.Lists.RODisciplineList.GetItem(DisciplineID).Discipline,
                                                                                          IIf(Errors Is Nothing, "", Errors),
                                                                                          Not IsValid
                                                                                        )).OrderBy(Function(d) d.HumanResource).ToList


        'Remove the drivers second postion
        HumanResources.Select(Function(d) d.HumanResourceID).Distinct.ToList.ForEach(Sub(hr)
                                                                                       'get this hr's disciplines
                                                                                       Dim DisciplineList As List(Of HumanResourceItem) = HumanResources.Where(Function(s) s.HumanResourceID = hr).ToList
                                                                                       'if he/she has more than 1
                                                                                       If DisciplineList.Count > 1 Then
                                                                                         'remove the one that is not driver
                                                                                         HumanResources.Remove(HumanResources.Where(Function(dupe) dupe.DisciplineID <> 5 AndAlso dupe.HumanResourceID = hr).FirstOrDefault())
                                                                                       End If
                                                                                     End Sub)

        'now we have a distinct list of disciplines
        HumanResources.ForEach(Sub(hr)
                                 mDisciplineGroupList.Where(Function(fr) fr.DisciplineID = hr.DisciplineID).FirstOrDefault().HumanResources.Add(hr)
                               End Sub)

        mHumanResourceItemList = HumanResources

      End Sub

      Private Sub BuildProductionDays(Production As OBLib.Productions.Old.Production)

        If Production.ProductionSystemAreaList(0).ProductionTimelineList IsNot Nothing Then

          mProductionTimelineDayList = New List(Of ProductionTimelineDay)
          mProductionTimelineItemList = New List(Of ProductionTimelineItem)

          Dim TimelineDays As List(Of ProductionTimelineDay) = (From data In Production.ProductionSystemAreaList(0).ProductionTimelineList
                                                                Order By data.StartDateTime.Value.Date
                                                                Group data By PTDay = data.StartDateTime.Value.Date Into Group
                                                                Select New ProductionTimelineDay(PTDay, PTDay.Date.ToString("dd MMMM yyy"))).ToList

          Dim Timelines As List(Of ProductionTimelineItem) = (From data In Production.ProductionSystemAreaList(0).ProductionTimelineList
                                                              Join type In OBLib.CommonData.Lists.ROProductionTimelineTypeList On type.ProductionTimelineTypeID Equals data.ProductionTimelineTypeID
                                                              Order By data.StartDateTime
                                                              Group data By PTID = data.ProductionTimelineID, PTDay = data.StartDateTime,
                                                                            PTType = type.ProductionTimelineType, PTStartTime = data.StartDateTime,
                                                                            PTEndTime = data.EndDateTime, PTimelineTypeID = data.ProductionTimelineTypeID,
                                                                            PT = data Into Group
                                                              Select New ProductionTimelineItem(
                                                                                                 PTID, PTDay, PTType, PTimelineTypeID,
                                                                                                 PTStartTime,
                                                                                                 PTEndTime,
                                                                                                 Singular.Misc.GetTimeSpanFromString(PTStartTime, Singular.Misc.TimeStringSingleNumberType.Hours).ToString,
                                                                                                 Singular.Misc.GetTimeSpanFromString(PTEndTime, Singular.Misc.TimeStringSingleNumberType.Hours).ToString,
                                                                                                 PT
                                                                                                )).ToList


          For Each PDay As ProductionTimelineDay In TimelineDays
            For Each PDTimeline As ProductionTimelineItem In Timelines
              If PDTimeline.Day.Date = PDay.Day Then
                mProductionTimelineItemList.Add(PDTimeline)
                PDay.ProductionTimelineItems.Add(PDTimeline)
              End If
            Next
            mProductionTimelineDayList.Add(PDay)
          Next

        End If

      End Sub

      Public Property ChangedHumanResourceIDs As New List(Of Integer)

      Public Sub SelectAllHR()

        Me.DisciplineGroupList.ForEach(Sub(Discipline)

                                         Discipline.HumanResources.ForEach(Sub(HR)
                                                                             If HR.HiddenInd = False Then
                                                                               HR.UpdateInd = True
                                                                             End If
                                                                           End Sub)

                                       End Sub)

      End Sub

      Public Sub UnSelectAllHR()

        Me.DisciplineGroupList.ForEach(Sub(Discipline)

                                         Discipline.HumanResources.ForEach(Sub(HR)
                                                                             HR.UpdateInd = False
                                                                           End Sub)

                                       End Sub)

      End Sub

      Public Function GetSelectedHR() As List(Of HumanResourceItem)

        Dim Result As New List(Of HumanResourceItem)
        Me.DisciplineGroupList.ForEach(Sub(Discipline)
                                         Discipline.HumanResources.ForEach(Sub(HR)
                                                                             If HR.UpdateInd Then
                                                                               Result.Add(HR)
                                                                             End If
                                                                           End Sub)
                                       End Sub)
        Return Result

      End Function

      Public Function GetSelectedTimelines() As List(Of ProductionTimelineItem)

        Dim Result As New List(Of ProductionTimelineItem)
        Me.ProductionTimelineDayList.ForEach(Sub(TimelineDay)
                                               TimelineDay.ProductionTimelineItems.ForEach(Sub(TimelineItem)
                                                                                             If TimelineItem.UpdateInd Then
                                                                                               Result.Add(TimelineItem)
                                                                                             End If
                                                                                           End Sub)
                                             End Sub)
        Return Result

      End Function

      Public Sub New(Production As Productions.Old.Production)

        BuildProductionHumanResources(Production)
        BuildProductionDays(Production)

      End Sub

      Public Sub New()

      End Sub

    End Class

  End Class

  Public Class ProductionSpecRequirements

    'Outside Broadcast----------------------------------------------------------------------------
    Public Shared Sub PopulateOBSpec(ByRef Production As OBLib.Productions.Old.Production, ByRef ProductionSpecRequirement As OBLib.Productions.Specs.Old.ProductionSpecRequirement, SystemID As Integer, ProductionAreaID As Integer)

      PopulateOBEquipment(Production, ProductionSpecRequirement, SystemID, ProductionAreaID)
      PopulateOBPositions(Production, ProductionSpecRequirement, SystemID, ProductionAreaID)

    End Sub

    Private Shared Sub PopulateOBEquipment(ByRef Production As OBLib.Productions.Old.Production, ProductionSpecRequirement As ProductionSpecRequirement, SystemID As Integer, ProductionAreaID As Integer)

      For Each child As ProductionSpecRequirementEquipmentType In ProductionSpecRequirement.ProductionSpecRequirementEquipmentTypeList
        If CompareSafe(child.SystemID, SystemID) And CompareSafe(child.ProductionAreaID, ProductionAreaID) Then
          Dim obj As ProductionSpecRequirementEquipmentType = Production.ProductionSystemAreaList(0).ProductionSpecRequirementEquipmentTypeList.AddNew
          obj.ProductionSpecRequirementID = Nothing
          obj.SystemID = child.SystemID
          obj.ProductionAreaID = child.ProductionAreaID
          obj.EquipmentTypeID = child.EquipmentTypeID
          obj.EquipmentSubTypeID = child.EquipmentSubTypeID
          obj.EquipmentQuantity = child.EquipmentQuantity
          obj.ProductionID = child.ProductionID
          obj.CheckRules()
        End If
      Next

    End Sub

    Private Shared Sub PopulateOBPositions(ByRef Production As OBLib.Productions.Old.Production, ProductionSpecRequirement As ProductionSpecRequirement, SystemID As Integer, ProductionAreaID As Integer)

      Dim list As ProductionSpecRequirementPositionList = ProductionSpecRequirement.ProductionSpecRequirementPositionList
      For Each child As ProductionSpecRequirementPosition In list
        If CompareSafe(child.SystemID, SystemID) And CompareSafe(child.ProductionAreaID, ProductionAreaID) Then
          Dim obj As ProductionSpecRequirementPosition = Production.ProductionSystemAreaList(0).ProductionSpecRequirementPositionList.AddNew
          'obj.Parent = Parent
          obj.ProductionSpecRequirementID = Nothing
          obj.SystemID = child.SystemID
          obj.ProductionAreaID = child.ProductionAreaID
          obj.DisciplineID = child.DisciplineID
          obj.PositionID = child.PositionID
          obj.EquipmentSubTypeID = child.EquipmentSubTypeID
          obj.EquipmentQuantity = child.EquipmentQuantity
          obj.ProductionID = child.ProductionID
          obj.CheckRules()
        End If
      Next

    End Sub

    'Studio---------------------------------------------------------------------------------------
    'Check Valid
    'Public Shared Sub IsSpecValid(ByRef Production As StudioProduction) 'As Boolean  'Production As Production As String

    '  Dim Area As StudioProductionArea = Production.StudioProductionAreaList(0)
    '  Dim ProductionSpecRequirementID As Integer? = Area.ProductionSpecRequirementID
    '  'this is a an Outside Broadcasting rule only
    '  If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CType(CommonData.Enums.System.ProductionServices, Integer)) Then
    '    Dim SpecRequirementEquipmentTypeMatch As Boolean = True
    '    Dim SpecRequirementPositionMatch As Boolean = True
    '    If ProductionSpecRequirementID IsNot Nothing Then
    '      Dim ROProductionSpec As ROProductionSpecRequirement = CommonData.Lists.ROProductionSpecRequirementList.GetItem(ProductionSpecRequirementID)
    '      'check to see if the position requirements match the spec
    '      Dim SpecEquipment As List(Of ROProductionSpecRequirementPosition) = ROProductionSpec.ROProductionSpecRequirementPositionList.Where(Function(d) CompareSafe(d.SystemID, OBLib.Security.Settings.CurrentUser.SystemID)).ToList
    '      Dim ProductionEquipmentCount = Area.ProductionSpecRequirementPositionList.Count
    '      Dim SpecEquipmentCount = SpecEquipment.Count
    '      If ProductionEquipmentCount <> SpecEquipmentCount Then
    '        SpecRequirementPositionMatch = False
    '      Else
    '        For Each item As OBLib.Productions.Specs.ProductionSpecRequirementPosition In Area.ProductionSpecRequirementPositionList
    '          If Not Singular.Misc.IsNullNothing(item.PositionID) Then
    '            Dim ROMatch As OBLib.Productions.Specs.ReadOnly.ROProductionSpecRequirementPosition = ROProductionSpec.ROProductionSpecRequirementPositionList.GetItemByPositionID(item.PositionID, item.EquipmentSubTypeID, OBLib.Security.Settings.CurrentUser.SystemID)
    '            If ROMatch Is Nothing OrElse ROMatch.EquipmentQuantity <> item.EquipmentQuantity Then
    '              SpecRequirementPositionMatch = False
    '              Exit For
    '            End If
    '          End If
    '        Next
    '      End If

    '      'check to see if the equipment requirements match the spec
    '      If Area.ProductionSpecRequirementEquipmentTypeList.Count <> ROProductionSpec.ROProductionSpecRequirementEquipmentTypeList.Where(Function(d) CompareSafe(d.SystemID, OBLib.Security.Settings.CurrentUser.SystemID)).Count Then
    '        SpecRequirementEquipmentTypeMatch = False
    '      Else
    '        For Each item As OBLib.Productions.Specs.ProductionSpecRequirementEquipmentType In Area.ProductionSpecRequirementEquipmentTypeList
    '          If Not Singular.Misc.IsNullNothing(item.EquipmentTypeID) Then
    '            Dim ROMatch As OBLib.Productions.Specs.ReadOnly.ROProductionSpecRequirementEquipmentType = ROProductionSpec.ROProductionSpecRequirementEquipmentTypeList.GetItemByEquipmentTypeID(item.EquipmentTypeID, item.EquipmentSubTypeID, OBLib.Security.Settings.CurrentUser.SystemID)
    '            If ROMatch Is Nothing OrElse ROMatch.EquipmentQuantity <> item.EquipmentQuantity Then
    '              SpecRequirementEquipmentTypeMatch = False
    '              Exit For
    '            End If
    '          End If
    '        Next
    '      End If

    '    End If

    '    If String.IsNullOrEmpty(Area.NotComplySpecReasons) And Not (SpecRequirementEquipmentTypeMatch And SpecRequirementPositionMatch) Then
    '      Area.SpecNotCompliant = True
    '      Exit Sub
    '    End If

    '  End If

    '  Area.SpecNotCompliant = False


    'End Sub

  End Class

  Public Class ProductionSchedules

    Public Shared Sub CheckProductionAvailabilityOB(ByRef ProductionSystemArea As ProductionSystemArea, Optional HumanResourceID As Integer? = Nothing)

      Dim ROHRClashOBList As OBLib.Productions.Temp.ROHRClashOBList = OBLib.Productions.Temp.ROHRClashOBList.GetROHRClashOBList(ProductionSystemArea, HumanResourceID)

      'Reset the clashes
      ProductionSystemArea.HRProductionScheduleList.Where(Function(d) HumanResourceID Is Nothing OrElse HumanResourceID = d.HumanResourceID).ToList.ForEach(Sub(CrewMember)
                                                                                                                                                              CrewMember.AllClashes = ""
                                                                                                                                                              CrewMember.HRProductionScheduleDetailList.ToList.ForEach(Sub(Detail)
                                                                                                                                                                                                                         Detail.Clash = ""
                                                                                                                                                                                                                       End Sub)
                                                                                                                                                            End Sub)

      If Not ProductionSystemArea.GetParent.Cancelled Then
        'Update the clashes
        ProductionSystemArea.HRProductionScheduleList.Where(Function(d) HumanResourceID Is Nothing OrElse HumanResourceID = d.HumanResourceID).ToList.ForEach(Sub(CrewMember)

                                                                                                                                                                If Not CrewMember.IsEventManager Then
                                                                                                                                                                  Dim ClashList As List(Of ROHRClashOB) = ROHRClashOBList.Where(Function(d) CompareSafe(d.HumanResourceID, CrewMember.HumanResourceID)).ToList
                                                                                                                                                                  If ClashList.Count > 0 Then
                                                                                                                                                                    Dim MinStartDateTime As DateTime = ClashList.Min(Function(d) d.StartDateTime)
                                                                                                                                                                    Dim MaxEndDateTime As DateTime = ClashList.Max(Function(d) d.EndDateTime)
                                                                                                                                                                    ClashList.ForEach(Sub(HROBClash)
                                                                                                                                                                                        If Not (CompareSafe(HROBClash.ProductionID, CrewMember.ProductionID) AndAlso CompareSafe(HROBClash.SystemID, CrewMember.SystemID) AndAlso CompareSafe(HROBClash.ProductionAreaID, CrewMember.ProductionAreaID)) Then
                                                                                                                                                                                          CrewMember.AllClashes &= HROBClash.ParentDesc & " (" & HROBClash.ChildDesc & ")" & " - " & HROBClash.StartDateTime.Value.ToString("dd-MMM-yy HH:mm") & vbCrLf
                                                                                                                                                                                          CrewMember.HRProductionScheduleDetailList.ToList.ForEach(Sub(Schedule)
                                                                                                                                                                                                                                                     If MiscHelper.DateRangesOverlap(MinStartDateTime, MaxEndDateTime, Schedule.StartDateTime, Schedule.EndDateTime, True) Then
                                                                                                                                                                                                                                                       Schedule.Clash &= HROBClash.ParentDesc & " (" & HROBClash.ChildDesc & ")" & " - " & HROBClash.StartDateTime.Value.ToString("dd-MMM-yy HH:mm")
                                                                                                                                                                                                                                                     End If
                                                                                                                                                                                                                                                   End Sub)
                                                                                                                                                                                        End If
                                                                                                                                                                                      End Sub)
                                                                                                                                                                  End If
                                                                                                                                                                End If

                                                                                                                                                              End Sub)
      End If

    End Sub

  End Class

  Public Class HRShiftHelper

    Public Shared Function ShiftAvailable(HumanResourceID As Integer?, HumanResourceShiftID As Integer?, StartDateTime As DateTime?, EndDateTime As DateTime?) As List(Of String)

      If HumanResourceShiftID = 0 Then
        HumanResourceShiftID = Nothing
      End If
      Dim cmd As New Singular.CommandProc("[RuleProcs].[ruleShiftAvailable]",
                                          New String() {"@HumanResourceID", "@HumanResourceShiftID", "@StartDateTime", "@EndDateTime"},
                                          New Object() {HumanResourceID, NothingDBNull(HumanResourceShiftID), StartDateTime, EndDateTime})
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute
      Dim l As New List(Of String)
      If cmd.Dataset IsNot Nothing AndAlso cmd.Dataset.Tables.Count > 0 Then
        For Each dr As DataRow In cmd.Dataset.Tables(0).Rows
          l.Add(dr(0))
        Next
      End If
      Return l
    End Function

  End Class

  'Public Class RoomHelper

  '  Public Shared Function RoomAvailable(RoomID As Integer?, RoomScheduleID As Integer?, StartDateTime As DateTime?, EndDateTime As DateTime?) As List(Of String)
  '    If RoomScheduleID = 0 Then
  '      RoomScheduleID = Nothing
  '    End If
  '    Dim cmd As New Singular.CommandProc("[RuleProcs].[ruleRoomAvailable]",
  '                                        New String() {"@RoomID", "@RoomScheduleID", "@StartDateTime", "@EndDateTime"},
  '                                        New Object() {RoomID, NothingDBNull(RoomScheduleID), StartDateTime, EndDateTime})
  '    cmd.FetchType = CommandProc.FetchTypes.DataSet
  '    cmd = cmd.Execute
  '    Dim l As New List(Of String)
  '    If cmd.Dataset IsNot Nothing AndAlso cmd.Dataset.Tables.Count > 0 Then
  '      For Each dr As DataRow In cmd.Dataset.Tables(0).Rows
  '        l.Add(dr(0))
  '      Next
  '    End If
  '    Return l
  '  End Function

  'End Class

  'Public Class EquipmentHelper

  '  Public Shared Function EquipmentAvailable(EquipmentID As Integer?, EquipmentScheduleID As Integer?, StartDateTime As DateTime?, EndDateTime As DateTime?, ResourceID As Integer?) As List(Of String)
  '    If EquipmentScheduleID = 0 Then
  '      EquipmentScheduleID = Nothing
  '    End If
  '    If EquipmentID = 0 Then
  '      EquipmentID = Nothing
  '    End If
  '    If ResourceID = 0 Then
  '      ResourceID = Nothing
  '    End If
  '    Dim cmd As New Singular.CommandProc("[RuleProcs].[ruleEquipmentAvailable]",
  '                                        New String() {"@EquipmentID", "@EquipmentScheduleID", "@StartDateTime", "@EndDateTime", "@ResourceID"},
  '                                        New Object() {NothingDBNull(EquipmentID), NothingDBNull(EquipmentScheduleID), StartDateTime, EndDateTime, NothingDBNull(ResourceID)})
  '    cmd.FetchType = CommandProc.FetchTypes.DataSet
  '    cmd = cmd.Execute
  '    Dim l As New List(Of String)
  '    If cmd.Dataset IsNot Nothing AndAlso cmd.Dataset.Tables.Count > 0 Then
  '      For Each dr As DataRow In cmd.Dataset.Tables(0).Rows
  '        l.Add(dr(0))
  '      Next
  '    End If
  '    Return l
  '  End Function

  'End Class

  'Public Class VehicleHelper

  '  Public Shared Function VehicleAvailable(VehicleID As Integer?, StartDateTime As DateTime?, EndDateTime As DateTime?) As String
  '    Dim cmd As New Singular.CommandProc("[RuleProcs].[ruleVehicleAvailable]",
  '                                        New String() {"@VehicleID", "@StartDateTime", "@EndDateTime"},
  '                                        New Object() {VehicleID, StartDateTime, EndDateTime})
  '    cmd.FetchType = CommandProc.FetchTypes.DataRow
  '    cmd = cmd.Execute
  '    If cmd.DataRow IsNot Nothing Then
  '      Return cmd.DataRow(1)
  '    End If
  '    Return ""
  '  End Function

  'End Class

  Public Class RuleHelper

    Public Class PaymentRunRules

      Public Shared Function CheckPaymentDateOverlaps(PaymentRun As PaymentRun) As String
        If PaymentRun.StartDate IsNot Nothing And PaymentRun.EndDate IsNot Nothing And PaymentRun.SystemID IsNot Nothing Then
          Dim PRID As Object = PaymentRun.PaymentRunID
          If PRID = 0 Then
            PRID = Nothing
          End If
          Dim cProc As New Singular.CommandProc("[RuleProcs].[rulePaymentRunDatesOverlap]",
                                                 New String() {"PaymentRunID", "StartDate", "EndDate", "SystemID"},
                                                 New Object() {NothingDBNull(PRID),
                                                               NothingDBNull(PaymentRun.StartDate),
                                                               NothingDBNull(PaymentRun.EndDate),
                                                               NothingDBNull(PaymentRun.SystemID)})
          cProc.CommandType = CommandType.StoredProcedure
          cProc.FetchType = Singular.CommandProc.FetchTypes.DataRow
          cProc = cProc.Execute
          If cProc.DataRow(0) <> "" Then
            Return cProc.DataRow(0)
          End If
        End If
        Return ""
      End Function

    End Class

    Public Class ProductionSystemAreaRules

      Public Shared Function CheckProductionSystemAreaExists(ProductionID As Integer, SystemID As Integer, ProductionAreaID As Integer) As Boolean
        Dim cProc As New Singular.CommandProc("[RuleProcs].[ruleProductionSystemAreaExists]",
                                                 New String() {"ProductionID", "SystemID", "ProductionAreaID"},
                                                 New Object() {ProductionID,
                                                               SystemID,
                                                               ProductionAreaID})
        cProc.CommandType = CommandType.StoredProcedure
        cProc.FetchType = Singular.CommandProc.FetchTypes.DataObject
        cProc = cProc.Execute
        Return CType(cProc.DataObject, Boolean)
      End Function

      Public Class DisciplineErrors
        Public Property DisciplineID As Integer? = Nothing
        Public Property Discipline As String = ""
        Public Property TimelineErrors As List(Of TimelineError) = Nothing
      End Class

      Public Class TimelineError
        Public Property TimelineGuid As String = ""
        Public Property ErrorString As String = ""
      End Class

      Public Class ProductionTimelineClient

        Public Property ID As String
        Public Property ProductionTimeline As ProductionTimeline

      End Class

    End Class

    Public Class VehicleRules

      Public Shared Function IsAvailable(ByVal ProductionID As Integer, ByVal VehicleID As Integer, ByVal StartDate As Date, ByVal EndDate As Date, ByRef ErrorMessage As String) As Boolean

        Dim SDStartDate As New SmartDate(StartDate)
        Dim SDEndDate As New SmartDate(EndDate)

        Dim cmd As New Singular.CommandProc("CmdProcs.cmdVehicleAvailable",
                                            New String() {"ProductionID", "VehicleID", "StartDate", "EndDate"},
                                            New Object() {ProductionID, VehicleID, SDStartDate.DBValue, SDEndDate.DBValue})
        cmd.FetchType = Singular.CommandProc.FetchTypes.DataRow
        cmd.Execute()
        ErrorMessage = cmd.DataRow(0)
        Return String.IsNullOrEmpty(ErrorMessage)

      End Function

      Public Shared Sub UpdateClash(ProductionVehicle As Productions.Vehicles.ProductionVehicle, ByVal StartDate As DateTime, ByVal EndDate As DateTime)

        Dim ds As New DataSet("DataSet")
        Dim dt As DataTable = ds.Tables.Add("Table")
        dt.Columns.Add("GUID", GetType(Guid)).ColumnMapping = MappingType.Attribute
        dt.Columns.Add("ProductionVehicleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
        dt.Columns.Add("VehicleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
        dt.Columns.Add("ProductionID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
        dt.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
        dt.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute

        Dim row As DataRow = dt.NewRow
        row("Guid") = ProductionVehicle.Guid
        row("ProductionVehicleID") = ProductionVehicle.ProductionVehicleID
        row("VehicleID") = ProductionVehicle.VehicleID
        row("ProductionID") = ProductionVehicle.ProductionID
        row("StartDateTime") = StartDate
        row("EndDateTime") = EndDate
        dt.Rows.Add(row)

        Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdVehicleAvailableWeb]",
                                            New String() {"@ProductionVehicleTimesXml", "@ProductionID"},
                                            New Object() {ds.GetXml, ProductionVehicle.ProductionID})
        cmd.FetchType = CommandProc.FetchTypes.DataSet
        cmd = cmd.Execute

        'Reset the clashes
        ProductionVehicle.Clash = ""

        Dim GuidString As String = ""
        Dim GUid As Guid = Nothing

        For Each dr As DataRow In cmd.Dataset.Tables(0).Rows
          ProductionVehicle.Clash &= "&nbsp;&nbsp;&nbsp;&nbsp;" & dr(6) & ", <br>"
          'ErrorMessage &= Discipline.Discipline & ": " & "<br>"
          'Clashes.ToList.ForEach(Sub(Err)
          '                         ErrorMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;" & Err.ProductionTimelineType1 & " " & (Err.StartDateTime1.ToShortDateString) & " overlaps with " & _
          '                                         Err.ProductionTimelineType2 & " " & (Err.StartDateTime2.ToShortDateString) & ", <br>"
          '                       End Sub)
        Next

      End Sub


    End Class

  End Class

  Public Class GeneralHelper

    Public Shared Function BuildCreateDetails(CreatedBy As Integer?, CreatedDateTime As DateTime?) As String

      Dim CB As String = ""
      If CreatedBy IsNot Nothing Then
        CB = CommonData.Lists.ROUserList.GetItem(CreatedBy).LoginName
      End If

      Dim CDT As String = ""
      If CreatedDateTime IsNot Nothing Then
        CDT = CreatedDateTime.Value.ToString("dd MMM yy HH:mm")
      End If

      Return CB & " - " & CDT

    End Function

    Public Shared Function BuildModDetails(ModBy As Integer?, ModDateTime As DateTime?) As String

      Dim CB As String = ""
      If ModBy IsNot Nothing Then
        CB = CommonData.Lists.ROUserList.GetItem(ModBy).LoginName
      End If

      Dim CDT As String = ""
      If ModDateTime IsNot Nothing Then
        CDT = ModDateTime.Value.ToString("dd MMM yy HH:mm")
      End If

      Return CB & " - " & CDT

    End Function

    Public Shared Function IsSkilled(HumanResourceID As Integer?, AtDate As Date,
                                     DisciplineID As Integer?, PositionID As Integer?,
                                     ProductionTypeID As Integer?) As Boolean

      Dim cmd As New Singular.CommandProc("[cmdProcs].[cmdIsSkilledDetailed]",
                                          New String() {"@HumanResourceID",
                                                        "@AtDate",
                                                        "@DisciplineID",
                                                        "@PositionID",
                                                        "@ProductionTypeID"},
                                          New Object() {NothingDBNull(HumanResourceID),
                                                        NothingDBNull(AtDate),
                                                        NothingDBNull(DisciplineID),
                                                        NothingDBNull(PositionID),
                                                        NothingDBNull(ProductionTypeID)})
      cmd.FetchType = CommandProc.FetchTypes.DataObject
      If cmd.DataObject IsNot Nothing Then
        Return CType(cmd.DataObject, Boolean)
      Else
        Return False
      End If

    End Function

  End Class

  Public Class BulkGroupSnTEdit

    Public Sub UpdateGroupHumanResourceSnT(GroupSnTBulkCrewList As GroupSnTBulkCrewList, SnTDayList As List(Of SnTDay), GroupSnTID As Integer, BreakfastInd As Boolean, LunchInd As Boolean, DinnerInd As Boolean, IncidentalInd As Boolean, ProductionID As Boolean)

      Dim ds As New DataSet("DataSet")
      Dim dt As DataTable = ds.Tables.Add("Table")
      dt.Columns.Add("HumanResourceID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("SnTDay", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("GroupSnTID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("BreakfastInd", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("LunchInd", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("DinnerInd", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("IncidentalInd", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      dt.Columns.Add("ProductionID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

      Dim query = From gsbc As GroupSnTBulkCrew In GroupSnTBulkCrewList
                  From sl As SnTDay In SnTDayList
                  Where gsbc.SnTInd _
                    And sl.SelectedInd
                  Select New With {
                                    Key .HumanResourceID = gsbc.HumanResourceID,
                                    Key .SnTDay = sl.SnTDay,
                                    Key .GroupSnTID = GroupSnTID,
                                    Key .BreakfastInd = BreakfastInd,
                                    Key .LunchInd = LunchInd,
                                    Key .DinnerInd = DinnerInd,
                                    Key .IncidentalInd = IncidentalInd,
                                    Key .ProductionID = ProductionID
                                  }
      For Each obj In query
        Dim row As DataRow = dt.NewRow
        row("HumanResourceID") = obj.HumanResourceID
        row("SnTDay") = obj.SnTDay
        row("GroupSnTID") = obj.GroupSnTID
        row("BreakfastInd") = obj.BreakfastInd
        row("LunchInd") = obj.LunchInd
        row("DinnerInd") = obj.DinnerInd
        row("IncidentalInd") = IncidentalInd
        row("ProductionID") = obj.ProductionID
        dt.Rows.Add(row)
      Next

      Dim cmd As New Singular.CommandProc("[UpdProcsWeb].[updBulkGroupHumanResourceSnTList]",
                                    New String() {"@BulkGroupHumanResourceSnTXML"},
                                    New Object() {ds.GetXml})
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute

    End Sub

  End Class

  <Serializable()>
  Public Class SnTDay
    Public Property SelectedInd As Boolean = False
    <Display(Name:="Day", Description:="")>
    Public Property SnTDay As DateTime? = Nothing
  End Class

  Public Class MiscHelper

    Public Shared Function StringArrayToXML(ByVal StrArray() As String) As String

      Try

        Dim ds As New DataSet("DataSet")
        ds.Tables.Add("Table")
        ds.Tables("Table").Columns.Add("ID")
        ds.Tables("Table").Columns("ID").ColumnMapping = MappingType.Attribute

        For J As Integer = 0 To StrArray.Length - 1

          ds.Tables("Table").Rows.Add(New Object() {StrArray(J)})

        Next

        Dim w As New IO.StringWriter
        ds.WriteXml(w)
        Return (w.ToString)

      Catch ex As Exception
        Return String.Empty
      End Try

    End Function

    Public Shared Function IntegerArrayToXML(ByVal IntArray() As Integer) As String

      Try
        If IntArray.Length > 0 Then
          Dim ds As New DataSet("DataSet")
          ds.Tables.Add("Table")
          ds.Tables("Table").Columns.Add("ID")
          ds.Tables("Table").Columns("ID").ColumnMapping = MappingType.Attribute
          For J As Integer = 0 To IntArray.Length - 1
            ds.Tables("Table").Rows.Add(New Object() {IntArray(J).ToString})
          Next
          Dim w As New IO.StringWriter
          ds.WriteXml(w)
          Return (w.ToString)
        End If
        Return ""
      Catch ex As Exception
        Return String.Empty
      End Try

    End Function

    Public Shared Function DateRangesOverlap(Range1Start As DateTime, Range1End As DateTime, Range2Start As DateTime, Range2End As DateTime, AllowStartAndEndEqual As Boolean) As Boolean
      If AllowStartAndEndEqual Then

        If Range1Start > Range2Start And Range1Start < Range2End Then
          Return True
        End If

        If Range1End > Range2Start And Range1End < Range2End Then
          Return True
        End If

        If Range2Start > Range1Start And Range2Start < Range1End Then
          Return True
        End If

        If Range2End > Range1Start And Range2End < Range1End Then
          Return True
        End If

        'If (Range1Start < Range2End And Range1Start > Range2Start) Or (Range1End < Range2End And Range1End > Range2Start) Then
        '  Return True
        'End If
      Else

        If Range1Start >= Range2Start And Range1Start <= Range2End Then
          Return True
        End If

        If Range1End >= Range2Start And Range1End <= Range2End Then
          Return True
        End If

        If Range2Start >= Range1Start And Range2Start <= Range1End Then
          Return True
        End If

        If Range2End >= Range1Start And Range2End <= Range1End Then
          Return True
        End If

        'If (Range1Start <= Range2End And Range1Start >= Range2Start) Or (Range1End <= Range2End And Range1End >= Range2Start) Then
        '  Return True
        'End If
      End If
      Return False
    End Function

  End Class

  Public Class Production

    Public Shared Function MainDetailsChanged(LoadedProductionTypeID As Integer?, ProductionTypeID As Integer?,
                                              LoadedEventTypeID As Integer?, EventTypeID As Integer?,
                                              LoadedProductionVenueID As Integer?, ProductionVenueID As Integer?,
                                              LoadedTeamsPlaying As String, TeamsPlaying As String,
                                              LoadedPlayStartDateTime As DateTime?, PlayStartDateTime As DateTime?,
                                              LoadedPlayEndDateTime As DateTime?, PlayEndDateTime As DateTime?,
                                              LoadedTitle As String, Title As String) As Boolean

      If CompareSafe(LoadedProductionTypeID, ProductionTypeID) _
         AndAlso CompareSafe(LoadedEventTypeID, EventTypeID) _
         AndAlso CompareSafe(LoadedProductionVenueID, ProductionVenueID) _
         AndAlso CompareSafe(LoadedTeamsPlaying, TeamsPlaying) _
         AndAlso CompareSafe(LoadedPlayStartDateTime, PlayStartDateTime) _
         AndAlso CompareSafe(LoadedPlayEndDateTime, PlayEndDateTime) _
         AndAlso CompareSafe(LoadedTitle, Title) Then
        Return False
      Else
        Return True
      End If

      Return False

    End Function

  End Class

  <Serializable()>
  Public Class RecordChange

    Public Property MainDescription As String
    Public Property SubDescription As String
    Public Property ChangeType As String
    Public Property ChangedBy As String
    Public Property RecordChangeDetails As List(Of RecordChangeDetail)

    Public Sub New(MainDescription As String, SubDescription As String, ChangedBy As String, ChangeType As String, RecordChangeDetails As List(Of RecordChangeDetail))
      Me.MainDescription = MainDescription
      Me.SubDescription = SubDescription
      Me.ChangedBy = ChangedBy
      Me.ChangeType = ChangeType
      Me.RecordChangeDetails = RecordChangeDetails
    End Sub

  End Class

  <Serializable()>
  Public Class RecordChangeDetail

    Public Property FieldName As String
    Public Property PreviousValue As String
    Public Property NewValue As String

    Public Sub New(FieldName As String, PreviousValue As String, NewValue As String)
      Me.FieldName = FieldName
      Me.PreviousValue = PreviousValue
      Me.NewValue = NewValue
    End Sub

  End Class

  <Serializable()>
  Public Class DisciplineSelect
    Inherits SingularBusinessBase(Of DisciplineSelect)

#Region " Properties "

    Public Shared DisciplineIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DisciplineID, "ID")
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key()>
    Public Property DisciplineID() As Integer?
      Get
        Return GetProperty(DisciplineIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DisciplineIDProperty, Value)
      End Set
    End Property

    Public Shared SelectedProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.Selected, "Selected", False)
    ''' <summary>
    ''' Gets and sets the Selected value
    ''' </summary>
    <Display(Name:="Selected", Description:="")>
    Public Property Selected() As Boolean
      Get
        Return GetProperty(SelectedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectedProperty, Value)
      End Set
    End Property

    Public Shared DisciplineProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Discipline, "Discipline")
    ''' <summary>
    ''' Gets and sets the Discipline value
    ''' </summary>
    <Display(Name:="Discipline", Description:="")>
    Public Property Discipline() As String
      Get
        Return GetProperty(DisciplineProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(DisciplineProperty, Value)
      End Set
    End Property

    Public Shared OrderNoProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.OrderNo, "Order No")
    ''' <summary>
    ''' Gets and sets the Order No value
    ''' </summary>
    <Display(Name:="Order No", Description:="")>
    Public Property OrderNo() As Integer
      Get
        Return GetProperty(OrderNoProperty)
      End Get
      Set(ByVal Value As Integer)
        SetProperty(OrderNoProperty, Value)
      End Set
    End Property

#End Region

  End Class

  <Serializable()>
  Public Class ProductionTypeSelect
    Inherits SingularBusinessBase(Of ProductionTypeSelect)

#Region " Properties "

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.ProductionTypeID)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key()>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared SelectedProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.Selected, False)
    ''' <summary>
    ''' Gets and sets the Selected value
    ''' </summary>
    <Display(Name:="Selected", Description:="")>
    Public Property Selected() As Boolean
      Get
        Return GetProperty(SelectedProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(SelectedProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Production Type")
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTypeProperty, Value)
      End Set
    End Property

    '_
    '                                                            .AddSetExpression("SkillsDatabase.ProductionTypeSelectedSet", False)

#End Region

  End Class

  <Serializable()>
  Public Class ProductionTypeTab
    Inherits SingularBusinessBase(Of ProductionTypeTab)

#Region " Properties "

    Public Shared ProductionTypeIDProperty As PropertyInfo(Of Integer) = RegisterSProperty(Of Integer)(Function(c) c.ProductionTypeID)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key()>
    Public Property ProductionTypeID() As Integer?
      Get
        Return GetProperty(ProductionTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionType, "Production Type")
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Production Type", Description:="")>
    Public Property ProductionType() As String
      Get
        Return GetProperty(ProductionTypeProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionTypeProperty, Value)
      End Set
    End Property

#End Region

  End Class

  Public Class SDTab
    Inherits SingularBusinessBase(Of SDTab)
#Region " Properties "

    Public Shared TabIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.TabID, Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key()>
    Public Property TabID() As Integer?
      Get
        Return GetProperty(TabIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TabIDProperty, Value)
      End Set
    End Property

    Public Shared TabIndexProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.TabIndex, Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property TabIndex() As Integer?
      Get
        Return GetProperty(TabIndexProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(TabIndexProperty, Value)
      End Set
    End Property

    Public Shared TabNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TabName, "Tab Name", "")
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Tab Name", Description:="")>
    Public Property TabName() As String
      Get
        Return GetProperty(TabNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TabNameProperty, Value)
      End Set
    End Property

    Public Shared TabTextProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.TabText, "Tab Text", "")
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Tab Text", Description:="")>
    Public Property TabText() As String
      Get
        Return GetProperty(TabTextProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(TabTextProperty, Value)
      End Set
    End Property

#End Region

  End Class

  Public Class SDHeader
    Inherits SingularBusinessBase(Of SDHeader)
#Region " Properties "

    Public Shared HeaderIDProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HeaderID, Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    <Key()>
    Public Property HeaderID() As Integer?
      Get
        Return GetProperty(HeaderIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HeaderIDProperty, Value)
      End Set
    End Property

    Public Shared HeaderIndexProperty As PropertyInfo(Of Integer?) = RegisterSProperty(Of Integer?)(Function(c) c.HeaderIndex, Nothing)
    ''' <summary>
    ''' Gets and sets the ID value
    ''' </summary>
    Public Property HeaderIndex() As Integer?
      Get
        Return GetProperty(HeaderIndexProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HeaderIndexProperty, Value)
      End Set
    End Property

    Public Shared HeaderNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HeaderName, "Header Name", "")
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Header Name", Description:="")>
    Public Property HeaderName() As String
      Get
        Return GetProperty(HeaderNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HeaderNameProperty, Value)
      End Set
    End Property

    Public Shared HeaderTextProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HeaderText, "Header Text", "")
    ''' <summary>
    ''' Gets and sets the Production Type value
    ''' </summary>
    <Display(Name:="Header Text", Description:="")>
    Public Property HeaderText() As String
      Get
        Return GetProperty(HeaderTextProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HeaderTextProperty, Value)
      End Set
    End Property

#End Region

  End Class

  <Serializable()>
  Public Class MRValues
    Property NoOfMeals As Integer = 0
    Property MRAmount As Decimal = 0
    Property SyncID As Integer
  End Class

  <Serializable()>
  Public Class ProductionClashDetails
    Public Property ProductionID As Integer
    Public Property ProductionDescription As String
    Public Property CrewFinalisedInd As Boolean
    Public Property TxDate As Nullable(Of Date)
    'Public Property ClashDescription As String

    Public Sub New(ProductionID As Integer, ProductionDescription As String, CrewFinalisedInd As Boolean, TxDate As Nullable(Of Date))
      Me.ProductionID = ProductionID
      Me.ProductionDescription = ProductionDescription
      Me.CrewFinalisedInd = CrewFinalisedInd
      Me.TxDate = TxDate
      'Me.ClashDescription = r><td>" & ProductionDescription & "</td></tr>"
    End Sub

  End Class

  <Serializable()>
  Public Class OffPeriodDisciplineHRClashes
    Public Property HumanResourceID As Integer
    Public Property Discipline As String
    Public Property HumanResource As String
    Public Property OffReason As String
    Public Property LeaveDetail As String
    Public Property StartDate As DateTime
    Public Property EndDate As DateTime

    Public Sub New(HumanResourceID As Integer, Discipline As String, HumanResource As String, OffReason As String, LeaveDetail As String,
                    StartDate As DateTime, EndDate As DateTime)
      Me.HumanResourceID = HumanResourceID
      Me.Discipline = Discipline
      Me.HumanResource = HumanResource
      Me.OffReason = LeaveDetail
      Me.StartDate = StartDate
      Me.EndDate = EndDate

    End Sub

  End Class

  <Serializable()>
  Public Class ProductionManagers
    Public Property ProductionID As Integer
    Public Property ManagerID As Object
    Public Property EmailAddress As String

    Public Sub New(ProductionID As Integer, ManagerID As Object, Optional EmailAddress As String = "")
      Me.ProductionID = ProductionID
      Me.ManagerID = ManagerID
      Me.EmailAddress = EmailAddress
    End Sub

  End Class

  Public Class RoomTimesCalculations

    Public Property RoomTimelineSetting As RORoomTimelineSetting
    Public Property CallTimeStart As DateTime?
    Public Property OnAirTimeStart As DateTime?
    Public Property OnAirTimeEnd As DateTime?
    Public Property WrapTimeEnd As DateTime?

    Public Sub New(RoomID As Integer?, OnAirStartDateTime As DateTime?, OnAirEndDateTime As DateTime?)

      If RoomID IsNot Nothing AndAlso OnAirStartDateTime IsNot Nothing AndAlso OnAirEndDateTime IsNot Nothing Then
        Dim RORoomTimelineSettingList As List(Of RORoomTimelineSetting) = OBLib.CommonData.Lists.RORoomTimelineSettingList.GetApplicableSettings(RoomID, OnAirStartDateTime)
        If RORoomTimelineSettingList.Count = 1 Then
          RoomTimelineSetting = RORoomTimelineSettingList(0)
        End If

        If RoomTimelineSetting IsNot Nothing Then
          CallTimeStart = OnAirStartDateTime.Value.AddMinutes(-RoomTimelineSetting.DefaultCallTime)
          OnAirTimeStart = OnAirStartDateTime
          OnAirTimeEnd = OnAirEndDateTime
          WrapTimeEnd = OnAirEndDateTime.Value.AddMinutes(RoomTimelineSetting.DefaultWrapTime)
        End If
      End If

    End Sub

  End Class

  Public Class RoomHelpers

    Public Shared Function GetRoomIDsXML(RoomID As Integer?) As String

      Dim RoomIDs As String = ""
      Dim RoomIDList As New List(Of String)
      RoomIDList.Add(RoomID.ToString)
      RoomIDs = OBLib.Helpers.MiscHelper.StringArrayToXML(RoomIDList.ToArray)
      Return RoomIDs

    End Function

    Public Shared Function GetRoomScheduleIDsXML(RoomScheduleID As Integer?) As String

      Dim RoomScheduleIDs As String = ""
      Dim RoomScheduleIDList As New List(Of String)
      RoomScheduleIDList.Add(RoomScheduleID.ToString)
      RoomScheduleIDs = OBLib.Helpers.MiscHelper.StringArrayToXML(RoomScheduleIDList.ToArray)
      Return RoomScheduleIDs

    End Function

  End Class

  <Serializable()>
  Public Class MultiSelectROHumanResource
    Public Property SelectInd As Boolean = False
    Public Property HumanResourceID As Integer
    Public Property PreferredFirstSurname As String
    Public Property EmailAddress As String

    Public Sub New(HumanResourceID As Integer, PreferredFirstSurname As String, EmailAddress As String)
      Me.HumanResourceID = HumanResourceID
      Me.PreferredFirstSurname = PreferredFirstSurname
      Me.EmailAddress = EmailAddress
    End Sub
  End Class

  <Serializable()>
  Public Class SelectedROProduction
    Public Property ProductionID As Integer
    Public Property ProductionRefNo As String
    Public Property ProductionDescription As String
    Public Property PlayStartDateTime As DateTime
    Public Property PlayEndDateTime As DateTime

  End Class

  <Serializable()>
  Public Class SelectedROHumanResource
    Public Property HumanResourceID As Integer
    Public Property PreferredFirstSurname As String

  End Class

  Public Class SynergyHelper

    Public Shared Sub CheckGenRef(CurrentUserID As Integer?, SynergyGenRefNo As Int64, ImportedEventID As Integer?, ImportData As Boolean)
      Dim ProductionList As OBLib.Productions.Base.ProductionDetailBaseList = OBLib.Productions.Base.ProductionDetailBaseList.GetProductionDetailBaseList(SynergyGenRefNo)
      If ProductionList.Count = 0 Then
        If ImportData Then
          OBLib.Synergy.Importer.[New].SynergyImporter.ImportData(SynergyGenRefNo)
        End If
        OBLib.Synergy.Importer.[New].SynergyImporter.CreateProduction(CurrentUserID, SynergyGenRefNo, ImportedEventID)
      End If
    End Sub

    Public Shared Sub CheckGenRefs(CurrentUserID As Integer?, SynergyGenRefNumbers As List(Of Int64))
      'OBLib.Synergy.Importer.[New].SynergyImporter.ImportData(SynergyGenRefNumbers)
      OBLib.Synergy.Importer.[New].SynergyImporter.CreateProductions(CurrentUserID, SynergyGenRefNumbers)
      'Dim GenRefsXml As String = OBMisc.IntegerListToXML(SynergyGenRefNumbers)
      'Dim ProductionList As OBLib.Productions.Base.ProductionDetailBaseList = OBLib.Productions.Base.ProductionDetailBaseList.GetProductionsByGenRefNumbers(GenRefsXml)
      'If ProductionList.Count = 0 Then
      '  'OBLib.Synergy.Importer.[New].SynergyImporter.CreateProduction(CurrentUserID, SynergyGenRefNo, ImportedEventID)
      'End If
    End Sub

    Public Shared Function AddGenRef(GenRefNo As Int64, ObjName As String) As OBLib.Productions.Base.ProductionDetailBaseList
      Dim ProductionList As OBLib.Productions.Base.ProductionDetailBaseList = New OBLib.Productions.Base.ProductionDetailBaseList
      Dim iel As OBLib.Synergy.ReadOnly.ROSynergyEventList = OBLib.Synergy.ReadOnly.ROSynergyEventList.GetROSynergyEventList(GenRefNo)
      If iel.Count = 0 Then
        'Can't find gen ref in SOBER, do import
        OBLib.Synergy.Importer.[New].SynergyImporter.ImportData(GenRefNo)
        iel = OBLib.Synergy.ReadOnly.ROSynergyEventList.GetROSynergyEventList(GenRefNo)
      End If
      If iel.Count = 0 Then
        OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, ObjName, "", "Gen Ref: " & GenRefNo.ToString & " could not be found")
        Throw New Exception("Gen Ref: " & GenRefNo.ToString & " could not be found" & ObjName)
      ElseIf iel.Count >= 1 Then
        ProductionList = OBLib.Productions.Base.ProductionDetailBaseList.GetProductionDetailBaseList(GenRefNo)
        If ProductionList.Count = 0 Then
          OBLib.Synergy.Importer.[New].SynergyImporter.CreateProduction(OBLib.Security.Settings.CurrentUser.UserID, GenRefNo, Nothing)
          ProductionList = OBLib.Productions.Base.ProductionDetailBaseList.GetProductionDetailBaseList(GenRefNo)
        End If
        If ProductionList.Count = 1 Then
        ElseIf ProductionList.Count = 0 Then
          OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, ObjName, "", "Failed to import production: " & GenRefNo.ToString & " could not be found")
          Throw New Exception("Failed to import production: " & GenRefNo.ToString & " could not be found")
        Else
          Dim prefs As String = ""
          ProductionList.Select(Function(d) d.ProductionRefNo).ToList.ForEach(Sub(d)
                                                                                prefs &= d & ", "
                                                                              End Sub)
          OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.LoginName, ObjName, "", GenRefNo.ToString & " exists on multiple productions: " & prefs)
        End If
      End If
      Return ProductionList
    End Function

  End Class

  Public Class ResourceHelpers

    <Serializable>
    Public Class ClashDetail
      Inherits OBLib.OBBusinessBase(Of ClashDetail)

      Public Property ResourceBookingGuid As Guid
      Public Property ResourceID As Integer?
      Public Property ResourceBookingID As Integer?
      Public Property StartDateTimeBuffer As DateTime?
      <Display(Name:="Start Time")>
      Public Property StartDateTime As DateTime?
      <Display(Name:="End Time")>
      Public Property EndDateTime As DateTime?
      Public Property EndDateTimeBuffer As DateTime?
      Public Property ResourceBookingTypeID As Integer?
      <Display(Name:="Booking Name")>
      Public Property ResourceBookingDescription As String
      Public Property ResourceBookingType As String
      Public Property ClashString As String

      Public ReadOnly Property OnAirTimes As String
        Get
          If StartDateTime IsNot Nothing And EndDateTime IsNot Nothing Then
            Return StartDateTime.Value.ToString("ddd dd MMM yy HH:mm") & " - " & EndDateTime.Value.ToString("HH:mm")
          End If
          Return ""
        End Get
      End Property

      <Display(Name:="Start Time")>
      Public Property StartDateTimeDisplay As String
      <Display(Name:="End Time")>
      Public Property EndDateTimeDisplay As String

    End Class

    <Serializable>
    Public Class ResourceBooking
      Inherits OBLib.OBBusinessBase(Of ResourceBooking)

      Public Property ObjectGuidString As String = ""
      Public Property ObjectGuid As Guid
      Public Property ResourceID As Integer?
      Public Property ResourceBookingID As Integer?
      Public Property StartDateTimeBuffer As DateTime?
      Public Property StartDateTime As DateTime?
      Public Property EndDateTime As DateTime?
      Public Property EndDateTimeBuffer As DateTime?
      Public Property RoomScheduleID As Integer?
      Public Property ProductionHRID As Integer?
      Public Property EquipmentScheduleID As Integer?
      Public Property HumanResourceOffPeriodID As Integer?
      Public Property HumanResourceSecondmentID As Integer?
      Public Property HumanResourceShiftID As Integer?
      Public Property ProductionSystemAreaID As Integer?


      Public Property ClashesWith As New List(Of String)
      Public Property ClashDetails As New List(Of ClashDetail)

      Public Sub New(ObjectGuid As Guid, ResourceID As Integer?, ResourceBookingID As Integer?,
                     StartDateTimeBuffer As DateTime?, StartDateTime As DateTime?,
                     EndDateTime As DateTime?, EndDateTimeBuffer As DateTime?,
                     RoomScheduleID As Integer?, HumanResourceShiftID As Integer?,
                     ProductionSystemAreaID As Integer?, HumanResourceSecondmentID As Integer?,
                     HumanResourceOffPeriodID As Integer?, EquipmentScheduleID As Integer?,
                     ProductionHRID As Integer?)
        Me.ObjectGuid = ObjectGuid
        Me.ResourceID = ResourceID
        Me.ResourceBookingID = ResourceBookingID
        Me.StartDateTimeBuffer = StartDateTimeBuffer
        Me.StartDateTime = StartDateTime
        Me.EndDateTime = EndDateTime
        Me.EndDateTimeBuffer = EndDateTimeBuffer
        Me.RoomScheduleID = RoomScheduleID
        Me.HumanResourceShiftID = HumanResourceShiftID
        Me.ProductionSystemAreaID = ProductionSystemAreaID
        Me.HumanResourceSecondmentID = HumanResourceSecondmentID
        Me.HumanResourceOffPeriodID = HumanResourceOffPeriodID
        Me.EquipmentScheduleID = EquipmentScheduleID
        Me.ProductionHRID = ProductionHRID

      End Sub

    End Class

    Public Shared Function GetSingleResourceClash(ResourceBooking As ResourceHelpers.ResourceBooking) As ResourceHelpers.ResourceBooking

      Dim ResourceBookings As New List(Of ResourceHelpers.ResourceBooking)
      ResourceBookings.Add(ResourceBooking)
      Dim ds As DataSet = GetClashesDataSet(ResourceBookings)
      For Each dr As DataRow In ds.Tables(0).Rows
        ResourceBooking.ClashesWith.Add(dr(13) & " - " & dr(12))
      Next
      Return ResourceBookings(0)

    End Function

    Public Shared Function GetResourceClashes(ByRef ResourceBookings As List(Of ResourceHelpers.ResourceBooking)) As List(Of ResourceHelpers.ResourceBooking)

      Dim ds As DataSet = GetClashesDataSet(ResourceBookings)
      For Each dr As DataRow In ds.Tables(0).Rows
        Dim rb As ResourceBooking = ResourceBookings.Where(Function(d) d.ObjectGuid = dr(0)).FirstOrDefault
        If rb IsNot Nothing Then
          rb.ClashesWith.Add(dr(13) & " - " & dr(12))
          Dim cd As New ClashDetail
          cd.ResourceBookingGuid = dr(0)
          'cd.LResourceID = dr(1)
          'cd.LResourceBookingID = dr(2)
          'cd.LStartDateTime = dr(3)
          'cd.LEndDateTime = dr(4)
          cd.ResourceID = dr(5)
          cd.ResourceBookingID = dr(6)
          cd.StartDateTimeBuffer = IIf(IsNullNothing(dr(7)), Nothing, dr(7))
          cd.StartDateTime = dr(8)
          cd.StartDateTimeDisplay = cd.StartDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          cd.EndDateTime = dr(9)
          cd.EndDateTimeDisplay = cd.EndDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          cd.EndDateTimeBuffer = IIf(IsNullNothing(dr(10)), Nothing, dr(10))
          cd.ResourceBookingTypeID = dr(11)
          cd.ResourceBookingDescription = dr(12)
          cd.ResourceBookingType = dr(13)
          cd.ClashString = cd.ResourceBookingDescription & ": " & cd.StartDateTimeDisplay & " - " & cd.EndDateTimeDisplay
          rb.ClashDetails.Add(cd)
        End If
      Next
      Return ResourceBookings

    End Function

    Private Shared Function GetClashesDataSet(ResourceBookings As List(Of ResourceHelpers.ResourceBooking)) As DataSet

      Dim RBTable As DataTable = CreateResourceBookingTableParameter(ResourceBookings)
      Dim cmd As New Singular.CommandProc("RuleProcs.ruleResourcesAvailable")
      Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      ResourceBookingsParam.Name = "@ResourceBookings"
      ResourceBookingsParam.SqlType = SqlDbType.Structured
      ResourceBookingsParam.Value = RBTable
      cmd.Parameters.Add(ResourceBookingsParam)
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute(0)
      Return cmd.Dataset

    End Function

    Private Shared Function CreateResourceBookingTableParameter(ResourceBookings As List(Of ResourceHelpers.ResourceBooking)) As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceBookingID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("RoomScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionHRID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EquipmentScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceShiftID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceOffPeriodID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceSecondmentID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionSystemAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As ResourceHelpers.ResourceBooking In ResourceBookings
        Dim row As DataRow = RBTable.NewRow
        row("ObjectGuid") = rb.ObjectGuid
        row("ResourceID") = NothingDBNull(rb.ResourceID)
        row("ResourceBookingID") = NothingDBNull(rb.ResourceBookingID)
        row("StartDateTimeBuffer") = NothingDBNull(rb.StartDateTimeBuffer)
        row("StartDateTime") = rb.StartDateTime.Value.ToString
        row("EndDateTime") = rb.EndDateTime.Value.ToString
        row("EndDateTimeBuffer") = NothingDBNull(rb.EndDateTimeBuffer)
        row("RoomScheduleID") = NothingDBNull(rb.RoomScheduleID)
        row("ProductionHRID") = NothingDBNull(rb.ProductionHRID)
        row("EquipmentScheduleID") = NothingDBNull(rb.EquipmentScheduleID)
        row("HumanResourceShiftID") = NothingDBNull(rb.HumanResourceShiftID)
        row("HumanResourceOffPeriodID") = NothingDBNull(rb.HumanResourceOffPeriodID)
        row("HumanResourceSecondmentID") = NothingDBNull(rb.HumanResourceSecondmentID)
        row("ProductionSystemAreaID") = NothingDBNull(rb.ProductionSystemAreaID)
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

    Public Shared Function GetResourceBookingsCleanClashes(ResourceBookingsClean As List(Of ResourceBookingClean)) As List(Of ResourceBookingClean)

      Dim ds As DataSet = GetClashesDataSet(ResourceBookingsClean)
      ResourceBookingsClean.ForEach(Sub(c)
                                      c.ClashDetailList.Clear()
                                    End Sub)
      For Each dr As DataRow In ds.Tables(0).Rows
        Dim rb As ResourceBookingClean = ResourceBookingsClean.Where(Function(d) d.Guid = dr(0)).FirstOrDefault
        If rb IsNot Nothing Then
          Dim cd As New ClashDetail
          cd.ResourceBookingGuid = dr(0)
          'cd.LResourceID = dr(1)
          'cd.LResourceBookingID = dr(2)
          'cd.LStartDateTime = dr(3)
          'cd.LEndDateTime = dr(4)
          cd.ResourceID = dr(5)
          cd.ResourceBookingID = dr(6)
          cd.StartDateTimeBuffer = IIf(IsNullNothing(dr(7)), Nothing, dr(7))
          cd.StartDateTime = dr(8)
          cd.StartDateTimeDisplay = cd.StartDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          cd.EndDateTime = dr(9)
          cd.EndDateTimeDisplay = cd.EndDateTime.Value.ToString("ddd dd MMM yy HH:mm")
          cd.EndDateTimeBuffer = IIf(IsNullNothing(dr(10)), Nothing, dr(10))
          cd.ResourceBookingTypeID = dr(11)
          cd.ResourceBookingDescription = dr(12)
          cd.ResourceBookingType = dr(13)
          cd.MarkDirty()
          rb.ClashDetailList.Add(cd)
        End If
      Next
      Return ResourceBookingsClean

    End Function

    Private Shared Function GetClashesDataSet(ResourceBookings As List(Of OBLib.Resources.ResourceBookingClean)) As DataSet

      Dim RBTable As DataTable = CreateResourceBookingTableParameter(ResourceBookings)
      Dim cmd As New Singular.CommandProc("RuleProcs.ruleResourcesAvailable")
      Dim ResourceBookingsParam As Singular.CommandProc.Parameter = New Singular.CommandProc.Parameter()
      ResourceBookingsParam.Name = "@ResourceBookings"
      ResourceBookingsParam.SqlType = SqlDbType.Structured
      ResourceBookingsParam.Value = RBTable
      cmd.Parameters.Add(ResourceBookingsParam)
      cmd.FetchType = CommandProc.FetchTypes.DataSet
      cmd = cmd.Execute(0)
      Return cmd.Dataset

    End Function

    Private Shared Function CreateResourceBookingTableParameter(ResourceBookings As List(Of OBLib.Resources.ResourceBookingClean)) As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceBookingID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("StartDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTime", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EndDateTimeBuffer", GetType(System.DateTime)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("RoomScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionHRID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("EquipmentScheduleID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceShiftID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceOffPeriodID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("HumanResourceSecondmentID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionSystemAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As OBLib.Resources.ResourceBookingClean In ResourceBookings
        'If CompareSafe(rb.ResourceBookingID, 0) Then
        '  rb.ResourceBookingID = Nothing
        'End If
        Dim row As DataRow = RBTable.NewRow
        row("ObjectGuid") = rb.Guid
        row("ResourceID") = NothingDBNull(rb.ResourceID)
        If CompareSafe(rb.ResourceBookingID, 0) Then
          row("ResourceBookingID") = ZeroDBNull(rb.ResourceBookingID)
        Else
          row("ResourceBookingID") = NothingDBNull(rb.ResourceBookingID)
        End If
        row("StartDateTimeBuffer") = NothingDBNull(rb.StartDateTimeBuffer)
        row("StartDateTime") = rb.StartDateTime.Value.ToString
        row("EndDateTime") = rb.EndDateTime.Value.ToString
        row("EndDateTimeBuffer") = NothingDBNull(rb.EndDateTimeBuffer)
        row("RoomScheduleID") = NothingDBNull(rb.RoomScheduleID)
        row("ProductionHRID") = NothingDBNull(rb.ProductionHRID)
        row("EquipmentScheduleID") = NothingDBNull(rb.EquipmentScheduleID)
        row("HumanResourceShiftID") = NothingDBNull(rb.HumanResourceShiftID)
        row("HumanResourceOffPeriodID") = NothingDBNull(rb.HumanResourceOffPeriodID)
        row("HumanResourceSecondmentID") = NothingDBNull(rb.HumanResourceSecondmentID)
        row("ProductionSystemAreaID") = NothingDBNull(rb.ProductionSystemAreaID)
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

    Public Shared Function GetPendingChangesSerialised() As String

      Dim pendingNotifications As OBLib.Resources.ResourceBookingPendingChangeNotificationList = OBLib.Resources.ResourceBookingPendingChangeNotificationList.GetResourceBookingPendingChangeNotificationList(True)
      Dim AddedResourceBookingIDs As String = OBLib.OBMisc.IntegerListToXML(pendingNotifications.Where(Function(d) d.ActionTypeID = 1).Select(Function(d) d.ResourceBookingID).ToList)

      Dim EditStatusChangeTypeIDs As New List(Of Integer)
      EditStatusChangeTypeIDs.Add(4) 'InEdit
      EditStatusChangeTypeIDs.Add(5) 'OutOfEdit

      Dim UpdatedResourceBookingIDs As String = OBLib.OBMisc.IntegerListToXML(pendingNotifications.Where(Function(d) d.ActionTypeID = 2).Select(Function(d) d.ResourceBookingID).ToList)
      Dim DeletedResourceBookingIDs As List(Of Integer) = pendingNotifications.Where(Function(d) d.ActionTypeID = 3).Select(Function(d) d.ResourceBookingID).ToList
      Dim EditStatusChangeBookingIDs As String = OBLib.OBMisc.IntegerListToXML(pendingNotifications.Where(Function(d) EditStatusChangeTypeIDs.Contains(d.ActionTypeID)).Select(Function(d) d.ResourceBookingID).ToList)

      Dim addedBookings As ResourceBookingCleanList = New ResourceBookingCleanList
      Dim updatedBookings As ResourceBookingCleanList = New ResourceBookingCleanList
      Dim editStatusChangeBookings As ResourceBookingCleanList = New ResourceBookingCleanList

      If AddedResourceBookingIDs <> "" Then
        addedBookings = OBLib.Resources.ResourceBookingCleanList.GetResourceBookingCleanList("", Nothing, Nothing, AddedResourceBookingIDs, Nothing, Nothing)
      End If

      If UpdatedResourceBookingIDs <> "" Then
        updatedBookings = OBLib.Resources.ResourceBookingCleanList.GetResourceBookingCleanList("", Nothing, Nothing, UpdatedResourceBookingIDs, Nothing, Nothing)
      End If

      If EditStatusChangeBookingIDs <> "" Then
        editStatusChangeBookings = OBLib.Resources.ResourceBookingCleanList.GetResourceBookingCleanList("", Nothing, Nothing, EditStatusChangeBookingIDs, Nothing, Nothing)
      End If

      Dim wr As Singular.Web.Result = New Singular.Web.Result(True)
      wr.Data = New With {
                          .AddedBookings = addedBookings,
                          .UpdatedBookings = updatedBookings,
                          .RemovedBookingIDs = DeletedResourceBookingIDs,
                          .EditStatusBookings = editStatusChangeBookings
      }
      Dim SerialisedChanges As String = Singular.Web.Data.JSonWriter.SerialiseObject(wr)
      Return SerialisedChanges

    End Function

    <Serializable>
    Public Class ApplySelectionTemplate
      Inherits OBLib.OBBusinessBase(Of ApplySelectionTemplate)

      Public Property ResourceID As Integer?
      Public Property DisciplineID As Integer?
      Public Property ResourceBookingID As Integer?
      Public Property ProductionSystemAreaID As Integer?

      Public Sub New()

      End Sub

    End Class

    Public Shared Function CreateApplySelectionTemplateTableParameter(templateList As List(Of ApplySelectionTemplate)) As DataTable

      Dim RBTable As New DataTable
      RBTable.Columns.Add("ObjectGuid", GetType(Guid)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceID", GetType(Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("DisciplineID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ResourceBookingID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute
      RBTable.Columns.Add("ProductionSystemAreaID", GetType(System.Int32)).ColumnMapping = MappingType.Attribute

      Dim rowList As New List(Of DataRow)
      For Each rb As ApplySelectionTemplate In templateList
        Dim row As DataRow = RBTable.NewRow
        row("ObjectGuid") = rb.Guid
        row("ResourceID") = NothingDBNull(rb.ResourceID)
        row("DisciplineID") = NothingDBNull(rb.DisciplineID)
        row("ResourceBookingID") = NothingDBNull(rb.ResourceBookingID)
        row("ProductionSystemAreaID") = NothingDBNull(rb.ProductionSystemAreaID)
        RBTable.Rows.Add(row)
      Next

      Return RBTable

    End Function

    <Serializable>
    Public Class ApplyResult
      Inherits OBLib.OBBusinessBase(Of ApplyResult)

      Public Property MessageID As Integer?
      Public Property MessageTypeID As Integer?
      Public Property [Message] As String
      Public Property ResourceID As Integer?
      Public Property ResourceName As String
      Public Property ResourceBookingID As Integer?
      Public Property BookingDescription As String
      Public Property ProductionHumanResourceID As Integer?
      Public Property ProductionSystemAreaID As Integer?
      Public Property DisciplineID As Integer?
      Public Property Discipline As String
      'Public Property PositionID As Integer?
      'Public Property Position As String
      Public Property SortOrder As Integer
      Public Property ActionTypeID As Integer?
      Public Property HumanResourceID As Integer?
      Public Property HasProcessed As Boolean
      Public Property ProcessedSuccessfully As Boolean
      Public Property ProcessedSuccessfullyMessage As String
      Public Property ProcessingFailedMessage As String

      Public Shared AddAnywayProperty As PropertyInfo(Of Boolean) = RegisterSProperty(Of Boolean)(Function(c) c.AddAnyway, False)
      <Display(Name:="Reason", Description:="")>
      Public Property AddAnyway() As Boolean
        Get
          Return GetProperty(AddAnywayProperty)
        End Get
        Set(ByVal Value As Boolean)
          SetProperty(AddAnywayProperty, Value)
        End Set
      End Property

      Public Shared AddAnywayReasonProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.AddAnywayReason, "")
      <Display(Name:="Reason for adding")>
      Public Property AddAnywayReason() As String
        Get
          Return GetProperty(AddAnywayReasonProperty)
        End Get
        Set(ByVal Value As String)
          SetProperty(AddAnywayReasonProperty, Value)
        End Set
      End Property

      Protected Overrides Sub AddBusinessRules()
        MyBase.AddBusinessRules()

        With AddWebRule(AddAnywayReasonProperty)
          .JavascriptRuleFunctionName = "ApplyResultBO.AddAnywayReasonValid"
          .ServerRuleFunction = AddressOf AddAnywayReasonValid
          .AddTriggerProperty(AddAnywayProperty)
        End With

      End Sub

      Private Shared Function AddAnywayReasonValid(ApplyResult As ApplyResult) As String
        If ApplyResult.ActionTypeID IsNot Nothing Then
          If {1, 2}.Contains(ApplyResult.ActionTypeID) AndAlso ApplyResult.AddAnyway Then
            If ApplyResult.AddAnywayReason.Replace(" ", "").Length = 0 Then
              Return "Reason for adding is required"
            End If
          End If
        End If
        Return ""
      End Function

      Public Sub New()

      End Sub

      Public Sub New(dr As DataRow)
        MessageID = dr(0)
        MessageTypeID = dr(1)
        Message = dr(2)
        ResourceID = IIf(IsNullNothing(dr(3)), Nothing, dr(3))
        ResourceName = dr(4)
        ResourceBookingID = IIf(IsNullNothing(dr(5)), Nothing, dr(5))
        BookingDescription = dr(6)
        ProductionSystemAreaID = IIf(IsNullNothing(dr(7)), Nothing, dr(7))
        ProductionHumanResourceID = IIf(IsNullNothing(dr(8)), Nothing, dr(8))
        DisciplineID = IIf(IsNullNothing(dr(9)), Nothing, dr(9))
        Discipline = dr(10)
        SortOrder = dr(11)
        ActionTypeID = IIf(IsNullNothing(dr(12)), Nothing, dr(12))
        HumanResourceID = IIf(IsNullNothing(dr(13)), Nothing, dr(13))
        AddAnyway = False
        AddAnywayReason = ""
        IsProcessing = False
        HasProcessed = False
        ProcessedSuccessfully = False
        ProcessingFailedMessage = ""
        CheckAllRules()
      End Sub

    End Class

    <Serializable>
    Public Class BeforeAddHRHeader
      Inherits OBLib.OBBusinessBase(Of BeforeAddHRHeader)

      Public Property HoursFromPreviousBooking As Decimal?
      Public Property HoursToNextBooking As Decimal?
      Public Property HasShortfallWithPreviousBooking As Boolean
      Public Property HasShortfallWithNextBooking As Boolean

      Public Sub New()

      End Sub

      Public Sub New(dr As DataRow)
        HoursFromPreviousBooking = IIf(IsNullNothing(dr(0)), Nothing, dr(0))
        HoursToNextBooking = IIf(IsNullNothing(dr(1)), Nothing, dr(1))
        HasShortfallWithPreviousBooking = IIf(IsNullNothing(dr(2)), False, dr(2))
        HasShortfallWithNextBooking = IIf(IsNullNothing(dr(3)), False, dr(3))
        CheckAllRules()
      End Sub

    End Class

    <Serializable>
    Public Class BeforeAddHRToRoomScheduleResult
      Inherits OBLib.OBBusinessBase(Of BeforeAddHRToRoomScheduleResult)

      Public Property BeforeAddHRHeader As BeforeAddHRHeader
      Public Property Clashes As New List(Of ClashDetail)

      Public Sub New(ds As DataSet)
        'Header

        For Each dr As DataRow In ds.Tables(0).Rows
          BeforeAddHRHeader = New BeforeAddHRHeader(dr)
        Next

        'Clashes
        For Each dr As DataRow In ds.Tables(1).Rows
          Dim cd As New ClashDetail
          cd.ResourceBookingID = dr(0)
          cd.ResourceID = dr(1)
          cd.ResourceBookingTypeID = dr(2)
          cd.ResourceBookingDescription = dr(3)
          cd.StartDateTimeBuffer = IIf(IsNullNothing(dr(4)), Nothing, dr(4))
          cd.StartDateTime = dr(5)
          cd.EndDateTime = dr(6)
          cd.EndDateTimeBuffer = IIf(IsNullNothing(dr(7)), Nothing, dr(7))
          'cd.ResourceBookingType = dr(13)
          Me.Clashes.Add(cd)
        Next

      End Sub

    End Class

  End Class

  Public Class ErrorHelpers

    Public Shared Sub LogClientError(User As String, Page As String, Method As String, ErrorMessage As String)

      Try
        Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdInsClientError]",
                                      New String() {"@User", "@Page", "@Method", "@Error"},
                                      New Object() {User, Page, Method, ErrorMessage})
        cmd = cmd.Execute
      Catch ex As Exception
      End Try

    End Sub

  End Class

  <Serializable>
  Public Class CopyResult
    Inherits OBLib.OBBusinessBase(Of CopyResult)

    Public Property CopyDate As String = ""
    Public Property Success As Boolean = False
    Public Property Title As String = ""
    Public Property Description As String = ""

    Public Sub New(dr As DataRow)
      CopyDate = dr(0)
      Success = dr(1)
      Title = dr(2)
      Description = dr(3)
    End Sub

  End Class

End Namespace

<Serializable>
Public Class VehicleData
  Inherits OBLib.OBBusinessBase(Of VehicleData)
  Public Property DepartFrom As String = ""
  Public Property TravelTo As String = ""
  Public Property Distance As Integer = 0
  Public Property CouldFind As Boolean = False
End Class



