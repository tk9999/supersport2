﻿' Generated 20 Oct 2013 14:48 - Singular Systems Object Generator Version 2.1.665
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.Misc
Imports OBLib.Security.ReadOnly

Namespace Security

  <Serializable()> _
  Public Class UserList
    Inherits Singular.Security.UserListBase(Of UserList, User)

#Region " Business Methods "



    Public Overrides Function ToString() As String

      Return "Users"

    End Function

    Public Function GetUserSystem(UserSystemID As Integer) As UserSystem

      Dim obj As UserSystem = Nothing
      For Each parent As User In Me
        obj = parent.UserSystemList.GetItem(UserSystemID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Shared Function GetUser(UserID As Integer) As User

      Dim ul As UserList = OBLib.Security.UserList.GetUserList(UserID)
      If ul.Count = 0 Then
        Return Nothing
      End If
      Return ul(0)

    End Function

    Public Shared Function GetUserByUserName(Username As String) As User

      Dim ul As UserList = OBLib.Security.UserList.GetUserList(Username)
      If ul.Count = 0 Then
        Return Nothing
      End If
      Return ul(0)

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria

      Public Property UserID As Object
      '? = Nothing 

      Public Property FilterName As String
      '  Get
      '    Return ReadProperty(FilterNameProperty)
      '  End Get
      '  Set(value As String)
      '    LoadProperty(FilterNameProperty, value)
      '  End Set
      'End Property

      Public Property ContractTypeIDs As List(Of Integer)

      Public Property SystemIDs As List(Of Integer)

      Public Property ProductionAreaIDs As List(Of Integer)

      Public Property SelectedUserIDs As List(Of Integer)

      Public Property Username As String

      Public Sub New(UserID As Integer?)
        Me.UserID = UserID
      End Sub

      Public Sub New(Username As String)
        Me.Username = Username
      End Sub

      Public Sub New()

      End Sub

    End Class

#Region " Common "

    Public Shared Function NewUserList() As UserList

      Return New UserList()

    End Function

    Public Shared Sub BeginGetUserList(CallBack As EventHandler(Of DataPortalResult(Of UserList)))

      Dim dp As New DataPortal(Of UserList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetUserList() As UserList

      Return DataPortal.Fetch(Of UserList)(New Criteria())

    End Function

    Public Shared Function GetUserList(Crit As Criteria) As UserList

      Return DataPortal.Fetch(Of UserList)(Crit)

    End Function

    Public Shared Function GetUserList(UserID As Integer?) As UserList

      Return DataPortal.Fetch(Of UserList)(New Criteria(UserID))

    End Function

    Public Shared Function GetUserList(Username As String) As UserList

      Return DataPortal.Fetch(Of UserList)(New Criteria(Username))

    End Function


    Private Sub Fetch(sdr As SafeDataReader)


      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(User.GetUser(sdr))
      End While
      Me.RaiseListChangedEvents = True

      Dim parent As User = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.UserID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.UserSystemList.RaiseListChangedEvents = False
          parent.UserSystemList.Add(UserSystem.GetUserSystem(sdr))
          parent.UserSystemList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentUserSystem As UserSystem = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentUserSystem Is Nothing OrElse parentUserSystem.UserSystemID <> sdr.GetInt32(1) Then
            parentUserSystem = Me.GetUserSystem(sdr.GetInt32(1))
          End If
          parentUserSystem.UserSystemAreaList.RaiseListChangedEvents = False
          parentUserSystem.UserSystemAreaList.Add(UserSystemArea.GetUserSystemArea(sdr))
          parentUserSystem.UserSystemAreaList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.UserID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROUserResourceSchedulerList.RaiseListChangedEvents = False
          parent.ROUserResourceSchedulerList.Add(ROUserResourceScheduler.GetROUserResourceScheduler(sdr))
          parent.ROUserResourceSchedulerList.RaiseListChangedEvents = True
        End While
      End If

      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.UserID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.UserResourceSchedulerList.RaiseListChangedEvents = False
          parent.UserResourceSchedulerList.Add(UserResourceScheduler.GetUserResourceScheduler(sdr))
          parent.UserResourceSchedulerList.RaiseListChangedEvents = True
        End While
      End If

      MyBase.LoadChildren(sdr)

      For Each child As User In Me
        child.LoadFlatSystemAreaList()
        child.CheckRules()
        For Each UserSystem As UserSystem In child.UserSystemList
          UserSystem.CheckRules()
          For Each UserSystemArea As UserSystemArea In UserSystem.UserSystemAreaList
            UserSystemArea.CheckRules()
          Next
        Next
        For Each scheduler As UserResourceScheduler In child.UserResourceSchedulerList
          scheduler.CheckRules()
        Next
      Next

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getUserList"
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(crit.UserID))
            cm.Parameters.AddWithValue("@FilterName", Singular.Strings.MakeEmptyDBNull(crit.FilterName))
            cm.Parameters.AddWithValue("@ContractTypeIDs", Singular.Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.ContractTypeIDs)))
            cm.Parameters.AddWithValue("@SystemIDs", Singular.Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.SystemIDs)))
            cm.Parameters.AddWithValue("@ProductionAreaIDs", Singular.Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.ProductionAreaIDs)))
            cm.Parameters.AddWithValue("@SelectedUserIDs", Singular.Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.SelectedUserIDs)))
            cm.Parameters.AddWithValue("@Username", Singular.Strings.MakeEmptyDBNull(crit.Username))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

    Friend Sub Update()

      Me.RaiseListChangedEvents = False
      Try
        ' Loop through each deleted child object and call its Update() method
        For Each Child As User In DeletedList
          Child.DeleteSelf()
        Next

        ' Then clear the list of deleted objects because they are truly gone now.
        DeletedList.Clear()
        Dim WasDirty = Me.IsDirty
        ' Loop through each non-deleted child object and call its Update() method
        For Each Child As User In Me
          If Child.IsNew Then
            Child.Insert()
          Else
            Child.Update()
          End If
        Next
        If WasDirty Then
          CommonData.Refresh("UserList")
          CommonData.Refresh("ROUserList")
        End If
      Finally
        Me.RaiseListChangedEvents = True
      End Try

    End Sub

    Protected Overrides Sub DataPortal_Update()

      UpdateTransactional(AddressOf Update)

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace