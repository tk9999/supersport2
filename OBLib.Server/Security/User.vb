﻿' Generated 20 Oct 2013 14:48 - Singular Systems Object Generator Version 2.1.665
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports OBLib.Maintenance.General.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations
Imports OBLib.Security.ReadOnly

Namespace Security

  <Serializable()> _
  Public Class User
    Inherits Singular.Security.UserBase(Of User)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared UserTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserTypeID, "User Type", Nothing)
    ''' <summary>
    ''' Gets and sets the User Type value
    ''' </summary>
    <Display(Name:="User Type", Description:="The type of user applicable to this user account"),
    DropDownWeb(GetType(ROUserTypeList))>
    Public Property UserTypeID() As Integer?
      Get
        Return GetProperty(UserTypeIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(UserTypeIDProperty, Value)
      End Set
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, "Modified By", Nothing)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property
    'OBLib.Security.Settings.CurrentUser.UserID

    Public Shared ModifiedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDate, "Modified Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDate() As SmartDate
      Get
        Return GetProperty(ModifiedDateProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.HumanResourceID, "Human Resource", Nothing)
    ''' <summary>
    ''' Gets and sets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="Link to Human Resource"),
     DropDownWeb(GetType(OBLib.HR.ReadOnly.ROHumanResourceFindDropDownList), DropDownType:=DropDownWeb.SelectType.AutoComplete,
                         BeforeFetchJS:="UserBO.setHumanResourceCriteriaBeforeRefresh",
                         PreFindJSFunction:="UserBO.triggerHumanResourceAutoPopulate",
                         AfterFetchJS:="UserBO.afterHumanResourceRefreshAjax",
                         LookupMember:="HumanResource", ValueMember:="HumanResourceID", DisplayMember:="HumanResource",
                         DropDownColumns:={"PreferredFirstSurname", "ContractType", "EmployeeCode", "IDNo"},
                         OnItemSelectJSFunction:="UserBO.onHumanResourceSelected", DropDownCssClass:="hr-dropdown")>
    Public Property HumanResourceID() As Integer?
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(HumanResourceIDProperty, Value)
      End Set
    End Property

    Public Shared HumanResourceProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResource, "Human Resource", "")
    <Display(Name:="Human Resource", Description:="The Human Resource this user is")> _
    Public Property HumanResource As String
      Get
        Return GetProperty(HumanResourceProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(HumanResourceProperty, Value)
      End Set
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Primary Sub-Dept", Description:="The Sub-Dept this user will log into"),
    Required(ErrorMessage:="Primary Sub-Dept required"),
    Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.ReadOnly.ROSystemList))> _
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public ReadOnly Property ShortName As String
      Get
        If FirstName.Length > 0 Then
          Return FirstName.Substring(0, 1) & Surname
        End If
        Return ""
      End Get
    End Property

    Public ReadOnly Property FirstNameSurname As String
      Get
        Return FirstName & " " & Surname
      End Get
    End Property

    Public Shared FirstLoginProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.FirstLogin, "First Login", False)
    Public ReadOnly Property FirstLogin As Boolean
      Get
        Return GetProperty(FirstLoginProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type", "")
    <DisplayNameAttribute("Contract Type")> _
    Public ReadOnly Property ContractType As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "Contract Type", Nothing)
    <DisplayNameAttribute("Contract Type")> _
    Public ReadOnly Property ContractTypeID As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared IsUserFromServiceStudioProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsUserFromServiceStudio, "Service Studio", False)

    Public ReadOnly Property IsUserFromServiceStudio As Boolean
      Get
        Return GetProperty(IsUserFromServiceStudioProperty)
      End Get
    End Property

    Public Shared IsUserFromContentStudioProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsUserFromContentStudio, "Content Studio", False)

    Public ReadOnly Property IsUserFromContentStudio As Boolean
      Get
        Return GetProperty(IsUserFromContentStudioProperty)
      End Get
    End Property

    Public Shared IsUserFromOutsideBroadcastProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsUserFromOutsideBroadcast, "OBs", False)

    Public ReadOnly Property IsUserFromOutsideBroadcast As Boolean
      Get
        Return GetProperty(IsUserFromOutsideBroadcastProperty)
      End Get
    End Property

    'Public Shared EncrytedPasswordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EncrytedPassword, "EncrytedPassword")
    '<Display(Name:="Password"),
    'StringLength(100, ErrorMessage:="Password cannot be more than 100 characters"),
    'PasswordPropertyText()>
    'Public Property EncrytedPassword() As String
    '  Get
    '    Return "********"
    '  End Get
    '  Set(ByVal Value As String)
    '    If Value <> "********" Then
    '      SetProperty(EncrytedPasswordProperty, Singular.Encryption.EncryptString(Value))
    '      MyBase.Password = Value
    '    End If
    '  End Set
    'End Property

    'Public ReadOnly Property PasswordValid As Boolean
    '  Get
    '    If Singular.Debug.InDebugMode Then
    '      Return True
    '    Else
    '      Dim pc As New Singular.Misc.Password.PasswordChecker(8, 1, 1, 2, 0)
    '      Return pc.CheckPassword(Singular.Encryption.DecryptString(GetProperty(PasswordProperty)))
    '    End If
    '  End Get
    'End Property

    'Private mPasswordChecker As Singular.Misc.Password.PasswordChecker = Nothing
    'Private ReadOnly Property PasswordChecker As Singular.Misc.Password.PasswordChecker
    '  Get
    '    If mPasswordChecker Is Nothing Then
    '      mPasswordChecker = New Singular.Misc.Password.PasswordChecker(8, 1, 1, 2, 0)
    '    End If
    '    Return mPasswordChecker
    '  End Get
    'End Property

    Private mROHumanResourceFull As OBLib.HR.ReadOnly.ROHumanResourceFull = Nothing
    <Browsable(False)>
    Public ReadOnly Property ROHumanResourceFull As OBLib.HR.ReadOnly.ROHumanResourceFull
      Get
        If HumanResourceID <> 0 Then
          Return CommonData.Lists.ROHumanResourceFullList.GetItem(HumanResourceID)
        End If
        Return Nothing
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Primary Area", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Primary Area", Description:="The Production Area this user will log into"),
    Required(ErrorMessage:="Primary Area required"),
    Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaList),
                                         ValueMember:="ProductionAreaID", DisplayMember:="ProductionArea",
                                         ThisFilterMember:="SystemID")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area", "")
    <DisplayNameAttribute("Area")> _
    Public ReadOnly Property ProductionArea As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "Sub-Dept", "")
    <DisplayNameAttribute("Sub-Dept")> _
    Public ReadOnly Property System As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared NewPasswordProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.NewPassword, "")
    <Display(Name:="New Password", Description:="New Password"),
     PasswordPropertyText()>
    Public Property NewPassword() As String
      Get
        Return GetProperty(NewPasswordProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(NewPasswordProperty, Value)
      End Set
    End Property

    Public Shared LastLoginDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.LastLoginDate, "Modified Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date value
    ''' </summary>
    Public ReadOnly Property LastLoginDate() As SmartDate
      Get
        Return GetProperty(LastLoginDateProperty)
      End Get
    End Property

    'Public Shared HasRequestedPasswordResetProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasRequestedPasswordReset, "First Login", False)
    'Public Property HasRequestedPasswordReset As Boolean
    '  Get
    '    Return GetProperty(HasRequestedPasswordResetProperty)
    '  End Get
    '  Set(value As Boolean)
    '    SetProperty(HasRequestedPasswordResetProperty, value)
    '  End Set
    'End Property

    'Public Shared ResetPasswordSucceededProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ResetPasswordSucceeded, "First Login", False)
    'Public Property ResetPasswordSucceeded As Boolean
    '  Get
    '    Return GetProperty(ResetPasswordSucceededProperty)
    '  End Get
    '  Set(value As Boolean)
    '    SetProperty(ResetPasswordSucceededProperty, value)
    '  End Set
    'End Property

    'Public Shared HasRequestedLoginDetailsProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.HasRequestedLoginDetails, "First Login", False)
    'Public Property HasRequestedLoginDetails As Boolean
    '  Get
    '    Return GetProperty(HasRequestedLoginDetailsProperty)
    '  End Get
    '  Set(value As Boolean)
    '    SetProperty(HasRequestedLoginDetailsProperty, value)
    '  End Set
    'End Property

    'Public Shared LoginDetailsSentSucceededProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.LoginDetailsSentSucceeded, "First Login", False)
    'Public Property LoginDetailsSentSucceeded As Boolean
    '  Get
    '    Return GetProperty(LoginDetailsSentSucceededProperty)
    '  End Get
    '  Set(value As Boolean)
    '    SetProperty(LoginDetailsSentSucceededProperty, value)
    '  End Set
    'End Property

    Public Shared IsManagerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsManager, "Is Manager", False)
    Public Property IsManager As Boolean
      Get
        Return GetProperty(IsManagerProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsManagerProperty, value)
      End Set
    End Property

    Public Shared ManagedHRCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ManagedHRCount, "Managed HR Count", 0)
    Public Property ManagedHRCount As Integer
      Get
        Return GetProperty(ManagedHRCountProperty)
      End Get
      Set(value As Integer)
        SetProperty(ManagedHRCountProperty, value)
      End Set
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name", "")
    <DisplayNameAttribute("Preferred Name")> _
    Public Property PreferredName As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
      Set(value As String)
        SetProperty(PreferredNameProperty, value)
      End Set
    End Property

    Public Shared IDNumberProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNumber, "ID Number", "")
    <DisplayNameAttribute("ID Number")> _
    Public Property IDNumber As String
      Get
        Return GetProperty(IDNumberProperty)
      End Get
      Set(value As String)
        SetProperty(IDNumberProperty, value)
      End Set
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code", "")
    <DisplayNameAttribute("Employee Code")> _
    Public Property EmployeeCode As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
      Set(value As String)
        SetProperty(EmployeeCodeProperty, value)
      End Set
    End Property

    Public Shared SecondNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SecondName, "Second Name", "")
    <DisplayNameAttribute("Second Name")> _
    Public Property SecondName As String
      Get
        Return GetProperty(SecondNameProperty)
      End Get
      Set(value As String)
        SetProperty(SecondNameProperty, value)
      End Set
    End Property

    Private Property WasNew As Boolean

    Public Shared IsFreelancerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsFreelancer, "Is Freelancer", False)
    Public Property IsFreelancer As Boolean
      Get
        Return GetProperty(IsFreelancerProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsFreelancerProperty, value)
      End Set
    End Property

    Public Shared UserProfileLargeImageIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserProfileLargeImageID, "Managed HR Count", Nothing)
    Public Property UserProfileLargeImageID As Integer?
      Get
        Return GetProperty(UserProfileLargeImageIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(UserProfileLargeImageIDProperty, value)
      End Set
    End Property

    Public Shared UserProfileSmallImageIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserProfileSmallImageID, "Managed HR Count", Nothing)
    Public Property UserProfileSmallImageID As Integer?
      Get
        Return GetProperty(UserProfileSmallImageIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(UserProfileSmallImageIDProperty, value)
      End Set
    End Property

    Public Shared UserProfileMediumImageIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserProfileMediumImageID, "Managed HR Count", Nothing)
    Public Property UserProfileMediumImageID As Integer?
      Get
        Return GetProperty(UserProfileMediumImageIDProperty)
      End Get
      Set(value As Integer?)
        SetProperty(UserProfileMediumImageIDProperty, value)
      End Set
    End Property

    Public Shared IsEventManagerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsEventManager, "Is Freelancer", False)
    Public Property IsEventManager As Boolean
      Get
        Return GetProperty(IsEventManagerProperty)
      End Get
      Set(value As Boolean)
        SetProperty(IsEventManagerProperty, value)
      End Set
    End Property

    Public Shared DepartmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DepartmentID, "Department", Nothing)
    ''' <summary>
    ''' Gets and sets the Department value
    ''' </summary>
    <Display(Name:="Department", Description:="The department this user belongs to"),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Company.ReadOnly.RODepartmentList))>
    Public Property DepartmentID() As Integer?
      Get
        Return GetProperty(DepartmentIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(DepartmentIDProperty, Value)
      End Set
    End Property

    Public ReadOnly Property IsSatOpsUser As Boolean
      Get
        If (CompareSafe(SystemID, CType(OBLib.CommonData.Enums.System.ProductionContent, Integer)) AndAlso CompareSafe(ProductionAreaID, CType(OBLib.CommonData.Enums.ProductionArea.SatOps, Integer))) Or (UserID = 1) Then
          Return True
        Else
          Return False
        End If
      End Get
    End Property

    Public ReadOnly Property IsSingleSystemUser As Boolean
      Get
        Return (Me.ROUserSystemList.Count = 1)
      End Get
    End Property

    Public ReadOnly Property IsSingleAreaUser As Boolean
      Get
        Return (Me.ROUserSystemAreaList.Count = 1)
      End Get
    End Property

    Public ReadOnly Property IsPlayoutUser As Boolean
      Get
        Dim userSystem As UserSystem = Nothing
        userSystem = Me.UserSystemList.ToList.Where(Function(d) CompareSafe(d.SystemID, CType(OBLib.CommonData.Enums.System.PlayoutOperations, Integer))).FirstOrDefault
        If userSystem IsNot Nothing Then
          Return True
        Else
          Return False
        End If
      End Get
    End Property

    Public ReadOnly Property IsProductionContentUser As Boolean
      Get
        Dim userSystem As UserSystem = Nothing
        userSystem = Me.UserSystemList.ToList.Where(Function(d) CompareSafe(d.SystemID, CType(OBLib.CommonData.Enums.System.ProductionContent, Integer))).FirstOrDefault
        If userSystem IsNot Nothing Then
          Return True
        Else
          Return False
        End If
      End Get
    End Property

    Public ReadOnly Property IsProductionServicesUser As Boolean
      Get
        Dim userSystem As UserSystem = Nothing
        userSystem = Me.UserSystemList.ToList.Where(Function(d) CompareSafe(d.SystemID, CType(OBLib.CommonData.Enums.System.ProductionServices, Integer))).FirstOrDefault
        If userSystem IsNot Nothing Then
          Return True
        Else
          Return False
        End If
      End Get
    End Property

    Public ReadOnly Property IsOBUser As Boolean
      Get
        Dim userSystem As UserSystem = Nothing
        userSystem = Me.UserSystemList.ToList.Where(Function(d) d.UserSystemAreaList.ToList.Where(Function(c) CompareSafe(c.ProductionAreaID, CType(OBLib.CommonData.Enums.ProductionArea.OB, Integer))).Count > 0).FirstOrDefault
        If userSystem IsNot Nothing Then
          Return True
        Else
          Return False
        End If
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared UserSystemListProperty As PropertyInfo(Of UserSystemList) = RegisterProperty(Of UserSystemList)(Function(c) c.UserSystemList, "User System List")
    Public ReadOnly Property UserSystemList() As UserSystemList
      Get
        If GetProperty(UserSystemListProperty) Is Nothing Then
          LoadProperty(UserSystemListProperty, Security.UserSystemList.NewUserSystemList())
        End If
        Return GetProperty(UserSystemListProperty)
      End Get
    End Property

    Public Shared ROUserSystemListProperty As PropertyInfo(Of ROUserSystemList) = RegisterProperty(Of ROUserSystemList)(Function(c) c.ROUserSystemList, "User System List")
    Public ReadOnly Property ROUserSystemList() As ROUserSystemList
      Get
        If GetProperty(ROUserSystemListProperty) Is Nothing Then
          If Not Singular.Misc.IsNullNothing(Me.UserID, True) Then
            LoadProperty(ROUserSystemListProperty, OBLib.Maintenance.General.ReadOnly.ROUserSystemList.GetROUserSystemList(Me.UserID))
          Else
            LoadProperty(ROUserSystemListProperty, OBLib.Maintenance.General.ReadOnly.ROUserSystemList.NewROUserSystemList())
          End If
        End If
        Return GetProperty(ROUserSystemListProperty)
      End Get
    End Property

    Public Shared ROUserSystemAreaListProperty As PropertyInfo(Of ROUserSystemAreaList) = RegisterProperty(Of ROUserSystemAreaList)(Function(c) c.ROUserSystemAreaList, "User System Area List")
    Public ReadOnly Property ROUserSystemAreaList() As ROUserSystemAreaList
      Get
        If GetProperty(ROUserSystemAreaListProperty) Is Nothing Then
          If Not Singular.Misc.IsNullNothing(Me.UserID, True) Then
            LoadProperty(ROUserSystemAreaListProperty, OBLib.Maintenance.General.ReadOnly.ROUserSystemAreaList.GetROUserSystemAreaList(Me.UserID, Nothing))
          Else
            LoadProperty(ROUserSystemAreaListProperty, OBLib.Maintenance.General.ReadOnly.ROUserSystemAreaList.NewROUserSystemAreaList())
          End If
        End If
        Return GetProperty(ROUserSystemAreaListProperty)
      End Get
    End Property

    Public Shared ROUserResourceSchedulerListProperty As PropertyInfo(Of ROUserResourceSchedulerList) = RegisterProperty(Of ROUserResourceSchedulerList)(Function(c) c.ROUserResourceSchedulerList, "RO User System List")
    Public ReadOnly Property ROUserResourceSchedulerList() As ROUserResourceSchedulerList
      Get
        If GetProperty(ROUserResourceSchedulerListProperty) Is Nothing Then
          LoadProperty(ROUserResourceSchedulerListProperty, OBLib.Security.ReadOnly.ROUserResourceSchedulerList.NewROUserResourceSchedulerList)
        End If
        Return GetProperty(ROUserResourceSchedulerListProperty)
      End Get
    End Property

    Public Shared ROUserSystemAreaFlatListProperty As PropertyInfo(Of ROUserSystemAreaFlatList) = RegisterProperty(Of ROUserSystemAreaFlatList)(Function(c) c.ROUserSystemAreaFlatList, "User System List")
    Public ReadOnly Property ROUserSystemAreaFlatList() As ROUserSystemAreaFlatList
      Get
        If GetProperty(ROUserSystemAreaFlatListProperty) Is Nothing Then
          LoadProperty(ROUserSystemAreaFlatListProperty, Maintenance.General.ReadOnly.ROUserSystemAreaFlatList.NewROUserSystemAreaFlatList())
        End If
        Return GetProperty(ROUserSystemAreaFlatListProperty)
      End Get
    End Property

    Public Shared UserResourceSchedulerListProperty As PropertyInfo(Of UserResourceSchedulerList) = RegisterProperty(Of UserResourceSchedulerList)(Function(c) c.UserResourceSchedulerList, "User System List")
    Public ReadOnly Property UserResourceSchedulerList() As UserResourceSchedulerList
      Get
        If GetProperty(UserResourceSchedulerListProperty) Is Nothing Then
          LoadProperty(UserResourceSchedulerListProperty, Security.UserResourceSchedulerList.NewUserResourceSchedulerList())
        End If
        Return GetProperty(UserResourceSchedulerListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Friend Shared Function GetUser(ByVal dr As SafeDataReader) As User

      Dim u As New User()
      u.Fetch(dr)
      Return u

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserIDProperty)

    End Function

    Public Overrides Function ToString() As String
      If Me.IsNew Then
        Return String.Format("New {0}", "User")
      ElseIf Me.LoginName.Length = 0 Then
        Return String.Format("Blank {0}", "User")
      Else
        Return Me.LoginName
      End If

    End Function

    Protected Overrides ReadOnly Property TableReferencesToIgnore() As String()
      Get
        Return New String() {"SecurityGroupUsers"}
      End Get
    End Property

    Public Function NewPasswordValid(NewPassword As String, ByRef PasswordError As String) As Boolean
      Dim PasswordChecker = New Singular.Misc.Password.PasswordChecker(8, 1, 1, 2, 0)
      Dim IsValid = PasswordChecker.CheckPassword(NewPassword)
      PasswordError = PasswordChecker.ErrorMessage
      Return IsValid
    End Function

    Public Function CurrentPasswordValid() As String
      Dim PasswordChecker = New Singular.Misc.Password.PasswordChecker(8, 1, 1, 2, 0)
      Dim IsValid = PasswordChecker.CheckPassword(Singular.Encryption.DecryptString(GetProperty(PasswordProperty)))
      Return PasswordChecker.ErrorMessage
    End Function

    'Public Function IsEventManager(AtDate As Date) As Boolean

    '  If ROHumanResourceFull IsNot Nothing Then
    '    Return ROHumanResourceFull.IsEventManager(Now)
    '  End If
    '  Return False

    'End Function

    Public Sub LoadFlatSystemAreaList()

      For Each us As UserSystem In Me.UserSystemList
        Dim n As New ROUserSystemAreaFlat
        n.SetUserSystemProperties(us)
        For Each usa As UserSystemArea In us.UserSystemAreaList
          n.SetUserSystemAreaProperties(usa)
        Next
        ROUserSystemAreaFlatList.Add(n)
      Next

    End Sub

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()
      MyBase.AddBusinessRules()

      '  With AddWebRule(NewPasswordProperty, Function(c) c.IsNew And c.NewPassword = "", Function(c) "Password required")
      '    .ServerRuleFunction = Function(o)

      '                            If o.NewPassword <> "" Then
      '                              Dim PasswordError As String = ""
      '                              If o.NewPasswordValid(o.NewPassword, PasswordError) Then
      '                                o.Password = o.NewPassword
      '                              End If
      '                              Return PasswordError
      '                            End If
      '                            Return ""
      '                          End Function
      '    .ASyncBusyText = "Checking rule..."

      '  End With

      With AddWebRule(NewPasswordProperty)
        .ServerRuleFunction = AddressOf NewPasswordValid
        .ASyncBusyText = "Checking New Password..."
      End With

      With AddWebRule(LoginNameProperty)
        .ServerRuleFunction = AddressOf LoginNameValid
        .ASyncBusyText = "Checking Login Name..."
      End With

      With AddWebRule(EmailAddressProperty, Singular.Rules.RuleSeverity.Warning)
        .ServerRuleFunction = AddressOf EmailAddressValid
        .ASyncBusyText = "Checking Email Address..."
      End With

      With AddWebRule(HumanResourceIDProperty, Singular.Rules.RuleSeverity.Error)
        .ServerRuleFunction = AddressOf HumanResourceIDValid
        .ASyncBusyText = "Checking Human Resource..."
      End With

      With AddWebRule(SecurityGroupUserListProperty)
        .ServerRuleFunction = AddressOf SecurityGroupUserListValid
        '.JavascriptRuleCode = OBLib.JSCode.UserJS.SecurityGroupUserListValid
      End With

    End Sub

    Public Shared Function LoginNameValid(User As User) As String
      Dim ErrorMsg As String = ""
      If User.LoginName <> "" Then
        Dim cmd As New Singular.CommandProc("[cmdProcs].[cmdUserLoginNameExists]",
                                    New String() {"UserID", "LoginName"},
                                    New Object() {NothingDBNull(User.UserID), User.LoginName})
        cmd.FetchType = CommandProc.FetchTypes.DataObject
        cmd = cmd.Execute
        Dim Count As Integer = cmd.DataObject
        If Count > 0 Then
          ErrorMsg = "A user with this Login Name already exists"
        End If
      Else
        ErrorMsg = "Login Name is required"
      End If
      Return ErrorMsg
    End Function

    Public Shared Function NewPasswordValid(User As User) As String
      Dim ErrorMsg As String = ""
      If User.NewPassword <> "" Then
        If User.IsNewPasswordValid() Then
          Return ""
        Else
          ErrorMsg = "New Password does not meet the password policy"
        End If
      Else
        If User.IsNew Then
          ErrorMsg = "New Password is required"
        End If
      End If
      Return ErrorMsg
    End Function

    Public Shared Function SecurityGroupUserListValid(User As User) As String
      Dim ErrorMsg As String = ""
      If User.SecurityGroupUserList.Count = 0 Then
        ErrorMsg = "This user needs to be added to at least 1 security group"
      End If
      Return ErrorMsg
    End Function

    Public Shared Function EmailAddressValid(User As User) As String
      Dim ErrorMsg As String = ""
      If User.EmailAddress <> "" Then
        Dim cmd As New Singular.CommandProc("[CmdProcs].[cmdUserEmailAddressExists]",
                                    New String() {"UserID", "EmailAddress"},
                                    New Object() {NothingDBNull(User.UserID), User.EmailAddress})
        cmd.FetchType = CommandProc.FetchTypes.DataObject
        cmd = cmd.Execute
        Dim Count As Integer = cmd.DataObject
        If Count > 0 Then
          ErrorMsg = "A user and/or human resource with this email address already exists"
        End If
      Else
        ErrorMsg = "Email Address is required"
      End If
      Return ErrorMsg
    End Function

    Public Shared Function HumanResourceIDValid(User As User) As String
      Dim ErrorMsg As String = ""
      If Not IsNullNothing(User.HumanResourceID) Then
        Dim cmd As New Singular.CommandProc("[cmdProcs].[cmdUserHumanResourceIDExists]",
                                    New String() {"UserID", "HumanResourceID"},
                                    New Object() {NothingDBNull(User.UserID), NothingDBNull(User.HumanResourceID)})
        cmd.FetchType = CommandProc.FetchTypes.DataObject
        cmd = cmd.Execute
        Dim Count As Integer = cmd.DataObject
        If Count > 0 Then
          ErrorMsg = "This human resource is already linked to another user"
        End If
      Else
        ErrorMsg = ""
      End If
      Return ErrorMsg
    End Function

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewUser() method.

    End Sub

    Public Shared Function NewUser() As User

      Return DataPortal.CreateChild(Of User)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Protected Overrides Sub FetchExtraProperties(ByRef sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(ModifiedByProperty, .GetInt32(9))
          LoadProperty(ModifiedDateProperty, .GetSmartDate(10))
          LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(11)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
          LoadProperty(FirstLoginProperty, .GetBoolean(13))
          LoadProperty(ContractTypeProperty, .GetString(14))
          LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(15)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(16)))
          LoadProperty(UserTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(17)))
          LoadProperty(ProductionAreaProperty, .GetString(18))
          LoadProperty(SystemProperty, .GetString(19))
          LoadProperty(HumanResourceProperty, .GetString(20))
          'LoadProperty(LastLoginDateProperty, .GetValue(21))
          LoadProperty(IsManagerProperty, .GetBoolean(22))
          LoadProperty(ManagedHRCountProperty, .GetInt32(23))
          LoadProperty(PreferredNameProperty, .GetString(24))
          LoadProperty(IDNumberProperty, .GetString(25))
          LoadProperty(EmployeeCodeProperty, .GetString(26))
          LoadProperty(SecondNameProperty, .GetString(27))
          LoadProperty(IsFreelancerProperty, .GetBoolean(28))
          LoadProperty(UserProfileLargeImageIDProperty, ZeroNothing(.GetInt32(29)))
          LoadProperty(UserProfileSmallImageIDProperty, ZeroNothing(.GetInt32(30)))
          LoadProperty(UserProfileMediumImageIDProperty, ZeroNothing(.GetInt32(31)))
          LoadProperty(IsEventManagerProperty, .GetBoolean(32))
          LoadProperty(DepartmentIDProperty, ZeroNothing(.GetInt32(33)))
          LoadProperty(IsUserFromServiceStudioProperty, .GetBoolean(34))
          LoadProperty(IsUserFromContentStudioProperty, .GetBoolean(35))
          LoadProperty(IsUserFromOutsideBroadcastProperty, .GetBoolean(36))
        End With
      End Using

    End Sub

    Public Overrides Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insUser"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Public Overrides Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updUser"
        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub AddExtraParameters(ByRef Parameters As SqlClient.SqlParameterCollection)

      WasNew = IsNew

      Parameters.AddWithValue("@PasswordChangeDate", (New SmartDate(GetProperty(PasswordChangeDateProperty))).DBValue)
      Parameters.AddWithValue("@HumanResourceID", NothingDBNull(GetProperty(HumanResourceIDProperty)))
      Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
      Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
      Parameters.AddWithValue("@LoggedInUserID", OBLib.Security.Settings.CurrentUserID)
      If NewPassword Is Nothing Or NewPassword = "" Then
        Parameters.AddWithValue("@NewPassword", Strings.MakeEmptyDBNull(GetProperty(NewPasswordProperty)))
      Else
        Parameters.AddWithValue("@NewPassword", Singular.Encryption.EncryptString(GetProperty(NewPasswordProperty)))
      End If
      Parameters.AddWithValue("@UserTypeID", NothingDBNull(UserTypeID))
      Parameters.AddWithValue("@PreferredName", GetProperty(PreferredNameProperty))
      Parameters.AddWithValue("@IDNumber", GetProperty(IDNumberProperty))
      Parameters.AddWithValue("@EmployeeCode", GetProperty(EmployeeCodeProperty))
      Parameters.AddWithValue("@SecondName", GetProperty(SecondNameProperty))
      Parameters.AddWithValue("@UserProfileLargeImageID", NothingDBNull(GetProperty(UserProfileLargeImageIDProperty)))
      Parameters.AddWithValue("@UserProfileSmallImageID", NothingDBNull(GetProperty(UserProfileSmallImageIDProperty)))
      Parameters.AddWithValue("@UserProfileMediumImageID", NothingDBNull(GetProperty(UserProfileMediumImageIDProperty)))
    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delUser"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@UserID", GetProperty(UserIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

    Protected Overrides Sub UpdateChildLists()
      UserSystemList.Update()
      UpdateChild(UserResourceSchedulerList)
    End Sub

    Function SendLoginDetails(LoginUrl As String) As Singular.Web.Result
      Try
        OBLib.Emails.UsernamePasswordEmail.SendEmail(Me, LoginUrl)
        Return New Singular.Web.Result(True)
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try
    End Function

    Function GetPassword() As String
      Dim Pword As String = ""
      Dim cpr As New Singular.CommandProc("[CmdProcs].[cmdGetPassword]",
                                          New String() {"UserID"},
                                          New Object() {UserID})
      cpr.FetchType = CommandProc.FetchTypes.DataObject
      cpr = cpr.Execute
      Return cpr.DataObject
    End Function

    Function ResetPassword() As Singular.Web.Result
      Try
        NewPassword = Singular.Encryption.EncryptString(Singular.Misc.Password.CreateRandomPassword(8, False, True, True, True))
        'Commit the update to the database
        Dim cpr As New Singular.CommandProc("[CmdProcs].[cmdResetPassword]",
                                            New String() {"UserID", "NewPassword"},
                                            New Object() {UserID, NewPassword})
        cpr = cpr.Execute
        'Make the sure the updated password is on the user
        Me.Password = NewPassword
        'Refresh global list
        OBLib.CommonData.Lists.Refresh("UserList")
        Return New Singular.Web.Result(True)
      Catch ex As Exception
        Return New Singular.Web.Result(False) With {.ErrorText = ex.Message}
      End Try
    End Function

    Public Function IsCurrentPasswordValid() As Boolean
      Dim pw As String = GetPassword()
      Dim dec As String = Singular.Encryption.DecryptString(pw)
      Return PasswordValid(dec)
    End Function

    Public Function IsNewPasswordValid() As Boolean
      Return PasswordValid(NewPassword)
    End Function

    Public Function PasswordValid(DecryptedPassword As String) As Boolean
      Dim pc As New Singular.Misc.Password.PasswordChecker(8, 1, 1, 2, 0)
      Return pc.CheckPassword(DecryptedPassword)
    End Function

  End Class

End Namespace