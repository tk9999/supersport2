﻿' Generated 20 Oct 2013 14:48 - Singular Systems Object Generator Version 2.1.665
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Security

  <Serializable()> _
  Public Class UserSystem
    Inherits OBBusinessBase(Of UserSystem)

#Region " Properties and Methods "

#Region " Properties "

    Public Property IsExpanded As Boolean

    Public Shared UserSystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserSystemID, "User System", 0)
    ''' <summary>
    ''' Gets the User System value
    ''' </summary>
    <Key>
    Public Property UserSystemID() As Integer
      Get
        Return GetProperty(UserSystemIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(UserSystemIDProperty, value)
      End Set
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "User", Nothing)
    ''' <summary>
    ''' Gets the User value
    ''' </summary>
    Public ReadOnly Property UserID() As Integer?
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:=""),
    Required(ErrorMessage:="Sub-Dept required"),
    DropDownWeb(GetType(ROSystemList), DisplayMember:="System", ValueMember:="SystemID")>
    Public Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(SystemIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared SystemNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SystemName, "Sub-Dept", "")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept")>
    Public Property SystemName() As String
      Get
        Return GetProperty(SystemNameProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(SystemNameProperty, Value)
      End Set
    End Property

#End Region

#Region " Child Lists "

    Public Shared UserSystemAreaListProperty As PropertyInfo(Of UserSystemAreaList) = RegisterProperty(Of UserSystemAreaList)(Function(c) c.UserSystemAreaList, "User System Area List")

    Public ReadOnly Property UserSystemAreaList() As UserSystemAreaList
      Get
        If GetProperty(UserSystemAreaListProperty) Is Nothing Then
          LoadProperty(UserSystemAreaListProperty, Security.UserSystemAreaList.NewUserSystemAreaList())
        End If
        Return GetProperty(UserSystemAreaListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As User

      Return CType(CType(Me.Parent, UserSystemList).Parent, User)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserSystemIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.UserID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "User System")
        Else
          Return String.Format("Blank {0}", "User System")
        End If
      Else
        Return Me.UserID.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewUserSystem() method.

    End Sub

    Public Shared Function NewUserSystem() As UserSystem

      Return DataPortal.CreateChild(Of UserSystem)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetUserSystem(dr As SafeDataReader) As UserSystem

      Dim u As New UserSystem()
      u.Fetch(dr)
      Return u

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(UserSystemIDProperty, .GetInt32(0))
          LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(SystemNameProperty, .GetString(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcs.insUserSystem"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcs.updUserSystem"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramUserSystemID As SqlParameter = .Parameters.Add("@UserSystemID", SqlDbType.Int)
          paramUserSystemID.Value = GetProperty(UserSystemIDProperty)
          If Me.IsNew Then
            paramUserSystemID.Direction = ParameterDirection.Output
          End If
          .Parameters.AddWithValue("@UserID", Me.GetParent().UserID)
          .Parameters.AddWithValue("@SystemID", GetProperty(SystemIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(UserSystemIDProperty, paramUserSystemID.Value)
          End If
          ' update child objects
          UserSystemAreaList.Update()
          MarkOld()
        End With
      Else
        UserSystemAreaList.Update()
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcs.delUserSystem"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@UserSystemID", GetProperty(UserSystemIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace