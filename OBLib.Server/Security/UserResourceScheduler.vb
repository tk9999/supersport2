﻿' Generated 23 Jun 2016 08:01 - Singular Systems Object Generator Version 2.2.684
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.DataAnnotations
Imports OBLib.Scheduling.ReadOnly

Namespace Security

  <Serializable()> _
  Public Class UserResourceScheduler
    Inherits OBBusinessBase(Of UserResourceScheduler)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return UserResourceSchedulerBO.UserResourceSchedulerBOToString(self)")

    Public Shared UserResourceSchedulerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserResourceSchedulerID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public ReadOnly Property UserResourceSchedulerID() As Integer
      Get
        Return GetProperty(UserResourceSchedulerIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserID, "User", Nothing)
    ''' <summary>
    ''' Gets and sets the User value
    ''' </summary>
    <Display(Name:="User", Description:="")>
    Public Property UserID() As Integer?
      Get
        Return GetProperty(UserIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(UserIDProperty, Value)
      End Set
    End Property

    Public Shared ResourceSchedulerIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ResourceSchedulerID, "Resource Scheduler", Nothing)
    ''' <summary>
    ''' Gets and sets the Resource Scheduler value
    ''' </summary>
    <Display(Name:="Resource Scheduler", Description:=""),
    Required(ErrorMessage:="Resource Scheduler required"),
    DropDownWeb(GetType(ROResourceSchedulerList), DisplayMember:="ResourceScheduler", ValueMember:="ResourceSchedulerID")>
    Public Property ResourceSchedulerID() As Integer?
      Get
        Return GetProperty(ResourceSchedulerIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ResourceSchedulerIDProperty, Value)
      End Set
    End Property

    Public Shared IsAdministratorProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsAdministrator, "Is Administrator", False)
    ''' <summary>
    ''' Gets and sets the Is Administrator value
    ''' </summary>
    <Display(Name:="Is Administrator", Description:=""),
    Required(ErrorMessage:="Is Administrator required")>
    Public Property IsAdministrator() As Boolean
      Get
        Return GetProperty(IsAdministratorProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(IsAdministratorProperty, Value)
      End Set
    End Property

    Public Shared ReadOnlyAccessProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ReadOnlyAccess, "Read Only Access", True)
    ''' <summary>
    ''' Gets and sets the Read Only Access value
    ''' </summary>
    <Display(Name:="Read Only Access", Description:=""),
    Required(ErrorMessage:="Read Only Access required")>
    Public Property ReadOnlyAccess() As Boolean
      Get
        Return GetProperty(ReadOnlyAccessProperty)
      End Get
      Set(ByVal Value As Boolean)
        SetProperty(ReadOnlyAccessProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserResourceSchedulerIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.UserResourceSchedulerID.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "User Resource Scheduler")
        Else
          Return String.Format("Blank {0}", "User Resource Scheduler")
        End If
      Else
        Return Me.UserResourceSchedulerID.ToString()
      End If

    End Function

    Public Function GetParent() As User
      Return CType(CType(Me.Parent, UserResourceSchedulerList).Parent, User)
    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewUserResourceScheduler() method.

    End Sub

    Public Shared Function NewUserResourceScheduler() As UserResourceScheduler

      Return DataPortal.CreateChild(Of UserResourceScheduler)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

    Friend Shared Function GetUserResourceScheduler(dr As SafeDataReader) As UserResourceScheduler

      Dim u As New UserResourceScheduler()
      u.Fetch(dr)
      Return u

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(UserResourceSchedulerIDProperty, .GetInt32(0))
          LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ResourceSchedulerIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(IsAdministratorProperty, .GetBoolean(3))
          LoadProperty(ReadOnlyAccessProperty, .GetBoolean(4))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Protected Overrides Function SetupSaveCommand(cm As SqlCommand) As Action(Of SqlCommand)

      AddPrimaryKeyParam(cm, UserResourceSchedulerIDProperty)

      cm.Parameters.AddWithValue("@UserID", Me.GetParent.UserID)
      cm.Parameters.AddWithValue("@ResourceSchedulerID", GetProperty(ResourceSchedulerIDProperty))
      cm.Parameters.AddWithValue("@IsAdministrator", GetProperty(IsAdministratorProperty))
      cm.Parameters.AddWithValue("@ReadOnlyAccess", GetProperty(ReadOnlyAccessProperty))

      Return Sub()
               'Post Save
               If Me.IsNew Then
                 LoadProperty(UserResourceSchedulerIDProperty, cm.Parameters("@UserResourceSchedulerID").Value)
               End If
             End Sub

    End Function

    Protected Overrides Sub SaveChildren()
      'No Children
    End Sub

    Protected Overrides Sub SetupDeleteCommand(cm As SqlCommand)
      cm.Parameters.AddWithValue("@UserResourceSchedulerID", GetProperty(UserResourceSchedulerIDProperty))
    End Sub

#End Region

  End Class

End Namespace