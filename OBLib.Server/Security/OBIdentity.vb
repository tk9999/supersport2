Imports Csla
Imports Csla.Serialization
Imports System.Linq.Expressions

Namespace Security

  <Serializable()>
  Public Class OBIdentity
    Inherits Singular.Web.Security.WebIdentity(Of OBIdentity)

    Private mHumanResourceID As Integer
    Public ReadOnly Property HumanResourceID As Integer
      Get
        Return mHumanResourceID
      End Get
    End Property

    Protected Overrides Sub SetupSqlCommand(cmd As System.Data.SqlClient.SqlCommand, Criteria As Singular.Security.IdentityCriterea)
      MyBase.SetupSqlCommand(cmd, Criteria)

      cmd.CommandText = "GetProcs.WebLogin"
      cmd.Parameters("@Password").Value = Singular.Encryption.EncryptString(cmd.Parameters("@Password").Value)
    End Sub

    Protected Overrides Sub ReadExtraProperties(sdr As Data.SafeDataReader, ByRef StartIndex As Integer)
      MyBase.ReadExtraProperties(sdr, StartIndex)

      mHumanResourceID = sdr.GetInt32(7)
    End Sub

  End Class

End Namespace
