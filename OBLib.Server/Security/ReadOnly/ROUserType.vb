﻿' Generated 26 Feb 2015 07:15 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Security

  <Serializable()> _
  Public Class ROUserType
    Inherits SingularReadOnlyBase(Of ROUserType)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared UserTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserTypeID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
  Public ReadOnly Property UserTypeID() As Integer
      Get
        Return GetProperty(UserTypeIDProperty)
      End Get
    End Property

    Public Shared UserTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.UserType, "User Type", "")
    ''' <summary>
    ''' Gets the User Type value
    ''' </summary>
    <Display(Name:="User Type", Description:="")>
  Public ReadOnly Property UserType() As String
      Get
        Return GetProperty(UserTypeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserTypeIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.UserType

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserType(dr As SafeDataReader) As ROUserType

      Dim r As New ROUserType()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(UserTypeIDProperty, .GetInt32(0))
        LoadProperty(UserTypeProperty, .GetString(1))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace