﻿' Generated 15 Feb 2016 10:25 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROUserSystemAreaList
    Inherits OBReadOnlyListBase(Of ROUserSystemAreaList, ROUserSystemArea)

#Region " Business Methods "

    Public Function GetItem(UserSystemAreaID As Integer) As ROUserSystemArea

      For Each child As ROUserSystemArea In Me
        If child.UserSystemAreaID = UserSystemAreaID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property UserID As Integer?
      Public Property SystemID As Integer?
      <PrimarySearchField>
      Public Property UserSystemArea As String

      Public Sub New(UserID As Integer?, SystemID As Integer?)
        Me.UserID = UserID
        Me.SystemID = SystemID
      End Sub

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROUserSystemAreaList() As ROUserSystemAreaList

      Return New ROUserSystemAreaList()

    End Function

    Public Shared Sub BeginGetROUserSystemAreaList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROUserSystemAreaList)))

      Dim dp As New DataPortal(Of ROUserSystemAreaList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROUserSystemAreaList(CallBack As EventHandler(Of DataPortalResult(Of ROUserSystemAreaList)))

      Dim dp As New DataPortal(Of ROUserSystemAreaList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROUserSystemAreaList() As ROUserSystemAreaList

      Return DataPortal.Fetch(Of ROUserSystemAreaList)(New Criteria())

    End Function

    Public Shared Function GetROUserSystemAreaList(UserID As Integer?, SystemID As Integer?) As ROUserSystemAreaList

      Return DataPortal.Fetch(Of ROUserSystemAreaList)(New Criteria(UserID, SystemID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUserSystemArea.GetROUserSystemArea(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROUserSystemAreaList"
            cm.Parameters.AddWithValue("@UserID", Singular.Misc.NothingDBNull(crit.UserID))
            cm.Parameters.AddWithValue("@SystemID", Singular.Misc.NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@UserSystemArea", Singular.Strings.MakeEmptyDBNull(crit.UserSystemArea))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
