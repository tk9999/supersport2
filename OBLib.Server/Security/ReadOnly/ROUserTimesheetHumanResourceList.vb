﻿' Generated 13 Jul 2016 16:38 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()>
  Public Class ROUserTimesheetHumanResourceList
    Inherits OBReadOnlyListBase(Of ROUserTimesheetHumanResourceList, ROUserTimesheetHumanResource)

#Region " Business Methods "

    Public Function GetItem(HumanResourceID As Integer) As ROUserTimesheetHumanResource

      For Each child As ROUserTimesheetHumanResource In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property UserID As Integer?

      <Singular.DataAnnotations.PrimarySearchField>
      Public Property User As String = ""

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROUserTimesheetHumanResourceList() As ROUserTimesheetHumanResourceList

      Return New ROUserTimesheetHumanResourceList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROUserTimesheetHumanResourceList() As ROUserTimesheetHumanResourceList

      Return DataPortal.Fetch(Of ROUserTimesheetHumanResourceList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUserTimesheetHumanResource.GetROUserTimesheetHumanResource(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROUserTimesheetHumanResourceList"
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(OBLib.Security.Settings.CurrentUser.UserID))
            cm.Parameters.AddWithValue("@User", Strings.MakeEmptyDBNull(crit.User))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace