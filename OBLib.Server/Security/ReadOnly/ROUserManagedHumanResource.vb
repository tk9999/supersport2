﻿' Generated 09 Apr 2016 20:59 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Users.ReadOnly

  <Serializable()> _
  Public Class ROUserManagedHumanResource
    Inherits OBReadOnlyBase(Of ROUserManagedHumanResource)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared HumanResourceIDKeyProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceIDKey, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property HumanResourceIDKey() As Integer
      Get
        Return GetProperty(HumanResourceIDKeyProperty)
      End Get
    End Property

    Public Shared ManagerHumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ManagerHumanResourceID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(Name:="Manager", Description:="")>
    Public ReadOnly Property ManagerHumanResourceID() As Integer
      Get
        Return GetProperty(ManagerHumanResourceIDProperty)
      End Get
    End Property

    Public Shared ManagerNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ManagerName, "Manager Name")
    ''' <summary>
    ''' Gets the Manager Name value
    ''' </summary>
    <Display(Name:="Manager Name", Description:="")>
    Public ReadOnly Property ManagerName() As String
      Get
        Return GetProperty(ManagerNameProperty)
      End Get
    End Property

    Public Shared ManagementLevelProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ManagementLevel, "Management Level")
    ''' <summary>
    ''' Gets the Management Level value
    ''' </summary>
    <Display(Name:="Management Level", Description:="")>
    Public ReadOnly Property ManagementLevel() As Integer
      Get
        Return GetProperty(ManagementLevelProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceName, "Human Resource Name")
    ''' <summary>
    ''' Gets the Human Resource Name value
    ''' </summary>
    <Display(Name:="Human Resource Name", Description:="")>
    Public ReadOnly Property HumanResourceName() As String
      Get
        Return GetProperty(HumanResourceNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(HumanResourceIDKeyProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ManagerName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserManagedHumanResource(dr As SafeDataReader) As ROUserManagedHumanResource

      Dim r As New ROUserManagedHumanResource()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(HumanResourceIDKeyProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(ManagerHumanResourceIDProperty, .GetInt32(0))
        LoadProperty(ManagerNameProperty, .GetString(1))
        LoadProperty(ManagementLevelProperty, .GetInt32(2))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(HumanResourceNameProperty, .GetString(4))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace