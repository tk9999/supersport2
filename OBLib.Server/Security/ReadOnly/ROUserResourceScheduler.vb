﻿' Generated 09 Mar 2016 15:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Security.ReadOnly

  <Serializable()> _
  Public Class ROUserResourceScheduler
    Inherits OBReadOnlyBase(Of ROUserResourceScheduler)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared UserResourceSchedulerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserResourceSchedulerID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property UserResourceSchedulerID() As Integer
      Get
        Return GetProperty(UserResourceSchedulerIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserID, "User")
    ''' <summary>
    ''' Gets the User value
    ''' </summary>
    <Display(Name:="User", Description:="")>
    Public ReadOnly Property UserID() As Integer
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared ResourceSchedulerIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ResourceSchedulerID, "Resource Scheduler")
    ''' <summary>
    ''' Gets the Resource Scheduler value
    ''' </summary>
    <Display(Name:="Resource Scheduler", Description:="")>
    Public ReadOnly Property ResourceSchedulerID() As Integer
      Get
        Return GetProperty(ResourceSchedulerIDProperty)
      End Get
    End Property

    Public Shared IsAdministratorProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsAdministrator, "Is Administrator", False)
    ''' <summary>
    ''' Gets the Is Administrator value
    ''' </summary>
    <Display(Name:="Is Administrator", Description:="")>
    Public ReadOnly Property IsAdministrator() As Boolean
      Get
        Return GetProperty(IsAdministratorProperty)
      End Get
    End Property

    Public Shared ReadOnlyAccessProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ReadOnlyAccess, "Read Only Access", False)
    ''' <summary>
    ''' Gets the Read Only Access value
    ''' </summary>
    <Display(Name:="Read Only Access", Description:="")>
    Public ReadOnly Property ReadOnlyAccess() As Boolean
      Get
        Return GetProperty(ReadOnlyAccessProperty)
      End Get
    End Property

    Public Shared ResourceSchedulerProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ResourceScheduler, "Resource Scheduler")
    ''' <summary>
    ''' Gets the Resource Scheduler value
    ''' </summary>
    <Display(Name:="Resource Scheduler", Description:="")>
    Public ReadOnly Property ResourceScheduler() As String
      Get
        Return GetProperty(ResourceSchedulerProperty)
      End Get
    End Property

    Public Shared PageNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PageName, "PageName")
    ''' <summary>
    ''' Gets the Resource Scheduler value
    ''' </summary>
    <Display(Name:="PageName")>
    Public ReadOnly Property PageName() As String
      Get
        Return GetProperty(PageNameProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserResourceSchedulerIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.ResourceScheduler

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserResourceScheduler(dr As SafeDataReader) As ROUserResourceScheduler

      Dim r As New ROUserResourceScheduler()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(UserResourceSchedulerIDProperty, .GetInt32(0))
        LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(ResourceSchedulerIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(IsAdministratorProperty, .GetBoolean(3))
        LoadProperty(ReadOnlyAccessProperty, .GetBoolean(4))
        LoadProperty(ResourceSchedulerProperty, .GetString(5))
        LoadProperty(PageNameProperty, .GetString(6))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace