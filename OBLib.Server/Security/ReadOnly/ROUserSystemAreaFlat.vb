﻿' Generated 15 Feb 2016 10:25 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROUserSystemAreaFlat
    Inherits OBReadOnlyBase(Of ROUserSystemAreaFlat)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared UserSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserSystemAreaID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key, Browsable(True)>
    Public ReadOnly Property UserSystemAreaID() As Integer
      Get
        Return GetProperty(UserSystemAreaIDProperty)
      End Get
    End Property

    Public Shared UserSystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserSystemID, "User System")
    ''' <summary>
    ''' Gets the User System value
    ''' </summary>
    <Display(Name:="User System", Description:="")>
    Public ReadOnly Property UserSystemID() As Integer
      Get
        Return GetProperty(UserSystemIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets the User System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "Sub-Dept")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="Sub-Dept", Description:="")>
    Public ReadOnly Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="Area", Description:="")>
    Public ReadOnly Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    'Public Function GetParent() As ROUserSystem

    '  Return CType(CType(Me.Parent, ROUserSystemAreaFlatList).Parent, ROUserSystem)

    'End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserSystemAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.UserSystemAreaID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserSystemAreaFlat(dr As SafeDataReader) As ROUserSystemAreaFlat

      Dim r As New ROUserSystemAreaFlat()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(UserSystemAreaIDProperty, .GetInt32(0))
        LoadProperty(UserSystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SystemIDProperty, .GetInt32(2))
        LoadProperty(SystemProperty, .GetString(3))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(ProductionAreaProperty, .GetString(5))
      End With

    End Sub

#End If

#End Region

#End Region

    Sub SetUserSystemProperties(us As Security.UserSystem)
      LoadProperty(UserSystemIDProperty, us.UserSystemID)
      LoadProperty(SystemIDProperty, us.SystemID)
      LoadProperty(SystemProperty, us.SystemName)
    End Sub

    Sub SetUserSystemAreaProperties(usa As Security.UserSystemArea)
      LoadProperty(UserSystemAreaIDProperty, usa.UserSystemAreaID)
      LoadProperty(ProductionAreaIDProperty, usa.ProductionAreaID)
      LoadProperty(ProductionAreaProperty, usa.ProductionArea)
    End Sub

  End Class

End Namespace
