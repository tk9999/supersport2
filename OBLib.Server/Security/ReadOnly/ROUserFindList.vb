﻿' Generated 30 Jun 2016 17:50 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Security

  <Serializable()> _
  Public Class ROUserFindList
    Inherits OBReadOnlyListBase(Of ROUserFindList, ROUserFind)

#Region " Business Methods "

    Public Function GetItem(UserID As Integer) As ROUserFind

      For Each child As ROUserFind In Me
        If child.UserID = UserID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      <Singular.DataAnnotations.PrimarySearchField>
      Public Property User As String

      Public Property UserID As Integer?
      Public Property DisciplineID As Integer?
      Public Property SystemID As Integer?
      Public Property ProductionAreaID As Integer?

      Public Sub New(UserID As Integer?, DisciplineID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?)
        Me.UserID = UserID
        Me.DisciplineID = DisciplineID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
      End Sub

      Public Sub New()

      End Sub

    End Class

    Public Shared Function NewROUserFindList() As ROUserFindList

      Return New ROUserFindList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROUserFindList() As ROUserFindList

      Return DataPortal.Fetch(Of ROUserFindList)(New Criteria())

    End Function

    Public Shared Function GetROUserFindList(UserID As Integer?, DisciplineID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?) As ROUserFindList

      Return DataPortal.Fetch(Of ROUserFindList)(New Criteria(UserID, DisciplineID, SystemID, ProductionAreaID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUserFind.GetROUserFind(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROUserFindList"
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(crit.UserID))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@User", Strings.MakeEmptyDBNull(crit.User))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace
