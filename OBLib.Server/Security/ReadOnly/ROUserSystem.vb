﻿' Generated 15 Feb 2016 09:59 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROUserSystem
    Inherits OBReadOnlyBase(Of ROUserSystem)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared UserSystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserSystemID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property UserSystemID() As Integer
      Get
        Return GetProperty(UserSystemIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserID, "User")
    ''' <summary>
    ''' Gets the User value
    ''' </summary>
    <Display(Name:="User", Description:="")>
    Public ReadOnly Property UserID() As Integer
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared DepartmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DepartmentID, "Department")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="Department", Description:="")>
    Public ReadOnly Property DepartmentID() As Integer
      Get
        Return GetProperty(DepartmentIDProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROUserSystemAreaListProperty As PropertyInfo(Of ROUserSystemAreaList) = RegisterProperty(Of ROUserSystemAreaList)(Function(c) c.ROUserSystemAreaList, "RO User System Area List")

    Public ReadOnly Property ROUserSystemAreaList() As ROUserSystemAreaList
      Get
        If GetProperty(ROUserSystemAreaListProperty) Is Nothing Then
          LoadProperty(ROUserSystemAreaListProperty, OBLib.Maintenance.General.ReadOnly.ROUserSystemAreaList.NewROUserSystemAreaList)
        End If
        Return GetProperty(ROUserSystemAreaListProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserSystemIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.UserSystemID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserSystem(dr As SafeDataReader) As ROUserSystem

      Dim r As New ROUserSystem()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(UserSystemIDProperty, .GetInt32(0))
        LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(SystemProperty, .GetString(3))
        LoadProperty(CreatedByProperty, .GetInt32(4))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(5))
        LoadProperty(ModifiedByProperty, .GetInt32(6))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(7))
        LoadProperty(DepartmentIDProperty, .GetInt32(8))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
