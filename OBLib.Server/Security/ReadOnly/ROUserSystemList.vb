﻿' Generated 15 Feb 2016 09:59 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular.Misc

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROUserSystemList
    Inherits OBReadOnlyListBase(Of ROUserSystemList, ROUserSystem)

#Region " Business Methods "

    Public Function GetItem(UserSystemID As Integer) As ROUserSystem

      For Each child As ROUserSystem In Me
        If child.UserSystemID = UserSystemID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Property UserID As Integer?

      Public Sub New(UserID As Integer?)
        Me.UserID = UserID
      End Sub

      Public Sub New()
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROUserSystemList() As ROUserSystemList

      Return New ROUserSystemList()

    End Function

    Public Shared Sub BeginGetROUserSystemList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROUserSystemList)))

      Dim dp As New DataPortal(Of ROUserSystemList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROUserSystemList(CallBack As EventHandler(Of DataPortalResult(Of ROUserSystemList)))

      Dim dp As New DataPortal(Of ROUserSystemList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROUserSystemList() As ROUserSystemList

      Return DataPortal.Fetch(Of ROUserSystemList)(New Criteria())

    End Function

    Public Shared Function GetROUserSystemList(UserID As Integer?) As ROUserSystemList

      Return DataPortal.Fetch(Of ROUserSystemList)(New Criteria(UserID))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUserSystem.GetROUserSystem(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROUserSystemList"
            cm.Parameters.AddWithValue("@UserID", Singular.Misc.NothingDBNull(crit.UserID))
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

    Function HasAccessToSubDept(SystemID As Integer) As Boolean

      Dim list As List(Of ROUserSystem) = Me.Where(Function(d) CompareSafe(d.SystemID, SystemID)).ToList
      If list.Count > 0 Then
        Return True
      Else
        Return False
      End If

    End Function

  End Class
End Namespace
