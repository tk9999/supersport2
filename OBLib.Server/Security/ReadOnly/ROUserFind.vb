﻿' Generated 30 Jun 2016 17:50 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Security

  <Serializable()> _
  Public Class ROUserFind
    Inherits OBReadOnlyBase(Of ROUserFind)

#Region " Properties and Methods "

#Region " Properties "


    Public Shared ToStringProperty As PropertyInfo(Of String) = RegisterReadOnlyProperty(Of String)(Function(c) c.ToString, "return ROUserFindBO.ROUserFindBOToString(self)")

    Public Shared UserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property UserID() As Integer
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared UserTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserTypeID, "User Type")
    ''' <summary>
    ''' Gets the User Type value
    ''' </summary>
    <Display(Name:="User Type", Description:="")>
    Public ReadOnly Property UserTypeID() As Integer
      Get
        Return GetProperty(UserTypeIDProperty)
      End Get
    End Property

    Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "First Name")
    ''' <summary>
    ''' Gets the First Name value
    ''' </summary>
    <Display(Name:="First Name", Description:="")>
    Public ReadOnly Property FirstName() As String
      Get
        Return GetProperty(FirstNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "Login Name")
    ''' <summary>
    ''' Gets the Login Name value
    ''' </summary>
    <Display(Name:="Login Name", Description:="")>
    Public ReadOnly Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
    End Property

    Public Shared PasswordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Password, "Password")
    ''' <summary>
    ''' Gets the Password value
    ''' </summary>
    <Display(Name:="Password", Description:="")>
    Public ReadOnly Property Password() As String
      Get
        Return GetProperty(PasswordProperty)
      End Get
    End Property

    Public Shared PasswordChangeDateProperty As PropertyInfo(Of Date) = RegisterProperty(Of Date)(Function(c) c.PasswordChangeDate, "Password Change Date")
    ''' <summary>
    ''' Gets the Password Change Date value
    ''' </summary>
    <Display(Name:="Password Change Date", Description:="")>
    Public ReadOnly Property PasswordChangeDate As Date
      Get
        Return GetProperty(PasswordChangeDateProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As DateTime
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDate, "Modified Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDate() As DateTime
      Get
        Return GetProperty(ModifiedDateProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource")
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared FullNameProperty As PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.FullName)
    ''' <summary>
    ''' Gets the Full Name value
    ''' </summary>
    <Display(Name:="Full Name", Description:="")>
    Public ReadOnly Property FullName() As String
      Get
        Return GetProperty(FullNameProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractType() As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Production Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property System() As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared IsManagerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsManager, "Is Manager", False)
    ''' <summary>
    ''' Gets the Is Manager value
    ''' </summary>
    <Display(Name:="Is Manager", Description:="")>
    Public ReadOnly Property IsManager() As Boolean
      Get
        Return GetProperty(IsManagerProperty)
      End Get
    End Property

    Public Shared ManagedHRCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ManagedHRCount, "Managed HR Count")
    ''' <summary>
    ''' Gets the Managed HR Count value
    ''' </summary>
    <Display(Name:="Managed HR Count", Description:="")>
    Public ReadOnly Property ManagedHRCount() As Integer
      Get
        Return GetProperty(ManagedHRCountProperty)
      End Get
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name")
    ''' <summary>
    ''' Gets the Preferred Name value
    ''' </summary>
    <Display(Name:="Preferred Name", Description:="")>
    Public ReadOnly Property PreferredName() As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ContractTypeID, "Contract Type")
    ''' <summary>
    ''' Gets the Contract Type value
    ''' </summary>
    <Display(Name:="Contract Type", Description:="")>
    Public ReadOnly Property ContractTypeID() As Integer
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID No")
    ''' <summary>
    ''' Gets the ID No value
    ''' </summary>
    <Display(Name:="ID No", Description:="")>
    Public ReadOnly Property IDNo() As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code")
    ''' <summary>
    ''' Gets the Employee Code value
    ''' </summary>
    <Display(Name:="Employee Code", Description:="")>
    Public ReadOnly Property EmployeeCode() As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public Shared SecondNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SecondName, "Second Name")
    ''' <summary>
    ''' Gets the Second Name value
    ''' </summary>
    <Display(Name:="Second Name", Description:="")>
    Public ReadOnly Property SecondName() As String
      Get
        Return GetProperty(SecondNameProperty)
      End Get
    End Property

    Public Shared IsFreelancerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsFreelancer, "Is Freelancer", False)
    ''' <summary>
    ''' Gets the Is Freelancer value
    ''' </summary>
    <Display(Name:="Is Freelancer", Description:="")>
    Public ReadOnly Property IsFreelancer() As Boolean
      Get
        Return GetProperty(IsFreelancerProperty)
      End Get
    End Property

    Public Shared UserProfileLargeImageIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserProfileLargeImageID, "User Profile Large Image")
    ''' <summary>
    ''' Gets the User Profile Large Image value
    ''' </summary>
    <Display(Name:="User Profile Large Image", Description:="")>
    Public ReadOnly Property UserProfileLargeImageID() As Integer
      Get
        Return GetProperty(UserProfileLargeImageIDProperty)
      End Get
    End Property

    Public Shared UserProfileSmallImageIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserProfileSmallImageID, "User Profile Small Image")
    ''' <summary>
    ''' Gets the User Profile Small Image value
    ''' </summary>
    <Display(Name:="User Profile Small Image", Description:="")>
    Public ReadOnly Property UserProfileSmallImageID() As Integer
      Get
        Return GetProperty(UserProfileSmallImageIDProperty)
      End Get
    End Property

    Public Shared UserProfileMediumImageIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserProfileMediumImageID, "User Profile Medium Image")
    ''' <summary>
    ''' Gets the User Profile Medium Image value
    ''' </summary>
    <Display(Name:="User Profile Medium Image", Description:="")>
    Public ReadOnly Property UserProfileMediumImageID() As Integer
      Get
        Return GetProperty(UserProfileMediumImageIDProperty)
      End Get
    End Property

    Public Shared DepartmentIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.DepartmentID, "Department")
    ''' <summary>
    ''' Gets the Department value
    ''' </summary>
    <Display(Name:="Department", Description:="")>
    Public ReadOnly Property DepartmentID() As Integer
      Get
        Return GetProperty(DepartmentIDProperty)
      End Get
    End Property

    Public Shared LinkedToHRProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.LinkedToHR, "Linked To HR", False)
    ''' <summary>
    ''' Gets the Linked To HR value
    ''' </summary>
    <Display(Name:="Linked To HR", Description:="")>
    Public ReadOnly Property LinkedToHR() As Boolean
      Get
        Return GetProperty(LinkedToHRProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FirstName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

    Friend Shared Function GetROUserFind(dr As SafeDataReader) As ROUserFind

      Dim r As New ROUserFind()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(UserIDProperty, .GetInt32(0))
        LoadProperty(UserTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(FirstNameProperty, .GetString(2))
        LoadProperty(SurnameProperty, .GetString(3))
        LoadProperty(LoginNameProperty, .GetString(4))
        LoadProperty(PasswordProperty, .GetString(5))
        LoadProperty(PasswordChangeDateProperty, .GetValue(6))
        LoadProperty(EmailAddressProperty, .GetString(7))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateProperty, .GetDateTime(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateProperty, .GetDateTime(11))
        LoadProperty(HumanResourceIDProperty, Singular.Misc.ZeroNothing(.GetInt32(12)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(FullNameProperty, .GetString(14))
        LoadProperty(ContractTypeProperty, .GetString(15))
        LoadProperty(ProductionAreaProperty, .GetString(16))
        LoadProperty(SystemProperty, .GetString(17))
        LoadProperty(IsManagerProperty, .GetBoolean(18))
        LoadProperty(ManagedHRCountProperty, .GetInt32(19))
        LoadProperty(PreferredNameProperty, .GetString(20))
        LoadProperty(ContractTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(21)))
        LoadProperty(IDNoProperty, .GetString(22))
        LoadProperty(EmployeeCodeProperty, .GetString(23))
        LoadProperty(SecondNameProperty, .GetString(24))
        LoadProperty(IsFreelancerProperty, .GetBoolean(25))
        LoadProperty(UserProfileLargeImageIDProperty, Singular.Misc.ZeroNothing(.GetInt32(26)))
        LoadProperty(UserProfileSmallImageIDProperty, Singular.Misc.ZeroNothing(.GetInt32(27)))
        LoadProperty(UserProfileMediumImageIDProperty, Singular.Misc.ZeroNothing(.GetInt32(28)))
        LoadProperty(DepartmentIDProperty, Singular.Misc.ZeroNothing(.GetInt32(29)))
        LoadProperty(LinkedToHRProperty, .GetBoolean(30))
      End With

    End Sub

#End Region

  End Class

End Namespace
