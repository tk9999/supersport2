﻿' Generated 15 Feb 2016 11:32 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROUserSystemAreaFlattened
    Inherits OBReadOnlyBase(Of ROUserSystemAreaFlattened)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared UserSystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserSystemID, "ID")
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key>
    Public ReadOnly Property UserSystemID() As Integer
      Get
        Return GetProperty(UserSystemIDProperty)
      End Get
    End Property

    Public Shared UserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserID, "User")
    ''' <summary>
    ''' Gets the User value
    ''' </summary>
    <Display(Name:="User", Description:="")>
    Public ReadOnly Property UserID() As Integer
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.SystemID, "System")
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="")>
    Public ReadOnly Property SystemID() As Integer
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared UserSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserSystemAreaID, "User System Area")
    ''' <summary>
    ''' Gets the User System Area value
    ''' </summary>
    <Display(Name:="User System Area", Description:="")>
    Public ReadOnly Property UserSystemAreaID() As Integer
      Get
        Return GetProperty(UserSystemAreaIDProperty)
      End Get
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ProductionAreaID, "Production Area")
    ''' <summary>
    ''' Gets the Production Area value
    ''' </summary>
    <Display(Name:="Production Area", Description:="")>
    Public ReadOnly Property ProductionAreaID() As Integer
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
    End Property

    Public Shared SystemManagementIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.SystemManagementInd, "System Management", False)
    ''' <summary>
    ''' Gets the System Management value
    ''' </summary>
    <Display(Name:="System Management", Description:="")>
    Public ReadOnly Property SystemManagementInd() As Boolean
      Get
        Return GetProperty(SystemManagementIndProperty)
      End Get
    End Property

    Public Shared EditTeamIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.EditTeamInd, "Edit Team", False)
    ''' <summary>
    ''' Gets the Edit Team value
    ''' </summary>
    <Display(Name:="Edit Team", Description:="")>
    Public ReadOnly Property EditTeamInd() As Boolean
      Get
        Return GetProperty(EditTeamIndProperty)
      End Get
    End Property

    Public Shared EditShiftPatternsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.EditShiftPatternsInd, "Edit Shift Patterns", False)
    ''' <summary>
    ''' Gets the Edit Shift Patterns value
    ''' </summary>
    <Display(Name:="Edit Shift Patterns", Description:="")>
    Public ReadOnly Property EditShiftPatternsInd() As Boolean
      Get
        Return GetProperty(EditShiftPatternsIndProperty)
      End Get
    End Property

    Public Shared ViewShiftPatternsIndProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.ViewShiftPatternsInd, "View Shift Patterns", False)
    ''' <summary>
    ''' Gets the View Shift Patterns value
    ''' </summary>
    <Display(Name:="View Shift Patterns", Description:="")>
    Public ReadOnly Property ViewShiftPatternsInd() As Boolean
      Get
        Return GetProperty(ViewShiftPatternsIndProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As DateTime
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of DateTime) = RegisterProperty(Of DateTime)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As DateTime
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserSystemIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.UserSystemID.ToString()

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUserSystemAreaFlattened(dr As SafeDataReader) As ROUserSystemAreaFlattened

      Dim r As New ROUserSystemAreaFlattened()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(UserSystemIDProperty, .GetInt32(0))
        LoadProperty(UserIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
        LoadProperty(UserSystemAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(3)))
        LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(4)))
        LoadProperty(SystemManagementIndProperty, .GetBoolean(5))
        LoadProperty(EditTeamIndProperty, .GetBoolean(6))
        LoadProperty(EditShiftPatternsIndProperty, .GetBoolean(7))
        LoadProperty(ViewShiftPatternsIndProperty, .GetBoolean(8))
        LoadProperty(CreatedByProperty, .GetInt32(9))
        LoadProperty(CreatedDateTimeProperty, .GetDateTime(10))
        LoadProperty(ModifiedByProperty, .GetInt32(11))
        LoadProperty(ModifiedDateTimeProperty, .GetDateTime(12))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
