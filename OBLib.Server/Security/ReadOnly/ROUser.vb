﻿' Generated 29 Aug 2014 12:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Security.ReadOnly

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROUser
    Inherits OBReadOnlyBase(Of ROUser)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared UserIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Display(AutoGenerateField:=False), Key()>
    Public ReadOnly Property UserID() As Integer
      Get
        Return GetProperty(UserIDProperty)
      End Get
    End Property

    Public Shared UserTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserTypeID, "User Type", Nothing)
    ''' <summary>
    ''' Gets the User Type value
    ''' </summary>
    <Display(Name:="User Type", Description:="UserTypeID ex UserTypes table")>
    Public ReadOnly Property UserTypeID() As Integer?
      Get
        Return GetProperty(UserTypeIDProperty)
      End Get
    End Property

    Public Shared FirstNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstName, "First Name", "")
    ''' <summary>
    ''' Gets the First Name value
    ''' </summary>
    <Display(Name:="First Name", Description:="User's first name")>
    Public ReadOnly Property FirstName() As String
      Get
        Return GetProperty(FirstNameProperty)
      End Get
    End Property

    Public Shared SurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Surname, "Surname", "")
    ''' <summary>
    ''' Gets the Surname value
    ''' </summary>
    <Display(Name:="Surname", Description:="User's surname")>
    Public ReadOnly Property Surname() As String
      Get
        Return GetProperty(SurnameProperty)
      End Get
    End Property

    Public Shared LoginNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.LoginName, "Login Name", "")
    ''' <summary>
    ''' Gets the Login Name value
    ''' </summary>
    <Display(Name:="Login Name", Description:="User's login name")>
    Public ReadOnly Property LoginName() As String
      Get
        Return GetProperty(LoginNameProperty)
      End Get
    End Property

    Public Shared PasswordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.Password, "Password", "pNuoSYc4K0V9kBHxS5arOw==")
    ''' <summary>
    ''' Gets the Password value
    ''' </summary>
    <Display(Name:="Password", Description:="Password for login (Consult Investec on their Security standards)")>
    Public ReadOnly Property Password() As String
      Get
        Return GetProperty(PasswordProperty)
      End Get
    End Property

    Public Shared PasswordChangeDateProperty As PropertyInfo(Of DateTime?) = RegisterProperty(Of DateTime?)(Function(c) c.PasswordChangeDate, "Password Change Date")
    ''' <summary>
    ''' Gets the Password Change Date value
    ''' </summary>
    <Display(Name:="Password Change Date", Description:="The date the user last changed their password. Used with Password Expiry in misc to determine if a user must change their password.")>
    Public ReadOnly Property PasswordChangeDate As DateTime?
      Get
        Return GetProperty(PasswordChangeDateProperty)
      End Get
    End Property

    Public Shared EmailAddressProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmailAddress, "Email Address", "")
    ''' <summary>
    ''' Gets the Email Address value
    ''' </summary>
    <Display(Name:="Email Address", Description:="Email address of the user")>
    Public ReadOnly Property EmailAddress() As String
      Get
        Return GetProperty(EmailAddressProperty)
      End Get
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDate, "Created Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDate() As SmartDate
      Get
        Return GetProperty(CreatedDateProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDate, "Modified Date", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDate() As SmartDate
      Get
        Return GetProperty(ModifiedDateProperty)
      End Get
    End Property

    Public Shared HumanResourceIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.HumanResourceID, "Human Resource", 0)
    ''' <summary>
    ''' Gets the Human Resource value
    ''' </summary>
    <Display(Name:="Human Resource", Description:="Link to Human Resource")>
    Public ReadOnly Property HumanResourceID() As Integer
      Get
        Return GetProperty(HumanResourceIDProperty)
      End Get
    End Property

    Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "System", Nothing)
    ''' <summary>
    ''' Gets the System value
    ''' </summary>
    <Display(Name:="System", Description:="The system this user will log into")>
    Public ReadOnly Property SystemID() As Integer?
      Get
        Return GetProperty(SystemIDProperty)
      End Get
    End Property

    Public Shared FirstSurnameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.FirstSurname, "First Name", "")
    ''' <summary>
    ''' Gets the First Name value
    ''' </summary>
    <Display(Name:="Full Name", Description:="User's full name")>
    Public ReadOnly Property FirstSurname() As String
      Get
        Return GetProperty(FirstSurnameProperty)
      End Get
    End Property

    Public Shared ContractTypeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ContractType, "Contract Type", "")
    <DisplayNameAttribute("Contract Type")> _
    Public ReadOnly Property ContractType As String
      Get
        Return GetProperty(ContractTypeProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Production Area", "")
    <DisplayNameAttribute("Production Area")> _
    Public ReadOnly Property ProductionArea As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
    End Property

    Public Shared SystemProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.System, "System", "")
    <DisplayNameAttribute("System")> _
    Public ReadOnly Property System As String
      Get
        Return GetProperty(SystemProperty)
      End Get
    End Property

    Public Shared HumanResourceNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.HumanResourceName, "Human Resource", "")
    <DisplayNameAttribute("Human Resource")> _
    Public ReadOnly Property HumanResourceName As String
      Get
        Return GetProperty(HumanResourceNameProperty)
      End Get
    End Property

    Public Shared IsManagerProperty As PropertyInfo(Of Boolean) = RegisterProperty(Of Boolean)(Function(c) c.IsManager, "Is Manager", False)
    Public ReadOnly Property IsManager As Boolean
      Get
        Return GetProperty(IsManagerProperty)
      End Get
    End Property

    Public Shared ManagedHRCountProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ManagedHRCount, "Managed HR Count", 0)
    Public ReadOnly Property ManagedHRCount As Integer
      Get
        Return GetProperty(ManagedHRCountProperty)
      End Get
    End Property

    Public Shared PreferredNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.PreferredName, "Preferred Name", "")
    <DisplayNameAttribute("Preferred Name")> _
    Public ReadOnly Property PreferredName As String
      Get
        Return GetProperty(PreferredNameProperty)
      End Get
    End Property

    Public Shared ContractTypeIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ContractTypeID, "Contract Type", Nothing)
    <DisplayNameAttribute("Contract Type")> _
    Public ReadOnly Property ContractTypeID As Integer?
      Get
        Return GetProperty(ContractTypeIDProperty)
      End Get
    End Property

    Public Shared IDNoProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.IDNo, "ID Number", "")
    <DisplayNameAttribute("ID Number")> _
    Public ReadOnly Property IDNo As String
      Get
        Return GetProperty(IDNoProperty)
      End Get
    End Property

    Public Shared EmployeeCodeProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.EmployeeCode, "Employee Code", "")
    <DisplayNameAttribute("Employee Code")> _
    Public ReadOnly Property EmployeeCode As String
      Get
        Return GetProperty(EmployeeCodeProperty)
      End Get
    End Property

    Public Shared SecondNameProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.SecondName, "Second Name", "")
    <DisplayNameAttribute("Second Name")> _
    Public ReadOnly Property SecondName As String
      Get
        Return GetProperty(SecondNameProperty)
      End Get
    End Property

    Public Shared DepartmentIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DepartmentID, "Department", Nothing)
    <DisplayNameAttribute("Department")> _
    Public ReadOnly Property DepartmentID As Integer?
      Get
        Return GetProperty(DepartmentIDProperty)
      End Get
    End Property

#End Region

#Region " Child Lists "

    Public Shared ROUserSystemListProperty As PropertyInfo(Of ROUserSystemList) = RegisterProperty(Of ROUserSystemList)(Function(c) c.ROUserSystemList, "RO User System List")
    Public ReadOnly Property ROUserSystemList() As ROUserSystemList
      Get
        If GetProperty(ROUserSystemListProperty) Is Nothing Then
          LoadProperty(ROUserSystemListProperty, OBLib.Maintenance.General.ReadOnly.ROUserSystemList.NewROUserSystemList)
        End If
        Return GetProperty(ROUserSystemListProperty)
      End Get
    End Property

#End Region

#Region " Methods "


    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserIDProperty)

    End Function

    Public Overrides Function ToString() As String

      Return Me.FirstName

    End Function

#End Region

#End Region

#Region " Data Access & Factory Methods "

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetROUser(dr As SafeDataReader) As ROUser

      Dim r As New ROUser()
      r.Fetch(dr)
      Return r

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      With sdr
        LoadProperty(UserIDProperty, .GetInt32(0))
        LoadProperty(UserTypeIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
        LoadProperty(FirstNameProperty, .GetString(2))
        LoadProperty(SurnameProperty, .GetString(3))
        LoadProperty(LoginNameProperty, .GetString(4))
        LoadProperty(PasswordProperty, .GetString(5))
        LoadProperty(PasswordChangeDateProperty, .GetValue(6))
        LoadProperty(EmailAddressProperty, .GetString(7))
        LoadProperty(CreatedByProperty, .GetInt32(8))
        LoadProperty(CreatedDateProperty, .GetSmartDate(9))
        LoadProperty(ModifiedByProperty, .GetInt32(10))
        LoadProperty(ModifiedDateProperty, .GetSmartDate(11))
        LoadProperty(HumanResourceIDProperty, .GetInt32(12))
        LoadProperty(SystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(13)))
        LoadProperty(FirstSurnameProperty, .GetString(14))
        LoadProperty(ContractTypeProperty, .GetString(15))
        LoadProperty(ProductionAreaProperty, .GetString(16))
        LoadProperty(SystemProperty, .GetString(17))
        LoadProperty(HumanResourceNameProperty, .GetString(18))
        LoadProperty(IsManagerProperty, .GetBoolean(19))
        LoadProperty(ManagedHRCountProperty, .GetInt32(20))
        LoadProperty(PreferredNameProperty, .GetString(21))
        LoadProperty(ContractTypeIDProperty, ZeroNothing(.GetInt32(22)))
        LoadProperty(IDNoProperty, .GetString(23))
        LoadProperty(EmployeeCodeProperty, .GetString(24))
        LoadProperty(SecondNameProperty, .GetString(25))
        LoadProperty(DepartmentIDProperty, .GetInt32(30))
      End With

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace