﻿' Generated 29 Aug 2014 12:44 - Singular Systems Object Generator Version 2.1.669
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc
Imports OBLib.Security.ReadOnly
Imports Singular.DataAnnotations

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROUserList
    Inherits OBReadOnlyListBase(Of ROUserList, ROUser)
    Implements Singular.Paging.IPagedList


    Private mTotalRecords As Integer = 0
    Public Property UserScreenInd As Boolean = False
    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property


#Region " Business Methods "

    Public Function GetItem(UserID As Integer) As ROUser

      For Each child As ROUser In Me
        If child.UserID = UserID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetROUserSystem(UserSystemID As Integer) As ROUserSystem

      Dim obj As ROUserSystem = Nothing
      For Each parent As ROUser In Me
        obj = parent.ROUserSystemList.GetItem(UserSystemID)
        If obj IsNot Nothing Then
          Return obj
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Users"

    End Function

#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property UserScreenInd As Boolean = False

      Public Shared FilterNameProperty As Csla.PropertyInfo(Of String) = RegisterSProperty(Of String)(Function(c) c.FilterName)
      <Singular.DataAnnotations.TextField(),
      SetExpression("ROUserListCriteriaBO.FilterNameSet(self)", , 200)>
      Public Property FilterName As String
        Get
          Return ReadProperty(FilterNameProperty)
        End Get
        Set(value As String)
          LoadProperty(FilterNameProperty, value)
        End Set
      End Property

      Public Property UserID As Integer?

      Public Property ContractTypeIDs As List(Of Integer)

      '<Display(Name:="System", Order:=1), ClientOnly>
      Public Property SystemIDs As List(Of Integer)

      '<Display(Name:="Area", Order:=2), ClientOnly>
      Public Property ProductionAreaIDs As List(Of Integer)

      Public Sub New()
        Me.PageSize = 20
        Me.SortColumn = "FirstName"
      End Sub

      Public Sub New(UserScreenInd As Boolean, FilterName As String, UserID As Integer?)
        Me.UserScreenInd = UserScreenInd
        Me.FilterName = FilterName
        Me.UserID = UserID
      End Sub

      Public Sub New(PageNo As Integer, PageSize As Integer, SortColumn As String, SortAsc As Boolean)
        Me.PageNo = PageNo
        Me.PageSize = PageSize
        Me.SortColumn = SortColumn
        Me.SortAsc = SortAsc
      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROUserList() As ROUserList

      Return New ROUserList()

    End Function

    Public Shared Sub BeginGetROUserList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROUserList)))

      Dim dp As New DataPortal(Of ROUserList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROUserList(CallBack As EventHandler(Of DataPortalResult(Of ROUserList)))

      Dim dp As New DataPortal(Of ROUserList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROUserList() As ROUserList

      Return DataPortal.Fetch(Of ROUserList)(New Criteria())

    End Function

    Public Shared Function GetROUserList(PageNo As Integer, PageSize As Integer, SortColumn As String, SortAsc As Boolean) As ROUserList

      Return DataPortal.Fetch(Of ROUserList)(New Criteria(PageNo, PageSize, SortColumn, SortAsc))

    End Function

    Public Shared Function GetROUserList(Crit As Criteria) As ROUserList

      Return DataPortal.Fetch(Of ROUserList)(Crit)

    End Function


    Private Sub Fetch(sdr As SafeDataReader)

      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUser.GetROUser(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

      Dim parent As ROUser = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parent Is Nothing OrElse parent.UserID <> sdr.GetInt32(1) Then
            parent = Me.GetItem(sdr.GetInt32(1))
          End If
          parent.ROUserSystemList.RaiseListChangedEvents = False
          parent.ROUserSystemList.Add(ROUserSystem.GetROUserSystem(sdr))
          parent.ROUserSystemList.RaiseListChangedEvents = True
        End While
      End If

      Dim parentROUserSystem As ROUserSystem = Nothing
      If sdr.NextResult() Then
        While sdr.Read
          If parentROUserSystem Is Nothing OrElse parentROUserSystem.UserSystemID <> sdr.GetInt32(1) Then
            parentROUserSystem = Me.GetROUserSystem(sdr.GetInt32(1))
          End If
          parentROUserSystem.ROUserSystemAreaList.RaiseListChangedEvents = False
          parentROUserSystem.ROUserSystemAreaList.Add(ROUserSystemArea.GetROUserSystemArea(sdr))
          parentROUserSystem.ROUserSystemAreaList.RaiseListChangedEvents = True
        End While
      End If

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROUserList"
            cm.Parameters.AddWithValue("@FilterName", Singular.Strings.MakeEmptyDBNull(crit.FilterName))
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(crit.UserID))
            cm.Parameters.AddWithValue("@ContractTypeIDs", Singular.Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.ContractTypeIDs)))
            cm.Parameters.AddWithValue("@SystemIDs", Singular.Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.SystemIDs)))
            cm.Parameters.AddWithValue("@ProductionAreaIDs", Singular.Strings.MakeEmptyDBNull(OBMisc.IntegerListToXML(crit.ProductionAreaIDs)))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace