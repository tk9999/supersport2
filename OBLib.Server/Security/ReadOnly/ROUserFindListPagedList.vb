﻿' Generated 01 Jul 2016 18:00 - Singular Systems Object Generator Version 2.2.687
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Maintenance.General.ReadOnly

  <Serializable()> _
  Public Class ROUserFindListPagedList
    Inherits OBReadOnlyListBase(Of ROUserFindListPagedList, ROUserFindListPaged)
    Implements Singular.Paging.IPagedList

#Region " Business Methods "

    Public Function GetItem(UserID As Integer) As ROUserFindListPaged

      For Each child As ROUserFindListPaged In Me
        If child.UserID = UserID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

    Private mTotalRecords As Integer = 0

    Public ReadOnly Property TotalRecords As Integer Implements Singular.Paging.IPagedList.TotalRecords
      Get
        Return mTotalRecords
      End Get
    End Property
#End Region

#Region " Data Access "

    <Serializable(), Singular.Web.WebFetchable(LoggedInOnly:=True)> _
    Public Class Criteria
      Inherits Paging.PageCriteria(Of Criteria)

      Public Property UserID As Integer?

      Public Shared SystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.SystemID, "Sub-Dept", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Sub-Dept", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.ReadOnly.ROSystemList), DisplayMember:="System", ValueMember:="SystemID"),
       Singular.DataAnnotations.SetExpression("ROUserFindListPagedBO.SystemIDSet(self)")>
      Public Overridable Property SystemID As Integer?
        Get
          Return ReadProperty(SystemIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(SystemIDProperty, Value)
        End Set
      End Property

      Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="Area", Description:=""), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.Areas.ReadOnly.ROProductionAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID"),
       Singular.DataAnnotations.SetExpression("ROUserFindListPagedBO.ProductionAreaIDSet(self)")>
      Public Overridable Property ProductionAreaID As Integer?
        Get
          Return ReadProperty(ProductionAreaIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(ProductionAreaIDProperty, Value)
        End Set
      End Property

      Public Shared DisciplineIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.DisciplineID, "Discipline ID", Nothing)
      ''' <summary>
      ''' Gets and sets the Discipline value
      ''' </summary>
      <Display(Name:="Discipline", Description:="Discipline the user has access to view"),
      Singular.DataAnnotations.DropDownWeb(GetType(Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplineList),
       UnselectedText:="Discipline", DisplayMember:="Discipline", ValueMember:="DisciplineID", FilterMethodName:="ROUserFindListPagedBO.AllowedDisciplines"),
     Singular.DataAnnotations.SetExpression("ROUserFindListPagedBO.DisciplineIDSet(self)")>
      Public Overridable Property DisciplineID() As Integer?
        Get
          Return ReadProperty(DisciplineIDProperty)
        End Get
        Set(ByVal Value As Integer?)
          LoadProperty(DisciplineIDProperty, Value)
        End Set
      End Property

      Public Shared KeyWordProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.KeyWord, "Keyword", "")
      ''' <summary>
      ''' Gets and sets the Production Venue value
      ''' </summary>
      <Display(Name:="KeyWord"),
       Singular.DataAnnotations.SetExpression("ROUserFindListPagedBO.KeywordSet(self)", , 250), Singular.DataAnnotations.TextField>
      Public Overridable Property KeyWord As String
        Get
          Return ReadProperty(KeyWordProperty)
        End Get
        Set(ByVal Value As String)
          LoadProperty(KeyWordProperty, Value)
        End Set
      End Property

      Public Sub New(UserID As Integer?, DisciplineID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?, KeyWord As String)
        Me.UserID = UserID
        Me.DisciplineID = DisciplineID
        Me.SystemID = SystemID
        Me.ProductionAreaID = ProductionAreaID
        Me.KeyWord = KeyWord
      End Sub

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewROUserFindListPagedList() As ROUserFindListPagedList

      Return New ROUserFindListPagedList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetROUserFindListPagedList() As ROUserFindListPagedList

      Return DataPortal.Fetch(Of ROUserFindListPagedList)(New Criteria())

    End Function

    Public Shared Function GetROUserFindList(UserID As Integer?, DisciplineID As Integer?, SystemID As Integer?, ProductionAreaID As Integer?, Keyword As String) As ROUserFindListPagedList

      Return DataPortal.Fetch(Of ROUserFindListPagedList)(New Criteria(UserID, DisciplineID, SystemID, ProductionAreaID, Keyword))

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      sdr.Read()
      mTotalRecords = sdr.GetInt32(0)
      sdr.NextResult()
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUserFindListPaged.GetROUserFindListPaged(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROUserFindListPaged"
            cm.Parameters.AddWithValue("@UserID", NothingDBNull(crit.UserID))
            cm.Parameters.AddWithValue("@DisciplineID", NothingDBNull(crit.DisciplineID))
            cm.Parameters.AddWithValue("@SystemID", NothingDBNull(crit.SystemID))
            cm.Parameters.AddWithValue("@ProductionAreaID", NothingDBNull(crit.ProductionAreaID))
            cm.Parameters.AddWithValue("@KeyWord", Singular.Strings.MakeEmptyDBNull(crit.KeyWord))
            crit.AddParameters(cm)
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace