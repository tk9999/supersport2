﻿' Generated 26 Feb 2015 07:15 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Security

  <Serializable()> _
  Public Class ROUserTypeList
    Inherits SingularReadOnlyListBase(Of ROUserTypeList, ROUserType)

#Region " Business Methods "

    Public Function GetItem(UserTypeID As Integer) As ROUserType

      For Each child As ROUserType In Me
        If child.UserTypeID = UserTypeID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "RO User Types"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROUserTypeList() As ROUserTypeList

      Return New ROUserTypeList()

    End Function

    Public Shared Sub BeginGetROUserTypeList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROUserTypeList)))

      Dim dp As New DataPortal(Of ROUserTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROUserTypeList(CallBack As EventHandler(Of DataPortalResult(Of ROUserTypeList)))

      Dim dp As New DataPortal(Of ROUserTypeList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROUserTypeList() As ROUserTypeList

      Return DataPortal.Fetch(Of ROUserTypeList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUserType.GetROUserType(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROUserTypeList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace