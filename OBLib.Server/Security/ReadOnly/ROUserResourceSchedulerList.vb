﻿' Generated 09 Mar 2016 15:22 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Security.ReadOnly

  <Serializable()> _
  Public Class ROUserResourceSchedulerList
    Inherits OBReadOnlyListBase(Of ROUserResourceSchedulerList, ROUserResourceScheduler)

#Region " Business Methods "

    Public Function GetItem(UserResourceSchedulerID As Integer) As ROUserResourceScheduler

      For Each child As ROUserResourceScheduler In Me
        If child.UserResourceSchedulerID = UserResourceSchedulerID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "S"

    End Function

#End Region

#Region " Data Access "

    <Serializable()> _
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

#Region " Common "

    Public Shared Function NewROUserResourceSchedulerList() As ROUserResourceSchedulerList

      Return New ROUserResourceSchedulerList()

    End Function

    Public Shared Sub BeginGetROUserResourceSchedulerList(criteria As Criteria, CallBack As EventHandler(Of DataPortalResult(Of ROUserResourceSchedulerList)))

      Dim dp As New DataPortal(Of ROUserResourceSchedulerList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(criteria)

    End Sub


    Public Shared Sub BeginGetROUserResourceSchedulerList(CallBack As EventHandler(Of DataPortalResult(Of ROUserResourceSchedulerList)))

      Dim dp As New DataPortal(Of ROUserResourceSchedulerList)()
      AddHandler dp.FetchCompleted, CallBack
      dp.BeginFetch(New Criteria())

    End Sub

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Public Shared Function GetROUserResourceSchedulerList() As ROUserResourceSchedulerList

      Return DataPortal.Fetch(Of ROUserResourceSchedulerList)(New Criteria())

    End Function

    Private Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      Me.IsReadOnly = False
      While sdr.Read
        Me.Add(ROUserResourceScheduler.GetROUserResourceScheduler(sdr))
      End While
      Me.IsReadOnly = True
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcsWeb.getROUserResourceSchedulerList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace