﻿Namespace Security

  Public Class Settings
    Inherits Singular.Settings

    Private Shared mUser As OBLib.Security.User
    Public Shared Shadows ReadOnly Property CurrentUser As OBLib.Security.User
      Get
        If Settings.CurrentUserID = 0 Then
          Return Nothing
        Else
          If mUser Is Nothing OrElse Not Singular.Misc.CompareSafe(mUser.UserID, Settings.CurrentUserID) Then
            mUser = OBLib.Security.UserList.GetUser(Settings.CurrentUserID)
          End If
          Return mUser
        End If
      End Get 
    End Property

    Public Shared ReadOnly Property OutgoingEmailAddress As String
      Get
        Return "sober@supersport.com"
      End Get
    End Property

  End Class

End Namespace
