﻿' Generated 15 Feb 2016 16:04 - Singular Systems Object Generator Version 2.1.676
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular.DataAnnotations
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.SystemManagement.ReadOnly

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If

Namespace Security

  <Serializable()> _
  Public Class UserSystemArea
    Inherits OBBusinessBase(Of UserSystemArea)

#Region " Properties and Methods "

#Region " Properties "

    Public Shared UserSystemAreaIDProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.UserSystemAreaID, "ID", 0)
    ''' <summary>
    ''' Gets the ID value
    ''' </summary>
    <Key>
    Public Property UserSystemAreaID() As Integer
      Get
        Return GetProperty(UserSystemAreaIDProperty)
      End Get
      Set(value As Integer)
        SetProperty(UserSystemAreaIDProperty, value)
      End Set
    End Property

    Public Shared UserSystemIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.UserSystemID, "User System", Nothing)
    ''' <summary>
    ''' Gets and sets the User System value
    ''' </summary>
    <Display(Name:="User System", Description:="Linked System of the User")>
    Public Property UserSystemID() As Integer?
      Get
        Return GetProperty(UserSystemIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(UserSystemIDProperty, Value)
      End Set
    End Property

    Public Shared ProductionAreaIDProperty As PropertyInfo(Of Integer?) = RegisterProperty(Of Integer?)(Function(c) c.ProductionAreaID, "Area", Nothing)
    ''' <summary>
    ''' Gets and sets the Production Area value
    ''' </summary>
    <Display(Name:="Area", Description:="Production Areas which the user has access to"),
    Required(ErrorMessage:="Area required"),
    DropDownWeb(GetType(ROSystemProductionAreaList), DisplayMember:="ProductionArea", ValueMember:="ProductionAreaID",
                FilterMethodName:="UserSystemAreaBO.AreaOptions")>
    Public Property ProductionAreaID() As Integer?
      Get
        Return GetProperty(ProductionAreaIDProperty)
      End Get
      Set(ByVal Value As Integer?)
        SetProperty(ProductionAreaIDProperty, Value)
      End Set
    End Property

    Public Shared CreatedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.CreatedBy, "Created By", 0)
    ''' <summary>
    ''' Gets the Created By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedBy() As Integer?
      Get
        Return GetProperty(CreatedByProperty)
      End Get
    End Property

    Public Shared CreatedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.CreatedDateTime, "Created Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Created Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property CreatedDateTime() As SmartDate
      Get
        Return GetProperty(CreatedDateTimeProperty)
      End Get
    End Property

    Public Shared ModifiedByProperty As PropertyInfo(Of Integer) = RegisterProperty(Of Integer)(Function(c) c.ModifiedBy, 0)
    ''' <summary>
    ''' Gets the Modified By value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedBy() As Integer?
      Get
        Return GetProperty(ModifiedByProperty)
      End Get
    End Property

    Public Shared ModifiedDateTimeProperty As PropertyInfo(Of SmartDate) = RegisterProperty(Of SmartDate)(Function(c) c.ModifiedDateTime, "Modified Date Time", New SmartDate(DateTime.Now()))
    ''' <summary>
    ''' Gets the Modified Date Time value
    ''' </summary>
    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property ModifiedDateTime() As SmartDate
      Get
        Return GetProperty(ModifiedDateTimeProperty)
      End Get
    End Property

    Public Shared ProductionAreaProperty As PropertyInfo(Of String) = RegisterProperty(Of String)(Function(c) c.ProductionArea, "Area", "")
    ''' <summary>
    ''' Gets and sets the System value
    ''' </summary>
    <Display(Name:="Area")>
    Public Property ProductionArea() As String
      Get
        Return GetProperty(ProductionAreaProperty)
      End Get
      Set(ByVal Value As String)
        SetProperty(ProductionAreaProperty, Value)
      End Set
    End Property

#End Region

#Region " Methods "

    Public Function GetParent() As UserSystem

      Return CType(CType(Me.Parent, UserSystemAreaList).Parent, UserSystem)

    End Function

    Protected Overrides Function GetIdValue() As Object

      Return GetProperty(UserSystemAreaIDProperty)

    End Function

    Public Overrides Function ToString() As String

      If Me.CreatedDateTime.ToString().Length = 0 Then
        If Me.IsNew Then
          Return String.Format("New {0}", "User System Area")
        Else
          Return String.Format("Blank {0}", "User System Area")
        End If
      Else
        Return Me.CreatedDateTime.ToString()
      End If

    End Function

#End Region

#End Region

#Region " Validation Rules "

    Protected Overrides Sub AddBusinessRules()

      MyBase.AddBusinessRules()

    End Sub

#End Region

#Region " Data Access & Factory Methods "

#Region " Common "

    Protected Overrides Sub OnCreate()

      'This is called when a new object is created
      'Set any variables here, not in the constructor or NewUserSystemArea() method.

    End Sub

    Public Shared Function NewUserSystemArea() As UserSystemArea

      Return DataPortal.CreateChild(Of UserSystemArea)()

    End Function

    Public Sub New()

      MarkAsChild()

    End Sub

#End Region

#Region " Silverlight "

#If SILVERLIGHT Then

#End Region

#Region " .NET Data Access "

#Else

#End Region

#Region " .Net Data Access "

    Friend Shared Function GetUserSystemArea(dr As SafeDataReader) As UserSystemArea

      Dim u As New UserSystemArea()
      u.Fetch(dr)
      Return u

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Using BypassPropertyChecks
        With sdr
          LoadProperty(UserSystemAreaIDProperty, .GetInt32(0))
          LoadProperty(UserSystemIDProperty, Singular.Misc.ZeroNothing(.GetInt32(1)))
          LoadProperty(ProductionAreaIDProperty, Singular.Misc.ZeroNothing(.GetInt32(2)))
          LoadProperty(CreatedByProperty, .GetInt32(3))
          LoadProperty(CreatedDateTimeProperty, .GetSmartDate(4))
          LoadProperty(ModifiedByProperty, .GetInt32(5))
          LoadProperty(ModifiedDateTimeProperty, .GetSmartDate(6))
          LoadProperty(ProductionAreaProperty, .GetString(7))
        End With
      End Using

      MarkAsChild()
      MarkOld()
      BusinessRules.CheckRules()

    End Sub

    Friend Sub Insert()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "InsProcsWeb.insUserSystemArea"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Friend Sub Update()

      ' if we're not dirty then don't update the database
      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "UpdProcsWeb.updUserSystemArea"

        DoInsertUpdateChild(cm)
      End Using

    End Sub

    Protected Overrides Sub InsertUpdate(cm As SqlCommand)

      If Me.IsSelfDirty Then

        With cm
          .CommandType = CommandType.StoredProcedure

          Dim paramUserSystemAreaID As SqlParameter = .Parameters.Add("@UserSystemAreaID", SqlDbType.Int)
          paramUserSystemAreaID.Value = GetProperty(UserSystemAreaIDProperty)
          If Me.IsNew Then
            paramUserSystemAreaID.Direction = ParameterDirection.Output
          End If
          'GetProperty(UserSystemIDProperty)
          .Parameters.AddWithValue("@UserSystemID", Me.GetParent.UserSystemID)
          .Parameters.AddWithValue("@ProductionAreaID", GetProperty(ProductionAreaIDProperty))
          .Parameters.AddWithValue("@ModifiedBy", OBLib.Security.Settings.CurrentUserID)

          .ExecuteNonQuery()

          If Me.IsNew Then
            LoadProperty(UserSystemAreaIDProperty, paramUserSystemAreaID.Value)
          End If
          ' update child objects
          ' mChildList.Update()
          MarkOld()
        End With
      Else
      End If

    End Sub

    Friend Sub DeleteSelf()

      ' if we're not dirty then don't update the database
      If Me.IsNew Then Exit Sub

      Using cm As SqlCommand = New SqlCommand
        cm.CommandText = "DelProcsWeb.delUserSystemArea"
        cm.CommandType = CommandType.StoredProcedure
        cm.Parameters.AddWithValue("@UserSystemAreaID", GetProperty(UserSystemAreaIDProperty))
        DoDeleteChild(cm)
      End Using

    End Sub

    Protected Overrides Sub DeleteFromDB(cm As SqlCommand)

      If Me.IsNew Then Exit Sub

      cm.ExecuteNonQuery()
      MarkNew()

    End Sub

#End If

#End Region

#End Region

  End Class

End Namespace
