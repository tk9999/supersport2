﻿Imports Csla.Serialization
Imports System.ComponentModel
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.Vehicles.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.General
Imports OBLib.Maintenance.Productions.Correspondence.ReadOnly

Public Class CommonData
  Inherits Singular.CommonData.CommonDataBase(Of OBCachedLists)

  <Serializable()>
  Public Class OBCachedLists
    Inherits Singular.CommonData.CommonDataBase(Of OBCachedLists).CachedLists

    Public ReadOnly Property ROSystemAreaAdHocBookingTypeList() As Maintenance.AdHoc.ReadOnly.ROSystemAreaAdHocBookingTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemAreaAdHocBookingTypeList, Function()
                                                                                                                   Return Maintenance.AdHoc.ReadOnly.ROSystemAreaAdHocBookingTypeList.GetROSystemAreaAdHocBookingTypeList(Nothing, Nothing)
                                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROTransactionTypeList() As Maintenance.Invoicing.ReadOnly.ROTransactionTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTransactionTypeList, Function()
                                                                                                        Return Maintenance.Invoicing.ReadOnly.ROTransactionTypeList.GetROTransactionTypeList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROVehicleTypeList() As Maintenance.Vehicles.ReadOnly.ROVehicleTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROVehicleTypeList, Function()
                                                                                                    Return Maintenance.Vehicles.ReadOnly.ROVehicleTypeList.GetROVehicleTypeList
                                                                                                  End Function)
      End Get
    End Property

    Public ReadOnly Property ROAccountList() As Maintenance.Invoicing.ReadOnly.ROAccountList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROAccountList, Function()
                                                                                                Return Maintenance.Invoicing.ReadOnly.ROAccountList.GetROAccountList
                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property RODebtorList() As Quoting.ReadOnly.RODebtorList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RODebtorList, Function()
                                                                                               Return Quoting.ReadOnly.RODebtorList.GetRODebtorListCommonData
                                                                                             End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionVenueImageList() As OBLib.PositionLayouts.ReadOnly.ROProductionVenueImageList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionVenueImageList, Function()
                                                                                                             Return OBLib.PositionLayouts.ReadOnly.ROProductionVenueImageList.GetROProductionVenueImageList
                                                                                                           End Function)
      End Get
    End Property

    Public ReadOnly Property ROCreditorList() As Maintenance.Creditors.ReadOnly.ROCreditorList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCreditorList, Function()
                                                                                                 Return Maintenance.Creditors.ReadOnly.ROCreditorList.GetROCreditorList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROCostCentreList() As Maintenance.Company.ReadOnly.ROCostCentreList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCostCentreList, Function()
                                                                                                   Return Maintenance.Company.ReadOnly.ROCostCentreList.GetROCostCentreList
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROAccessLogActionList() As AccessLogs.ReadOnly.ROAccessLogActionList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROAccessLogActionList, Function()
                                                                                                        Return AccessLogs.ReadOnly.ROAccessLogActionList.GetROAccessLogActionList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROCreditorInvoiceDetailTypeList() As Maintenance.Invoicing.ReadOnly.ROCreditorInvoiceDetailTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCreditorInvoiceDetailTypeList, Function()
                                                                                                                  Return Maintenance.Invoicing.ReadOnly.ROCreditorInvoiceDetailTypeList.GetROCreditorInvoiceDetailTypeList
                                                                                                                End Function)
      End Get
    End Property

    Public ReadOnly Property ROVatTypeList() As Maintenance.ReadOnly.ROVATTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROVatTypeList, Function()
                                                                                                Return Maintenance.ReadOnly.ROVATTypeList.GetROVATTypeList
                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROHumanResourceList() As HR.ReadOnly.ROHumanResourceList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROHumanResourceList, Function()
                                                                                                      Return HR.ReadOnly.ROHumanResourceList.GetROHumanResourceList(True)
                                                                                                    End Function)
      End Get
    End Property

    Public ReadOnly Property AllROHumanResourceList() As HR.ReadOnly.ROHumanResourceList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROHumanResourceList, Function()
                                                                                                      Return HR.ReadOnly.ROHumanResourceList.GetROHumanResourceList
                                                                                                    End Function)
      End Get
    End Property

    Public ReadOnly Property ROICRHumanResourceList() As HR.ReadOnly.ROHumanResourceList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROHumanResourceList, Function()
                                                                                                      Return HR.ReadOnly.ROHumanResourceList.GetROHumanResourceList(Nothing, Nothing, "", "", "", "", "", "",
                                                                                                                                                                    Nothing, Nothing, OBLib.Security.Settings.CurrentUser.SystemID,
                                                                                                                                                                    Nothing, Nothing, Nothing, Nothing)
                                                                                                    End Function)
      End Get
    End Property

    Public ReadOnly Property ROStudioHumanResourceList() As HR.ReadOnly.ROHumanResourceList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROHumanResourceList, Function()
                                                                                                      Return HR.ReadOnly.ROHumanResourceList.GetROHumanResourceList(Nothing, Nothing, "", "", "", "", "", "",
                                                                                                                                                                    Nothing, Nothing, Nothing,
                                                                                                                                                                    OBLib.CommonData.Enums.ProductionArea.Studio, Nothing, Nothing, Nothing)
                                                                                                    End Function)
      End Get
    End Property

    Public ReadOnly Property ROHumanResourceFullList() As HR.ReadOnly.ROHumanResourceFullList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROHumanResourceFullList, Function()
                                                                                                          Return HR.ReadOnly.ROHumanResourceFullList.GetROHumanResourceFullList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property MiscList() As Maintenance.MiscList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.MiscList, Function()
                                                                                           Return Maintenance.MiscList.GetMiscList
                                                                                         End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemList() As Maintenance.ReadOnly.ROSystemList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemList, Function()
                                                                                               Return Maintenance.ReadOnly.ROSystemList.GetROSystemList
                                                                                             End Function)
      End Get
    End Property

    Public ReadOnly Property CompanyList() As Maintenance.Company.CompanyList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.CompanyList, Function()
                                                                                              Return Maintenance.Company.CompanyList.GetCompanyList
                                                                                            End Function)
      End Get
    End Property

    Public ReadOnly Property ROPostProductionReportSectionList() As OBLib.PostProductionReports.ReadOnly.ROPostProductionReportSectionList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROPostProductionReportSectionList, Function()
                                                                                                                    Return PostProductionReports.ReadOnly.ROPostProductionReportSectionList.GetROPostProductionReportSectionList
                                                                                                                  End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionTypeList() As Maintenance.Productions.ReadOnly.Old.ROProductionTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionTypeList, Function()
                                                                                                       Return Maintenance.Productions.ReadOnly.Old.ROProductionTypeList.GetROProductionTypeList
                                                                                                     End Function)
      End Get
    End Property

    Public ReadOnly Property ROEventTypeList() As Maintenance.Productions.ReadOnly.Old.ROEventTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROEventTypeList, Function()
                                                                                                  Return Maintenance.Productions.ReadOnly.Old.ROEventTypeList.GetROEventTypeList
                                                                                                End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionVenueList() As Maintenance.Productions.ReadOnly.ROProductionVenueOldList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionVenueList, Function()
                                                                                                        Return Maintenance.Productions.ReadOnly.ROProductionVenueOldList.GetROProductionVenueOldList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionVenueFullList() As Maintenance.General.ReadOnly.ROProductionVenueFullList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionVenueFullList, Function()
                                                                                                            Return Maintenance.General.ReadOnly.ROProductionVenueFullList.GetROProductionVenueFullList
                                                                                                          End Function)
      End Get
    End Property

    Public ReadOnly Property ROChannelList() As Maintenance.ReadOnly.ROChannelList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROChannelList, Function()
                                                                                                Return Maintenance.ReadOnly.ROChannelList.GetROChannelList
                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionGradeList() As Maintenance.ReadOnly.ROProductionGradeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionGradeList, Function()
                                                                                                        Return Maintenance.ReadOnly.ROProductionGradeList.GetROProductionGradeList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROCountryList() As Maintenance.Locations.ReadOnly.ROCountryList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCountryList, Function()
                                                                                                Return Maintenance.Locations.ReadOnly.ROCountryList.GetROCountryList
                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROCityPagedList() As Maintenance.Locations.ReadOnly.ROCityPagedList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCityPagedList, Function()
                                                                                                  Return Maintenance.Locations.ReadOnly.ROCityPagedList.GetROCityListCommonData
                                                                                                End Function)
      End Get
    End Property

    Public ReadOnly Property ROProvinceList() As Maintenance.Locations.ReadOnly.ROProvinceList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProvinceList, Function()
                                                                                                 Return Maintenance.Locations.ReadOnly.ROProvinceList.GetROProvinceList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property RODisciplineProductionAreaCriteriaList() As OBLib.RODisciplineProductionAreaCriteriaList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RODisciplineProductionAreaCriteriaList, Function()
                                                                                                                         Return OBLib.RODisciplineProductionAreaCriteriaList.GetRODisciplineProductionAreaCriteriaList(Nothing, Nothing, OBLib.Security.Settings.CurrentUser.SystemID)
                                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROCityList() As Maintenance.Locations.ReadOnly.ROCityList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCityList, Function()
                                                                                             Return Maintenance.Locations.ReadOnly.ROCityList.GetROCityList
                                                                                           End Function)
      End Get
    End Property

    Public ReadOnly Property ROLocationList() As Maintenance.Locations.ReadOnly.ROLocationList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROLocationList, Function()
                                                                                                 Return Maintenance.Locations.ReadOnly.ROLocationList.GetROLocationList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionChannelStatusList() As Maintenance.ReadOnly.ROProductionChannelStatusList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionChannelStatusList, Function()
                                                                                                                Return Maintenance.ReadOnly.ROProductionChannelStatusList.GetROProductionChannelStatusList
                                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionSpecRequirementList() As Productions.Specs.ReadOnly.Old.ROProductionSpecRequirementListOld
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionSpecRequirementList, Function()
                                                                                                                  Return Productions.Specs.ReadOnly.Old.ROProductionSpecRequirementListOld.GetROProductionSpecRequirementListOld(True)
                                                                                                                End Function)
      End Get
    End Property

    Public ReadOnly Property ProductionSpecRequirementList() As Productions.Specs.Old.ProductionSpecRequirementList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ProductionSpecRequirementList, Function()
                                                                                                                Return Productions.Specs.Old.ProductionSpecRequirementList.GetProductionSpecRequirementList
                                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionCommentReportSectionList() As Productions.ReadOnly.ROProductionCommentReportSectionList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionCommentReportSectionList, Function()
                                                                                                                       Return Productions.ReadOnly.ROProductionCommentReportSectionList.GetROProductionCommentReportSectionList
                                                                                                                     End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionCommentHeaderList() As Productions.ReadOnly.ROProductionCommentHeaderList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionCommentHeaderList, Function()
                                                                                                                Return Productions.ReadOnly.ROProductionCommentHeaderList.GetROProductionCommentHeaderList
                                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionCommentList() As Productions.ReadOnly.ROProductionCommentList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionCommentList, Function()
                                                                                                          Return Productions.ReadOnly.ROProductionCommentList.GetROProductionCommentList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionList() As Productions.ReadOnly.ROProductionList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionList, Function()
                                                                                                   Return Productions.ReadOnly.ROProductionList.GetROProductionList(True, Nothing, Nothing)
                                                                                                 End Function)
      End Get
    End Property

    'Public ReadOnly Property ProductionList() As Productions.ProductionList
    '  Get
    '    Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ProductionList, Function()
    '                                                                                             Return Productions.ProductionList.GetProductionList(Nothing, Security.Settings.CurrentUser.SystemID, Security.Settings.CurrentUser.ProductionAreaID)
    '                                                                                           End Function)
    '  End Get
    'End Property

    Public ReadOnly Property ROAudioConfigurationList() As Productions.ReadOnly.ROAudioConfigurationList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROAudioConfigurationList, Function()
                                                                                                           Return Productions.ReadOnly.ROAudioConfigurationList.GetROAudioConfigurationList
                                                                                                         End Function)
      End Get
    End Property

    Public ReadOnly Property ROOutsourceServiceTypeList() As [ReadOnly].ROOutsourceServiceTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROOutsourceServiceTypeList, Function()
                                                                                                             Return [ReadOnly].ROOutsourceServiceTypeList.GetROOutsourceServiceTypeList
                                                                                                           End Function)
      End Get
    End Property

    Public ReadOnly Property ROSupplierList() As Maintenance.ReadOnly.ROSupplierList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSupplierList, Function()
                                                                                                 Return Maintenance.ReadOnly.ROSupplierList.GetROSupplierList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROPaymentRunStatusList() As Maintenance.Invoicing.ReadOnly.ROPaymentRunStatusList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROPaymentRunStatusList, Function()
                                                                                                         Return Maintenance.Invoicing.ReadOnly.ROPaymentRunStatusList.GetROPaymentRunStatusList
                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionTimelineTypeList As ROProductionTimelineTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionTimelineTypeList, Function()
                                                                                                               Return OBLib.Maintenance.Productions.ReadOnly.ROProductionTimelineTypeList.GetROProductionTimelineTypeList
                                                                                                             End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionTimelineTypeDisciplineList As ROProductionTimelineTypeDisciplineList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionTimelineTypeDisciplineList, Function()
                                                                                                                         Return OBLib.Maintenance.Productions.ReadOnly.ROProductionTimelineTypeDisciplineList.GetROProductionTimelineTypeDisciplineList
                                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROVehicleList As ROVehicleOldList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROVehicleList, Function()
                                                                                                Return OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleOldList.GetROVehicleOldList(Now, Now, Nothing)
                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROEquipmentList As OBLib.Equipment.ReadOnly.ROEquipmentList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROEquipmentList, Function()
                                                                                                  Return OBLib.Equipment.ReadOnly.ROEquipmentList.GetROEquipmentList
                                                                                                End Function)
      End Get
    End Property

    Public ReadOnly Property ROVehicleFullList As ROVehicleFullList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROVehicleFullList, Function()
                                                                                                    Return OBLib.Maintenance.Vehicles.ReadOnly.ROVehicleFullList.GetROVehicleFullList
                                                                                                  End Function)
      End Get
    End Property

    Public ReadOnly Property RODisciplineList As RODisciplineList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RODisciplineList, Function()
                                                                                                   Return OBLib.Maintenance.General.ReadOnly.RODisciplineList.GetRODisciplineList
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROPositionList() As Maintenance.General.ReadOnly.ROPositionList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROPositionList, Function()
                                                                                                 Return Maintenance.General.ReadOnly.ROPositionList.GetROPositionList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROPositionTypeList() As Maintenance.General.ReadOnly.ROPositionTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROPositionTypeList, Function()
                                                                                                     Return Maintenance.General.ReadOnly.ROPositionTypeList.GetROPositionTypeList
                                                                                                   End Function)
      End Get
    End Property

    Public ReadOnly Property NSWTimesheetCategoryList() As NSWTimesheets.NSWTimesheetCategoryList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.NSWTimesheetCategoryList, Function()
                                                                                                           Return NSWTimesheets.NSWTimesheetCategoryList.GetNSWTimesheetCategoryList
                                                                                                         End Function)
      End Get
    End Property

    Public ReadOnly Property PublicHolidayList() As OBLib.Maintenance.General.PublicHolidayList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.PublicHolidayList, Function()
                                                                                                    Return OBLib.Maintenance.General.PublicHolidayList.GetPublicHolidayList
                                                                                                  End Function)
      End Get
    End Property

    Public ReadOnly Property RONSWOpenMonthProductionList() As NSWTimesheets.ReadOnly.RONSWOpenMonthProductionList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RONSWOpenMonthProductionList, Function()
                                                                                                               Return NSWTimesheets.ReadOnly.RONSWOpenMonthProductionList.GetRONSWOpenMonthProductionList
                                                                                                             End Function)
      End Get
    End Property

    Public ReadOnly Property ROEquipmentTypeList() As OBLib.Maintenance.General.ReadOnly.ROEquipmentTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROEquipmentTypeList, Function()
                                                                                                      Return OBLib.Maintenance.General.ReadOnly.ROEquipmentTypeList.GetROEquipmentTypeList
                                                                                                    End Function)
      End Get
    End Property

    Public ReadOnly Property ROEquipmentSubTypeList() As OBLib.Maintenance.General.ReadOnly.ROEquipmentSubTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROEquipmentSubTypeList, Function()
                                                                                                         Return OBLib.Maintenance.General.ReadOnly.ROEquipmentSubTypeList.GetROEquipmentSubTypeList
                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROTimeSheetHRList() As Timesheets.ReadOnly.ROTimeSheetHRList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTimeSheetHRList, Function()
                                                                                                    Return Timesheets.ReadOnly.ROTimeSheetHRList.GetROTimeSheetHRList()
                                                                                                  End Function)
      End Get
    End Property

    Public ReadOnly Property RODocumentTypeList() As Maintenance.General.ReadOnly.RODocumentTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RODocumentTypeList, Function()
                                                                                                     Return Maintenance.General.ReadOnly.RODocumentTypeList.GetRODocumentTypeList
                                                                                                   End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionPettyCashTypeList() As ROProductionPettyCashTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionPettyCashTypeList, Function()
                                                                                                                Return OBLib.Maintenance.Productions.Correspondence.ReadOnly.ROProductionPettyCashTypeList.GetROProductionPettyCashTypeList
                                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionTypeBasherRequirementList() As OBLib.Maintenance.Productions.Specs.ReadOnly.ROProductionTypeBasherRequirementList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionTypeBasherRequirementList, Function()
                                                                                                                        Return OBLib.Maintenance.Productions.Specs.ReadOnly.ROProductionTypeBasherRequirementList.GetROProductionTypeBasherRequirementList
                                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionTypeCrewRequirementList() As OBLib.Maintenance.Productions.Specs.ReadOnly.ROProductionTypeCrewRequirementList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionTypeCrewRequirementList, Function()
                                                                                                                      Return OBLib.Maintenance.Productions.Specs.ReadOnly.ROProductionTypeCrewRequirementList.GetROProductionTypeCrewRequirementList
                                                                                                                    End Function)
      End Get
    End Property

    Public ReadOnly Property ROEventManagerList() As HR.ReadOnly.ROEventManagerList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROEventManagerList, Function()
                                                                                                     Return HR.ReadOnly.ROEventManagerList.GetROEventManagerList(Now)
                                                                                                   End Function)
      End Get
    End Property

    Public ReadOnly Property ROStudioSupervisorList() As HR.ReadOnly.ROStudioSupervisorList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROStudioSupervisorList, Function()
                                                                                                         Return HR.ReadOnly.ROStudioSupervisorList.GetROStudioSupervisorList
                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROManagerList() As HR.ReadOnly.ROManagerList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROManagerList, Function()
                                                                                                Return HR.ReadOnly.ROManagerList.GetROManagerList
                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROUnitSupervisorList() As HR.ReadOnly.ROUnitSupervisorList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROUnitSupervisorList, Function()
                                                                                                       Return HR.ReadOnly.ROUnitSupervisorList.GetROUnitSupervisorList(Now)
                                                                                                     End Function)
      End Get
    End Property

    Public ReadOnly Property ROMCRControllerList() As HR.ReadOnly.ROMCRControllerList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROMCRControllerList, Function()
                                                                                                      Return HR.ReadOnly.ROMCRControllerList.GetROMCRControllerList(Now)
                                                                                                    End Function)
      End Get
    End Property

    Public ReadOnly Property ROSCCROperatorList() As HR.ReadOnly.ROSCCROperatorList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSCCROperatorList, Function()
                                                                                                     Return HR.ReadOnly.ROSCCROperatorList.GetROSCCROperatorList(Now)
                                                                                                   End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionAreaList() As Maintenance.Productions.Areas.ReadOnly.ROProductionAreaList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionAreaList, Function()
                                                                                                       Return Maintenance.Productions.Areas.ReadOnly.ROProductionAreaList.GetROProductionAreaList
                                                                                                     End Function)
      End Get
    End Property

    Public ReadOnly Property ROCrewTypeList() As Maintenance.General.ReadOnly.ROCrewTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCrewTypeList, Function()
                                                                                                 Return Maintenance.General.ReadOnly.ROCrewTypeList.GetROCrewTypeList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROCrewTypeDisciplineList() As Maintenance.General.ReadOnly.ROCrewTypeDisciplineList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCrewTypeDisciplineList, Function()
                                                                                                           Return Maintenance.General.ReadOnly.ROCrewTypeDisciplineList.GetROCrewTypeDisciplineList
                                                                                                         End Function)
      End Get
    End Property

    Public ReadOnly Property ROAccommodationProviderList() As Maintenance.Travel.ReadOnly.ROAccommodationProviderList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROAccommodationProviderList, Function()
                                                                                                              Return Maintenance.Travel.ReadOnly.ROAccommodationProviderList.GetROAccommodationProviderList
                                                                                                            End Function)
      End Get
    End Property

    Public ReadOnly Property ROTravelDirectionList() As Maintenance.General.ReadOnly.ROTravelDirectionList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTravelDirectionList, Function()
                                                                                                        Return Maintenance.General.ReadOnly.ROTravelDirectionList.GetROTravelDirectionList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROFlightTypeList() As Maintenance.General.ReadOnly.ROFlightTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROFlightTypeList, Function()
                                                                                                   Return Maintenance.General.ReadOnly.ROFlightTypeList.GetROFlightTypeList
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROAirportList() As Maintenance.Travel.ReadOnly.ROAirportList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROAirportList, Function()
                                                                                                Return Maintenance.Travel.ReadOnly.ROAirportList.GetROAirportList
                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROFlightClassList() As Maintenance.Travel.ReadOnly.ROFlightClassList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROFlightClassList, Function()
                                                                                                    Return Maintenance.Travel.ReadOnly.ROFlightClassList.GetROFlightClassList
                                                                                                  End Function)
      End Get
    End Property

    Public ReadOnly Property RORentalCarAgentList() As Maintenance.Travel.ReadOnly.RORentalCarAgentList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RORentalCarAgentList, Function()
                                                                                                       Return Maintenance.Travel.ReadOnly.RORentalCarAgentList.GetRORentalCarAgentList
                                                                                                     End Function)
      End Get
    End Property

    Public ReadOnly Property ROCarTypeList() As Maintenance.Travel.ReadOnly.ROCarTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCarTypeList, Function()
                                                                                                Return Maintenance.Travel.ReadOnly.ROCarTypeList.GetROCarTypeList
                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property RORentalCarAgentBranchList() As Maintenance.Travel.ReadOnly.RORentalCarAgentBranchList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RORentalCarAgentBranchList, Function()
                                                                                                             Return Maintenance.Travel.ReadOnly.RORentalCarAgentBranchList.GetRORentalCarAgentBranchList
                                                                                                           End Function)
      End Get
    End Property

    Public ReadOnly Property ROGroupSnTList() As Travel.SnTs.ReadOnly.ROGroupSnTList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROGroupSnTList, Function()
                                                                                                 Return Travel.SnTs.ReadOnly.ROGroupSnTList.GetROGroupSnTList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROCurrencyList() As Maintenance.General.ReadOnly.ROCurrencyList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCurrencyList, Function()
                                                                                                 Return Maintenance.General.ReadOnly.ROCurrencyList.GetROCurrencyList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROTravelAdvanceTypeList() As Maintenance.Travel.ReadOnly.ROTravelAdvanceTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTravelAdvanceTypeList, Function()
                                                                                                          Return Maintenance.Travel.ReadOnly.ROTravelAdvanceTypeList.GetROTravelAdvanceTypeList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property ROPayTypeList() As Maintenance.General.ReadOnly.ROPayTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROPayTypeList, Function()
                                                                                                Return Maintenance.General.ReadOnly.ROPayTypeList.GetROPayTypeList
                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROCommentTypeList() As Maintenance.Travel.ReadOnly.ROCommentTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCommentTypeList, Function()
                                                                                                    Return Maintenance.Travel.ReadOnly.ROCommentTypeList.GetROCommentTypeList
                                                                                                  End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionAreaStatusList() As Maintenance.Productions.Areas.ReadOnly.ROProductionAreaStatusList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionAreaStatusList, Function()
                                                                                                             Return Maintenance.Productions.Areas.ReadOnly.ROProductionAreaStatusList.GetROProductionAreaStatusList
                                                                                                           End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionAreaAllowedStatusList() As Maintenance.Productions.Areas.ReadOnly.ROProductionAreaAllowedStatusList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionAreaAllowedStatusList, Function()
                                                                                                                    Return Maintenance.Productions.Areas.ReadOnly.ROProductionAreaAllowedStatusList.GetROProductionAreaAllowedStatusList
                                                                                                                  End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionAreaAllowedTimelineTypeList() As Maintenance.Productions.Areas.ReadOnly.ROProductionAreaAllowedTimelineTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionAreaAllowedTimelineTypeList, Function()
                                                                                                                          Return Maintenance.Productions.Areas.ReadOnly.ROProductionAreaAllowedTimelineTypeList.GetROProductionAreaAllowedTimelineTypeList
                                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property ROProductionAreaAllowedDisciplineList() As Maintenance.Productions.Areas.ReadOnly.ROProductionAreaAllowedDisciplineList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROProductionAreaAllowedDisciplineList, Function()
                                                                                                                        Return Maintenance.Productions.Areas.ReadOnly.ROProductionAreaAllowedDisciplineList.GetROProductionAreaAllowedDisciplineList
                                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROContractTypeList() As Maintenance.HR.ReadOnly.ROContractTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROContractTypeList, Function()
                                                                                                     Return Maintenance.HR.ReadOnly.ROContractTypeList.GetROContractTypeList
                                                                                                   End Function)
      End Get
    End Property

    Public ReadOnly Property RORaceList() As Maintenance.General.ReadOnly.RORaceList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RORaceList, Function()
                                                                                             Return Maintenance.General.ReadOnly.RORaceList.GetRORaceList
                                                                                           End Function)
      End Get
    End Property

    Public ReadOnly Property ROGenderList() As Maintenance.General.ReadOnly.ROGenderList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROGenderList, Function()
                                                                                               Return Maintenance.General.ReadOnly.ROGenderList.GetROGenderList
                                                                                             End Function)
      End Get
    End Property

    Public ReadOnly Property ROBankList() As Maintenance.General.ReadOnly.ROBankList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROBankList, Function()
                                                                                             Return Maintenance.General.ReadOnly.ROBankList.GetROBankList
                                                                                           End Function)
      End Get
    End Property

    Public ReadOnly Property ROBankBranchList() As Maintenance.General.ReadOnly.ROBankBranchList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROBankBranchList, Function()
                                                                                                   Return Maintenance.General.ReadOnly.ROBankBranchList.GetROBankBranchList
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROBankAccountTypeList() As Maintenance.General.ReadOnly.ROBankAccountTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROBankAccountTypeList, Function()
                                                                                                        Return Maintenance.General.ReadOnly.ROBankAccountTypeList.GetROBankAccountTypeList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROOffReasonList() As Maintenance.HR.ReadOnly.ROOffReasonList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROOffReasonList, Function()
                                                                                                  Return Maintenance.HR.ReadOnly.ROOffReasonList.GetROOffReasonList
                                                                                                End Function)
      End Get
    End Property

    Public ReadOnly Property ROSkillLevelList() As Maintenance.General.ReadOnly.ROSkillLevelList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSkillLevelList, Function()
                                                                                                   Return Maintenance.General.ReadOnly.ROSkillLevelList.GetROSkillLevelList
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROHumanResourceSkillList() As HR.ReadOnly.ROHumanResourceSkillList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROHumanResourceSkillList, Function()
                                                                                                           Return HR.ReadOnly.ROHumanResourceSkillList.GetROHumanResourceSkillList
                                                                                                         End Function)
      End Get
    End Property

    Public ReadOnly Property RORoomScheduleTypeList() As Maintenance.Rooms.ReadOnly.RORoomScheduleTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RORoomScheduleTypeList, Function()
                                                                                                         Return Maintenance.Rooms.ReadOnly.RORoomScheduleTypeList.GetRORoomScheduleTypeList
                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemProductionAreaList() As OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemProductionAreaList, Function()
                                                                                                             Return OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaList.GetROSystemProductionAreaList
                                                                                                           End Function)
      End Get
    End Property

    Public ReadOnly Property ROShiftDurationList() As Maintenance.General.ReadOnly.ROShiftDurationList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROShiftDurationList, Function()
                                                                                                      Return Maintenance.General.ReadOnly.ROShiftDurationList.GetROShiftDurationList
                                                                                                    End Function)
      End Get
    End Property

    Public ReadOnly Property ROTeamNumberList() As Maintenance.General.ReadOnly.ROTeamNumberList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTeamNumberList, Function()
                                                                                                   Return Maintenance.General.ReadOnly.ROTeamNumberList.GetROTeamNumberList
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROShiftPatternWeekDayList() As Maintenance.General.ReadOnly.ROShiftPatternWeekDayList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROShiftPatternWeekDayList, Function()
                                                                                                            Return Maintenance.General.ReadOnly.ROShiftPatternWeekDayList.GetROShiftPatternWeekDayList
                                                                                                          End Function)
      End Get
    End Property

    Public ReadOnly Property ROShiftPatternList() As Maintenance.General.ReadOnly.ROShiftPatternList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROShiftPatternList, Function()
                                                                                                     Return Maintenance.General.ReadOnly.ROShiftPatternList.GetROShiftPatternList
                                                                                                   End Function)
      End Get
    End Property

    Public ReadOnly Property RORoomList() As Maintenance.Rooms.ReadOnly.RORoomList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RORoomList, Function()
                                                                                             Return Maintenance.Rooms.ReadOnly.RORoomList.GetRORoomList
                                                                                           End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemAreaShiftPatternList() As Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemAreaShiftPatternList, Function()
                                                                                                               Return Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftPatternList.GetROSystemAreaShiftPatternList
                                                                                                             End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemAreaShiftTypeList() As Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemAreaShiftTypeList, Function()
                                                                                                            Return Maintenance.ShiftPatterns.ReadOnly.ROSystemAreaShiftTypeList.GetROSystemAreaShiftTypeList
                                                                                                          End Function)
      End Get
    End Property

    Public ReadOnly Property ROUserList() As Maintenance.General.ReadOnly.ROUserList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROUserList, Function()
                                                                                             Return Maintenance.General.ReadOnly.ROUserList.GetROUserList(1, 2000, "FirstName", True)
                                                                                           End Function)
      End Get
    End Property

    Public ReadOnly Property UserList() As OBLib.Security.UserList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.UserList, Function()
                                                                                           Return OBLib.Security.UserList.GetUserList()
                                                                                         End Function)
      End Get
    End Property

    Public ReadOnly Property ROClashTypeList() As Maintenance.Company.ReadOnly.ROClashTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROClashTypeList, Function()
                                                                                                  Return Maintenance.Company.ReadOnly.ROClashTypeList.GetROClashTypeList
                                                                                                End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemEmailTypeList() As Maintenance.Company.ReadOnly.ROSystemEmailTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemEmailTypeList, Function()
                                                                                                        Return Maintenance.Company.ReadOnly.ROSystemEmailTypeList.GetROSystemEmailTypeList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemAllowedAreaList() As Maintenance.Company.ReadOnly.ROSystemAllowedAreaList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemAllowedAreaList, Function()
                                                                                                          Return Maintenance.Company.ReadOnly.ROSystemAllowedAreaList.GetROSystemAllowedAreaList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemEmailList() As Maintenance.Company.ReadOnly.ROSystemEmailList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemEmailList, Function()
                                                                                                    Return Maintenance.Company.ReadOnly.ROSystemEmailList.GetROSystemEmailList
                                                                                                  End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemEmailRecipientAllList() As Maintenance.Company.ReadOnly.ROSystemEmailRecipientsAllList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemEmailRecipientAllList, Function()
                                                                                                                Return Maintenance.Company.ReadOnly.ROSystemEmailRecipientsAllList.GetROSystemEmailRecipientsAllList
                                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROAdHocBookingTypeList() As Maintenance.AdHoc.ReadOnly.ROAdHocBookingTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROAdHocBookingTypeList, Function()
                                                                                                         Return Maintenance.AdHoc.ReadOnly.ROAdHocBookingTypeList.GetROAdHocBookingTypeList
                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROICRMonthList() As Maintenance.ICR.ReadOnly.ROICRMonthList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROICRMonthList, Function()
                                                                                                 Return Maintenance.ICR.ReadOnly.ROICRMonthList.GetROICRMonthList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROUserSystemList() As OBLib.Maintenance.General.ReadOnly.ROUserSystemList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROUserSystemList, Function()
                                                                                                   Return OBLib.Maintenance.General.ReadOnly.ROUserSystemList.GetROUserSystemList(OBLib.Security.Settings.CurrentUser.UserID)
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemTeamList() As OBLib.TeamManagement.ReadOnly.ROSystemTeamList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemTeamList, Function()
                                                                                                   Return OBLib.TeamManagement.ReadOnly.ROSystemTeamList.GetROSystemTeamList
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROShiftTypeList() As Maintenance.ShiftPatterns.ROShiftTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROShiftTypeList, Function()
                                                                                                  Return Maintenance.ShiftPatterns.ROShiftTypeList.GetROShiftTypeList
                                                                                                End Function)
      End Get
    End Property

    Public ReadOnly Property RORoomTypeList() As Maintenance.Rooms.ReadOnly.RORoomTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RORoomTypeList, Function()
                                                                                                 Return Maintenance.Rooms.ReadOnly.RORoomTypeList.GetRORoomTypeList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROTimesheetMonthListOld() As Maintenance.General.ReadOnly.ROTimesheetMonthListOld
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTimesheetMonthListOld, Function()
                                                                                                          Return Maintenance.General.ReadOnly.ROTimesheetMonthListOld.GetROTimesheetMonthList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property ROTimesheetMonthList() As Maintenance.Timesheets.ReadOnly.ROTimesheetMonthList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTimesheetMonthList, Function()
                                                                                                       Return Maintenance.Timesheets.ReadOnly.ROTimesheetMonthList.GetROTimesheetMonthListAll
                                                                                                     End Function)
      End Get
    End Property

    Public ReadOnly Property ROCompanyMonthList() As Maintenance.Company.ReadOnly.ROCompanyMonthList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCompanyMonthList, Function()
                                                                                                     Return Maintenance.Company.ReadOnly.ROCompanyMonthList.GetROCompanyMonthList
                                                                                                   End Function)
      End Get
    End Property

    Public ReadOnly Property RORoomTimelineSettingList() As Maintenance.Rooms.ReadOnly.RORoomTimelineSettingList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RORoomTimelineSettingList, Function()
                                                                                                            Return Maintenance.Rooms.ReadOnly.RORoomTimelineSettingList.GetRORoomTimelineSettingList
                                                                                                          End Function)
      End Get
    End Property

    Public ReadOnly Property ROCostTypeList() As OBLib.Quoting.ReadOnly.ROCostTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCostTypeList, Function()
                                                                                                 Return OBLib.Quoting.ReadOnly.ROCostTypeList.GetROCostTypeList()
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property RORateTypeList() As OBLib.Quoting.ReadOnly.RORateTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RORateTypeList, Function()
                                                                                                 Return OBLib.Quoting.ReadOnly.RORateTypeList.GetRORateTypeList()
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROTravelRateList() As OBLib.Quoting.ReadOnly.ROTravelRateList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTravelRateList, Function()
                                                                                                   Return OBLib.Quoting.ReadOnly.ROTravelRateList.GetROTravelRateList()
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROCompanyRateCardList() As OBLib.Quoting.ReadOnly.ROCompanyRateCardList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCompanyRateCardList, Function()
                                                                                                        Return OBLib.Quoting.ReadOnly.ROCompanyRateCardList.GetROCompanyRateCardList()
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROSupplierRateCardList() As OBLib.Quoting.ReadOnly.ROSupplierRateCardList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSupplierRateCardList, Function()
                                                                                                         Return OBLib.Quoting.ReadOnly.ROSupplierRateCardList.GetROSupplierRateCardList()
                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROTravelTypeList() As OBLib.Maintenance.Travel.ReadOnly.ROTravelTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTravelTypeList, Function()
                                                                                                   Return OBLib.Maintenance.Travel.ReadOnly.ROTravelTypeList.GetROTravelTypeList()
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROCompanyList() As OBLib.Maintenance.Company.ReadOnly.ROCompanyList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCompanyList, Function()
                                                                                                Return OBLib.Maintenance.Company.ReadOnly.ROCompanyList.GetROCompanyList()
                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROAccessTerminalList() As OBLib.AccessLogs.ReadOnly.ROAccessTerminalList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROAccessTerminalList, Function()
                                                                                                       Return OBLib.AccessLogs.ReadOnly.ROAccessTerminalList.GetROAccessTerminalList()
                                                                                                     End Function)
      End Get
    End Property

    Public ReadOnly Property ROAccessFlagList() As OBLib.Biometrics.ROAccessFlagList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROAccessFlagList, Function()
                                                                                                   Return OBLib.Biometrics.ROAccessFlagList.GetROAccessFlagList
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROMRMonthList() As OBLib.NSWTimesheets.ReadOnly.ROMRMonthList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROMRMonthList, Function()
                                                                                                Return OBLib.NSWTimesheets.ReadOnly.ROMRMonthList.GetROMRMonthList()
                                                                                              End Function)
      End Get
    End Property

    Public ReadOnly Property ROMRMonthLastElevenList() As OBLib.NSWTimesheets.ReadOnly.ROMRMonthList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROMRMonthLastElevenList, Function()
                                                                                                          Return OBLib.NSWTimesheets.ReadOnly.ROMRMonthList.GetROMRMonthList(11)
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property ROMRMonthCurrentPrevOpenList() As OBLib.NSWTimesheets.ReadOnly.ROMRMonthList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROMRMonthCurrentPrevOpenList, Function()
                                                                                                               Return OBLib.NSWTimesheets.ReadOnly.ROMRMonthList.GetROMRMonthList(True)
                                                                                                             End Function)
      End Get
    End Property

    Public ReadOnly Property ROHRDocumentTypeList() As OBLib.HR.ReadOnly.ROHRDocumentTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROHRDocumentTypeList, Function()
                                                                                                       Return OBLib.HR.ReadOnly.ROHRDocumentTypeList.GetROHRDocumentTypeList()
                                                                                                     End Function)
      End Get
    End Property

    Public ReadOnly Property ROCommentCategoryList() As OBLib.Maintenance.Travel.ReadOnly.ROCommentCategoryList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCommentCategoryList, Function()
                                                                                                        Return OBLib.Maintenance.Travel.ReadOnly.ROCommentCategoryList.GetROCommentCategoryList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROPublicHolidayList() As OBLib.Maintenance.General.ReadOnly.ROPublicHolidayList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROPublicHolidayList, Function()
                                                                                                      Return OBLib.Maintenance.General.ReadOnly.ROPublicHolidayList.GetROPublicHolidayList
                                                                                                    End Function)
      End Get
    End Property

    Public ReadOnly Property ROFinanceDocumentTypeList() As Maintenance.General.ReadOnly.ROFinanceDocumentTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROFinanceDocumentTypeList, Function()
                                                                                                            Return Maintenance.General.ReadOnly.ROFinanceDocumentTypeList.GetROFinanceDocumentTypeList
                                                                                                          End Function)
      End Get
    End Property

    Public ReadOnly Property ROISPCancellationRuleList() As Maintenance.General.ReadOnly.ROISPCancellationRuleList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROISPCancellationRuleList, Function()
                                                                                                            Return Maintenance.General.ReadOnly.ROISPCancellationRuleList.GetROISPCancellationRuleList
                                                                                                          End Function)
      End Get
    End Property

    Public ReadOnly Property ROPlanningLevelHourList() As Maintenance.Productions.ReadOnly.ROPlanningLevelHourList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROPlanningLevelHourList, Function()
                                                                                                          Return Maintenance.Productions.ReadOnly.ROPlanningLevelHourList.GetROPlanningLevelHourList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property ROPlanningLevelTypeList() As Maintenance.Productions.ReadOnly.ROPlanningLevelTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROPlanningLevelTypeList, Function()
                                                                                                          Return Maintenance.Productions.ReadOnly.ROPlanningLevelTypeList.GetROPlanningLevelTypeList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property ROTimesheetCategoryList() As Maintenance.Timesheets.ReadOnly.ROTimesheetCategoryList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTimesheetCategoryList, Function()
                                                                                                          Return Maintenance.Timesheets.ReadOnly.ROTimesheetCategoryList.GetROTimesheetCategoryList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property ROTimesheetHourTypeList() As Maintenance.Timesheets.ReadOnly.ROTimesheetHourTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTimesheetHourTypeList, Function()
                                                                                                          Return Maintenance.Timesheets.ReadOnly.ROTimesheetHourTypeList.GetROTimesheetHourTypeList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property ROGroupSnTTypeList() As Maintenance.Travel.ReadOnly.ROGroupSnTTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROGroupSnTTypeList, Function()
                                                                                                     Return Maintenance.Travel.ReadOnly.ROGroupSnTTypeList.GetROGroupSnTTypeList
                                                                                                   End Function)
      End Get
    End Property

    Public ReadOnly Property ROPositionLevelList() As Maintenance.HR.ReadOnly.ROPositionLevelList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROPositionLevelList, Function()
                                                                                                      Return Maintenance.HR.ReadOnly.ROPositionLevelList.GetROPositionLevelList
                                                                                                    End Function)
      End Get
    End Property

    Public ReadOnly Property ROFacilityTypeList() As Maintenance.General.ReadOnly.ROFacilityTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROFacilityTypeList, Function()
                                                                                                     Return Maintenance.General.ReadOnly.ROFacilityTypeList.GetROFacilityTypeList
                                                                                                   End Function)
      End Get
    End Property

    Public ReadOnly Property ROFacilityList() As Maintenance.General.ReadOnly.ROFacilityList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROFacilityList, Function()
                                                                                                 Return Maintenance.General.ReadOnly.ROFacilityList.GetROFacilityList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROICRContactDetailList() As Maintenance.General.ReadOnly.ROICRContactDetailList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROICRContactDetailList, Function()
                                                                                                         Return Maintenance.General.ReadOnly.ROICRContactDetailList.GetROICRContactDetailList
                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROPaymentRunList() As Invoicing.ReadOnly.ROPaymentRunList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROPaymentRunList, Function()
                                                                                                   Return Invoicing.ReadOnly.ROPaymentRunList.GetROPaymentRunList
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROCalculationChangedDateList() As Maintenance.General.ReadOnly.ROCalculationChangedDateList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCalculationChangedDateList, Function()
                                                                                                               Return Maintenance.General.ReadOnly.ROCalculationChangedDateList.GetROCalculationChangedDateList
                                                                                                             End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemParameterList() As Maintenance.General.ReadOnly.ROSystemParameterList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemParameterList, Function()
                                                                                                        Return Maintenance.General.ReadOnly.ROSystemParameterList.GetROSystemParameterList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROUserTypeList() As Security.ROUserTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROUserTypeList, Function()
                                                                                                 Return Security.ROUserTypeList.GetROUserTypeList()
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROAuditTrailTableList() As AuditTrails.ReadOnly.ROAuditTrailTableList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROAuditTrailTableList, Function()
                                                                                                        Return AuditTrails.ReadOnly.ROAuditTrailTableList.GetROAuditTrailTableList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROAudioSettingList() As Maintenance.SatOps.ReadOnly.ROAudioSettingList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROAudioSettingList, Function()
                                                                                                     Return Maintenance.SatOps.ReadOnly.ROAudioSettingList.GetROAudioSettingList(True)
                                                                                                   End Function)
      End Get
    End Property

    Public ReadOnly Property ROVideoSettingList() As Maintenance.SatOps.ReadOnly.ROVideoSettingList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROVideoSettingList, Function()
                                                                                                     Return Maintenance.SatOps.ReadOnly.ROVideoSettingList.GetROVideoSettingList
                                                                                                   End Function)
      End Get
    End Property

    Public ReadOnly Property ROFeedTypeList() As Maintenance.SatOps.ReadOnly.ROFeedTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROFeedTypeList, Function()
                                                                                                 Return Maintenance.SatOps.ReadOnly.ROFeedTypeList.GetROFeedTypeList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property ROTurnAroundPointList() As Maintenance.SatOps.ReadOnly.ROTurnAroundPointList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTurnAroundPointList, Function()
                                                                                                        Return Maintenance.SatOps.ReadOnly.ROTurnAroundPointList.GetROTurnAroundPointList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROEquipmentTypeAllowedScheduleTypeList() As Maintenance.SatOps.ReadOnly.ROEquipmentTypeAllowedScheduleTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROEquipmentTypeAllowedScheduleTypeList, Function()
                                                                                                                         Return Maintenance.SatOps.ReadOnly.ROEquipmentTypeAllowedScheduleTypeList.GetROEquipmentTypeAllowedScheduleTypeList
                                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROEquipmentScheduleTypeList() As Maintenance.SatOps.ReadOnly.ROEquipmentScheduleTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROEquipmentScheduleTypeList, Function()
                                                                                                              Return Maintenance.SatOps.ReadOnly.ROEquipmentScheduleTypeList.GetROEquipmentScheduleTypeList
                                                                                                            End Function)
      End Get
    End Property

    Public ReadOnly Property AccessZoneList() As Biometrics.Maintenance.AccessZoneList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.AccessZoneList, Function()
                                                                                                 Return Biometrics.Maintenance.AccessZoneList.GetAccessZoneList
                                                                                               End Function)
      End Get
    End Property

    Public ReadOnly Property AccessTerminalGroupList() As Biometrics.Maintenance.AccessTerminalGroupList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.AccessTerminalGroupList, Function()
                                                                                                          Return Biometrics.Maintenance.AccessTerminalGroupList.GetAccessTerminalGroupList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property AccessTerminalTypeList() As Biometrics.Maintenance.AccessTerminalTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.AccessTerminalTypeList, Function()
                                                                                                         Return Biometrics.Maintenance.AccessTerminalTypeList.GetAccessTerminalTypeList
                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property AccessLogActionList() As Biometrics.Maintenance.AccessLogActionList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.AccessLogActionList, Function()
                                                                                                      Return Biometrics.Maintenance.AccessLogActionList.GetAccessLogActionList
                                                                                                    End Function)
      End Get
    End Property

    Public ReadOnly Property AccessLogSourceList() As Biometrics.Maintenance.AccessLogSourceList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.AccessLogSourceList, Function()
                                                                                                      Return Biometrics.Maintenance.AccessLogSourceList.GetAccessLogSourceList
                                                                                                    End Function)
      End Get
    End Property

    Public ReadOnly Property ROIngestSourceTypeList() As OBLib.SatOps.ReadOnly.ROIngestSourceTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROIngestSourceTypeList, Function()
                                                                                                         Return OBLib.SatOps.ReadOnly.ROIngestSourceTypeList.GetROIngestSourceTypeList
                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROSmsTemplateTypeList() As OBLib.Notifications.SmS.ReadOnly.ROSmsTemplateTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSmsTemplateTypeList, Function()
                                                                                                        Return OBLib.Notifications.SmS.ReadOnly.ROSmsTemplateTypeList.GetROSmsTemplateTypeList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROSmsBatchTemplateList() As OBLib.Notifications.SmS.ReadOnly.ROSmsBatchTemplateList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSmsBatchTemplateList, Function()
                                                                                                         Return OBLib.Notifications.SmS.ReadOnly.ROSmsBatchTemplateList.GetROSmsBatchTemplateList
                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROEmailBatchTemplateList() As OBLib.Notifications.Email.ReadOnly.ROEmailBatchTemplateList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROEmailBatchTemplateList, Function()
                                                                                                           Return OBLib.Notifications.Email.ReadOnly.ROEmailBatchTemplateList.GetROEmailBatchTemplateList
                                                                                                         End Function)
      End Get
    End Property

    Public ReadOnly Property ROImageTypeList() As OBLib.Maintenance.Images.ReadOnly.ROImageTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROImageTypeList, Function()
                                                                                                  Return OBLib.Maintenance.Images.ReadOnly.ROImageTypeList.GetROImageTypeList
                                                                                                End Function)
      End Get
    End Property

    Public ReadOnly Property ROEmailTemplateTypeList() As OBLib.Notifications.Email.ReadOnly.ROEmailTemplateTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROEmailTemplateTypeList, Function()
                                                                                                          Return OBLib.Notifications.Email.ReadOnly.ROEmailTemplateTypeList.GetROEmailTemplateTypeList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property ROFeedbackReportDropDownItemList() As OBLib.FeedbackReports.ROFeedbackReportDropDownItemList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROFeedbackReportDropDownItemList, Function()
                                                                                                                   Return OBLib.FeedbackReports.ROFeedbackReportDropDownItemList.GetROFeedbackReportDropDownItemList
                                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROFeedbackReportSettingList() As OBLib.FeedbackReports.ROFeedbackReportSettingList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROFeedbackReportSettingList, Function()
                                                                                                              Return OBLib.FeedbackReports.ROFeedbackReportSettingList.GetROFeedbackReportSettingList
                                                                                                            End Function)
      End Get
    End Property

    Public ReadOnly Property ROFeedbackReportDropDownList() As OBLib.FeedbackReports.ROFeedbackReportDropDownList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROFeedbackReportDropDownList, Function()
                                                                                                               Return OBLib.FeedbackReports.ROFeedbackReportDropDownList.GetROFeedbackReportDropDownList
                                                                                                             End Function)
      End Get
    End Property

    Public ReadOnly Property ROEmailGroupList() As OBLib.FeedbackReports.ROEmailGroupList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROEmailGroupList, Function()
                                                                                                   Return OBLib.FeedbackReports.ROEmailGroupList.GetROEmailGroupList
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROFeedbackReportFlagTypeList() As OBLib.FeedbackReports.ROFeedbackReportFlagTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROFeedbackReportFlagTypeList, Function()
                                                                                                               Return OBLib.FeedbackReports.ROFeedbackReportFlagTypeList.GetROFeedbackReportFlagTypeList
                                                                                                             End Function)
      End Get
    End Property

    Public ReadOnly Property ROFeebackReportCompleteByTypeList() As OBLib.FeedbackReports.ROFeebackReportCompleteByTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROFeebackReportCompleteByTypeList, Function()
                                                                                                                    Return OBLib.FeedbackReports.ROFeebackReportCompleteByTypeList.GetROFeebackReportCompleteByTypeList
                                                                                                                  End Function)
      End Get
    End Property

    Public ReadOnly Property ROResourceTypeList() As Maintenance.Resources.ReadOnly.ROResourceTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROResourceTypeList, Function()
                                                                                                     Return Maintenance.Resources.ReadOnly.ROResourceTypeList.GetROResourceTypeList
                                                                                                   End Function)
      End Get
    End Property

    Public ReadOnly Property ROSkillProductionTypeList() As HR.ReadOnly.ROSkillProductionTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSkillProductionTypeList, Function()
                                                                                                            Return HR.ReadOnly.ROSkillProductionTypeList.GetROSkillProductionTypeList
                                                                                                          End Function)
      End Get
    End Property

    Public ReadOnly Property ROSatOpsEquipmentList() As Equipment.ReadOnly.ROSatOpsEquipmentList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSatOpsEquipmentList, Function()
                                                                                                        Return Equipment.ReadOnly.ROSatOpsEquipmentList.GetROSatOpsEquipmentList
                                                                                                      End Function)
      End Get
    End Property

    Public ReadOnly Property ROMonthList() As Maintenance.ReadOnly.ROMonthList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROMonthList, Function()
                                                                                              Return OBLib.Maintenance.ReadOnly.ROMonthList.GetROMonthList
                                                                                            End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemTeamNumberList As TeamManagement.ReadOnly.ROSystemTeamNumberList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemTeamNumberList, Function()
                                                                                                         Return OBLib.TeamManagement.ReadOnly.ROSystemTeamNumberList.GetROSystemTeamNumberList
                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROSiteList As Maintenance.General.ReadOnly.ROSiteList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSiteList, Function()
                                                                                             Return Maintenance.General.ReadOnly.ROSiteList.GetROSiteList
                                                                                           End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemProductionAreaDisciplinePositionTypeList As Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplinePositionTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemProductionAreaDisciplinePositionTypeList, Function()
                                                                                                                                   Return Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplinePositionTypeList.GetROSystemProductionAreaDisciplinePositionTypeList
                                                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemProductionAreaDisciplinePositionTypeListFlat As Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplinePositionTypeListFlat
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemProductionAreaDisciplinePositionTypeListFlat, Function()
                                                                                                                                       Return Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplinePositionTypeListFlat.GetROSystemProductionAreaDisciplinePositionTypeListFlat
                                                                                                                                     End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemProductionAreaDisciplineList As Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplineList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemProductionAreaDisciplineList, Function()
                                                                                                                       Return Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplineList.GetROSystemProductionAreaDisciplineList
                                                                                                                     End Function)
      End Get
    End Property

    Public ReadOnly Property SystemProductionAreaDisciplineSelectList As Maintenance.SystemManagement.SystemProductionAreaDisciplineSelectList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.SystemProductionAreaDisciplineSelectList, Function()
                                                                                                                           Return Maintenance.SystemManagement.SystemProductionAreaDisciplineSelectList.GetSystemProductionAreaDisciplineSelectList
                                                                                                                         End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemProductionAreaStatusSelectList As Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaStatusSelectList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemProductionAreaStatusSelectList, Function()
                                                                                                                         Return Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaStatusSelectList.GetROSystemProductionAreaStatusSelectList
                                                                                                                       End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemProductionAreaCallTimeSettingList As Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaCallTimeSettingList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemProductionAreaCallTimeSettingList, Function()
                                                                                                                            Return Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaCallTimeSettingList.GetROSystemProductionAreaCallTimeSettingList
                                                                                                                          End Function)
      End Get
    End Property

    Public ReadOnly Property ROTurnAroundPointContactList As Maintenance.SatOps.ReadOnly.ROTurnAroundPointContactList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROTurnAroundPointContactList, Function()
                                                                                                               Return Maintenance.SatOps.ReadOnly.ROTurnAroundPointContactList.GetROTurnAroundPointContactList
                                                                                                             End Function)
      End Get
    End Property

    Public ReadOnly Property RODepartmentList As Maintenance.Company.ReadOnly.RODepartmentList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RODepartmentList, Function()
                                                                                                   Return Maintenance.Company.ReadOnly.RODepartmentList.GetRODepartmentList
                                                                                                 End Function)
      End Get
    End Property

    Public ReadOnly Property ROCustomResourceList As Resources.ReadOnly.ROCustomResourceList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROCustomResourceList, Function()
                                                                                                       Return Resources.ReadOnly.ROCustomResourceList.GetROCustomResourceList
                                                                                                     End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemProductionAreaDisciplineReportList As OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplineReportList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemProductionAreaDisciplineReportList, Function()
                                                                                                                             Return OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaDisciplineReportList.GetROSystemProductionAreaDisciplineReportList
                                                                                                                           End Function)

      End Get
    End Property

    Public ReadOnly Property ROUserSystemAreaList As OBLib.Maintenance.General.ReadOnly.ROUserSystemAreaList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROUserSystemAreaList, Function()
                                                                                                       Return OBLib.Maintenance.General.ReadOnly.ROUserSystemAreaList.GetROUserSystemAreaList
                                                                                                     End Function)

      End Get
    End Property

    Public ReadOnly Property ROSynergyChangeTypeList As OBLib.Synergy.ReadOnly.ROSynergyChangeTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSynergyChangeTypeList, Function()
                                                                                                          Return OBLib.Synergy.ReadOnly.ROSynergyChangeTypeList.GetROSynergyChangeTypeList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property RORoomTypeReportList As OBLib.Maintenance.Rooms.ReadOnly.RORoomTypeReportList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.RORoomTypeReportList, Function()
                                                                                                       Return OBLib.Maintenance.Rooms.ReadOnly.RORoomTypeReportList.GetRORoomTypeReportList
                                                                                                     End Function)
      End Get
    End Property

    Public ReadOnly Property ROSmsSearchScenarioList As OBLib.Notifications.SmS.ReadOnly.ROSmsSearchScenarioList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSmsSearchScenarioList, Function()
                                                                                                          Return OBLib.Notifications.Sms.ReadOnly.ROSmsSearchScenarioList.GetROSmsSearchScenarioList
                                                                                                        End Function)
      End Get
    End Property

    Public ReadOnly Property ROShiftAllowanceTypeList As OBLib.Maintenance.SystemManagement.ROShiftAllowanceTypeList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROShiftAllowanceTypeList, Function()
                                                                                                           Return OBLib.Maintenance.SystemManagement.ROShiftAllowanceTypeList.GetROShiftAllowanceTypeList
                                                                                                         End Function)
      End Get
    End Property

    Public ReadOnly Property ROSystemProductionAreaChannelDefaultList As OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaChannelDefaultList
      Get
        Return RegisterList(Singular.CommonData.ContextType.Any, Function(c) c.ROSystemProductionAreaChannelDefaultList, Function()
                                                                                                                           Return OBLib.Maintenance.SystemManagement.ReadOnly.ROSystemProductionAreaChannelDefaultList.GetROSystemProductionAreaChannelDefaultList
                                                                                                                         End Function)
      End Get
    End Property

  End Class

  Public Shadows Class Enums

    Enum ProductionType
      Soccer = 1
      Rugby = 2
      Cricket = 3
    End Enum

    Public Enum BiometricsImportType
      AnvizImport = 1
      CentralSecurityImport = 2
    End Enum

    Public Enum Department As Integer
      <DescriptionAttribute("Production")> Production = 1
      <DescriptionAttribute("Finance")> Finance = 2
      <DescriptionAttribute("SS Media Solutions")> SSMediaSolutions = 3
      <DescriptionAttribute("Other")> Other = 4
      <DescriptionAttribute("Scheduling")> Scheduling = 5
    End Enum

    Public Enum System As Integer
      <DescriptionAttribute("Production Services")> ProductionServices = 1
      <DescriptionAttribute("Production Content")> ProductionContent = 2
      <DescriptionAttribute("Finance")> Finance = 3
      <DescriptionAttribute("ICR")> ICR = 4
      <DescriptionAttribute("Playout Operations")> PlayoutOperations = 5
      <DescriptionAttribute("Unknown")> Unknown = 6
      <DescriptionAttribute("NSW Timesheets")> NSWTimesheets = 7
      <DescriptionAttribute("Programming")> Programming = 8
    End Enum

    Public Enum ProductionArea As Integer
      <DescriptionAttribute("Outside Broadcast")> OB = 1
      <DescriptionAttribute("Studio (Randburg)")> Studio = 2
      <DescriptionAttribute("ICR (Randburg)")> ICRRandburg = 3
      <DescriptionAttribute("ICR (Samrand)")> ICRSamrand = 4
      <DescriptionAttribute("Unknown")> Unknown = 5
      <DescriptionAttribute("Maximo (Randburg)")> Maximo = 6
      <DescriptionAttribute("Blitz (Randburg)")> Blitz = 7
      <DescriptionAttribute("SatOps (Randburg)")> SatOps = 8
      <DescriptionAttribute("Media Operations (Randburg)")> MediaOperationsRandburg = 9
      <DescriptionAttribute("MCR (Randburg)")> MCRRandburg = 10
      <DescriptionAttribute("MCR (Samrand)")> MCRSamrand = 11
      <DescriptionAttribute("SCCR (Randburg)")> SCCRRandburg = 12
      <DescriptionAttribute("Programming (Randburg)")> ProgrammingRandburg = 13
      <DescriptionAttribute("Programming (Samrand)")> ProgrammingSamrand = 14
      <DescriptionAttribute("SCCR (Samrand)")> SCCRSamrand = 15
    End Enum

    Public Enum CrewType As Integer
      <DescriptionAttribute("Driver")> Driver = 1
      <DescriptionAttribute("Rig")> Rig = 2
      <DescriptionAttribute("Ops")> Ops = 3
      <DescriptionAttribute("Production")> Production = 4
      <DescriptionAttribute("Event Manager")> EventManager = 5
    End Enum

    Public Enum ProductionAreaStatus As Integer
      <DescriptionAttribute("New")> [New] = 1
      <DescriptionAttribute("Crew Finalised")> CrewFinalised = 2
      <DescriptionAttribute("Planning Finalised")> PlanningFinalised = 3
      <DescriptionAttribute("Reconciled")> Reconciled = 4
      <DescriptionAttribute("Cancelled")> Cancelled = 5
      <DescriptionAttribute("Confirmed")> Confirmed = 9
    End Enum

    Public Enum TransactionType As Integer
      <DescriptionAttribute("Invoice")> Invoice = 1
      <DescriptionAttribute("Credit Note")> CreditNote = 2
    End Enum

    Public Enum VatType As Integer
      <DescriptionAttribute("Standard")> Standard = 1
      <DescriptionAttribute("Zero")> Zero = 2
    End Enum

    Public Enum CreditorInvoiceDetailType As Integer
      <DescriptionAttribute("Service Fees")> ServiceFees = 1
      <DescriptionAttribute("Accommodation")> Accommodation = 2
      <DescriptionAttribute("Flights")> Flights = 3
      <DescriptionAttribute("Car Rentals")> CarRentals = 4
      <DescriptionAttribute("HR Invoice")> HRInvoice = 5
      <DescriptionAttribute("SnT")> SnT = 6
    End Enum

    Public Enum UserType As Integer
      SuperUser = 1
      Administrator = 2
      EventManager = 3
      Web = 4
      Finance = 5
      LineManager = 6
      AdministratorKerry = 7
      UnitSupervisor = 8
      AudioSupervisor = 10
      LineManagerDheshnie = 11
      LineManagerJakkals = 12
      ProductionManager = 13
      EventAdministrator = 14
      FinanceGraham = 15
      InvoiceCapturer = 16
      ResourceScheduler = 25
      EventManagerIntern = 28
    End Enum

    Public Enum Users As Integer
      SuperUser = 1
      KerryAnn = 6
      Chevani = 152
    End Enum

    Public Enum ContractType As Integer
      <DescriptionAttribute("None")> None = 0
      <DescriptionAttribute("Permanent")> Permanent = 2
      <DescriptionAttribute("Contract")> Contract = 3
      <DescriptionAttribute("Temp")> Temp = 4
      <DescriptionAttribute("Freelancer")> Freeleancer = 6
      <DescriptionAttribute("Trainee")> Trainee = 7
      <DescriptionAttribute("Independant Service Provider")> ISP = 8
    End Enum

    Public Enum PositionLayoutType As Integer
      <DescriptionAttribute("Camera")> Camera = 1
      <DescriptionAttribute("Audio")> Audio = 2
    End Enum

    Public Enum EmployeeMatch
      <DescriptionAttribute("ID Num match")> IDNummatch = 1
      <DescriptionAttribute("First 10 characters of ID Num match")> First10charactersofIDNummatch = 2
      <DescriptionAttribute("Employee Code match")> EmployeeCodematch = 3
      <DescriptionAttribute("First Name, Surname and DOB alike")> FirstNameandSurnameandDOBalike = 4
      <DescriptionAttribute("First Name, Surname match")> FirstNameandSurnamematch = 5
      <DescriptionAttribute("First Name, Surname alike")> FirstNameandSurnamealike = 6
    End Enum

    Public Enum PaymentRunStatus As Integer
      <DescriptionAttribute("Open")> Open = 1
      <DescriptionAttribute("Closed")> Closed = 2
      <DescriptionAttribute("Completed")> Completed = 3
    End Enum

    Public Enum Discipline As Integer
      <DescriptionAttribute("None")> None = 0
      <DescriptionAttribute("Audio Assistant")> AudioAssistant = 1
      <DescriptionAttribute("Audio Mixer")> AudioMixer = 2
      <DescriptionAttribute("Bashers")> Bashers = 3
      <DescriptionAttribute("Camera Operator")> CameraOperator = 4
      <DescriptionAttribute("Driver")> Driver = 5
      <DescriptionAttribute("Engineer")> Engineer = 6
      <DescriptionAttribute("Event Manager")> EventManager = 7
      <DescriptionAttribute("EVS Operator")> EVSOperator = 8
      <DescriptionAttribute("Jib Assistant")> JibAssistant = 9
      <DescriptionAttribute("Jib Operator")> JibOperator = 10
      <DescriptionAttribute("Unit Supervisor")> UnitSupervisor = 11
      <DescriptionAttribute("Vision Controller")> VisionController = 12
      <DescriptionAttribute("Trainee")> Trainee = 20
      <DescriptionAttribute("Hand Held Mic Carrier")> HandHeldMicCarrier = 22
      <DescriptionAttribute("Relief Crew - Driver")> ReliefCrewDriver = 23
      <DescriptionAttribute("Relief Crew - Ops")> ReliefCrewOps = 24
      <DescriptionAttribute("Relief Crew - Rig")> ReliefCrewRig = 25

      <DescriptionAttribute("Commentator")> Commentator = 29
      <DescriptionAttribute("Production Manager")> ProductionManager = 31
      <DescriptionAttribute("Production Co-Ordinator")> ProductionCoOrdinator = 32
      <DescriptionAttribute("Head of Production")> HeadofProduction = 33
      <DescriptionAttribute("Operations Manager")> OperationsManager = 34
      <DescriptionAttribute("Logistics Manager")> LogisticsManager = 35
      <DescriptionAttribute("Producer")> Producer = 36
      <DescriptionAttribute("Director")> Director = 37
      <DescriptionAttribute("VT Director")> VTDirector = 38
      <DescriptionAttribute("Floor Manager")> FloorManager = 39
      <DescriptionAttribute("Production Assistant")> ProductionAssistant = 40
      <DescriptionAttribute("Vision Mixer")> VisionMixer = 41
      <DescriptionAttribute("Stats")> Stats = 42
      <DescriptionAttribute("Graphics")> Graphics = 43
      <DescriptionAttribute("Make Up Artist")> MakeUpArtist = 44
      <DescriptionAttribute("Sideline Commentator")> SidelineCommentator = 45
      <DescriptionAttribute("OB Field Presenter")> OBFieldPresenter = 46
      <DescriptionAttribute("Studio Presenter")> StudioPresenter = 47
      <DescriptionAttribute("Studio Guest")> StudioGuest = 48
      <DescriptionAttribute("ENG Cameraman")> ENGCameraman = 49
      <DescriptionAttribute("Editor")> Editor = 50
      <DescriptionAttribute("OB Guest")> OBGuest = 64
      '--
      <DescriptionAttribute("Senior Manager Production Services")> SeniorManagerProductionServices = 73
      <DescriptionAttribute("Production Services Operations Manager")> ProductionServicesOperationsManager = 74
      <DescriptionAttribute("Production Services Resource Office Manager")> ProductionServicesResourceOfficeManager = 75
      <DescriptionAttribute("Production Services Technical Manager")> ProductionServicesTechnicalManager = 76
      <DescriptionAttribute("Production Services Logistics Manager")> ProductionServicesLogisticsManager = 77
      <DescriptionAttribute("Production Services Crew Manager")> ProductionServicesCrewManager = 78
      <DescriptionAttribute("Vision Control Supervisor")> VisionControlSupervisor = 86
      '--
      <DescriptionAttribute("Event Manager Intern")> EventManagerIntern = 284
    End Enum

    Public Enum VehicleType As Integer
      <DescriptionAttribute("OB Van")> OBVan = 1
      <DescriptionAttribute("Jib")> Jib = 2
    End Enum

    Public Enum ProductionTimelineType As Integer
      <DescriptionAttribute("Vehicle Pre Travel")> VehiclePreTravel = 1
      <DescriptionAttribute("Rig Pre Travel")> RigPreTravel = 2
      <DescriptionAttribute("Ops Pre Travel")> OpsPreTravel = 3
      <DescriptionAttribute("Rig")> Rig = 4
      <DescriptionAttribute("Ops Crew Setup")> OpsCrewSetup = 5
      <DescriptionAttribute("Technical Checks")> TechnicalChecks = 6
      <DescriptionAttribute("Production Meeting")> ProductionMeeting = 7
      <DescriptionAttribute("Meals")> Meals = 8
      <DescriptionAttribute("Facility Checks")> FacilityChecks = 9
      <DescriptionAttribute("Rehearsal")> Rehearsal = 10
      <DescriptionAttribute("Transmission")> Transmission = 11
      <DescriptionAttribute("Rig Post Travel")> RigPostTravel = 12
      <DescriptionAttribute("Ops Post Travel")> OpsPostTravel = 13
      <DescriptionAttribute("Vehicle Post Travel")> VehiclePostTravel = 14
      <DescriptionAttribute("Ops De-Rig")> OpsDeRig = 20
      <DescriptionAttribute("Rig De-Rig")> RigDeRig = 21
      <DescriptionAttribute("Driver De-Rig")> DriverDeRig = 22
      <DescriptionAttribute("Build Up")> BuildUp = 23
      <DescriptionAttribute("Reserve Day")> ReserveDay = 24
      <DescriptionAttribute("Driver Pre Travel")> DriverPreTravel = 25
      <DescriptionAttribute("Driver Post Travel")> DriverPostTravel = 26
      <DescriptionAttribute("Credit Day")> CreditDay = 27
      <DescriptionAttribute("Meals - Ops")> MealsOps = 28
      <DescriptionAttribute("Meals - Rig")> MealsRig = 29
      <DescriptionAttribute("Day Away")> DayAway = 27
      <DescriptionAttribute("Driver Travel from Hotel to venue")> DriverTravelFromHotelToVenue = 34
      <DescriptionAttribute("Driver Travel back to the Hotel")> DriverTravelBackToTheHotel = 37
      <DescriptionAttribute("Temp Timeline - Bookings")> TempTimelineBookings = 41
      <DescriptionAttribute("Travel To Production")> TravelToProduction = 42
      <DescriptionAttribute("Return From Production")> ReturnFromProduction = 43
      <DescriptionAttribute("Call Time")> CallTime = 51
      <DescriptionAttribute("On Air")> OnAir = 52
      <DescriptionAttribute("Off Air")> OffAir = 53
      <DescriptionAttribute("Wrap")> Wrap = 54
      <DescriptionAttribute("Crew Hand Over")> CrewHandOver = 55
    End Enum

    Public Enum RentalCarAgents As Integer
      <DescriptionAttribute("Budget")> Budget = 1
      <DescriptionAttribute("Europcar")> Europcar = 2
      <DescriptionAttribute("Avis")> Avis = 3
      <DescriptionAttribute("Bidvest")> Bidvest = 4
    End Enum

    Public Enum EquipmentType As Integer
      <DescriptionAttribute("Camera")> Camera = 1
      <DescriptionAttribute("Lens")> Lens = 2
      <DescriptionAttribute("Camera Supports")> CameraSupports = 3
      <DescriptionAttribute("Generator")> Generator = 4
      <DescriptionAttribute("Lighting")> Lighting = 5
      <DescriptionAttribute("Floor monitors")> Floormonitors = 9
      <DescriptionAttribute("FX Mics")> FXMics = 10
      <DescriptionAttribute("Presentation Mics")> PresentationMics = 12
      <DescriptionAttribute("Radio Mics")> RadioMics = 13
      <DescriptionAttribute("Combination")> Combination = 19
    End Enum

    Public Enum EquipmentSubType As Integer
      <DescriptionAttribute("Pencil")> Pencil = 21
      <DescriptionAttribute("Net")> Net = 22
      <DescriptionAttribute("Super Slow")> SuperSlow = 23
      <DescriptionAttribute("Ultramotion")> Ultramotion = 24
      <DescriptionAttribute("Stump")> Stump = 25
      <DescriptionAttribute("Chopper Cam")> ChopperCam = 26
      <DescriptionAttribute("Pole Cam")> PoleCam = 27
      <DescriptionAttribute("Run-Out")> RunOut = 28
      '<DescriptionAttribute("4.5 x 11 (W/A)")> 4.5x11 = 29
      '<DescriptionAttribute("4.8 x 11 (W/A)")> 4.8x11 = 30
      '<DescriptionAttribute("15 x")> 15x = 31
      '<DescriptionAttribute("16 x")> 16x = 32
      '<DescriptionAttribute("17 x")> 17x = 33
      '<DescriptionAttribute("18 x")> 18x = 34
      '<DescriptionAttribute("19 x")> 19x = 35
      '<DescriptionAttribute("20 x")> 20x = 36
      '<DescriptionAttribute("21 x")> 21x = 37
      '<DescriptionAttribute("22 x")> 22x = 38
      '<DescriptionAttribute("33 x")> 33x = 39
      '<DescriptionAttribute("36 x")> 36x = 40
      '<DescriptionAttribute("40 x")> 40x = 41
      '<DescriptionAttribute("72 x")> 72x = 42
      '<DescriptionAttribute("75 x")> 75x = 43
      '<DescriptionAttribute("86 x")> 86x = 44
      '<DescriptionAttribute("87 x")> 87x = 45
      <DescriptionAttribute("Fish-eye")> FishEye = 46
      <DescriptionAttribute("Tripod")> Tripod = 47
      <DescriptionAttribute("Gunmount")> Gunmount = 48
      <DescriptionAttribute("Steady Cam")> SteadyCam = 50
      <DescriptionAttribute("Dolly")> Dolly = 51
      <DescriptionAttribute("Standard")> Standard = 65
      <DescriptionAttribute("Hand Held")> HandHeld = 67
      '<DescriptionAttribute("75 x 9.3")> 75x9.3 = 74
      '<DescriptionAttribute("100 x")> 100x = 76
      '<DescriptionAttribute("40 x 10")> 40x10 = 77
      '<DescriptionAttribute("40 x 14")> 40x14 = 78
      '<DescriptionAttribute("22 x 7.6")> 22x7.6 = 79
      '<DescriptionAttribute("14 x 4.2 W/A")> 14x4.2W/A = 80
      <DescriptionAttribute("Minicam")> Minicam = 81
      <DescriptionAttribute("Comm Cam")> CommCam = 98
      <DescriptionAttribute("Dinkycam")> Dinkycam = 100
      <DescriptionAttribute("ENG Kit")> ENGKit = 104
      <DescriptionAttribute("Goal-Line Cam")> GoalLineCam = 111
      <DescriptionAttribute("Vis.Cntr Panel")> VisCntrPanel = 136
      '<DescriptionAttribute("W/A 4.5 x")> W/A4.5x = 137
      <DescriptionAttribute("Camcorder")> Camcorder = 194
      <DescriptionAttribute("Fish eye dinky")> Fisheyedinky = 195
      <DescriptionAttribute("Remote control dinky cam")> Remotecontroldinkycam = 196
      '<DescriptionAttribute("100 x 9.3")> 100x9.3 = 197
      '<DescriptionAttribute("11 x 4.7")> 11x4.7 = 198
      '<DescriptionAttribute("14 x 4.3")> 14x4.3 = 199
      '<DescriptionAttribute("55 x ")> 55x = 200
      '<DescriptionAttribute("86 x 9.3")> 86x9.3 = 201
      <DescriptionAttribute("Blonds")> Blonds = 202
      <DescriptionAttribute("Red Heads")> RedHeads = 203
      '<DescriptionAttribute("150m")> 150m = 204
      '<DescriptionAttribute("250m")> 250m = 205
      '<DescriptionAttribute("300m")> 300m = 206
      '<DescriptionAttribute("450m")> 450m = 207
      '<DescriptionAttribute("Heavy Duty Sachtler 90")> HeavyDutySachtler90 = 209
      <DescriptionAttribute("Jib")> Jib = 210
      '<DescriptionAttribute("Light Duty System 20 Plus")> LightDutySystem20Plus = 211
      '<DescriptionAttribute("Light Duty System 25")> LightDutySystem25 = 212
      '<DescriptionAttribute("Medium Duty Sachtler 60")> MediumDutySachtlerSixty = 213
      <DescriptionAttribute("Steadicam")> Steadicam = 214
      '<DescriptionAttribute("Clearcom RX Packs")> ClearcomRXPacks = 215
      '<DescriptionAttribute("Clearcom TX")> ClearcomTX = 216
      '<DescriptionAttribute("External comms")> Externalcomms = 217
      '<DescriptionAttribute("Kenwood")> Kenwood = 218
      '<DescriptionAttribute("Outstations")> Outstations = 219
      '<DescriptionAttribute("Telex TX/RX")> TelexTXRX = 220
      '<DescriptionAttribute("Telex TX/RX Packs")> TelexTXRXPacks = 221
      '<DescriptionAttribute("17 LCD")> LCD17 = 222
      '<DescriptionAttribute("20 LCD")> LCD20 = 223
      '<DescriptionAttribute("9 LCD")> LCD9 = 224
      '<DescriptionAttribute("Bravia 32 LCD")> Bravia32LCD = 225
      '<DescriptionAttribute("5.1 Microphone ( DFS 2 & 3 )")> 51Microphone(DFS2&3) = 226
      '<DescriptionAttribute("ME 66 Short Riffle Microphone")> ME66ShortRiffleMicrophone = 227
      '<DescriptionAttribute("MKH 418S M/S Riffle Microphone")> MKH418SMSRiffleMicrophone = 228
      '<DescriptionAttribute("MKH 60 Medium Riffle Microphone")> MKH60MediumRiffleMicrophone = 229
      '<DescriptionAttribute("MKH 70 Long Riffle Microphone")> MKH70LongRiffleMicrophone = 230
      '<DescriptionAttribute("VP88 Short Stereo Microphone")> VP88ShortStereoMicrophone = 231
      '<DescriptionAttribute("100 KVA")> 100KVA = 232
      '<DescriptionAttribute("60 KVA")> 60KVA = 233
      '<DescriptionAttribute("Beta 58")> Beta58 = 234
      '<DescriptionAttribute("E6 Omni Earset Microphone")> E6OmniEarsetMicrophone = 235
      '<DescriptionAttribute("MKE2 Lavalier Microphone")> MKE2LavalierMicrophone = 236
      '<DescriptionAttribute("RE50")> RE50 = 237
      '<DescriptionAttribute("Radio Handhels Microphone")> RadioHandhelsMicrophone = 238
      '<DescriptionAttribute("Radio Lavalier Microphone")> RadioLavalierMicrophone = 239
      '<DescriptionAttribute("4Ch Max")> 4ChMax = 240
      '<DescriptionAttribute("6Ch")> 6Ch = 241
      '<DescriptionAttribute("Betacam SP")> BetacamSP = 242
      '<DescriptionAttribute("Blu-Ray")> BluRay = 243
      '<DescriptionAttribute("CD Player")> CDPlayer = 244
      '<DescriptionAttribute("DSR1000P")> DSR1000P = 245
      '<DescriptionAttribute("DVD")> DVD = 246
      '<DescriptionAttribute("FX PC")> FXPC = 247
      <DescriptionAttribute("IP Director")> IPDirector = 248
      '<DescriptionAttribute("Sony DSR2000")> SonyDSR2000 = 249
      '<DescriptionAttribute("Sony HDW M2000P")> SonyHDWM2000P = 250
      <DescriptionAttribute("Super Slo Option")> SuperSloOption = 251
      <DescriptionAttribute("XD Cam")> XDCam = 252
      <DescriptionAttribute("Studio")> Studio = 253
      <DescriptionAttribute("RF - Cam")> RFCam = 254
      <DescriptionAttribute("Beauty Locked off")> BeautyLockedoff = 255
      <DescriptionAttribute("Matt Cam")> MattCam1 = 256
      <DescriptionAttribute("Worm Cam")> WormCam = 258
      <DescriptionAttribute("Run Out Support")> RunOutSupport = 259
      <DescriptionAttribute("Matt Cam")> MattCam2 = 260
      <DescriptionAttribute("Hawkeye")> Hawkeye = 261
    End Enum

    Public Enum Position As Integer
      <DescriptionAttribute("Camera 1")> Camera1 = 1
      <DescriptionAttribute("Camera 2")> Camera2 = 2
      <DescriptionAttribute("Camera 3")> Camera3 = 3
      <DescriptionAttribute("Camera 4")> Camera4 = 4
      <DescriptionAttribute("Camera 5")> Camera5 = 5
      <DescriptionAttribute("Camera 6")> Camera6 = 6
      <DescriptionAttribute("Camera 7")> Camera7 = 7
      <DescriptionAttribute("Camera 8")> Camera8 = 8
      <DescriptionAttribute("Camera 9")> Camera9 = 9
      <DescriptionAttribute("Red/Blue")> RedBlue = 17
      <DescriptionAttribute("Purple")> Purple = 18
      <DescriptionAttribute("Orange")> Orange = 19
      <DescriptionAttribute("Gold")> Gold = 20
      <DescriptionAttribute("Silver")> Silver = 21
      <DescriptionAttribute("White")> White = 22
      <DescriptionAttribute("VC1")> VC1 = 23
      <DescriptionAttribute("VC2")> VC2 = 24
      <DescriptionAttribute("VC3")> VC3 = 25
      <DescriptionAttribute("VC4")> VC4 = 26
      <DescriptionAttribute("VC5")> VC5 = 27
      '     <DescriptionAttribute("Camera 10")> Camera10 = 28
      '     <DescriptionAttribute("Camera 11")> Camera11 = 29
      '     <DescriptionAttribute("Camera 12")> Camera12 = 30
      '     <DescriptionAttribute("Camera 13")> Camera13 = 31
      '     <DescriptionAttribute("Camera 14")> Camera14 = 32
      '     <DescriptionAttribute("Yellow")> Yellow = 33
      '     <DescriptionAttribute("Green")> Green = 34
      '     <DescriptionAttribute("Pink")> Pink = 35
      '     <DescriptionAttribute("VC6")> VC6 = 36
      '     <DescriptionAttribute("Camera 15")> Camera15 = 37
      '     <DescriptionAttribute("Camera 16")> Camera16 = 38
      '     <DescriptionAttribute("Camera 17")> Camera17 = 39
      '     <DescriptionAttribute("Camera 18")> Camera18 = 40
      '     <DescriptionAttribute("Camera 19")> Camera19 = 41
      '     <DescriptionAttribute("Camera 20")> Camera20 = 42
      '     <DescriptionAttribute("Camera 21")> Camera21 = 43
      '     <DescriptionAttribute("Camera 22")> Camera22 = 44
      '     <DescriptionAttribute("Camera 23")> Camera23 = 45
      '     <DescriptionAttribute("Camera 24")> Camera24 = 46
      '     <DescriptionAttribute("VC7")> VC7 = 47
      '     <DescriptionAttribute("Jib Operator")> JibOperator = 48
      '     <DescriptionAttribute("Jib Assistant")> JibAssistant = 49
      <DescriptionAttribute("Unit Supervisor")> UnitSupervisor = 50
      <DescriptionAttribute("Engineer")> Engineer = 51
      '     <DescriptionAttribute("Camera 25")> Camera25 = 52
      '     <DescriptionAttribute("Camera 26")> Camera26 = 53
      '     <DescriptionAttribute("Camera 27")> Camera27 = 54
      '     <DescriptionAttribute("Cyan")> Cyan = 55
      '     <DescriptionAttribute("Run Out 1")> RunOut1 = 56
      '     <DescriptionAttribute("Run Out 2")> RunOut2 = 57
      '     <DescriptionAttribute("Run Out 3")> RunOut3 = 59
      '     <DescriptionAttribute("Run Out 4")> RunOut4 = 60
      '     <DescriptionAttribute("Stump 1")> Stump1 = 61
      '     <DescriptionAttribute("Stump 2")> Stump2 = 62
      '     <DescriptionAttribute("Studio 1")> Studio1 = 63
      '     <DescriptionAttribute("Studio 2")> Studio2 = 64
      '     <DescriptionAttribute("Studio -Locked Off")> StudioLockedOff = 65
      '     <DescriptionAttribute("Q Ball")> QBall = 66
      '     <DescriptionAttribute("Beauty Locked Off")> BeautyLockedOff = 67
      '     <DescriptionAttribute("Studio 3")> Studio3 = 68
      '     <DescriptionAttribute("Camera 28")> Camera28 = 69
      '     <DescriptionAttribute("Camera 29")> Camera29 = 70
      '     <DescriptionAttribute("Camera 30")> Camera30 = 71
      '     <DescriptionAttribute("Camera 31")> Camera31 = 72
      '     <DescriptionAttribute("Camera 32")> Camera32 = 73
      '     <DescriptionAttribute("Camera 33")> Camera33 = 74
      '     <DescriptionAttribute("Camera 34")> Camera34 = 75
      '     <DescriptionAttribute("Camera 35")> Camera35 = 76
      '     <DescriptionAttribute("Camera 36")> Camera36 = 77
      '     <DescriptionAttribute("Camera 37")> Camera37 = 78
      '     <DescriptionAttribute("Camera 38")> Camera38 = 79
      '     <DescriptionAttribute("Camera 39")> Camera39 = 80
      '     <DescriptionAttribute("Camera 40")> Camera40 = 81
      '     <DescriptionAttribute("Camera 41")> Camera41 = 82
      '     <DescriptionAttribute("VC8")> VC8 = 83
      '     <DescriptionAttribute("VC9")> VC9 = 84
      '     <DescriptionAttribute("VC10")> VC10 = 85
      '     <DescriptionAttribute("VC11")> VC11 = 86
      '     <DescriptionAttribute("VC12")> VC12 = 87
      '     <DescriptionAttribute("Relief")> Relief = 88
      '     <DescriptionAttribute("VC2")> VC2Duplicate = 89
      '     <DescriptionAttribute("VC3")> VC3Duplicate = 90
      '     <DescriptionAttribute("EVS 1")> EVS1 = 91
      '     <DescriptionAttribute("EVS 2")> EVS2 = 92
      '     <DescriptionAttribute("EVS 3")> EVS3 = 93
      '     <DescriptionAttribute("Camera1")> Camera1Duplicate = 94
      '     <DescriptionAttribute("Relief")> Relief = 95
      '     <DescriptionAttribute("Relief")> Relief = 96
      '<DescriptionAttribute("Camera 4 (RCD)")>Camera4(RCD) = 97
      '<DescriptionAttribute("Goal-Line Cam 1")>Goal-LineCam1 = 98
      '<DescriptionAttribute("Goal -Line Cam 2")>Goal-LineCam2 = 99
      '     <DescriptionAttribute("Comm Cam 1")> CommCam1 = 100
      '     <DescriptionAttribute("Comm Cam 2")> CommCam2 = 101
      '     <DescriptionAttribute("Ultramotion")> Ultramotion = 102
      '<DescriptionAttribute("H/H Mic 1")>H/HMic1 = 103
      '<DescriptionAttribute("H/H Mic 2")>H/HMic2 = 104
      '<DescriptionAttribute("H/H Mic 3")>H/HMic3 = 105
      '<DescriptionAttribute("H/H Mic 4")>H/HMic4 = 106
      '<DescriptionAttribute("H/H Mic 5")>H/HMic5 = 107
      '<DescriptionAttribute("H/H Mic 6")>H/HMic6 = 108
      '<DescriptionAttribute("H/H Mic 7")>H/HMic7 = 109
      '<DescriptionAttribute("H/H Mic 8")>H/HMic8 = 110
      '<DescriptionAttribute("H/H Mic 9")>H/HMic9 = 111
      '<DescriptionAttribute("H/H Mic 10")>H/HMic10 = 112
      '<DescriptionAttribute("H/H Mic 11")>H/HMic11 = 113
      '<DescriptionAttribute("H/H Mic 12")>H/HMic12 = 114
      '<DescriptionAttribute("H/H Mic 13")>H/HMic13 = 115
      '     <DescriptionAttribute("Comm Cam 3")> CommCam3 = 116
      '     <DescriptionAttribute("Comm Cam 4")> CommCam4 = 117
      '     <DescriptionAttribute("SuperSlow")> SuperSlow = 118
      '     <DescriptionAttribute("Matt Cam 1")> MattCam1 = 119
      '     <DescriptionAttribute("Matt Cam 2")> MattCam2 = 120
      '     <DescriptionAttribute("Hawkeye")> Hawkeye = 121
      '     <DescriptionAttribute("RF Camera")> RFCamera = 122
      '     <DescriptionAttribute("Pencil Cam")> PencilCam = 123
      '     <DescriptionAttribute("Xhosa")> Xhosa = 124
      '     <DescriptionAttribute("Zulu")> Zulu = 125
      '     <DescriptionAttribute("Sotho")> Sotho = 126
      '     <DescriptionAttribute("Setswana")> Setswana = 127
      '     <DescriptionAttribute("English")> English = 131
      '     <DescriptionAttribute("Afrikaans")> Afrikaans = 132
      '     <DescriptionAttribute("OB")> OB = 133
      '     <DescriptionAttribute("Randburg Studio")> RandburgStudio = 134
      '     <DescriptionAttribute("OB")> OB = 135
      '     <DescriptionAttribute("Randburg Studio")> RandburgStudio = 136
      '     <DescriptionAttribute("OB")> OB = 137
      '     <DescriptionAttribute("Randburg Studio")> RandburgStudio = 138
      '     <DescriptionAttribute("Afrikaans Off Tube")> AfrikaansOffTube = 141
      '     <DescriptionAttribute("English Off Tube")> EnglishOffTube = 142
      '     <DescriptionAttribute("Setswana Off Tube")> SetswanaOffTube = 143
      '     <DescriptionAttribute("Sotho Off Tube")> SothoOffTube = 144
      '     <DescriptionAttribute("Xhosa Off Tube")> XhosaOffTube = 145
      '     <DescriptionAttribute("Zulu Off Tube")> ZuluOffTube = 146
      '     <DescriptionAttribute("OB")> OB = 147
      '     <DescriptionAttribute("Randburg Studio")> RandburgStudio = 148
      '     <DescriptionAttribute("OB")> OB = 149
      '     <DescriptionAttribute("Randburg Studio")> RandburgStudio = 150
    End Enum

    Public Enum ProductionCommentReportSection As Integer
      <DescriptionAttribute("Client Details")> ClientDetails = 1
      <DescriptionAttribute("Production Details")> ProductionDetails = 2
      <DescriptionAttribute("Production Crew Assignments")> ProductionCrewAssignments = 3
      <DescriptionAttribute("Broadcast Requirements")> BroadcastRequirements = 4
      <DescriptionAttribute("Audio Requirements")> AudioRequirements = 5
      <DescriptionAttribute("Equipment")> Equipment = 6
      <DescriptionAttribute("Additional Matches")> AdditionalMatches = 7
      <DescriptionAttribute("Special Arrangements")> SpecialArrangements = 11
    End Enum

    Public Enum RoomScheduleType As Integer
      <DescriptionAttribute("Production")> Production = 1
      <DescriptionAttribute("Maintenance")> Maintenance = 2
      <DescriptionAttribute("Set Change")> SetChange = 3
      <DescriptionAttribute("Ad Hoc")> AdHoc = 4
      <DescriptionAttribute("PlaceHolder")> PlaceHolder = 5
    End Enum

    Public Enum Country As Integer
      <DescriptionAttribute("South Africa")> SouthAfrica = 1
    End Enum

    Public Enum ClashType
      <DescriptionAttribute("Leave Before Crew Finalised")> LeaveBeforeCrewFinalised = 1
      <DescriptionAttribute("Leave After Crew Finalised")> LeaveAfterCrewFinalised = 2
    End Enum

    Public Enum OffPeriodAuthorisationType As Integer
      <DescriptionAttribute("Pending")> Pending = 1
      <DescriptionAttribute("Authorised")> Authorised = 2
      <DescriptionAttribute("Rejected")> Rejected = 3
    End Enum

    Public Enum DayOfWeek As Integer
      <DescriptionAttribute("Monday")> Monday = 1
      <DescriptionAttribute("Tuesday")> Tuesday = 2
      <DescriptionAttribute("Wednesday")> Wednesday = 3
      <DescriptionAttribute("Thursday")> Thursday = 4
      <DescriptionAttribute("Friday")> Friday = 5
      <DescriptionAttribute("Saturday")> Saturday = 6
      <DescriptionAttribute("Sunday")> Sunday = 7
    End Enum

    Public Enum SQLDayOfWeek As Integer
      <DescriptionAttribute("Sunday")> Sunday = 1
      <DescriptionAttribute("Monday")> Monday = 2
      <DescriptionAttribute("Tuesday")> Tuesday = 3
      <DescriptionAttribute("Wednesday")> Wednesday = 4
      <DescriptionAttribute("Thursday")> Thursday = 5
      <DescriptionAttribute("Friday")> Friday = 6
      <DescriptionAttribute("Saturday")> Saturday = 7
    End Enum

    Public Enum HumanResourceScheduleType
      <DescriptionAttribute("Production")> Production = 1
      <DescriptionAttribute("Shift")> Shift = 2
      <DescriptionAttribute("AdHoc")> AdHoc = 3
      <DescriptionAttribute("Leave")> Leave = 4
      <DescriptionAttribute("PreparationTime")> PreparationTime = 5
      <DescriptionAttribute("Secondment")> Secondment = 6
    End Enum

    Public Enum ProductionImportStatus
      <DescriptionAttribute("My Area Not Added")> MyAreaNotAdded = 1
      <DescriptionAttribute("My Area Added")> MyAreaAdded = 2
    End Enum

    Public Enum GroupSnTPolicies As Integer
      <DescriptionAttribute("South Africa - Domestic")> SouthAfricaDomestic = 13
      <DescriptionAttribute("South Africa - Domestic (1 June 2016)")> SouthAfricaDomesticNew = 17
    End Enum

    Public Enum SystemEmailTypes
      <DescriptionAttribute("Human Resources added/amended")> HumanResourcesAddedAmended = 1
      <DescriptionAttribute("Check Queries")> CheckQueries = 2
      <DescriptionAttribute("Production Timeline Clashes")> ProductionTimelineClashes = 3
      <DescriptionAttribute("System Errors")> SystemErrors = 4
      <DescriptionAttribute("Travel Req")> TravelReq = 5
      <DescriptionAttribute("Travel Req Authorised")> TravelReqAuthorised = 6
      <DescriptionAttribute("Travel Req Declined")> TravelReqDeclined = 7
      <DescriptionAttribute("Removed Production Human Resource")> RemovedProductionHumanResource = 8
      <DescriptionAttribute("Un-Reconciled Production")> UnReconciledProduction = 9
      <DescriptionAttribute("Room Changed")> RoomChanged = 10
      <DescriptionAttribute("Production Details Changed")> ProductionDetailsChanged = 11
      <DescriptionAttribute("Changed Human Resources After Crew Finalised")> ChangedHumanResourcesAfterCrewFinalised = 12
      <DescriptionAttribute("Un-Finalising Crew")> UnFinalisingCrew = 13
      <DescriptionAttribute("Disputed Shifts")> DisputedShifts = 14
      <DescriptionAttribute("Discipline Leave Overlaps")> DisciplineLeaveOverlaps = 15
      <DescriptionAttribute("ICR Staff Notification")> ICRStaffNotification = 16
      <DescriptionAttribute("Delegate Action")> DelegateAction = 31
    End Enum

    Public Enum NSWAuthoriseStatus
      <DataAnnotations.Display(Order:=1)> Pending = 0
      <DataAnnotations.Display(Order:=2)> Authorised = 1
      <DataAnnotations.Display(Order:=3)> Rejected = 2
    End Enum

    Public Enum MRMonthDay As Integer
      <DescriptionAttribute("Start Day")> StartDay = 25
      <DescriptionAttribute("End Day")> EndDay = 24
    End Enum

    Public Const MRRateAmount As Decimal = 28.0

    Public Enum Month As Integer
      <DescriptionAttribute("January")> January = 1
      <DescriptionAttribute("February")> February = 2
      <DescriptionAttribute("March")> March = 3
      <DescriptionAttribute("April")> April = 4
      <DescriptionAttribute("May")> May = 5
      <DescriptionAttribute("June")> June = 6
      <DescriptionAttribute("July")> July = 7
      <DescriptionAttribute("August")> August = 8
      <DescriptionAttribute("September")> September = 9
      <DescriptionAttribute("October")> October = 10
      <DescriptionAttribute("November")> November = 11
      <DescriptionAttribute("December")> December = 12
    End Enum

    Public Enum ShiftDuration As Integer
      <DescriptionAttribute("2 Weeks")> TwoWeeks = 2
      <DescriptionAttribute("3 Weeks")> ThreeWeeks = 3
      <DescriptionAttribute("4 Weeks")> FourWeeks = 4
      <DescriptionAttribute("5 Weeks")> FiveWeeks = 5
      <DescriptionAttribute("6 Weeks")> SixWeeks = 6
    End Enum

    Public Enum ShiftType As Integer
      <DescriptionAttribute("Regular Shift")> RegularShift = 1
      <DescriptionAttribute("Night Shift")> NightShift = 2
      <DescriptionAttribute("Day Off")> DayOff = 3
      <DescriptionAttribute("Ad Hoc")> AdHoc = 4
      <DescriptionAttribute("On Day")> OnDay = 5
    End Enum

    Public Enum ResourceType As Integer
      Human = 1
      Room = 2
      Vehicle = 3
      Channel = 4
      Equipment = 5
      EventFacilitator = 6
    End Enum

    Public Enum ResourceBookingType As Integer
      HROBCity = 1
      HROBContent = 2
      HRStudio = 3
      HRLeave = 4
      HRSecondment = 5
      HRAdHocTeamBuilding = 6
      HRPrepTime = 7
      RoomProduction = 8
      RoomAdHoc = 9
      EquipmentFeed = 10
      Vehicle = 11
      HRPlaceholder = 12
      RoomPlaceholder = 13
      EquipmentService = 14
      VehiclePlaceholder = 15
      HRAdHocTraining = 16
      HRAdHocMeeting = 17
      HRAdHocConference = 18
      EventFacilitatorShift = 19
    End Enum

    Public Enum Resource As Integer
      EventFacilitator = 5114
    End Enum

    Public Enum AdHocBookingType As Integer
      <DescriptionAttribute("Team Building")> TeamBuilding = 1
      <DescriptionAttribute("Training")> Training = 2
      <DescriptionAttribute("Travel")> Travel = 3
      <DescriptionAttribute("Meeting")> Meeting = 4
      <DescriptionAttribute("Conference")> Conference = 5
      <DescriptionAttribute("Production")> Production = 6
      <DescriptionAttribute("Workshop")> Workshop = 7
      <DescriptionAttribute("Studio Maintenance")> StudioMaintenance = 8
    End Enum

    Public Enum RoomType As Integer
      <DescriptionAttribute("Unknown")> Unknown = 1
      <DescriptionAttribute("Studio")> Studio = 2
      <DescriptionAttribute("Gallery")> Gallery = 3
      <DescriptionAttribute("Graphics")> Graphics = 4
      <DescriptionAttribute("PMR")> PMR = 5
      <DescriptionAttribute("AFM")> AFM = 6
      <DescriptionAttribute("HD SCCR")> HDSCCR = 7
      <DescriptionAttribute("SD SCCR")> SDSCCR = 8
      <DescriptionAttribute("Make-Up")> MakeUp = 9
    End Enum

    Public Enum TriStatBoolean As Integer
      <DescriptionAttribute("All")> All = 1
      <DescriptionAttribute("True")> TrueState = 2
      <DescriptionAttribute("False")> FalseState = 3
    End Enum

    Public Enum PublicHolidayCalculationType
      Original = 0
      UpToMidnight = 1
      OvertimeDependent = 2
    End Enum

    Public Enum TimesheetCategories
      ProductionDay = 1
      TravelDay10Hour = 2
      Leave = 3
      DayAway = 4
      AdHoc = 5
      RigDay = 6
      Off = 7
      TravelDay5Hour = 8
      '9 is a missing ID in database
      TravelAdHoc = 10
      UnBooked = 11
      FacilityBooking = 12
      Course = 13
      HalfDayLeave = 14
      UnpaidLeave = 15
    End Enum

    Public Enum CalculationChange
      PublicHolidayHours = 1
    End Enum

    Public Enum TimesheetHourType
      Normal = 1
      Overtime = 2
      Shortfall = 3
      Planning = 4
      PublicHoliday = 5
    End Enum

    Public Enum FlightType As Integer
      <DescriptionAttribute("Commercial Flight")> CommercialFlight = 1
      <DescriptionAttribute("Chartered Flight")> CharteredFlight = 2
    End Enum

    Public Enum EquipmentScheduleType As Integer
      <DescriptionAttribute("Feed")> Feed = 1
      <DescriptionAttribute("Service")> Service = 2
    End Enum

    Public Enum CreditorInvoiceDetailTypes As Integer
      <DescriptionAttribute("Freelance Work")> FreelanceWork = 5
      <DescriptionAttribute("S&T")> SnT = 6
    End Enum

    Public Enum Currency As Integer
      <DescriptionAttribute("South African Rand")> SouthAfricanRand = 1
      <DescriptionAttribute("Maloti")> Maloti = 2
      <DescriptionAttribute("British Pound")> BritishPound = 15
      <DescriptionAttribute("US Dollar")> USDollar = 16
      <DescriptionAttribute("Euro")> Euro = 17
      <DescriptionAttribute("Dollar (NZ)")> NZDollar = 18
      <DescriptionAttribute("AUD")> AusDollar = 19
      <DescriptionAttribute("Singapore Dollar")> SingaporeDollar = 20
      <DescriptionAttribute("China")> China = 21
      <DescriptionAttribute("Hong Kong Dollar")> HongKongDollar = 22
      <DescriptionAttribute("Nigerian Naira")> NigerianNaira = 23
      <DescriptionAttribute("Kenya Shilling")> KenyaShilling = 24
    End Enum

    Public Enum CurrencySymbols As Integer
      <DescriptionAttribute("R")> SouthAfricanRand = 1
      <DescriptionAttribute("LSL")> Maloti = 2
      <DescriptionAttribute("₤")> BritishPound = 15
      <DescriptionAttribute("$")> USDollar = 16
      <DescriptionAttribute("€")> Euro = 17
      <DescriptionAttribute("$")> NZDollar = 18
      <DescriptionAttribute("$")> AusDollar = 19
      <DescriptionAttribute("$")> SingaporeDollar = 20
      <DescriptionAttribute("¥")> China = 21
      <DescriptionAttribute("$")> HongKongDollar = 22
      <DescriptionAttribute("₦")> NigerianNaira = 23
      <DescriptionAttribute("KSh")> KenyaShilling = 24
    End Enum

    Public Enum TravelAdvanceType As Integer
      <DescriptionAttribute("Contingency")> Contingency = 1
    End Enum

    Public Enum AudioGroupType As Integer
      <DescriptionAttribute("Standard")> Standard = 1
    End Enum

    Public Enum AudioPairType As Integer
      <DescriptionAttribute("Standard")> Standard = 1
      <DescriptionAttribute("DolbyE")> DolbyE = 2
    End Enum

    Public Enum FeedType As Integer
      <DescriptionAttribute("Main")> Main = 1
      <DescriptionAttribute("MainBackup")> MainBackup = 2
      <DescriptionAttribute("Unilateral")> Unilateral = 3
      <DescriptionAttribute("UnilateralSS")> UnilateralSS = 4
      <DescriptionAttribute("ProducedFeed")> ProducedFeed = 5
      <DescriptionAttribute("ConsumedFeed")> ConsumedFeed = 6
    End Enum

    Public Enum CreationType As Integer
      <DescriptionAttribute("Imported")> Imported = 1
      <DescriptionAttribute("Manual")> Manual = 2
    End Enum

    Public Enum OutsourceServiceType As Integer
      <DescriptionAttribute("Catering")> Catering = 1
      <DescriptionAttribute("Scaffolding")> Scaffolding = 2
      <DescriptionAttribute("Toilets")> Toilets = 3
      <DescriptionAttribute("OBFacility")> OBFacility = 32
      <DescriptionAttribute("Graphics")> Graphics = 27
    End Enum

    Public Enum OutsourceServiceSuppliers As Integer
      <DescriptionAttribute("VisionView")> VisionView = 95
    End Enum

#Region " Biometrics "

    Public Enum AccessTerminalGroup As Integer
      <DescriptionAttribute("ICR")> ICR = 1
      <DescriptionAttribute("Studios")> Studios = 2
      <DescriptionAttribute("Outside_Broadcast")> Outside_Broadcast = 3
      <DescriptionAttribute("Playout_Operations_Randburg")> Playout_Operations_Randburg = 4
      <DescriptionAttribute("Playout_Operations_Samrand")> Playout_Operations_Samrand = 6
    End Enum

    Public Enum AccessFlag As Integer
      <DescriptionAttribute("No Arriving record")> No_Arriving_record = 1
      <DescriptionAttribute("Arrived Slightly Late")> Arrived_Slightly_Late = 2
      <DescriptionAttribute("Arrived Too Late")> Arrived_Too_Late = 4
      <DescriptionAttribute("Left Slightly Early")> Left_Slightly_Early = 8
      <DescriptionAttribute("Left Too Early")> Left_Too_Early = 16
      <DescriptionAttribute("No leaving record")> No_leaving_record = 32
      <DescriptionAttribute("Biometrics Missing")> Biometrics_Missing = 64
    End Enum

#End Region

    Public Enum IngestSourceType As Integer
      <DescriptionAttribute("Incoming Feed")> IncomingFeed = 1
      <DescriptionAttribute("Gallery")> Gallery = 2
      <DescriptionAttribute("Studio")> Studio = 3
      <DescriptionAttribute("Other")> Other = 4
    End Enum

    Public Enum SmsBatchTemplate As Integer
      <DescriptionAttribute("Schedule - Production Services")> Schedule_ProductionServices = 1
      <DescriptionAttribute("Schedule - Playout Operations")> Schedule_PlayoutOperations = 2
      <DescriptionAttribute("Schedule - OB")> Schedule_OB = 3
      <DescriptionAttribute("Schedule - ICR")> Schedule_ICR = 4
      <DescriptionAttribute("Schedule - Content Studio")> Schedule_ContentStudio = 5
      <DescriptionAttribute("Schedule - Maximo")> Schedule_Maximo = 6
    End Enum

    Public Enum EmailBatchTemplate As Integer
      <DescriptionAttribute("Schedule - Production Services")> Schedule_ProductionServices = 1
      <DescriptionAttribute("Schedule - Playout Operations")> Schedule_PlayoutOperations = 2
      <DescriptionAttribute("Schedule - Production Content")> Schedule_ProductionContent = 3
    End Enum

    Public Enum ImageType As Integer
      <DescriptionAttribute("Profile Photo - Large")> ProfilePhotoLarge = 1
      <DescriptionAttribute("Profile Photo - Small")> ProfilePhotoSmall = 2
      <DescriptionAttribute("Accreditation Photo")> AccreditationPhoto = 3
      <DescriptionAttribute("Profile Photo - Medium")> ProfilePhotoMedium = 4
      <DescriptionAttribute("Internal Staff Photo")> InternalStaffPhoto = 5
    End Enum

    Public Enum DefaultViewType As Integer
      <DescriptionAttribute("Today")> Today = 1
      <DescriptionAttribute("3 Days")> ThreeDays = 2
      <DescriptionAttribute("Current Week")> CurrentWeek = 3
      <DescriptionAttribute("Current Month")> CurrentMonth = 4
    End Enum

    Public Enum FeedbackReportSubSectionType As Integer
      <DescriptionAttribute("Free Text")> FreeText = 1
      <DescriptionAttribute("Drop Down")> DropDown = 2
      <DescriptionAttribute("Yes Or No")> YesOrNo = 3
      <DescriptionAttribute("Comments Only")> CommentsOnly = 4
    End Enum

    Public Enum FeedbackReportFlagType As Integer
      <DescriptionAttribute("Green Flag")> GreenFlag = 1
      <DescriptionAttribute("Orange Flag")> OrangeFlag = 2
      <DescriptionAttribute("Red Flag")> RedFlag = 3
      <DescriptionAttribute("No Flag")> NoFlag = 4
    End Enum

    Public Enum ResourceBookingChangeType As Integer
      <DescriptionAttribute("Insert")> Insert = 1
      <DescriptionAttribute("Update")> Update = 2
      <DescriptionAttribute("Delete")> Delete = 3
      <DescriptionAttribute("IntoEdit")> IntoEdit = 4
      <DescriptionAttribute("OutOfEdit")> OutOfEdit = 4
    End Enum

    Public Enum ProductionRoomScheduleTemplate As Integer
      <DescriptionAttribute("Blitz AM")> BlitzAM = 1
      <DescriptionAttribute("Blitz PM")> BlitzPM = 2
      <DescriptionAttribute("Carte Blanche")> CarteBlanche = 3
      <DescriptionAttribute("Blank")> Blank = 4
    End Enum

    Public Enum HRStatsMode As Integer
      <DescriptionAttribute("TimesheetMonth")> TimesheetMonth = 1
      <DescriptionAttribute("NoRules")> NoRules = 2
      <DescriptionAttribute("Shift and Team based")> ShiftAndTeamBased = 3
    End Enum

    Public Enum SecurityGroup As Integer
      <DescriptionAttribute("ICRManager")> ICRManager = 19
    End Enum

  End Class

  'Shared Sub New()

  '  mLists = New OBCachedLists()

  'End Sub

  'Public Overloads Shared ReadOnly Property Lists() As OBCachedLists
  '  Get
  '    Return mLists
  '  End Get
  'End Property

  'Public Overloads Shared Sub Refresh()

  '  mLists = New OBCachedLists

  'End Sub

  Public Shared ReadOnly Property TimelineReadyEnabled
    Get
      If OBLib.CommonData.Lists.MiscList.Count = 1 Then
        Return OBLib.CommonData.Lists.MiscList(0).OBCityTimelineReadyEnabled
      End If
      Return False
    End Get
  End Property

  Public Shared ReadOnly Property Misc As OBLib.Maintenance.Misc
    Get
      If OBLib.CommonData.Lists.MiscList.Count = 1 Then
        Return OBLib.CommonData.Lists.MiscList(0)
      End If
      Return Nothing
    End Get
  End Property

End Class
