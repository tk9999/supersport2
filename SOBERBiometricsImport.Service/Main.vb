﻿Imports Singular.Service.Scheduling
Imports Singular.Service
Imports OBLib.Biometrics
Imports OBLib
Imports System.Data.SqlClient

Public Class Main
  Inherits System.ServiceProcess.ServiceBase

  Private mAnvizImporter As AnvizImport
  Private mCentralSecurityImport As CentralSecurityImport



  Protected ReadOnly Property ServiceDescription As String
    Get
      Return "SOBER Biometrics Scheduler"
    End Get
  End Property

  Protected ReadOnly Property VersionNo As String
    Get
      Return "1.0.0.0.0"
    End Get
  End Property

  Private mServiceLogList As New ServiceLogList

  Protected Overrides Sub OnStart(ByVal args() As String)

    ' Write Progress  
    SchedulerServiceEventLog.WriteEntry("Attempting to Start Biometrics Scheduler Service...")
     

    Dim ConnectionString As String = ""
    Dim StartTime = Singular.Dates.Parse("01 Jan 2000 00:00", "dd MMM yyyy HH:mm")
    Dim EndTime = Singular.Dates.Parse("01 Jan 3000 23:59", "dd MMM yyyy HH:mm")
    Dim EveryXTimeMeasure As Integer = 3
    Dim FrequencyType As DailyFrequencyType = DailyFrequencyType.Every

    If Singular.Debug.InDebugMode Then

      ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("Obsql").ConnectionString
    Else
      ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("SOBER").ConnectionString
      StartTime = Singular.Dates.Parse("01 Jan 2000 " & System.Configuration.ConfigurationManager.AppSettings("StartTime"), "dd MMM yyyy HH:mm")
      EndTime = Singular.Dates.Parse("01 Jan 3000 " & System.Configuration.ConfigurationManager.AppSettings("EndTime"), "dd MMM yyyy HH:mm")
      EveryXTimeMeasure = CInt(System.Configuration.ConfigurationManager.AppSettings("EveryXMinutes"))
      FrequencyType = CInt(System.Configuration.ConfigurationManager.AppSettings("FrequencyType"))
    End If

    Singular.Settings.SetConnectionString(ConnectionString)


    ' create a bit of a delay to enable one to attach the debugger
    Dim tmr As New Timers.Timer(10000)
    tmr.Start()
    AddHandler tmr.Elapsed, Sub(o, e)

                              tmr.Stop()

                              Dim schedule As New Schedule() With {.OccursType = OccursType.Daily,
                                                                  .DailyFrequencyType = DailyFrequencyType.Once}

                              schedule.OccursType = OccursType.Daily
                              schedule.OccursDaily = New OccursDaily() With {.Type = OccursType.Daily,
                                                                             .DayInterval = 1}

                              schedule.DailyFrequencyType = FrequencyType

                              If FrequencyType = DailyFrequencyType.Once Then
                                schedule.DailyFrequencyOnce = New DailyFrequencyOnce() With {
                                                                    .AtTime = StartTime,
                                                                    .Type = DailyFrequencyType.Once
                                                                }
                              Else
                                schedule.DailyFrequencyEvery = New DailyFrequencyEvery() With {
                                                          .StartTime = StartTime,
                                                          .EndTime = EndTime,
                                                          .Type = DailyFrequencyType.Every,
                                                          .TimeMeasure = TimeMeasure.Minute,
                                                          .Unit = EveryXTimeMeasure
                                                      }
                              End If
                              ' AddServerPrograms()
                              Try
                                Dim timer As New ScheduleTimer(schedule, AddressOf ImportData)
                                timer.Start()

                                SchedulerServiceEventLog.WriteEntry(String.Format("Time schedule started Successfully"))
                              Catch ex As Exception
                                SchedulerServiceEventLog.WriteEntry(String.Format("Error Start : {0}", ex.Message))
                              End Try


                            End Sub


  End Sub
   
  Public ReadOnly Property EventSource() As String
    Get
      Return "SOBER Biometrics Import"
    End Get
  End Property
 
   
  Private Sub ImportData()

    SchedulerServiceEventLog.WriteEntry("Time up...")
    If CBool(System.Configuration.ConfigurationManager.AppSettings("AnvizActiveInd")) Then
      SchedulerServiceEventLog.WriteEntry("Starting Anviz Import")
      Try
        If mAnvizImporter Is Nothing Then
          mAnvizImporter = New AnvizImport()
        End If
        mAnvizImporter.TimeUp()
      Catch ex As Exception
        SchedulerServiceEventLog.WriteEntry(String.Format("Error During Anviz Import: {0}", ex.Message))
      End Try
    Else

      SchedulerServiceEventLog.WriteEntry("Anviz not run because marked as inactive")
       
    End If


    If CBool(System.Configuration.ConfigurationManager.AppSettings("CentralSecurityActiveInd")) Then
      SchedulerServiceEventLog.WriteEntry("Starting Central Security Import")
      Try
        If mCentralSecurityImport Is Nothing Then
          mCentralSecurityImport = New CentralSecurityImport()
        End If
        mCentralSecurityImport.TimeUp()
      Catch ex As Exception
        SchedulerServiceEventLog.WriteEntry(String.Format("Error During Central Security Import: {0}", ex.Message))
      End Try
    Else
     
        SchedulerServiceEventLog.WriteEntry("Central Security import not run because marked as inactive")
       
    End If

    ' Sync the New data to Shifts
    Try
      SyncData()
    Catch ex As Exception
      SchedulerServiceEventLog.WriteEntry(String.Format("Error on Data Sync : {0}", ex.Message))
      Exit Sub
    End Try

  End Sub

  Public Sub SyncData()

    Using cn As New SqlConnection(Singular.Settings.ConnectionString)
      cn.Open()
      'Run matching procs

      If CBool(System.Configuration.ConfigurationManager.AppSettings("CentralSecurityActiveInd")) OrElse _
        CBool(System.Configuration.ConfigurationManager.AppSettings("AnvizActiveInd")) Then
        Try
          Dim cmdSync As New SqlCommand("[CmdProcs].[cmdAccessDataSync]", cn)
          cmdSync.CommandTimeout = 240
          cmdSync.ExecuteNonQuery()
        Catch ex As Exception
          mCentralSecurityImport.AddProgress("Data Sync:" & ex.Message)
        End Try
      End If
      If CBool(System.Configuration.ConfigurationManager.AppSettings("CentralSecurityActiveInd")) Then
        Try
          Dim cmdShifts As New SqlCommand("[CmdProcs].[cmdAccessShiftSync] '" & mCentralSecurityImport.MinAccessDateTime.ToString("yyyy-MM-dd") & "','" & New Csla.SmartDate(Now.AddDays(1)).ToString("yyyy-MM-dd") & "'", cn)
          cmdShifts.CommandTimeout = 240
          cmdShifts.ExecuteNonQuery()


        Catch ex As Exception
          mCentralSecurityImport.AddProgress("Shift Sync:" & ex.Message)
        End Try
      End If
    End Using

  End Sub
   
End Class
