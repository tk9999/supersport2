﻿Imports System.Data.OleDb 
Imports OBLib.Biometrics
Public Class CentralSecurityImport 

  Public Sub New()

  End Sub

  Public ReadOnly Property Name()
    Get
      Return "Central Security Import"
    End Get
  End Property

  Public ReadOnly Property MinAccessDateTime() As Csla.SmartDate
    Get
      Return ImpronetInterface.MinAccessDateTime
    End Get
  End Property

  Private mServiceLogList As New ServiceLogList

  Public Sub AddProgress(ByVal Text As String)
    WriteServiceLogProgress(Text)
  End Sub

  Public Sub WriteServiceLogProgress(Progress As String)
    mServiceLogList.WriteLogProgress(Name, Progress)
  End Sub



  Public Sub TimeUp()

    Dim ConnectionString As String
    Try
      AddProgress("Reading Central Security logs: ")

      If Singular.Debug.InDebugMode Then
        ConnectionString = "server=Singularjhbdev1\SQL08;Integrated Security=false;uid=SS_Webuser;pwd=Singular1;database=Impronet"
      Else
        ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("Impronet").ConnectionString
      End If
      Try
        ImpronetInterface.GetData(ConnectionString)
      Catch ex As Exception
        AddProgress(String.Format("Error reading logs from Impronet Database: {0}", ex.Message))
        Throw New Exception(ex.Message)
        Exit Sub
      End Try

      AddProgress("Impronet logs are Imported Successfully.")
    Catch ex As Exception
      AddProgress("Impronet failed: " & ex.Message)
    End Try


  End Sub


   
End Class
