﻿Imports System.Data.OleDb
Imports OBLib
Imports OBLib.Biometrics
Public Class AnvizImport


  Public Sub New() 
  End Sub

  Public ReadOnly Property Name()
    Get
      Return "Anviz Import Service"
    End Get
  End Property

  Private mServiceLogList As New ServiceLogList

  Public Sub AddProgress(ByVal Text As String)

    WriteServiceLogProgress(Text)
  End Sub

  Private Sub WriteServiceLogProgress(Progress As String)
    mServiceLogList.WriteLogProgress(Name, Progress)
  End Sub



  Protected Function StartSchedule() As Boolean
    AddProgress("Anviz Import Service started")
    Return True
  End Function



  Public Sub TimeUp()
    Dim MiscList = MiscBiometricList.GetMiscBiometricList
    If MiscList.Count > 0 Then
      Dim AnvizLastLogID As Integer = MiscList(0).AnvizLastLogID
      Dim Accesspath = MiscList(0).AnvizAccessPath

      Try
        AddProgress("Reading Check Ins And outs From log: " & AnvizLastLogID.ToString)
        Dim List As AccessStagingAnvizList = Nothing

        ' reads from access db
        Try
          List = GetStagingList(Accesspath, AnvizLastLogID)
        Catch ex As Exception
          AddProgress(String.Format("Error reading list from Anviz Database: {0}", ex.Message))
          Throw ex
        End Try

        'saves to Sober Db
        Try
          List.Save()
        Catch ex As Exception
          AddProgress(String.Format("Error saving list to SOBER Database: {0}", Singular.Debug.RecurseExceptionMessage(ex)))
          Throw ex
        End Try
        If List.Count > 0 Then
          AddProgress(String.Format("{0} log{1} were Imported Successfully.", List.Count, IIf(List.Count > 1, "s", "")))

        End If
      Catch ex As Exception
        AddProgress("Anviz Import failed: " & ex.Message)
        Throw ex
      End Try
    End If

  End Sub


  Private Function GetStagingList(OrigAccesspath As String, AnvizLastLogID As Integer) As AccessStagingAnvizList


    Dim file As New System.IO.FileInfo(OrigAccesspath)
    Dim Accesspath As String = OrigAccesspath.Replace(".mdb", "temp.mdb")

    file.CopyTo(Accesspath, True)

    '1. get new transactions with no access Employee records
    Try
      Dim HrNoIDs As String = AccessStagingAnvizList.GetAHRIsToString(Accesspath)



      '2. get HR Names from SOBER and insert the missing IDs and names into access DB
      AccessStagingAnvizList.GetIdAndNameToInsert(HrNoIDs, Accesspath)
    Catch

    End Try



    Dim List As AccessStagingAnvizList = AccessStagingAnvizList.GetAccessStagingAnvizList(Accesspath, AnvizLastLogID)
    Dim tempfile As New System.IO.FileInfo(Accesspath)
    tempfile.Delete()
    Return List

  End Function

  Public Shared Function AccessConnectionString(ByVal Path As String, Optional ByVal Password As String = "") As String
    If Path.ToLower.EndsWith(".accdb") Then
      ' Microsoft.ACE.OLEDB.12.0
      Return "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Path & ";" & IIf(Password = "", "", "Database Password=" & Password)
    Else
      Return "Provider=Microsoft.Jet.OLEDB.4.0;Data source=" & Path & ";Jet OLEDB:Database Password=" & Password
    End If
  End Function

End Class
