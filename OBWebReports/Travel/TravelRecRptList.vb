﻿Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance

  <Serializable()> _
  Public Class TravelRecRptList
    Inherits SingularReadOnlyListBase(Of TravelRecRptList, TravelRecRpt)

    Public Property ProductionID As Integer

#Region " Business Methods "

    Public Function GetBlankPreTravel(ByVal HumanResourceID As Integer) As TravelRecRpt

      For Each child As TravelRecRpt In Me.Where(Function(f) f.HumanResourceID = HumanResourceID And f.HasPreTravel = False)
        Return child
      Next
      Return Nothing

    End Function

    Public Function GetBlankPostTravel(ByVal HumanResourceID As Integer, CancelledDateTimePost_IsEmpty As Boolean, VersionNoPost As Integer) As TravelRecRpt

      For Each child As TravelRecRpt In Me.Where(Function(f) f.HasPostTravel = False And
                                                   f.HumanResourceID = HumanResourceID And
                                                   f.CancelledDateTimePre.IsEmpty = CancelledDateTimePost_IsEmpty And
                                                   If(f.CancelledDateTimePre.IsEmpty, 0, f.VersionNoPre) = If(CancelledDateTimePost_IsEmpty, 0, VersionNoPost))
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Function GetBlankAccommodation(ByVal HumanResourceID As Integer) As TravelRecRpt

      For Each child As TravelRecRpt In Me.Where(Function(f) f.HumanResourceID = HumanResourceID And
                                                   (f.HasTravel = False Or f.TravelIsCancelled = False) And
                                                   f.HasAccommodation = False).OrderBy(Function(f) If(f.PreTravelStartDateTime.IsEmpty, f.PostTravelStartDateTime, f.PreTravelStartDateTime))
        Return child
      Next
      Return Nothing

    End Function

    Public Function GetItem(ByVal HumanResourceID As Integer) As TravelRecRpt

      For Each child As TravelRecRpt In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Protected Overrides Function AddNewCore() As Object

      Dim obj As TravelRecRpt = TravelRecRpt.NewTravelRecRpt
      Me.Add(obj)
      Return obj

    End Function

    Public Overrides Function ToString() As String

      Return "TravelRecRptList"

    End Function

    Public Function GetDatasetForReport() As DataTable

      Dim dt As New DataTable("Table1")

      dt.Columns.Add("HumanResource", GetType(System.String))
      dt.Columns.Add("HumanResourceID", GetType(System.Int32))
      dt.Columns.Add("ProductionID", GetType(System.Int32))
      dt.Columns.Add("PreTravelAirport", GetType(System.String))
      dt.Columns.Add("PreTravelDepartureTime", GetType(System.TimeSpan))
      dt.Columns.Add("PreTravelArrivalTime", GetType(System.TimeSpan))
      dt.Columns.Add("PreTravelDepartureDate", GetType(System.DateTime))
      dt.Columns.Add("SR", GetType(System.String))
      dt.Columns.Add("PostTravelAirport", GetType(System.String))
      dt.Columns.Add("PostTravelDepartureTime", GetType(System.TimeSpan))
      dt.Columns.Add("PostTravelArrivalTime", GetType(System.TimeSpan))
      dt.Columns.Add("PostTravelDepartureDate", GetType(System.DateTime))
      dt.Columns.Add("InvoiceAmount", GetType(System.Decimal))
      dt.Columns.Add("ServiceFee", GetType(System.Decimal))
      dt.Columns.Add("InvoiceNumber", GetType(System.String))
      dt.Columns.Add("FlightDatePaid", GetType(System.DateTime))
      dt.Columns.Add("CancelledDateTime", GetType(System.DateTime))
      dt.Columns.Add("VersionNo", GetType(System.Int32))
      dt.Columns.Add("HasPreTravelFlight", GetType(System.Boolean))
      dt.Columns.Add("HasPostTravelFlight", GetType(System.Boolean))
      dt.Columns.Add("DuplicateInd", GetType(System.Boolean))
      dt.Columns.Add("AccommodationStartDate", GetType(System.DateTime))
      dt.Columns.Add("AccommodationEndDate", GetType(System.DateTime))
      dt.Columns.Add("AccommodationProvider", GetType(System.String))
      dt.Columns.Add("SingleDouble", GetType(System.String))
      dt.Columns.Add("BandB", GetType(System.String))
      dt.Columns.Add("BOnly", GetType(System.String))
      dt.Columns.Add("InvAmount", GetType(System.Decimal))
      dt.Columns.Add("InvNumber", GetType(System.String))
      dt.Columns.Add("AccommodationDatePaid", GetType(System.DateTime))
      dt.Columns.Add("CrewTypeID", GetType(System.Int32))
      dt.Columns.Add("IsEventManager", GetType(System.Boolean))
      dt.Columns.Add("SortOrder", GetType(System.Int32))
      dt.Columns.Add("CrewType", GetType(System.String))
      dt.Columns.Add("AccommodationCancelledDateTime", GetType(System.DateTime))

      Me.OrderBy(Function(f) f.SortOrder).ToList.ForEach(Sub(s)
                                                           dt.Rows.Add(s.HumanResource, s.HumanResourceID, s.ProductionID, s.PreTravelAirport, s.PreTravelDepartureTime, s.PreTravelArrivalTime,
                                                                       If(s.PreTravelStartDateTime.IsEmpty, DBNull.Value, s.PreTravelStartDateTime.Date),
                                                                       s.SR, s.PostTravelAirport, s.PostTravelDepartureTime, s.PostTravelArrivalTime,
                                                                       If(s.PostTravelStartDateTime.IsEmpty, DBNull.Value, s.PostTravelStartDateTime.Date),
                                                                       s.InvoiceAmount, s.ServiceFee, s.InvoiceNumber, DBNull.Value,
                                                                       If(s.CancelledDateTimePre.IsEmpty, If(s.CancelledDateTimePost.IsEmpty, DBNull.Value, s.CancelledDateTimePost.Date), s.CancelledDateTimePre.Date),
                                                                       s.VersionNo, s.HasPreTravel, s.HasPostTravel, False,
                                                                       If(s.AccommodationStartDate.IsEmpty, DBNull.Value, s.AccommodationStartDate.Date),
                                                                       If(s.AccommodationEndDate.IsEmpty, DBNull.Value, s.AccommodationEndDate.Date),
                                                                       s.AccommodationProvider, s.SingleDouble, s.BandB, s.BOnly, s.InvAmount, s.InvNumber,
                                                                       If(s.AccommodationDatePaid.IsEmpty, DBNull.Value, s.AccommodationDatePaid.Date),
                                                                       s.CrewTypeID, s.IsEventManager, s.SortOrder, s.CrewType,
                                                                       If(s.AccommodationCancelledDateTime.IsEmpty, DBNull.Value, s.AccommodationCancelledDateTime.Date))
                                                         End Sub)

      Return dt

    End Function

    Public Overloads Sub Add(ByVal Child As TravelRecRpt)

      Me.IsReadOnly = False

      Dim otherObj = Me.Where(Function(f) f.HumanResourceID = Child.HumanResourceID).FirstOrDefault
      If otherObj IsNot Nothing Then
        Child.CrewTypeID = otherObj.CrewTypeID
        Child.CrewType = otherObj.CrewType
        Child.IsEventManager = otherObj.IsEventManager
      End If
      Child.ProductionID = Me.ProductionID
      MyBase.Add(Child)
      Me.IsReadOnly = True

    End Sub

#End Region

#Region " Factory Methods "

    Public Shared Function NewTravelRecRptList(ProductionID As Integer) As TravelRecRptList

      Return New TravelRecRptList(ProductionID)

    End Function

    Private Sub New(ProductionID As Integer)

      Me.ProductionID = ProductionID

    End Sub

#End Region

  End Class

End Namespace