﻿Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Travel.New

#Region " Travel Rec - Main Class "

  <Serializable()> _
  Public Class ROTravelRecNew
    Inherits SingularBusinessBase(Of ROTravelRecNew)

#Region " Properties and Methods "

#Region " Properties "

    Public Property TravelRequisitionID As Integer
    Public Property ROTravelHRIDNewList As ROTravelHRIDNewList = ROTravelHRIDNewList.NewROTravelHRIDNewList()
    Public Property ROTravelPreNewList As ROTravelPreNewList = ROTravelPreNewList.NewROTravelPreNewList()
    Public Property ROTravelPostNewList As ROTravelPostNewList = ROTravelPostNewList.NewROTravelPostNewList()
    Public Property ROTravelAccNewList As ROTravelAccNewList = ROTravelAccNewList.NewROTravelAccNewList()

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return TravelRequisitionID

    End Function

    Public Overrides Function ToString() As String

      Return TravelRequisitionID

    End Function

#End Region

#End Region

#Region " Factory Methods "

    Public Shared Function GetROTravelRecNew(TravelRequisitionID As Integer?, ProductionID As Integer?) As ROTravelRecNew

      Return DataPortal.Fetch(Of ROTravelRecNew)(New Criteria(TravelRequisitionID, ProductionID))

    End Function

    Private Sub New()

    End Sub

#End Region

#Region " Data Access "

    <Serializable()> _
    Private Class Criteria

      Public Property TravelRequisitionID As Integer? = Nothing
      Public Property ProductionID As Integer? = Nothing

      Public Sub New(TravelRequisitionID As Integer?, ProductionID As Integer?)

        Me.TravelRequisitionID = TravelRequisitionID
        Me.ProductionID = ProductionID

      End Sub

    End Class

    Private Sub Fetch(ByVal sdr As SafeDataReader)

      Me.ROTravelHRIDNewList.Fetch(sdr)

      If sdr.NextResult Then
        Me.ROTravelPreNewList.Fetch(sdr)
      End If

      If sdr.NextResult Then
        Me.ROTravelPostNewList.Fetch(sdr)
      End If

      If sdr.NextResult Then
        Me.ROTravelAccNewList.Fetch(sdr)
      End If

    End Sub

    Protected Sub DataPortal_Fetch(ByVal criteria As Object)

      Dim crit As Criteria = criteria
      Me.TravelRequisitionID = If(crit.TravelRequisitionID Is Nothing, 0, crit.TravelRequisitionID)
      Using cn As New SqlClient.SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Using cm As SqlClient.SqlCommand = cn.CreateCommand
          cm.CommandType = CommandType.StoredProcedure
          cm.CommandText = "GetProcs.getROTravelRec"
          cm.Parameters.AddWithValue("@TravelRequisitionID", crit.TravelRequisitionID)
          cm.Parameters.AddWithValue("@ProductionID", crit.ProductionID)
          cm.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)

          Using sdr As New SafeDataReader(cm.ExecuteReader)
            Fetch(sdr)
          End Using
        End Using
      End Using

    End Sub

#End Region


  End Class

#End Region

#Region " Human Resources "

  <Serializable()> _
  Public Class ROTravelHRIDNew
    Inherits Singular.SingularReadOnlyBase(Of ROTravelHRIDNew)

#Region " Properties and Methods "

#Region " Properties "

    Public Property HumanResourceID As Integer
    Public Property CrewTypeID As Integer
    Public Property CrewType As String
    Public Property IsEventManager As Boolean
    Public Property HumanResource As String

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return HumanResourceID

    End Function

    Public Overrides Function ToString() As String

      Return HumanResourceID

    End Function

#End Region

#End Region

#Region " Factory Methods "

    Friend Shared Function GetROTravelHRIDNew(ByVal dr As SafeDataReader) As ROTravelHRIDNew

      Return New ROTravelHRIDNew(dr)

    End Function

    Private Sub New(ByVal dr As SafeDataReader)

      Fetch(dr)

    End Sub

#End Region

#Region " Data Access "

    Protected Sub Fetch(ByRef sdr As SafeDataReader)

      With sdr
        HumanResourceID = .GetInt32(0)
        CrewTypeID = .GetInt32(1)
        CrewType = .GetString(2)
        IsEventManager = .GetBoolean(3)
        HumanResource = .GetString(4)
      End With

    End Sub

#End Region

  End Class

  <Serializable()> _
  Public Class ROTravelHRIDNewList
    Inherits Singular.SingularReadOnlyListBase(Of ROTravelHRIDNewList, ROTravelHRIDNew)

#Region " Business Methods "

    Public Function GetItem(ByVal HumanResourceID As Integer) As ROTravelHRIDNew

      For Each child As ROTravelHRIDNew In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Factory Methods "

    Public Shared Function NewROTravelHRIDNewList() As ROTravelHRIDNewList

      Return New ROTravelHRIDNewList()

    End Function

    Private Sub New()

    End Sub

#End Region

#Region " Data Access "

    Friend Sub Fetch(ByVal sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ROTravelHRIDNew.GetROTravelHRIDNew(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

#End Region

  End Class

#End Region

#Region " Pre Travel "

  <Serializable()> _
  Public Class ROTravelPreNew
    Inherits Singular.SingularReadOnlyBase(Of ROTravelPreNew)

#Region " Properties and Methods "

#Region " Properties "

    Public Property HumanResourceID As Integer
    Public Property PreTravelDepartureTime As TimeSpan
    Public Property PreTravelArrivalTime As TimeSpan
    Public Property TravelStartDateTime As New SmartDate
    Public Property TravelEndDateTime As New SmartDate
    Public Property ProductionCrewTravelID As Integer
    Public Property VersionNo As Integer
    Public Property TicketPrice As Decimal
    Public Property RefNo As String
    Public Property CancelledDateTime As New SmartDate
    Public Property LatestPreInd As Boolean
    Public Property PreTravelAirport As String
    Public Property InvoiceAmount As Decimal
    Public Property ServiceFee As Decimal
    Public Property InvoiceNumber As Decimal
    Public Property SortOrder As Integer
    Public Property HumanResource As String

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return HumanResourceID

    End Function

    Public Overrides Function ToString() As String

      Return HumanResourceID

    End Function

#End Region

#End Region

#Region " Factory Methods "

    Friend Shared Function GetROTravelPreNew(ByVal dr As SafeDataReader) As ROTravelPreNew

      Return New ROTravelPreNew(dr)

    End Function

    Private Sub New(ByVal dr As SafeDataReader)

      Fetch(dr)

    End Sub

#End Region

#Region " Data Access "

    Protected Sub Fetch(ByRef sdr As SafeDataReader)

      With sdr
        HumanResourceID = .GetInt32(0)
        PreTravelDepartureTime = .GetValue(1)
        PreTravelArrivalTime = .GetValue(2)
        TravelStartDateTime = .GetSmartDate(3)
        TravelEndDateTime = .GetSmartDate(4)
        ProductionCrewTravelID = .GetInt32(5)
        VersionNo = .GetInt32(6)
        TicketPrice = .GetDecimal(7)
        ServiceFee = .GetDecimal(8)
        RefNo = .GetString(9)
        CancelledDateTime = .GetSmartDate(10)
        LatestPreInd = .GetBoolean(11)
        PreTravelAirport = .GetString(12)
        SortOrder = .GetInt32(13)
        HumanResource = .GetString(15)
      End With

    End Sub

#End Region

  End Class

  <Serializable()> _
  Public Class ROTravelPreNewList
    Inherits Singular.SingularReadOnlyListBase(Of ROTravelPreNewList, ROTravelPreNew)

#Region " Business Methods "

    Public Function GetItem(ByVal HumanResourceID As Integer) As ROTravelPreNew

      For Each child As ROTravelPreNew In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Factory Methods "

    Public Shared Function NewROTravelPreNewList() As ROTravelPreNewList

      Return New ROTravelPreNewList()

    End Function

    Private Sub New()

    End Sub

#End Region

#Region " Data Access "

    Friend Sub Fetch(ByVal sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ROTravelPreNew.GetROTravelPreNew(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

#End Region

  End Class

#End Region

#Region " Post Travel "

  <Serializable()> _
  Public Class ROTravelPostNew
    Inherits Singular.SingularReadOnlyBase(Of ROTravelPostNew)

#Region " Properties and Methods "

#Region " Properties "

    Public Property HumanResourceID As Integer
    Public Property PostTravelDepartureTime As TimeSpan
    Public Property ArrivalTime As TimeSpan
    Public Property TravelStartDateTime As New SmartDate
    Public Property TravelEndDateTime As New SmartDate
    Public Property ProductionCrewTravelID As Integer
    Public Property VersionNo As Integer
    Public Property TicketPrice As Decimal
    Public Property RefNo As String
    Public Property CancelledDateTime As New SmartDate
    Public Property LatestPostInd As Boolean
    Public Property PostTravelAirport As String
    Public Property InvoiceAmount As Decimal
    Public Property ServiceFee As Decimal
    Public Property InvoiceNumber As Decimal
    Public Property SortOrder As Integer
    Public Property HumanResource As String

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return HumanResourceID

    End Function

    Public Overrides Function ToString() As String

      Return HumanResourceID

    End Function

#End Region

#End Region

#Region " Factory Methods "

    Friend Shared Function GetROTravelPostNew(ByVal dr As SafeDataReader) As ROTravelPostNew

      Return New ROTravelPostNew(dr)

    End Function

    Private Sub New(ByVal dr As SafeDataReader)

      Fetch(dr)

    End Sub

#End Region

#Region " Data Access "

    Protected Sub Fetch(ByRef sdr As SafeDataReader)

      With sdr
        HumanResourceID = .GetInt32(0)
        PostTravelDepartureTime = .GetValue(1)
        ArrivalTime = .GetValue(2)
        TravelStartDateTime = .GetSmartDate(3)
        TravelEndDateTime = .GetSmartDate(4)
        ProductionCrewTravelID = .GetInt32(5)
        VersionNo = .GetInt32(6)
        TicketPrice = .GetDecimal(7)
        ServiceFee = .GetDecimal(8)
        RefNo = .GetString(9)
        CancelledDateTime = .GetSmartDate(10)
        LatestPostInd = .GetBoolean(11)
        PostTravelAirport = .GetString(12)
        SortOrder = .GetInt32(13)
        HumanResource = .GetString(15)
      End With

    End Sub

#End Region

  End Class

  <Serializable()> _
  Public Class ROTravelPostNewList
    Inherits Singular.SingularReadOnlyListBase(Of ROTravelPostNewList, ROTravelPostNew)

#Region " Business Methods "

    Public Function GetItem(ByVal HumanResourceID As Integer) As ROTravelPostNew

      For Each child As ROTravelPostNew In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Factory Methods "

    Public Shared Function NewROTravelPostNewList() As ROTravelPostNewList

      Return New ROTravelPostNewList()

    End Function

    Private Sub New()

    End Sub

#End Region

#Region " Data Access "

    Friend Sub Fetch(ByVal sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ROTravelPostNew.GetROTravelPostNew(sdr))
      End While
      Me.RaiseListChangedEvents = True


    End Sub

#End Region

  End Class

#End Region

#Region " Accommodation "

  <Serializable()> _
  Public Class ROTravelAccNew
    Inherits Singular.SingularReadOnlyBase(Of ROTravelAccNew)

#Region " Properties and Methods "

#Region " Properties "

    Public Property ProductionID As Integer
    Public Property HumanResource As String
    Public Property AccommodationStartDate As New SmartDate
    Public Property AccommodationEndDate As New SmartDate
    Public Property AccommodationProvider As String
    Public Property SingleDouble As String
    Public Property BandB As String
    Public Property BOnly As String
    Public Property InvAmount As Decimal
    Public Property InvNumber As String
    Public Property DatePaid As New SmartDate
    Public Property HumanResourceID As Integer
    Public Property SupplierHumanResourceID As Integer
    Public Property AccommodationCancelledDateTime As New SmartDate

#End Region

#Region " Methods "

    Protected Overrides Function GetIdValue() As Object

      Return HumanResourceID

    End Function

    Public Overrides Function ToString() As String

      Return HumanResourceID

    End Function

#End Region

#End Region

#Region " Factory Methods "

    Friend Shared Function GetROTravelAccNew(ByVal dr As SafeDataReader) As ROTravelAccNew

      Return New ROTravelAccNew(dr)

    End Function

    Private Sub New(ByVal dr As SafeDataReader)

      Fetch(dr)

    End Sub

#End Region

#Region " Data Access "

    Protected Sub Fetch(ByRef sdr As SafeDataReader)

      With sdr
        ProductionID = .GetInt32(0)
        HumanResource = .GetString(1)
        AccommodationStartDate = .GetSmartDate(2)
        AccommodationEndDate = .GetSmartDate(3)
        AccommodationProvider = .GetString(4)
        SingleDouble = .GetString(5)
        BandB = .GetString(6)
        BOnly = .GetString(7)
        InvAmount = .GetDecimal(8)
        InvNumber = .GetString(9)
        DatePaid = .GetSmartDate(10)
        HumanResourceID = .GetInt32(11)
        SupplierHumanResourceID = .GetInt32(12)
        AccommodationCancelledDateTime = .GetSmartDate(13)
      End With

    End Sub

#End Region

  End Class

  <Serializable()> _
  Public Class ROTravelAccNewList
    Inherits Singular.SingularReadOnlyListBase(Of ROTravelAccNewList, ROTravelAccNew)

#Region " Business Methods "

    Public Function GetItem(ByVal HumanResourceID As Integer) As ROTravelAccNew

      For Each child As ROTravelAccNew In Me
        If child.HumanResourceID = HumanResourceID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

#End Region

#Region " Factory Methods "

    Public Shared Function NewROTravelAccNewList() As ROTravelAccNewList

      Return New ROTravelAccNewList()

    End Function

    Private Sub New()

    End Sub

#End Region

#Region " Data Access "

    Friend Sub Fetch(ByVal sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ROTravelAccNew.GetROTravelAccNew(sdr))
      End While
      Me.RaiseListChangedEvents = True


    End Sub

#End Region

  End Class

#End Region

End Namespace