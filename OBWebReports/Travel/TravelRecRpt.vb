﻿Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations

#If SILVERLIGHT = False Then
Imports System.Data.SqlClient
#End If
Imports Singular
Imports Singular.Misc

Namespace Maintenance

  <Serializable()> _
  Public Class TravelRecRpt
    Inherits SingularReadOnlyBase(Of TravelRecRpt)

    Public Property HumanResourceID As Integer
    Public Property HumanResource As String

    Public Property HasPreTravel As Boolean = False
    Public Property PreTravelDepartureTime As TimeSpan
    Public Property PreTravelArrivalTime As TimeSpan
    Public Property PreTravelStartDateTime As New SmartDate
    Public Property PreTravelEndDateTime As New SmartDate
    Public Property ProductionCrewTravelIDPre As Integer
    Public Property VersionNoPre As Integer
    'Public Property TicketPricePre As Decimal
    'Public Property ServiceFeePre As Decimal
    Public Property RefNoPre As String
    Public Property CancelledDateTimePre As New SmartDate
    Public Property LatestPreIndPre As Boolean
    Public Property PreTravelAirport As String

    Public Property HasPostTravel As Boolean = False
    Public Property PostTravelDepartureTime As TimeSpan
    Public Property PostTravelArrivalTime As TimeSpan
    Public Property PostTravelStartDateTime As New SmartDate
    Public Property PostTravelEndDateTime As New SmartDate
    Public Property ProductionCrewTravelIDPost As Integer
    Public Property VersionNoPost As Integer
    'Public Property TicketPricePost As Decimal
    'Public Property ServiceFeePost As Decimal
    Public Property RefNoPost As String
    Public Property CancelledDateTimePost As New SmartDate
    Public Property LatestPostIndPost As Boolean
    Public Property PostTravelAirport As String

    'Single/Return
    Public ReadOnly Property SR As String
      Get
        If HasPreTravel And HasPostTravel Then
          Return "R"
        ElseIf HasPreTravel And Not HasPostTravel Or Not HasPreTravel And HasPostTravel Then
          Return "S"
        Else
          Return ""
        End If
      End Get
    End Property

    Public ReadOnly Property VersionNo As String
      Get
        Return If(VersionNoPre = 0, VersionNoPost, VersionNoPre).ToString
      End Get
    End Property

    Public ReadOnly Property CancelledDateTime As DateTime
      Get
        If If(CancelledDateTimePre.IsEmpty, Date.MinValue, CancelledDateTimePre.Date) > If(CancelledDateTimePost.IsEmpty, Date.MinValue, CancelledDateTimePost.Date) Then
          Return CancelledDateTimePre.Date
        Else
          Return CancelledDateTimePost.Date
        End If
      End Get
    End Property

    Public Property InvoiceAmount As Decimal
    Public Property ServiceFee As Decimal
    Public Property InvoiceNumber As Decimal
    Public Property CrewTypeID As Integer
    Public Property CrewType As String
    Public Property IsEventManager As Boolean
    Public Property SortOrder As Integer = Integer.MaxValue

    Public Property HasAccommodation As Boolean = False
    Public Property ProductionID As Integer
    Public Property AccommodationStartDate As New SmartDate
    Public Property AccommodationEndDate As New SmartDate
    Public Property AccommodationProvider As String
    Public Property SingleDouble As String
    Public Property BandB As String
    Public Property BOnly As String
    Public Property InvAmount As Decimal
    Public Property InvNumber As String
    Public Property DatePaid As New SmartDate
    Public Property AccommodationDatePaid As New SmartDate
    Public Property SupplierHumanResourceID As Integer
    Public Property AccommodationCancelledDateTime As New SmartDate

    Public ReadOnly Property HasTravel As Boolean
      Get
        Return HasPreTravel OrElse HasPostTravel
      End Get
    End Property

    Public ReadOnly Property TravelIsCancelled As Boolean
      Get
        Return Not CancelledDateTimePost.IsEmpty OrElse Not CancelledDateTimePre.IsEmpty
      End Get
    End Property

    Protected Overrides Function GetIdValue() As Object
      Return Nothing
    End Function

#Region " Factory Methods "

    Public Shared Function NewTravelRecRpt(HumanResourceID As Integer, SupplierHumanResourceID As Integer, HumanResource As String) As TravelRecRpt

      Dim obj = New TravelRecRpt()
      If Not IsNullNothing(HumanResourceID, True) Then
        obj.HumanResourceID = HumanResourceID
        'Dim ROHR = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(HumanResourceID)
        'If ROHR IsNot Nothing Then
        '  obj.HumanResource = ROHR.Firstname + " " + If(ROHR.SecondName.Trim() = "", "", ROHR.SecondName + " ") + ROHR.Surname
        'End If
        obj.HumanResource = HumanResource
      ElseIf Not IsNullNothing(SupplierHumanResourceID, True) Then
        obj.SupplierHumanResourceID = SupplierHumanResourceID
        obj.HumanResource = HumanResource
      End If

      Return obj

    End Function

    Public Shared Function NewTravelRecRpt() As TravelRecRpt

      Dim obj = New TravelRecRpt()
      Return obj

    End Function

    'Friend Shared Function GetTravelRecRpt(ByVal dr As SafeDataReader) As TravelRecRpt

    '  Return New TravelRecRpt(dr)

    'End Function

    'Private Sub New(ByVal dr As SafeDataReader)

    '  Fetch(dr)

    'End Sub

    Public Sub New()

    End Sub

#End Region

  End Class

End Namespace