﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.HR.ReadOnly
Imports Singular.DataAnnotations

Public Class InvoiceReports

  Public Class MonthlyCreditorInvoices
    Inherits Singular.Reporting.ReportBase(Of Singular.Reporting.DefaultCriteria)

    Public Property StartDate As Date?
    Public Property EndDate As Date?

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Monthly Creditor Invoices"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptMonthlyCreditorInvoices]"
      cmd.Parameters.AddWithValue("@StartDate", NothingDBNull(StartDate))
      cmd.Parameters.AddWithValue("@EndDate", NothingDBNull(EndDate))
    End Sub

  End Class

  Public Class PaymentRunCreditorInvoices
    Inherits Singular.Reporting.ReportBase(Of Singular.Reporting.DefaultCriteria)

    Public Property PaymentRunID As Integer? = Nothing
    Public Property ProductionAreaIDsXML As String = ""

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Monthly Creditor Invoices"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptPaymentRunCreditorInvoices]"
      cmd.Parameters.AddWithValue("@PaymentRunID", NothingDBNull(PaymentRunID))
      cmd.Parameters.AddWithValue("@ProductionAreaIDs", Strings.MakeEmptyDBNull(ProductionAreaIDsXML))
      Dim h As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      If h IsNot Nothing Then
        cmd.Parameters.Remove(h)
      End If
    End Sub

  End Class

#Region "Creditor Invoices"
  Public Class CreditorInvoices
    Inherits ReportBase(Of CreditorInvoicesCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Creditor Invoices"
      End Get
    End Property

    Public Sub New()

    End Sub

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptCreditorInvoices]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(CreditorInvoicesCriteriaControl)
      End Get
    End Property

  End Class

  Public Class CreditorInvoicesCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Human Resource", Order:=3), Required(ErrorMessage:="Human Resource Required")>
    Public Property HumanResourceID As Integer?

    <Display(Name:="System", Order:=1), ClientOnly, Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?

    <Display(Name:="Production Area", Order:=2), ClientOnly, Required(ErrorMessage:="Production Area Required")>
    Public Property ProductionAreaID As Integer?

    <Display(Name:="Read Only Human Resource List"), ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

  End Class

  Public Class CreditorInvoicesCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase
    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of CreditorInvoicesCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 2)
                With .Helpers.FieldSet("Sub-Depts and Areas")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "CreditorInvoicesReport.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                        With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                          .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                          With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                            .Button.AddBinding(KnockoutBindingString.click, "CreditorInvoicesReport.AddProductionArea($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Human Resource Filters")
                    .Attributes("id") = "HumanResourceFilters"
                    .AddClass("FadeHide")
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 8)
                With .Helpers.FieldSet("Human Resources")
                  .Attributes("id") = "HumanResources"
                  .AddClass("FadeHide")
                  With .Helpers.Div
                    .Attributes("id") = "HRDiv"
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "CreditorInvoicesReport.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub
  End Class
#End Region

#Region "Creditor Invoice Details"
  Public Class CreditorInvoiceDetails
    Inherits Singular.Reporting.ReportBase(Of CreditorInvoiceDetailsCriteria)

    'Public Property StartDate As Date?
    'Public Property EndDate As Date?

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Creditor Invoice Details"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptCreditorInvoiceDetails)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "[RptProcs].[rptCreditorInvoiceDetails]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(CreditorInvoiceDetailsCriteriaControl)
      End Get
    End Property

  End Class

  Public Class CreditorInvoiceDetailsCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Human Resources"), ClientOnly>
    Public Property HumanResources As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

    <Display(Name:="Human Resource", Order:=1), ClientOnly>
    Public Property HumanResourceIDs As List(Of Integer)

    <Display(Name:="Human Resource", Order:=3)>
    Public Property HumanResourceID As Integer?

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer?

    <Display(Name:="Production Area", Order:=2), Required(ErrorMessage:="Production Area Required")>
    Public Property ProductionAreaID As Integer?

    <Display(Name:="Read Only Human Resource List"), ClientOnly>
    Public Property ROHumanResourceList As List(Of ROHumanResourceFind) = New List(Of ROHumanResourceFind)

  End Class

  Public Class CreditorInvoiceDetailsCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of CreditorInvoiceDetailsCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 2)
                With .Helpers.FieldSet("Sub-Depts and Areas")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                    With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "CreditorInvoiceDetailsReport.AddSystem($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                      .Button.AddClass("btn-block buttontext")
                    End With
                    With .Helpers.Bootstrap.Row
                      .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                      With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                        With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                          .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                          With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                            .Button.AddBinding(KnockoutBindingString.click, "CreditorInvoiceDetailsReport.AddProductionArea($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.With(Of OBLib.HR.ReadOnly.ROHumanResourceFindList.Criteria)("ViewModel.ROHumanResourceFindListCriteria()")
                With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
                  With .Helpers.FieldSet("Human Resource Filters")
                    .Attributes("id") = "HumanResourceFilters"
                    .AddClass("FadeHide")
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.FirstName, BootstrapEnums.InputSize.Small, , "Firstname")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.Surname, BootstrapEnums.InputSize.Small, , "Surname")
                      End With
                    End With
                    With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                      With .Helpers.Bootstrap.FormControlFor(Function(d) d.PreferredName, BootstrapEnums.InputSize.Small, , "Pref. Name")
                      End With
                    End With
                  End With
                End With
              End With
              With .Helpers.Bootstrap.Column(12, 12, 6, 8)
                With .Helpers.FieldSet("Human Resources")
                  .Attributes("id") = "HumanResources"
                  .AddClass("FadeHide")
                  With .Helpers.Div
                    .Attributes("id") = "HRDiv"
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                        With .Helpers.ForEachTemplate(Of ROHumanResource)(Function(d) d.ROHumanResourceList)
                          With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                            With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "CreditorInvoiceDetailsReport.AddHumanResource($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Firstname + " " + c.Surname)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub
  End Class
#End Region
End Class
