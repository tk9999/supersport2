﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports Singular.Strings
Imports System.ComponentModel
Imports OBLib.Maintenance.ReadOnly
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports OBLib.Maintenance.Equipment.ReadOnly
Imports Singular.DataAnnotations
Imports OBLib
Imports OBLib.Equipment.ReadOnly

Public Class SatOpsReports

#Region "Equipment Bookings Report (Excel)"

  Class EquipmentBookingReportExcel
    Inherits Singular.Reporting.ReportBase(Of EquipmentBookingReportExcelCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Equipment Bookings Report (Excel)"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(EquipmentBookingReportExcelCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      'If Me.ReportCriteria.UpdateReports Then
      '  OBLib.ServiceHelpers.SSWSDHelpers.UpdateSatOpsReports(Me.ReportCriteria.StartDate, Me.ReportCriteria.EndDate)
      'End If
      cmd.CommandText = "[RptProcs].[rptEquipmentBookingReportExcel]"
    End Sub

  End Class

  Class EquipmentBookingReportExcelCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    Public Property UpdateReports As Boolean = False

  End Class

  Public Class EquipmentBookingReportExcelCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of EquipmentBookingReportExcelCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
            'With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
            '  With .Helpers.Bootstrap.StateButtonNew(Function(d As EquipmentBookingReportExcelCriteria) d.UpdateReports, "Update Reports", "Don't Update Reports", , , , , "btn-md")

            '  End With
            'End With
          End With
        End With

      End With
    End Sub

  End Class

#End Region

#Region "Feed Report (Excel)"

  Class FeedReportExcel
    Inherits Singular.Reporting.ReportBase(Of FeedReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Feed Report (Excel)"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(FeedReportCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)

      Try
        Dim SponsorshipService As SSWSD.SSWSDSVCClient
        SponsorshipService = New SSWSD.SSWSDSVCClient
        Dim returnedList As New SSWSD.RetObj_GetBookingItemList
        returnedList = SponsorshipService.GetBookingItemList("94208fee-5615-11e5-a800-005056943beb", "SSWSD_Sober", Me.ReportCriteria.StartDate.ToString("dd-MMM-yyyy"), Me.ReportCriteria.EndDate.ToString("dd-MMM-yyyy"))
        'Dim x As Object = Nothing
      Catch ex As Exception
        OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.FirstNameSurname, "SatOpsReports.vb", "Feed - Report", ex.Message)
      End Try

      cmd.CommandText = "[RptProcs].[rptFeedReportExcel]"
    End Sub

  End Class

  Class FeedReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

  End Class

  Public Class FeedReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of EquipmentBookingReportExcelCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With

      End With
    End Sub

  End Class

#End Region

#Region "ICR Incident Reports"

  Class ICRIncidentReport
    Inherits Singular.Reporting.ReportBase(Of ICRIncidentReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "ICR Incident Reports"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ICRIncidentReportCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      If Me.ReportCriteria.UpdateReports Then
        OBLib.ServiceHelpers.SSWSDHelpers.UpdateSatOpsReportsByDate(Me.ReportCriteria.StartDate, Me.ReportCriteria.EndDate)
      End If
      cmd.CommandText = "[RptProcs].[rptICRIncidentReportExcel]"
    End Sub

  End Class

  Class ICRIncidentReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    Public Property UpdateReports As Boolean = False

  End Class

  Public Class ICRIncidentReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ICRIncidentReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.StateButtonNew("ViewModel.Report().ReportCriteriaGeneric().UpdateReports",
                                                     "Update Reports", "Do not Update Reports", , , , , "btn-md")
              End With
            End With
          End With
        End With

      End With
    End Sub

  End Class

#End Region

#Region "Equipment Booking Costs (Excel)"

  Class EquipmentBookingCostsReportExcel
    Inherits Singular.Reporting.ReportBase(Of EquipmentBookingCostsReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Feed Costs (Excel)"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(EquipmentBookingCostsReportCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)

      'Try
      '  Dim SponsorshipService As SponsorshipWebService.SSWSDSVCClient
      '  SponsorshipService = New SponsorshipWebService.SSWSDSVCClient
      '  Dim returnedList As New SponsorshipWebService.RetObj_GetBookingItemList
      '  returnedList = SponsorshipService.GetBookingItemList("94208fee-5615-11e5-a800-005056943beb", "SSWSD_Sober", Me.ReportCriteria.StartDate.ToString("dd-MMM-yyyy"), Me.ReportCriteria.EndDate.ToString("dd-MMM-yyyy"))
      '  'Dim x As Object = Nothing
      'Catch ex As Exception
      '  OBLib.Helpers.ErrorHelpers.LogClientError(OBLib.Security.Settings.CurrentUser.FirstNameSurname, "SatOpsReports.vb", "Feed - Report", ex.Message)
      'End Try

      cmd.CommandText = "[RptProcs].[rptEquipmentBookingCostsReport]"
    End Sub

  End Class

  Class EquipmentBookingCostsReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

  End Class

  Public Class EquipmentBookingCostsReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of EquipmentBookingCostsReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With

      End With
    End Sub

  End Class

#End Region

#Region "Equipment Work Order Report"

  Class EquipmentWorkOrderReport
    Inherits Singular.Reporting.ReportBase(Of EquipmentWorkOrderReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Equipment Work Order Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(EquipmentWorkOrderReportCriteriaControl)
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As Type
      Get
        Return GetType(rptEquipmentWorkOrder)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptEquipmentWorkOrder]"
    End Sub

  End Class

  Class EquipmentWorkOrderReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    'XML Equipment list
    <Display(Name:="Equipment"), ClientOnly>
    Public Property ROCircuitSelectList As List(Of ROCircuitSelect) = New List(Of ROCircuitSelect)

    <Display(Name:="Equipment")>
    Public Property EquipmentIDs As List(Of Integer) = New List(Of Integer)

    <Display(Name:="Sub-Depts"), Required(ErrorMessage:="Sub Dept. required"), ClientOnly>
    Public Property SystemID As Integer?

  End Class

  Public Class EquipmentWorkOrderReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of EquipmentWorkOrderReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 4, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 4, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .Helpers.Bootstrap.LabelFor(Function(d) d.SystemID)
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.StateButton(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "EquipmentWorkOrderReport.AddSystem($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                    .Button.AddClass("btn-block buttontext")
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Equipment")
          With .Helpers.Bootstrap.Row
            With .Helpers.ForEachTemplate(Of ROCircuitSelect)(Function(d) d.ROCircuitSelectList)
              With .Helpers.Bootstrap.Column(12, 12, 4, 3, 2)
                With .Helpers.Bootstrap.StateButton(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                  .Button.AddBinding(KnockoutBindingString.click, "EquipmentWorkOrderReport.AddEquipment($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.EquipmentName)
                  .Button.AddClass("btn-block buttontext")
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub

  End Class

#End Region

End Class
