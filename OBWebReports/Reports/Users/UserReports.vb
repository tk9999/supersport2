﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports Singular.Strings
Imports System.ComponentModel
Imports OBLib.Maintenance.ReadOnly
Imports System.ComponentModel.DataAnnotations
Imports Singular.Web
Imports OBLib.Maintenance.Equipment.ReadOnly
Imports Singular.DataAnnotations

Public Class UserReports

#Region "User Actions (Summary)"

  Class UserActionSummary
    Inherits Singular.Reporting.ReportBase(Of UserActionSummaryCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "User Actions (Summary)"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(UserActionSummaryCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptUserActionSummary]"
      cmd.CommandTimeout = 0
    End Sub

  End Class

  Class UserActionSummaryCriteria
    Inherits Singular.Reporting.DefaultCriteria

    <Display(Name:="Start Date"), Required(ErrorMessage:="Start Date is required")>
    Public Property StartDate As DateTime?

    <Display(Name:="End Date"), Required(ErrorMessage:="End Date is required")>
    Public Property EndDate As DateTime?

    <Display(Name:="Sub-Dept"), Required(ErrorMessage:="Sub-Dept is required")>
    Public Property SystemID As Integer?

  End Class

  Public Class UserActionSummaryCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of UserActionSummaryCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
              With .Helpers.FieldSet("Sub-Depts")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "UserActionSummaryReport.AddSystem($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                    .Button.AddClass("btn-block buttontext")
                  End With
                End With
              End With

            End With
          End With
        End With

      End With
    End Sub

  End Class

#End Region

End Class
