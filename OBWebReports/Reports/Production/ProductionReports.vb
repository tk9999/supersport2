﻿Imports Singular.Reporting
Imports Singular
Imports Singular.Misc
Imports System.ComponentModel.DataAnnotations
Imports OBLib.Maintenance.Productions.ReadOnly
Imports OBLib.Maintenance.General.ReadOnly
Imports OBLib.Maintenance.Locations.ReadOnly
Imports Infragistics.Documents.Excel
Imports System.Drawing.Color
Imports System.Drawing
Imports Singular.Web
Imports Singular.DataAnnotations
Imports OBLib
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports OBLib.Maintenance.ReadOnly
Imports OBLib.Maintenance.Productions.Areas.ReadOnly
Imports OBLib.Maintenance.Rooms.ReadOnly
Imports OBLib.Productions.ReadOnly
Imports OBLib.Maintenance.Productions.ReadOnly.Old
Imports OBLib.Maintenance.HR.ReadOnly
Imports OBLib.Maintenance.SystemManagement.ReadOnly
Imports OBLib.ProductionSystemAreas.ReadOnly

Public Class ProductionReports

#Region "Production Status Report"

  Public Class ProductionStatusReport
    Inherits Singular.Reporting.ReportBase(Of ProductionStatusReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Status Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionGridReport"
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionStatusCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each col In DataSet.Tables(0).Columns
        Select Case col.ToString
          Case "ProductionID"
            col.ExtendedProperties.Add("AutoGenerate", False)
        End Select
      Next

    End Sub

    Protected Overrides Sub ModifyExcelWorkbook(Workbook As Workbook)
      MyBase.ModifyExcelWorkbook(Workbook)

      Dim rowNum As Integer = 1
      For Each row In Workbook.Worksheets(0).Rows
        If rowNum <> 1 AndAlso rowNum <> Workbook.Worksheets(0).Rows.Count Then
          Dim Status As String = row.Cells(19).Value
          Select Case Status
            Case "Crew Finalised", "Planning Finalised"
              For Each col In Workbook.Worksheets(0).Columns
                row.Cells(col.Index).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(Color.LightGreen)
              Next

            Case "Reconciled"
              For Each col In Workbook.Worksheets(0).Columns
                row.Cells(col.Index).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(System.Drawing.ColorTranslator.FromHtml("#fdfd96"))
              Next

            Case "Cancelled"
              For Each col In Workbook.Worksheets(0).Columns
                row.Cells(col.Index).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(System.Drawing.ColorTranslator.FromHtml("#b19cd9"))
              Next

          End Select
        End If
        rowNum += 1
      Next

    End Sub

  End Class

  Public Class ProductionStatusReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Production Type", Order:=3)>
    Public Property ProductionTypeID As Integer?

    <Display(Name:="Read Only Production Type List"), ClientOnly>
    Public Property ROProductionTypeList As List(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType) = OBLib.CommonData.Lists.ROProductionTypeList.ToList

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID

    <Display(name:="Production Area", order:=2), Required(errormessage:="Production Area required")>
    Public Property ProductionAreaID As Integer = OBLib.Security.Settings.CurrentUser.ProductionAreaID

    <Display(Name:="Crew Member", Order:=4)>
    Public Property HumanResourceID As Integer?

    <Display(Name:="Crew Member", Order:=4), ClientOnly>
    Public Property HumanResource As String = ""

  End Class

  Public Class ProductionStatusCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase
    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionStatusReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.FieldSet("Production Types")
                  .Attributes("id") = "Productions"
                  With .Helpers.Div
                    With .Helpers.Div
                      With .Helpers.Bootstrap.Row
                        With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                          With .Helpers.With(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport.Criteria)("ViewModel.ROProductionTypeReportPagedListCriteria()")
                            With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord(), Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search Production Types")
                            End With
                          End With
                        End With
                      End With
                    End With
                    .Helpers.HTML.NewLine()
                    With .Helpers.DivC("add-vertical-scroll-600")
                      With .Helpers.Bootstrap.Row
                        With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType)(Function(d) d.ROProductionTypeList)
                          With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "ProductionStatusReport.AddProductionType($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionType)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Div
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                      With .Helpers.Div
                        .Helpers.LabelFor(Function(d As ProductionStatusReportCriteria) d.HumanResourceID)
                      End With
                      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionStatusReportCriteria) d.HumanResource, "ProductionStatusReport.FindCrewMember()",
                                                                "Search for Crew Member", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "ProductionStatusReport.ClearHR()")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub
  End Class
#End Region

#Region " Production Schedule "

  Public Class ProductionReport
    Inherits ReportBase(Of ProductionScheduleReportCriteria)

    Private mDataSet As DataSet

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Schedule"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptProductionReport)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)

      mDataSet = OBLib.PositionLayouts.PositionLayout.GetTemplateDataset
      For Each layout As OBLib.PositionLayouts.PositionLayout In OBLib.PositionLayouts.PositionLayoutList.GetPositionLayoutList(CInt(ReportCriteria.ProductionID))
        mDataSet = layout.GetDataSet(mDataSet)
      Next
      cmd.CommandText = "RptProcs.rptProductionReport"

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionScheduleCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As System.Data.DataSet)
      MyBase.ModifyDataSet(DataSet)

      If Not IsNothing(mDataSet) Then

        Dim dt As DataTable = mDataSet.Tables("Image")
        mDataSet.Tables.Remove(dt)

        Dim HasLayouts As Boolean = False
        For Each drw As DataRow In dt.Rows
          HasLayouts = True
        Next
        DataSet.Tables.Add(dt)

        If HasLayouts Then
          With DataSet.Tables("Table1")
            Dim drwPosLayouts As DataRow = .NewRow()
            drwPosLayouts("Section") = "Position Layouts"
            .Rows.Add(drwPosLayouts)
          End With
        End If


        dt = mDataSet.Tables("PositionLayoutPositionList")
        mDataSet.Tables.Remove(dt)
        DataSet.Tables.Add(dt)

        dt = mDataSet.Tables("PositionLayoutSubPositionList")
        mDataSet.Tables.Remove(dt)
        DataSet.Tables.Add(dt)

      Else

        Dim tbl As New DataTable("Image")

        Dim clm As New DataColumn("Image")
        clm.DataType = GetType(Byte())
        tbl.Columns.Add(clm)
        clm = New DataColumn("LayoutName")
        clm.DataType = GetType(String)
        tbl.Columns.Add(clm)

        DataSet.Tables.Add(tbl)

        tbl = New DataTable("PositionLayoutPositionList")


        clm = New DataColumn("CreatedBy")
        clm.DataType = GetType(Integer)
        tbl.Columns.Add(clm)
        clm = New DataColumn("EquipmentSubType")
        clm.DataType = GetType(String)
        tbl.Columns.Add(clm)
        clm = New DataColumn("LensEquipmentSubType")
        clm.DataType = GetType(String)
        tbl.Columns.Add(clm)
        clm = New DataColumn("TripodEquipmentSubType")
        clm.DataType = GetType(String)
        tbl.Columns.Add(clm)
        clm = New DataColumn("EVSPosition")
        clm.DataType = GetType(String)
        tbl.Columns.Add(clm)
        clm = New DataColumn("Notes")
        clm.DataType = GetType(String)
        tbl.Columns.Add(clm)
        clm = New DataColumn("PositionCode")
        clm.DataType = GetType(String)
        tbl.Columns.Add(clm)

        DataSet.Tables.Add(tbl)

        tbl = New DataTable("PositionLayoutSubPositionList")

        clm = New DataColumn("TripodEquipmentSubType")
        clm.DataType = GetType(String)
        tbl.Columns.Add(clm)
        clm = New DataColumn("Notes")
        clm.DataType = GetType(String)
        tbl.Columns.Add(clm)
        clm = New DataColumn("PositionCode")
        clm.DataType = GetType(String)
        tbl.Columns.Add(clm)

        DataSet.Tables.Add(tbl)

      End If

    End Sub

  End Class

  Public Class ProductionScheduleReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Production", Order:=1), Required(ErrorMessage:="Production Required")>
    Public Property ProductionID As Integer?

    <Display(Name:="Read Only Production List"), ClientOnly>
    Public Property ROProductionList As List(Of ROProductionFind) = New List(Of ROProductionFind)

    <Display(Name:="Production Ref No"), ClientOnly>
    Public Property ProductionRefNo As Integer?

    <Display(Name:="System", Order:=1), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer = OBLib.Security.Settings.CurrentUser.SystemID

  End Class

  Public Class ProductionScheduleCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionScheduleReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
            With .Helpers.Bootstrap.Row()
              With .Helpers.FieldSet("Productions")
                .Attributes("id") = "Productions"
                With .Helpers.Div
                  With .Helpers.Div
                    With .Helpers.Bootstrap.Row
                      With .Helpers.With(Of OBLib.Productions.ReadOnly.ROProductionFindList.Criteria)("ViewModel.ROProductionFindListCriteria()")
                        With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionRefNo, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Ref No")
                          End With
                        End With
                        With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Descriptions")
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.DivC("add-vertical-scroll-600")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of ROProduction)(Function(d) d.ROProductionList)
                        With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                            .Button.AddBinding(KnockoutBindingString.click, "ProductionScheduleReport.AddProduction($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionRefNo + ": " + c.ProductionDescription)
                            .Button.AddClass("btn-block buttontext add-text-align-left")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub

  End Class

#End Region

#Region " The Board "

  Public Class TheBoard
    Inherits ReportBase(Of TheBoardCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "The Board"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "GetProcsWeb.getROVehicleEquipmentScheduleListNew"
      'cmd.CommandTimeout = 0
      Dim pt As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ProductionType").FirstOrDefault
      cmd.Parameters.Remove(pt)
      Dim et As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@EventType").FirstOrDefault
      cmd.Parameters.Remove(et)
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(BoardCriteriaControl)
      End Get
    End Property

    'Public Overrides Function GetDataSource() As DataSet

    '  Return New DataSet

    'End Function

    Private Const WorksheetName As String = "The Board"
    Private Const WeekDayRowNo As Integer = 0
    Private Const MonthDayRowNo As Integer = 1
    Private Const EventManagerOffRowNo As Integer = 2
    Private Const NoOfHeaderRows As Integer = 3
    Private Const ColumnOffset As Integer = 2
    Private Const ItemColumnIndex As Integer = 1
    Private Const DefaultItemRowHeight As Decimal = 25.5
    Private Const DefaultColumnWidth As Decimal = 17.5
    Private Const SeperatingRowHeight As Integer = 6
    Private ColumnWidths As New Dictionary(Of String, System.Drawing.Size)

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim ReportMonthStartDate As Date = Me.ReportCriteria.StartDate
      Dim ReportEndDate As Date = Me.ReportCriteria.EndDate
      Dim ProductionTypeID As Object = Me.ReportCriteria.ProductionTypeID
      Dim EventTypeID As Object = Me.ReportCriteria.EventTypeID
      Dim List As OBLib.Reports.ROVehicleEquipmentScheduleList = OBLib.Reports.ROVehicleEquipmentScheduleList.GetROVehicleEquipmentScheduleList(ReportMonthStartDate, ReportEndDate, ProductionTypeID, EventTypeID)

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add(WorksheetName)
      ws.Columns(0).CellFormat.ShrinkToFit = ExcelDefaultableBoolean.True
      ws.Columns(ItemColumnIndex).CellFormat.ShrinkToFit = ExcelDefaultableBoolean.True
      ws.Columns(ItemColumnIndex).CellFormat.WrapText = ExcelDefaultableBoolean.True
      ws.DisplayOptions.PanesAreFrozen = True
      ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 2


      Dim ListOfItems As List(Of String) = List.GetListOfItems

      Dim TotalRows As Integer = (DateDiff(DateInterval.Month, ReportMonthStartDate, ReportEndDate) + 1) * (ListOfItems.Count + NoOfHeaderRows + 1) - 1

      SetCellBorders(ws, 0, 0, 0, TotalRows, True, System.Drawing.Color.White, System.Drawing.Color.White, System.Drawing.Color.White, System.Drawing.Color.White, CellBorderLineStyle.None, CellBorderLineStyle.None, CellBorderLineStyle.None, CellBorderLineStyle.None)
      SetCellBorders(ws, ItemColumnIndex, ColumnOffset + 31, 0, TotalRows, False, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black)
      SetCellBorders(ws, ItemColumnIndex, ItemColumnIndex, 0, TotalRows, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, , , CellBorderLineStyle.Medium, CellBorderLineStyle.Medium)

      Dim MonthCount As Integer = 0
      Dim StartRow As Integer = 0

      'if the start and end dates are not in the same month then move the end date to the end of the month
      Dim ReportMonthEndDate As Date = ReportEndDate
      If ReportMonthStartDate.Month <> ReportEndDate.Month Then
        ReportMonthStartDate = Singular.Misc.Dates.DateMonthStart(ReportMonthStartDate.Date)
        ReportMonthEndDate = Singular.Misc.Dates.DateMonthEnd(ReportEndDate.Date)
      End If

      'loop though every month in between Start and End dates
      While ReportMonthStartDate < ReportMonthEndDate
        'set the column headers for the current month being generated for

        Dim iStart As Integer = 0
        Dim iEnd As Integer = Date.DaysInMonth(ReportMonthStartDate.Year, ReportMonthStartDate.Month) - 1
        If ReportMonthStartDate.Month = ReportEndDate.Month Then
          iStart = ReportMonthStartDate.Day - 1
          iEnd = ReportMonthEndDate.Day - 1
        End If
        For i As Integer = iStart To iEnd
          Dim Col As Integer = i + ColumnOffset
          ws.Columns(Col).Width = DefaultColumnWidth * 256
          SetText(ws, StartRow + WeekDayRowNo, Col, DateAdd(DateInterval.Day, i, ReportMonthStartDate.Date).ToString("dddd").ToUpper, ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center)
          SetText(ws, StartRow + MonthDayRowNo, Col, i + 1, ExcelDefaultableBoolean.True, , , HorizontalCellAlignment.Center)
        Next

        'add the event managers row with all their off weekends
        Dim EventManagerOffWeekendList As OBLib.HR.ReadOnly.ROHumanResourceOffWeekendList = OBLib.HR.ReadOnly.ROHumanResourceOffWeekendList.GetROHumanResourceOffWeekendList(ReportMonthStartDate, Singular.Misc.Dates.DateMonthEnd(ReportMonthStartDate.Date), OBLib.CommonData.Enums.Discipline.EventManager)
        For Each ShiftPatternID As Integer In EventManagerOffWeekendList.GetListOfShiftPatterIDs
          Dim EMShiftPatternIDList As OBLib.HR.ReadOnly.ROHumanResourceOffWeekendList = EventManagerOffWeekendList.FilterList("ShiftPatternID", ShiftPatternID)

          Dim ColEventManagerOffWeekendStart As Integer = ColumnOffset + EMShiftPatternIDList(0).WeekendStartDate.Value.Day - 1
          Dim ColEventManagerOffWeekendEnd As Integer = ColumnOffset + EMShiftPatternIDList(0).WeekendEndDate.Value.Day - 1
          Dim ColorStart As System.Drawing.Color
          Dim ColorEnd As System.Drawing.Color
          If EMShiftPatternIDList.Count = 1 Then
            ColorStart = EMShiftPatternIDList(0).ReportingColour
            ColorEnd = EMShiftPatternIDList(0).ReportingColour
          Else 'If EMShiftPatternIDList.Count = 2 Then
            ColorStart = EMShiftPatternIDList(0).ReportingColour
            ColorEnd = EMShiftPatternIDList(1).ReportingColour
            'Else

          End If
          SetText(ws, StartRow + EventManagerOffRowNo, ColEventManagerOffWeekendStart + 1, EMShiftPatternIDList.GetNames)
          ws.Rows(EventManagerOffRowNo + StartRow).Cells(ColEventManagerOffWeekendStart).CellFormat.FillPatternForegroundColor = ColorStart
          ws.Rows(EventManagerOffRowNo + StartRow).Cells(ColEventManagerOffWeekendStart).CellFormat.FillPattern = FillPatternStyle.Solid
          ws.Rows(EventManagerOffRowNo + StartRow).Cells(ColEventManagerOffWeekendEnd).CellFormat.FillPatternForegroundColor = ColorEnd
          ws.Rows(EventManagerOffRowNo + StartRow).Cells(ColEventManagerOffWeekendEnd).CellFormat.FillPattern = FillPatternStyle.Solid
        Next

        'add the Item name rows
        Dim ItemName As String = ""
        For i As Integer = 0 To ListOfItems.Count - 1
          ItemName = ListOfItems(i)
          Dim Row As Integer = StartRow + NoOfHeaderRows + i
          ws.Rows(Row).Height = DefaultItemRowHeight * 20
          SetText(ws, Row, ItemColumnIndex, ItemName, ExcelDefaultableBoolean.True)

          'all entries for the current item
          Dim subList As OBLib.Reports.ROVehicleEquipmentScheduleList = List.FilterList(ItemName, ReportMonthStartDate.Year, ReportMonthStartDate.Month)
          For Each obj As OBLib.Reports.ROVehicleEquipmentSchedule In subList

            'Column index = day of month (plus the two header columns)
            Dim Col As Integer = ColumnOffset + CDate(obj.StartDateTime).Day - 1

            'if all the items fall on the same day
            If subList.AllOnSameDay(ItemName) Then
              If obj.ServiceInd Then
                SetText(ws, Row, Col, obj.ProductionDescription, ExcelDefaultableBoolean.True, , 12, , FontUnderlineStyle.Single, VerticalCellAlignment.Center)
              Else
                SetText(ws, Row, Col, obj.Location)
                Dim pt As OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType = OBLib.CommonData.Lists.ROProductionTypeList.Where(Function(c) CompareSafe(c.ProductionTypeID, obj.ProductionTypeID)).FirstOrDefault
                'Dim pt As OBLib.Maintenance.ProductionType = CommonData.Lists.ProductionTypeList.FindFast("ProductionTypeID", obj.ProductionTypeID)
                If pt IsNot Nothing AndAlso obj.CancelInd Then
                  FormatCells(ws, Col, Col, Row, Row, Drawing.Color.FromName(pt.ReportingColour), FillPatternStyle.Default)
                  SetDiagonalCellBorders(ws, Col, Row, DiagonalBorders.DiagonalDown, CellBorderLineStyle.Thick, System.Drawing.Color.Black)
                ElseIf obj.HDRequiredInd Then
                  FormatCells(ws, Col, Col, Row, Row, Drawing.Color.FromName(pt.ReportingColour), FillPatternStyle.Gray6percent)
                Else
                  FormatCells(ws, Col, Col, Row, Row, Drawing.Color.FromName(pt.ReportingColour), FillPatternStyle.Default)
                End If
              End If
            Else

              'otherwise all items do not fall on the same day
              Select Case obj.ProductionTimelineTypeID
                Case 0
                  'if no timeline found
                  If obj.ServiceInd Then
                    SetText(ws, Row, Col, obj.ProductionDescription, ExcelDefaultableBoolean.True, , 12, , FontUnderlineStyle.Single, VerticalCellAlignment.Center)
                  Else
                    SetText(ws, Row, Col, obj.ProductionTimelineType, ExcelDefaultableBoolean.True, , 12, , FontUnderlineStyle.Single, VerticalCellAlignment.Center)
                  End If
                Case OBLib.CommonData.Enums.ProductionTimelineType.Rig
                  SetText(ws, Row, Col, "Rig", ExcelDefaultableBoolean.True, , 12, , FontUnderlineStyle.Single, VerticalCellAlignment.Center)
                Case OBLib.CommonData.Enums.ProductionTimelineType.VehiclePreTravel, OBLib.CommonData.Enums.ProductionTimelineType.VehiclePostTravel
                  SetText(ws, Row, Col, "Trav", ExcelDefaultableBoolean.True, , 12, , FontUnderlineStyle.Single, VerticalCellAlignment.Center)
                Case OBLib.CommonData.Enums.ProductionTimelineType.Transmission
                  Dim hr As OBLib.HR.ReadOnly.ROHumanResource = OBLib.HR.ReadOnly.ROHumanResource.GetROHumanResource(obj.HumanResourceIDEventManager)
                  'Dim pt As OBLib.Maintenance.ProductionType = CommonData.Lists.ProductionTypeList.FindFast("ProductionTypeID", obj.ProductionTypeID)
                  Dim pt As OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType = OBLib.CommonData.Lists.ROProductionTypeList.Where(Function(c) CompareSafe(c.ProductionTypeID, obj.ProductionTypeID)).FirstOrDefault
                  SetText(ws, Row, Col, obj.Location, ExcelDefaultableBoolean.True, , , , FontUnderlineStyle.Single, VerticalCellAlignment.Center)
                  ws.Rows(Row).Cells(Col).CellFormat.WrapText = ExcelDefaultableBoolean.True

                  If hr IsNot Nothing AndAlso Not hr.ReportingColour.Trim = "" Then
                    SetCellBorders(ws, Col, Col, Row, Row, True, Drawing.Color.FromName(hr.ReportingColour), Drawing.Color.FromName(hr.ReportingColour), Drawing.Color.FromName(hr.ReportingColour), Drawing.Color.FromName(hr.ReportingColour), CellBorderLineStyle.Thick, CellBorderLineStyle.Thick, CellBorderLineStyle.Thick, CellBorderLineStyle.Thick)
                  End If
                  If pt IsNot Nothing Then
                    If obj.CancelInd Then
                      FormatCells(ws, Col, Col, Row, Row, Drawing.Color.FromName(pt.ReportingColour), FillPatternStyle.Default)
                      SetDiagonalCellBorders(ws, Col, Row, DiagonalBorders.DiagonalDown, CellBorderLineStyle.Thick, System.Drawing.Color.Black)
                    ElseIf obj.HDRequiredInd Then
                      FormatCells(ws, Col, Col, Row, Row, Drawing.Color.FromName(pt.ReportingColour), FillPatternStyle.Gray6percent)
                    Else
                      FormatCells(ws, Col, Col, Row, Row, Drawing.Color.FromName(pt.ReportingColour), FillPatternStyle.Default)
                    End If

                    'get the best (white or black) colour for the text for the cell
                    ws.Rows(Row).Cells(Col).CellFormat.Font.Color = Singular.Misc.Colors.GetTextColorForBackground(Drawing.Color.FromName(pt.ReportingColour))
                  End If
                  Dim comment As WorksheetCellComment = New WorksheetCellComment()
                  Dim EventManager As String = "TBC"
                  Dim EventManagerContact As String = "TBC"
                  If hr IsNot Nothing Then
                    EventManager = hr.FirstPreferredSurname
                    EventManagerContact = hr.CellPhoneNumber
                  End If

                  Dim FacsStartDateTime As String = ""
                  If obj.FacsStartDateTime.HasValue Then
                    FacsStartDateTime = obj.FacsStartDateTime.Value.ToString("HH") & "h" & obj.FacsStartDateTime.Value.ToString("mm")
                  End If

                  Dim formatted As New FormattedString(IIf(obj.CancelInd, "CANCELLED" & vbCrLf, "") &
                                                       "Production: " & obj.ProductionID & vbCrLf &
                                                      "Event Manager: " & EventManager & vbCrLf &
                                                     "Contact #: " & EventManagerContact & vbCrLf &
                                                     "Production: " & obj.ProductionDescription & vbCrLf &
                                                     "Time: " & CDate(obj.StartDateTime).ToString("HH") & "h" & CDate(obj.StartDateTime).ToString("mm") & vbCrLf &
                                                     IIf(FacsStartDateTime = "", "", "Facs Check: " & FacsStartDateTime & vbCrLf) &
                                                     "Venue: " & obj.ProductionVenue & vbCrLf &
                                                     "City: " & obj.City & " (" & obj.CityCode & ")" & vbCrLf &
                                                     "Client: " & "Supersport" & vbCrLf &
                                                     "Producer: " & "TBC" & vbCrLf &
                                                     "HD?: " & IIf(obj.HDRequiredInd, "YES", "NO"))

                  comment.Text = formatted
                  comment.Visible = True
                  ws.Rows(Row).Cells(Col).Comment = comment

                  'if cancelled production then add "CANCLLED" in bold/red at the top of the comment
                  If obj.CancelInd Then
                    Dim CancelledStringFont As FormattedStringFont = formatted.GetFont(0, 9)
                    CancelledStringFont.Bold = ExcelDefaultableBoolean.True
                    CancelledStringFont.Height = 16 * 2
                    CancelledStringFont.Color = System.Drawing.Color.Red
                  End If

                  Dim font As FormattedStringFont = formatted.GetFont(0)
                  font.Height = 8 * 20
                  comment.Visible = False
                  comment.PositioningMode = ShapePositioningMode.MoveAndSizeWithCells
                  comment.SetBoundsInTwips(ws, New System.Drawing.Rectangle(0, 0, 300 * 20, 130 * 20))
                  Dim FirstvbCrLf As Integer = 0
                  Dim UnformattedString As String = comment.Text.UnformattedString
                  Do
                    Dim Start As Integer = FirstvbCrLf
                    Dim Length As Integer = UnformattedString.IndexOf(":", Start) + 1 - Start
                    font = comment.Text.GetFont(Start, Length)
                    font.Bold = ExcelDefaultableBoolean.True
                    If UnformattedString.IndexOf(vbCrLf, FirstvbCrLf) < 0 Then Exit Do
                    FirstvbCrLf = UnformattedString.IndexOf(vbCrLf, FirstvbCrLf) + vbCrLf.Length
                  Loop

                Case Else

              End Select
            End If

          Next

          'CURRENT CITY CHECKING------------------------------------------------------------------------

          'get the days on which nothing happens
          Dim EmptyDays As List(Of Date) = subList.GetFreeDays(ReportMonthStartDate, ReportMonthEndDate)

          'For Each day where nothing happens
          For Each d As Date In EmptyDays

            Dim prevItem As OBLib.Reports.ROVehicleEquipmentSchedule = subList.GetPreviousItem(d)
            Dim nextItem As OBLib.Reports.ROVehicleEquipmentSchedule = subList.GetNextItem(d)

            'The Column to set the text for
            Dim Col As Integer = ColumnOffset + CDate(d).Day - 1


            If nextItem Is Nothing And prevItem Is Nothing Then 'Both Nothing
              SetText(ws, Row, Col, "") 'Base

            ElseIf nextItem IsNot Nothing And prevItem IsNot Nothing Then 'Both Something
              Dim dayDiff As Integer = nextItem.StartDateTime.Value.Date.Subtract(prevItem.StartDateTime.Value.Date).TotalDays

              If IsPreTravel(nextItem) And IsPostTravel(prevItem) Then 'left and right have/are travel items
                SetText(ws, Row, Col, "") 'Base

              ElseIf IsPostTravel(prevItem) And Not IsPreTravel(nextItem) Then 'left has/is a travel item, right does not have a travel item
                'In Next City
                SetText(ws, Row, Col, nextItem.CityCode, ExcelDefaultableBoolean.True, , 12, , FontUnderlineStyle.Single, VerticalCellAlignment.Center)
              ElseIf Not IsPostTravel(prevItem) And IsPreTravel(nextItem) Then
                'In Prev City
                If prevItem.CityCode <> nextItem.CityCode Then
                  If prevItem.CityCode <> "JHB" Then
                    SetText(ws, Row, Col, prevItem.CityCode, ExcelDefaultableBoolean.True, , 12, , FontUnderlineStyle.Single, VerticalCellAlignment.Center)
                  End If
                Else
                  SetText(ws, Row, Col, "")
                End If
              Else
                SetText(ws, Row, Col, prevItem.CityCode + "/" + nextItem.CityCode)
              End If

            Else

              If prevItem IsNot Nothing And nextItem Is Nothing Then
                'SetText(ws, Row, Col, prevItem.CityCode)
              ElseIf prevItem Is Nothing And nextItem IsNot Nothing Then
                'SetText(ws, Row, Col, nextItem.CityCode)
              End If

            End If

          Next

        Next

        SetText(ws, StartRow + 1, 0, ReportMonthStartDate.ToString("MMMM"), ExcelDefaultableBoolean.True, , 10, HorizontalCellAlignment.Center)
        'set the first row in the month column so that when we merge this group it has a value
        ReportMonthStartDate = DateAdd(DateInterval.Month, 1, ReportMonthStartDate)
        Dim PrevStartRow As Integer = StartRow
        StartRow += ListOfItems.Count + NoOfHeaderRows

        'merge the rows in the month (first) column to highlight which month this is for column
        If MonthCount Mod 2 = 0 Then
          FormatCells(ws, 0, 0, PrevStartRow + 1, StartRow - 1, System.Drawing.Color.FromArgb(191, 191, 191), FillPatternStyle.Gray12percent)
        Else
          'leave it white background
        End If

        MonthCount += 1

        'lets add a separating row
        FormatCells(ws, 0, ColumnOffset + 31, StartRow, StartRow, System.Drawing.Color.White, FillPatternStyle.ThinDiagonalStripe)
        ws.Rows(StartRow).Height = SeperatingRowHeight * 20
        StartRow += 1

      End While
      StartRow -= 1

      'For Each kp In ColumnWidths
      '  Dim cell = ws.GetCell(kp.Key)
      '  ws.Columns(cell.ColumnIndex).Width = kp.Value.Width
      'Next
      'Dim icol As Integer = 0
      'Do
      '  If String.IsNullOrEmpty(ws.Rows(1).Cells(icol).ToString) Then Exit Do
      '  Dim irow As Integer = 0

      '  icol += 1
      'Loop

      'SetCellBorders(ws, 0, 0, 0, StartRow, True, Drawing.Color.White, Drawing.Color.White, Drawing.Color.White, Drawing.Color.White, CellBorderLineStyle.None, CellBorderLineStyle.None, CellBorderLineStyle.None, CellBorderLineStyle.None)
      'SetCellBorders(ws, ItemColumnIndex, ColumnOffset + 31, 0, StartRow, False, Drawing.Color.Black, Drawing.Color.Black, Drawing.Color.Black, Drawing.Color.Black)
      'SetCellBorders(ws, ItemColumnIndex, ItemColumnIndex, 0, StartRow, True, Drawing.Color.Black, Drawing.Color.Black, Drawing.Color.Black, Drawing.Color.Black, , , CellBorderLineStyle.Medium, CellBorderLineStyle.Medium)

      Return wb

    End Function

    Private Sub SetText(ByVal ws As Worksheet, ByVal Row As Integer, ByVal Column As Integer, ByVal Text As String, _
                    Optional ByVal Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.Default, _
                    Optional ByVal FontName As String = "Arial", Optional ByVal FontSize As Integer = 8, _
                    Optional ByVal HAlign As HorizontalCellAlignment = HorizontalCellAlignment.Center, _
                    Optional ByVal FontUnderlineStyle As FontUnderlineStyle = FontUnderlineStyle.None, _
                    Optional ByVal VerticalAlignment As VerticalCellAlignment = VerticalCellAlignment.Default)

      If Integer.TryParse(Text, 0) Then
        ws.Rows(Row).Cells(Column).Value = CInt(Text)
      Else
        ws.Rows(Row).Cells(Column).Value = Text
      End If

      ws.Rows(Row).Cells(Column).CellFormat.Font.Bold = Bold
      ws.Rows(Row).Cells(Column).CellFormat.Font.Name = FontName
      ws.Rows(Row).Cells(Column).CellFormat.Font.Height = FontSize * 20
      ws.Rows(Row).Cells(Column).CellFormat.Alignment = HAlign
      ws.Rows(Row).Cells(Column).CellFormat.Font.UnderlineStyle = FontUnderlineStyle
      ws.Rows(Row).Cells(Column).CellFormat.VerticalAlignment = VerticalAlignment

      Dim CellKey As String = ws.Rows(Row).Cells(Column).ToString
      Dim size = New System.Drawing.Size
      If Not ColumnWidths.ContainsKey(CellKey) Then
        size.Width = IIf(ws.Columns(Column).Width = -1, ws.DefaultColumnWidth, ws.Columns(Column).Width) / 256.0
        size.Height = IIf(ws.Rows(Row).Height = -1, ws.DefaultRowHeight, ws.Rows(Row).Height) / 20.0
        ColumnWidths.Add(CellKey, size)
      Else
        size = ColumnWidths(CellKey)
      End If

      '''dummy label to get a graphics object to measure the width (in pixels) of a string
      'Dim l As New System.Windows.Forms.Label
      'l.Text = Text
      'l.Size = size
      'Dim g As System.Drawing.Graphics = l.CreateGraphics
      'Dim s As System.Drawing.SizeF
      ''For Each cell As Infragistics.Win.UltraWinGrid.UltraGridCell In Row.Cells
      'Dim iFont = ws.Rows(Row).Cells(Column).CellFormat.Font
      'Dim windowsFont = New Drawing.Font(iFont.Name, iFont.Height / 20.0)
      's = g.MeasureString(Text, windowsFont)
      'size.Width = IIf(CInt(size.Width) < CInt(s.Width), CInt(s.Width), size.Width)

    End Sub

    ''' <summary>
    ''' Applies formatting to an Infragistics excel spreadsheet
    ''' </summary>
    ''' <param name="ws">The excel worksheet to use</param>
    ''' <param name="ColumnFrom">The starting column index to apply formatting to</param>
    ''' <param name="ColumnTo">The ending column index to apply formatting to</param>
    ''' <param name="RowFrom">The starting row index to apply formatting to</param>
    ''' <param name="RowTo">The ending row index to apply formatting to</param>
    ''' <param name="FillColor">The background colour for the cell</param>
    ''' <param name="FillPattern">The fill pattern for the cell</param>
    ''' <remarks></remarks>
    Private Sub FormatCells(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, _
                            ByVal FillColor As System.Drawing.Color, ByVal FillPattern As FillPatternStyle)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          ws.Rows(i).Cells(j).CellFormat.FillPatternBackgroundColor = FillColor
          Select Case FillPattern
            Case FillPatternStyle.Solid, FillPatternStyle.Default, FillPatternStyle.None
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = FillColor
            Case Else
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = System.Drawing.Color.Black
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
          End Select
        Next
      Next

    End Sub

    Private Sub SetCellBorders(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, ByVal ReplaceExistingBorders As Boolean, _
                           ByVal TopBorderColor As System.Drawing.Color, ByVal BottomBorderColor As System.Drawing.Color, _
                           ByVal LeftBorderColor As System.Drawing.Color, ByVal RightBorderColor As System.Drawing.Color, _
                           Optional ByVal TopBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal BottomBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                           Optional ByVal LeftBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal RightBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                           Optional ByVal DiagonalBorder As DiagonalBorders = DiagonalBorders.None, Optional DiagonalBorderStyle As CellBorderLineStyle = CellBorderLineStyle.None,
                           Optional ByVal DiagonalBorderColor As System.Drawing.Color = Nothing)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = TopBorderStyle
            ws.Rows(i).Cells(j).CellFormat.TopBorderColor = TopBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = BottomBorderStyle
            ws.Rows(i).Cells(j).CellFormat.BottomBorderColor = BottomBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = LeftBorderStyle
            ws.Rows(i).Cells(j).CellFormat.LeftBorderColor = LeftBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = RightBorderStyle
            ws.Rows(i).Cells(j).CellFormat.RightBorderColor = RightBorderColor
          End If

          ws.Rows(i).Cells(j).CellFormat.DiagonalBorders = DiagonalBorder
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderColor = DiagonalBorderColor

        Next
      Next
    End Sub

    Private Sub SetDiagonalCellBorders(ByVal ws As Worksheet, ByVal Column As Integer, ByVal Row As Integer,
                           ByVal DiagonalBorder As DiagonalBorders, DiagonalBorderStyle As CellBorderLineStyle,
                           ByVal DiagonalBorderColor As System.Drawing.Color)

      ws.Rows(Row).Cells(Column).CellFormat.DiagonalBorders = DiagonalBorder
      ws.Rows(Row).Cells(Column).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
      ws.Rows(Row).Cells(Column).CellFormat.DiagonalBorderColor = DiagonalBorderColor

    End Sub

    Private Function IsPostTravel(ByVal item As OBLib.Reports.ROVehicleEquipmentSchedule) As Boolean

      If item.TravelAndRigInd Then Return True

      Select Case item.ProductionTimelineTypeID
        Case OBLib.CommonData.Enums.ProductionTimelineType.VehiclePostTravel, OBLib.CommonData.Enums.ProductionTimelineType.RigPostTravel 'if post travel
          Return True
        Case Else
          Return False
      End Select

    End Function

    Private Function IsPreTravel(ByVal item As OBLib.Reports.ROVehicleEquipmentSchedule) As Boolean

      If item.TravelAndRigInd Then Return True

      Select Case item.ProductionTimelineTypeID
        Case OBLib.CommonData.Enums.ProductionTimelineType.VehiclePreTravel, OBLib.CommonData.Enums.ProductionTimelineType.RigPreTravel 'if pre travel
          Return True
        Case Else
          Return False
      End Select

    End Function

    Private Function IsSameCity(ByVal prevItem As OBLib.Reports.ROVehicleEquipmentSchedule, ByVal nextItem As OBLib.Reports.ROVehicleEquipmentSchedule) As Boolean

      If prevItem.CityCode = nextItem.CityCode Then
        Return True
      End If

      Return False

    End Function

  End Class

  Public Class TheBoardCriteria
    Inherits StartAndEndDateReportCriteria

    <Display(Name:="Production Type", Order:=1)>
    Public Property ProductionTypeID As Integer?
    <Display(Name:="Production Type", Order:=1)>
    Public Property ProductionType As String = ""

    <Display(Name:="Event Type", Order:=2)>
    Public Property EventTypeID As Integer?
    <Display(Name:="Event Type", Order:=2)>
    Public Property EventType As String = ""

  End Class

  Public Class BoardCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of TheBoardCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 2)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 2)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                .Helpers.LabelFor(Function(d As TheBoardCriteria) d.ProductionTypeID)
              End With
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As TheBoardCriteria) d.ProductionType, "BoardReport.FindProductionTypeEventType($element)",
                                                         "Select Production Type", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                .Helpers.LabelFor(Function(d As TheBoardCriteria) d.EventTypeID)
              End With
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As TheBoardCriteria) d.EventType, "BoardReport.FindProductionTypeEventType($element)",
                                                         "Select Event Type", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
        End With
      End With

    End Sub

  End Class

#End Region

#Region "Crew Schedule Report"

  Public Class CrewScheduleReport
    Inherits ReportBase(Of CrewScheduleReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Crew Schedule Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptCrewSchedule)
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(CrewScheduleReportCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptCrewScheduleNew"
      'cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      Dim pt As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@Production").FirstOrDefault
      cmd.Parameters.Remove(pt)
      Dim hr As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@HumanResource").FirstOrDefault
      cmd.Parameters.Remove(hr)
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              'Case "HumanResourceID", "ProductionID", "HumanResourceOffPeriodID", "Production", "HumanResource"
              '  col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

  End Class

  Public Class CrewScheduleReportCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    <Display(Name:="Production", Description:="", Order:=3)>
    Public Property ProductionID As Integer?

    <Display(Name:="Production", Description:="", Order:=3)>
    Public Property Production As String = ""

    <Display(Name:="Crew Member", Description:="", Order:=4)>
    Public Property HumanResourceID As Integer?

    <Display(Name:="Crew Member", Description:="", Order:=4)>
    Public Property HumanResource As String = ""

    <Display(Name:="Hide Detail?", Description:="", Order:=5)>
    Public Property HideDetailInd As Boolean = True

    <Display(Name:="OB?", Description:="", Order:=6)>
    Public Property OBInd As Boolean = False

    <Display(Name:="Studios?", Description:="", Order:=7)>
    Public Property StudiosInd As Boolean = False

    <Display(Name:="Discipline", Description:="", Order:=4)>
    Public Property DisciplineID As Integer?

  End Class

  Public Class CrewScheduleReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of CrewScheduleReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 2)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 2)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                .Helpers.LabelFor(Function(d As CrewScheduleReportCriteria) d.ProductionID)
              End With
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As CrewScheduleReportCriteria) d.Production, "CrewScheduleReport.FindProduction($element)",
                                                         "Search for Production", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Div
                .Helpers.LabelFor(Function(d As CrewScheduleReportCriteria) d.HumanResourceID)
              End With
              With .Helpers.Div
                With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As CrewScheduleReportCriteria) d.HumanResource, "CrewScheduleReport.FindCrewMember($element)",
                                                        "Search for Crew Member", , BootstrapEnums.Style.DefaultStyle, "fa-search", True)
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 2)
              With .Helpers.Bootstrap.StateButtonNew(Function(d As CrewScheduleReportCriteria) d.HideDetailInd, "Hide Detail", "Do not Hide Detail", , "btn-warning", , , "btn-sm")

              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              .Helpers.Bootstrap.LabelDisplay("Areas to Include")
            End With
          End With
          With .Helpers.Bootstrap.Row
            If Singular.Security.HasAccess("Production Area.Outside Broadcast") Then
              With .Helpers.Bootstrap.Column(12, 12, 3, 1)
                With .Helpers.Bootstrap.StateButtonNew(Function(d As CrewScheduleReportCriteria) d.OBInd, "OB", "OB", , "btn-warning", , , "btn-sm")

                End With
              End With
            End If
            If Singular.Security.HasAccess("Production Area.Studios") Then
              With .Helpers.Bootstrap.Column(12, 12, 3, 1)
                With .Helpers.Bootstrap.StateButtonNew(Function(d As CrewScheduleReportCriteria) d.StudiosInd, "Studio", "Studio", , "btn-warning", , , "btn-sm")

                End With
              End With
            End If
          End With
        End With
      End With

    End Sub

  End Class

#End Region

#Region " Studio PA Form "

  Public Class StudioPAForm
    Inherits ReportBase(Of StudioPAFormCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Studio PA Form - Sign In"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptStudioPAForm)
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(StudioPAFormCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)

      cmd.CommandText = "RptProcs.rptPAFormStudiosSignIn"
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "RoomScheduleID"
                col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next



    End Sub

  End Class

  Public Class StudioPAFormCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="System"), Required(ErrorMessage:="System is required")>
    Public Property SystemID As Integer?

    <Display(Name:="Production Area"), Required(ErrorMessage:="Production Area is required")>
    Public Property ProductionAreaID As Integer?

  End Class

  Public Class StudioPAFormCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of StudioPAFormCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
            With .Helpers.FieldSet("Sub-Depts and Areas")
              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                  .Button.AddBinding(KnockoutBindingString.click, "StudioPAFormSignIn.AddSystem($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                  .Button.AddClass("btn-block buttontext")
                End With
                With .Helpers.Bootstrap.Row
                  .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                    With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                      .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "StudioPAFormSignIn.AddProductionArea($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Studio PA Form Sign-Out"

  Public Class StudioPAFormSignOut
    Inherits ReportBase(Of StudioPAFormSignOutCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Studio PA Form - Sign Out"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptStudioPAFormSignOut)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptPAFormStudiosSignOut"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(StudioPAFormSignOutCriteriaControl)
      End Get
    End Property

  End Class

  Public Class StudioPAFormSignOutCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="System"), Required(ErrorMessage:="System is required")>
    Public Property SystemID As Integer?

    <Display(Name:="Production Area"), Required(ErrorMessage:="Production Area is required")>
    Public Property ProductionAreaID As Integer?

  End Class

  Public Class StudioPAFormSignOutCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of StudioPAFormSignOutCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
            With .Helpers.FieldSet("Sub-Depts and Areas")
              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                  .Button.AddBinding(KnockoutBindingString.click, "StudioPAFormSignOut.AddSystem($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                  .Button.AddClass("btn-block buttontext")
                End With
                With .Helpers.Bootstrap.Row
                  .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                    With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                      .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "StudioPAFormSignOut.AddProductionArea($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region " OB Production PA Form - Sign In"

  Public Class ProductionPAForm
    Inherits ReportBase(Of ProductionPAFormCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "OB PA Form - Sign In"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptProductionPAForm)
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionPAFormCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)

      cmd.CommandText = "RptProcs.rptProductionPAForm"
      cmd.CommandTimeout = 0
    End Sub

  End Class

  Public Class ProductionPAFormCriteria
    Inherits DefaultCriteria

    <Display(Name:="Production", Order:=1), Required(ErrorMessage:="Production Required")>
    Public Property ProductionID As Integer?

    <Display(Name:="Read Only Production List"), ClientOnly>
    Public Property ROProductionList As List(Of ROProduction) = OBLib.CommonData.Lists.ROProductionList.ToList

  End Class

  Public Class ProductionPAFormCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionPAFormCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
            With .Helpers.FieldSet("Productions")
              .Attributes("id") = "Productions"
              With .Helpers.Div
                With .Helpers.Div
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.Productions.ReadOnly.ROProductionFindList.Criteria)("ViewModel.ROProductionFindListCriteria()")
                      With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionRefNo, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Ref No")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Descriptions")
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.DivC("add-vertical-scroll-600")
                  With .Helpers.Bootstrap.Row
                    With .Helpers.ForEachTemplate(Of ROProduction)(Function(d) d.ROProductionList)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                          .Button.AddBinding(KnockoutBindingString.click, "OBProductionPAFormReport.AddProduction($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionRefNo + ": " + c.ProductionDescription)
                          .Button.AddClass("btn-block buttontext")
                          .Button.AddClass("add-text-align-left")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With

    End Sub

  End Class

#End Region

#Region "Crew Schedule SMS Report"

  Public Class CrewScheduleSMSReport
    Inherits ReportBase(Of CrewScheduleReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Crew Schedule SMS Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptCrewSchedule)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptCrewScheduleSMS]"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "HumanResourceID", "ProductionID", "HumanResourceOffPeriodID"
                col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

  End Class

  Public Class CrewScheduleSMSReportCriteria
    Inherits StartAndEndDateReportCriteria

    <Display(Name:="Production", Description:="", Order:=1),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Productions.ReadOnly.ROProductionList.ProductionReportCriteria),
                                         DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.FindScreen,
                                         UnselectedText:="All")>
    Public Property ProductionID As Integer?

    <Display(Name:="Crew Member", Description:="", Order:=2),
    Singular.DataAnnotations.DropDownWeb(GetType(OBLib.HR.ReadOnly.ROHumanResourceList.ReportCriteria),
                                         DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.FindScreen,
                                         UnselectedText:="All")>
    Public Property HumanResourceID As Integer?

    <Display(Name:="Hide Detail?", Description:="", Order:=3)>
    Public Property HideDetailInd As Boolean = True

  End Class

#End Region

#Region " Post Prod report "

  Public Class PostProductionReportCriteria
    Inherits DefaultCriteria

    '<System.ComponentModel.DisplayName("Production"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Productions.ReadOnly.ROProductionList), ValueMember:="ProductionID", DisplayMember:="ProductionRefNo", UnselectedText:="All Productions")>
    Public Property ProductionID As Object

  End Class

  Public Class PostProductionReport
    Inherits ReportBase(Of PostProductionReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Post Production Report"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptPostProductionReport)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)

      cmd.CommandText = "RptProcs.rptPostProductionReport"
    End Sub

  End Class

#End Region

#Region " Production Derig Form "

  Public Class ProductionDerigForm
    Inherits ReportBase(Of ProductionDerigFormCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "PA Form - Sign Out"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptProductionDerigForm)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptProductionDerigForm"
      'cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
    End Sub

  End Class

  Public Class ProductionDerigFormCriteria
    Inherits DefaultCriteria

    <System.ComponentModel.DisplayName("Production"),
     Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Productions.ReadOnly.ROProductionList.ProductionReportCriteria),
                                          DropDownType:=Singular.DataAnnotations.DropDownWeb.SelectType.FindScreen),
     Required(ErrorMessage:="Production Required")>
    Public Property ProductionID As Integer

    Public Property SystemID As Object

  End Class

#End Region

#Region " Commentator Production PA Form "

  Public Class CommentatorProductionPAForm
    Inherits ReportBase(Of CommentatorProductionPAFormCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Commentator PA Form - Sign In"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptCommentatorProductionPAForm)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptCommentatorProductionPAForm"
      cmd.CommandTimeout = 0
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each table As DataTable In DataSet.Tables
        For Each col In table.Columns
          If col.ToString = "City" Then
            col.ExtendedProperties.Add("AutoGenerate", False)
          End If
        Next
      Next
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(CommentatorProductionPAFormCriteriaControl)
      End Get
    End Property

  End Class

  Public Class CommentatorProductionPAFormCriteria
    Inherits DefaultCriteria

    <Display(Name:="Production", Order:=1), Required(ErrorMessage:="Production is Required")>
    Public Property ProductionID As Integer?

    <Display(Name:="Read Only Production List"), ClientOnly>
    Public Property ROProductionList As List(Of ROProduction) = OBLib.CommonData.Lists.ROProductionList.ToList

    <Display(Name:="Production Ref No"), ClientOnly>
    Public Property ProductionRefNo As Integer?

  End Class

  Public Class CommentatorProductionPAFormCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of CommentatorProductionPAFormCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
            With .Helpers.FieldSet("Productions")
              .Attributes("id") = "Productions"
              With .Helpers.Div
                With .Helpers.Div
                  With .Helpers.Bootstrap.Row
                    With .Helpers.With(Of OBLib.Productions.ReadOnly.ROProductionFindList.Criteria)("ViewModel.ROProductionFindListCriteria()")
                      With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionRefNo, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Ref No")
                        End With
                      End With
                      With .Helpers.Bootstrap.Column(12, 6, 6, 6, 6)
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search By: Production Descriptions")
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.DivC("add-vertical-scroll-600")
                  With .Helpers.Bootstrap.Row
                    With .Helpers.ForEachTemplate(Of ROProduction)(Function(d) d.ROProductionList)
                      With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                          .Button.AddBinding(KnockoutBindingString.click, "OBProductionPAFormReport.AddProduction($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionRefNo + ": " + c.ProductionDescription)
                          .Button.AddClass("btn-block buttontext")
                          .Button.AddClass("add-text-align-left")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub
  End Class

#End Region

#Region " Commentator Production PA Form (New)"

  Public Class CommentatorProductionPAFormNew
    Inherits ReportBase(Of CommentatorProductionPAFormCriteriaNew)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Commentator PA Form - Sign In (New)"
      End Get
    End Property

    Public Overrides ReadOnly Property CrystalReportType As System.Type
      Get
        Return GetType(rptCommentatorProductionPAFormNew)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptCommentatorProductionPAFormNew"
      cmd.CommandTimeout = 0
    End Sub

    'Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
    '  MyBase.ModifyDataSet(DataSet)

    '  For Each table As DataTable In DataSet.Tables
    '    For Each col In table.Columns
    '      If col.ToString = "City" Then
    '        col.ExtendedProperties.Add("AutoGenerate", False)
    '      End If
    '    Next
    '  Next
    'End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(CommentatorProductionPAFormCriteriaControlNew)
      End Get
    End Property

  End Class

  Public Class CommentatorProductionPAFormCriteriaNew
    Inherits DefaultCriteria

    '<Display(Name:="Production", Order:=1)>
    'Public Property ProductionID As Integer?

    ', Required(ErrorMessage:="Production is Required")

    <Display(Name:="Read Only Production List"), ClientOnly>
    Public Property ROPSAReportList As List(Of ROPSAReport) = Nothing

    <Display(Name:="Production Ref No"), ClientOnly>
    Public Property ProductionRefNo As Integer?

    <Display(Name:="Start Date"), ClientOnly,
    SetExpression("CommentatorPAFormReportNew.StartDateSet(self)")>
    Public Property StartDate As Date?

    <Display(Name:="End Date"), ClientOnly,
    SetExpression("CommentatorPAFormReportNew.EndDateSet(self)")>
    Public Property EndDate As Date?

    <Display(Name:="Keyword"), ClientOnly,
    TextField(MultiLine:=False),
    SetExpression("CommentatorPAFormReportNew.KeywordSet(self)", , 250)>
    Public Property Keyword As String

    <Display(Name:="Production", Order:=1)>
    Public Property ProductionSystemAreaIDs As List(Of Integer)

    'Public Property ReportIsBusy As Boolean

  End Class

  Public Class CommentatorProductionPAFormCriteriaControlNew
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of CommentatorProductionPAFormCriteriaNew)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.Bootstrap.Row
          With .Helpers.Bootstrap.Column(12, 12, 4, 2, 2)
            With .Helpers.FieldSet("Areas")
              With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                  .Button.AddBinding(KnockoutBindingString.click, "CommentatorPAFormReportNew.SystemSelected($data)")
                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                  .Button.AddClass("btn-block buttontext")
                End With
                With .Helpers.Bootstrap.Row
                  .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                  '.AddBinding(KnockoutBindingString.visible, "$data.IsSelected()")
                  With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                    With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                      .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "CommentatorPAFormReportNew.ProductionAreaSelected($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.FieldSet("Dates")
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                '.Style.Width 
                '.AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                '.AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
            'With .Helpers.FieldSet("Other")

            'End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 8, 10, 10)
            With .Helpers.FieldSet("Bookings")
              With .Helpers.Div
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  '.AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.Keyword)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.Keyword, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Keyword"
                  End With
                End With
              End With
              With .Helpers.Div
                With .Helpers.Bootstrap.Button(, "Select All", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, , "fa-sort-desc", , PostBackType.None, "CommentatorPAFormReportNew.selectAll()", )
                End With
                With .Helpers.Bootstrap.Button(, "DeSelect All", BootstrapEnums.Style.DefaultStyle, , BootstrapEnums.ButtonSize.Small, , "fa-sort-asc", , PostBackType.None, "CommentatorPAFormReportNew.deselectAll()", )
                End With
              End With
              With .Helpers.ForEachTemplate(Of ROPSAReport)(Function(d) d.ROPSAReportList)
                With .Helpers.Bootstrap.Column(12, 12, 12, 6, 6)
                  With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                    .Button.AddBinding(KnockoutBindingString.click, "CommentatorPAFormReportNew.PSASelected($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As ROPSAReport) c.ProductionSystemAreaName + ": " + c.Area)
                    .Button.AddClass("btn-block buttontext")
                    .Button.AddClass("add-text-align-left")
                  End With
                End With
              End With
            End With
            With .Helpers.DivC("loading-custom")
              .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
              .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Production Tx and Rig Days"

  Public Class ProductionTxAndRigDays
    Inherits ReportBase(Of ProductionTxAndRigDaysCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Tx And Rig Days"
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionTxAndRigDaysControl)
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
      cmd.CommandText = "RptProcs.rptProductionTxAndRigDays"
    End Sub

  End Class

  Public Class ProductionTxAndRigDaysCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

  End Class

  Public Class ProductionTxAndRigDaysControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionTxAndRigDaysCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With


    End Sub
  End Class

#End Region

#Region "Production Venue Report"

  Public Class ProductionVenueReport
    Inherits Singular.Reporting.ReportBase(Of ProductionVenueReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Venue Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[RptProcs].[rptProductionVenueReport]"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionVenueReportCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ProductionVenueReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="City"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Locations.ReadOnly.ROCityList), UnselectedText:="City")>
    Public Property CityID As Integer?

    <Display(Name:="Province"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Locations.ReadOnly.ROProvinceList), UnselectedText:="Province")>
    Public Property ProvinceID As Integer?

    <Display(Name:="Production Venue"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Maintenance.Productions.ReadOnly.ROProductionVenueOldList), UnselectedText:="Production Venue")>
    Public Property ProductionVenueID As Integer?

    <Display(Name:="Production Type", Order:=1)>
    Public Property ProductionTypeID As Integer?

    <Display(Name:="Production Types"), ClientOnly>
    Public Property ProductionTypes As String

    <Display(Name:="Read Only Production Type List"), ClientOnly>
    Public Property ROProductionTypeList As List(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType) = OBLib.CommonData.Lists.ROProductionTypeList.ToList

    <Display(Name:="Read Only Event Type Report List"), ClientOnly>
    Public Property ROEventTypeListReportList As List(Of ROEventTypeReport)

    <Display(Name:="Event Type", Order:=3)>
    Public Property EventTypeID As Integer?

    <Display(Name:="Event Type", Order:=4), ClientOnly>
    Public Property EventTypes As String = ""

  End Class

  Public Class ProductionVenueReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionVenueReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
            With .Helpers.FieldSet("Production Types")
              .Attributes("id") = "Productions"
              With .Helpers.Div
                With .Helpers.Div
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                      With .Helpers.With(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport.Criteria)("ViewModel.ROProductionTypeReportPagedListCriteria()")
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord(), Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search Production Types")
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.DivC("add-vertical-scroll-600")
                  With .Helpers.Bootstrap.Row
                    With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType)(Function(d) d.ROProductionTypeList)
                      With .Helpers.Bootstrap.Column(12, 6, 6, 4, 4)
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                          .Button.AddBinding(KnockoutBindingString.click, "ProductionVenueReport.AddProductionType($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionType)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Div
                  .Helpers.LabelFor(Function(d As ProductionVenueReportCriteria) d.CityID)
                End With
                With .Helpers.Div
                  With .Helpers.Bootstrap.FormControlFor(Function(d As ProductionVenueReportCriteria) d.CityID, BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Div
                  .Helpers.LabelFor(Function(d As ProductionVenueReportCriteria) d.ProvinceID)
                End With
                With .Helpers.Div
                  With .Helpers.Bootstrap.FormControlFor(Function(d As ProductionVenueReportCriteria) d.ProvinceID, BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                With .Helpers.Div
                  .Helpers.LabelFor(Function(d As ProductionVenueReportCriteria) d.ProductionVenueID)
                End With
                With .Helpers.Div
                  With .Helpers.Bootstrap.FormControlFor(Function(d As ProductionVenueReportCriteria) d.ProductionVenueID, BootstrapEnums.InputSize.Small)
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
            With .Helpers.FieldSet("Event Types")
              With .Helpers.Div
                With .Helpers.Div
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                      With .Helpers.With(Of OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport.Criteria)("ViewModel.ROEventTypeReportPagedListCriteria()")
                        With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord(), Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search Event Types")
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.DivC("add-vertical-scroll-600")
                  With .Helpers.Bootstrap.Row
                    With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROEventType)(Function(d) d.ROEventTypeListReportList)
                      With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                          .Button.AddBinding(KnockoutBindingString.click, "ProductionVenueReport.AddEventType($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.EventType)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub

  End Class

#End Region

#Region " Production Region Report "

  Public Class ProductionRegionReport
    Inherits Singular.Reporting.ReportBase(Of ProductionRegionReportCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Region"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionProvinceReport"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
      cmd.CommandTimeout = 0

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionRegionReportCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "ProductionID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "ProductionTypeID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "EventTypeID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "ProductionVenueID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "CityID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "ProvinceID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "CountryID"
                col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

  End Class

  Public Class ProductionRegionReportCriteria
    Inherits Singular.Reporting.StartAndEndDateReportCriteria

    <Display(Name:="Production Type", Order:=3), ClientOnly>
    Public Property ProductionTypeID As Integer?

    <Display(Name:="Read Only Production Type List"), ClientOnly>
    Public Property ROProductionTypeList As List(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType) = OBLib.CommonData.Lists.ROProductionTypeList.ToList

    <Display(Name:="Production Types", Description:="", Order:=3), ClientOnly>
    Public Property ProductionTypes As String = ""

    <Display(Name:="Productions List"), ClientOnly>
    Public Property Productions As List(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType) = New List(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType)

    <Display(Name:="Disciplines", Description:="", Order:=4), ClientOnly>
    Public Property Disciplines As String = ""

    <Display(Name:="Provinces", Description:="", Order:=5), ClientOnly>
    Public Property Provinces As String = ""

    <Display(Name:="Cities", Description:="", Order:=6), ClientOnly>
    Public Property Cities As String = ""

    <Display(Name:="Production Type", Description:="", Order:=5)>
    Public Property ProductionTypeIDs As List(Of Integer)

    <Display(Name:="Discipline", Description:="", Order:=6)>
    Public Property DisciplineIDs As List(Of Integer)

    <Display(Name:="Province", Description:="", Order:=7)>
    Public Property ProvinceIDs As List(Of Integer)

    <Display(Name:="HR City", Description:="", Order:=8)>
    Public Property HRCityIDs As List(Of Integer)

  End Class

  Public Class ProductionRegionReportCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionRegionReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Production Types")
                .Attributes("id") = "Productions"
                With .Helpers.Div
                  With .Helpers.Div
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                        With .Helpers.With(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport.Criteria)("ViewModel.ROProductionTypeReportPagedListCriteria()")
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord(), Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search Production Types")
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.DivC("add-vertical-scroll-600")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType)(Function(d) d.ROProductionTypeList)
                        With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                            .Button.AddBinding(KnockoutBindingString.click, "ProductionRegionReport.AddProductionType($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionType)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Selected Production Types")
                .Attributes("id") = "SelectedProductionTypes"
                With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType)(Function(d) d.Productions)
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                    With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", , , , , )
                      .Button.AddBinding(KnockoutBindingString.click, "ProductionRegionReport.AddProductionType($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionType)
                      .Button.AddClass("btn-block buttontext")
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Column(12, 12, 12, 12)
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                With .Helpers.Div
                  .Helpers.LabelFor(Function(d As ProductionRegionReportCriteria) d.ProvinceIDs)
                End With
                With .Helpers.Div
                  With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionRegionReportCriteria) d.Provinces, "ProductionRegionReport.FindProvinces($element)",
                                                         "Search For Provinces", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "ProductionRegionReport.ClearProvinces()")
                  End With
                End With
                With .Helpers.Bootstrap.Row
                  With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                    With .Helpers.Div
                      .Helpers.LabelFor(Function(d As ProductionRegionReportCriteria) d.DisciplineIDs)
                    End With
                    With .Helpers.Div
                      With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionRegionReportCriteria) d.Disciplines, "ProductionRegionReport.GetDiscipline($element)",
                                                             "Search For Disciplines", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "ProductionRegionReport.ClearDisciplines()")
                      End With
                    End With
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 12, 12, 12)
                        With .Helpers.Div
                          .Helpers.LabelFor(Function(d As ProductionRegionReportCriteria) d.Cities)
                        End With
                        With .Helpers.Div
                          With .Helpers.Bootstrap.ReadOnlyComboFor(Function(d As ProductionRegionReportCriteria) d.Cities, "ProductionRegionReport.FindCities($element)",
                                                                 "Search For Cities", , BootstrapEnums.Style.DefaultStyle, "fa-search", True, "ProductionRegionReport.ClearCities()")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub
  End Class
#End Region

#Region " Production Services - Production Weekend Report "

  Public Class ProductionServicesWeekendProductionReport
    Inherits ReportBase(Of ProductionServicesWeekendProductionReportCriteria)

#Region " HeaderNames "
    Private Class HeaderNames

      Private mHeaderName As New List(Of String)(New String() {"Date", "Venue", "City", "OB Unit", "Event", "Lang", "TX Time", "Name", "Tel No", _
                                                               "Name", "Tel No", "Trav Day", "Rig Day", "Mode", "Accom", "Trav Day", "Mode", "Accom"})
      Private mCurrentHeader As Integer = -1

      Public Function GetNextHeaderName() As String
        mCurrentHeader += 1
        Return (mHeaderName.Item(mCurrentHeader))
      End Function

      Public Function GetCount() As Integer
        Return mHeaderName.Count()
      End Function

    End Class
#End Region

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Weekend Production Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptWeekendProductionReport"
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionServicesWeekendProductionReportCriteriaControl)
      End Get
    End Property

    Private Const WorksheetName As String = "Weekend Production Report"
    Private Const DefaultColumnWidth As Decimal = 17.5
    Private ColumnWidths As New Dictionary(Of String, System.Drawing.Size)

    Public Class ProductionServicesWeekendProductionReportCriteria
      Inherits StartAndEndDateReportCriteria

      <Display(Name:="System"), Required(ErrorMessage:="System is required")>
      Public Property SystemID As Integer?

      <Display(Name:="Production Area"), Required(ErrorMessage:="Production Area is required")>
      Public Property ProductionAreaID As Integer?

    End Class

    Public Class ProductionServicesWeekendProductionReportCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase
      Protected Overrides Sub Setup()
        MyBase.Setup()
        With Helpers.With(Of ProductionServicesWeekendProductionReportCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Date Selection")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
              With .Helpers.FieldSet("Sub-Depts and Areas")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "WeekendProductionReport.AddSystem($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                    .Button.AddClass("btn-block buttontext")
                  End With
                  With .Helpers.Bootstrap.Row
                    .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                      With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                        .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "WeekendProductionReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
        End With

      End Sub

    End Class

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim HeaderName As New HeaderNames

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add(WorksheetName)

      'Freeze columns
      ws.DisplayOptions.PanesAreFrozen = True
      ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 8
      ws.DisplayOptions.FrozenPaneSettings.FrozenRows = 2

      'Add borders to cells
      SetCellBorders(ws, 0, HeaderName.GetCount - 1, 0, ((Data.Tables.Item(0).Rows.Count) * 4) + 1, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      SetCellBorders(ws, 0, HeaderName.GetCount - 1, 0, 1, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Medium, CellBorderLineStyle.Medium, CellBorderLineStyle.Medium, CellBorderLineStyle.Medium)

      Dim MergeCellStartPoint As Integer = 2
      Dim MergeCellEndPoint As Integer = 1

      'Data.Tables.Item(0).Rows(0).ItemArray - First table with the first row
      'Setup Merge Cells First
      Dim CountEventDates As Integer = 0
      For Col As Integer = 0 To (HeaderName.GetCount - 1)
        ws.Columns(Col).CellFormat.WrapText = ExcelDefaultableBoolean.True

        If (Col = 0) Then
          ws.Columns(Col).Width = DefaultColumnWidth * 140
        ElseIf (Col > 4 AndAlso Col < 7) Then 'Date, HD/SD, Lang and TX Time columns are narrower
          ws.Columns(Col).Width = DefaultColumnWidth * 100
        Else
          ws.Columns(Col).Width = DefaultColumnWidth * 225
        End If

        For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count) 'Loop through number of Events (first 2 rows are for headings)

          If Row = 0 Then
            If Col < 7 Then 'Merge Header Rows
              SetText(ws, Row, Col, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              ws.MergedCellsRegions.Add(0, Col, 1, Col)
            ElseIf (Col = 7 Or Col = 9) Then
              Select Case Col
                Case 7
                  SetText(ws, Row, Col, "Management", ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                Case 9
                  SetText(ws, Row, Col, "Drivers", ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              End Select
              ws.MergedCellsRegions.Add(0, Col, 0, Col + 1)
            ElseIf Col = 11 Then
              SetText(ws, Row, Col, "Rig Crew", ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              ws.MergedCellsRegions.Add(0, Col, 0, Col + 3)
            ElseIf Col = 15 Then
              SetText(ws, Row, Col, "Ops Crew", ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              ws.MergedCellsRegions.Add(0, Col, 0, Col + 2)
            End If

          Else
            CountEventDates += 1
            If Col = 0 Then 'Dates need to merge across all Events for that date
              If Not Row - 1 = Data.Tables.Item(0).Rows.Count - 1 Then
                If Not Data.Tables.Item(0).Rows(Row - 1).ItemArray(Col) = Data.Tables.Item(0).Rows(Row).ItemArray(Col) Then
                  Dim EventDate As String = CType(Data.Tables.Item(0).Rows(Row - 1).ItemArray(Col), Date).ToString("ddd d MMMM")
                  SetText(ws, MergeCellStartPoint, Col, EventDate, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center) 'Inserting dates
                  MergeCellEndPoint = MergeCellEndPoint + ((CountEventDates) * 4)
                  ws.MergedCellsRegions.Add(MergeCellStartPoint, 0, MergeCellEndPoint, 0)
                  MergeCellStartPoint = MergeCellEndPoint + 1
                  CountEventDates = 0
                End If
              Else '
                SetText(ws, MergeCellStartPoint, Col, CType(Data.Tables.Item(0).Rows(Row - 1).ItemArray(Col), Date).ToString("ddd d MMMM"), ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center) 'Inserting dates
                MergeCellEndPoint = MergeCellEndPoint + ((CountEventDates) * 4)
                ws.MergedCellsRegions.Add(MergeCellStartPoint, 0, MergeCellEndPoint, 0)
              End If

              'Trying to insert data at the same time as merging cells
            ElseIf (Col > 0 AndAlso Col < 7) Then
              If Col = 6 Then 'TX Time needs to be formatted
                SetText(ws, MergeCellStartPoint, Col, CType(Data.Tables.Item(0).Rows(Row - 1).ItemArray(Col), DateTime).ToString("HH:mm"), ExcelDefaultableBoolean.False, , , HorizontalCellAlignment.Center, , VerticalCellAlignment.Center) ' Inserting Data
              Else
                SetText(ws, MergeCellStartPoint, Col, Data.Tables.Item(0).Rows(Row - 1).ItemArray(Col), ExcelDefaultableBoolean.False, , , HorizontalCellAlignment.Center, , VerticalCellAlignment.Center) ' Inserting Data
              End If

              ws.MergedCellsRegions.Add(MergeCellStartPoint, Col, MergeCellEndPoint, Col)
              MergeCellStartPoint = MergeCellEndPoint + 1
              MergeCellEndPoint += 4

            ElseIf Col >= 7 Then
              If Row = 1 Then
                SetText(ws, Row, Col, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              End If
              If (Col > 11) Then
                ws.MergedCellsRegions.Add(MergeCellStartPoint, Col, MergeCellEndPoint, Col)
                MergeCellStartPoint = MergeCellEndPoint + 1
                MergeCellEndPoint += 4
              End If

            End If

          End If

        Next
        MergeCellStartPoint = 2
        MergeCellEndPoint = 5
      Next

      'Fill Heading Cells
      FormatCells(ws, 0, HeaderName.GetCount - 1, 0, 1, System.Drawing.Color.LightGray, FillPatternStyle.Solid)

      'Display Management Data
      Dim RowNumber As Integer = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        Dim EventManager As String = ""
        Dim EventManagerNo As String = ""
        Dim UnitSupervisor As String = ""
        Dim UnitSupervisorNo As String = ""
        Dim Director As String = ""
        Dim DirectorNo As String = ""
        Dim AudioMixer As String = ""
        Dim AudioMixerNo As String = ""
        Dim PositionID As Integer? = Nothing

        For i As Integer = 0 To Data.Tables.Item(1).Rows.Count - 1
          If Data.Tables.Item(1).Rows(i).ItemArray(2) = Data.Tables.Item(0).Rows(Row).ItemArray(7) Then

            Select Case Data.Tables.Item(1).Rows(i).ItemArray(5)
              Case 2
                AudioMixer = Data.Tables.Item(1).Rows(i).ItemArray(0) + " : Mixer"
                AudioMixerNo = Data.Tables.Item(1).Rows(i).ItemArray(1)

              Case 7
                EventManager = Data.Tables.Item(1).Rows(i).ItemArray(0) + " : E"
                EventManagerNo = Data.Tables.Item(1).Rows(i).ItemArray(1)

              Case 6
                'UnitSupervisor = Data.Tables.Item(1).Rows(i).ItemArray(0) + " : U"
                'UnitSupervisorNo = Data.Tables.Item(1).Rows(i).ItemArray(1)
                PositionID = Data.Tables.Item(1).Rows(i).ItemArray(9)
                If PositionID = 50 Then
                  UnitSupervisor = Data.Tables.Item(1).Rows(i).ItemArray(0) + " : U"
                  UnitSupervisorNo = Data.Tables.Item(1).Rows(i).ItemArray(1)
                End If

              Case 37
                If Director = "" Then
                  Director = Data.Tables.Item(1).Rows(i).ItemArray(0) + " : D"
                  DirectorNo = Data.Tables.Item(1).Rows(i).ItemArray(1)
                End If

              Case 999
                Director = Data.Tables.Item(1).Rows(i).ItemArray(0) + " : D"
                DirectorNo = Data.Tables.Item(1).Rows(i).ItemArray(1)

            End Select
          End If
        Next

        SetText(ws, RowNumber, 7, EventManager, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        SetText(ws, RowNumber, 8, EventManagerNo, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        RowNumber += 1

        SetText(ws, RowNumber, 7, UnitSupervisor, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        SetText(ws, RowNumber, 8, UnitSupervisorNo, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        RowNumber += 1

        SetText(ws, RowNumber, 7, Director, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        SetText(ws, RowNumber, 8, DirectorNo, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        RowNumber += 1

        SetText(ws, RowNumber, 7, AudioMixer, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        SetText(ws, RowNumber, 8, AudioMixerNo, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        RowNumber += 1
      Next

      'Display Driver Data
      RowNumber = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        If Not Data.Tables.Item(0).Rows(Row).ItemArray(13) Then 'Check if Vehicles are external or not
          For i As Integer = 0 To Data.Tables.Item(2).Rows.Count - 1
            If Data.Tables.Item(2).Rows(i).ItemArray(2) = Data.Tables.Item(0).Rows(Row).ItemArray(7) AndAlso Count < 4 Then
              SetText(ws, RowNumber, 9, Data.Tables.Item(2).Rows(i).ItemArray(0), ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              SetText(ws, RowNumber, 10, Data.Tables.Item(2).Rows(i).ItemArray(1), ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              RowNumber += 1
              Count += 1
            End If
          Next
          RowNumber += (4 - Count)
        Else
          For k As Integer = 0 To 3
            SetText(ws, RowNumber, 9, "EXTERNAL", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
            RowNumber += 1
          Next
        End If
      Next

      'CType(Data.Tables.Item(0).Rows(Row - 1).ItemArray(Col), Date).ToString("ddd d MMMM")
      'Rig Crew Travel Day
      RowNumber = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        Dim RigCrewTravDayInd As Boolean = False
        For i As Integer = 0 To Data.Tables.Item(3).Rows.Count - 1
          If Data.Tables.Item(3).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(7) AndAlso Data.Tables.Item(3).Rows(i).ItemArray(1) = 2 Then
            SetText(ws, RowNumber, 11, CType(Data.Tables.Item(3).Rows(i).ItemArray(2), Date).ToString("ddd d MMMM"), ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
            RowNumber += 4
            RigCrewTravDayInd = True
          End If
        Next
        If RigCrewTravDayInd = False Then
          SetText(ws, RowNumber, 11, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          RowNumber += 4
        End If
      Next

      'Rig Crew Rig Day
      RowNumber = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        Dim FirstInd As Boolean = False
        Dim FirstRigDay As String = ""
        Dim LastRigDay As String = ""
        For i As Integer = 0 To Data.Tables.Item(4).Rows.Count - 1
          If Data.Tables.Item(4).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(7) Then
            If FirstInd = False Then
              FirstInd = True
              FirstRigDay = CType(Data.Tables.Item(4).Rows(i).ItemArray(1), Date).ToString("ddd d MMMM")
            Else
              LastRigDay = CType(Data.Tables.Item(4).Rows(i).ItemArray(1), Date).ToString("ddd d MMMM")
            End If
          End If
        Next

        If FirstRigDay = "" Then
          SetText(ws, RowNumber, 12, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        ElseIf LastRigDay = "" Then
          SetText(ws, RowNumber, 12, FirstRigDay, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        Else
          SetText(ws, RowNumber, 12, FirstRigDay + " - " + LastRigDay, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        End If
        RowNumber += 4
      Next

      'Rig Crew Travel Mode
      RowNumber = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        Dim RigCrewTravModeInd As Boolean = False
        Dim RigCrewMode As String = ""
        For i As Integer = 0 To Data.Tables.Item(5).Rows.Count - 1
          If Data.Tables.Item(5).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(7) AndAlso Data.Tables.Item(5).Rows(i).ItemArray(1) = 2 Then
            If RigCrewTravModeInd Then
              RigCrewMode += " & "
            End If
            RigCrewMode = RigCrewMode + Data.Tables.Item(5).Rows(i).ItemArray(2)
            RigCrewTravModeInd = True
          End If
        Next
        If RigCrewTravModeInd = False Then
          SetText(ws, RowNumber, 13, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        Else
          SetText(ws, RowNumber, 13, RigCrewMode, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        End If
        RowNumber += 4
      Next

      'Rig Crew Accommodation
      RowNumber = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        Dim RigCrewAccomInd As Boolean = False
        Dim RigCrewAccom As String = ""
        For i As Integer = 0 To Data.Tables.Item(6).Rows.Count - 1
          If Data.Tables.Item(6).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(7) AndAlso Data.Tables.Item(6).Rows(i).ItemArray(2) = 2 Then
            If RigCrewAccomInd Then
              RigCrewAccom += " & "
            End If
            RigCrewAccom = RigCrewAccom + Data.Tables.Item(6).Rows(i).ItemArray(1)
            RigCrewAccomInd = True
          End If
        Next
        If RigCrewAccomInd = False Then
          SetText(ws, RowNumber, 14, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        Else
          SetText(ws, RowNumber, 14, RigCrewAccom, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        End If
        RowNumber += 4
      Next

      'Ops Crew Travel Day
      RowNumber = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        Dim OpsCrewTravDayInd As Boolean = False
        For i As Integer = 0 To Data.Tables.Item(3).Rows.Count - 1
          If Data.Tables.Item(3).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(7) AndAlso Data.Tables.Item(3).Rows(i).ItemArray(1) = 3 Then
            SetText(ws, RowNumber, 15, Data.Tables.Item(3).Rows(i).ItemArray(2), ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
            RowNumber += 4
            OpsCrewTravDayInd = True
          End If
        Next
        If OpsCrewTravDayInd = False Then
          SetText(ws, RowNumber, 15, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          RowNumber += 4
        End If
      Next

      'Ops Crew Travel Mode
      RowNumber = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        Dim OpsCrewTravModeInd As Boolean = False
        Dim OpsCrewMode As String = ""
        For i As Integer = 0 To Data.Tables.Item(5).Rows.Count - 1
          If Data.Tables.Item(5).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(7) AndAlso Data.Tables.Item(5).Rows(i).ItemArray(1) = 3 Then
            If OpsCrewTravModeInd Then
              OpsCrewMode += " & "
            End If
            OpsCrewMode = OpsCrewMode + Data.Tables.Item(5).Rows(i).ItemArray(2)
            OpsCrewTravModeInd = True
          End If
        Next
        If OpsCrewTravModeInd = False Then
          SetText(ws, RowNumber, 16, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        Else
          SetText(ws, RowNumber, 16, OpsCrewMode, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        End If
        RowNumber += 4
      Next

      'Ops Crew Accommodation
      RowNumber = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        Dim OpsCrewTravModeInd As Boolean = False
        Dim OpsCrewAccom As String = ""
        For i As Integer = 0 To Data.Tables.Item(6).Rows.Count - 1
          If Data.Tables.Item(6).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(7) AndAlso Data.Tables.Item(6).Rows(i).ItemArray(2) = 3 Then
            If OpsCrewTravModeInd Then
              OpsCrewAccom += " & "
            End If
            OpsCrewAccom = OpsCrewAccom + Data.Tables.Item(6).Rows(i).ItemArray(1)
            OpsCrewTravModeInd = True
          End If
        Next
        If OpsCrewTravModeInd = False Then
          SetText(ws, RowNumber, 17, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        Else
          SetText(ws, RowNumber, 17, OpsCrewAccom, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        End If
        RowNumber += 4
      Next

      Return wb

    End Function

    Private Sub SetText(ByVal ws As Worksheet, ByVal Row As Integer, ByVal Column As Integer, ByVal Text As String, _
                       Optional ByVal Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.Default, _
                       Optional ByVal FontName As String = "Arial", Optional ByVal FontSize As Integer = 8, _
                       Optional ByVal HAlign As HorizontalCellAlignment = HorizontalCellAlignment.Center, _
                       Optional ByVal FontUnderlineStyle As FontUnderlineStyle = FontUnderlineStyle.None, _
                       Optional ByVal VerticalAlignment As VerticalCellAlignment = VerticalCellAlignment.Default)

      If Integer.TryParse(Text, 0) Then
        ws.Rows(Row).Cells(Column).Value = CInt(Text)
      Else
        ws.Rows(Row).Cells(Column).Value = Text
      End If

      ws.Rows(Row).Cells(Column).CellFormat.Font.Bold = Bold
      ws.Rows(Row).Cells(Column).CellFormat.Font.Name = FontName
      ws.Rows(Row).Cells(Column).CellFormat.Font.Height = FontSize * 20
      ws.Rows(Row).Cells(Column).CellFormat.Alignment = HAlign
      ws.Rows(Row).Cells(Column).CellFormat.Font.UnderlineStyle = FontUnderlineStyle
      ws.Rows(Row).Cells(Column).CellFormat.VerticalAlignment = VerticalAlignment

      Dim CellKey As String = ws.Rows(Row).Cells(Column).ToString
      Dim size = New System.Drawing.Size
      If Not ColumnWidths.ContainsKey(CellKey) Then
        size.Width = IIf(ws.Columns(Column).Width = -1, ws.DefaultColumnWidth, ws.Columns(Column).Width) / 256.0
        size.Height = IIf(ws.Rows(Row).Height = -1, ws.DefaultRowHeight, ws.Rows(Row).Height) / 20.0
        ColumnWidths.Add(CellKey, size)
      Else
        size = ColumnWidths(CellKey)
      End If

      '''dummy label to get a graphics object to measure the width (in pixels) of a string
      'Dim l As New System.Windows.Forms.Label
      'l.Text = Text
      'l.Size = size
      'Dim g As System.Drawing.Graphics = l.CreateGraphics
      'Dim s As System.Drawing.SizeF
      ''For Each cell As Infragistics.Win.UltraWinGrid.UltraGridCell In Row.Cells
      'Dim iFont = ws.Rows(Row).Cells(Column).CellFormat.Font
      'Dim windowsFont = New Drawing.Font(iFont.Name, iFont.Height / 20.0)
      's = g.MeasureString(Text, windowsFont)
      'size.Width = IIf(CInt(size.Width) < CInt(s.Width), CInt(s.Width), size.Width)

    End Sub

    Private Sub SetCellBorders(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, ByVal ReplaceExistingBorders As Boolean, _
                              ByVal TopBorderColor As System.Drawing.Color, ByVal BottomBorderColor As System.Drawing.Color, _
                              ByVal LeftBorderColor As System.Drawing.Color, ByVal RightBorderColor As System.Drawing.Color, _
                              Optional ByVal TopBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal BottomBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal LeftBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal RightBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal DiagonalBorder As DiagonalBorders = DiagonalBorders.None, Optional DiagonalBorderStyle As CellBorderLineStyle = CellBorderLineStyle.None,
                              Optional ByVal DiagonalBorderColor As System.Drawing.Color = Nothing)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = TopBorderStyle
            ws.Rows(i).Cells(j).CellFormat.TopBorderColor = TopBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = BottomBorderStyle
            ws.Rows(i).Cells(j).CellFormat.BottomBorderColor = BottomBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = LeftBorderStyle
            ws.Rows(i).Cells(j).CellFormat.LeftBorderColor = LeftBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = RightBorderStyle
            ws.Rows(i).Cells(j).CellFormat.RightBorderColor = RightBorderColor
          End If
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorders = DiagonalBorder
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderColor = DiagonalBorderColor

        Next
      Next
    End Sub

    Private Sub FormatCells(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, _
                           ByVal FillColor As System.Drawing.Color, ByVal FillPattern As FillPatternStyle)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          ws.Rows(i).Cells(j).CellFormat.FillPatternBackgroundColor = FillColor
          Select Case FillPattern
            Case FillPatternStyle.Solid, FillPatternStyle.Default, FillPatternStyle.None
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = FillColor
            Case Else
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = System.Drawing.Color.Black
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
          End Select
        Next
      Next

    End Sub

  End Class

#End Region

#Region " Production Content - Production Weekend Report "

  Public Class ProductionContentWeekendProductionReport
    Inherits ReportBase(Of StartAndEndDateReportCriteria)

#Region " HeaderNames "
    Private Class HeaderNames

      Private mHeaderName As New List(Of String)(New String() {"Date", "Venue", "City", "OB Unit", "Event", "HD/SD", "Lang", "TX Time", "Name", "Tel No", _
                                                               "Name", "Tel No", "Trav Day", "Mode", "Accom"})
      Private mCurrentHeader As Integer = -1

      Public Function GetNextHeaderName() As String
        mCurrentHeader += 1
        Return (mHeaderName.Item(mCurrentHeader))
      End Function

      Public Function GetCount() As Integer
        Return mHeaderName.Count()
      End Function

    End Class
#End Region

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Weekend Production Report"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionWeekendProductionReport"
      cmd.CommandTimeout = 0
    End Sub

    Private Const WorksheetName As String = "Weekend Production Report"
    Private Const DefaultColumnWidth As Decimal = 17.5
    Private ColumnWidths As New Dictionary(Of String, System.Drawing.Size)

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim HeaderName As New HeaderNames

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add(WorksheetName)

      'Freeze columns
      ws.DisplayOptions.PanesAreFrozen = True
      ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 8
      ws.DisplayOptions.FrozenPaneSettings.FrozenRows = 2

      'Add borders to cells
      SetCellBorders(ws, 0, HeaderName.GetCount - 1, 0, ((Data.Tables.Item(0).Rows.Count) * 4) + 1, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)
      SetCellBorders(ws, 0, HeaderName.GetCount - 1, 0, 1, True, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Medium, CellBorderLineStyle.Medium, CellBorderLineStyle.Medium, CellBorderLineStyle.Medium)

      Dim MergeCellStartPoint As Integer = 2
      Dim MergeCellEndPoint As Integer = 1

      'Data.Tables.Item(0).Rows(0).ItemArray - First table with the first row
      'Setup Merge Cells First
      Dim CountEventDates As Integer = 0
      For Col As Integer = 0 To (HeaderName.GetCount - 1)
        ws.Columns(Col).CellFormat.WrapText = ExcelDefaultableBoolean.True

        If (Col = 0) Then
          ws.Columns(Col).Width = DefaultColumnWidth * 140
        ElseIf (Col > 4 AndAlso Col < 8) Then 'Date, HD/SD, Lang and TX Time columns are narrower
          ws.Columns(Col).Width = DefaultColumnWidth * 100
        Else
          ws.Columns(Col).Width = DefaultColumnWidth * 225
        End If

        For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count) 'Loop through number of Events (first 2 rows are for headings)

          If Row = 0 Then
            If Col < 8 Then 'Merge Header Rows
              SetText(ws, Row, Col, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              ws.MergedCellsRegions.Add(0, Col, 1, Col)
            ElseIf (Col = 8 Or Col = 10) Then
              Select Case Col
                Case 8
                  SetText(ws, Row, Col, "Management", ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
                Case 10
                  SetText(ws, Row, Col, "Directors", ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              End Select
              ws.MergedCellsRegions.Add(0, Col, 0, Col + 1)
            ElseIf Col = 12 Then
              SetText(ws, Row, Col, "Production Crew", ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              ws.MergedCellsRegions.Add(0, Col, 0, Col + 2)
            End If

          Else
            CountEventDates += 1
            If Col = 0 Then 'Dates need to merge across all Events for that date
              If Not Row - 1 = Data.Tables.Item(0).Rows.Count - 1 Then
                If Not Data.Tables.Item(0).Rows(Row - 1).ItemArray(Col) = Data.Tables.Item(0).Rows(Row).ItemArray(Col) Then
                  Dim EventDate As String = CType(Data.Tables.Item(0).Rows(Row - 1).ItemArray(Col), Date).ToString("ddd d MMMM")
                  SetText(ws, MergeCellStartPoint, Col, EventDate, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center) 'Inserting dates
                  MergeCellEndPoint = MergeCellEndPoint + ((CountEventDates) * 4)
                  ws.MergedCellsRegions.Add(MergeCellStartPoint, 0, MergeCellEndPoint, 0)
                  MergeCellStartPoint = MergeCellEndPoint + 1
                  CountEventDates = 0
                End If
              Else '
                SetText(ws, MergeCellStartPoint, Col, CType(Data.Tables.Item(0).Rows(Row - 1).ItemArray(Col), Date).ToString("ddd d MMMM"), ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center) 'Inserting dates
                MergeCellEndPoint = MergeCellEndPoint + ((CountEventDates) * 4)
                ws.MergedCellsRegions.Add(MergeCellStartPoint, 0, MergeCellEndPoint, 0)
              End If

              'Trying to insert data at the same time as merging cells
            ElseIf (Col > 0 AndAlso Col < 8) Then
              If Col = 7 Then 'TX Time nees to be formatted
                SetText(ws, MergeCellStartPoint, Col, CType(Data.Tables.Item(0).Rows(Row - 1).ItemArray(Col), DateTime).ToString("HH:mm"), ExcelDefaultableBoolean.False, , , HorizontalCellAlignment.Center, , VerticalCellAlignment.Center) ' Inserting Data
              Else
                SetText(ws, MergeCellStartPoint, Col, Data.Tables.Item(0).Rows(Row - 1).ItemArray(Col), ExcelDefaultableBoolean.False, , , HorizontalCellAlignment.Center, , VerticalCellAlignment.Center) ' Inserting Data
              End If

              ws.MergedCellsRegions.Add(MergeCellStartPoint, Col, MergeCellEndPoint, Col)
              MergeCellStartPoint = MergeCellEndPoint + 1
              MergeCellEndPoint += 4

            ElseIf Col > 7 Then
              If Row = 1 Then
                SetText(ws, Row, Col, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
              End If
              If (Col > 11) Then
                ws.MergedCellsRegions.Add(MergeCellStartPoint, Col, MergeCellEndPoint, Col)
                MergeCellStartPoint = MergeCellEndPoint + 1
                MergeCellEndPoint += 4
              End If

            End If

          End If

        Next
        MergeCellStartPoint = 2
        MergeCellEndPoint = 5
      Next

      'Fill Heading Cells
      FormatCells(ws, 0, HeaderName.GetCount - 1, 0, 1, System.Drawing.Color.LightGray, FillPatternStyle.Solid)

      'Display Management Data
      Dim RowNumber As Integer = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        Dim EventManager As String = ""
        Dim EventManagerNo As String = ""
        Dim UnitSupervisor As String = ""
        Dim UnitSupervisorNo As String = ""
        Dim Director As String = ""
        Dim DirectorNo As String = ""
        Dim AudioMixer As String = ""
        Dim AudioMixerNo As String = ""

        For i As Integer = 0 To Data.Tables.Item(1).Rows.Count - 1
          If Data.Tables.Item(1).Rows(i).ItemArray(2) = Data.Tables.Item(0).Rows(Row).ItemArray(8) Then

            Select Case Data.Tables.Item(1).Rows(i).ItemArray(5)
              Case 2
                AudioMixer = Data.Tables.Item(1).Rows(i).ItemArray(0) + " : Mixer"
                AudioMixerNo = Data.Tables.Item(1).Rows(i).ItemArray(1)

              Case 7
                EventManager = Data.Tables.Item(1).Rows(i).ItemArray(0) + " : E"
                EventManagerNo = Data.Tables.Item(1).Rows(i).ItemArray(1)

              Case 11
                UnitSupervisor = Data.Tables.Item(1).Rows(i).ItemArray(0) + " : U"
                UnitSupervisorNo = Data.Tables.Item(1).Rows(i).ItemArray(1)

              Case 999
                If Director = "" Then
                  Director = Data.Tables.Item(1).Rows(i).ItemArray(0) + " : D"
                  DirectorNo = Data.Tables.Item(1).Rows(i).ItemArray(1)
                End If

            End Select
          End If
        Next

        SetText(ws, RowNumber, 8, EventManager, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        SetText(ws, RowNumber, 9, EventManagerNo, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        RowNumber += 1

        SetText(ws, RowNumber, 8, UnitSupervisor, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        SetText(ws, RowNumber, 9, UnitSupervisorNo, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        RowNumber += 1

        SetText(ws, RowNumber, 8, Director, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        SetText(ws, RowNumber, 9, DirectorNo, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        RowNumber += 1

        SetText(ws, RowNumber, 8, AudioMixer, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        SetText(ws, RowNumber, 9, AudioMixerNo, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        RowNumber += 1
      Next

      'Display Director Data
      RowNumber = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        'If Not Data.Tables.Item(0).Rows(Row).ItemArray(14) Then 'Check if Vehicles are external or not
        For i As Integer = 0 To Data.Tables.Item(2).Rows.Count - 1
          If Data.Tables.Item(2).Rows(i).ItemArray(2) = Data.Tables.Item(0).Rows(Row).ItemArray(8) AndAlso Count < 4 Then
            SetText(ws, RowNumber, 10, Data.Tables.Item(2).Rows(i).ItemArray(0), ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
            SetText(ws, RowNumber, 11, Data.Tables.Item(2).Rows(i).ItemArray(1), ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
            RowNumber += 1
            Count += 1
          End If
        Next
        RowNumber += (4 - Count)
        'Else
        'For k As Integer = 0 To 3
        '  SetText(ws, RowNumber, 10, "EXTERNAL", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        '  RowNumber += 1
        'Next
        'End If
      Next

      'CType(Data.Tables.Item(0).Rows(Row - 1).ItemArray(Col), Date).ToString("ddd d MMMM")
      ''Rig Crew Travel Day
      'RowNumber = 2
      'For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
      '  Dim Count As Integer = 0
      '  Dim RigCrewTravDayInd As Boolean = False
      '  For i As Integer = 0 To Data.Tables.Item(3).Rows.Count - 1
      '    If Data.Tables.Item(3).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(8) AndAlso Data.Tables.Item(3).Rows(i).ItemArray(1) = 2 Then
      '      SetText(ws, RowNumber, 12, CType(Data.Tables.Item(3).Rows(i).ItemArray(2), Date).ToString("ddd d MMMM"), ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
      '      RowNumber += 4
      '      RigCrewTravDayInd = True
      '    End If
      '  Next
      '  If RigCrewTravDayInd = False Then
      '    SetText(ws, RowNumber, 12, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
      '    RowNumber += 4
      '  End If
      'Next

      '''Rig Crew Rig Day
      ''RowNumber = 2
      ''For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
      ''  Dim Count As Integer = 0
      ''  Dim FirstInd As Boolean = False
      ''  Dim FirstRigDay As String = ""
      ''  Dim LastRigDay As String = ""
      ''  For i As Integer = 0 To Data.Tables.Item(4).Rows.Count - 1
      ''    If Data.Tables.Item(4).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(8) Then
      ''      If FirstInd = False Then
      ''        FirstInd = True
      ''        FirstRigDay = CType(Data.Tables.Item(4).Rows(i).ItemArray(1), Date).ToString("ddd d MMMM")
      ''      Else
      ''        LastRigDay = CType(Data.Tables.Item(4).Rows(i).ItemArray(1), Date).ToString("ddd d MMMM")
      ''      End If
      ''    End If
      ''  Next

      ''  If FirstRigDay = "" Then
      ''    SetText(ws, RowNumber, 13, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
      ''  ElseIf LastRigDay = "" Then
      ''    SetText(ws, RowNumber, 13, FirstRigDay, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
      ''  Else
      ''    SetText(ws, RowNumber, 13, FirstRigDay + " - " + LastRigDay, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
      ''  End If
      ''  RowNumber += 4
      ''Next

      ''Rig Crew Travel Mode
      'RowNumber = 2
      'For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
      '  Dim Count As Integer = 0
      '  Dim RigCrewTravModeInd As Boolean = False
      '  Dim RigCrewMode As String = ""
      '  For i As Integer = 0 To Data.Tables.Item(5).Rows.Count - 1
      '    If Data.Tables.Item(5).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(8) AndAlso Data.Tables.Item(5).Rows(i).ItemArray(1) = 2 Then
      '      If RigCrewTravModeInd Then
      '        RigCrewMode += " & "
      '      End If
      '      RigCrewMode = RigCrewMode + Data.Tables.Item(5).Rows(i).ItemArray(2)
      '      RigCrewTravModeInd = True
      '    End If
      '  Next
      '  If RigCrewTravModeInd = False Then
      '    SetText(ws, RowNumber, 14, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
      '  Else
      '    SetText(ws, RowNumber, 14, RigCrewMode, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
      '  End If
      '  RowNumber += 4
      'Next

      ''Rig Crew Accommodation
      'RowNumber = 2
      'For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
      '  Dim Count As Integer = 0
      '  Dim RigCrewAccomInd As Boolean = False
      '  Dim RigCrewAccom As String = ""
      '  For i As Integer = 0 To Data.Tables.Item(6).Rows.Count - 1
      '    If Data.Tables.Item(6).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(8) AndAlso Data.Tables.Item(6).Rows(i).ItemArray(2) = 2 Then
      '      If RigCrewAccomInd Then
      '        RigCrewAccom += " & "
      '      End If
      '      RigCrewAccom = RigCrewAccom + Data.Tables.Item(6).Rows(i).ItemArray(1)
      '      RigCrewAccomInd = True
      '    End If
      '  Next
      '  If RigCrewAccomInd = False Then
      '    SetText(ws, RowNumber, 15, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
      '  Else
      '    SetText(ws, RowNumber, 15, RigCrewAccom, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
      '  End If
      '  RowNumber += 4
      'Next

      'Ops Crew Travel Day
      RowNumber = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        Dim OpsCrewTravDayInd As Boolean = False
        For i As Integer = 0 To Data.Tables.Item(3).Rows.Count - 1
          If Data.Tables.Item(3).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(8) AndAlso Data.Tables.Item(3).Rows(i).ItemArray(1) = CInt(OBLib.CommonData.Enums.CrewType.Production) Then
            SetText(ws, RowNumber, 12, Data.Tables.Item(3).Rows(i).ItemArray(2), ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
            RowNumber += 4
            OpsCrewTravDayInd = True
          End If
        Next
        If OpsCrewTravDayInd = False Then
          SetText(ws, RowNumber, 12, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
          RowNumber += 4
        End If
      Next

      'Ops Crew Travel Mode
      RowNumber = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        Dim OpsCrewTravModeInd As Boolean = False
        Dim OpsCrewMode As String = ""
        For i As Integer = 0 To Data.Tables.Item(4).Rows.Count - 1
          If Data.Tables.Item(4).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(8) AndAlso Data.Tables.Item(4).Rows(i).ItemArray(1) = CInt(OBLib.CommonData.Enums.CrewType.Production) Then
            If OpsCrewTravModeInd Then
              OpsCrewMode += " & "
            End If
            OpsCrewMode = OpsCrewMode + Data.Tables.Item(4).Rows(i).ItemArray(2)
            OpsCrewTravModeInd = True
          End If
        Next
        If OpsCrewTravModeInd = False Then
          SetText(ws, RowNumber, 13, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        Else
          SetText(ws, RowNumber, 13, OpsCrewMode, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        End If
        RowNumber += 4
      Next

      'Ops Crew Accommodation
      RowNumber = 2
      For Row As Integer = 0 To (Data.Tables.Item(0).Rows.Count - 1)
        Dim Count As Integer = 0
        Dim OpsCrewTravModeInd As Boolean = False
        Dim OpsCrewAccom As String = ""
        For i As Integer = 0 To Data.Tables.Item(5).Rows.Count - 1
          If Data.Tables.Item(5).Rows(i).ItemArray(0) = Data.Tables.Item(0).Rows(Row).ItemArray(8) AndAlso Data.Tables.Item(5).Rows(i).ItemArray(2) = CInt(OBLib.CommonData.Enums.CrewType.Production) Then
            If OpsCrewTravModeInd Then
              OpsCrewAccom += " & "
            End If
            OpsCrewAccom = OpsCrewAccom + Data.Tables.Item(5).Rows(i).ItemArray(1)
            OpsCrewTravModeInd = True
          End If
        Next
        If OpsCrewTravModeInd = False Then
          SetText(ws, RowNumber, 14, "NONE", ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        Else
          SetText(ws, RowNumber, 14, OpsCrewAccom, ExcelDefaultableBoolean.False, , 8, HorizontalCellAlignment.Center, , VerticalCellAlignment.Center)
        End If
        RowNumber += 4
      Next


      Return wb


    End Function

    Private Sub SetText(ByVal ws As Worksheet, ByVal Row As Integer, ByVal Column As Integer, ByVal Text As String, _
                       Optional ByVal Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.Default, _
                       Optional ByVal FontName As String = "Arial", Optional ByVal FontSize As Integer = 8, _
                       Optional ByVal HAlign As HorizontalCellAlignment = HorizontalCellAlignment.Center, _
                       Optional ByVal FontUnderlineStyle As FontUnderlineStyle = FontUnderlineStyle.None, _
                       Optional ByVal VerticalAlignment As VerticalCellAlignment = VerticalCellAlignment.Default)

      If Integer.TryParse(Text, 0) Then
        ws.Rows(Row).Cells(Column).Value = CInt(Text)
      Else
        ws.Rows(Row).Cells(Column).Value = Text
      End If

      ws.Rows(Row).Cells(Column).CellFormat.Font.Bold = Bold
      ws.Rows(Row).Cells(Column).CellFormat.Font.Name = FontName
      ws.Rows(Row).Cells(Column).CellFormat.Font.Height = FontSize * 20
      ws.Rows(Row).Cells(Column).CellFormat.Alignment = HAlign
      ws.Rows(Row).Cells(Column).CellFormat.Font.UnderlineStyle = FontUnderlineStyle
      ws.Rows(Row).Cells(Column).CellFormat.VerticalAlignment = VerticalAlignment

      Dim CellKey As String = ws.Rows(Row).Cells(Column).ToString
      Dim size = New System.Drawing.Size
      If Not ColumnWidths.ContainsKey(CellKey) Then
        size.Width = IIf(ws.Columns(Column).Width = -1, ws.DefaultColumnWidth, ws.Columns(Column).Width) / 256.0
        size.Height = IIf(ws.Rows(Row).Height = -1, ws.DefaultRowHeight, ws.Rows(Row).Height) / 20.0
        ColumnWidths.Add(CellKey, size)
      Else
        size = ColumnWidths(CellKey)
      End If

      '''dummy label to get a graphics object to measure the width (in pixels) of a string
      'Dim l As New System.Windows.Forms.Label
      'l.Text = Text
      'l.Size = size
      'Dim g As System.Drawing.Graphics = l.CreateGraphics
      'Dim s As System.Drawing.SizeF
      ''For Each cell As Infragistics.Win.UltraWinGrid.UltraGridCell In Row.Cells
      'Dim iFont = ws.Rows(Row).Cells(Column).CellFormat.Font
      'Dim windowsFont = New Drawing.Font(iFont.Name, iFont.Height / 20.0)
      's = g.MeasureString(Text, windowsFont)
      'size.Width = IIf(CInt(size.Width) < CInt(s.Width), CInt(s.Width), size.Width)

    End Sub

    Private Sub SetCellBorders(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, ByVal ReplaceExistingBorders As Boolean, _
                              ByVal TopBorderColor As System.Drawing.Color, ByVal BottomBorderColor As System.Drawing.Color, _
                              ByVal LeftBorderColor As System.Drawing.Color, ByVal RightBorderColor As System.Drawing.Color, _
                              Optional ByVal TopBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal BottomBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal LeftBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal RightBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal DiagonalBorder As DiagonalBorders = DiagonalBorders.None, Optional DiagonalBorderStyle As CellBorderLineStyle = CellBorderLineStyle.None,
                              Optional ByVal DiagonalBorderColor As System.Drawing.Color = Nothing)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = TopBorderStyle
            ws.Rows(i).Cells(j).CellFormat.TopBorderColor = TopBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = BottomBorderStyle
            ws.Rows(i).Cells(j).CellFormat.BottomBorderColor = BottomBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = LeftBorderStyle
            ws.Rows(i).Cells(j).CellFormat.LeftBorderColor = LeftBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = RightBorderStyle
            ws.Rows(i).Cells(j).CellFormat.RightBorderColor = RightBorderColor
          End If

          ws.Rows(i).Cells(j).CellFormat.DiagonalBorders = DiagonalBorder
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderColor = DiagonalBorderColor

        Next
      Next
    End Sub

    Private Sub FormatCells(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, _
                           ByVal FillColor As System.Drawing.Color, ByVal FillPattern As FillPatternStyle)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          ws.Rows(i).Cells(j).CellFormat.FillPatternBackgroundColor = FillColor
          Select Case FillPattern
            Case FillPatternStyle.Solid, FillPatternStyle.Default, FillPatternStyle.None
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = FillColor
            Case Else
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = System.Drawing.Color.Black
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
          End Select
        Next
      Next

    End Sub

  End Class

#End Region

#Region " Production Discipline Count Per Contract Type "

  Public Class ProductionDisciplineCountPerContractType
    Inherits ReportBase(Of ProductionDisciplineCountPerContractTypeCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Discipline Count per Contract Type"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionDisciplineCountPerContractType"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)
      If DataSet.Tables.Count > 1 Then
        DataSet.Relations.Add("ProductionIDRelation", DataSet.Tables(0).Columns("ProductionID"), DataSet.Tables(1).Columns("ProductionID"))
      End If

      For Each table As DataTable In DataSet.Tables
        For Each col In table.Columns
          If col.ToString = "ProductionID" Then
            col.ExtendedProperties.Add("AutoGenerate", False)
          End If
        Next
      Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionDisciplineCountPerContractTypeCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ProductionDisciplineCountPerContractTypeCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    '<Display(Name:="Human Resource", Order:=3)>
    'Public Property HumanResourceIDs As List(Of Integer)

    '<Display(Name:="Pay Half Month", Order:=4)>
    'Public Property PayHalfMonthInd As Boolean

  End Class

  Public Class ProductionDisciplineCountPerContractTypeCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionDisciplineCountPerContractTypeCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region " Production Discipline Count Per Contract Type (Content)"

  Public Class ProductionDisciplineCountPerContractTypeContent
    Inherits ReportBase(Of ProductionDisciplineCountPerContractTypeCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Discipline Count per Contract Type (Content)"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionDisciplineCountPerContractTypeContent"
      cmd.Parameters.AddWithValue("@SystemID", OBLib.Security.Settings.CurrentUser.SystemID)
      cmd.Parameters.AddWithValue("@ProductionAreaID", OBLib.Security.Settings.CurrentUser.ProductionAreaID)
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)
      'If DataSet.Tables.Count > 1 Then
      '  DataSet.Relations.Add("ProductionIDRelation", DataSet.Tables(0).Columns("ProductionID"), DataSet.Tables(1).Columns("ProductionID"))
      'End If

      'For Each table As DataTable In DataSet.Tables
      '  For Each col In table.Columns
      '    If col.ToString = "ProductionID" Then
      '      col.ExtendedProperties.Add("AutoGenerate", False)
      '    End If
      '  Next
      'Next

    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionDisciplineCountPerContractTypeContentControl)
      End Get
    End Property

  End Class

  Public Class ProductionDisciplineCountPerContractTypeContentCriteria
    Inherits DefaultCriteria

    <Display(Name:="Start Date", Order:=1),
    Required, DateField(MaxDateProperty:="EndDate")>
    Public Property StartDate As Date? = Singular.Dates.DateMonthStart(Now)

    <Display(Name:="End Date", Order:=2),
    Required, DateField(MinDateProperty:="StartDate")>
    Public Property EndDate As Date? = Singular.Dates.DateMonthEnd(Now)

    '<Display(Name:="Human Resource", Order:=3)>
    'Public Property HumanResourceIDs As List(Of Integer)

    '<Display(Name:="Pay Half Month", Order:=4)>
    'Public Property PayHalfMonthInd As Boolean

  End Class

  Public Class ProductionDisciplineCountPerContractTypeContentControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionDisciplineCountPerContractTypeCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 3, 3)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Productions Budgeted Report"
  <Serializable()>
  Public Class ProductionsBudgetedReport
    Inherits ReportBase(Of ProductionsBudgetedTypeCriteria)

    Public Property CustomExportName As String

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Productions Budgeted"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionsBudgetedReport"
    End Sub

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      For Each col In DataSet.Tables(0).Columns
        Select Case col.ToString
          Case "ProductionID", "ProductionTypeID", "EventTypeID", "ProductionVenueID", "DebtorID"
            col.ExtendedProperties.Add("AutoGenerate", False)
        End Select
      Next

    End Sub

    Protected Overrides ReadOnly Property ExportFileName As String
      Get
        If CustomExportName <> "" Then
          Return CustomExportName
        End If
        Return Me.ReportName
      End Get
    End Property

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionsBudgetedTypeCriteriaControl)
      End Get
    End Property

  End Class

  Public Class ProductionsBudgetedTypeCriteria
    Inherits Singular.Reporting.DefaultCriteria
    <Singular.DataAnnotations.DateField(AlwaysShow:=True, AutoChange:=Singular.DataAnnotations.AutoChangeType.StartOfMonth, MaxDateProperty:="PlayEndDateTime"), _
    Display(Name:="Play Start Date", Order:=1), Required(ErrorMessage:="Start Date of Production Required")>
    Public Property PlayStartDateTime As Date = Singular.Dates.DateMonthStart(Now.Date)

    <Singular.DataAnnotations.DateField(AlwaysShow:=True, AutoChange:=Singular.DataAnnotations.AutoChangeType.EndOfMonth, MinDateProperty:="PlayStartDateTime"), _
    Display(Name:="Play End Date", Order:=2), Required(ErrorMessage:="End Date of Production Required")>
    Public Property PlayEndDateTime As Date = Singular.Dates.DateMonthEnd(Now.Date)

    <Display(Name:="Production Type", Order:=1)>
    Public Property ProductionTypeID As Integer?

    <Display(Name:="Read Only Production Type List"), ClientOnly>
    Public Property ROProductionTypeList As List(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType) = OBLib.CommonData.Lists.ROProductionTypeList.ToList

    <Display(Name:="Read Only Event Type List Report List"), ClientOnly>
    Public Property ROEventTypeListReportList As List(Of ROEventTypeReport)

    <Display(Name:="Event Type", Order:=3)>
    Public Property EventTypeID As Integer?

    <Display(Name:="Event Type", Order:=4), ClientOnly>
    Public Property EventTypes As String

    <Display(Name:="Production Ref No")>
    Public Property ProductionRefNo As Integer?


  End Class

  Public Class ProductionsBudgetedTypeCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of ProductionsBudgetedTypeCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.PlayStartDateTime)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.PlayStartDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.PlayEndDateTime)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.PlayEndDateTime, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.ProductionRefNo, Singular.Web.BootstrapEnums.InputSize.Small)
                      .Editor.Attributes("placeholder") = "Production Ref. No"
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Production Types")
                .Attributes("id") = "Productions"
                With .Helpers.Div
                  With .Helpers.Div
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                        With .Helpers.With(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport.Criteria)("ViewModel.ROProductionTypeReportPagedListCriteria()")
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord(), Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search Production Types")
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.DivC("add-vertical-scroll-600")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType)(Function(d) d.ROProductionTypeList)
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                            .Button.AddBinding(KnockoutBindingString.click, "ProductionsBudgetedReport.AddProductionType($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionType)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Event Types")
                With .Helpers.Div
                  With .Helpers.Div
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                        With .Helpers.With(Of OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport.Criteria)("ViewModel.ROEventTypeReportPagedListCriteria()")
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord(), Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search Event Types")
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.DivC("add-vertical-scroll-600")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of ROEventTypeReport)(Function(d) d.ROEventTypeListReportList)
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                            .Button.AddBinding(KnockoutBindingString.click, "ProductionsBudgetedReport.AddEventType($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.EventType)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With

          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub
  End Class

#End Region

  '#Region " Monthly Production Crew Schedule "

  '  Public Class MonthlyProductionCrewSchedule
  '    Inherits ReportBase(Of MonthlyProductionCrewScheduleCriteria)

  '    Public Overrides ReadOnly Property ReportName As String
  '      Get
  '         Return "Monthly Production Crew Schedule"
  '      End Get
  '    End Property

  '    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)

  '    End Sub

  '#Region " HeaderNames "
  '    Private Class HeaderNames

  '      Private mHeaderName As New List(Of String)(New String() {"Day", "Date", "Production", "Production Meeting", "Facility Checks Start", _
  '                                                               "Commentator Meeting Start", "Build Up Start", "Event Start", "Match TX Details", _
  '                                                               "CAM SPEC", "OB Unit", "Event Manager ", "Studio Producer", "Studio Director", _
  '                                                               "Studio PA", "Producer", "Director", "VT Director", "Floor Manager", "PA", _
  '                                                               "Vision Mixer", "Stats", "Graphics", "Commentator (E)", "Commentator (E)", _
  '                                                               "Commentator (E)", "Commentator (E)", "Commentator (E)", "Commentator (A)", _
  '                                                               "Commentator (A)", "Commentator (A)", "Commentator (A)", "Commentator (A)", _
  '                                                               "Commentator (X)", "Commentator (X)", "Commentator (S)", "Commentator (Z)", _
  '                                                               "OB Field Presenter", "OB Studio Guest", "OB Studio Guest", "Randburg Studio Presenter", _
  '                                                               "Randburg Studio Guest", "Randburg Studio Guest", "Randburg Studio Guest", "Leave", "Notes"})
  '      Private mCurrentHeader As Integer = -1

  '      Public Sub AddHeader(HeaderName As String)
  '        mHeaderName.Add(HeaderName)
  '      End Sub

  '      Public Function GetNextHeaderName() As String
  '        mCurrentHeader += 1
  '        Return (mHeaderName.Item(mCurrentHeader))
  '      End Function

  '      Public Function GetCount() As Integer
  '        Return mHeaderName.Count()
  '      End Function

  '    End Class
  '#End Region

  '    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

  '      Dim wb As New Workbook()
  '      Dim ws As Worksheet = wb.Worksheets.Add("Monthly Crew Schedule")

  '      'Freeze columns
  '      ws.DisplayOptions.PanesAreFrozen = True
  '      ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 1

  '      'Column Headers
  '      Dim HeaderName As New HeaderNames
  '      HeaderName.AddHeader("Prod. Ref No.")
  '      HeaderName.AddHeader("Day")
  '      HeaderName.AddHeader("Date")
  '      HeaderName.AddHeader("Production")
  '      HeaderName.AddHeader("Venue")
  '      HeaderName.AddHeader("OB Unit")
  '      HeaderName.AddHeader("CAM SPEC")
  '      HeaderName.AddHeader("ENHANCEMENT")
  '      HeaderName.AddHeader("HD/SD")
  '      If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionContent)) Then
  '        HeaderName.AddHeader("Production Call Time")
  '        HeaderName.AddHeader("Talent Call Time")
  '        HeaderName.AddHeader("Facility Checks Start")
  '        HeaderName.AddHeader("Build Up Start")
  '        HeaderName.AddHeader("Event Start")
  '      End If
  '      HeaderName.AddHeader("ADDITIONAL MATCHES")
  '      If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionContent)) Then
  '        HeaderName.AddHeader("Status")
  '      End If
  '      HeaderName.AddHeader("Event Manager")



  '      If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionServices)) Then

  '      ElseIf CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(OBLib.CommonData.Enums.System.ProductionContent)) Then

  '      End If

  '      'For i As Integer = 0 To HeaderName.GetCount - 1
  '      '  SetText(ws, i, 0, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.False, , , HorizontalCellAlignment.Left, , )
  '      'Next
  '      'ws.Columns(0).Width = DefaultColumnWidth * 300s

  '      Return MyBase.CreateExcelWorkbook(Data)
  '    End Function

  '    Private Sub SetText(ByVal ws As Worksheet, ByVal Row As Integer, ByVal Column As Integer, ByVal Text As String, _
  '                   Optional ByVal Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.Default, _
  '                   Optional ByVal FontName As String = "Arial", Optional ByVal FontSize As Integer = 8, _
  '                   Optional ByVal HAlign As HorizontalCellAlignment = HorizontalCellAlignment.Center, _
  '                   Optional ByVal FontUnderlineStyle As FontUnderlineStyle = FontUnderlineStyle.None, _
  '                   Optional ByVal VerticalAlignment As VerticalCellAlignment = VerticalCellAlignment.Default)

  '      If Integer.TryParse(Text, 0) Then
  '        ws.Rows(Row).Cells(Column).Value = CInt(Text)
  '      Else
  '        ws.Rows(Row).Cells(Column).Value = Text
  '      End If

  '      ws.Rows(Row).Cells(Column).CellFormat.Font.Bold = Bold
  '      ws.Rows(Row).Cells(Column).CellFormat.Font.Name = FontName
  '      ws.Rows(Row).Cells(Column).CellFormat.Font.Height = FontSize * 20
  '      ws.Rows(Row).Cells(Column).CellFormat.Alignment = HAlign
  '      ws.Rows(Row).Cells(Column).CellFormat.Font.UnderlineStyle = FontUnderlineStyle
  '      ws.Rows(Row).Cells(Column).CellFormat.VerticalAlignment = VerticalAlignment

  '      Dim CellKey As String = ws.Rows(Row).Cells(Column).ToString
  '      Dim size = New System.Drawing.Size
  '      If Not ColumnWidths.ContainsKey(CellKey) Then
  '        size.Width = IIf(ws.Columns(Column).Width = -1, ws.DefaultColumnWidth, ws.Columns(Column).Width) / 256.0
  '        size.Height = IIf(ws.Rows(Row).Height = -1, ws.DefaultRowHeight, ws.Rows(Row).Height) / 20.0
  '        ColumnWidths.Add(CellKey, size)
  '      Else
  '        size = ColumnWidths(CellKey)
  '      End If

  '      '''dummy label to get a graphics object to measure the width (in pixels) of a string
  '      'Dim l As New System.Windows.Forms.Label
  '      'l.Text = Text
  '      'l.Size = size
  '      'Dim g As System.Drawing.Graphics = l.CreateGraphics
  '      'Dim s As System.Drawing.SizeF
  '      ''For Each cell As Infragistics.Win.UltraWinGrid.UltraGridCell In Row.Cells
  '      'Dim iFont = ws.Rows(Row).Cells(Column).CellFormat.Font
  '      'Dim windowsFont = New Drawing.Font(iFont.Name, iFont.Height / 20.0)
  '      's = g.MeasureString(Text, windowsFont)
  '      'size.Width = IIf(CInt(size.Width) < CInt(s.Width), CInt(s.Width), size.Width)

  '    End Sub

  '    Private Sub SetCellBorders(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, ByVal ReplaceExistingBorders As Boolean, _
  '                              ByVal TopBorderColor As System.Drawing.Color, ByVal BottomBorderColor As System.Drawing.Color, _
  '                              ByVal LeftBorderColor As System.Drawing.Color, ByVal RightBorderColor As System.Drawing.Color, _
  '                              Optional ByVal TopBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal BottomBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
  '                              Optional ByVal LeftBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal RightBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
  '                              Optional ByVal DiagonalBorder As DiagonalBorders = DiagonalBorders.None, Optional DiagonalBorderStyle As CellBorderLineStyle = CellBorderLineStyle.None,
  '                              Optional ByVal DiagonalBorderColor As System.Drawing.Color = Nothing)

  '      For i As Integer = RowFrom To RowTo
  '        For j As Integer = ColumnFrom To ColumnTo
  '          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = CellBorderLineStyle.Default Then
  '            ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = TopBorderStyle
  '            ws.Rows(i).Cells(j).CellFormat.TopBorderColor = TopBorderColor
  '          End If

  '          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = CellBorderLineStyle.Default Then
  '            ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = BottomBorderStyle
  '            ws.Rows(i).Cells(j).CellFormat.BottomBorderColor = BottomBorderColor
  '          End If

  '          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = CellBorderLineStyle.Default Then
  '            ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = LeftBorderStyle
  '            ws.Rows(i).Cells(j).CellFormat.LeftBorderColor = LeftBorderColor
  '          End If

  '          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = CellBorderLineStyle.Default Then
  '            ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = RightBorderStyle
  '            ws.Rows(i).Cells(j).CellFormat.RightBorderColor = RightBorderColor
  '          End If

  '          ws.Rows(i).Cells(j).CellFormat.DiagonalBorders = DiagonalBorder
  '          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
  '          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderColor = DiagonalBorderColor

  '        Next
  '      Next
  '    End Sub

  '    Private Sub FormatCells(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, _
  '                           ByVal FillColor As System.Drawing.Color, ByVal FillPattern As FillPatternStyle)

  '      For i As Integer = RowFrom To RowTo
  '        For j As Integer = ColumnFrom To ColumnTo
  '          ws.Rows(i).Cells(j).CellFormat.FillPatternBackgroundColor = FillColor
  '          Select Case FillPattern
  '            Case FillPatternStyle.Solid, FillPatternStyle.Default, FillPatternStyle.None
  '              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
  '              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = FillColor
  '            Case Else
  '              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = System.Drawing.Color.Black
  '              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
  '          End Select
  '        Next
  '      Next

  '    End Sub


  '  End Class

  '  Public Class MonthlyProductionCrewScheduleCriteria
  '    Inherits StartAndEndDateReportCriteria

  '    <Display(Name:="Exclude Off")>
  '    Public Property ExcludeOff As Boolean = False

  '  End Class

  '#End Region

  'Public Class CrewSchedule
  '  Inherits ReportBase(Of CrewScheduleCriteria)

  '  Public Overrides ReadOnly Property ReportName As String
  '    Get
  '      Return "Crew Schedule"
  '    End Get
  '  End Property

  '  Public Overrides ReadOnly Property CrystalReportType As System.Type
  '    Get
  '      Return GetType(rptCrewSchedule)
  '    End Get
  '  End Property

  '  Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
  '    cmd.CommandText = "RptProcs.rptCrewSchedule"
  '  End Sub

  'End Class

  'Public Class CrewScheduleCriteria
  '  Inherits StartAndEndDateReportCriteria


  '  <System.ComponentModel.DisplayName("Production"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Productions.ReadOnly.ROProductionList), ValueMember:="ProductionID", DisplayMember:="ProductionRefNo", UnselectedText:="All Productions")>
  '  Public Property ProductionID As Object

  '  <System.ComponentModel.DisplayName("HumanResource"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.HR.ReadOnly.ROHumanResourceList), ValueMember:="HumanResourceID", DisplayMember:="HumanResourceName", UnselectedText:="All Human Resources")>
  '  Public Property HumanResourceID As Object

  '  '<System.ComponentModel.DisplayName("System"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Security.OBIdentity), ValueMember:="ProductionID", DisplayMember:="ProductionRefNo", UnselectedText:="All Productions")>
  '  Public Property SystemID As Object = 1

  '  '<System.ComponentModel.DisplayName("HideDetail"), Singular.DataAnnotations.DropDownField(GetType(OBLib.HR.ReadOnly.ROHumanResourceList), ValueMember:="HumanResourceID", DisplayMember:="HumanResourceName", UnselectedText:="All Human Resources")>
  '  Public Property HideDetailInd As Boolean = False



  'End Class



  'Public Class CommentatorTravelRec
  '  Inherits ReportBase(Of CommentatorTravelRecCriteria)

  '  Public Overrides ReadOnly Property ReportName As String
  '    Get
  '      Return "Commentator Travel Req Report"
  '    End Get
  '  End Property

  '  Public Overrides ReadOnly Property CrystalReportType As System.Type
  '    Get
  '      Return GetType(rptCommentatorTravelRec)
  '    End Get
  '  End Property

  '  Protected Overrides Sub SetupCommandProc(cmd As Singular.CommandProc)
  '    cmd.CommandText = "RptProcs.rptCommentatorTravelRec"
  '  End Sub

  'End Class

  'Public Class CommentatorTravelRecCriteria
  '  Inherits DefaultCriteria


  '  <System.ComponentModel.DisplayName("Production"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Productions.ReadOnly.ROProductionList), ValueMember:="ProductionID", DisplayMember:="ProductionRefNo", UnselectedText:="All Productions")>
  '  Public Property ProductionID As Object

  '  '<System.ComponentModel.DisplayName("User"), Singular.DataAnnotations.DropDownWeb(GetType(CSLAlib.Users.Settings.CurrentUser.UserID), ValueMember:="UserID", DisplayMember:="UserName", UnselectedText:="All Users")>
  '  Public Property UserID As Integer? = 1

  '  '<System.ComponentModel.DisplayName("System"), Singular.DataAnnotations.DropDownWeb(GetType(OBLib.Security.OBIdentity), ValueMember:="ProductionID", DisplayMember:="ProductionRefNo", UnselectedText:="All Productions")>
  '  Public Property SystemID As Object = 1



  'End Class

#Region " Monthly Production Crew Schedule "

  Public Class MonthlyProductionCrewSchedule
    Inherits ReportBase(Of MonthlyProductionCrewScheduleCriteria)

    Private mFontDataList As OBLib.Reports.ROProductionScheduleList.FontDataList
    Dim list As OBLib.Reports.ROProductionScheduleList
    Private Const DefaultColumnWidth As Decimal = 17.5
    Private ColumnWidths As New Dictionary(Of String, System.Drawing.Size)

    Private Const MaxEnglishCommentators As Integer = 5
    Private Const MaxAfrikaansCommentators As Integer = 5
    Private Const MaxSothoCommentators As Integer = 1
    Private Const MaxZuluCommentators As Integer = 1
    Private Const MaxXhosaCommentators As Integer = 2
    Private Const MaxOBStudioGuests As Integer = 2
    Private Const MaxRandburgStudioGuests As Integer = 3

    Public Property CustomExportName As String

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Monthly Production Crew Schedule"
      End Get
    End Property

    Protected Overrides ReadOnly Property ExportFileName As String
      Get
        If CustomExportName <> "" Then
          Return CustomExportName
        End If
        Return Me.ReportName
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "[getProcsWeb].[getROProductionScheduleList]"
      Dim eo As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ExcludeOff").FirstOrDefault
      cmd.Parameters.Remove(eo)
      Dim eh As Singular.CommandProc.Parameter = cmd.Parameters.Where(Function(d) d.Name = "@ExcludeHistory").FirstOrDefault
      cmd.Parameters.Remove(eh)
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(MonthlyProductionCrewScheduleCriteriaControl)
      End Get
    End Property

#Region " HeaderNames "
    Private Class HeaderNames

      Private mHeaderName As New List(Of String)(New String() {"Travel Req", "Day", "Date", "Production", "OB Unit", "Venue", "Enhancement", "HD/SD", "Additional Matches", "Status", _
                                                               "CAM SPEC", "", "", "", ""})
      Private mCurrentHeader As Integer = -1

      Public Function GetNextHeaderName() As String
        mCurrentHeader += 1
        Return (mHeaderName.Item(mCurrentHeader))
      End Function

      Public Function GetCount() As Integer
        Return mHeaderName.Count()
      End Function

    End Class
#End Region

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim PrimaryKeyDiscipline As String = "Discipline"
      Dim PrimaryKeyPosition As String = "Position"
      Dim RowHeaderColName As String = "Trvl req nbr"
      Dim SortOrderColName As String = "Sort Order"
      Dim StatusRowNumber As Integer = 10
      Dim HeaderRows As Integer = 11

      CommonData.Refresh("ProductionTypeList")

      list = OBLib.Reports.ROProductionScheduleList.GetROProductionScheduleList(ReportCriteria.StartDate, ReportCriteria.EndDate, ReportCriteria.ProductionTypeID, ReportCriteria.EventTypeID, ReportCriteria.EventManagerHumanResourceID, ReportCriteria.SystemID)

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add("Monthly Crew Schedule")

      Dim mReportDataset As DataSet = New DataSet
      Dim dt As New DataTable("Table")
      mReportDataset.Tables.Add(dt)

      dt.Columns.Add(PrimaryKeyDiscipline, GetType(System.String))
      dt.Columns.Add(PrimaryKeyPosition, GetType(System.String))
      dt.PrimaryKey = New DataColumn() {dt.Columns(PrimaryKeyDiscipline), dt.Columns(PrimaryKeyPosition)}

      dt.Columns.Add(RowHeaderColName, GetType(System.String))
      dt.Columns.Add(SortOrderColName, GetType(System.Int32))

      Dim AddStatusRow As Boolean = list.HasAtLeastOneCancelledProduction()
      Dim ProductionIDList As String() = list.Select(Function(pd) pd.ProductionID.ToString).Distinct.ToArray
      Dim ProductionIDXML As String = Singular.Data.XML.StringArrayToXML(ProductionIDList)
      Dim HistoryList As OBLib.Reports.ROProductionHumanResourceHistoryList = OBLib.Reports.ROProductionHumanResourceHistoryList.GetROProductionHumanResourceHistoryList(ProductionIDXML)

      'add the production metadata to the report first

      For Each item As OBLib.Reports.ROProductionSchedule In list.OrderBy(Function(f) f.SortID)
        dt.Columns.Add(item.ProductionPK, GetType(System.String))
        list.GetDataRow(dt, "Day", "", "Day")(item.ProductionPK) = CDate(item.EventStart).ToString("dddd")
        list.GetDataRow(dt, "Date", "", "Date")(item.ProductionPK) = item.ROProductionScheduleTxTimelineList.GetStringValue
        list.GetDataRow(dt, "Production", "", "Production")(item.ProductionPK) = item.ProductionDescription
        list.GetDataRow(dt, "Venue", "", "Venue")(item.ProductionPK) = item.ProductionVenue & " " & item.City
        list.GetDataRow(dt, "OB Unit", "", "OB Unit")(item.ProductionPK) = item.VehicleName
        list.GetDataRow(dt, "CAM SPEC", "", "CAM SPEC")(item.ProductionPK) = IIf(Not IsNullNothing(item.TotalCameras), item.TotalCameras, "")
        list.GetDataRow(dt, "ENHANCEMENT", "", "ENHANCEMENT")(item.ProductionPK) = item.ROProductionScheduleEnhancementList.GetStringValue
        list.GetDataRow(dt, "HD/SD", "", "HD/SD")(item.ProductionPK) = IIf(item.HDRequiredInd, "HD BROADCAST", "")

        'Commentator specific code
        Dim colour As System.Drawing.Color = System.Drawing.Color.Black
        If Not IsNullNothing(ZeroDBNull(ReportCriteria.SystemID)) Then
          colour = System.Drawing.Color.FromName(OBLib.CommonData.Lists.ROProductionTypeList.GetItem(item.ProductionTypeID).TextColour)
        End If
        If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CommonData.Enums.System.ProductionServices) OrElse ReportCriteria.SystemID = 0 Then
          'set the colors
          If ReportCriteria.SystemID <> 0 Then
            list.SetCellFont(list.GetDataRow(dt, "Day", "", "Day"), item.ProductionPK, colour, True)
            list.SetCellFont(list.GetDataRow(dt, "Date", "", "Date"), item.ProductionPK, colour, True)
            list.SetCellFont(list.GetDataRow(dt, "Production", "", "Production"), item.ProductionPK, colour, True)
            list.SetCellFont(list.GetDataRow(dt, "Venue", "", "Venue"), item.ProductionPK, colour, True)
            list.SetCellFont(list.GetDataRow(dt, "OB Unit", "", "OB Unit"), item.ProductionPK, colour, True)
            list.SetCellFont(list.GetDataRow(dt, "CAM SPEC", "", "CAM SPEC"), item.ProductionPK, colour, True)
            list.SetCellFont(list.GetDataRow(dt, "ENHANCEMENT", "", "ENHANCEMENT"), item.ProductionPK, colour, True)
            list.SetCellFont(list.GetDataRow(dt, "HD/SD", "", "HD/SD"), item.ProductionPK, colour, True)
          End If
          'set the text          
          'list.GetDataRow(dt, "Production Call Time", "", "Production Call Time")(item.ProductionPK) = If(item.ProductionCallTime = "", "", DateTime.Parse(item.ProductionCallTime).ToString("HH:mm"))
          'list.SetCellFont(list.GetDataRow(dt, "Production Call Time", "", "Production Call Time"), item.ProductionPK, colour, True)
          'list.GetDataRow(dt, "Talent Call Time", "", "Talent Call Time")(item.ProductionPK) = If(item.TalentCallTime = "", "", DateTime.Parse(item.TalentCallTime).ToString("HH:mm"))
          'list.SetCellFont(list.GetDataRow(dt, "Talent Call Time", "", "Talent Call Time"), item.ProductionPK, colour, True)
          'list.GetDataRow(dt, "Facility Checks Start", "", "Facility Checks Start")(item.ProductionPK) = If(item.FacilityChecks = "", "", DateTime.Parse(item.FacilityChecks).ToString("HH:mm"))
          'list.SetCellFont(list.GetDataRow(dt, "Facility Checks Start", "", "Facility Checks Start"), item.ProductionPK, colour, True)
          'list.GetDataRow(dt, "Build Up Start", "", "Build Up Start")(item.ProductionPK) = If(item.BuildUp = "", "", DateTime.Parse(item.BuildUp).ToString("HH:mm"))
          'list.SetCellFont(list.GetDataRow(dt, "Build Up Start", "", "Build Up Start"), item.ProductionPK, colour, True)
          'list.GetDataRow(dt, "Event Start", "", "Event Start")(item.ProductionPK) = CDate(item.EventStart).ToString("HH:mm")
          'list.SetCellFont(list.GetDataRow(dt, "Event Start", "", "Event Start"), item.ProductionPK, colour, True)
        End If
        list.GetDataRow(dt, "ADDITIONAL MATCHES", "", "ADDITIONAL MATCHES")(item.ProductionPK) = item.AdditionalMatches
        If AddStatusRow Then
          list.GetDataRow(dt, "Status", "", "Status")(item.ProductionPK) = IIf(item.CancelledDate.HasValue, "CANCELLED", "")
        End If
      Next

      Dim BlankRowCount As Integer = 1
      list.AddBlankRow(dt, BlankRowCount)

      Dim ReportDatasetFontData As OBLib.Reports.ROProductionScheduleList.FontDataList = New OBLib.Reports.ROProductionScheduleList.FontDataList

      Dim ListOfDisciplines As New List(Of CommonData.Enums.Discipline)
      ListOfDisciplines.Add(CommonData.Enums.Discipline.EventManager)
      ListOfDisciplines.Add(CommonData.Enums.Discipline.EventManagerIntern)

      If ReportCriteria.SystemID = CommonData.Enums.System.ProductionServices OrElse ReportCriteria.SystemID = 0 Then
        ListOfDisciplines.Add(CommonData.Enums.Discipline.SeniorManagerProductionServices)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ProductionServicesOperationsManager)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ProductionServicesResourceOfficeManager)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ProductionServicesTechnicalManager)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ProductionServicesLogisticsManager)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ProductionServicesCrewManager)
      End If

      If ReportCriteria.SystemID = 0 Then
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Director)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Producer)
      End If

      If ReportCriteria.SystemID = CommonData.Enums.System.ProductionServices OrElse ReportCriteria.SystemID = 0 Then
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Engineer)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Driver)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.AudioMixer)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.AudioAssistant)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.CameraOperator)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.VisionController)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.EVSOperator)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ReliefCrewDriver)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ReliefCrewOps)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ReliefCrewRig)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.HandHeldMicCarrier)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Bashers)
        'ListOfDisciplines.Add(CommonData.Enums.Discipline.Trainee)
      End If

      If ReportCriteria.SystemID = CommonData.Enums.System.ProductionContent OrElse ReportCriteria.SystemID = 0 Then
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ProductionManager)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ProductionCoOrdinator)
        If ReportCriteria.SystemID <> 0 Then
          ListOfDisciplines.Add(CommonData.Enums.Discipline.Producer)
          ListOfDisciplines.Add(CommonData.Enums.Discipline.Director)
        End If
        ListOfDisciplines.Add(CommonData.Enums.Discipline.VTDirector)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.FloorManager)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ProductionAssistant)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.VisionMixer)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Stats)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Graphics)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.MakeUpArtist)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Commentator)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.SidelineCommentator)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.OBFieldPresenter)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.StudioPresenter)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.StudioGuest)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.ENGCameraman)
        ListOfDisciplines.Add(CommonData.Enums.Discipline.Editor)
      End If

      'Freeze columns
      ws.DisplayOptions.PanesAreFrozen = True
      ws.DisplayOptions.FrozenPaneSettings.FrozenColumns = 1
      ws.DisplayOptions.FrozenPaneSettings.FrozenRows = 1

      'Column Headers
      Dim HeaderName As New HeaderNames
      For i As Integer = 0 To HeaderName.GetCount - 1
        SetText(ws, i, 0, HeaderName.GetNextHeaderName(), ExcelDefaultableBoolean.True, , 8, , HorizontalCellAlignment.Center, , )
      Next
      ws.Columns(0).Width = DefaultColumnWidth * 300

      'Setting other properties
      Dim ProductionIndex As Integer = 1
      ws.Rows(3).Height = DefaultColumnWidth * 80
      ws.Rows(3).CellFormat.WrapText = ExcelDefaultableBoolean.True
      ws.Rows(5).Height = DefaultColumnWidth * 40
      ws.Rows(5).CellFormat.WrapText = ExcelDefaultableBoolean.True
      ws.Rows(6).Height = DefaultColumnWidth * 80
      ws.Rows(6).CellFormat.WrapText = ExcelDefaultableBoolean.True

      'Add borders to cells
      'SetCellBorders(ws, 0, list.GetNumberOfSchedules, 0, HeaderName.GetCount - 1, True, System.Drawing.Color.Black, _
      '               System.Drawing.Color.Black, System.Drawing.Color.Black, System.Drawing.Color.Black, CellBorderLineStyle.Thin, _
      '               CellBorderLineStyle.Thin, CellBorderLineStyle.Thin, CellBorderLineStyle.Thin)

      'Worksheet Properties
      ws.PrintOptions.BottomMargin = 0
      ws.PrintOptions.FooterMargin = 0
      ws.PrintOptions.HeaderMargin = 0
      ws.PrintOptions.LeftMargin = 0
      ws.PrintOptions.RightMargin = 0
      ws.PrintOptions.TopMargin = 0

      ws.PrintOptions.Orientation = Orientation.Landscape
      ws.PrintOptions.ScalingFactor = 85

      'Repeat Column header
      Dim rtr As New RepeatTitleRange(0, 0)
      ws.PrintOptions.ColumnsToRepeatAtLeft = rtr

      For Each Item As OBLib.Reports.ROProductionSchedule In list
        ws.Columns(ProductionIndex).Width = DefaultColumnWidth * 400
        ws.Rows(ProductionIndex).CellFormat.WrapText = ExcelDefaultableBoolean.True
        SetText(ws, 0, ProductionIndex, Item.ProductionPK, , , , , , )
        SetText(ws, 1, ProductionIndex, CDate(Item.EventStart).ToString("dddd"), , , , , , )
        SetText(ws, 2, ProductionIndex, Item.ROProductionScheduleTxTimelineList.GetStringValue, , , , , , )
        SetText(ws, 3, ProductionIndex, Item.ProductionDescription, , , , , , )
        SetText(ws, 4, ProductionIndex, Item.VehicleName, , , , , , VerticalCellAlignment.Center)
        SetText(ws, 5, ProductionIndex, Item.ProductionVenue & " " & Item.City, , , , , , )
        SetText(ws, 6, ProductionIndex, Item.ROProductionScheduleEnhancementList.GetStringValue, , , , , , )
        SetText(ws, 7, ProductionIndex, IIf(Item.HDRequiredInd, "HD BROADCAST", ""), , , , , , )
        SetText(ws, 8, ProductionIndex, Item.AdditionalMatches, , , , , , )
        SetText(ws, 9, ProductionIndex, IIf(Item.CancelledDate.HasValue, "CANCELLED", ""), , , , , , )
        SetText(ws, 10, ProductionIndex, IIf(Not IsNullNothing(Item.TotalCameras), Item.TotalCameras, ""), , , , , , )

        'Commentator specific code
        Dim colour As System.Drawing.Color = System.Drawing.Color.Black
        'If Not IsNullNothing(ZeroDBNull(ReportCriteria.SystemID)) Then
        '  colour = System.Drawing.Color.FromName(OBLib.CommonData.Lists.ROProductionTypeList.GetItem(Item.ProductionTypeID).TextColour)
        'End If
        'If CompareSafe(OBLib.Security.Settings.CurrentUser.SystemID, CInt(CommonData.Enums.System.ProductionServices)) Then
        '  'set the text          
        '  SetText(ws, 12, ProductionIndex, If(Item.ProductionCallTime = "", "", DateTime.Parse(Item.ProductionCallTime).ToString("HH:mm")), , , , colour, , )
        '  SetText(ws, 13, ProductionIndex, If(Item.TalentCallTime = "", "", DateTime.Parse(Item.TalentCallTime).ToString("HH:mm")), , , , colour, , )
        '  SetText(ws, 14, ProductionIndex, If(Item.FacilityChecks = "", "", DateTime.Parse(Item.FacilityChecks).ToString("HH:mm")), , , , colour, , )
        '  SetText(ws, 15, ProductionIndex, If(Item.BuildUp = "", "", DateTime.Parse(Item.BuildUp).ToString("HH:mm")), , , , colour, , )
        '  SetText(ws, 16, ProductionIndex, CDate(Item.EventStart).ToString("HH:mm"), , , , colour, , )
        'End If
        ProductionIndex += 1
      Next
      '----------------------------------------------------------------------------------------------------------------------------------
      Dim StartRow As Integer = 13
      Dim RowInd As Integer = 0
      Dim ItemCount As Integer = 1
      Dim AlreadyParsed As Boolean = False
      Dim CameraPositionStart As Integer = 0

      Dim DisciplineRowNumbers As New Dictionary(Of Integer, Integer)
      Dim MaxDisciplinesForHR As New Dictionary(Of Integer, Integer)
      Dim CameraPositions As New Dictionary(Of Integer, String)
      Dim PositionIDs As List(Of KeyValuePair(Of String, List(Of Integer))) = New List(Of KeyValuePair(Of String, List(Of Integer))) ' Prod Ref No, PositionCode

      Dim DisciplineCount As Integer = 0
      ' Find Max HR for each discipline for report layout
      For Each item As OBLib.Reports.ROProductionSchedule In list.OrderBy(Function(f) f.SortID)
        Dim MergeChildList As OBLib.Reports.ROProductionScheduleDetailList = item.ROProductionScheduleDetailList 'list.GetAllROProductionScheduleDetailList
        For Each Discipline As CommonData.Enums.Discipline In ListOfDisciplines
          For Each HR As OBLib.Reports.ROProductionScheduleDetail In MergeChildList.Where(Function(c) Singular.Misc.CompareSafe(c.DisciplineID, CInt(Discipline)))
            If HR.DisciplineID = CommonData.Enums.Discipline.CameraOperator AndAlso Not CameraPositions.ContainsValue(HR.PositionCode) _
              AndAlso (Not PositionIDs.Exists(Function(c) c.Key = HR.PositionCode)) Then
              Dim tempList As List(Of Integer) = New List(Of Integer)
              tempList.Add(HR.ProductionRefNo)
              tempList.Add(HR.PositionID)
              tempList.Add(HR.SortOrder)
              tempList.Add(0) ' initially set index to 0
              Dim tempKVP As KeyValuePair(Of String, List(Of Integer)) = New KeyValuePair(Of String, List(Of Integer))(HR.PositionCode, tempList)
              PositionIDs.Add(tempKVP)
            End If
            If HR.DisciplineID = Discipline Then
              DisciplineCount += 1
              If Not MaxDisciplinesForHR.ContainsKey(Discipline) Then
                MaxDisciplinesForHR.Add(Discipline, DisciplineCount)
              ElseIf MaxDisciplinesForHR.Item(Discipline) < DisciplineCount Then
                MaxDisciplinesForHR.Item(Discipline) = DisciplineCount
              End If
            End If
          Next
          DisciplineCount = 0
        Next
      Next
      PositionIDs = PositionIDs.OrderBy(Function(c) c.Value(2)).ToList
      ' Add indexes to positions for later row traversal
      For i = 0 To PositionIDs.Count - 1
        PositionIDs(i).Value(3) = i
      Next

      Dim DisciplineIDList As String() = ListOfDisciplines.Select(Function(pd) CInt(pd).ToString).Distinct.ToArray
      Dim DisciplineIDXML As String = Singular.Data.XML.StringArrayToXML(DisciplineIDList)

      Dim ROProductionScheduleOffResourceList As List(Of OBLib.Reports.ROProductionScheduleOffResource) = New List(Of OBLib.Reports.ROProductionScheduleOffResource)
      Dim ROProductionScheduleOffResourceList2 As List(Of OBLib.Reports.ROProductionScheduleOffResource) = New List(Of OBLib.Reports.ROProductionScheduleOffResource)
      Dim tempROProductionScheduleOffResourceList As List(Of OBLib.Reports.ROProductionScheduleOffResource) = New List(Of OBLib.Reports.ROProductionScheduleOffResource)
      Dim ROProductionScheduleOffResourcesPerProduction As List(Of KeyValuePair(Of Integer, List(Of OBLib.Reports.ROProductionScheduleOffResource))) = New List(Of KeyValuePair(Of Integer, List(Of OBLib.Reports.ROProductionScheduleOffResource)))

      Dim leavecolour As System.Drawing.Color = System.Drawing.Color.Black

      ' Max HR on leave for report spacing
      Dim DisciplineMaxLeaveHRList As List(Of KeyValuePair(Of Integer, Integer)) = New List(Of KeyValuePair(Of Integer, Integer))

      For Each item As OBLib.Reports.ROProductionSchedule In list.OrderBy(Function(f) f.SortID)
        ' Fetch all leave for Schedules for multiple disciplines
        tempROProductionScheduleOffResourceList = OBLib.Reports.ROProductionScheduleOffResourceList.GetROProductionScheduleOffResourceList(item.EventStart, DisciplineIDXML, ReportCriteria.SystemID).ToList
        ROProductionScheduleOffResourcesPerProduction.Add(New KeyValuePair(Of Integer, List(Of OBLib.Reports.ROProductionScheduleOffResource))(item.ProductionID, tempROProductionScheduleOffResourceList))
        ROProductionScheduleOffResourceList.AddRange(tempROProductionScheduleOffResourceList)
      Next

      For Each Discipline In ListOfDisciplines
        Dim tempMaxLeaveHR As Integer = 0
        For Each leaveitem In ROProductionScheduleOffResourcesPerProduction
          If tempMaxLeaveHR < leaveitem.Value.Where(Function(c) c.DisciplineID = Discipline).Count Then
            tempMaxLeaveHR = leaveitem.Value.Where(Function(c) c.DisciplineID = Discipline).Count
          End If
        Next
        DisciplineMaxLeaveHRList.Add(New KeyValuePair(Of Integer, Integer)(Discipline, tempMaxLeaveHR))
      Next

      For Each item As OBLib.Reports.ROProductionSchedule In list.OrderBy(Function(f) f.SortID)
        'leave ---
        If Not ReportCriteria.ExcludeOff Then
          ROProductionScheduleOffResourceList2 = ROProductionScheduleOffResourcesPerProduction.Where(Function(c) c.Key = item.ProductionID)(0).Value 'OBLib.Reports.ROProductionScheduleOffResourceList.GetROProductionScheduleOffResourceList(item.EventStart, Discipline, ReportCriteria.SystemID).ToList
          If Not IsNullNothing(ReportCriteria.SystemID) Then
            leavecolour = Color.FromName(OBLib.CommonData.Lists.ROProductionTypeList.GetItem(item.ProductionTypeID).TextColour)
          End If
        End If
        '-----
        Dim MergeChildList As OBLib.Reports.ROProductionScheduleDetailList = item.ROProductionScheduleDetailList 'list.GetAllROProductionScheduleDetailList
        Dim OffHRPrevDisc As Integer = 0
        For Each Discipline As CommonData.Enums.Discipline In ListOfDisciplines
          Dim FirstDiscipline As Boolean = True
          Dim CrewLeadersAdded As Boolean = False
          Dim DisciplineRowsAdded As Boolean = False
          Dim OverridePosition As Boolean = False
          Dim DummyPositionID As Integer = 1
          Dim CrewLeaderHRs As String = ""
          Select Case Discipline
            Case CommonData.Enums.Discipline.Bashers, CommonData.Enums.Discipline.Engineer, CommonData.Enums.Discipline.Driver, _
                  CommonData.Enums.Discipline.AudioAssistant, CommonData.Enums.Discipline.AudioMixer, CommonData.Enums.Discipline.EVSOperator, _
                  CommonData.Enums.Discipline.Trainee, CommonData.Enums.Discipline.ReliefCrewDriver, CommonData.Enums.Discipline.ReliefCrewOps, _
                  CommonData.Enums.Discipline.ReliefCrewRig, CommonData.Enums.Discipline.HandHeldMicCarrier, CommonData.Enums.Discipline.EventManager, _
                  CommonData.Enums.Discipline.Commentator, CommonData.Enums.Discipline.Graphics, CommonData.Enums.Discipline.VisionController, _
                  CommonData.Enums.Discipline.EventManagerIntern
              OverridePosition = True
          End Select
          Dim DisciplineMergeChildList As OBLib.Reports.ROProductionScheduleDetailList = New OBLib.Reports.ROProductionScheduleDetailList
          Dim DisciplineMergeChildListOrdered As List(Of OBLib.Reports.ROProductionScheduleDetail) = New List(Of OBLib.Reports.ROProductionScheduleDetail)
          If Discipline <> CommonData.Enums.Discipline.Commentator Then
            DisciplineMergeChildListOrdered = MergeChildList.FilterList(Discipline).ToList.OrderBy(Function(x) x.SortOrder).ToList
          Else
            DisciplineMergeChildListOrdered = MergeChildList.FilterList(Discipline).ToList
          End If

          ' Set Far left column with disciplines
          If ItemCount = 1 AndAlso Not Singular.Misc.CompareSafe(Discipline, CInt(CommonData.Enums.Discipline.Trainee)) Then
            Dim str = Trim(System.Text.RegularExpressions.Regex.Replace(Discipline.ToString, "[A-Z][^A-Z].*?(?=[A-Z\d]|$)|[A-Z]+(?=[A-Z\d][a-z]|$)|\d+(?=[\w-\d])", " ${0}"))
            SetText(ws, RowInd + StartRow, 0, str, ExcelDefaultableBoolean.True, , , , , )
            ws.Rows(RowInd + StartRow).CellFormat.WrapText = ExcelDefaultableBoolean.True
            DisciplineRowNumbers.Add(Discipline, RowInd + StartRow)
          End If

          If DisciplineMergeChildListOrdered.Count > 0 Then
            Dim HRForThisDisciplineProduction As Integer = 0
            For i = 0 To DisciplineMergeChildListOrdered.Count - 1
              DisciplineMergeChildList.Add(DisciplineMergeChildListOrdered(i))
            Next

            ' Now iterate through each HR with this discipline
            Dim CrewLeaders As String = ""
            Dim CrewLeaderRowNumber As Integer = DisciplineRowNumbers.Where(Function(c) c.Key = Discipline)(0).Value - 1
            For Each HR As OBLib.Reports.ROProductionScheduleDetail In DisciplineMergeChildList
              Dim ProductionTypeID As Integer = Nothing
              Dim colour As System.Drawing.Color = System.Drawing.Color.Black
              DisciplineRowsAdded = True
              Dim Position As String = ""
              Dim RowHeader As String = ""
              'Determine what position to use for the HR
              If OverridePosition Then
                list.GetNextDummyPositionKey(dt, HR.ProductionPK, DummyPositionID, Discipline)
              End If
              Position = IIf(OverridePosition, DummyPositionID, HR.PositionCode)
              '--------------------
              Dim SkippedHR As Boolean = False
              If Not HR.TrainingInd Then
                If FirstDiscipline Then
                  RowHeader = HR.Discipline & " " & HR.PositionCode
                  FirstDiscipline = False
                Else
                  If Discipline <> CommonData.Enums.Discipline.Commentator Then
                    RowHeader = HR.PositionCode
                  Else
                    RowHeader = HR.Discipline & " " & HR.PositionCode
                  End If
                End If
                Dim HistoryText As String = Nothing
                Dim clr As System.Drawing.Color = System.Drawing.Color.Black
                Dim History As OBLib.Reports.ROProductionHumanResourceHistory = Nothing
                Try
                  History = HistoryList.GetHistoryItem(HR.ProductionID, HR.DisciplineID, HR.PositionID, 2)
                Catch ex As Exception
                  History = Nothing
                End Try
                If History IsNot Nothing AndAlso Not ReportCriteria.ExcludeHistory Then
                  Dim HResource As OBLib.HR.ReadOnly.ROHumanResource = OBLib.CommonData.Lists.ROHumanResourceList.GetItem(History.HumanResourceID)
                  If HResource IsNot Nothing AndAlso Not Singular.Misc.CompareSafe(HR.HumanResourceID, HResource.HumanResourceID) Then
                    HistoryText = HR.HumanResource & " ( previously: " & HResource.FirstPreferredSurname.ToString & " )"
                    clr = System.Drawing.Color.Red
                  End If
                End If
                Dim itemProductionPK As Integer
                Dim hrProductionPK As Integer
                Int32.TryParse(HR.ProductionPK, itemProductionPK)
                Int32.TryParse(HR.ProductionPK, hrProductionPK)
                If Discipline = CommonData.Enums.Discipline.CameraOperator Then
                  If AlreadyParsed = False Then
                    'Write the positions to first column
                    For i = 0 To PositionIDs.Count - 1
                      SetText(ws, RowInd + StartRow + i, 0, PositionIDs(i).Key, ExcelDefaultableBoolean.True, , , , , )
                      ws.Rows(RowInd + StartRow + i).CellFormat.WrapText = ExcelDefaultableBoolean.True
                    Next
                    CameraPositionStart = RowInd + StartRow
                  End If
                  AlreadyParsed = True
                  Dim n = PositionIDs.Where(Function(c) c.Key = HR.PositionCode)(0)
                  If Not Singular.Misc.CompareSafe(n.Key, Nothing) Then
                    SetText(ws, CameraPositionStart + n.Value(3), ItemCount, IIf(HistoryText IsNot Nothing, HistoryText, HR.ToString), , , , clr, , )
                    ws.Rows(CameraPositionStart + n.Value(3)).CellFormat.WrapText = ExcelDefaultableBoolean.True
                    If HR.CrewLeaderInd AndAlso Singular.Misc.CompareSafe(HR.DisciplineID, CInt(Discipline)) Then
                      CrewLeaders &= HR.HumanResource & ", "
                    End If
                  Else
                    SkippedHR = True
                  End If
                Else
                  ' All other disciplines besides cam ops
                  If HR.DisciplineID = Discipline Then
                    SetText(ws, RowInd + StartRow, ItemCount, HR.ToString, , , , , , )
                    ws.Rows(RowInd + StartRow).CellFormat.WrapText = ExcelDefaultableBoolean.True
                    If HR.CrewLeaderInd AndAlso Singular.Misc.CompareSafe(HR.DisciplineID, CInt(Discipline)) Then
                      CrewLeaders &= HR.HumanResource & ", "
                    End If
                  End If
                End If
              End If
              RowInd += 1
              If Not SkippedHR Then
                HRForThisDisciplineProduction += 1
              End If
              '--------------------
            Next
            CrewLeaders = CrewLeaders.Trim
            If CrewLeaders.EndsWith(",") Then
              CrewLeaders = CrewLeaders.Substring(0, CrewLeaders.LastIndexOf(","))
            End If
            If CrewLeaders.Trim <> "" Then
              SetText(ws, CrewLeaderRowNumber, 0, "Crew Leaders", ExcelDefaultableBoolean.True, , , , , )
              SetText(ws, CrewLeaderRowNumber, ItemCount, CrewLeaders, , , , , , )
              FormatCells(ws, 0, list.Count, CrewLeaderRowNumber, CrewLeaderRowNumber, System.Drawing.Color.LightGray, FillPatternStyle.None)
            End If
            RowInd += ((MaxDisciplinesForHR.Item(Discipline) - HRForThisDisciplineProduction) + 1)
          Else
            If MaxDisciplinesForHR.ContainsKey(Discipline) Then
              RowInd += (MaxDisciplinesForHR.Item(Discipline) + 1)
            End If
          End If
          If Discipline = CommonData.Enums.Discipline.CameraOperator Then
            RowInd += 10
          End If
          If Not ReportCriteria.ExcludeOff Then
            For Each ROProductionScheduleOffResource As OBLib.Reports.ROProductionScheduleOffResource In ROProductionScheduleOffResourceList2.Where(Function(c) c.DisciplineID = Discipline)
              Select Case Discipline
                Case CommonData.Enums.Discipline.CameraOperator, CommonData.Enums.Discipline.Bashers, CommonData.Enums.Discipline.EVSOperator
                  SetText(ws, RowInd + StartRow, ItemCount, ROProductionScheduleOffResource.HumanResource + " (" + ROProductionScheduleOffResource.OffReason + ")", ExcelDefaultableBoolean.True, , , leavecolour, , )
                  ws.Rows(RowInd + StartRow).CellFormat.WrapText = ExcelDefaultableBoolean.True
                  RowInd += 1
                  OffHRPrevDisc += 1
                Case CommonData.Enums.Discipline.EventManager, CommonData.Enums.Discipline.EventManagerIntern
                  'do nothing
                  RowInd += 1
                Case Else
                  If Not String.IsNullOrEmpty(ROProductionScheduleOffResource.OffReason) Then
                    SetText(ws, RowInd + StartRow, ItemCount, ROProductionScheduleOffResource.HumanResource + " (" + ROProductionScheduleOffResource.OffReason + ")", ExcelDefaultableBoolean.True, , , leavecolour, , )
                    ws.Rows(RowInd + StartRow).CellFormat.WrapText = ExcelDefaultableBoolean.True
                    RowInd += 1
                    OffHRPrevDisc += 1
                  End If
              End Select
              SetText(ws, RowInd + StartRow - 1, 0, "Off / Leave", ExcelDefaultableBoolean.True, , , , , )
              list.HROffLeaveList.Add(New Helpers.ProductionScheduleHRLeave(item.ProductionID, ROProductionScheduleOffResource.HumanResource))
            Next
            RowInd += DisciplineMaxLeaveHRList.Where(Function(c) c.Key = Discipline)(0).Value + _
              (DisciplineMaxLeaveHRList.Where(Function(c) c.Key = Discipline)(0).Value - ROProductionScheduleOffResourcesPerProduction. _
               Where(Function(c) c.Key = item.ProductionID)(0).Value.Where(Function(d) d.DisciplineID = Discipline).Count)
          End If
        Next
        ItemCount += 1
        RowInd = 0
      Next
      AlreadyParsed = False
      Return wb

    End Function

    Private Sub SetText(ByVal ws As Worksheet, ByVal Row As Integer,
                        ByVal Column As Integer, ByVal Text As String, _
                           Optional ByVal Bold As ExcelDefaultableBoolean = ExcelDefaultableBoolean.Default, _
                           Optional ByVal FontName As String = "Arial",
                           Optional ByVal FontSize As Integer = 8, _
                           Optional ByVal TextColor As System.Drawing.Color? = Nothing,
                           Optional ByVal HAlign As HorizontalCellAlignment = HorizontalCellAlignment.Center, _
                           Optional ByVal FontUnderlineStyle As FontUnderlineStyle = FontUnderlineStyle.None, _
                           Optional ByVal VerticalAlignment As VerticalCellAlignment = VerticalCellAlignment.Default)

      If Integer.TryParse(Text, 0) Then
        ws.Rows(Row).Cells(Column).Value = CInt(Text)
      Else
        ws.Rows(Row).Cells(Column).Value = Text
      End If

      ws.Rows(Row).Cells(Column).CellFormat.Font.Bold = Bold
      ws.Rows(Row).Cells(Column).CellFormat.Font.Name = FontName
      ws.Rows(Row).Cells(Column).CellFormat.Font.Height = FontSize * 20
      If TextColor IsNot Nothing Then
        If TextColor.Value.IsKnownColor Then
          ws.Rows(Row).Cells(Column).CellFormat.Font.ColorInfo = New Infragistics.Documents.Excel.WorkbookColorInfo(TextColor)
        End If
      End If
      ws.Rows(Row).Cells(Column).CellFormat.Alignment = HAlign
      ws.Rows(Row).Cells(Column).CellFormat.Font.UnderlineStyle = FontUnderlineStyle
      ws.Rows(Row).Cells(Column).CellFormat.VerticalAlignment = VerticalAlignment

      Dim CellKey As String = ws.Rows(Row).Cells(Column).ToString
      Dim size = New System.Drawing.Size
      If Not ColumnWidths.ContainsKey(CellKey) Then
        size.Width = IIf(ws.Columns(Column).Width = -1, ws.DefaultColumnWidth, ws.Columns(Column).Width) / 256.0
        size.Height = IIf(ws.Rows(Row).Height = -1, ws.DefaultRowHeight, ws.Rows(Row).Height) / 20.0
        ColumnWidths.Add(CellKey, size)
      Else
        size = ColumnWidths(CellKey)
      End If

      '''dummy label to get a graphics object to measure the width (in pixels) of a string
      'Dim l As New System.Windows.Forms.Label
      'l.Text = Text
      'l.Size = size
      'Dim g As System.Drawing.Graphics = l.CreateGraphics
      'Dim s As System.Drawing.SizeF
      ''For Each cell As Infragistics.Win.UltraWinGrid.UltraGridCell In Row.Cells
      'Dim iFont = ws.Rows(Row).Cells(Column).CellFormat.Font
      'Dim windowsFont = New Drawing.Font(iFont.Name, iFont.Height / 20.0)
      's = g.MeasureString(Text, windowsFont)
      'size.Width = IIf(CInt(size.Width) < CInt(s.Width), CInt(s.Width), size.Width)

    End Sub

    Private Sub SetCellBorders(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, ByVal ReplaceExistingBorders As Boolean, _
                              ByVal TopBorderColor As System.Drawing.Color, ByVal BottomBorderColor As System.Drawing.Color, _
                              ByVal LeftBorderColor As System.Drawing.Color, ByVal RightBorderColor As System.Drawing.Color, _
                              Optional ByVal TopBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal BottomBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal LeftBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, Optional ByVal RightBorderStyle As CellBorderLineStyle = CellBorderLineStyle.Thin, _
                              Optional ByVal DiagonalBorder As DiagonalBorders = DiagonalBorders.None, Optional DiagonalBorderStyle As CellBorderLineStyle = CellBorderLineStyle.None,
                              Optional ByVal DiagonalBorderColor As System.Drawing.Color = Nothing)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.TopBorderStyle = TopBorderStyle
            ws.Rows(i).Cells(j).CellFormat.TopBorderColor = TopBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.BottomBorderStyle = BottomBorderStyle
            ws.Rows(i).Cells(j).CellFormat.BottomBorderColor = BottomBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.LeftBorderStyle = LeftBorderStyle
            ws.Rows(i).Cells(j).CellFormat.LeftBorderColor = LeftBorderColor
          End If

          If ReplaceExistingBorders OrElse ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = CellBorderLineStyle.Default Then
            ws.Rows(i).Cells(j).CellFormat.RightBorderStyle = RightBorderStyle
            ws.Rows(i).Cells(j).CellFormat.RightBorderColor = RightBorderColor
          End If

          ws.Rows(i).Cells(j).CellFormat.DiagonalBorders = DiagonalBorder
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderStyle = DiagonalBorderStyle
          ws.Rows(i).Cells(j).CellFormat.DiagonalBorderColor = DiagonalBorderColor

        Next
      Next
    End Sub

    Private Sub FormatCells(ByVal ws As Worksheet, ByVal ColumnFrom As Integer, ByVal ColumnTo As Integer, ByVal RowFrom As Integer, ByVal RowTo As Integer, _
                           ByVal FillColor As System.Drawing.Color, ByVal FillPattern As FillPatternStyle)

      For i As Integer = RowFrom To RowTo
        For j As Integer = ColumnFrom To ColumnTo
          ws.Rows(i).Cells(j).CellFormat.FillPatternBackgroundColor = FillColor
          Select Case FillPattern
            Case FillPatternStyle.Solid, FillPatternStyle.Default, FillPatternStyle.None
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = FillColor
            Case Else
              ws.Rows(i).Cells(j).CellFormat.FillPatternForegroundColor = System.Drawing.Color.Black
              ws.Rows(i).Cells(j).CellFormat.FillPattern = FillPattern
          End Select
        Next
      Next

    End Sub

  End Class

  Public Class MonthlyProductionCrewScheduleCriteria
    Inherits StartAndEndDateReportCriteria

    <Display(Name:="Production Type", Order:=1)>
    Public Property ProductionTypeID As Integer?

    <Display(Name:="Read Only Production Type List"), ClientOnly>
    Public Property ROProductionTypeList As List(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType) = OBLib.CommonData.Lists.ROProductionTypeList.ToList

    <Display(Name:="Read Only Event Type List Report List"), ClientOnly>
    Public Property ROEventTypeListReportList As List(Of ROEventTypeReport)

    <Display(Name:="Event Type", Order:=3)>
    Public Property EventTypeID As Integer?

    <Display(Name:="Event Type", Order:=4), ClientOnly>
    Public Property EventTypes As String = ""

    <Display(Name:="Exclude Off")>
    Public Property ExcludeOff As Boolean = False

    <Display(Name:="Exclude History")>
    Public Property ExcludeHistory As Boolean = False

    <Display(Name:="Sub-Dept."), Required(ErrorMessage:="Sub-Dept Required")>
    Public Property SystemID As Integer? = OBLib.Security.Settings.CurrentUser.SystemID

    <Display(Name:="Event Manager")>
    Public Property EventManagerHumanResourceID As Integer?

    <Display(Name:="Read Only Event Manager List"), ClientOnly>
    Public Property ROEventManagerList As List(Of OBLib.HR.ReadOnly.ROEventManager) = OBLib.CommonData.Lists.ROEventManagerList.ToList

  End Class

  Public Class MonthlyProductionCrewScheduleCriteriaControl
    Inherits Singular.Web.Reporting.CriteriaControlBase

    Protected Overrides Sub Setup()
      MyBase.Setup()

      With Helpers.With(Of MonthlyProductionCrewScheduleCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
        With .Helpers.FieldSet("Date Selection")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "Start Date"
                End With
              End With
              With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                .AddClass("date-picker-reports")
                .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                  .Editor.Attributes("placeholder") = "End Date"
                End With
              End With
            End With
          End With
        End With
        With .Helpers.FieldSet("Criteria")
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 6, 6, 3, 2)
              With .Helpers.FieldSet("Sub-Depts")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "MonthlyProductionCrewScheduleReport.AddSystem($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                    .Button.AddClass("btn-block buttontext")
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Event Managers")
                .AddClass("add-vertical-scroll")
                With .Helpers.ForEachTemplate(Of OBLib.HR.ReadOnly.ROEventManager)(Function(d) d.ROEventManagerList)
                  With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                    With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                      .Button.AddBinding(KnockoutBindingString.click, "MonthlyProductionCrewScheduleReport.AddEventManager($data)")
                      .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.PreferredFirstSurname)
                      .Button.AddClass("btn-block buttontext")
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                  With .Helpers.FieldSet("Production Types")
                    .Attributes("id") = "Productions"
                    With .Helpers.Div
                      With .Helpers.Div
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                            With .Helpers.With(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport.Criteria)("ViewModel.ROProductionTypeReportPagedListCriteria()")
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord(), Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search Production Types")
                              End With
                            End With
                          End With
                        End With
                      End With
                      With .Helpers.DivC("add-vertical-scroll-600")
                        With .Helpers.Bootstrap.Row
                          With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType)(Function(d) d.ROProductionTypeList)
                            With .Helpers.Bootstrap.Column(12, 12, 4, 4)
                              With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                                .Button.AddBinding(KnockoutBindingString.click, "MonthlyProductionCrewScheduleReport.AddProductionType($data)")
                                .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionType)
                                .Button.AddClass("btn-block buttontext")
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.Bootstrap.Row
                    With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                      With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.StateButtonNew(Function(d) d.ExcludeOff, "Exclude Off", "Exclude Off", , , , , )
                            .Button.AddClass("btn-block button-text")
                          End With
                        End With
                        With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                          With .Helpers.Bootstrap.StateButtonNew(Function(d) d.ExcludeHistory, "Exclude History", "Exclude History", , , , , )
                            .Button.AddClass("btn-block button-text")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
                With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                  With .Helpers.FieldSet("Event Types")
                    With .Helpers.Div
                      With .Helpers.Div
                        With .Helpers.Bootstrap.Row
                          With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                            With .Helpers.With(Of OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport.Criteria)("ViewModel.ROEventTypeReportPagedListCriteria()")
                              With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord(), Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search Event Types")
                              End With
                            End With
                          End With
                        End With
                        With .Helpers.DivC("add-vertical-scroll-600")
                          With .Helpers.Bootstrap.Row
                            With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROEventType)(Function(d) d.ROEventTypeListReportList)
                              With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                                With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                                  .Button.AddBinding(KnockoutBindingString.click, "MonthlyProductionCrewScheduleReport.AddEventType($data)")
                                  .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.EventType)
                                  .Button.AddClass("btn-block buttontext")
                                End With
                              End With
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With

              End With
            End With
          End With
        End With
        With .Helpers.DivC("loading-custom")
          .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
          .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
        End With
      End With
    End Sub
  End Class

#End Region

#Region "Production Overview"

  Public Class ProductionOverview
    Inherits ReportBase(Of ProductionOverviewCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Production Overview"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptProductionOverview"
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(ProductionOverviewCriteriaControl)
      End Get
    End Property

    Protected Overrides Sub ModifyDataSet(DataSet As DataSet)
      MyBase.ModifyDataSet(DataSet)

      DataSet.Tables(0).DefaultView.Sort = "SortOrder ASC"
      DataSet.Tables(1).DefaultView.Sort = "SortOrder ASC"
      DataSet.Tables(2).DefaultView.Sort = "SortOrder ASC"

      DataSet.Tables(0).ChildRelations.Add("ProductionID", New DataColumn() {DataSet.Tables(0).Columns("ProductionID")}, _
                                                          New DataColumn() {DataSet.Tables(1).Columns("ProductionID")})

      DataSet.Tables(1).ChildRelations.Add("ProductionSystemAreaID", New DataColumn() {DataSet.Tables(1).Columns("ProductionSystemAreaID")}, _
                                                                     New DataColumn() {DataSet.Tables(2).Columns("ProductionSystemAreaID")})

      For Each table As DataTable In DataSet.Tables
        If table.TableName <> "Information" Then
          For Each col In table.Columns
            Select Case col.ToString
              Case "ProductionID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "ProductionSystemAreaID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "RoomScheduleID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "ProductionHumanResourceID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "DisciplineID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "PositionID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "HumanResourceID"
                col.ExtendedProperties.Add("AutoGenerate", False)
              Case "SortOrder"
                col.ExtendedProperties.Add("AutoGenerate", False)
            End Select
          Next
        End If
      Next

    End Sub

    Public Class ProductionOverviewCriteria
      Inherits StartAndEndDateReportCriteria

      <Display(Name:="Production Type", Order:=1)>
      Public Property ProductionTypeID As Integer?

      <Display(Name:="Read Only Production Type List"), ClientOnly>
      Public Property ROProductionTypeList As List(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType) = OBLib.CommonData.Lists.ROProductionTypeList.ToList

      <Display(Name:="Read Only Event Type List Report List"), ClientOnly>
      Public Property ROEventTypeListReportList As List(Of ROEventTypeReport)

      <Display(Name:="Event Type", Order:=3)>
      Public Property EventTypeID As Integer?

      <Display(Name:="Event Type", Order:=4), ClientOnly>
      Public Property EventTypes As String = ""

      <Display(Name:="Room"), Singular.DataAnnotations.DropDownWeb(GetType(RORoomList), UnselectedText:="Room")>
      Public Property RoomID As Integer?

    End Class

    Public Class ProductionOverviewCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase

      Protected Overrides Sub Setup()
        MyBase.Setup()
        With Helpers.With(Of ProductionOverviewCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.FieldSet("Date Selection")
            With .Helpers.Bootstrap.Row
              With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.FieldSet("Criteria")
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Production Types")
                .Attributes("id") = "Productions"
                With .Helpers.Div
                  With .Helpers.Div
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                        With .Helpers.With(Of OBLib.Maintenance.Productions.ReadOnly.ROProductionTypeListReport.Criteria)("ViewModel.ROProductionTypeReportPagedListCriteria()")
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord(), Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search Production Types")
                          End With
                        End With
                      End With
                    End With
                  End With
                  With .Helpers.DivC("add-vertical-scroll-600")
                    With .Helpers.Bootstrap.Row
                      With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROProductionType)(Function(d) d.ROProductionTypeList)
                        With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                          With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                            .Button.AddBinding(KnockoutBindingString.click, "ProductionOverview.AddProductionType($data)")
                            .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ProductionType)
                            .Button.AddClass("btn-block buttontext")
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 6)
              With .Helpers.FieldSet("Event Types")
                With .Helpers.Div
                  With .Helpers.Div
                    With .Helpers.Bootstrap.Row
                      With .Helpers.Bootstrap.Column(12, 8, 6, 6)
                        With .Helpers.With(Of OBLib.Maintenance.Productions.ReadOnly.ROEventTypeListReport.Criteria)("ViewModel.ROEventTypeReportPagedListCriteria()")
                          With .Helpers.Bootstrap.FormControlFor(Function(d) d.KeyWord(), Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall, , "Search Event Types")
                          End With
                        End With
                      End With
                    End With
                    With .Helpers.DivC("add-vertical-scroll-600")
                      With .Helpers.Bootstrap.Row
                        With .Helpers.ForEachTemplate(Of OBLib.Maintenance.Productions.ReadOnly.Old.ROEventType)(Function(d) d.ROEventTypeListReportList)
                          With .Helpers.Bootstrap.Column(12, 12, 6, 6)
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", , , )
                              .Button.AddBinding(KnockoutBindingString.click, "ProductionOverview.AddEventType($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.EventType)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 12, 12)
              With .Helpers.Bootstrap.Row
                With .Helpers.Bootstrap.Column(12, 12, 3, 3)
                  With .Helpers.Bootstrap.FormGroup(BootstrapEnums.FormGroupSize.Small)
                    With .Helpers.Bootstrap.FormControlFor(Function(d) d.RoomID, Singular.Web.BootstrapEnums.ButtonSize.ExtraSmall)
                      .Editor.Attributes("placeholder") = "Room"
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub
    End Class
  End Class

#End Region

#Region "RoomScheduleGrid"

  Public Class RoomScheduleGrid
    Inherits ReportBase(Of RoomScheduleGridCriteria)

    Public Overrides ReadOnly Property ReportName As String
      Get
        Return "Room Schedule Grid"
      End Get
    End Property

    Protected Overrides Sub SetupCommandProc(cmd As CommandProc)
      cmd.CommandText = "RptProcs.rptRoomBookingsGrid"
      cmd.CommandTimeout = 0
    End Sub

    Public Overrides ReadOnly Property CustomCriteriaControlType As Type
      Get
        Return GetType(RoomScheduleGridCriteriaControl)
      End Get
    End Property

    Protected Overrides Function CreateExcelWorkbook(Data As DataSet) As Workbook

      Dim wb As New Workbook(WorkbookFormat.Excel2007)
      Dim ws As Worksheet = wb.Worksheets.Add("Room Booking Grid Report")

      ws.Rows(0).Cells(0).Value = If(Data.Tables(1).Rows.Count > 0, "", "No Data")

      Dim Col As Integer = 1

      Dim HRCol As Integer = 0

      Dim ColIndex As Integer = 2

      Dim RowIndex As Integer = 1

      If Data.Tables(1).Rows.Count > 0 Then
        'For when the report has employees
        If Data.Tables(2).Rows.Count > 0 Then
          'Table 0 = Shows distinct dates used from top row
          'Table 1 = Shows which events are on each day
          'Table 2 = Shows the HR details
          'Table 3 = Shows the count of the HR
          'Table 4 = Shows the number of events per day
          'Table 5 = Shows the maximum number of events per the date selection
          'Table 6 = Discipline List that will need to be looped through
          'Table 7 = Contract Type List that is selected for the report
          'Table 8 = HR List which contains the Discipline they are skilled in and their Contract Type

          Dim CurrentDate As Date = Data.Tables(1).Rows(0)("CallTimeDate")
          Dim CurrentContractTypeID As Integer = Data.Tables(2).Rows(0)("ContractTypeID")
          Dim CurrentEmployeeID As Integer = Data.Tables(2).Rows(0)("HumanResourceID")
          Dim FullNameCount As Integer = Data.Tables(3).Rows(0)("FullNameCount")
          Dim EventsPerDay As String = Data.Tables(5).Rows(0)("MaxNumberOfEventsPerDay")
          Dim MustHighlight As Boolean = Data.Tables(1).Rows(0)("MustHighlight")
          Dim DisciplineID As Integer = Data.Tables(6).Rows(0)("DisciplineID")
          Dim ContractTypeID As Integer = Data.Tables(7).Rows(0)("ContractTypeID")
          Dim FullName As String = Data.Tables(8).Rows(0)("FullName")

          ColIndex = 2

          'Sets the top row to just a day of the week and the day of the month
          For Each row In Data.Tables(0).Rows
            Dim ColDate As Date = row("CallTimeDate")
            ws.Rows(0).Cells(ColIndex).Value = ColDate.ToString("ddd dd")
            ColIndex += 1
          Next

          'Sets the borders for the entire worksheet
          For i As Integer = 1 To Data.Tables(0).Rows.Count + 1
            For j As Integer = 0 To EventsPerDay + Data.Tables(8).Rows.Count + (Data.Tables(7).Rows.Count * 3)
              ws.Rows(j).Cells(i).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
              ws.Rows(j).Cells(i).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
              ws.Rows(j).Cells(i).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
              ws.Rows(j).Cells(i).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
            Next
          Next

          'Sets the column width for the HR so that names can be read
          For HRCol = 0 To 0
            ws.Columns(HRCol).Width = 8000
          Next

          'Sets the dates in the top row to be bolded and centred
          'sets the column width of the rows which have data in them
          'Sets the font size to be 12
          With ws.Rows(RowIndex = 0)
            For i As Integer = 1 To Data.Tables(0).Rows.Count
              ws.DefaultColumnWidth = 5000
              .Cells(i).CellFormat.Alignment = HorizontalCellAlignment.Center
              .Cells(i).CellFormat.Font.Bold = ExcelDefaultableBoolean.True
              .Cells(i).CellFormat.Font.Height = 240
            Next
          End With

          'Links each event to its date given in row 1(Excel)
          'Iterates through the list of event until the date is no longer equal to columns date
          'When no longer equal to column date, moves to the next column until all columns are 
          'filled with the correct data
          ColIndex = 2
          For Each row In Data.Tables(1).Rows
            If row("CallTimeDate") <> CurrentDate Then
              RowIndex = 1
              ColIndex += 1
              CurrentDate = row("CallTimeDate")
            End If
            If row("MustHighlight") = True Then
              ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(FromArgb(193, 240, 246))
            End If
            Dim eventName = row("EventTime")
            If eventName <> "" Then
              ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Alignment = HorizontalCellAlignment.Fill
            End If
            ws.Rows(RowIndex).Cells(ColIndex).Value = eventName
            RowIndex += 1

            With ws.Rows(RowIndex - 1)
              .Cells(ColIndex).CellFormat.Font.Height = 180
            End With
          Next

          ColIndex = 0

          RowIndex = EventsPerDay + 2
          'Links to contractType data

          For Each DisciplineRow In Data.Tables(6).Rows
            ColIndex = 0
            ws.Rows(RowIndex).Cells(ColIndex).Value = DisciplineRow("Discipline")
            ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Bold = ExcelDefaultableBoolean.True
            ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Name = "Century Gothic"
            ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Height = 240
            RowIndex += 1

            'Places the contract types as values in the cells
            'Sets font to be century gothic
            'Sets font size to be 12
            ColIndex = 1
            For Each ContractTypeRow As DataRow In Data.Tables(7).Select("DisciplineID =" & DisciplineRow(1))
              ws.Rows(RowIndex).Cells(ColIndex).Value = ContractTypeRow("ContractType")
              ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Bold = ExcelDefaultableBoolean.True
              ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Name = "Century Gothic"
              ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Height = 240
              RowIndex += 1

              For Each FN As DataRow In Data.Tables(8).Select("DisciplineID = " & DisciplineRow(1) & "AND ContractTypeID =" & ContractTypeRow(1))
                ws.Rows(RowIndex).Cells(ColIndex).Value = FN("FullName")
                ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Name = "Century Gothic"
                ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Font.Height = 180
                RowIndex += 1
              Next

              'gives a 2 cell gap between each contract type
              RowIndex += 1
            Next
          Next

          'Freezes Top Row
          ws.DisplayOptions.PanesAreFrozen = True
          ws.DisplayOptions.FrozenPaneSettings.FrozenRows = 1

          'Runs the report when there are no employees 
        ElseIf Data.Tables(2).Rows.Count = 0 Then
          ColIndex = 0

          For Each row In Data.Tables(0).Rows
            Dim ColDate As Date = row("CallTimeDate")
            ws.Rows(0).Cells(ColIndex).Value = ColDate.ToString("ddd dd")
            ColIndex += 1
          Next

          Dim CurrentDate As Date = Data.Tables(1).Rows(0)("CallTimeDate")
          Dim FullNameCount As Integer = Data.Tables(3).Rows(0)("FullNameCount")
          Dim EventsPerDay As String = Data.Tables(5).Rows(0)("MaxNumberOfEventsPerDay")
          Dim MustHighlight As Boolean = Data.Tables(1).Rows(0)("MustHighlight")

          'Sets the borders for the entire worksheet
          For i As Integer = 0 To Data.Tables(0).Rows.Count - 1
            For j As Integer = 0 To EventsPerDay
              ws.Rows(j).Cells(i).CellFormat.BottomBorderStyle = CellBorderLineStyle.Thin
              ws.Rows(j).Cells(i).CellFormat.TopBorderStyle = CellBorderLineStyle.Thin
              ws.Rows(j).Cells(i).CellFormat.LeftBorderStyle = CellBorderLineStyle.Thin
              ws.Rows(j).Cells(i).CellFormat.RightBorderStyle = CellBorderLineStyle.Thin
            Next
          Next

          'Sets the dates in the top row to be bolded and centred
          'sets the column width of the rows which have data in them
          'Sets the font size to be 12
          With ws.Rows(RowIndex = 0)
            For i As Integer = 0 To Data.Tables(0).Rows.Count
              ws.DefaultColumnWidth = 5000
              .Cells(i).CellFormat.Alignment = HorizontalCellAlignment.Center
              .Cells(i).CellFormat.Font.Bold = ExcelDefaultableBoolean.True
              .Cells(i).CellFormat.Font.Height = 240
            Next
          End With

          'Links each event to its date given in row 1(Excel)
          'Iterates through the list of event until the date is no longer equal to columns date
          'When no longer equal to column date, moves to the next column until all columns are 
          'filled with the correct data
          ColIndex = 0
          For Each row In Data.Tables(1).Rows
            If row("CallTimeDate") <> CurrentDate Then
              RowIndex = 1
              ColIndex += 1
              CurrentDate = row("CallTimeDate")
            End If
            If row("MustHighlight") = True Then
              ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Fill = Infragistics.Documents.Excel.CellFill.CreateSolidFill(FromArgb(193, 240, 246))
            End If
            Dim eventName = row("EventTime")
            If eventName <> "" Then
              ws.Rows(RowIndex).Cells(ColIndex).CellFormat.Alignment = HorizontalCellAlignment.Fill
            End If
            ws.Rows(RowIndex).Cells(ColIndex).Value = eventName
            RowIndex += 1

            With ws.Rows(RowIndex - 1)
              .Cells(ColIndex).CellFormat.Font.Height = 180
            End With
          Next

        End If
      End If
      Return wb

    End Function

    Public Class RoomScheduleGridCriteria
      Inherits StartAndEndDateReportCriteria

      <Display(Name:="Sub-Depts", Order:=1), ClientOnly>
      Public Property SystemIDs As List(Of Integer)

      <Display(Name:="Areas", Order:=2), ClientOnly>
      Public Property ProductionAreaIDs As List(Of Integer)

      <Display(Name:="Contract Types", Order:=3), ClientOnly>
      Public Property ROContractTypeList As List(Of ROContractType) = OBLib.CommonData.Lists.ROContractTypeList.ToList

      <Display(Name:="Contract Types", Order:=4)>
      Public Property ContractTypeIDs As List(Of Integer)

      <Display(Name:="Disciplines", Order:=5), ClientOnly>
      Public Property RODisciplineList As List(Of RODisciplineReport) = New List(Of RODisciplineReport)

      <Display(Name:="Disciplines", Order:=6)>
      Public Property DisciplineIDs As List(Of Integer)

      <Display(Name:="Rooms", Order:=7)>
      Public Property RoomIDs As List(Of Integer)

      <Display(Name:="Room Type List", Order:=8), ClientOnly>
      Public Property RORoomTypeReportList As List(Of RORoomTypeReport) = OBLib.CommonData.Lists.RORoomTypeReportList.ToList

    End Class

    Public Class RoomScheduleGridCriteriaControl
      Inherits Singular.Web.Reporting.CriteriaControlBase

      Protected Overrides Sub Setup()
        MyBase.Setup()
        With Helpers.With(Of RoomScheduleGridCriteria)(Function(c) c.Report.ReportCriteriaGeneric)
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
              With .Helpers.FieldSet("Date Selection")
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.StartDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.StartDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "Start Date"
                  End With
                End With
                With .Helpers.Bootstrap.FormGroup(Singular.Web.BootstrapEnums.FormGroupSize.Small)
                  .AddClass("date-picker-reports")
                  .Helpers.Bootstrap.LabelFor(Function(d) d.EndDate)
                  With .Helpers.Bootstrap.FormControlFor(Function(d) d.EndDate, Singular.Web.BootstrapEnums.InputSize.Small)
                    .Editor.Attributes("placeholder") = "End Date"
                  End With
                End With
              End With
            End With
          End With
          'Row 2-----------------------------------------------
          '----------------------------------------------------
          With .Helpers.Bootstrap.Row
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.FieldSet("Sub-Depts and Areas")
                With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystem)("ViewModel.UserSystemList()")
                  With .Helpers.Bootstrap.StateButtonNew(Function(c As OBLib.Security.UserSystem) c.IsSelected(), "", "", "btn-primary", "btn-default", "fa fa-check-square-o", , )
                    .Button.AddBinding(KnockoutBindingString.click, "RoomBookingGridReport.AddSystem($data)")
                    .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystem) c.SystemName)
                    .Button.AddClass("btn-block buttontext")
                  End With
                  With .Helpers.Bootstrap.Row
                    .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                    With .Helpers.ForEachTemplate(Of OBLib.Security.UserSystemArea)("$data.UserSystemAreaList()")
                      With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                        .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                        With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa fa-check-square-o", , )
                          .Button.AddBinding(KnockoutBindingString.click, "RoomBookingGridReport.AddProductionArea($data)")
                          .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As OBLib.Security.UserSystemArea) c.ProductionArea)
                          .Button.AddClass("btn-block buttontext")
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.FieldSet("Contract Types")
                With .Helpers.Bootstrap.Row
                  With .Helpers.ForEachTemplate(Of ROContractType)(Function(d) d.ROContractTypeList)
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "RoomBookingGridReport.AddContractType($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.ContractType)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 2)
              With .Helpers.FieldSet("Rooms")
                With .Helpers.Bootstrap.Row
                  With .Helpers.ForEachTemplate(Of RORoomTypeReport)(Function(d) d.RORoomTypeReportList)
                    With .Helpers.Bootstrap.Column(12, 12, 12, 12, 12)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "RoomBookingGridReport.RORoomTypeReportSelected($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As RORoomTypeReport) c.RoomType)
                        .Button.AddClass("btn-block buttontext")
                      End With
                      With .Helpers.Bootstrap.Row
                        .AddBinding(KnockoutBindingString.if, "$data.IsSelected()")
                        With .Helpers.ForEachTemplate(Of RORoomTypeReportRoom)("$data.RORoomTypeReportRoomList()")
                          With .Helpers.Bootstrap.Column(10, 10, 10, 10, 10)
                            .AddClass("col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xl-offset-2")
                            With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa-check-square-o", , )
                              .Button.AddBinding(KnockoutBindingString.click, "RoomBookingGridReport.AddRoom($data)")
                              .ButtonText.AddBinding(KnockoutBindingString.html, Function(c As RORoomTypeReportRoom) c.Room)
                              .Button.AddClass("btn-block buttontext")
                            End With
                          End With
                        End With
                      End With
                    End With
                  End With
                End With
              End With
            End With
            With .Helpers.Bootstrap.Column(12, 12, 6, 3, 6)
              With .Helpers.FieldSet("Disciplines")
                With .Helpers.Bootstrap.Row
                  With .Helpers.ForEachTemplate(Of RODisciplineReport)(Function(d) d.RODisciplineList)
                    With .Helpers.Bootstrap.Column(12, 12, 12, 4, 4)
                      With .Helpers.Bootstrap.StateButtonNew(Function(c) c.IsSelected(), "", "", "btn-success", "btn-default", "fa-check-square-o", , )
                        .Button.AddBinding(KnockoutBindingString.click, "RoomBookingGridReport.AddDiscipline($data)")
                        .ButtonText.AddBinding(KnockoutBindingString.html, Function(c) c.Discipline)
                        .Button.AddClass("btn-block buttontext")
                      End With
                    End With
                  End With
                End With
              End With
            End With
          End With
          With .Helpers.DivC("loading-custom")
            .AddBinding(Singular.Web.KnockoutBindingString.visible, "ViewModel.IsReportBusy()")
            .Helpers.Bootstrap.FontAwesomeIcon("fa fa-refresh fa-spin", "fa-5x")
          End With
        End With
      End Sub

    End Class
  End Class
#End Region

End Class
